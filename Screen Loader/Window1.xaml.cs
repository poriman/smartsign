﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Forms;
using System.Reflection;

using WPFDesigner;

namespace Screen_Loader
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        OpenFileDialog ofd;
        public Window1()
        {
            InitializeComponent();
            ofd = new OpenFileDialog();
            ofd.Filter = "Screen|*.bin";
			String sExecutingPath = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
			ofd.InitialDirectory = sExecutingPath + "\\DesignerData\\Screens";
        }

        private void openScreen1_Click(object sender, RoutedEventArgs e)
        {
            grid1.Children.Clear();
            ofd.ShowDialog();
            grid1.Children.Add(ProjectManager.LoadScreen(ofd.FileName));
            WPFDesigner.Screen screen = grid1.Children[0] as WPFDesigner.Screen;
            screen.FillParent();
            screen.EditingMode = InkCanvasEditingMode.None;
        }

        private void openScreen2_Click(object sender, RoutedEventArgs e)
        {
            ofd.ShowDialog();
            grid2.Children.Clear();
            WPFDesigner.Screen screen = ProjectManager.LoadScreen(ofd.FileName);
            grid2.Width = screen.Width;
            grid2.Height = screen.Height;
            grid2.Children.Add(screen);
            screen.FillParent();
            grid2.UpdateLayout();
            grid2.VerticalAlignment = VerticalAlignment.Stretch;
            grid2.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
            grid2.Height = Double.NaN;
            grid2.Width = Double.NaN;
        }

        private void playButton_Click(object sender, RoutedEventArgs e)
        {
            if (grid1.Children.Count > 0)
            {
                ((IDesignElement)((WPFDesigner.Screen)grid1.Children[0])).Play();
            }

            if (grid2.Children.Count > 0)
            {
                ((IDesignElement)((WPFDesigner.Screen)grid2.Children[0])).Play();
            }
        }

        private void stopButton_Click(object sender, RoutedEventArgs e)
        {
            if (grid1.Children.Count > 0)
            {
                ((IDesignElement)((WPFDesigner.Screen)grid1.Children[0])).Stop();
            }

            if (grid2.Children.Count > 0)
            {
                ((IDesignElement)((WPFDesigner.Screen)grid2.Children[0])).Stop();
            }
        }

        private void fillButton_Click(object sender, RoutedEventArgs e)
        {
            grid2.Height = Double.NaN;
            grid2.Width = Double.NaN;
            grid2.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
            grid2.VerticalAlignment = VerticalAlignment.Stretch;
        }
    }
}
