﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace Smil.Library
{
	#region Structure Modules
	/// <summary>
	/// Element Base
	/// </summary>
	public class BaseObject
	{
		#region Core Attributes
		[XmlAttribute(AttributeName = "alt")]
		public String alt { get; set; }
		[XmlAttribute(AttributeName = "baseProfile")]
		[DefaultValueAttribute(Profile.Language)]
		public Profile baseProfile { get; set; }
		[XmlAttribute(AttributeName = "class")]
		public String Class { get; set; }
		[XmlAttribute(AttributeName = "label")]
		public String label { get; set; }
		[XmlAttribute(AttributeName = "longdesc")]
		public String longdesc { get; set; }
		[XmlAttribute(AttributeName = "readIndex")]
		[DefaultValueAttribute(0)]
		public int readIndex { get; set; }
		[XmlAttribute(AttributeName = "title")]
		public String title { get; set; }
		[XmlAttribute(AttributeName = "version")]
		[DefaultValueAttribute(Version.ver30)]
		public Version version { get; set; }
		[XmlAttribute(AttributeName = "xml:base")]
		public String xmlbase { get; set; }
		[XmlAttribute(AttributeName = "xml:id")]
		public String id { get; set; }
		#endregion

		#region I18n Attributes
		//[XmlAttribute(AttributeName = "its:dir")]
		//public itsdir itsdir { get; set; }
		//[XmlAttribute(AttributeName = "its:locNote")]
		//public String itslocNote { get; set; }
		//[XmlAttribute(AttributeName = "its:locNoteRef")]
		//public String itslocNoteRef { get; set; }
		//[XmlAttribute(AttributeName = "its:locNoteType")]
		//public itslocNoteType itslocNoteType { get; set; }
		//[XmlAttribute(AttributeName = "its:term")]
		//public bool itsterm { get; set; }
		//[XmlAttribute(AttributeName = "its:termInfoRef")]
		//public String itstermInfoRef { get; set; }
		//[XmlAttribute(AttributeName = "its:translate")]
		//public bool itstranslate { get; set; }
		[XmlAttribute(AttributeName = "xml:lang")]
		public String lang { get; set; }

		#endregion

		public BaseObject()
		{
			baseProfile = Profile.Language;
			version = Version.ver30;
			readIndex = 0;
		}
	}

	/// <summary>
	/// Test Object Element
	/// </summary>
	public class TestObjectInheritedBase : BaseObject
	{
		#region Test Attributes
		[XmlAttribute(AttributeName = "system-bitrate")]
		public String system_bitrate { get; set; }
		[XmlAttribute(AttributeName = "system-captions")]
		public String system_captions { get; set; }
		[XmlAttribute(AttributeName = "system-language")]
		public String system_language { get; set; }
		[XmlAttribute(AttributeName = "systemoverdub-or-caption")]
		public String systemoverdub_or_caption { get; set; }
		[XmlAttribute(AttributeName = "system-required")]
		public String system_required { get; set; }
		[XmlAttribute(AttributeName = "systemscreen-depth")]
		public String systemscreen_depth { get; set; }
		[XmlAttribute(AttributeName = "system-screen-size")]
		public String system_screen_size { get; set; }
		[XmlAttribute(AttributeName = "systemAudioDesc")]
		public String systemAudioDesc { get; set; }
		[XmlAttribute(AttributeName = "systemBaseProfile")]
		public String systemBaseProfile { get; set; }
		[XmlAttribute(AttributeName = "systemBitrate")]
		public String systemBitrate { get; set; }
		[XmlAttribute(AttributeName = "systemCPU")]
		public String systemCPU { get; set; }
		[XmlAttribute(AttributeName = "systemCaptions")]
		public String systemCaptions { get; set; }
		[XmlAttribute(AttributeName = "systemComponent")]
		public String systemComponent { get; set; }
		[XmlAttribute(AttributeName = "systemLanguage")]
		public String systemLanguage { get; set; }
		[XmlAttribute(AttributeName = "systemOperatingSystem")]
		public String systemOperatingSystem { get; set; }
		[XmlAttribute(AttributeName = "systemOverdubOrSubtitle")]
		public String systemOverdubOrSubtitle { get; set; }
		[XmlAttribute(AttributeName = "systemRequired")]
		public String systemRequired { get; set; }
		[XmlAttribute(AttributeName = "systemScreenDepth")]
		public String systemScreenDepth { get; set; }
		[XmlAttribute(AttributeName = "systemScreenSize")]
		public String systemScreenSize { get; set; }
		[XmlAttribute(AttributeName = "systemVersion")]
		[DefaultValueAttribute("3.0")]
		public String systemVersion { get; set; }

		#endregion

		public TestObjectInheritedBase()
		{
			#region Test Attributes
			systemVersion = "3.0";
			#endregion
		}
	}

	/// <summary>
	/// SMIL Element
	/// </summary>
	[XmlRoot(ElementName = "smil", Namespace="http://www.w3.org/ns/SMIL"), Serializable]
	public class Smil : TestObjectInheritedBase
	{
		[XmlAttribute(AttributeName = "xmlns")]
		public String xmlns { get; set; }
		//[XmlAttribute(AttributeName = "xmlns:its", Namespace = "http://www.w3.org/2005/11/its")]
		//public String xmlnsits { get; set; }

		[XmlElement(ElementName = "head", IsNullable = true)]
		public Head head { get; set; }
		[XmlElement(ElementName = "body", IsNullable = false)]
		public Body body { get; set; }

		[XmlElement(ElementName = "metadata", IsNullable = true)]
		public Collection<Metadata> arrMetadataCollection = new Collection<Metadata>();

		/// <summary>
		/// 생성자
		/// </summary>
		public Smil()
		{
			//			xmlnsits = "http://www.w3.org/2005/11/its";
		}

		/// <summary>
		/// 스크린의 총 시간을 반환한다.
		/// </summary>
		/// <returns>총 시간 Timespan</returns>
		public TimeSpan TotalDuration()
		{
			try
			{
				Par screen = body.arrCollection.Single(par => par is Par) as Par;

				return TimeSpan.FromSeconds(Library.Convert.ToInt32(screen.dur));

			}
			catch {
				return TimeSpan.Zero;
			}
		}

		public int Width()
		{
			try
			{
				RootLayout root = this.head.layout.arrCollection.Single(t => t is RootLayout) as RootLayout;

				return Convert.ToInt32(root.width);
			}
			catch { return 800; }
		}

		public int Height()
		{
			try
			{
				RootLayout root = this.head.layout.arrCollection.Single(t => t is RootLayout) as RootLayout;
				
				return Convert.ToInt32(root.height);
			}
			catch { return 600; }
		}

		public void SaveToSmil(string filepath)
		{
			using(System.IO.TextWriter tr = new System.IO.StreamWriter(filepath))
			{
				System.Xml.Serialization.XmlSerializer sr = new System.Xml.Serialization.XmlSerializer(typeof(Smil));
				sr.Serialize(tr, this);
				tr.Close();
				tr.Dispose();
			}
		}

		public static Smil LoadObjectFromSmilFile(string filepath)
		{
			Smil objReturn;
			using (System.IO.TextReader tr = new System.IO.StreamReader(filepath))
			{
				System.Xml.Serialization.XmlSerializer sr = new System.Xml.Serialization.XmlSerializer(typeof(Smil));
				objReturn = sr.Deserialize(tr) as Smil;
				tr.Close();
				tr.Dispose();
			}

			return objReturn;
		}
	}

	/// <summary>
	/// Head Element
	/// </summary>
	[XmlRoot(ElementName = "head", Namespace = "http://www.w3.org/ns/SMIL"), Serializable]
	public class Head : BaseObject
	{
		[XmlElement(ElementName = "layout", IsNullable = true)]
		public Layout layout { get; set; }

		[XmlElement(ElementName = "meta", IsNullable = true)]
		public Collection<Meta> arrMetaCollection = new Collection<Meta>();

		[XmlElement(ElementName = "metadata", IsNullable = true)]
		public Collection<Metadata> arrMetadataCollection = new Collection<Metadata>();

		[XmlElement(ElementName = "customAttributes", IsNullable = true)]
		public Collection<CustomAttributes> arrCustomAttributesCollection = new Collection<CustomAttributes>();

		[XmlElement(ElementName = "textStyling", IsNullable = true)]
		public Collection<TextStyling> arrTextStylingCollection = new Collection<TextStyling>();

		[XmlElement(ElementName = "switch", IsNullable = true)]
		public Collection<Switch> arrSwitchCollection = new Collection<Switch>();

		[XmlElement(ElementName = "state", IsNullable = true)]
		public Collection<State> arrStateCollection = new Collection<State>();

		[XmlElement(ElementName = "submission", IsNullable = true)]
		public Collection<Submission> arrSubmissionCollection = new Collection<Submission>();
		
		[XmlElement(ElementName = "transition", IsNullable = true)]
		public Collection<Transition> arrTransitionCollection = new Collection<Transition>();

		[XmlElement(ElementName = "paramGroup", IsNullable = true)]
		public Collection<ParamGroup> arrParamGroupCollection = new Collection<ParamGroup>();

	}

	/// <summary>
	/// Body Element
	/// </summary>
	[XmlRoot(ElementName = "body", Namespace = "http://www.w3.org/ns/SMIL"), Serializable]
	public class Body : BaseObject
	{
		#region Default Attribute
		[XmlAttribute(AttributeName = "fill")]
		[DefaultValueAttribute(Fill.Default)]
		public Fill fill { get; set; }
		#endregion

		#region MediaDescription Attributes
		[XmlAttribute(AttributeName = "abstract")]
		public String Abstract { get; set; }
		[XmlAttribute(AttributeName = "author")]
		public String author { get; set; }
		[XmlAttribute(AttributeName = "copyright")]
		public String copyright { get; set; }
		#endregion

		#region Timing Attributes
		[XmlAttribute(AttributeName = "begin")]
		public String begin { get; set; }
		[XmlAttribute(AttributeName = "dur")]
		public String dur { get; set; }
		[XmlAttribute(AttributeName = "end")]
		public String end { get; set; }
		[XmlAttribute(AttributeName = "max")]
		[DefaultValueAttribute("indefinite")]
		public String max { get; set; }
		[XmlAttribute(AttributeName = "min")]
		[DefaultValueAttribute("0")]
		public String min { get; set; }
		[XmlAttribute(AttributeName = "repeat")]
		public String repeat { get; set; }
		[XmlAttribute(AttributeName = "fillDefault")]
		[DefaultValueAttribute(FillDefault.inherit)]
		public FillDefault fillDefault { get; set; }
		[XmlAttribute(AttributeName = "repeatCount")]
		public String repeatCount { get; set; }
		[XmlAttribute(AttributeName = "repeatDur")]
		public String repeatDur { get; set; }
		[XmlAttribute(AttributeName = "restart")]
		[DefaultValueAttribute(Restart.Default)]
		public Restart restart { get; set; }
		[XmlAttribute(AttributeName = "restartDefault")]
		[DefaultValueAttribute(RestartDefault.inherit)]
		public RestartDefault restartDefault { get; set; }
		[XmlAttribute(AttributeName = "syncBehavior")]
		[DefaultValueAttribute(SyncBehavior.Default)]
		public SyncBehavior syncBehavior { get; set; }
		[XmlAttribute(AttributeName = "syncBehaviorDefault")]
		[DefaultValueAttribute(SyncBehaviorDefault.inherit)]
		public SyncBehaviorDefault syncBehaviorDefault { get; set; }
		[XmlAttribute(AttributeName = "syncTolerance")]
		[DefaultValueAttribute("default")]
		public String syncTolerance { get; set; }
		[XmlAttribute(AttributeName = "syncToleranceDefault")]
		[DefaultValueAttribute("inherit")]
		public String syncToleranceDefault { get; set; }
		#endregion

		[XmlElement(Type = typeof(Par), ElementName = "par", IsNullable = true)]
		[XmlElement(Type = typeof(Seq), ElementName = "seq", IsNullable = true)]
		[XmlElement(Type = typeof(Excl), ElementName = "excl", IsNullable = true)]
		[XmlElement(Type = typeof(Ref), ElementName = "ref", IsNullable = true)]
		[XmlElement(Type = typeof(Animation), ElementName = "animation", IsNullable = true)]
		[XmlElement(Type = typeof(Audio), ElementName = "audio", IsNullable = true)]
		[XmlElement(Type = typeof(Img), ElementName = "img", IsNullable = true)]
		[XmlElement(Type = typeof(Text), ElementName = "text", IsNullable = true)]
		[XmlElement(Type = typeof(TextStream), ElementName = "textstream", IsNullable = true)]
		[XmlElement(Type = typeof(Video), ElementName = "video", IsNullable = true)]
        //[XmlElement(Type = typeof(Multimedia), ElementName = "multimedia", IsNullable = true)]
        [XmlElement(Type = typeof(BasicButton), ElementName = "buton", IsNullable = true)]
        [XmlElement(Type = typeof(RectArea), ElementName = "rectarea", IsNullable = true)]
		public Collection<object> arrCollection = new Collection<object>();

		[XmlElement(ElementName = "metadata", IsNullable = true)]
		public Collection<Metadata> arrMetaCollection = new Collection<Metadata>();

		[XmlElement(ElementName = "a", IsNullable = true)]
		public Collection<a> arrARefCollection = new Collection<a>();

		public Body()
		{
			fill = Fill.Default;

			#region Timing
			fillDefault = FillDefault.inherit;
			max = "indefinite";
			min = "0";
			restart = Restart.Default;
			restartDefault = RestartDefault.inherit;
			syncBehavior = SyncBehavior.Default;
			syncBehaviorDefault = SyncBehaviorDefault.inherit;
			syncTolerance = "default";
			syncToleranceDefault = "inherit";
			#endregion
		}
	}
	#endregion

	#region Animation Modules

	public class AnmationObject
	{

	}

	[XmlRoot(ElementName = "animate", Namespace = "http://www.w3.org/ns/SMIL"), Serializable]
	public class Animate : AnmationObject
	{
	}

	[XmlRoot(ElementName = "animateColor", Namespace = "http://www.w3.org/ns/SMIL"), Serializable]
	public class AnimateColor : AnmationObject
	{
	}

	[XmlRoot(ElementName = "animateMotion", Namespace = "http://www.w3.org/ns/SMIL"), Serializable]
	public class AnimateMotion : AnmationObject
	{
	}

	[XmlRoot(ElementName = "set", Namespace = "http://www.w3.org/ns/SMIL"), Serializable]
	public class Set : AnmationObject
	{
	}

	#endregion

	#region ContentControl Modules
	public class ContentControlObject : TestObjectInheritedBase
	{
		[XmlAttribute(AttributeName = "customTest")]
		public String customTest { get; set; }

		[XmlElement(ElementName = "metadata", IsNullable = true)]
		public Collection<Metadata> arrMetaCollection = new Collection<Metadata>();

		public ContentControlObject()
		{
		}
	}

	[XmlRoot(ElementName = "prefetch", Namespace = "http://www.w3.org/ns/SMIL"), Serializable]
	public class Prefetch : ContentControlObject
	{
		#region Timing Attributes
		[XmlAttribute(AttributeName = "begin")]
		public String begin { get; set; }
		[XmlAttribute(AttributeName = "dur")]
		public String dur { get; set; }
		[XmlAttribute(AttributeName = "end")]
		public String end { get; set; }
		[XmlAttribute(AttributeName = "max")]
		[DefaultValueAttribute("indefinite")]
		public String max { get; set; }
		[XmlAttribute(AttributeName = "min")]
		[DefaultValueAttribute("0")]
		public String min { get; set; }
		[XmlAttribute(AttributeName = "repeat")]
		public String repeat { get; set; }
		[XmlAttribute(AttributeName = "fillDefault")]
		[DefaultValueAttribute(FillDefault.inherit)]
		public FillDefault fillDefault { get; set; }
		[XmlAttribute(AttributeName = "repeatCount")]
		public String repeatCount { get; set; }
		[XmlAttribute(AttributeName = "repeatDur")]
		public String repeatDur { get; set; }
		[XmlAttribute(AttributeName = "restart")]
		[DefaultValueAttribute(Restart.Default)]
		public Restart restart { get; set; }
		[XmlAttribute(AttributeName = "restartDefault")]
		[DefaultValueAttribute(RestartDefault.inherit)]
		public RestartDefault restartDefault { get; set; }
		[XmlAttribute(AttributeName = "syncBehavior")]
		[DefaultValueAttribute(SyncBehavior.Default)]
		public SyncBehavior syncBehavior { get; set; }
		[XmlAttribute(AttributeName = "syncBehaviorDefault")]
		[DefaultValueAttribute(SyncBehaviorDefault.inherit)]
		public SyncBehaviorDefault syncBehaviorDefault { get; set; }
		[XmlAttribute(AttributeName = "syncTolerance")]
		[DefaultValueAttribute("default")]
		public String syncTolerance { get; set; }
		[XmlAttribute(AttributeName = "syncToleranceDefault")]
		[DefaultValueAttribute("inherit")]
		public String syncToleranceDefault { get; set; }
		#endregion

		[XmlAttribute(AttributeName = "clip-begin")]
		public string clip_begin;
		[XmlAttribute(AttributeName = "clip-end")]
		public string clip_end;
		[XmlAttribute(AttributeName = "clipBegin")]
		public string clipBegin
		{
			get { return clip_begin; }
			set { clip_begin = value; }
		}
		[XmlAttribute(AttributeName = "clipEnd")]
		public string clipEnd
		{
			get { return clip_end; }
			set { clip_end = value; }
		}
		[XmlAttribute(AttributeName = "bandwidth")]
		[DefaultValueAttribute("100%")]
		public String bandwidth { get; set; }
		[XmlAttribute(AttributeName = "expr")]
		public String expr { get; set; }
		[XmlAttribute(AttributeName = "mediaSize")]
		public String mediaSize { get; set; }
		[XmlAttribute(AttributeName = "mediaTime")]
		public String mediaTime { get; set; }
		[XmlAttribute(AttributeName = "skip-content")]
		[DefaultValueAttribute(true)]
		public bool skip_content { get; set; }
		[XmlAttribute(AttributeName = "src")]
		public String src { get; set; }


		public Prefetch()
		{
			#region Timing
			fillDefault = FillDefault.inherit;
			max = "indefinite";
			min = "0";
			restart = Restart.Default;
			restartDefault = RestartDefault.inherit;
			syncBehavior = SyncBehavior.Default;
			syncBehaviorDefault = SyncBehaviorDefault.inherit;
			syncTolerance = "default";
			syncToleranceDefault = "inherit";
			#endregion

			skip_content = true;
		}
	}

	[XmlRoot(ElementName = "switch", Namespace = "http://www.w3.org/ns/SMIL"), Serializable]
	public class Switch : ContentControlObject
	{
		[XmlAttribute(AttributeName = "allowReorder")]
		[DefaultValueAttribute(AllowReorder.no)]
		public AllowReorder allowReorder { get; set; }

		[XmlElement(Type = typeof(Delvalue), ElementName = "delvalue", IsNullable = true)]
		[XmlElement(Type = typeof(Newvalue), ElementName = "newvalue", IsNullable = true)]
		[XmlElement(Type = typeof(Send), ElementName = "send", IsNullable = true)]
		[XmlElement(Type = typeof(Setvalue), ElementName = "setvalue", IsNullable = true)]
		[XmlElement(Type = typeof(Animate), ElementName = "animate", IsNullable = true)]
		[XmlElement(Type = typeof(AnimateColor), ElementName = "animateColor", IsNullable = true)]
		[XmlElement(Type = typeof(AnimateMotion), ElementName = "animateMotion", IsNullable = true)]
		[XmlElement(Type = typeof(Set), ElementName = "set", IsNullable = true)]
		[XmlElement(Type = typeof(Par), ElementName = "par", IsNullable = true)]
		[XmlElement(Type = typeof(Seq), ElementName = "seq", IsNullable = true)]
		[XmlElement(Type = typeof(Excl), ElementName = "excl", IsNullable = true)]
		[XmlElement(Type = typeof(Ref), ElementName = "ref", IsNullable = true)]
		[XmlElement(Type = typeof(Animation), ElementName = "animation", IsNullable = true)]
		[XmlElement(Type = typeof(Audio), ElementName = "audio", IsNullable = true)]
		[XmlElement(Type = typeof(Img), ElementName = "img", IsNullable = true)]
		[XmlElement(Type = typeof(Text), ElementName = "text", IsNullable = true)]
		[XmlElement(Type = typeof(TextStream), ElementName = "textstream", IsNullable = true)]
		[XmlElement(Type = typeof(Video), ElementName = "video", IsNullable = true)]
        //[XmlElement(Type = typeof(Multimedia), ElementName = "multimedia", IsNullable = true)]
		[XmlElement(Type = typeof(Switch), ElementName = "switch", IsNullable = true)]
		[XmlElement(Type = typeof(Prefetch), ElementName = "prefetch", IsNullable = true)]
		[XmlElement(Type = typeof(a), ElementName = "a", IsNullable = true)]
		[XmlElement(Type = typeof(Param), ElementName = "param", IsNullable = true)]
		[XmlElement(Type = typeof(Anchor), ElementName = "anchor", IsNullable = true)]
		[XmlElement(Type = typeof(Layout), ElementName = "layout", IsNullable = true)]
		public Collection<object> arrCollection = new Collection<object>();

		public Switch()
		{
			allowReorder = AllowReorder.no;
		}
	}

	#endregion

	#region Layout Modules
	/// <summary>
	/// Layout Element
	/// </summary>
	[XmlRoot(ElementName = "layout", Namespace = "http://www.w3.org/ns/SMIL"), Serializable]
	public class Layout : RegionBase
	{
		[XmlAttribute(AttributeName = "customTest")]
		public String customTest { get; set; }
		[XmlAttribute(AttributeName = "type")]
		[DefaultValueAttribute("text/smil-basic-layout")]
		public String type { get; set; }

		[XmlElement(Type = typeof(TopLayout), ElementName = "topLayout", IsNullable = true)]
		[XmlElement(Type = typeof(RootLayout), ElementName = "root-layout", IsNullable = true)]
		[XmlElement(Type = typeof(Region), ElementName = "region", IsNullable = true)]
		[XmlElement(Type = typeof(RegPoint), ElementName = "regPoint", IsNullable = true)]
		public Collection<RegionBase> arrCollection = new Collection<RegionBase>();
		

		[XmlElement(ElementName = "metadata", IsNullable = true)]
		public Collection<Metadata> arrMetaCollection = new Collection<Metadata>();

		public Layout()
		{
			type = "text/smil-basic-layout";

		}
	}

	/// <summary>
	/// Region Interface
	/// </summary>
	[Serializable]
	public class RegionBase : TestObjectInheritedBase
	{
		public RegionBase()
		{
		}

	}

	/// <summary>
	/// RootLayout Element
	/// </summary>
	[XmlRoot(ElementName = "regPoint", IsNullable = false), Serializable]
	public class RegPoint : RegionBase
	{
		[XmlAttribute(AttributeName = "bottom")]
		[DefaultValueAttribute("auto")]
		public String bottom { get; set; }
		[XmlAttribute(AttributeName = "customTest")]
		public String customTest { get; set; }
		[XmlAttribute(AttributeName = "left")]
		[DefaultValueAttribute("auto")]
		public String left { get; set; }
		[XmlAttribute(AttributeName = "regAlign")]
		[DefaultValueAttribute(Align.topLeft)]
		public Align regAlign { get; set; }
		[XmlAttribute(AttributeName = "right")]
		[DefaultValueAttribute("auto")]
		public String right { get; set; }
		[XmlAttribute(AttributeName = "top")]
		[DefaultValueAttribute("auto")]
		public String top { get; set; }
		[XmlAttribute(AttributeName = "skip-content")]
		[DefaultValueAttribute(true)]
		public bool skipcontent { get; set; }

		[XmlElement(ElementName = "metadata", IsNullable = true)]
		public Collection<Metadata> arrMetaCollection = new Collection<Metadata>();

		public RegPoint()
		{
			bottom = left = right = top = "auto";
			regAlign = Align.topLeft;
			skipcontent = true;
		}
	}

	/// <summary>
	/// RootLayout Element
	/// </summary>
	[XmlRoot(ElementName = "root-layout", IsNullable = false), Serializable]
	public class RootLayout : RegionBase
	{
		[XmlAttribute(AttributeName = "background-color")]
		public String background_color { get; set; }
		[XmlAttribute(AttributeName = "backgroundColor")]
		public String backgroundColor { get; set; }
		[XmlAttribute(AttributeName = "backgroundImage")]
		[DefaultValueAttribute("none")]
		public String backgroundImage { get; set; }
		[XmlAttribute(AttributeName = "backgroundOpacity")]
		[DefaultValueAttribute("100%")]
		public String backgroundOpacity { get; set; }
		[XmlAttribute(AttributeName = "backgroundRepeat")]
		[DefaultValueAttribute(backgroundRepeat.repeat)]
		public backgroundRepeat backgroundRepeat { get; set; }

		[XmlAttribute(AttributeName = "customTest")]
		public String customTest { get; set; }

		[XmlAttribute(AttributeName = "height")]
		[DefaultValueAttribute("auto")]
		public String height { get; set; }
		[XmlAttribute(AttributeName = "width")]
		[DefaultValueAttribute("auto")]
		public String width { get; set; }
		[XmlAttribute(AttributeName = "skip-content")]
		[DefaultValueAttribute(true)]
		public bool skipcontent { get; set; }

		[XmlElement(ElementName = "metadata", IsNullable = true)]
		public Collection<Metadata> arrMetaCollection = new Collection<Metadata>();

		public RootLayout()
		{
			backgroundRepeat = backgroundRepeat.repeat;
			backgroundImage = "none";
			backgroundOpacity = "100%";
			skipcontent = true;

			height = width = "auto";
		}
	}

	/// <summary>
	/// TopLayout Element
	/// </summary>
	[XmlRoot(ElementName = "topLayout", IsNullable = false), Serializable]
	public class TopLayout : RegionBase
	{
		[XmlAttribute(AttributeName = "background-color")]
		public String background_color { get; set; }
		[XmlAttribute(AttributeName = "backgroundColor")]
		public String backgroundColor { get; set; }
		[XmlAttribute(AttributeName = "backgroundImage")]
		[DefaultValueAttribute("none")]
		public String backgroundImage { get; set; }
		[XmlAttribute(AttributeName = "backgroundOpacity")]
		[DefaultValueAttribute("100%")]
		public String backgroundOpacity { get; set; }
		[XmlAttribute(AttributeName = "backgroundRepeat")]
		[DefaultValueAttribute(backgroundRepeat.repeat)]
		public backgroundRepeat backgroundRepeat { get; set; }
		[XmlAttribute(AttributeName = "close")]
		[DefaultValueAttribute(close.onRequest)]
		public close close { get; set; }
		[XmlAttribute(AttributeName = "open")]
		[DefaultValueAttribute(open.onStart)]
		public open open { get; set; }

		[XmlAttribute(AttributeName = "customTest")]
		public String customTest { get; set; }

		[XmlAttribute(AttributeName = "height")]
		[DefaultValueAttribute("auto")]
		public String height { get; set; }
		[XmlAttribute(AttributeName = "width")]
		[DefaultValueAttribute("auto")]
		public String width { get; set; }
		[XmlAttribute(AttributeName = "skip-content")]
		[DefaultValueAttribute(true)]
		public bool skipcontent { get; set; }

		[XmlElement(ElementName = "metadata", IsNullable = true)]
		public Collection<Metadata> arrMetaCollection = new Collection<Metadata>();
		
		[XmlElement(ElementName = "region", IsNullable = true)]
		public Collection<Region> arrRegionCollection = new Collection<Region>();

		public TopLayout()
		{
			backgroundRepeat = backgroundRepeat.repeat;
			backgroundImage = "none";
			backgroundOpacity = "100%";
			skipcontent = true;

			close = close.onRequest;
			open = open.onStart;

			height = width = "auto";

		}
	}

	/// <summary>
	/// Region Element
	/// </summary>
	[XmlRoot(ElementName = "region", IsNullable = false), Serializable]
	public class Region : RegionBase
	{

		#region Default Attribute
		[XmlAttribute(AttributeName = "background-color")]
		public String background_color { get; set; }
		[XmlAttribute(AttributeName = "backgroundImage")]
		[DefaultValueAttribute("none")]
		public String backgroundImage { get; set; }
		[XmlAttribute(AttributeName = "backgroundRepeat")]
		[DefaultValueAttribute(backgroundRepeat.repeat)]
		public backgroundRepeat backgroundRepeat { get; set; }
		[XmlAttribute(AttributeName = "customTest")]
		public String customTest { get; set; }
		[XmlAttribute(AttributeName = "erase")]
		[DefaultValueAttribute(erase.whenDone)]
		public erase erase { get; set; }
		[XmlAttribute(AttributeName = "panZoom")]
		public String panZoom { get; set; }
		[XmlAttribute(AttributeName = "regionName")]
		public String regionName { get; set; }
		[XmlAttribute(AttributeName = "sensitivity")]
		[DefaultValueAttribute("opaque")]
		public String sensitivity { get; set; }
		[XmlAttribute(AttributeName = "showBackground")]
		[DefaultValueAttribute(showBackground.always)]
		public showBackground showBackground { get; set; }
		[XmlAttribute(AttributeName = "skip-content")]
		[DefaultValueAttribute(true)]
		public bool skipcontent { get; set; }
		[XmlAttribute(AttributeName = "soundLevel")]
		[DefaultValueAttribute("+0.0dB")]
		public String soundLevel { get; set; }
		[XmlAttribute(AttributeName = "textAlign")]
		[DefaultValueAttribute(textAlign.inherit)]
		public textAlign textAlign { get; set; }
		[XmlAttribute(AttributeName = "textDirection")]
		[DefaultValueAttribute(textDirection.inherit)]
		public textDirection textDirection { get; set; }
		[XmlAttribute(AttributeName = "textMode")]
		[DefaultValueAttribute(textMode.inherit)]
		public textMode textMode { get; set; }
		[XmlAttribute(AttributeName = "textPlace")]
		[DefaultValueAttribute(textPlace.inherit)]
		public textPlace textPlace { get; set; }
		[XmlAttribute(AttributeName = "textWrapOption")]
		[DefaultValueAttribute(textWrapOption.wrap)]
		public textWrapOption textWrapOption { get; set; }
		[XmlAttribute(AttributeName = "textWritingMode")]
		[DefaultValueAttribute(textWritingMode.inherit)]
		public textWritingMode textWritingMode { get; set; }
		#endregion

		#region MediaOpacity Attributes
		[XmlAttribute(AttributeName = "chromaKey")]
		public String chromaKey { get; set; }
		[XmlAttribute(AttributeName = "chromaKeyOpacity")]
		public String chromaKeyOpacity { get; set; }
		[XmlAttribute(AttributeName = "chromaKeyTolerance")]
		public String chromaKeyTolerance { get; set; }
		[XmlAttribute(AttributeName = "mediaBackgroundOpacity")]
		public String mediaBackgroundOpacity { get; set; }
		[XmlAttribute(AttributeName = "mediaOpacity")]
		public String mediaOpacity { get; set; }
		#endregion

		#region Subregion Attributes
		[XmlAttribute(AttributeName = "backgroundColor")]
		public String backgroundColor { get; set; }
		[XmlAttribute(AttributeName = "backgroundOpacity")]
		[DefaultValueAttribute("100%")]
		public String backgroundOpacity { get; set; }
		[XmlAttribute(AttributeName = "bottom")]
		[DefaultValueAttribute("auto")]
		public String bottom { get; set; }
		[XmlAttribute(AttributeName = "fit")]
		[DefaultValueAttribute(Fit.fill)]
		public Fit fit { get; set; }
		[XmlAttribute(AttributeName = "height")]
		[DefaultValueAttribute("auto")]
		public String height { get; set; }
		[XmlAttribute(AttributeName = "left")]
		[DefaultValueAttribute("auto")]
		public String left { get; set; }
		[XmlAttribute(AttributeName = "mediaAlign")]
		[DefaultValueAttribute(Align.topLeft)]
		public Align mediaAlign { get; set; }
		[XmlAttribute(AttributeName = "regAlign")]
		[DefaultValueAttribute(Align.topLeft)]
		public Align regAlign { get; set; }
		[XmlAttribute(AttributeName = "regPoint")]
		public String regPoint { get; set; }
		[XmlAttribute(AttributeName = "right")]
		[DefaultValueAttribute("auto")]
		public String right { get; set; }
		[XmlAttribute(AttributeName = "soundAlign")]
		[DefaultValueAttribute(soundAlign.both)]
		public soundAlign soundAlign { get; set; }
		[XmlAttribute(AttributeName = "top")]
		[DefaultValueAttribute("auto")]
		public String top { get; set; }
		[XmlAttribute(AttributeName = "width")]
		[DefaultValueAttribute("auto")]
		public String width { get; set; }
		[XmlAttribute(AttributeName = "z-index")]
		public String zindex { get; set; }
		#endregion

		#region Text Attributes
		[XmlAttribute(AttributeName = "textBackgroundColor")]
		[DefaultValueAttribute("transparent")]
		public String textBackgroundColor { get; set; }
		[XmlAttribute(AttributeName = "textColor")]
		public String textColor { get; set; }
		[XmlAttribute(AttributeName = "textFontFamily")]
		[DefaultValueAttribute("inherit")]
		public String textFontFamily { get; set; }
		[XmlAttribute(AttributeName = "textFontSize")]
		[DefaultValueAttribute("inherit")]
		public String textFontSize { get; set; }
		[XmlAttribute(AttributeName = "textFontStyle")]
		[DefaultValueAttribute(textFontStyle.inherit)]
		public textFontStyle textFontStyle { get; set; }
		[XmlAttribute(AttributeName = "textFontWeight")]
		[DefaultValueAttribute(textFontWeight.inherit)]
		public textFontWeight textFontWeight { get; set; }
		[XmlAttribute(AttributeName = "textStyle")]
		public String textStyle { get; set; }
		[XmlAttribute(AttributeName = "xml:space")]
		[DefaultValueAttribute(space.Default)]
		public space xmlspace { get; set; }
	
		#endregion

		[XmlElement(ElementName = "metadata", IsNullable = true)]
		public Collection<Metadata> arrMetaCollection = new Collection<Metadata>();

		[XmlElement(ElementName = "region", IsNullable = true)]
		public Collection<Region> arrRegionCollection = new Collection<Region>();


		public Region()
		{
			fit = Fit.fill;

			backgroundImage = "none";
			backgroundRepeat = backgroundRepeat.repeat;
			erase = erase.whenDone;
			sensitivity = "opaque";
			showBackground = showBackground.always;
			skipcontent = true;
			soundLevel = "+0.0dB";
			textAlign = textAlign.inherit;
			textMode = textMode.inherit;
			textPlace = textPlace.inherit;
			textWrapOption = textWrapOption.wrap;
			textWritingMode = textWritingMode.inherit;

			#region Subregion Attributes
			backgroundOpacity = "100%";
			left = top = right = bottom = width = height = "auto";
			regAlign = mediaAlign = Align.topLeft;
			soundAlign = soundAlign.both;
			#endregion

			#region Text Attributes
			textBackgroundColor = "transparent";
			textFontFamily = textFontSize = "inherit";
			textFontStyle = textFontStyle.inherit;
			textFontWeight = textFontWeight.inherit;
			xmlspace = space.Default;
			#endregion

		}
	}
	#endregion

	#region LinkAnchor Modules (Not Implement)
	public class LinkAnchorObject
	{
	}

	[XmlRoot(ElementName = "a", Namespace = "http://www.w3.org/ns/SMIL"), Serializable]
	public class a : LinkAnchorObject
	{
	}

	[XmlRoot(ElementName = "area", Namespace = "http://www.w3.org/ns/SMIL"), Serializable]
	public class Area : LinkAnchorObject
	{
	}

	[XmlRoot(ElementName = "anchor", Namespace = "http://www.w3.org/ns/SMIL"), Serializable]
	public class Anchor : LinkAnchorObject
	{
	}

	#endregion

	#region MediaContent Modules
	public class MediaObject : TestObjectInheritedBase
	{
		public MediaObject()
		{
			endsync = "media";
			mediaRepeat = mediaRepeat.preserve;
			backgroundOpacity = "100%";
			height = width = right = left = top = bottom = "auto";
			mediaAlign = regAlign = Align.topLeft;
			soundAlign = soundAlign.both;
			fit = Fit.fill;

			fillDefault = FillDefault.inherit;
			fill = Fill.Default;
			max = "indefinite";
			min = "0";
			restart = Restart.Default;
			restartDefault = RestartDefault.inherit;
			syncBehavior = SyncBehavior.Default;
			syncBehaviorDefault = SyncBehaviorDefault.inherit;
			syncTolerance = "default";
			syncToleranceDefault = "inherit";

			erase = erase.whenDone;
			sensitivity = "opaque";
		}

		#region Default Attribute
		[XmlAttribute(AttributeName = "fill")]
		[DefaultValueAttribute(Fill.Default)]
		public Fill fill { get; set; }

		[XmlAttribute(AttributeName = "customTest")]
		public String customTest { get; set; }

		[XmlAttribute(AttributeName = "endsync")]
		[DefaultValueAttribute("media")]
		public String endsync { get; set; }
		[XmlAttribute(AttributeName = "erase")]
		[DefaultValueAttribute(erase.whenDone)]
		public erase erase { get; set; }
		[XmlAttribute(AttributeName = "expr")]
		public String expr { get; set; }
		[XmlAttribute(AttributeName = "mediaRepeat")]
		[DefaultValueAttribute(mediaRepeat.preserve)]
		public mediaRepeat mediaRepeat { get; set; }
		[XmlAttribute(AttributeName = "panZoom")]
		public String panZoom { get; set; }
		[XmlAttribute(AttributeName = "region")]
		public String region { get; set; }
		[XmlAttribute(AttributeName = "sensitivity")]
		[DefaultValueAttribute("opaque")]
		public String sensitivity { get; set; }
		[XmlAttribute(AttributeName = "soundLevel")]
		[DefaultValueAttribute("+0.0dB")]
		public String soundLevel { get; set; }
		[XmlAttribute(AttributeName = "tabindex")]
		public String tabindex { get; set; }
		[XmlAttribute(AttributeName = "transIn")]
		public String transIn { get; set; }
		[XmlAttribute(AttributeName = "transOut")]
		public String transOut { get; set; }

		[XmlAttribute(AttributeName = "src")]
		public String src { get; set; }
		[XmlAttribute(AttributeName = "type")]
		public String type { get; set; }

		[XmlAttribute(AttributeName = "paramGroup")]
		public string paramGroup { get; set; }

		[XmlAttribute(AttributeName = "clip-begin")]
		public string clip_begin;
		[XmlAttribute(AttributeName = "clip-end")]
		public string clip_end;
		[XmlAttribute(AttributeName = "clipBegin")]
		public string clipBegin { 
			get {return clip_begin;}
			set {clip_begin = value;}
		}
		[XmlAttribute(AttributeName = "clipEnd")]
		public string clipEnd {
			get { return clip_end; }
			set { clip_end = value; }
		}

		#endregion

		#region MediaDescription Attributes
		[XmlAttribute(AttributeName = "abstract")]
		public String Abstract { get; set; }
		[XmlAttribute(AttributeName = "author")]
		public String author { get; set; }
		[XmlAttribute(AttributeName = "copyright")]
		public String copyright { get; set; }
		#endregion

		#region MediaOpacity Attributes
		[XmlAttribute(AttributeName = "chromaKey")]
		public String chromaKey { get; set; }
		[XmlAttribute(AttributeName = "chromaKeyOpacity")]
		public String chromaKeyOpacity { get; set; }
		[XmlAttribute(AttributeName = "chromaKeyTolerance")]
		public String chromaKeyTolerance { get; set; }
		[XmlAttribute(AttributeName = "mediaBackgroundOpacity")]
		public String mediaBackgroundOpacity { get; set; }
		[XmlAttribute(AttributeName = "mediaOpacity")]
		public String mediaOpacity { get; set; }
		#endregion

		#region Subregion Attributes
		[XmlAttribute(AttributeName = "backgroundColor")]
		public String backgroundColor { get; set; }
		[XmlAttribute(AttributeName = "backgroundOpacity")]
		[DefaultValueAttribute("100%")]
		public String backgroundOpacity { get; set; }
		[XmlAttribute(AttributeName = "bottom")]
		[DefaultValueAttribute("auto")]
		public String bottom { get; set; }
		[XmlAttribute(AttributeName = "fit")]
		[DefaultValueAttribute(Fit.fill)]
		public Fit fit { get; set; }
		[XmlAttribute(AttributeName = "height")]
		[DefaultValueAttribute("auto")]
		public String height { get; set; }
		[XmlAttribute(AttributeName = "left")]
		[DefaultValueAttribute("auto")]
		public String left { get; set; }
		[XmlAttribute(AttributeName = "mediaAlign")]
		[DefaultValueAttribute(Align.topLeft)]
		public Align mediaAlign { get; set; }
		[XmlAttribute(AttributeName = "regAlign")]
		[DefaultValueAttribute(Align.topLeft)]
		public Align regAlign { get; set; }
		[XmlAttribute(AttributeName = "regPoint")]
		public String regPoint { get; set; }
		[XmlAttribute(AttributeName = "right")]
		[DefaultValueAttribute("auto")]
		public String right { get; set; }
		[XmlAttribute(AttributeName = "soundAlign")]
		[DefaultValueAttribute(soundAlign.both)]
		public soundAlign soundAlign { get; set; }
		[XmlAttribute(AttributeName = "top")]
		[DefaultValueAttribute("auto")]
		public String top { get; set; }
		[XmlAttribute(AttributeName = "width")]
		[DefaultValueAttribute("auto")]
		public String width { get; set; }
		[XmlAttribute(AttributeName = "z-index")]
		public String zindex { get; set; }
		#endregion

		#region Timing Attributes
		[XmlAttribute(AttributeName = "begin")]
		public String begin { get; set; }
		[XmlAttribute(AttributeName = "dur")]
		public String dur { get; set; }
		[XmlAttribute(AttributeName = "end")]
		public String end { get; set; }
		[XmlAttribute(AttributeName = "max")]
		[DefaultValueAttribute("indefinite")]
		public String max { get; set; }
		[XmlAttribute(AttributeName = "min")]
		[DefaultValueAttribute("0")]
		public String min { get; set; }
		[XmlAttribute(AttributeName = "repeat")]
		public String repeat { get; set; }
		[XmlAttribute(AttributeName = "fillDefault")]
		[DefaultValueAttribute(FillDefault.inherit)]
		public FillDefault fillDefault { get; set; }
		[XmlAttribute(AttributeName = "repeatCount")]
		public String repeatCount { get; set; }
		[XmlAttribute(AttributeName = "repeatDur")]
		public String repeatDur { get; set; }
		[XmlAttribute(AttributeName = "restart")]
		[DefaultValueAttribute(Restart.Default)]
		public Restart restart { get; set; }
		[XmlAttribute(AttributeName = "restartDefault")]
		[DefaultValueAttribute(RestartDefault.inherit)]
		public RestartDefault restartDefault { get; set; }
		[XmlAttribute(AttributeName = "syncBehavior")]
		[DefaultValueAttribute(SyncBehavior.Default)]
		public SyncBehavior syncBehavior { get; set; }
		[XmlAttribute(AttributeName = "syncBehaviorDefault")]
		[DefaultValueAttribute(SyncBehaviorDefault.inherit)]
		public SyncBehaviorDefault syncBehaviorDefault { get; set; }
		[XmlAttribute(AttributeName = "syncTolerance")]
		[DefaultValueAttribute("default")]
		public String syncTolerance { get; set; }
		[XmlAttribute(AttributeName = "syncToleranceDefault")]
		[DefaultValueAttribute("inherit")]
		public String syncToleranceDefault { get; set; }
		#endregion

		[XmlElement(Type = typeof(Animate), ElementName = "animate", IsNullable = true)]
		[XmlElement(Type = typeof(AnimateColor), ElementName = "animateColor", IsNullable = true)]
		[XmlElement(Type = typeof(AnimateMotion), ElementName = "animateMotion", IsNullable = true)]
		[XmlElement(Type = typeof(Set), ElementName = "set", IsNullable = true)]
		public Collection<AnmationObject> arrAnimationObjectCollection = new Collection<AnmationObject>();
			
		[XmlElement(ElementName = "param", Type = typeof(Param))]
		public Collection<Param> arrParamCollection = new Collection<Param>();

		[XmlElement(ElementName = "metadata", IsNullable = true)]
		public Collection<Metadata> arrMetadataCollection = new Collection<Metadata>();

		[XmlElement(ElementName = "area", IsNullable = true)]
		public Collection<Area> arrAreaCollection = new Collection<Area>();

		[XmlElement(ElementName = "anchor", IsNullable = true)]
		public Collection<Anchor> arrAnchorCollection = new Collection<Anchor>();

		[XmlElement(ElementName = "switch", IsNullable = true)]
		public Collection<Switch> arrSwitchCollection = new Collection<Switch>();

	}

	[XmlRoot(ElementName = "ref", IsNullable = false), Serializable]
	public class Ref : MediaObject
	{
	}

	[XmlRoot(ElementName = "animation", IsNullable = false), Serializable]
	public class Animation : MediaObject
	{
	}

	[XmlRoot(ElementName = "audio", IsNullable = false), Serializable]
	public class Audio : MediaObject
	{
	}

	[XmlRoot(ElementName = "img", IsNullable = false), Serializable]
	public class Img : MediaObject
	{
	}

	[XmlRoot(ElementName = "inputtext", IsNullable = false), Serializable]
	public class InputText : MediaObject
	{
		[XmlText]
		public String Content = String.Empty;
	
	}

    [XmlRoot(ElementName = "text", IsNullable = false), Serializable]
    public class Text : MediaObject
    {
        [XmlText]
        public String Content = String.Empty;

    }

    [XmlRoot(ElementName = "label", IsNullable = false), Serializable]
    public class Label : MediaObject
    {
        [XmlText]
        public String Content = String.Empty;

    }

	[XmlRoot(ElementName = "textstream", IsNullable = false), Serializable]
	public class TextStream : MediaObject
	{
	}

	[XmlRoot(ElementName = "video", IsNullable = false), Serializable]
	public class Video : MediaObject
	{
	}

    [XmlRoot(ElementName = "multimedia", IsNullable = false), Serializable]
    public class Multimedia : MediaObject
    {
    }

    [XmlRoot(ElementName = "button", IsNullable = false), Serializable]
    public class BasicButton : MediaObject
    {
    }

    [XmlRoot(ElementName = "rectArea", IsNullable = false), Serializable]
    public class RectArea : MediaObject
    {
    }

    [XmlRoot(ElementName = "imageviewer", IsNullable = false), Serializable]
    public class ImageViewer : MediaObject
    {
    }

	[XmlRoot(ElementName = "brush", IsNullable = false), Serializable]
	public class Brush : TestObjectInheritedBase
	{
		public Brush()
		{
			endsync = "media";
			backgroundOpacity = "100%";
			height = width = right = left = top = bottom = "auto";
			mediaAlign = regAlign = Align.topLeft;
			fit = Fit.fill;

			fillDefault = FillDefault.inherit;
			fill = Fill.Default;
			max = "indefinite";
			min = "0";
			restart = Restart.Default;
			restartDefault = RestartDefault.inherit;
			syncBehavior = SyncBehavior.Default;
			syncBehaviorDefault = SyncBehaviorDefault.inherit;
			syncTolerance = "default";
			syncToleranceDefault = "inherit";

			erase = erase.whenDone;
			skip_content = true;
			sensitivity = "opaque";
		}

		#region Default Attribute
		[XmlAttribute(AttributeName = "backgroundColor")]
		public String backgroundColor { get; set; }
		[XmlAttribute(AttributeName = "backgroundOpacity")]
		[DefaultValueAttribute("100%")]
		public String backgroundOpacity { get; set; }
		[XmlAttribute(AttributeName = "bottom")]
		[DefaultValueAttribute("auto")]
		public String bottom { get; set; }
		[XmlAttribute(AttributeName = "color")]
		public String color { get; set; }
		[XmlAttribute(AttributeName = "customTest")]
		public String customTest { get; set; }
		[XmlAttribute(AttributeName = "endsync")]
		[DefaultValueAttribute("media")]
		public String endsync { get; set; }
		[XmlAttribute(AttributeName = "erase")]
		[DefaultValueAttribute(erase.whenDone)]
		public erase erase { get; set; }
		[XmlAttribute(AttributeName = "expr")]
		public String expr { get; set; }
		[XmlAttribute(AttributeName = "fill")]
		[DefaultValueAttribute(Fill.Default)]
		public Fill fill { get; set; }
		[XmlAttribute(AttributeName = "fit")]
		[DefaultValueAttribute(Fit.fill)]
		public Fit fit { get; set; }
		[XmlAttribute(AttributeName = "height")]
		[DefaultValueAttribute("auto")]
		public String height { get; set; }
		[XmlAttribute(AttributeName = "left")]
		[DefaultValueAttribute("auto")]
		public String left { get; set; }
		[XmlAttribute(AttributeName = "mediaAlign")]
		[DefaultValueAttribute(Align.topLeft)]
		public Align mediaAlign { get; set; }
		[XmlAttribute(AttributeName = "paramGroup")]
		public string paramGroup { get; set; }
		[XmlAttribute(AttributeName = "regAlign")]
		[DefaultValueAttribute(Align.topLeft)]
		public Align regAlign { get; set; }
		[XmlAttribute(AttributeName = "regPoint")]
		public String regPoint { get; set; }
		[XmlAttribute(AttributeName = "region")]
		public String region { get; set; }
		[XmlAttribute(AttributeName = "right")]
		[DefaultValueAttribute("auto")]
		public String right { get; set; }
		[XmlAttribute(AttributeName = "sensitivity")]
		[DefaultValueAttribute("opaque")]
		public String sensitivity { get; set; }
		[XmlAttribute(AttributeName = "skip-content")]
		[DefaultValueAttribute(true)]
		public bool skip_content { get; set; }
		[XmlAttribute(AttributeName = "tabindex")]
		public String tabindex { get; set; }
		[XmlAttribute(AttributeName = "top")]
		[DefaultValueAttribute("auto")]
		public String top { get; set; }
		[XmlAttribute(AttributeName = "transIn")]
		public String transIn { get; set; }
		[XmlAttribute(AttributeName = "transOut")]
		public String transOut { get; set; }
		[XmlAttribute(AttributeName = "width")]
		[DefaultValueAttribute("auto")]
		public String width { get; set; }
		[XmlAttribute(AttributeName = "z-index")]
		public String zindex { get; set; }

		#endregion

		#region MediaDescription Attributes
		[XmlAttribute(AttributeName = "abstract")]
		public String Abstract { get; set; }
		[XmlAttribute(AttributeName = "author")]
		public String author { get; set; }
		[XmlAttribute(AttributeName = "copyright")]
		public String copyright { get; set; }
		#endregion

		#region Timing Attributes
		[XmlAttribute(AttributeName = "begin")]
		public String begin { get; set; }
		[XmlAttribute(AttributeName = "dur")]
		public String dur { get; set; }
		[XmlAttribute(AttributeName = "end")]
		public String end { get; set; }
		[XmlAttribute(AttributeName = "max")]
		[DefaultValueAttribute("indefinite")]
		public String max { get; set; }
		[XmlAttribute(AttributeName = "min")]
		[DefaultValueAttribute("0")]
		public String min { get; set; }
		[XmlAttribute(AttributeName = "repeat")]
		public String repeat { get; set; }
		[XmlAttribute(AttributeName = "fillDefault")]
		[DefaultValueAttribute(FillDefault.inherit)]
		public FillDefault fillDefault { get; set; }
		[XmlAttribute(AttributeName = "repeatCount")]
		public String repeatCount { get; set; }
		[XmlAttribute(AttributeName = "repeatDur")]
		public String repeatDur { get; set; }
		[XmlAttribute(AttributeName = "restart")]
		[DefaultValueAttribute(Restart.Default)]
		public Restart restart { get; set; }
		[XmlAttribute(AttributeName = "restartDefault")]
		[DefaultValueAttribute(RestartDefault.inherit)]
		public RestartDefault restartDefault { get; set; }
		[XmlAttribute(AttributeName = "syncBehavior")]
		[DefaultValueAttribute(SyncBehavior.Default)]
		public SyncBehavior syncBehavior { get; set; }
		[XmlAttribute(AttributeName = "syncBehaviorDefault")]
		[DefaultValueAttribute(SyncBehaviorDefault.inherit)]
		public SyncBehaviorDefault syncBehaviorDefault { get; set; }
		[XmlAttribute(AttributeName = "syncTolerance")]
		[DefaultValueAttribute("default")]
		public String syncTolerance { get; set; }
		[XmlAttribute(AttributeName = "syncToleranceDefault")]
		[DefaultValueAttribute("inherit")]
		public String syncToleranceDefault { get; set; }
		#endregion

		[XmlElement(Type = typeof(Animate), ElementName = "animate", IsNullable = true)]
		[XmlElement(Type = typeof(AnimateColor), ElementName = "animateColor", IsNullable = true)]
		[XmlElement(Type = typeof(AnimateMotion), ElementName = "animateMotion", IsNullable = true)]
		[XmlElement(Type = typeof(Set), ElementName = "set", IsNullable = true)]
		public Collection<AnmationObject> arrAnimationObjectCollection = new Collection<AnmationObject>();
			
		[XmlElement(ElementName = "param", Type = typeof(Param))]
		public Collection<Param> arrParamCollection = new Collection<Param>();

		[XmlElement(ElementName = "metadata", IsNullable = true)]
		public Collection<Metadata> arrMetadataCollection = new Collection<Metadata>();

		[XmlElement(ElementName = "area", IsNullable = true)]
		public Collection<Area> arrAreaCollection = new Collection<Area>();

		[XmlElement(ElementName = "anchor", IsNullable = true)]
		public Collection<Anchor> arrAnchorCollection = new Collection<Anchor>();

		[XmlElement(ElementName = "switch", IsNullable = true)]
		public Collection<Switch> arrSwitchCollection = new Collection<Switch>();
	}

	[XmlRoot(ElementName = "smilText", IsNullable = false), Serializable]
	public class SmilText
	{
	}

	#endregion

	#region Metainformation Modules
	/// <summary>
	/// Meta Element
	/// </summary>
	[XmlRoot(ElementName = "meta", Namespace = "http://www.w3.org/ns/SMIL"), Serializable]
	public class Meta : BaseObject
	{
		[XmlAttribute(AttributeName = "name")]
		public String name { get; set; }
		[XmlAttribute(AttributeName = "content")]
		public String content { get; set; }
		[XmlAttribute(AttributeName = "skip-content")]
		[DefaultValueAttribute(true)]
		public bool skipContent { get; set; }

		public Meta()
		{
			skipContent = true;
		}
	}

	/// <summary>
	/// Metadata Element
	/// </summary>
	[XmlRoot(ElementName = "metadata", Namespace = "http://www.w3.org/ns/SMIL"), Serializable]
	public class Metadata : BaseObject
	{
		[XmlAttribute(AttributeName = "skip-content")]
		[DefaultValueAttribute(true)]
		public bool skipContent { get; set; }

		public Metadata()
		{
			skipContent = true;
		}
	}
	#endregion

	#region Schedule Modules
	[Serializable]
	public class ScheduleCollection : TestObjectInheritedBase
	{
		#region MediaDescription Attributes
		[XmlAttribute(AttributeName = "abstract")]
		public String Abstract { get; set; }
		[XmlAttribute(AttributeName = "author")]
		public String author { get; set; }
		[XmlAttribute(AttributeName = "copyright")]
		public String copyright { get; set; }
		#endregion

		#region Timing Attributes
		[XmlAttribute(AttributeName = "begin")]
		public String begin { get; set; }
		[XmlAttribute(AttributeName = "dur")]
		public String dur { get; set; }
		[XmlAttribute(AttributeName = "end")]
		public String end { get; set; }
		[XmlAttribute(AttributeName = "max")]
		[DefaultValueAttribute("indefinite")]
		public String max { get; set; }
		[XmlAttribute(AttributeName = "min")]
		[DefaultValueAttribute("0")]
		public String min { get; set; }
		[XmlAttribute(AttributeName = "repeat")]
		public String repeat { get; set; }
		[XmlAttribute(AttributeName = "fillDefault")]
		[DefaultValueAttribute(FillDefault.inherit)]
		public FillDefault fillDefault { get; set; }
		[XmlAttribute(AttributeName = "repeatCount")]
		public String repeatCount { get; set; }
		[XmlAttribute(AttributeName = "repeatDur")]
		public String repeatDur { get; set; }
		[XmlAttribute(AttributeName = "restart")]
		[DefaultValueAttribute(Restart.Default)]
		public Restart restart { get; set; }
		[XmlAttribute(AttributeName = "restartDefault")]
		[DefaultValueAttribute(RestartDefault.inherit)]
		public RestartDefault restartDefault { get; set; }
		[XmlAttribute(AttributeName = "syncBehavior")]
		[DefaultValueAttribute(SyncBehavior.Default)]
		public SyncBehavior syncBehavior { get; set; }
		[XmlAttribute(AttributeName = "syncBehaviorDefault")]
		[DefaultValueAttribute(SyncBehaviorDefault.inherit)]
		public SyncBehaviorDefault syncBehaviorDefault { get; set; }
		[XmlAttribute(AttributeName = "syncTolerance")]
		[DefaultValueAttribute("default")]
		public String syncTolerance { get; set; }
		[XmlAttribute(AttributeName = "syncToleranceDefault")]
		[DefaultValueAttribute("inherit")]
		public String syncToleranceDefault { get; set; }
		#endregion

		[XmlAttribute(AttributeName = "customTest")]
		public String customTest { get; set; }
		[XmlAttribute(AttributeName = "fill")]
		[DefaultValueAttribute(Fill.Default)]
		public Fill fill { get; set; }
		[XmlAttribute(AttributeName = "expr")]
		public String expr { get; set; }
		[XmlAttribute(AttributeName = "region")]
		public String region { get; set; }

		[XmlElement(Type = typeof(Par), ElementName = "par", IsNullable = true)]
		[XmlElement(Type = typeof(Seq), ElementName = "seq", IsNullable = true)]
		[XmlElement(Type = typeof(Excl), ElementName = "excl", IsNullable = true)]
		[XmlElement(Type = typeof(Ref), ElementName = "ref", IsNullable = true)]
		[XmlElement(Type = typeof(Animation), ElementName = "animation", IsNullable = true)]
		[XmlElement(Type = typeof(Audio), ElementName = "audio", IsNullable = true)]
		[XmlElement(Type = typeof(Img), ElementName = "img", IsNullable = true)]
		[XmlElement(Type = typeof(Text), ElementName = "text", IsNullable = true)]
		[XmlElement(Type = typeof(TextStream), ElementName = "textstream", IsNullable = true)]
		[XmlElement(Type = typeof(Video), ElementName = "video", IsNullable = true)]
        //[XmlElement(Type = typeof(Multimedia), ElementName = "multimedia", IsNullable = true)]
		public Collection<object> arrCollection = new Collection<object>();

		[XmlElement(Type = typeof(Animate), ElementName = "animate", IsNullable = true)]
		[XmlElement(Type = typeof(AnimateColor), ElementName = "animateColor", IsNullable = true)]
		[XmlElement(Type = typeof(AnimateMotion), ElementName = "animateMotion", IsNullable = true)]
		[XmlElement(Type = typeof(Set), ElementName = "set", IsNullable = true)]
		public Collection<AnmationObject> arrAnimationObjectCollection = new Collection<AnmationObject>();

		[XmlElement(Type = typeof(Prefetch), ElementName = "prefetch", IsNullable = true)]
		[XmlElement(Type = typeof(Switch), ElementName = "switch", IsNullable = true)]
		public Collection<ContentControlObject> arrContentControlObjectCollection = new Collection<ContentControlObject>();

		[XmlElement(Type = typeof(Delvalue), ElementName = "delvalue", IsNullable = true)]
		[XmlElement(Type = typeof(Newvalue), ElementName = "newvalue", IsNullable = true)]
		[XmlElement(Type = typeof(Send), ElementName = "send", IsNullable = true)]
		[XmlElement(Type = typeof(Setvalue), ElementName = "setvalue", IsNullable = true)]
		public Collection<StateObject> arrStateObjectCollection = new Collection<StateObject>();
			

		[XmlElement(ElementName = "a", Type = typeof(a))]
		public Collection<a> arraRefCollection = new Collection<a>();

		[XmlElement(ElementName = "metadata", IsNullable = true)]
		public Collection<Metadata> arrMetadataCollection = new Collection<Metadata>();

		public ScheduleCollection()
		{
			readIndex = 0;
			fill = Fill.Default;
			baseProfile = Profile.Language;
			version = Version.ver30;

			#region Timing
			fillDefault = FillDefault.inherit;
			max = "indefinite";
			min = "0";
			restart = Restart.Default;
			restartDefault = RestartDefault.inherit;
			syncBehavior = SyncBehavior.Default;
			syncBehaviorDefault = SyncBehaviorDefault.inherit;
			syncTolerance = "default";
			syncToleranceDefault = "inherit";
			#endregion
		}
	}

	[XmlRoot(ElementName = "par", IsNullable = false), Serializable]
	public class Par : ScheduleCollection
	{
		[XmlAttribute(AttributeName = "endsync")]
		[DefaultValueAttribute("last")]
		public String endsync { get; set; }

		public Par()
		{
			endsync = "last";
		}
	}

	[XmlRoot(ElementName = "seq", IsNullable = false), Serializable]
	public class Seq : ScheduleCollection
	{
	}


	[XmlRoot(ElementName = "excl", IsNullable = false), Serializable]
	public class Excl : ScheduleCollection
	{
		[XmlAttribute(AttributeName = "endsync")]
		[DefaultValueAttribute("last")]
		public String endsync { get; set; }
		[XmlAttribute(AttributeName = "skip-content")]
		[DefaultValueAttribute(true)]
		public bool skip_content { get; set; }

		[XmlElement(ElementName = "priorityClass", IsNullable = true)]
		public Collection<PriorityClass> arrPriorityClassCollection = new Collection<PriorityClass>();


		public Excl()
		{
			endsync = "last";
			skip_content = true;
		}
	}

	#endregion

	#region State Modules (Not Implement)

	public class StateObject
	{
	}

	[XmlRoot(ElementName = "delvalue", Namespace = "http://www.w3.org/ns/SMIL"), Serializable]
	public class Delvalue : StateObject
	{
	}

	[XmlRoot(ElementName = "newvalue", Namespace = "http://www.w3.org/ns/SMIL"), Serializable]
	public class Newvalue : StateObject
	{
	}

	[XmlRoot(ElementName = "send", Namespace = "http://www.w3.org/ns/SMIL"), Serializable]
	public class Send : StateObject
	{
	}

	[XmlRoot(ElementName = "setvalue", Namespace = "http://www.w3.org/ns/SMIL"), Serializable]
	public class Setvalue : StateObject
	{
	}

	#endregion

	#region TextContent Modules (Not Implement)
	[XmlRoot(ElementName = "tev", Namespace = "http://www.w3.org/ns/SMIL"), Serializable]
	public class Tev
	{
	}

	[XmlRoot(ElementName = "clear", Namespace = "http://www.w3.org/ns/SMIL"), Serializable]
	public class Clear
	{
	}

	[XmlRoot(ElementName = "br", Namespace = "http://www.w3.org/ns/SMIL"), Serializable]
	public class Br
	{
	}

	[XmlRoot(ElementName = "span", Namespace = "http://www.w3.org/ns/SMIL"), Serializable]
	public class Span
	{
	}

	[XmlRoot(ElementName = "b", Namespace = "http://www.w3.org/ns/SMIL"), Serializable]
	public class b
	{
	}

	[XmlRoot(ElementName = "div", Namespace = "http://www.w3.org/ns/SMIL"), Serializable]
	public class Div
	{
	}

	[XmlRoot(ElementName = "textStyle", Namespace = "http://www.w3.org/ns/SMIL"), Serializable]
	public class TextStyle
	{
	}

	[XmlRoot(ElementName = "textStyling", Namespace = "http://www.w3.org/ns/SMIL"), Serializable]
	public class TextStyling
	{
	}

	#endregion

	#region Transition Modules
	[XmlRoot(ElementName = "transition", Namespace = "http://www.w3.org/ns/SMIL"), Serializable]
	public class Transition : TestObjectInheritedBase
	{
		[XmlAttribute(AttributeName = "skip-content")]
		[DefaultValueAttribute(true)]
		public bool skip_content { get; set; }
		[XmlAttribute(AttributeName = "borderWidth")]
		[DefaultValueAttribute(0)]
		public int borderWidth { get; set; }
		[XmlAttribute(AttributeName = "customTest")]
		public String customTest { get; set; }
		[XmlAttribute(AttributeName = "direction")]
		[DefaultValueAttribute(Direction.forward)]
		public Direction direction { get; set; }
		[XmlAttribute(AttributeName = "dur")]
		public String dur { get; set; }
		[XmlAttribute(AttributeName = "endProgress")]
		[DefaultValueAttribute("1.0")]
		public String endProgress { get; set; }
		[XmlAttribute(AttributeName = "fadeColor")]
		[DefaultValueAttribute("black")]
		public String fadeColor { get; set; }
		[XmlAttribute(AttributeName = "scope")]
		[DefaultValueAttribute(Scope.region)]
		public Scope scope { get; set; }
		[XmlAttribute(AttributeName = "startProgress")]
		[DefaultValueAttribute("1.0")]
		public String startProgress { get; set; }
		[XmlAttribute(AttributeName = "subtype")]
		public Subtype subtype { get; set; }
		[XmlAttribute(AttributeName = "type")]
		public type type { get; set; }
		[XmlAttribute(AttributeName = "vertRepeat")]
		[DefaultValueAttribute("1")]
		public String vertRepeat { get; set; }
		
		public Transition()
		{
			borderWidth = 0;
			startProgress = endProgress = "1.0";
			fadeColor = "black";
			vertRepeat = "1";
		}
	}

	#endregion

	#region Other
	[XmlRoot(ElementName = "paramGroup", Namespace = "http://www.w3.org/ns/SMIL"), Serializable]
	public class ParamGroup : BaseObject
	{
		#region Default Attribute
		[XmlAttribute(AttributeName = "skip-content")]
		[DefaultValueAttribute(true)]
		public bool skip_content { get; set; }
		#endregion

		[XmlElement(ElementName = "param", IsNullable = true)]
		public Collection<Param> arrParamCollection = new Collection<Param>();

		[XmlElement(ElementName = "metadata", IsNullable = true)]
		public Collection<Metadata> arrMetadataCollection = new Collection<Metadata>();

		public ParamGroup()
		{
			skip_content = true;
		}
	}

	[XmlRoot(ElementName = "param", Namespace = "http://www.w3.org/ns/SMIL"), Serializable]
	public class Param : TestObjectInheritedBase
	{
		#region Default Attribute
		[XmlAttribute(AttributeName = "name")]
		public String name { get; set; }
		[XmlAttribute(AttributeName = "customTest")]
		public String customTest { get; set; }
		[XmlAttribute(AttributeName = "skip-content")]
		[DefaultValueAttribute(true)]
		public bool skip_content { get; set; }
		[XmlAttribute(AttributeName = "type")]
		public String type { get; set; }
		[XmlAttribute(AttributeName = "value")]
		public String Value { get; set; }
		[XmlAttribute(AttributeName = "valuetype")]
		[DefaultValueAttribute(ValueType._data)]
		public ValueType valuetype { get; set; }
		#endregion

		[XmlElement(ElementName = "metadata", IsNullable = true)]
		public Collection<Metadata> arrMetadataCollection = new Collection<Metadata>();

		public Param()
		{
			valuetype = ValueType._data;
			skip_content = true;
		}
		public Param(string _name, string _value, ValueType _valuetype)
		{
			name = _name;
			Value = _value;
			valuetype = _valuetype;
			skip_content = true;
		}
	}
	
	[XmlRoot(ElementName = "customAttributes", Namespace = "http://www.w3.org/ns/SMIL"), Serializable]
	public class CustomAttributes : BaseObject
	{
		[XmlAttribute(AttributeName = "skip-content")]
		[DefaultValueAttribute(true)]
		public bool skip_content { get; set; }

		[XmlElement(ElementName = "customTest", IsNullable = true)]
		public Collection<CustomTest> arrCustomTestCollection = new Collection<CustomTest>();

		[XmlElement(ElementName = "metadata", IsNullable = true)]
		public Collection<Metadata> arrMetadataCollection = new Collection<Metadata>();


		public CustomAttributes()
		{
			skip_content = true;
		}
	}

	[XmlRoot(ElementName = "customTest", Namespace = "http://www.w3.org/ns/SMIL"), Serializable]
	public class CustomTest : BaseObject
	{
		[XmlAttribute(AttributeName = "skip-content")]
		[DefaultValueAttribute(true)]
		public bool skip_content { get; set; }
		[XmlAttribute(AttributeName = "defaultState")]
		[DefaultValueAttribute(false)]
		public bool defaultState { get; set; }
		[XmlAttribute(AttributeName = "override")]
		[DefaultValueAttribute(Override.hidden)]
		public Override Override { get; set; }
		[XmlAttribute(AttributeName = "uid")]
		public String uid { get; set; }

		[XmlElement(ElementName = "metadata", IsNullable = true)]
		public Collection<Metadata> arrMetadataCollection = new Collection<Metadata>();

		public CustomTest()
		{
			skip_content = true;
			defaultState = false;
			Override = Override.hidden;
		}

	}

	[XmlRoot(ElementName = "priorityClass", Namespace = "http://www.w3.org/ns/SMIL"), Serializable]
	public class PriorityClass : TestObjectInheritedBase
	{
		#region MediaDescription Attributes
		[XmlAttribute(AttributeName = "abstract")]
		public String Abstract { get; set; }
		[XmlAttribute(AttributeName = "author")]
		public String author { get; set; }
		[XmlAttribute(AttributeName = "copyright")]
		public String copyright { get; set; }
		#endregion

		[XmlAttribute(AttributeName = "customTest")]
		public String customTest { get; set; }
		[XmlAttribute(AttributeName = "higher")]
		[DefaultValueAttribute(Higher.pause)]
		public Higher higher { get; set; }
		[XmlAttribute(AttributeName = "lower")]
		[DefaultValueAttribute(Lower.defer)]
		public Lower lower { get; set; }
		[XmlAttribute(AttributeName = "pauseDisplay")]
		[DefaultValueAttribute(PauseDisplay.show)]
		public PauseDisplay pauseDisplay { get; set; }
		[XmlAttribute(AttributeName = "peers")]
		[DefaultValueAttribute(Peers.stop)]
		public Peers peers { get; set; }
		[XmlAttribute(AttributeName = "skip-content")]
		[DefaultValueAttribute(true)]
		public bool skip_content { get; set; }

		[XmlElement(Type = typeof(Par), ElementName = "par", IsNullable = true)]
		[XmlElement(Type = typeof(Seq), ElementName = "seq", IsNullable = true)]
		[XmlElement(Type = typeof(Excl), ElementName = "excl", IsNullable = true)]
		[XmlElement(Type = typeof(Ref), ElementName = "ref", IsNullable = true)]
		[XmlElement(Type = typeof(Animation), ElementName = "animation", IsNullable = true)]
		[XmlElement(Type = typeof(Audio), ElementName = "audio", IsNullable = true)]
		[XmlElement(Type = typeof(Img), ElementName = "img", IsNullable = true)]
		[XmlElement(Type = typeof(Text), ElementName = "text", IsNullable = true)]
		[XmlElement(Type = typeof(TextStream), ElementName = "textstream", IsNullable = true)]
		[XmlElement(Type = typeof(Video), ElementName = "video", IsNullable = true)]
        //[XmlElement(Type = typeof(Multimedia), ElementName = "multimedia", IsNullable = true)]
		public Collection<object> arrCollection = new Collection<object>();

		[XmlElement(Type = typeof(Animate), ElementName = "animate", IsNullable = true)]
		[XmlElement(Type = typeof(AnimateColor), ElementName = "animateColor", IsNullable = true)]
		[XmlElement(Type = typeof(AnimateMotion), ElementName = "animateMotion", IsNullable = true)]
		[XmlElement(Type = typeof(Set), ElementName = "set", IsNullable = true)]
		public Collection<AnmationObject> arrAnimationObjectCollection = new Collection<AnmationObject>();

		[XmlElement(Type = typeof(Prefetch), ElementName = "prefetch", IsNullable = true)]
		[XmlElement(Type = typeof(Switch), ElementName = "switch", IsNullable = true)]
		public Collection<ContentControlObject> arrContentControlObjectCollection = new Collection<ContentControlObject>();

		[XmlElement(Type = typeof(Delvalue), ElementName = "delvalue", IsNullable = true)]
		[XmlElement(Type = typeof(Newvalue), ElementName = "newvalue", IsNullable = true)]
		[XmlElement(Type = typeof(Send), ElementName = "send", IsNullable = true)]
		[XmlElement(Type = typeof(Setvalue), ElementName = "setvalue", IsNullable = true)]
		public Collection<StateObject> arrStateObjectCollection = new Collection<StateObject>();

		[XmlElement(ElementName = "a", Type = typeof(a))]
		public Collection<a> arraRefCollection = new Collection<a>();

		[XmlElement(ElementName = "metadata", IsNullable = true)]
		public Collection<Metadata> arrMetadataCollection = new Collection<Metadata>();

		public PriorityClass()
		{
			lower = Lower.defer;
			higher = Higher.pause;
			pauseDisplay = PauseDisplay.show;
			peers = Peers.stop;
			skip_content = true;
		}
	}

	[XmlRoot(ElementName = "state", Namespace = "http://www.w3.org/ns/SMIL"), Serializable]
	public class State : BaseObject
	{
		[XmlAttribute(AttributeName = "language")]
		[DefaultValueAttribute("http://www.w3.org/TR/1999/REC-xpath-19991116")]
		public String language { get; set; }
		[XmlAttribute(AttributeName = "skip-content")]
		[DefaultValueAttribute(true)]
		public bool skip_content { get; set; }
		[XmlAttribute(AttributeName = "src")]
		public String src { get; set; }

		public State()
		{
			language = "http://www.w3.org/TR/1999/REC-xpath-19991116";
			skip_content = true;
		}
	}

	[XmlRoot(ElementName = "submission", Namespace = "http://www.w3.org/ns/SMIL"), Serializable]
	public class Submission : BaseObject
	{
		[XmlAttribute(AttributeName = "action")]
		public String action { get; set; }
		[XmlAttribute(AttributeName = "method")]
		public Method method { get; set; }
		[XmlAttribute(AttributeName = "ref")]
		public String Ref { get; set; }
		[XmlAttribute(AttributeName = "replace")]
		public Replace replace { get; set; }
		[XmlAttribute(AttributeName = "target")]
		public String target { get; set; }

		public Submission()
		{
		}
	}

	#endregion

	public class Convert
	{
		private static string GetRidOfStuff(string objValue)
		{
			objValue = objValue.Replace("px", "");
			objValue = objValue.Replace("s", "");
			objValue = objValue.Trim();

			return objValue;		
		}

		static public Int32 ToInt32(string objValue)
		{
			string sTemp = GetRidOfStuff(objValue);
			try
			{
				return System.Convert.ToInt32(sTemp);
			}
			catch
			{
				//if (sTemp.Equals("indefinite")) return 0;
				//else if (sTemp.Equals("Auto")) return 0;
				//else
				return 0;
			}
		}

		static public Double ToDouble(string objValue)
		{
			string sTemp = GetRidOfStuff(objValue);
			try
			{
				return System.Convert.ToDouble(GetRidOfStuff(objValue));
			}
			catch
			{
				if (sTemp.ToLower().Equals("indefinite")) return 0;
				//else if (sTemp.ToLower().Equals("auto")) return Double.NaN;
				else
				return Double.NaN;
			}
		}
	}
}
