﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Smil.Library
{
	[Serializable]
	public enum ValueType
	{
		[XmlEnum(Name = "data")]
		_data,
		[XmlEnum(Name = "ref")]
		_ref,
		[XmlEnum(Name = "object")]
		_object
	}

	[Serializable]
	public enum Version
	{
		[XmlEnum(Name = "1.0")]
		ver10,
		[XmlEnum(Name = "2.0")]
		ver20,
		[XmlEnum(Name = "2.1")]
		ver21,
		[XmlEnum(Name = "3.0")]
		ver30
	}

	[Serializable]
	public enum Profile
	{
		[XmlEnum(Name = "Language")]
		Language,
		[XmlEnum(Name = "UnifiedMobile")]
		UnifiedMobile,
		[XmlEnum(Name = "Daisy")]
		Daisy,
		[XmlEnum(Name = "Tiny")]
		Tiny,
		[XmlEnum(Name = "smilText")]
		smilText
	}

	[Serializable]
	public enum Fit
	{
		[XmlEnum(Name = "fill")]
		fill,
		[XmlEnum(Name = "hidden")]
		hidden,
		[XmlEnum(Name = "meet")]
		meet,
		[XmlEnum(Name = "meetBest")]
		meetBest,
		[XmlEnum(Name = "scroll")]
		scroll,
		[XmlEnum(Name = "slice")]
		slice
	}

	[Serializable]
	public enum Fill
	{
		[XmlEnum(Name = "remove")]
		remove,
		[XmlEnum(Name = "freeze")]
		freeze,
		[XmlEnum(Name = "hold")]
		hold,
		[XmlEnum(Name = "transition")]
		transition,
		[XmlEnum(Name = "auto")]
		auto,
		[XmlEnum(Name = "default")]
		Default
	}

	[Serializable]
	public enum mediaRepeat
	{
		[XmlEnum(Name = "preserve")]
		preserve,
		[XmlEnum(Name = "strip")]
		strip
	}

	[Serializable]
	public enum itsdir
	{
		[XmlEnum(Name = "lro")]
		lro,
		[XmlEnum(Name = "ltr")]
		ltr,
		[XmlEnum(Name = "rlo")]
		rlo,
		[XmlEnum(Name = "rtl")]
		rtl
	}

	[Serializable]
	public enum itslocNoteType
	{
		[XmlEnum(Name = "alert")]
		alert,
		[XmlEnum(Name = "description")]
		description
	}

	[Serializable]
	public enum Align
	{
		[XmlEnum(Name = "bottomLeft")]
		bottomLeft,
		[XmlEnum(Name = "bottomMid")]
		bottomMid,
		[XmlEnum(Name = "bottomRight")]
		bottomRight,
		[XmlEnum(Name = "center")]
		center,
		[XmlEnum(Name = "midLeft")]
		midLeft,
		[XmlEnum(Name = "midRight")]
		midRight,
		[XmlEnum(Name = "topLeft")]
		topLeft,
		[XmlEnum(Name = "topMid")]
		topMid,
		[XmlEnum(Name = "topRight")]
		topRight
	}

	[Serializable]
	public enum soundAlign
	{
		[XmlEnum(Name = "both")]
		both,
		[XmlEnum(Name = "left")]
		left,
		[XmlEnum(Name = "right")]
		right
	}

	[Serializable]
	public enum backgroundRepeat
	{
		[XmlEnum(Name = "repeat")]
		repeat,
		[XmlEnum(Name = "inherit")]
		inherit,
		[XmlEnum(Name = "noRepeat")]
		noRepeat,
		[XmlEnum(Name = "repeatX")]
		repeatX,
		[XmlEnum(Name = "repeatY")]
		repeatY
	}

	[Serializable]
	public enum textFontStyle
	{
		[XmlEnum(Name = "inherit")]
		inherit,
		[XmlEnum(Name = "italic")]
		italic,
		[XmlEnum(Name = "normal")]
		normal,
		[XmlEnum(Name = "oblique")]
		oblique,
		[XmlEnum(Name = "reverseOblique")]
		reverseOblique
	}

	[Serializable]
	public enum textFontWeight
	{
		[XmlEnum(Name = "inherit")]
		inherit,
		[XmlEnum(Name = "bold")]
		bold,
		[XmlEnum(Name = "normal")]
		normal
	}

	[Serializable]
	public enum space
	{
		[XmlEnum(Name = "default")]
		Default,
		[XmlEnum(Name = "preserve")]
		preserve
	}

	[Serializable]
	public enum erase
	{
		[XmlEnum(Name = "whenDone")]
		whenDone,
		[XmlEnum(Name = "never")]
		never
	}
	[Serializable]
	public enum showBackground
	{
		[XmlEnum(Name = "always")]
		always,
		[XmlEnum(Name = "whenActive")]
		whenActive
	}
	[Serializable]
	public enum textAlign
	{
		[XmlEnum(Name = "inherit")]
		inherit,
		[XmlEnum(Name = "center")]
		center,
		[XmlEnum(Name = "end")]
		end,
		[XmlEnum(Name = "left")]
		left,
		[XmlEnum(Name = "right")]
		right,
		[XmlEnum(Name = "start")]
		start
	}
	[Serializable]
	public enum textDirection
	{
		[XmlEnum(Name = "inherit")]
		inherit,
		[XmlEnum(Name = "ltr")]
		ltr,
		[XmlEnum(Name = "ltro")]
		ltro,
		[XmlEnum(Name = "rtl")]
		rtl,
		[XmlEnum(Name = "rtlo")]
		rtlo
	}
	[Serializable]
	public enum textMode
	{
		[XmlEnum(Name = "inherit")]
		inherit,
		[XmlEnum(Name = "append")]
		append,
		[XmlEnum(Name = "crawl")]
		crawl,
		[XmlEnum(Name = "jump")]
		jump,
		[XmlEnum(Name = "replace")]
		replace,
		[XmlEnum(Name = "scroll")]
		scroll
	}
	[Serializable]
	public enum textPlace
	{
		[XmlEnum(Name = "inherit")]
		inherit,
		[XmlEnum(Name = "center")]
		center,
		[XmlEnum(Name = "end")]
		end,
		[XmlEnum(Name = "start")]
		start
	}
	[Serializable]
	public enum textWrapOption
	{
		[XmlEnum(Name = "wrap")]
		wrap,
		[XmlEnum(Name = "noWrap")]
		noWrap,
		[XmlEnum(Name = "inherit")]
		inherit
	}
	[Serializable]
	public enum textWritingMode
	{
		[XmlEnum(Name = "inherit")]
		inherit,
		[XmlEnum(Name = "lr")]
		lr,
		[XmlEnum(Name = "lr-tb")]
		lr_tb,
		[XmlEnum(Name = "rl")]
		rl,
		[XmlEnum(Name = "rl-tb")]
		rl_tb,
		[XmlEnum(Name = "tb-lr")]
		tb_lr,
		[XmlEnum(Name = "tb-rl")]
		tb_rl
	}
	[Serializable]
	public enum close
	{
		[XmlEnum(Name = "onRequest")]
		onRequest,
		[XmlEnum(Name = "whenNotActive")]
		whenNotActive
	}
	[Serializable]
	public enum open
	{
		[XmlEnum(Name = "onStart")]
		onStart,
		[XmlEnum(Name = "whenNotActive")]
		whenNotActive
	}

	[Serializable]
	public enum FillDefault
	{
		[XmlEnum(Name = "auto")]
		auto,
		[XmlEnum(Name = "freeze")]
		freeze,
		[XmlEnum(Name = "hold")]
		hold,
		[XmlEnum(Name = "inherit")]
		inherit,
		[XmlEnum(Name = "remove")]
		remove,
		[XmlEnum(Name = "transition")]
		transition
	}	
	[Serializable]
	public enum Restart
	{
		[XmlEnum(Name = "always")]
		always,
		[XmlEnum(Name = "default")]
		Default,
		[XmlEnum(Name = "never")]
		never,
		[XmlEnum(Name = "whenNotActive")]
		whenNotActive
	}

	[Serializable]
	public enum RestartDefault
	{
		[XmlEnum(Name = "always")]
		always,
		[XmlEnum(Name = "inherit")]
		inherit,
		[XmlEnum(Name = "never")]
		never,
		[XmlEnum(Name = "whenNotActive")]
		whenNotActive
	}	
	[Serializable]
	public enum SyncBehavior
	{
		[XmlEnum(Name = "canSlip")]
		canSlip,
		[XmlEnum(Name = "default")]
		Default,
		[XmlEnum(Name = "independent")]
		independent,
		[XmlEnum(Name = "locked")]
		locked
	}

	[Serializable]
	public enum SyncBehaviorDefault
	{
		[XmlEnum(Name = "canSlip")]
		canSlip,
		[XmlEnum(Name = "independent")]
		independent,
		[XmlEnum(Name = "inherit")]
		inherit,
		[XmlEnum(Name = "locked")]
		locked
	}

	[Serializable]
	public enum Higher
	{
		[XmlEnum(Name = "pause")]
		pause,
		[XmlEnum(Name = "stop")]
		stop
	}
	[Serializable]
	public enum Lower
	{
		[XmlEnum(Name = "defer")]
		defer,
		[XmlEnum(Name = "never")]
		never
	}	

	[Serializable]
	public enum PauseDisplay
	{
		[XmlEnum(Name = "disable")]
		disable,
		[XmlEnum(Name = "hide")]
		hide,
		[XmlEnum(Name = "show")]
		show
	}	
	
	[Serializable]
	public enum Peers
	{
		[XmlEnum(Name = "defer")]
		defer,
		[XmlEnum(Name = "never")]
		never,
		[XmlEnum(Name = "pause")]
		pause,
		[XmlEnum(Name = "stop")]
		stop
	}

	[Serializable]
	public enum Override
	{
		[XmlEnum(Name = "hidden")]
		hidden,
		[XmlEnum(Name = "visible")]
		visible
	}

	[Serializable]
	public enum AllowReorder
	{
		[XmlEnum(Name = "yes")]
		yes,
		[XmlEnum(Name = "no")]
		no
	}

	[Serializable]
	public enum SystemCaption
	{
		[XmlEnum(Name = "on")]
		on,
		[XmlEnum(Name = "off")]
		off
	}

	[Serializable]
	public enum OnOff
	{
		[XmlEnum(Name = "on")]
		on,
		[XmlEnum(Name = "off")]
		off
	}

	[Serializable]
	public enum Direction
	{
		[XmlEnum(Name = "forward")]
		forward,
		[XmlEnum(Name = "reverse")]
		reverse
	}

	[Serializable]
	public enum Scope
	{
		[XmlEnum(Name = "region")]
		region,
		[XmlEnum(Name = "screen")]
		screen
	}

	[Serializable]
	public enum Method
	{
		[XmlEnum(Name = "get")]
		get,
		[XmlEnum(Name = "post")]
		post,
		[XmlEnum(Name = "put")]
		put
	}

	[Serializable]
	public enum Replace
	{
		[XmlEnum(Name = "none")]
		none,
		[XmlEnum(Name = "all")]
		all,
		[XmlEnum(Name = "instance")]
		instance
	}
	
	[Serializable]
	public enum Subtype
	{
		[XmlEnum(Name = "bottom")]
		bottom,
		[XmlEnum(Name = "bottomCenter")]
		bottomCenter,
		[XmlEnum(Name = "bottomLeft")]
		bottomLeft,
		[XmlEnum(Name = "bottomLeftClockwise")]
		bottomLeftClockwise,
		[XmlEnum(Name = "bottomLeftCounterClockwise")]
		bottomLeftCounterClockwise,
		[XmlEnum(Name = "bottomLeftDiagonal")]
		bottomLeftDiagonal,
		[XmlEnum(Name = "bottomRight")]
		bottomRight,
		[XmlEnum(Name = "bottomRightClockwise")]
		bottomRightClockwise,
		[XmlEnum(Name = "bottomRightCounterClockwise")]
		bottomRightCounterClockwise,
		[XmlEnum(Name = "bottomRightDiagonal")]
		bottomRightDiagonal,
		[XmlEnum(Name = "centerRight")]
		centerRight,
		[XmlEnum(Name = "centerTop")]
		centerTop,
		[XmlEnum(Name = "circle")]
		circle,
		[XmlEnum(Name = "clockwiseBottom")]
		clockwiseBottom,
		[XmlEnum(Name = "clockwiseBottomRight")]
		clockwiseBottomRight,
		[XmlEnum(Name = "clockwiseLeft")]
		clockwiseLeft,
		[XmlEnum(Name = "clockwiseNine")]
		clockwiseNine,
		[XmlEnum(Name = "clockwiseRight")]
		clockwiseRight,
		[XmlEnum(Name = "clockwiseSix")]
		clockwiseSix,
		[XmlEnum(Name = "clockwiseThree")]
		clockwiseThree,
		[XmlEnum(Name = "clockwiseTop")]
		clockwiseTop,
		[XmlEnum(Name = "clockwiseTopLeft")]
		clockwiseTopLeft,
		[XmlEnum(Name = "clockwiseTwelve")]
		clockwiseTwelve,
		[XmlEnum(Name = "cornersIn")]
		cornersIn,
		[XmlEnum(Name = "cornersOut")]
		cornersOut,
		[XmlEnum(Name = "counterClockwiseBottomLeft")]
		counterClockwiseBottomLeft,
		[XmlEnum(Name = "counterClockwiseTopRight")]
		counterClockwiseTopRight,
		[XmlEnum(Name = "crossfade")]
		crossfade,
		[XmlEnum(Name = "diagonalBottomLeft")]
		diagonalBottomLeft,
		[XmlEnum(Name = "diagonalBottomLeftOpposite")]
		diagonalBottomLeftOpposite,
		[XmlEnum(Name = "diagonalTopLeft")]
		diagonalTopLeft,
		[XmlEnum(Name = "diagonalTopLeftOpposite")]
		diagonalTopLeftOpposite,
		[XmlEnum(Name = "diamond")]
		diamond,
		[XmlEnum(Name = "doubleBarnDoor")]
		doubleBarnDoor,
		[XmlEnum(Name = "doubleDiamond")]
		doubleDiamond,
		[XmlEnum(Name = "down")]
		down,
		[XmlEnum(Name = "fadeFromColor")]
		fadeFromColor,
		[XmlEnum(Name = "fadeToColor")]
		fadeToColor,
		[XmlEnum(Name = "fanInHorizontal")]
		fanInHorizontal,
		[XmlEnum(Name = "fanInVertical")]
		fanInVertical,
		[XmlEnum(Name = "fanOutHorizontal")]
		fanOutHorizontal,
		[XmlEnum(Name = "fanOutVertical")]
		fanOutVertical,
		[XmlEnum(Name = "fivePoint")]
		fivePoint,
		[XmlEnum(Name = "fourBlade")]
		fourBlade,
		[XmlEnum(Name = "fourBoxHorizontal")]
		fourBoxHorizontal,
		[XmlEnum(Name = "fourBoxVertical")]
		fourBoxVertical,
		[XmlEnum(Name = "fourPoint")]
		fourPoint,
		[XmlEnum(Name = "fromBottom")]
		fromBottom,
		[XmlEnum(Name = "fromLeft")]
		fromLeft,
		[XmlEnum(Name = "fromRight")]
		fromRight,
		[XmlEnum(Name = "fromTop")]
		fromTop,
		[XmlEnum(Name = "heart")]
		heart,
		[XmlEnum(Name = "horizontal")]
		horizontal,
		[XmlEnum(Name = "horizontalLeft")]
		horizontalLeft,
		[XmlEnum(Name = "horizontalLeftSame")]
		horizontalLeftSame,
		[XmlEnum(Name = "horizontalRight")]
		horizontalRight,
		[XmlEnum(Name = "horizontalRightSame")]
		horizontalRightSame,
		[XmlEnum(Name = "horizontalTopLeftOpposite")]
		horizontalTopLeftOpposite,
		[XmlEnum(Name = "horizontalTopRightOpposite")]
		horizontalTopRightOpposite,
		[XmlEnum(Name = "keyhole")]
		keyhole,
		[XmlEnum(Name = "left")]
		left,
		[XmlEnum(Name = "leftCenter")]
		leftCenter,
		[XmlEnum(Name = "leftToRight")]
		leftToRight,
		[XmlEnum(Name = "oppositeHorizontal")]
		oppositeHorizontal,
		[XmlEnum(Name = "oppositeVertical")]
		oppositeVertical,
		[XmlEnum(Name = "parallelDiagonal")]
		parallelDiagonal,
		[XmlEnum(Name = "parallelDiagonalBottomLeft")]
		parallelDiagonalBottomLeft,
		[XmlEnum(Name = "parallelDiagonalTopLeft")]
		parallelDiagonalTopLeft,
		[XmlEnum(Name = "parallelVertical")]
		parallelVertical,
		[XmlEnum(Name = "rectangle")]
		rectangle,
		[XmlEnum(Name = "right")]
		right,
		[XmlEnum(Name = "rightCenter")]
		rightCenter,
		[XmlEnum(Name = "sixPoint")]
		sixPoint,
		[XmlEnum(Name = "top")]
		top,
		[XmlEnum(Name = "topCenter")]
		topCenter,
		[XmlEnum(Name = "topLeft")]
		topLeft,
		[XmlEnum(Name = "topLeftClockwise")]
		topLeftClockwise,
		[XmlEnum(Name = "topLeftCounterClockwise")]
		topLeftCounterClockwise,
		[XmlEnum(Name = "topLeftDiagonal")]
		topLeftDiagonal,
		[XmlEnum(Name = "topLeftHorizontal")]
		topLeftHorizontal,
		[XmlEnum(Name = "topLeftVertical")]
		topLeftVertical,
		[XmlEnum(Name = "topRight")]
		topRight,
		[XmlEnum(Name = "topRightClockwise")]
		topRightClockwise,
		[XmlEnum(Name = "topRightCounterClockwise")]
		topRightCounterClockwise,
		[XmlEnum(Name = "topRightDiagonal")]
		topRightDiagonal,
		[XmlEnum(Name = "topToBottom")]
		topToBottom,
		[XmlEnum(Name = "twoBladeHorizontal")]
		twoBladeHorizontal,
		[XmlEnum(Name = "twoBladeVertical")]
		twoBladeVertical,
		[XmlEnum(Name = "twoBoxBottom")]
		twoBoxBottom,
		[XmlEnum(Name = "twoBoxLeft")]
		twoBoxLeft,
		[XmlEnum(Name = "twoBoxRight")]
		twoBoxRight,
		[XmlEnum(Name = "twoBoxTop")]
		twoBoxTop,
		[XmlEnum(Name = "up")]
		up,
		[XmlEnum(Name = "vertical")]
		vertical,
		[XmlEnum(Name = "verticalBottomLeftOpposite")]
		verticalBottomLeftOpposite,
		[XmlEnum(Name = "verticalBottomSame")]
		verticalBottomSame,
		[XmlEnum(Name = "verticalLeft")]
		verticalLeft,
		[XmlEnum(Name = "verticalRight")]
		verticalRight,
		[XmlEnum(Name = "verticalTopLeftOpposite")]
		verticalTopLeftOpposite,
		[XmlEnum(Name = "verticalTopSame")]
		verticalTopSame
	}

	[Serializable]
	public enum type
	{
		[XmlEnum(Name = "audioFade")]
		audioFade,
		[XmlEnum(Name = "audioVisualFade")]
		audioVisualFade,
		[XmlEnum(Name = "barWipe")]
		barWipe,
		[XmlEnum(Name = "barnDoorWipe")]
		barnDoorWipe,
		[XmlEnum(Name = "barnVeeWipe")]
		barnVeeWipe,
		[XmlEnum(Name = "barnZigZagWipe")]
		barnZigZagWipe,
		[XmlEnum(Name = "bowTieWipe")]
		bowTieWipe,
		[XmlEnum(Name = "boxSnakesWipe")]
		boxSnakesWipe,
		[XmlEnum(Name = "boxWipe")]
		boxWipe,
		[XmlEnum(Name = "clockWipe")]
		clockWipe,
		[XmlEnum(Name = "diagonalWipe")]
		diagonalWipe,
		[XmlEnum(Name = "doubleFanWipe")]
		doubleFanWipe,
		[XmlEnum(Name = "doubleSweepWipe")]
		doubleSweepWipe,
		[XmlEnum(Name = "ellipseWipe")]
		ellipseWipe,
		[XmlEnum(Name = "eyeWipe")]
		eyeWipe,
		[XmlEnum(Name = "fade")]
		fade,
		[XmlEnum(Name = "fanWipe")]
		fanWipe,
		[XmlEnum(Name = "fourBoxWipe")]
		fourBoxWipe,
		[XmlEnum(Name = "hexagonWipe")]
		hexagonWipe,
		[XmlEnum(Name = "irisWipe")]
		irisWipe,
		[XmlEnum(Name = "miscDiagonalWipe")]
		miscDiagonalWipe,
		[XmlEnum(Name = "miscShapeWipe")]
		miscShapeWipe,
		[XmlEnum(Name = "parallelSnakesWipe")]
		parallelSnakesWipe,
		[XmlEnum(Name = "pentagonWipe")]
		pentagonWipe,
		[XmlEnum(Name = "pinWheelWipe")]
		pinWheelWipe,
		[XmlEnum(Name = "pushWipe")]
		pushWipe,
		[XmlEnum(Name = "roundRectWipe")]
		roundRectWipe,
		[XmlEnum(Name = "saloonDoorWipe")]
		saloonDoorWipe,
		[XmlEnum(Name = "singleSweepWipe")]
		singleSweepWipe,
		[XmlEnum(Name = "slideWipe")]
		slideWipe,
		[XmlEnum(Name = "snakeWipe")]
		snakeWipe,
		[XmlEnum(Name = "spiralWipe")]
		spiralWipe,
		[XmlEnum(Name = "starWipe")]
		starWipe,
		[XmlEnum(Name = "triangleWipe")]
		triangleWipe,
		[XmlEnum(Name = "veeWipe")]
		veeWipe,
		[XmlEnum(Name = "waterfallWipe")]
		waterfallWipe,
		[XmlEnum(Name = "windshieldWipe")]
		windshieldWipe,
		[XmlEnum(Name = "zigZagWipe")]
		zigZagWipe
	}
}
