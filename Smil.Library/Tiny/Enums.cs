﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace Smil.Profile.Tiny.Library
{
	public class Keyword
	{
		public static string Auto = "auto";
		public static string Indefinite = "indefinite";
		public static string Media = "media";
		public static string Last = "last";
	}

	[Serializable]
	public enum ValueType
	{
		[XmlEnum(Name = "data")]
		_data,
		[XmlEnum(Name = "ref")]
		_ref,
		[XmlEnum(Name = "object")]
		_object
	}

	[Serializable]
	public enum Version
	{
		[XmlEnum(Name = "1.0")]
		ver10,
		[XmlEnum(Name = "2.0")]
		ver20,
		[XmlEnum(Name = "2.1")]
		ver21,
		[XmlEnum(Name = "3.0")]
		ver30
	}

	[Serializable]
	public enum Profile
	{
		[XmlEnum(Name = "Language")]
		Language,
		[XmlEnum(Name = "UnifiedMobile")]
		UnifiedMobile,
		[XmlEnum(Name = "Daisy")]
		Daisy,
		[XmlEnum(Name = "Tiny")]
		Tiny,
		[XmlEnum(Name = "smilText")]
		smilText
	}

	[Serializable]
	public enum Fit
	{
		[XmlEnum(Name = "fill")]
		fill,
		[XmlEnum(Name = "hidden")]
		hidden,
		[XmlEnum(Name = "meet")]
		meet,
		[XmlEnum(Name = "meetBest")]
		meetBest,
		[XmlEnum(Name = "scroll")]
		scroll,
		[XmlEnum(Name = "slice")]
		slice
	}

	[Serializable]
	public enum Fill
	{
		[XmlEnum(Name = "remove")]
		remove,
		[XmlEnum(Name = "freeze")]
		freeze,
		[XmlEnum(Name = "hold")]
		hold,
		[XmlEnum(Name = "transition")]
		transition,
		[XmlEnum(Name = "auto")]
		auto,
		[XmlEnum(Name = "default")]
		Default
	}

	[Serializable]
	public enum mediaRepeat
	{
		[XmlEnum(Name = "preserve")]
		preserve,
		[XmlEnum(Name = "strip")]
		strip
	}
	
}
