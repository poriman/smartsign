﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace Smil.Profile.Tiny.Library
{
	#region Structure Modules
	/// <summary>
	/// Element Base
	/// </summary>
	public class BaseObject
	{
		#region Core Attributes
		[XmlAttribute(AttributeName = "alt")]
		public String alt { get; set; }
		[XmlAttribute(AttributeName = "baseProfile")]
		[DefaultValueAttribute(Profile.Tiny)]
		public Profile baseProfile { get; set; }
		[XmlAttribute(AttributeName = "class")]
		public String Class { get; set; }
		[XmlAttribute(AttributeName = "label")]
		public String label { get; set; }
		[XmlAttribute(AttributeName = "longdesc")]
		public String longdesc { get; set; }
		[XmlAttribute(AttributeName = "readIndex")]
		[DefaultValueAttribute(0)]
		public int readIndex { get; set; }
		[XmlAttribute(AttributeName = "title")]
		public String title { get; set; }
		[XmlAttribute(AttributeName = "version")]
		[DefaultValueAttribute(Version.ver30)]
		public Version version { get; set; }
		[XmlAttribute(AttributeName = "xml:base")]
		public String xmlbase { get; set; }
		[XmlAttribute(AttributeName = "xml:id")]
		public String id { get; set; }
		#endregion

		#region I18n Attributes
		[XmlAttribute(AttributeName = "xml:lang")]
		public String lang { get; set; }
		#endregion

		public BaseObject()
		{
			baseProfile = Profile.Tiny;
			version = Version.ver30;
			readIndex = 0;
		}
	}

	/// <summary>
	/// SMIL Element
	/// </summary>
	[XmlRoot(ElementName = "smil", Namespace="http://www.w3.org/ns/SMIL"), Serializable]
	public class Smil : BaseObject
	{
		[XmlAttribute(AttributeName = "systemRequired")]
		public String systemRequired { get; set; }
		[XmlAttribute(AttributeName = "xmlns")]
		public String xmlns { get; set; }

		[XmlElement(ElementName = "head", IsNullable = true)]
		public Head head { get; set; }
		[XmlElement(ElementName = "body", IsNullable = false)]
		public Body body { get; set; }

		[XmlElement(ElementName = "metadata", IsNullable = true)]
		public Collection<Metadata> arrMetaCollection = new Collection<Metadata>();

		public Smil()
		{
		}
	}

	/// <summary>
	/// Head Element
	/// </summary>
	[XmlRoot(ElementName = "head", Namespace = "http://www.w3.org/ns/SMIL"), Serializable]
	public class Head : BaseObject
	{
		[XmlElement(ElementName = "layout", IsNullable = true)]
		public Layout layout { get; set; }

		[XmlElement(ElementName = "meta", IsNullable = true)]
		public Collection<Meta> arrMetaCollection = new Collection<Meta>();

		[XmlElement(ElementName = "metadata", IsNullable = true)]
		public Collection<Metadata> arrMetadataCollection = new Collection<Metadata>();
	}

	/// <summary>
	/// Body Element
	/// </summary>
	[XmlRoot(ElementName = "body", Namespace = "http://www.w3.org/ns/SMIL"), Serializable]
	public class Body : BaseObject
	{
		#region Default Attribute
		[XmlAttribute(AttributeName = "fill")]
		[DefaultValueAttribute(Fill.Default)]
		public Fill fill { get; set; }
		#endregion

		#region MediaDescription Attributes
		[XmlAttribute(AttributeName = "abstract")]
		public String Abstract { get; set; }
		[XmlAttribute(AttributeName = "author")]
		public String author { get; set; }
		[XmlAttribute(AttributeName = "copyright")]
		public String copyright { get; set; }
		#endregion

		#region Timing Attributes
		[XmlAttribute(AttributeName = "begin")]
		public String begin { get; set; }
		[XmlAttribute(AttributeName = "dur")]
		public String dur { get; set; }
		[XmlAttribute(AttributeName = "end")]
		public String end { get; set; }
		#endregion

		[XmlElement(Type = typeof(Par), ElementName = "par", IsNullable = true)]
		[XmlElement(Type = typeof(Seq), ElementName = "seq", IsNullable = true)]
//		[XmlElement(Type = typeof(Excl), ElementName = "excl", IsNullable = true)]
		[XmlElement(Type = typeof(Ref), ElementName = "ref", IsNullable = true)]
		[XmlElement(Type = typeof(Animation), ElementName = "animation", IsNullable = true)]
		[XmlElement(Type = typeof(Audio), ElementName = "audio", IsNullable = true)]
		[XmlElement(Type = typeof(Img), ElementName = "img", IsNullable = true)]
		[XmlElement(Type = typeof(Text), ElementName = "text", IsNullable = true)]
		[XmlElement(Type = typeof(TextStream), ElementName = "textstream", IsNullable = true)]
		[XmlElement(Type = typeof(Video), ElementName = "video", IsNullable = true)]
        //[XmlElement(Type = typeof(Multimedia), ElementName = "multimedia", IsNullable = true)]
		public Collection<object> arrCollection = new Collection<object>();

		[XmlElement(ElementName = "metadata", IsNullable = true)]
		public Collection<Metadata> arrMetaCollection = new Collection<Metadata>();

		public Body()
		{
			fill = Fill.Default;
		}
	}
	#endregion

	#region Layout Modules
	/// <summary>
	/// Layout Element
	/// </summary>
	[XmlRoot(ElementName = "layout", Namespace = "http://www.w3.org/ns/SMIL"), Serializable]
	public class Layout : BaseObject
	{
		[XmlAttribute(AttributeName = "systemRequired")]
		public String systemRequired { get; set; }

		[XmlAttribute(AttributeName = "type")]
		[DefaultValueAttribute("text/smil-basic-layout")]
		public String type { get; set; }

		/*
		[XmlElement(Type = typeof(RootLayout), ElementName = "root-layout", IsNullable = true)]
		[XmlElement(Type = typeof(Region), ElementName = "region", IsNullable = true)]
		public Collection<RegionBase> arrCollection = new Collection<RegionBase>();
		*/

		[XmlElement(ElementName = "metadata", IsNullable = true)]
		public Collection<Metadata> arrMetaCollection = new Collection<Metadata>();

		public Layout()
		{
			type = "text/smil-basic-layout";
		}
	}

	/// <summary>
	/// Region Interface
	/// </summary>
	[Serializable]
	public class RegionBase
	{
		#region Default Attribute
		[XmlAttribute(AttributeName = "width")]
		public String width { get; set; }
		[XmlAttribute(AttributeName = "height")]
		public String height { get; set; }
		[XmlAttribute(AttributeName = "backgroundColor")]
		public String backgroundColor { get; set; }
		[XmlAttribute(AttributeName = "backgroundOpacity")]
		public String backgroundOpacity { get; set; }
		#endregion
	}

	/// <summary>
	/// RootLayout Element
	/// </summary>
	[XmlRoot(ElementName = "root-layout", IsNullable = false), Serializable]
	public class RootLayout : RegionBase
	{
	}

	/// <summary>
	/// Region Element
	/// </summary>
	[XmlRoot(ElementName = "region", IsNullable = false), Serializable]
	public class Region : RegionBase
	{
		#region Default Attribute
		[XmlAttribute(AttributeName = "xml:id")]
		public String id { get; set; }
		[XmlAttribute(AttributeName = "top")]
		public String top { get; set; }
		[XmlAttribute(AttributeName = "left")]
		public String left { get; set; }
		[XmlAttribute(AttributeName = "bottom")]
		public String bottom { get; set; }
		[XmlAttribute(AttributeName = "right")]
		public String right { get; set; }
		[XmlAttribute(AttributeName = "fit")]
		public Fit fit { get; set; }
		[XmlAttribute(AttributeName = "regionName")]
		public String regionName { get; set; }
		[XmlAttribute(AttributeName = "showBackground")]
		public String showBackground { get; set; }
		[XmlAttribute(AttributeName = "z-index")]
		public String zindex { get; set; }
		#endregion
	}
	#endregion

	#region Schedule Modules
	[Serializable]
	public class ScheduleCollection
	{
		#region Core Attributes
		[XmlAttribute(AttributeName = "alt")]
		public String alt { get; set; }
		[XmlAttribute(AttributeName = "baseProfile")]
		[DefaultValueAttribute(Profile.Tiny)]
		public Profile baseProfile { get; set; }
		[XmlAttribute(AttributeName = "class")]
		public String Class { get; set; }
		[XmlAttribute(AttributeName = "label")]
		public String label { get; set; }
		[XmlAttribute(AttributeName = "longdesc")]
		public String longdesc { get; set; }
		[XmlAttribute(AttributeName = "readIndex")]
		[DefaultValueAttribute(0)]
		public int readIndex { get; set; }
		[XmlAttribute(AttributeName = "title")]
		public String title { get; set; }
		[XmlAttribute(AttributeName = "version")]
		[DefaultValueAttribute(Version.ver30)]
		public Version version { get; set; }
		[XmlAttribute(AttributeName = "xml:base")]
		public String xmlbase { get; set; }
		[XmlAttribute(AttributeName = "xml:id")]
		public String id { get; set; }
		#endregion

		#region I18n Attributes
		[XmlAttribute(AttributeName = "xml:lang")]
		public String lang { get; set; }
		#endregion

		#region MediaDescription Attributes
		[XmlAttribute(AttributeName = "abstract")]
		public String Abstract { get; set; }
		[XmlAttribute(AttributeName = "author")]
		public String author { get; set; }
		[XmlAttribute(AttributeName = "copyright")]
		public String copyright { get; set; }
		#endregion

		#region Timing Attributes
		[XmlAttribute(AttributeName = "begin")]
		public String begin { get; set; }
		[XmlAttribute(AttributeName = "dur")]
		public String dur { get; set; }
		[XmlAttribute(AttributeName = "end")]
		public String end { get; set; }
		//[XmlAttribute(AttributeName = "repeatCount")]
		//public String repeatCount { get; set; }
		//[XmlAttribute(AttributeName = "repeatDur")]
		//public String repeatDur { get; set; }
		//[XmlAttribute(AttributeName = "min")]
		//public String min { get; set; }
		//[XmlAttribute(AttributeName = "max")]
		//public String max { get; set; }
		//private Fill _fill = Fill.Default;
		//[XmlAttribute(AttributeName = "fill")]
		//public Fill fill
		//{
		//    get { return _fill; }
		//    set { _fill = value; }
		//}
		#endregion

		[XmlAttribute(AttributeName = "fill")]
		[DefaultValueAttribute(Fill.Default)]
		public Fill fill { get; set; }

		[XmlAttribute(AttributeName = "systemRequired")]
		public String systemRequired { get; set; }

		[XmlElement(Type = typeof(Par), ElementName = "par", IsNullable = true)]
		[XmlElement(Type = typeof(Seq), ElementName = "seq", IsNullable = true)]
//		[XmlElement(Type = typeof(Excl), ElementName = "excl", IsNullable = true)]
		[XmlElement(Type = typeof(Ref), ElementName = "ref", IsNullable = true)]
		[XmlElement(Type = typeof(Animation), ElementName = "animation", IsNullable = true)]
		[XmlElement(Type = typeof(Audio), ElementName = "audio", IsNullable = true)]
		[XmlElement(Type = typeof(Img), ElementName = "img", IsNullable = true)]
		[XmlElement(Type = typeof(Text), ElementName = "text", IsNullable = true)]
		[XmlElement(Type = typeof(TextStream), ElementName = "textstream", IsNullable = true)]
		[XmlElement(Type = typeof(Video), ElementName = "video", IsNullable = true)]
        //[XmlElement(Type = typeof(Multimedia), ElementName = "multimedia", IsNullable = true)]
		public Collection<object> arrCollection = new Collection<object>();

		public ScheduleCollection()
		{
			readIndex = 0;
			fill = Fill.Default;
			baseProfile = Profile.Tiny;
			version = Version.ver30;
		}
		//#region Region Attribute
		//[XmlAttribute(AttributeName = "region")]
		//public String region { get; set; }
		//#endregion
	}

	[XmlRoot(ElementName = "par", IsNullable = false), Serializable]
	public class Par : ScheduleCollection
	{
		[XmlAttribute(AttributeName = "endsync")]
		[DefaultValueAttribute("last")]
		public String endsync { get; set; }

		public Par()
		{
			endsync = "last";
		}
	}

	[XmlRoot(ElementName = "seq", IsNullable = false), Serializable]
	public class Seq : ScheduleCollection
	{
	}

	/*
	[XmlRoot(ElementName = "excl", IsNullable = false), Serializable]
	public class Excl : ScheduleCollection
	{
		[XmlAttribute(AttributeName = "endsync")]
		public String endsync { get; set; }
	}
	*/
	#endregion

	#region MediaContent Modules
	public class MediaObject
	{
		public MediaObject()
		{
			fill = Fill.Default;
			endsync = "media";
			mediaRepeat = mediaRepeat.preserve;
		}
		#region Default Attribute
		[XmlAttribute(AttributeName = "endsync")]
		[DefaultValueAttribute("media")]
		public String endsync { get; set; }
		[XmlAttribute(AttributeName = "fill")]
		[DefaultValueAttribute(Fill.Default)]
		public Fill fill { get; set; }
		[XmlAttribute(AttributeName = "mediaRepeat")]
		[DefaultValueAttribute(mediaRepeat.preserve)]
		public mediaRepeat mediaRepeat { get; set; }
		[XmlAttribute(AttributeName = "src")]
		public String src { get; set; }
		[XmlAttribute(AttributeName = "systemRequired")]
		public String systemRequired { get; set; }
		[XmlAttribute(AttributeName = "type")]
		public String type { get; set; }

//		[XmlAttribute(AttributeName = "paramGroup")]
//		public string paramGroup { get; set; }
		#endregion

		#region MediaDescription Attributes
		[XmlAttribute(AttributeName = "abstract")]
		public String Abstract { get; set; }
		[XmlAttribute(AttributeName = "author")]
		public String author { get; set; }
		[XmlAttribute(AttributeName = "copyright")]
		public String copyright { get; set; }
		#endregion

		#region Timing Attributes
		[XmlAttribute(AttributeName = "begin")]
		public String begin { get; set; }
		[XmlAttribute(AttributeName = "dur")]
		public String dur { get; set; }
		[XmlAttribute(AttributeName = "end")]
		public String end { get; set; }
		//[XmlAttribute(AttributeName = "repeatCount")]
		//public String repeatCount { get; set; }
		//[XmlAttribute(AttributeName = "repeatDur")]
		//public String repeatDur { get; set; }
		//[XmlAttribute(AttributeName = "min")]
		//public String min { get; set; }
		//[XmlAttribute(AttributeName = "max")]
		//public String max { get; set; }
		//private Fill _fill = Fill.Default;
		//[XmlAttribute(AttributeName = "fill")]
		//public Fill fill
		//{
		//    get { return _fill; }
		//    set { _fill = value; }
		//}
		#endregion

		#region Region Attribute
		/*
		[XmlAttribute(AttributeName = "region")]
		public String region { get; set; }
		*/
		#endregion

		/*
		[XmlElement(ElementName = "paramGroup", Type = typeof(ParamGroup))]
		[XmlElement(ElementName = "param", Type = typeof(Param))]
		public Collection<object> arrParamCollection = new Collection<object>();
		*/

		[XmlElement(ElementName = "metadata", IsNullable = true)]
		public Collection<Metadata> arrMetaCollection = new Collection<Metadata>();

	}

	[XmlRoot(ElementName = "ref", IsNullable = false), Serializable]
	public class Ref : MediaObject
	{
	}

	[XmlRoot(ElementName = "animation", IsNullable = false), Serializable]
	public class Animation : MediaObject
	{
	}

	[XmlRoot(ElementName = "audio", IsNullable = false), Serializable]
	public class Audio : MediaObject
	{
	}

	[XmlRoot(ElementName = "img", IsNullable = false), Serializable]
	public class Img : MediaObject
	{
	}

	[XmlRoot(ElementName = "text", IsNullable = false), Serializable]
	public class Text : MediaObject
	{
	}

	[XmlRoot(ElementName = "textstream", IsNullable = false), Serializable]
	public class TextStream : MediaObject
	{
	}

	[XmlRoot(ElementName = "video", IsNullable = false), Serializable]
	public class Video : MediaObject
	{
	}

    [XmlRoot(ElementName = "multimedia", IsNullable = false), Serializable]
    public class Multimedia : MediaObject
    {
    }
	#endregion

	#region Metainformation Modules
	/// <summary>
	/// Meta Element
	/// </summary>
	[XmlRoot(ElementName = "meta", Namespace = "http://www.w3.org/ns/SMIL"), Serializable]
	public class Meta : BaseObject
	{
		[XmlAttribute(AttributeName = "content")]
		public String content { get; set; }
		[XmlAttribute(AttributeName = "name")]
		public String name { get; set; }
		[XmlAttribute(AttributeName = "skip-content")]
		[DefaultValueAttribute(true)]
		public bool skipContent { get; set; }

		public Meta()
		{
			skipContent = true;
		}
	}

	/// <summary>
	/// Metadata Element
	/// </summary>
	[XmlRoot(ElementName = "metadata", Namespace = "http://www.w3.org/ns/SMIL"), Serializable]
	public class Metadata : BaseObject
	{
		[XmlAttribute(AttributeName = "skip-content")]
		[DefaultValueAttribute(true)]
		public bool skipContent { get; set; }

		public Metadata()
		{
			skipContent = true;
		}
	}
	#endregion

	#region Other
/*	[XmlRoot(ElementName = "paramGroup", Namespace = "http://www.w3.org/ns/SMIL"), Serializable]
	public class ParamGroup : Collection<Param>
	{
		#region Default Attribute
		[XmlAttribute(AttributeName = "xml:id")]
		public String id { get; set; }
		#endregion
	}

	[XmlRoot(ElementName = "param", Namespace = "http://www.w3.org/ns/SMIL"), Serializable]
	public class Param
	{
		#region Default Attribute
		[XmlAttribute(AttributeName = "name")]
		public String name { get; set; }
		[XmlAttribute(AttributeName = "value")]
		public String Value { get; set; }
		[XmlAttribute(AttributeName = "valuetype")]
		public ValueType valueType { get; set; }
		[XmlAttribute(AttributeName = "type")]
		public ValueType type { get; set; }
		#endregion

		public Param()
		{ }
		public Param(string _name, string _value, ValueType _valuetype)
		{
			name = _name;
			Value = _value;
			valueType = _valuetype;
		}
	}
 */
		#endregion
}
