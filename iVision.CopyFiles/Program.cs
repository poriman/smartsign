﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Diagnostics;
using System.Reflection;
using System.Threading;

namespace iVision.CopyFiles
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                string sDestFolder = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetModules()[0].FullyQualifiedName);
                string sWillUpdateFolder = String.Format("{0}\\updates\\{1}",sDestFolder, args[0]);

                Console.WriteLine("Stopping DS Updater..");
                StopProcess(FindProcess("DS.Updater.exe"));
                Thread.Sleep(1000);

                if (RecursiveCopy(sWillUpdateFolder, sDestFolder))
                {
                    Console.WriteLine("Update Success!");
                }
            }
            catch (System.Exception e)
            {
                Console.WriteLine("Update Failed! : " + e.Message);
            }
            finally
            {
                Console.WriteLine("Running DS.Updater Updater..");

                RunProcess("DS.Updater.exe");

                Thread.Sleep(3000);
            }

        }

        public static bool RecursiveCopy(String sSource, String sDest)
        {
            if (!Directory.Exists(sDest))
                Directory.CreateDirectory(sDest);


            string[] directories = Directory.GetDirectories(sSource);

            foreach (string directory in directories)
            {
                DirectoryInfo dic = new DirectoryInfo(directory);
                if (dic.Exists)
                {
                    if (!RecursiveCopy(directory, sDest + "\\" + dic.Name))
                        return false;
                }
            }

            string[] files = Directory.GetFiles(sSource);

            foreach (string file in files)
            {
                FileInfo finfo = new FileInfo(file);
                if (finfo.Exists)
                {
                    try
                    {
                        Console.WriteLine(String.Format("Copying file [{0}] to [{1}]", file, sDest + "\\" + finfo.Name));
                        File.Copy(file, sDest + "\\" + finfo.Name, true);
                    }
                    catch (Exception ex) 
                    {
                        Console.WriteLine(ex.Message);
                        return false; 
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// 프로세스 찾기
        /// </summary>
        /// <param name="sFilename"></param>
        /// <returns></returns>
        static Process FindProcess(string sFilename)
        {
            String sProcessName = sFilename.ToLower().Replace(".exe", "");
            Process[] processlist = Process.GetProcessesByName(sProcessName);
            foreach (Process proc in processlist)
            {
                try
                {
                    String fname = System.IO.Path.GetFileName(proc.MainModule.FileName).ToLower();
                    if (fname.Equals(sFilename.ToLower()))
                    {
                        return proc;
                    }
                }
                catch (Exception err)
                {
                    Console.WriteLine(err.Message);
                }
            }
            return null;
        }

        /// <summary>
        /// 프로세스 실행
        /// </summary>
        /// <param name="sFilename"></param>
        static void RunProcess(String sFilename)
        {
            try
            {
                string strAppDir = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetModules()[0].FullyQualifiedName);

                Process proc = new Process();
                proc.StartInfo.FileName = strAppDir + "\\" + sFilename;
                proc.StartInfo.WorkingDirectory = strAppDir;
                proc.Start();


            }
            catch (System.Exception e)
            {
                Console.WriteLine(e.Message);
            }

        }

        /// <summary>
        /// 프로세스 종료
        /// </summary>
        /// <param name="proc"></param>
        static void StopProcess(Process proc)
        {
            try
            {
                if (proc == null) return;

                String fname = proc.ProcessName;

                proc.Kill();
                proc.Dispose();
                Console.WriteLine("Founded '" + fname + "' process. It will be closed.");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

    }
}
