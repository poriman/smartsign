﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//using System.Windows.Forms.Integration;
using TVControl.Library;

using DirectX.Capture;
using DirectShowLib.BDA;
using DirectShowLib;

namespace TVControl
{
	public partial class TVControl : UserControl
	{
        bool bPsi = false;
		public TVControl()
		{
			InitializeComponent();
			this.Disposed += new EventHandler(TVControl_Disposed);

			_ControlCount++;
		}
        
		private static int _ControlCount = 0;
		private static int _PlayCount = 0;

		void TVControl_Disposed(object sender, EventArgs e)
		{
			try
			{
				if (!this.IsDisposed)
				{
					_ControlCount--;
				}

				if (GBTV != null)
				{
					Stop();

					lock (lockObj)
					{
						if (_ControlCount == 0) Clear();
					}
				}
			}
			catch
			{
				
			}

		}

		private DeviceInfo _deviceInfo = null;
		public DeviceInfo Device
		{
			get { return _deviceInfo; }
			set { _deviceInfo = value; }
		}

		private static object lockObj = new object();
		
		private static GraphBuilderTV GBTV = null;

		private Library.Channel channelinfo = null;

		private Timer _tmStatistics = null;

		public Library.Channel ChannelInfo
		{
			get { return channelinfo; }
			set { channelinfo = value; }
		}
        public bool PSIMODE 
        {
            set 
            {
                bPsi = value;
                GBTV.PSIMODE = bPsi; 
            }
        }
		private bool _isPlaying = false;

		public bool IsVMR9
		{
			get { return Device == null ? false : Device.IsVMR9; }
		}

		/// <summary>
		/// 통계정보가 업데이트 됨
		/// </summary>
		public event EventHandler StatisticsUpdated = null;

		#region TV Statistics attributes

		bool _locked = false;
		bool _present = false;
		int _strength = -1;
		int _quality = -1;

		public bool Looked { get { return _locked; } }
		public bool Present { get { return _present; } }
		public int Strength { get { return _strength; } }
		public int Quality { get { return _quality; } }

		#endregion

        public IPsiFilter GetPsiFilter() { return GBTV.GetPsiFilter; }
        public IAMTVTuner GetAMTVTuner() { return GBTV.GetAMTVTuner(); }

		public String BDADeviceName
		{
			get { return Device == null ? null : Device.BDADevice; }
		}

		public String WDMDeviceName
		{
			get { return Device == null ? null : Device.WDMVideoDevice; }
		}

		public String AudioDeviceName
		{
			get { return Device == null ? null : Device.WDMAudioDevice; }
		}

		public bool IsBDA
		{
			get {
				if (ChannelInfo == null)
					throw new NullReferenceException("Channel class is null.");

				if(ChannelInfo is ChannelAnalogic)
					return false;	
				return true;
			}
		}

		public System.Drawing.Bitmap Thumbnail(int cx, int cy)
		{

			return null;

		}

		private void TVControl_Load(object sender, EventArgs e)
		{

		}

		private void TVControl_Resize(object sender, EventArgs e)
		{

		}

		#region Updating Statistics
		private void SetTimer()
		{
			KillTimer();

			_tmStatistics = new Timer();
			_tmStatistics.Tick += new EventHandler(_tmStatistics_Tick);
			_tmStatistics.Interval = 3000;
			
			_tmStatistics.Start();
		}
		public void KillTimer()
		{
			if (_tmStatistics != null)
			{
				_tmStatistics.Stop();
				_tmStatistics.Dispose();
				_tmStatistics = null;
			}
		}

		void _tmStatistics_Tick(object sender, EventArgs e)
		{
			if(_isPlaying && GBTV != null)
			{
				try
				{
					lock (lockObj)
					{
                        if (GBTV.GetSignalStatistics(out _locked, out _present, out _strength, out _quality))
                        {
                            int strengthPercentage = _strength;
                            if (_strength < 0)
                            {
                                strengthPercentage = 100 + _strength;
                                if (strengthPercentage < 0)
                                    strengthPercentage = 0;
                            }
                            else if (_strength <= 100)
                                strengthPercentage = _strength;
                            else if (_strength < 40000)
                                strengthPercentage = (int)(5.0f * (float)_strength / 2000.0f);
                            else
                                strengthPercentage = 100;
                            _strength = strengthPercentage;
                        }
					}
					if (StatisticsUpdated != null) StatisticsUpdated(this, new EventArgs());

				}
				catch {}
			}
		}

		public bool HasSignal()
		{
            //try
            //{
            //    lock (lockObj)
            //    {
            //        GBTV.GetSignalStatistics(out _locked, out _present, out _strength, out _quality);
            //    }

            //    if (StatisticsUpdated != null) StatisticsUpdated(this, new EventArgs());

            //    return _locked && _present;
            //}
            //catch { }

            //return false;
            return true;
		}
		#endregion

		#region Stop and Play Functions
		private bool Initialize()
		{
			Clear();
			Stop();

			lock (lockObj)
			{
				if (IsBDA)
				{
					GBTV = (GraphBuilderTV)new BDAGraphBuilder(this);
					GBTV.VideoCaptureDevice = GraphBuilderTV.GetBDADsDeviceByName(BDADeviceName);
				}
				else
				{
					GBTV = (GraphBuilderTV)new WDMGraphBuilder(this);
					GBTV.VideoCaptureDevice = GraphBuilderTV.GetVideoDsDeviceByName(WDMDeviceName);
					GBTV.AudioCaptureDevice = GraphBuilderTV.GetAudioDsDeviceByName(AudioDeviceName);
				}

				GBTV.IsVMR9 = IsVMR9;
				GBTV.BuildGraph(ChannelInfo);
			}

			return true;
		}

		public void SubmitRequest()
		{
			lock (lockObj)
			{
				if (GBTV != null)
					GBTV.SubmitTuneRequest(ChannelInfo);
			}

			this.Invalidate();
		}
		public void Play()
		{
			if (Initialize())
			{
				lock (lockObj)
				{
					GBTV.RunGraph();
					SubmitRequest();

					_isPlaying = true;
					SetTimer();
					
					_PlayCount++;
				}
			}
		}

		public void Stop()
		{
			lock (lockObj)
			{
				if (_isPlaying) 
				{
					if(_PlayCount > 0) _PlayCount--;

					if (_PlayCount == 0 && GBTV != null)
					{
                        //GBTV.Dispose();
						GBTV.StopGraph();
						Clear();
					}
					KillTimer();

					_isPlaying = false;
				}
			}
		}

		public void SaveFilterGraph(String pathfile)
		{
			if(GBTV != null)
			{
				GBTV.SaveGraph(pathfile);
			}
		}

		public void Clear()
		{
			if (GBTV != null)
			{
                GBTV.Dispose();
				GBTV = null;
				this.Invalidate();
			}
		}

        public delegate bool PaintBackgroundEventHandler(object sender, System.Windows.Forms.PaintEventArgs pevent);
        public event PaintBackgroundEventHandler PaintBackground;

        protected override void OnPaintBackground(System.Windows.Forms.PaintEventArgs pevent)
        {            
            if (PaintBackground != null)
            {
                if (PaintBackground(this, pevent))
                    base.OnPaintBackground(pevent);
            }
            else
                base.OnPaintBackground(pevent);
            
        }
		#endregion
	}
}
