/****************************************************************************
While the underlying libraries are covered by LGPL, this sample is released 
as public domain.  It is distributed in the hope that it will be useful, but 
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
or FITNESS FOR A PARTICULAR PURPOSE.  
*****************************************************************************/

using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;

using Microsoft.Win32;

using DirectShowLib;
using DirectShowLib.Utils;

namespace TVControl.Library
{
	public class WDMGraphBuilder : GraphBuilderTV 
	{
		protected IBaseFilter videoCaptureFilter;
		protected IBaseFilter audioCaptureFilter;

		protected IAMTVTuner tuner;
		protected IAMTVAudio amTVAudio;
		protected IBaseFilter crossbar;
		protected CrossbarHelper crossbarHelper;

		public IAMTVTuner Tuner { get { return this.tuner; } }
		public IAMTVAudio TVAudio { get { return this.amTVAudio; } }
		public IBaseFilter Crossbar { get { return this.crossbar; } }
		public CrossbarHelper WDMCrossbar { get { return this.crossbarHelper; } }

		public IBaseFilter VideoCaptureFilter { get { return this.videoCaptureFilter; } }
		public IBaseFilter AudioCaptureFilter { get { return this.audioCaptureFilter; } }

        public WDMGraphBuilder(TVControl renderingControl)
		{
			this.hostingControl = renderingControl;
		}
        public override IAMTVTuner GetAMTVTuner() { return null; }

		public override void BuildGraph(Channel defaultChannel)
		{
			this.graphBuilder = (IFilterGraph2) new FilterGraph();
			rot = new DsROTEntry(this.graphBuilder);

			// Method names should be self explanatory

			AddRenderers();
			if (IsVMR9)
				ConfigureVMR9InWindowlessMode();

			AddAndConnectWDMBoardFilters();
			if (this.videoCaptureFilter == null)
				throw new ApplicationException("No video capture devices found!");

			GetControlInterface();

			if (!IsVMR9)
				ConfigureBaseVideoMode();

		}
		public override void GetMinMaxChannels(out int nMin, out int nMax)
		{
			if(tuner != null)
			{
				tuner.ChannelMinMax(out nMin, out nMax);
			}
			else { nMin = nMax = -1; }
			
		}

		private void GetControlInterface()
		{
			this.tuner = null;
			this.crossbar = null;

			object o;

			int hr = this.captureGraphBuilder.FindInterface(null, null, this.videoCaptureFilter, typeof(IAMTVTuner).GUID, out o);
			if (hr >= 0)
			{
				this.tuner = o as IAMTVTuner;
				o = null;

				hr = this.captureGraphBuilder.FindInterface(null, null, this.videoCaptureFilter, typeof(IAMCrossbar).GUID, out o);
				if (hr >= 0)
				{
					this.crossbar = o as IBaseFilter;
					o = null;
				}

				// Use the crossbar class to help us sort out all the possible video inputs
				// The class needs to be given the capture filters ANALOGVIDEO input pin
				IPin pinVideo = DsFindPin.ByCategory(this.videoCaptureFilter, PinCategory.AnalogVideoIn, 0);
				if (pinVideo != null)
				{
					try
					{
						this.crossbarHelper = new CrossbarHelper(pinVideo);
					}
					catch { }
					Marshal.ReleaseComObject(pinVideo);
				}

				hr = this.captureGraphBuilder.FindInterface(null, null, this.videoCaptureFilter, typeof(IAMTVAudio).GUID, out o);
				if (hr >= 0)
				{
					this.amTVAudio = o as IAMTVAudio;
					o = null;
				}
			}
		}

		public override void SubmitTuneRequest(Channel channel)
		{
			if (channel is ChannelAnalogic)
			{
				ChannelAnalogic channelAnalogic = channel as ChannelAnalogic;

				int hr = 0;

				//SetCaptureResolution(this.captureFormat); // channelAnalogic.CaptureResolution);

				if (this.videoCaptureFilter != null)
				{
					IAMAnalogVideoDecoder analogVideoDecoder = this.videoCaptureFilter as IAMAnalogVideoDecoder;
					if (analogVideoDecoder != null)
					{
						AnalogVideoStandard videoStandard;
						hr = analogVideoDecoder.get_TVFormat(out videoStandard);
						if (videoStandard != channelAnalogic.VideoStandard)
							hr = analogVideoDecoder.put_TVFormat(channelAnalogic.VideoStandard);
					}
				}

				if (this.tuner != null)
				{
					int tuningSpace = 0;
					hr = this.tuner.get_TuningSpace(out tuningSpace);
					if (hr < 0 || channelAnalogic.TuningSpace != tuningSpace)
						this.tuner.put_TuningSpace(channelAnalogic.TuningSpace);

					int countryCode = 0;
					hr = this.tuner.get_CountryCode(out countryCode);
					if (hr < 0 || channelAnalogic.CountryCode != countryCode)
						this.tuner.put_CountryCode(channelAnalogic.CountryCode);

					int connectInput = 0;
					hr = this.tuner.get_ConnectInput(out connectInput);
					if (hr < 0 || channelAnalogic.ConnectInput != connectInput)
						this.tuner.put_ConnectInput(channelAnalogic.ConnectInput);

					TunerInputType inputType = TunerInputType.Antenna;
					hr = this.tuner.get_InputType(channelAnalogic.ConnectInput, out inputType);
					if (hr < 0 || channelAnalogic.InputType != inputType)
						this.tuner.put_InputType(channelAnalogic.ConnectInput, channelAnalogic.InputType);

					AMTunerModeType mode = AMTunerModeType.Default;
					hr = this.tuner.get_Mode(out mode);
					if (hr < 0 || channelAnalogic.Mode != mode)
						this.tuner.put_Mode(channelAnalogic.Mode);

					if (channelAnalogic.ChannelNumber > 0)
					{
						hr = this.tuner.put_Channel(channelAnalogic.ChannelNumber, AMTunerSubChannel.Default, AMTunerSubChannel.Default);
						DsError.ThrowExceptionForHR(hr);
					}

					//int plChannel;
					//AMTunerSubChannel plVideoSubChannel;
					//AMTunerSubChannel plAudioSubChannel;
					//hr = this.tuner.get_Channel(out plChannel, out plVideoSubChannel, out plAudioSubChannel);

					//if (plChannel != channelAnalogic.Channel)
					//{
					//    DsError.ThrowExceptionForHR(-2147024809);
					//}
				}

				if (this.amTVAudio != null && channelAnalogic.AudioMode != TVAudioMode.None)
				{
					TVAudioMode audioMode = TVAudioMode.Stereo;
					hr = this.amTVAudio.get_TVAudioMode(out audioMode);
					if (hr < 0 || channelAnalogic.AudioMode != audioMode)
						this.amTVAudio.put_TVAudioMode(channelAnalogic.AudioMode);
				}


				if (WDMCrossbar != null)
				{
					try
					{
						if (channelAnalogic.AudioSource != WDMCrossbar.GetAudioInput())
							WDMCrossbar.SetAudioInput(channelAnalogic.AudioSource, false);
					}
					catch { }

					try
					{
						if (channelAnalogic.VideoSource != WDMCrossbar.GetVideoInput())
							WDMCrossbar.SetVideoInput(channelAnalogic.VideoSource, false);
					}
					catch { }
				}
				

				RunGraph();
			}

		}

		public override void RunGraph()
		{
			int hr = 0;

			hr = (this.graphBuilder as IMediaControl).Run();
			DsError.ThrowExceptionForHR(hr);
		}

		public override void StopGraph()
		{
			int hr = 0;

			hr = (this.graphBuilder as IMediaControl).Stop();
			DsError.ThrowExceptionForHR(hr);
		}

		public void SaveGraph(string filepath)
		{
			// Nothing to do with a DTV viewer but can be useful
			FilterGraphTools.SaveGraphFile(this.graphBuilder, filepath);
		}

		#region Membres de IDisposable

		public void Dispose()
		{
			Decompose();
		}

		#endregion

		private void AddAndConnectWDMBoardFilters()
		{
			int hr = 0;
			DsDevice[] devices;
			Guid iid = typeof(IBaseFilter).GUID;

			captureGraphBuilder = (ICaptureGraphBuilder2)new CaptureGraphBuilder2();
			captureGraphBuilder.SetFiltergraph(this.graphBuilder);

			try
			{
				if (videoCaptureDevice != null)
				{
					IBaseFilter tmp;

					object o;
					this.VideoCaptureDevice.Mon.BindToObject(null, null, ref iid, out o);
					tmp = o as IBaseFilter;
					//MessageBox.Show(this.VideoInputDevice.DevicePath);
					//Add the Video input device to the graph
					hr = graphBuilder.AddFilter(tmp, this.VideoCaptureDevice.Name);

					this.videoCaptureFilter = tmp;

					this.videoCaptureFilter = null;

					//Render any preview pin of the device
					//int hr1 = captureGraphBuilder.RenderStream(PinCategory.Preview, MediaType.Video, tmp, null, this.videoRenderer);
					int hr1 = captureGraphBuilder.RenderStream(PinCategory.Capture, MediaType.Video, tmp, null, this.videoRenderer);

					if (hr >= 0 && hr1 >= 0)
					{
						// Got it !
						this.videoCaptureFilter = tmp;
					}
					else
					{
						// Try another...
						int hr2 = graphBuilder.RemoveFilter(tmp);
						Marshal.ReleaseComObject(tmp);
						if (hr >= 0 && hr1 < 0)
							DsError.ThrowExceptionForHR(hr1);
						DsError.ThrowExceptionForHR(hr);
						return;
					}
				}
				else
				{
					// Enumerate WDM Source filters category and found one that can connect to the network provider
					devices = DsDevice.GetDevicesOfCat(FilterCategory.VideoInputDevice);
					for (int i = 0; i < devices.Length; i++)
					{
						IBaseFilter tmp;

						object o;
						devices[i].Mon.BindToObject(null, null, ref iid, out o);
						tmp = o as IBaseFilter;

						//Add the Video input device to the graph
						hr = graphBuilder.AddFilter(tmp, devices[i].Name);

						//Render any preview pin of the device
						//int hr1 = captureGraphBuilder.RenderStream(PinCategory.Preview, MediaType.Video, tmp, null, this.videoRenderer);
						int hr1 = captureGraphBuilder.RenderStream(PinCategory.Capture, MediaType.Video, tmp, null, this.videoRenderer);

						if (hr >= 0 && hr1 >= 0)
						{
							// Got it !
							this.videoCaptureFilter = tmp;
							break;
						}
						else
						{
							// Try another...
							hr = graphBuilder.RemoveFilter(tmp);
							Marshal.ReleaseComObject(tmp);
						}
					}
				}
				// Assume we found a video filter...


				if (this.AudioCaptureDevice != null)
				{
					if (this.VideoCaptureDevice != null && this.videoCaptureFilter != null && this.AudioCaptureDevice.DevicePath == this.VideoCaptureDevice.DevicePath)
					{
						//Render any preview pin of the device
						int hr1 = captureGraphBuilder.RenderStream(null, MediaType.Audio, this.videoCaptureFilter, null, this.audioRenderer);
						DsError.ThrowExceptionForHR(hr);

						if (hr1 >= 0)
						{
							// Got it !
							this.audioCaptureFilter = null; // this.videoCaptureFilter;
						}
						else
						{
							// No audio?
						}
					}
					else
					{
						IBaseFilter tmp;

						object o;
						this.AudioCaptureDevice.Mon.BindToObject(null, null, ref iid, out o);
						tmp = o as IBaseFilter;

						//Add the audio input device to the graph
						hr = graphBuilder.AddFilter(tmp, this.AudioCaptureDevice.Name);
						DsError.ThrowExceptionForHR(hr);

						//Render any preview pin of the device
						int hr1 = captureGraphBuilder.RenderStream(null, MediaType.Audio, tmp, null, this.audioRenderer);
						DsError.ThrowExceptionForHR(hr);

						if (hr >= 0 && hr1 >= 0)
						{
							// Got it !
							this.audioCaptureFilter = tmp;
						}
						else
						{
							// Try another...
							int hr2 = graphBuilder.RemoveFilter(tmp);
							Marshal.ReleaseComObject(tmp);
							DsError.ThrowExceptionForHR(hr);
							return;
						}
					}
				}
				else
				{
					// Then enumerate WDM AudioInputDevice category
					//devices = DsDevice.GetDevicesOfCat(FilterCategory.AudioInputDevice);
					devices = DsDevice.GetDevicesOfCat(FilterCategory.AMKSCapture);

					for (int i = 0; i < devices.Length; i++)
					{
						IBaseFilter tmp;

						object o;
						devices[i].Mon.BindToObject(null, null, ref iid, out o);
						tmp = o as IBaseFilter;

						//Add the audio input device to the graph
						hr = graphBuilder.AddFilter(tmp, devices[i].Name);
						DsError.ThrowExceptionForHR(hr);

						//Render any preview pin of the device
						int hr1 = captureGraphBuilder.RenderStream(null, MediaType.Audio, tmp, null, this.audioRenderer);

						if (hr >= 0 && hr1 >= 0)
						{
							// Got it !
							this.audioCaptureFilter = tmp;

							break;
						}
						else
						{
							// Try another...
							hr = graphBuilder.RemoveFilter(tmp);
							Marshal.ReleaseComObject(tmp);
						}
					}
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}
			finally
			{
			}
		}

		protected override void Decompose()
		{
			if (this.graphBuilder != null)
			{
				int hr = 0;

				// Decompose the graph
				try { hr = (this.graphBuilder as IMediaControl).StopWhenReady(); }
				catch { }
				try { hr = (this.graphBuilder as IMediaControl).Stop(); }
				catch { }
				RemoveHandlers();


				FilterGraphTools.RemoveAllFilters(this.graphBuilder);

				if (this.crossbarHelper != null) this.crossbarHelper.Dispose(); this.crossbarHelper = null;
				if (this.tuner != null) Marshal.ReleaseComObject(this.tuner); this.tuner = null;
				if (this.amTVAudio != null) Marshal.ReleaseComObject(this.amTVAudio); this.amTVAudio = null;
				if (this.crossbar != null) Marshal.ReleaseComObject(this.crossbar); this.crossbar = null;
				if (this.audioRenderer != null) Marshal.ReleaseComObject(this.audioRenderer); this.audioRenderer = null;
				if (this.videoRenderer != null) Marshal.ReleaseComObject(this.videoRenderer); this.videoRenderer = null;
				if (this.captureGraphBuilder != null) Marshal.ReleaseComObject(this.captureGraphBuilder); this.captureGraphBuilder = null;

				try { rot.Dispose(); }
				catch { }
				try { Marshal.ReleaseComObject(this.graphBuilder); this.graphBuilder = null; }
				catch { }
			}
		}

		private void AddRenderers()
		{
			int hr = 0;

			// To hear something
			this.audioRenderer = (IBaseFilter) new DSoundRender();

			hr = this.graphBuilder.AddFilter(this.audioRenderer, "DirectSound Renderer");
			DsError.ThrowExceptionForHR(hr);

			if (IsVMR9)
			{
				// To see something
				this.videoRenderer = (IBaseFilter)new VideoMixingRenderer9();

				hr = this.graphBuilder.AddFilter(this.videoRenderer, "Video Mixing Renderer Filter 9");
				DsError.ThrowExceptionForHR(hr);
			}
			else
			{
				// To see something
				this.videoRenderer = (IBaseFilter)new VideoRenderer();

				hr = this.graphBuilder.AddFilter(this.videoRenderer, "VideoRenderer");
				DsError.ThrowExceptionForHR(hr);

			}
		}

		private void ConfigureBaseVideoMode()
		{
			int hr = 0;
			//get the video window from the graph
			IVideoWindow videoWindow = null;
			videoWindow = (IVideoWindow)this.graphBuilder;

			//Set the owener of the videoWindow to an IntPtr of some sort (the Handle of any control - could be a form / button etc.)
			hr = videoWindow.put_Owner(this.hostingControl.Handle);
			DsError.ThrowExceptionForHR(hr);

			//Set the style of the video window
			hr = videoWindow.put_WindowStyle(WindowStyle.Child | WindowStyle.ClipChildren);
			DsError.ThrowExceptionForHR(hr);

			// Make the video window visible
			hr = videoWindow.put_Visible(OABool.True);
			DsError.ThrowExceptionForHR(hr);

			// Init the VMR-9 with default size values
			ResizeMoveHandler(null, null);

			// Add Windows Messages handlers
			AddHandlers();
		}

		private void ConfigureVMR9InWindowlessMode()
		{
			int hr = 0;
			IVMRFilterConfig filterConfig = this.videoRenderer as IVMRFilterConfig;

			// Configure VMR-9 in Windowsless mode
			hr = filterConfig.SetRenderingMode(VMRMode.Windowless);
			DsError.ThrowExceptionForHR(hr);

			// With 1 input stream
			hr = filterConfig.SetNumberOfStreams(1);
			DsError.ThrowExceptionForHR(hr);

			IVMRWindowlessControl windowlessControl = this.videoRenderer as IVMRWindowlessControl;

			// The main form is hosting the VMR-9
			hr = windowlessControl.SetVideoClippingWindow(this.hostingControl.Handle);
			DsError.ThrowExceptionForHR(hr);

			// Keep the aspect-ratio OK
			hr = windowlessControl.SetAspectRatioMode(VMRAspectRatioMode.LetterBox);
			DsError.ThrowExceptionForHR(hr);

			// Init the VMR-9 with default size values
			ResizeMoveHandler(null, null);

			// Add Windows Messages handlers
			AddHandlers();
		}

		private void AddHandlers()
		{
			// Add Windows Messages handlers
			if(IsVMR9) this.hostingControl.Paint += new PaintEventHandler(PaintHandler); // for WM_PAINT
			this.hostingControl.Resize += new EventHandler(ResizeMoveHandler); // for WM_SIZE
			this.hostingControl.Move += new EventHandler(ResizeMoveHandler); // for WM_MOVE
			SystemEvents.DisplaySettingsChanged += new EventHandler(DisplayChangedHandler); // for WM_DISPLAYCHANGE
		}

		private void RemoveHandlers()
		{
			// Remove Windows Messages handlers
			
			this.hostingControl.Paint -= new PaintEventHandler(PaintHandler); // for WM_PAINT
			this.hostingControl.Resize -= new EventHandler(ResizeMoveHandler); // for WM_SIZE
			this.hostingControl.Move -= new EventHandler(ResizeMoveHandler); // for WM_MOVE
			SystemEvents.DisplaySettingsChanged -= new EventHandler(DisplayChangedHandler); // for WM_DISPLAYCHANGE
		}

		private void PaintHandler(object sender, PaintEventArgs e)
		{
			if (this.videoRenderer != null)
			{
				IntPtr hdc = e.Graphics.GetHdc();
				int hr = (this.videoRenderer as IVMRWindowlessControl).RepaintVideo(this.hostingControl.Handle, hdc);
				e.Graphics.ReleaseHdc(hdc);
			}
		}

		private void ResizeMoveHandler(object sender, EventArgs e)
		{
			int hr = 0;
			if (this.videoRenderer != null)
			{
				if(IsVMR9)
				{
					hr = (this.videoRenderer as IVMRWindowlessControl).SetVideoPosition(null, DsRect.FromRectangle(this.hostingControl.ClientRectangle));
					DsError.ThrowExceptionForHR(hr);
				}
				else
				{
					IVideoWindow videoWindow = null;
					videoWindow = (IVideoWindow)this.graphBuilder;

					hr = videoWindow.SetWindowPosition(0, 0, hostingControl.Width, hostingControl.Height);
					DsError.ThrowExceptionForHR(hr);
				}
			}
		}

		private void DisplayChangedHandler(object sender, EventArgs e)
		{
			if (this.videoRenderer != null && IsVMR9)
			{
				int hr = (this.videoRenderer as IVMRWindowlessControl).DisplayModeChanged();
			}
		}

		public override bool GetSignalStatistics(out bool locked, out bool present, out int strength, out int quality)
		{
			strength = quality = 0;
			locked = present = false;

			if (this.Tuner != null)
			{
				AMTunerSignalStrength signalStrength = AMTunerSignalStrength.NoSignal;
				this.Tuner.SignalPresent(out signalStrength);
// 				Tuner.
				switch (signalStrength)
				{
					case AMTunerSignalStrength.HasNoSignalStrength:
						break;
					case AMTunerSignalStrength.NoSignal:
						break;
					case AMTunerSignalStrength.SignalPresent:
						locked = present = true;
						strength = quality = 100;
						return true;
				}
			}
			else
			{
				locked = present = true;
				strength = quality = 100;
				return true;
			}
			return false;
		}
	}
}
