/****************************************************************************
While the underlying libraries are covered by LGPL, this sample is released 
as public domain.  It is distributed in the hope that it will be useful, but 
WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
or FITNESS FOR A PARTICULAR PURPOSE.  
*****************************************************************************/

using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;

using Microsoft.Win32;

using DirectShowLib;
using DirectShowLib.BDA;
using DirectShowLib.Utils;
using System.Drawing;
using System.Collections;

namespace TVControl.Library
{
	public class BDAGraphBuilder : GraphBuilderTV
	{
		IBaseFilter networkProvider = null;
		IBaseFilter mpeg2Demux = null;
		IBaseFilter tuner = null;
		IBaseFilter capture = null;
		IBaseFilter bdaTIF = null;
		IBaseFilter bdaSecTab = null;
        IBaseFilter antunerFilter = null;
        IBaseFilter decoderFilter = null;
        IBaseFilter analogTuner = null;
        protected DsDevice tunerDevice;
        protected DsDevice captureDevice;
        protected IBaseFilter skyPsiFilter = null;        

        public DsDevice TunerDevice { get { return this.tunerDevice; } set { this.tunerDevice = value; } }
        public DsDevice CaptureDevice { get { return this.captureDevice; } set { this.captureDevice = value; } }

        public IBaseFilter NetworkProvider { get { return this.networkProvider; } }
        public IBaseFilter TunerFilter { get { return this.tuner; } }
        public IBaseFilter CaptureFilter { get { return this.capture; } }
        public IBaseFilter Demultiplexer { get { return this.mpeg2Demux; } }

		DsDevice bda_device = null;

		ITuningSpace tuningSpace = null;
		IChannelTuneRequest tuneRequest = null;

		#region Properties

		public int GetSignalStrength
		{
			get {
				if (this.networkProvider != null)
				{
					ITuner tune = this.networkProvider as ITuner;
					int nStrlenth = 0;
					int hr = tune.get_SignalStrength(out nStrlenth);
					if (hr == 0 && nStrlenth >= 0)
					{
						return nStrlenth;
					}
				}
				return -1;
			}
		}

		public int Frequency
		{
			get
			{
				if (this.networkProvider != null)
				{
					ITuner tune = this.networkProvider as ITuner;
					ITuneRequest request;
					int hr = tune.get_TuneRequest(out request);

					if (hr == 0 && null != (request as IATSCChannelTuneRequest))
					{
						int ch;
						ILocator locator;
						hr = ((IATSCChannelTuneRequest)request).get_Locator(out locator);
						if (hr == 0 && null != (locator as IATSCLocator))
						{
							hr = ((IATSCLocator)locator).get_CarrierFrequency(out ch);
							return ch;
						}
					}
				}
				return -1;
			}
		}
		public int Channel
		{
			get
			{
				if (this.networkProvider != null)
				{
					ITuner tune = this.networkProvider as ITuner;
					ITuneRequest request;
					int hr = tune.get_TuneRequest(out request);

					if (hr == 0 && null != (request as IATSCChannelTuneRequest))
					{
						int ch;
						hr = ((IATSCChannelTuneRequest)request).get_Channel(out ch);
// 						((IATSCChannelTuneRequest)request).get_MinorChannel(out ch);

						return ch;
					}
				}
				return -1;
			}
		}

		public int PhysicalChannel
		{
			get
			{
				if (this.networkProvider != null)
				{
					ITuner tune = this.networkProvider as ITuner;
					ITuneRequest request;
					int hr = tune.get_TuneRequest(out request);

					if (hr == 0 && null != (request as IATSCChannelTuneRequest))
					{
						int ch;
						ILocator locator;
						hr = ((IATSCChannelTuneRequest)request).get_Locator(out locator);
						if(hr == 0 && null != (locator as IATSCLocator))
						{
							hr = ((IATSCLocator)locator).get_SymbolRate(out ch);
							hr = ((IATSCLocator)locator).get_CarrierFrequency(out ch);
							hr = ((IATSCLocator)locator).get_TSID(out ch);
							hr = ((IATSCLocator)locator).get_PhysicalChannel(out ch);
							return ch;
						}
					}
				}
				return -1;
			}
		}
		public int MinorChannel
		{
			get
			{
				if (this.networkProvider != null)
				{
					ITuner tune = this.networkProvider as ITuner;
					ITuneRequest request;
					int hr = tune.get_TuneRequest(out request);

					if (hr == 0 && null != (request as IATSCChannelTuneRequest))
					{
						int ch;
						hr = ((IATSCChannelTuneRequest)request).get_MinorChannel(out ch);

						return ch;
					}
				}
				return -1;
			}
		}
		#endregion

        public BDAGraphBuilder(TVControl renderingControl)
		{
			this.hostingControl = renderingControl;
		}
        
        public override IAMTVTuner GetAMTVTuner() 
        {
            return this.antuner;;
        }
        
		public override void BuildGraph(Channel defaultChannel)
		{
            try
            {
                int hr;

                this.graphBuilder = (IFilterGraph2)new FilterGraph();
                rot = new DsROTEntry(this.graphBuilder);
                //전대 병원 TV 카드
                //DsDevice anDevice = GetVideoDsDeviceByName("SKYTV HD Red Tuner", 0);

                DsDevice anDevice = GetVideoDsDeviceByName("SKYTV HD USB Maxx Analog Tuner", 0);
                IBaseFilter tmp;
                hr = graphBuilder.AddSourceFilterForMoniker(anDevice.Mon, null, anDevice.Name, out tmp);
                DsError.ThrowExceptionForHR(hr);
                // 도로공사 USB형 TV 수신 모듈;

               // if (PSIMODE)
                {
                    skyPsiFilter = FilterGraphTools.AddFilterFromClsid(this.graphBuilder, new Guid("{989DE245-538F-4211-84A2-A37BE207D4C4}"), "Sky PSI Filter");
                    PsiFilter = (IPsiFilter)skyPsiFilter;
                }

                // Method names should be self explanatory
                AddNetworkProviderFilter();
                AddMPEG2DemuxFilter();
                CreateMPEG2DemuxPins();
                AddAndConnectBDABoardFilters();
                AddRenderers();                
                ConfigureVMR9InWindowlessMode();                
                ConnectAudioAndVideoFilters();
                IMediaFilter mf = this.graphBuilder as IMediaFilter;
				hr = mf.SetSyncSource(this.mpeg2Demux as IReferenceClock);
                DsError.ThrowExceptionForHR(hr);

            }
        catch (Exception ex)
            {
                Decompose();
                throw ex;
            }

		}
		public override void GetMinMaxChannels(out int nMin, out int nMax)
		{
			nMin = 2; nMax = 80;
		}

        protected void ConnectAudioAndVideoFilters()
        {
            int hr = 0;
            IPin pinOut;

            hr = this.mpeg2Demux.FindPin("MPG2", out pinOut);
            if (pinOut != null)
            {
                try
                {                    
                    IPin pinInFromFilterOut = DsFindPin.ByDirection(this.videoRenderer, PinDirection.Input, 0);
                    if (pinInFromFilterOut != null)
                    {
                        try
                        {
                            hr = this.graphBuilder.Connect(pinOut, pinInFromFilterOut);
                        }
                        finally
                        {
                            Marshal.ReleaseComObject(pinInFromFilterOut);
                        }
                    }                   
                }
                finally
                {
                    Marshal.ReleaseComObject(pinOut);
                }
            }
            
            hr = this.mpeg2Demux.FindPin("Audio", out pinOut);
            if (pinOut != null)
            {
                hr = this.graphBuilder.Render(pinOut);
                //DsError.ThrowExceptionForHR(hr);
                Marshal.ReleaseComObject(pinOut);
            }

            hr = this.mpeg2Demux.FindPin("PSI", out pinOut);
            if (pinOut != null)
            {
                hr = this.graphBuilder.Render(pinOut);
                //DsError.ThrowExceptionForHR(hr);
                Marshal.ReleaseComObject(pinOut);
            }
        }

        private int currentMpeg2VideoPid = -1;
        private int currentAudioPid = -1;

		public override void SubmitTuneRequest(Channel channel)
		{
            if (channel is ChannelATSC)
            {
                ChannelATSC channelDVB = channel as ChannelATSC;
                AnChannelChange(channel);
                int hr = 0;               
                IMpeg2Demultiplexer mpeg2Demultiplexer = this.mpeg2Demux as IMpeg2Demultiplexer;
               
                if (channelDVB.VideoPidNum != -1 && (channelDVB.PSIMODE == false))
                {
                    {
                        IPin pinDemuxerVideoMPEG2;
                        hr = this.mpeg2Demux.FindPin("MPG2", out pinDemuxerVideoMPEG2);
                        if (pinDemuxerVideoMPEG2 != null)
                        {
                            IMPEG2PIDMap mpeg2PIDMap = pinDemuxerVideoMPEG2 as IMPEG2PIDMap;
                            if (mpeg2PIDMap != null)
                            {
                                if (this.currentMpeg2VideoPid >= 0)
                                    hr = mpeg2PIDMap.UnmapPID(1, new int[] { this.currentMpeg2VideoPid });

                                hr = mpeg2PIDMap.MapPID(1, new int[] { channelDVB.VideoPidNum }, MediaSampleContent.ElementaryStream);
                                this.currentMpeg2VideoPid = channelDVB.VideoPidNum;
                            }
                            Marshal.ReleaseComObject(pinDemuxerVideoMPEG2);
                        }
                    }
                }

                if (channelDVB.AudioPidNum != -1 && (channelDVB.PSIMODE == false))
                {
                    IPin pinDemuxerAudio;
                    hr = this.mpeg2Demux.FindPin("Audio", out pinDemuxerAudio);
                    if (pinDemuxerAudio != null)
                    {
                        IMPEG2PIDMap mpeg2PIDMap = pinDemuxerAudio as IMPEG2PIDMap;
                        if (mpeg2PIDMap != null)
                        {
                            if (this.currentAudioPid >= 0)
                                hr = mpeg2PIDMap.UnmapPID(1, new int[] { this.currentAudioPid });

                            hr = mpeg2PIDMap.MapPID(1, new int[] { channelDVB.AudioPidNum }, MediaSampleContent.ElementaryStream);
                            this.currentAudioPid = channelDVB.AudioPidNum;
                        }
                        Marshal.ReleaseComObject(pinDemuxerAudio);
                    }
                }

                if (channelDVB.PSIMODE)
                {
                    IPin pinDemuxerSectionsAndTables;
                    hr = this.mpeg2Demux.FindPin("PSI", out pinDemuxerSectionsAndTables);
                    if (pinDemuxerSectionsAndTables != null)
                    {
                        IMPEG2PIDMap mpeg2PIDMap = pinDemuxerSectionsAndTables as IMPEG2PIDMap;
                        if (mpeg2PIDMap != null)
                        {
                            int[] p = new int[2];
                            p[0] = 0x000;
                            p[1] = 0x011;
                            hr = mpeg2PIDMap.MapPID(2, p, MediaSampleContent.Mpeg2PSI);
                        }

                        Marshal.ReleaseComObject(pinDemuxerSectionsAndTables);
                    }

                    AddConnectPSIFilter();
                    
                }
            }

		}

		public override void RunGraph()
		{
            IMediaControl mediaControl = this.graphBuilder as IMediaControl;
            FilterState pfs;
            mediaControl.GetState(0, out pfs);
            if (pfs != FilterState.Running)
            {
                int hr = mediaControl.Run();
                DsError.ThrowExceptionForHR(hr);
            }
		}

		public override void StopGraph()
		{
            IMediaControl mediaControl = this.graphBuilder as IMediaControl;
            FilterState pfs;
            mediaControl.GetState(0, out pfs);
            if (pfs == FilterState.Running || pfs == FilterState.Paused)
            {
                mediaControl.Pause();
                mediaControl.StopWhenReady();
                int hr = mediaControl.Stop();
                DsError.ThrowExceptionForHR(hr);
            }
		}

        private bool AnChannelChange(Channel channel)
        {
            int hr = 0;

            object o;
           // IBaseFilter filter = FilterGraphTools.FindFilterByName(graphBuilder, "SKYTV HD Red Tuner");

           IBaseFilter filter = FilterGraphTools.FindFilterByName(graphBuilder, "SKYTV HD USB Maxx Analog Tuner");

           hr = this.captureGraphBuilder.FindInterface(null, null, filter, typeof(IAMTVTuner).GUID, out o);
            if (hr >= 0)
            {
                antuner = o as IAMTVTuner;
                o = null;
            }

            if (antuner != null)
            {
                ChannelATSC channelDVB = channel as ChannelATSC;

                TunerInputType inputType = TunerInputType.Antenna;
                hr = antuner.get_InputType(0, out inputType);
                hr = antuner.put_InputType(0, channelDVB.InputType);

                AMTunerModeType mode = AMTunerModeType.Default;
                hr = antuner.get_Mode(out mode);
                if (mode != AMTunerModeType.DTV)
                    antuner.put_Mode(AMTunerModeType.DTV);

                {
                    int chanel = channelDVB.ChannelNumber;
                    
                    hr = antuner.put_Channel(chanel, AMTunerSubChannel.Default, AMTunerSubChannel.Default);
                   // DsError.ThrowExceptionForHR(hr);
                }
            }

            return false;
        }

        public static DsDevice[] GetVideoDsDevices(int type)
        {
            DsDevice[] devices = null;
            try
            {
                if (type == 0)
                {
                    devices = DsDevice.GetDevicesOfCat(FilterCategory.AMKSTVTuner);
                }
                else if (type == 1)
                {
                    devices = DsDevice.GetDevicesOfCat(FilterCategory.BDASourceFiltersCategory);
                }
                else if (type == 2)
                {
                    devices = DsDevice.GetDevicesOfCat(FilterCategory.BDAReceiverComponentsCategory);
                }

                return devices;
            }
            catch { return null; }
        }

        public static DsDevice GetVideoDsDeviceByName(String deviceName, int type)
        {
            try
            {
                DsDevice[] devices = GetVideoDsDevices(type);

                if (devices != null)
                {
                    foreach (DsDevice device in devices)
                    {
                        if (device.Name != null && device.Name.Equals(deviceName)) return device;
                    }
                }
            }
            catch
            {
            }
            return null;
        }

		private void AddNetworkProviderFilter()
		{
            this.networkProvider = FilterGraphTools.AddFilterFromClsid(this.graphBuilder, new Guid("{2084F34B-7DF8-4135-8311-19191019DC09}"), "Sky Net Filter");
		}

        protected void AddMPEG2DemuxFilter()
        {
            this.mpeg2Demux = (IBaseFilter)new MPEG2Demultiplexer();
            int hr = this.graphBuilder.AddFilter(this.mpeg2Demux, "MPEG2 Demultiplexer");
            DsError.ThrowExceptionForHR(hr);

        }

        protected virtual void CreateMPEG2DemuxPins()
        {
            IMpeg2Demultiplexer mpeg2Demultiplexer = this.mpeg2Demux as IMpeg2Demultiplexer;

            {
                AMMediaType mediaMPG2 = new AMMediaType();
                mediaMPG2.majorType = MediaType.Video;
                mediaMPG2.subType = MediaSubType.Mpeg2Video;
                mediaMPG2.fixedSizeSamples = true;
                mediaMPG2.temporalCompression = false; // true???
                mediaMPG2.sampleSize = 0;
                mediaMPG2.formatType = FormatType.Mpeg2Video;
                mediaMPG2.unkPtr = IntPtr.Zero;
                mediaMPG2.formatSize = 132;

                MPEG2VideoInfo videoMPEG2PinFormat = GetVideoMPEG2PinFormat();
                mediaMPG2.formatSize = Marshal.SizeOf(videoMPEG2PinFormat);
                mediaMPG2.formatPtr = Marshal.AllocHGlobal(mediaMPG2.formatSize);
                Marshal.StructureToPtr(videoMPEG2PinFormat, mediaMPG2.formatPtr, false);

                IPin pinDemuxerVideoMPEG2;
                int hr = mpeg2Demultiplexer.CreateOutputPin(mediaMPG2, "MPG2", out pinDemuxerVideoMPEG2);
                if (pinDemuxerVideoMPEG2 != null)
                    Marshal.ReleaseComObject(pinDemuxerVideoMPEG2);

                Marshal.FreeHGlobal(mediaMPG2.formatPtr);
            }

            {
                AMMediaType mediaAudio = new AMMediaType();
                mediaAudio.majorType = MediaType.Audio;
                mediaAudio.subType = MediaSubType.DolbyAC3;
                mediaAudio.sampleSize = 0;
                mediaAudio.temporalCompression = false;
                mediaAudio.fixedSizeSamples = true;
                mediaAudio.unkPtr = IntPtr.Zero;
                mediaAudio.formatType = FormatType.WaveEx;

                MPEG1WaveFormat audioPinFormat = GetAudioPinFormat();
                mediaAudio.formatSize = Marshal.SizeOf(audioPinFormat);
                mediaAudio.formatPtr = Marshal.AllocHGlobal(mediaAudio.formatSize);
                Marshal.StructureToPtr(audioPinFormat, mediaAudio.formatPtr, false);

                IPin pinDemuxerAudio;
                int hr = mpeg2Demultiplexer.CreateOutputPin(mediaAudio, "Audio", out pinDemuxerAudio);
                if (pinDemuxerAudio != null)
                    Marshal.ReleaseComObject(pinDemuxerAudio);

                Marshal.FreeHGlobal(mediaAudio.formatPtr);
            }

            {
                //Pin 5 connected to "MPEG-2 Sections and Tables" (Allows to grab custom PSI tables)
                //    Major Type	MEDIATYPE_MPEG2_SECTIONS {455F176C-4B06-47CE-9AEF-8CAEF73DF7B5}
                //    Sub Type		MEDIASUBTYPE_MPEG2DATA {C892E55B-252D-42B5-A316-D997E7A5D995}
                //    Format		None

                AMMediaType mediaSectionsAndTables = new AMMediaType();
                mediaSectionsAndTables.majorType = MediaType.Mpeg2Sections;
                mediaSectionsAndTables.subType = MediaSubType.Mpeg2Data;
                mediaSectionsAndTables.sampleSize = 0; // 1;
                mediaSectionsAndTables.temporalCompression = false;
                mediaSectionsAndTables.fixedSizeSamples = true;
                mediaSectionsAndTables.unkPtr = IntPtr.Zero;
                mediaSectionsAndTables.formatType = FormatType.None;
                mediaSectionsAndTables.formatSize = 0;
                mediaSectionsAndTables.formatPtr = IntPtr.Zero;

                IPin pinDemuxerSectionsAndTables;
                int hr = mpeg2Demultiplexer.CreateOutputPin(mediaSectionsAndTables, "PSI", out pinDemuxerSectionsAndTables);
                if (pinDemuxerSectionsAndTables != null)
                    Marshal.ReleaseComObject(pinDemuxerSectionsAndTables);
            }           
        }

        protected void AddConnectPSIFilter()
        {
            int hr = 0;
            IPin pinOut;
            
            hr = this.mpeg2Demux.FindPin("PSI", out pinOut);
            if (pinOut != null)
            {
                IPin pinIn = DsFindPin.ByDirection(skyPsiFilter, PinDirection.Input, 0);
                if (pinIn != null)
                {                    
                    hr = this.graphBuilder.Connect(pinOut, pinIn);
                    if (hr != 0)
                    {
                        graphBuilder.Render(pinOut);
                    }
                    Marshal.ReleaseComObject(pinIn);
                }
                Marshal.ReleaseComObject(pinOut);
            }

            DsDevice[] devices = DsDevice.GetDevicesOfCat(FilterCategory.BDATransportInformationRenderersCategory);
            for (int i = 0; i < devices.Length; i++)
            {
                //if (devices[i].Name.Equals("SKY HDTV PSI Parser"))
                if (devices[i].Name.Equals("Sky PSI Filter"))                
                {
                    hr = graphBuilder.AddSourceFilterForMoniker(devices[i].Mon, null, devices[i].Name, out this.skyPsiFilter);
                    DsError.ThrowExceptionForHR(hr);

                    hr = this.mpeg2Demux.FindPin("PSI", out pinOut);
                    if (pinOut != null)
                    {
                        IPin pinIn = DsFindPin.ByDirection(this.skyPsiFilter, PinDirection.Input, 0);
                        if (pinIn != null)
                        {
                            hr = this.graphBuilder.Connect(pinOut, pinIn);
                            if (hr != 0)
                            {
                                this.graphBuilder.Render(pinOut);
                            }
                            Marshal.ReleaseComObject(pinIn);
                        }

                        Marshal.ReleaseComObject(pinOut);
                    }

                    continue;
                }
            }
        }

		private void AddAndConnectBDABoardFilters()
		{
            int hr = 0;
            DsDevice[] devices;

            this.captureGraphBuilder = (ICaptureGraphBuilder2)new CaptureGraphBuilder2();
            captureGraphBuilder.SetFiltergraph(this.graphBuilder);

            try
            {
                if (!PSIMODE)
                {
                  //  decoderFilter = FilterGraphTools.AddFilterFromClsid(this.graphBuilder, new Guid("{35EACE15-58A2-443C-94A5-963F0C4D9933}"), "SKY MPEG2 Video Decoder");
                }

                //GetVideoDsDeviceByName("SKYTV HD Red Tuner/", 0);
                TunerDevice = GetVideoDsDeviceByName("SKYTV HD USB Maxx Digital Tuner", 1);
                if (this.TunerDevice != null)
                {
                    IBaseFilter tmp;

                    hr = graphBuilder.AddSourceFilterForMoniker(this.TunerDevice.Mon, null, this.TunerDevice.Name, out tmp);
                    DsError.ThrowExceptionForHR(hr);

                    hr = captureGraphBuilder.RenderStream(null, null, this.networkProvider, null, tmp);
                    if (hr == 0)
                    {
                        // Got it !
                        this.tuner = tmp;
                    }
                    else
                    {
                        // Try another...
                        int hr2 = graphBuilder.RemoveFilter(tmp);
                        Marshal.ReleaseComObject(tmp);
                        DsError.ThrowExceptionForHR(hr);
                        return;
                    }
                }
                else
                {
                    // Enumerate BDA Source filters category and found one that can connect to the network provider
                    devices = DsDevice.GetDevicesOfCat(FilterCategory.BDASourceFiltersCategory);
                    for (int i = 0; i < devices.Length; i++)
                    {
                        IBaseFilter tmp;

                        hr = graphBuilder.AddSourceFilterForMoniker(devices[i].Mon, null, devices[i].Name, out tmp);
                        DsError.ThrowExceptionForHR(hr);

                        hr = captureGraphBuilder.RenderStream(null, null, this.networkProvider, null, tmp);
                        if (hr == 0)
                        {
                            // Got it !
                            this.tuner = tmp;
                            break;
                        }
                        else
                        {
                            // Try another...
                            hr = graphBuilder.RemoveFilter(tmp);
                            Marshal.ReleaseComObject(tmp);
                        }
                    }
                }
                // Assume we found a tuner filter...

                //GetVideoDsDeviceByName("SKYTV HD Stream Capture", 2);
                CaptureDevice = GetVideoDsDeviceByName("SKYTV HD USB Maxx Stream Capture", 2);
                if (this.CaptureDevice != null)
                {
                    IBaseFilter tmp;

                    hr = graphBuilder.AddSourceFilterForMoniker(this.CaptureDevice.Mon, null, this.CaptureDevice.Name, out tmp);
                    DsError.ThrowExceptionForHR(hr);

                    hr = captureGraphBuilder.RenderStream(null, null, this.tuner, null, tmp);
                    if (hr == 0)
                    {
                        // Got it !
                        this.capture = tmp;

                        // Connect it to the MPEG-2 Demux
                        hr = captureGraphBuilder.RenderStream(null, null, this.capture, null, this.mpeg2Demux);
                        if (hr < 0)
                            // BDA also support 3 filter scheme (never show it in real life).
                            throw new ApplicationException("This application only support the 2 filters BDA scheme");
                    }
                    else
                    {
                        // Try another...
                        int hr2 = graphBuilder.RemoveFilter(tmp);
                        Marshal.ReleaseComObject(tmp);
                        DsError.ThrowExceptionForHR(hr);
                        return;
                    }
                }
                else
                {
                    this.capture = null;
                    capture = FilterGraphTools.AddFilterFromClsid(this.graphBuilder, new Guid("{17CCA71B-ECD7-11D0-B908-00A0C9223196}"), "Generic WDM Filter Proxy");
                    // Connect it to the MPEG-2 Demux
                    hr = captureGraphBuilder.RenderStream(null, null, this.capture, null, this.mpeg2Demux);
                    if (hr < 0)
                        // BDA also support 3 filter scheme (never show it in real life).
                        throw new ApplicationException("This application only support the 2 filters BDA scheme");
                }
            }
            finally
            {
               
            }
		}

		public void AddTransportStreamFiltersToGraph()
		{
			int hr = 0;
			DsDevice[] devices;

			// Add two filters needed in a BDA graph
			devices = DsDevice.GetDevicesOfCat(FilterCategory.BDATransportInformationRenderersCategory);
			for(int i = 0; i < devices.Length; i++)
			{
				if (devices[i].Name.Equals("BDA MPEG2 Transport Information Filter"))
				{
					hr = graphBuilder.AddSourceFilterForMoniker(devices[i].Mon, null, devices[i].Name, out this.bdaTIF);
					DsError.ThrowExceptionForHR(hr);
					continue;
				}

				if (devices[i].Name.Equals("MPEG-2 Sections and Tables"))
				{
					hr = graphBuilder.AddSourceFilterForMoniker(devices[i].Mon, null, devices[i].Name, out this.bdaSecTab);
					DsError.ThrowExceptionForHR(hr);
					continue;
				}
			}
		}

		private void AddRenderers()
		{
			int hr = 0;

			// To hear something
			this.audioRenderer = (IBaseFilter) new DSoundRender();

			hr = this.graphBuilder.AddFilter(this.audioRenderer, "DirectSound Renderer");
			DsError.ThrowExceptionForHR(hr);

            this.videoRenderer = FilterGraphTools.AddFilterFromClsid(this.graphBuilder, new Guid("{51B4ABF3-748F-4E3B-A276-C828330E926A}"), "Video Mixing Renderer 9");
            
		}

		private void ConfigureBaseVideoMode()
		{
			int hr = 0;
			//get the video window from the graph
			IVideoWindow videoWindow = null;
			videoWindow = (IVideoWindow)this.graphBuilder;

			//Set the owener of the videoWindow to an IntPtr of some sort (the Handle of any control - could be a form / button etc.)
			hr = videoWindow.put_Owner(this.hostingControl.Handle);
			DsError.ThrowExceptionForHR(hr);

			//Set the style of the video window
			hr = videoWindow.put_WindowStyle(WindowStyle.Child | WindowStyle.ClipChildren);
			DsError.ThrowExceptionForHR(hr);

			// Make the video window visible
			hr = videoWindow.put_Visible(OABool.True);
			DsError.ThrowExceptionForHR(hr);

			// Init the VMR-9 with default size values
			ResizeMoveHandler(null, null);

			// Add Windows Messages handlers
			AddHandlers();
		}

		private void ConfigureVMR9InWindowlessMode()
		{
            //int hr = 0;           
            
            //IVMRFilterConfig filterConfig = this.videoRenderer as IVMRFilterConfig;

            //hr = filterConfig.SetRenderingMode(VMRMode.Windowless);
            //DsError.ThrowExceptionForHR(hr);

            //int numberOfStream = 1;
            //hr = filterConfig.SetNumberOfStreams(numberOfStream);
            //DsError.ThrowExceptionForHR(hr);

            //IVMRWindowlessControl windowlessControl = this.videoRenderer as IVMRWindowlessControl;

            //hr = windowlessControl.SetVideoClippingWindow(this.hostingControl.Handle);
            //DsError.ThrowExceptionForHR(hr);

            //hr = windowlessControl.SetAspectRatioMode(VMRAspectRatioMode.None);
            //DsError.ThrowExceptionForHR(hr);

            //ResizeMoveHandler(null, null);

            //AddHandlers();
            int hr = 0;
            IVMRFilterConfig9 filterConfig = this.videoRenderer as IVMRFilterConfig9;

            // Configure VMR-9 in Windowsless mode
            hr = filterConfig.SetRenderingMode(VMR9Mode.Windowless);

            // With 1 input stream
            int numberOfStream = 0;
            hr = filterConfig.SetNumberOfStreams(numberOfStream);

            IVMRWindowlessControl9 windowlessControl = this.videoRenderer as IVMRWindowlessControl9;

            // The main form is hosting the VMR-9
            hr = windowlessControl.SetVideoClippingWindow(this.hostingControl.Handle);

            // Keep the aspect-ratio OK
            //hr = windowlessControl.SetAspectRatioMode(VMR9AspectRatioMode.LetterBox);
            hr = windowlessControl.SetAspectRatioMode(VMR9AspectRatioMode.None);

            // Init the VMR-9 with default size values
            ResizeMoveHandler(null, null);

            // Add Windows Messages handlers
            AddHandlers();
            
		}

		private void AddHandlers()
		{
			// Add Windows Messages handlers
            this.hostingControl.PaintBackground += new TVControl.PaintBackgroundEventHandler(OnPaintBackground);
            this.hostingControl.Paint += new PaintEventHandler(OnPaintHandler); // for WM_PAINT
			this.hostingControl.Resize += new EventHandler(ResizeMoveHandler); // for WM_SIZE
			this.hostingControl.Move += new EventHandler(ResizeMoveHandler); // for WM_MOVE
			SystemEvents.DisplaySettingsChanged += new EventHandler(DisplayChangedHandler); // for WM_DISPLAYCHANGE
		}

		private void RemoveHandlers()
		{
            this.hostingControl.PaintBackground -= new TVControl.PaintBackgroundEventHandler(OnPaintBackground);
            this.hostingControl.Paint -= new PaintEventHandler(OnPaintHandler); 
			this.hostingControl.Resize -= new EventHandler(ResizeMoveHandler); // for WM_SIZE
			this.hostingControl.Move -= new EventHandler(ResizeMoveHandler); // for WM_MOVE
			SystemEvents.DisplaySettingsChanged -= new EventHandler(DisplayChangedHandler); // for WM_DISPLAYCHANGE
		}

        protected virtual bool OnPaintBackground(object sender, PaintEventArgs e)
        {
            // The OnPaintBackground is call a lot often than OnPaintHandler!!!!
            if (this.videoRenderer != null)
            {
                //Trace.WriteLineIf(trace.TraceInfo, "OnPaintBackground()");
                return false;
            }
            return true;
        }

        void OnPaintHandler(object sender, PaintEventArgs e)
        {
            if (this.videoRenderer != null)
            {
                //Trace.WriteLineIf(trace.TraceInfo, "OnPaintHandler()");

                IntPtr hdc = e.Graphics.GetHdc();
                try
                {
                    int hr = (this.videoRenderer as IVMRWindowlessControl).RepaintVideo(this.hostingControl.Handle, hdc);
                    DsError.ThrowExceptionForHR(hr);
                }
                catch { }

                e.Graphics.ReleaseHdc(hdc);

                PaintBlackBands(e.Graphics);
            }
        }

        protected virtual void PaintBlackBands(Graphics g)
        {
            //if (this.videoRenderer != null)
            //{
            //    IVMRWindowlessControl vmrWindowlessControl = this.videoRenderer as IVMRWindowlessControl;
            //    Rectangle outerRectangle = this.hostingControl.ClientRectangle;
            //    DsRect innerDsRect = new DsRect();
            //    vmrWindowlessControl.GetVideoPosition(null, innerDsRect);
            //    Rectangle innerRectangle = innerDsRect.ToRectangle();

            //    //Trace.WriteLineIf(trace.TraceVerbose, string.Format(("\tvideoRenderer.GetVideoPosition({0})"), innerRectangle.ToString()));
            //    //Trace.WriteLineIf(trace.TraceVerbose, string.Format(("\thostingControl.ClientRectangle({0})"), outerRectangle.ToString()));

            //    ArrayList alRectangles = new ArrayList();

            //    if (innerRectangle.Top > outerRectangle.Top)
            //        alRectangles.Add(new Rectangle(outerRectangle.Left, outerRectangle.Top, outerRectangle.Width - 1, innerRectangle.Top - 1));

            //    if (innerRectangle.Bottom < outerRectangle.Bottom)
            //        alRectangles.Add(new Rectangle(outerRectangle.Left, innerRectangle.Bottom, outerRectangle.Width - 1, outerRectangle.Height - (innerRectangle.Bottom + 1)));

            //    if (innerRectangle.Left > outerRectangle.Left)
            //    {
            //        Rectangle rectangleLeft = new Rectangle(outerRectangle.Left, innerRectangle.Top, innerRectangle.Left - 1, innerRectangle.Height - 1);
            //        rectangleLeft.Intersect(outerRectangle);
            //        alRectangles.Add(rectangleLeft);
            //    }

            //    if (innerRectangle.Right < outerRectangle.Right)
            //    {
            //        Rectangle rectangleLeft = new Rectangle(innerRectangle.Right, innerRectangle.Top, outerRectangle.Width - (innerRectangle.Right + 1), innerRectangle.Height - 1);
            //        rectangleLeft.Intersect(outerRectangle);
            //        alRectangles.Add(rectangleLeft);
            //    }

            //    if (alRectangles.Count > 0)
            //    {
            //        g.FillRectangles(new SolidBrush(System.Drawing.Color.Black), (Rectangle[])alRectangles.ToArray(typeof(Rectangle)));
            //        g.DrawRectangles(new System.Drawing.Pen(System.Drawing.Color.Black), (Rectangle[])alRectangles.ToArray(typeof(Rectangle)));
            //    }
            //}

            if (this.videoRenderer != null)
            {
                //Trace.WriteLineIf(trace.TraceInfo, "PaintBlackBands()");

                IVMRWindowlessControl9 vmrWindowlessControl9 = this.videoRenderer as IVMRWindowlessControl9;
                Rectangle outerRectangle = this.hostingControl.ClientRectangle;
                DsRect innerDsRect = new DsRect();
                vmrWindowlessControl9.GetVideoPosition(null, innerDsRect);
                Rectangle innerRectangle = innerDsRect.ToRectangle();

                ArrayList alRectangles = new ArrayList();

                if (innerRectangle.Top > outerRectangle.Top)
                    alRectangles.Add(new Rectangle(outerRectangle.Left, outerRectangle.Top, outerRectangle.Width - 1, innerRectangle.Top - 1));

                if (innerRectangle.Bottom < outerRectangle.Bottom)
                    alRectangles.Add(new Rectangle(outerRectangle.Left, innerRectangle.Bottom, outerRectangle.Width - 1, outerRectangle.Height - (innerRectangle.Bottom + 1)));

                if (innerRectangle.Left > outerRectangle.Left)
                {
                    Rectangle rectangleLeft = new Rectangle(outerRectangle.Left, innerRectangle.Top, innerRectangle.Left - 1, innerRectangle.Height - 1);
                    rectangleLeft.Intersect(outerRectangle);
                    alRectangles.Add(rectangleLeft);
                }

                if (innerRectangle.Right < outerRectangle.Right)
                {
                    Rectangle rectangleLeft = new Rectangle(innerRectangle.Right, innerRectangle.Top, outerRectangle.Width - (innerRectangle.Right + 1), innerRectangle.Height - 1);
                    rectangleLeft.Intersect(outerRectangle);
                    alRectangles.Add(rectangleLeft);
                }

                if (alRectangles.Count > 0)
                {
                    g.FillRectangles(new SolidBrush(System.Drawing.Color.Black), (Rectangle[])alRectangles.ToArray(typeof(Rectangle)));
                    g.DrawRectangles(new System.Drawing.Pen(System.Drawing.Color.Black), (Rectangle[])alRectangles.ToArray(typeof(Rectangle)));
                }
            }
        }

		protected override void Decompose()
		{
			int hr = 0;            

			// Decompose the graph
            hr = (this.graphBuilder as IMediaControl).Pause();
			hr = (this.graphBuilder as IMediaControl).StopWhenReady();
			hr = (this.graphBuilder as IMediaControl).Stop();

			RemoveHandlers();

			FilterGraphTools.RemoveAllFilters(this.graphBuilder);
            if (decoderFilter != null) Marshal.ReleaseComObject(this.decoderFilter); this.decoderFilter = null;
            if (skyMpeg2VideoDecoder != null) Marshal.ReleaseComObject(this.skyMpeg2VideoDecoder); this.skyMpeg2VideoDecoder = null;
            if (skyPsiFilter != null) Marshal.ReleaseComObject(this.skyPsiFilter); this.skyPsiFilter = null;
            if (this.antunerFilter != null) Marshal.ReleaseComObject(this.antunerFilter); this.antunerFilter = null;
            if (this.networkProvider != null) Marshal.ReleaseComObject(this.networkProvider); this.networkProvider = null;
            if (this.mpeg2Demux != null) Marshal.ReleaseComObject(this.mpeg2Demux); this.mpeg2Demux = null;
            if (this.tuner != null) Marshal.ReleaseComObject(this.tuner); this.tuner = null;
            if (this.capture != null) Marshal.ReleaseComObject(this.capture); this.capture = null;
            if (this.bdaTIF != null) Marshal.ReleaseComObject(this.bdaTIF); this.bdaTIF = null;
            if (this.bdaSecTab != null) Marshal.ReleaseComObject(this.bdaSecTab); this.bdaSecTab = null;
            if (this.audioRenderer != null) Marshal.ReleaseComObject(this.audioRenderer); this.audioRenderer = null;
            if (this.videoRenderer != null) Marshal.ReleaseComObject(this.videoRenderer); this.videoRenderer = null;

			rot.Dispose();
			Marshal.ReleaseComObject(this.graphBuilder); this.graphBuilder = null;
		}

		public override bool GetSignalStatistics(out bool locked, out bool present, out int strength, out int quality)
		{
			int longVal = strength = quality = 0;
			bool byteVal = locked = present = false;

			//Get IID_IBDA_Topology
            IBDA_Topology bdaNetTop = this.tuner as IBDA_Topology;
			if (bdaNetTop == null)
			{
				return false;
			}

			int nodeTypes;
			int[] nodeType = new int[32];
			int hr = bdaNetTop.GetNodeTypes(out nodeTypes, 32, nodeType);
			DsError.ThrowExceptionForHR(hr);

			for (int i = 0; i < nodeTypes; i++)
			{
				object iNode;
				hr = bdaNetTop.GetControlNode(0, 1, nodeType[i], out iNode);
				if (hr == 0)
				{
					IBDA_SignalStatistics pSigStats = iNode as IBDA_SignalStatistics;
					if (pSigStats != null)
					{
						longVal = 0;
						if (pSigStats.get_SignalStrength(out longVal) == 0)
							strength = longVal;

						longVal = 0;
						if (pSigStats.get_SignalQuality(out longVal) == 0)
							quality = longVal;

						byteVal = false;
						if (pSigStats.get_SignalLocked(out byteVal) == 0)
							locked = byteVal;

						byteVal = false;
						if (pSigStats.get_SignalPresent(out byteVal) == 0)
							present = byteVal;

					}
					Marshal.ReleaseComObject(iNode);
					return true;
				}
			}
			return false;
		}

		private void PaintHandler(object sender, PaintEventArgs e)
		{
			if (this.videoRenderer != null)
			{
				IntPtr hdc = e.Graphics.GetHdc();
                int hr = (this.videoRenderer as IVMRWindowlessControl).RepaintVideo(this.hostingControl.Handle, hdc);
                DsError.ThrowExceptionForHR(hr);
				e.Graphics.ReleaseHdc(hdc);
			}
		}

		private void ResizeMoveHandler(object sender, EventArgs e)
		{
			int hr = 0;
			if (this.videoRenderer != null)
			{
                //hr = (this.videoRenderer as IVMRWindowlessControl).SetVideoPosition(null, DsRect.FromRectangle(this.hostingControl.ClientRectangle));
                hr = (this.videoRenderer as IVMRWindowlessControl9).SetVideoPosition(null, DsRect.FromRectangle(this.hostingControl.ClientRectangle));
                DsError.ThrowExceptionForHR(hr);
			}
		}

		private void DisplayChangedHandler(object sender, EventArgs e)
		{
			if (this.videoRenderer != null)
			{
                int hr = (this.videoRenderer as IVMRWindowlessControl).DisplayModeChanged();
                DsError.ThrowExceptionForHR(hr);
			}
		}

        public enum AM_MPEG2Profile
        {
            Simple = 1,
            Main,
            SNRScalable,
            SpatiallyScalable,
            High
        }

        public enum AM_MPEG2Level
        {
            Low = 1,
            Main,
            High1440,
            High
        }

        [Flags]
        public enum AMMPEG2
        {
            DoPanScan = 0x00000001,
            DVDLine21Field1 = 0x00000002,
            DVDLine21Field2 = 0x00000004,
            SourceIsLetterboxed = 0x00000008,
            FilmCameraMode = 0x00000010,
            LetterboxAnalogOut = 0x00000020,
            DSS_UserData = 0x00000040,
            DVB_UserData = 0x00000080,
            Timebase27Mhz = 0x00000100,
            WidescreenAnalogOut = 0x00000200
        }

        /// <summary>
        /// From MPEG2VIDEOINFO
        /// </summary>
        [StructLayout(LayoutKind.Sequential, Pack = 4)]
        public class MPEG2VideoInfo
        {
            public VideoInfoHeader2 hdr;
            public uint StartTimeCode;
            public uint SequenceHeader;
            public AM_MPEG2Profile Profile;
            public AM_MPEG2Level Level;
            public AMMPEG2 Flags;
        }

        /// <summary>
        /// From MPEG1WAVEFORMAT
        /// </summary>
        [StructLayout(LayoutKind.Sequential, Pack = 2)]
        public class MPEG1WaveFormat //: WaveFormatEx // produce a bug in ngen 
        {
            // WaveFormatEx
            public short wFormatTag;
            public short nChannels;
            public int nSamplesPerSec;
            public int nAvgBytesPerSec;
            public short nBlockAlign;
            public short wBitsPerSample;
            public short cbSize;

            // MPEG1WaveFormat
            public ushort HeadLayer;
            public uint HeadBitrate;
            public ushort HeadMode;
            public ushort HeadModeExt;
            public ushort HeadEmphasis;
            public ushort HeadFlags;
            public uint PTSLow;
            public uint PTSHigh;
        }

        protected static MPEG2VideoInfo videoMPEG2PinFormat;
        protected static MPEG1WaveFormat audioPinFormat;

        protected static MPEG2VideoInfo GetVideoMPEG2PinFormat()
        {
            if (videoMPEG2PinFormat == null)
            {
                MPEG2VideoInfo videoPinFormat = new MPEG2VideoInfo();
                videoPinFormat.hdr = new VideoInfoHeader2();
                videoPinFormat.hdr.SrcRect = new DsRect();
                videoPinFormat.hdr.SrcRect.left = 0;		//0x00, 0x00, 0x00, 0x00,  //00  .hdr.rcSource.left              = 0x00000000
                videoPinFormat.hdr.SrcRect.top = 0;			//0x00, 0x00, 0x00, 0x00,  //04  .hdr.rcSource.top               = 0x00000000
                videoPinFormat.hdr.SrcRect.right = 0;		//0xD0, 0x02, 0x00, 0x00,  //08  .hdr.rcSource.right             = 0x000002d0 //720
                videoPinFormat.hdr.SrcRect.bottom = 0;		//0x40, 0x02, 0x00, 0x00,  //0c  .hdr.rcSource.bottom            = 0x00000240 //576
                videoPinFormat.hdr.TargetRect = new DsRect();
                videoPinFormat.hdr.TargetRect.left = 0;		//0x00, 0x00, 0x00, 0x00,  //10  .hdr.rcTarget.left              = 0x00000000
                videoPinFormat.hdr.TargetRect.top = 0;		//0x00, 0x00, 0x00, 0x00,  //14  .hdr.rcTarget.top               = 0x00000000
                videoPinFormat.hdr.TargetRect.right = 0;	//0xD0, 0x02, 0x00, 0x00,  //18  .hdr.rcTarget.right             = 0x000002d0 //720
                videoPinFormat.hdr.TargetRect.bottom = 0;	//0x40, 0x02, 0x00, 0x00,  //1c  .hdr.rcTarget.bottom            = 0x00000240// 576
                videoPinFormat.hdr.BitRate = 0x003d0900;	//0x00, 0x09, 0x3D, 0x00,  //20  .hdr.dwBitRate                  = 0x003d0900
                videoPinFormat.hdr.BitErrorRate = 0;		//0x00, 0x00, 0x00, 0x00,  //24  .hdr.dwBitErrorRate             = 0x00000000

                ////0x051736=333667-> 10000000/333667 = 29.97fps
                ////0x061A80=400000-> 10000000/400000 = 25fps
                videoPinFormat.hdr.AvgTimePerFrame = 400000;				//0x80, 0x1A, 0x06, 0x00, 0x00, 0x00, 0x00, 0x00, //28  .hdr.AvgTimePerFrame            = 0x0000000000051763 ->1000000/ 40000 = 25fps

                videoPinFormat.hdr.InterlaceFlags = AMInterlace.None;		//0x00, 0x00, 0x00, 0x00, 
                videoPinFormat.hdr.CopyProtectFlags = AMCopyProtect.None;	//0x00, 0x00, 0x00, 0x00,                         //30  .hdr.dwCopyProtectFlags         = 0x00000000
                videoPinFormat.hdr.PictAspectRatioX = 4;					//0x04, 0x00, 0x00, 0x00,                         //34  .hdr.dwPictAspectRatioX         = 0x00000004
                videoPinFormat.hdr.PictAspectRatioY = 3;					//0x03, 0x00, 0x00, 0x00,                         //38  .hdr.dwPictAspectRatioY         = 0x00000003
                videoPinFormat.hdr.ControlFlags = AMControl.None;			//0x00, 0x00, 0x00, 0x00,                         //3c  .hdr.dwReserved1                = 0x00000000
                videoPinFormat.hdr.Reserved2 = 0;							//0x00, 0x00, 0x00, 0x00,                         //40  .hdr.dwReserved2                = 0x00000000
                videoPinFormat.hdr.BmiHeader = new BitmapInfoHeader();
                videoPinFormat.hdr.BmiHeader.Size = 0x00000028;				//0x28, 0x00, 0x00, 0x00,  //44  .hdr.bmiHeader.biSize           = 0x00000028
                videoPinFormat.hdr.BmiHeader.Width = 720;					//0xD0, 0x02, 0x00, 0x00,  //48  .hdr.bmiHeader.biWidth          = 0x000002d0 //720
                videoPinFormat.hdr.BmiHeader.Height = 576;					//0x40, 0x02, 0x00, 0x00,  //4c  .hdr.bmiHeader.biHeight         = 0x00000240 //576
                videoPinFormat.hdr.BmiHeader.Planes = 0;					//0x00, 0x00,              //50  .hdr.bmiHeader.biPlanes         = 0x0000
                videoPinFormat.hdr.BmiHeader.BitCount = 0;					//0x00, 0x00,              //54  .hdr.bmiHeader.biBitCount       = 0x0000
                videoPinFormat.hdr.BmiHeader.Compression = 0;				//0x00, 0x00, 0x00, 0x00,  //58  .hdr.bmiHeader.biCompression    = 0x00000000
                videoPinFormat.hdr.BmiHeader.ImageSize = 0;					//0x00, 0x00, 0x00, 0x00,  //5c  .hdr.bmiHeader.biSizeImage      = 0x00000000
                videoPinFormat.hdr.BmiHeader.XPelsPerMeter = 0x000007d0;	//0xD0, 0x07, 0x00, 0x00,  //60  .hdr.bmiHeader.biXPelsPerMeter  = 0x000007d0
                videoPinFormat.hdr.BmiHeader.YPelsPerMeter = 0x0000cf27;	//0x27, 0xCF, 0x00, 0x00,  //64  .hdr.bmiHeader.biYPelsPerMeter  = 0x0000cf27
                videoPinFormat.hdr.BmiHeader.ClrUsed = 0;					//0x00, 0x00, 0x00, 0x00,  //68  .hdr.bmiHeader.biClrUsed        = 0x00000000
                videoPinFormat.hdr.BmiHeader.ClrImportant = 0;				//0x00, 0x00, 0x00, 0x00,  //6c  .hdr.bmiHeader.biClrImportant   = 0x00000000
                videoPinFormat.StartTimeCode = 0x0006f498;		//0x98, 0xF4, 0x06, 0x00,    //70  .dwStartTimeCode                = 0x0006f498
                videoPinFormat.SequenceHeader = 0;				//0x00, 0x00, 0x00, 0x00,    //74  .cbSequenceHeader               = 0x00000000
                videoPinFormat.Profile = AM_MPEG2Profile.Main;	//0x02, 0x00, 0x00, 0x00,    //78  .dwProfile                      = 0x00000002
                videoPinFormat.Level = AM_MPEG2Level.Main;		//0x02, 0x00, 0x00, 0x00,    //7c  .dwLevel                        = 0x00000002
                videoPinFormat.Flags = (AMMPEG2)0;				//0x00, 0x00, 0x00, 0x00,    //80  .Flags    

                videoMPEG2PinFormat = videoPinFormat;
            }
            return videoMPEG2PinFormat;
        }

        protected static MPEG1WaveFormat GetAudioPinFormat()
        {
            if (audioPinFormat == null)
            {
                audioPinFormat = new MPEG1WaveFormat();
                audioPinFormat.wFormatTag = 0x0050; // WAVE_FORMAT_MPEG
                audioPinFormat.nChannels = 2;
                audioPinFormat.nSamplesPerSec = 48000;
                audioPinFormat.nAvgBytesPerSec = 32000;
                audioPinFormat.nBlockAlign = 768;
                audioPinFormat.wBitsPerSample = 16;
                audioPinFormat.cbSize = 22; // extra size

                audioPinFormat.HeadLayer = 2;
                audioPinFormat.HeadBitrate = 0x00177000;
                audioPinFormat.HeadMode = 1;
                audioPinFormat.HeadModeExt = 1;
                audioPinFormat.HeadEmphasis = 1;
                audioPinFormat.HeadFlags = 0x1c;
                audioPinFormat.PTSLow = 0;
                audioPinFormat.PTSHigh = 0;
            }

            return audioPinFormat;
        }
	}
}
