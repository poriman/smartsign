﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;
using System.Security;

namespace TVControl.Library
{
    [ComImport, SuppressUnmanagedCodeSecurity]
    [Guid("33828C11-CBB2-4e53-A3B1-FD59D614DDC9")]
    [InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
    public interface IPsiFilter
    {

        [PreserveSig]
        void ResetParsing();
        
        [PreserveSig]
        void SetProgPidEventHandle([In] IntPtr hwnd);
        
        [PreserveSig]
        void SetProgNameEventHandle([In] IntPtr hwnd);
        
        [PreserveSig]
        void GetProgramCount([Out, MarshalAs(UnmanagedType.I4)] out int pcount);
        
        [PreserveSig]
        void GetProgramNumber([In, MarshalAs(UnmanagedType.I4)] int index, [Out, MarshalAs(UnmanagedType.I4)] out int program_number);
         
        [PreserveSig]
        void GetProgramInfo([In] int program_number,
                            [MarshalAs(UnmanagedType.LPWStr,SizeConst=32)] System.Text.StringBuilder short_name,
                            [Out] out int major_channel_number,
                            [Out] out int minor_channel_number,
                            [Out] out int elementary_stream_count,
                            [Out] out int source_id,
                            [Out] out int PcrPID);

        [PreserveSig]
        void GetElementaryPID([In] int program_number,
                                [In] int index,
                                [Out] out int ptype,
                                [Out] out int pPID,
                                [MarshalAs(UnmanagedType.LPWStr)] System.Text.StringBuilder codestring,
                                [Out] out int scrambled);
        [PreserveSig]
        void EpgControlMsg([In, MarshalAs(UnmanagedType.I4)] int EpgMsg, [Out, MarshalAs(UnmanagedType.I4)] out int Opt);

        [PreserveSig]
        void AcquireDemux([In, MarshalAs(UnmanagedType.I4)] int OnOff);
    }

}
