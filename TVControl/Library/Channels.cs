//    Copyright (C) 2006-2007  Regis COSNIER
//
//    This program is free software; you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation; either version 2 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program; if not, write to the Free Software
//    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Drawing;
using System.Drawing.Design;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using System.Windows.Forms.Design;
using System.Xml;
using System.Xml.Serialization;

using DirectShowLib;
using System.Diagnostics;

namespace TVControl.Library
{
	public enum TunerType { Unknown, DVBT, DVBC, DVBS, Analogic, ATSC };

	public class Channel : ICloneable
	{
		protected string name = "No Name";
		private object tag;

		public Channel() { }
		public Channel(string name)
		{
			this.name = name;
		}

		private int channel; // the TV channel that the tuner is set to.  
		private int countryCode; // the country/region code. 
        private TunerInputType inputType; // the tuner input type (cable or antenna).
        private int videopid;
        private int audiopid;

		public string Name { get { return this.name; } set { this.name = value; } }
        public TunerInputType InputType { get { return this.inputType; } set { this.inputType = value; } }
		public int ChannelNumber { get { return this.channel; } set { this.channel = value; } }
		public int CountryCode { get { return this.countryCode; } set { this.countryCode = value; } }

        public int VideoPidNum { get { return this.videopid; } set { this.videopid = value; } }
        public int AudioPidNum { get { return this.audiopid; } set { this.audiopid = value; } }
        
		public object Clone()
		{
			return base.MemberwiseClone(); 
		}

		public TunerType TunerType
		{
			get
			{
				if (this is Channel)
				{
					if (this is ChannelDVBT) return TunerType.DVBT;
					else if (this is ChannelDVBC) return TunerType.DVBC;
					else if (this is ChannelDVBS) return TunerType.DVBS;
					else if (this is ChannelAnalogic) return TunerType.Analogic;
					else if (this is ChannelATSC) return TunerType.ATSC;
				}

				return TunerType.Unknown;
			}
		}
		public XmlNode ConvertToXml(XmlDocument doc)
		{
			XmlNode nodeChannel = doc.CreateNode(XmlNodeType.Element, "Channel", "");

			XmlAttribute attr1 = doc.CreateAttribute("Name");
			attr1.Value = this.Name;
			XmlAttribute attr2 = doc.CreateAttribute("Number");
			attr2.Value = this.ChannelNumber.ToString();
			XmlAttribute attr3 = doc.CreateAttribute("CountryCode");
			attr3.Value = this.CountryCode.ToString();
			XmlAttribute attr4 = doc.CreateAttribute("TunerType");
			attr4.Value = this.TunerType.ToString();

			nodeChannel.Attributes.Append(attr1);
			nodeChannel.Attributes.Append(attr2);
			nodeChannel.Attributes.Append(attr3);
			nodeChannel.Attributes.Append(attr4);           

			if (this is ChannelAnalogic)
			{
				ChannelAnalogic analch = this as ChannelAnalogic;
				XmlAttribute attr5 = doc.CreateAttribute("InputType");
				attr5.Value = analch.InputType.ToString();

				nodeChannel.Attributes.Append(attr5);
			}
			else if (this is ChannelATSC)
			{
				ChannelATSC atscch = this as ChannelATSC;

				XmlAttribute attr5 = doc.CreateAttribute("Modulation");
                attr5.Value = atscch.Modulation.ToString();

                XmlAttribute attr6 = doc.CreateAttribute("VideoPid");
                attr6.Value = this.VideoPidNum.ToString();

                XmlAttribute attr7 = doc.CreateAttribute("AudioPid");
                attr7.Value = this.AudioPidNum.ToString();

                XmlAttribute attr8 = doc.CreateAttribute("InputType");
                attr8.Value = atscch.InputType.ToString();	

				nodeChannel.Attributes.Append(attr5);
                nodeChannel.Attributes.Append(attr6);
                nodeChannel.Attributes.Append(attr7);
                nodeChannel.Attributes.Append(attr8);
			}
			else
				throw new NotImplementedException();

			return nodeChannel;

		}

		public static Channel GetChannelFromXml(XmlNode node)
		{
			Channel ch = null;
			if(node.Attributes["TunerType"].Value.Equals(TunerType.ATSC.ToString()))
			{
				ch = new ChannelATSC();

				if(node.Attributes["Modulation"].Value.Equals(DirectShowLib.BDA.ModulationType.Mod8Vsb.ToString()))
					((ChannelATSC)ch).Modulation = DirectShowLib.BDA.ModulationType.Mod8Vsb;
				else if (node.Attributes["Modulation"].Value.Equals(DirectShowLib.BDA.ModulationType.Mod32Qam.ToString()))
					((ChannelATSC)ch).Modulation = DirectShowLib.BDA.ModulationType.Mod32Qam;
				else if (node.Attributes["Modulation"].Value.Equals(DirectShowLib.BDA.ModulationType.Mod128Qam.ToString()))
					((ChannelATSC)ch).Modulation = DirectShowLib.BDA.ModulationType.Mod128Qam;
				else
					((ChannelATSC)ch).Modulation = DirectShowLib.BDA.ModulationType.Mod8Vsb;

                ((ChannelATSC)ch).VideoPidNum = int.Parse(node.Attributes["VideoPid"].Value.ToString());
                ((ChannelATSC)ch).AudioPidNum = int.Parse(node.Attributes["AudioPid"].Value.ToString());
                ((ChannelATSC)ch).InputType = node.Attributes["InputType"].Value.Equals(DirectShowLib.TunerInputType.Antenna.ToString()) ? DirectShowLib.TunerInputType.Antenna : DirectShowLib.TunerInputType.Cable;

			}
			else if(node.Attributes["TunerType"].Value.Equals(TunerType.Analogic.ToString()))
			{
				ch = new ChannelAnalogic();

				((ChannelAnalogic)ch).InputType = node.Attributes["InputType"].Value.Equals(DirectShowLib.TunerInputType.Antenna.ToString()) ? DirectShowLib.TunerInputType.Antenna : DirectShowLib.TunerInputType.Cable;
				((ChannelAnalogic)ch).AudioMode = TVAudioMode.Stereo;
				((ChannelAnalogic)ch).VideoSource = "Video Tuner";
				((ChannelAnalogic)ch).VideoStandard = AnalogVideoStandard.NTSC_M;
				((ChannelAnalogic)ch).Mode = AMTunerModeType.TV;
			}
			else if (node.Attributes["TunerType"].Value.Equals(TunerType.DVBC.ToString()))
			{
				ch = new ChannelDVBC();
			}
			else if (node.Attributes["TunerType"].Value.Equals(TunerType.DVBS.ToString()))
			{
				ch = new ChannelDVBS();
			}
			else if (node.Attributes["TunerType"].Value.Equals(TunerType.DVBT.ToString()))
			{
				ch = new ChannelDVBT();
			}
			else
			{
				throw new NotImplementedException();
			}

			ch.ChannelNumber = Convert.ToInt32(node.Attributes["Number"].Value);
			ch.CountryCode = Convert.ToInt32(node.Attributes["CountryCode"].Value);
			ch.Name = node.Attributes["Name"].Value;

			return ch;
		}


		public override string ToString()
		{
			return this.name;
		}
	}

	public class ChannelAnalogic : Channel
	{
		protected string videoCaptureDeviceName = "";
		protected string audioCaptureDeviceName = "";

		private AMTunerModeType mode; // the current mode on a multifunction tuner.  
		private int tuningSpace; // the storage index for regional fine tuning.

		private int connectInput; // the hardware tuner input connection. 		
		private AnalogVideoStandard videoStandard = AnalogVideoStandard.None; // PAL/SECAM/NTSC

		private string audioSource = "";
		private string videoSource = "";
		private TVAudioMode audioMode = TVAudioMode.Stereo;

		public ChannelAnalogic() { }

		public string VideoCaptureDeviceName { get { return this.videoCaptureDeviceName; } set { this.videoCaptureDeviceName = value; } }
		public string AudioCaptureDeviceName { get { return this.audioCaptureDeviceName; } set { this.audioCaptureDeviceName = value; } }

		public AMTunerModeType Mode { get { return this.mode; } set { this.mode = value; } }
		public int TuningSpace { get { return this.tuningSpace; } set { this.tuningSpace = value; } }

		public int ConnectInput { get { return this.connectInput; } set { this.connectInput = value; } }		
		public AnalogVideoStandard VideoStandard { get { return this.videoStandard; } set { this.videoStandard = value; } }

		public string AudioSource { get { return this.audioSource; } set { this.audioSource = value; } }
		public string VideoSource { get { return this.videoSource; } set { this.videoSource = value; } }

		public TVAudioMode AudioMode { get { return this.audioMode; } set { this.audioMode = value; } }

		public override string ToString()
		{
			return base.ToString() + ", videoDevice: " + this.VideoCaptureDeviceName + ", audioDevice: " + this.AudioCaptureDeviceName;
		}
	}

	public class ChannelATSC : Channel
	{
		public enum VideoType { MPEG2, H264 }
		public enum Clock { Default, MPEGDemultiplexer, AudioRenderer };

		private Clock referenceClock = Clock.AudioRenderer;
		private VideoType videoType = VideoType.MPEG2;

		private string audioDecoderDevice = "";
		private string mpeg2DecoderDevice = "";

        private TunerInputType inputType;
        private bool bPsiMode = false;

		protected string tunerDevice = "";
        private int videoPid = -1;
        private int audioPid = -1;
		private int frequency = -1;

		private DirectShowLib.BDA.ModulationType modulation = DirectShowLib.BDA.ModulationType.ModNotSet;
		private int symbolRate = -1;
		private DirectShowLib.BDA.FECMethod innerFEC;
		private DirectShowLib.BDA.BinaryConvolutionCodeRate innerFECRate;
		private DirectShowLib.BDA.FECMethod outerFEC;
		private DirectShowLib.BDA.BinaryConvolutionCodeRate outerFECRate;

		public ChannelATSC()
		{

		}
        public bool PSIMODE { get { return bPsiMode; } set { bPsiMode = value; } }
		public Clock ReferenceClock { get { return this.referenceClock; } set { this.referenceClock = value; } }
		public string AudioDecoderDevice { get { return this.audioDecoderDevice; } set { this.audioDecoderDevice = value; } }

		public VideoType VideoDecoderType { get { return this.videoType; } set { this.videoType = value; } }

		public string TunerDevice { get { return this.tunerDevice; } set { this.tunerDevice = value; } }

		public int Frequency { get { return this.frequency; } set { this.frequency = value; } }

		public DirectShowLib.BDA.ModulationType Modulation { get { return this.modulation; } set { this.modulation = value; } }
		public int SymbolRate { get { return this.symbolRate; } set { this.symbolRate = value; } }
		public DirectShowLib.BDA.FECMethod InnerFEC { get { return this.innerFEC; } set { this.innerFEC = value; } }
		public DirectShowLib.BDA.BinaryConvolutionCodeRate InnerFECRate { get { return this.innerFECRate; } set { this.innerFECRate = value; } }
		public DirectShowLib.BDA.FECMethod OuterFEC { get { return this.outerFEC; } set { this.outerFEC = value; } }
		public DirectShowLib.BDA.BinaryConvolutionCodeRate OuterFECRate { get { return this.outerFECRate; } set { this.outerFECRate = value; } }

        public int VideoPid { get { return this.videoPid; } set { this.videoPid = value; } }
        public int AudioPid { get { return this.audioPid; } set { this.audioPid = value; } }
        public TunerInputType InputType { get { return this.inputType; } set { this.inputType = value; } }

		public override string ToString()
		{
			return base.ToString() + ", tunerDevice: " + this.TunerDevice + ", frequency: " + this.Frequency + " KHz, Modulation: " + this.modulation.ToString() + ", SymbolRate: " + this.symbolRate;
		}

	}
	public class ChannelDVB : Channel
	{
		public enum VideoType { MPEG2, H264 }
		public enum Clock { Default, MPEGDemultiplexer, AudioRenderer };

		private Clock referenceClock = Clock.AudioRenderer;
		private VideoType videoType = VideoType.MPEG2;
		private string audioDecoderDevice = "";
		private string mpeg2DecoderDevice = "";
		private string h264DecoderDevice = "";
		protected string tunerDevice = "";
		protected string captureDevice = "";

		private int frequency = -1;

		private int onid = -1;
		private int tsid = -1;
		private int sid = -1;

		private int pmtPid = -1;
		private int videoPid = -1;
		private int pcrPid = -1;
		private int audioPid = -1;
		private int[] audioPids = new int[0];
		private int teletextPid = -1;
		private int ecmPid = -1;
		private int[] ecmPids = new int[0];

		private DirectShowLib.BDA.ModulationType modulation = DirectShowLib.BDA.ModulationType.ModNotSet;
		private int symbolRate = -1;
		private DirectShowLib.BDA.FECMethod innerFEC;
		private DirectShowLib.BDA.BinaryConvolutionCodeRate innerFECRate;
		private DirectShowLib.BDA.FECMethod outerFEC;
		private DirectShowLib.BDA.BinaryConvolutionCodeRate outerFECRate;

		public ChannelDVB() { }

		public Clock ReferenceClock { get { return this.referenceClock; } set { this.referenceClock = value; } }
		public string AudioDecoderDevice { get { return this.audioDecoderDevice; } set { this.audioDecoderDevice = value; } }

		public VideoType VideoDecoderType { get { return this.videoType; } set { this.videoType = value; } }
		public string MPEG2DecoderDevice { get { return this.mpeg2DecoderDevice; } set { this.mpeg2DecoderDevice = value; } }
		public string H264DecoderDevice { get { return this.h264DecoderDevice; } set { this.h264DecoderDevice = value; } }
		public string TunerDevice { get { return this.tunerDevice; } set { this.tunerDevice = value; } }
		public string CaptureDevice { get { return this.captureDevice; } set { this.captureDevice = value; } }

		public int Frequency { get { return this.frequency; } set { this.frequency = value; } }

		public DirectShowLib.BDA.ModulationType Modulation { get { return this.modulation; } set { this.modulation = value; } }
		public int SymbolRate { get { return this.symbolRate; } set { this.symbolRate = value; } }
		public DirectShowLib.BDA.FECMethod InnerFEC { get { return this.innerFEC; } set { this.innerFEC = value; } }
		public DirectShowLib.BDA.BinaryConvolutionCodeRate InnerFECRate { get { return this.innerFECRate; } set { this.innerFECRate = value; } }
		public DirectShowLib.BDA.FECMethod OuterFEC { get { return this.outerFEC; } set { this.outerFEC = value; } }
		public DirectShowLib.BDA.BinaryConvolutionCodeRate OuterFECRate { get { return this.outerFECRate; } set { this.outerFECRate = value; } }

		public int ONID { get { return this.onid; } set { this.onid = value; } }
		public int TSID { get { return this.tsid; } set { this.tsid = value; } }
		public int SID { get { return this.sid; } set { this.sid = value; } }

		public int PmtPid { get { return this.pmtPid; } set { this.pmtPid = value; } }
		public int VideoPid { get { return this.videoPid; } set { this.videoPid = value; } }
		public int PcrPid { get { return this.pcrPid; } set { this.pcrPid = value; } }
		public int AudioPid { get { return this.audioPid; } set { this.audioPid = value; } }
		public int[] AudioPids { get { return this.audioPids; } set { this.audioPids = value; } }
		public int TeletextPid { get { return this.teletextPid; } set { this.teletextPid = value; } }
		public int EcmPid { get { return this.ecmPid; } set { this.ecmPid = value; } }
		public int[] EcmPids { get { return this.ecmPids; } set { this.ecmPids = value; } }

		public override string ToString()
		{
			return base.ToString() + ", tunerDevice: " + this.TunerDevice + ", captureDevice: " + this.CaptureDevice + ", frequency: " + this.Frequency + " KHz, onid: " + this.ONID + ", TSID: " + this.TSID + ", SID: " + this.SID + ", Modulation: " + this.modulation.ToString() + ", SymbolRate: " + this.symbolRate;
		}
	}

	public class ChannelDVBT : ChannelDVB
	{
		private int bandwidth = -1;
		private DirectShowLib.BDA.HierarchyAlpha hAlpha = DirectShowLib.BDA.HierarchyAlpha.HAlphaNotSet;
		private DirectShowLib.BDA.GuardInterval guard = DirectShowLib.BDA.GuardInterval.GuardNotSet;
		private DirectShowLib.BDA.TransmissionMode mode = DirectShowLib.BDA.TransmissionMode.ModeNotSet;
		private DirectShowLib.BDA.FECMethod lpInnerFEC;
		private DirectShowLib.BDA.BinaryConvolutionCodeRate lpInnerFECRate;
		private bool otherFrequencyInUse = false;

		public ChannelDVBT() { }

		public int Bandwidth { get { return this.bandwidth; } set { this.bandwidth = value; } }
		public DirectShowLib.BDA.HierarchyAlpha HAlpha { get { return this.hAlpha; } set { this.hAlpha = value; } }
		public DirectShowLib.BDA.GuardInterval Guard { get { return this.guard; } set { this.guard = value; } }
		public DirectShowLib.BDA.TransmissionMode Mode { get { return this.mode; } set { this.mode = value; } }
		public DirectShowLib.BDA.FECMethod LPInnerFEC { get { return this.lpInnerFEC; } set { this.lpInnerFEC = value; } }
		public DirectShowLib.BDA.BinaryConvolutionCodeRate LPInnerFECRate { get { return this.lpInnerFECRate; } set { this.lpInnerFECRate = value; } }
		public bool OtherFrequencyInUse { get { return this.otherFrequencyInUse; } set { this.otherFrequencyInUse = value; } }


		public override string ToString()
		{
			return base.ToString() + ", bandwidth: " + this.bandwidth +
				", hAlpha: " + this.hAlpha.ToString() +
				", guard: " + this.guard.ToString() +
				", mode: " + this.mode.ToString();
		}
	}

	public class ChannelDVBC : ChannelDVB
	{
		public ChannelDVBC() { }

		public override string ToString()
		{
			return base.ToString();
		}
	}

	public class ChannelDVBS : ChannelDVB
	{
		private DirectShowLib.BDA.Polarisation signalPolarisation = DirectShowLib.BDA.Polarisation.NotSet;
		private bool westPosition = false;
		private int orbitalPosition = -1;
		private int azimuth = -1;
		private int elevation = -1;

		public ChannelDVBS() { }

		public DirectShowLib.BDA.Polarisation SignalPolarisation { get { return this.signalPolarisation; } set { this.signalPolarisation = value; } }
		public bool WestPosition { get { return this.westPosition; } set { this.westPosition = value; } }
		public int OrbitalPosition { get { return this.orbitalPosition; } set { this.orbitalPosition = value; } }
		public int Azimuth { get { return this.azimuth; } set { this.azimuth = value; } }
		public int Elevation { get { return this.elevation; } set { this.elevation = value; } }

		public override string ToString()
		{
			return base.ToString() + ", signalPolarisation: " + this.signalPolarisation +
				", westPosition: " + this.westPosition +
				", orbitalPosition: " + this.orbitalPosition +
				", azimuth: " + this.azimuth +
				", elevation: " + this.elevation;
		}
	}

}