﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace TVControl.Library
{
	/// <summary>
	/// TVSetting.xml를 불러와 클래스로 구성한다.
	/// </summary>
	public class DeviceInfo
	{
		public DeviceInfo()
		{
		}

		public DeviceInfo(string xmlfile)
		{
			LoadXmlFile(xmlfile);
		}

		#region Properties

		string	_WDMVideoDevice = "NONE";
		string	_WDMAudioDevice = "NONE";
		string	_BDADevice = "NONE";
		bool	_isVMR9 = false;

		public string WDMVideoDevice { get { return _WDMVideoDevice; } set {_WDMVideoDevice = value;} }
		public string WDMAudioDevice { get { return _WDMAudioDevice; } set { _WDMAudioDevice = value; } }
		public string BDADevice { get { return _BDADevice; } set { _BDADevice = value; } }
		public bool IsVMR9 { get { return _isVMR9; } set { _isVMR9 = value; } }

		#endregion

		public bool SaveXmlFile(string xmlfile)
		{
			try
			{
				XmlDocument doc = new XmlDocument();

				XmlElement root = doc.CreateElement("Configurations");
				XmlElement WDM_VD = doc.CreateElement("VideoCaptureDevice");
				WDM_VD.InnerText = _WDMVideoDevice;
				root.AppendChild(WDM_VD as XmlNode);

				XmlElement WDM_AD = doc.CreateElement("AudioCaptureDevice");
				WDM_AD.InnerText = _WDMAudioDevice;
				root.AppendChild(WDM_AD as XmlNode);

				XmlElement BDA_D = doc.CreateElement("BDADevice");
				BDA_D.InnerText = _BDADevice;
				root.AppendChild(BDA_D as XmlNode);

				XmlElement VMR9 = doc.CreateElement("IsVMR9");
				VMR9.InnerText = _isVMR9.ToString();
				root.AppendChild(VMR9 as XmlNode);
				
				doc.AppendChild(root as XmlNode);
				
				doc.Save(xmlfile);
				return true;
			}
			catch 
			{
			}
			return false;
		}

		public bool LoadXmlFile(string xmlfile)
		{
			try
			{
				XmlDocument doc = new XmlDocument();

				doc.Load(xmlfile);
				
				#region WDM Video Device 얻어오기
				try
				{
					XmlNode node = doc.SelectSingleNode("//child::VideoCaptureDevice");
					_WDMVideoDevice = node.InnerText;
				}
				catch { _WDMVideoDevice = "NONE"; }
				#endregion

				#region WDM Audio Device 얻어오기
				try
				{
					XmlNode node = doc.SelectSingleNode("//child::AudioCaptureDevice");
					_WDMAudioDevice = node.InnerText;
				}
				catch { _WDMAudioDevice = "NONE"; }
				#endregion

				#region BDA Device 얻어오기
				try
				{
					XmlNode node = doc.SelectSingleNode("//child::BDADevice");
					_BDADevice = node.InnerText;
				}
				catch { _BDADevice = "NONE"; }
				#endregion

				#region Video Renderer Mode 얻어오기
				try
				{
					XmlNode node = doc.SelectSingleNode("//child::IsVMR9");
					_isVMR9 = Convert.ToBoolean(node.InnerText);
				}
				catch { _isVMR9 = false; }
				#endregion

				return true;
			}
			catch 
			{
			}
			return false;
		}
	}
}
