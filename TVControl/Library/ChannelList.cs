﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Collections.ObjectModel;

namespace TVControl.Library
{
	public class ChannelList
	{
		Collection<Channel> arrChannels = new Collection<Channel>();

		string _filepath = "";

		public ChannelList(String xmlPath)
		{
			Initialize();
			LoadFromXml(_filepath = xmlPath);
		}
		public static Channel ComponentChannel
		{
			get {
				ChannelAnalogic comp = new ChannelAnalogic();

				comp.Name = "Component";
				comp.InputType = DirectShowLib.TunerInputType.Cable;
				comp.Mode = DirectShowLib.AMTunerModeType.Default;
				comp.VideoSource = "Video Composite";
				comp.VideoStandard = DirectShowLib.AnalogVideoStandard.NTSC_M;
				comp.CountryCode = 82;
				comp.ChannelNumber = 0;
				comp.AudioMode = DirectShowLib.TVAudioMode.Stereo;

				return comp;
			}
		}

        public static Channel DTVChannel
        {
            get
            {
                ChannelAnalogic comp = new ChannelAnalogic();

                comp.Name = "DTV";
                comp.InputType = DirectShowLib.TunerInputType.Cable;
                comp.Mode = DirectShowLib.AMTunerModeType.DTV;
                comp.VideoSource = "DTV Composite";
                comp.VideoStandard = DirectShowLib.AnalogVideoStandard.NTSC_M;
                comp.CountryCode = 82;
                comp.ChannelNumber = 2;
                comp.AudioPidNum = 0;
                comp.VideoPidNum = 0;
                comp.AudioMode = DirectShowLib.TVAudioMode.Stereo;

                return comp;
            }
        }

		public static Channel SVideoChannel
		{
			get
			{
				ChannelAnalogic comp = new ChannelAnalogic();

				comp.Name = "Component";
				comp.InputType = DirectShowLib.TunerInputType.Cable;
				comp.Mode = DirectShowLib.AMTunerModeType.Default;
				comp.VideoSource = "Video SVideo";
				comp.VideoStandard = DirectShowLib.AnalogVideoStandard.NTSC_M;
				comp.CountryCode = 82;
				comp.ChannelNumber = 0;
				comp.AudioMode = DirectShowLib.TVAudioMode.Stereo;

				return comp;
			}
		}

		void LoadFromXml(string xmlPath)
		{
			try
			{
				XmlDocument doc = new XmlDocument();
				doc.Load(xmlPath);

				XmlNodeList nodeChannels = doc.SelectNodes("//child::Channel");

				if (nodeChannels == null)
					return;

				foreach (XmlNode node in nodeChannels)
				{
					Channel ch = Channel.GetChannelFromXml(node);

					arrChannels.Add(ch);
				}
			}
			catch
			{
			}
		}

		void Initialize()
		{
			Clear();
		}

		public void Clear()
		{
			arrChannels.Clear();
		}

		public void AddChannel(Channel ch)
		{
			arrChannels.Add(ch);
		}
		public void UpdateChannel(int nIndex, String name)
		{
			arrChannels[nIndex].Name = name;
		}

		public Channel[] GetChannels()
		{
			return arrChannels.ToArray<Channel>();
		}
		public Channel GetChannel(int nIndex)
		{
            if (nIndex < 0)
                nIndex = 0;
			return arrChannels[nIndex];
		}

		public Channel FindChannel(string ch_name)
		{
			foreach (Channel ch in arrChannels)
			{
				if (ch.Name.Equals(ch_name))
					return ch;
			}

			return null;
		}

		public Channel FindChannel(string ch_name, bool isAnalog)
		{
			return FindChannel(isAnalog ? TunerType.Analogic : TunerType.ATSC, ch_name);
		}

		public Channel FindChannel(TunerType chtype, string ch_name)
		{
			foreach (Channel ch in arrChannels)
			{
				if (ch.TunerType.Equals(chtype) && ch.Name.Equals(ch_name))
					return ch;
			}

			return null;
		}

		public void Save()
		{
			XmlDocument doc = new XmlDocument();

			XmlNode nodeChannels = doc.CreateNode(XmlNodeType.Element, "Channels", "");

			foreach(Channel ch in arrChannels)
			{
				nodeChannels.AppendChild(ch.ConvertToXml(doc));
			}

			doc.AppendChild(nodeChannels);

			doc.Save(_filepath);
		}
	}
}
