﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DirectShowLib;
using DirectShowLib.Utils;

using System.Windows.Forms;
using System.Drawing;
using System.Collections;
using System.Runtime.InteropServices;

namespace TVControl.Library
{
	public class GraphBuilderTV : IDisposable
	{
		protected Channel _current_channel = null;

		protected bool bIsVMR9 = false;
        protected TVControl hostingControl = null;

		protected ICaptureGraphBuilder2 captureGraphBuilder = null;
		protected IFilterGraph2 graphBuilder = null;
		protected DsROTEntry rot = null;

		protected IBaseFilter audioRenderer = null;
		protected IBaseFilter videoRenderer = null;

		protected DsDevice videoCaptureDevice = null;
		protected DsDevice audioCaptureDevice = null;
        protected IBaseFilter skyMpeg2VideoDecoder = null;
        
        protected IPsiFilter PsiFilter = null;
        protected IAMTVTuner antuner = null;

        public bool bPSIMODE = false;

		public bool IsVMR9 { get { return bIsVMR9; } set { bIsVMR9 = value; } }

		public DsDevice VideoCaptureDevice { get { return this.videoCaptureDevice; } set { this.videoCaptureDevice = value; } }
		public DsDevice AudioCaptureDevice { get { return this.audioCaptureDevice; } set { this.audioCaptureDevice = value; } }

		public Channel CurrentChannel { get { return this._current_channel; } }
        public bool PSIMODE 
        { 
            set 
            { 
                bPSIMODE = value; 
            } 
            get
            { 
                return bPSIMODE; 
            } 
        }
		public GraphBuilderTV()
		{

		}

		#region Functions
		public virtual void BuildGraph(Channel defaultChannel) { }
		public virtual void RunGraph() { }
		public virtual void StopGraph() { }

        public IPsiFilter GetPsiFilter
        {
            get { return PsiFilter; }
        }

        public virtual IAMTVTuner GetAMTVTuner() { return antuner; }

		public void SaveGraph(String sFilePath) 
		{
			// Nothing to do with a DTV viewer but can be useful
			FilterGraphTools.SaveGraphFile(this.graphBuilder, sFilePath);
		}
		public virtual void SubmitTuneRequest(Channel channel) { }
		public virtual void GetMinMaxChannels(out int nMin, out int nMax)
		{ nMin = -1; nMax = -1; }
		protected virtual void Decompose() {}
        
		public virtual bool GetSignalStatistics(out bool locked, out bool present, out int strength, out int quality)
		{
			strength = quality = 0;
			locked = present = false;
			return false;
		}
		
		public void Dispose()
		{
            if (antuner != null) Marshal.ReleaseComObject(this.antuner); this.antuner = null;      
			Decompose();                  
		}

		#endregion

		#region Get Devices
		public static DsDevice[] GetVideoDsDevices()
		{
			DsDevice[] devices = null;
			try
			{
				devices = DsDevice.GetDevicesOfCat(FilterCategory.VideoInputDevice);
				return devices;
			}
			catch { return null; }
		}
		public static DsDevice[] GetAudioDsDevices()
		{
			DsDevice[] devices = null;
			try
			{
				devices = DsDevice.GetDevicesOfCat(FilterCategory.AMKSCapture);
				return devices;
			}
			catch { return null; }
		}
		public static DsDevice GetVideoDsDeviceByName(String deviceName)
		{
			try
			{
				DsDevice[] devices = GetVideoDsDevices();

				if (devices != null)
				{
					foreach (DsDevice device in devices)
					{
						if (device.Name != null && device.Name.Equals(deviceName)) return device;
					}
				}
			}
			catch
			{
			}
			return null;
		}

		public static DsDevice GetAudioDsDeviceByName(String deviceName)
		{
			try
			{
				DsDevice[] devices = GetAudioDsDevices();

				if (devices != null)
				{
					foreach (DsDevice device in devices)
					{
						if (device.Name != null && device.Name.Equals(deviceName)) return device;
					}
				}
			}
			catch
			{
			}
			return null;
		}

		public static DsDevice[] GetBDASourceDsDevices()
		{
			int hr = 0;
			DsDevice[] devices = null;

			try
			{
				// Enumerate BDA Source filters category and found one that can connect to the network provider
				devices = DsDevice.GetDevicesOfCat(FilterCategory.BDASourceFiltersCategory);
				return devices;
			}
			catch { return null; }
		}

		public static DsDevice GetBDADsDeviceByName(String deviceName)
		{
			try
			{
				DsDevice[] devices = GetBDASourceDsDevices();

				if (devices != null)
				{
					foreach (DsDevice device in devices)
					{
						if (device.Name != null && device.Name.Equals(deviceName)) return device;
					}
				}
			}
			catch
			{
			}
			return null;
		}

		#endregion


	}
}
