﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigitalSignage.EventReciever
{
    /// <summary>
    /// 이벤트 반환 데이터를 관리한다.
    /// </summary>
    public class EventDataHelper
    {
        /// <summary>
        /// 데이터 사전
        /// </summary>
        Dictionary<String, String> _dicData = new Dictionary<string, string>();

        /// <summary>
        /// 이벤트 관리 딕셔너리 생성
        /// </summary>
        public static EventDataHelper MakeEventDataDictionary(String sData)
        {
            return new EventDataHelper(sData);
        }

        /// <summary>
        /// 생성자
        /// </summary>
        /// <param name="data"></param>
        public EventDataHelper(string data)
        {
            if (!String.IsNullOrEmpty(data))
            {
                _dicData.Clear();

                String[] arrTemp = data.Split(')');

                foreach (string t in arrTemp)
                {
                    string sTrimed = t.Trim(new char[] {'(', ' '});
                    String[] arrData = sTrimed.Split('=');

                    if (arrData.Length == 2)
                    {
                        _dicData.Add(arrData[0], arrData[1]);
                    }
                }
            }
        }

        /// <summary>
        /// Key에 해당하는 값을 가져온다.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public string GetValue(String key)
        {
            try
            {
                return _dicData[key];
            }
            catch {}
            
            return null;
        }
    }
}
