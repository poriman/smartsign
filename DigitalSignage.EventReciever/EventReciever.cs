﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net.Sockets;
using System.ComponentModel;
using NLog;
using System.Net;
using System.IO;

namespace DigitalSignage.EventReciever
{
	/// <summary>
	/// 이벤트 Listener
	/// </summary>
	public class EventReciever
	{
		private static Logger logger = LogManager.GetCurrentClassLogger();

        private readonly object lockSequence = new object();

		public EventReciever()
		{
            try
            {
                isUDP = Properties.Settings.Default.Method.ToLower().Equals("udp");
            }
            catch { isUDP = false; }

            try
            {
                Port = Properties.Settings.Default.Port;
            }
            catch { Port = 8888; }
		}

        public EventReciever(string method, int port)
        {
            isUDP = method.ToLower().Equals("udp");
            Port = port;
        }

        public EventReciever(int port)
        {
            try
            {
                isUDP = Properties.Settings.Default.Method.ToLower().Equals("udp");
            }
            catch { isUDP = false; }

            Port = port;
        }

		/// <summary>
		/// 리스너 소켓
		/// </summary>
		Socket sockListener = null;

		/// <summary>
		/// 클라이언트 소켓
		/// </summary>
		Socket sockClient = null;

		/// <summary>
		/// UDP 관련 EndPoint
		/// </summary>
		EndPoint remoteEndPoint = new IPEndPoint(IPAddress.Any, 0);

		/// <summary>
		/// 운영 중인지
		/// </summary>
		bool isRunning = false;

        bool isPrepared = false;

		/// <summary>
		/// UDP 소켓인지
		/// </summary>
		bool isUDP = false;

        /// <summary>
        /// 포트 설정
        /// </summary>
        int Port = 8888;

		/// <summary>
		/// Recv 함수
		/// </summary>
		BackgroundWorker bwReceiver = null;

        /// <summary>
        /// 서버 소켓 관리 함수
        /// </summary>
        BackgroundWorker bwAccepter = null;

		/// <summary>
		/// 이벤트 발생
		/// </summary>
		public event RecieverEventHandler OnEventOccured;

		/// <summary>
		/// 시작
		/// </summary>
		public void Start()
		{
			Stop();

            if (bwAccepter == null)
            {
                bwAccepter = new BackgroundWorker();
                bwAccepter.WorkerSupportsCancellation = true;
                bwAccepter.DoWork += new DoWorkEventHandler(bwAccepter_DoWork);
            }

			if (bwReceiver == null)
			{
				bwReceiver = new BackgroundWorker();
				bwReceiver.WorkerSupportsCancellation = true;
				bwReceiver.DoWork += new DoWorkEventHandler(bwReceiver_DoWork);
			}

            isPrepared = MakeSocket() && ListenSocket();

            isRunning = true;

            bwAccepter.RunWorkerAsync();
            bwReceiver.RunWorkerAsync();
		}

        void bwAccepter_DoWork(object sender, DoWorkEventArgs e)
        {
            while (isRunning)
            {
                if (!isPrepared || sockListener == null || !sockListener.IsBound)
                {
                    isPrepared = MakeSocket() && ListenSocket();
                    System.Threading.Thread.Sleep(1000);
                    continue;
                }

				if (!isUDP)
				{
					try
					{
						sockClient = sockListener.Accept();
						logger.Debug("Socket Accept!!");
					}
					catch { }
				}
                System.Threading.Thread.Sleep(10);
            }

        }

		void bwReceiver_DoWork(object sender, DoWorkEventArgs e)
		{
			while (isRunning)
			{
				try
				{
					if (!isUDP)
					{
						#region TCP 경우
						if (sockClient != null && sockClient.Connected)
						{
							lock (lockSequence)
							{
								using (NetworkStream stream = new NetworkStream(sockClient))
								{
									if (stream.CanRead)
									{
										using (StreamReader sr = new StreamReader(stream))
										{
											try
											{
												String Message = String.Empty;
												while ((Message = sr.ReadLine()) != null)
												{
													//String Message = UTF8Encoding.UTF8.GetString(arrBytes);
													if (!String.IsNullOrEmpty(Message))
													{
														String[] arrSplit = Message.Split(new String[] { "##" }, StringSplitOptions.None);
														if (arrSplit.Length > 1)
														{
															int ModuleID = Convert.ToInt32(arrSplit[0]);
															int EventCode = Convert.ToInt32(arrSplit[1]);
															String Values = arrSplit.Length >= 3 ? arrSplit[2] : String.Empty;
															//logger.Info(String.Format("Event Occurred [{0}] : Code {1}, Values {2}", ModuleID, EventCode, Values));

															if (arrSplit.Length != 3) logger.Error("Event Length is not collect : " + arrSplit.Length);

															if (OnEventOccured != null) OnEventOccured(this, new RecieverEventArgs(ModuleID, EventCode, Values));
														}
														else
														{
															logger.Debug("Illigal Event Occurred: " + Message);
														}
													}
													else
													{
														logger.Debug("Client Disconnected!");
														sockClient.Disconnect(false);
													}
												}
											}
											catch (Exception ex) { logger.Error("Received Data Exception : " + ex.ToString()); }

											sr.Dispose();
										}
									}
									stream.Dispose();
								}
							}
						}
						#endregion
					}
					else
					{
						#region UDP 인 경우
						if (sockListener != null && sockListener.IsBound)
						{
							byte[] arrBytes = new byte[1024];
							int nRecved = 0;

							if (0 < (nRecved = sockListener.ReceiveFrom(arrBytes, ref remoteEndPoint)))
							{
								String Message = ASCIIEncoding.ASCII.GetString(arrBytes, 0, nRecved);

								String[] arrSplit = Message.Split(new String[] { "##" }, StringSplitOptions.None);
								if (arrSplit.Length > 1)
								{
									int ModuleID = Convert.ToInt32(arrSplit[0]);
									int EventCode = Convert.ToInt32(arrSplit[1]);
									String Values = arrSplit.Length >= 3 ? arrSplit[2] : String.Empty;
									//logger.Info(String.Format("Event Occurred [{0}] : Code {1}, Values {2}", ModuleID, EventCode, Values));

									if (arrSplit.Length != 3) logger.Error("Event Length is not collect : " + arrSplit.Length);

									if (OnEventOccured != null) OnEventOccured(this, new RecieverEventArgs(ModuleID, EventCode, Values));
								}

							}
						}
						#endregion
					}
				}
				catch (SocketException sockex)
				{
					if(sockex.SocketErrorCode != SocketError.TimedOut)
						logger.Error("Sock.Receive Exception : " + sockex.ToString());
				}
				catch (Exception ex2) { logger.Error("Sock.Receive Exception : " + ex2.ToString()); }

				System.Threading.Thread.Sleep(10);
			}
		
		}

		/// <summary>
		/// 메시지 송신
		/// </summary>
		/// <param name="nModuleID"></param>
		/// <param name="nEventCode"></param>
		/// <param name="Values"></param>
		/// <returns></returns>
		public bool SendMessage(int nModuleID, int nEventCode, string Values)
		{
			String Message = String.Format("{0}##{1}##{2}", nModuleID, nEventCode, Values);

			if (!isUDP)
			{
				using (NetworkStream stream = new NetworkStream(sockClient))
				{
					using (System.IO.StreamWriter sw = new System.IO.StreamWriter(stream))
					{
						sw.WriteLine(Message);
						sw.Flush();
						sw.Dispose();
					}
					stream.Dispose();
				}
			}
			else
			{
				sockListener.SendTo(ASCIIEncoding.UTF8.GetBytes(Message), remoteEndPoint);
			}
//			return 0 < sockClient.Send(UTF8Encoding.UTF8.GetBytes(Message));

			return true;
		}

        /// <summary>
        /// 에코 Send 메시지
        /// </summary>
        /// <param name="nModuleID"></param>
        /// <param name="nEventCode"></param>
        /// <param name="Values"></param>
        /// <returns></returns>
        public String SendAndWaitReturn(int nModuleID, int nEventCode, string Values)
        {
            String Message = String.Format("{0}##{1}##{2}", nModuleID, nEventCode, Values);
            lock (lockSequence)
            {
				if (!isUDP)
				{
					using (NetworkStream stream = new NetworkStream(sockClient))
					{
						using (System.IO.StreamWriter sw = new System.IO.StreamWriter(stream))
						{
							sw.WriteLine(Message);
							sw.Flush();
							sw.Dispose();
						}
						//logger.Info("Sent: "+ Message);

						System.Threading.Thread.Sleep(50);
						try
						{
							using (System.IO.StreamReader sr = new System.IO.StreamReader(stream))
							{
								Message = sr.ReadLine();
								sr.Dispose();
							}

							stream.Dispose();

							logger.Info("Recv: " + Message);

						}
						catch { Message = String.Empty; }
					}
				}
				else
				{
					sockListener.SendTo(ASCIIEncoding.UTF8.GetBytes(Message), remoteEndPoint);
				}
            }

            return Message;
        }

		/// <summary>
		/// 소켓 생성
		/// </summary>
		bool MakeSocket()
		{
			try
			{
				if (isUDP)
				{
					sockListener = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
                    //sockListener.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReceiveTimeout, 5000);
					//sockListener.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.SendTimeout, 2000);
				}
				else
				{
					sockListener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
					//sockListener.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReceiveTimeout, 5000);
					//sockListener.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.SendTimeout, 2000);
					//sockListener.SetSocketOption(SocketOptionLevel.Tcp, SocketOptionName.NoDelay, true);
				}

                return true;
			}
			catch (Exception ex)
			{
				logger.Error("Error: " + ex.ToString());
			}

            return false;
		}

		/// <summary>
		/// 소켓 대기
		/// </summary>
		bool ListenSocket()
		{
			try
			{
                IPAddress hostIP = IPAddress.Any;
                logger.Info("Prepare Listen: Port(" + this.Port + ")");
                IPEndPoint ep = new IPEndPoint(hostIP, this.Port);     
				sockListener.Bind(ep);     
				sockListener.Blocking = true;     // The server socket is working in blocking mode     
				if(!isUDP) sockListener.Listen(5);

                return true;
			}
			catch (Exception ex)
			{
				logger.Error("Error: " + ex.ToString());
			}

            return false;
		}

		/// <summary>
		/// 정지
		/// </summary>
		public void Stop()
		{
			isRunning = false;

			if (sockClient != null)
			{
				try
				{
					sockClient.Close();
					logger.Debug("Socket Close!!");
				}
				catch { }

				sockClient = null;
			}

			if (sockListener != null)
			{
				try
				{
					sockListener.Close();
					logger.Debug("Server Socket Close!!");
				}
				catch { }

				sockListener = null;
			}
            if (bwAccepter != null && bwAccepter.IsBusy)
            {
                try
                {
                    bwAccepter.CancelAsync();
                }
                catch { }
                try
                {
                    bwAccepter.Dispose();
                }
                catch { }

                bwAccepter = null;
            }
			
			if (bwReceiver != null && bwReceiver.IsBusy)
			{
				try
				{
					bwReceiver.CancelAsync();
				}
				catch { }
				try
				{
					bwReceiver.Dispose();
				}
				catch { }

				bwReceiver = null;
			}
		}
	}

	#region Event Handler Define
	/// <summary>
	/// 이벤트 리스너 핸들러
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="?"></param>
	public delegate void RecieverEventHandler(object sender, RecieverEventArgs e);

	/// <summary>
	/// 이벤트 리스너 Argument
	/// </summary>
	public class RecieverEventArgs : EventArgs
	{
		/// <summary>
		/// 모듈 ID
		/// </summary>
		public Int32 EventModuleID;
		/// <summary>
		/// 이벤트 코드
		/// </summary>
		public Int32 EventCode;
		/// <summary>
		/// 이벤트 전달 값
		/// </summary>
		public String EventValues;

		/// <summary>
		/// 생성자
		/// </summary>
		/// <param name="moduleID"></param>
		/// <param name="code"></param>
		/// <param name="values"></param>
		public RecieverEventArgs(Int32 moduleID, Int32 code, String values)
		{
			EventModuleID = moduleID;
			EventCode = code;
			EventValues = values;
		}
	}
	#endregion

}
