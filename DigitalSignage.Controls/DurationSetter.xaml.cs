﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Threading;

namespace DigitalSignage.Controls
{
    /// <summary>
    /// Interaction logic for DurationSetter.xaml
    /// </summary>
    public partial class DurationSetter : Window
    {
        TimeSpan ts;

        /// <summary>
        /// 생성자
        /// </summary>
        public DurationSetter()
        {
            InitializeComponent();
            secondsBox.PreviewTextInput += new TextCompositionEventHandler(box_0_to_59_PreviewTextInput);
            minutesBox.PreviewTextInput += new TextCompositionEventHandler(box_0_to_59_PreviewTextInput);
            hoursBox.PreviewTextInput += new TextCompositionEventHandler(box_0_to_23_PreviewTextInput);
            //secondsBox.TextInput += new TextCompositionEventHandler(secondsBox_TextInput);
        }

        /// <summary>
        /// 0~23 텍스트 입력 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void box_0_to_23_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            int value;

            if (Int32.TryParse(e.Text, out value))
            {
                if (value < 0) e.Handled = true;
                if (value > 23) e.Handled = true;
            }
            else e.Handled = true;
        }

        //void secondsBox_TextInput(object sender, TextCompositionEventArgs e)
        //{
            
        //}

        /// <summary>
        /// 0~59 텍스트 입력 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void box_0_to_59_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            int value;

            if (Int32.TryParse(e.Text, out value))
            {
                if (value < 0) e.Handled = true;
                if (value > 59) e.Handled = true;
            }
            else e.Handled = true;
        }

        /// <summary>
        /// 열기 함수
        /// </summary>
        /// <param name="timeSpan"></param>
        public void Load(TimeSpan timeSpan)
        {
            ts = timeSpan;
            previewDurationTextBox.Text = ts.ToString();
            daysBox.Text = ts.Days.ToString();
            hoursBox.Text = ts.Hours.ToString();
            minutesBox.Text = ts.Minutes.ToString();
            secondsBox.Text = ts.Seconds.ToString();
            this.ShowDialog();
        }

        /// <summary>
        /// 닫기 버튼 클릭 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void closeButton_Click(object sender, RoutedEventArgs e)
        {
            ts = new TimeSpan(Int32.Parse(daysBox.Text), Int32.Parse(hoursBox.Text), Int32.Parse(minutesBox.Text), Int32.Parse(secondsBox.Text));
            previewDurationTextBox.Text = ts.ToString();
            Close();
        }

        /// <summary>
        /// 계산 버튼 클릭 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void calculateButton_Click(object sender, RoutedEventArgs e)
        {
            ts = new TimeSpan(Int32.Parse(daysBox.Text), Int32.Parse(hoursBox.Text), Int32.Parse(minutesBox.Text), Int32.Parse(secondsBox.Text));
            previewDurationTextBox.Text = ts.ToString();
            daysBox.Text = ts.Days.ToString();
            hoursBox.Text = ts.Hours.ToString();
            minutesBox.Text = ts.Minutes.ToString();
            secondsBox.Text = ts.Seconds.ToString();
        }

        /// <summary>
        /// 시간 Duration 반환
        /// </summary>
        /// <returns></returns>
        public TimeSpan GetDuration()
        {
            if (ts != null)
                return ts;
            else return new TimeSpan();
        }
    }
}