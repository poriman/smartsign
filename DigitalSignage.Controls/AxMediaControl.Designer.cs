﻿namespace DigitalSignage.Controls
{
    /// <summary>
    /// DirectShow 기반 미디어 컨트롤
    /// </summary>
	partial class AxMediaControl
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.SuspendLayout();
            // 
            // AxMediaControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.DoubleBuffered = true;
            this.Name = "AxMediaControl";
            this.Load += new System.EventHandler(this.AxMediaControl_Loaded);
            this.SizeChanged += new System.EventHandler(this.AxMediaControl_SizeChanged);
            this.ResumeLayout(false);

		}

		#endregion
	}
}
