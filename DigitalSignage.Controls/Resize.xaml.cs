﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DigitalSignage.Controls
{
    /// <summary>
    /// 사이즈 변경 컨트롤
    /// </summary>
    public partial class Resize : UserControl
    {
        /// <summary>
        /// 컨트롤 사이즈 변경
        /// </summary>
        public Resize()
        {
            InitializeComponent();
        }

        /// <summary>
        /// content element property
        /// </summary>
        public UIElement ContentElement
        {
            get
            {
                if (content.Children.Count > 0)
                    return content.Children[0];
                else
                    return null;
            }
            set
            {
                content.Children.Clear();
                content.Children.Add(value);
            }
        }

        /// <summary>
        /// property for X scale value
        /// </summary>
        public double ScaleX
        {
            get { return scale.ScaleX; }
            set { scale.ScaleX = value; }
        }

        /// <summary>
        /// property for Y scale value
        /// </summary>
        public double ScaleY
        {
            get { return scale.ScaleY; }
            set { scale.ScaleY = value; }
        }
    }
}
