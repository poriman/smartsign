﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;


using DigitalSignage.Common;

namespace DigitalSignage.Controls
{
    /// <summary>
    /// Interaction logic for TimeDiagram
    /// </summary>
    public partial class TimeDiagram : UserControl
    {
        private DateTime day;
        private ArrayList tasksList;
        private double hourLength;
		private DigitalSignage.Controls.TaskItem prevSelection = null;
		private long zoomStart;
        private long zoomEnd;

        #region Events
        public delegate void SelectionChanged(object sender, Task newSelection);
        public event SelectionChanged OnSelectionChanged = null;

		public delegate void CallContextMenu(object sender, MouseButtonEventArgs e);
		public event CallContextMenu OnContextMenu = null;

        #endregion

        public TimeDiagram()
        {
            InitializeComponent();
        }

        private void buildUI()
        {
            mainCanvas.Children.Clear(); // !!!!! this should be optimized

            // 1. draw hour lines
            hourLength = (ActualWidth - 100) / 26;
            int lstep = 1;
            if (hourLength < 36) lstep = 2;
            if (hourLength < 18) lstep = 4;
            for (int i = 0; i < 25; i = i + lstep)
            {
                // line
                Line line = new Line();
                line.X1 = (hourLength / 2) + i * hourLength;
                line.X2 = line.X1;
                line.Y1 = 25;
                line.Y2 = ActualHeight - 25;
                line.Stroke = Brushes.Gray;
                line.StrokeThickness = 1.0;
                mainCanvas.Children.Add(line);
                // header
                TextBlock header = new TextBlock();
                header.Foreground = Brushes.White;
                header.Text = (i < 10 ? "0" + i : i + "") + ":00";
                Canvas.SetLeft(header, line.X1 - 20);
                Canvas.SetTop(header, 3);
                mainCanvas.Children.Add(header);
            }

			Line vline = new Line();
			vline.X1 = hourLength / 2;
			vline.X2 = vline.X1 + 24 * hourLength;
			Double dY = edVideoTask.ActualHeight + 10.0;
			vline.Y1 = vline.Y2 = dY > 60.0 ? dY : 60.0;
			vline.Stroke = Brushes.Gray;
			vline.StrokeThickness = 1.0;
			mainCanvas.Children.Add(vline);

            drawTasks(tasksList);

        }

        private void drawTasks(ArrayList tasks)
        {
            if (tasks == null) return;
            DateTime cDay = new DateTime(day.Year, day.Month, day.Day, 0, 0, 0);
            long daystart = TimeConverter.ConvertToUTP(cDay.ToUniversalTime());
            cDay = new DateTime(day.Year, day.Month, day.Day, 23, 59, 59);
            long dayend = TimeConverter.ConvertToUTP(cDay.ToUniversalTime());
            for (int i = 0; i < tasks.Count; i++)
            {
                Task task = tasks[i] as Task;
                // checking
                if ((task.TaskEnd < daystart) || (task.TaskStart > dayend)) continue;

				DigitalSignage.Controls.TaskItem rect = null;
				double dTaskHeight = 0.0;

                if (task.type == TaskCommands.CmdScreen)
                {
					dTaskHeight = edVideoTask.ActualHeight / 4.0;
					rect = new DigitalSignage.Controls.TaskItem();
					
 					Canvas.SetTop(rect, task.pid == "-1" ? 25 : 25 + dTaskHeight * 2);
					
                }
                else if (task.type == TaskCommands.CmdSubtitles)
                {
					dTaskHeight = edSubtileTask.ActualHeight / 4.0;
					rect = new DigitalSignage.Controls.TaskItem();

					int nTop = 25 + Convert.ToInt32(edVideoTask.ActualHeight);
					Canvas.SetTop(rect, task.pid == "-1" ? nTop : nTop + dTaskHeight * 2);
                }

				rect.Height = dTaskHeight;

				if (SelectedTask != null && task.uuid == SelectedTask.uuid)
				{
					prevSelection = rect;
					rect.Background = Brushes.White;
				}
				else
					rect.Background = task.pid == "-1" ? Brushes.Tomato : Brushes.RoyalBlue;

				rect.Border = Brushes.White;
				rect.Text = task.description;

				if (task.TaskStart < daystart) task.TaskStart = daystart - 900;
				if (task.TaskEnd > dayend) task.TaskEnd = dayend + 900;

				rect.Width = (task.TaskEnd - task.TaskStart) * hourLength / 3600;
				double left = (hourLength / 2) + (task.TaskStart - daystart) * hourLength / 3600;
				Canvas.SetLeft(rect, left);

				//	Task 데이터를 저장하고 Tooltip을 내부적으로 생성한다
				rect.DataContextWithMakeToolTip = task;
				rect.MouseLeftButtonUp += new MouseButtonEventHandler(rectMouseEnter);
				rect.MouseRightButtonDown += new MouseButtonEventHandler(rectMouseEnter);
				mainCanvas.Children.Add(rect);
            }
        }

        private void mainCanvas_Loaded(object sender, RoutedEventArgs e)
        {
            UpdateLayout();
            buildUI();
        }

        public void Rebuild()
        {
            buildUI();
            if (prevSelection != null)
            {
				Task selected = prevSelection.DataContext as Task;
				prevSelection.Background = selected.pid == "-1" ? Brushes.Tomato : Brushes.RoyalBlue;
				
				prevSelection = null;

                if (OnSelectionChanged != null)
                    OnSelectionChanged(this, null);
            }
        }

        #region events handlers
        private void rectMouseEnter(object sender, MouseButtonEventArgs args)
        {
			DigitalSignage.Controls.TaskItem rect = sender as DigitalSignage.Controls.TaskItem;
            if (rect == null) return;
            if (prevSelection != null)
            {
				Task curr = (Task)prevSelection.DataContext;
				prevSelection.Background = curr.pid == "-1" ? Brushes.Tomato : Brushes.RoyalBlue;

            }
            rect.Background = Brushes.White;
            prevSelection = rect;
            if (OnSelectionChanged != null)
                OnSelectionChanged(this, (Task)rect.DataContext);
        }
            
        private void rectMouseLeave(object sender, MouseEventArgs args)
        {
			DigitalSignage.Controls.TaskItem rect = sender as DigitalSignage.Controls.TaskItem;
            if (rect == null) return;
            if (prevSelection != null)
                if (prevSelection != null)
                {
					Task curr = (Task)prevSelection.DataContext;
					prevSelection.Background = curr.pid == "-1" ? Brushes.Tomato : Brushes.RoyalBlue;

                }

            prevSelection = null;

            if (OnSelectionChanged != null)
                OnSelectionChanged(this, null);
        }

        private void mainCanvas_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            buildUI();
        }


		private void TimeDiagram_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
		{
			if (OnContextMenu != null) OnContextMenu(sender, e);
		}

        #endregion

        #region properties
        public DateTime Day
        {
            get { return day; }
            set { day = value; }
        }

        public ArrayList Tasks
        {
            get { return tasksList; }
            set { tasksList = value; }
        }

        public Task SelectedTask
        {
            get
            {
                if (prevSelection == null)
                    return null;
                return (Task)prevSelection.DataContext;
            }
        }
        #endregion


    }
}