﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Media.Effects;

using QuartzTypeLib;

namespace DigitalSignage.Controls
{
    /// <summary>
    /// Interaction logic for cm_ContentItem.xaml
    /// </summary>
    public partial class PlayListGroup : TreeViewItem
    {
        /// <summary>
        /// Creates a new playlist item
        /// </summary>
        /// <param name="index">Number (index) of item that will be displayed on the current item</param>
        /// <param name="contents">A contents object that will be displayed on the current item</param>
        /// <param name="duration">A duration of appearance of item`s content</param>
        /// <param name="element">An UIElement object that playlist is belong to</param>
        public PlayListGroup( TimeSpan duration )
        {
            InitializeComponent();
            durationUI.SelectedHour = duration.Hours;
            durationUI.SelectedMinute = duration.Minutes;
            durationUI.SelectedSecond = duration.Seconds;
        }
   }
}