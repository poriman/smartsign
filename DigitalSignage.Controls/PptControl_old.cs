﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Diagnostics;

namespace DigitalSignage.Controls
{
    public partial class PPTControl : UserControl
    {
        /// <summary>
        /// Initializes the new PPTControl class instance
        /// </summary>
        public PPTControl()
        {
            InitializeComponent();
            this.Disposed += new EventHandler(PPTControl_Disposed);
			viewer.BackColor = Color.White;
		}

        void PPTControl_Disposed(object sender, EventArgs e)
		{
			Stop();
		}

		public void CheatDispose()
		{
			this.Controls.Clear();
			this.Dispose();

		}

        /// <summary>
        /// Plays the specified PPT presentation file
        /// </summary>
        /// <param name="source">Full PPT file path</param>
        public void Play(string source)
        {
			if (viewer != null && !viewer.IsDisposed)
			{
				try
				{
					ResizeViewer();
					viewer.SlideShowOpenAndPlay(source, false, false, true, false);
	// 				viewer.Open(source);
	// 				viewer.SlideShowPlay(false, false, true, false);
					viewer.Visible = true;
					ResizeViewer();
				}
				catch/* (System.Exception e)*/
				{
					viewer.Dispose();
					viewer = null;
				}
				
			}
		}

        /// <summary>
        /// Stops the presentation playback
        /// </summary>
        public void Stop()
        {
			try
			{
				if (viewer != null && !viewer.IsDisposed)
				{
					viewer.Visible = false;
					if (viewer.IsOpened)
					{
						// 					viewer.SlideShowExit();
						viewer.Close();
					}
				}
			}
			catch (System.Exception e)
			{
				
			}
       }
		public System.Drawing.Bitmap Thumbnail(int cx, int cy)
		{
			try
			{
				Bitmap bmp = new Bitmap(cx, cy, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
				using (Graphics g = Graphics.FromImage(bmp))
				{
					Point pos = this.PointToScreen(new Point(0, 0));
					g.CopyFromScreen(pos, new Point(0, 0), this.Size, CopyPixelOperation.SourceCopy);
				}

				return bmp;
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}

			return null;

		}

		private void ResizeViewer()
		{
			if (viewer != null)
			{
				viewer.Width = this.Width;
				viewer.Height = this.Height;
				viewer.Left = 0;
				viewer.Top = 0;
			}
		}
		private void PPTControl_SizeChanged(object sender, EventArgs e)
		{
			try
			{
				ResizeViewer();
			}
			catch 
			{
				
			}
		}
    }
}