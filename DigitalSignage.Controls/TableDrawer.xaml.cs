﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Media.Effects;

namespace DigitalSignage.Controls
{
    /// <summary>
    /// Interaction logic Table drawer
    /// </summary>
    public partial class TableDrawer : Grid
    {
        private static Color unselectedPane = Color.FromRgb(110, 160, 176);
        private static Color selectedPane = Color.FromRgb(183, 156, 190);
        private Brush unselectedPaneBrush = new SolidColorBrush(unselectedPane);
        private Brush selectedPaneBrush = new SolidColorBrush(selectedPane);
        private ArrayList selectedItems = new ArrayList();
        private XmlDocument document = new XmlDocument();

		private double dScaleRate = 1;
		private bool bAssociated = false;

        /// <summary>
        /// 
        /// </summary>
		public bool IsAssociated
		{
			get { return bAssociated; }
			set { 
				bAssociated = value;
				ResizeMargin();
			}
		}
        /// <summary>
        /// 
        /// </summary>
		public double ScaleRate
		{
			get { return dScaleRate; }
			set { 
				dScaleRate = value;
				ResizeMargin();
			}
		}
		private void Resize()
		{
			double height = 0;
			double width = 0;

			double realWidth = this.ActualWidth + Margin.Left + Margin.Right;
			double realHeight = this.ActualHeight + Margin.Top + Margin.Bottom;

			width = realWidth;
			height = width / dScaleRate;

			if (realHeight < height)
			{
				width = realHeight * dScaleRate;
				height = realHeight;
			}

			double marginHeight = (realHeight - height) / 2;
			double marginWidth = (realWidth - width) / 2;

			this.Margin = new Thickness(marginWidth, marginHeight, marginWidth, marginHeight);
			
		}
		private void ResizeMargin()
		{
			if(bAssociated)
			{

				if (dScaleRate == 0)
					return;

				else if (dScaleRate > 1)
				{
					Resize();
				}
				else
				{
					Resize();
				}
			}
			else 
			{
				this.Margin = new Thickness(0);
			}
		}

        /// <summary>
        /// Creates a new TableDrawer item
        /// </summary>
        public TableDrawer()
        {
            InitializeComponent();
        }

        #region Draw functions
        private void rDraw(Grid parent, XmlNode node, int row, int column )
        {
            if (node.Name.Equals("pane"))
            {
                // this is pane element
                Border item = new Border();
                item.BorderBrush = Brushes.White;
                item.BorderThickness = new Thickness(0.75);
                item.Background = unselectedPaneBrush;
                item.DataContext = node;
                item.Margin = new Thickness(9);
                item.MouseUp += new MouseButtonEventHandler(item_MouseUp);
                item.ClipToBounds = true;
                parent.Children.Add( item );
                Grid.SetColumn( item, column );
                Grid.SetRow(item, row);
                item.UpdateLayout();

                if (node.Attributes["playlist"] != null)
                {
					try
					{
						String[] arrFileName = System.IO.Path.GetFileName(node.Attributes["playlist"].Value).Split('.');
						int fontSize = (int)item.ActualWidth / (arrFileName[0].Length + 2);
						Label label = new Label();
						label.Content = arrFileName[0];
						if (fontSize != 0 && item.ActualHeight - 10 > 0)
							label.FontSize = fontSize > item.ActualHeight - 10 ? item.ActualHeight - 10 : fontSize;
						else
							label.FontSize = 8;
						label.HorizontalAlignment
							= HorizontalAlignment.Center;
						label.VerticalAlignment = VerticalAlignment.Center;
						label.Foreground = Brushes.White;
						DropShadowBitmapEffect effect = new DropShadowBitmapEffect();
						effect.Color = Colors.Black;
						effect.Direction = 320;
						effect.ShadowDepth = 3;
						effect.Softness = 0.3;
						effect.Opacity = 0.5;
						label.BitmapEffect = effect;
						item.Child = label;
					}
					catch (Exception e)
					{
						MessageBox.Show(e.Message);
					}

                }
            }
            if ( node.Name.Equals("grid"))
            {
                // this is Grid container
                Grid item = new Grid();
                DropShadowBitmapEffect effect = new DropShadowBitmapEffect();
                effect.Color = Colors.Black;
                effect.Direction = 320;
                effect.ShadowDepth = 3;
                effect.Softness = 0.3;
                effect.Opacity = 0.5;
                item.BitmapEffect = effect;
                item.DataContext = node;
                item.Background = Brushes.Gray;
                item.Margin = new Thickness(5);
                item.MouseUp += new MouseButtonEventHandler(group_MouseUp);
                parent.Children.Add(item);
                Grid.SetColumn(item, column);
                Grid.SetRow(item, row);
                item.UpdateLayout();


                int rc = Int32.Parse(node.Attributes["rows"].Value);
                int cc = Int32.Parse(node.Attributes["columns"].Value);
                string[] sizes = null;
                if (node.Attributes["rowsizes"] != null)
                    sizes = node.Attributes["rowsizes"].Value.Split(',');

                for (int i = 0; i < rc + (rc - 1); i++)
                {
                    RowDefinition def = new RowDefinition();
                    if ((i % 2) == 1)
                        def.Height = new GridLength(3);
                    else
                        if (sizes != null)
                            def.Height = //new GridLength(Double.Parse( sizes[i / 2])/100*item.ActualHeight);
                                new GridLength(Double.Parse(sizes[i / 2]) / 100 * item.ActualHeight, GridUnitType.Star);
                    item.RowDefinitions.Add(def);
                }

                sizes = null;
                if (node.Attributes["colsizes"] != null)
                    sizes = node.Attributes["colsizes"].Value.Split(',');
                for (int i = 0; i < cc + (cc - 1); i++)
                {
                    ColumnDefinition def = new ColumnDefinition();
                    if ( (i % 2)==1)
                        def.Width = new GridLength(3);
                    else
                        if ( sizes!=null)
                            def.Width = new GridLength(Double.Parse(sizes[i / 2]) / 100 * item.ActualWidth, GridUnitType.Star);
                    item.ColumnDefinitions.Add(def);
                }
                // creating row splitters
                for (int i = 0; i < rc - 1; i++)
                    for (int j = 0; j < cc; j++)
                    {
                        GridSplitter splitter = new GridSplitter();
						splitter.DragDelta += new System.Windows.Controls.Primitives.DragDeltaEventHandler(splitter_DragDelta);
						splitter.PreviewKeyUp += new KeyEventHandler(splitter_KeyUp);
                        splitter.BorderThickness = new Thickness(1);
                        splitter.BorderBrush = Brushes.WhiteSmoke;
                        splitter.HorizontalAlignment = HorizontalAlignment.Stretch;
                        splitter.VerticalAlignment = VerticalAlignment.Stretch;
                        splitter.Margin = new Thickness(0);
                        splitter.Height = 3;
                        splitter.Width = Double.NaN;
                        item.Children.Add(splitter);
                        Grid.SetRow(splitter, i * 2 + 1);
                        Grid.SetColumn(splitter, j * 2);
                    }

                // creating column splitters
                for (int i = 0; i < cc - 1; i++)
                    for (int j = 0; j < rc; j++)
                    {
                        GridSplitter splitter = new GridSplitter();
						splitter.DragDelta += new System.Windows.Controls.Primitives.DragDeltaEventHandler(splitter_DragDelta);
						splitter.PreviewKeyUp += new KeyEventHandler(splitter_KeyUp);
						splitter.BorderThickness = new Thickness(1);
                        splitter.BorderBrush = Brushes.WhiteSmoke;
                        splitter.HorizontalAlignment = HorizontalAlignment.Stretch;
                        splitter.VerticalAlignment = VerticalAlignment.Stretch;
                        splitter.Margin = new Thickness(0);
                        splitter.Height = Double.NaN;
                        splitter.Width = 3;
                        item.Children.Add(splitter);
                        Grid.SetColumn(splitter, i * 2 + 1);
                        Grid.SetRow(splitter, j * 2);
                    }

                for (int i = 0; i < node.ChildNodes.Count; i++)
                {
                    rDraw(item, node.ChildNodes[i], (i / cc)*2, (i % cc)*2 );
                }
            }
            return;
        }

		void splitter_KeyUp(object sender, KeyEventArgs e)
		{
			if (null != GridSplitterDragDelta)
				GridSplitterDragDelta(sender, null);
		}

		void splitter_DragDelta(object sender, System.Windows.Controls.Primitives.DragDeltaEventArgs e)
		{
			if (null != GridSplitterDragDelta)
				GridSplitterDragDelta(sender, e);
		}

        private void draw()
        {
            dataUI.Children.Clear();
            //drawing elements
            if (document.ChildNodes.Count > 0)
            {
                if ( document.DocumentElement.Name.Equals("program"))
                    rDraw(dataUI, document.DocumentElement.ChildNodes[0], 0, 0);
            }
        }
        #endregion

        #region Events
        public delegate void SelectionChangedEvent(object sender, ArrayList selectedItems );
        public event SelectionChangedEvent SelectedItemChanged = null;

		public delegate void GridSplitterDragDeltaEvent(object sender, System.Windows.Controls.Primitives.DragDeltaEventArgs e);
		public event GridSplitterDragDeltaEvent GridSplitterDragDelta = null;

        public delegate void DropEvent(object sender, DragEventArgs e);
        public event DropEvent DropPI = null;
        #endregion

        #region properties
        public ArrayList SelectedItems
        {
            get { return selectedItems; }
        }

        public XmlDocument Document
        {
            get { return document; }
            set
            {
                document = value;
                selectedItems.Clear();
                draw();
                if (SelectedItemChanged != null)
                    SelectedItemChanged(this, selectedItems);
            }
        }
        #endregion

        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                /*if (MouseDownPI != null)
                    MouseDownPI(this, e );*/
            }
            catch (Exception err)
            {
                string errmsg = err.Message.ToString();
            }
        }

        private void Grid_Drop(object sender, DragEventArgs e)
        {
            try
            {
                if ( DropPI != null)
                    DropPI(this, e);
                e.Handled = true;
            }
            catch (Exception err)
            {
                string errmsg = err.Message.ToString();
            }
        }

        private void canvas_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                draw();
            }
            catch (Exception err)
            {
                string errmsg = err.Message.ToString();
            }
        }

        void item_MouseUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
				
				if(!Keyboard.IsKeyDown(Key.LeftCtrl))
	                unselectAll();
    
				Border newitem = sender as Border;
                if ( newitem == null ) return;
                newitem.Background = selectedPaneBrush;
                selectedItems.Add(newitem);
                if (SelectedItemChanged != null)
                    SelectedItemChanged(this, selectedItems);
                e.Handled = true;
            }
            catch (Exception err)
            {
                string errmsg = err.Message.ToString();
            }
        }

        private void Grid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                unselectAll(); 
                if (SelectedItemChanged != null)
                    SelectedItemChanged(this, selectedItems);
                e.Handled = true;
            }
            catch (Exception err)
            {
                string errmsg = err.Message.ToString();
            }
        }


        void group_MouseUp(object sender, MouseButtonEventArgs e)
        {
            try
            {
                unselectAll();
                Grid newitem = sender as Grid;
                if (newitem == null) return;
                newitem.Background = Brushes.White;
                selectedItems.Add(newitem);
                if (SelectedItemChanged != null)
                    SelectedItemChanged(this, selectedItems);
                e.Handled = true;
            }
            catch (Exception err)
            {
                string errmsg = err.Message.ToString();
            }
        }

        private void unselectAll()
        {
            if (selectedItems.Count > 0)
            {
                //unselect them
                for (int i = 0; i < selectedItems.Count; i++)
                {
                    if (selectedItems[i].GetType().Equals(typeof(Grid)))
                        ((Grid)selectedItems[i]).Background = Brushes.Gray;
                    if (selectedItems[i].GetType().Equals(typeof(Border)))
                        ((Border)selectedItems[i]).Background = unselectedPaneBrush;
                }
                selectedItems.Clear();
            }
        }

        private void rSaveSizes(Grid item)
        {
            XmlNode node = item.DataContext as XmlNode;
            
            string sizes = "";
            int adh = 3 * (item.ColumnDefinitions.Count / 2);
            for ( int i = 0; i < item.ColumnDefinitions.Count/2+1; i ++ )
                sizes += (int)(Math.Round(item.ColumnDefinitions[i*2].ActualWidth/(item.ActualWidth-adh)*100))+",";
            XmlAttribute atr = node.Attributes["colsizes"];
            if (atr == null)
            {
                atr = document.CreateAttribute("colsizes");
                node.Attributes.Append(atr);
            }
            atr.Value = sizes;

            sizes = "";
            adh = 3 * (item.RowDefinitions.Count / 2);
            for (int i = 0; i < item.RowDefinitions.Count/2+1; i++)
                sizes += (int)(Math.Round(item.RowDefinitions[i*2].ActualHeight/(item.ActualHeight-adh)*100)) + ",";
            atr = node.Attributes["rowsizes"];
            if (atr == null)
            {
                atr = document.CreateAttribute("rowsizes");
                node.Attributes.Append(atr);
            }
            atr.Value = sizes;
            for (int i = 0; i < item.Children.Count; i++)
            {
                Grid subnode = item.Children[i] as Grid;
                if (subnode != null)
                    rSaveSizes(subnode);
            }
        }

        public void SaveSizes()
        {
            if (dataUI.Children.Count < 1) return;
            Grid item = dataUI.Children[0] as Grid;
            if (item == null) return;
            rSaveSizes(item);
        }

		public void UpdateSize(FrameworkElement item, Size percent)
		{
			throw new NotImplementedException();
		}

		public Size ActualSize(FrameworkElement item)
		{
			Size actualSize = new Size(0,0);

			FindActualSize(dataUI, item, ref actualSize);

			return actualSize;
		}

		private bool FindActualSize(FrameworkElement parent, FrameworkElement dest, ref Size size)
		{
			bool bRet = false;

			Grid grid = parent as Grid;
			if(grid != null)
			{
				foreach(FrameworkElement item in grid.Children)
				{
					if(true == (bRet = FindActualSize(item, dest, ref size)))
					{
						int col = Grid.GetColumn(item);
						int row = Grid.GetRow(item);

						int colsplittersize = 3 * (grid.ColumnDefinitions.Count / 2);
						int rowsplittersize = 3 * (grid.RowDefinitions.Count / 2);

						if(grid.ColumnDefinitions.Count > 0)
						{
							double dwidth = grid.ColumnDefinitions[col].ActualWidth / (grid.ActualWidth - colsplittersize) * 100;
							size.Width = size.Width == 0 ? dwidth : size.Width * dwidth / 100;
						}

						if (grid.RowDefinitions.Count > 0)
						{
							double dheight = grid.RowDefinitions[row].ActualHeight / (grid.ActualHeight - rowsplittersize) * 100;
							size.Height = size.Height == 0 ? dheight : size.Height * dheight / 100;
						}

						return true;
					}
				}
			}

			Border border = parent as Border;
			
			if(border != null)
			{
				if (border.Equals(dest))
				{
					bRet = true;
				}
			}
			return bRet;

		}

		private void Grid_SizeChanged(object sender, SizeChangedEventArgs e)
		{
			ResizeMargin();
		}

    }
}