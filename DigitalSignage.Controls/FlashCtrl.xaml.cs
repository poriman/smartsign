﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DigitalSignage.Controls
{
    /// <summary>
    /// FlashCtrl.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class FlashCtrl : UserControl
    {
        AxShockwaveFlashObjects.AxShockwaveFlash viewer;

        /// <summary>
        /// 생성자
        /// </summary>
        public FlashCtrl()
        {
            this.viewer = new AxShockwaveFlashObjects.AxShockwaveFlash();
            this.viewer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.viewer.Enabled = true;
            this.viewer.Location = new System.Drawing.Point(0, 0);
            this.viewer.Name = "viewer";
            this.viewer.Size = new System.Drawing.Size(150, 150);
            this.viewer.TabIndex = 0;

            InitializeComponent();

            Host.Child = viewer;
        }

        /// <summary>
        /// Plays the loaded file
        /// </summary>
        public void Play(string source)
        {
            try
            {
                viewer.Movie = source;
                viewer.Play();
                Host.Visibility = Visibility.Visible;
            }
            catch (Exception ex)
            {
                string errmsg = ex.Message.ToString();
            }
        }

        /// <summary>
        /// Stops playing
        /// </summary>
        public void Stop()
        {
            Host.Visibility = Visibility.Hidden;
            viewer.StopPlay();
            viewer.Movie = "";
        }
    }
}
