﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Windows.Media.Animation;
using DigitalSignage.Common;

namespace DigitalSignage.Controls
{
    /// <summary>
    /// Interaction logic for Clock.xaml
    /// </summary>
    public partial class Clock : Grid, IDecoratable, IShape
    {
        private DispatcherTimer dt;

        #region GMT property defining
        /// <summary>
        /// GMT 관련 의존 프로퍼티
        /// </summary>
        public static readonly DependencyProperty GMTProperty = DependencyProperty.Register("GMT", typeof(int), typeof(Clock), new PropertyMetadata(GMTPropertyValueChanged));
        
        /// <summary>
        /// GMT를 가져오거나 설정합니다.
        /// </summary>
        public int GMT
        {
            get
            {
                return (int)GetValue(GMTProperty);
            }

            set
            {
                SetValue(GMTProperty, value);
            }
        }
        /// <summary>
        /// GMT 값 변경 이벤트
        /// </summary>
        /// <param name="d"></param>
        /// <param name="e"></param>
        private static void GMTPropertyValueChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            Clock dc = (Clock)d;
            dc.GMT = (int)e.NewValue;
        } 
        #endregion

        /// <summary>
        /// 시간대 정보
        /// </summary>
        public TimeZoneInfo gmt;

        /// <summary>
        /// 시, 분, 초침 정보
        /// </summary>
        public Path hHand, mHand, sHand;

        /// <summary>
        /// 가운데 구
        /// </summary>
        public Ellipse centerCircle;

        private bool _StrokesVisible;

        /// <summary>
        /// /
        /// </summary>
        public Rectangle[] FaceStrokes
        {
            get
            {
                return (Rectangle[])strokeCanvas.Children.OfType<Rectangle>().ToArray();
            }
        }

        bool _Rectangular;

        /// <summary>
        /// 
        /// </summary>
        public bool Rectangular
        {
            get
            {
                return _Rectangular;
            }
            set
            {
                _Rectangular = value;
                if (_Rectangular)
                {
                    clockBg.RadiusX = 0;
                    clockBg.RadiusY = 0;
                }

                else
                {
                    clockBg.RadiusX = 150;
                    clockBg.RadiusY = 150;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool StrokesVisible
        {
            get
            {
                return _StrokesVisible;
            }

            set
            {
                _StrokesVisible = value;
                if (_StrokesVisible)
                {
                    foreach (Rectangle stroke in strokeCanvas.Children)
                    {
                        strokeCanvas.Visibility = Visibility.Visible;
                    }
                }
                else
                {
                    foreach (Rectangle stroke in strokeCanvas.Children)
                    {
                        strokeCanvas.Visibility = Visibility.Hidden;
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Rectangle GetClockBg()
        {
            return clockBg;
        }

        /// <summary>
        /// 생성자
        /// </summary>
        public Clock()
        {
            InitializeComponent();
            _StrokesVisible = true;
            _Rectangular = false;
            // Insert code required on object creation below this point.
            dt = new DispatcherTimer();
            dt.Interval = new TimeSpan(0, 0, 1);
            dt.Tick += new EventHandler(dt_Tick);
            gmt = TimeZoneInfo.Local;
            // set the initial angle values so hands aren't all pointing up when we start
            SetTimeAngles();

            hHand = hourArrow;
            mHand = minuteArrow;
            sHand = secondArrow;
            centerCircle = circle;
        }

        /// <summary>
        /// 재생
        /// </summary>
        public void Start()
        {
            dt.Start();
        }

        /// <summary>
        /// 정지
        /// </summary>
        public void Stop()
        {
            dt.Stop();
        }

        /// <summary>
        /// Ticker
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void dt_Tick(object sender, EventArgs e)
        {
            SetTimeAngles();
        }

        /// <summary>
        /// /
        /// </summary>
        public void SetTimeAngles()
        {
            int MinuteAngle = (int)DateTime.Now.Minute * 6;
            int DivMinute = MinuteAngle / 30;
            double FactorMinute = DivMinute * 2.5;
            this.Resources["dHourAsAngle"] = ((double)(DateTime.Now.Hour - TimeZoneInfo.Local.BaseUtcOffset.Hours + gmt.BaseUtcOffset.Hours) * 30) + FactorMinute;
            this.Resources["dMinuteAsAngle"] = (double)(DateTime.Now.Minute - TimeZoneInfo.Local.BaseUtcOffset.Minutes + gmt.BaseUtcOffset.Minutes) * 6;
            this.Resources["dSecondAsAngle"] = (double)DateTime.Now.Second * 6;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="borderbrush"></param>
        public void SetBorderBrush(Brush borderbrush)
        {

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="borderThickness"></param>
        public void SetBorderThickness(Thickness borderThickness)
        {
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="background"></param>
        public void SetBackround(Brush background)
        {
            clockBg.Fill = background;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cornerRadius"></param>
        public void SetCornerRadius(double cornerRadius)
        {

        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Brush GetBorderBrush()
        {
            return null;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Brush GetBackgound()
        {
            return clockBg.Fill;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Thickness GetBorderThickness()
        {
            return new Thickness(0);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public double GetCornerRadius()
        {
            return 0;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="brush"></param>
        public void SetStroke(Brush brush)
        {
            clockBg.Stroke = brush;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="brush"></param>
        public void SetFill(Brush brush)
        {
            clockBg.Fill = brush;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sdarray"></param>
        public void SetStrokeDashArray(int[] sdarray)
        {
            clockBg.StrokeDashArray.Clear();
            for (int i = 0; i < sdarray.Count(); i++)
                clockBg.StrokeDashArray.Add(sdarray[i]);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="strokedashcap"></param>
        public void SetStrokeDashCap(string strokedashcap)
        {
            switch (strokedashcap)
            {
                case "Flat":
                    {
                        clockBg.StrokeDashCap = PenLineCap.Flat;
                        break;
                    }
                case "Round":
                    {
                        
                        clockBg.StrokeDashCap = PenLineCap.Round;
                        break;
                    }
                case "Triangle":
                    {
                        clockBg.StrokeDashCap = PenLineCap.Triangle;
                        break;
                    }
                case "Square":
                    {
                        clockBg.StrokeDashCap = PenLineCap.Square;
                        break;
                    }
                default: break;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="strokethickness"></param>
        public void SetStrokeThickness(double strokethickness)
        {
            clockBg.StrokeThickness = strokethickness;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Brush GetStroke()
        {
            return clockBg.Stroke;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Brush GetFill()
        {
            return clockBg.Fill;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public int[] GetStrokeDashArray()
        {
            int[] sdarray = new int[clockBg.StrokeDashArray.Count];
            int i = 0;
            foreach (double d in clockBg.StrokeDashArray)
            {
                sdarray[i] = Convert.ToInt32(d);
                i++;
            }
            return sdarray;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string GetStrokeDashCap()
        {
            return clockBg.StrokeDashCap.ToString();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public double GetStrokeThickness()
        {
            return clockBg.StrokeThickness;
        }
    }
}