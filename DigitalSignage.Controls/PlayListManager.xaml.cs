﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Controls.Primitives;

namespace DigitalSignage.Controls
{
    /// <summary>
    /// Allows editing of element`s playlist
    /// </summary>
    public partial class PlayListManager : Window
    {
        cm_ContentItem playlistitem, currentitem;
        /// <summary>
        /// 재생 목록 파일들
        /// </summary>
        public ArrayList PlayListFiles;
        /// <summary>
        /// 재생 목록 타이밍
        /// </summary>
        public ArrayList PlayListTimings;

        /// <summary>
        /// Public constructor for Playlist Manager window
        /// </summary>
        public PlayListManager()
        {
            InitializeComponent();
            PlayListFiles = new ArrayList();
            PlayListTimings = new ArrayList();
        }

        /// <summary>
        /// 목록 지정
        /// </summary>
        /// <param name="files"></param>
        /// <param name="timings"></param>
        /// <returns></returns>
        public int SetItems( ArrayList files, ArrayList timings )
        {
            PlayListFiles = files;
            PlayListTimings = timings;
            if (files.Count != timings.Count) return -2;
            for ( int i = 0; i < files.Count; i++ )
            {
                playlistitem = new cm_ContentItem(i + 1, (String)files[i], (TimeSpan)timings[i]);
                playlistitem.MouseDown += new MouseButtonEventHandler(playlistitem_MouseDown);
                ItemsContainer.Children.Add(playlistitem);
            }
            Total.Content = ItemsContainer.Children.Count;
            return 0;
        }

         /// <summary>
        /// Adds new empty item to playlist
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddContentItem_Click(object sender, RoutedEventArgs e)
        {
            playlistitem = new cm_ContentItem(ItemsContainer.Children.Count, null, new TimeSpan(0, 0, 5) );
            playlistitem.MouseDown += new MouseButtonEventHandler(playlistitem_MouseDown);
            ItemsContainer.Children.Add(playlistitem);
            Total.Content = ItemsContainer.Children.Count;
        }

        /// <summary>
        /// Highlights currently selected item on item MouseDown event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void playlistitem_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (currentitem != (cm_ContentItem)sender)
            {
                if ( currentitem != null )
                    currentitem.SwitchSelection();
                ((cm_ContentItem)sender).SwitchSelection();
                currentitem = (cm_ContentItem)sender;
            }
        }

        /// <summary>
        /// Removes currently selected item
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RemoveContentItem_Click(object sender, RoutedEventArgs e)
        {
            if (ItemsContainer.Children.Contains(currentitem) && ItemsContainer.Children.Count > 1)
            {
                for (int i = Int32.Parse(currentitem.ContentID.Text); i < ItemsContainer.Children.Count; i++)
                {
                    ((cm_ContentItem)ItemsContainer.Children[i]).ContentID.Text = (i - 1).ToString();
                }
                ItemsContainer.Children.Remove(currentitem);
            }
            Total.Content = ItemsContainer.Children.Count;
        }

        /// <summary>
        /// Closes current playlist window and saves playlist for further use
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Close_Click(object sender, RoutedEventArgs e)
        {
            PlayListFiles = new ArrayList();
            PlayListTimings = new ArrayList();
            foreach (cm_ContentItem item in ItemsContainer.Children)
            {
                if (item.itemContent.Content != null)
                {
                    PlayListFiles.Add(item.FilePath);
                    PlayListTimings.Add(item.Duration);
                }
            }

            ItemsContainer.Children.Clear();
            this.Close();
        }

        /// <summary>
        /// Moves selected item up
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MoveContentItemUp_Click(object sender, RoutedEventArgs e)
        {
            int index = int.Parse(((cm_ContentItem)currentitem).ContentID.Text);

            if (index > 0)
            {
                object itemContent = ((cm_ContentItem)ItemsContainer.Children[index - 1]).itemContent.Content;
                TimeSpan ts = (TimeSpan)((cm_ContentItem)ItemsContainer.Children[index - 1]).Duration;
                object tt = ((cm_ContentItem)ItemsContainer.Children[index - 1]).ToolTip;

                ((cm_ContentItem)ItemsContainer.Children[index - 1]).itemContent.Content = ((cm_ContentItem)ItemsContainer.Children[index]).itemContent.Content;
                ((cm_ContentItem)ItemsContainer.Children[index - 1]).Duration = ((cm_ContentItem)ItemsContainer.Children[index]).Duration;
                ((cm_ContentItem)ItemsContainer.Children[index - 1]).ToolTip = ((cm_ContentItem)ItemsContainer.Children[index]).ToolTip;

                ((cm_ContentItem)ItemsContainer.Children[index]).itemContent.Content = itemContent;
                ((cm_ContentItem)ItemsContainer.Children[index]).Duration = ts;
                ((cm_ContentItem)ItemsContainer.Children[index]).ToolTip = tt;
            }
        }

        /// <summary>
        /// Moves selected item down
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MoveContentItemDown_Click(object sender, RoutedEventArgs e)
        {
            int index = int.Parse(((cm_ContentItem)currentitem).ContentID.Text);

            if (index < ItemsContainer.Children.Count + 1)
            {
                object itemContent = ((cm_ContentItem)ItemsContainer.Children[index + 1]).itemContent.Content;
                TimeSpan ts = (TimeSpan)((cm_ContentItem)ItemsContainer.Children[index + 1]).Duration;
                object tt = ((cm_ContentItem)ItemsContainer.Children[index + 1]).ToolTip;

                ((cm_ContentItem)ItemsContainer.Children[index + 1]).itemContent.Content = ((cm_ContentItem)ItemsContainer.Children[index]).itemContent.Content;
                ((cm_ContentItem)ItemsContainer.Children[index + 1]).Duration = ((cm_ContentItem)ItemsContainer.Children[index]).Duration;
                ((cm_ContentItem)ItemsContainer.Children[index + 1]).ToolTip = ((cm_ContentItem)ItemsContainer.Children[index]).ToolTip;

                ((cm_ContentItem)ItemsContainer.Children[index]).itemContent.Content = itemContent;
                ((cm_ContentItem)ItemsContainer.Children[index]).Duration = ts;
                ((cm_ContentItem)ItemsContainer.Children[index]).ToolTip = tt;
            }
        }
    }
}