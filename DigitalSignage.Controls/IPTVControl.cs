﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DigitalSignage.Controls
{
    public partial class IPTVControl : UserControl
    {
        /// <summary>
        /// Gets of sets the source for the movie
        /// </summary>
        public string source;

        bool _muted;

        /// <summary>
        /// Get or sets the volume of IPTVPlayer
        /// </summary>
        public double Volume
        {
            get
            {
                return Convert.ToDouble(iptv.Volume);
            }

            set
            {
                iptv.Volume = Convert.ToInt32(value);
            }
        }

        /// <summary>
        /// Get or sets the mute of IPTVPlayer
        /// </summary>
        public bool IsMuted
        {
            get
            {
                return _muted;
            }

            set
            {
                iptv.toggleMute();
                _muted = !_muted;
            }
        }

        public IPTVControl()
        {
            InitializeComponent();
            _muted = false;
        }

        private void IPTVControl_Load(object sender, EventArgs e)
        {
            
        }

        /// <summary>
        /// Plays the the movie that the source points to
        /// </summary>
        public void Play()
        {
            iptv.playlistClear();
            iptv.addTarget(source, null, AXVLC.VLCPlaylistMode.VLCPlayListReplace, 0);
            iptv.play();
        }

        /// <summary>
        /// Stops playing
        /// </summary>
        public void Stop()
        {
            try
            {
                iptv.stop();
            }
            catch (Exception ex)
            {
                System.IO.File.AppendAllText(@"D:\" + "iptvlog.txt", "\n" + ex.Message + "\n" + ex.InnerException + "\n" + ex.StackTrace);
            }
        }
    }
}