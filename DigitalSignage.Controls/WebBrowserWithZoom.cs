﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace DigitalSignage.Controls
{
	/// <summary>
	/// 줌기능을 포함한 웹브라우져
	/// </summary>
	public partial class WebBrowserWithZoom : WebBrowser
	{
		/// <summary>
		///	생성자
		/// </summary>
		public WebBrowserWithZoom()
		{
			InitializeComponent();
		}

		#region enums
        /// <summary>
        /// 
        /// </summary>
		public enum OLECMDID
		{
            /// <summary>
            /// 
            /// </summary>
			OLECMDID_OPEN = 1,
            /// <summary>
            /// 
            /// </summary>
            OLECMDID_NEW = 2,
            /// <summary>
            /// 
            /// </summary>
            OLECMDID_SAVE = 3,
            /// <summary>
            /// 
            /// </summary>
            OLECMDID_SAVEAS = 4,
            /// <summary>
            /// 
            /// </summary>
            OLECMDID_SAVECOPYAS = 5,
            /// <summary>
            /// 
            /// </summary>
            OLECMDID_PRINT = 6,
            /// <summary>
            /// 
            /// </summary>
            OLECMDID_PRINTPREVIEW = 7,
            /// <summary>
            /// 
            /// </summary>
            OLECMDID_PAGESETUP = 8,
            /// <summary>
            /// 
            /// </summary>
            OLECMDID_SPELL = 9,
            /// <summary>
            /// 
            /// </summary>
            OLECMDID_PROPERTIES = 10,
            /// <summary>
            /// 
            /// </summary>
            OLECMDID_CUT = 11,
            /// <summary>
            /// 
            /// </summary>
            OLECMDID_COPY = 12,
            /// <summary>
            /// 
            /// </summary>
            OLECMDID_PASTE = 13,
            /// <summary>
            /// 
            /// </summary>
            OLECMDID_PASTESPECIAL = 14,
            /// <summary>
            /// 
            /// </summary>
            OLECMDID_UNDO = 15,
            /// <summary>
            /// 
            /// </summary>
            OLECMDID_REDO = 16,
            /// <summary>
            /// 
            /// </summary>
            OLECMDID_SELECTALL = 17,
            /// <summary>
            /// 
            /// </summary>
            OLECMDID_CLEARSELECTION = 18,
            /// <summary>
            /// 
            /// </summary>
            OLECMDID_ZOOM = 19,
            /// <summary>
            /// 
            /// </summary>
            OLECMDID_GETZOOMRANGE = 20,
            /// <summary>
            /// 
            /// </summary>
            OLECMDID_UPDATECOMMANDS = 21,
            /// <summary>
            /// 
            /// </summary>
            OLECMDID_REFRESH = 22,
            /// <summary>
            /// 
            /// </summary>
            OLECMDID_STOP = 23,
            /// <summary>
            /// 
            /// </summary>
            OLECMDID_HIDETOOLBARS = 24,
            /// <summary>
            /// 
            /// </summary>
            OLECMDID_SETPROGRESSMAX = 25,
            /// <summary>
            /// 
            /// </summary>
            OLECMDID_SETPROGRESSPOS = 26,
            /// <summary>
            /// 
            /// </summary>
            OLECMDID_SETPROGRESSTEXT = 27,
            /// <summary>
            /// 
            /// </summary>
            OLECMDID_SETTITLE = 28,
            /// <summary>
            /// 
            /// </summary>
            OLECMDID_SETDOWNLOADSTATE = 29,
            /// <summary>
            /// 
            /// </summary>
            OLECMDID_STOPDOWNLOAD = 30,
            /// <summary>
            /// 
            /// </summary>
            OLECMDID_ONTOOLBARACTIVATED = 31,
            /// <summary>
            /// 
            /// </summary>
            OLECMDID_FIND = 32,
            /// <summary>
            /// 
            /// </summary>
            OLECMDID_DELETE = 33,
            /// <summary>
            /// 
            /// </summary>
            OLECMDID_HTTPEQUIV = 34,
            /// <summary>
            /// 
            /// </summary>
            OLECMDID_HTTPEQUIV_DONE = 35,
            /// <summary>
            /// 
            /// </summary>
            OLECMDID_ENABLE_INTERACTION = 36,
            /// <summary>
            /// 
            /// </summary>
            OLECMDID_ONUNLOAD = 37,
            /// <summary>
            /// 
            /// </summary>
            OLECMDID_PROPERTYBAG2 = 38,
            /// <summary>
            /// 
            /// </summary>
            OLECMDID_PREREFRESH = 39,
            /// <summary>
            /// 
            /// </summary>
            OLECMDID_SHOWSCRIPTERROR = 40,
            /// <summary>
            /// 
            /// </summary>
            OLECMDID_SHOWMESSAGE = 41,
            /// <summary>
            /// 
            /// </summary>
            OLECMDID_SHOWFIND = 42,
            /// <summary>
            /// 
            /// </summary>
            OLECMDID_SHOWPAGESETUP = 43,
            /// <summary>
            /// 
            /// </summary>
            OLECMDID_SHOWPRINT = 44,
            /// <summary>
            /// 
            /// </summary>
            OLECMDID_CLOSE = 45,
            /// <summary>
            /// 
            /// </summary>
            OLECMDID_ALLOWUILESSSAVEAS = 46,
            /// <summary>
            /// 
            /// </summary>
            OLECMDID_DONTDOWNLOADCSS = 47,
            /// <summary>
            /// 
            /// </summary>
            OLECMDID_UPDATEPAGESTATUS = 48,
            /// <summary>
            /// 
            /// </summary>
            OLECMDID_PRINT2 = 49,
            /// <summary>
            /// 
            /// </summary>
            OLECMDID_PRINTPREVIEW2 = 50,
            /// <summary>
            /// 
            /// </summary>
            OLECMDID_SETPRINTTEMPLATE = 51,
            /// <summary>
            /// 
            /// </summary>
            OLECMDID_GETPRINTTEMPLATE = 52,
            /// <summary>
            /// 
            /// </summary>
            OLECMDID_PAGEACTIONBLOCKED = 55,
            /// <summary>
            /// 
            /// </summary>
            OLECMDID_PAGEACTIONUIQUERY = 56,
            /// <summary>
            /// 
            /// </summary>
            OLECMDID_FOCUSVIEWCONTROLS = 57,
            /// <summary>
            /// 
            /// </summary>
            OLECMDID_FOCUSVIEWCONTROLSQUERY = 58,
            /// <summary>
            /// 
            /// </summary>
            OLECMDID_SHOWPAGEACTIONMENU = 59,
            /// <summary>
            /// 
            /// </summary>
            OLECMDID_ADDTRAVELENTRY = 60,
            /// <summary>
            /// 
            /// </summary>
            OLECMDID_UPDATETRAVELENTRY = 61,
            /// <summary>
            /// 
            /// </summary>
            OLECMDID_UPDATEBACKFORWARDSTATE = 62,
            /// <summary>
            /// 
            /// </summary>
            OLECMDID_OPTICAL_ZOOM = 63,
            /// <summary>
            /// 
            /// </summary>
            OLECMDID_OPTICAL_GETZOOMRANGE = 64,
            /// <summary>
            /// 
            /// </summary>
            OLECMDID_WINDOWSTATECHANGED = 65
		}

        /// <summary>
        /// 
        /// </summary>
		public enum OLECMDEXECOPT
		{
            /// <summary>
            /// 
            /// </summary>
			OLECMDEXECOPT_DODEFAULT,
            /// <summary>
            /// 
            /// </summary>
            OLECMDEXECOPT_PROMPTUSER,
            /// <summary>
            /// 
            /// </summary>
            OLECMDEXECOPT_DONTPROMPTUSER,
            /// <summary>
            /// 
            /// </summary>
            OLECMDEXECOPT_SHOWHELP
		}

        /// <summary>
        /// 
        /// </summary>
        public enum OLECMDF
		{
            /// <summary>
            /// 
            /// </summary>
            OLECMDF_DEFHIDEONCTXTMENU = 0x20,
            /// <summary>
            /// 
            /// </summary>
            OLECMDF_ENABLED = 2,
            /// <summary>
            /// 
            /// </summary>
            OLECMDF_INVISIBLE = 0x10,
            /// <summary>
            /// 
            /// </summary>
            OLECMDF_LATCHED = 4,
            /// <summary>
            /// 
            /// </summary>
            OLECMDF_NINCHED = 8,
            /// <summary>
            /// 
            /// </summary>
            OLECMDF_SUPPORTED = 1
		}
		#endregion

		#region IWebBrowser2
		
        /// <summary>
        /// IWebBrowser2 인터페이스
        /// </summary>
        [ComImport, /*SuppressUnmanagedCodeSecurity,*/ TypeLibType(TypeLibTypeFlags.FOleAutomation | TypeLibTypeFlags.FDual | TypeLibTypeFlags.FHidden), Guid("D30C1661-CDAF-11d0-8A3E-00C04FC9E26E")]
		public interface IWebBrowser2
		{
            /// <summary>
            /// GoBack
            /// </summary>
			[DispId(100)]
			void GoBack();
            /// <summary>
            /// GoBack
            /// </summary>
            [DispId(0x65)]
			void GoForward();
			[DispId(0x66)]
            /// <summary>
            /// GoBack
            /// </summary>
            void GoHome();
			[DispId(0x67)]
			void GoSearch();
            /// <summary>
            /// GoBack
            /// </summary>
            [DispId(0x68)]
			void Navigate([In] string Url, [In] ref object flags, [In] ref object targetFrameName, [In] ref object postData, [In] ref object headers);
            /// <summary>
            /// GoBack
            /// </summary>
            [DispId(-550)]
			void Refresh();
            /// <summary>
            /// GoBack
            /// </summary>
            [DispId(0x69)]
			void Refresh2([In] ref object level);
            /// <summary>
            /// GoBack
            /// </summary>
            [DispId(0x6a)]
			void Stop();
            /// <summary>
            /// GoBack
            /// </summary>
            [DispId(200)]
			object Application { [return: MarshalAs(UnmanagedType.IDispatch)] get; }
            /// <summary>
            /// GoBack
            /// </summary>
            [DispId(0xc9)]
			object Parent { [return: MarshalAs(UnmanagedType.IDispatch)] get; }
            /// <summary>
            /// GoBack
            /// </summary>
            [DispId(0xca)]
			object Container { [return: MarshalAs(UnmanagedType.IDispatch)] get; }
            /// <summary>
            /// GoBack
            /// </summary>
            [DispId(0xcb)]
			object Document { [return: MarshalAs(UnmanagedType.IDispatch)] get; }
            /// <summary>
            /// GoBack
            /// </summary>
            [DispId(0xcc)]
			bool TopLevelContainer { get; }
            /// <summary>
            /// GoBack
            /// </summary>
            [DispId(0xcd)]
			string Type { get; }
            /// <summary>
            /// GoBack
            /// </summary>
            [DispId(0xce)]
			int Left { get; set; }
            /// <summary>
            /// GoBack
            /// </summary>
            [DispId(0xcf)]
			int Top { get; set; }
            /// <summary>
            /// GoBack
            /// </summary>
            [DispId(0xd0)]
			int Width { get; set; }
            /// <summary>
            /// GoBack
            /// </summary>
            [DispId(0xd1)]
			int Height { get; set; }
            /// <summary>
            /// GoBack
            /// </summary>
            [DispId(210)]
			string LocationName { get; }
            /// <summary>
            /// GoBack
            /// </summary>
            [DispId(0xd3)]
			string LocationURL { get; }
            /// <summary>
            /// GoBack
            /// </summary>
            [DispId(0xd4)]
			bool Busy { get; }
            /// <summary>
            /// GoBack
            /// </summary>
            [DispId(300)]
			void Quit();
            /// <summary>
            /// GoBack
            /// </summary>
            [DispId(0x12d)]
			void ClientToWindow(out int pcx, out int pcy);
            /// <summary>
            /// GoBack
            /// </summary>
            [DispId(0x12e)]
			void PutProperty([In] string property, [In] object vtValue);
            /// <summary>
            /// GoBack
            /// </summary>
            [DispId(0x12f)]
			object GetProperty([In] string property);
            /// <summary>
            /// GoBack
            /// </summary>
            [DispId(0)]
			string Name { get; }
            /// <summary>
            /// GoBack
            /// </summary>
            [DispId(-515)]
			int HWND { get; }
            /// <summary>
            /// GoBack
            /// </summary>
            [DispId(400)]
			string FullName { get; }
            /// <summary>
            /// GoBack
            /// </summary>
            [DispId(0x191)]
			string Path { get; }
            /// <summary>
            /// GoBack
            /// </summary>
            [DispId(0x192)]
			bool Visible { get; set; }
            /// <summary>
            /// GoBack
            /// </summary>
            [DispId(0x193)]
			bool StatusBar { get; set; }
            /// <summary>
            /// GoBack
            /// </summary>
            [DispId(0x194)]
			string StatusText { get; set; }
            /// <summary>
            /// GoBack
            /// </summary>
            [DispId(0x195)]
			int ToolBar { get; set; }
            /// <summary>
            /// GoBack
            /// </summary>
            [DispId(0x196)]
			bool MenuBar { get; set; }
            /// <summary>
            /// GoBack
            /// </summary>
            [DispId(0x197)]
			bool FullScreen { get; set; }
            /// <summary>
            /// GoBack
            /// </summary>
            [DispId(500)]
			void Navigate2([In] ref object URL, [In] ref object flags, [In] ref object targetFrameName, [In] ref object postData, [In] ref object headers);
            /// <summary>
            /// GoBack
            /// </summary>
            [DispId(0x1f5)]
			OLECMDF QueryStatusWB([In] OLECMDID cmdID);
            /// <summary>
            /// GoBack
            /// </summary>
            [DispId(0x1f6)]
			void ExecWB([In] OLECMDID cmdID, [In] OLECMDEXECOPT cmdexecopt, ref object pvaIn, IntPtr pvaOut);
            /// <summary>
            /// GoBack
            /// </summary>
            [DispId(0x1f7)]
			void ShowBrowserBar([In] ref object pvaClsid, [In] ref object pvarShow, [In] ref object pvarSize);
            /// <summary>
            /// GoBack
            /// </summary>
            [DispId(-525)]
			WebBrowserReadyState ReadyState { get; }
            /// <summary>
            /// GoBack
            /// </summary>
            [DispId(550)]
			bool Offline { get; set; }
            /// <summary>
            /// GoBack
            /// </summary>
            [DispId(0x227)]
			bool Silent { get; set; }
            /// <summary>
            /// GoBack
            /// </summary>
            [DispId(0x228)]
			bool RegisterAsBrowser { get; set; }
            /// <summary>
            /// GoBack
            /// </summary>
            [DispId(0x229)]
			bool RegisterAsDropTarget { get; set; }
            /// <summary>
            /// GoBack
            /// </summary>
            [DispId(0x22a)]
			bool TheaterMode { get; set; }
            /// <summary>
            /// GoBack
            /// </summary>
            [DispId(0x22b)]
			bool AddressBar { get; set; }
            /// <summary>
            /// GoBack
            /// </summary>
            [DispId(0x22c)]
			bool Resizable { get; set; }
		}
		#endregion

		private IWebBrowser2 axIWebBrowser2;

		/// <summary>
		/// ActiveX 인터페이스 연결
		/// </summary>
		/// <param name="nativeActiveXObject"></param>
		protected override void AttachInterfaces(object nativeActiveXObject)
		{
			base.AttachInterfaces(nativeActiveXObject);
			this.axIWebBrowser2 = (IWebBrowser2)nativeActiveXObject;
		}

		/// <summary>
		/// 인터페이스 해제
		/// </summary>
		protected override void DetachInterfaces()
		{
			base.DetachInterfaces();
			this.axIWebBrowser2 = null;
		}

		/// <summary>
		/// 줌 기능 지원.
		/// </summary>
		/// <param name="factor">줌 요소</param>
		public void Zoom(int factor)
		{

			object pvaIn = factor;
			try
			{
				this.axIWebBrowser2.ExecWB(OLECMDID.OLECMDID_OPTICAL_ZOOM, OLECMDEXECOPT.OLECMDEXECOPT_DONTPROMPTUSER, ref pvaIn, IntPtr.Zero);
			}
			catch (Exception)
			{
				throw;
			}
		}
	}
}
