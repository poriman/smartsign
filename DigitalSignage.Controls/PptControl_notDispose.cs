﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Diagnostics;

namespace DigitalSignage.Controls
{
    public partial class PPTControl : UserControl
    {
        /// <summary>
        /// Initializes the new PPTControl class instance
        /// </summary>
		public PPTControl()
		{
			try
			{
				InitializeComponent();
				this.Disposed += new EventHandler(PPTControl_Disposed);
				viewer.BackColor = Color.White;
			}
			catch { try { viewer.Dispose(); viewer = null; } catch { } }
		}

		/// <summary>
		/// 화면 비율
		/// </summary>
		float _screenRatio = float.NaN;

        void PPTControl_Disposed(object sender, EventArgs e)
		{
			try
			{
				Stop();
			}
			catch
			{
				try { viewer.Dispose(); viewer = null; } catch {} 
			}
		}

		public void CheatDispose()
		{
			this.Controls.Clear();
			this.Dispose();

		}

        /// <summary>
        /// Plays the specified PPT presentation file
        /// </summary>
        /// <param name="source">Full PPT file path</param>
        public void Play(string source)
        {
			try
			{
				if (viewer != null && !viewer.IsDisposed)
				{
					// 				InitialSize(source);

					viewer.SlideShowOpenAndPlay(source, false, false, true, false);
					// 				viewer.Open(source);

					#region SlideShow Play
					try
					{
						Microsoft.Office.Interop.PowerPoint._Application app = viewer.Application as Microsoft.Office.Interop.PowerPoint._Application;
						Microsoft.Office.Interop.PowerPoint._Presentation pres = app.ActivePresentation;

						app = new Microsoft.Office.Interop.PowerPoint.Application();
						pres = app.Presentations.Open(source, Microsoft.Office.Core.MsoTriState.msoTrue, Microsoft.Office.Core.MsoTriState.msoTriStateMixed, Microsoft.Office.Core.MsoTriState.msoFalse);

						if (pres != null)
						{
							_screenRatio = pres.PageSetup.SlideWidth / pres.PageSetup.SlideHeight;
						}
					}
					catch (Exception ex)
					{
						_screenRatio = float.NaN;
						Trace.WriteLine(ex.Message);
					}
					#endregion
					ResizeViewer();

					// 				viewer.SlideShowPlay(false, false, true, false);

					viewer.Visible = true;
				}
			}
			catch
			{
				try { viewer.Dispose(); viewer = null; } catch {} 
			}

		}

		private void InitialSize(string source)
		{
			try
			{
				Microsoft.Office.Interop.PowerPoint._Application app = null;
				Microsoft.Office.Interop.PowerPoint._Presentation pres = null;

				app = new Microsoft.Office.Interop.PowerPoint.Application();
				pres = app.Presentations.Open(source, Microsoft.Office.Core.MsoTriState.msoTrue, Microsoft.Office.Core.MsoTriState.msoTriStateMixed, Microsoft.Office.Core.MsoTriState.msoFalse);

				if (pres != null)
				{
					_screenRatio = pres.PageSetup.SlideWidth / pres.PageSetup.SlideHeight;
				}
			}
			catch (Exception ex) { 
				_screenRatio = float.NaN;
				Trace.WriteLine(ex.Message);
			}
		}

        /// <summary>
        /// Stops the presentation playback
        /// </summary>
        public void Stop()
        {
			try
			{
				_screenRatio = float.NaN;

				if (viewer != null && !viewer.IsDisposed)
				{
					viewer.Visible = false;
					if (viewer.IsOpened)
					{
						// 					viewer.SlideShowExit();
						viewer.Close();
					}
				}
			}
			catch
			{
				try { viewer.Dispose(); viewer = null; }
				catch { } 
			}

        }
		public System.Drawing.Bitmap Thumbnail(int cx, int cy)
		{
			try
			{
				Bitmap bmp = new Bitmap(cx, cy, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
				using (Graphics g = Graphics.FromImage(bmp))
				{
					Point pos = this.PointToScreen(new Point(0, 0));
					g.CopyFromScreen(pos, new Point(0, 0), this.Size, CopyPixelOperation.SourceCopy);
				}

				return bmp;
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}

			return null;

		}

		private void ResizeViewer()
		{
			if (viewer != null)
			{
				try
				{
					double compRatio = this.Width / this.Height;
					if (_screenRatio == float.NaN)
					{
						viewer.Width = this.Width;
						viewer.Height = this.Height;
						viewer.Left = 0;
						viewer.Top = 0;
					}
					else if (compRatio > _screenRatio)	//	높이가 큰 경우
					{
						viewer.Height = this.Height;
						viewer.Width = (int)Math.Round((float)viewer.Height * _screenRatio);
						viewer.Left = (this.Width - viewer.Width) / 2;
						viewer.Top = 0;
					}
					else if (compRatio <= _screenRatio)
					{
						viewer.Width = this.Width;
						viewer.Height = (int)Math.Round((float)viewer.Width / _screenRatio);
						viewer.Left = 0;
						viewer.Top = (this.Height - viewer.Height) / 2;
					}
					else
					{

					}
				}
				catch { try { viewer.Dispose(); viewer = null; } catch { } }

// 				else if (_screenRatio > 0 && _screenRatio > 1)
// 				{
// 					//	너비가 큰 경우
// 					viewer.Width = this.Width;
// 					viewer.Height = (int)Math.Round((float)viewer.Width / _screenRatio);
// 					viewer.Left = 0;
// 					viewer.Top = (this.Height - viewer.Height) / 2;
// 
// 				}
// 				else if (_screenRatio > 0 && _screenRatio <= 1)
// 				{
// 					//	높이가 큰 경우
// 					viewer.Height = this.Height;
// 					viewer.Width = (int)Math.Round((float)viewer.Height * _screenRatio);
// 					viewer.Left = (this.Width - viewer.Width) / 2;
// 					viewer.Top = 0;
// 				}

			}
		}
		private void PPTControl_SizeChanged(object sender, EventArgs e)
		{
			try
			{
				ResizeViewer();
			}
			catch
			{
				try { viewer.Dispose(); viewer = null; }
				catch { } 
			}
		}
    }
}