﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DigitalSignage.Controls
{
    /// <summary>
    /// ActiveX 기반 퀵타임 플레이어
    /// </summary>
    public partial class QuickTimeControl : UserControl
    {
        /// <summary>
        /// 생성자
        /// </summary>
        public QuickTimeControl()
        {
            InitializeComponent();
			this.Disposed += new EventHandler(UserControl_Disposed);
        }
		private AxQTOControlLib.AxQTControl axQTControl1 = null;

		private bool isMute = false;
		private float volume = 1.0F;

        /// <summary>
        /// 음소거 여부
        /// </summary>
		public bool IsMute
		{
			get { return isMute; }
            set
            {
                try
                {
                    axQTControl1.Movie.AudioMute = isMute = value;
                }
                catch
                {
                    isMute = value;
                }
            }
		}

        /// <summary>
        /// 볼륨 조절
        /// </summary>
		public float Volume
		{
			get { return volume; }
			set { 
                try
                {
                    axQTControl1.Movie.AudioVolume = volume = value;
                }
                catch 
                {
                    volume = value;
                }
            }
		}
		
		/// <summary>
        /// Plays the nedia located by the specified path
        /// </summary>
        /// <param name="source">Path to the media</param>
        public void Play(string source)
        {
			AxQTOControlLib.AxQTControl axQT = InitQuickTime();
			if (axQT.get_IsQuickTimeAvailable(0))
			{
				axQT.URL = source;
			}
			axQT.Movie.KeysEnabled = false;
			axQT.Movie.AudioMute = IsMute;
			axQT.Movie.AudioVolume = Volume;
			axQT.Movie.Loop = true;
			axQT.Movie.Play(axQT.Movie.PreferredRate);
			if (axQTControl1 != null)
			{
				this.Controls.Remove(axQTControl1);

				axQTControl1.QuickTimeTerminate();
				axQTControl1.Dispose();
				axQTControl1 = null;
			}

			axQTControl1 = axQT;
        }

		public void Pause()
		{
			try
			{
				if (axQTControl1 != null)
				{
					axQTControl1.Movie.Pause();
				}
			}
			catch { }
		}

        /// <summary>
        /// 퀵타임 객체 초기화
        /// </summary>
        /// <returns></returns>
		private AxQTOControlLib.AxQTControl InitQuickTime()
		{
			AxQTOControlLib.AxQTControl axQT = new AxQTOControlLib.AxQTControl();
// 			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(QuickTimeControl));
			((System.ComponentModel.ISupportInitialize)axQT).BeginInit();
			this.SuspendLayout();

			axQT.Enabled = false;
			axQT.Name = "axQTControl1";
			axQT.TabIndex = 0;
// 			axQT.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axQTControl1.OcxState")));

			axQT.Parent = this;

			axQT.Size = new Size(this.Width, this.Height);

			this.Controls.Add(axQT);

			axQT.QuickTimeInitialize();
			axQT.Sizing = QTOControlLib.QTSizingModeEnum.qtMovieFitsControl;
			axQT.MovieControllerVisible = false;
			axQT.BorderStyle = QTOControlLib.BorderStylesEnum.bsNone;

			this.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)axQT).EndInit();

			return axQT;
		}

        /// <summary>
        /// 객체 소멸
        /// </summary>
		private void Clear()
		{
			this.Controls.Clear();

			if (axQTControl1 != null)
			{
				axQTControl1.QuickTimeTerminate();
				axQTControl1.Dispose();
				axQTControl1 = null;
			}

			GC.Collect();

		}

        /// <summary>
        /// Stops playing the movie
        /// </summary>
        public void Stop()
        {
            try
            {
				if (axQTControl1 != null)
				{
					if (axQTControl1.get_IsQuickTimeAvailable(0))
					{
						axQTControl1.Movie.Stop();
						axQTControl1.URL = "";
					}
				}
            }
            catch { }
        }

        /// <summary>
        /// 썸네일 추출
        /// </summary>
        /// <param name="cx"></param>
        /// <param name="cy"></param>
        /// <returns></returns>
		public System.Drawing.Bitmap Thumbnail(int cx, int cy)
		{
			try
			{
				Bitmap bmp = new Bitmap(cx, cy, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
				using (Graphics g = Graphics.FromImage(bmp))
				{
					Point pos = this.PointToScreen(new Point(0, 0));
					g.CopyFromScreen(pos, new Point(0, 0), this.Size, CopyPixelOperation.SourceCopy);
				}

				return bmp;
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}

			return null;

		}

		/// <summary>QTOControlWindow
        /// Returns the duration of QuickTime movie
        /// </summary>
        /// <param name="moviePath">Path to the .mov file</param>
        /// <returns>TimeSpan that represents the movie length</returns>
        public TimeSpan GetQuickTimeMovieDuration(string moviePath)
        {
            try
            {
				using (AxQTOControlLib.AxQTControl axQT = InitQuickTime())
				{
					if (axQT.get_IsQuickTimeAvailable(0))
					{
						axQT.URL = moviePath;
					}
					axQT.Movie.SelectAll();
					TimeSpan ts = new TimeSpan(0, 0, axQT.Movie.Duration / axQT.Movie.TimeScale);

					axQT.Dispose();
					return ts;
				}
            }
            catch (Exception ex)
			{
// 				MessageBox.Show("QuickTime has following error: " + ex.Message);
				return new TimeSpan(0,0,0,0); 
			}
        }

        /// <summary>
        /// 객체 소멸 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
		void UserControl_Disposed(Object sender, EventArgs args)
		{
			try
			{
				Stop();
				Clear();
			}
			catch
			{

			}
		}
        
        /// <summary>
        /// 객체 리사이즈 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
		private void QuickTimeControl_SizeChanged(object sender, EventArgs e)
		{
			try
			{
				if (axQTControl1 != null)
				{
					axQTControl1.Size = new Size(this.Size.Width, this.Size.Height);
				}
			}
			catch
			{
				
			}
		}
		
    }
}