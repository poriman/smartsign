﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Media.Effects;

using QuartzTypeLib;

namespace DigitalSignage.Controls
{
    /// <summary>
    /// Interaction logic for cm_ContentItem.xaml
    /// </summary>
    public partial class cm_ContentItem : UserControl
    {
        private String filePath;
        private TimeSpan duration;
        
        //An effect that is used to highlight item
        OuterGlowBitmapEffect selectionEffect;

        //Dialog used for opening images, audio and video files.
        //Initiated according to ContentType of item
        System.Windows.Forms.OpenFileDialog OpenFileDialog;

        //UrlSetter us;

        DurationSetter ds;

 //       SolidColorBrush defaultBack;

        //ScrollTextSetter sts;

        //RSSText rts;
        //WeatherReaderUI weather;
        //CopyBox cb;

        /// <summary>
        /// Creates a new playlist item
        /// </summary>
        /// <param name="index">Number (index) of item that will be displayed on the current item</param>
        /// <param name="contents">A contents object that will be displayed on the current item</param>
        /// <param name="duration">A duration of appearance of item`s content</param>
        public cm_ContentItem(int index, string contents, TimeSpan duration )
        {
            InitializeComponent();
            //Initiating visual appearance of item
            ContentID.Text = index.ToString();
            itemContent.Content = contents;
            
            filePath = contents;
            Duration = duration;

            //defaultBack = (SolidColorBrush)Contents.Background;
            //Initiating a new image that is used as a thumbnail in a tooltip of current item
            //im = new Image();
            //im.Stretch = Stretch.UniformToFill;

            //Initiating an effect that is used as a selection highlight of item if it is clicked
            selectionEffect = new OuterGlowBitmapEffect();
            selectionEffect.GlowColor = Color.FromArgb(255, 255, 255, 255);
            selectionEffect.GlowSize = 5;

            //Creating new dialog instance
            OpenFileDialog = new System.Windows.Forms.OpenFileDialog();
            OpenFileDialog.FileOk += new System.ComponentModel.CancelEventHandler(OpenFileDialog_FileOk);

        }

        void OpenFileDialog_FileOk(object sender, System.ComponentModel.CancelEventArgs e)
        {
            filePath = OpenFileDialog.FileName;
            itemContent.Content = System.IO.Path.GetFileName(OpenFileDialog.FileName);
        }

        //Providing an editing of current content item depending on it`s type of content
        private void itemContent_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            OpenFileDialog.ShowDialog();
        }

        private void Duration_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ds = new DurationSetter();
            ds.Load(Duration);
            Duration = ds.GetDuration();
        }

        /// <summary>
        /// Applies an visual effect to current item indicating that it was clicked
        /// </summary>
        public void SwitchSelection()
        {
            if (this.BitmapEffect == null)
            {
                this.BitmapEffect = selectionEffect;
                cm_Border.BorderBrush = new SolidColorBrush(Color.FromArgb(255, 255, 255, 255));
            }
            else
            {
                this.BitmapEffect = null;
                cm_Border.BorderBrush = new SolidColorBrush(Color.FromArgb(255, 97, 86, 86));
            }
        }

        #region Propertyes
        public String FilePath
        {
            get { return filePath; }
            set { filePath = value; }
        }

        public TimeSpan Duration
        {
            get 
            { 
                return duration; 
            }
            set 
            { 
                duration = value;
                durationLabel.Content = duration;
            }
        }        
        #endregion
    }
}