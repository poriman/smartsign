﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.Windows.Threading;

using Microsoft.Office.Core;
using PowerPoint = Microsoft.Office.Interop.PowerPoint;
using System.Runtime.InteropServices;

namespace DigitalSignage.Controls
{
    public partial class PPTControl : UserControl
    {
        #region Windows API

        [DllImport("user32")]
        public static extern int SetWindowPos(IntPtr hwnd, IntPtr hWndInsertAfter, int x, int y, int cx, int cy, int wFlags);
        //remove border
        //Import window changing function
        [DllImport("USER32.DLL")]
        public static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);
        //Import force window draw function
        [DllImport("user32.dll")]
        static extern bool DrawMenuBar(IntPtr hWnd);
        [DllImport("user32.dll")]
        static extern bool ShowWindow(IntPtr hWnd, int flag);
        [DllImport("user32")]
        private static extern IntPtr SetParent(IntPtr hWnd, IntPtr hWndParent);


        private const int GWL_STYLE = -16;              //hex constant for style changing
        private const int WS_SYSMENU = 0x00080000;      //window with no borders etc.
        private const int SW_HIDE = 0;      //window with no borders etc.
        private const int SW_SHOW = 5;      //window with no borders etc.
        private const int WS_CHILD = 0x40000000;
        #endregion

        DispatcherTimer tm = null;
 
        bool bVaildatePPTSize = true;

		static string sLastLoadedPPT = String.Empty;

        static PowerPoint.Application objApp = null;
        PowerPoint.Presentations objPresSet = null;
        PowerPoint._Presentation objPres = null;
        PowerPoint.Slides objSlides = null;
        PowerPoint.SlideShowWindow objSSW = null;

        PowerPoint.SlideShowSettings objSSS = null;

		static object lockObject = new object();

        /// <summary>
        /// Initializes the new PPTControl class instance
        /// </summary>
        public PPTControl()
        {
            InitializeComponent();
            this.Disposed += new EventHandler(PPTControl_Disposed);

            try
            {
                if (System.Configuration.ConfigurationManager.AppSettings["VaildatePPTSize"] != null)
	               bVaildatePPTSize = Convert.ToBoolean(System.Configuration.ConfigurationManager.AppSettings["VaildatePPTSize"]);
            }
            catch
            {
                bVaildatePPTSize = true;
            }
		}

        void PPTControl_Disposed(object sender, EventArgs e)
		{
			try
			{
				if(viewer != null && !viewer.IsDisposed)
				{
					Stop();
				}

				if (tm != null)
				{
					tm.Stop();
					tm = null;
				}
			}
			catch
			{

			}
		}

		/// <summary>
		/// 화면 비율
		/// </summary>
		float _screenRatio = float.NaN;


        /// <summary>
        /// Plays the specified PPT presentation file
        /// </summary>
        /// <param name="source">Full PPT file path</param>
        public void Play(string source)
        {
			_screenRatio = float.NaN;
			try
			{
				lock (lockObject)
				{

					try
					{
                        System.IO.FileInfo fi = new System.IO.FileInfo(source);

                        if (sLastLoadedPPT.Equals(fi.Name))
						{
                            fi.CopyTo(source = (fi.FullName + fi.Extension), false);
						}
					}
					catch { }

                    if(objApp == null)
                        objApp = new PowerPoint.Application();

                    //objApp.Visible = MsoTriState.msoTrue;
                    objApp.DisplayAlerts = PowerPoint.PpAlertLevel.ppAlertsNone;

                    ShowWindow((IntPtr)objApp.HWND, SW_HIDE);

                    objPresSet = objApp.Presentations;


                    //objPres = objPresSet.Open2007(source, MsoTriState.msoTrue, MsoTriState.msoFalse, MsoTriState.msoFalse, MsoTriState.msoFalse);
                    objPres = objPresSet.Open(source, MsoTriState.msoTrue, MsoTriState.msoFalse, MsoTriState.msoFalse);

                    if (objPres != null)
					{
						try
						{
                            objSlides = objPres.Slides;

                            objSSS = objPres.SlideShowSettings;
                            //  셋팅을 하나라도 바꾸면 파워포인트 저장하라고 표시된
                            //objSSS.LoopUntilStopped = MsoTriState.msoTrue;

                            System.IO.FileInfo fi = new System.IO.FileInfo(source);
                            sLastLoadedPPT = fi.Name;

                            tm = new DispatcherTimer();
                            tm.Interval = TimeSpan.FromMilliseconds(1);

                            tm.Tick += new EventHandler(tm_Tick);
                            tm.Start();

						}
						catch
						{

						}
						
					}
				}
			}
			catch 
			{
			}

		}

		public void Pause()
		{
		}

		void tm_Tick(object sender, EventArgs e)
		{
            tm.Stop();

            if (ControlPPT())
            {
                viewer.Visible = true;
            }
            else
            {
                tm = new DispatcherTimer();
                tm.Interval = TimeSpan.FromMilliseconds(100);

                tm.Tick += new EventHandler(tm_Tick);
                tm.Start();
            }
		}

        private bool ControlPPT()
        {
            lock (lockObject)
            {
                try
                {
                    if(objSSW == null)
                        objSSW = objSSS.Run();

                    if (objSSW != null)
                    {
                        try
                        {
                            SetWindowLong((IntPtr)objSSW.HWND, GWL_STYLE, WS_CHILD & WS_SYSMENU);
                            SetWindowPos((IntPtr)objSSW.HWND, (IntPtr)(-2), 0, 0, this.Width, this.Height, 0x0040);
                            SetParent((IntPtr)objSSW.HWND, viewer.Handle);
                            DrawMenuBar((IntPtr)objSSW.HWND);

                            //ResizeViewer();

                            return true;
                        }
                        catch
                        {

                        }
                    }
                    else
                        return false;

                }
                catch { }
            }
            return false;
        }

        /// <summary>
        /// Stops the presentation playback
        /// </summary>
        public void Stop()
        {
			if(viewer != null)
			{
				try
				{
					lock (lockObject)
					{
						viewer.Visible = false;

                        try
                        {
                            objPres.Close();
                            //objApp.Quit();
                        }
                        catch { }

                        finally
                        {
                            objApp = null;
                            objPresSet = null;
                            objPres = null;
                            objSlides = null;
                            objSSW = null;

                            objSSS = null;
                        }

					}
					if (tm != null)
					{
						tm.Stop();
						tm = null;
					}
				}
				catch
				{
				}

			}
        }

		private void Clear()
		{

			this.Controls.Clear();

			if (viewer != null)
			{
				viewer.Dispose();
				viewer = null;
			}

			if(tm != null)
			{
				tm.Stop();
				tm = null;
			}
			GC.Collect();
		}
        private void ResizeViewer()
        {
            return;

            try
            {
                if (objSSW.HWND != 0)
                {
                    MethodInvoker methodProgress = delegate
                    {
                        lock (lockObject)
                        {
                            try
                            {
                                SetWindowPos((IntPtr)objSSW.HWND, (IntPtr)(-2), 0, 0, this.Width, this.Height, 0x0040);
                            }
                            catch { }
                        }
                    };

                    if (InvokeRequired)
                    {
                        IAsyncResult o = BeginInvoke(methodProgress);
                        o.AsyncWaitHandle.WaitOne();
                    }
                    else
                        methodProgress.Invoke();
                }
            }
            catch { }

        }

		private void PPTControl_SizeChanged(object sender, EventArgs e)
		{
			ResizeViewer();
		}

		public System.Drawing.Bitmap Thumbnail(int cx, int cy)
		{
			try
			{
				Bitmap bmp = new Bitmap(cx, cy, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
				using (Graphics g = Graphics.FromImage(bmp))
				{
					Point pos = this.PointToScreen(new Point(0, 0));
					g.CopyFromScreen(pos, new Point(0, 0), this.Size, CopyPixelOperation.SourceCopy);
				}

				return bmp;
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}

			return null;

		}

    }
}