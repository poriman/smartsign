﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DirectShowLib;
using DirectShowLib.DES;

using System.Runtime.InteropServices;
using DigitalSignage.Common;
using System.Configuration;
using NLog;
using MediaFoundation.EVR;
using MediaFoundation;

namespace DigitalSignage.Controls
{

    /// <summary>
    /// 미디어 상태 정보
    /// </summary>
	public enum MediaStatus
	{
        /// <summary>
        /// 재생됨
        /// </summary>
		Played,
        /// <summary>
        /// 정지됨
        /// </summary>
		Stopped,
        /// <summary>
        /// 중지됨
        /// </summary>
		Paused
	};

    
    /// <summary>
    /// COM 기반 Direct Show 미디어 컨트롤
    /// </summary>
	public partial class AxMediaControl : UserControl
	{
		private const int WMGraphNotify = 0x0400 + 13;
		private const int WMDisplayChanged = 0x007E;

		private IGraphBuilder graphBuilder = null;
		private IMediaControl mediaControl = null;
        private IMediaPosition mediaPosition = null;
		private IVideoWindow videoWindow = null;
        private IVMRWindowlessControl windowless = null;

        private IMFVideoDisplayControl evrMediaControl = null;

        private IBasicVideo basicVideo = null;
		private IBasicAudio basicAudio = null;
		private IMediaEventEx mediaEventEx = null;
		private IMediaSeeking mediaSeeking = null;

        private IQualProp qualProp = null;

        System.Threading.Timer tmSeeking = null;
        System.Threading.Timer tmShowingOSD = null;
        System.Threading.Timer tmPlaying = null;
        
        double dCurrentRate = 1.0;

        object lockObject = new object();

        public DateTime PlayStarted
        {
            get { return _dtPlayStarted; }
            set { _dtPlayStarted = value; }
        }

        DateTime _dtPlayStarted = DateTime.Now;

		public enum RenderFilters : int
		{
			None = 0,
			VideoRenderer = 1,
			VMR7 = 2,
			VMR9 = 3,
			EVR = 4
		}
        private static Logger logger = LogManager.GetCurrentClassLogger();
        
        /// <summary>
        /// 상태 이벤트 핸들러
        /// </summary>
        /// <param name="status"></param>
		public delegate void StatusEventHandler(MediaStatus status);
        /// <summary>
        /// 상태 이벤트 핸들러
        /// </summary>
		public event StatusEventHandler OnStateChanged = null;

		private double volume = 1.0;
		private bool isMute = false;
		private string filePath = "";

        /// <summary>
        /// 생성자
        /// </summary>
		public AxMediaControl()
		{
			InitializeComponent();
			this.Disposed += new EventHandler(AxMediaControl_Disposed);
		}

        /// <summary>
        /// 썸네일 추출
        /// </summary>
        /// <param name="cx"></param>
        /// <param name="cy"></param>
        /// <returns></returns>
		public System.Drawing.Bitmap Thumbnail(int cx, int cy)
		{
			try
			{
				MediaDet realmd = new MediaDet();
				IMediaDet md = realmd as IMediaDet;

				DsError.ThrowExceptionForHR(md.put_Filename(filePath));

				int nStreamNum = 0;
				DsError.ThrowExceptionForHR(md.get_OutputStreams(out nStreamNum));

				//	현재 스트림을 0으로 변경
				if (nStreamNum > 0) DsError.ThrowExceptionForHR(md.put_CurrentStream(0));

				for (int i = 0; i < nStreamNum; ++i)
				{
					AMMediaType mediaType = new AMMediaType();
					DsError.ThrowExceptionForHR(md.put_CurrentStream(i));
					DsError.ThrowExceptionForHR(md.get_StreamMediaType(mediaType));

					if (mediaType.majorType == MediaType.Video)
					{
						double dStreamLen = 0.0d;

						DsError.ThrowExceptionForHR(md.get_StreamLength(out dStreamLen));

						double dSeek = dStreamLen / 5;

						int nLength;
						DsError.ThrowExceptionForHR(md.GetBitmapBits(dSeek, out nLength, IntPtr.Zero, cx, cy));

						IntPtr ptr = Marshal.AllocHGlobal(nLength);
						DsError.ThrowExceptionForHR(md.GetBitmapBits(dSeek, out nLength, ptr, cx, cy));

						byte[] buffer = new byte[nLength + 14];

						//	타입
						buffer[0] = 0x42;
						buffer[1] = 0x4D;
						//	용량.. 2바이트
						buffer[2] = 0x3E;
						buffer[3] = 0x52;
						//	모지.
						buffer[10] = 0x36;

						Marshal.Copy(ptr, buffer, 14, nLength);
                        int nRetry = 10;
                        int nIndex = 0;
                        bool bSuccess = false;
                        string sTempFileName = "tempthumb.bmp";
                        
                        do
                        {
                            sTempFileName = String.Format("tempthumb{0}.bmp", nIndex);
                            try
                            { 
                                using (System.IO.FileStream writer = new System.IO.FileStream(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + sTempFileName, System.IO.FileMode.Create))
						        {

                                    writer.Write(buffer, 0, nLength + 14);
                                    writer.Close();
                                    bSuccess = true;
                                    break;
                                }
                            }
                            catch { }
                            nIndex++;
                           
                        } while (nRetry > nIndex);

                        if (bSuccess) return new Bitmap(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + sTempFileName);
                        else return null;
					}
				}
			}
			catch
			{
			}

			return null;

		}

        private T CreateComObject<T>(Guid guid) where T: class
        {
            Type comType = Type.GetTypeFromCLSID(guid);
            object o = Activator.CreateInstance(comType);
            if (o == null)
                return null;

            else
                return (T) o;
        }

        public void Play(string sSource, DateTime started)
        {
            _dtPlayStarted = started;

            Play(sSource);
        }

        /// <summary>
        /// 재생 함수
        /// </summary>
        /// <param name="sSource"></param>
		public void Play(string sSource)
		{
			Initialize();

			int hr = 0;
			string sContentsName = sSource.ToLower();
			if(sContentsName.StartsWith("http://") || 
				sContentsName.StartsWith("mms://") ||
				sContentsName.StartsWith("rtsp://"))
			{
				//	스트리밍 콘텐츠
				#region VMR 7
				IBaseFilter fwmasf = (IBaseFilter)new WMAsfReader();
				graphBuilder.AddFilter(fwmasf, "ASF Reader");
				
				IBaseFilter bfilter = (IBaseFilter)new VideoMixingRenderer();
				graphBuilder.AddFilter(bfilter, "VMR7");

				IFileSourceFilter source = (IFileSourceFilter)fwmasf;
				hr = source.Load(sSource, null);
				DsError.ThrowExceptionForHR(hr);

				IEnumPins pins = null;
				hr = fwmasf.EnumPins(out pins);
				DsError.ThrowExceptionForHR(hr);

				IPin[] pin = new IPin[1];
				IntPtr nPatched = IntPtr.Zero;
				while (0 == (hr = pins.Next(1, pin, nPatched)))
				{
					hr = graphBuilder.Render(pin[0]);
					DsError.ThrowExceptionForHR(hr);
				}

				IVMRAspectRatioControl arc = (IVMRAspectRatioControl)bfilter;
				hr = arc.SetAspectRatioMode(VMRAspectRatioMode.None);
				DsError.ThrowExceptionForHR(hr);

				IVMRFilterConfig fc = (IVMRFilterConfig)bfilter;
				hr = fc.SetRenderingPrefs(VMRRenderPrefs.AllowOverlays);
				DsError.ThrowExceptionForHR(hr);

				#endregion

			}
// 			else if(sContentsName.EndsWith(".wmv"))
// 			{
// 				#region Video Renderer
// 
// 				IBaseFilter bfilter = (IBaseFilter)new VideoRenderer();
// 				graphBuilder.AddFilter(bfilter, "VideoRenderer");
// 
// 				hr = graphBuilder.RenderFile(sSource, null);
// 				DsError.ThrowExceptionForHR(hr);
// 
// 				#endregion
// 			}
			else
			{
				RenderFilters renderfilter = RenderFilters.VMR7;
				try
				{
					object objRender = ConfigurationManager.AppSettings["Renderer"];
					renderfilter = objRender == null ? RenderFilters.VMR7 : (RenderFilters)Convert.ToInt32(objRender);
				}
				catch { renderfilter = RenderFilters.VMR7; }

				logger.Debug("Render Filter: {0}", renderfilter);

				if (renderfilter == RenderFilters.VideoRenderer)
				{
					#region Video Renderer
					IBaseFilter bfilter = (IBaseFilter)new VideoRenderer();
					graphBuilder.AddFilter(bfilter, "CustomFilters");

					hr = graphBuilder.RenderFile(sSource, null);
					DsError.ThrowExceptionForHR(hr);
					#endregion
				}
				else if (renderfilter == RenderFilters.VMR9)
				{
					#region VMR 9
					IBaseFilter bfilter = (IBaseFilter)new VideoMixingRenderer9();
					graphBuilder.AddFilter(bfilter, "CustomFilters");

					IVMRFilterConfig9 fc = (IVMRFilterConfig9)bfilter;

					IVMRAspectRatioControl9 arc = (IVMRAspectRatioControl9)bfilter;
					hr = arc.SetAspectRatioMode(VMRAspectRatioMode.None);
					DsError.ThrowExceptionForHR(hr);

					hr = graphBuilder.RenderFile(sSource, null);
					DsError.ThrowExceptionForHR(hr);

					hr = fc.SetRenderingPrefs(VMR9RenderPrefs.DoNotRenderBorder);

                    //qualProp = bfilter as IQualProp;

					DsError.ThrowExceptionForHR(hr);
					#endregion
				}
				else if (renderfilter == RenderFilters.VMR7)
				{
					#region VMR 7

					IBaseFilter bfilter = (IBaseFilter)new VideoMixingRenderer();
					graphBuilder.AddFilter(bfilter, "CustomFilters");

					IVMRFilterConfig fc = (IVMRFilterConfig)bfilter;


					/*
					hr = fc.SetRenderingMode(VMRMode.Windowless);
					DsError.ThrowExceptionForHR(hr);
                
					IVMRAspectRatioControl arc = (IVMRAspectRatioControl)bfilter;
					hr = arc.SetAspectRatioMode(VMRAspectRatioMode.None);
					DsError.ThrowExceptionForHR(hr);

					windowless = (IVMRWindowlessControl)bfilter;

					hr = windowless.SetVideoClippingWindow(this.Handle);
					DsError.ThrowExceptionForHR(hr);

					int lWidth, lHeight;
					int arWidth, arHeight;
					hr = windowless.GetNativeVideoSize(out lWidth, out lHeight, out arWidth, out arHeight);
					DsError.ThrowExceptionForHR(hr);

					DsRect rectSrc = new DsRect(0, 0, lWidth, lHeight);
					DsRect rectDest = new DsRect(0, 0, this.Width, this.Height);
					hr = windowless.SetVideoPosition(rectSrc, rectDest);
					DsError.ThrowExceptionForHR(hr);
					*/


					hr = graphBuilder.RenderFile(sSource, null);
					DsError.ThrowExceptionForHR(hr);

					IVMRAspectRatioControl arc = (IVMRAspectRatioControl)bfilter;
					hr = arc.SetAspectRatioMode(VMRAspectRatioMode.None);
					DsError.ThrowExceptionForHR(hr);

					hr = fc.SetRenderingPrefs(VMRRenderPrefs.AllowOverlays);

                    //qualProp = bfilter as IQualProp;

					DsError.ThrowExceptionForHR(hr);

					#endregion
				}
				else if (renderfilter == RenderFilters.EVR)
				{
					#region EVR
                    IBaseFilter bfilter = this.CreateComObject<IBaseFilter>(typeof(EnhancedVideoRenderer).GUID);
					graphBuilder.AddFilter(bfilter, "EVR");

					hr = graphBuilder.RenderFile(sSource, null);
					DsError.ThrowExceptionForHR(hr);

					try
					{
                        
                        IMFGetService mfs = bfilter as IMFGetService;

                        if (mfs != null)
                        {
                            object o;

                            hr = mfs.GetService(MediaFoundation.MFServices.MR_VIDEO_RENDER_SERVICE, typeof(IMFVideoDisplayControl).GUID, out o);
                            DsError.ThrowExceptionForHR(hr);

                            this.evrMediaControl = (IMFVideoDisplayControl)o;

                            hr = this.evrMediaControl.SetVideoWindow(this.Handle);
                            DsError.ThrowExceptionForHR(hr);

                            hr = this.evrMediaControl.SetAspectRatioMode(MFVideoAspectRatioMode.None);
                            DsError.ThrowExceptionForHR(hr);

                        }

                        if (mfs != null)
                            Marshal.ReleaseComObject(mfs); mfs = null;
                        
                        /*
						IVMRAspectRatioControl arc = (IVMRAspectRatioControl)bfilter;
						hr = arc.SetAspectRatioMode(VMRAspectRatioMode.None);
						DsError.ThrowExceptionForHR(hr);

						IVMRFilterConfig fc = (IVMRFilterConfig)bfilter;
						hr = fc.SetRenderingPrefs(VMRRenderPrefs.AllowOverlays);
						DsError.ThrowExceptionForHR(hr);
                        */
					}
					catch { }
					#endregion
				}
				else
				{
					#region System Default

					hr = graphBuilder.RenderFile(sSource, null);
					DsError.ThrowExceptionForHR(hr);

					#endregion
				}
            }

            logger.Debug("Render Pin Finished:");


			// QueryInterface for DirectShow interfaces
			QueryInterfaceForDirectShow();
			
            

			// Have the graph signal event via window callbacks for performance
			hr = this.mediaEventEx.SetNotifyWindow(this.Handle, WMGraphNotify, IntPtr.Zero);
			DsError.ThrowExceptionForHR(hr);

            if (windowless == null && evrMediaControl == null)
            {
                hr = this.videoWindow.put_Owner(this.Handle);
                DsError.ThrowExceptionForHR(hr);

                hr = this.videoWindow.put_WindowStyle(WindowStyle.Child | WindowStyle.ClipSiblings | WindowStyle.ClipChildren);
                DsError.ThrowExceptionForHR(hr);
                
                hr = this.videoWindow.SetWindowPosition(0, 0, this.Width, this.Height);

                DsError.ThrowExceptionForHR(hr);
            }
            else if (windowless == null && evrMediaControl != null)
            {
                this.evrMediaControl.SetVideoPosition(null, new MediaFoundation.Misc.MFRect(0,0, this.Width, this.Height));
            }

			basicAudio.put_Volume(isMute ? -10000 : Convert.ToInt32(GetDShowVolume())/*Convert.ToInt32((volume - 1.0) * 10000.0)*/);
		
			this.Focus();

            if (windowless == null && evrMediaControl == null)
            {
                hr = this.videoWindow.put_WindowState(WindowState.ShowNoActivate);
                DsError.ThrowExceptionForHR(hr);
            }

            #region Seek
            if (ScreenStartInfoInSchedule.GetInstance.IsUsingFirstSeek)
            {
                if (DateTime.Now > ScreenStartInfoInSchedule.GetInstance.ScreenStartDT) 
                {
                    procSeek(false);
                }
            }
            #endregion

            logger.Debug("Video Seeked!");

            //hr = this.mediaControl.Run();
            hr = this.mediaControl.Pause();
			DsError.ThrowExceptionForHR(hr);

			filePath = sSource;

			if (OnStateChanged != null) OnStateChanged(MediaStatus.Played);

            /// 타이머 생성
            closeTimer();


            dCurrentRate = 1.0;

            if (ScreenStartInfoInSchedule.GetInstance.IsUsingSeek)
            {
                tmSeeking = new System.Threading.Timer(seekCallBack, null, ScreenStartInfoInSchedule.GetInstance.DueTime, ScreenStartInfoInSchedule.GetInstance.DueTime);
            }

            if (OSDObject.GetInstance.HasOSDObject)
            {
                tmShowingOSD = new System.Threading.Timer(osdShowCallBack, null, 1000, 1000);
            }

            logger.Debug("AxMediaControl.Prepare: {0}", DateTime.Now);

            if (DateTime.Now >= ScreenStartInfoInSchedule.GetInstance.ScreenStartDT)
            {
                this.mediaControl.Run();
                logger.Debug("AxMediaControl.Play After: {0}", DateTime.Now.ToString("yyyyMMddHHmmssfff"));
            }
            else
            {
                thPlaying = new System.Threading.Thread(new System.Threading.ParameterizedThreadStart(thProcPlaying));
                thPlaying.Start(ScreenStartInfoInSchedule.GetInstance.ScreenStartDT);
            }

            ScreenStartInfoInSchedule.GetInstance.ScreenPaused += new EventHandler(GetInstance_ScreenPaused);
		}

        void thProcPlaying(object afterStartDT)
        {
            try
            {
                DateTime dt = (DateTime)afterStartDT;
                while (this.graphBuilder != null)
                {
                    if (DateTime.Now >= dt)
                    {
                        try
                        {
                            this.mediaControl.Run();

                        }
                        catch { }

                        logger.Debug("AxMediaControl.Play  Will: {0} After: {1}", dt.ToString("yyyyMMddHHmmssfff"), DateTime.Now.ToString("yyyyMMddHHmmssfff"));
                        break;
                    }
                    System.Threading.Thread.Sleep(1);
                }
            }
            catch { }

            thPlaying = null;
        }

        System.Threading.Thread thPlaying = null;

        void GetInstance_ScreenPaused(object sender, EventArgs e)
        {
            try
            {
                this.Pause();
                logger.Debug("AxMediaControl.Pause");
            }
            catch (Exception ex) { logger.Error(ex.Message); }
        }

        void osdShowCallBack(object state)
        {
            try
            {
                long lCurrPos, lEndPos;

                GetSeekPosition(out lCurrPos, out lEndPos);

                int nRate = 0;
                if (qualProp != null)
                    qualProp.get_AvgFrameRate(out nRate);

                TimeSpan ts = TimeSpan.FromMilliseconds((DateTime.Now - ScreenStartInfoInSchedule.GetInstance.ScreenStartDT).TotalMilliseconds % ((double)lEndPos / 10000.0f));
                long delay = (long)(ts.TotalMilliseconds - ((double)lCurrPos / 10000.0f));

                if (nRate == 0)
                    OSDObject.GetInstance.OnOSDChanged(this, String.Format("Delay: {0}ms", delay));
                else
                    OSDObject.GetInstance.OnOSDChanged(this, String.Format("Delay: {0}ms, Fps: {1:F2}", delay, (float)nRate / 100.0f));
            }
            catch { }
            
        }


        void closeTimer()
        {
            if (tmSeeking != null)
            {
                try
                {
                    tmSeeking.Dispose();
                }
                catch { }
                finally
                {
                    tmSeeking = null;
                }
            }

            if (tmShowingOSD != null)
            {
                try
                {
                    tmShowingOSD.Dispose();
                }
                catch { }
                finally
                {
                    tmShowingOSD = null;
                }
            }
        }

        DateTime dtControlTM = DateTime.Now;

        void rateCallBack(object state)
        {
            try
            {
                if (ScreenStartInfoInSchedule.GetInstance.IsSyncTask)
                {
                    lock (lockObject)
                    {
                        if ((DateTime.Now - dtControlTM).TotalSeconds < 5) return;

                        int threshold = DigitalSignage.Common.ScreenStartInfoInSchedule.GetInstance.SyncThreshold;
                        int adjustValue = Math.Abs(DigitalSignage.Common.ScreenStartInfoInSchedule.GetInstance.AdjustValue);

                        long lCurrPos, lEndPos;

                        GetSeekPosition(out lCurrPos, out lEndPos);

                        TimeSpan ts = TimeSpan.FromMilliseconds((DateTime.Now - _dtPlayStarted).TotalMilliseconds % ((double)lEndPos / 10000.0f));
                        long delay = (long)(ts.TotalMilliseconds - ((double)lCurrPos / 10000.0f));

                        if (Math.Abs((lEndPos / 10000.0f) - ((lCurrPos / 10000.0f) + ts.Milliseconds)) < threshold) return;

//                        TimeSpan ts = (DateTime.Now - _dtPlayStarted);
//                        long delay = (long)((ts.TotalMilliseconds - ((double)lCurrPos / 10000.0f)) % ((double)lEndPos / 10000.0f));

                        Console.Write("{0}, ", delay);

                        if (delay < -adjustValue && (dCurrentRate > 1.0))
                        {
                            SetSpeedRate(1.0);
                            WriteLog("Set Speed Rate: {0}", dCurrentRate);

                            dtControlTM = DateTime.Now;
                        }
                        else if (Math.Abs(delay) > threshold && dCurrentRate <= 1.0)
                        {

                            WriteLog("Current Position: {0}, Delay: {1}", lCurrPos / 10000, delay);

                            if (Math.Abs(delay) > 1000)
                            {
                                WriteLog("Current Position: {0}, Delay: {1}", lCurrPos / 10000, delay);

                                long lSeekValue = (((long)ts.TotalMilliseconds + adjustValue) * 10000);

                                WriteLog("Set Position: {0}", lSeekValue / 10000);

                                SetSeekPosition(lSeekValue);

                                dtControlTM = DateTime.Now;

                            }
                            else
                            {

                                if (delay > 0)
                                {
                                    SetSpeedRate(1.05);

                                    WriteLog("Set Speed Rate: {0}", dCurrentRate);
                                }
                                else
                                {

                                    SetSpeedRate(0.95);

                                    WriteLog("Set Speed Rate: {0}", dCurrentRate);

                                }
                            }
                        }

                    }
                }
            }
            catch { }
        }

        void seekCallBack(object state)
        {
            procSeek();
        }

        bool procSeek(bool bRunAfterSeek = true)
        {
            bool bDoSeek = false;
            try
            {
                if (ScreenStartInfoInSchedule.GetInstance.IsSyncTask)
                {
                    lock (lockObject)
                    {
                        int threshold = DigitalSignage.Common.ScreenStartInfoInSchedule.GetInstance.SyncThreshold;
                        int adjustValue = Math.Abs(DigitalSignage.Common.ScreenStartInfoInSchedule.GetInstance.AdjustValue);

                        long lCurrPos, lEndPos;

                        DateTime dtNow = DateTime.Now;
                        GetSeekPosition(out lCurrPos, out lEndPos);

                        TimeSpan ts = TimeSpan.FromMilliseconds((dtNow - /*_dtPlayStarted*/DigitalSignage.Common.ScreenStartInfoInSchedule.GetInstance.ScreenStartDT).TotalMilliseconds % ((double)lEndPos / 10000.0f));
                        long delay = (long)(ts.TotalMilliseconds - ((double)lCurrPos / 10000.0f));

                        Console.Write("{0}, ", delay);

                        if (Math.Abs(delay) > threshold)
                        {
                            int adjusttimepos = 1000;
                            DateTime dtStartingPoint = dtNow + TimeSpan.FromMilliseconds(adjusttimepos);

                            WriteLog("Current Position: {0}, Delay: {1}", lCurrPos / 10000, delay);

                            long lSeekValue = (((long)ts.TotalMilliseconds + adjusttimepos + adjustValue) * 10000);

                            WriteLog("Set Position: {0}", lSeekValue / 10000);

                            DateTime dtNow2 = DateTime.Now;

                            try
                            {
                            }
                            catch { }

                            
                            SetSeekPosition(lSeekValue);

                            try
                            {
                                if (bRunAfterSeek)
                                {
                                    this.mediaControl.Pause();

                                    thPlaying = new System.Threading.Thread(new System.Threading.ParameterizedThreadStart(thProcPlaying));
                                    thPlaying.Start(dtStartingPoint);
                                }
                            }
                            catch { }

                            bDoSeek = true;

                            WriteLog("Seeking Function Delay: {0}", (DateTime.Now - dtNow2).TotalMilliseconds);
                        }
                    }
                }
            }
            catch { }
            finally
            {
                //if(tmSeeking != null) tmSeeking.Change(bSeeking ? 5000 : 1000, bSeeking ? 5000 : 1000);
            }

            return bDoSeek;
        }

        private void WriteLog(string format, params object[] listParam)
        {
            logger.Debug(format, listParam);
            Console.WriteLine(format, listParam);
        }

        /// <summary>
        /// 볼륨 가져오기
        /// </summary>
        /// <returns></returns>
		double GetDShowVolume()
		{
			double lfPower10 = Math.Pow(10, -10);
			double lfVolume = (1 - lfPower10) * volume + lfPower10;
			lfVolume = 10 * Math.Log10(lfVolume);
			lfVolume *= 100;
			return lfVolume;
		}
		/// <summary>
		/// QueryInterface for DirectShow interfaces
		/// </summary>
		private void QueryInterfaceForDirectShow()
		{
			this.mediaControl = this.graphBuilder as IMediaControl;
			this.videoWindow = this.graphBuilder as IVideoWindow;
			this.basicAudio = this.graphBuilder as IBasicAudio;
			this.basicVideo = this.graphBuilder as IBasicVideo;
			this.mediaEventEx = this.graphBuilder as IMediaEventEx;
			this.mediaSeeking = this.graphBuilder as IMediaSeeking;
            this.mediaPosition = this.graphBuilder as IMediaPosition;

		}

		public void Pause()
		{
			if (mediaControl != null)
			{
				mediaControl.Pause();
			}
		}

        /// <summary>
        /// 정지
        /// </summary>
		public void Stop()
		{
            logger.Debug("AxMediaControl.Stoping");

            closeTimer();

			int hr = 0;
			if (this.mediaEventEx != null)
			{
				hr = this.mediaEventEx.SetNotifyWindow(IntPtr.Zero, 0, IntPtr.Zero);
				DsError.ThrowExceptionForHR(hr);
			}

			if (mediaControl != null)
			{
				mediaControl.Pause();
			}
            if (windowless != null)
            {

            }
            else if (videoWindow != null && evrMediaControl == null)
			{
				hr = this.videoWindow.put_Visible(OABool.False);
				DsError.ThrowExceptionForHR(hr);
// 					hr = this.videoWindow.SetWindowPosition(0, 0, 0, 0);
// 					DsError.ThrowExceptionForHR(hr);
				hr = this.videoWindow.put_Owner(IntPtr.Zero);
				DsError.ThrowExceptionForHR(hr);
			}
            else if (evrMediaControl != null)
            {
                try
                {
                    evrMediaControl.SetVideoWindow(IntPtr.Zero);
                }
                catch { }
            }

			if (OnStateChanged != null) OnStateChanged(MediaStatus.Paused);


			Clear();

			filePath = "";

            if (thPlaying != null)
            {
                try
                {
                    thPlaying.Abort();
                }
                catch { }
                finally
                {
                    thPlaying = null;
                }
            }

            logger.Debug("AxMediaControl.Stoped");

		}

        /// <summary>
        /// 포지션 가져오기
        /// </summary>
        /// <param name="lPos"></param>
        /// <param name="lEndPos"></param>
        /// <returns></returns>
        private int GetSeekPosition(out long lPos, out long lEndPos)
        {
            lPos = -1;
            lEndPos = -1;
            try
            {
                
                // Reset to first frame of movie
                return this.mediaSeeking.GetPositions(out lPos, out lEndPos);
            }
            catch {}

            return -1;
        }

        /// <summary>
        /// 속도 조절
        /// </summary>
        /// <param name="dSpeed"></param>
        /// <returns></returns>
        private int SetSpeedRate(double dSpeed)
        {
            try
            {
                if (dCurrentRate == dSpeed) return 0;

                //int hr = this.mediaPosition.put_Rate(dCurrentRate = dSpeed);
                int hr = this.mediaSeeking.SetRate(dCurrentRate = dSpeed);
                
                return hr;
            }
            catch { }

            return -1;
        }

        /// <summary>
        /// 포지션 설정
        /// </summary>
        /// <param name="nPos"></param>
        /// <returns></returns>
        private int SetSeekPosition(long nPos)
        {
            try
            {
                DsLong pos = new DsLong(nPos);
                // Reset to first frame of movie
                int hr = this.mediaSeeking.SetPositions(pos, AMSeekingSeekingFlags.AbsolutePositioning,
                  null, AMSeekingSeekingFlags.NoPositioning);
                if (hr < 0)
                {
                    // Some custom filters (like the Windows CE MIDI filter)
                    // may not implement seeking interfaces (IMediaSeeking)
                    // to allow seeking to the start.  In that case, just stop
                    // and restart for the same effect.  This should not be
                    // necessary in most cases.
                    try
                    {
                        //hr = this.mediaControl.Stop();
                        //hr = this.mediaControl.Run();
                    }
                    catch
                    {

                    }
                }

                return hr;
            }
            catch { }

            return -1;
        }

        /// <summary>
        /// 객체 소멸
        /// </summary>
		private void Clear()
		{
            try
            {
                if (this.qualProp != null)
                    Marshal.ReleaseComObject(this.qualProp); this.qualProp = null;
            }
            catch { }

			// Release and zero DirectShow interfaces
			if (this.mediaControl != null)
                Marshal.ReleaseComObject(this.mediaControl); this.mediaControl = null;
			if (this.basicAudio != null)
                Marshal.ReleaseComObject(this.basicAudio); this.basicAudio = null;
			if (this.basicVideo != null)
                Marshal.ReleaseComObject(this.basicVideo); this.basicVideo = null;
			if (this.videoWindow != null)
                Marshal.ReleaseComObject(this.videoWindow); this.videoWindow = null;
			if (this.mediaEventEx != null)
                Marshal.ReleaseComObject(this.mediaEventEx); this.mediaEventEx = null;
			if (this.mediaSeeking != null)
                Marshal.ReleaseComObject(this.mediaSeeking); this.mediaSeeking = null;
            if(this.evrMediaControl != null)
                Marshal.FinalReleaseComObject(evrMediaControl); evrMediaControl = null;
			if (this.mediaPosition != null)
                Marshal.ReleaseComObject(this.mediaPosition); this.mediaPosition = null;

            if (this.windowless != null)
                Marshal.ReleaseComObject(this.windowless); this.windowless = null;

			if (this.graphBuilder != null)
				Marshal.ReleaseComObject(this.graphBuilder); this.graphBuilder = null;

            try
            {
                ScreenStartInfoInSchedule.GetInstance.ScreenPaused -= new EventHandler(GetInstance_ScreenPaused);
            }
            catch { }

			GC.Collect();
		}
		#region Events
        /// <summary>
        /// 윈도우 이벤트 오버라이딩
        /// </summary>
        /// <param name="m"></param>
		protected override void WndProc(ref Message m)
		{
			try
			{
				switch (m.Msg)
				{
					case WMGraphNotify:
						{
							HandleGraphEvent();
							break;
						}
					case WMDisplayChanged:
						{
							if (videoWindow != null)
							{
								videoWindow.put_WindowState(WindowState.Restore);
							}
							break;
						}
				}

				// Pass this message to the video window for notification of system changes
				if (this.videoWindow != null)
					this.videoWindow.NotifyOwnerMessage((IntPtr)m.HWnd, m.Msg, m.WParam, m.LParam);
			}
			catch { }

			base.WndProc (ref m);
		}

        /// <summary>
        /// 핸들 이벤트 캡처링
        /// </summary>
		private void HandleGraphEvent()
		{
			try
			{
				int hr = 0;
				EventCode evCode;
				IntPtr evParam1, evParam2;

				// Make sure that we don't access the media event interface
				// after it has already been released.
				if (this.mediaEventEx == null)
					return;

				// Process all queued events
				while (this.mediaEventEx.GetEvent(out evCode, out evParam1, out evParam2, 0) == 0)
				{
					// Free memory associated with callback, since we're not using it
					hr = this.mediaEventEx.FreeEventParams(evCode, evParam1, evParam2);

					// If this is the end of the clip, reset to beginning

					if (evCode == EventCode.Complete)
					{
                        lock (lockObject)
                        {
                            DsLong pos = new DsLong(0);
                            // Reset to first frame of movie

                            hr = this.mediaSeeking.SetPositions(pos, AMSeekingSeekingFlags.AbsolutePositioning,
                              null, AMSeekingSeekingFlags.NoPositioning);
                            if (hr < 0)
                            {
                                // Some custom filters (like the Windows CE MIDI filter)
                                // may not implement seeking interfaces (IMediaSeeking)
                                // to allow seeking to the start.  In that case, just stop
                                // and restart for the same effect.  This should not be
                                // necessary in most cases.
                                try
                                {
                                    hr = this.mediaControl.Stop();
                                    hr = this.mediaControl.Run();
                                }
                                catch
                                {

                                }
                            }
                        }
					}
				}
			}
			catch
			{

			}
		}
		#endregion

		#region Properties
        /// <summary>
        /// 볼륨 속성
        /// </summary>
		public double Volume
		{
			get{
				return volume;
			}
			set{
				volume = value;
				if (null != basicAudio)
				{
					//	볼륨을 변경하는 루틴을 수행하자
					basicAudio.put_Volume(isMute ? -10000 : Convert.ToInt32(GetDShowVolume())/*Convert.ToInt32((volume - 1.0) * 10000.0)*/);
				}
			}
		}

        /// <summary>
        /// 음소거 여부
        /// </summary>
		public bool IsMuted
		{
			get
			{
				return isMute;
			}
			set
			{
				isMute = value;
				if (null != basicAudio)
				{
					//	볼륨을 변경하는 루틴을 수행하자
					basicAudio.put_Volume(isMute ? -10000 : Convert.ToInt32(GetDShowVolume())/*Convert.ToInt32((volume - 1.0) * 10000.0)*/);
				}
			}
		}

        /// <summary>
        /// 현재 재생 컨텐트
        /// </summary>
		public string CurrentFilePath
		{
			get {
				return filePath;
			}
		}
		#endregion

        /// <summary>
        /// 사이즈 변경 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
		private void AxMediaControl_SizeChanged(object sender, EventArgs e)
		{
			try
			{
                if (windowless == null)
                {
                    if (videoWindow == null)
                        return;

                    int hr = 0;
                    hr = this.videoWindow.SetWindowPosition(0, 0, this.Width, this.Height);
                    DsError.ThrowExceptionForHR(hr);
                }
                else if (windowless == null && evrMediaControl != null)
                {
                    this.evrMediaControl.SetVideoPosition(null, new MediaFoundation.Misc.MFRect(0, 0, this.Width, this.Height));
                }
                else
                {
                    int hr = 0;
                    int lWidth, lHeight;
                    int arWidth, arHeight;
                    hr = windowless.GetNativeVideoSize(out lWidth, out lHeight, out arWidth, out arHeight);
                    DsError.ThrowExceptionForHR(hr);

                    DsRect rectSrc = new DsRect(0, 0, lWidth, lHeight);
                    DsRect rectDest = new DsRect(0, 0, this.Width, this.Height);
                    hr = windowless.SetVideoPosition(rectSrc, rectDest);
                    DsError.ThrowExceptionForHR(hr);
                }
			}
			catch{}
		}

        /// <summary>
        /// 객체 소멸 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
		private void AxMediaControl_Disposed(object sender, EventArgs e)
		{
			Stop();
		}

        /// <summary>
        /// 객체 생성 시 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
		private void AxMediaControl_Loaded(object sender, EventArgs e)
		{
		}

        /// <summary>
        /// 객체 초기화
        /// </summary>
		private void Initialize()
		{

			if (null != graphBuilder)
			{
				Stop();
			}

			graphBuilder = (IGraphBuilder)new FilterGraph();
		}
	}
}
