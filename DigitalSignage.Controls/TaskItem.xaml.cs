﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DigitalSignage.Controls
{
	/// <summary>
	/// Interaction logic for TaskItem.xaml
	/// </summary>
	public partial class TaskItem : UserControl
	{
		public TaskItem()
		{
			InitializeComponent();
		}

		public Brush Border
		{
			set
			{
				border.BorderThickness = new Thickness(1);
				border.BorderBrush = value;
			}
		}
		public String Text
		{
			get
			{
				return text.Text;
			}
			set
			{
				text.Text = value;
			}
		}
		public String TextToolTip
		{
			get
			{
				return textToolTip.Text;
			}
			set
			{
				textToolTip.Text = value;
			}
		}
	
		public object DataContextWithMakeToolTip
		{
			set
			{
				DataContext = value;

				DigitalSignage.Common.Task task = value as DigitalSignage.Common.Task;

				TextToolTip = task.ToTooltipString();
			}
		}

	}
}
