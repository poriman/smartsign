﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using DigitalSignage.Common;

namespace DigitalSignage.Controls
{
    /// <summary>
    /// ActiveX 기반 Flash 컨트롤
    /// </summary>
    public partial class FlashControl : UserControl
    {
		private string sRefPath = null;

		IHookFlashControl viewer = null;

        /// <summary>
        /// 플래시 컨텍스트 매뉴 오픈 이벤트
        /// </summary>
		public event EventHandler OnContextOpen = null;

        System.Threading.Timer tmPlaying = null;

        /// <summary>
        /// 외부데이터 경로
        /// </summary>
		public String RefDataPath
		{
			get { return sRefPath; }
			set { sRefPath = value; }
		}

        /// <summary>
        /// 생성자
        /// </summary>
        public FlashControl()
        {
            InitializeComponent();
			this.Disposed += new EventHandler(UserControl_Disposed);

            viewer = InitFlash();
        }

        /// <summary>
        /// 썸네일 추출
        /// </summary>
        /// <param name="cx"></param>
        /// <param name="cy"></param>
        /// <returns></returns>
		public System.Drawing.Bitmap Thumbnail(int cx, int cy)
		{
			try
			{
				Bitmap bmp = new Bitmap(cx, cy, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
				using (Graphics g = Graphics.FromImage(bmp))
				{
					Point pos = this.PointToScreen(new Point(0, 0));
					g.CopyFromScreen(pos, new Point(0, 0), this.Size, CopyPixelOperation.SourceCopy);
				}

				return bmp;
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}

			return null;

		}
        /// <summary>
        /// Plays the loaded file
        /// </summary>
        public void Play(string source)
        {
			//IHookFlashControl flash = InitFlash();

            if (viewer == null) viewer = InitFlash();

// 			viewer.LoadMovie(0, source);
// 			viewer.LoadMovie(0, source);
            viewer.Movie = source;
            viewer.SetVariable("xml_path", RefDataPath);
// 			this.Visible = true;

            /*
            flash.BackgroundColor = -1;
            flash.WMode = "transparent";
            flash.BGColor = "#CCCCCC";
            */

// 			ResizeFlash();
            //flash.Play();
            viewer.StopPlay();

            /*
			if (viewer != null)
			{
				viewer.Stop();
				viewer.Dispose();
				viewer = null;
			}

			viewer = flash;
            */
            tmPlaying = new System.Threading.Timer(playingCallBack, null, 0, 10);

            DigitalSignage.Common.ScreenStartInfoInSchedule.GetInstance.ScreenPaused += new EventHandler(GetInstance_ScreenPaused);
        }

        void playingCallBack(object state)
        {
            try
            {
                if (DateTime.Now >= ScreenStartInfoInSchedule.GetInstance.ScheduleStartDT)
                {
                    viewer.Play();

                    if (tmPlaying != null)
                    {
                        try
                        {
                            tmPlaying.Dispose();
                        }
                        catch { }
                        finally
                        {
                            tmPlaying = null;
                        }
                    }
                }
            }
            catch { }
        }


        void GetInstance_ScreenPaused(object sender, EventArgs e)
        {
            try
            {
                viewer.StopPlay();
            }
            catch { }
        }

		public void Pause()
		{
			if (viewer != null)
			{
                viewer.StopPlay();
			}
		}
        /// <summary>
        /// 플래시 초기화
        /// </summary>
        /// <returns></returns>
		private IHookFlashControl InitFlash()
		{
			IHookFlashControl flash = new IHookFlashControl();

			((System.ComponentModel.ISupportInitialize)(flash)).BeginInit();
			this.SuspendLayout();

			flash.Enabled = false;
			flash.Name = "viewer";
			flash.TabIndex = 0;


			this.panelFlashHolder.Controls.Add(flash);


			((System.ComponentModel.ISupportInitialize)(flash)).EndInit();
			this.ResumeLayout(false);
			flash.Size = new Size(this.Size.Width, this.Size.Height);

			flash.ScaleMode = 2;
			flash.Loop = true;
			flash.OnRightButton_Clicked += new EventHandler(flash_OnRightButton_Clicked);

			return flash;
		}

        /// <summary>
        /// 오른쪽 버튼 눌렀을때
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
		void flash_OnRightButton_Clicked(object sender, EventArgs e)
		{
			if (OnContextOpen != null) OnContextOpen(sender, e);
		}

        /// <summary>
        /// Stops playing
        /// </summary>
        public void Stop()
        {
			if (viewer != null)
			{
// 				this.Visible = false;
// 				viewer.Stop();
				viewer.StopPlay();
// 				Clear();
			}

            if (tmPlaying != null)
            {
                try
                {
                    tmPlaying.Dispose();
                }
                catch { }
                finally
                {
                    tmPlaying = null;
                }
            }

		}

        /// <summary>
        /// 객체 소멸
        /// </summary>
		public void Clear()
		{

			this.panelFlashHolder.Controls.Clear();

			if(viewer != null)
			{
				viewer.OnRightButton_Clicked -= new EventHandler(flash_OnRightButton_Clicked);
				viewer.Stop();
				viewer.Dispose();
				viewer = null;
			}

            try
            {
                DigitalSignage.Common.ScreenStartInfoInSchedule.GetInstance.ScreenPaused -= new EventHandler(GetInstance_ScreenPaused);
            }
            catch { }
		}

        /// <summary>
        /// 객체가 소멸될때 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
		public void UserControl_Disposed(Object sender, EventArgs args)
		{
			try
			{
				Clear();               				
			}
			catch
			{
				
			}
		}

        /// <summary>
        /// 플래시 크기 조절
        /// </summary>
		private void ResizeFlash()
		{
			try
			{
				if (viewer != null)
					viewer.Size = new Size(this.Size.Width, this.Size.Height);
			}
			catch
			{
				
			}
		}

        /// <summary>
        /// 사이즈 변경 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
		private void FlashControl_SizeChanged(object sender, EventArgs e)
		{
			ResizeFlash();
		}
    }

	/// <summary>
	/// 마우스 오른쪽을 막는 플래시 컨트롤
	/// </summary>
	public class IHookFlashControl : AxShockwaveFlashObjects.AxShockwaveFlash
	{
		#region 오른쪽 버튼 이벤트
		const int WM_RBUTTONDOWN = 0x0204;

        /// <summary>
        /// 마우스 오른쪽 버튼 이벤트
        /// </summary>
		public event EventHandler OnRightButton_Clicked = null;

        /// <summary>
        /// 윈도우 메시지 후킹
        /// </summary>
        /// <param name="m"></param>
		protected override void WndProc(ref Message m)
		{
			if (m.Msg == WM_RBUTTONDOWN)
			{
				m.Result = IntPtr.Zero;
				if (OnRightButton_Clicked != null) OnRightButton_Clicked(this, null);
				return;
			}
			base.WndProc(ref m);
		}
		#endregion
	}
}
