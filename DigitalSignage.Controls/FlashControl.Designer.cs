﻿namespace DigitalSignage.Controls
{
    /// <summary>
    /// Flash ActiveX 컨트롤 
    /// </summary>
    partial class FlashControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelFlashHolder = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // panelFlashHolder
            // 
            this.panelFlashHolder.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelFlashHolder.Location = new System.Drawing.Point(0, 0);
            this.panelFlashHolder.Margin = new System.Windows.Forms.Padding(0);
            this.panelFlashHolder.Name = "panelFlashHolder";
            this.panelFlashHolder.Size = new System.Drawing.Size(266, 231);
            this.panelFlashHolder.TabIndex = 0;
            // 
            // FlashControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gray;
            this.Controls.Add(this.panelFlashHolder);
            this.DoubleBuffered = true;
            this.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "FlashControl";
            this.Size = new System.Drawing.Size(266, 231);
            this.SizeChanged += new System.EventHandler(this.FlashControl_SizeChanged);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelFlashHolder;


    }
}
