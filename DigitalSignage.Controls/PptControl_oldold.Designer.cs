﻿namespace DigitalSignage.Controls
{
    partial class PPTControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PPTControl));
			this.viewer = new AxOfficeViewer.AxOfficeViewer();
			((System.ComponentModel.ISupportInitialize)(this.viewer)).BeginInit();
			this.SuspendLayout();
			// 
			// viewer
			// 
			this.viewer.Enabled = true;
			this.viewer.Location = new System.Drawing.Point(0, 0);
			this.viewer.Name = "viewer";
			this.viewer.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("viewer.OcxState")));
			this.viewer.Size = new System.Drawing.Size(175, 138);
			this.viewer.TabIndex = 0;
			this.viewer.Visible = false;
			// 
			// PPTControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.viewer);
			this.Name = "PPTControl";
			this.SizeChanged += new System.EventHandler(this.PPTControl_SizeChanged);
			((System.ComponentModel.ISupportInitialize)(this.viewer)).EndInit();
			this.ResumeLayout(false);

        }

        #endregion

        private AxOfficeViewer.AxOfficeViewer viewer;

    }
}
