﻿using System;
using System.IO;
using System.Collections;
using System.Xml;
using System.Linq;
using System.Text;

using DigitalSignage.Common;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace DigitalSignage.Scheduler
{
    class ProgramUploader
    {
// 		public delegate void EventHandler(Object sender, EventArgs e);
// 		public delegate void EventHandler(Object sender, EventArgs e);
		public EventHandler OnSetProgress = null;
		public EventHandler OnStepIt = null;
		public StatusEventHandler OnUpdateLabel = null;
		public event StatusEventHandler OnUpdateTitle = null;
		
        private string tempDir = null;
         private Config config = null;

		/// <summary>
		/// 로드된 페이지 목록을 가져온다 
		/// </summary>
		public Collection<ScreenPropertyLoader> LoadedScreenProperties = null;


		private string lastErrorMessage;

		public string LastErrorMessage
		{
			get { return lastErrorMessage; }
		}

		#region 광고 관련 프로퍼티 클래스


		#endregion

		public bool PrepareUpload(ref string programName, Config cfg, Guid duuid)
		{
			using (new DigitalSignage.Common.WaitCursor())
			{
				lastErrorMessage = "";

				//	hsshin
				if (OnUpdateTitle != null) OnUpdateTitle(this, new StatusEventArgs(Properties.Resources.progNowPreparing));

				config = cfg;

				// creating temporary program directory
				String sExecutingPath = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
				tempDir = sExecutingPath + "\\SchedulerData\\temp\\" + duuid.ToString();
				if (!Directory.Exists(tempDir))
					Directory.CreateDirectory(tempDir);

				if (programName.ToLower().EndsWith(".bin") || 
					programName.ToLower().EndsWith(".smil") ||
					programName.ToLower().EndsWith(".ipr") ||
					programName.ToLower().EndsWith(".xml") ||
                    programName.ToLower().EndsWith(".tpf")) //Touch : Touchflow file 확장자 추가.
				{
					//	hsshin 확장자가 BIN인경우는 중간 ipr과 ipl을 만들어줘야한다.
					if (programName.ToLower().EndsWith(".bin") || programName.ToLower().EndsWith(".smil"))
					{
						ScreenPropertyLoader ad = new ScreenPropertyLoader(programName);

// 						TimeSpan ts = GetDurationFromBin(programName);
						TimeSpan ts = new TimeSpan(23, 59, 59);
						if (MakeIPLFile(sExecutingPath, programName, ts, ad) &&
							MakeIPRFile(sExecutingPath, ts, ad))
						{
							programName = sExecutingPath + "\\SchedulerData\\temp\\tempPr.xml";
						}
						else
						{
							return false;
						}
					}
                    else if (programName.ToLower().EndsWith(".tpf"))//Touch : Touchflow file 확장자 추가.
                    {
						ScreenPropertyLoader ad = new ScreenPropertyLoader();
// 						TimeSpan ts = GetDurationFromBin(programName);
						TimeSpan ts = new TimeSpan(23, 59, 59);

						if (MakeIPLFile(sExecutingPath, programName, ts, ad) &&
							MakeIPRFile(sExecutingPath, ts, ad))
                        {
                            programName = sExecutingPath + "\\SchedulerData\\temp\\tempPr.xml";
                        }
                        else
                        {
                            return false;
                        }
                    }

					//	프로그램 만들기
					ivisionFileHelper helper = new ivisionFileHelper();
					if (!helper.MakeProgram(programName, tempDir, false))
					{
						lastErrorMessage = helper.LastErrorMessage;
						try
						{
							FTPFunctions.RecursiveDelete(tempDir, true);
						}
						catch {}
						return false;
					}

					this.LoadedScreenProperties = helper.LoadedScreenProperties;
				}
				else
					return true;
			}
			return true;
		}

		public bool DownloadProgram(Config cfg, XmlNode group, Guid duuid, String destPath)
		{
			using (new DigitalSignage.Common.WaitCursor())
			{
				try
				{
					//	hsshin
					String sTitle = String.Format("{0} ({1}({2}) - {3}@{4}/{5})", Properties.Resources.progNowDownloading, group.Attributes["name"].Value, group.Attributes["gid"].Value, group.Attributes["username"].Value, group.Attributes["source"].Value, group.Attributes["path"].Value);
					if (OnUpdateTitle != null) OnUpdateTitle(this, new StatusEventArgs(sTitle));

					// FTP transfer
					FTPFunctions ftp = new FTPFunctions(cfg.AnalyzeSource(group.Attributes["source"].Value), group.Attributes["path"].Value, group.Attributes["username"].Value, group.Attributes["password"].Value, cfg.FTP_Timeout);

					//	hsshin 이벤트 추가
					if (OnSetProgress != null) ftp.OnSetProgress += OnSetProgress;
					if (OnStepIt != null) ftp.OnStepIt += OnStepIt;
					if (OnUpdateLabel != null) ftp.OnUpdateLabel += OnUpdateLabel;

					ftp.GetFTPRecursiveFileCount(duuid.ToString());
							
					if(false == ftp.FTPRecursiveDownload(duuid.ToString(), destPath))
					{
						lastErrorMessage = String.Format(Properties.Resources.mbErrorDownloadFromFTP, String.Format("{0}({1}) - Cont.Svr.({2}@{3}/{4})", group.Attributes["name"].Value, group.Attributes["gid"].Value, group.Attributes["username"].Value, group.Attributes["source"].Value, group.Attributes["path"].Value), ftp.LastErrorMessage);
						return false;
					}

					return true;
				}
				catch (Exception ex)
				{
					lastErrorMessage = ex.Message + " - " + String.Format("{0}({1}) - Cont.Svr.({2}@{3}/{4})", group.Attributes["name"].Value, group.Attributes["gid"].Value, group.Attributes["username"].Value, group.Attributes["source"].Value, group.Attributes["path"].Value);

				}
			}
			return false;
		}

		public bool DeleteProgram(Config cfg, XmlNode group, Guid duuid)
		{
			using (new DigitalSignage.Common.WaitCursor())
			{
				try
				{
					// FTP transfer
					FTPFunctions ftp = new FTPFunctions(cfg.AnalyzeSource(group.Attributes["source"].Value), group.Attributes["path"].Value, group.Attributes["username"].Value, group.Attributes["password"].Value, cfg.FTP_Timeout);
					return ftp.FTPRecursiveDelete(duuid.ToString());
				}
				catch {}
			}
			return false;
		}
        public Guid UploadProgram(Config cfg, XmlNode group, Guid duuid)
        {
			using (new DigitalSignage.Common.WaitCursor())
			{
				try
				{
					//	hsshin
					String sTitle = String.Format("{0} ({1}({2}) - {3}@{4}/{5})", Properties.Resources.progNowUploading, group.Attributes["name"].Value, group.Attributes["gid"].Value, group.Attributes["username"].Value, group.Attributes["source"].Value, group.Attributes["path"].Value);
					if (OnUpdateTitle != null) OnUpdateTitle(this, new StatusEventArgs(sTitle));

					// FTP transfer
					FTPFunctions ftp = new FTPFunctions(cfg.AnalyzeSource(group.Attributes["source"].Value), group.Attributes["path"].Value, group.Attributes["username"].Value, group.Attributes["password"].Value, cfg.FTP_Timeout);

					//	hsshin 이벤트 추가
					if (OnSetProgress != null) ftp.OnSetProgress += OnSetProgress;
					if (OnStepIt != null) ftp.OnStepIt += OnStepIt;
					if (OnUpdateLabel != null) ftp.OnUpdateLabel += OnUpdateLabel;

					ftp.GetRecursiveFileCount(tempDir);

					//	업로드 실패
					if (false == ftp.FTPRecursiveCopy(tempDir, duuid.ToString(), "", false))
					{

						lastErrorMessage = String.Format(Properties.Resources.mbErrorUploadToFTP, String.Format("{0}({1}) - Cont.Svr.({2}@{3}/{4})", group.Attributes["name"].Value, group.Attributes["gid"].Value, group.Attributes["username"].Value, group.Attributes["source"].Value, group.Attributes["path"].Value), ftp.LastErrorMessage);
						return Guid.Empty;
					}

					return duuid;

				}
				catch (Exception ee)
				{
					lastErrorMessage = ee.Message + " - " + String.Format("{0}({1}) - Cont.Svr.({2}@{3}/{4})", group.Attributes["name"].Value, group.Attributes["gid"].Value, group.Attributes["username"].Value, group.Attributes["source"].Value, group.Attributes["path"].Value);
				}
			}
			return Guid.Empty;
        }
		public void RemoveTemporaryFiles()
		{
			RemoveLocalFiles(tempDir);

		}
		public void RemoveLocalFiles(String localpath)
		{
			using (new DigitalSignage.Common.WaitCursor())
			{
				try
				{
					//removing temporary directory
					FTPFunctions.RecursiveDelete(localpath, true);
				}
				catch { }
			}
		}

		private TimeSpan GetDurationFromBin(string programName)
		{
			//	Dration 추출
			TimeSpan ts;
			FileStream fileStream = new FileStream(programName, FileMode.Open, FileAccess.Read);
			BinaryFormatter binaryFormatter = new BinaryFormatter();
			List<Dictionary<string, object>> propertiesSet = new List<Dictionary<string, object>>();

			try
			{
				propertiesSet = binaryFormatter.Deserialize(fileStream) as List<Dictionary<string, object>>;
				ts = (TimeSpan)propertiesSet[0]["PlayTime"];
				if (ts.TotalSeconds == 0)
					throw new Exception("a day looping");
			}
			catch
			{
				ts = new TimeSpan(23, 59, 59);
			}
			fileStream.Close();
			
			return ts;
		}



		private bool MakeIPLFile(string sExecutingPath, string programName, TimeSpan ts, ScreenPropertyLoader ad)
		{
			try
			{
				//	PlayList파일을 만들어준다
                String sIPLFormat = "<?xml version=\"1.0\"?><playlist Count=\"1\" IsAd=\"{2}\" PlayConstrait=\"{3}\" AdStartTime=\"{4}\" AdEndTime=\"{5}\" MetaTags=\"{6}\"><screen time=\"{0}\" selected=\"1\" ID=\"{1}\" IsAd=\"{2}\" PlayConstrait=\"{3}\" AdStartTime=\"{4}\" AdEndTime=\"{5}\" MetaTags=\"{6}\"/></playlist>";
				String sIPL = String.Format(sIPLFormat, TimeConverter.TimeToStr(ts.Hours, ts.Minutes, ts.Seconds), programName, ad.IsAd, ad.PlayConstrait, ad.StartDateTime, ad.EndDateTime, ad.MetaTags);

				StreamWriter fsPL = new StreamWriter(sExecutingPath + "\\SchedulerData\\temp\\tempPl.xml", false);

				if (fsPL != null)
				{
					fsPL.Write(sIPL);
					fsPL.Flush();
					fsPL.Close();
					fsPL.Dispose();
				}
				return true;
			}
			catch (Exception e)
			{
				lastErrorMessage = Properties.Resources.errorMakeIPLFile + "\n" + e.Message;
				return false;
			}
		}
		private bool MakeIPRFile(string sExecutingPath, TimeSpan ts, ScreenPropertyLoader ad)
		{
			try
			{
				//	Program 파일을 만들어준다
                String sIPRFormat = "<?xml version=\"1.0\"?><program TotalScreenCount=\"1\" PaneCount=\"1\" ID=\"NewProgram\" IsAd=\"{2}\" PlayConstrait=\"{3}\" AdStartTime=\"{4}\" AdEndTime=\"{5}\" MetaTags=\"{6}\"><pane playlist=\"{0}\" duration=\"{1}\"/></program>";
				String sIPR = String.Format(sIPRFormat, sExecutingPath + "\\SchedulerData\\temp\\tempPl.xml", TimeConverter.TimeToStr(ts.Hours, ts.Minutes, ts.Seconds), ad.IsAd, ad.PlayConstrait, ad.StartDateTime, ad.EndDateTime, ad.MetaTags);

				StreamWriter fsPR = new StreamWriter(sExecutingPath + "\\SchedulerData\\temp\\tempPr.xml", false);
				if (fsPR != null)
				{
					fsPR.Write(sIPR);
					fsPR.Flush();
					fsPR.Close();
					fsPR.Dispose();
				}
				return true;
			}
			catch (Exception e)
			{
				lastErrorMessage = Properties.Resources.errorMakeIPLFile + "\n" + e.Message;
				return false;
			}
		}
     }
}
