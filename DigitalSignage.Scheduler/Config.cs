﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Security.Cryptography;
using System.Reflection;

using DigitalSignage.Common;

using NLog;
using DigitalSignage.SerialKey;
using System.Runtime.InteropServices;

    public class Config
    {
        #region SYSTEMTIME definition
        [DllImport("kernel32.dll")]
        private static extern bool SetSystemTime([In] ref SYSTEMTIME st);
        struct SYSTEMTIME
        {
            public short wYear;
            public short wMonth;
            public short wDayOfWeek;
            public short wDay;
            public short wHour;
            public short wMinute;
            public short wSecond;
            public short wMilliseconds;
        }
        #endregion

        public static string ROOT_GROUP_ID = "0000000000000";

        private static Logger logger = LogManager.GetCurrentClassLogger();
        private static Config instance = null;
        private static readonly object semaphore = new object();

        public static string SAAS_DOMAIN = "tcp://saas.myivision.com:888/";

        public static Config GetConfig
        {
            get
            {
                lock (semaphore)
                {
                    if (instance == null)
                    {
                        instance = new Config();
                        instance.init();
                    }
                    return instance;
                }
            }
        }

        private string appData;
        private string resData;
        ConfigSettings settings = null;
        ServerConfigSettings server_settings = null;

        // remote interfaces
        IServerCmdReceiver serverCmdReceiver = null;
        ITaskList serverTasksList = null;
        IGroupList serverGroupsList = null;
        IPlayersList serverPlayersList = null;
        IUsersList serverUsersList = null;
        ILogList serverLogList = null;
        IDownloadList serverDownloadList = null;
        IDownloadTimer serverDownloadTimer = null;
        IDataCacheList serverDataCacheList = null;

        IDataInfoList serverDataInfoList = null;
        IDataFieldList serverDataFieldList = null;
        IContentsList serverContentsList = null;
        IResultDataList serverResultDataList = null;

        // server version
        public long ServerVersion;
        // scheduler version
        private long version;
        //
        public bool ServerFound = false;

        public int FTP_Timeout = 10000;

        private NESerialKey._ProductCode _productCode = NESerialKey._ProductCode.Undefined;

        public NESerialKey._ProductCode ProductCode
        {
            get { return _productCode; }
            set { _productCode = value; }
        }

        private AuthTicket AT = new AuthTicket();

        public Config()
        {
            String sExecutingPath = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            appData = sExecutingPath + "\\SchedulerData\\";
            if (!Directory.Exists(appData))
                Directory.CreateDirectory(appData);

            try
            {
                FTP_Timeout = int.Parse(DigitalSignage.Scheduler.Properties.Settings.Default.FTP_Timeout);
            }
            catch
            {
                FTP_Timeout = 10000;
            }
        }
        public NESerialKey._ProductCode CheckProductCode()
        {
            try
            {
                // 			// SaaS 업데이트를 위해 프로덕트 타입을 SaaS로 입력해 준다. //////////////
                // 			DigitalSignage.Common.KeyChecker.GetInstance.SetProductType(SerialKey._ProductCode.SpecialEdition);
                // 			//////////////////////////////////////////////////////////////////////////

                if (_productCode == SerialKey._ProductCode.Undefined)
                    _productCode = DigitalSignage.Common.KeyChecker.GetInstance.CheckProductType();


                return _productCode;
            }
            catch { return NESerialKey._ProductCode.Undefined; }
        }

        public void init()
        {
            try
            {
                CheckProductCode();

                // get own version
                Assembly myAssembly = Assembly.GetExecutingAssembly();
                AssemblyName myAssemblyName = myAssembly.GetName();
                version = ((long)myAssemblyName.Version.Major << 48) |
                    ((long)myAssemblyName.Version.Minor << 32) |
                    (myAssemblyName.Version.Build << 16) |
                    myAssemblyName.Version.Revision;
                resData = System.IO.Path.GetDirectoryName(myAssembly.Location) + "\\res";

                // reading configuration file
                settings = new ConfigSettings(appData + "config.xml");
                server_settings = new ServerConfigSettings(appData + "server_config.xml");

                logger.Info("DigitalSignage.Scheduler " + ((version >> 48) & 0xFFFF) + "." +
                    ((version >> 32) & 0xFFFF) + "." +
                    ((version >> 16) & 0xFFFF) + "." +
                    (version & 0xFFFF));

                ConnectToServer();
            }
            catch (Exception e)
            {
                // conection failed
                logger.Error(e + "");
                return;
            }
        }

        public bool ConnectToServer()
        {
            try
            {
                // initializing remote interfaces
                serverTasksList = (ITaskList)Activator.GetObject(
                    typeof(ITaskList), ServerPath + "TaskListHost/rt");
                serverCmdReceiver = (IServerCmdReceiver)Activator.GetObject(
                    typeof(IServerCmdReceiver), ServerPath + "CmdReceiverHost/rt");
                serverGroupsList = (IGroupList)Activator.GetObject(
                    typeof(IGroupList), ServerPath + "GroupListHost/rt");
                serverPlayersList = (IPlayersList)Activator.GetObject(
                    typeof(IPlayersList), ServerPath + "PlayerListHost/rt");
                serverUsersList = (IUsersList)Activator.GetObject(
                    typeof(IUsersList), ServerPath + "UsersListHost/rt");
                serverLogList = (ILogList)Activator.GetObject(
                    typeof(ILogList), ServerPath + "LogListHost/rt");
                serverDownloadList = (IDownloadList)Activator.GetObject(
                    typeof(IDownloadList), ServerPath + "DownloadListHost/rt");
                serverDownloadTimer = (IDownloadTimer)Activator.GetObject(
                    typeof(IDownloadTimer), ServerPath + "DownloadTimerHost/rt");
                serverDataCacheList = (IDataCacheList)Activator.GetObject(
                    typeof(IDataCacheList), ServerPath + "DataCacheHost/rt");

                serverDataInfoList = (IDataInfoList)Activator.GetObject(
                    typeof(IDataInfoList), ServerPath + "DataInfoHost/rt");
                serverDataFieldList = (IDataFieldList)Activator.GetObject(
                    typeof(IDataFieldList), ServerPath + "DataFieldHost/rt");
                serverContentsList = (IContentsList)Activator.GetObject(
                    typeof(IContentsList), ServerPath + "ContentsHost/rt");
                serverResultDataList = (IResultDataList)Activator.GetObject(
                    typeof(IResultDataList), ServerPath + "ResultDataHost/rt");

                ServerVersion = serverCmdReceiver.ProcessSimpleCommand(CmdReceiverCommands.CmdGetVersion);
                logger.Info("DigitalSignage.SchedulerService version " + ((ServerVersion >> 48) & 0xFFFF) + "." +
                        ((ServerVersion >> 32) & 0xFFFF) + "." +
                        ((ServerVersion >> 16) & 0xFFFF) + "." + (ServerVersion & 0xFFFF));

                try
                {
                    SYSTEMTIME st = new SYSTEMTIME();
                    long current_ts = ServerCmdReceiver.ProcessSimpleCommand(CmdReceiverCommands.CmdGetTimeStamp);
                    logger.Info("Server TS=" + current_ts);

                    DateTime ctDay = TimeConverter.ConvertFromUTP(current_ts);
                    st.wYear = (short)ctDay.Year;
                    st.wMonth = (short)ctDay.Month;
                    st.wDay = (short)ctDay.Day;
                    st.wHour = (short)ctDay.Hour;
                    st.wMinute = (short)ctDay.Minute;
                    st.wSecond = (short)(ctDay.Second + 1);
                    if (!SetSystemTime(ref st))
                        logger.Error("[Synchronize to server] SetSystemTime has been failed");
                }
                catch { }

            }
            catch (Exception e)
            {
                // conection failed
                logger.Error(e + "");
                return (ServerFound = false);
            }
            return (ServerFound = true);
        }


        #region Check Plug-in Module in Windows Registry
        //	hsshin 제어스케줄과 자막스케줄을 Registry 상에 등록되어있는지 검사
        private bool? _isAvailableControlSchedule = null;
        private bool? _isAvailableSubtitleSchedule = null;
        private bool? _isAvailableTemplate = null;
        private bool? _isAvailableComplexPane = null;

        public bool IsAvailableControlSchedule
        {
            get
            {
                //  항상 제어 스케줄 사용 가능하도록
                _isAvailableControlSchedule = true;

                if (_isAvailableControlSchedule == null || _isAvailableControlSchedule.HasValue == false)
                {
                    _isAvailableControlSchedule = new Nullable<bool>();
                    _isAvailableControlSchedule = DigitalSignage.Common.KeyChecker.GetInstance.CheckKey(DigitalSignage.Common.iVisionAddition.ADD_CONTROL_SCHEDULE);
                }
                return _isAvailableControlSchedule.Value;
            }

            set
            {
                if (DigitalSignage.Common.KeyChecker.GetInstance.SetKey(DigitalSignage.Common.iVisionAddition.ADD_CONTROL_SCHEDULE, value))
                {
                    if (_isAvailableControlSchedule == null || _isAvailableControlSchedule.HasValue == false)
                    {
                        _isAvailableControlSchedule = new Nullable<bool>();
                        _isAvailableControlSchedule = value;
                    }
                    else _isAvailableControlSchedule = value;
                }
            }
        }

        public bool IsAvailableSubtitleSchedule
        {
            get
            {
                if (_isAvailableSubtitleSchedule == null || _isAvailableSubtitleSchedule.HasValue == false)
                {
                    _isAvailableSubtitleSchedule = new Nullable<bool>();
                    _isAvailableSubtitleSchedule = DigitalSignage.Common.KeyChecker.GetInstance.CheckKey(DigitalSignage.Common.iVisionAddition.ADD_SUBTITLE_SCHEDULE);
                }
                return _isAvailableSubtitleSchedule.Value;
            }

            set
            {
                if (DigitalSignage.Common.KeyChecker.GetInstance.SetKey(DigitalSignage.Common.iVisionAddition.ADD_SUBTITLE_SCHEDULE, value))
                {
                    if (_isAvailableSubtitleSchedule == null || _isAvailableSubtitleSchedule.HasValue == false)
                    {
                        _isAvailableSubtitleSchedule = new Nullable<bool>();
                        _isAvailableSubtitleSchedule = value;
                    }
                    else _isAvailableSubtitleSchedule = value;
                }
            }
        }

        public bool IsAvailableTemplate
        {
            get
            {
                if (_isAvailableTemplate == null || _isAvailableTemplate.HasValue == false)
                {
                    _isAvailableTemplate = new Nullable<bool>();
                    _isAvailableTemplate = DigitalSignage.Common.KeyChecker.GetInstance.CheckKey(DigitalSignage.Common.iVisionAddition.ADD_TEMPLATE);
                }
                return _isAvailableTemplate.Value;
            }

            set
            {
                if (DigitalSignage.Common.KeyChecker.GetInstance.SetKey(DigitalSignage.Common.iVisionAddition.ADD_TEMPLATE, value))
                {
                    if (_isAvailableTemplate == null || _isAvailableTemplate.HasValue == false)
                    {
                        _isAvailableTemplate = new Nullable<bool>();
                        _isAvailableTemplate = value;
                    }
                    else _isAvailableTemplate = value;
                }
            }
        }

        public bool IsAvailableComplexPane
        {
            get
            {
                if (_isAvailableComplexPane == null || _isAvailableComplexPane.HasValue == false)
                {
                    _isAvailableComplexPane = new Nullable<bool>();
                    _isAvailableComplexPane = DigitalSignage.Common.KeyChecker.GetInstance.CheckKey(DigitalSignage.Common.iVisionAddition.ADD_COMPLEX_PANE);
                }
                return _isAvailableComplexPane.Value;
            }

            set
            {
                if (DigitalSignage.Common.KeyChecker.GetInstance.SetKey(DigitalSignage.Common.iVisionAddition.ADD_COMPLEX_PANE, value))
                {
                    if (_isAvailableComplexPane == null || _isAvailableComplexPane.HasValue == false)
                    {
                        _isAvailableComplexPane = new Nullable<bool>();
                        _isAvailableComplexPane = value;
                    }
                    else _isAvailableComplexPane = value;
                }
            }
        }
        #endregion

        #region Show/Hide Schedules
        public bool IsShowRepeatationSchedule
        {
            get
            {
                try
                {
                    string result = ConfigSettings.ReadSetting("ShowRepeatSched");

                    if (String.IsNullOrEmpty(result))
                        result = "";

                    if (result.Equals(""))
                    {
                        result = "true"; //default value
                        ConfigSettings.WriteSetting("ShowRepeatSched", result);
                    }

                    return Convert.ToBoolean(result);
                }
                catch { }

                return true;
            }
            set
            {
                ConfigSettings.WriteSetting("ShowRepeatSched", value.ToString());
            }
        }

        public bool IsShowTimeSchedule
        {
            get
            {
                try
                {
                    string result = ConfigSettings.ReadSetting("ShowTimeSched");

                    if (String.IsNullOrEmpty(result))
                        result = "";

                    if (result.Equals(""))
                    {
                        result = "true"; //default value
                        ConfigSettings.WriteSetting("ShowTimeSched", result);
                    }

                    return Convert.ToBoolean(result);
                }
                catch { }

                return true;
            }
            set
            {
                ConfigSettings.WriteSetting("ShowTimeSched", value.ToString());
            }
        }

        public bool IsShowSubtitleSchedule
        {
            get
            {
                try
                {
                    string result = ConfigSettings.ReadSetting("ShowSubtitleSched");

                    if (String.IsNullOrEmpty(result))
                        result = "";

                    if (result.Equals(""))
                    {
                        result = "true"; //default value
                        ConfigSettings.WriteSetting("ShowSubtitleSched", result);
                    }

                    return Convert.ToBoolean(result);
                }
                catch { }

                return true;
            }
            set
            {
                ConfigSettings.WriteSetting("ShowSubtitleSched", value.ToString());
            }
        }

        public bool IsShowControlSchedule
        {
            get
            {
                try
                {
                    string result = ConfigSettings.ReadSetting("ShowControlSched");

                    if (String.IsNullOrEmpty(result))
                        result = "";

                    if (result.Equals(""))
                    {
                        result = "true"; //default value
                        ConfigSettings.WriteSetting("ShowControlSched", result);
                    }

                    return Convert.ToBoolean(result);
                }
                catch { }

                return true;
            }
            set
            {
                ConfigSettings.WriteSetting("ShowControlSched", value.ToString());
            }
        }
        #endregion

        #region GridLength
        public double RepeatSchedValue
        {
            get
            {
                try
                {
                    string result = ConfigSettings.ReadSetting("RepeatSchedValue");

                    if (String.IsNullOrEmpty(result))
                        result = "";

                    if (result.Equals(""))
                    {
                        result = "10"; //default value
                        ConfigSettings.WriteSetting("RepeatSchedValue", result);
                    }

                    return Convert.ToDouble(result);
                }
                catch { }

                return 10.0;
            }
            set
            {
                ConfigSettings.WriteSetting("RepeatSchedValue", value.ToString());
            }
        }
        public double TimeSchedValue
        {
            get
            {
                try
                {
                    string result = ConfigSettings.ReadSetting("TimeSchedValue");

                    if (String.IsNullOrEmpty(result))
                        result = "";

                    if (result.Equals(""))
                    {
                        result = "10"; //default value
                        ConfigSettings.WriteSetting("TimeSchedValue", result);
                    }

                    return Convert.ToDouble(result);
                }
                catch { }

                return 10.0;
            }
            set
            {
                ConfigSettings.WriteSetting("TimeSchedValue", value.ToString());
            }
        }
        public double SubtitleSchedValue
        {
            get
            {
                try
                {
                    string result = ConfigSettings.ReadSetting("SubtitleSchedValue");

                    if (String.IsNullOrEmpty(result))
                        result = "";

                    if (result.Equals(""))
                    {
                        result = "10"; //default value
                        ConfigSettings.WriteSetting("SubtitleSchedValue", result);
                    }

                    return Convert.ToDouble(result);
                }
                catch { }

                return 10.0;
            }
            set
            {
                ConfigSettings.WriteSetting("SubtitleSchedValue", value.ToString());
            }
        }
        public double ControlSchedValue
        {
            get
            {
                try
                {
                    string result = ConfigSettings.ReadSetting("ControlSchedValue");

                    if (String.IsNullOrEmpty(result))
                        result = "";

                    if (result.Equals(""))
                    {
                        result = "10"; //default value
                        ConfigSettings.WriteSetting("ControlSchedValue", result);
                    }

                    return Convert.ToDouble(result);
                }
                catch { }

                return 10.0;
            }
            set
            {
                ConfigSettings.WriteSetting("ControlSchedValue", value.ToString());
            }
        }
        #endregion


        public string ServerPath
        {
            get
            {
                string result = ServerConfigSettings.ReadSetting("server_path");
                //             if (result.Equals(""))
                //             {
                //                 result = "tcp://127.0.0.1:888/"; //default value
                //                 ConfigSettings.WriteSetting("server_path", result );
                //             }
                if (String.IsNullOrEmpty(result))
                    result = "";

                return result;
            }
            set
            {
                ServerConfigSettings.WriteSetting("server_path", value);
            }
        }

        public String AnalyzeSource(String source)
        {
            if (source.StartsWith("$"))
            {
                int nIndex = source.IndexOf(":");

                if (source.ToLower().Contains("$local"))
                {
                    source = "127.0.0.1" +
                        (nIndex != -1 ? source.Substring(nIndex) : "");
                }
                else if (source.ToLower().Contains("$server"))
                {
                    string server = ServerPath.ToLower();
                    server = server.StartsWith("tcp://") ? server.Substring(6) : server;
                    server = server.LastIndexOf(":") != -1 ? server.Substring(0, server.LastIndexOf(":")) : server;

                    source = server +
                        (nIndex != -1 ? source.Substring(nIndex) : "");
                }
            }

            return source;
        }

        public int GetConfigInt(string valueName, int defvalue)
        {
            int retvalue = defvalue;
            try
            {
                string result = ConfigSettings.ReadSetting(valueName);
                if (!result.Equals(""))
                    int.TryParse(result, out retvalue);
            }
            catch (Exception e)
            {
                logger.Warn(e + "");
                ConfigSettings.WriteSetting(valueName, defvalue + "");
            }
            return retvalue;
        }

        public void SetConfigInt(string valueName, int value)
        {
            try
            {
                ConfigSettings.WriteSetting(valueName, value + "");
            }
            catch (Exception e)
            {
                logger.Error(e + "");

            }
            return;
        }

        public string GetConfigStr(string valueName, string defvalue)
        {
            try
            {
                return ConfigSettings.ReadSetting(valueName);
            }
            catch (Exception e)
            {
                logger.Warn(e + "");
                ConfigSettings.WriteSetting(valueName, defvalue + "");
            }
            return defvalue;
        }

        public void SetConfigStr(string valueName, string value)
        {
            ConfigSettings.WriteSetting(valueName, value);
        }

        //properties
        public string AppData { get { return appData; } }
        public string ResData { get { return resData; } }
        public IServerCmdReceiver ServerCmdReceiver { get { return serverCmdReceiver; } }
        public ITaskList ServerTasksList { get { return serverTasksList; } }
        public IGroupList ServerGroupsList { get { return serverGroupsList; } }
        public IPlayersList ServerPlayersList { get { return serverPlayersList; } }
        public IUsersList ServerUsersList { get { return serverUsersList; } }
        public ILogList ServerLogList { get { return serverLogList; } }
        public IDownloadList ServerDownloadList { get { return serverDownloadList; } }
        public IDownloadTimer ServerDownloadTimer { get { return serverDownloadTimer; } }
        public IDataCacheList ServerDataCacheList { get { return serverDataCacheList; } }

        public IDataInfoList ServerDataInfoList { get { return serverDataInfoList; } }
        public IDataFieldList ServerDataFieldList { get { return serverDataFieldList; } }
        public IContentsList ServerContentsList { get { return serverContentsList; } }
        public IResultDataList ServerResultDataList { get { return serverResultDataList; } }

        public string UserPassword
        {
            set
            {
                StringBuilder sb = new StringBuilder();
                MD5 md5 = new MD5CryptoServiceProvider();
                Byte[] original;
                Byte[] encoded;
                original = ASCIIEncoding.Default.GetBytes(value);
                encoded = md5.ComputeHash(original);
                foreach (byte hex in encoded) sb.Append(hex.ToString("x2"));
                AT.passhash = sb.ToString().ToLower();
            }
        }
        public AuthTicket UserAT
        {
            get
            {
                return AT;
            }
            set
            {
                AT = value;
            }
        }

        bool _useScroll = false;
        public bool UseScrollInTimeline
        {
            get { return _useScroll; }
            set { _useScroll = value; }
        }
    }
