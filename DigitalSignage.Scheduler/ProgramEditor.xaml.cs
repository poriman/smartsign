﻿using System;
using System.Xml;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Interop;
using System.Windows.Media.Animation;

using NLog;


namespace DigitalSignage.Scheduler
{
    /// <summary>
    /// Interaction logic for NewDlgItem.xaml
    /// </summary>
    public partial class ProgramEditor : Window
    {
 
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private Config cfg = null;
 
        public ProgramEditor(Config config)
        {
            InitializeComponent();
            
			cfg = config;
			uiProgramEditorMain.Cfg = config;
			uiProgramEditorMain.ParentWindow = this;

//             restoreParams();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
//             cfg.SetConfigInt("PrgE.width", (int)this.ActualWidth);
//             cfg.SetConfigInt("PrgE.height", (int)this.ActualHeight);
//             cfg.SetConfigInt("PrgE.left", (int)this.Left);
//             cfg.SetConfigInt("PrgE.top", (int)this.Top);
        }

		private void restoreParams()
		{
			this.Width = cfg.GetConfigInt("PrgE.width", 800);
			this.Height = cfg.GetConfigInt("PrgE.height", 600);
			this.Left = cfg.GetConfigInt("PrgE.left", 100);
			this.Top = cfg.GetConfigInt("PrgE.top", 100);

			if (this.Top < 0)
				this.Top = 0;
			if (this.Top < 0)
				this.Top = 0;

		}

		private void dataUI_KeyUp(object sender, KeyEventArgs e)
		{
			try
			{
				//if (e.Key == Key.Insert)
				//splitPane();
				//if (e.Key == Key.Delete)
			}
			catch (Exception err)
			{
				logger.Error(err + " " + err.StackTrace);
			}
		}
    }
}
