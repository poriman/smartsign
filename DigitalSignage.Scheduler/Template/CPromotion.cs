﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigitalSignage.Scheduler.Template
{
    public class LoopData
    {
        private string name_Field;
        private string code_Field;
        private string type_Field;
        private string maxcount_Field;

        public string Name
        {
            set { name_Field = value; }
            get { return name_Field; }
        }

        public string Code
        {
            set { code_Field = value; }
            get { return code_Field; }
        }

        public string Type
        {
            set { type_Field = value; }
            get { return type_Field; }
        }

        public string Max_Count
        {
            set { maxcount_Field = value; }
            get { return maxcount_Field; }
        }
    }

    public class promotion
    {
        private string id_dcField;

        private string desc_dcField;

        private string name_dcField;

        private string code_dcField;

        private string contype_cdField;

        private string filename_cdField;

        private string text_cdField;

        private string text_sizeField;

        private string text_colorField;

        public string fontsize
        {
            get
            {
                return this.text_sizeField;
            }
            set
            {
                this.text_sizeField = value;
            }
        }

        public string fontcolor
        {
            get
            {
                return this.text_colorField;
            }
            set
            {
                this.text_colorField = value;
            }
        }

        public string id
        {
            get
            {
                return this.id_dcField;
            }
            set
            {
                this.id_dcField = value;
            }
        }

        public string name
        {
            get
            {
                return this.name_dcField;
            }
            set
            {
                this.name_dcField = value;
            }
        }

        public string desc
        {
            get
            {
                return this.desc_dcField;
            }
            set
            {
                this.desc_dcField = value;
            }
        }

        public string code
        {
            get
            {
                return this.code_dcField;
            }
            set
            {
                this.code_dcField = value;
            }
        }

        public string cont_type
        {
            get
            {
                return this.contype_cdField;
            }
            set
            {
                this.contype_cdField = value;
            }
        }

        public string cont_nm
        {
            get
            {
                return this.filename_cdField;
            }
            set
            {
                this.filename_cdField = value;
            }
        }

        public string cont_text
        {
            get
            {
                return this.text_cdField;
            }
            set
            {
                this.text_cdField = value;
            }
        }
    }
}
