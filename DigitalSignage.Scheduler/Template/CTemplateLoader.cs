﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Collections.ObjectModel;
using System.Reflection;
using System.Xml;
using System.Diagnostics;

namespace DigitalSignage.Scheduler.Template
{
    public class Template_T
    {
        private bool _IsSelect;
        private template _template;

        public template TEMPLATE
        {
            set { _template = value; }
            get { return _template; }
        }

        public bool ISSelect
        {
            set { _IsSelect = value; }
            get { return _IsSelect; }
        }
    }

    public class TemplateArry : ObservableCollection<Template_T>
    {
        public TemplateArry() : base() { }
    }

    class TemplateListLoader
    {
        public TemplateArry Load()
        {
            try
            {
                String sExecutingPath = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                string appData = sExecutingPath + "\\TEMPLATE_XML\\";
                string path = appData + "templatelists.xml";
                if (!Directory.Exists(appData) || false == File.Exists(path))
                    return null;
                TemplateArry _templateList = new TemplateArry();

                {
                    XmlDocument _doc = new XmlDocument();
                    _doc.Load(path);
                    XmlNodeList nodeList = _doc.SelectNodes("//template_list/template");
                    for (int i = 0; i < nodeList.Count; i++)
                    {
                        XmlNode nodes = nodeList.Item(i);

                        Template_T _template = new Template_T();
                        template temp = new template();
                        _template.ISSelect = false;
                        
                        temp.id = nodes.Attributes["id"].Value;
                        temp.name = nodes.Attributes["name"].Value;
                        temp.file_nm = nodes.Attributes["file_nm"].Value;
                        temp.ex_file_nm = nodes.Attributes["ex_file_nm"].Value;
                        temp.desc = nodes.Attributes["desc"].Value;
                        temp.count = nodes.Attributes["count"].Value;
                        temp.type = nodes.Attributes["type"].Value;
                        try
                        {
                            temp.dbid = nodes.Attributes["dbid"].Value;
                        }
                        catch (Exception err)
                        {
                            temp.dbid = temp.id;
                        }
                        temp.form = nodes.Attributes["form"].Value;
                        temp.thumbnail_nm = nodes.Attributes["thumbnail_nm"].Value;
                        temp.isused = nodes.Attributes["isused"].Value;
                        temp.HeaderType = nodes.Attributes["recommandcount"].Value;

                        temp.buttimg = nodes.Attributes["buttimg"].Value;
                        temp.logo = nodes.Attributes["logo"].Value;
                        temp.category = nodes.Attributes["category"].Value;
                        temp.backImage = nodes.Attributes["backImage"].Value;
                        temp.weather = nodes.Attributes["weather"].Value;

                        _template.TEMPLATE = temp;

                        //XmlNode promotionNode       = nodes.SelectSingleNode("//promotions");
                        XmlNodeList childList = nodes.ChildNodes;
                        for (int t = 0; t < childList.Count; t++)
                        {
                            XmlNodeList xnodelist = childList[t].ChildNodes;

                            if (childList[t].Name.Equals("settings"))
                            {
                                LoopData[] loopdata = new LoopData[xnodelist.Count];
                                for (int p = 0; p < xnodelist.Count; p++)
                                {
                                    LoopData data = new LoopData();
                                    XmlNode selectNode = xnodelist.Item(p);
                                    data.Name = selectNode.Attributes["id"].Value;
                                    data.Code = selectNode.Attributes["code"].Value;
                                    data.Type = selectNode.Attributes["type"].Value;
                                    if (selectNode.ChildNodes.Count > 0)
                                        data.Max_Count = selectNode.FirstChild.InnerText;

                                    loopdata[p] = data;
                                }

                                _template.TEMPLATE.LoopDatas = loopdata;
                            }
                            if (childList[t].Name.Equals("promotions"))
                            {
                                promotion[] promotions = new promotion[xnodelist.Count];

                                for (int p = 0; p < xnodelist.Count; p++)
                                {
                                    promotion _promotion = new promotion();
                                    XmlNode selectNode = xnodelist.Item(p);
                                    _promotion.name = selectNode.Attributes["name"].Value;
                                    _promotion.code = selectNode.Attributes["code"].Value;
                                    if (selectNode.ChildNodes.Count > 0)
                                        _promotion.cont_type = selectNode.FirstChild.InnerText;

                                    promotions[p] = _promotion;
                                }

                                _template.TEMPLATE.promotions = promotions;
                            }
                        }
                        _templateList.Add(_template);
                    }
                }

                return _templateList;
                
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message.ToString());
            }
            return null;
        }

        public TemplateArry GetTemplateList(int type)
        {
            try
            {
               TemplateArry list = Load();
               if (type == 0)
                   return list;

               TemplateArry arry = new TemplateArry();
               foreach (Template_T temp in list)
               {
                   if (GetDataCategory(temp.TEMPLATE.category, type) == true)
                       arry.Add(temp);
               }
               return arry;
            }
            catch
            {
            }
            return null;
        }

        private bool GetDataCategory(string data, int index)
        {
            try
            {
                if (index == 0)
                    return true;

                if (data != null && data.Length == 20)
                {
                    return int.Parse(string.Format("{0}", data[index - 1])) == 1;
                }
            }
            catch
            {
            }

            return false;
        }
    }
}
