﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Reflection;
using System.Xml;
using System.IO;
using Wpf.Controls;
using System.Diagnostics;

namespace DigitalSignage.Scheduler.Template.Wizard
{
    /// <summary>
    /// NewTemplateWizard.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class NewTemplateWizard : Window
    {

#region 템플릿 수정시에 필요
        private string screen_name;
        private string play_time;
        private string applyTemplate;
        public bool isNew;
        public PageCommand pcd = null;
        private int pageposion = 0;
        Config m_cfg;
        public string m_media_folder;
        double m_resource;
        private List<string> DataFieldList = new List<string>();
        System.Windows.Controls.TabControl tab_control = null;
        private static bool isDefaultSetting = false;
        GridLength[] starHeight;

        List<string> textColors = new List<string>();
        Scheduler.Template.UserControl.WeatherControl defaultControl = null;
        double gridpromotionHight = 0;
        string m_areaCode = "";
        string m_logoName = "";
        string m_backimage = "";
        public List<string> GetDataFieldList()
        {
            return DataFieldList;
        }

        public double RESOUL_VALUE
        {
            set { m_resource = value; }
            get { return m_resource; }
        }
        //페이지 이름

        List<UserControl.PROTextControl> PTextControlList = new List<UserControl.PROTextControl>();
        List<UserControl.PROFileControl> PFileControlList = new List<UserControl.PROFileControl>();

        PrevPlayer.PlayerForm preview;
        private bool isPlay = false;
        System.Windows.Forms.Timer Play_timer = new System.Windows.Forms.Timer();
        System.Windows.Forms.Timer cont_tick = new System.Windows.Forms.Timer();

        public string SCREEN_NAME
        {
            set { screen_name = value; }
            get { return screen_name; }
        }

        // 플레이 타임
        public string PLAY_TIME
        {
            set { play_time = value; }
            get { return play_time; }
        }

        // 적용된 템플릿 아이디
        public string APPLY_TEMPLATE_ID        
        {
            set { applyTemplate = value; }
            get { return applyTemplate; }
        }

        public bool ISNEW
        {
            set { isNew = value; }
            get { return isNew; }
        }

        public string SCREENNAME
        {
            set { screen_name = value; }
            get { return screen_name; }
        }

        public bool SCREENNAME_READONLY
        {
            set {  }
        }

        public string DATACOUNT
        {
            set {  }
        }

#endregion 템플릿 수정시에 필요


        public NewTemplateWizard()
        {
            InitializeComponent();
            ISNEW           = true;
            m_cfg           = Config.GetConfig;
            pcd             = new PageCommand();
            pcd.MAINWINDOW  = this;

            TabControl_SetEvent();
            SetCategory();

            Play_timer.Tick += new EventHandler(Play_timer_Tick);
            cont_tick.Tick += new EventHandler(cont_tick_Tick);
            cont_tick.Interval = 1000;
            cont_tick.Start();
        }

        public NewTemplateWizard(string fileName)
        {
            InitializeComponent();
            ISNEW           = false;
            m_cfg           = Config.GetConfig;
            pcd             = new PageCommand();
            pcd.MAINWINDOW  = this;
            
            FileInfo FILE   = new FileInfo(fileName);
            SCREENNAME      = FILE.Name.Replace(".bin", ""); ;
            m_media_folder  = FILE.DirectoryName + "\\media_files\\";

            TemplateList.IsEnabled = false;            
            TabControl_SetEvent();

            SetTemplateID();
            SetCategory();
            cb_category.Visibility = Visibility.Hidden;

            Play_timer.Tick += new EventHandler(Play_timer_Tick);
            cont_tick.Tick += new EventHandler(cont_tick_Tick);
            cont_tick.Interval = 1000;
            cont_tick.Start();
        }

        private void SetTemplateID()
        {
            try
            {
                DirectoryInfo dirinfo = new DirectoryInfo(m_media_folder);
                FileInfo[] files = dirinfo.GetFiles();
                foreach (FileInfo info in files)
                {
                    if (info.Extension.ToLower().Equals(".xml"))
                    {
                        XmlDocument _doc = new XmlDocument();
                        _doc.Load(info.FullName);
                        XmlNode xNode = _doc.DocumentElement;
                        SetListBox_Selectindex (xNode.Attributes["template_id"].Value);
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("NewTemplateWizard : SetTemplateID = " + ex.Message.ToString());
            }
        }

        private void SetCategory()
        {
            
            string path = GetTemplateXmlFolder() + "ResDataHeaderInfo.xml";
            if (!File.Exists(path))
                return ;

            try
            {
                List<string> selectlist = new List<string>();
                XmlDocument _doc = new XmlDocument();
                _doc.Load(path);
                XmlNodeList _headlist = _doc.DocumentElement.ChildNodes;
                for (int i = 0; i < _headlist.Count; i++)
                {
                    if (_headlist[i].Name.Equals("CategoryInfo"))
                    {
                        XmlNodeList childList = _headlist[i].ChildNodes;
                        for (int t = 0; t < childList.Count; t++)
                        {
                            string value = string.Format("Value{0:00}", t);
                            for (int j = 0; j < childList.Count; j++)
                            {
                                if (childList[j].Attributes["id"].Value.Equals(value))
                                {
                                    selectlist.Add(childList[j].Attributes["desc"].Value);
                                    break;
                                }
                            }
                        }
                    }
                }
                cb_category.Items.Clear();
                foreach (string value in selectlist)
                {
                   // cb_category
                    ComboBoxItem item = new ComboBoxItem();
					item.Content = value;
					item.DataContext = value;
                    cb_category.Items.Add(item);
                }

                cb_category.SelectedIndex = 0;
            }
            catch (ArgumentException aex)
            {
                MessageBox.Show(aex.Message.ToString());
                Debug.WriteLine("ResultDlg : LoadResultHeader = " + aex.Message.ToString());
            }
            catch (InvalidOperationException iex)
            {
                MessageBox.Show(iex.Message.ToString());
                Debug.WriteLine("ResultDlg : LoadResultHeader = " + iex.Message.ToString());
            }
            catch (XmlException xmlEx)
            {
                MessageBox.Show(xmlEx.Message.ToString());
                Debug.WriteLine("ResultDlg : LoadResultHeader = " + xmlEx.Message.ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
                Debug.WriteLine("ResultDlg : LoadResultHeader = " + ex.Message.ToString());
            }
        }

        public void DialogClose()
        {
            this.DialogResult = true;
        }    

        public void CancelClose()
        {
            this.DialogResult = false;
        }

        public string GetRootFolder()
        {
            return  System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
        }

        public string GetContentsFolder()
        {
            string path = GetRootFolder() + "\\Contents\\";
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);
            return path;
        }

        public string GetTemplateXmlFolder()
        {
            string path = GetRootFolder() + "\\Template_xml\\";
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);
            return path;
        }

        public string GetTempFolder()
        {
            string path = GetRootFolder() + "\\temp\\";
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);
            return path;
        }

        public string GetScreenFolder()
        {
            string path = GetRootFolder() + "\\TScreen\\";
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);
            return path;
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            try
            {
				try
				{
					StopFlashControl();
				}
				catch { } 
				
				DirectoryInfo dirinfo = new DirectoryInfo(GetContentsFolder());
                FileInfo[] files = dirinfo.GetFiles();
                foreach (FileInfo info in files)
                {
                    if (info.Extension.ToLower().Equals(".xml"))
                    {
                        File.Delete(info.FullName);
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("NewTemplateWizard : Window_Closed = " + ex.Message.ToString());
            }
        }

        private void closeButton_Click(object sender, RoutedEventArgs e)
        {
            CancelClose();
        }

        private void maximiseButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.WindowState == WindowState.Normal)
            {
                this.WindowState = WindowState.Maximized;
            }
            else
            {
                this.WindowState = WindowState.Normal;
            }
        }

        private void minimiseButton_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }

        public new WindowState WindowState
        {
            get
            {
                return base.WindowState;
            }
            set
            {
                base.WindowState = value;
            }
        }

        private void TitleBar_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                this.DragMove();
            }
        }

        private void TitleBar_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount > 1)
            {
                if (this.WindowState == WindowState.Normal)
                {
                    this.WindowState = WindowState.Maximized;
                }
                else
                {
                    this.WindowState = WindowState.Normal;
                }
            }
        }

        void cont_tick_Tick(object sender, EventArgs e)
        {
            SetCount();
        }

        public int SetCount()
        {
            try
            {
                int count = 0;
                for (int i = 0; i < tab_data.Items.Count; i++)
                {
                    Wpf.Controls.TabItem tabitem = tab_data.Items[i] as Wpf.Controls.TabItem;

                    Scheduler.Template.UserControl.TemplateListviewControl _control = (Scheduler.Template.UserControl.TemplateListviewControl)tabitem.Content;
                    foreach (Scheduler.Template.DataField_T field in _control.TabArry)
                    {
                        if (field.ISSELECT == true)
                            count++;
                    }
                }

                pcd.itemCount = string.Format("{0}/{1}", count, pcd.CURRENTTEMPLATE.count);
                return count;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("NewTemplateWizard : SetCount = " + ex.Message.ToString());
            }

            return 0;
        }

        private bool CheckScreen()
        {
            try
            {
                if (SCREENNAME == null || SCREENNAME.Length == 0)
                {
                    AddTabDlg dlg = new AddTabDlg(false);
                    if (dlg.ShowDialog() == true)
                    {
                        SCREENNAME = dlg.TITLE;
                    }
                    else
                    {
                        System.Windows.MessageBox.Show(Properties.Resources.mbScreenName,
                                                       Properties.Resources.titleMessageBox,
                                                       MessageBoxButton.OK, MessageBoxImage.Error);
                        return false;
                    }
                }

                int icount = SetCount();
                if ((int.Parse(pcd.CURRENTTEMPLATE.count) > 0) && (icount == 0 || icount > int.Parse(pcd.CURRENTTEMPLATE.count)))
                {
                    System.Windows.MessageBox.Show(Properties.Resources.mbSelectData,
                                                   Properties.Resources.titleMessageBox,
                                                   MessageBoxButton.OK, MessageBoxImage.Error);
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("NewTemplateWizard : CheckScreen = " + ex.Message.ToString());
            }
            return false;
        }

        private bool ValueCheck()
        {
            try
            {
                Scheduler.Template.DataFieldArry arry = GetSelectedDataArry();
                foreach (Scheduler.Template.DataField_T field in arry)
                {
                    int index = 0;
                    foreach (Scheduler.Template.Model model in field.CONTENTS)
                    {
                        if (index < pcd.CURRENTTEMPLATE.LoopDatas.Length && pcd.CURRENTTEMPLATE.LoopDatas[index++].Name != null)
                        {
                            if (model.CONTENTS == null || model.CONTENTS.VALUE == null || model.CONTENTS.VALUE.Length == 0)
                                return false;
                        }
                        else
                            break;
                    }
                }

                if (pcd.CURRENTTEMPLATE.type.ToLower().Equals("w") == false)
                {
                    foreach (UserControl.PROTextControl Textcontrol in PTextControlList)
                    {
                        if (Textcontrol.Data_Value == null || Textcontrol.Data_Value.Length == 0)
                            return false;
                    }

                    foreach (UserControl.PROFileControl Filecontrol in PFileControlList)
                    {
                        if (Filecontrol.Data_Value == null || Filecontrol.Data_Value.Length == 0)
                            return false;
                    }
                }
                else
                {
                }

                if (pcd.CURRENTTEMPLATE.logo.Equals("use"))
                {
                    if (defaultControl == null || defaultControl.LogoFileName == "")
                        return false;

                    m_logoName = defaultControl.LogoFileName;
                    if (false == File.Exists(GetContentsFolder() + m_logoName))
                    {
                        string errmsg = m_logoName + "  " + Properties.Resources.mbErrorFileNotFound;
                        System.Windows.MessageBox.Show(errmsg,
                                                   Properties.Resources.titleMessageBox,
                                                   MessageBoxButton.OK, MessageBoxImage.Error);
                        return false;
                    }
                }

                if (pcd.CURRENTTEMPLATE.weather.Equals("use"))
                {
                    if (defaultControl == null)
                        return false;
                    m_areaCode = defaultControl.WeaterCode.ToString();                    
                }

                if (pcd.CURRENTTEMPLATE.backImage.Equals("use"))
                {
                    if (defaultControl == null || defaultControl.BackimageName == "")
                        return false;
                    m_backimage = defaultControl.BackimageName;
                    if (false == File.Exists(GetContentsFolder() + m_backimage))
                    {//
                        string errmsg = m_backimage + "  " + Properties.Resources.mbErrorFileNotFound;
                        System.Windows.MessageBox.Show(errmsg,
                                                   Properties.Resources.titleMessageBox,
                                                   MessageBoxButton.OK, MessageBoxImage.Error);
                        return false;
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("NewTemplateWizard : ValueCheck = " + ex.Message.ToString());
            }
            return false;
        }

        private bool MakeScreenFolder()
        {
            try
            {
                if (isNew == false)
                {
                    DirectoryInfo info = new DirectoryInfo(m_media_folder);
                    foreach (FileInfo file in info.GetFiles())
                    {
                        try
                        {
                            if (file.Name.ToLower().EndsWith("template_file.swf") == false)
                            {
                                File.Delete(file.FullName);
                            }
                        }
                        catch (Exception ex)
                        {
                            string errmsg = ex.Message.ToString();
                        }
                    }
                    return true;
                }

                string folder = GetScreenFolder();
                if (!Directory.Exists(folder))
                    Directory.CreateDirectory(folder);

                folder = folder + SCREENNAME;
                if (!Directory.Exists(folder))
                {
                    Directory.CreateDirectory(folder);
                    File.Copy(GetTemplateXmlFolder() + "Screen.bin", folder + "\\" + SCREENNAME + ".bin");
                    File.Copy(GetContentsFolder() + pcd.CURRENTTEMPLATE.thumbnail_nm, folder + "\\thumb.png");
                }

                m_media_folder = folder + "\\media_files\\";
                if (!Directory.Exists(m_media_folder))
                {
                    Directory.CreateDirectory(m_media_folder);
                    File.Copy(pcd.MAINWINDOW.GetContentsFolder() + pcd.CURRENTTEMPLATE.file_nm, m_media_folder + "Template_File.swf");
                }


                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
                Debug.WriteLine("NewTemplateWizard : MakeScreenFolder = " + ex.Message.ToString());
            }

            return false;
        }

        private List<string> CheckContentsFolder()
        {
            List<string> filelist = new List<string>();
            try
            {
                Scheduler.Template.DataFieldArry arry = GetSelectedDataArry();
                foreach (Scheduler.Template.DataField_T field in arry)
                {
                    foreach (Scheduler.Template.Model model in field.CONTENTS)
                    {
                        if (model != null && model.CONTENTS != null && model.CONTENTS.TYPE.ToUpper().Equals("T") != true)
                        {
                            string filepath = GetContentsFolder() + model.CONTENTS.NEW_NAME;
                            if (File.Exists(filepath))
                            {
                                try
                                {
                                    string newfile = m_media_folder + model.CONTENTS.NEW_NAME;
                                    if (File.Exists(newfile))
                                        File.Delete(newfile);

                                    if (m_media_folder != null && m_media_folder != "" && Directory.Exists(m_media_folder))
                                        File.Copy(filepath, newfile);
                                }
                                catch
                                {
                                    filelist.Add(model.CONTENTS.NEW_NAME);
                                }
                            }
                            else
                            {
                                filelist.Add(model.CONTENTS.NEW_NAME);
                            }
                        }
                    }
                }

                if (pcd.CURRENTTEMPLATE.type.ToLower().Equals("w") == false)
                {
                    foreach (UserControl.PROFileControl Filecontrol in PFileControlList)
                    {
                        if (Filecontrol.Data_Value != null || Filecontrol.Data_Value.Length > 0)
                        {
                            try
                            {
                                if (File.Exists(this.GetContentsFolder() + Filecontrol.Data_Value) && false == File.Exists(m_media_folder + Filecontrol.Data_Value))
                                {
                                    if (m_media_folder != null && m_media_folder != "" && Directory.Exists(m_media_folder))
                                        File.Copy(GetContentsFolder() + Filecontrol.Data_Value, m_media_folder + Filecontrol.Data_Value, true);
                                }
                            }
                            catch { }
                        }
                    }
                }
                else
                {
                    foreach (System.Windows.Controls.TabItem item in tab_control.Items)
                    {
                        Scheduler.Template.UserControl.DataListControl _control = item.Content as Scheduler.Template.UserControl.DataListControl;
                        Scheduler.Template.DataFieldArry DataAArry = _control.DataAArry;

                        foreach (Scheduler.Template.DataField_T data in DataAArry)
                        {
                            foreach (Scheduler.Template.Model col in data.CONTENTS)
                            {
                                try
                                {
                                    if (col.CONTENTS.TYPE.Equals("T") == false)
                                    {
                                        if (File.Exists(this.GetContentsFolder() + col.CONTENTS.NEW_NAME) && false == File.Exists(m_media_folder + col.CONTENTS.NEW_NAME))
                                        {
                                            if (m_media_folder!= null && m_media_folder != "" && Directory.Exists(m_media_folder))
                                                File.Copy(GetContentsFolder() + col.CONTENTS.NEW_NAME, m_media_folder + col.CONTENTS.NEW_NAME, true);
                                        }
                                        else
                                            filelist.Add(col.CONTENTS.NEW_NAME);
                                    }
                                }
                                catch { }
                            }
                        }
                    }
                }
                if (pcd.CURRENTTEMPLATE.backImage.Equals("use"))
                {
                    if (m_media_folder != null && m_media_folder != "" && Directory.Exists(m_media_folder))
                        File.Copy(GetContentsFolder() + m_backimage, m_media_folder + m_backimage);
                }
                if (pcd.CURRENTTEMPLATE.logo.Equals("use"))
                {
                    if (m_media_folder != null && m_media_folder != "" && Directory.Exists(m_media_folder))
                        File.Copy(GetContentsFolder() + m_logoName, m_media_folder + m_logoName);
                }

                return filelist;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("NewTemplateWizard : CheckContentsFolder = " + ex.Message.ToString());
            }
            return null;
        }

        private bool ContentsDownload(List<string> list)
        {
            try
            {
                String sReturn = m_cfg.ServerDataCacheList.GetInheritedFTPInfo("0000");
                if (sReturn == null || sReturn.Length == 0)
                    return false;

                ContentsUpDownDlg dlg = new ContentsUpDownDlg(m_cfg, sReturn, "", ServerDataCache.GetInstance.CurrentUser.Attributes["id"].Value, false, list);
                if (dlg.ShowDialog() == false)
                    return false;

                return true;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("NewTemplateWizard : ContentsDownload = " + ex.Message.ToString());
            }
            return false;
        }

        private void btn_Save_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (CheckScreen() == false)
                    return;

                string caption = "Screen save";
                MessageBoxButton buttons = MessageBoxButton.OKCancel;
                MessageBoxResult result = System.Windows.MessageBox.Show(Properties.Resources.mbSaveProject, caption, buttons);
                if (result == MessageBoxResult.Cancel)
                    return;
                if (false == ValueCheck())
                {//mberrdatainput
                    System.Windows.MessageBox.Show(Properties.Resources.mberrdatainput,
                                                   Properties.Resources.titleMessageBox,
                                                   MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }

                if (false == MakeScreenFolder())
                    return;

                List<string> list = CheckContentsFolder();

                if (list != null && list.Count > 0)
                {
                    if (ContentsDownload(list) == false)
                        return;
                    foreach (string file in list)
                    {
                        File.Copy(pcd.MAINWINDOW.GetContentsFolder() + file, m_media_folder + file);
                    }
                }

                if (false == MakeExFile(m_media_folder + pcd.CURRENTTEMPLATE.ex_file_nm))
                {
                    return;
                }

                if (GetDefaultData() > 0)
                {
                    try
                    {
                        String sExecutingPath = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                        string appData = sExecutingPath + "\\TEMPLATE_XML\\";
                        string path = appData + "templatelists.xml";
                        if (File.Exists(path))
                        {
                            XmlDocument _doc = new XmlDocument();
                            _doc.Load(path);
                            XmlNode node = _doc.SelectSingleNode("//template_list");
                            if (pcd.CURRENTTEMPLATE.weather != null && pcd.CURRENTTEMPLATE.weather.Equals("use"))
                                node.Attributes["areaCode"].Value = m_areaCode;

                            if (pcd.CURRENTTEMPLATE.logo != null && pcd.CURRENTTEMPLATE.logo.Equals("use"))
                                node.Attributes["logo"].Value = m_logoName;

                            _doc.Save(path);
                        }
                    }
                    catch
                    {
                    }
                }
                MessageBox.Show(Properties.Resources.mbSavedProject, Properties.Resources.titleMessageBox);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("NewTemplateWizard : btn_Save_Click = " + ex.Message.ToString());
            }

            DialogClose();
        }

        public string GetSelectData()
        {
            try
            {
                for (int i = 0; i < tab_data.Items.Count; i++)
                {
                    Wpf.Controls.TabItem tabitem = tab_data.Items[i] as Wpf.Controls.TabItem;

                    Scheduler.Template.UserControl.TemplateListviewControl _control = (Scheduler.Template.UserControl.TemplateListviewControl)tabitem.Content;
                    foreach (Scheduler.Template.DataField_T field in _control.TabArry)
                    {
                        if (field.ISSELECT == true)
                        {
                            return field.ID;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("NewTemplateWizard : GetSelectData = " + ex.Message.ToString());
            }

            return "";
        }

        private void btn_Cancel_Click(object sender, RoutedEventArgs e)
        {
            //CancelClose();
            string dataid = GetSelectData();
            if (dataid == "")
            {
                ResultDlg dlg = new ResultDlg(this.m_cfg, pcd);
                dlg.ShowDialog();
            }
            else
            {
                ResultDlg dlg = new ResultDlg(this.m_cfg, pcd, dataid);
                dlg.ShowDialog();
            }
        }

        #region 데이터 설정 부분
        private List<string> LoadExFileData(string keyvalue)
        {
            string path = m_media_folder + pcd.CURRENTTEMPLATE.ex_file_nm;
            if (File.Exists(path) == false)
                return null;

            try
            {
                List<string> selectlist = new List<string>();
                XmlDocument _doc = new XmlDocument();
                _doc.Load(path);
                XmlNode root = _doc.DocumentElement;
                XmlNode mediainfo1 = root.ChildNodes[0];
                XmlNodeList nodeList = mediainfo1.ChildNodes;
                for (int i = 0; i < nodeList.Count; i++)
                {
                    XmlNode node = nodeList.Item(i);
                    string value = node.Attributes["menu_id"].Value.ToString();
                    if (value.Length > 0)
                    {
                        selectlist.Add(value);
                    }
                }

                return selectlist;
            }
            catch (ArgumentException aex)
            {
                Debug.WriteLine("NewTemplateWizard : LoadExFileData = " + aex.Message.ToString());
            }
            catch (InvalidOperationException iex)
            {
                Debug.WriteLine("NewTemplateWizard : LoadExFileData = " + iex.Message.ToString());
            }
            catch (XmlException xmlEx)
            {
                Debug.WriteLine("NewTemplateWizard : LoadExFileData = " + xmlEx.Message.ToString());
            }
            catch (Exception ex)
            {
                Debug.WriteLine("NewTemplateWizard : LoadExFileData = " + ex.Message.ToString());
            }

            return null;
        }

        #endregion 데이터 설정 부분

        #region tab 설정

        private void TabControl_SetEvent()
        {
            tab_data.Visibility = Visibility.Collapsed;
            tab_data.TabItemAdded += new EventHandler<Wpf.Controls.TabItemEventArgs>(tabControl_TabItemAdded);
            tab_data.TabItemClosing += new EventHandler<Wpf.Controls.TabItemCancelEventArgs>(tabControl_TabItemCancel);
            tab_data.SelectionChanged += new SelectionChangedEventHandler(tabControl_SelectionChanged);

            tab_data.CULTURES = Properties.Resources.btnAdd.Equals("Add") ? "ENG" : "KOR";

            starHeight = new GridLength[grid_promotion.RowDefinitions.Count];
            starHeight[0] = grid_promotion.RowDefinitions[0].Height;
            starHeight[1] = grid_promotion.RowDefinitions[1].Height;
            starHeight[2] = grid_promotion.RowDefinitions[2].Height;

            promotion_expander.Expanded += ExpandedOrCollapsed;
            promotion_expander.Collapsed += ExpandedOrCollapsed;
            preview_expander.Expanded += ExpandedOrCollapsed;
            preview_expander.Collapsed += ExpandedOrCollapsed;        

        }

        void tabControl_TabItemAdded(object sender, TabItemEventArgs e)
        {
            try
            {
                if (pcd.CURRENTTEMPLATE == null)
                {
                    tab_data.Items.Remove(e.TabItem);
                    return;
                }

                AddTabDlg dlg = new AddTabDlg(true);
                if (dlg.ShowDialog() == true)
                {
                    Scheduler.Template.DataInfo info = new DigitalSignage.Scheduler.Template.DataInfo();
                    info.ID = DateTime.Now.ToString("yyyyMMddHHmmssffff");
                    info.TITLE = dlg.TITLE;
                    string templateid = (pcd.CURRENTTEMPLATE.dbid.Length != 0) ? pcd.CURRENTTEMPLATE.dbid : pcd.CURRENTTEMPLATE.id;
                    info.TEMPLATE_ID = templateid;
                    info.USER_ID = ServerDataCache.GetInstance.CurrentUser.Attributes["id"].Value;
                    if (false == m_cfg.ServerDataInfoList.AddDataInfo(info.ID,
                                                                    long.Parse(info.USER_ID),
                                                                    info.TEMPLATE_ID,
                                                                    info.TITLE))
                    {
                        System.Windows.MessageBox.Show(Properties.Resources.mbErrorFTPUpload,
                                                       Properties.Resources.titleMessageBox,
                                                       MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }

                    TextBlock tb = new TextBlock();
                    tb.Text = info.TITLE;
                    tb.TextTrimming = TextTrimming.CharacterEllipsis;
                    tb.TextWrapping = TextWrapping.NoWrap;
                    e.TabItem.Header = tb;

                    Scheduler.Template.UserControl.TemplateListviewControl _control = new DigitalSignage.Scheduler.Template.UserControl.TemplateListviewControl(this.pcd);
                    _control.CURRENT_DATAINFO = info;
                    _control.CurrentTemplate = pcd.CURRENTTEMPLATE;
                    _control.MakeListViewColumn();
                    e.TabItem.Content = _control;
                }
                else
                    tab_data.Items.Remove(e.TabItem);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("NewTemplateWizard : tabControl_TabItemAdded = " + ex.Message.ToString());
            }
        }

        void tabControl_TabItemCancel(object sender, TabItemCancelEventArgs e)
        {
            try
            {
                if (tab_data.SelectedItem == null)
                    return;
                Wpf.Controls.TabItem _tabitem = tab_data.SelectedItem as Wpf.Controls.TabItem;
                if (_tabitem == null)
                    return;

                Scheduler.Template.UserControl.TemplateListviewControl _control = (Scheduler.Template.UserControl.TemplateListviewControl)_tabitem.Content;//as Scheduler.Template.UserControl.TemplateListviewControl

                string tempDir = pcd.MAINWINDOW.GetTempFolder() + _control.CURRENT_DATAINFO.USER_ID + "\\";
                Scheduler.Template.DataFieldArry task = _control.TabArry;

                String sReturn = m_cfg.ServerDataCacheList.GetInheritedFTPInfo("0000");
                if (sReturn == null || sReturn.Length == 0)
                    return;
                String[] arrFTPInfo = sReturn.Split('|');

                UserControl.DataStatusDlg dlg = new DigitalSignage.Scheduler.Template.UserControl.DataStatusDlg(arrFTPInfo, m_cfg, task, _control.CURRENT_DATAINFO.USER_ID, _control.CURRENT_DATAINFO.ID, pcd.MAINWINDOW.GetTempFolder());
                if (dlg.ShowDialog() == false)
                    return;
                tab_data.Items.Clear();

                AddDataTabitems();
            }
            catch (Exception ex)
            {
                Debug.WriteLine("NewTemplateWizard : tabControl_TabItemCancel = " + ex.Message.ToString());
            }
        }



        void tabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message.ToString());
            }
        }

        private bool AddDataTabitems()
        {
            try
            {
                tab_data.Items.Clear();
                DataFieldList.Clear();

                Scheduler.Template.DataInfoLoader loader = new DigitalSignage.Scheduler.Template.DataInfoLoader();
                string templateid = (pcd.CURRENTTEMPLATE.dbid.Length != 0) ? pcd.CURRENTTEMPLATE.dbid : pcd.CURRENTTEMPLATE.id;
                Scheduler.Template.DataInfoArry arry = loader.load(ServerDataCache.GetInstance.CurrentUser.Attributes["id"].Value.ToString(), templateid);
                if (arry != null)
                {
                    List<string> list = null;
                    if (ISNEW == false)
                        list = LoadExFileData("menu_list");

                    foreach (Scheduler.Template.DataInfo info in arry)
                    {
                        int i = tab_data.SelectedIndex;
                        Wpf.Controls.TabItem item = new Wpf.Controls.TabItem();
                        TextBlock tb = new TextBlock();
                        tb.Text = info.TITLE;
                        tb.TextTrimming = TextTrimming.CharacterEllipsis;
                        tb.TextWrapping = TextWrapping.NoWrap;
                        item.Header = tb;

                        tab_data.Items.Add(item);
                        tab_data.SelectedItem = item;

                        Scheduler.Template.UserControl.TemplateListviewControl _control = new DigitalSignage.Scheduler.Template.UserControl.TemplateListviewControl(this.pcd);
                        _control.CURRENT_DATAINFO = info;
                        DataFieldList.Add(info.ID);

                        _control.CurrentTemplate = pcd.CURRENTTEMPLATE;
                        if (ISNEW == false)
                            _control.DataIdList = list;
                        _control.MakeListViewColumn();

                        item.Content = _control;

                        VirtualizingTabPanel itemsHost = Wpf.Controls.Helper.FindVirtualizingTabPanel(this);
                        if (itemsHost != null)
                            itemsHost.MakeVisible(item, Rect.Empty);
                        item.Focus();

                    }
                }
                

                SetPreview();
                SetResolution();
                if (false == SetPromotion())
                {
                    GridLength length = new GridLength(1);
                    grid_promotion.RowDefinitions[0].Height = length;
                    promotion_expander.Visibility   = Visibility.Collapsed;
                    splitter.Visibility             = Visibility.Collapsed;
                }
                else
                {
                    promotion_expander.Visibility   = Visibility.Visible;
                    splitter.Visibility             = Visibility.Visible;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("NewTemplateWizard : AddDataTabitems = " + ex.Message.ToString());
            }
            return false;
        }

        private bool SetPreview()
        {
            try
            {
                string filePaht = GetContentsFolder() + pcd.CURRENTTEMPLATE.thumbnail_nm;
                if (File.Exists(filePaht))
                {
                    BitmapImage bi = new BitmapImage();
                    bi.BeginInit();
                    bi.UriSource = new Uri(filePaht, UriKind.RelativeOrAbsolute);
                    bi.EndInit();
                    previewimage.Source = bi;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("NewTemplateWizard : SetPreview = " + ex.Message.ToString());
            }
            return false;
        }

        private bool SetResult()
        {
            try
            {
                
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message.ToString());
            }
            return false;
        }

        
#endregion

        #region 리스트 박스 이벤트
        private void SetListBox_Selectindex(string id)
        {
            try
            {
                tab_data.Visibility = Visibility.Visible;

                Scheduler.Template.TemplateArry task = this.TemplateList.ItemsSource as Scheduler.Template.TemplateArry;
                int index = 0;
                foreach (Scheduler.Template.Template_T _temp in task)
                {
                    if (id.Equals(_temp.TEMPLATE.id))
                    {
                        TemplateList.SelectedIndex = index;
                        pcd.CURRENTTEMPLATE = _temp.TEMPLATE;
                        AddDataTabitems();
                        return;
                    }
                    index++;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("NewTemplateWizard : SetListBox_Selectindex = " + ex.Message.ToString());
            }
        }

        private bool GetDataSaveChecking()
        {
            try
            {
                Scheduler.Template.DataFieldArry arry = GetSelectedDataArry();
                if (arry != null)
                {
                    foreach (Scheduler.Template.DataField_T field in arry)
                    {
                        if (field.ISCHANGE == true)
                            return true;
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("NewTemplateWizard : GetDataSaveChecking = " + ex.Message.ToString());
            }

            return false;
        }

        private void ItemClicked(object sender, RoutedEventArgs e)
        {
            try
            {
                if (GetDataSaveChecking())
                {
                    string caption = "Data save";
                    MessageBoxButton buttons = MessageBoxButton.OKCancel;
                    MessageBoxResult result = System.Windows.MessageBox.Show(Properties.Resources.mbDataSaving, caption, buttons);
                    if (result == MessageBoxResult.OK)
                    {
                        try
                        {
                            for (int i = 0; i < tab_data.Items.Count; i++)
                            {
                                Wpf.Controls.TabItem tabitem = tab_data.Items[i] as Wpf.Controls.TabItem;
                                bool isChange = false;
                                Scheduler.Template.UserControl.TemplateListviewControl _control = (Scheduler.Template.UserControl.TemplateListviewControl)tabitem.Content;
                                foreach (Scheduler.Template.DataField_T field in _control.TabArry)
                                {
                                    if (field.ISCHANGE)
                                    {
                                        isChange = true;
                                        break;
                                    }
                                }
                                if (isChange == true)
                                {
                                    _control.Save_Click(null, null);
                                }
                            }
                        }
                        catch
                        {
                            return;
                        }
                    }
                }
                
                PTextControlList.Clear();
                PFileControlList.Clear();
                Button btn_Template = sender as Button;
                if (isNew == true)
                    SetListBox_Selectindex(((Scheduler.Template.Template_T)btn_Template.DataContext).TEMPLATE.id);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("NewTemplateWizard : ItemClicked = " + ex.Message.ToString());
            }
        }
        #endregion 리스트 박스 이벤트

        private Scheduler.Template.DataFieldArry GetSelectedDataArry()
        {
            try
            {
                Scheduler.Template.DataFieldArry arry = new DigitalSignage.Scheduler.Template.DataFieldArry();
                for (int i = 0; i < tab_data.Items.Count; i++)
                {
                    Wpf.Controls.TabItem tabitem = tab_data.Items[i] as Wpf.Controls.TabItem;

                    Scheduler.Template.UserControl.TemplateListviewControl _control = (Scheduler.Template.UserControl.TemplateListviewControl)tabitem.Content;
                    foreach (Scheduler.Template.DataField_T field in _control.TabArry)
                    {
                        if (field.ISSELECT == true && field.ID != null && field.ID.Length == 18)
                        {
                            arry.Add(field);
                        }
                    }
                }

                return arry;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("NewTemplateWizard : GetSelectedDataArry = " + ex.Message.ToString());
            }
            return null;
        }

        private bool MakeExFile(string filename)
        {
            if (File.Exists(filename))
                File.Delete(filename);

            Encoding code = Encoding.GetEncoding(51949);
            XmlWriter xw = new XmlTextWriter(filename, code);
            try
            {
                xw.WriteStartDocument();
                {//last_play_row_seq
                    xw.WriteStartElement("ref_array");
                    xw.WriteAttributeString("template_id", pcd.CURRENTTEMPLATE.id);
                    xw.WriteAttributeString("update_time", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                    xw.WriteAttributeString("last_play_row_seq", "0");
                    #region 반복 데이터 세팅
                    if (pcd.CURRENTTEMPLATE.type.Equals("W") == false && int.Parse(pcd.CURRENTTEMPLATE.count) > 0)
                    {
                        xw.WriteStartElement("ref_conf");
                        {
                            //menu_list
                            xw.WriteAttributeString("desc", "menu_list");
                            Scheduler.Template.DataFieldArry dataarry = GetSelectedDataArry();
                            if (dataarry == null)
                                return false;

                            if (pcd.CURRENTTEMPLATE.logo.Equals("use"))
                            {//logo_img
                                if (defaultControl == null || defaultControl.LogoFileName == "")
                                    return false;

                                xw.WriteAttributeString("logo_img", defaultControl.LogoFileName);
                            }

                            if (pcd.CURRENTTEMPLATE.weather.Equals("use"))
                            {//area
                                if (defaultControl == null || defaultControl.WeaterCode == -1)
                                    return false;

                                xw.WriteAttributeString("area", defaultControl.WeaterCode.ToString());
                            }

                            if (pcd.CURRENTTEMPLATE.backImage.Equals("use"))
                            {//bg_img
                                if (defaultControl == null || defaultControl.BackimageName == "")
                                    return false;

                                xw.WriteAttributeString("background", defaultControl.BackimageName);
                            }

                            foreach (Scheduler.Template.DataField_T field in dataarry)
                            {
                                xw.WriteStartElement("ref_row");
                                xw.WriteAttributeString("menu_id", field.ID);
                                {
                                    int index = 0;
                                    foreach (Scheduler.Template.Model col in field.CONTENTS)
                                    {
                                        if (col != null && col.CONTENTS != null && col.CONTENTS.ID != null)
                                        {
                                            xw.WriteStartElement("data");
                                            {
                                                xw.WriteAttributeString("type", col.CONTENTS.TYPE);
                                                xw.WriteAttributeString("font_size", "38");
                                                xw.WriteAttributeString("font_color", "#000000");
                                                xw.WriteAttributeString("column", pcd.CURRENTTEMPLATE.LoopDatas[index].Code);
                                                string str = col.CONTENTS.TYPE.Equals("T") ? col.CONTENTS.VALUE.Replace(",", "<br>") : col.CONTENTS.NEW_NAME;
                                                xw.WriteCData(str);
                                            }
                                            xw.WriteEndElement();
                                        }
                                        index++;
                                    }
                                }
                                xw.WriteEndElement();
                            }
                        }
                        xw.WriteEndElement();
                    }
                    #endregion 반복 데이터 세팅

                    #region 프로모션 세팅
                    if (pcd.CURRENTTEMPLATE.type.Equals("W") == false)
                    {
                        if (pcd.CURRENTTEMPLATE.promotions != null)
                        {
                            xw.WriteStartElement("ref_conf");
                            {
                                xw.WriteAttributeString("desc", "promotion_info");
                                if (pcd.CURRENTTEMPLATE.logo.Equals("use"))
                                {//logo_img
                                    if (defaultControl == null || defaultControl.LogoFileName == "")
                                        return false;

                                    xw.WriteAttributeString("logo_img", defaultControl.LogoFileName);
                                }

                                if (pcd.CURRENTTEMPLATE.weather.Equals("use"))
                                {//area
                                    if (defaultControl == null || defaultControl.WeaterCode == -1)
                                        return false;

                                    xw.WriteAttributeString("area", defaultControl.WeaterCode.ToString());
                                }

                                if (pcd.CURRENTTEMPLATE.backImage.Equals("use"))
                                {//bg_img
                                    if (defaultControl == null || defaultControl.BackimageName == "")
                                        return false;

                                    xw.WriteAttributeString("background", defaultControl.BackimageName);
                                }

                                foreach (UserControl.PROTextControl Textcontrol in PTextControlList)
                                {
                                    if (Textcontrol.Data_Value != null && Textcontrol.Data_Value.Length > 0)
                                    {
                                        xw.WriteStartElement("ref_row");
                                        {
                                            //컨텐츠 타임 구분 M: 동영상, I: 이미지, S: 플래시
                                            xw.WriteStartElement("data");
                                            {
                                                xw.WriteAttributeString("col_seq", "1");
                                                xw.WriteAttributeString("position", Textcontrol.PROMOTION.code);
                                                xw.WriteAttributeString("is_linear_included", "true");
                                                xw.WriteCData("T");
                                            }
                                            xw.WriteEndElement();
                                            // 컨텐츠 이름
                                            xw.WriteStartElement("data");
                                            {
                                                xw.WriteAttributeString("font_size", "30");
                                                xw.WriteAttributeString("font_color", Textcontrol.FontColor);
                                                xw.WriteCData(Textcontrol.Data_Value);
                                            }
                                            xw.WriteEndElement();
                                        }
                                        xw.WriteEndElement();
                                    }
                                }

                                foreach (UserControl.PROFileControl Filecontrol in PFileControlList)
                                {
                                    if (Filecontrol.Data_Value != null && Filecontrol.Data_Value.Length > 0)
                                    {
                                        xw.WriteStartElement("ref_row");
                                        {
                                            //컨텐츠 타임 구분 M: 동영상, I: 이미지, S: 플래시
                                            xw.WriteStartElement("data");
                                            {
                                                xw.WriteAttributeString("col_seq", "1");
                                                xw.WriteAttributeString("position", Filecontrol.PROMOTION.code);
                                                xw.WriteAttributeString("is_linear_included", "true");
                                                xw.WriteCData(Filecontrol.PROMOTION.cont_type);
                                            }
                                            xw.WriteEndElement();
                                            // 컨텐츠 이름
                                            xw.WriteStartElement("data");
                                            {
                                                xw.WriteAttributeString("font_size", "30");
                                                xw.WriteAttributeString("font_color", "#FFFFFF");
                                                xw.WriteCData(Filecontrol.Data_Value);
                                            }
                                            xw.WriteEndElement();
                                        }
                                        xw.WriteEndElement();
                                    }
                                }

                            }
                            xw.WriteEndElement();
                        }
                    }
                    else if (pcd.CURRENTTEMPLATE.type.Equals("W"))
                    {
                        xw.WriteStartElement("ref_conf");
                        {
                            xw.WriteAttributeString("desc", "newmenu_touch");
                            if (pcd.CURRENTTEMPLATE.logo.Equals("use"))
                            {//logo_img
                                if (defaultControl == null || defaultControl.LogoFileName == "")
                                    return false;

                                xw.WriteAttributeString("logo_img", defaultControl.LogoFileName);
                            }

                            if (pcd.CURRENTTEMPLATE.weather.Equals("use"))
                            {//area
                                if (defaultControl == null || defaultControl.WeaterCode == -1)
                                    return false;

                                xw.WriteAttributeString("area", defaultControl.WeaterCode.ToString());
                            }

                            if (pcd.CURRENTTEMPLATE.backImage.Equals("use"))
                            {//bg_img
                                if (defaultControl == null || defaultControl.BackimageName == "")
                                    return false;

                                xw.WriteAttributeString("background", defaultControl.BackimageName);
                            }

                            {
                                foreach (System.Windows.Controls.TabItem item in tab_control.Items)
                                {
                                    Scheduler.Template.UserControl.DataListControl _control = item.Content as Scheduler.Template.UserControl.DataListControl;
                                    xw.WriteStartElement("ref_day");
                                    xw.WriteAttributeString("day_id", _control.DAY_ID);
                                    Scheduler.Template.DataFieldArry DataAArry = _control.DataAArry;

                                    foreach (Scheduler.Template.DataField_T data in DataAArry)
                                    {//이미지 이동관련 추가 ............//////////////

                                        xw.WriteStartElement("ref_row");
                                        xw.WriteAttributeString("menu_id", data.ID);
                                        {
                                            int index = 0;
                                            foreach (Scheduler.Template.Model col in data.CONTENTS)
                                            {
                                                if (col != null && col.CONTENTS != null && col.CONTENTS.ID != null)
                                                {
                                                    xw.WriteStartElement("data");
                                                    {
                                                        xw.WriteAttributeString("type", col.CONTENTS.TYPE);
                                                        xw.WriteAttributeString("font_size", "38");
                                                        xw.WriteAttributeString("font_color", "#000000");
                                                        xw.WriteAttributeString("column", pcd.CURRENTTEMPLATE.LoopDatas[index].Code);
                                                        string str = col.CONTENTS.TYPE.Equals("T") ? col.CONTENTS.VALUE.Replace(",", "<br>") : col.CONTENTS.NEW_NAME;
                                                        xw.WriteCData(str);
                                                    }
                                                    xw.WriteEndElement();
                                                }
                                                index++;
                                            }
                                        }
                                        xw.WriteEndElement();
                                    }
                                    xw.WriteEndElement();
                                }
                            }

                        }
                        xw.WriteEndElement();                        
                    }
                    #endregion 프로모션 세팅
                }
                xw.WriteEndDocument();
                if (xw != null)
                {
                    xw.Close();
                }

                return true;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("NewTemplateWizard : MakeExFile = " + ex.Message.ToString());
            }

            return false;
        }

        public string GetExFilePath()
        {
            try
            {
                string FilePath = GetContentsFolder() + pcd.CURRENTTEMPLATE.ex_file_nm;
                if (File.Exists(FilePath))
                    File.Delete(FilePath);
                if (MakeExFile(FilePath) == false)
                {
                    return "";
                }

                return FilePath;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("NewTemplateWizard : GetExFilePath = " + ex.Message.ToString());
            }
            return "";
        }

        #region 미리보기 구현 부분
        void Play_timer_Tick(object sender, EventArgs e)
        {
			try
			{
				StopFlashControl();
			}
			catch { }
            //throw new NotImplementedException();
        }

        private void btn_Play_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (isPlay == false)
                {
                    if (false == ValueCheck())
                    {
                        System.Windows.MessageBox.Show(Properties.Resources.mberrdatainput,
                                                       Properties.Resources.titleMessageBox,
                                                       MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }

                    cm_preview_SizeChanged(null, null);

                    List<string> list = CheckContentsFolder();

                    if (list != null && list.Count > 0)
                    {
                        if (ContentsDownload(list) == false)
                            return;
                    }

                    int playTime = 0;
                    preview = new PrevPlayer.PlayerForm();
                    Host.Child = preview;

                    List<PrevPlayer.PlayerForm.playlist> pList = new List<PrevPlayer.PlayerForm.playlist>();

                    {
                        string exFile = GetExFilePath();
                        if (exFile == "")
                            return;
                        string swfFilePath = GetContentsFolder() + pcd.CURRENTTEMPLATE.file_nm; //dataDlg.GetExFilePath();
                        FileInfo Fileinfo = new FileInfo(swfFilePath);

                        PrevPlayer.PlayerForm.playlist p = new PrevPlayer.PlayerForm.playlist();
                        p.path = swfFilePath;
                        p.playtime = 60;
                        p.ext = Fileinfo.Extension;
                        p.ext = p.ext.Substring(1, p.ext.Length - 1);
                        p.exfilenm = exFile;
                        playTime += p.playtime;
                        pList.Add(p);
                    }
                    preview.StartPlayList(pList);
                    Play_timer.Interval = 60000;
                    Play_timer.Start();
                    isPlay = true;

                    previewimage.Visibility = Visibility.Collapsed;
                    Host.Visibility = Visibility.Visible;
                }
                else
                {
                    StopFlashControl();
                }

                PlayStatus(isPlay);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("NewTemplateWizard : btn_Play_Click = " + ex.Message.ToString());
            }
        }

        public void PlayStatus(bool isPlay)
        {
            if (isPlay == true)
            {
                btn_Play.Content = "Stop";
                btn_Play.Foreground = Brushes.Red;
            }
            else
            {
                btn_Play.Content = "Play";
                btn_Play.Foreground = Brushes.Blue;
            }
        }

        void StopFlashControl()
        {
			try
			{
				if (isPlay == true)
				{
					Play_timer.Stop();
					preview.Close();
					Host.Child = null;

					Host.Visibility = Visibility.Collapsed;
					previewimage.Visibility = Visibility.Visible;
					isPlay = false;
				}

				PlayStatus(isPlay);
			}
			catch { }
        }

        public void cm_preview_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            try
            {
                double _width = 0.0;
                double _height = 0.0;
                if (pcd.CURRENTTEMPLATE.form.ToLower().Equals("height"))
                {
                    _height = preview_grid.RenderSize.Height - 45;
                    _width = (preview_grid.RenderSize.Height - 45) * RESOUL_VALUE;
                }
                else
                {
                    _width = preview_grid.RenderSize.Width;
                    _height = preview_grid.RenderSize.Width * RESOUL_VALUE;
                }

                Host.Height = _height - 6;
                Host.Width = _width - 6;
                previewimage.Height = _height;
                previewimage.Width = _width;

                Host.UpdateLayout();
                previewimage.UpdateLayout();
            }
            catch (Exception ex)
            {
                Debug.WriteLine("NewTemplateWizard : cm_preview_SizeChanged = " + ex.Message.ToString());
            }
        }

        public bool SetResolution()
        {
            try
            {
                cb_resolution.Items.Clear();
                if (pcd.CURRENTTEMPLATE.form.ToLower().Equals("width"))
                {
                    cb_resolution.Items.Add(" 4 : 3");
                    cb_resolution.Items.Add("16 : 9");
                }
                else
                {
                    cb_resolution.Items.Add("3 :  4");
                    cb_resolution.Items.Add("9 : 16");
                }

                RESOUL_VALUE = (double)(9.0 / 16.0);
                cb_resolution.SelectedIndex = 1;
                return true;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("NewTemplateWizard : SetResolution = " + ex.Message.ToString());
            }
            return false;
        }

        private void cb_resolution_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cb_resolution.SelectedIndex > -1)
            {
                double res = 0.0;

                if (cb_resolution.SelectedIndex == 0)
                    res = (double)(3.0 / 4.0);
                else
                    res = (double)(9.0 / 16.0);

                RESOUL_VALUE = res;
                cm_preview_SizeChanged(null, null);
            }
        }

        #endregion
        private int GetDefaultData()        
        {
            int count = 0;
            if (pcd.CURRENTTEMPLATE.backImage.Equals("use"))
                count++;
            if (pcd.CURRENTTEMPLATE.logo.Equals("use"))
                count++;
            if (pcd.CURRENTTEMPLATE.weather.Equals("use"))
                count++;

            return count;
        }

        #region 프로모션 구현 부분
        private bool SetPromotion()
        {
            try
            {
                isDefaultSetting = false;
                gridpromotionHight = 0;
                int iDcount = GetDefaultData();
                if (pcd.CURRENTTEMPLATE.promotions == null && iDcount == 0)
                {                    
                    return false;
                }
                _AddUserControl.Children.Clear();
                textColors.Clear();
                if (iDcount > 0)
                {
                    isDefaultSetting = true;
                    defaultControl = new DigitalSignage.Scheduler.Template.UserControl.WeatherControl();
                    defaultControl.MAINWINDOW = this;
                    defaultControl.CURRENTTEMPLATE = pcd.CURRENTTEMPLATE;

                    defaultControl.LogoFileName  = GetLoadData("logo_img");
                    try
                    {
                        defaultControl.WeaterCode = int.Parse(GetLoadData("area"));
                    }
                    catch
                    {//기본을 서울로 설정 함.
                        defaultControl.WeaterCode = 9;
                    }
                    defaultControl.BackimageName = GetLoadData("background");

                    defaultControl.SetValue();
                   
                    _AddUserControl.Children.Add(defaultControl);
                }

                if (pcd.CURRENTTEMPLATE.type.Equals("W") == false)
                {

                    Scheduler.Template.promotion[] pm = pcd.CURRENTTEMPLATE.promotions;
                    List<string> textlist = LoadExFileData("promotion_info", true);
                    List<string> movielist = LoadExFileData("promotion_info", false);
                    int itextCnt = 0;
                    int imovieCnt = 0;

                    int textCount = 0;
                    int fileCount = 0;
                    PTextControlList.Clear();
                    PFileControlList.Clear();

                    if (pm != null)
                    {
                        foreach (Scheduler.Template.promotion pmt in pm)
                        {
                            string textColor = "000000";
                            if ((pmt.cont_type.Equals("텍스트") || pmt.cont_type.Equals("T")) && textlist != null && textlist.Count > 0 && textlist.Count > itextCnt)
                            {
                                textColor   = textColors[itextCnt];
                                pmt.cont_text = textlist[itextCnt++];
                            }
                            else if (movielist != null && movielist.Count > 0 && movielist.Count > imovieCnt)
                                pmt.cont_nm = movielist[imovieCnt++];

                            if (pmt.cont_type.Equals("텍스트") || pmt.cont_type.Equals("T"))
                            {
                                UserControl.PROTextControl pControl = new UserControl.PROTextControl();
                                pControl.PROMOTION = pmt;
                                if (pcd.CURRENTTEMPLATE.backImage.Equals("use"))
                                    pControl.FontColor = textColor;
                                pControl.Data_Value = (pmt.cont_text != null) ? pmt.cont_text : "";
                                this.PTextControlList.Add(pControl);
                                _AddUserControl.Children.Add(pControl);
                                textCount++;

                            }
                            else
                            {
                                UserControl.PROFileControl pControl = new UserControl.PROFileControl(this);
                                pControl.PROMOTION = pmt;
                                pControl.Data_Value = (pmt.cont_nm != null) ? pmt.cont_nm : "";
                                PFileControlList.Add(pControl);
                                _AddUserControl.Children.Add(pControl);
                                fileCount++;
                            }
                        }
                    }
                    gridpromotionHight = (textCount * 30) + (28 * (fileCount + 1)) + (iDcount * 27);
                }
                else
                {
                    Scheduler.Template.promotion[] pm = pcd.CURRENTTEMPLATE.promotions;
                    if (pm != null)
                    {
                        if (tab_control != null)
                        {
                            tab_control.Items.Clear();
                        }

                        tab_control = new System.Windows.Controls.TabControl();
                        tab_control.Height = 200;
                        foreach (Scheduler.Template.promotion pmt in pm)
                        {
                            System.Windows.Controls.TabItem item = new System.Windows.Controls.TabItem();
                            TextBlock tb = new TextBlock();
                            tb.Text = pmt.name;
                            tb.TextTrimming = TextTrimming.CharacterEllipsis;
                            tb.TextWrapping = TextWrapping.NoWrap;
                            item.Header = tb;

                            Scheduler.Template.UserControl.DataListControl dControl = new DigitalSignage.Scheduler.Template.UserControl.DataListControl(this, pcd.CURRENTTEMPLATE, pmt.code);

                            item.Content = dControl;
                            tab_control.Items.Add(item);
                            tab_control.SelectedItem = item;
                            item.Focus();

                        }
                        _AddUserControl.Children.Add(tab_control);

                        gridpromotionHight = 230 + (iDcount * 27);
                    }
                    else
                    {
                        gridpromotionHight = 1 + (iDcount * 27);
                        
                    }
                }

                if (gridpromotionHight != 0 && promotion_expander.IsExpanded)
                {
                    GridLength length = new GridLength(gridpromotionHight);
                    grid_promotion.RowDefinitions[0].Height = length;
                }
                else
                {
                    GridLength length = new GridLength(gridpromotionHight);
                    starHeight[0] = length; 
                }

                return true;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("NewTemplateWizard : SetPromotion = " + ex.Message.ToString());
            }
            return false;
        }

        private string GetLoadData(string code)
        {
            string path = m_media_folder + pcd.CURRENTTEMPLATE.ex_file_nm;
            if (!File.Exists(path))
                return "";
            try
            {
                List<string> selectlist = new List<string>();
                XmlDocument _doc = new XmlDocument();
                _doc.Load(path);
                XmlNode node = _doc.SelectSingleNode("//ref_array/ref_conf");

                return node.Attributes[code].Value;
            }
            catch
            {
            }
            return "";
        }

        private List<string> LoadExFileData(string keyvalue, bool isText)
        {

            string path = m_media_folder + pcd.CURRENTTEMPLATE.ex_file_nm;
            if (!File.Exists(path))
                return null;

            try
            {               

                List<string> selectlist = new List<string>();
                XmlDocument _doc = new XmlDocument();
                _doc.Load(path);
                XmlNode xNode = _doc.DocumentElement;
                XmlNodeList _menuList = xNode.ChildNodes; ;
                for (int i = 0; i < _menuList.Count; i++)
                {
                    if (_menuList[i].Attributes["desc"].Value.Equals(keyvalue))
                    {
                        XmlNodeList list = _menuList[i].ChildNodes;
                        for (int j = 0; j < list.Count; j++)
                        {
                            XmlNode firstnode = list[j].FirstChild;
                            XmlNode lastnode = list[j].LastChild;
                            if (isText)
                            {
                                if (firstnode.InnerText.Equals("T") && lastnode.InnerText.Length > 0)
                                {
                                    selectlist.Add(lastnode.InnerText);
                                    textColors.Add(lastnode.Attributes["font_color"].Value);
                                }
                            }
                            else
                            {
                                if (!firstnode.InnerText.Equals("T") && lastnode.InnerText.Length > 0)
                                    selectlist.Add(lastnode.InnerText);
                            }
                        }
                    }
                }

                return selectlist;
            }
            catch (ArgumentException aex)
            {
                Debug.WriteLine("NewTemplateWizard : LoadExFileData = " + aex.Message.ToString());
            }
            catch (InvalidOperationException iex)
            {
                Debug.WriteLine("NewTemplateWizard : LoadExFileData = " + iex.Message.ToString());
            }
            catch (XmlException xmlEx)
            {
                Debug.WriteLine("NewTemplateWizard : LoadExFileData = " + xmlEx.Message.ToString());
            }
            catch (Exception ex)
            {
                Debug.WriteLine("NewTemplateWizard : LoadExFileData = " + ex.Message.ToString());
            }

            return null;
        }

        #endregion

        void ExpandedOrCollapsed(object sender, RoutedEventArgs e) 
        { 
            ExpandedOrCollapsed(sender as Expander); 
        }        
        
        void ExpandedOrCollapsed(Expander expander) 
        { 
            var rowIndex = Grid.GetRow(expander);
            var row = grid_promotion.RowDefinitions[rowIndex]; 
            if (expander.IsExpanded) 
            { 
                row.Height = starHeight[rowIndex]; 
                if (promotion_expander.Visibility != Visibility.Collapsed)
                    row.MinHeight = 50; 
                else
                    row.MinHeight = 0; 
            } 
            else 
            { 
                starHeight[rowIndex] = row.Height; 
                row.Height = GridLength.Auto; 
                row.MinHeight = 0; 
            }
            var bothExpanded = promotion_expander.IsExpanded && preview_expander.IsExpanded; 
            splitter.Visibility = bothExpanded ? Visibility.Visible : Visibility.Collapsed; 
        }

        private void cb_category_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int index = cb_category.SelectedIndex;
            if (index > -1)
            {
                TemplateListLoader loader = new TemplateListLoader();
                TemplateArry arry = loader.GetTemplateList(index);
                if (arry != null)
                {
                    TemplateList.ItemsSource = arry;
                }
            }

        }   
    }
}
