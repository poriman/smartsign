﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigitalSignage.Scheduler.Template.Wizard
{
    public class PageCommand
    {
        private Template.template currentTemplate;
        private NewTemplateWizard mainWindow;
        private string _itemCount;
        public Template.template CURRENTTEMPLATE
        {
            set { currentTemplate = value; }
            get { return currentTemplate; }
        }

        public string itemCount
        {
            set { _itemCount = value; }
            get { return _itemCount; }
        }

        public NewTemplateWizard MAINWINDOW
        {
            set { mainWindow = value; }
            get { return mainWindow; }
        }
    }
}
