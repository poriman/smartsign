﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DigitalSignage.Scheduler.Template.Wizard
{
    /// <summary>
    /// AddTabDlg.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class AddTabDlg : Window
    {
        private string title;
        public string TITLE
        {
            set { title = value; }
            get { return title; }
        }

        public AddTabDlg(bool isTabAdd)
        {
            InitializeComponent();
			tab_name.Focus();

			if (isTabAdd == false)
            {
                this.title = Properties.Resources.titleAddtemplate;
            }

        }

        private void btn_OK_Click(object sender, RoutedEventArgs e)
        {
            if (tab_name.Text.Length <= 0)
            {//mbTabName
                MessageBox.Show(Properties.Resources.mbTabName, 
                                Properties.Resources.titleMessageBox,
                                MessageBoxButton.OK, MessageBoxImage.Stop);
                return;
            }
            TITLE = tab_name.Text;
            this.DialogResult = true;
        }

        private void btn_Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}
