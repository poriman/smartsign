﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using DigitalSignage.Common;
using System.IO;


namespace DigitalSignage.Scheduler.Template.Wizard
{
    class ContensUpDn
    {
        public EventHandler OnSetProgress = null;
        public EventHandler OnStepIt = null;
        public StatusEventHandler OnUpdateLabel = null;
        public event StatusEventHandler OnUpdateTitle = null;

        private string tempDir = null;
        private int id = 1;
        private int pid = 1;
        private Config config = null;
        private string user_id;

        private string lastErrorMessage;

        public string USER_ID
        {
            set { user_id = value; }
            get { return user_id; }
        }

        public string LastErrorMessage
        {
            get { return lastErrorMessage; }
        }


        public bool ContentsUpload(Config cfg, String group, string path)
        {
            using (new DigitalSignage.Common.WaitCursor())
            {
                String[] arrFTPInfo = group.Split('|');
                try
                {
                    tempDir = path;

                    String sTitle = String.Format("{0} ({1}({2}) - ftp://{3}@{4}/{5})", Properties.Resources.progNowUploading, "", "", "", arrFTPInfo[0], arrFTPInfo[3]);
                    if (OnUpdateTitle != null) OnUpdateTitle(this, new StatusEventArgs(sTitle));

                    // FTP transfer
                    FTPFunctions ftp = new FTPFunctions(cfg.AnalyzeSource(arrFTPInfo[0]), arrFTPInfo[3], arrFTPInfo[1], arrFTPInfo[2], cfg.FTP_Timeout);

                    //	hsshin 이벤트 추가
                    if (OnSetProgress != null) ftp.OnSetProgress += OnSetProgress;
                    if (OnStepIt != null) ftp.OnStepIt += OnStepIt;
                    if (OnUpdateLabel != null) ftp.OnUpdateLabel += OnUpdateLabel;

                    ftp.GetRecursiveFileCount(tempDir);

                    //	업로드 실패
                    if (false == ftp.FTPRecursiveCopy(tempDir, USER_ID.ToString(), "", true))
                    {
                        lastErrorMessage = String.Format(Properties.Resources.mbErrorUploadToFTP, String.Format("{0}({1}) - FTP (ftp://{2}@{3}/{4})", Properties.Resources.progNowUploading, "", "", arrFTPInfo[0], arrFTPInfo[3]));
                        return false;
                    }

                    return true;

                }
                catch (Exception ee)
                {
                    lastErrorMessage = ee.Message + " - " + String.Format("{0}({1}) - FTP (ftp://{2}@{3}/{4})", Properties.Resources.progNowUploading, "", "", "", arrFTPInfo[0], arrFTPInfo[3]);
                }
            }

            return false;
        }

        public bool ContentsDownload(Config cfg, String group, string localpath, string filename)
        {
            using (new DigitalSignage.Common.WaitCursor())
            {
                String[] arrFTPInfo = group.Split('|');
                try
                {  
                    String sTitle = String.Format("{0} ({1}({2}) - ftp://{3}@{4}/{5})", Properties.Resources.titleContentsDownload, "", "", "", arrFTPInfo[0], arrFTPInfo[3]);
                    if (OnUpdateTitle != null) OnUpdateTitle(this, new StatusEventArgs(sTitle));

                    // FTP transfer
                    FTPFunctions ftp = new FTPFunctions(cfg.AnalyzeSource(arrFTPInfo[0]), arrFTPInfo[3], arrFTPInfo[1], arrFTPInfo[2], cfg.FTP_Timeout);

                    //	hsshin 이벤트 추가
                    if (OnSetProgress != null) ftp.OnSetProgress += OnSetProgress;
                    if (OnStepIt != null) ftp.OnStepIt += OnStepIt;
                    if (OnUpdateLabel != null) ftp.OnUpdateLabel += OnUpdateLabel;

                    //	업로드 실패
                    string source = USER_ID.ToString();
                    if (false == ftp.FTPFileDownload(source, localpath, filename))
                    {
                        lastErrorMessage = String.Format(Properties.Resources.mbErrorUploadToFTP, String.Format("{0}({1}) - FTP (ftp://{2}@{3}/{4})", Properties.Resources.titleContentsDownload, "", "", "", arrFTPInfo[0], arrFTPInfo[3]));
                        return false;
                    }

                    return true;

                }
                catch (Exception ee)
                {
                    lastErrorMessage = ee.Message + " - " + String.Format("{0}({1}) - FTP (ftp://{2}@{3}/{4})", Properties.Resources.titleContentsDownload, "", "", "", arrFTPInfo[0], arrFTPInfo[3]);
                }
            }

            return false;
        }

        public void RemoveTemporaryFiles()
        {
            using (new DigitalSignage.Common.WaitCursor())
            {
                try
                {
                    //removing temporary directory
                    FTPFunctions.RecursiveDelete(tempDir, true);
                }
                catch { }
            }
        }
    }


}
