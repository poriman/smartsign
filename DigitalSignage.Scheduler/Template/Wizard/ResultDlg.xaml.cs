﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Xml;
using System.IO;
using System.Data;
using System.Diagnostics;

namespace DigitalSignage.Scheduler.Template.Wizard
{
    /// <summary>
    /// ResultDlg.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class ResultDlg : Window
    {
        Config m_cfg;
        PageCommand m_pcd;
        string m_dataid = "";
        string[] m_HeaderName;
        string[] m_excelHeader;
        List<string> m_playerName = new List<string>();

        public ResultDlg(Config cfg, PageCommand pcd)
        {
            InitializeComponent();
            m_cfg = cfg;
            m_pcd = pcd;

            LoadPlayer();
            SetListColum();
        }

        public ResultDlg(Config cfg, PageCommand pcd, string dataid)
        {
            InitializeComponent();
            m_cfg = cfg;
            m_pcd = pcd;
            m_dataid = dataid;

            LoadPlayer();
            SetListColum();
        }

        private void LoadPlayer()
        {
            res_GPTree.SetParent(m_cfg);
            res_GPTree.ShowPlayers = true;

            res_GPTree.UpdateLayout();
            res_GPTree.SelectedItem = null;

            startDateUI.SelectedDateTime = DateTime.Today;
            
            endDateUI.SelectedDateTime = DateTime.Today;
        }

        private List<string> LoadResultHeader()
        {
            List<string> list = new List<string>();
            string path = m_pcd.MAINWINDOW.GetTemplateXmlFolder() + "ResDataHeaderInfo.xml";
            if (!File.Exists(path))
                return list;

            try
            {
                List<string> selectlist = new List<string>();
                XmlDocument _doc = new XmlDocument();
                _doc.Load(path);
                XmlNodeList _headlist = _doc.DocumentElement.ChildNodes;
                for (int i = 0; i < _headlist.Count; i++)
                {
                    if (_headlist[i].Name.Equals("DataHeader") && _headlist[i].Attributes["Type"].Value.Equals(m_pcd.CURRENTTEMPLATE.HeaderType))
                    {
                        XmlNodeList childList = _headlist[i].ChildNodes;
                        for (int t = 0; t < childList.Count; t++)
                        {
                            string value = string.Format("Value{0:00}", t+1);
                            for (int j = 0; j < childList.Count; j++)
                            {
                                if (childList[j].Attributes["id"].Value.Equals(value))
                                {
                                    selectlist.Add(childList[j].Attributes["desc"].Value);
                                    break;
                                }
                            }
                        }
                    }
                }

                return selectlist;
            }
            catch (ArgumentException aex)
            {
                MessageBox.Show(aex.Message.ToString());
                Debug.WriteLine("ResultDlg : LoadResultHeader = " + aex.Message.ToString());
            }
            catch (InvalidOperationException iex)
            {
                MessageBox.Show(iex.Message.ToString());
                Debug.WriteLine("ResultDlg : LoadResultHeader = " + iex.Message.ToString());
            }
            catch (XmlException xmlEx)
            {
                MessageBox.Show(xmlEx.Message.ToString());
                Debug.WriteLine("ResultDlg : LoadResultHeader = " + xmlEx.Message.ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
                Debug.WriteLine("ResultDlg : LoadResultHeader = " + ex.Message.ToString());
            }

            return list;
        }

        private void SetListColum()
        {
            List<string> HeaderList = LoadResultHeader();
            m_HeaderName = new string[HeaderList.Count];
            m_excelHeader = new string[HeaderList.Count + 3];
            m_excelHeader[0] = Properties.Resources.labelDate;
            m_excelHeader[1] = Properties.Resources.labelPlayerID;
            m_excelHeader[2] = "Name";

            GridView grvlist = new GridView();
            list_resValue.View = grvlist;
            GridViewColumn col = null;
            DataTemplate template = null;
            FrameworkElementFactory elusercontrol = null;
            Binding bind = null;

            string title = string.Format("Date");
            col = new GridViewColumn();
            col.Header = Properties.Resources.labelDate;
            col.Width = 80;
            template = new DataTemplate();
            col.CellTemplate = template;
            elusercontrol = new FrameworkElementFactory(typeof(TextBlock));
            template.VisualTree = elusercontrol;
            bind = new Binding(title);
            elusercontrol.SetBinding(TextBlock.TextProperty, bind);
            grvlist.Columns.Add(col);

            title = string.Format("PlayerID");
            col = new GridViewColumn();
            col.Header = Properties.Resources.labelPlayerID;
            col.Width = 80;
            template = new DataTemplate();
            col.CellTemplate = template;
            elusercontrol = new FrameworkElementFactory(typeof(TextBlock));
            template.VisualTree = elusercontrol;
            bind = new Binding(title);
            elusercontrol.SetBinding(TextBlock.TextProperty, bind);
            grvlist.Columns.Add(col);

            title = string.Format("Name");
            col = new GridViewColumn();
            col.Header = "Name";
            col.Width = 80;
            template = new DataTemplate();
            col.CellTemplate = template;
            elusercontrol = new FrameworkElementFactory(typeof(TextBlock));
            template.VisualTree = elusercontrol;
            bind = new Binding(title);
            elusercontrol.SetBinding(TextBlock.TextProperty, bind);
            grvlist.Columns.Add(col);

            for (int i = 0; i < HeaderList.Count; i++)
            {

                title = string.Format("VALUES[{0}]", i);
                col = new GridViewColumn();
                col.Header = HeaderList[i];
                m_HeaderName[i] = HeaderList[i];
                m_excelHeader[i + 3] = HeaderList[i];
                col.Width = 80;
                template = new DataTemplate();

                col.CellTemplate = template;

                elusercontrol = new FrameworkElementFactory(typeof(TextBlock));
                template.VisualTree = elusercontrol;
                bind = new Binding(title);
                elusercontrol.SetBinding(TextBlock.TextProperty, bind);

                grvlist.Columns.Add(col);
            }

        }

        private void btn_serch_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string[] players = LoadByPlayers();
                if (players == null || players.Length == 0)
                {
                    MessageBox.Show(Properties.Resources.exceptionCantSpecified,
                                    Properties.Resources.titleMessageBox,
                                    MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                if (startDateUI.SelectedDateTime > endDateUI.SelectedDateTime)
                {
                    MessageBox.Show(Properties.Resources.mbErrorTimeZone,
                                    Properties.Resources.titleMessageBox,
                                    MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                long from_date = long.Parse(string.Format("{0}{1:00}{2:00}", startDateUI.SelectedDateTime.Value.Year, startDateUI.SelectedDateTime.Value.Month, startDateUI.SelectedDateTime.Value.Day));
                long end_date = long.Parse(string.Format("{0}{1:00}{2:00}", endDateUI.SelectedDateTime.Value.Year, endDateUI.SelectedDateTime.Value.Month, endDateUI.SelectedDateTime.Value.Day));
                ResultDataLoader loder = new ResultDataLoader();

                if (m_dataid != "")
                {
                    list_resValue.ItemsSource = loder.LoadByDataid(from_date, end_date, m_dataid, players, m_playerName);
                }
                else
                {
                    string templateid = (m_pcd.CURRENTTEMPLATE.dbid.Length != 0) ? m_pcd.CURRENTTEMPLATE.dbid : m_pcd.CURRENTTEMPLATE.id;
                    list_resValue.ItemsSource = loder.LoadByTemplateid(from_date, end_date, templateid, players, m_playerName);
                }

                if (list_resValue.ItemsSource == null || list_resValue.Items.Count == 0)
                {

                    MessageBox.Show(Properties.Resources.mbNoResult,
                                    Properties.Resources.titleMessageBox,
                                    MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }

                ResDataChartValueArry dataArry = new ResDataChartValueArry();
                string[] Values = new string[m_HeaderName.Length];
                for (int i = 0; i < m_HeaderName.Length; i++)
                {
                    ResDataChartValue data = new ResDataChartValue();
                    data.VALUE = loder.TOTALVALUES[i].ToString();
                    Values[i] = loder.TOTALVALUES[i].ToString();
                    data.TITLE = m_HeaderName[i];
                    dataArry.Add(data);
                }
                {//상단의 총 합 표시을 위해.
                    ResultDataArry arry = list_resValue.ItemsSource as ResultDataArry;
                    ResultData tdata = new ResultData();
                    tdata.Date = "";
                    tdata.PlayerID = "";
                    tdata.Name = "";
                    tdata.VALUES = Values;

                    arry.Insert(0, tdata);
                    list_resValue.ItemsSource = arry;
                }


                result_chart.Reset();
                result_chart.SmartAxisLabel = true;
                result_chart.Title = "";
                result_chart.ValueField.Add("Amount");
                result_chart.ToolTipText = "{field}";
                // result_chart.XAxisText = "Sales";
                result_chart.XAxisField = "Region";
                result_chart.ShowValueOnBar = true;
                string path = m_pcd.MAINWINDOW.GetTempFolder() + "ResultData.xml";
                if (CreateExDataXML(path, dataArry))
                {
                    System.Data.DataSet ds = new System.Data.DataSet("dsSales");
                    ds.ReadXml(path);
                    result_chart.SetSize(chart_view.ActualHeight, chart_view.ActualWidth);
                    result_chart.DataSource = ds;
                    result_chart.Generate();
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("ResultDlg : btn_serch_Click = " + ex.Message.ToString());
            }
            chart_view_SizeChanged(null, null);
        }

        private bool CreateExDataXML(string filename, ResDataChartValueArry dataArry)
        {
            if (File.Exists(filename))
                File.Delete(filename);

            Encoding code = Encoding.UTF8;
            XmlWriter xw = new XmlTextWriter(filename, code);
            int index = 1;
            try
            {
                xw.WriteStartDocument();
                {//last_play_row_seq
                    xw.WriteStartElement("SalesSummary");
                    {
                        foreach (ResDataChartValue value in dataArry)
                        {
                            xw.WriteStartElement("Sales");
                            {
                                xw.WriteAttributeString("Id", index.ToString());
                                xw.WriteAttributeString("Region", value.TITLE);
                                xw.WriteAttributeString("Amount", value.VALUE);
                            }
                            xw.WriteEndElement();
                            index++;
                        }
                    }
                    xw.WriteEndElement();
                }
                xw.WriteEndDocument();
                if (xw != null)
                {
                    xw.Close();
                }
                return true;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("ResultDlg : CreateExDataXML = " + ex.Message.ToString());
            }

            return false;
        }

        private string[] LoadByPlayers()
        {
            using (new DigitalSignage.Common.WaitCursor())
            {
                try
                {
                    m_playerName.Clear();
                    if (!res_GPTree.SelectedGroupGID.Equals("-1"))
                    {
                        ServerDataCache.GetInstance.UpdatePlayersInGroup(res_GPTree.SelectedGroupGID);
                        XmlNodeList nodelist = ServerDataCache.GetInstance.GetPlayerList(res_GPTree.SelectedGroupGID, false);
                        string[] players = new string[nodelist.Count];
                        int index = 0;
                        foreach (XmlNode node in nodelist)
                        {
                            XmlDocument doc = new XmlDocument();
                            doc.LoadXml(node.OuterXml);
                            XmlNode _node = doc.SelectSingleNode("//child::player");

                            players[index++] = _node.Attributes["pid"].Value;
                            m_playerName.Add(_node.Attributes["name"].Value);
                        }
                        return players;
                    }
                    else
                    {
                        if (String.IsNullOrEmpty(res_GPTree.SelectPlayerID) || res_GPTree.SelectPlayerID == "-1") return null;

                        string[] player = new string[1];
                        player[0] = res_GPTree.SelectPlayerID;

                        XmlNode _node = ServerDataCache.GetInstance.GetPlayer(res_GPTree.SelectPlayerID);
                        m_playerName.Add(_node.Attributes["name"].Value);
                        return player;
                    }

                }
                catch (Exception ex)
                {
                    Debug.WriteLine("ResultDlg : LoadByPlayers = " + ex.Message.ToString());
                }
            }

            return null;
        }

        private void chart_view_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            result_chart.SetSize(chart_view.ActualHeight - 10, chart_view.ActualWidth - 10);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ResultDataArry arry = list_resValue.ItemsSource as ResultDataArry;

                {
                    Scheduler.Template.Charts.ExcelFileWriter<ResultDataArry> _Excel = new ExcelWrite(m_excelHeader);

                    string fileName = DateTime.Now.ToString("yyyyMMdd") + "_설문조사결과.xls";
                    string path = m_pcd.MAINWINDOW.GetTempFolder() + fileName;
                    if (File.Exists(path))
                        File.Delete(path);

                    _Excel.WriteDateToExcel(path, arry, "A1", "Z1");

                    System.Diagnostics.Process process = new System.Diagnostics.Process();
                    System.Diagnostics.ProcessStartInfo info = new System.Diagnostics.ProcessStartInfo(path);
                    process.StartInfo = info;
                    process.Start();

                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("ResultDlg : Button_Click = " + ex.Message.ToString());
            }
        }
    }

    public class ExcelWrite : Scheduler.Template.Charts.ExcelFileWriter<ResultDataArry>
    {
        public object[,] myExcelData;
        private int myRowCnt;
        object[] headerName = null;

        public ExcelWrite(string[] hearder)
        {
            headerName = hearder;
        }

        public override object[] Headers
        {
            get
            {                
                return headerName;
            }
        }

        public override void FillRowData(object list)
        {
            ResultDataArry _list = (ResultDataArry)list;
            myRowCnt = _list.Count;
            myExcelData = new object[RowCount + 1, Headers.Length];
            int row = 1;
            foreach (ResultData result in _list)
            {
                myExcelData[row, 0] = result.Date;
                myExcelData[row, 1] = result.PlayerID;
                myExcelData[row, 2] = result.Name; 
                for (int i = 0; i < Headers.Length - 3; i++)
                {
                    myExcelData[row, i + 3] = result.VALUES[i];                    
                }
                row++;
            }
        }

        public override object[,] ExcelData
        {
            get
            {
                return myExcelData;
            }
        }

        public override int ColumnCount
        {
            get
            {
                return Headers.Length;
            }
        }

        public override int RowCount
        {
            get
            {
                return myRowCnt;
            }
        }
    }
}
