﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Diagnostics;

namespace DigitalSignage.Scheduler.Template
{
    public class DataField_T
    {
        private bool isSelect;
        private bool isChanage;
        private string id;
        private string datainfo_id;
        private string template_id;
        private Model[] contents;

        public bool ISSELECT
        {
            set { isSelect = value;}
            get { return isSelect; }
        }

        public bool ISCHANGE
        {
            set 
            { 
                isChanage = value;
            }
            get { return isChanage; }
        }

        public string ID
        {
            set { id = value; }
            get { return id; }
        }

        public string TEMPLATEID
        {
            set { template_id = value; }
            get { return template_id; }
        }

        public string DATAINFO_ID
        {
            set { datainfo_id = value; }
            get { return datainfo_id; }
        }

        public Model[] CONTENTS
        {
            set { contents = value; }
            get { return contents; }
        }
    }

    public class DataFieldArry : ObservableCollection<DataField_T>
    {
        public DataFieldArry() : base() { }
    }

    public class DataLoader
    {
        private List<Model> contentslist = new List<Model>();

        public DataFieldArry Load(string datainfo_id)
        {
            DataFieldArry Arry = new DataFieldArry();
            Config cfg = Config.GetConfig;
            try
            {
                using (ServerDatabase.ServerDataSet.Data_FieldDataTable tb = cfg.ServerDataFieldList.GetDataFieldList(datainfo_id))
                {
                    foreach (ServerDatabase.ServerDataSet.Data_FieldRow row in tb.Rows)
                    {
                        if (LoadbyDataFieldID(cfg, row.Id) == false)
                            continue;

                        DataField_T data = new DataField_T();
                        data.ID = row.Id;
                        data.DATAINFO_ID = row.DataInfo_id;
                        data.ISSELECT = false;
                        data.ISCHANGE = false;

                        List<string> values = new List<string>();

                        values.Add(row.IsValue1Null()?"":row.Value1);
                        values.Add(row.IsValue2Null()?"":row.Value2);
                        values.Add(row.IsValue3Null()?"":row.Value3);
                        values.Add(row.IsValue4Null()?"":row.Value4);
                        values.Add(row.IsValue5Null()?"":row.Value5);
                        values.Add(row.IsValue6Null()?"":row.Value6);
                        values.Add(row.IsValue7Null()?"":row.Value7);
                        values.Add(row.IsValue8Null()?"":row.Value8);
                        values.Add(row.IsValue9Null()?"":row.Value9);
                        values.Add(row.IsValue10Null()?"":row.Value10);

                        values.Add(row.IsValue11Null() ? "" : row.Value11);
                        values.Add(row.IsValue12Null() ? "" : row.Value12);
                        values.Add(row.IsValue13Null() ? "" : row.Value13);
                        values.Add(row.IsValue14Null() ? "" : row.Value14);
                        values.Add(row.IsValue15Null() ? "" : row.Value15);
                        values.Add(row.IsValue16Null() ? "" : row.Value16);
                        values.Add(row.IsValue17Null() ? "" : row.Value17);
                        values.Add(row.IsValue18Null() ? "" : row.Value18);
                        values.Add(row.IsValue19Null() ? "" : row.Value19);
                        values.Add(row.IsValue20Null() ? "" : row.Value20);

                        values.Add(row.IsValue21Null() ? "" : row.Value21);
                        values.Add(row.IsValue22Null() ? "" : row.Value22);
                        values.Add(row.IsValue23Null() ? "" : row.Value23);
                        values.Add(row.IsValue24Null() ? "" : row.Value24);
                        values.Add(row.IsValue25Null() ? "" : row.Value25);
                        values.Add(row.IsValue26Null() ? "" : row.Value26);
                        values.Add(row.IsValue27Null() ? "" : row.Value27);
                        values.Add(row.IsValue28Null() ? "" : row.Value28);
                        values.Add(row.IsValue29Null() ? "" : row.Value29);
                        values.Add(row.IsValue30Null() ? "" : row.Value30);

                        Model[] contents = new Model[values.Count];
                        for(int i = 0; i < values.Count; i++)
                        {
                            contents[i] = GetContentsbyID(values[i]);
                        }

                        data.CONTENTS = contents;

                        Arry.Add(data);
                    }

                    tb.Dispose();
                }
                return Arry;
                
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message.ToString());
            }
            return null;
        }

        protected bool LoadbyDataFieldID(Config cfg, string datafield_id)
        {
            try
            {
                contentslist.Clear();

                using(ServerDatabase.ServerDataSet.ContentsDataTable tb = cfg.ServerContentsList.GetDatabyDataFieldid(datafield_id))
                {
                    
                    foreach (ServerDatabase.ServerDataSet.ContentsRow row in tb.Rows)
                    {
                        Model _model = new Model();
                        Contents cont = new Contents();

                        cont.ISCHANGE = false;
                        cont.ID = row.id;
                        cont.DATAFIELD_ID = row.Data_field_id;
                        cont.VALUE = row._Value;
                        cont.NEW_NAME = row.File_nm;
                        cont.TYPE = row._Type;
                        cont.PATH = row._Path;

                        _model.CONTENTS = cont;
                        contentslist.Add(_model);
                    }
                    tb.Dispose();
                }
                return true;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message.ToString());
            }
            return false;
        }

        private Model GetContentsbyID(string cont_id)
        {
            try
            {
                foreach (Model cont in contentslist)
                {
                    if (cont.CONTENTS.ID == cont_id)
                        return cont;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message.ToString());
            }
            return null;
        }
    }
}
