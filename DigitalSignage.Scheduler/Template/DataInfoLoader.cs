﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Diagnostics;

namespace DigitalSignage.Scheduler.Template
{
    public class DataInfo
    {
        private string id;
        private string title;
        private string template_id;
        private string user_id;

        public string ID
        {
            set { id = value; }
            get { return id; }
        }

        public string TITLE
        {
            set { title = value; }
            get { return title; }
        }

        public string TEMPLATE_ID
        {
            set { template_id = value; }
            get { return template_id; }
        }

        public string USER_ID
        {
            set { user_id = value; }
            get { return user_id; }
        }
    }

    public class DataInfoArry : ObservableCollection<DataInfo>
    {
        public DataInfoArry() : base() { }
    }

    public class DataInfoLoader
    {
        public DataInfoArry load(string user_id, string template_id)
        {
            Config cfg = Config.GetConfig;
            try
            {
                DataInfoArry arry = new DataInfoArry();

                using (ServerDatabase.ServerDataSet.DataInfoDataTable tb = cfg.ServerDataInfoList.GetDatainfoList(long.Parse(user_id), template_id))
                {
                    foreach(ServerDatabase.ServerDataSet.DataInfoRow row in tb)
                    {
                        DataInfo info = new DataInfo();
                        info.ID = row.Id;
                        info.TITLE = row._Title;
                        info.USER_ID = row.User_id.ToString();
                        info.TEMPLATE_ID = row.Template_id;

                        arry.Add(info);
                    }

                    tb.Dispose();
                }

                return arry;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message.ToString());
            }
            return null;
        }
    }
}
