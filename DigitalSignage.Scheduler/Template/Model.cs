﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigitalSignage.Scheduler.Template
{
    public class Model : ViewModelBase
    {
        private Contents contents;

        public Contents CONTENTS
        {
            set { contents = value; OnPropertyChanged("CONTENTS"); }
            get { return contents; }
        }

        public Model()
        {
        }

        public Model(string type)
        {
            contents = new Contents();
            contents.TYPE = type;
            contents.ISCHANGE = false;
            contents.VALUE = "";
        }
    }
}
