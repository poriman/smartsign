﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Data;
using System.Globalization;
using System.Reflection;
using System.Windows.Media.Imaging;
using System.Windows.Media;
using System.Windows.Controls;

namespace DigitalSignage.Scheduler.Template
{
    class TextTypeConverter : IValueConverter
    {

        #region IValueConverter 멤버

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                string strType = (string)value;
                if (strType.Equals("T"))
                    return Visibility.Visible;
                else
                    return Visibility.Collapsed;
            }
            else
            {
                return Visibility.Visible;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }

        #endregion
    }

    class MovieTypeConverter : IValueConverter
    {

        #region IValueConverter 멤버

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                string strType = (string)value;
                if (strType.Equals("T"))
                    return Visibility.Collapsed;
                else
                    return Visibility.Visible;
            }
            else
            {
                return Visibility.Collapsed;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }

        #endregion
    }

    class ThumbnailConverter : IValueConverter
    {

        #region IValueConverter 멤버

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                String sExecutingPath = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                string appData = sExecutingPath + "\\Contents\\" + (string)value;

                return appData;
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }

        #endregion
    }

    class FileConverter : IValueConverter
    {

        #region IValueConverter 멤버

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {

            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }

        #endregion
    }

    class ImageConverter : IValueConverter
    {

        #region IValueConverter 멤버

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                bool isChange = (bool)value;
                try
                {
                    if (isChange == true)
                    {
                        BitmapImage bi = new BitmapImage();
                        bi.BeginInit();
                        bi.UriSource = new Uri("pack://application:,,,/../../images/create.gif");
                        bi.EndInit();

                        return bi;
                    }                    
                }
                catch
                { }
                return null;
            }
            else
            {
                return null;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }

        #endregion
    }

    class DisplayConverter : IValueConverter
    {

        #region IValueConverter 멤버

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                string str = (string)value;
                if (str.ToLower().Equals("width"))
                {
                    return Properties.Resources.mbhorizontal;
                }

                return Properties.Resources.mbvertical;
            }
            else
            {
                return Properties.Resources.mbvertical;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }

        #endregion
    }

    class SelectTemplateConverter : IValueConverter
    {

        #region IValueConverter 멤버

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                if (value != null)
                {
                    //System.Windows.Controls.Button button = value as System.Windows.Controls.Button;
                    Scheduler.Template.Template_T data = value as Scheduler.Template.Template_T;
                    return data.TEMPLATE.isused.Equals("true") ? true : false;
                }
            }
            catch
            {
            }
            return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return false;
        }

        #endregion
    }

    class ForegroundConverter : IValueConverter
    {

        #region IValueConverter 멤버

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                if (value != null)
                {
                    Scheduler.Template.Template_T data = value as Scheduler.Template.Template_T;
                    return data.TEMPLATE.isused.Equals("true") ? Brushes.White : Brushes.Black;
                }
            }
            catch
            {
            }
            return Brushes.Black;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Brushes.Black;
        }

        #endregion
    }

    class SelectitemConverter : IValueConverter
    {

        #region IValueConverter 멤버

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                if (value != null)
                {
                    ComboBoxItem item = value as ComboBoxItem;
                    return item.Content.ToString();
                }
            }
            catch
            {
            }
            return "";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return "";
        }

        #endregion
    }
}
