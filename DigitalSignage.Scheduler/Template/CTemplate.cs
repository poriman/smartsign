﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigitalSignage.Scheduler.Template
{
    public class template
    {
        private string idField;

        private string nameField;

        private string filenmField;

        private string exfilenmField;

        private string contentsidField;

        private string descField;

        private string countField;

        private string typeField;

        private string dbidField;

        private string formField;

        private string thumbnailField;

        private string isusedField;

        private string hederType;

        private string categoryField;

        private string logoPathField;

        private string weatherField;

        private string backImageField;

        private string buttimgField;

        private LoopData[] loopdataField;

        private promotion[] promotionsField;

        public string id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        public string name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        public string file_nm
        {
            get
            {
                return this.filenmField;
            }
            set
            {
                this.filenmField = value;
            }
        }

        public string contents_id
        {
            get
            {
                return this.contentsidField;
            }
            set
            {
                this.contentsidField = value;
            }
        }

        public string ex_file_nm
        {
            get
            {
                return this.exfilenmField;
            }
            set
            {
                this.exfilenmField = value;
            }
        }

        public string desc
        {
            get
            {
                return this.descField;
            }
            set
            {
                this.descField = value;
            }
        }

        public string count
        {
            get
            {
                return this.countField;
            }
            set
            {
                this.countField = value;
            }
        }

        public string type
        {
            get
            {
                return this.typeField;
            }
            set
            {
                this.typeField = value;
            }
        }

        public string dbid
        {
            get
            {
                return this.dbidField;
            }
            set
            {
                this.dbidField = value;
            }
        }

        public string HeaderType
        {
            set { hederType = value; }
            get { return hederType; }
        }

        public string form
        {
            get
            {
                return this.formField;
            }
            set
            {
                this.formField = value;
            }
        }

        public string thumbnail_nm
        {
            get
            {
                return this.thumbnailField;
            }
            set
            {
                this.thumbnailField = value;
            }
        }

        public string isused
        {
            get
            {
                return this.isusedField;
            }
            set
            {
                this.isusedField = value;
            }
        }

        public string category
        {
            get
            {
                return this.categoryField;
            }
            set
            {
                this.categoryField = value;
            }
        }

        public string logo
        {
            get
            {
                return this.logoPathField;
            }
            set
            {
                this.logoPathField = value;
            }
        }

        public string weather
        {
            get
            {
                return this.weatherField;
            }
            set
            {
                this.weatherField = value;
            }
        }

        public string backImage
        {
            get
            {
                return this.backImageField;
            }
            set
            {
                this.backImageField = value;
            }
        }

        public string buttimg
        {
            get
            {
                return this.buttimgField;
            }
            set
            {
                this.buttimgField = value;
            }
        }

        public LoopData[] LoopDatas
        {
            set { loopdataField = value; }
            get { return loopdataField; }
        }

        public promotion[] promotions
        {
            get
            {
                return this.promotionsField;
            }
            set
            {
                this.promotionsField = value;
            }
        }
    }
}
