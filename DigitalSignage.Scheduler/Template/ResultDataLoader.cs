﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Diagnostics;

namespace DigitalSignage.Scheduler.Template
{
    public class ResDataChartValue
    {
        private string Title;    // 챠트의 헤더 이름
        private string Values;    // 실제 결과 값

        public string TITLE
        {
            set { Title = value; }
            get { return Title; }
        }

        public string VALUE
        {
            set { Values = value; }
            get { return Values; }
        }
    }

    public class ResDataChartValueArry : ObservableCollection<ResDataChartValue>
    {
        public ResDataChartValueArry() : base() { }
    }

    public class ResultData
    {
        private string player_id;   // 플레이어 아이디
        private string data_id;     // 데이터 아이디
        private string template_id; // 템플릿 아이디
        private string name;        // 데이터 이름
        private string date;        // 설문 조사 일
        private string[] Titles;    // 챠트의 헤더 이름
        private string PlayerName;  // 플레이어 이름
        private string[] Values;    // 실제 결과 값

        public string Date
        {
            set { date = value; }
            get { return date; }
        }

        public string Name
        {
            set { name = value; }
            get { return name; }
        }

        public string PlayerID
        {
            set { player_id = value; }
            get { return player_id; }
        }

        public string DataID
        {
            set { data_id = value; }
            get { return data_id; }
        }

        public string TemplateID
        {
            set { template_id = value; }
            get { return template_id; }
        }

        public string[] VALUES
        {
            set { Values = value; }
            get { return Values; }
        }

        public string[] TITLE
        {
            set { Titles = value; }
            get { return Titles; }
        }

        public string PLAYERNAME
        {
            set { PlayerName = value; }
            get { return PlayerName; }
        }
    }

    public class ResultDataArry : ObservableCollection<ResultData>
    {
        public ResultDataArry() : base() { }
    }

    public class ResultDataLoader
    {
        private int[] total_values;
        public int[] TOTALVALUES
        {
            get { return total_values; }
        }

        public ResultDataArry LoadByTemplateid(long from_date, long end_date, string template_id, string[] players, List<string> playerNames)
        {
            ResultDataArry arry = new ResultDataArry();
            Config cfg = Config.GetConfig;
            bool isFirst = true;
            try
            {
                int index = 0; 
                foreach (string playerid in players)
                {
                    string playerName = "";
                    if (playerNames.Count > index)
                        playerName = playerNames[index];

                    using (ServerDatabase.ServerDataSet.resultdatasDataTable ret = cfg.ServerResultDataList.GetResultDatabyTemplateid(from_date,
                        end_date, playerid, template_id))
                    {
                        foreach(ServerDatabase.ServerDataSet.resultdatasRow row in ret.Rows)
                        {
                            ResultData data = new ResultData();
                            data.Date = row.Isapply_dateNull()?"0": row.apply_date.ToString();
                            data.Name = row.Isdata_descNull() ? "" : row.data_desc;
                            data.PlayerID = String.Format("{0} ({1})", playerName, playerid); //row.IsplayeridNull() ? "" : row.playerid;
                            data.DataID = row.IsdatafieldidNull() ? "" : row.datafieldid;
                            data.TemplateID = row.IstempalteidNull() ? "" : row.tempalteid;
                            data.VALUES = row.Isdata_valuesNull() ? null : row.data_values.Replace(" ", "").Split('|');
                            if (isFirst == true)
                            {
                                isFirst = false;
                                total_values = new int[data.VALUES.Length];
                                for(int i = 0; i < data.VALUES.Length; i++)
                                    total_values[i] = 0;
                            }
                            if (data.VALUES != null)
                            {
                                for(int i = 0; i < data.VALUES.Length; i++)
                                {
                                    if (data.VALUES[i] != null && data.VALUES[i].Length > 0)
                                        total_values[i] += int.Parse(data.VALUES[i]);
                                }
                            }

                            arry.Add(data);
                        }
                        ret.Dispose();
                    }

                    index++;
                }

                return arry;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message.ToString());
            }

            return null;
        }

        public ResultDataArry LoadByDataid(long from_date, long end_date, string datafield_id, string[] players, List<string> playerNames)
        {
            ResultDataArry arry = new ResultDataArry();
            Config cfg = Config.GetConfig;
            bool isFirst = true;

            try
            {
                int index = 0;                
                foreach (string playerid in players)
                {
                    string playerName = "";
                    if (playerNames.Count > index)
                        playerName = playerNames[index];

                    using (ServerDatabase.ServerDataSet.resultdatasDataTable ret = cfg.ServerResultDataList.GetResultDatabyDatafieldid(from_date,
                        end_date, playerid, datafield_id))
                    {
                        foreach (ServerDatabase.ServerDataSet.resultdatasRow row in ret.Rows)
                        {
                            ResultData data = new ResultData();
                            data.Date = row.Isapply_dateNull() ? "0" : row.apply_date.ToString();
                            data.Name = row.Isdata_descNull() ? "" : row.data_desc;
                            data.PlayerID = String.Format("{0} ({1})", playerName, playerid);// row.IsplayeridNull() ? "" : row.playerid;
                            data.DataID = row.IsdatafieldidNull() ? "" : row.datafieldid;
                            data.TemplateID = row.IstempalteidNull() ? "" : row.tempalteid;
                            data.VALUES = row.Isdata_valuesNull() ? null : row.data_values.Replace(" ", "").Split('|');
                            if (isFirst == true)
                            {
                                isFirst = false;
                                total_values = new int[data.VALUES.Length];
                                for (int i = 0; i < data.VALUES.Length; i++)
                                    total_values[i] = 0;
                            }
                            if (data.VALUES != null)
                            {
                                for (int i = 0; i < data.VALUES.Length; i++)
                                {
                                    if (data.VALUES[i] != null && data.VALUES[i].Length > 0)
                                        total_values[i] += int.Parse(data.VALUES[i]);
                                }
                            }
                            arry.Add(data);
                        }
                        ret.Dispose();
                    }
                    index++;
                }

                return arry;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message.ToString());
            }

            return null;
        }
    }
}
