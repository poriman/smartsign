﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Xml;

namespace DigitalSignage.Scheduler.Template.UserControl
{
    /// <summary>
    /// WeatherControl.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class WeatherControl : System.Windows.Controls.UserControl
    {
        private string[] area = {"속초","철원","동두천","문산","대관령","춘천","백령도","강릉","동해","서울","인천",
                                  "원주", "울릉도","수원","영월", "충주","서산","울진","청주","대전","추풍령","안동",
                                  "상주","포항","군산","대구","전주","울산","마산","광주","부산","통영","목포","여수",
                                  "흑산도","완도","진도","제주","고산","서귀포","진주","강화","양평","이천","인제",
                                  "홍천","태백","제천","보은","천안","보령","부여","금산","부안","임실","정읍","남원",
                                  "장수","순천","장흥","해남","고흥","성산포","봉화","명주","문경","영덕","의성","구미",
                                  "영천","거창","합천","밀양","산청","거제","남해"};

        private string[] area2 = {"강릉", "강화",  "거제", "거창", "고산", "고흥", "광주", "구미", "군산", "금산",   "남원", "남해", "대관령","대구",
                                  "대전", "동두천","동해", "마산", "명주", "목포", "문경", "문산", "밀양", "백령도", "보령", "보은", "봉화",  "부산",
                                  "부안","부여","산청","상주","서귀포","서산","서울","성산포","속초","수원","순천","안동","양평","여수",
                                  "영덕","영월","영천","완도","울릉도","울산","울진","원주","의성","이천","인제","인천","임실","장수",
                                  "장흥","전주","정읍","제주","제천","진도","진주","천안","철원","청주","추풍령","춘천","충주","태백","통영",
                                  "포항","합천","해남","홍천","흑산도"};

        private Template.Wizard.NewTemplateWizard mainWindow;
        public Template.Wizard.NewTemplateWizard MAINWINDOW
        {
            set { mainWindow = value; }
            get { return mainWindow; }
        }
        private Template.template currentTemplate;
        public Template.template CURRENTTEMPLATE
        {
            set { currentTemplate = value; }
            get { return currentTemplate; }
        }

        public string LogoFileName
        {
            set { txt_logoValue.Text = value; }
            get { return txt_logoValue.Text;}
        }

        public string BackimageName
        {
            set { txt_backimValue.Text = value; }
            get { return txt_backimValue.Text; }
        }

        public int WeaterCode
        {
            set { cb_weater.SelectedIndex = Setindex(value); }
            get { return GetOrgIndex(cb_weater.SelectedIndex); }
        }

        public WeatherControl()
        {
            InitializeComponent();
            SetComboBox();
        }

        private string LoadData(string code)
        {
            try
            {
                string path = MAINWINDOW.GetTemplateXmlFolder() + "templatelists.xml";
                if (File.Exists(path))
                {
                    XmlDocument _doc = new XmlDocument();
                    _doc.Load(path);
                    XmlNode node = _doc.SelectSingleNode("//template_list");
                    return node.Attributes[code].Value;
                }
            }
            catch
            {
            }
            return "";
        }

        public void SetValue()
        {
            if (CURRENTTEMPLATE.logo != null && CURRENTTEMPLATE.logo.Equals("use"))
            {
                if (LogoFileName == "")
                    LogoFileName = LoadData("logo");
                layout1.Visibility = Visibility.Visible;
            }
            else
            {
                layout1.Visibility = Visibility.Collapsed;
            }

            if (CURRENTTEMPLATE.weather != null && CURRENTTEMPLATE.weather.Equals("use"))
            {//"areaCode"
                if (WeaterCode == 9 || WeaterCode == -1)
                {
                    try
                    {
                        WeaterCode = int.Parse(LoadData("areaCode"));
                    }
                    catch
                    {
                        WeaterCode = 9;
                    }
                }
                layout2.Visibility = Visibility.Visible;
            }
            else
            {
                layout2.Visibility = Visibility.Collapsed;
            }

            if (CURRENTTEMPLATE.backImage != null && CURRENTTEMPLATE.backImage.Equals("use"))
            {
                layout3.Visibility = Visibility.Visible;
            }
            else
            {
                layout3.Visibility = Visibility.Collapsed;
            }
        }

        private void SetComboBox()
        {
            try
            {
                foreach (string str in area2)
                {
                    ComboBoxItem item = new ComboBoxItem();
                    item.Content = str;
                    item.DataContext = str;
                    cb_weater.Items.Add(item);
                }
            }
            catch { }

            cb_weater.SelectedIndex = 9;
        }

        private int Setindex(int index)
        {
            try
            {
                if (index >= 0 || index < 76)
                {
                    string strArea = area[index];
                    for (int i = 0; i < area2.Length; i++)
                    {
                        if (strArea == area2[i])
                        {
                            return i;
                        }
                    }
                }
            }
            catch
            {
            }

            return 34;
        }

        private int GetOrgIndex(int index)
        {
            try
            {
                if (index >= 0 || index < 76)
                {
                    string strArea = area2[index];
                    for (int i = 0; i < area.Length; i++)
                    {
                        if (strArea == area[i])
                        {
                            return i;
                        }
                    }
                }
            }
            catch
            {
            }
            return 34;
        }

        private void btn_FileOpen_Click(object sender, RoutedEventArgs e)
        {
            string filter = "";
            string title = Properties.Resources.titlelogo;
            //"txt files (*.txt)|*.txt|All files (*.*)|*.*"

            string images = "*.PNG;*.JPG;*.JPEG;";
            string flash = "*.swf";
            filter = string.Format("Image ({0})|{0}|Flash({1})|{1}",images, flash);


            System.Windows.Forms.OpenFileDialog fileDlg = new System.Windows.Forms.OpenFileDialog();
            fileDlg.Title = title;
            fileDlg.Filter = filter;
            if (fileDlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                DirectoryInfo info = new DirectoryInfo(fileDlg.FileName);
                txt_logoValue.Text = info.Name;
                try
                {
                    File.Copy(info.FullName, MAINWINDOW.GetContentsFolder() + info.Name, true);
                }
                catch { }
            }
        }

        private void cb_weater_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void btn_backimFileOpen_Click(object sender, RoutedEventArgs e)
        {
            string filter = "";
            string title = Properties.Resources.titleBackground;
            string images = "*.PNG;*.JPG;*.JPEG;";
            string flash = "*.swf";
            filter = string.Format("Image ({0})|{0}|Flash({1})|{1}", images, flash);


            System.Windows.Forms.OpenFileDialog fileDlg = new System.Windows.Forms.OpenFileDialog();
            fileDlg.Title = title;
            fileDlg.Filter = filter;
            if (fileDlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                DirectoryInfo info = new DirectoryInfo(fileDlg.FileName);
                txt_backimValue.Text = info.Name;
                try
                {
                    File.Copy(info.FullName, MAINWINDOW.GetContentsFolder() + info.Name, true);
                }
                catch { }
            }
        }

    }
}
