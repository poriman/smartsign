﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Threading;
using System.Windows.Threading;
using DigitalSignage.Common;
using System.IO;
using System.Diagnostics;

namespace DigitalSignage.Scheduler.Template.UserControl
{
    /// <summary>
    /// DataStatusDlg.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class DataStatusDlg : Window
    {
        public enum DeleteStatus
        {
            Data_info = 0,
            Data_field,
            Data_value
        }

        private Thread m_thProcess = null;
        private DeleteStatus _status;
        public DeleteStatus STATUS
        {
            set { _status = value; }
            get { return _status; }
        }
        String[] m_arrFTPInfo;
        Config m_cfg;
        Scheduler.Template.DataFieldArry m_task;
        string m_sExecutingPath;
        string m_userid;
        string m_contentsPaht;
        string m_datainfoId;
        public DataStatusDlg(String[] arrFTPInfo, Config cfg, Scheduler.Template.DataFieldArry task,string userid, string sExecutingPath)
        {
            InitializeComponent();
            m_cfg = cfg;
            m_arrFTPInfo = arrFTPInfo;
            m_task = task;
            m_userid = userid;
            m_sExecutingPath = sExecutingPath;
            STATUS = DeleteStatus.Data_field;
        }

        public DataStatusDlg(String[] arrFTPInfo, Config cfg, Scheduler.Template.DataFieldArry task, string userid, string infoid, string contentsPath)
        {
            InitializeComponent();
            m_cfg = cfg;
            m_arrFTPInfo = arrFTPInfo;
            m_task = task;
            m_userid = userid;
            m_contentsPaht = contentsPath;
            m_datainfoId = infoid;
            STATUS = DeleteStatus.Data_info;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            switch (STATUS)
            {
                case DeleteStatus.Data_info:
                    m_thProcess = new Thread(this.DataInfoDelete);
                    break;
                case DeleteStatus.Data_field:
                    m_thProcess = new Thread(this.DataFieldDelete);
                    break;
            }

            m_thProcess.Start();
        }

        

        private void Window_Unloaded(object sender, RoutedEventArgs e)
        {
            try
            {
                if (m_thProcess != null && m_thProcess.IsAlive)
                    m_thProcess.Abort();
            }
            catch
            {
            }
        }
        private void DataInfoDelete()
        {
            try
            {
                FTPFunctions ftp = new FTPFunctions(Config.GetConfig.AnalyzeSource(m_arrFTPInfo[0]), m_arrFTPInfo[3], m_arrFTPInfo[1], m_arrFTPInfo[2], m_cfg.FTP_Timeout);

                bool hasField = false;
                int tcount = 0;
                if (m_task.Count <= 1)
                {
                    StepIt(100);
                    tcount = 100;
                }
                else
                    tcount = (int)(100.0 / m_task.Count);

                foreach (Scheduler.Template.DataField_T _field in m_task)
                {
                    if (_field.ID != null && _field.ID.Length == 18)
                        hasField = true;
                    foreach (Scheduler.Template.Model cont in _field.CONTENTS)
                    {
                        if (cont != null && cont.CONTENTS != null && cont.CONTENTS.TYPE.Equals("T") == false && cont.CONTENTS.NEW_NAME.Length > 0)
                        {
                            string contents_path = m_contentsPaht + cont.CONTENTS.NEW_NAME;
                            UpdateLabel("Delete File :" + cont.CONTENTS.VALUE);

                            if (File.Exists(contents_path))
                                File.Delete(contents_path);
                            if (ftp.HasFile(m_userid, cont.CONTENTS.NEW_NAME))
                            {
                                string filename = FTPFunctions.EncodeTo64M(cont.CONTENTS.NEW_NAME);
                                ftp.FTPDeleteFile(m_userid + "\\" + filename);
                                filename = filename + ".md5";
                                ftp.FTPDeleteFile(m_userid + "\\" + filename);
                            }
                        }
                    }

                    StepIt(tcount);
                }

                UpdateLabel("Delete Contents DB:");
                if (hasField && false == m_cfg.ServerContentsList.DeletebyDataInfoid(m_datainfoId))
                {
                    MessageBox.Show("Delete Failed.");
                    throw new Exception("Delete Contents error");
                }

                UpdateLabel("Delete Data Field DB:");
                if (hasField && false == m_cfg.ServerDataFieldList.DelDataFieldByDatainfoid(m_datainfoId))
                {
                    MessageBox.Show("Delete Failed.");
                    throw new Exception("Delete field error");
                }

                UpdateLabel("Delete Data Info DB:");
                if (false == m_cfg.ServerDataInfoList.DelDataInfo(m_datainfoId))
                {
                    MessageBox.Show("Delete Failed.");
                    throw new Exception("Delete Info error");
                }

                Finish(true);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message.ToString());
            }
            Finish(false);
        }

        private void DataFieldDelete()
        {
            try
            {
                FTPFunctions ftp = new FTPFunctions(Config.GetConfig.AnalyzeSource(m_arrFTPInfo[0]), m_arrFTPInfo[3], m_arrFTPInfo[1], m_arrFTPInfo[2], m_cfg.FTP_Timeout);

                Scheduler.Template.DataFieldArry task = m_task;
                int count = 0, tcount = 0; ;
                foreach (Scheduler.Template.DataField_T field in task)
                {
                    if (field.ISSELECT == true)
                        count++;
                }
                if (count == 1)
                {
                    StepIt(100);
                    tcount = 100;
                }
                else
                    tcount = (int)(100.0 / count);

                foreach (Scheduler.Template.DataField_T _field in task)
                {
                    bool hasField = false;

                    if (_field.ISSELECT == true)
                    {
                        if (_field.ID != null && _field.ID.Length == 18)
                            hasField = true;

                        foreach (Scheduler.Template.Model cont in _field.CONTENTS)
                        {
                            if (cont != null && cont.CONTENTS != null && cont.CONTENTS.TYPE.Equals("T") == false)
                            {
                                string contents_path = m_sExecutingPath + "\\Contents\\" + cont.CONTENTS.NEW_NAME;
                                UpdateLabel("File Delete :" + cont.CONTENTS.VALUE);
                                if (File.Exists(contents_path))
                                    File.Delete(contents_path);

                                if (hasField && ftp.HasFile(m_userid, cont.CONTENTS.NEW_NAME))
                                {
                                    string filename = FTPFunctions.EncodeTo64M(cont.CONTENTS.NEW_NAME);
                                    ftp.FTPDeleteFile(m_userid + "/" + filename);
                                    filename = filename + ".md5";
                                    ftp.FTPDeleteFile(m_userid + "/" + filename);
                                }
                            }
                        }

                        UpdateLabel("Delete Contetns DB.....");
                        if (hasField && false == m_cfg.ServerContentsList.DeletebyDataFieldid(_field.ID))
                        {
                            MessageBox.Show("Delete Failed.");
                            throw new Exception("Delete contents error");
                        }

                        UpdateLabel("Delete Data Field DB.....");
                        if (hasField && false == m_cfg.ServerDataFieldList.DelDataFieldByid(_field.ID))
                        {
                            MessageBox.Show("Delete Failed.");
                            throw new Exception("Delete field error");
                        }

                        StepIt(tcount);
                    }
                }

                Finish(true);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message.ToString());
            }

            Finish(false);
        }


        delegate void finishCB(bool bExitCode);
        public void Finish(bool bExitCode)
        {
            try
            {
                if (!this.Dispatcher.CheckAccess())
                {
                    this.Dispatcher.BeginInvoke(DispatcherPriority.Send,
                       new finishCB(Finish), bExitCode);
                }
                else
                {
                    this.DialogResult = bExitCode;
                    Close();
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message.ToString());
            }
        }

        delegate void StepItCB(int value);
        void StepIt(int value)
        {
            try
            {
                if (!this.Dispatcher.CheckAccess())
                {
                    this.Dispatcher.BeginInvoke(DispatcherPriority.Send,
                       new StepItCB(StepIt), value);
                }
                else
                {
                    progressBarCopyProgress.Value += value;
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                Debug.WriteLine(e.Message.ToString());
            }
        }

        delegate void UpdateLabelCB(string statusLabel);
        void UpdateLabel(string statusLabel)
        {
            try
            {
                if (!this.Dispatcher.CheckAccess())
                {
                    this.Dispatcher.BeginInvoke(DispatcherPriority.Send,
                       new UpdateLabelCB(UpdateLabel), statusLabel);
                }
                else
                {
                    lbStatusContent.Content = statusLabel;
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                Debug.WriteLine(e.Message.ToString());
            }
        }
    }
}
