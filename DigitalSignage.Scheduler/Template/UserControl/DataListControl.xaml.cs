﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Collections.ObjectModel;
using System.Xml;

namespace DigitalSignage.Scheduler.Template.UserControl
{
    /// <summary>
    /// PreviewControl.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class DataListControl : System.Windows.Controls.UserControl
    {
        Scheduler.Template.Wizard.NewTemplateWizard maindlg = null;
        Scheduler.Template.template currentTemplate = null;
        ListViewDragDropManager<Scheduler.Template.DataField_T> dragMgr;
        private string day_id;
        public string DAY_ID
        {
            set { day_id = value; }
            get { return day_id; }
        }

        public Scheduler.Template.DataFieldArry DataAArry
        {
            set
            {
                Scheduler.Template.DataFieldArry Arry = value;
                if (Arry != null && Arry.Count > 0)
                    DataListView.ItemsSource = Arry;
            }
            get
            {
                ObservableCollection<Scheduler.Template.DataField_T> itemarry = DataListView.ItemsSource as ObservableCollection<Scheduler.Template.DataField_T>;
                DigitalSignage.Scheduler.Template.DataFieldArry arry = new DigitalSignage.Scheduler.Template.DataFieldArry();
                foreach (Scheduler.Template.DataField_T data in itemarry)
                {
                    if (data != null && data.ID != null && data.ID.Length > 0)
                        arry.Add(data);
                }
                return arry;
            }
        }

        public DataListControl(Scheduler.Template.Wizard.NewTemplateWizard dlg, Scheduler.Template.template _template, string dayid)
        {
            InitializeComponent();
            DataListView.ItemsSource = new ObservableCollection<Scheduler.Template.DataField_T>();
            this.dragMgr = new ListViewDragDropManager<Scheduler.Template.DataField_T>(DataListView);
            this.dragMgr.ListView = DataListView;
            this.dragMgr.ShowDragAdorner = true;

            DataListView.DragEnter  += OnListViewDragEnter;
            DataListView.Drop       += OnListViewDrop;
            DAY_ID = dayid;
            maindlg = dlg;
            currentTemplate = _template;
            SetColum();
        }

        public List<string> LoadExFileData(string keyvalue)
        {
            string path = maindlg.m_media_folder + currentTemplate.ex_file_nm;
            if (File.Exists(path) == false)
                return null;

            try
            {
                List<string> selectlist = new List<string>();
                XmlDocument _doc = new XmlDocument();
                _doc.Load(path);
                XmlNode xNode = _doc.DocumentElement;
                XmlNodeList _menuList = xNode.ChildNodes; ;
                for (int i = 0; i < _menuList.Count; i++)
                {
                    if (_menuList[i].Attributes["desc"].Value.Equals(keyvalue))
                    {
                        XmlNodeList list = _menuList[i].ChildNodes;
                        for (int t = 0; t < list.Count; t++)
                        {
                            XmlNode node = list.Item(t);
                            string value = node.Attributes["day_id"].Value.ToString();

                            if (value.Equals(DAY_ID))
                            {
                                XmlNodeList dataList = node.ChildNodes; ;
                                for (int j = 0; j < dataList.Count; j++)
                                {
                                    XmlNode node2 = dataList.Item(j);
                                    string value2 = node2.Attributes["menu_id"].Value.ToString();
                                    if (value2.Length > 0)
                                        selectlist.Add(value2);
                                }
                            }
                        }
                    }
                }

                return selectlist;
            }
            catch (ArgumentException aex)
            {
                string errmsg = aex.Message.ToString();
            }
            catch (InvalidOperationException iex)
            {
                string errmsg = iex.Message.ToString();
            }
            catch (XmlException xmlEx)
            {
                string errmsg = xmlEx.Message.ToString();
            }
            catch (Exception ex)
            {
                string errmsg = ex.Message.ToString();
            }

            return null;
        }

        void OnListViewDragEnter(object sender, DragEventArgs e)
        {
            e.Effects = DragDropEffects.Move;
        }

        void OnListViewDrop(object sender, DragEventArgs e)
        {
            if (e.Effects == DragDropEffects.None)
                return;
        }

        private void SetColum()
        {
            try
            {
                //this.datalistview.Content = listData;

                GridView grvlist = new GridView();
                DataListView.View = grvlist;
                GridViewColumn col = null;
                DataTemplate template = null;
                FrameworkElementFactory elusercontrol = null;
                Binding bind = null;

                col = new GridViewColumn();
                col.Header = Properties.Resources.btnSelect;
                col.Width = 60;
                template = new DataTemplate();
                col.CellTemplate = template;
                elusercontrol = new FrameworkElementFactory(typeof(CheckBox));
                template.VisualTree = elusercontrol;
                bind = new Binding("ISSELECT");
                elusercontrol.SetBinding(CheckBox.IsCheckedProperty, bind);
                grvlist.Columns.Add(col);


                int index = currentTemplate.LoopDatas.Length;
                for (int i = 0; i < index; i++)
                {
                    string title = string.Format("CONTENTS[{0}].CONTENTS.VALUE", i);
                    col = new GridViewColumn();
                    col.Header = currentTemplate.LoopDatas[i].Name;
                    col.Width = 150;
                    template = new DataTemplate();

                    col.CellTemplate = template;

                    elusercontrol = new FrameworkElementFactory(typeof(TextBlock));
                    template.VisualTree = elusercontrol;
                    bind = new Binding(title);
                    elusercontrol.SetBinding(TextBlock.TextProperty, bind);

                    grvlist.Columns.Add(col);
                }

                SetData();
            }
            catch { }
        }

        private void SetData()
        {
            try
            {
                if (maindlg.isNew == true)
                    return;

                List<string> list = LoadExFileData("newmenu_touch");
                DigitalSignage.Scheduler.Template.DataFieldArry arry = new DigitalSignage.Scheduler.Template.DataFieldArry();

                Scheduler.Template.DataLoader loader = new DigitalSignage.Scheduler.Template.DataLoader();
                foreach (string datafieldid in maindlg.GetDataFieldList())
                {
                    DigitalSignage.Scheduler.Template.DataFieldArry _arry = loader.Load(datafieldid);
                    if (_arry != null)
                    {
                        foreach (Scheduler.Template.DataField_T data in _arry)
                        {
                            arry.Add(data);
                        }
                    }

                }

                ObservableCollection<Scheduler.Template.DataField_T> itemarry = new ObservableCollection<Scheduler.Template.DataField_T>();
                foreach (string id in list)
                {
                    foreach (Scheduler.Template.DataField_T data in arry)
                    {
                        if (data.ID.Equals(id))
                        {
                            itemarry.Add(data);
                            break;
                        }
                    }
                }

                DataListView.ItemsSource = itemarry;
            }
            catch
            {
            }


        }

        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //DigitalSignage.Scheduler.Template.DataFieldArry itemarry = DataListView.ItemsSource as DigitalSignage.Scheduler.Template.DataFieldArry;
                //DigitalSignage.Scheduler.Template.DataFieldArry arry = new DigitalSignage.Scheduler.Template.DataFieldArry();

                ObservableCollection<Scheduler.Template.DataField_T> itemarry = DataListView.ItemsSource as ObservableCollection<Scheduler.Template.DataField_T>;
                ObservableCollection<Scheduler.Template.DataField_T> arry = new ObservableCollection<Scheduler.Template.DataField_T>();

                foreach (Scheduler.Template.DataField_T data in itemarry)
                {
                    if (data.ISSELECT == false)
                    {
                        arry.Add(data);
                    }
                }

                DataListView.ItemsSource = arry;
            }
            catch (Exception ex)
            {
                string msgerr = ex.Message.ToString();
            }
        }
    }
}
