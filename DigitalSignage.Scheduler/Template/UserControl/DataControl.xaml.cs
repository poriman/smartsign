﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.ComponentModel;
using System.Diagnostics;
namespace DigitalSignage.Scheduler.Template.UserControl
{
    /// <summary>
    /// DataControl.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class DataControl : System.Windows.Controls.UserControl, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        string str_CheckValue = "";
        public void OnPropertyChanged(String propertyName)
        {
            if (PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));

        }

        public static readonly DependencyProperty DataListViewProperty = DependencyProperty.Register("DataListView", typeof(TemplateListviewControl), typeof(DataControl), new PropertyMetadata(DataListViewPropertyValueChanged));
        public TemplateListviewControl DataListView
        {
            get
            {
                return (TemplateListviewControl)GetValue(DataListViewProperty);
            }

            set
            {
                SetValue(DataListViewProperty, value);
            }
        }

        private static void DataListViewPropertyValueChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            DataControl dc = (DataControl)d;
            dc.DataListView = (TemplateListviewControl)e.NewValue;
        }

        public DataControl()
        {
            InitializeComponent();
        }

        private void Edit_TextBlock_Click(object sender, RoutedEventArgs args)
        {
            try
            {
                ContentControl targetContentCtrl = (ContentControl)sender;
                string changeTemplateTo = "editTextTemplate";
                DigitalSignage.Scheduler.Template.Model _mode = DataContext as DigitalSignage.Scheduler.Template.Model;
                str_CheckValue = (_mode.CONTENTS.VALUE == null) ? "" : _mode.CONTENTS.VALUE;
                targetContentCtrl.Template = (ControlTemplate)targetContentCtrl.Style.Resources[changeTemplateTo];
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message.ToString());
            }
        }

        private void Edit_TextBox_LostFocus(object sender, RoutedEventArgs args)
        {
            try
            {
                ContentControl targetContentCtrl = (ContentControl)sender;
                string changeTemplateTo = "defaultTemplate";

                targetContentCtrl.Template = (ControlTemplate)targetContentCtrl.Style.Resources[changeTemplateTo];
                DigitalSignage.Scheduler.Template.Model _mode = DataContext as DigitalSignage.Scheduler.Template.Model;
                if (false == str_CheckValue.Equals(_mode.CONTENTS.VALUE))
                {
                    _mode.CONTENTS.ISCHANGE = true;
                    SetSelectBox(_mode.CONTENTS.DATAFIELD_ID);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message.ToString());
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DigitalSignage.Scheduler.Template.Model _mode = DataContext as DigitalSignage.Scheduler.Template.Model;

            string value = _mode.CONTENTS.TYPE;
            string filter = "";
            string title = "";
            switch (_mode.CONTENTS.TYPE)
            {
                case "T":
                    return;
                case "F":
                    title = "Open Flash";
                    filter = string.Format("Flash (*.swf;)|*.swf;");
                    break;
                case "M":
                    title = "Open Video";
                    filter = string.Format("Video (*.flv;)|*.flv;");
                    break;
                case "I":
                    title = "Open Image";
                    string images = "*.PNG;*.TIF;*.WMF;*.JPG;*.GIF;*.JPEG;*.EMF";
                    filter = string.Format("Image ({0})|{0}", images);
                    break;
            }

            System.Windows.Forms.OpenFileDialog fileDlg = new System.Windows.Forms.OpenFileDialog();
            fileDlg.Title = title;
            fileDlg.Filter = filter;
            if (fileDlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                DirectoryInfo info = new DirectoryInfo(fileDlg.FileName);

                _mode.CONTENTS.VALUE = info.Name;
                txt_value.Text = info.Name;
                _mode.CONTENTS.PATH = info.FullName;
                _mode.CONTENTS.ISCHANGE = true;

                SetSelectBox(_mode.CONTENTS.DATAFIELD_ID);
            }
        }

        private void SetSelectBox(string id)
        {
            DataListView.OnDataValueChange(id);
        }

        private void UserControl_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            try
            {
                contents_main.Width = _viewbox.ActualWidth;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("DataControl : UserControl_SizeChanged = "+ ex.Message.ToString());
            }
        }
        
    }
}
