﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DigitalSignage.Common;
using System.Xml;
using System.Reflection;
using System.IO;
using System.Threading;
using System.Diagnostics;
namespace DigitalSignage.Scheduler.Template.UserControl
{
    /// <summary>
    /// TemplateListviewControl.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class TemplateListviewControl : System.Windows.Controls.UserControl
    {
        private Template.template currentTemplate;
        Scheduler.Template.DataField_T currentDatafield;
        Scheduler.Template.DataInfo current_datainfo;
        private List<string> promotion_type;
        private List<string> m_DataIdList;
        ListViewDragDropManager<Scheduler.Template.DataField_T> dragMgr;
        private Scheduler.Template.Wizard.PageCommand _pageCommand;
        System.Windows.Forms.Timer cont_tick = new System.Windows.Forms.Timer();

        Config m_cfg;
        public Scheduler.Template.DataInfo CURRENT_DATAINFO
        {
            set { current_datainfo = value; }
            get { return current_datainfo; }
        }

        public List<string> PROMOTIONSTYPE
        {
            set { promotion_type = value; }
            get 
            {
                if (promotion_type == null)
                    promotion_type = new List<string>();

                return promotion_type; 
            }
        }

        public List<string> DataIdList
        {
            set 
            { 
                m_DataIdList = value; 
            }
            get { return m_DataIdList; }
        }

        public Template.template CurrentTemplate
        {
            set { currentTemplate = value; }
            get { return currentTemplate; }
        }

        public Scheduler.Template.DataField_T CURRENT_DATAFIELD
        {
            set { currentDatafield = value; }
            get { return currentDatafield; }
        }

        public TemplateListviewControl(Scheduler.Template.Wizard.PageCommand pcd)
        {
            InitializeComponent();
            m_cfg = Config.GetConfig;
            _pageCommand = pcd;

            if (pcd.CURRENTTEMPLATE.type.ToLower().Equals("w"))
            {
                this.dragMgr = new ListViewDragDropManager<Scheduler.Template.DataField_T>(listData);
                this.dragMgr.ListView = listData;
                this.dragMgr.ShowDragAdorner = true;
            }

            cont_tick.Tick += new EventHandler(cont_tick_Tick);
            cont_tick.Interval = 1000;
            cont_tick.Start();
        }

        //void listData_DragOver(object sender, DragEventArgs e)
        //{
        //    //ListView listview = sender as ListView;
        //    //Scheduler.Template.DataField_T data = listview.SelectedItem as Scheduler.Template.DataField_T;
        //    //if (data == null || data.ID == null || data.ID.Length == 0)
        //    {
        //        e.Effects = DragDropEffects.None;
        //    }  
        //    return;
        //}

        void cont_tick_Tick(object sender, EventArgs e)
        {
            btn_displaycount.Content = _pageCommand.itemCount;
        }

        public Scheduler.Template.DataFieldArry TabArry
        {
            get { return listData.ItemsSource as Scheduler.Template.DataFieldArry; }
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {

        }

        public bool MakeListViewColumn()
        {
            try
            {                
                //this.datalistview.Content = listData;
                
                GridView grvlist = new GridView();
                listData.View = grvlist;
                GridViewColumn col = null;
                DataTemplate template = null;
                FrameworkElementFactory elusercontrol = null;
                Binding bind = null;

                col = new GridViewColumn();
                col.Header = Properties.Resources.btnSelect;
                col.Width = 60;
                template = new DataTemplate();
                col.CellTemplate = template;                
                elusercontrol = new FrameworkElementFactory(typeof(CheckBox));
                template.VisualTree = elusercontrol;
                bind = new Binding("ISSELECT");
                elusercontrol.SetBinding(CheckBox.IsCheckedProperty, bind);

                elusercontrol = new FrameworkElementFactory(typeof(Image));
                template.VisualTree.AppendChild(elusercontrol);
                bind = new Binding("ISCHANGE");
                DigitalSignage.Scheduler.Template.ImageConverter converter = new DigitalSignage.Scheduler.Template.ImageConverter();
                bind.Converter = converter;
                elusercontrol.SetBinding(Image.SourceProperty, bind);
                bind = new Binding();
                bind.Source = 16;
                elusercontrol.SetBinding(Image.WidthProperty, bind);
                bind = new Binding();
                bind.Source = 16;
                elusercontrol.SetBinding(Image.HeightProperty, bind);

                grvlist.Columns.Add(col);


                int index = CurrentTemplate.LoopDatas.Length;
                PROMOTIONSTYPE.Clear();
                for (int i = 0; i < index; i++)
                {
                    PROMOTIONSTYPE.Add(CurrentTemplate.LoopDatas[i].Type);

                    string title = string.Format("CONTENTS[{0}]", i);
                    col = new GridViewColumn();
                    col.Header = CurrentTemplate.LoopDatas[i].Name;
                    col.Width = 150;
                    template = new DataTemplate();
                    
                    col.CellTemplate = template;
                    
                    elusercontrol = new FrameworkElementFactory(typeof(DataControl));
                    template.VisualTree = elusercontrol;
                    bind = new Binding(title);
                    elusercontrol.SetBinding(DataControl.DataContextProperty, bind);
                    bind = new Binding();
                    bind.Source = this;
                    elusercontrol.SetBinding(DataControl.DataListViewProperty, bind);
                    bind = new Binding();  

                    grvlist.Columns.Add(col);
                }

                ReFresh();

                return true;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message.ToString());
            }

            return false;
        }

        void TemplateListviewControl_Checked(object sender, RoutedEventArgs e)
        {
            //throw new NotImplementedException();
        }

        public void ReFresh()
        {
            Scheduler.Template.DataLoader loader = new DigitalSignage.Scheduler.Template.DataLoader();
            DigitalSignage.Scheduler.Template.DataFieldArry arry = loader.Load(CURRENT_DATAINFO.ID);
            if (arry != null)
            {
                if (DataIdList == null || DataIdList.Count == 0)
                {
                    listData.ItemsSource = arry;
                }
                else if (DataIdList != null || DataIdList.Count > 0)
                {
                    Scheduler.Template.DataFieldArry tarry = new DigitalSignage.Scheduler.Template.DataFieldArry();

                    foreach (Scheduler.Template.DataField_T field in arry)
                    {
                        foreach (string id in DataIdList)
                        {
                            if (field.ID == id)
                            {
                                field.ISSELECT = true;
                            }                            
                        }                        
                        tarry.Add(field);
                    }

                    listData.ItemsSource = tarry;
                }
            }

            Add_Click(null, null);
        }

        private void Add_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Scheduler.Template.DataField_T Field = new DigitalSignage.Scheduler.Template.DataField_T();

                Field.ISSELECT = false;
                Field.ISCHANGE = false;
                Field.DATAINFO_ID = CURRENT_DATAINFO.ID;
                Field.ID = DateTime.Now.ToString("HHmmssffff");
                Scheduler.Template.Model[] conts = new DigitalSignage.Scheduler.Template.Model[PROMOTIONSTYPE.Count];

                for (int i = 0; i < PROMOTIONSTYPE.Count; i++ )
                {
                    Scheduler.Template.Model _mode = new Scheduler.Template.Model();
                    Scheduler.Template.Contents cont = new DigitalSignage.Scheduler.Template.Contents();
                    cont.ISCHANGE   = false;
                    cont.ID = "";
                    cont.DATAFIELD_ID = Field.ID;
                    cont.VALUE = "";
                    cont.NEW_NAME = "";
                    cont.TYPE = PROMOTIONSTYPE[i];
                    cont.PATH = "";
                    cont.DESC = "";
                    _mode.CONTENTS = cont;
                    conts[i] = _mode;
                }

                Field.CONTENTS = conts;

                Scheduler.Template.DataFieldArry  task;
                if (listData.Items.Count > 0)
                {
                    task = listData.ItemsSource as Scheduler.Template.DataFieldArry;
                }
                else
                {
                    task = new Scheduler.Template.DataFieldArry();
                }
                task.Add(Field);

                listData.ItemsSource = task;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message.ToString());
            }
        }

        public void Save_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (IsSelectCont() == 0)
                    return;

                if (false == DataValueCheck())
                {
                    System.Windows.MessageBox.Show(Properties.Resources.mberrdatainput,
                                                   Properties.Resources.titleMessageBox,
                                                   MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }

                Scheduler.Template.DataFieldArry task = listData.ItemsSource as Scheduler.Template.DataFieldArry;
                
                foreach (Scheduler.Template.DataField_T _field in task)
                {
                    if (_field.ISSELECT == true)
                    {
                        bool isNew = false;
                        if (_field.ID == null || _field.ID.Length != 18)
                        {
                            _field.ID = DateTime.Now.ToString("yyyyMMddHHmmssffff");
                            _field.TEMPLATEID = CURRENT_DATAINFO.TEMPLATE_ID;
                            isNew = true;
                        }
                        if (false == UploadContens(_field, m_cfg, isNew))
                        {
                            System.Windows.MessageBox.Show(Properties.Resources.mbErrorFTPUpload, 
                                                           Properties.Resources.titleMessageBox, 
                                                           MessageBoxButton.OK, MessageBoxImage.Error);
                            return;
                        }

                        ReFresh();
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message.ToString());
            }
        }

        private bool DataValueCheck()
        {
            try
            {
                Scheduler.Template.DataFieldArry task = listData.ItemsSource as Scheduler.Template.DataFieldArry;

                foreach (Scheduler.Template.DataField_T _field in task)
                {
                    if (_field.ISSELECT == true)
                    {
                        foreach (Scheduler.Template.Model cont in _field.CONTENTS)
                        {
                            if (cont == null || cont.CONTENTS == null)
                                continue;
                            if (cont.CONTENTS.VALUE == null || cont.CONTENTS.VALUE.Length == 0)
                            {
                                return false;
                            }
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message.ToString());
            }
            return false;
        }

        private bool UploadContens(Scheduler.Template.DataField_T field, Config cfg, bool isNew)
        {
            try
            {
                String sExecutingPath = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                string tempDir = sExecutingPath + "\\temp\\" + current_datainfo.USER_ID + "\\";
                if (!Directory.Exists(tempDir))
                    Directory.CreateDirectory(tempDir);

                foreach (Scheduler.Template.Model cont in field.CONTENTS)
                {
                    if (cont == null || cont.CONTENTS == null)
                        continue;

                    cont.CONTENTS.DESC = "";
                    if (cont.CONTENTS.ID == null || cont.CONTENTS.ID.Length != 18)
                    {
                        cont.CONTENTS.ID = DateTime.Now.ToString("yyyyMMddHHmmssffff");
                        cont.CONTENTS.DATAFIELD_ID = current_datainfo.ID;
                        cont.CONTENTS.DESC = "NEW";
                        Thread.Sleep(143);
                    }

                    if (cont.CONTENTS.TYPE.Equals("T") == false && cont.CONTENTS.ISCHANGE == true)
                    {
                        string contents_path = "";

                        if (cont.CONTENTS.NEW_NAME.Length > 0)
                            contents_path = sExecutingPath + "\\Contents\\" + cont.CONTENTS.NEW_NAME;

                        if (cont.CONTENTS.DESC.Equals("NEW") == false && contents_path != "" && File.Exists(contents_path))
                                File.Delete(contents_path);

                        if (File.Exists(cont.CONTENTS.PATH))
                        {
                            DirectoryInfo info = new DirectoryInfo(cont.CONTENTS.PATH);
                            cont.CONTENTS.NEW_NAME = cont.CONTENTS.ID + info.Extension;

                            string path = tempDir + cont.CONTENTS.NEW_NAME;
                            File.Copy(cont.CONTENTS.PATH, path, true);

                            if (contents_path == "")
                                contents_path = sExecutingPath + "\\Contents\\" + cont.CONTENTS.NEW_NAME;
                            
                            File.Copy(cont.CONTENTS.PATH, contents_path, true);
                        }
                    }

                    if (cont.CONTENTS.VALUE == null || cont.CONTENTS.VALUE.Length == 0)
                    {//mbTemplateData
                        System.Windows.MessageBox.Show(Properties.Resources.mbTemplateData,
                                                       Properties.Resources.titleMessageBox,
                                                       MessageBoxButton.OK, MessageBoxImage.Error);
                        return false;
                    }
                }

                String sReturn = cfg.ServerDataCacheList.GetInheritedFTPInfo(Config.ROOT_GROUP_ID);
                if (sReturn == null || sReturn.Length == 0)
                    return false;

                ContentsUpDownDlg dlg = new ContentsUpDownDlg(cfg, sReturn, tempDir, current_datainfo.USER_ID, true, null);
                if (dlg.ShowDialog() == false)
                    return false;

                string[] values = new string[field.CONTENTS.Length];
                int i = 0;
                foreach (Scheduler.Template.Model cont in field.CONTENTS)
                {
                    if (cont == null || cont.CONTENTS == null)
                        continue;

                    if (cont.CONTENTS.DESC.Equals("NEW"))
                    {
                        if (false == cfg.ServerContentsList.AddContets(cont.CONTENTS.ID, field.ID, cont.CONTENTS.TYPE, cont.CONTENTS.VALUE, cont.CONTENTS.NEW_NAME, "", "", current_datainfo.ID))
                        {
                            MessageBox.Show(Properties.Resources.mbErrorAddUser);
                            return false;
                        }
                    }
                    else
                    {
                        if (cont.CONTENTS.ISCHANGE == true && false == cfg.ServerContentsList.UpdateContents(cont.CONTENTS.ID, cont.CONTENTS.TYPE, cont.CONTENTS.VALUE, cont.CONTENTS.NEW_NAME, ""))
                        {
                            MessageBox.Show(Properties.Resources.mbErrorAddUser);
                            return false;
                        }
                    }
                    values[i++] = cont.CONTENTS.ID;
                }

                if (isNew)
                {
                    if (false == cfg.ServerDataFieldList.AddDataField(field.ID, field.DATAINFO_ID, field.TEMPLATEID, values))
                    {
                        MessageBox.Show(Properties.Resources.mbErrorAddUser);
                        return false;
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message.ToString());
            }

            return false;
        }

        private int IsSelectCont()
        {
            try
            {
                int count = 0;
                Scheduler.Template.DataFieldArry task = listData.ItemsSource as Scheduler.Template.DataFieldArry;
                foreach (Scheduler.Template.DataField_T _field in task)
                {
                    if (_field.ISSELECT == true)
                    {
                        count++;
                    }
                }
                return count;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message.ToString());
            }
            return 0;
        }

        public void Delete_Click(object sender, RoutedEventArgs e)
        {
            try
            {//mbDeleteData
                if (IsSelectCont() == 0)
                    return;

                string message = Properties.Resources.mbDeleteData;
                string caption = "Delete";
                MessageBoxButton buttons = MessageBoxButton.OKCancel;

                MessageBoxResult result = System.Windows.MessageBox.Show(message, caption, buttons);
                if (result == MessageBoxResult.Cancel)
                    return;

                String sExecutingPath = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                string tempDir = sExecutingPath + "\\temp\\" + current_datainfo.USER_ID + "\\";
                if (!Directory.Exists(tempDir))
                    Directory.CreateDirectory(tempDir);;

                String sReturn = m_cfg.ServerDataCacheList.GetInheritedFTPInfo(Config.ROOT_GROUP_ID);
                if (sReturn == null || sReturn.Length == 0)
                    return ;
                String[] arrFTPInfo = sReturn.Split('|');
                Scheduler.Template.DataFieldArry task = listData.ItemsSource as Scheduler.Template.DataFieldArry;
                DataStatusDlg dlg = new DataStatusDlg(arrFTPInfo, m_cfg, task, current_datainfo.USER_ID, sExecutingPath);
                if (dlg.ShowDialog() == false)
                    return;
                ReFresh();
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message.ToString());
            }
        }

        public  void OnDataValueChange(string fildid)
        {//mberrdatainput
            try
            {
                Scheduler.Template.DataFieldArry task = listData.ItemsSource as Scheduler.Template.DataFieldArry;
                Scheduler.Template.DataFieldArry arry = new DigitalSignage.Scheduler.Template.DataFieldArry();

                bool ischange = true;
                int tcount = 0, count = 0;
                foreach (Scheduler.Template.DataField_T _field in task)
                {
                    if (_field.ID != null && _field.ID.Equals(fildid))
                    {
                        count = tcount + 1;
                        if (_field.ISSELECT == true && _field.ISCHANGE == true)
                            return;
                        _field.ISSELECT = true;
                        _field.ISCHANGE = true;
                        arry.Add(_field);
                    }
                    else
                    {
                        arry.Add(_field);
                    }
                    tcount++;                    
                }                

                listData.ItemsSource = arry;
                if (count == task.Count)
                {
                    Add_Click(null, null);
                }
                
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message.ToString());
            }
        }

    }
}
