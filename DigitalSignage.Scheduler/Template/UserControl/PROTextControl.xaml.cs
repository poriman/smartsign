﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Globalization;

namespace DigitalSignage.Scheduler.Template.UserControl
{
    /// <summary>
    /// PROTextControl.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class PROTextControl : System.Windows.Controls.UserControl
    {
        Scheduler.Template.promotion _promition;
        bool isColor = false;
        private string _Color;
        private System.Windows.Media.Color m_Color = System.Windows.Media.Color.FromArgb(255, 255, 255, 255);
        public string FontColor
        {
            set
            {
                _Color = value;
                _Color = _Color.Replace("#", "");
                byte R = Byte.Parse(_Color.Substring(0, 2), NumberStyles.HexNumber);
                byte G = Byte.Parse(_Color.Substring(2, 2), NumberStyles.HexNumber);
                byte B = Byte.Parse(_Color.Substring(4, 2), NumberStyles.HexNumber);
                m_Color = System.Windows.Media.Color.FromArgb(255, R, G, B);
                r_color.Fill = new SolidColorBrush(m_Color);
                txt_Value.Foreground = new SolidColorBrush(m_Color);
                btn_color.Visibility = Visibility.Visible;
                isColor = true;
            }
            get
            {
                _Color = string.Format("#{0:X2}{1:X2}{2:X2}", m_Color.R, m_Color.G, m_Color.B);
                return _Color;
            }
        }

        public Scheduler.Template.promotion PROMOTION
        {
            set
            {
                _promition = value;
                if (_promition != null)
                {
                    tb_Title.Text = _promition.name;
                }
            }
            get { return _promition; }
        }

        public string Data_Value
        {
            set { this.txt_Value.Text = value; }
            get 
            {
                txt_Value.UpdateLayout();
                return this.txt_Value.Text; 
            }
        }

        public PROTextControl()
        {
            InitializeComponent();
        }

        private void _viewbox_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (isColor)
            {
                if (_viewbox.ActualWidth > 32)
                    txt_Value.Width = _viewbox.ActualWidth - 32;
            }
            else
            {
                if (_viewbox.ActualWidth > 2)
                    txt_Value.Width = _viewbox.ActualWidth - 2;
            }
        }

        private void btn_color_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.ColorDialog cd = new System.Windows.Forms.ColorDialog();
            cd.AllowFullOpen = true;
            cd.AnyColor = true;
            cd.Color = System.Drawing.Color.FromArgb(255, m_Color.R, m_Color.G, m_Color.B);

            if (cd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                m_Color =  System.Windows.Media.Color.FromArgb(255, cd.Color.R, cd.Color.G, cd.Color.B); 
                r_color.Fill = new SolidColorBrush(m_Color);
                txt_Value.Foreground = new SolidColorBrush(m_Color);  
            }
        }
    }
}
