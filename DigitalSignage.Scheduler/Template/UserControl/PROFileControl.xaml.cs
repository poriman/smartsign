﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;

namespace DigitalSignage.Scheduler.Template.UserControl
{
    /// <summary>
    /// PROFileControl.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class PROFileControl : System.Windows.Controls.UserControl
    {
        Scheduler.Template.promotion _promition;
        private string FullPaht = "";
        Scheduler.Template.Wizard.NewTemplateWizard maindlg = null;
        public Scheduler.Template.promotion PROMOTION
        {
            set
            {
                _promition = value;
                this.tb_Title.Text = _promition.name;
            }
            get { return _promition; }
        }

        public string Data_Value
        {
            set { this.txt_Value.Text = value; }
            get { return this.txt_Value.Text; }
        }

        public string FULLPATH
        {
            set { FullPaht = value; }
            get { return FullPaht; }
        }

        public PROFileControl(Scheduler.Template.Wizard.NewTemplateWizard dlg)
        {
            InitializeComponent();
            maindlg = dlg;
        }

        private void btn_FileOpen_Click(object sender, RoutedEventArgs e)
        {
            string value = _promition.cont_type;
            string filter = "";
            string title = "";
            switch (value)
            {
                case "T":
                    return;
                case "F":
                    title = "Open Flash";
                    filter = string.Format("Flash (*.swf;)|*.swf;");
                    break;
                case "M":
                    title = "Open Video";
                    filter = string.Format("Video (*.flv;)|*.flv;");
                    break;
                case "I":
                    title = "Open Image";
                    string images = "*.PNG;*.TIF;*.WMF;*.JPG;*.GIF;*.JPEG;*.EMF";
                    filter = string.Format("Image ({0})|{0}", images);
                    break;
            }

            System.Windows.Forms.OpenFileDialog fileDlg = new System.Windows.Forms.OpenFileDialog();
            fileDlg.Title = title;
            fileDlg.Filter = filter;
            if (fileDlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                DirectoryInfo info = new DirectoryInfo(fileDlg.FileName);
                txt_Value.Text = info.Name;
                FULLPATH = info.FullName;
                try
                {
                    File.Copy(info.FullName, maindlg.GetContentsFolder() + info.Name, true);
                }
                catch { }
            }
        }
    }
}
