﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigitalSignage.Scheduler.Template
{
    public class Contents
    {
        private string id;
        private string datafield_id;
        private string name;
        private string org_name;
        private string type;
        private string path;
        private string desc;
        private bool ischange;
        private string posion;

        public Contents()
        {
            id              = "";
            datafield_id    = "";
            name            = "";
            org_name        = "";
            type            = "";
            path            = "";
            desc            = "";
            ischange        = false;
            posion          = "";
        }

        public bool ISCHANGE
        {
            set { ischange = value; }
            get { return ischange; }
        }

        public string ID
        {
            set { id = value; }
            get { return id; }
        }

        public string DATAFIELD_ID
        {
            set { datafield_id = value; }
            get { return datafield_id; }
        }

        public string VALUE
        {
            set { name = value; }
            get { return name; }
        }

        public string NEW_NAME
        {
            set { org_name = value; }
            get { return org_name; }
        }

        public string TYPE
        {
            set { type = value; }
            get { return type; }
        }

        public string PATH
        {
            set { path = value; }
            get { return path; }
        }

        public string DESC
        {
            set { desc = value; }
            get { return desc; }
        }

        public string POSION
        {
            set { posion = value; }
            get { return posion; }
        }
    }
}
