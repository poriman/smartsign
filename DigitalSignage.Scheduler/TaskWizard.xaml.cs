﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using NLog;

namespace DigitalSignage.Scheduler
{
	/// <summary>
	/// Interaction logic for TaskWizard.xaml
	/// </summary>
	public partial class TaskWizard : Window
	{
		private static Logger logger = LogManager.GetCurrentClassLogger();
		private Config cfg = null;

		//	적용할 날짜
		private Nullable<DateTime> _dateOfSelected = null;
		public DateTime ApplyDate
		{
			set { _dateOfSelected = value; }
		}


		private object targetObject = null;

		public Object TargetObject
		{
			set
			{
				targetObject = value;
				uiTaskMaker.TargetObject = value;
			}
		}

		public Config Cfg
		{
			set 
			{ 
				cfg = value;
				uiTaskMaker.Cfg = value;
				uiPlaylistEditorMain.Cfg = value;
				uiProgramEditorMain.Cfg = value;
			}
		}
		public TaskWizard(Config cfg)
		{
			InitializeComponent();
			Cfg = cfg;
		}

		#region Event handlers
		private void btnClose_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				this.DialogResult = false;

				this.Close();
			}
			catch (Exception err)
			{
				logger.Error(err + "");
			}
		}

		private void Window_Loaded(object sender, RoutedEventArgs e)
		{
			try
			{
				DateTime now = DateTime.Now.AddHours(1);
				uiTaskMaker.StartDateUI.SelectedDateTime = false == _dateOfSelected.HasValue ? now : _dateOfSelected.Value;
				uiTaskMaker.StartTimeUI.SelectedHour = now.Hour;
				uiTaskMaker.StartTimeUI.SelectedMinute = 0;
				uiTaskMaker.StartTimeUI.SelectedSecond = 0;

				now = DateTime.Now.AddHours(2);
				uiTaskMaker.FinDateUI.SelectedDateTime = false == _dateOfSelected.HasValue ? now : _dateOfSelected.Value;
				uiTaskMaker.FinTimeUI.SelectedHour = now.Hour;
				uiTaskMaker.FinTimeUI.SelectedMinute = 0;
				uiTaskMaker.FinTimeUI.SelectedSecond = 0;

				uiTaskMaker.ParentWindow = this;
				uiPlaylistEditorMain.ParentWindow = this;
				uiProgramEditorMain.ParentWindow = this;

				//	Type을 선택할수 있도록한다
				uiTaskMaker.CanSelectType = true;
				uiTaskMaker.ProgramType = 0;

				uiTaskMaker.TaskName = Properties.Resources.labelDefaultTaskName;
				uiTaskMaker.ParentWindow = this;

			}
			catch (Exception err)
			{
				logger.Error(err + "");
			}
		}

		private void btnOk_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				if(false == uiTaskMaker.ValidationCheck())
				{
					return;
				}

				DateTime SelectedStartDate = uiTaskMaker.StartDateUI.SelectedDateTime != null ? uiTaskMaker.StartDateUI.SelectedDateTime.Value : DateTime.Now;
				DateTime SelectedEndDate = uiTaskMaker.FinDateUI.SelectedDateTime != null ? uiTaskMaker.FinDateUI.SelectedDateTime.Value : DateTime.Now;

				DateTime from = new DateTime(SelectedStartDate.Year,
							SelectedStartDate.Month,
							SelectedStartDate.Day,
							uiTaskMaker.StartTimeUI.SelectedHour,
							uiTaskMaker.StartTimeUI.SelectedMinute,
							uiTaskMaker.StartTimeUI.SelectedSecond);
				DateTime till = new DateTime(SelectedEndDate.Year,
							SelectedEndDate.Month,
							SelectedEndDate.Day,
							uiTaskMaker.FinTimeUI.SelectedHour,
							uiTaskMaker.FinTimeUI.SelectedMinute,
							uiTaskMaker.FinTimeUI.SelectedSecond);
// 				if (from > till)
// 				{
// 					System.Windows.MessageBox.Show(Properties.Resources.mbWrongTime, Properties.Resources.titleMessageBox,
// 						MessageBoxButton.OK, MessageBoxImage.Error);
// 					return;
// 				}

				int nRet = 0;
				if ((nRet = uiTaskMaker.createTask(from, till, uiTaskMaker.TaskName)) != 0)
				{
					//	오류가 났기 때문에 메시지를 출력한다.
					if (nRet == -1) System.Windows.MessageBox.Show(Properties.Resources.mbErrorFTPUpload, Properties.Resources.titleMessageBox,
						MessageBoxButton.OK, MessageBoxImage.Error);
					return;
				}
				this.DialogResult = true;
				this.Close();
			}
			catch (Exception err)
			{
				MessageBox.Show(err.Message, Properties.Resources.titleMessageBox, MessageBoxButton.OK, MessageBoxImage.Error);
				logger.Error(err + "");
			}
		}

		#endregion

		private void wndTab_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			try
			{
				if (tabPlaylist.Equals(wndTab.SelectedItem))
				{
					tabPlaylist.Focus();
				}
				else if (tabProgram.Equals(wndTab.SelectedItem))
				{
					tabProgram.Focus();
				}
				else if (tabTask.Equals(wndTab.SelectedItem))
				{
					tabTask.Focus();
				}
			}
			catch (System.Exception err)
			{
				logger.Error(err + "");
			}

		}

	}
}
