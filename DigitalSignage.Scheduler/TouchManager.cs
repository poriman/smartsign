﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace DigitalSignage.Scheduler
{
    static class TouchManager
    {
        /// <summary>
        /// by_JunYoung_Touchflow: Touchflow file deserialze 
        /// </summary>
        /// <param name="path"></param>
        internal static object GetTouchProjectInfoFromTouchFile(string path, string prop_name)
        {
            FileStream fileStream = new FileStream(path, FileMode.Open, FileAccess.Read);
            try
            {
                BinaryFormatter binaryFormatter = new BinaryFormatter();
                List<Dictionary<string, object>> propertiesSet = new List<Dictionary<string, object>>();

                propertiesSet = binaryFormatter.Deserialize(fileStream) as List<Dictionary<string, object>>;
                return propertiesSet[0][prop_name];
            }
            catch (Exception /*ee*/)
            {
                return new TimeSpan(23, 59, 59);
            }
            finally
            {
                fileStream.Close();
            }
        }

        internal static bool IsTouchFile(string path)
        {
            FileInfo fi = new FileInfo(path);
            if (fi.Extension.ToLower().Equals(".tpf") == true)
                return true;
            return false;
        }

        internal static void UpdateTouchProjectInfo(string path, string prop_name, object update_value)
        {
            List<Dictionary<string, object>> propertiesSet = new List<Dictionary<string, object>>();
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            
            try
            {
                using (FileStream fileStream = new FileStream(path, FileMode.Open, FileAccess.Read))
                {
                    propertiesSet = binaryFormatter.Deserialize(fileStream) as List<Dictionary<string, object>>;
                    if (propertiesSet[0].Keys.Contains(prop_name) == false)
                        propertiesSet[0].Add(prop_name, update_value);
                    else
                        propertiesSet[0][prop_name] = update_value;
                }

                using (FileStream fileStream = new FileStream(path, FileMode.Open, FileAccess.Write))
                {
                    binaryFormatter.Serialize(fileStream, propertiesSet);
                }
            }
            catch (Exception ee)
            {                
            }           
        }
    }
}
