﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

using System.Net;
using System.Net.Sockets;
using System.Threading;

using NLog;

using DigitalSignage.ServerDatabase;
using System.Xml;
using System.Collections.ObjectModel;
using System.Security.Cryptography;
using DigitalSignage.Common;

namespace DigitalSignage.Scheduler
{
    /// <summary>
    /// Interaction logic for NewDlgItem.xaml
    /// </summary>
    public partial class EditUsers : Window
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private Config cfg = null;

		private XmlNode current_user = ServerDataCache.GetInstance.CurrentUser;

		private bool editMode = false;
        private long userID = -1;
		private long parentID = -1;

		private String[] groups = null;
		private String[] players = null;

        public EditUsers( Config config, XmlNode user, long parent_id, bool _editMode)
        {
            InitializeComponent();

			editMode = _editMode;
			parentID = parent_id;
			cfg = config;
			
			PrepareData();

			if (user != null)
				loadData(user);
			else
			{
				Title = Properties.Resources.menuitemAddUsers;
			}
        }

		private void PrepareData()
		{
			try
			{
				userGroup.Items.Clear();

				foreach (XmlNode rolenode in current_user.ChildNodes)
				{
					if (rolenode.LocalName.Equals("role") && rolenode.Attributes["is_enabled"].Value == "Y")
					{
						using (ServerDataSet.rolesDataTable rdt = cfg.ServerUsersList.GetAvailableRoleList(rolenode.Attributes["role_name"].Value, Convert.ToInt32(rolenode.Attributes["role_level"].Value)))
						{
							try
							{
								foreach(ServerDataSet.rolesRow row in rdt.Rows)
								{
									AddRole(row);
								}

								rdt.Dispose();
							}
							catch (Exception ex)
							{
								logger.Error(ex.ToString());
							}
						}

					}
				}
			}
			catch {}
		}
		private void AddRole(ServerDataSet.rolesRow role)
		{
// 			int MinRoleLevel = ServerDataCache.GetInstance.GetRoleLevel();
// 			//	역할의 레벨이 같거나 큰것만 추가할 수 있다.
// 			if (role.role_level < MinRoleLevel)
// 				return;

			foreach(CheckBox item in userGroup.Items)
			{
				//	 중복된 Role은 추가하지 않는다.
				if (((long)(item.DataContext)) == role.role_id)
					return;
			}
			CheckBox cbRole = new CheckBox();
			
			cbRole.DataContext = role.role_id;
			cbRole.Content = role.role_name;

			userGroup.Items.Add(cbRole);
		}

		String GetDeviceUIText()
		{
			ServerDataCache cache = ServerDataCache.GetInstance;

			String sReturn = "";

			try
			{
				if (groups != null)
				{
					foreach (string groupid in groups)
					{
						XmlNode node = cache.GetGroupByGID(groupid);

						sReturn += node.Attributes["name"].Value;
						sReturn += ", ";
					}
				}

				if (players != null)
				{
					foreach (string playerid in players)
					{
						XmlNode node = cache.GetPlayer(playerid);

						sReturn += node.Attributes["name"].Value;
						sReturn += ", ";
					}
				}
			}
			catch { }

			char[] trims = { ',', ' ' };
			return sReturn.TrimEnd(trims);
		}

		private void loadData(XmlNode user)
        {
            try
            {
                editMode = true;
				userID = Convert.ToInt64(user.Attributes["id"].Value);
				userLogin.Text = user.Attributes["name"].Value;
                userLogin.IsEnabled = false;

				parentID = Convert.ToInt64(user.Attributes["parent_id"].Value);

				groups = cfg.ServerUsersList.GetGroupList(userID);
				players = cfg.ServerUsersList.GetPlayerList(userID);
				deviceUI.Text = GetDeviceUIText();

				CheckRoles(user.Attributes["role_ids"].Value);
				
            }
            catch (Exception err)
            {
                logger.Error(err + "");
				MessageBox.Show(String.Format(Properties.Resources.mbErrorServer, err.Message), Properties.Resources.titleEditUser, MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

		private bool HasRolesChecked()
		{
			try
			{
				foreach (CheckBox cb in userGroup.Items)
				{
					if (cb.IsChecked.Value) return true;
				}
			}
			catch {}

			return false;
		}

		private String CheckedRoles()
		{
			try
			{
				String sReturn = "";
				foreach (CheckBox cb in userGroup.Items)
				{
					if (cb.IsChecked.Value) sReturn += (cb.DataContext.ToString() + "|");
				}
				
				char[] trims = {'|', ' '};
				return sReturn.TrimEnd(trims);
			}
			catch { }

			return null;
		}

		private void CheckRoles(String role_ids)
		{
			try
			{
				String[] arrRoles = role_ids.Split('|');
				foreach (CheckBox cb in userGroup.Items)
				{
					if(arrRoles.Contains<String>(cb.DataContext.ToString()))
					{
						cb.IsChecked = true;
					}
				}
			}
			catch { }
		}

		private void OK_Buttion_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (userLogin.Text.Equals(""))
                {
					MessageBox.Show(Properties.Resources.mbUserLogin);
                    return;
                }
                if (userPassword.Password.Equals(""))
                {
					MessageBox.Show(Properties.Resources.mbUserPassword);
                    return;
                }
                if ( !userPassword.Password.Equals( userPassword2.Password ) )
                {
					MessageBox.Show(Properties.Resources.mbPasswordWrong);
                    return;
                }

				if (!HasRolesChecked())
				{
					MessageBox.Show(Properties.Resources.mbCheckRoles);
                    return;
				}

                if (!editMode)
                {
					if (!cfg.ServerUsersList.AddUser(userLogin.Text, GetHash(userPassword.Password), CheckedRoles(), parentID, groups, players))
					{
						try
						{
							///	사용자 생성 기록
							cfg.ServerLogList.InsertDetailLog(ServerDataCache.GetInstance.Connected_UserName, (int)(LogType.LogType_Managing | LogType.LogType_Failed), "", "USER_CREATE", userLogin.Text, 0, 0, TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()));
						}
						catch { } 
						
						MessageBox.Show(Properties.Resources.mbErrorAddUser);
						return;
					}
					else
					{
						try
						{
							///	사용자 생성 기록
							cfg.ServerLogList.InsertDetailLog(ServerDataCache.GetInstance.Connected_UserName, (int)(LogType.LogType_Managing | LogType.LogType_Proceed), "", "USER_CREATE", userLogin.Text, 0, 0, TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()));
						}
						catch { }
					}
                }
                else
                {
					if (!cfg.ServerUsersList.EditUser(userID, GetHash(userPassword.Password), CheckedRoles(), parentID, groups, players))
					{
						try
						{
							///	사용자 수정 기록
							cfg.ServerLogList.InsertDetailLog(ServerDataCache.GetInstance.Connected_UserName, (int)(LogType.LogType_Managing | LogType.LogType_Failed), "", "USER_UPDATE", userLogin.Text, 0, 0, TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()));
						}
						catch { }

						MessageBox.Show(Properties.Resources.mbErrorAddUser);
						return;
					}
					else
					{
						try
						{
							///	사용자 수정 기록
							cfg.ServerLogList.InsertDetailLog(ServerDataCache.GetInstance.Connected_UserName, (int)(LogType.LogType_Managing | LogType.LogType_Proceed), "", "USER_UPDATE", userLogin.Text, 0, 0, TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()));
						}
						catch { }
					}

                }

                this.Close();
            }
            catch (Exception err)
            {
                logger.Error(err + "");
				MessageBox.Show(String.Format(Properties.Resources.mbErrorServer, err.Message), Properties.Resources.titleEditUser, MessageBoxButton.OK, MessageBoxImage.Error);

            }
        }

		private void Cancel_Buttion_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

		private Collection<XmlNode> PrepareCheckedNode()
		{
			Collection<XmlNode> returnData = new Collection<XmlNode>();
			ServerDataCache insCache = ServerDataCache.GetInstance;

			try
			{
				if( groups != null)
				{
					foreach (string group in groups)
					{
						returnData.Add(insCache.GetGroupByGID(group));
					}
				}

				if( players != null)
				{
					foreach (string player in players)
					{
						returnData.Add(insCache.GetPlayer(player));
					}

				}
			}
			catch { returnData = null; }

			return returnData;
		}

        private void Group_Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                GroupPicker dlg = new GroupPicker(cfg, false, true);
                dlg.Owner = this;
				dlg.TargetObject = PrepareCheckedNode();
                dlg.ShowDialog();

				if (dlg.DialogResult == null || dlg.DialogResult.Value == false)
					return;

				deviceUI.Text = "";

				if (dlg.groupTree.CheckedItems != null)
				{
					players = null;
					groups = new String[dlg.groupTree.CheckedItems.Count];
					int i = 0;
					foreach(TreeViewItem item in dlg.groupTree.CheckedItems)
					{
						try
						{
							XmlNode node = item.DataContext as XmlNode;
							groups[i++] = node.Attributes["gid"].Value;
						}
						catch {}
					}
					deviceUI.Text = GetDeviceUIText();
				}
				else
				{
					XmlNode item = dlg.groupTree.SelectedItem as XmlNode;
					if (item.LocalName.Equals("group"))
					{
						players = null;
						groups = new String[1];
						groups[0] = item.Attributes["gid"].Value;
						deviceUI.Text = item.Attributes["name"].Value;
					}
					else
						if (item.LocalName.Equals("player"))
						{
							groups = null;
							players = new String[1];

							players[0] = item.Attributes["pid"].Value;
							deviceUI.Text = item.Attributes["name"].Value;
						}
				}

            }
            catch (Exception err)
            {
                logger.Error(err + " " + err.StackTrace);
            }
        }
		/// <summary>
		/// 패스워드 해시값 추출
		/// </summary>
		/// <param name="origin"></param>
		/// <returns></returns>
		private string GetHash(string origin)
		{
			if (String.IsNullOrEmpty(origin))
				return origin;

			StringBuilder sb = new StringBuilder();
			MD5 md5 = new MD5CryptoServiceProvider();
			Byte[] original;
			Byte[] encoded;
			original = ASCIIEncoding.Default.GetBytes(origin);
			encoded = md5.ComputeHash(original);
			foreach (byte hex in encoded) sb.Append(hex.ToString("x2"));
			return sb.ToString().ToLower();
		}

		void tb_GotKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
		{
			try
			{
				TextBox tb = sender as TextBox;
				if (tb != null) tb.SelectAll();
				else
				{
					PasswordBox pb = sender as PasswordBox;
					pb.SelectAll();
				}

				e.Handled = true;
			}
			catch { }
		}
    }
}
