﻿using System;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;

using DigitalSignage.Common;

class Program
{
    protected static readonly long epoch_start = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1)).Ticks;

    public static long toUTP(DateTime dt)
    {
        //converting from 100-nanosecons intervals to 1-second intervals
        return (dt.Ticks - epoch_start) / 10000000;
    }

    public Program()
    {
    }

    public void testAddTask()
    {
        Task t = new Task();
        t.type = Task.TaskCommands.CmdUndefined;
        //t.name = "Test task";
        t.group_id = 0;
        t.TaskStart = toUTP( DateTime.Now ) + 30;
        t.uuid = Guid.NewGuid();
        //t.uuid = System.Guid.NewGuid();
        ITaskList rt = (ITaskList)Activator.GetObject(
          typeof(ITaskList),
          "http://127.0.0.1:888/TaskListHost/rt");
        rt.AddTask( t );
    }

    [STAThread]
    static void Main(string[] args)
    {
        Program p = new Program();
        p.testAddTask();
        //Console.ReadLine();
    }
}

