﻿#pragma checksum "..\..\..\..\Components\NPCUResult.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "3715176633E533AB47AE45FB3A6E5096"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34209
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using DigitalSignage.Scheduler;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace DigitalSignage.Scheduler {
    
    
    /// <summary>
    /// NPCUResult
    /// </summary>
    public partial class NPCUResult : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 153 "..\..\..\..\Components\NPCUResult.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock tbTotal;
        
        #line default
        #line hidden
        
        
        #line 155 "..\..\..\..\Components\NPCUResult.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock tbSuccess;
        
        #line default
        #line hidden
        
        
        #line 157 "..\..\..\..\Components\NPCUResult.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock tbFailed;
        
        #line default
        #line hidden
        
        
        #line 159 "..\..\..\..\Components\NPCUResult.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock tbNotConn;
        
        #line default
        #line hidden
        
        
        #line 161 "..\..\..\..\Components\NPCUResult.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock tbTimeout;
        
        #line default
        #line hidden
        
        
        #line 163 "..\..\..\..\Components\NPCUResult.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock tbEtc;
        
        #line default
        #line hidden
        
        
        #line 167 "..\..\..\..\Components\NPCUResult.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListView lvResult;
        
        #line default
        #line hidden
        
        
        #line 169 "..\..\..\..\Components\NPCUResult.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.GridView gvLogs;
        
        #line default
        #line hidden
        
        
        #line 170 "..\..\..\..\Components\NPCUResult.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.GridViewColumn colGroup;
        
        #line default
        #line hidden
        
        
        #line 171 "..\..\..\..\Components\NPCUResult.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal DigitalSignage.Scheduler.GridViewColumnHeaderEx hdGroup;
        
        #line default
        #line hidden
        
        
        #line 173 "..\..\..\..\Components\NPCUResult.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.GridViewColumn colPlayer;
        
        #line default
        #line hidden
        
        
        #line 174 "..\..\..\..\Components\NPCUResult.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal DigitalSignage.Scheduler.GridViewColumnHeaderEx hdPlayer;
        
        #line default
        #line hidden
        
        
        #line 176 "..\..\..\..\Components\NPCUResult.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.GridViewColumn colPlayerID;
        
        #line default
        #line hidden
        
        
        #line 177 "..\..\..\..\Components\NPCUResult.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal DigitalSignage.Scheduler.GridViewColumnHeaderEx hdPlayerID;
        
        #line default
        #line hidden
        
        
        #line 179 "..\..\..\..\Components\NPCUResult.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.GridViewColumn colIsSuccess;
        
        #line default
        #line hidden
        
        
        #line 180 "..\..\..\..\Components\NPCUResult.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal DigitalSignage.Scheduler.GridViewColumnHeaderEx hdIsSuccess;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/DS.ScreenManager;component/components/npcuresult.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\Components\NPCUResult.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal System.Delegate _CreateDelegate(System.Type delegateType, string handler) {
            return System.Delegate.CreateDelegate(delegateType, this, handler);
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 6 "..\..\..\..\Components\NPCUResult.xaml"
            ((DigitalSignage.Scheduler.NPCUResult)(target)).Loaded += new System.Windows.RoutedEventHandler(this.Window_Loaded);
            
            #line default
            #line hidden
            return;
            case 2:
            this.tbTotal = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 3:
            this.tbSuccess = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 4:
            this.tbFailed = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 5:
            this.tbNotConn = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 6:
            this.tbTimeout = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 7:
            this.tbEtc = ((System.Windows.Controls.TextBlock)(target));
            return;
            case 8:
            this.lvResult = ((System.Windows.Controls.ListView)(target));
            return;
            case 9:
            this.gvLogs = ((System.Windows.Controls.GridView)(target));
            return;
            case 10:
            this.colGroup = ((System.Windows.Controls.GridViewColumn)(target));
            return;
            case 11:
            this.hdGroup = ((DigitalSignage.Scheduler.GridViewColumnHeaderEx)(target));
            return;
            case 12:
            this.colPlayer = ((System.Windows.Controls.GridViewColumn)(target));
            return;
            case 13:
            this.hdPlayer = ((DigitalSignage.Scheduler.GridViewColumnHeaderEx)(target));
            return;
            case 14:
            this.colPlayerID = ((System.Windows.Controls.GridViewColumn)(target));
            return;
            case 15:
            this.hdPlayerID = ((DigitalSignage.Scheduler.GridViewColumnHeaderEx)(target));
            return;
            case 16:
            this.colIsSuccess = ((System.Windows.Controls.GridViewColumn)(target));
            return;
            case 17:
            this.hdIsSuccess = ((DigitalSignage.Scheduler.GridViewColumnHeaderEx)(target));
            return;
            case 18:
            
            #line 187 "..\..\..\..\Components\NPCUResult.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.btnOK_Click);
            
            #line default
            #line hidden
            return;
            case 19:
            
            #line 188 "..\..\..\..\Components\NPCUResult.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.btnCancel_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

