﻿#pragma checksum "..\..\..\Components\ReportViewer.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "6D7482DE2AE6EF9C50AF231162954EB2"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34209
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using AC.AvalonControlsLibrary.Controls;
using AxMicrosoft.Office.Interop.Owc11;
using DigitalSignage.Controls;
using DigitalSignage.Scheduler;
using FarsiLibrary.FX.Win.Controls;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace DigitalSignage.Scheduler {
    
    
    /// <summary>
    /// ReportViewer
    /// </summary>
    public partial class ReportViewer : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 171 "..\..\..\Components\ReportViewer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid gridReportType;
        
        #line default
        #line hidden
        
        
        #line 178 "..\..\..\Components\ReportViewer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cbReportType;
        
        #line default
        #line hidden
        
        
        #line 191 "..\..\..\Components\ReportViewer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid gridReportDateUnit;
        
        #line default
        #line hidden
        
        
        #line 198 "..\..\..\Components\ReportViewer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cbReportUnit;
        
        #line default
        #line hidden
        
        
        #line 206 "..\..\..\Components\ReportViewer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid gridSelectYear;
        
        #line default
        #line hidden
        
        
        #line 213 "..\..\..\Components\ReportViewer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cbSelectYear;
        
        #line default
        #line hidden
        
        
        #line 217 "..\..\..\Components\ReportViewer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid gridSelectMonth;
        
        #line default
        #line hidden
        
        
        #line 224 "..\..\..\Components\ReportViewer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cbSelectMonth;
        
        #line default
        #line hidden
        
        
        #line 227 "..\..\..\Components\ReportViewer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid gridSelectDate;
        
        #line default
        #line hidden
        
        
        #line 234 "..\..\..\Components\ReportViewer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal FarsiLibrary.FX.Win.Controls.FXDatePicker dtSelectDate;
        
        #line default
        #line hidden
        
        
        #line 237 "..\..\..\Components\ReportViewer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid gridTargetGroup;
        
        #line default
        #line hidden
        
        
        #line 244 "..\..\..\Components\ReportViewer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cbTargetGroup;
        
        #line default
        #line hidden
        
        
        #line 252 "..\..\..\Components\ReportViewer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid gridFilter;
        
        #line default
        #line hidden
        
        
        #line 259 "..\..\..\Components\ReportViewer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox cbReportFilter;
        
        #line default
        #line hidden
        
        
        #line 265 "..\..\..\Components\ReportViewer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid gridTarget;
        
        #line default
        #line hidden
        
        
        #line 272 "..\..\..\Components\ReportViewer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal DigitalSignage.Scheduler.GroupTree groupBox;
        
        #line default
        #line hidden
        
        
        #line 274 "..\..\..\Components\ReportViewer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnMakeReport;
        
        #line default
        #line hidden
        
        
        #line 278 "..\..\..\Components\ReportViewer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label lbMakeReportResult;
        
        #line default
        #line hidden
        
        
        #line 280 "..\..\..\Components\ReportViewer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Forms.Integration.WindowsFormsHost excelHost;
        
        #line default
        #line hidden
        
        
        #line 283 "..\..\..\Components\ReportViewer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnExport;
        
        #line default
        #line hidden
        
        
        #line 284 "..\..\..\Components\ReportViewer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button btnSaveToFile;
        
        #line default
        #line hidden
        
        
        #line 286 "..\..\..\Components\ReportViewer.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button okButton;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/DS.ScreenManager;component/components/reportviewer.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\Components\ReportViewer.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal System.Delegate _CreateDelegate(System.Type delegateType, string handler) {
            return System.Delegate.CreateDelegate(delegateType, this, handler);
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.gridReportType = ((System.Windows.Controls.Grid)(target));
            return;
            case 2:
            this.cbReportType = ((System.Windows.Controls.ComboBox)(target));
            
            #line 178 "..\..\..\Components\ReportViewer.xaml"
            this.cbReportType.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.cbReportType_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 3:
            this.gridReportDateUnit = ((System.Windows.Controls.Grid)(target));
            return;
            case 4:
            this.cbReportUnit = ((System.Windows.Controls.ComboBox)(target));
            
            #line 198 "..\..\..\Components\ReportViewer.xaml"
            this.cbReportUnit.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.cbReportUnit_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 5:
            this.gridSelectYear = ((System.Windows.Controls.Grid)(target));
            return;
            case 6:
            this.cbSelectYear = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 7:
            this.gridSelectMonth = ((System.Windows.Controls.Grid)(target));
            return;
            case 8:
            this.cbSelectMonth = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 9:
            this.gridSelectDate = ((System.Windows.Controls.Grid)(target));
            return;
            case 10:
            this.dtSelectDate = ((FarsiLibrary.FX.Win.Controls.FXDatePicker)(target));
            return;
            case 11:
            this.gridTargetGroup = ((System.Windows.Controls.Grid)(target));
            return;
            case 12:
            this.cbTargetGroup = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 13:
            this.gridFilter = ((System.Windows.Controls.Grid)(target));
            return;
            case 14:
            this.cbReportFilter = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 15:
            this.gridTarget = ((System.Windows.Controls.Grid)(target));
            return;
            case 16:
            this.groupBox = ((DigitalSignage.Scheduler.GroupTree)(target));
            return;
            case 17:
            this.btnMakeReport = ((System.Windows.Controls.Button)(target));
            
            #line 274 "..\..\..\Components\ReportViewer.xaml"
            this.btnMakeReport.Click += new System.Windows.RoutedEventHandler(this.btnMakeReport_Click);
            
            #line default
            #line hidden
            return;
            case 18:
            this.lbMakeReportResult = ((System.Windows.Controls.Label)(target));
            return;
            case 19:
            this.excelHost = ((System.Windows.Forms.Integration.WindowsFormsHost)(target));
            return;
            case 20:
            this.btnExport = ((System.Windows.Controls.Button)(target));
            
            #line 283 "..\..\..\Components\ReportViewer.xaml"
            this.btnExport.Click += new System.Windows.RoutedEventHandler(this.btnExport_Click);
            
            #line default
            #line hidden
            return;
            case 21:
            this.btnSaveToFile = ((System.Windows.Controls.Button)(target));
            
            #line 284 "..\..\..\Components\ReportViewer.xaml"
            this.btnSaveToFile.Click += new System.Windows.RoutedEventHandler(this.btnSaveToFile_Click);
            
            #line default
            #line hidden
            return;
            case 22:
            this.okButton = ((System.Windows.Controls.Button)(target));
            
            #line 286 "..\..\..\Components\ReportViewer.xaml"
            this.okButton.Click += new System.Windows.RoutedEventHandler(this.okButton_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

