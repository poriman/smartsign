﻿using System;
using System.Xml;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Interop;
using System.Windows.Media.Animation;

using NLog;

using DigitalSignage.Common;
using DigitalSignage.Controls;
using DigitalSignage.PlayControls;

namespace DigitalSignage.Scheduler
{
    /// <summary>
    /// Interaction logic for NewDlgItem.xaml
    /// </summary>
    public partial class PlayListEditor : Window
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private Config cfg = null;

        public PlayListEditor(Config config)
        {
            InitializeComponent();
            cfg = config;
			uiPlaylistEditorMain.Cfg = config;
//			uiPlaylistEditorMain.ParentWindow = this;
//             restoreParams();
        }

        private void restoreParams()
        {
            this.Width = cfg.GetConfigInt("PE.width", 800);
            this.Height = cfg.GetConfigInt("PE.height", 600);
            this.Left = cfg.GetConfigInt("PE.left", 100);
            this.Top = cfg.GetConfigInt("PE.top", 100);
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
//             cfg.SetConfigInt("PE.width", (int)this.ActualWidth);
//             cfg.SetConfigInt("PE.height", (int)this.ActualHeight);
//             cfg.SetConfigInt("PE.left", (int)this.Left);
//             cfg.SetConfigInt("PE.top", (int)this.Top);
        }
    }
}
