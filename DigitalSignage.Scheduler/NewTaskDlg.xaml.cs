﻿using System;
using System.Collections.Generic;
using System.Collections;

using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Forms;

using DigitalSignage.Common;
using DigitalSignage.Controls;

using NLog;
using DigitalSignage.ServerDatabase;
using System.Xml;

namespace DigitalSignage.Scheduler
{
    /// <summary>
    /// Interaction logic for NewDlgItem.xaml
    /// </summary>
    public partial class NewTaskDlg : Window
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private Config cfg;
        private bool editMode = false;
        public bool result = false;
        private string projectFileName = null;

		private XmlNode selectedTarget = null;

        private Task oldTask = null;
        // multi project data (project file name and timings)
        private ArrayList multiProjects = new ArrayList(); // should contains project paths
        private ArrayList multiDuuids = new ArrayList(); // should contains project duuids
        private ArrayList multiTargets = new ArrayList(); // should contains target list
        private ArrayList multiTimings = new ArrayList(); // should contain timespan objects

		enum _category { taskProgram = 0, taskSubtitle = 1, taskDefaultSchedule = 2, taskRS232C = 3 };

        public NewTaskDlg(Config config, String taskGuid)
        {
            cfg = config;
            InitializeComponent();
            if ((taskGuid != null) && (!taskGuid.Equals("")))
            {
                editMode = true;
                SetEditGUIMode();
                loadTaskData(taskGuid);
            }
        }

        private void SetEditGUIMode()
        {
            projectFileNameStr.IsEnabled = false;
            projectFileNameStrSelector.IsEnabled = false;
        }

        private void loadTaskData(String guuid)
        {
            ServerDataSet.tasksDataTable table = cfg.ServerTasksList.GetTasksGroup(guuid);
			if (table.Rows.Count < 1) throw new Exception(Properties.Resources.exceptionTaskNotFound);
			long taskStart = ((ServerDataSet.tasksRow)table.Rows[0]).starttime;
			long taskEnd = ((ServerDataSet.tasksRow)table.Rows[0]).endtime;
            bool multiTask = false;
            for (int i = 0; i < table.Rows.Count; i++)
            {
				if (taskStart > ((ServerDataSet.tasksRow)table.Rows[i]).starttime)
                {
					taskStart = ((ServerDataSet.tasksRow)table.Rows[i]).starttime;
                    multiTask = true;
                }
				if (taskEnd < ((ServerDataSet.tasksRow)table.Rows[i]).endtime)
                {
					taskEnd = ((ServerDataSet.tasksRow)table.Rows[i]).endtime;
                    multiTask = true;
                }
            }

            oldTask = new Task((ServerDataSet.tasksRow)table.Rows[0]);
			if ((String.IsNullOrEmpty(oldTask.pid) || oldTask.pid == "-1"))
			{
				selectedTarget = ServerDataCache.GetInstance.GetGroupByGID(oldTask.gid);
				if (selectedTarget != null)
					subjectName.Text = selectedTarget.Attributes["name"].Value;
				else subjectName.Text = "";
			}
			else
			{
				selectedTarget = ServerDataCache.GetInstance.GetPlayer(oldTask.pid);
				if (selectedTarget != null)
				{
					// this is project for one player
					subjectName.Text = selectedTarget.Attributes["name"].Value;
				}
				else subjectName.Text = "";
			}

			this.cboxAllowDup.IsChecked = (oldTask.show_flag & (int)ShowFlag.Show_Allow_Duplication) > 0;

            multiPrj.IsChecked = false;
            singlePrj.IsChecked = true;

			multiPrj.IsEnabled = false;
            singlePrj.IsEnabled = false;
            targetButton.IsEnabled = false;

            projectDesc.Text = ((ServerDataSet.tasksRow)table.Rows[0]).name;
            projectFileNameStrSelector.IsEnabled = false;
            DateTime dt = TimeConverter.ConvertFromUTP(taskStart).ToLocalTime();
            dateFrom.SelectedDateTime = dt;
            timeFrom.SelectedHour = dt.Hour;
            timeFrom.SelectedMinute = dt.Minute;
            timeFrom.SelectedSecond = dt.Second;
            dt = TimeConverter.ConvertFromUTP(taskEnd).ToLocalTime();
			dateTill.SelectedDateTime = dt;
            timeTill.SelectedHour = dt.Hour;
            timeTill.SelectedMinute = dt.Minute;
            timeTill.SelectedSecond = dt.Second;
			int intTypeRepresentation = ((ServerDataSet.tasksRow)table.Rows[0]).type;

			// Duration
			duration.SelectedTime = new TimeSpan(0, 0, ((ServerDataSet.tasksRow)table.Rows[0]).duration);

			//	day of week
			DayOfWeekField = ((ServerDataSet.tasksRow)table.Rows[0]).daysofweek;
			daysTill.IsEnabled = true;
			dateTill.IsEnabled = true;
			dateFrom.IsEnabled = true;
			timeTill.IsEnabled = true;
			timeFrom.IsEnabled = true;
			duration.IsEnabled = false;
			cboxAllowDup.Visibility = System.Windows.Visibility.Hidden;

			switch ((TaskCommands)intTypeRepresentation)
			{
				case TaskCommands.CmdProgram:
					categoryUI.SelectedIndex = (int)_category.taskProgram;
					duration.IsEnabled = true;
					cboxAllowDup.Visibility = System.Windows.Visibility.Visible;
					break;
				case TaskCommands.CmdSubtitles:
					categoryUI.SelectedIndex = (int)_category.taskSubtitle;
					break;
                case TaskCommands.CmdTouchDefaultProgram:
				case TaskCommands.CmdDefaultProgram:
					categoryUI.SelectedIndex = (int)_category.taskDefaultSchedule;
					daysTill.IsEnabled = false;
					dateTill.IsEnabled = false;
					dateFrom.IsEnabled = false;
					timeTill.IsEnabled = false;
					timeFrom.IsEnabled = false;
					duration.IsEnabled = true;
					break;
			}


            projectFileNameStr.Text = ((ServerDataSet.tasksRow)table.Rows[0]).name;
        }

		/// <summary>
		/// 시간대 중복을 허용할지
		/// </summary>
		public bool AllowDuplication
		{
			get
			{
				if ((_category)categoryUI.SelectedIndex == _category.taskProgram)
					return cboxAllowDup.IsChecked == true;
				else
					return true;
			}
		}

        public new bool? ShowDialog()
        {
            return base.ShowDialog();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            result = false;
			this.DialogResult = false;

            this.Close();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            try
            {
                if (!daysTill.Text.Equals(""))
                {
                    int days = System.Convert.ToInt32(daysTill.Text, 10);
					dateTill.SelectedDateTime = dateFrom.SelectedDateTime != null ? dateFrom.SelectedDateTime.Value.AddDays(days) : DateTime.Now;
                }
				DateTime SelectedStartDate = dateFrom.SelectedDateTime != null ? dateFrom.SelectedDateTime.Value : DateTime.Now;
				DateTime SelectedEndDate = dateTill.SelectedDateTime != null ? dateTill.SelectedDateTime.Value : DateTime.Now;

                DateTime from = new DateTime(SelectedStartDate.Year,
							SelectedStartDate.Month,
							SelectedStartDate.Day,
                            timeFrom.SelectedHour, timeFrom.SelectedMinute,
                            timeFrom.SelectedSecond);
				DateTime till = new DateTime(SelectedEndDate.Year,
							SelectedEndDate.Month,
							SelectedEndDate.Day,
                            timeTill.SelectedHour, timeTill.SelectedMinute,
                            timeTill.SelectedSecond);
                if (from > till)
                {
                    System.Windows.MessageBox.Show(Properties.Resources.mbWrongTime);
                    return;
                }

                if (!editMode)
                {
					throw new Exception("Do not Suppose to new task in this dialog.");
                }
                else
                {
					if (singlePrj.IsChecked == true)
					{
						/*	hsshin 중복 체크 루틴 제거

                        if (oldTask.type != TaskCommands.CmdDefaultProgram && oldTask.type != TaskCommands.CmdTouchDefaultProgram && false == CheckTimeScope(oldTask.uuid.ToString(), from, till, oldTask.type, oldTask.gid, oldTask.pid))
						{
							System.Windows.MessageBox.Show(Properties.Resources.mbAlreadyAdded, Properties.Resources.titleMessageBox, MessageBoxButton.OK, MessageBoxImage.Stop);
							return;
						}
						*/
						editTask(null, oldTask, from, till, projectDesc.Text);
					}
                }

                result = true;
				this.DialogResult = true;

                this.Close();
            }
            catch (Exception err)
            {
                logger.Error(err + "");
                System.Windows.MessageBox.Show(Properties.Resources.mbCheckLog);
            }
        }

        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            try
            {
                if (singlePrj.IsChecked == true)
                {
                    OpenFileDialog ofd = new OpenFileDialog();
                    ofd.Filter = "DigitalSignage Data files|*.dsd|All|*.*";
                    DialogResult res = ofd.ShowDialog();
                    projectFileName = ofd.FileName;
                    projectFileNameStr.Text = projectFileName;
                }
                else
                {
                    PlayListManager plm = new PlayListManager();
                    plm.SetItems(multiProjects, multiTimings);
                    plm.ShowDialog();
                    multiProjects = plm.PlayListFiles;
                    multiTimings = plm.PlayListTimings;
                }
            }
            catch (Exception err)
            {
                logger.Error(err + "");
            }
        }

		private void btnChangeSubject(object sender, RoutedEventArgs e)
        {
            try
            {
                GroupPicker dlg = new GroupPicker(cfg, false);
				dlg.TargetObject = TargetObject;

                dlg.ShowDialog();
                if (dlg.result != null)
                {
                    TargetObject = dlg.result;
                }
            }
            catch (Exception err)
            {
				logger.Error(err + "");
			}
        }

//         private int uploadProject(string projectName, Object target, Guid uuid)
//         {
//             try
//             {
// 				XmlNode group = null;
//                 XmlNode node = target as XmlNode;
// 				if (node != null && node.LocalName.Equals("player"))
//                 {
// 					group = ServerDataCache.GetInstance.GetGroupByPID(node.Attributes["gid"].Value);
// 					if (group == null) throw new Exception(Properties.Resources.exceptionUnknownGroup);
// 					selectedTarget = group;
//                 }
//                 else
//                     group = node;
// 
//                 // transfer files to source FTP
// 				FTPFunctions ftp = new FTPFunctions(group.Attributes["source"].Value, group.Attributes["path"].Value, group.Attributes["username"].Value, group.Attributes["password"].Value, cfg.FTP_Timeout);
//                 return ftp.Upload(projectName, uuid.ToString());
//             }
//             catch (Exception e)
//             {
//                 logger.Error(e + "");
//             }
//             return -1;
//         }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (!editMode)
            {

                DateTime now = DateTime.Now.AddMinutes(1);
                dateFrom.SelectedDateTime = now;
                timeFrom.SelectedHour = now.Hour;
                timeFrom.SelectedMinute = now.Minute;
                timeFrom.SelectedSecond = 0;

                now = DateTime.Now.AddMinutes(10);
				dateTill.SelectedDateTime = now;
                timeTill.SelectedHour = now.Hour;
                timeTill.SelectedMinute = now.Minute;
                timeTill.SelectedSecond = 0;
            }
        }

        public object TargetObject
        {
            get
            {
                return selectedTarget;
            }
            set
            {
				if (value == null || value as XmlNode != null)
                {
					selectedTarget = null;
                    subjectName.Text = "";
                }
                else
                {
					selectedTarget = value as XmlNode;
					subjectName.Text = selectedTarget.Attributes["name"].Value;
                }
            }
        }

        private void multiPrj_Checked(object sender, RoutedEventArgs e)
        {
            if (editMode) return;
            projectFileNameStr.IsEnabled = false;
            projectFileNameStr.Text = "";
        }

        private void singlePrj_Checked(object sender, RoutedEventArgs e)
        {
            if (editMode) return;
            if (projectFileNameStr != null)
                projectFileNameStr.IsEnabled = true;
        }

        private void editTask(Object target, Task srcTask, DateTime start, DateTime end, String name)
        {
            // filling task structure
            Task newTask = new Task();
			newTask.TaskStart = TimeConverter.ConvertToUTP(start.ToUniversalTime());
            newTask.TaskEnd = TimeConverter.ConvertToUTP(end.ToUniversalTime());
			newTask.state = TaskState.StateEdit;
			newTask.type = /*TaskCommands.CmdProgram*/srcTask.type;
            newTask.duuid = srcTask.duuid;
            newTask.guuid = srcTask.guuid;
			newTask.pid = srcTask.pid;
			newTask.gid = srcTask.gid;
            newTask.description = name;
			newTask.duration = (long)duration.SelectedTime.TotalSeconds;
			
			newTask.show_flag = oldTask.show_flag;
			if (newTask.type == TaskCommands.CmdProgram)
			{

				if (cboxAllowDup.IsChecked == true)
				{
					newTask.show_flag = newTask.show_flag | (int)ShowFlag.Show_Allow_Duplication;
				}
				else
				{
					newTask.show_flag = newTask.show_flag & ~((int)ShowFlag.Show_Allow_Duplication);
				}

			}

/*
#region Touch - 스케줄 업데이트시 시간 스케줄 Duration 시간 설정
            if (oldTask.type == TaskCommands.CmdProgram)//시간 스케줄
            {
                newTask.duration = (long)(end - start).TotalSeconds;
            }
            else
            {
                newTask.duration = (long)duration.SelectedTime.TotalSeconds;
            }
            #endregion
*/
			
            newTask.priority = srcTask.priority;
			newTask.starttimeofday = srcTask.starttimeofday;
			newTask.endtimeofday = srcTask.endtimeofday;
			newTask.daysofweek = DayOfWeekField;
            cfg.ServerTasksList.EditTasks(srcTask, newTask);
            return;
        }

		private bool CheckTimeScope(String uuid, DateTime from, DateTime till, TaskCommands type, String gid, String pid)
		{
			long start = TimeConverter.ConvertToUTP(from.ToUniversalTime()) + 1;
			long end = TimeConverter.ConvertToUTP(till.ToUniversalTime()) - 1;

			Task task = new Task();
			task.uuid = new Guid(uuid);
			task.TaskStart = start;
			task.TaskEnd = end;
			task.type = type;
			task.gid = gid;
			task.pid = pid;

			if ((String.IsNullOrEmpty(pid) || pid == "-1"))
			{
				try
				{
					return cfg.ServerTasksList.CanAddTaskToGroup(task, gid);
				}
				catch
				{
					//	그룹 스케줄인 경우	(중복 허용 여부 bool 추가)
					return cfg.ServerTasksList.CanAddTaskToGroup(uuid, start, end, (long)type, gid);
				}
			}
			else
			{
				try
				{
					return cfg.ServerTasksList.CanAddTaskToPlayer(task, pid);
				}
				catch
				{
					//	Player 스케줄인 경우 (중복 허용 여부 bool 추가)
					return cfg.ServerTasksList.CanAddTaskToPlayer(uuid, start, end, (long)type, pid);
				}
			}
		}

		void tb_GotKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
		{
			try
			{
				System.Windows.Controls.TextBox tb = sender as System.Windows.Controls.TextBox;
				if (tb != null) tb.SelectAll();
				else
				{
					System.Windows.Controls.PasswordBox pb = sender as System.Windows.Controls.PasswordBox;
					pb.SelectAll();
				}

				e.Handled = true;
			}
			catch { }
		}

        #region Properties
        public DateTime Date
        {
            get
            {
				return dateTill.SelectedDateTime != null ? dateTill.SelectedDateTime.Value : DateTime.Now;
            }
            set
            {
				dateTill.SelectedDateTime = value;
				dateFrom.SelectedDateTime = value;
            }
        }

		public int DayOfWeekField
		{
			get
			{
				try
				{
					int dayofweek = 0;
					if (btnSun.IsChecked.Value)
					{
						dayofweek |= (int)TaskApplyDayOfWeek.Day_SUN;
					}
					if (btnMon.IsChecked.Value)
					{
						dayofweek |= (int)TaskApplyDayOfWeek.Day_MON;
					}
					if (btnTue.IsChecked.Value)
					{
						dayofweek |= (int)TaskApplyDayOfWeek.Day_TUE;
					}
					if (btnWed.IsChecked.Value)
					{
						dayofweek |= (int)TaskApplyDayOfWeek.Day_WED;
					}
					if (btnThu.IsChecked.Value)
					{
						dayofweek |= (int)TaskApplyDayOfWeek.Day_THU;
					}
					if (btnFri.IsChecked.Value)
					{
						dayofweek |= (int)TaskApplyDayOfWeek.Day_FRI;
					}
					if (btnSat.IsChecked.Value)
					{
						dayofweek |= (int)TaskApplyDayOfWeek.Day_SAT;
					}

					return dayofweek;
				}
				catch
				{
					//	전체 체크
					return 127;
				}
			}
			set
			{
				int dayofweek = value;
				btnSun.IsChecked = 0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_SUN)
					?  true : false;
				btnMon.IsChecked = 0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_MON)
					? true : false;
				btnTue.IsChecked = 0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_TUE)
					? true : false;
				btnWed.IsChecked = 0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_WED)
					? true : false;
				btnThu.IsChecked = 0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_THU)
					? true : false;
				btnFri.IsChecked = 0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_FRI)
					? true : false;
				btnSat.IsChecked = 0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_SAT)
					? true : false;
			}
		}
        #endregion
    }
}
