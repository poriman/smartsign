﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows;

namespace DigitalSignage.Scheduler
{
	class GridViewColumnHeaderEx : GridViewColumnHeader
	{
		static GridViewColumnHeaderEx()
		{
            DefaultStyleKeyProperty.OverrideMetadata(typeof(GridViewColumnHeaderEx), new FrameworkPropertyMetadata(typeof(GridViewColumnHeaderEx)));
		}

		/// <summary>
		/// 정렬 열거자
		/// </summary>
		public enum ColumnSort
		{
			/// <summary>
			/// 정렬 없음
			/// </summary>
			Sort_None,
			/// <summary>
			/// Descending
			/// </summary>
			Sort_Descending,
			/// <summary>
			/// Ascending
			/// </summary>
			Sort_Ascending
		};

		private ColumnSort curr_sort = ColumnSort.Sort_None;

		#region IsSortable
		public static readonly DependencyProperty IsSortableProperty =
			DependencyProperty.Register("IsSortable", typeof(bool), typeof(GridViewColumnHeaderEx),
				new FrameworkPropertyMetadata(true));

		public bool IsSortable
		{
			get { return (bool)GetValue(IsSortableProperty); }
			set { SetValue(IsSortableProperty, value); }
		}
		#endregion

		#region IsAscending
		public static readonly DependencyProperty IsAscendingProperty =
			DependencyProperty.Register("IsAscending", typeof(bool), typeof(GridViewColumnHeaderEx),
				new FrameworkPropertyMetadata(false));

		public bool IsAscending
		{
			get { return (bool)GetValue(IsAscendingProperty); }
		}
		#endregion

		#region IsDescending
		public static readonly DependencyProperty IsDescendingProperty =
			DependencyProperty.Register("IsDescending", typeof(bool), typeof(GridViewColumnHeaderEx),
				new FrameworkPropertyMetadata(false));

		public bool IsDescending
		{
			get { return (bool)GetValue(IsDescendingProperty); }
		}
		#endregion

		#region IsSorting
		public static readonly DependencyProperty IsSortingProperty =
			DependencyProperty.Register("IsSorting", typeof(bool), typeof(GridViewColumnHeaderEx),
				new FrameworkPropertyMetadata(false));


		public bool IsSorting
		{
			get { return (bool)GetValue(IsSortingProperty); }
		}
		#endregion

		#region CanFilter
		public static readonly DependencyProperty CanFilterProperty =
			DependencyProperty.Register("CanFilter", typeof(bool), typeof(GridViewColumnHeaderEx),
				new FrameworkPropertyMetadata(false));

		public bool CanFilter
		{
			get { return (bool)GetValue(CanFilterProperty); }
			set { SetValue(CanFilterProperty, value); }
		}
		#endregion

		#region HasFilter
		public static readonly DependencyProperty HasFilterProperty =
			DependencyProperty.Register("HasFilter", typeof(bool), typeof(GridViewColumnHeaderEx),
				new FrameworkPropertyMetadata(false));

		public bool HasFilter
		{
			get { return (bool)GetValue(HasFilterProperty); }
			set { SetValue(HasFilterProperty, value); }
		}
		#endregion


		#region Filter
		public static readonly DependencyProperty FilterProperty =
			DependencyProperty.Register("Filter", typeof(object), typeof(GridViewColumnHeaderEx),
				new FrameworkPropertyMetadata(null));

		public object Filter
		{
			get { return (object)GetValue(FilterProperty); }
			set {
				if (this.CanFilter)
				{
					SetValue(FilterProperty, value);

					if (value is String)
						HasFilter = !String.IsNullOrEmpty((String)value);
					else
						HasFilter = Filter != null; 

				}
			}
		}
		#endregion


		/// <summary>
		/// 정렬을 수행한다.
		/// </summary>
		public void Sort()
		{
			if (this.IsSortable)
			{
				switch (curr_sort)
				{
					case ColumnSort.Sort_None:
						SetValue(IsSortingProperty, true);
						SetValue(IsAscendingProperty, true);
						SetValue(IsDescendingProperty, false);
						curr_sort = ColumnSort.Sort_Ascending;
						break;
					case ColumnSort.Sort_Ascending:
						SetValue(IsSortingProperty, true);
						SetValue(IsAscendingProperty, false);
						SetValue(IsDescendingProperty, true);
						curr_sort = ColumnSort.Sort_Descending;

						break;
					case ColumnSort.Sort_Descending:
						SetValue(IsSortingProperty, true);
						SetValue(IsAscendingProperty, true);
						SetValue(IsDescendingProperty, false);
						curr_sort = ColumnSort.Sort_Ascending;
						break;
				}
			}
		}

		/// <summary>
		/// 정렬을 해제한다.
		/// </summary>
		public void UnSort()
		{
			if (this.IsSortable)
			{
				SetValue(IsSortingProperty, false);
				SetValue(IsAscendingProperty, false);
				SetValue(IsDescendingProperty, false);
				curr_sort = ColumnSort.Sort_None;
			}
		}

	}
}
