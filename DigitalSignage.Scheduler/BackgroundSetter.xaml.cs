﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Media.Effects;

using NLog;
using WPFDesigner;

namespace DigitalSignage.Scheduler
{
    /// <summary>
    /// Interaction logic for BackgroundSetter.xaml
    /// </summary>
    public partial class BackgroundSetter : Window
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private bool isApply = false;
        /// <summary>
        /// An image brush used for painting image backgrounds
        /// </summary>
        ImageBrush imageBrush;

        Brush loadedBrush, currentBrush;

        /// <summary>
        /// Dialog used for opening images
        /// </summary>
        System.Windows.Forms.OpenFileDialog OpenFileDialog;
        Label clabel;
        int count = 0;
        /// <summary>
        /// A gradient brush used for painting gradients
        /// </summary>
        LinearGradientBrush lgb;

        /// <summary>
        /// A radial gradient brush
        /// </summary>
        RadialGradientBrush rgb;

        /// <summary>
        /// A currently active brush
        /// </summary>
        Brush activeBrush;

        SolidColorBrush scb;

        /// <summary>
        /// A dialog for color choosing 
        /// </summary>
        System.Windows.Forms.ColorDialog cd;

        /// <summary>
        /// Image source used for setting an image as a background
        /// </summary>
        BitmapImage imageSource;

        int labelindex;

        /// <summary>
        /// An outer glow effect used for indicating what color is currently selected
        /// </summary>
        OuterGlowBitmapEffect selectionEffect;

        /// <summary>
        /// Public constructor
        /// </summary>
        public BackgroundSetter()
        {
            InitializeComponent();
            labelindex = 0;

            #region Initializing comboboxes
            TileModeCB.Items.Add(TileMode.None);
            TileModeCB.Items.Add(TileMode.Tile);
            TileModeCB.Items.Add(TileMode.FlipX);
            TileModeCB.Items.Add(TileMode.FlipXY);
            TileModeCB.Items.Add(TileMode.FlipY);
            TileModeCB.SelectedItem = TileModeCB.Items[0];
            TileModeCB.SelectionChanged += new SelectionChangedEventHandler(TileModeCB_SelectionChanged);

            HAlignCB.Items.Add(AlignmentX.Center);
            HAlignCB.Items.Add(AlignmentX.Left);
            HAlignCB.Items.Add(AlignmentX.Right);
            HAlignCB.SelectedItem = HAlignCB.Items[0];
            HAlignCB.SelectionChanged += new SelectionChangedEventHandler(HAlignCB_SelectionChanged);

            VAlignCB.Items.Add(AlignmentY.Center);
            VAlignCB.Items.Add(AlignmentY.Top);
            VAlignCB.Items.Add(AlignmentY.Bottom);
            VAlignCB.SelectedItem = VAlignCB.Items[0];
            VAlignCB.SelectionChanged += new SelectionChangedEventHandler(VAlignCB_SelectionChanged);

            StretchModeCB.Items.Add(Stretch.None);
            StretchModeCB.Items.Add(Stretch.Fill);
            StretchModeCB.Items.Add(Stretch.Uniform);
            StretchModeCB.Items.Add(Stretch.UniformToFill);
            StretchModeCB.SelectedItem = StretchModeCB.Items[0];
            StretchModeCB.SelectionChanged += new SelectionChangedEventHandler(StretchModeCB_SelectionChanged);
            #endregion
            //WindowStyle = WindowStyle.None;
            //AllowsTransparency = true;
        }

        public bool ISApply
        {
            set { isApply = value; }
            get { return isApply; }
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            clabel = new Label();

            //Initializing a dialog that is used for opening images
            OpenFileDialog = new System.Windows.Forms.OpenFileDialog();
            OpenFileDialog.Filter = "All supported|*.jpg;*.bmp;*.gif;*.png|JPEG files|*.jpg|BMP files| *.bmp|GIF files|*.gif|PNG files|*.png";
            OpenFileDialog.Title = "Add an image";
            OpenFileDialog.FileOk += new System.ComponentModel.CancelEventHandler(OpenFileDialog_FileOk);

            //Initializing a bitmap effect that highlights a currently editing color
            selectionEffect = new OuterGlowBitmapEffect();
            selectionEffect.GlowColor = Color.FromArgb(255, 255, 255, 255);
            selectionEffect.GlowSize = 3;

            //Initializing color dialog
            cd = new System.Windows.Forms.ColorDialog();
            cd.AllowFullOpen = true;
            cd.AnyColor = true;

            opacitySlider.AutoToolTipPlacement = System.Windows.Controls.Primitives.AutoToolTipPlacement.TopLeft;
            opacitySlider.AutoToolTipPrecision = 2;

            #region
            GeometryDrawing gd = new GeometryDrawing();

            RectangleGeometry rg = new RectangleGeometry();
            rg.Rect = new Rect(new Size(4, 4));
            gd.Geometry = rg;
            gd.Brush = Brushes.White;
            Pen p = new Pen();
            p.Brush = Brushes.Black;
            p.Thickness = 1;
            gd.Pen = p;

            DrawingBrush db = new DrawingBrush();
            db.TileMode = TileMode.Tile;
            db.ViewportUnits = BrushMappingMode.Absolute;
            db.Viewport = new Rect(0, 0, 4, 4);
            db.Drawing = gd;
            backgroundRect.Fill = db;

            stopsPanelBorder.Background = db;
            #endregion

            //Signing color labels event handlers
            solidLabel.PreviewMouseDoubleClick += new MouseButtonEventHandler(colorLabel_PreviewMouseDoubleClick);
            solidLabel.MouseDown += new MouseButtonEventHandler(solidLabel_MouseDown);
        }

        void solidLabel_MouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                labelindex = -1;
                solidColorRB.IsChecked = true;
                opacitySlider.Value = ((SolidColorBrush)solidLabel.Background).Opacity;
            }
            catch { }
        }

        /// <summary>
        /// Opens a color dialog that allows to select a color of currently selected label
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void colorLabel_PreviewMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            labelindex = -1;
            cd.Color = ColorFromBrush((SolidColorBrush)((Label)sender).Background);
            int[] arr = DesignerWindow.GetCustomColors();
            if (arr != null)
                cd.CustomColors = arr;
            cd.ShowDialog();
            ((Label)sender).Background = BrushFromColor(cd.Color);
            currentBrush = solidLabel.Background;
            SetCustomColors(cd.CustomColors);
            UpdateBackground();
            e.Handled = true;
        }

        void BackgroundSetter_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {

        }

        /// <summary>
        /// Returns a System.Drawing.Color from a specified SolidColorBrush
        /// </summary>
        /// <param name="scb"></param>
        /// <returns></returns>
        public static System.Drawing.Color ColorFromBrush(SolidColorBrush scb)
        {
            return System.Drawing.Color.FromArgb(255, scb.Color.R, scb.Color.G, scb.Color.B);
        }

        /// <summary>
        /// Returns a SolidColorBrush based on a specified System.Drawing.Color variable
        /// </summary>
        /// <param name="color">System.Drawing.Color variable</param>
        /// <returns>SolidColorBrush created basing on System.Drawing.Color provided</returns>
        public static SolidColorBrush BrushFromColor(System.Drawing.Color color)
        {
            SolidColorBrush scb = new SolidColorBrush(Color.FromArgb(color.A, color.R, color.G, color.B));
            return scb;
        }

        /// <summary>
        /// Returns a SolidColorBrush based on a specified System.Windows.Media.Color variable
        /// </summary>
        /// <param name="color">System.Windows.Media.Color variable</param>
        /// <returns>SolidColorBrush created basing on System.Windows.Media.Color provided</returns>
        public static SolidColorBrush BrushFromColor(System.Windows.Media.Color color)
        {
            SolidColorBrush scb = new SolidColorBrush(Color.FromArgb(color.A, color.R, color.G, color.B));
            scb.Opacity = ((double)color.A) / 255;
            return scb;
        }

        /// <summary>
        /// Updates a color background
        /// </summary>
        void UpdateBackground()
        {
            try
            {
                if (gradientRB.IsChecked.Value)
                {
                    if (gradientStyle.SelectedIndex == 0)
                    {
                        activeBrush = new LinearGradientBrush();
                    }
                    else
                    {
                        activeBrush = new RadialGradientBrush();
                    }
                }
                else if (solidColorRB.IsChecked.Value)
                {
                    activeBrush = new SolidColorBrush();
                }
                switch (activeBrush.GetType().Name)
                {
                    case "LinearGradientBrush":
                        {
                            lgb = new LinearGradientBrush();
                            if (gradientDirection.SelectedIndex == 0)
                            {
                                lgb.StartPoint = new Point(0, 0);
                                lgb.EndPoint = new Point(1, 0);
                            }
                            else
                            {
                                lgb.StartPoint = new Point(0, 0);
                                lgb.EndPoint = new Point(0, 1);
                            }

                            double currentstop = 0;

                            foreach (Label l in stopsPanel.Children)
                            {
                                lgb.GradientStops.Add(new GradientStop(ColorFromBrush(l.Background), currentstop));

                                if (stopsPanel.Children.Count > 2)
                                    currentstop += 1.0 / (stopsPanel.Children.Count - 1);
                                else
                                    currentstop += 1.0 / stopsPanel.Children.Count;
                            }
                            //                                 foreach (Label l in stopsPanel.Children)
                            //                                 {
                            //                                     lgb.GradientStops.Add(new GradientStop(ColorFromBrush(l.Background), currentstop));
                            //                                     currentstop += 1.0 / stopsPanel.Children.Count;
                            //                                 }
                            if (lgb.GradientStops.Count > 1)
                                lgb.GradientStops[stopsPanel.Children.Count - 1].Offset = 1;

                            PreviewBackground.Background = lgb;
                            gradientLabel.Background = lgb;
                            currentBrush = lgb;
                            activeBrush = lgb;
                            break;
                        }
                    case "RadialGradientBrush":
                        {
                            rgb = new RadialGradientBrush();
                            double currentstop = 0;

                            foreach (Label l in stopsPanel.Children)
                            {
                                rgb.GradientStops.Add(new GradientStop(ColorFromBrush(l.Background), currentstop));
                                currentstop += 1.0 / stopsPanel.Children.Count;
                            }
                            activeBrush = rgb;
                            currentBrush = rgb;
                            gradientLabel.Background = rgb;
                            PreviewBackground.Background = rgb;
                            break;
                        }

                    case "SolidColorBrush":
                        {
                            scb = (SolidColorBrush)solidLabel.Background;
                            PreviewBackground.Background = scb;
                            currentBrush = scb;
                            activeBrush = scb;
                            opacitySlider.Value = scb.Opacity;
                            break;
                        }
                    default: break;
                }
            }
            catch { }

        }

        private Color ColorFromBrush(Brush brush)
        {
            return Color.FromArgb(Convert.ToByte(((double)((SolidColorBrush)brush).Color.A) * brush.Opacity), ((SolidColorBrush)brush).Color.R, ((SolidColorBrush)brush).Color.G, ((SolidColorBrush)brush).Color.B);
        }

        /// <summary>
        /// Loads the specified brush into the BackgroundSetter for editing and shows the editor
        /// </summary>
        /// <param name="brush">Brush to load</param>
        public void Load(Brush brush)
        {
            try
            {
                isApply = false;
                brush.GetType();
            }
            catch
            {
                brush = new ImageBrush();
            }

            switch (brush.GetType().Name)
            {
                case "SolidColorBrush":
                    {
                        tabControl.SelectedIndex = 1;
                        loadedBrush = brush;
                        solidColorRB.IsChecked = true;
                        solidLabel.Background = brush;
                        activeBrush = brush;
                        currentBrush = solidLabel.Background;
                        opacitySlider.Value = brush.Opacity;
                        UpdateBackground();
                        break;
                    }
                case "LinearGradientBrush":
                    {
                        tabControl.SelectedIndex = 1;
                        loadedBrush = brush;
                        gradientRB.IsChecked = true;
                        currentBrush = new LinearGradientBrush();
                        opacitySlider.Value = brush.Opacity;
                        foreach (GradientStop gs in ((LinearGradientBrush)brush).GradientStops)
                        {
                            clabel = new Label();
                            clabel.Name = "gs" + count.ToString();
                            clabel.Height = 17;
                            clabel.Width = stopsPanel.Width / ((LinearGradientBrush)brush).GradientStops.Count;
                            clabel.Background = BrushFromColor(gs.Color);
                            clabel.MouseDown += new MouseButtonEventHandler(clabel_MouseDown);
                            clabel.MouseDoubleClick += new MouseButtonEventHandler(clabel_MouseDoubleClick);
                            stopsPanel.Children.Add(clabel);
                            ((LinearGradientBrush)currentBrush).GradientStops.Add(gs);
                            count++;
                        }
                        if (((LinearGradientBrush)brush).StartPoint.X == ((LinearGradientBrush)brush).EndPoint.X)
                        {
                            gradientDirection.SelectedIndex = 1;
                        }
                        else gradientDirection.SelectedIndex = 0;
                        gradientStyle.SelectedIndex = 0;
                        currentBrush = brush;
                        gradientLabel.Background = brush;
                        activeBrush = brush;
                        solidLabel.Background = new SolidColorBrush(Color.FromArgb(255, 255, 255, 255));
                        UpdateBackground();
                        break;
                    }
                case "RadialGradientBrush":
                    {
                        tabControl.SelectedIndex = 1;
                        loadedBrush = brush;
                        gradientRB.IsChecked = true;
                        opacitySlider.Value = brush.Opacity;
                        foreach (GradientStop gs in ((RadialGradientBrush)brush).GradientStops)
                        {
                            clabel = new Label();
                            clabel.Name = "gs" + count.ToString();
                            clabel.Height = 17;
                            clabel.Width = stopsPanel.Width / ((RadialGradientBrush)brush).GradientStops.Count;
                            clabel.Background = BrushFromColor(gs.Color);
                            clabel.MouseDown += new MouseButtonEventHandler(clabel_MouseDown);
                            clabel.MouseDoubleClick += new MouseButtonEventHandler(clabel_MouseDoubleClick);
                            stopsPanel.Children.Add(clabel);
                            ((LinearGradientBrush)currentBrush).GradientStops.Add(gs);
                            count++;
                        }
                        gradientStyle.SelectedIndex = 1;
                        currentBrush = brush;
                        gradientLabel.Background = brush;
                        activeBrush = brush;
                        solidLabel.Background = new SolidColorBrush(Color.FromArgb(255, 255, 255, 255));
                        UpdateBackground();
                        break;
                    }
                case "ImageBrush":
                    {
                        tabControl.SelectedIndex = 0;
                        imageBrush = (ImageBrush)brush;
                        loadedBrush = brush;
                        PreviewBackground.Background = (ImageBrush)imageBrush;
                        tabControl.SelectedIndex = 0;
                        imageOpacity.Value = imageBrush.Opacity;
                        TileModeCB.SelectedItem = imageBrush.TileMode;
                        StretchModeCB.SelectedItem = imageBrush.Stretch;
                        VAlignCB.SelectedItem = imageBrush.AlignmentY;
                        HAlignCB.SelectedItem = imageBrush.AlignmentX;

                        solidLabel.Background = new SolidColorBrush(Color.FromArgb(255, 255, 255, 255));
                        activeBrush = brush;
                        break;
                    }
                default:
                    {
                        currentBrush = loadedBrush = activeBrush = new SolidColorBrush(Color.FromArgb(255, 255, 255, 255));
                        break;
                    }
            }
            this.ShowDialog();
        }

        void clabel_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            try
            {
                labelindex = stopsPanel.Children.IndexOf((Label)sender);
                cd.Color = ColorFromBrush((SolidColorBrush)((Label)sender).Background);
                int[] arr = DesignerWindow.GetCustomColors();
                if (arr != null)
                    cd.CustomColors = arr;
                cd.ShowDialog();
                ((Label)sender).Background = BrushFromColor(cd.Color);
                SetCustomColors(cd.CustomColors);
                currentBrush = gradientLabel.Background;
                UpdateBackground();
            }
            catch { }
        }

        void SetCustomColors(int[] colors)
        {
            try
            {
                string strcustoncolors = "";
                for (int i = 0; i < colors.Length; i++)
                {
                    strcustoncolors += colors[i].ToString();
                    if ((i + 1) < colors.Length)
                        strcustoncolors += ",";
                }
                DesignerWindow.SetAppSeting("CustomColors", strcustoncolors);
            }
            catch (Exception ex)
            {
                string errmsg = ex.Message.ToString();
            }
        }

        void clabel_MouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                solidColorRB.IsChecked = false;
                gradientRB.IsChecked = true;

                labelindex = stopsPanel.Children.IndexOf((Label)sender);
                opacitySlider.Value = ((SolidColorBrush)((Label)sender).Background).Opacity;
            }
            catch { }
        }

        /// <summary>
        /// Applies the chosen image as a background
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void OpenFileDialog_FileOk(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                SetImageBrush();
            }
            catch
            {
            }
        }

        /// <summary>
        /// Opens a dialog the allows to choose an image as background
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OpenImageB_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog.ShowDialog();
        }

        /// <summary>
        /// Applies an image background
        /// </summary>
        void SetImageBrush()
        {
            IMediaFilesManager manager = ProjectManager.GetCurrentProjectManager().Pane as IMediaFilesManager;
            string s = manager.AddNewFileWithModifiedName(OpenFileDialog.FileName);
            imageSource = new BitmapImage();
            imageSource.BeginInit();
            imageSource.UriSource = new UriBuilder(manager.GetFullFilePathByName(System.IO.Path.GetFileName(s))).Uri;
            imageSource.EndInit();
            imageBrush = new ImageBrush();
            imageBrush.Stretch = Stretch.None;
            imageBrush.ImageSource = imageSource;
            PreviewBackground.Background = imageBrush;
            activeBrush = imageBrush;
        }

        /// <summary>
        /// Setting a TileMode of background
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TileModeCB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (PreviewBackground.Background.GetType().Name != "ImageBrush") return;

                PreviewBackground.Background.SetValue(TileBrush.TileModeProperty, (TileMode)TileModeCB.SelectedValue);
                if ((TileMode)PreviewBackground.Background.GetValue(TileBrush.TileModeProperty) == TileMode.None)
                {
                    PreviewBackground.Background.SetValue(TileBrush.ViewportProperty, new Rect(new Size(1, 1)));
                    PreviewBackground.Background.SetValue(TileBrush.ViewportUnitsProperty, BrushMappingMode.RelativeToBoundingBox);
                }
                else
                {
                    PreviewBackground.Background.SetValue(TileBrush.ViewportProperty, new Rect(new Size(((ImageBrush)PreviewBackground.Background).ImageSource.Width, ((ImageBrush)PreviewBackground.Background).ImageSource.Height)));
                    PreviewBackground.Background.SetValue(TileBrush.ViewportUnitsProperty, BrushMappingMode.Absolute);
                }
            }
            catch
            {

            }

        }

        /// <summary>
        /// Setting a horizontal alignment of background
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HAlignCB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (PreviewBackground == null || PreviewBackground.Background.GetType().Name != "ImageBrush") return;
                PreviewBackground.Background.SetValue(ImageBrush.AlignmentXProperty, (AlignmentX)HAlignCB.SelectedValue);
            }
            catch
            {

            }
        }

        /// <summary>
        /// Setting a vertical alignment of background
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void VAlignCB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (PreviewBackground == null || PreviewBackground.Background.GetType().Name != "ImageBrush") return;
                PreviewBackground.Background.SetValue(ImageBrush.AlignmentYProperty, (AlignmentY)VAlignCB.SelectedValue);
            }
            catch
            {

            }
        }

        /// <summary>
        /// Setting a stretch property of background
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void StretchModeCB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (PreviewBackground == null || PreviewBackground.Background.GetType().Name != "ImageBrush") return;

                PreviewBackground.Background.SetValue(ImageBrush.StretchProperty, (Stretch)StretchModeCB.SelectedValue);
            }
            catch
            {

            }
        }

        /// <summary>
        /// Returns a modified background brush
        /// </summary>
        /// <returns>Modified background</returns>
        public Brush GetContent()
        {
            return loadedBrush;
        }

        /// <summary>
        /// Changes an opacity of curretnly selected color
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void opacitySlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            try
            {
                if (currentBrush != null)
                {
                    if (currentBrush.GetType().Name == "SolidColorBrush")
                    {
                        try
                        {
                            currentBrush.Opacity = e.NewValue;
                            solidLabel.Background = currentBrush;
                        }
                        catch { }
                    }
                    else if (stopsPanel.Children.Count > 0)
                    {
                        try
                        {
                            if (labelindex == -1) labelindex = 0;
                            ((SolidColorBrush)((Label)stopsPanel.Children[labelindex]).Background).Opacity = e.NewValue;
                        }
                        catch
                        { }
                    }
                    UpdateBackground();
                }
            }
            catch { }
        }

        /// <summary>
        /// Changes the direction of gradient
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void gradientDirection_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (gradientRB != null && !gradientRB.IsChecked.Value) return;
            if (currentBrush != null)
                UpdateBackground();
        }

        /// <summary>
        /// Changes the opacity of image background
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void imageOpacity_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (PreviewBackground == null || PreviewBackground.Background.GetType().Name != "ImageBrush") return;

            PreviewBackground.Background.Opacity = e.NewValue;
        }

        /// <summary>
        /// Closes the BackgroundSetter
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void closeButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Sets both of colors to transparent
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void setTransparent_Click(object sender, RoutedEventArgs e)
        {
            if (activeBrush.GetType() == typeof(SolidColorBrush))
            {
                try
                {
                    solidLabel.Background.Opacity = 0;
                }
                catch { }
            }
            else
                foreach (Label l in stopsPanel.Children)
                {
                    l.Background.Opacity = 0;
                }

            UpdateBackground();
        }

        private void gradientStyle_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (gradientRB != null && !gradientRB.IsChecked.Value) return;
            if (activeBrush != null)
            {
                if (gradientStyle.SelectedIndex == 1)
                {
                    activeBrush = new RadialGradientBrush();
                }
                else activeBrush = new LinearGradientBrush();
                currentBrush = gradientLabel.Background;
                UpdateBackground();
            }
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            loadedBrush = activeBrush;
            ISApply = true;
            this.Close();
        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void button3_Click(object sender, RoutedEventArgs e)
        {
            loadedBrush = activeBrush;
            ISApply = true;
        }

        private void solidColorRB_Checked(object sender, RoutedEventArgs e)
        {
            gradientRB.IsChecked = false;
            activeBrush = new SolidColorBrush();
            currentBrush = new SolidColorBrush();
            UpdateBackground();
        }

        private void gradientRB_Checked(object sender, RoutedEventArgs e)
        {
            solidColorRB.IsChecked = false;
            activeBrush = new LinearGradientBrush();
            currentBrush = new LinearGradientBrush();
            UpdateBackground();
        }

        private void addStop_Click(object sender, RoutedEventArgs e)
        {
            solidColorRB.IsChecked = false;
            gradientRB.IsChecked = true;

            if (stopsPanel.Children.Count < 20)
            {
                count++;
                clabel = new Label();
                clabel.Background = new SolidColorBrush(Color.FromArgb(255, 255, 255, 255));
                clabel.Name = "gs" + count.ToString();
                stopsPanel.Children.Add(clabel);
                clabel.MouseDown += new MouseButtonEventHandler(clabel_MouseDown);
                clabel.MouseDoubleClick += new MouseButtonEventHandler(clabel_MouseDoubleClick);
                foreach (Label l in stopsPanel.Children)
                {
                    l.Width = stopsPanel.Width / stopsPanel.Children.Count;
                }
                currentBrush = new LinearGradientBrush();
                UpdateBackground();
            }
        }

        private void removeStop_Click(object sender, RoutedEventArgs e)
        {
            solidColorRB.IsChecked = false;
            gradientRB.IsChecked = true;

            if (stopsPanel.Children.Count > 0)
            {
                count--;
                stopsPanel.Children.RemoveAt(stopsPanel.Children.Count - 1);
                foreach (Label l in stopsPanel.Children)
                {
                    l.Width = stopsPanel.Width / stopsPanel.Children.Count;
                }
                currentBrush = gradientLabel.Background;
                UpdateBackground();
            }
        }

        private void button4_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}