﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Windows.Interop;

using System.Net;
using System.Net.Sockets;
using System.Threading;

using NLog;

using DigitalSignage.DataBase;

namespace DigitalSignage.Scheduler
{
    /// <summary>
    /// Interaction logic for NewDlgItem.xaml
    /// </summary>
    public partial class RemoteMonitorDlg : Window
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        public bool result = false;
        private Config cfg = null;
        private String client;
        private Thread udpThread;
        private int udpPort = 1891;
        private int localUdpPort = 1892;
        private bool process = true;

        public RemoteMonitorDlg(Config config, String client)
        {
            InitializeComponent();
            cfg = config;
            this.client = client;
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //Starting the UDP Server thread.
            process = true;
            udpThread = new Thread(new ThreadStart(Start));
            udpThread.Priority = ThreadPriority.BelowNormal;
            udpThread.Start();
        }

        public void Start()
        {
            IPEndPoint point = new IPEndPoint(IPAddress.Parse(client), udpPort);
            IPEndPoint localPoint = new IPEndPoint(0, localUdpPort);
            string query = "DigitalSignageScreenRequest";
            Socket s;
            while (process)
            {
                try
                {
                    Thread.Sleep(5000);
                    // Create a UDP socket
                    s = new Socket(point.Address.AddressFamily, SocketType.Dgram, ProtocolType.Udp);
                    s.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReceiveTimeout, 4000);
                    s.Bind(localPoint);
                    s.SendTo(System.Text.Encoding.ASCII.GetBytes(query.ToCharArray()), point);

                    Byte[] received = new Byte[256];
                    IPEndPoint tmpIpEndPoint = new IPEndPoint(0, localUdpPort);
                    EndPoint remoteEP = (tmpIpEndPoint);
                    MemoryStream ms = new MemoryStream();
                    int recv = 0;
                    while ((recv = s.ReceiveFrom(received, ref remoteEP)) > 0)
                    {
                        ms.Write(received, 0, recv);
                    }
                    System.Drawing.Bitmap png = new System.Drawing.Bitmap( ms );
                    remoteUI.Source = Imaging.CreateBitmapSourceFromHBitmap(png.GetHbitmap(), IntPtr.Zero,
                        new Int32Rect(0, 0, png.Width, png.Height), BitmapSizeOptions.FromEmptyOptions());
                }
                catch
                {
                }
            }
        }
    }
}
