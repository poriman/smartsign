﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

using System.Net;
using System.Net.Sockets;
using System.Threading;

using NLog;

using DigitalSignage.Common;

namespace DigitalSignage.Scheduler
{
    /// <summary>
    /// Interaction logic for NewDlgItem.xaml
    /// </summary>
    public partial class NewCmdTaskDlg : Window
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        public Boolean result = false;
        private Config cfg = null;

        /// <summary>
        /// 수정 모드 여부
        /// </summary>
        private bool editMode = false;

		/// <summary>
		/// 적용할 날짜
		/// </summary>
		private Nullable<DateTime> _dateOfSelected = null;

		/// <summary>
		/// 적용할 날짜
		/// </summary>
		public DateTime ApplyDate
		{
			set { _dateOfSelected = value; }
		}

		/// <summary>
		/// 생성자
		/// </summary>
		/// <param name="config"></param>
		/// <param name="category"></param>
		public NewCmdTaskDlg(Config config, int category)
        {
            InitializeComponent();
            cfg = config;
			uiTaskMaker.TaskName = Properties.Resources.labelDefaultTaskName;
			uiTaskMaker.ProgramType = category;
			uiTaskMaker.ParentWindow = this;

		}

		/// <summary>
		/// 스케줄 타입을 항목 코드로 변환
		/// </summary>
		/// <param name="type"></param>
		/// <returns></returns>
		public static int GetCategoryFromTaskType(TaskCommands type)
		{
			switch(type)
			{
				case TaskCommands.CmdProgram:
					return 0;
				case TaskCommands.CmdDefaultProgram:
					return 1;
				case TaskCommands.CmdSubtitles:
					return 2;
				case TaskCommands.CmdControlSchedule:
					return 3;
			}

			return 0;
		}

		/// <summary>
		/// 타깃 객체 (그룹, 플레이어)
		/// </summary>
		public object TargetObject
		{
			get
			{
				return uiTaskMaker.TargetObject;
			}
			set
			{
				uiTaskMaker.TargetObject = value;
			}
		}

		Task _task = null;
		public Task WillEditTask
		{
			get
			{
				return _task;
			}
			set
			{
				editMode = (_task = value) != null;
			}
		}

        #region Event handlers
		private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                result = false;
				this.DialogResult = false;
                this.Close();
            }
            catch (Exception err)
            {
                logger.Error(err + "");
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                uiTaskMaker.Cfg = Config.GetConfig;

				switch (uiTaskMaker.ProgramType)
				{
					case 0 :
						cmdTask.Title = !editMode ? Properties.Resources.menuitemAddTaskProgram : Properties.Resources.menuitemEditTask;
						this.Height = 450;
						break;
					case 1:
						cmdTask.Title = !editMode ? Properties.Resources.menuitemAddSubtitle : Properties.Resources.menuitemEditTask;
						this.Height = 450;
						break;
					case 2:
						cmdTask.Title = !editMode ? Properties.Resources.menuitemAddDefaultSchedule : Properties.Resources.menuitemEditTask;
						this.Height = 400;
						break;
					case 3:
						cmdTask.Title = !editMode ? Properties.Resources.menuitemAddControl : Properties.Resources.menuitemEditTask;
						this.Height = 500;
						break;
				}
                uiTaskMaker.EditMode = editMode;
                if (!editMode)
                {
					DateTime time = new DateTime(_dateOfSelected.Value.Year, 
						_dateOfSelected.Value.Month,
						_dateOfSelected.Value.Day,
						DateTime.Now.Hour,0 ,0);

					time = time.AddHours(1);
					uiTaskMaker.StartDateUI.SelectedDateTime = time;
					uiTaskMaker.StartTimeUI.SelectedHour = time.Hour;
					uiTaskMaker.StartTimeUI.SelectedMinute = 0;
					uiTaskMaker.StartTimeUI.SelectedSecond = 0;

					time = time.AddHours(1);

					uiTaskMaker.FinDateUI.SelectedDateTime = time;
					uiTaskMaker.FinTimeUI.SelectedHour = time.Hour;
					uiTaskMaker.FinTimeUI.SelectedMinute = 0;
					uiTaskMaker.FinTimeUI.SelectedSecond = 0;
                }
				else 
				{
					uiTaskMaker.LoadTask(_task);
				}
				//	hsshin
                uiTaskMaker.Cfg = cfg;
//				uiTaskMaker.ParentWindow = this;
	//			this.Focus();

				e.Handled = true;
            }
            catch (Exception err)
            {
                logger.Error(err + "");
            }
        }

		private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            try
            {
				if(false == uiTaskMaker.ValidationCheck())
				{
					return;
				}
				DateTime SelectedStartDate = uiTaskMaker.StartDateUI.SelectedDateTime != null ? uiTaskMaker.StartDateUI.SelectedDateTime.Value : DateTime.Now;
				DateTime SelectedEndDate = uiTaskMaker.FinDateUI.SelectedDateTime != null ? uiTaskMaker.FinDateUI.SelectedDateTime.Value : DateTime.Now;

				DateTime from = new DateTime(SelectedStartDate.Year,
							SelectedStartDate.Month,
							SelectedStartDate.Day,
							uiTaskMaker.StartTimeUI.SelectedHour,
							uiTaskMaker.StartTimeUI.SelectedMinute,
							uiTaskMaker.StartTimeUI.SelectedSecond);


				DateTime till = new DateTime(SelectedEndDate.Year,
							SelectedEndDate.Month,
							SelectedEndDate.Day,
							uiTaskMaker.FinTimeUI.SelectedHour,
							uiTaskMaker.FinTimeUI.SelectedMinute,
							uiTaskMaker.FinTimeUI.SelectedSecond);
//                 if (from > till)
//                 {
// 					System.Windows.MessageBox.Show(Properties.Resources.mbWrongTime, Properties.Resources.titleMessageBox,
//                         MessageBoxButton.OK, MessageBoxImage.Error);
//                     return;
//                 }


				int nRet = 0;
				if ((nRet = uiTaskMaker.createTask(from, till, uiTaskMaker.TaskName)) != 0)
				{
					//	오류가 났기 때문에 메시지를 출력한다.
					if (nRet == -1) System.Windows.MessageBox.Show(Properties.Resources.mbErrorFTPUpload, Properties.Resources.titleMessageBox,
						MessageBoxButton.OK, MessageBoxImage.Error);

					return;
				}

				result = true;
				this.DialogResult = true;
                this.Close();
            }
            catch (Exception err)
            {
				MessageBox.Show(err.Message, Properties.Resources.titleMessageBox, MessageBoxButton.OK, MessageBoxImage.Error);
                logger.Error(err + "");
            }
        }

        #endregion

    }
}
