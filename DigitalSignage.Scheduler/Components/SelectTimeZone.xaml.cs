﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Reflection;

using NLog;
using DigitalSignage.Common;
using System.Collections.ObjectModel;


namespace DigitalSignage.Scheduler
{
    /// <summary>
    /// Interaction logic for GroupTree.xaml
    /// </summary>
    public partial class SelectTimeZone : Window
    {
		private static Logger logger = LogManager.GetCurrentClassLogger();

	
		public TimeSpan StartTime;
		public TimeSpan EndTime;

		public struct _trGroupTimeScope
		{
			public TimeSpan StartTime;
			public TimeSpan EndTime;
		};

		Collection<_trGroupTimeScope> arrGroupTimeScopes = new Collection<_trGroupTimeScope>();
		

		public SelectTimeZone(TimeSpan start, TimeSpan end, bool HasGroupLimitation)
        {
            InitializeComponent();

			GroupLimitPanel.Height = HasGroupLimitation ? new GridLength(90) : new GridLength(0);

			if(!HasGroupLimitation)
			{
				this.Height -= 90;
			}
			timeStart.SelectedTime = start;
			timeEnd.SelectedTime = end;

			arrGroupTimeScopes.Clear();
        }

		public void AddGroupTimeScope(TimeSpan start, TimeSpan end)
		{
			lstTimeScope.Items.Add(start.ToString() + "-" + end.ToString());
			
			_trGroupTimeScope gts = new _trGroupTimeScope();

			gts.StartTime = start;
			gts.EndTime = end;

			arrGroupTimeScopes.Add(gts);
		}

		private bool IsValidScope(TimeSpan start, TimeSpan end)
		{
			if (arrGroupTimeScopes.Count == 0)
				return true;

			foreach(_trGroupTimeScope gts in arrGroupTimeScopes)
			{
				if (gts.StartTime <= start && gts.EndTime >= end)
					return true;
			}

			return false;
		}

		private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
				if (timeStart.SelectedTime >= timeEnd.SelectedTime)
				{
					MessageBox.Show(Properties.Resources.mbErrorTimeZone, Properties.Resources.titleMessageBox, MessageBoxButton.OK, MessageBoxImage.Warning);
				}
				else if(!IsValidScope(timeStart.SelectedTime, timeEnd.SelectedTime))
				{
					MessageBox.Show(Properties.Resources.mbErrorTimeZone, Properties.Resources.titleMessageBox, MessageBoxButton.OK, MessageBoxImage.Warning);
				}
				else
				{
					StartTime = timeStart.SelectedTime;
					EndTime = timeEnd.SelectedTime;

					this.DialogResult = true;
					this.Close();
				}
            }
            catch (Exception err)
            {
                logger.Error(err + "");
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
			this.DialogResult = false;
            this.Close();
        }
	}
}
