﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Reflection;

using NLog;
using DigitalSignage.Common;
using System.Collections.ObjectModel;
using System.Xml;


namespace DigitalSignage.Scheduler
{
    /// <summary>
    /// Interaction logic for GroupTree.xaml
    /// </summary>
    public partial class GroupPicker : Window
    {
        public Object result = null;
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private Config config;
        private bool ShowPlayers = false;
		private bool checkbox = false;
		
		private Object _target = null;
		public Object TargetObject
		{
			get {return _target;}
			set {
				_target = value;
			}
		}

        public GroupPicker( Config cfg, bool showPlayers)
        {
            config = cfg;
            InitializeComponent();
            ShowPlayers = showPlayers;
		}
		public GroupPicker(Config cfg, bool showPlayers, bool hasCheck)
		{
			config = cfg;
			InitializeComponent();
			ShowPlayers = showPlayers;
			checkbox = hasCheck;
		}
		private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            try
            {
				Collection<TreeViewItem> items = groupTree.CheckedItems;
				if (items == null && !checkbox)
					result = groupTree.SelectedItem;
				else
					result = (object)items;

				if (result == null)
				{
					MessageBox.Show(Properties.Resources.mbSelectPlayerOrGroup, Properties.Resources.titleMessageBox, MessageBoxButton.OK, MessageBoxImage.Error);
					return;
				}
				this.DialogResult = true;
//                 this.Close();
            }
            catch (Exception err)
            {
                logger.Error(err + "");
            }
        }

		private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
			this.DialogResult = false;
//             this.Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)        
        {
            try
            {
				groupTree.HasCheckBox = checkbox;
                groupTree.ShowPlayers = ShowPlayers;
                groupTree.SetParent(null, config);
//                 groupTree.UpdateTree();
				groupTree.SelectedItem = _target;
				groupTree.HasPopupMenu = false;

            }
            catch (Exception err)
            {
                logger.Error(err + "");
            }
        }
    }
}
