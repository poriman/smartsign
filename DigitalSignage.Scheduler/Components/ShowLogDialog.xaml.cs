﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using DigitalSignage.ServerDatabase;
using DigitalSignage.Common;

using NLog;

namespace DigitalSignage.Scheduler
{
	/// <summary>
	/// Interaction logic for ShowLogDialog.xaml
	/// </summary>
	public partial class ShowLogDialog : Window
	{
		private static Logger logger = LogManager.GetCurrentClassLogger();

		Config config;
		string selectedPID;

		ServerDataSet.logsDataTable dtAll = null;
		string ordering = "DESC";
		string column = "reg_dt"; 

		public ShowLogDialog(Config cfg, string pid, string name)
		{
			config = cfg;
			selectedPID = pid; 
			
			InitializeComponent();

			DateTime now = DateTime.Now;
			dtStart.SelectedDateTime = new DateTime(now.Year, now.Month, now.Day, 0,0,0);
			dtEnd.SelectedDateTime = new DateTime(now.Year, now.Month, now.Day, 23, 59, 59);
			playerName.Text = name;
		}

		private void okButton_Click(object sender, RoutedEventArgs e)
		{
			this.Close();
		}

		private void btnSearch_Click(object sender, RoutedEventArgs e)
		{
			if(false == ValidationCheck())
			{
				MessageBox.Show(Properties.Resources.mbErrorSearch);
				return;
			}
			try
			{
				using (new DigitalSignage.Common.WaitCursor())
				{

					dtAll = new ServerDataSet.logsDataTable();

					//	시간 세팅
					DateTime temp = dtStart.SelectedDateTime.Value;
					DateTime start = new DateTime(temp.Year, temp.Month, temp.Day, 0, 0, 0);
					
					temp = dtEnd.SelectedDateTime.Value;
					DateTime end = new DateTime(temp.Year, temp.Month, temp.Day, 23, 59, 59);

					long startTime = TimeConverter.ConvertToUTP(start.ToUniversalTime());
					long endTime = TimeConverter.ConvertToUTP(end.ToUniversalTime());

					if (cCategoryDownload.IsChecked.Value)
					{
						if (cTypeDefaultSched.IsChecked.Value)
						{
							ServerDataSet.logsDataTable dt = config.ServerLogList.GetLogsFromTo(selectedPID, (int)TaskCommands.CmdDefaultProgram, (int)LogCategory.CatDownload, startTime, endTime);

							dtAll.Merge(dt);
							dt.Dispose();
						}

						if (cTypeSubtitle.IsChecked.Value)
						{
							ServerDataSet.logsDataTable dt = config.ServerLogList.GetLogsFromTo(selectedPID, (int)TaskCommands.CmdSubtitles, (int)LogCategory.CatDownload, startTime, endTime);
							dtAll.Merge(dt);
							dt.Dispose();
						}

						if (cTypeTimeSched.IsChecked.Value)
						{
							ServerDataSet.logsDataTable dt = config.ServerLogList.GetLogsFromTo(selectedPID, (int)TaskCommands.CmdProgram, (int)LogCategory.CatDownload, startTime, endTime);
							dtAll.Merge(dt);
							dt.Dispose();
						}

					}

					if (cCategoryError.IsChecked.Value)
					{
						if (cTypeDefaultSched.IsChecked.Value)
						{
							ServerDataSet.logsDataTable dt = config.ServerLogList.GetLogsFromTo(selectedPID, (int)TaskCommands.CmdDefaultProgram, (int)LogCategory.CatError, startTime, endTime);
							dtAll.Merge(dt);
							dt.Dispose();
						}

						if (cTypeSubtitle.IsChecked.Value)
						{
							ServerDataSet.logsDataTable dt = config.ServerLogList.GetLogsFromTo(selectedPID, (int)TaskCommands.CmdSubtitles, (int)LogCategory.CatError, startTime, endTime);
							dtAll.Merge(dt);
							dt.Dispose();
						}

						if (cTypeTimeSched.IsChecked.Value)
						{
							ServerDataSet.logsDataTable dt = config.ServerLogList.GetLogsFromTo(selectedPID, (int)TaskCommands.CmdProgram, (int)LogCategory.CatError, startTime, endTime);
							dtAll.Merge(dt);
							dt.Dispose();
						}

					}

					if(cCategoryPlay.IsChecked.Value)
					{
						if (cTypeDefaultSched.IsChecked.Value)
						{
							ServerDataSet.logsDataTable dt = config.ServerLogList.GetLogsFromTo(selectedPID, (int)TaskCommands.CmdDefaultProgram, (int)LogCategory.CatProcceed, startTime, endTime);
							dtAll.Merge(dt);
							dt.Dispose();
						}

						if (cTypeSubtitle.IsChecked.Value)
						{
							ServerDataSet.logsDataTable dt = config.ServerLogList.GetLogsFromTo(selectedPID, (int)TaskCommands.CmdSubtitles, (int)LogCategory.CatProcceed, startTime, endTime);
							dtAll.Merge(dt);
							dt.Dispose();
						}

						if (cTypeTimeSched.IsChecked.Value)
						{
							ServerDataSet.logsDataTable dt = config.ServerLogList.GetLogsFromTo(selectedPID, (int)TaskCommands.CmdProgram, (int)LogCategory.CatProcceed, startTime, endTime);
							dtAll.Merge(dt);
							dt.Dispose();
						}
					}

					if (lvLogs != null)
					{
						lvLogs.DataContext = dtAll.Select("", String.Format("{0} {1}", column, ordering));
					}

					if(dtAll.Rows.Count == 0)
					{
						MessageBox.Show(Properties.Resources.mbNoResult);
					}
				}
			}
			catch (Exception ex)
			{
				logger.Error(ex.Message);
			}
		}

		/// <summary>
		/// 각 그룹마다 하나라도 체크가 되어있어야 한다. 시작 시간과 끝 시간이 맞아야 한다.
		/// </summary>
		/// <returns></returns>
		private bool ValidationCheck()
		{
			if (cCategoryDownload.IsChecked.Value || cCategoryPlay.IsChecked.Value || cCategoryError.IsChecked.Value)
			{
				if (cTypeDefaultSched.IsChecked.Value || cTypeTimeSched.IsChecked.Value || cTypeSubtitle.IsChecked.Value)
				{
					if (dtStart.SelectedDateTime <= dtEnd.SelectedDateTime)
						return true;
				}
			}

			return false;
		}

		private void uuidColumn_Click(object sender, RoutedEventArgs e)
		{
			SortColumn("uuid");
			e.Handled = true;
		}
		private void nameColumn_Click(object sender, RoutedEventArgs e)
		{
			SortColumn("name");
			e.Handled = true;
		}
		private void codeColumn_Click(object sender, RoutedEventArgs e)
		{
			SortColumn("code");
			e.Handled = true;
		}
		private void reg_dtColumn_Click(object sender, RoutedEventArgs e)
		{
			SortColumn("reg_dt");
			e.Handled = true;
		}
		private void descriptionColumn_Click(object sender, RoutedEventArgs e)
		{
			SortColumn("description");
			e.Handled = true;
		}

		private void SortColumn(string col)
		{
			if (dtAll != null && lvLogs != null)
			{
				if (column.Equals(col))
				{
					if (0 == ordering.CompareTo("DESC")) ordering = "ASC";
					else ordering = "DESC";
				}
				else
				{
					ordering = "DESC";
				}

				column = col;

				lvLogs.DataContext = dtAll.Select("", String.Format("{0} {1}", column, ordering));
			}
		}

	}
}
