﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NLog;
using System.Xml;
using System.Security.Cryptography;
using System.ComponentModel;

namespace DigitalSignage.Scheduler
{
	/// <summary>
	/// 서버의 정보를 담고 있는 클래스. 주로 Network Tree 또는 User별 Permission 정보를 담는다.
	/// </summary>
	class ServerDataCache
	{
		private static Logger logger = LogManager.GetCurrentClassLogger();

		private static ServerDataCache instance = null;
		private static readonly object semaphore = new object();

		private string connected_username = "";

		private XmlDocument userTree = null;
		private XmlDocument networkTree = null;

		public XmlDocument NetworkTree { get { return networkTree; } }

		private Dictionary<String, String> _dicPlayerName = new Dictionary<string, string>();

		/// <summary>
		///	네트워크 트리를 갱신하는 백그라운드 스레드
		/// </summary>
		private BackgroundWorker bwThreadRefreshNetwork = null;
		
		/// <summary>
		/// 서버의 스케줄 핸들러 서비스가 돌고있는지
		/// </summary>
		public bool IsScheduleHandlerEnabled = false;

		/// <summary>
		/// 서버에 KVM 프록시 서버가 돌고있는지
		/// </summary>
		public bool IsRunningProxyServer = false;

		/// <summary>
		/// 서버에 NPCU 서버가 돌고 있는지
		/// </summary>
		public bool IsRunningNPCUServer = false;

        /// <summary>
        /// 섬네일 기능이 켜져 있는지
        /// </summary>
        public bool IsSupportedThumbnail = false;

        public string ThumbnailGetUrl = String.Empty;
        public string ThumbnailSetUrl = String.Empty;
        public string ThumbnailSetAuthID = String.Empty;
        public string ThumbnailSetAuthPass = String.Empty;


		/// <summary>
		/// 패스워드 해시값 추출
		/// </summary>
		/// <param name="origin"></param>
		/// <returns></returns>
		private string GetHash(string origin)
		{
			if (String.IsNullOrEmpty(origin))
				return origin;

			StringBuilder sb = new StringBuilder();
			MD5 md5 = new MD5CryptoServiceProvider();
			Byte[] original;
			Byte[] encoded;
			original = ASCIIEncoding.Default.GetBytes(origin);
			encoded = md5.ComputeHash(original);
			foreach (byte hex in encoded) sb.Append(hex.ToString("x2"));
			return sb.ToString().ToLower();
		}

		public string Connected_UserName
		{
			get { return connected_username; }
		}

		public static ServerDataCache GetInstance
		{
			get
			{
				lock (semaphore)
				{
					if (instance == null)
					{
						instance = new ServerDataCache();
					}
					return instance;
				}
			}
		}
		public XmlNode CurrentUser
		{
			get {
				if(userTree != null && !String.IsNullOrEmpty(connected_username))
				{
					return userTree.SelectSingleNode("//child::user[attribute::name=\"" + connected_username + "\"]");
				}
				return null;
			}
		}

		/// <summary>
		/// 플레이어 ID에 해당하는 플레이어 이름 사전을 만든다.
		/// </summary>
		void MakePlayerDictionary()
		{
			try
			{
				XmlNodeList list = this.NetworkTree.SelectNodes("//child::player");
				_dicPlayerName.Clear();

				foreach (XmlNode node in list)
				{
					try
					{
						_dicPlayerName.Add(node.Attributes["pid"].Value, node.Attributes["name"].Value);
					}
					catch { }
				}
			}
			catch (Exception ex)
			{
				logger.Error(ex.ToString());
			}
		}

		/// <summary>
		/// Dictionary를 조회하여 플레이어 이름을 반환한다.
		/// </summary>
		/// <param name="pid"></param>
		/// <returns></returns>
		public String GetPlayerName(String pid)
		{
			try
			{
				lock (semaphore)
				{
					return _dicPlayerName[pid];
				}
			}
			catch { return pid; }
		}

        /// <summary>
        /// 플레이어ID를 기준으로 그룹 이름을 반환한다.
        /// </summary>
        /// <param name="pid"></param>
        /// <returns></returns>
        public String GetGroupNameByPID(String pid)
        {
            try
            {
                XmlNode gnode = GetGroupByPID(pid);

				lock (semaphore)
				{

					if (gnode == null) return Properties.Resources.exceptionUnknownGroup;

					return gnode.Attributes["name"].Value;
				}
            }
            catch { return Properties.Resources.exceptionUnknownGroup; }
        }

		/// <summary>
		/// 로그인
		/// </summary>
		/// <param name="username"></param>
		/// <param name="password"></param>
		/// <returns></returns>
		public bool Initialize(string username, string password)
		{
			try
			{
				Config cfg = Config.GetConfig;
				String sReturn = cfg.ServerDataCacheList.GetUserNode(username, GetHash(password));

				if (String.IsNullOrEmpty(sReturn))
				{
					return false;
				}

				userTree = new XmlDocument();
				userTree.LoadXml(sReturn);

				sReturn = cfg.ServerDataCacheList.GetNetworkTreeByUser(username);

				lock (semaphore)
				{

					networkTree = new XmlDocument();
					if (!String.IsNullOrEmpty(sReturn))
					{
						networkTree.LoadXml(sReturn);
					}
					connected_username = username;

					try
					{
						IsScheduleHandlerEnabled = Convert.ToBoolean(networkTree.SelectSingleNode("//child::NetworkTree").Attributes["ScheduleHandlerService"].Value);
					}
					catch { IsScheduleHandlerEnabled = false; }

					try
					{
						IsRunningProxyServer = Convert.ToBoolean(networkTree.SelectSingleNode("//child::NetworkTree").Attributes["KVMProxy"].Value);
					}
					catch { IsRunningProxyServer = false; }

					try
					{
						IsRunningNPCUServer = Convert.ToBoolean(networkTree.SelectSingleNode("//child::NetworkTree").Attributes["NPCUService"].Value);
					}
					catch { IsRunningNPCUServer = false; }

                    try
                    {
                        IsSupportedThumbnail = Convert.ToBoolean(networkTree.SelectSingleNode("//child::NetworkTree").Attributes["UseThumb"].Value);
                        ThumbnailGetUrl = networkTree.SelectSingleNode("//child::NetworkTree").Attributes["ThumbGetURL"].Value;
                        ThumbnailSetUrl = networkTree.SelectSingleNode("//child::NetworkTree").Attributes["ThumbSetURL"].Value;
                        ThumbnailSetAuthID = networkTree.SelectSingleNode("//child::NetworkTree").Attributes["ThumbSetAuthID"].Value;
                        ThumbnailSetAuthPass = networkTree.SelectSingleNode("//child::NetworkTree").Attributes["ThumbSetAuthPass"].Value;
                    }
                    catch {
                        IsSupportedThumbnail = false;
                        ThumbnailGetUrl = ThumbnailSetUrl = ThumbnailSetAuthID = ThumbnailSetAuthPass = String.Empty;
                    }

					MakePlayerDictionary();

					try
					{
						if (bwThreadRefreshNetwork == null)
						{
							bwThreadRefreshNetwork = new BackgroundWorker();
							bwThreadRefreshNetwork.WorkerSupportsCancellation = true;
							bwThreadRefreshNetwork.DoWork += new DoWorkEventHandler(bwThreadRefreshNetwork_DoWork);
						}

						bwThreadRefreshNetwork.RunWorkerAsync();
					}
					catch { }
				}
				return true;
			}
			catch (Exception ex)
			{
				logger.Error(ex.ToString());
			}

			return false;
		}

		/// <summary>
		///	네트워크 트리 갱신 스레드
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		void bwThreadRefreshNetwork_DoWork(object sender, DoWorkEventArgs e)
		{
			try
			{
				Config cfg = Config.GetConfig;
				while (!bwThreadRefreshNetwork.CancellationPending)
				{
					System.Threading.Thread.Sleep(10000);

                    try
                    {
                        String sReturn = cfg.ServerDataCacheList.GetNetworkTreeByUser(this.Connected_UserName);

                        lock (semaphore)
                        {
                            this.networkTree = new XmlDocument();
                            if (!String.IsNullOrEmpty(sReturn))
                            {
                                this.networkTree.LoadXml(sReturn);
                            }

                            try
                            {
                                IsScheduleHandlerEnabled = Convert.ToBoolean(networkTree.SelectSingleNode("//child::NetworkTree").Attributes["ScheduleHandlerService"].Value);
                            }
                            catch { IsScheduleHandlerEnabled = false; }

                            try
                            {
                                IsRunningProxyServer = Convert.ToBoolean(networkTree.SelectSingleNode("//child::NetworkTree").Attributes["KVMProxy"].Value);
                            }
                            catch { IsRunningProxyServer = false; }

                            try
                            {
                                IsRunningNPCUServer = Convert.ToBoolean(networkTree.SelectSingleNode("//child::NetworkTree").Attributes["NPCUService"].Value);
                            }
                            catch { IsRunningNPCUServer = false; }

                            try
                            {
                                IsSupportedThumbnail = Convert.ToBoolean(networkTree.SelectSingleNode("//child::NetworkTree").Attributes["UseThumb"].Value);
                                ThumbnailGetUrl = networkTree.SelectSingleNode("//child::NetworkTree").Attributes["ThumbGetURL"].Value;
                                ThumbnailSetUrl = networkTree.SelectSingleNode("//child::NetworkTree").Attributes["ThumbSetURL"].Value;
                                ThumbnailSetAuthID = networkTree.SelectSingleNode("//child::NetworkTree").Attributes["ThumbSetAuthID"].Value;
                                ThumbnailSetAuthPass = networkTree.SelectSingleNode("//child::NetworkTree").Attributes["ThumbSetAuthPass"].Value;
                            }
                            catch
                            {
                                IsSupportedThumbnail = false;
                                ThumbnailGetUrl = ThumbnailSetUrl = ThumbnailSetAuthID = ThumbnailSetAuthPass = String.Empty;
                            }

                            MakePlayerDictionary();
                        }
                    }
                    catch { }
					
				}
			}
			catch { }
		}

		/// <summary>
		/// 사용자 정보를 서버에서 갱신받는다.
		/// </summary>
		/// <returns></returns>
		public bool RefreshUserTreeInfo()
		{
			try
			{
				Config cfg = Config.GetConfig;
				String sReturn = cfg.ServerDataCacheList.GetUserNode(connected_username, CurrentUser.Attributes["hash"].Value);

				if (!String.IsNullOrEmpty(sReturn))
				{
					userTree = null;
					userTree = new XmlDocument();

					userTree.LoadXml(sReturn);

					return true;
				}

			}
			catch(Exception ex)
			{
				logger.Error(ex.ToString());
			}
			return false;

		}

		/// <summary>
		/// 네트워크 트리 정보를 서버에서 갱신받는다.
		/// </summary>
		/// <returns></returns>
		public bool RefreshNetworkTreeInfo()
		{
			try
			{
				Config cfg = Config.GetConfig;
				String sReturn = cfg.ServerDataCacheList.GetNetworkTreeByUser(connected_username);

				if (!String.IsNullOrEmpty(sReturn))
				{
					lock (semaphore)
					{

						networkTree = null;
						networkTree = new XmlDocument();

						networkTree.LoadXml(sReturn);

						try
						{
							IsScheduleHandlerEnabled = Convert.ToBoolean(networkTree.SelectSingleNode("//child::NetworkTree").Attributes["ScheduleHandlerService"].Value);
						}
						catch { IsScheduleHandlerEnabled = false; }

						try
						{
							IsRunningProxyServer = Convert.ToBoolean(networkTree.SelectSingleNode("//child::NetworkTree").Attributes["KVMProxy"].Value);
						}
						catch { IsRunningProxyServer = false; }

						try
						{
							IsRunningNPCUServer = Convert.ToBoolean(networkTree.SelectSingleNode("//child::NetworkTree").Attributes["NPCUService"].Value);
						}
						catch { IsRunningNPCUServer = false; }

                        try
                        {
                            IsSupportedThumbnail = Convert.ToBoolean(networkTree.SelectSingleNode("//child::NetworkTree").Attributes["UseThumb"].Value);
                            ThumbnailGetUrl = networkTree.SelectSingleNode("//child::NetworkTree").Attributes["ThumbGetURL"].Value;
                            ThumbnailSetUrl = networkTree.SelectSingleNode("//child::NetworkTree").Attributes["ThumbSetURL"].Value;
                            ThumbnailSetAuthID = networkTree.SelectSingleNode("//child::NetworkTree").Attributes["ThumbSetAuthID"].Value;
                            ThumbnailSetAuthPass = networkTree.SelectSingleNode("//child::NetworkTree").Attributes["ThumbSetAuthPass"].Value;
                        }
                        catch
                        {
                            IsSupportedThumbnail = false;
                            ThumbnailGetUrl = ThumbnailSetUrl = ThumbnailSetAuthID = ThumbnailSetAuthPass = String.Empty;
                        }
						MakePlayerDictionary();

						return true;
					}
				}

			}
			catch(Exception ex)
			{
				logger.Error(ex.ToString());
			}
			return false;

		}

		/// <summary>
		/// 부여된 역활의 최소 레벨을 반환한다.
		/// </summary>
		/// <returns>25535가 리턴되면 에러라는 뜻</returns>
		public int GetRoleLevel()
		{
			try
			{
				if (userTree == null || String.IsNullOrEmpty(connected_username)) return 25535;

				XmlNodeList nodelist = userTree.SelectNodes("//child::user[attribute::name=\"" + connected_username + "\"]/child::role");
				int level = 25535;
				foreach (XmlNode node in nodelist)
				{
					int currLevel = Convert.ToInt32(node.Attributes["role_level"].Value);

					if (level > currLevel) level = currLevel;
				}

				return level;
			}
			catch (Exception ex)
			{
				logger.Error(ex.ToString());
			}

			return 25535;
		}

		/// <summary>
		/// 현재 접속된 사용자에 해당 퍼미션이 존재하는지 검사
		/// </summary>
		/// <param name="permission"></param>
		/// <returns></returns>
		public bool HasPermission(string permission)
		{
			try
			{
				if (userTree == null || String.IsNullOrEmpty(connected_username)) return false;

				XmlNodeList nodelist = userTree.SelectNodes("//child::user[attribute::name=\"" + connected_username + "\"]/child::role");

				foreach(XmlNode node in nodelist)
				{
					if(node.SelectNodes("//child::permission[attribute::perm_name=\"" + permission.ToUpper() + "\" or attribute::perm_name=\"ADMINISTRATOR\"]").Count > 0)
						return true;
				}

			}
			catch (Exception ex)
			{
				logger.Error(ex.ToString());
			}
			return false; 
		}

		/// <summary>
		/// 사용자의 리스트를 가져온다.
		/// </summary>
		/// <returns></returns>
		public XmlNodeList GetUserList()
		{
			try
			{
				if (userTree == null) return null;

				return userTree.SelectNodes("//child::user");
				
			}
			catch(Exception ex)
			{
				logger.Error(ex.ToString());
			}
			return null;
		}

		/// <summary>
		/// 해당 그룹노드에 하위 노드가 존재하는지
		/// </summary>
		/// <param name="gnode">그룹 노드</param>
		/// <param name="bOnlyAvailable">true인 경우, DEL_YN 속성이 Y일때 없는 노드로 인식한다.</param>
		/// <returns></returns>
		public bool HasChild(XmlNode gnode, bool bOnlyAvailable)
		{
			if (gnode == null) return false;

			lock (semaphore)
			{

				try
				{
					if (bOnlyAvailable)
					{
						foreach (XmlNode node in gnode.ChildNodes)
						{
							try
							{
								if (node.Attributes["del_yn"].Value.Equals("N")) return true;
							}
							catch { }
						}

						return false;
					}
				}
				catch { }

				return gnode.HasChildNodes;
			}
		}

		/// <summary>
		/// 그룹에 해당하는 하위의 Player의 리스트를 가져온다.
		/// </summary>
		/// <param name="gid"></param>
		/// <param name="bOnlyChildren">true 면 깊이를 막론하고 모든 Player를 가져온다.</param>
		/// <returns></returns>
		public XmlNodeList GetPlayerList(string gid, bool bOnlyChildren)
		{
			try
			{
				if (networkTree == null) return null;

				lock (semaphore)
				{
					if (bOnlyChildren)
						return networkTree.SelectNodes("//child::group[attribute::gid=\"" + gid + "\" and attribute::del_yn=\"N\"]/child::player[attribute::del_yn=\"N\"]");
					else
						return networkTree.SelectNodes("//child::group[attribute::gid=\"" + gid + "\" and attribute::del_yn=\"N\"]/descendant::player[attribute::del_yn=\"N\"]");
				}
			}
			catch (Exception ex)
			{
				logger.Error(ex.ToString());
			}
			return null;
		}

		/// <summary>
		/// 그룹 ID에 해당하는 Player들을 서버에서 갱신 받는다.
		/// </summary>
		/// <param name="gid"></param>
		public void UpdatePlayersInGroup(string gid)
		{
			try
			{
				lock (semaphore)
				{

					string sXmlNode = Config.GetConfig.ServerDataCacheList.GetGroupNode(gid);

					XmlDocument doc = new XmlDocument();
					doc.LoadXml(sXmlNode);
					XmlNodeList list = doc.SelectNodes("//child::player");

					foreach (XmlNode node in list)
					{
						string pid = node.Attributes["pid"].Value;
						XmlNode finded = GetPlayer(pid);

						if (finded == null) continue;

						long serverstamp = Convert.ToInt64(node.Attributes["_timestamp"].Value);
						long currstamp = Convert.ToInt64(finded.Attributes["_timestamp"].Value);

						if (serverstamp > currstamp)
						{
							finded.Attributes["_timestamp"].Value = node.Attributes["_timestamp"].Value;
							finded.Attributes["gid"].Value = node.Attributes["gid"].Value;
							finded.Attributes["name"].Value = node.Attributes["name"].Value;
							finded.Attributes["host"].Value = node.Attributes["host"].Value;
							finded.Attributes["state"].Value = node.Attributes["state"].Value;
							finded.Attributes["freespace"].Value = node.Attributes["freespace"].Value;
							finded.Attributes["port"].Value = node.Attributes["port"].Value;
							finded.Attributes["ComSpeed"].Value = node.Attributes["ComSpeed"].Value;
							finded.Attributes["ComBits"].Value = node.Attributes["ComBits"].Value;
							finded.Attributes["temperature"].Value = node.Attributes["uptime"].Value;
							finded.Attributes["uptime"].Value = node.Attributes["uptime"].Value;
							finded.Attributes["versionH"].Value = node.Attributes["versionH"].Value;
							finded.Attributes["versionL"].Value = node.Attributes["versionL"].Value;
							finded.Attributes["angle"].Value = node.Attributes["angle"].Value;
							finded.Attributes["memoryusage"].Value = node.Attributes["memoryusage"].Value;
							finded.Attributes["cpurate"].Value = node.Attributes["cpurate"].Value;
							finded.Attributes["curr_subtitle"].Value = node.Attributes["curr_subtitle"].Value;
							finded.Attributes["curr_screen"].Value = node.Attributes["curr_screen"].Value;
							finded.Attributes["curr_d_progress"].Value = node.Attributes["curr_d_progress"].Value;
							finded.Attributes["all_d_progress"].Value = node.Attributes["all_d_progress"].Value;
							try
							{
								finded.Attributes["_serial_device_name"].Value = node.Attributes["_serial_device_name"].Value;
								finded.Attributes["_serial_stat_power"].Value = node.Attributes["_serial_stat_power"].Value;
								finded.Attributes["_serial_stat_source"].Value = node.Attributes["_serial_stat_source"].Value;
								finded.Attributes["_serial_stat_volume"].Value = node.Attributes["_serial_stat_volume"].Value;

								try
								{
									finded.Attributes["curr_volume"].Value = node.Attributes["curr_volume"].Value;
								}
								catch { }

								finded.Attributes["lastconndt"].Value = node.Attributes["lastconndt"].Value;
								finded.Attributes["del_yn"].Value = node.Attributes["del_yn"].Value;

							}
							catch
							{
							}

							try
							{
								finded.Attributes["_ipv4addr"].Value = node.Attributes["_ipv4addr"].Value;
								finded.Attributes["_macaddr"].Value = node.Attributes["_macaddr"].Value;
								finded.Attributes["_is_dhcp"].Value = node.Attributes["_is_dhcp"].Value;
								finded.Attributes["_mnttype"].Value = node.Attributes["_mnttype"].Value;
								finded.Attributes["_auth_id"].Value = node.Attributes["_auth_id"].Value;
								finded.Attributes["_auth_pass"].Value = node.Attributes["_auth_pass"].Value;
							}
							catch
							{
							}
						}
					}
				}
			}
			catch (Exception ex)
			{
				logger.Error(ex.ToString());
			}
		}

		/// <summary>
		/// 플레이어 ID에 해당하는 Player를 서버에서 갱신 받는다.
		/// </summary>
		/// <param name="pid"></param>
		public void UpdatePlayer(string pid)
		{
			try
			{
				lock (semaphore)
				{

					string sXmlNode = Config.GetConfig.ServerDataCacheList.GetPlayerNode(pid);

					XmlDocument doc = new XmlDocument();
					doc.LoadXml(sXmlNode);
					XmlNode node = doc.SelectSingleNode("//child::player");

					XmlNode finded = GetPlayer(pid);

					if (finded == null) return;

					long serverstamp = Convert.ToInt64(node.Attributes["_timestamp"].Value);
					long currstamp = Convert.ToInt64(finded.Attributes["_timestamp"].Value);

					if (serverstamp > currstamp)
					{
						finded.Attributes["_timestamp"].Value = node.Attributes["_timestamp"].Value;
						finded.Attributes["gid"].Value = node.Attributes["gid"].Value;
						finded.Attributes["name"].Value = node.Attributes["name"].Value;
						finded.Attributes["host"].Value = node.Attributes["host"].Value;
						finded.Attributes["state"].Value = node.Attributes["state"].Value;
						finded.Attributes["freespace"].Value = node.Attributes["freespace"].Value;
						finded.Attributes["port"].Value = node.Attributes["port"].Value;
						finded.Attributes["ComSpeed"].Value = node.Attributes["ComSpeed"].Value;
						finded.Attributes["ComBits"].Value = node.Attributes["ComBits"].Value;
						finded.Attributes["temperature"].Value = node.Attributes["uptime"].Value;
						finded.Attributes["uptime"].Value = node.Attributes["uptime"].Value;
						finded.Attributes["versionH"].Value = node.Attributes["versionH"].Value;
						finded.Attributes["versionL"].Value = node.Attributes["versionL"].Value;
						finded.Attributes["angle"].Value = node.Attributes["angle"].Value;
						finded.Attributes["memoryusage"].Value = node.Attributes["memoryusage"].Value;
						finded.Attributes["cpurate"].Value = node.Attributes["cpurate"].Value;
						finded.Attributes["curr_subtitle"].Value = node.Attributes["curr_subtitle"].Value;
						finded.Attributes["curr_screen"].Value = node.Attributes["curr_screen"].Value;
						finded.Attributes["curr_d_progress"].Value = node.Attributes["curr_d_progress"].Value;
						finded.Attributes["all_d_progress"].Value = node.Attributes["all_d_progress"].Value;

						try
						{
							finded.Attributes["_serial_device_name"].Value = node.Attributes["_serial_device_name"].Value;
							finded.Attributes["_serial_stat_power"].Value = node.Attributes["_serial_stat_power"].Value;
							finded.Attributes["_serial_stat_source"].Value = node.Attributes["_serial_stat_source"].Value;
							finded.Attributes["_serial_stat_volume"].Value = node.Attributes["_serial_stat_volume"].Value;

							try
							{
								finded.Attributes["curr_volume"].Value = node.Attributes["curr_volume"].Value;
							}
							catch { }

							finded.Attributes["lastconndt"].Value = node.Attributes["lastconndt"].Value;
							finded.Attributes["del_yn"].Value = node.Attributes["del_yn"].Value;
						}
						catch
						{
						}
						try
						{
							finded.Attributes["_ipv4addr"].Value = node.Attributes["_ipv4addr"].Value;
							finded.Attributes["_macaddr"].Value = node.Attributes["_macaddr"].Value;
							finded.Attributes["_is_dhcp"].Value = node.Attributes["_is_dhcp"].Value;
							finded.Attributes["_mnttype"].Value = node.Attributes["_mnttype"].Value;
							finded.Attributes["_auth_id"].Value = node.Attributes["_auth_id"].Value;
							finded.Attributes["_auth_pass"].Value = node.Attributes["_auth_pass"].Value;
						}
						catch
						{
						}
					}
				}
			}
			catch (Exception ex)
			{
				logger.Error(ex.ToString());
			}
		}

		public XmlNodeList GetPlayerToReturnList(string pid)
		{
			try
			{
				if (networkTree == null) return null;

				lock (semaphore)
				{
					return networkTree.SelectNodes("//child::player[attribute::pid=\"" + pid + "\" and attribute::del_yn=\"N\"]");
				}
			}
			catch (Exception ex)
			{
				logger.Error(ex.ToString());
			}
			return null;
		}

        /// <summary>
        /// 최상위 그룹 노드를 서버에서 가져온다.
        /// </summary>
        /// <returns></returns>
        public XmlNode GetRootGroupNode()
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(Config.GetConfig.ServerDataCacheList.GetGroupNode("0000000000000"));

                return doc.SelectSingleNode("child::group");
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }

            return null;

        }

		public XmlNode GetPlayer(string pid)
		{
			try
			{
				if (networkTree == null) return null;

				lock (semaphore)
				{
					return networkTree.SelectSingleNode("//child::player[attribute::pid=\"" + pid + "\" and attribute::del_yn=\"N\"]");
				}
			}
			catch (Exception ex)
			{
				logger.Error(ex.ToString());
			}
			return null;
		}

		public XmlNode GetGroupByPID(string pid)
		{
			try
			{
				if (networkTree == null) return null;

				lock (semaphore)
				{
					return networkTree.SelectSingleNode("//child::player[attribute::pid=\"" + pid + "\" and attribute::del_yn=\"N\"]").ParentNode;
				}
			}
			catch (Exception ex)
			{
				logger.Error(ex.ToString());
			}
			return null;
		}

		public XmlNode GetGroupByGID(string gid)
		{
			try
			{
				if (networkTree == null) return null;

				lock (semaphore)
				{
					return networkTree.SelectSingleNode("//child::group[attribute::gid=\"" + gid + "\" and attribute::del_yn=\"N\"]");
				}
			}
			catch (Exception ex)
			{
				logger.Error(ex.ToString());
			}
			return null;
		}
		/// <summary>
		/// 로그인이 되어있는지 검사.
		/// </summary>
		public bool IsConnected { get { return userTree != null; } }
	}
}
