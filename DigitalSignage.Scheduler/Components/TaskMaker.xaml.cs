﻿using System.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Reflection;

using NLog;

using DigitalSignage.Common;
using System.Xml;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections.ObjectModel;
using DigitalSignage.Scheduler.ViewModels;

namespace DigitalSignage.Scheduler
{
    public class ProjectItem
    {
        public string UUID { get; set; }

        public string Pid { get; set; }

        public string GroupID { get; set; }
     
        public string DownLoadID { get; set; }

        public string TargetUI { get; set; }

        public string ProjectUI { get;set;}

        public bool IsMaster { get; set; }

        public string MasterC
        {
            get
            {
                if (IsMaster == true)
                    return "M";
                else
                    return "S";

            }

        }

        public ProjectItem()
        {
            IsMaster = false;
            DownLoadID = "";
        }
        
    }


	/// <summary>
	/// Interaction logic for TaskMaker.xaml
	/// </summary>
	public partial class TaskMaker : Grid
	{
		private static Logger logger = LogManager.GetCurrentClassLogger();
		private Config cfg = null;
		private Window parentwindow = null;
		//projects
		private string projectProgram = null;
		private string subtilesProgram = null;

		private bool bIsEditUI = false;
		private Task OldTask = null;

        private TaskUploader uploader = null;

//		private TaskControlSchedules controlProgram = TaskControlSchedules.CtrlUndefined;

		private ServerDatabase.ServerDataSet.RSCommandsRow projectRS232 = null;
		private Collection<TreeViewItem> checkedItems = null;

		Collection<XmlNode> m_groups = new Collection<XmlNode>();
		private XmlNode selectedTarget = null;

        //TackCommands
        private TaskCommands TaskCommandType;
        public bool EditMode { get; set; }       
        
        private FileTransferInfo TaskFileTrans = FileTransferInfo.FILE_TEMP;

        enum _category { taskProgram = 0/*기본 스케줄*/, 
            taskSubtitle = 1/*자막 스케줄*/, 
            taskDefaultSchedule = 2, 
            taskControl = 3/*제어 스케줄*/, 
            taskRS232C = 4, 
            taskEventSchedule = 5/*이벤트 스케줄*/, 
            taskUrgentSchedule = 6/*긴급 스케줄*/ };

		enum _repeat_options { norepeat = 0, everydays, daysofweek, dayofmonth };

		/// <summary>
		/// 업로드가 완료된 페이지
		/// </summary>
		Collection<ScreenPropertyLoader> UploadedScreens = null;

        //List<string> pidList = new List<string>();
        List<ProjectItem> itemList = new List<ProjectItem>();

		#region 광고 관련
		private bool _isAd = false;
		private int _constrait = (int)ShowFlag.Show_NonEvent;
		private string _metaTags = string.Empty;
		/// <summary>
		/// 광고 여부
		/// </summary>
		public bool IsAd
		{
			get { return _isAd; }
		}

		/// <summary>
		/// 이벤트 발생시만 재생할지 여부 선택
		/// </summary>
		public int AdPlayConstrait
		{
			get
			{
				return _isAd ? _constrait : (int)DigitalSignage.Common.ShowFlag.Show_NonEvent;
			}
		}

		/// <summary>
		/// 메타 태그 정보
		/// </summary>
		public string MetaTags
		{
			get { return _metaTags; }
		}

		/// <summary>
		/// 시간대 중복을 허용할지
		/// </summary>
		public bool AllowDuplication
		{
			get
			{
				if ((_category)categoryUI.SelectedIndex == _category.taskProgram)
					return cboxAllowDup.IsChecked == true;
				else
					return true;
			}
		}

		#endregion

		/// <summary>
		/// 생성자
		/// </summary>
		public TaskMaker()
		{
			InitializeComponent();

			#region Initialize Comboboxes
			int nIdx = 0;
			foreach (ComboBoxItem item in categoryUI.Items)
			{
				item.DataContext = (_category)(nIdx);
				nIdx++;
			}

			nIdx = 0;
			foreach (ComboBoxItem item in cmbRepeatUI.Items)
			{
				item.DataContext = (_repeat_options)(nIdx);
				nIdx++;
			}
			cmbRepeatUI.SelectedIndex = 0;


			cbitemSubtitle.Visibility = Config.GetConfig.IsAvailableSubtitleSchedule ? Visibility.Visible : Visibility.Collapsed;
			cbitemControl.Visibility = Config.GetConfig.IsAvailableControlSchedule ? Visibility.Visible : Visibility.Collapsed;

			bool bHasAdminPermission = ServerDataCache.GetInstance.HasPermission("ADMINISTRATOR");

			//	리부트
			{
				ComboBoxItem item = new ComboBoxItem();
				item.Content = Properties.Resources.labelCtrlReboot;
				item.DataContext = TaskControlSchedules.CtrlReboot;
				cbControlList.Items.Add(item);
			}

			//	전원 켜기
			if(ServerDataCache.GetInstance.IsScheduleHandlerEnabled)
			{
				ComboBoxItem item = new ComboBoxItem();
				item.Content = Properties.Resources.labelCtrlPowerOn;
				item.DataContext = TaskControlSchedules.CtrlPowerOn;
				cbControlList.Items.Add(item);
			}

			//	전원 종료
			{
				ComboBoxItem item = new ComboBoxItem();
				item.Content = Properties.Resources.labelCtrlPowerOff;
				item.DataContext = TaskControlSchedules.CtrlPowerOff;
				cbControlList.Items.Add(item);
			}

// 			//	플레이어 켜기
// 			{
// 				ComboBoxItem item = new ComboBoxItem();
// 				item.Content = Properties.Resources.labelCtrlPlayerOn;
// 				item.DataContext = TaskControlSchedules.CtrlPlayerOn;
// 				cbControlList.Items.Add(item);
// 			}
// 
// 			//	플레이어 끄기
// 			{
// 				ComboBoxItem item = new ComboBoxItem();
// 				item.Content = Properties.Resources.labelCtrlPlayerOff;
// 				item.DataContext = TaskControlSchedules.CtrlPlayerOff;
// 				cbControlList.Items.Add(item);
// 			}
            /*
			//	볼륨 조절
			{
				ComboBoxItem item = new ComboBoxItem();
				item.Content = Properties.Resources.labelCtrlVolume;
				item.DataContext = TaskControlSchedules.CtrlVolume;
				cbControlList.Items.Add(item);
			}

			//	스케줄 동기화
			if (bHasAdminPermission)
			{
				ComboBoxItem item = new ComboBoxItem();
				item.Content = Properties.Resources.labelCtrlSyncSchedule;
				item.DataContext = TaskControlSchedules.CtrlSyncSchedule;
				cbControlList.Items.Add(item);
			}

            //	이벤트 볼륨 조절
            {
                ComboBoxItem item = new ComboBoxItem();
                item.Content = Properties.Resources.labelCtrlEventVolume;
                item.DataContext = TaskControlSchedules.CtrlEventVolume;
                cbControlList.Items.Add(item);
            }

            //	자막 설정
            {
                ComboBoxItem item = new ComboBoxItem();
                item.Content = Properties.Resources.labelCtrlSubtitle;
                item.DataContext = TaskControlSchedules.CtrlSubtitle;
                cbControlList.Items.Add(item);
            }
            
			//	모니터 전원 켜기
			{
				ComboBoxItem item = new ComboBoxItem();
				item.Content = Properties.Resources.labelCtrlSerialPowerOn;
				item.DataContext = TaskControlSchedules.CtrlSerialPowerOn;
				cbControlList.Items.Add(item);
			}

			//	모니터 전원 종료
			{
				ComboBoxItem item = new ComboBoxItem();
				item.Content = Properties.Resources.labelCtrlSerialPowerOff;
				item.DataContext = TaskControlSchedules.CtrlSerialPowerOff;
				cbControlList.Items.Add(item);
			}

			//	모니터 자동 조절
			{
				ComboBoxItem item = new ComboBoxItem();
				item.Content = Properties.Resources.labelCtrlSerialAuto;
				item.DataContext = TaskControlSchedules.CtrlSerialAuto;
				cbControlList.Items.Add(item);
			}

			//	모니터 TV 소스 선택
			{
				ComboBoxItem item = new ComboBoxItem();
				item.Content = Properties.Resources.labelCtrlSerialSourceGeneral;
				item.DataContext = TaskControlSchedules.CtrlSerialSourceTV;
				cbControlList.Items.Add(item);
			}

			//	모니터 Component 소스 선택
			{
				ComboBoxItem item = new ComboBoxItem();
				item.Content = Properties.Resources.labelCtrlSerialSourceComponent;
				item.DataContext = TaskControlSchedules.CtrlSerialSourceComponent;
				cbControlList.Items.Add(item);
			}

			//	모니터 RGB 소스 선택
			{
				ComboBoxItem item = new ComboBoxItem();
				item.Content = Properties.Resources.labelCtrlSerialSourceRGB;
				item.DataContext = TaskControlSchedules.CtrlSerialSourceRGB;
				cbControlList.Items.Add(item);
			}

			//	모니터 DVI 소스 선택
			{
				ComboBoxItem item = new ComboBoxItem();
				item.Content = Properties.Resources.labelCtrlSerialSourceDVI;
				item.DataContext = TaskControlSchedules.CtrlSerialSourceDVI;
				cbControlList.Items.Add(item);
			}

			//	모니터 볼륨 조절
			{
				ComboBoxItem item = new ComboBoxItem();
				item.Content = Properties.Resources.labelCtrlSerialVolume;
				item.DataContext = TaskControlSchedules.CtrlSerialVolume;
				cbControlList.Items.Add(item);
			}
            
             * */
            

// 			cbControlList.Items.Add(Properties.Resources.labelCtrlSerialSourceHDMI);
// 			cbControlList.Items.Add(Properties.Resources.labelCtrlSerialSourceSVideo);
// 			cbControlList.Items.Add(Properties.Resources.labelCtrlSerialSourceComposite);

			cbControlList.SelectedIndex = 0;

			#endregion

			DateTime now = DateTime.Now.AddHours(1);
			endtimeofdayUI.SelectedHour = starttimeofdayUI.SelectedHour = now.Hour;
			endtimeofdayUI.SelectedMinute = 30;
		}
		
		#region EventHandler

		private Collection<XmlNode> PrepareCheckedNode()
		{
			Collection<XmlNode> returnData = new Collection<XmlNode>();
			ServerDataCache insCache = ServerDataCache.GetInstance;

			try
			{
				if (checkedItems != null)
				{
					foreach (TreeViewItem item in checkedItems)
					{
						returnData.Add(item.DataContext as XmlNode);
					}
				}
			}
			catch { returnData = null; }

			return returnData;
		}

		private void btnTarget_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                GroupPicker dlg = new GroupPicker(cfg, true, true);
				dlg.Owner = parentwindow;
				dlg.TargetObject = GetTargetToXmlNode();

                dlg.ShowDialog();
                if (dlg.result != null)
                    TargetObject = dlg.result;
            }
            catch (Exception err)
            {
                logger.Error(err + "");
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
				switch ((_category)categoryUI.SelectedIndex)
                {
					case _category.taskRS232C:
                        {

                        }
                        break;
					case _category.taskProgram:
					case _category.taskDefaultSchedule:
                        System.Windows.Forms.OpenFileDialog ofd = new System.Windows.Forms.OpenFileDialog();
                        ofd.Filter = "Supported files(*.ipr, *.bin, *.smil, *.xml, *.tpf)|*.ipr;*.bin;*.smil;*.xml;*.tpf";//Touch - Touch file 확장자 추가
                        System.Windows.Forms.DialogResult res = ofd.ShowDialog();
                        
                        string pid = "";

                        if (executeSyncCheckBox.IsChecked == true)
                        {
                            var os = (FrameworkElement)e.OriginalSource;
                            var targetitem = os.DataContext as ProjectItem;                           

                            pid = targetitem.Pid;
                        }


                        if (res == System.Windows.Forms.DialogResult.OK)
                        {
                            if (ofd.FileName.ToLower().EndsWith(".bin"))
                                SetInformationFromBIN(ofd.FileName);
							else if (ofd.FileName.ToLower().EndsWith(".smil"))
							{
								#region SMIL
								Smil.Library.Smil smil = Smil.Library.Smil.LoadObjectFromSmilFile(ofd.FileName);

								bool bAuthor = false;

								#region 초기화
								
								tbIsAd.Text = "Unknown";
								_isAd = false;
								this.gridIncluedAdScreen.Visibility = System.Windows.Visibility.Collapsed;

								tbAdConstrait.Text = Properties.Resources.labelAdConstraintNonEvent;
								_constrait = (int)DigitalSignage.Common.ShowFlag.Show_NonEvent;
								
								DateTime start = DateTime.MinValue;
								DateTime end = DateTime.MaxValue;

								tbAdTimeRange.Text = String.Empty;

								tbAdMetaTags.Text = "Unknown";
								_metaTags = String.Empty;

								#endregion

								foreach (Smil.Library.Meta meta in smil.head.arrMetaCollection)
								{
									if (meta.name.Equals("author"))
									{
										if (!meta.content.Equals(Properties.Resources.titleMetaKey))
										{
											MessageBox.Show(Properties.Resources.mbNotSupportedSMILFile, Properties.Resources.titleMessageBox, MessageBoxButton.OK, MessageBoxImage.Error);
											throw new NotSupportedException(Properties.Resources.mbNotSupportedSMILFile);
										}
										bAuthor = true;
									}
									else if (meta.name.Equals("title"))
									{
                                        if(nameUI.Text == "새로운 스케줄")
										    nameUI.Text = meta.content;
									}
									#region 광고 여부 추출
									else if (meta.name.Equals("isAd"))
									{
										_isAd = Convert.ToBoolean(meta.content);

										tbIsAd.Text = _isAd ? "Y" : "N";
										this.gridIncluedAdScreen.Visibility = _isAd ? Visibility.Visible : Visibility.Collapsed;
									}
									#endregion

									#region 재생 조건 추출
									else if (meta.name.Equals("showFlag"))
									{
										_constrait = Convert.ToInt32(meta.content);

										tbAdConstrait.Text = ConvertPlayConstraitToString(_constrait);
									}
									#endregion

									#region 광고 시간 추출
									else if (meta.name.Equals("AdStartTime"))
									{
										start = Convert.ToDateTime(meta.content);
									}
									else if (meta.name.Equals("AdEndTime"))
									{
										end = Convert.ToDateTime(meta.content);
										
										if (_isAd)
										{
											tbAdConstrait.Text = ConvertPlayConstraitToString(_constrait);

											tbAdTimeRange.Text = String.Format("{0} - {1}", start.ToString(), end.ToString());

											this.startDateUI.SelectedDateTime = new DateTime(start.Year, start.Month, start.Day);
											this.finDateUI.SelectedDateTime = new DateTime(end.Year, end.Month, end.Day);

											this.startTimeUI.SelectedTime = new TimeSpan(start.Hour, start.Minute, start.Second);
											this.finTimeUI.SelectedTime = new TimeSpan(end.Hour, end.Minute, end.Second);
										}
									}
									#endregion

									#region 광고 메타태그 추출
									else if (meta.name.Equals("MetaTags"))
									{
										_metaTags = tbAdMetaTags.Text = meta.content;
									}
									#endregion
								}
								if (!bAuthor)
								{
									MessageBox.Show(Properties.Resources.mbNotSupportedSMILFile, Properties.Resources.titleMessageBox, MessageBoxButton.OK, MessageBoxImage.Error);
									throw new NotSupportedException(Properties.Resources.mbNotSupportedSMILFile);
								}

								SetDurationFromSmil(ofd.FileName);
								#endregion
							}
                            else if (ofd.FileName.ToLower().EndsWith(".tpf"))//Touch - Touch file load
                                duration.SelectedTime = (TimeSpan)TouchManager.GetTouchProjectInfoFromTouchFile(ofd.FileName, "PlayTime"); //SetDurationFromTouchFile(ofd.FileName);
                            else
                            {
                                #region 프로그램 파일 검증
                                XmlDocument document = new XmlDocument();
                                document.Load(ofd.FileName);
                                if (null == document.SelectSingleNode("child::program") ||
                                    null == document.SelectSingleNode("descendant::pane") || null == document.SelectSingleNode("descendant::pane").Attributes["playlist"])
                                {
                                    MessageBox.Show(Properties.Resources.mbErrorProgramFile, Properties.Resources.titleMessageBox, MessageBoxButton.OK, MessageBoxImage.Error);
                                    throw new Exception();
                                }
                                #endregion

                                SetDurationFromIPR(ofd.FileName);
                            }

                            if (executeSyncCheckBox.IsChecked != true)
                            {

                                projectProgram = ofd.FileName;
                                projectUI.Text = projectProgram;
                            }
                            else
                            {
                                if(itemList.Count >0)
                                {
                                    foreach (ProjectItem item in itemList)
                                    {
                                        if (item.Pid == pid)
                                        {
                                            item.ProjectUI = ofd.FileName;                                            
                                            break;
                                        }

                                    }

                                    UpdatePidList(false);

                                }
                            }


                        }
                        break;
					case _category.taskSubtitle:
                        TextEditor editor = new TextEditor();
						editor.Owner = parentwindow;
						if (subtilesProgram != null)
						{
							if (bIsEditUI)
							{
								try
								{
									Guid _duuid = new Guid(subtilesProgram);

									if (_duuid != null)
									{
										if (!DownloadSubtitle(_duuid))
										{
											editor.ShowDialog();
										}
										else
										{
											editor.Load(subtilesProgram);
										}
									}
								}
								catch { 
									if(!String.IsNullOrEmpty(subtilesProgram))
										editor.Load(subtilesProgram);
 									else
										editor.ShowDialog();
								}
							}
							else
							{
								editor.Load(subtilesProgram);
							}
						}
						else
							editor.ShowDialog();

						subtilesProgram = editor.GetDocument();
						projectUI.Text = subtilesProgram;

						break;
				}
            }
            catch (Exception err)
            {
                logger.Error(err + "");
            }
        }

		private bool DownloadSubtitle(Guid duuid)
		{
			XmlNode nodeGroup = null;
			if (TargetObject as XmlNode != null && ((XmlNode)TargetObject).LocalName.Equals("group"))
				nodeGroup = TargetObject as XmlNode;

			else if (TargetObject as XmlNode != null && ((XmlNode)TargetObject).LocalName.Equals("player"))
			{
				XmlNode player = (XmlNode)TargetObject;
				nodeGroup = ServerDataCache.GetInstance.GetGroupByPID(player.Attributes["pid"].Value);
			}
			else
			{
				Collection<TreeViewItem> items = TargetObject as Collection<TreeViewItem>;
				if (items == null) return false;

				foreach (TreeViewItem item in items)
				{
					try
					{
						if (item.DataContext == null)
							continue;
						else if (item.DataContext as XmlNode != null && ((XmlNode)item.DataContext).LocalName.Equals("group"))
						{	
							nodeGroup = item.DataContext as XmlNode;
							break;
						}
						else if (item.DataContext as XmlNode != null && ((XmlNode)item.DataContext).LocalName.Equals("player"))
						{
							XmlNode player = (XmlNode)item.DataContext;
							if (!IsExistInGroupArray(player.Attributes["gid"].Value))
							{
								nodeGroup = ServerDataCache.GetInstance.GetGroupByPID(player.Attributes["pid"].Value);
								break;
							}

						}
					}
					catch (Exception /*ex*/)
					{
						continue;
					}
				}
			}

			if(nodeGroup != null)
			{
				//	스케줄 다운로드
				String sExecutingPath = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\SchedulerData\\temp\\" + duuid.ToString() + "\\";
				TaskDownloader downloader = new TaskDownloader(cfg, nodeGroup, duuid, sExecutingPath);
				
				if(true == downloader.ShowDialog())
				{
					//	처리
					TextReader tw = new StreamReader(sExecutingPath + "program.xml");
					this.projectUI.Text = subtilesProgram = tw.ReadToEnd();
					tw.Close();

					return true;
				}
			}

			return false;
		}

		private void SetInformationFromBIN(string path)
		{
			try
			{
				FileStream fileStream = new FileStream(path, FileMode.Open, FileAccess.Read);
                BinaryFormatter binaryFormatter = new BinaryFormatter();
                List<Dictionary<string, object>> propertiesSet = new List<Dictionary<string, object>>();

				#region Duration 추출
				try
                {
                    propertiesSet = binaryFormatter.Deserialize(fileStream) as List<Dictionary<string, object>>;
					duration.SelectedTime = (TimeSpan)propertiesSet[0]["PlayTime"];
                }
                catch
                {
				}
				#endregion

				#region 광고 여부 추출
				try
				{
					_isAd = (bool)propertiesSet[0]["isAd"];

					tbIsAd.Text = _isAd ? "Y" : "N";
					this.gridIncluedAdScreen.Visibility = _isAd ? Visibility.Visible : Visibility.Collapsed; 
				}
				catch
				{
					tbIsAd.Text = "Unknown";
					_isAd = false;
					this.gridIncluedAdScreen.Visibility = System.Windows.Visibility.Collapsed;
				}
				#endregion

				#region 재생 조건 추출
				try
				{
					_constrait = (int)propertiesSet[0]["showFlag"];

					tbAdConstrait.Text = ConvertPlayConstraitToString(_constrait);
				}
				catch
				{
					tbAdConstrait.Text = Properties.Resources.labelAdConstraintNonEvent;
					_constrait = (int)DigitalSignage.Common.ShowFlag.Show_NonEvent;
				}
				#endregion

				#region 광고 시간 추출
				try
				{
                    if (_isAd)
                    {
                        DateTime start = (DateTime)propertiesSet[0]["AdStartTime"];
					    DateTime end = (DateTime)propertiesSet[0]["AdEndTime"];

					    tbAdTimeRange.Text = String.Format("{0} - {1}", start.ToString(), end.ToString());

                        this.startDateUI.SelectedDateTime = new DateTime(start.Year, start.Month, start.Day);
                        this.finDateUI.SelectedDateTime = new DateTime(end.Year, end.Month, end.Day);

                        this.startTimeUI.SelectedTime = new TimeSpan(start.Hour, start.Minute, start.Second);
                        this.finTimeUI.SelectedTime = new TimeSpan(end.Hour, end.Minute, end.Second);
                    }
				}
				catch
				{
					tbAdTimeRange.Text = "Unknown";
				}
				#endregion

				#region 광고 메타태그 추출
				try
				{
					_metaTags = tbAdMetaTags.Text = (String)propertiesSet[0]["MetaTags"];
				}
				catch
				{
					tbAdMetaTags.Text = "Unknown";
					_metaTags = String.Empty;
				}
				#endregion

				fileStream.Close();
 
			}
			catch
			{

			}
		}
		private void SetDurationFromSmil(string path)
		{
			try
			{

				duration.SelectedTime = Smil.Library.Smil.LoadObjectFromSmilFile(path).TotalDuration();
			}
			catch
			{
			}
		}

		/// <summary>
		/// 재생 조건을 문자열로 변환
		/// </summary>
		/// <param name="_constrait"></param>
		/// <returns></returns>
		private String ConvertPlayConstraitToString(int _con)
		{
			String sReturn = String.Empty;
			if ((_con & (int)ShowFlag.Show_NonEvent) > 0)
			{
				sReturn = Properties.Resources.labelAdConstraintNonEvent;
			}

			if ((_con & (int)ShowFlag.Show_Event) > 0)
			{
				if (String.IsNullOrEmpty(sReturn))
					sReturn = Properties.Resources.labelAdConstraintEvent;
				else
					sReturn = String.Format("{0}, {1}", sReturn, Properties.Resources.labelAdConstraintEvent);
			}

			if (String.IsNullOrEmpty(sReturn))
			{
				sReturn = Properties.Resources.labelAdConstraintNone;
			}

			return sReturn;
		}

		private void SetDurationFromIPR(string path)
		{
			try
			{
				XmlDocument doc = new XmlDocument();
				doc.Load(path);

				TimeSpan ts = new TimeSpan(0, 0, 0, 0);
				XmlNode programNode = doc.SelectSingleNode("//child::program");
				duration.SelectedTime = TimeConverter.StrToTime(programNode.Attributes["time"].Value);

			}
			catch
			{

			}
		}
        // program type change event handler
        private void categoryUI_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (projectUI == null) return;
                projectUI.Text = projectProgram = "";

				duration.IsEnabled = false;
				gridDuration.Visibility = Visibility.Collapsed;

				startDateUI.IsEnabled = true;
				finDateUI.IsEnabled = true;
				startTimeUI.IsEnabled = true;
				finTimeUI.IsEnabled = true;
				btnSelectProject.Visibility = Visibility.Visible;
				projectUI.Visibility = Visibility.Visible;
				cbControlList.Visibility = Visibility.Hidden;
				cbApplyImmediate.Visibility = Visibility.Hidden;
				nameUI.IsReadOnly = false;

				spRepeatOpts.IsEnabled = true;
				spRepeatOpts.Visibility = Visibility.Visible;
				
				cmbRepeatUI.SelectedIndex = 0;

				gridDuplication.Visibility = Visibility.Hidden;
                this.programGrid.Visibility = System.Windows.Visibility.Visible;
                this.fileTransmissionGroupBox.Visibility = System.Windows.Visibility.Collapsed;

				ComboBoxItem item = categoryUI.SelectedItem as ComboBoxItem;
				switch ((_category)item.DataContext)
				{
					case _category.taskRS232C:
                        if (projectRS232 != null)
                            projectUI.Text = projectRS232.name;
                        break;
// 					case _category.taskSubtitle:
//                         if (projectProgram != null)
//                             projectUI.Text = projectProgram;
	
                        break;
					case _category.taskProgram:
						/* hsshin 시간 스케줄 반복 주기가 필요함 */
						duration.IsEnabled = true;
						gridDuration.Visibility = Visibility.Visible;
						gridDuplication.Visibility = Visibility.Visible;
						break;
					case _category.taskDefaultSchedule:
						duration.IsEnabled = true;
						gridDuration.Visibility = Visibility.Visible;
						startDateUI.IsEnabled = false;
						finDateUI.IsEnabled = false;
						startTimeUI.IsEnabled = false;
						finTimeUI.IsEnabled = false;
						spRepeatOpts.IsEnabled = false;
						spRepeatOpts.Visibility = Visibility.Collapsed;

						break;

					case _category.taskControl:
						cbControlList.Visibility = Visibility.Visible;
						cbApplyImmediate.Visibility = Visibility.Visible;
						projectUI.Visibility = Visibility.Hidden;
						btnSelectProject.Visibility = Visibility.Hidden;
						nameUI.IsReadOnly = true;
						UpdateControlScheduleTitle();
						break;
						
                }
            }
            catch (Exception err)
            {
                logger.Error(err + "");
            }
        }
	
		#endregion

		#region Properties

		public TaskControlSchedules SelectedCtrlSched
		{
			get 
			{
				try
				{
					ComboBoxItem item = cbControlList.SelectedItem as ComboBoxItem;
					return (TaskControlSchedules)item.DataContext;
				}
				catch 
				{
					return TaskControlSchedules.CtrlUndefined;
				}
			}

			set
			{
				foreach(ComboBoxItem item in cbControlList.Items)
				{
					try
					{
						if (item.DataContext.Equals(value))
						{
							item.IsSelected = true;
							break;
						}
					}
					catch
					{
					}

				}
			}
		}

		public bool CanSelectType
		{
			set 
			{
				categoryUI.IsEnabled = value;
			}
		}
		public Config Cfg
		{
			set 
			{
				cfg = value;
			}
		}
		public Window ParentWindow
		{
			set
			{
				parentwindow = value;
			}
		}
		public string TaskName
		{
			get
			{
				return nameUI.Text;
			}
			set
			{
				nameUI.Text = value;
			}
		}
		public object GetTargetToXmlNode()
		{
				if (selectedTarget != null)
					return selectedTarget;
				else
					return PrepareCheckedNode();
		}

		public object TargetObject
		{
			get
			{
				if (selectedTarget != null)
					return selectedTarget;
				else
					return checkedItems;
			}
			set
			{
				if (value == null)
				{
					selectedTarget = null;
					targetUI.Text = "";
				}
				else
				{
					if (value as XmlNode != null)
					{
						checkedItems = null;
						selectedTarget = value as XmlNode;
						targetUI.Text = selectedTarget.Attributes["name"].Value;

                        itemList.Clear();
                        if (selectedTarget.Name == "group")
                        {
                            //group id로 하위 pid를 검색해서 첫번째 아이템을 master로 설정한다                            

                            foreach (XmlNode player in selectedTarget.SelectNodes("descendant::player[attribute::del_yn=\"N\"]"))
                            {
                                ProjectItem newitem = new ProjectItem();
                                newitem.Pid = newitem.TargetUI = player.Attributes["pid"].Value;
                                try
                                {
                                    newitem.TargetUI = player.Attributes["name"].Value;
                                }
                                catch { }
                                newitem.GroupID = player.ParentNode.Attributes["gid"].Value;

                                if (itemList.Count == 0)
                                {
                                    newitem.IsMaster = true;
                                }

                                itemList.Add(newitem);

                            }
                        }
                        else
                        {
                            ProjectItem newitem = new ProjectItem();
                            newitem.Pid = newitem.TargetUI = selectedTarget.Attributes["pid"].Value;
                            try
                            {
                                newitem.TargetUI = selectedTarget.Attributes["name"].Value;
                            }
                            catch { }
                            newitem.GroupID = selectedTarget.ParentNode.Attributes["gid"].Value;
                            newitem.IsMaster = true;
                            itemList.Add(newitem);
                        }

					}
					else
					{
						selectedTarget = null;
						checkedItems = value as Collection<TreeViewItem>;

						if (checkedItems != null)
						{
							string sCaption = "";

                            itemList.Clear();
                            

							foreach (TreeViewItem item in checkedItems)
							{
								if (!String.IsNullOrEmpty(sCaption))
								{
									sCaption += ", ";
								}
								try
								{
									if (item.DataContext == null)
										continue;
                                    else if (item.DataContext as XmlNode != null)
                                    {
                                        sCaption += ((XmlNode)item.DataContext).Attributes["name"].Value;
                                        //이때 item의 pid로 스켸줄 리스팅하게 하고
                                        //item list를 구성하게 한다.
                                        //bmwe3
                                        if(((XmlNode)item.DataContext).Name == "group")
                                        {
                                            //group id로 하위 pid를 검색해서 첫번째 아이템을 master로 설정한다
                                            XmlNode groupnode = item.DataContext as XmlNode;

                                            foreach (XmlNode player in groupnode.SelectNodes("descendant::player[attribute::del_yn=\"N\"]"))
                                            {
                                                ProjectItem newitem = new ProjectItem();
                                                newitem.Pid = newitem.TargetUI = player.Attributes["pid"].Value;
                                                try
                                                {
                                                    newitem.TargetUI = player.Attributes["name"].Value;
                                                }
                                                catch { }
                                                newitem.GroupID = player.ParentNode.Attributes["gid"].Value;

                                                if (itemList.Count == 0)
                                                {
                                                    newitem.IsMaster = true;
                                                }

                                                itemList.Add(newitem);

                                                //pidList.Add(player.Attributes["pid"].Value);
                                                //player.ParentNode.Attributes["gid"].Value
                                            }
                                            break;
                                        }
                                        else
                                        {

                                            ProjectItem newitem = new ProjectItem();
                                            newitem.Pid = newitem.TargetUI = ((XmlNode)item.DataContext).Attributes["pid"].Value;
                                            try
                                            {
                                                newitem.TargetUI = ((XmlNode)item.DataContext).Attributes["name"].Value;
                                            }
                                            catch { }
                                            newitem.GroupID = ((XmlNode)item.DataContext).ParentNode.Attributes["gid"].Value;

                                            if (itemList.Count == 0)
                                            {
                                                newitem.IsMaster = true;
                                            }

                                            itemList.Add(newitem);

                                            //pidList.Add(((XmlNode)item.DataContext).Attributes["pid"].Value);
                                        }
                                        
                                    }
								}
								catch
								{
								}
							}

							targetUI.Text = sCaption;

                            if (executeSyncCheckBox.IsChecked == true && itemList.Count > 0)
                            {
                                SetProjectUI(true);                                
                            }
                            else
                            {
                                SetProjectUI(false);
                            }
						}
					}
				}
			}
		}

        private void SetProjectUI(bool value)
        {
            if (value == true)
            {
                projectUI.Visibility = System.Windows.Visibility.Collapsed;
                btnSelectProject.Visibility = System.Windows.Visibility.Collapsed;
                //cbControlList.Visibility = System.Windows.Visibility.Collapsed;
                cbApplyImmediate.Visibility = System.Windows.Visibility.Collapsed;
                ui_splList.Visibility = System.Windows.Visibility.Visible;

                UpdatePidList(true);

            }
            else
            {
                projectUI.Visibility = System.Windows.Visibility.Visible;
                btnSelectProject.Visibility = System.Windows.Visibility.Visible;
                //cbControlList.Visibility = System.Windows.Visibility.Visible;
                //cbApplyImmediate.Visibility = System.Windows.Visibility.Visible;
                ui_splList.Visibility = System.Windows.Visibility.Collapsed; ;
            }

        }

        private void MasterRdB_Click(object sender, RoutedEventArgs e)
        {
            var os = (FrameworkElement)e.OriginalSource;
            var targetitem = os.DataContext as ProjectItem;


            if (itemList.Count > 0)
            {
                foreach (ProjectItem item in itemList)
                {
                    if (item.Pid == targetitem.Pid)
                    {
                        item.IsMaster = true;

                    }
                    else
                    {
                        item.IsMaster = false;

                    }
                }

                UpdatePidList(false);

            }


        }

        private void UpdatePidList(bool totalupdate)
        {
            if (totalupdate == true)
            {

                //itemList.Clear();

                //bool isMaster = false;

                //foreach (string pid in pidList)
                //{
                //    XmlNode targetplayer = ServerDataCache.GetInstance.GetPlayer(pid);

                //    ProjectItem item = new ProjectItem();

                //    item.Pid = targetplayer.Attributes["pid"].Value;
                //    item.TargetUI = targetplayer.Attributes["pid"].Value;

                //    if (isMaster != true)
                //    {
                //        item.IsMaster = true;
                //        isMaster = true;
                //    }

                //    itemList.Add(item);
                //}
            }

            ui_lbxProg.ItemsSource = null;
            ui_lbxProg.ItemsSource = itemList;

        }

		public int ProgramType
		{
			get
			{
				ComboBoxItem item = categoryUI.SelectedItem as ComboBoxItem;

				if (item == null) return -1;

				if (item.DataContext == null) return -1;

				return (int)item.DataContext;
			}
			set
			{
                if (value != 0)
                {
                    ui_grdsyncCheck.Visibility = System.Windows.Visibility.Collapsed;
                }

				foreach (ComboBoxItem item in categoryUI.Items)
				{
					if ((int)item.DataContext == value)
					{
						item.IsSelected = true;
						break;
					}
				}
			}
		}

		public FarsiLibrary.FX.Win.Controls.FXDatePicker StartDateUI
		{
			get { return startDateUI; }
		}

		public AC.AvalonControlsLibrary.Controls.TimePicker StartTimeUI
		{
			get { return startTimeUI; }
		}

		public FarsiLibrary.FX.Win.Controls.FXDatePicker FinDateUI
		{
			get { return finDateUI; }
		}

		public AC.AvalonControlsLibrary.Controls.TimePicker FinTimeUI
		{
			get { return finTimeUI; }
		}

		/// <summary>
		/// 일일 시작 시간 
		/// </summary>
		public int StartTimeOfDayField
		{
			get {
				try
				{
					_repeat_options option = (_repeat_options)cmbRepeatUI.SelectedIndex;
					if (option == _repeat_options.norepeat || 
						(cbApplyImmediate.Visibility == Visibility.Visible && cbApplyImmediate.IsChecked.Value)) return 0;

					return Convert.ToInt32(this.starttimeofdayUI.SelectedTime.TotalSeconds);
				}
				catch
				{
					return 0;
				}
			}
			set { this.starttimeofdayUI.SelectedTime = TimeSpan.FromSeconds(value); }
		}

		/// <summary>
		/// 일일 끝시간
		/// </summary>
		public int EndTimeOfDayField
		{
			get {
				try
				{
					_repeat_options option = (_repeat_options)cmbRepeatUI.SelectedIndex;
					if (option == _repeat_options.norepeat || 
						(cbApplyImmediate.Visibility == Visibility.Visible && cbApplyImmediate.IsChecked.Value)) return 0;

					if(this.endtimeofdayUI.IsEnabled)
						return Convert.ToInt32(this.endtimeofdayUI.SelectedTime.TotalSeconds);
					else
						return Convert.ToInt32(this.starttimeofdayUI.SelectedTime.TotalSeconds) + 30;
				}
				catch
				{
					return 0;
				}
			}
			set { this.endtimeofdayUI.SelectedTime = TimeSpan.FromSeconds(value); }
		}

		/// <summary>
		/// 요일 필드
		/// </summary>
		public int DayOfWeekField
		{
			get
			{
				try
				{
					_repeat_options option = (_repeat_options)cmbRepeatUI.SelectedIndex;

					if (option != _repeat_options.daysofweek) return 127;

					int dayofweek = 0;
					if (btnSun.IsChecked.Value)
					{
						dayofweek |= (int)TaskApplyDayOfWeek.Day_SUN;
					}
					if (btnMon.IsChecked.Value)
					{
						dayofweek |= (int)TaskApplyDayOfWeek.Day_MON;
					}
					if (btnTue.IsChecked.Value)
					{
						dayofweek |= (int)TaskApplyDayOfWeek.Day_TUE;
					}
					if (btnWed.IsChecked.Value)
					{
						dayofweek |= (int)TaskApplyDayOfWeek.Day_WED;
					}
					if (btnThu.IsChecked.Value)
					{
						dayofweek |= (int)TaskApplyDayOfWeek.Day_THU;
					}
					if (btnFri.IsChecked.Value)
					{
						dayofweek |= (int)TaskApplyDayOfWeek.Day_FRI;
					}
					if (btnSat.IsChecked.Value)
					{
						dayofweek |= (int)TaskApplyDayOfWeek.Day_SAT;
					}

					return dayofweek;
				}
				catch
				{
					//	전체 체크
					return 127;
				}
			}
			set
			{
				try
				{
					btnSun.IsChecked = (value & (int)TaskApplyDayOfWeek.Day_SUN) > 0;
					btnMon.IsChecked = (value & (int)TaskApplyDayOfWeek.Day_MON) > 0;
					btnTue.IsChecked = (value & (int)TaskApplyDayOfWeek.Day_TUE) > 0;
					btnWed.IsChecked = (value & (int)TaskApplyDayOfWeek.Day_WED) > 0;
					btnThu.IsChecked = (value & (int)TaskApplyDayOfWeek.Day_THU) > 0;
					btnFri.IsChecked = (value & (int)TaskApplyDayOfWeek.Day_FRI) > 0;
					btnSat.IsChecked = (value & (int)TaskApplyDayOfWeek.Day_SAT) > 0;
				}
				catch
				{
				}
			}
		}
		#endregion

		#region internal routines
		public int QueryMinimumPriorityFromGID(String gid)
		{
			return cfg.ServerTasksList.GetMinPriorityFromDefaultTasks(gid);
		}

		#region Create Task
		public int createTask(DateTime from, DateTime till, string name)
		{
			if (name.Equals(TaskControlSchedules.CtrlPowerOn.ToString()))
			{
				if (MessageBoxResult.Cancel == MessageBox.Show(Properties.Resources.mbWarnPowerOn, Properties.Resources.titleScheduler, MessageBoxButton.OKCancel, MessageBoxImage.Warning))
					return -2;
			}

			//	hsshin 반복 조건에 따른 스케줄 시간 리스트를 만든다.
			if (!makeRepeatInformation(from, till) || arrDurations.Count == 0)
			{
				MessageBox.Show(Properties.Resources.mbErrorDataInsufficiency, Properties.Resources.titleMessageBox, MessageBoxButton.OK, MessageBoxImage.Stop);
				return -1;
			}

			Guid duuid = Guid.NewGuid();

			if (bIsEditUI && projectUI.Text.Equals(OldTask.duuid.ToString()))
			{
				duuid = OldTask.duuid;
			}

			TaskCommands command = TaskCommands.CmdUndefined;

			ComboBoxItem item = categoryUI.SelectedItem as ComboBoxItem;

			switch ((_category)item.DataContext)
			{
				case _category.taskProgram:
					{
						command = TaskCommands.CmdProgram;
                        
                        #region Touch - 시간 스케줄 Duration 시간 설정.
                        //this.duration.SelectedTime = till - from;
                        #endregion

						int nRet = createProgramTask(from, till, name, duuid);

						if (nRet < 0) return nRet;

					}
					break;
				case _category.taskRS232C:
					if (projectRS232 == null)
					{
						MessageBox.Show(Properties.Resources.mbSelectTaskProgram, Properties.Resources.titleMessageBox, MessageBoxButton.OK, MessageBoxImage.Stop);
						return -1;
					}
					duuid = new System.Guid((int)projectRS232.id,
						(short)((projectRS232.etime & 0xFFFF0000) >> 16),
						(short)(projectRS232.etime & 0xFFFF),
						new byte[] { 0, 0, 0, 0, 0, 0, 0, 0 });
					command = TaskCommands.CmdWriteRS232;
					break;
				case _category.taskSubtitle:
					{
						command = TaskCommands.CmdSubtitles;

						int nRet = createSubtitleTask(from, till, name, duuid);

						if (nRet < 0) return nRet;


					}
					break;
				case _category.taskDefaultSchedule:
					{
                        command = TaskCommands.CmdDefaultProgram;
                        #region 2012.02.20: 스케줄 편집시 Touch 스케줄을 구분하기 위한 코드. New & Edit  
                        if (this.EditMode == true)
                        {
                            if (TaskCommandType == TaskCommands.CmdTouchDefaultProgram)
                            {
                                command = TaskCommands.CmdTouchDefaultProgram;
                            }
                        }
                        else
                        {
                            if (TouchManager.IsTouchFile(this.projectProgram) == true)
                            {
                                command = TaskCommands.CmdTouchDefaultProgram;
                            }
                        }                       
                        #endregion

						
						int nRet = createDefaultTask(from, till, name, duuid);

						if (nRet < 0) return nRet;

					}
					break;
				case _category.taskControl:
					{
						command = TaskCommands.CmdControlSchedule;

                        TaskControlSchedules controltype = this.SelectedCtrlSched;
                        if (controltype == TaskControlSchedules.CtrlFileTransmit)
                        {
                            if (targetFilePath.Text.Length == 0 || false == File.Exists(targetFilePath.Text))
                            {
                                MessageBox.Show(Properties.Resources.mberrSelectFile, Properties.Resources.titleMessageBox, MessageBoxButton.OK, MessageBoxImage.Stop);
                                return -1;
                            }

                            int nRet = createControlTask(from, till, name, duuid);

                            if (nRet < 0)
                                return nRet;
                            FileInfo fileinfo = new FileInfo(targetFilePath.Text);

                            this._metaTags = String.Format("File[{0}],Run[{1}],Param[{2}],Folder[{3}],Subpath[{4}]", fileinfo.Name, (executeFileCheckBox.IsChecked == true) ? "Y" : "N", executeParameterTextBox.Text, (int)TaskFileTrans, txt_subfolder.Text);

                        }
                    }
					break;
			}
			return registerTask(TargetObject, duuid, bIsEditUI ? OldTask.guuid : Guid.NewGuid(), from, till, name, command);
		}
		
		private int createProgramTask(DateTime from, DateTime till, string name, Guid duuid)
		{
            List<string> programList = new List<string>();          

            bool reliable = false;

            if (executeSyncCheckBox.IsChecked != true && !String.IsNullOrEmpty(projectProgram))
            {
                reliable = true;
                programList.Add(projectProgram);                
            }

            if(executeSyncCheckBox.IsChecked == true)
            {
                if (itemList.Count > 0)
                {
                    foreach (ProjectItem item in itemList)
                    {
                        if (string.IsNullOrEmpty(item.ProjectUI) != true)
                        {
                            reliable = true;

                            programList.Add(item.ProjectUI);
                            
                        }

                    }
                }

            }

            if (reliable != true) 
			{
				MessageBox.Show(Properties.Resources.mbSelectTaskProgram, Properties.Resources.titleMessageBox, MessageBoxButton.OK, MessageBoxImage.Stop);
				return -2;
			}

            int result = 0;
            for (int i = 0; i < programList.Count; i++)
            {
                Guid uid = Guid.NewGuid();//new Guid(guidList[i]);
                if (executeSyncCheckBox.IsChecked != true)
                {
                    return UploadingTask(programList[i], duuid);

                }
                else
                {
                    result = UploadingTask(programList[i], uid);
                    if (result != 0)
                    {
                        //비정상
                        return result;
                    }

                    try
                    {
                        Guid tid = new Guid(programList[i]);

                        if (tid != null)
                        {
                            //bmwe3 - 컨텐트가 변경되지 않으면 ID유지
                            itemList[i].DownLoadID = tid.ToString();
                            continue;
                        }
                    }
                    catch { };                    
                    
                    //컨텐츠가 변경되면 새GUID할당
                    itemList[i].DownLoadID = uid.ToString();
                }
            }

            //정상적인 업로드 종료
            return 0;// 
            //return UploadingTask(programList, guidList);
		}

		private int createControlTask(DateTime from, DateTime till, string name, Guid duuid)
		{
            try
            {
                // creating temporary program directory
                String sExecutingPath = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                string tempDir = sExecutingPath + "\\SchedulerData\\temp\\" + duuid.ToString();
                if (!Directory.Exists(tempDir))
                    Directory.CreateDirectory(tempDir);

                FileInfo info = new FileInfo(targetFilePath.Text);
                tempDir = tempDir + "\\" + info.Name;
                File.Copy(targetFilePath.Text, tempDir);

                return UploadingTask(tempDir, duuid);
            }
            catch
            {
            }

			return 0;
		}

		private int createSubtitleTask(DateTime from, DateTime till, string name, Guid duuid)
		{
			if (String.IsNullOrEmpty(subtilesProgram))
			{
				MessageBox.Show(Properties.Resources.mbSelectTaskProgram, Properties.Resources.titleMessageBox, MessageBoxButton.OK, MessageBoxImage.Stop);
				return -2;
			}

			bool bIsUpload = false;
			try
			{
				Guid _duuid = new Guid(subtilesProgram);

				if (_duuid != null)
				{
					bIsUpload = false;
				}
			}
			catch { bIsUpload = true; };
			if (bIsUpload)
			{
				// creating temporary program directory
				String sExecutingPath = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
				string tempDir = sExecutingPath + "\\SchedulerData\\temp\\" + duuid.ToString();
				if (!Directory.Exists(tempDir))
					Directory.CreateDirectory(tempDir);
				TextWriter tw = new StreamWriter(tempDir + "\\program.xml");
				tw.Write(subtilesProgram);
				tw.Close();

				return UploadingTask(tempDir, duuid);
			}
			else return UploadingTask(subtilesProgram, duuid);
		}

		private int createDefaultTask(DateTime from, DateTime till, string name, Guid duuid)
		{
			if (projectProgram == null)
			{
				MessageBox.Show(Properties.Resources.mbSelectTaskProgram, Properties.Resources.titleMessageBox, MessageBoxButton.OK, MessageBoxImage.Stop);
				return -2;
			}

			return UploadingTask(projectProgram, duuid);
		}

		private bool IsExistInGroupArray(string gid)
		{
			foreach (XmlNode item in m_groups)
			{
				if (item.Attributes["gid"].Value.Equals(gid))
					return true;
			}
			return false;
		}

        private int SetPlayerSetting()
        {
            m_groups.Clear();

            if (TargetObject as XmlNode != null && ((XmlNode)TargetObject).LocalName.Equals("group"))
                m_groups.Add(TargetObject as XmlNode);
            else if (TargetObject as XmlNode != null && ((XmlNode)TargetObject).LocalName.Equals("player"))
            {
                XmlNode player = (XmlNode)TargetObject;
                XmlNode group = ServerDataCache.GetInstance.GetGroupByPID(player.Attributes["pid"].Value);
                m_groups.Add(group);
            }
            else
            {
                Collection<TreeViewItem> items = TargetObject as Collection<TreeViewItem>;
                if (items == null) return -1;

                foreach (TreeViewItem item in items)
                {
                    try
                    {
                        if (item.DataContext == null)
                            continue;
                        else if (item.DataContext as XmlNode != null && ((XmlNode)item.DataContext).LocalName.Equals("group"))
                            m_groups.Add(item.DataContext as XmlNode);
                        else if (item.DataContext as XmlNode != null && ((XmlNode)item.DataContext).LocalName.Equals("player"))
                        {
                            XmlNode player = (XmlNode)item.DataContext;
                            if (!IsExistInGroupArray(player.Attributes["gid"].Value))
                            {
                                XmlNode group = ServerDataCache.GetInstance.GetGroupByPID(player.Attributes["pid"].Value);
                                m_groups.Add(group);
                            }

                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(String.Format(Properties.Resources.mbErrorExcluded, ((GroupTreeItem)item.Header).Text, ex.Message));
                    }
                }
            }
            return 1;
        }

		private int UploadingTask(string srcDirectory, Guid duuid)
		{
            if (SetPlayerSetting() == -1)
                return -1;

			try
			{
				Guid _duuid = new Guid(srcDirectory);

				if (_duuid != null)
				{
					duuid = _duuid;
					return 0;
				}
			}
			catch { };

			try
			{
				uploader = new TaskUploader(srcDirectory, cfg, ref m_groups, duuid);
				if (uploader.ShowDialog() == false)
					return -1;

				this.UploadedScreens = uploader.UploadedScreens;
			}
			catch (WebException ee)
			{
				MessageBox.Show(ee.Message);
				return -1;
			}

			return 0;
		}

		#endregion
		Collection<Duration> arrDurations = new Collection<Duration>();

		class Duration
		{
			public Duration(long _start, long _end)
			{
				StartT = _start;
				EndT = _end;
				if (StartT >= EndT)
				{
					throw new Exception(Properties.Resources.mbWrongTime);
				}
			}

			public long StartT;
			public long EndT;
		}

		private bool makeRepeatInformation(DateTime from, DateTime till)
		{
			const int IMMEDIATE_TIME = 50;

			arrDurations.Clear();

			ComboBoxItem item = categoryUI.SelectedItem as ComboBoxItem;

			_category category = (_category)item.DataContext;
			//	반복 스케줄인 경우 반복을 넣을 수 없다
			if (category == _category.taskDefaultSchedule)
			{
				arrDurations.Add(new Duration(
					TimeConverter.ConvertToUTP(DateTime.MinValue),
				TimeConverter.ConvertToUTP(DateTime.MaxValue)));

				return true;
			}
			else
			{
				TaskControlSchedules control = this.SelectedCtrlSched;

				_repeat_options option = (_repeat_options)cmbRepeatUI.SelectedIndex;

				switch (option)
				{
					case _repeat_options.norepeat:
						{
							#region 반복 없음
							if (category == _category.taskControl)
							{
								//	즉시 적용이라면 즉시 만들어서 배포
								if (cbApplyImmediate.IsChecked.Value)
								{
                                    try
                                    {
                                        long server_ts = cfg.ServerCmdReceiver.ProcessSimpleCommand(CmdReceiverCommands.CmdGetTimeStamp);

                                        DateTime utcnow = TimeConverter.ConvertFromUTP(server_ts);
                                        arrDurations.Add(new Duration(
                                            TimeConverter.ConvertToUTP(utcnow),
										TimeConverter.ConvertToUTP(utcnow.AddSeconds(IMMEDIATE_TIME))));

                                    }
                                    catch
                                    {
                                        DateTime utcnow = DateTime.UtcNow;
                                        arrDurations.Add(new Duration(
                                            TimeConverter.ConvertToUTP(utcnow),
										TimeConverter.ConvertToUTP(utcnow.AddSeconds(IMMEDIATE_TIME))));
                                    }
                                    return true;
								}
								else
								{
									//	즉시적용이 아니라면 시간 입력
									if (control == TaskControlSchedules.CtrlVolume || control == TaskControlSchedules.CtrlSerialVolume)
									{
										//	볼륨인 경우 Range 입력
										arrDurations.Add(new Duration(
											TimeConverter.ConvertToUTP(from.ToUniversalTime()),
										TimeConverter.ConvertToUTP(till.ToUniversalTime())));
										return true;
									}
									else
									{
										//	볼륨이 아닌경우는 시작 시간만 입력
										arrDurations.Add(new Duration(
											TimeConverter.ConvertToUTP(from.ToUniversalTime()),
										TimeConverter.ConvertToUTP(from.AddSeconds(IMMEDIATE_TIME).ToUniversalTime())));
										return true;
									}
								}
							}
							else
							{
								//	제어 스케줄이 아닌경우
								arrDurations.Add(new Duration(
									TimeConverter.ConvertToUTP(from.ToUniversalTime()),
								TimeConverter.ConvertToUTP(till.ToUniversalTime())));

								return true;
							}
							#endregion
						}
					case _repeat_options.everydays:
						{

							#region old 매일 반복
							//	즉시적용이 아니라면 시간 입력
							DateTime starttime = new DateTime(from.Year, from.Month, from.Day, 0, 0, 0) + starttimeofdayUI.SelectedTime;
							DateTime endtime;
							TimeSpan duration;

							//	볼륨 제어가 아닌 제어스케줄인 경우
							if (category == _category.taskControl && control != TaskControlSchedules.CtrlVolume && control != TaskControlSchedules.CtrlSerialVolume)
							{
								duration = new TimeSpan(0, 0, IMMEDIATE_TIME);
								endtime = new DateTime(till.Year, till.Month, till.Day, 0, 0, 0)
									+ starttimeofdayUI.SelectedTime + duration;
							}
							else
							{
								endtime = new DateTime(till.Year, till.Month, till.Day, 0, 0, 0) + endtimeofdayUI.SelectedTime;
								duration = endtimeofdayUI.SelectedTime - starttimeofdayUI.SelectedTime;
							}

							arrDurations.Add(new Duration(TimeConverter.ConvertToUTP(starttime.ToUniversalTime()), TimeConverter.ConvertToUTP(endtime.ToUniversalTime() /*+ duration*/)));

// 
// 							//	등록
// 							DateTime iter = starttime;
// 
// 							while (iter <= endtime)
// 							{
// 								arrDurations.Add(new Duration(
// 									TimeConverter.ConvertToUTP(iter.ToUniversalTime()),
// 								TimeConverter.ConvertToUTP(iter.ToUniversalTime() + duration)));
// 
// 								iter += TimeSpan.FromDays(1);
// 							}
							#endregion
							return true;
						}
					case _repeat_options.daysofweek:
						{
							#region old 특정 요일 반복
							//	즉시적용이 아니라면 시간 입력
							DateTime starttime = new DateTime(from.Year, from.Month, from.Day, 0, 0, 0) + starttimeofdayUI.SelectedTime;
							DateTime endtime;
							TimeSpan duration;

							//	볼륨 제어가 아닌 제어스케줄인 경우
							if (category == _category.taskControl && control != TaskControlSchedules.CtrlVolume && control != TaskControlSchedules.CtrlSerialVolume)
							{
								duration = new TimeSpan(0, 0, IMMEDIATE_TIME);
								endtime = new DateTime(till.Year, till.Month, till.Day, 0, 0, 0)
									+ starttimeofdayUI.SelectedTime + duration;
							}
							else
							{
								endtime = new DateTime(till.Year, till.Month, till.Day, 0, 0, 0) + endtimeofdayUI.SelectedTime;
								duration = endtimeofdayUI.SelectedTime - starttimeofdayUI.SelectedTime;
							}

							arrDurations.Add(new Duration(TimeConverter.ConvertToUTP(starttime.ToUniversalTime()), TimeConverter.ConvertToUTP(endtime.ToUniversalTime()/* + duration*/)));

// 
// 							//	등록
// 							DateTime iter = starttime;
// 
// 							while (iter <= endtime)
// 							{
// 								if (!IsCheckedDayOfWeeks(iter.DayOfWeek))
// 								{
// 									iter += TimeSpan.FromDays(1);
// 									continue;
// 								}
// 
// 								arrDurations.Add(new Duration(
// 									TimeConverter.ConvertToUTP(iter.ToUniversalTime()),
// 								TimeConverter.ConvertToUTP(iter.ToUniversalTime() + duration)));
// 
// 								iter += TimeSpan.FromDays(1);
// 							}
							#endregion
							return true;

						}
					case _repeat_options.dayofmonth:
						{

							//	즉시적용이 아니라면 시간 입력
							DateTime starttime = new DateTime(from.Year, from.Month, from.Day, 0, 0, 0) + starttimeofdayUI.SelectedTime;
							DateTime endtime;
							TimeSpan duration;

							//	볼륨 제어가 아닌 제어스케줄인 경우
							if (category == _category.taskControl && control != TaskControlSchedules.CtrlVolume && control != TaskControlSchedules.CtrlSerialVolume)
							{
								duration = new TimeSpan(0, 0, IMMEDIATE_TIME);
								endtime = new DateTime(till.Year, till.Month, till.Day, 0, 0, 0)
									+ starttimeofdayUI.SelectedTime + duration;
							}
							else
							{
								endtime = new DateTime(till.Year, till.Month, till.Day, 0, 0, 0) + endtimeofdayUI.SelectedTime;
								duration = endtimeofdayUI.SelectedTime - starttimeofdayUI.SelectedTime;
							}

							//	등록
							DateTime iter = starttime;

							while (iter <= endtime)
							{
								int nSelectedDay = ckEndOfMonth.IsChecked.Value ? DateTime.DaysInMonth(iter.Year, iter.Month) : Convert.ToInt32(tbDayOfMonth.Text);

								if (nSelectedDay != iter.Day)
								{
									iter += TimeSpan.FromDays(1);
									continue;
								}

								arrDurations.Add(new Duration(
									TimeConverter.ConvertToUTP(iter.ToUniversalTime()),
								TimeConverter.ConvertToUTP(iter.ToUniversalTime() + duration)));

								iter += TimeSpan.FromDays(1);
							}
							return true;
						}
				}
			}
			return false;

		}

		private bool IsCheckedDayOfWeeks(DayOfWeek dayofweek)
		{
			try
			{
				switch (dayofweek)
				{
					case DayOfWeek.Sunday:
						return btnSun.IsChecked.Value;
					case DayOfWeek.Monday:
						return btnMon.IsChecked.Value;
					case DayOfWeek.Tuesday:
						return btnTue.IsChecked.Value;
					case DayOfWeek.Wednesday:
						return btnWed.IsChecked.Value;
					case DayOfWeek.Thursday:
						return btnThu.IsChecked.Value;
					case DayOfWeek.Friday:
						return btnFri.IsChecked.Value;
					case DayOfWeek.Saturday:
						return btnSat.IsChecked.Value;
				}
			}
			catch { }

			return false;
		}

		public void LoadTask(Task _task)
		{
			this.nameUI.Text = _task.description;
            TaskCommandType = _task.type;
            switch (TaskCommandType)
			{
				case TaskCommands.CmdDefaultProgram:
					this.ProgramType = (int)_category.taskDefaultSchedule;
					this.projectProgram = projectUI.Text = _task.duuid.ToString();
					break;
                case TaskCommands.CmdTouchDefaultProgram://2012.02.20: Touch 스케줄에 대해서도 고려.
                    this.ProgramType = (int)_category.taskDefaultSchedule;
					this.projectProgram = projectUI.Text = _task.duuid.ToString();
                    break; 
				case TaskCommands.CmdProgram:
                    if (((_task.show_flag & (int)ShowFlag.Show_SyncMaster) > 0 || (_task.show_flag &  (int)ShowFlag.Show_SyncSlave) > 0))
                    {
                        //싱크일때
                        //bmwe3 동기화일때 해당 Task로 엮인 Task를 모두 읽어오는 부분이 필요해지는데.
                        //_task.guuid 이용.
                        SYNC_TASK[] slist = cfg.ServerTasksList.GetSyncTask(_task.guuid.ToString());

                        if (slist != null)
                        {
                            itemList.Clear();
                            for (int i = 0; i < slist.Count(); i++)
                            {
                                ProjectItem pitem = new ProjectItem();
                                pitem.UUID = slist[i].UUID;
                                pitem.Pid = slist[i].PlayerID;
                                try
                                {
                                    pitem.TargetUI = ServerDataCache.GetInstance.GetPlayerName(pitem.Pid);
                                }
                                catch { }
                                pitem.GroupID = slist[i].GroupID;
                                //pitem.TargetUI = slist[i].PlayerID;
                                pitem.ProjectUI = slist[i].DUUID;
                                pitem.IsMaster = slist[i].MasterYN;

                                itemList.Add(pitem);
                            }

                            executeSyncCheckBox.IsChecked = true;
                            SetProjectUI(true);
                        }

                    }
                    else
                    {
                        this.ProgramType = (int)_category.taskProgram;
                        this.projectProgram = projectUI.Text = _task.duuid.ToString();
                    }
                    
					break;
				case TaskCommands.CmdSubtitles:
					this.ProgramType = (int)_category.taskSubtitle;
					this.subtilesProgram = projectUI.Text = _task.duuid.ToString();
					break;
				case TaskCommands.CmdControlSchedule:
					this.ProgramType = (int)_category.taskControl;
					break;                                 
				default:
					this.ProgramType = (int)_category.taskDefaultSchedule;
					this.projectProgram = projectUI.Text = _task.duuid.ToString();
					break;
			}

			DateTime dtStartTime = TimeConverter.ConvertFromUTP(_task.TaskStart).ToLocalTime();
			DateTime dtEndTime = TimeConverter.ConvertFromUTP(_task.TaskEnd).ToLocalTime();

			this.StartDateUI.SelectedDateTime = dtStartTime;
			this.FinDateUI.SelectedDateTime = dtEndTime;

			this.StartTimeUI.SelectedHour = dtStartTime.Hour;
			this.StartTimeUI.SelectedMinute = dtStartTime.Minute;
			this.StartTimeUI.SelectedSecond = dtStartTime.Second;

			this.FinTimeUI.SelectedHour = dtEndTime.Hour;
			this.FinTimeUI.SelectedMinute = dtEndTime.Minute;
			this.FinTimeUI.SelectedSecond = dtEndTime.Second;

			if (_task.starttimeofday != _task.endtimeofday)
			{
				if(_task.daysofweek == 127)
				{
					cmbRepeatUI.SelectedIndex = (int)_repeat_options.everydays;
				}
				else cmbRepeatUI.SelectedIndex = (int)_repeat_options.daysofweek;

				this.StartTimeOfDayField = _task.starttimeofday;
				this.EndTimeOfDayField = _task.endtimeofday;
			}
			else cmbRepeatUI.SelectedIndex = (int)_repeat_options.norepeat;


			this.duration.SelectedTime = TimeSpan.FromSeconds(_task.duration);


			#region 광고 여부 추출
			try
			{
				_isAd = _task.isAd;

				tbIsAd.Text = _isAd ? "Y" : "N";
				this.gridIncluedAdScreen.Visibility = _isAd ? Visibility.Visible : Visibility.Collapsed;
			}
			catch
			{
				tbIsAd.Text = "Unknown";
				_isAd = false;
				this.gridIncluedAdScreen.Visibility = System.Windows.Visibility.Collapsed;
			}
			#endregion

			#region 재생 조건 추출
			try
			{
				_constrait = _task.show_flag;
                _constrait &= ~(int)ShowFlag.Show_Allow_Duplication;

				tbAdConstrait.Text = ConvertPlayConstraitToString(_constrait);
			}
			catch
			{
				tbAdConstrait.Text = Properties.Resources.labelAdConstraintNonEvent;
				_constrait = (int)DigitalSignage.Common.ShowFlag.Show_NonEvent;
			}
			#endregion

			#region 광고 시간 추출
			
			tbAdTimeRange.Text = "Unknown";

			#endregion

			#region 광고 메타태그 추출
			try
			{
				_metaTags = tbAdMetaTags.Text = _task.MetaTags;
			}
			catch
			{
				tbAdMetaTags.Text = "Unknown";
				_metaTags = String.Empty;
			}
			#endregion

			cboxAllowDup.IsChecked = _task.AllowDuplication;

			this.DayOfWeekField = _task.daysofweek;

			#region 타겟 설정
            if (string.IsNullOrEmpty(_task.pid) || _task.pid.Contains("-1"))
			{
				selectedTarget = ServerDataCache.GetInstance.GetGroupByGID(_task.gid);
				if (selectedTarget != null)
					targetUI.Text = selectedTarget.Attributes["name"].Value;
				else targetUI.Text = "";
			}
			else
			{
				selectedTarget = ServerDataCache.GetInstance.GetPlayer(_task.pid);
				if (selectedTarget != null)
				{
					// this is project for one player
					targetUI.Text = selectedTarget.Attributes["name"].Value;
				}
				else targetUI.Text = "";
			}
			btnTarget.IsEnabled = false;
			#endregion

			bIsEditUI = true;
			OldTask = _task;
		}

		private int registerTask(Object target, Guid duuid, Guid guuid, DateTime start,
			DateTime end, String name, TaskCommands command)
		{
			if (command != TaskCommands.CmdControlSchedule && m_groups.Count == 0)
				return -1;

            int resultvalue = -1;
            bool alreadySync = false;

            if (!bIsEditUI)
            {
                #region 새로 만들기
                if (target.GetType().Equals(typeof(Collection<TreeViewItem>)))
                {

                    foreach (TreeViewItem item in (Collection<TreeViewItem>)target)
                    {
                        if (alreadySync == true)
                            break;

                        string gid = "";
                        string pid = null;

                        XmlNode node = item.DataContext as XmlNode;

                        if (node.LocalName.Equals("group"))
                        {
                            gid = node.Attributes["gid"].Value;
                            pid = null;
                        }
                        else if (node.LocalName.Equals("player"))
                        {
                            gid = node.Attributes["gid"].Value;
                            pid = node.Attributes["pid"].Value;
                        }

                        //	업로드가 완료되었는지 검사
                        if (command != TaskCommands.CmdControlSchedule)
                        {
                            bool uploaded = false;
                            foreach (XmlNode row in m_groups)
                            {
                                if (row.Attributes["gid"].Value.Equals(gid))
                                {
                                    uploaded = true;
                                    break;
                                }
                            }

                            //	업로드가 안되었기때문에 스킵한다.
                            if (!uploaded) continue;
                        }

                        guuid = Guid.NewGuid();

                        foreach (Duration piece in arrDurations)
                        {
                            // 						//	그룹이 변경되므로 Guuid를 새로 생성하자
                            // 						if (command != TaskCommands.CmdControlSchedule) guuid = Guid.NewGuid();

                            // filling task structure


                            //bmwe3 동기화 스켸줄일때 여기를 복수로 처리하게 
                            Task newTask = new Task();
                            newTask.TaskStart = piece.StartT; //TimeConverter.ConvertToUTP(DateTime.MinValue);
                            newTask.TaskEnd = piece.EndT;
                            newTask.state = TaskState.StateWait;
                            newTask.type = command;
                            newTask.duuid = duuid;
                            newTask.guuid = guuid;
                            newTask.daysofweek = DayOfWeekField;
                            newTask.description = name;
                            newTask.duration = (int)duration.SelectedTime.TotalSeconds;
                            newTask.uuid = Guid.NewGuid();
                            newTask.starttimeofday = StartTimeOfDayField;
                            newTask.endtimeofday = EndTimeOfDayField;
                            newTask.pid = pid;
                            newTask.gid = gid;

                            #region 광고 관련 Tag
                            newTask.isAd = this._isAd;
                            newTask.MetaTags = this._metaTags;

                            if (newTask.isAd)
                            {
                                newTask.show_flag = 0;
                                if (this._constrait > 0)
                                    newTask.show_flag |= _constrait;
                                else
                                    newTask.show_flag = (int)ShowFlag.Show_NonEvent;
                            }
                            else
                            {
                                newTask.show_flag = (int)ShowFlag.Show_NonEvent;
                            }

                            #endregion

                            #region 중복 관련 Flag 설정

                            if (AllowDuplication)
                                newTask.show_flag |= (int)ShowFlag.Show_Allow_Duplication;

                            #endregion

                            if (string.IsNullOrEmpty(pid) || pid.Equals("-1"))
                            {
                                //	그룹 스케줄인 경우
                                int nDepth = 0;
                                try
                                {
                                    nDepth = Convert.ToInt32(node.Attributes["depth"].Value);
                                }
                                catch { nDepth = 0; }

                                if (newTask.type == TaskCommands.CmdDefaultProgram || newTask.type == TaskCommands.CmdTouchDefaultProgram)
                                    newTask.priority = QueryMinimumPriorityFromGID(node.Attributes["gid"].Value) - 10;
                                else
                                    newTask.priority = nDepth * 10 + 0;
                            }
                            else
                            {
                                //	플레이어 스케줄인 경우
                                int nDepth = 0;
                                try
                                {
                                    nDepth = cfg.ServerDataCacheList.GetPlayerDepth(newTask.pid);
                                }
                                catch { nDepth = 0; }

                                if (newTask.type == TaskCommands.CmdDefaultProgram || newTask.type == TaskCommands.CmdTouchDefaultProgram)
                                    newTask.priority = QueryMinimumPriorityFromGID(node.Attributes["gid"].Value) - 1;
                                else
                                    newTask.priority = nDepth * 10 + 1;
                            }



                            if (executeSyncCheckBox.IsChecked == true && itemList.Count > 0)
                            {
                                List<SYNC_TASK> arrList = new List<SYNC_TASK>();

                                foreach (ProjectItem syncitem in itemList)
                                {
                                    SYNC_TASK newitem = new SYNC_TASK();
                                    newitem.DUUID = syncitem.DownLoadID.ToString();
                                    newitem.GroupID = syncitem.GroupID;
                                    newitem.GUUID = newTask.guuid.ToString();
                                    newitem.PlayerID = syncitem.Pid;
                                    newitem.MasterYN = syncitem.IsMaster;

                                    if (syncitem.Pid == newTask.pid)
                                    {
                                        if (syncitem.IsMaster == true)
                                        {
                                            newTask.show_flag |= (int)ShowFlag.Show_SyncMaster;
                                        }
                                        else
                                        {
                                            newTask.show_flag |= (int)ShowFlag.Show_SyncSlave;
                                        }
                                    }

                                    arrList.Add(newitem);

                                }

                                resultvalue = cfg.ServerTasksList.AddSyncTask(newTask, arrList.ToArray());
                                alreadySync = true;
                            }
                            else
                            {
                                resultvalue = cfg.ServerTasksList.AddTask(newTask);
                            }



                            //bmwe3 Task 및 pidList만 주면 될거 같은데...?
                            //현재 선택한 Task는 Task 아이템 그대로 서버에서는 pid리스트에 나머지 값은 모두 동일하니까.
                            //구조체(스켸줄 아뒤, pid, 프로그램, ms여부
                            if (-1 != resultvalue)
                            {
                                try
                                {
                                    ///	스케줄 생성 기록
                                    cfg.ServerLogList.InsertDetailLog(ServerDataCache.GetInstance.Connected_UserName, (int)(LogType.LogType_Managing | LogType.LogType_Proceed), newTask.uuid.ToString(), "SCHEDULE_CREATE", newTask.description, 0, 0, TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()));
                                }
                                catch { }

                                ///	페이지 등록
                                try
                                {
                                    if (UploadedScreens != null)
                                    {
                                        int nIndex = 0;
                                        ServerDataCache sdc = ServerDataCache.GetInstance;

                                        foreach (ScreenPropertyLoader screen in this.UploadedScreens)
                                        {
                                            String sThumbnailURL = String.Empty;
                                            if (ServerDataCache.GetInstance.IsSupportedThumbnail)
                                            {
                                                /// 섬네일 등록
                                                String sThumbName = String.Format("{0}.png", screen.ScreenID);
                                                if (UploadThumbnail(screen.LocalThumbPath, sdc.ThumbnailSetUrl, sThumbName))
                                                    sThumbnailURL = sdc.ThumbnailGetUrl +
                                                        (sdc.ThumbnailGetUrl.EndsWith("/") ? "" : "/") + sThumbName;
                                            }

                                            cfg.ServerTasksList.InsertScreen(screen.ScreenID, screen.ScreenName, String.Empty, screen.ScreenWidth, screen.ScreenHeight, screen.ScreenPlaytime, screen.PlayConstrait, 0, screen.IsAd ? "Y" : "N", screen.MetaTags, TimeConverter.ConvertToUTP(screen.StartDateTime.ToUniversalTime()), TimeConverter.ConvertToUTP(screen.EndDateTime.ToUniversalTime()));
                                            cfg.ServerTasksList.InsertTaskAndScreenMappingDetail(screen.ScreenID, newTask.uuid.ToString(), nIndex, sThumbnailURL);

                                            nIndex++;
                                        }
                                    }
                                }
                                catch { }
                            }
                            else
                            {
                                try
                                {
                                    ///	스케줄 생성 기록
                                    cfg.ServerLogList.InsertDetailLog(ServerDataCache.GetInstance.Connected_UserName, (int)(LogType.LogType_Managing | LogType.LogType_Failed), newTask.uuid.ToString(), "SCHEDULE_CREATE", newTask.description, 0, 0, TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()));
                                }
                                catch { }
                            }
                        }
                    }
                }
                else
                {
                    // 				//	그룹이 변경되므로 Guuid를 새로 생성하자
                    // 				if (command == TaskCommands.CmdControlSchedule) guuid = Guid.NewGuid();
                    guuid = Guid.NewGuid();

                    foreach (Duration piece in arrDurations)
                    {
                        // 					if (command != TaskCommands.CmdControlSchedule) guuid = Guid.NewGuid();

                        // filling task structure
                        Task newTask = new Task();
                        newTask.TaskStart = piece.StartT;
                        newTask.TaskEnd = piece.EndT;
                        newTask.state = TaskState.StateWait;
                        newTask.type = command;
                        newTask.duuid = duuid;
                        newTask.guuid = guuid;
                        newTask.daysofweek = DayOfWeekField;
                        newTask.description = name;
                        newTask.duration = (long)duration.SelectedTime.TotalSeconds;
                        newTask.uuid = Guid.NewGuid();
                        newTask.starttimeofday = StartTimeOfDayField;
                        newTask.endtimeofday = EndTimeOfDayField;

                        #region 광고 관련 Tag
                        newTask.isAd = this._isAd;
                        newTask.MetaTags = this._metaTags;

                        if (newTask.isAd)
                        {
                            newTask.show_flag = 0;
                            if (this._constrait > 0)
                                newTask.show_flag |= _constrait;
                            else
                                newTask.show_flag = (int)ShowFlag.Show_NonEvent;
                        }
                        else
                        {
                            newTask.show_flag = (int)ShowFlag.Show_NonEvent;
                        }

                        #endregion

                        #region 중복 관련 Flag 설정

                        if (AllowDuplication)
                            newTask.show_flag |= (int)ShowFlag.Show_Allow_Duplication;

                        #endregion

                        if (target as XmlNode != null && ((XmlNode)target).LocalName.Equals("group"))
                        {
                            XmlNode group = (XmlNode)target;
                            int nDepth = 0;
                            try
                            {
                                nDepth = Convert.ToInt32(group.Attributes["depth"].Value);
                            }
                            catch { nDepth = 0; }
                            //	hsshin 그룹 스케줄인 경우
                            newTask.pid = null;
                            newTask.gid = group.Attributes["gid"].Value;
                            if (newTask.type == TaskCommands.CmdDefaultProgram || newTask.type == TaskCommands.CmdTouchDefaultProgram)
                                newTask.priority = QueryMinimumPriorityFromGID(group.Attributes["gid"].Value) - 10;
                            else
                                newTask.priority = nDepth * 10 + 0;
                        }
                        else
                        {
                            XmlNode player = (XmlNode)target;
                            //	플레이어 스케줄인경우
                            newTask.pid = player.Attributes["pid"].Value;
                            newTask.gid = player.Attributes["gid"].Value;

                            int nDepth = 0;
                            try
                            {
                                nDepth = cfg.ServerDataCacheList.GetPlayerDepth(newTask.pid);
                            }
                            catch { nDepth = 0; }

                            if (newTask.type == TaskCommands.CmdDefaultProgram || newTask.type == TaskCommands.CmdTouchDefaultProgram)
                                newTask.priority = QueryMinimumPriorityFromGID(player.Attributes["gid"].Value) - 1;
                            else
                                newTask.priority = nDepth * 10 + 1;

                        }
                        
                        if (executeSyncCheckBox.IsChecked == true && itemList.Count > 0)
                        {
                            List<SYNC_TASK> arrList = new List<SYNC_TASK>();

                            foreach (ProjectItem syncitem in itemList)
                            {
                                SYNC_TASK newitem = new SYNC_TASK();
                                newitem.DUUID = syncitem.DownLoadID.ToString();
                                newitem.GroupID = syncitem.GroupID;
                                newitem.GUUID = newTask.guuid.ToString();
                                newitem.PlayerID = syncitem.Pid;
                                newitem.MasterYN = syncitem.IsMaster;

                                if (syncitem.Pid == newTask.pid)
                                {
                                    if (syncitem.IsMaster == true)
                                    {
                                        newTask.show_flag |= (int)ShowFlag.Show_SyncMaster;
                                    }
                                    else
                                    {
                                        newTask.show_flag |= (int)ShowFlag.Show_SyncSlave;
                                    }
                                }

                                arrList.Add(newitem);

                            }

                            resultvalue = cfg.ServerTasksList.AddSyncTask(newTask, arrList.ToArray());
                            alreadySync = true;
                        }
                        else
                        {
                            resultvalue = cfg.ServerTasksList.AddTask(newTask);
                        }

                        if (-1 != resultvalue)//cfg.ServerTasksList.AddTask(newTask))
                        {
                            try
                            {
                                ///	스케줄 생성 기록
                                cfg.ServerLogList.InsertDetailLog(ServerDataCache.GetInstance.Connected_UserName, (int)(LogType.LogType_Managing | LogType.LogType_Proceed), newTask.uuid.ToString(), "SCHEDULE_CREATE", newTask.description, 0, 0, TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()));
                            }
                            catch { }

                            ///	페이지 등록
                            try
                            {
                                if (UploadedScreens != null)
                                {
                                    int nIndex = 0;
                                    ServerDataCache sdc = ServerDataCache.GetInstance;

                                    foreach (ScreenPropertyLoader screen in this.UploadedScreens)
                                    {
                                        String sThumbnailURL = String.Empty;
                                        if (ServerDataCache.GetInstance.IsSupportedThumbnail)
                                        {
                                            /// 섬네일 등록
                                            String sThumbName = String.Format("{0}.png", screen.ScreenID);
                                            if (UploadThumbnail(screen.LocalThumbPath, sdc.ThumbnailSetUrl, sThumbName))
                                                sThumbnailURL = sdc.ThumbnailGetUrl +
                                                    (sdc.ThumbnailGetUrl.EndsWith("/") ? "" : "/") + sThumbName;
                                        }

                                        cfg.ServerTasksList.InsertScreen(screen.ScreenID, screen.ScreenName, String.Empty, screen.ScreenWidth, screen.ScreenHeight, screen.ScreenPlaytime, screen.PlayConstrait, 0, screen.IsAd ? "Y" : "N", screen.MetaTags, TimeConverter.ConvertToUTP(screen.StartDateTime.ToUniversalTime()), TimeConverter.ConvertToUTP(screen.EndDateTime.ToUniversalTime()));
                                        cfg.ServerTasksList.InsertTaskAndScreenMappingDetail(screen.ScreenID, newTask.uuid.ToString(), nIndex, sThumbnailURL);

                                        nIndex++;
                                    }
                                }
                            }
                            catch { }

                        }
                        else
                        {
                            try
                            {
                                ///	스케줄 생성 기록
                                cfg.ServerLogList.InsertDetailLog(ServerDataCache.GetInstance.Connected_UserName, (int)(LogType.LogType_Managing | LogType.LogType_Failed), newTask.uuid.ToString(), "SCHEDULE_CREATE", newTask.description, 0, 0, TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()));
                            }
                            catch { }
                        }
                    }
                }
                #endregion 새로 만들기
            }
            else // Edit Mode
            {
                if (target.GetType().Equals(typeof(Collection<TreeViewItem>)))
                {
                    foreach (TreeViewItem item in (Collection<TreeViewItem>)target)
                    {
                        if (alreadySync == true)
                            break;
                        string gid = "";
                        string pid = null;

                        XmlNode node = item.DataContext as XmlNode;

                        if (node.LocalName.Equals("group"))
                        {
                            gid = node.Attributes["gid"].Value;
                            pid = null;
                        }
                        else if (node.LocalName.Equals("player"))
                        {
                            gid = node.Attributes["gid"].Value;
                            pid = node.Attributes["pid"].Value;
                        }

                        //	업로드가 완료되었는지 검사
                        if (command != TaskCommands.CmdControlSchedule)
                        {
                            bool uploaded = false;
                            foreach (XmlNode row in m_groups)
                            {
                                if (row.Attributes["gid"].Value.Equals(gid))
                                {
                                    uploaded = true;
                                    break;
                                }
                            }

                            //	업로드가 안되었기때문에 스킵한다.
                            if (!uploaded) continue;
                        }

                        guuid = OldTask.guuid;

                        foreach (Duration piece in arrDurations)
                        {
                            // 						//	그룹이 변경되므로 Guuid를 새로 생성하자
                            // 						if (command != TaskCommands.CmdControlSchedule) guuid = Guid.NewGuid();

                            // filling task structure
                            Task newTask = new Task();
                            newTask.TaskStart = piece.StartT;
                            newTask.TaskEnd = piece.EndT;
                            newTask.state = TaskState.StateEdit;
                            newTask.type = command;
                            newTask.duuid = duuid;
                            newTask.guuid = guuid;
                            newTask.daysofweek = DayOfWeekField;
                            newTask.description = name;
                            newTask.duration = (long)duration.SelectedTime.TotalSeconds;
                            newTask.uuid = OldTask.uuid;
                            newTask.starttimeofday = StartTimeOfDayField;
                            newTask.endtimeofday = EndTimeOfDayField;

                            #region 광고 관련 Tag
                            newTask.isAd = this._isAd;
                            newTask.MetaTags = this._metaTags;

                            if (newTask.isAd)
                            {
                                newTask.show_flag = 0;
                                if (this._constrait > 0)
                                    newTask.show_flag |= _constrait;
                                else
                                    newTask.show_flag = (int)ShowFlag.Show_NonEvent;
                            }
                            else
                            {
                                newTask.show_flag = (int)ShowFlag.Show_NonEvent;
                            }

                            #endregion

                            #region 중복 관련 Flag 설정

                            if (AllowDuplication)
                                newTask.show_flag |= (int)ShowFlag.Show_Allow_Duplication;

                            #endregion

                            newTask.pid = pid;
                            newTask.gid = gid;
                            newTask.priority = OldTask.priority;

                            if (executeSyncCheckBox.IsChecked == true && itemList.Count > 0)
                            {
                                List<SYNC_TASK> arrList = new List<SYNC_TASK>();

                                foreach (ProjectItem syncitem in itemList)
                                {
                                    SYNC_TASK newitem = new SYNC_TASK();
                                    newitem.UUID = syncitem.UUID;
                                    newitem.DUUID = syncitem.DownLoadID.ToString();
                                    newitem.GroupID = syncitem.GroupID;
                                    newitem.GUUID = newTask.guuid.ToString();
                                    newitem.PlayerID = syncitem.Pid;
                                    newitem.MasterYN = syncitem.IsMaster;

                                    if (syncitem.Pid == newTask.pid)
                                    {
                                        if (syncitem.IsMaster == true)
                                        {
                                            newTask.show_flag |= (int)ShowFlag.Show_SyncMaster;
                                        }
                                        else
                                        {
                                            newTask.show_flag |= (int)ShowFlag.Show_SyncSlave;
                                        }
                                    }

                                    arrList.Add(newitem);

                                }

                                resultvalue = cfg.ServerTasksList.EditSyncTasks(OldTask, newTask, arrList.ToArray());
                                alreadySync = true;
                            }
                            else
                            {
                                resultvalue = cfg.ServerTasksList.EditTasks(OldTask, newTask);
                            }



                            if (-1 != resultvalue)
                            {
                                try
                                {
                                    ///	스케줄 수정 기록
                                    cfg.ServerLogList.InsertDetailLog(ServerDataCache.GetInstance.Connected_UserName, (int)(LogType.LogType_Managing | LogType.LogType_Proceed), OldTask.uuid.ToString(), "SCHEDULE_UPDATE", newTask.description, 0, 0, TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()));
                                }
                                catch { }

                                ///	페이지 등록
                                try
                                {
                                    if (UploadedScreens != null)
                                    {
                                        int nIndex = 0;
                                        ServerDataCache sdc = ServerDataCache.GetInstance;

                                        foreach (ScreenPropertyLoader screen in this.UploadedScreens)
                                        {
                                            String sThumbnailURL = String.Empty;
                                            if (ServerDataCache.GetInstance.IsSupportedThumbnail)
                                            {
                                                /// 섬네일 등록
                                                String sThumbName = String.Format("{0}.png", screen.ScreenID);
                                                if (UploadThumbnail(screen.LocalThumbPath, sdc.ThumbnailSetUrl, sThumbName))
                                                    sThumbnailURL = sdc.ThumbnailGetUrl +
                                                        (sdc.ThumbnailGetUrl.EndsWith("/") ? "" : "/") + sThumbName;
                                            }

                                            cfg.ServerTasksList.InsertScreen(screen.ScreenID, screen.ScreenName, String.Empty, screen.ScreenWidth, screen.ScreenHeight, screen.ScreenPlaytime, screen.PlayConstrait, 0, screen.IsAd ? "Y" : "N", screen.MetaTags, TimeConverter.ConvertToUTP(screen.StartDateTime.ToUniversalTime()), TimeConverter.ConvertToUTP(screen.EndDateTime.ToUniversalTime()));
                                            cfg.ServerTasksList.InsertTaskAndScreenMappingDetail(screen.ScreenID, newTask.uuid.ToString(), nIndex, sThumbnailURL);

                                            nIndex++;
                                        }
                                    }
                                }
                                catch { }
                            }
                            else
                            {
                                try
                                {
                                    ///	스케줄 수정 기록
                                    cfg.ServerLogList.InsertDetailLog(ServerDataCache.GetInstance.Connected_UserName, (int)(LogType.LogType_Managing | LogType.LogType_Failed), OldTask.uuid.ToString(), "SCHEDULE_UPDATE", newTask.description, 0, 0, TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()));
                                }
                                catch { }
                            }
                        }
                    }
                }
                else
                {
                    // 				//	그룹이 변경되므로 Guuid를 새로 생성하자
                    // 				if (command == TaskCommands.CmdControlSchedule) guuid = Guid.NewGuid();
                    guuid = OldTask.guuid;

                    foreach (Duration piece in arrDurations)
                    {
                        // filling task structure
                        Task newTask = new Task();
                        newTask.TaskStart = piece.StartT;
                        newTask.TaskEnd = piece.EndT;
                        newTask.state = TaskState.StateEdit;
                        newTask.type = command;
                        newTask.duuid = duuid;
                        newTask.guuid = guuid;
                        newTask.daysofweek = DayOfWeekField;
                        newTask.description = name;
                        newTask.duration = (long)duration.SelectedTime.TotalSeconds;
                        newTask.uuid = OldTask.uuid;
                        newTask.starttimeofday = StartTimeOfDayField;
                        newTask.endtimeofday = EndTimeOfDayField;
                        newTask.priority = OldTask.priority;

                        #region 광고 관련 Tag
                        newTask.isAd = this._isAd;
                        newTask.MetaTags = this._metaTags;

                        if (newTask.isAd)
                        {
                            newTask.show_flag = 0;
                            if (this._constrait > 0)
                                newTask.show_flag |= _constrait;
                            else
                                newTask.show_flag = (int)ShowFlag.Show_NonEvent;
                        }
                        else
                        {
                            newTask.show_flag = (int)ShowFlag.Show_NonEvent;
                        }

                        #endregion

                        #region 중복 관련 Flag 설정

                        if (AllowDuplication)
                            newTask.show_flag |= (int)ShowFlag.Show_Allow_Duplication;

                        #endregion


                        if (executeSyncCheckBox.IsChecked == true && itemList.Count > 0)
                        {
                            List<SYNC_TASK> arrList = new List<SYNC_TASK>();

                            foreach (ProjectItem syncitem in itemList)
                            {
                                SYNC_TASK newitem = new SYNC_TASK();
                                newitem.UUID = syncitem.UUID;
                                newitem.DUUID = syncitem.DownLoadID.ToString();
                                newitem.GroupID = syncitem.GroupID;
                                newitem.GUUID = newTask.guuid.ToString();
                                newitem.PlayerID = syncitem.Pid;
                                newitem.MasterYN = syncitem.IsMaster;

                                if (syncitem.Pid == newTask.pid)
                                {
                                    if (syncitem.IsMaster == true)
                                    {
                                        newTask.show_flag |= (int)ShowFlag.Show_SyncMaster;
                                    }
                                    else
                                    {
                                        newTask.show_flag |= (int)ShowFlag.Show_SyncSlave;
                                    }
                                }

                                arrList.Add(newitem);

                            }

                            resultvalue = cfg.ServerTasksList.EditSyncTasks(OldTask, newTask, arrList.ToArray());
                            alreadySync = true;
                        }
                        else
                        {
                            resultvalue = cfg.ServerTasksList.EditTasks(OldTask, newTask);
                        }


                        if (-1 != resultvalue)//cfg.ServerTasksList.EditTasks(OldTask, newTask))
                        {
                            try
                            {
                                ///	스케줄 수정 기록
                                cfg.ServerLogList.InsertDetailLog(ServerDataCache.GetInstance.Connected_UserName, (int)(LogType.LogType_Managing | LogType.LogType_Proceed), OldTask.uuid.ToString(), "SCHEDULE_UPDATE", newTask.description, 0, 0, TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()));
                            }
                            catch { }

                            ///	페이지 등록
                            try
                            {
                                if (UploadedScreens != null)
                                {
                                    int nIndex = 0;
                                    ServerDataCache sdc = ServerDataCache.GetInstance;

                                    foreach (ScreenPropertyLoader screen in this.UploadedScreens)
                                    {
                                        String sThumbnailURL = String.Empty;
                                        if (ServerDataCache.GetInstance.IsSupportedThumbnail)
                                        {
                                            /// 섬네일 등록
                                            String sThumbName = String.Format("{0}.png", screen.ScreenID);
                                            if (UploadThumbnail(screen.LocalThumbPath, sdc.ThumbnailSetUrl, sThumbName))
                                                sThumbnailURL = sdc.ThumbnailGetUrl +
                                                    (sdc.ThumbnailGetUrl.EndsWith("/") ? "" : "/") + sThumbName;
                                        }

                                        cfg.ServerTasksList.InsertScreen(screen.ScreenID, screen.ScreenName, String.Empty, screen.ScreenWidth, screen.ScreenHeight, screen.ScreenPlaytime, screen.PlayConstrait, 0, screen.IsAd ? "Y" : "N", screen.MetaTags, TimeConverter.ConvertToUTP(screen.StartDateTime.ToUniversalTime()), TimeConverter.ConvertToUTP(screen.EndDateTime.ToUniversalTime()));
                                        cfg.ServerTasksList.InsertTaskAndScreenMappingDetail(screen.ScreenID, newTask.uuid.ToString(), nIndex, sThumbnailURL);

                                        nIndex++;
                                    }
                                }
                            }
                            catch { }
                        }
                        else
                        {
                            try
                            {
                                ///	스케줄 수정 기록
                                cfg.ServerLogList.InsertDetailLog(ServerDataCache.GetInstance.Connected_UserName, (int)(LogType.LogType_Managing | LogType.LogType_Failed), OldTask.uuid.ToString(), "SCHEDULE_UPDATE", newTask.description, 0, 0, TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()));
                            }
                            catch { }
                        }
                    }
                }
            }

            if(uploader != null)
            {
                try
                {
                    uploader.RemoveTemporaryFiles();
                }
                catch {}
            }

			return 0;
		}

		public static bool CheckTimeScope(Task task, Object target)
		{
			Config cfg = Config.GetConfig;

			if (target.GetType().Equals(typeof(Collection<TreeViewItem>)))
			{
				//	BETWEEN을 사용하기때문에  start+1 end-1을 해줘야합니다.
				long start = task.TaskStart/*TimeConverter.ConvertToUTP(from.ToUniversalTime())*/ + 1;
				long end = task.TaskEnd/*TimeConverter.ConvertToUTP(till.ToUniversalTime())*/ - 1;

				Collection<TreeViewItem> items = target as Collection<TreeViewItem>;

				foreach (TreeViewItem item in items)
				{
					try
					{
						if (item.DataContext == null)
							continue;
						else if (item.DataContext as XmlNode != null)
						{
							XmlNode row = (XmlNode)item.DataContext;

							if (row.LocalName.Equals("group"))
							{
								task.gid = row.Attributes["gid"].Value;
								task.pid = null;

								try
								{
									task.TaskStart = start;
									task.TaskEnd = end;

									if (!cfg.ServerTasksList.CanAddTaskToGroup(task, row.Attributes["gid"].Value))
										return false;
								}
								catch
								{
									if (!cfg.ServerTasksList.CanAddTaskToGroup("", start, end, (long)task.type, row.Attributes["gid"].Value))
										return false;
								}
							}
							else
							{
								task.gid = row.Attributes["gid"].Value;
								task.pid = row.Attributes["pid"].Value;

								try
								{
									if (!cfg.ServerTasksList.CanAddTaskToPlayer(task, row.Attributes["pid"].Value))
										return false;
								}
								catch
								{
									if (!cfg.ServerTasksList.CanAddTaskToPlayer("", start, end, (long)task.type, row.Attributes["pid"].Value))
										return false;
								}
							}
						}
					}
					catch
					{
					}
				}
			}
			else
			{
				//	BETWEEN을 사용하기때문에  start+1 end-1을 해줘야합니다.
				long start = task.TaskStart/*TimeConverter.ConvertToUTP(from.ToUniversalTime())*/ + 1;
				long end = task.TaskEnd/*TimeConverter.ConvertToUTP(till.ToUniversalTime())*/ - 1;

				XmlNode node = target as XmlNode;
					
				if (node != null && node.LocalName.Equals("group"))
				{
					task.gid = node.Attributes["gid"].Value;
					task.pid = null;

					try
					{
						if (!cfg.ServerTasksList.CanAddTaskToGroup(task, node.Attributes["gid"].Value))
							return false;
					}
					catch
					{
						if (!cfg.ServerTasksList.CanAddTaskToGroup("", start, end, (long)task.type, node.Attributes["gid"].Value))
							return false;
					}
				}
				else
				{
					task.gid = node.Attributes["gid"].Value;
					task.pid = node.Attributes["pid"].Value;

					try
					{
						task.TaskStart = start;
						task.TaskEnd = end;

						if (!cfg.ServerTasksList.CanAddTaskToPlayer(task, node.Attributes["pid"].Value))
							return false;
					}
					catch
					{
						if (!cfg.ServerTasksList.CanAddTaskToPlayer("", start, end, (long)task.type, node.Attributes["pid"].Value))
							return false;
					}

				}
			}
			return true;
		}

		public bool CheckTimeScope(String uuid, long from, long till, TaskCommands type, Object target)
		{
			Task task = new Task();
			task.uuid = new Guid();
			task.type = type;
			task.TaskStart = from;
			task.TaskEnd = till;
			task.starttimeofday = StartTimeOfDayField;
			task.endtimeofday = EndTimeOfDayField;
			task.daysofweek = DayOfWeekField;

			return CheckTimeScope(task, target);
		}

		void tb_GotKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
		{
			try
			{
				TextBox tb = sender as TextBox;
				if (tb != null) tb.SelectAll();
				else
				{
					PasswordBox pb = sender as PasswordBox;
					pb.SelectAll();
				}

				e.Handled = true;
			}
			catch { }
		}
		public bool ValidationCheck()
		{
			bool bRet = false;
			if (String.IsNullOrEmpty(TaskName))
			{
				MessageBox.Show(Properties.Resources.mbTaskName, Properties.Resources.titleMessageBox,
					MessageBoxButton.OK, MessageBoxImage.Stop);
				
				return bRet = false;
			}

            //#region Touch - ScheduleTime Update
            //if (TouchManager.IsTouchFile(this.projectProgram) == true)
            //{
            //    TouchManager.UpdateTouchProjectInfo(this.projectProgram, "ScheduleTime", this.duration.SelectedTime);
            //}
            //#endregion

            if (TargetObject == null)
            {
                MessageBox.Show(Properties.Resources.mbErrorTarget, Properties.Resources.titleMessageBox,
                    MessageBoxButton.OK, MessageBoxImage.Stop);

                return bRet = false;
            }

			ComboBoxItem item = categoryUI.SelectedItem as ComboBoxItem;
			switch ((_category)item.DataContext)
			{
				case _category.taskProgram:
				case _category.taskDefaultSchedule:

                    bool checkProject = false;
                    if (executeSyncCheckBox.IsChecked == true )
                    {
                        int count = 0;
                        foreach(ProjectItem pitem in itemList)
                        {
                            if(String.IsNullOrEmpty(pitem.ProjectUI) != true)
                            {
                                count++;
                            }
                        }

                        if(count == itemList.Count)
                            checkProject = true;
                    }
                    else
                    {
                        checkProject = !String.IsNullOrEmpty(projectUI.Text);
                    }

                    if(checkProject != true)
                    //if (String.IsNullOrEmpty(projectUI.Text))
                    {
                        MessageBox.Show(Properties.Resources.mbSelectTaskProgram, Properties.Resources.titleMessageBox,
                            MessageBoxButton.OK, MessageBoxImage.Stop);

                        return bRet = false;
                    }

					if(duration.SelectedTime.TotalSeconds == 0)
					{
						MessageBox.Show(Properties.Resources.mbErrorSaveDuration, Properties.Resources.titleMessageBox,
							MessageBoxButton.OK, MessageBoxImage.Stop);
						bRet = false;
					}
					else
					{
						bRet = true;
					}
					break;
				case _category.taskSubtitle:

                    if (String.IsNullOrEmpty(projectUI.Text))
                    {
                        MessageBox.Show(Properties.Resources.mbSelectTaskProgram, Properties.Resources.titleMessageBox,
                            MessageBoxButton.OK, MessageBoxImage.Stop);

                        return bRet = false;
                    }

					bRet = true;
					break;
				case _category.taskRS232C:
					bRet = true;
					break;
				case _category.taskControl:
					bRet = true;
					break;
			}

			return bRet;
		}
		#endregion

		private void cbControlList_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			UpdateControlScheduleTitle();
		}

		private String UpdateControlScheduleTitle()
		{
			try
			{
				ComboBoxItem item = categoryUI.SelectedItem as ComboBoxItem;
                if (item == null)
                    return null;
				if ((_category)item.DataContext != _category.taskControl)
					return null;

				TaskControlSchedules controltype = this.SelectedCtrlSched;
				String sReturn = controltype.ToString();
				startDateUI.IsEnabled = startTimeUI.IsEnabled = false;
				finDateUI.IsEnabled = finTimeUI.IsEnabled = false;
				spAudioBox.Visibility = Visibility.Collapsed;
                spSubtitleBox.Visibility = Visibility.Collapsed;
                spRepeatOpts.Visibility = cbApplyImmediate.IsChecked.Value ? Visibility.Collapsed : Visibility.Visible;
				lbMaster.Content = Properties.Resources.labelMasterVolume;
                this.fileTransmissionGroupBox.Visibility = System.Windows.Visibility.Collapsed;

				switch (controltype)
				{
					case TaskControlSchedules.CtrlVolume:
						spAudioBox.Visibility = Visibility.Visible;
						startDateUI.IsEnabled = startTimeUI.IsEnabled = true;
						finDateUI.IsEnabled = finTimeUI.IsEnabled = true;
						sReturn = String.Format("{0} [{1}|{2}]", cbApplyImmediate.IsChecked.Value
							? TaskControlSchedules.CtrlVolumeImmediately.ToString() : controltype.ToString(), MasterValue, WaveValue);
						break;
					case TaskControlSchedules.CtrlSerialVolume:
						spAudioBox.Visibility = Visibility.Visible;
						startDateUI.IsEnabled = startTimeUI.IsEnabled = true;
						finDateUI.IsEnabled = finTimeUI.IsEnabled = true;
						sReturn = String.Format("{0} [{1}|{2}]", cbApplyImmediate.IsChecked.Value
							? TaskControlSchedules.CtrlSerialVolumeNow.ToString() : controltype.ToString(), MasterValue, WaveValue);
						break;
                    case TaskControlSchedules.CtrlEventVolume:
                        spAudioBox.Visibility = Visibility.Visible;
                        startDateUI.IsEnabled = startTimeUI.IsEnabled = true;
                        finDateUI.IsEnabled = finTimeUI.IsEnabled = true;
                        sReturn = String.Format("{0} [{1}|{2}]", controltype.ToString(), MasterValue, WaveValue);
                        break;

                    case TaskControlSchedules.CtrlSubtitle:
                        spSubtitleBox.Visibility = Visibility.Visible;
                        startDateUI.IsEnabled = startTimeUI.IsEnabled = true;
                        finDateUI.IsEnabled = finTimeUI.IsEnabled = true;
                        if(cbTransparent.IsChecked == true)
                            sReturn = String.Format("{0} [{1}|{2}]", controltype.ToString(), "Y", ((ComboBoxItem)cbSpeed.SelectedItem).Content);
                        else
                            sReturn = String.Format("{0} [{1}|{2}|{3}]", controltype.ToString(), "N", ((ComboBoxItem)cbBackColor.SelectedItem).DataContext, ((ComboBoxItem)cbSpeed.SelectedItem).Content);
                        break;

                    case TaskControlSchedules.CtrlFileTransmit:
                        {
                            this.fileTransmissionGroupBox.Visibility = System.Windows.Visibility.Visible;
                            startDateUI.IsEnabled = startTimeUI.IsEnabled = true;
                            finDateUI.IsEnabled = finTimeUI.IsEnabled = false;
                            cmbRepeatUI.IsEditable = false;
                        }
                        break;

				}

				UpdateControlLayoutByCheck();

				return nameUI.Text = sReturn;
			}
			catch
			{

			}

			return null;
		}

		private void UpdateControlLayoutByCheck()
		{
			try
			{
				if (((_category)((ComboBoxItem)categoryUI.SelectedItem).DataContext) != _category.taskControl)
					return;

				bool is_Checked = cbApplyImmediate.IsChecked.Value;
				_repeat_options option = (_repeat_options)cmbRepeatUI.SelectedIndex;


				startDateUI.IsEnabled = startTimeUI.IsEnabled = !is_Checked;
				endtimeofdayUI.IsEnabled = finDateUI.IsEnabled = finTimeUI.IsEnabled = false;

				TaskControlSchedules controltype = this.SelectedCtrlSched;
				if (controltype == TaskControlSchedules.CtrlVolume || controltype == TaskControlSchedules.CtrlSerialVolume)
				{
					endtimeofdayUI.IsEnabled = finDateUI.IsEnabled = finTimeUI.IsEnabled = !is_Checked;

				}

				if (!is_Checked && option != _repeat_options.norepeat)
				{
					finDateUI.IsEnabled = finTimeUI.IsEnabled = true;
					endtimeofdayUI.IsEnabled = controltype == TaskControlSchedules.CtrlVolume || controltype == TaskControlSchedules.CtrlSerialVolume;
				}

			}
			catch { }
		}

		int nMaxValue = 100;
		int nMinValue = 0;

		public int MaxValue
		{
			get { return nMaxValue; }
			set { nMaxValue = value; }
		}

		public int MinValue
		{
			get { return nMinValue; }
			set { nMinValue = value; }
		}

		public int MasterValue
		{
			get { return Convert.ToInt32(volMasterUI.Value); }
			set
			{
				volMasterUI.Value = value;
			}
		}

		public int WaveValue
		{
			get { return Convert.ToInt32(volWaveUI.Value); }
			set
			{
				volWaveUI.Value = value;
			}
		}

		private void volMasterUI_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
		{
			try
			{
				TaskControlSchedules controltype = this.SelectedCtrlSched;
				if (controltype == TaskControlSchedules.CtrlVolume)
					nameUI.Text = String.Format("{0} [{1}|{2}]", cbApplyImmediate.IsChecked.Value
							? TaskControlSchedules.CtrlVolumeImmediately.ToString() : controltype.ToString(), MasterValue, WaveValue);
				else if (controltype == TaskControlSchedules.CtrlSerialVolume)
					nameUI.Text = String.Format("{0} [{1}|{2}]", cbApplyImmediate.IsChecked.Value
							? TaskControlSchedules.CtrlSerialVolumeNow.ToString() : controltype.ToString(), MasterValue, WaveValue);
                else if (controltype == TaskControlSchedules.CtrlEventVolume)
                    nameUI.Text = String.Format("{0} [{1}|{2}]", controltype.ToString(), MasterValue, WaveValue);
            }
			catch { }

		}

		private void volWaveUI_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
		{
			try
			{
				TaskControlSchedules controltype = this.SelectedCtrlSched;
				if (controltype == TaskControlSchedules.CtrlVolume)
					nameUI.Text = String.Format("{0} [{1}|{2}]", cbApplyImmediate.IsChecked.Value
							? TaskControlSchedules.CtrlVolumeImmediately.ToString() : controltype.ToString(), MasterValue, WaveValue);
				else if (controltype == TaskControlSchedules.CtrlSerialVolume)
					nameUI.Text = String.Format("{0} [{1}|{2}]", cbApplyImmediate.IsChecked.Value
							? TaskControlSchedules.CtrlSerialVolumeNow.ToString() : controltype.ToString(), MasterValue, WaveValue);
                else if (controltype == TaskControlSchedules.CtrlEventVolume)
                    nameUI.Text = String.Format("{0} [{1}|{2}]", controltype.ToString(), MasterValue, WaveValue);
            }
			catch { }
		}

		private void cmbRepeatUI_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			try
			{
				_repeat_options option = (_repeat_options)cmbRepeatUI.SelectedIndex;

				spDayOfWeek.Visibility = Visibility.Collapsed;
				spDayOfMonth.Visibility = Visibility.Collapsed;
				spRangeOfTime.Visibility = Visibility.Collapsed;
				startTimeUI.Visibility = Visibility.Collapsed;
				finTimeUI.Visibility = Visibility.Collapsed;

				switch (option)
				{
					case _repeat_options.norepeat:
						startTimeUI.Visibility = Visibility.Visible;
						finTimeUI.Visibility = Visibility.Visible;
						break;
					case _repeat_options.daysofweek:
						spDayOfWeek.Visibility = Visibility.Visible;
						spRangeOfTime.Visibility = Visibility.Visible;
						break;
					case _repeat_options.dayofmonth:
						spDayOfMonth.Visibility = Visibility.Visible;
						spRangeOfTime.Visibility = Visibility.Visible;
						break;
					case _repeat_options.everydays:
						spRangeOfTime.Visibility = Visibility.Visible;
						break;
				}

				UpdateControlLayoutByCheck();
			}
			catch { }

		}

		private void ckEndOfMonth_Checked(object sender, RoutedEventArgs e)
		{
			try
			{
				tbDayOfMonth.IsEnabled = !ckEndOfMonth.IsChecked.Value;
			}
			catch { }
		}

		private void cbApplyImmediate_Click(object sender, RoutedEventArgs e)
		{
			//	초기화
			UpdateControlScheduleTitle();
			cmbRepeatUI.SelectedIndex = 0;
		}

        private void cbTransparent_Checked(object sender, RoutedEventArgs e)
        {
            cbBackColor.IsEnabled = false;
            try
            {
                if (cbTransparent.IsChecked == true)
                    nameUI.Text = String.Format("{0} [{1}|{2}]", TaskControlSchedules.CtrlSubtitle.ToString(), "Y", ((ComboBoxItem)cbSpeed.SelectedItem).Content);
                else
                    nameUI.Text = String.Format("{0} [{1}|{2}|{3}]", TaskControlSchedules.CtrlSubtitle.ToString(), "N", ((ComboBoxItem)cbBackColor.SelectedItem).DataContext, ((ComboBoxItem)cbSpeed.SelectedItem).Content);
            }
            catch { }
        }

        private void cbTransparent_Unchecked(object sender, RoutedEventArgs e)
        {
            cbBackColor.IsEnabled = true;
            try
            {
                if (cbTransparent.IsChecked == true)
                    nameUI.Text = String.Format("{0} [{1}|{2}]", TaskControlSchedules.CtrlSubtitle.ToString(), "Y", ((ComboBoxItem)cbSpeed.SelectedItem).Content);
                else
                    nameUI.Text = String.Format("{0} [{1}|{2}|{3}]", TaskControlSchedules.CtrlSubtitle.ToString(), "N", ((ComboBoxItem)cbBackColor.SelectedItem).DataContext, ((ComboBoxItem)cbSpeed.SelectedItem).Content);
            }
            catch { }
        }

        private void cbBackColor_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (cbTransparent.IsChecked == true)
                    nameUI.Text = String.Format("{0} [{1}|{2}]", TaskControlSchedules.CtrlSubtitle.ToString(), "Y", ((ComboBoxItem)cbSpeed.SelectedItem).Content);
                else
                    nameUI.Text = String.Format("{0} [{1}|{2}|{3}]", TaskControlSchedules.CtrlSubtitle.ToString(), "N", ((ComboBoxItem)cbBackColor.SelectedItem).DataContext, ((ComboBoxItem)cbSpeed.SelectedItem).Content);
            }
            catch { }
        }

        private void cbSpeed_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (cbTransparent.IsChecked == true)
                    nameUI.Text = String.Format("{0} [{1}|{2}]", TaskControlSchedules.CtrlSubtitle.ToString(), "Y", ((ComboBoxItem)cbSpeed.SelectedItem).Content);
                else
                    nameUI.Text = String.Format("{0} [{1}|{2}|{3}]", TaskControlSchedules.CtrlSubtitle.ToString(), "N", ((ComboBoxItem)cbBackColor.SelectedItem).DataContext, ((ComboBoxItem)cbSpeed.SelectedItem).Content);
            }
            catch { }
        }

        /// <summary>
        /// 썸네일을 올린다.
        /// </summary>
        /// <param name="ThumbnailFile"></param>
        /// <param name="remotePath"></param>
        /// <param name="remoteName"></param>
        /// <returns></returns>
        private bool UploadThumbnail(string ThumbnailFile, string remotePath, string remoteName)
        {
            FtpWebRequest reqFTP = null;

            int retr = 2;
            ServerDataCache sdc = ServerDataCache.GetInstance;
            
            string reqString = sdc.ThumbnailSetUrl + (sdc.ThumbnailSetUrl.EndsWith("/") ? "" : "/") + remoteName;
            do
            {
                try
                {
                    FileInfo fInfo = new FileInfo(ThumbnailFile);
                    using (FileStream inStream = new FileStream(ThumbnailFile, FileMode.Open))
                    {
                        reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri(reqString));
                        reqFTP.Method = WebRequestMethods.Ftp.UploadFile;
                        reqFTP.KeepAlive = false;
                        reqFTP.UseBinary = true;
                        reqFTP.UsePassive = true;
                        reqFTP.Timeout = 5000;
                        reqFTP.Credentials = new NetworkCredential(sdc.ThumbnailSetAuthID, sdc.ThumbnailSetAuthPass);
                        reqFTP.ContentLength = fInfo.Length;
                        using (Stream outStream = reqFTP.GetRequestStream())
                        {
                            long size = fInfo.Length;
                            int bufferSize = 65536;
                            int readed;
                            byte[] buffer = new byte[bufferSize];

                            while (size > 0)
                            {
                                readed = inStream.Read(buffer, 0, bufferSize);
                                size -= readed;
                                outStream.Write(buffer, 0, readed);
                            }
                            outStream.Close();
                        }
                        inStream.Close();
                    }

                    return true;
                }
                catch (Exception e)
                {
                    logger.Error(e.Message);
                }

                retr--;
            } while (retr > 0);

            return false;
        }
        private void executeFileCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            if (executeFileCheckBox != null && executeParameterGrid != null)
            {
                if (executeFileCheckBox.IsChecked == true)
                    executeParameterGrid.IsEnabled = true;
                else
                    executeParameterGrid.IsEnabled = false;
            }
        }
        private void btnFileTarget_Click(object sender, RoutedEventArgs e)
        {
            string filter = "";
            string title = Properties.Resources.titleFileSelect;
            filter = "모든 파일 (*.*)|*.*";

            System.Windows.Forms.OpenFileDialog fileDlg = new System.Windows.Forms.OpenFileDialog();
            fileDlg.Title = title;
            fileDlg.Filter = filter;
            if (fileDlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                targetFilePath.Text = fileDlg.FileName;
            }
        }

        private void fileTranPostionComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox cb = sender as ComboBox;
            int index = cb.SelectedIndex;
            if (index != -1)
            {
                switch(index)
                {
                    case 0:
                        TaskFileTrans = FileTransferInfo.FILE_MYDOCUMENT;
                        break;
                    case 1: TaskFileTrans = FileTransferInfo.FILE_REFDATA;
                        break;
                    case 2:
                        TaskFileTrans = FileTransferInfo.FILE_IVISION;
                        break;
                    case 3:
                        TaskFileTrans = FileTransferInfo.FILE_INSTALLDRIVE;
                        break;
                }
            }
        }

        private void executeSyncCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            //스켸줄에 동기화 체크를 추가한다

            //체크설정시엔 하단의 프로그램 리스트르를 조회해서 추가한다

            //체크 해제시엔 원래 프로그램 설정으로 변경해준다.

            if (executeSyncCheckBox.IsChecked == true)
            {

                SetProjectUI(true);
            }
            else
            {
                SetProjectUI(false);

            }
            

        }

	}

    public class ScheduleCategory : ViewModelBase
    {
        private int id;
        /// <summary>
        /// Schedule Category ID
        /// </summary>
        public int ID
        {
            get { return id; }
            set { id = value; OnPropertyChanged("ID"); }
        }

        private string name;
        /// <summary>
        /// Schedule Category Name
        /// </summary>
        public string Name
        {
            get { return name; }
            set { name = value; OnPropertyChanged("Name"); }
        }

        public ScheduleCategory(int id, string name)
        {
            this.id = id;
            this.name = name;
        }

        public override string ToString()
        {
            return this.name;
            //return base.ToString();
        }
    }
}
