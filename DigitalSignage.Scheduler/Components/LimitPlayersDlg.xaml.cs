﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.ComponentModel;

using NLog;
using System.Xml;

namespace DigitalSignage.Scheduler
{
    /// <summary>
    /// Interaction logic for NewDlgItem.xaml
    /// </summary>
    public partial class LimitPlayersDlg : Window
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        public bool result = false;
        private Config cfg = null;

        private bool isInt = true;
        
        public string GROUPID = "";

		public LimitPlayersDlg(Config config)
        {
            InitializeComponent();
            cfg = config;

			groupBox.SetParent(null, cfg);
			tbPlayersCount.Focus();

// 			groupBox.UpdateTree();
			groupBox.SelectedGroupGID = GROUPID;
        }

		public XmlNode PlayerGroup
		{
			get { return groupBox.SelectedGroup; }
			set
			{
				if (value != null)
					groupBox.SelectedGroup = value;
				OnPropertyChanged("PlayerGroup");
			}
		}

		public void LoadData()
		{
			try
			{
				string gid = groupBox.SelectedGroupGID;
				int nCnt = cfg.ServerPlayersList.GetLimitInfo(gid);

				tbPlayersCount.Text = nCnt.ToString();
			}
			catch (System.Exception e)
			{
				tbPlayersCount.Text = (-1).ToString();
			}
		}

		public bool SaveData()
		{
			try
			{
				string gid = groupBox.SelectedGroupGID;
				int nCnt = 0;
				if (!int.TryParse(tbPlayersCount.Text, out nCnt))
					nCnt = -1;

				cfg.ServerPlayersList.AddPlayerLimitInfo(nCnt, gid);
				return true;
			}
			catch
			{
			}
			return false;
		}

		#region Event Handler

		#region INotify
		public event PropertyChangedEventHandler PropertyChanged;
		protected void OnPropertyChanged(string propertyName)
		{
			if (this.PropertyChanged != null)
				PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
		}
		#endregion

		void tbPlayersCount_PreviewTextInput(object sender, TextCompositionEventArgs e)
		{
			try
			{
				int digit = 0;
				if (!Int32.TryParse(e.Text, out digit))
				{
					e.Handled = true;
					isInt = false;
				}
				else
					isInt = true;
			}
			catch { }
		}
		void groupBox_OnSelectionChanged(object sender, TreeViewItem newSelection)
		{
			LoadData();
		}


		private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            try
            {
				SaveData();
                this.Close();
            }
            catch (Exception err)
            {
                logger.Error(err + "");
            }
        }

		private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

		void tb_GotKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
		{
			try
			{
				TextBox tb = sender as TextBox;
				if (tb != null) tb.SelectAll();
				else
				{
					PasswordBox pb = sender as PasswordBox;
					pb.SelectAll();
				}

				e.Handled = true;
			}
			catch { }
		}
		#endregion
    }
}
