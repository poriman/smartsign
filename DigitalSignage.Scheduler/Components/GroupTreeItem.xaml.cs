﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DigitalSignage.Common;
using System.Windows.Interop;
using System.Drawing;

namespace DigitalSignage.Scheduler
{

	/// <summary>
	/// Interaction logic for TaskItem.xaml
	/// </summary>
    public partial class GroupTreeItem : System.Windows.Controls.UserControl
	{

		public GroupTreeItem(bool bIsGroup, string body)
		{
			InitializeComponent();

			image.Source = GetIconImage(bIsGroup ? "../images/network-group.png" : "../images/player-group.png");
           
			this.body.Text = body;

			bCheck.Visibility = Visibility.Collapsed;
		}
		public GroupTreeItem(bool bIsGroup, string body, bool hasCheckButton)
		{
			InitializeComponent();

			image.Source = GetIconImage(bIsGroup ? "../images/network-group.png" : "../images/player-group.png");

			this.body.Text = body; 
			
			bCheck.Visibility = hasCheckButton ? Visibility.Visible : Visibility.Collapsed;
		}		
		public String Text
		{
			get
			{
				return body.Text;
			}
			set
			{
				body.Text = value;
			}
		}
		public String TextToolTip
		{
			get
			{
                return textToolTip.ToolTipText;
			}
			set
			{
                textToolTip.ToolTipText = value;
			}
		}

		public bool IsChecked
		{ 
			get { return bCheck.IsChecked == null ? false : bCheck.IsChecked.Value; }
			set
			{
				if (bCheck.Visibility == Visibility.Visible)
				{
					ChangeCheckState(this.DataContext as TreeViewItem, value);
				}
			}
		}

		public void UpdateToolTip()
		{
// 			try
// 			{
// 				Task task = this.DataContext as Task;
// 				textToolTip.Text = String.Format(Properties.Resources.tooltipTaskItem, task.description, task.pid.Contains("-1") ? "N/A" : task.pid, task.gid, task.type == TaskCommands.CmdDefaultProgram ? "N/A" : TimeConverter.ConvertFromUTP(task.TaskStart).ToLocalTime().ToString(), task.type == TaskCommands.CmdDefaultProgram ? "N/A" : TimeConverter.ConvertFromUTP(task.TaskEnd).ToLocalTime().ToString(), task.type == TaskCommands.CmdDefaultProgram ? task.duration.ToString() : "N/A");
// 
// 			}
// 			catch
// 			{
// 				TextToolTip = "";
// 			}
		}

		private BitmapImage GetIconImage(String source)
		{
			try
			{
				BitmapImage _bi = new BitmapImage();

				_bi.BeginInit();
				_bi.UriSource = new Uri("pack://application:,,,/" + source);
				_bi.EndInit();

				return _bi;
			}
			catch (Exception e)
			{
				MessageBox.Show(e.Message);
			}
			return null;
		}

		private void UserControl_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
		{
			e.Handled = false;
		}

		public void ChangeCheckState(TreeViewItem item, bool _bcheck)
		{
			GroupTreeItem chgti = item.Header as GroupTreeItem;
			if (chgti != null)
			{
				chgti.bCheck.IsChecked = _bcheck;
				RecursiveChangeChildState(item, _bcheck);
				RecursiveChangeParentState(item, _bcheck);
			}
		}

		private void UserControl_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{
			TreeViewItem item = this.DataContext as TreeViewItem;
			GroupTreeItem chgti = item.Header as GroupTreeItem;

			bool bIsCheck = false;
			if (chgti.bCheck.IsChecked == true)
				bIsCheck = false;
			else
			{
				bIsCheck = true;
			}

			ChangeCheckState(item, bIsCheck);
		}

		private void bCheck_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				TreeViewItem item = this.DataContext as TreeViewItem;              
				GroupTreeItem chgti = item.Header as GroupTreeItem;

				bool bIsCheck = false;
				if (chgti.bCheck.IsChecked == true)
					bIsCheck = true;
				else
				{
					bIsCheck = false;
				}

				RecursiveChangeChildState(item, bIsCheck);
				RecursiveChangeParentState(item, bIsCheck);
				e.Handled = true;
			}
			catch { }
		}

		void RecursiveChangeChildState(TreeViewItem item, bool _check)
		{
			if (!item.HasItems)
				return;

			foreach (TreeViewItem child in item.Items)
			{
				GroupTreeItem gti = child.Header as GroupTreeItem;

				if (gti != null && gti.bCheck.IsChecked != _check)
				{
					gti.bCheck.IsChecked = _check;
				}

				RecursiveChangeChildState(child, _check);
			}
		}

		void RecursiveChangeParentState(TreeViewItem item, bool _check)
		{
			if (item.Parent == null)
				return;

			TreeViewItem _parent = item.Parent as TreeViewItem;
			if (_parent != null)
			{
				GroupTreeItem gti = _parent.Header as GroupTreeItem;
				if (gti != null)
				{
					int nCheckedCount = 0;
					int nIndeterminate = 0;

					foreach (TreeViewItem child in _parent.Items)
					{
						GroupTreeItem chgti = child.Header as GroupTreeItem;

						if (chgti != null && chgti.bCheck.IsChecked == true)
							nCheckedCount++;
						else if (chgti != null && chgti.bCheck.IsChecked == null)
							nIndeterminate++;
					}

					if(nIndeterminate > 0)
						gti.bCheck.IsChecked = null;
					else if (nCheckedCount == 0)
						gti.bCheck.IsChecked = false;
					//else if (_parent.Items.Count == nCheckedCount)
					//{
					//    gti.bCheck.IsChecked = true;
					//}
					else gti.bCheck.IsChecked = null;
				}

				RecursiveChangeParentState(_parent, _check);
			}
		}
	}
}
