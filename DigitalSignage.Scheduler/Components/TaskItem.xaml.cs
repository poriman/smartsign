﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DigitalSignage.Common;
using System.Xml;
using System.ComponentModel;
using System.Collections.ObjectModel;

namespace DigitalSignage.Scheduler
{

	/// <summary>
	/// Interaction logic for TaskItem.xaml
	/// </summary>
    public partial class TaskItem : System.Windows.Controls.UserControl
	{
		//	Vertical의 순서가 변경되었을경우 호출된다.
		public delegate void ItemOrderChanged(object sender, System.Windows.Controls.Primitives.DragCompletedEventArgs e);
		public event ItemOrderChanged VerticalItemOrderChanged = null;

// 		public delegate void DraggingCompleted(object sender, EventArgs e)
// 		public event DraggingCompleted DragCompleted = null;

		public delegate void DraggingStarted(object sender, EventArgs e);
		public event DraggingStarted VerticalDraggingStarted = null;
		
		public delegate void ItemChanged(object sender, EventArgs e);
		public event ItemChanged HorizontalItemChanged = null;

		public ServerDatabase.ServerDataSet.downloadmasterRow[] tblDownloaded = null;

        public ServerDatabase.ServerDataSet.DownloadSummaryRow tblSummaryDownloaded = null;

		#region 중복 라인 경계 관련
		/// <summary>
		/// 중복 라인 경계 클래스
		/// </summary>
		public class LineScope
		{
			/// <summary>
			/// 좌측
			/// </summary>
			public double Left;
			/// <summary>
			/// 우측
			/// </summary>
			public double Right;

			/// <summary>
			/// 생성자
			/// </summary>
			/// <param name="left"></param>
			/// <param name="right"></param>
			public LineScope(double left, double right)
			{
				Left = left;
				Right = right;
			}

			/// <summary>
			/// 교집합이 있는지 검사
			/// </summary>
			/// <param name="desc"></param>
			/// <returns></returns>
			public bool HasIntersectionWith(LineScope desc)
			{
				if (this.Left > desc.Right) return false;
				if (this.Right < desc.Left) return false;

				return true;
			}

			/// <summary>
			/// 합집합 반환
			/// </summary>
			/// <param name="desc"></param>
			/// <returns></returns>
			public LineScope UnionWith(LineScope desc)
			{
				return new LineScope(Math.Min(this.Left, desc.Left), Math.Max(this.Right, desc.Right));
			}
		};

		/// <summary>
		/// 중복된 시간대 리스트
		/// </summary>
		private Collection<LineScope> arrDuplications = new Collection<LineScope>();

		/// <summary>
		/// 중복된 시간대 리스트
		/// </summary>
		public Collection<LineScope> Duplications
		{
			get { return arrDuplications; }
		}

		/// <summary>
		/// 중복 표시를 그린다.
		/// </summary>
		public void RedrawDuplications()
		{
			try
			{
				ArrangeDuplications();
				dup_grid.Children.Clear();
				foreach (LineScope line in arrDuplications)
				{
					Rectangle rect = new Rectangle();
//                    rect.Fill = Resources["CheckBackground"] as Brush;
                    rect.Fill = Application.Current.Resources["DuplicatedScheduleBrush"] as Brush;
//                    rect.Opacity = 0.8;
					rect.VerticalAlignment = System.Windows.VerticalAlignment.Stretch;
					rect.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
					dup_grid.Children.Add(rect);

					rect.Margin = new Thickness(line.Left, 0, 0, 0);
					rect.Width = line.Right - line.Left;

				}
			}
			catch { }
		}

		/// <summary>
		/// Duplication 겹침 정리
		/// </summary>
		private void ArrangeDuplications()
		{
			for (int i = 0; i < arrDuplications.Count; ++i)
			{
				LineScope scope = arrDuplications[i];
				try
				{
					for (int j = i + 1; j < arrDuplications.Count; ++j)
					{
						LineScope other = arrDuplications[j];

						if (scope.HasIntersectionWith(other))
						{
							arrDuplications[i] = scope.UnionWith(other);
							scope = arrDuplications[i];
							arrDuplications.RemoveAt(j--);
						}
					}
				}
				catch { }
			}
		}

		#endregion

        /// <summary>
        /// 생성자
        /// </summary>
		public TaskItem()
		{
			InitializeComponent();
		}
		
		bool _isModified = false;
		/// <summary>
		/// 수정 여부 판단
		/// </summary>
		public bool IsModified
		{
			get{
				return _isModified;
			}
			set{
				_isModified = value;
			}
		}

		#region be able to Move, Edit
		bool _isEditableLeftSide = true;
		bool _isEditableRightSide = true;
		public bool IsEditableLeftSide	//	왼쪽 모서리를 드래그 하여 수정가능한지
		{
			get { return _isEditableLeftSide; }
			set { _isEditableLeftSide = value; }
		}
		public bool IsEditableRightSide	//	오른쪽 모서리를 드래그 하여 수정가능한지
		{
			get { return _isEditableRightSide; }
			set { _isEditableRightSide = value; }
		}
		public bool IsMovable	//	드래그 하여 이동가능한지
		{
			get { return _isEditableLeftSide && _isEditableRightSide; }
		}
		#endregion

		//	10분마다 이동 거리
		double _valuePer10M = 0.0;
		/// <summary>
		/// 10분 마다 이동거리 계산
		/// </summary>
		public double ValuePer10M
		{
			get { return _valuePer10M; }
			set { _valuePer10M = value; }
		}

		public int totalPlayers = 0;

		/// <summary>
		/// 보더 색상
		/// </summary>
		public Brush Border
		{
			set
			{
				border.BorderBrush = value;
			}
		}

        #region DependencyProperty 속성
        /// <summary>
        /// 그룹 스케줄인지 아닌지
        /// </summary>
        public bool IsGroup
        {
            get 
            {
                return (bool)GetValue(IsGroupProperty);
            }
            set
            {
                SetValue(IsGroupProperty, value);
                Coloring();
                UpdateIcons(false);
            }
            
        }

        public static readonly DependencyProperty IsGroupProperty =
            DependencyProperty.Register("IsGroup", typeof(bool), typeof(TaskItem), new UIPropertyMetadata(false,
                new PropertyChangedCallback(OnIsGroupChanged)));

        private static void OnIsGroupChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            
        }

        void Coloring()
        {
            Task t = this.DataContext as Task;
            if (t != null)
            {
                text.Foreground = (Brush)Application.Current.Resources["ScheduleForegroundBrush"];

                if (!IsDownloaded)
                {

                    body.Background =  (Brush)Application.Current.Resources["DuplicatedScheduleBrush"] ;

                }
                else if (IsGroup)
                {
                    body.Background = t.is_accessable ? (Brush)Application.Current.Resources["GroupScheduleBrush"] : (Brush)Application.Current.Resources["ParentScheduleBrush"] ;

                }
                else
                {
                    body.Background = t.is_accessable ? (Brush)Application.Current.Resources["PlayerScheduleBrush"] : (Brush)Application.Current.Resources["ParentScheduleBrush"] ;
                }
            }
        }

        /// <summary>
        /// 선택되었는지
        /// </summary>
        public bool IsSelected
        {
            get
            {
                return (bool)GetValue(IsSelectedProperty);
            }
            private set
            {
                SetValue(IsSelectedProperty, value);
                UpdateIcons(false);
            }

        }

        public static readonly DependencyProperty IsSelectedProperty =
            DependencyProperty.Register("IsSelected", typeof(bool), typeof(TaskItem), new UIPropertyMetadata(false,
                new PropertyChangedCallback(OnIsSelectedChanged)));

        private static void OnIsSelectedChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {

        }

        /// <summary>
        /// 다운로드가 완료되었는지
        /// </summary>
        public bool IsDownloaded
        {
            get
            {
                return (bool)GetValue(IsDownloadedProperty);
            }
            private set
            {
                SetValue(IsDownloadedProperty, value);
                UpdateIcons(false);
                Coloring();
            }

        }

        public static readonly DependencyProperty IsDownloadedProperty =
            DependencyProperty.Register("IsDownloaded", typeof(bool), typeof(TaskItem), new UIPropertyMetadata(false,
                new PropertyChangedCallback(OnIsDownloadedChanged)));

        private static void OnIsDownloadedChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {

        }

        #endregion


		public DigitalSignage.Common.TaskCommands TaskType
		{
			get
			{
				try
				{
					Task item = this.DataContext as Task;

					return item.type;

				}
				catch
				{
					return TaskCommands.CmdUndefined;
				}
			}
		}

        public void Select(TaskItem item)
        {
            Task t = this.DataContext as Task;
            Task st = item.DataContext as Task;

            if (this.Equals(item) || (t != null && st != null && t.uuid == st.uuid))
            {
                this.Opacity = 1.0;

                IsSelected = true;
            }
            else
            {
                this.Opacity = 0.3;

                IsSelected = false;
            }
        }

        public void Deselect()
        {
            this.Opacity = 1;

            IsSelected = false;
        }

		/// <summary>
		/// 텍스트
		/// </summary>
		public String Text
		{
			get
			{
				return text.Text;
			}
			set
			{
				text.Text = value;
			}
		}

		/// <summary>
		/// 툴팁 텍스트
		/// </summary>
		public String TextToolTip
		{
			get
			{
				return textToolTip.ToolTipText;
			}
			set
			{
				textToolTip.ToolTipText = value;
			}
		}

		/// <summary>
		/// 연결된 스케줄이 있는지
		/// </summary>
		public bool IsLinkedTask
		{
			get {
				try
				{
					return ((Task)DataContext).starttimeofday != ((Task)DataContext).endtimeofday;
				}
				catch {
					return false;
				}
			}
		}

		/// <summary>
		/// Task 데이터 대입
		/// </summary>
		public object DataContextWithMakeToolTip
		{
			set
			{
				DataContext = value;
				UpdateIcons(false);
				UpdateToolTip();
			}
		}

		private void UpdateIcons(bool isSelected)
        {
            try
            {
                Task t = DataContext as Task;
                spIcons.Children.Clear();

                //if ((t.show_flag & (int)ShowFlag.Show_Event) > 0)
                if (t.type == TaskCommands.CmdTouchDefaultProgram)
                {
                    Image img = GetIconImage(Properties.Resources.timeline_touch);
                    img.Stretch = Stretch.None;
                    img.VerticalAlignment = System.Windows.VerticalAlignment.Center;
                    img.Margin = new Thickness(0, 0, 3, 0);
                    spIcons.Children.Add(img);
                }
                if (t.isAd)
                {
                    Image img = GetIconImage(Properties.Resources.timeline_AD);
                    img.Stretch = Stretch.None;
                    img.VerticalAlignment = System.Windows.VerticalAlignment.Center;
                    img.Margin = new Thickness(0, 0, 3, 0);
                    spIcons.Children.Add(img);
                }
                if ((t.show_flag & (int)ShowFlag.Show_Allow_Duplication) == 0)
                {
                    Image img = GetIconImage(Properties.Resources.timeline_Only);
                    img.Stretch = Stretch.None;
                    img.VerticalAlignment = System.Windows.VerticalAlignment.Center;
                    img.Margin = new Thickness(0, 0, 3, 0);
                    spIcons.Children.Add(img);
                }

                if ((t.show_flag & (int)ShowFlag.Show_Event) > 0 && (t.show_flag & (int)ShowFlag.Show_NonEvent) == 0)
                {
                    Image img = GetIconImage(Properties.Resources.timeline_EventOnly);
                    img.Stretch = Stretch.None;
                    img.VerticalAlignment = System.Windows.VerticalAlignment.Center;
                    img.Margin = new Thickness(0, 0, 3, 0);
                    spIcons.Children.Add(img);
                }
                else if ((t.show_flag & (int)ShowFlag.Show_Event) > 0)
                {
                    Image img = GetIconImage(Properties.Resources.timeline_Event);
                    img.Stretch = Stretch.None;
                    img.VerticalAlignment = System.Windows.VerticalAlignment.Center;
                    img.Margin = new Thickness(0, 0, 3, 0);
                    spIcons.Children.Add(img);
                }
                
                if ((t.show_flag & (int)ShowFlag.Show_SyncMaster) > 0)
                {
                    Image img = GetIconImage(Properties.Resources.timeline_sync);
                    img.Stretch = Stretch.None;
                    img.VerticalAlignment = System.Windows.VerticalAlignment.Center;
                    img.Margin = new Thickness(0, 0, 3, 0);
                    spIcons.Children.Add(img);
                }
                else if ((t.show_flag & (int)ShowFlag.Show_SyncSlave) > 0)
                {
                    Image img = GetIconImage(Properties.Resources.timeline_sync);
                    img.Stretch = Stretch.None;
                    img.VerticalAlignment = System.Windows.VerticalAlignment.Center;
                    img.Margin = new Thickness(0, 0, 3, 0);
                    spIcons.Children.Add(img);
                }
            }
            catch { }

            #region Old
            /*
			try
			{
				Task t = DataContext as Task;
				spIcons.Children.Clear();

				//if ((t.show_flag & (int)ShowFlag.Show_Event) > 0)
				if (t.isAd)
				{
					Border bd = new Border();
					bd.Margin = new Thickness(0, 0, 2, 0);
					bd.BorderThickness = new Thickness(1);
					bd.CornerRadius = new CornerRadius(5);
					bd.BorderBrush = isSelected ? Brushes.LightGray : Brushes.White;
					
					bd.VerticalAlignment = System.Windows.VerticalAlignment.Center;
					bd.Width = 16;
					bd.Height = 16;

					TextBlock tb = new TextBlock();
					tb.Text = "AD";

					tb.Foreground = isSelected ? Brushes.LightGray : Brushes.White;
					tb.TextAlignment = TextAlignment.Center;
					tb.VerticalAlignment = System.Windows.VerticalAlignment.Center;
					tb.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
					tb.FontSize = 8;

					bd.Child = tb;

					spIcons.Children.Add(bd);

					//Image img = GetIconImage(Properties.Resources.icon_status_on);
					//img.VerticalAlignment = System.Windows.VerticalAlignment.Center;
					//img.Margin = new Thickness(5, 0, 0, 0);
					//spIcons.Children.Add(img);
				}
				if ((t.show_flag & (int)ShowFlag.Show_Allow_Duplication) == 0)
				{
					Border bd = new Border();
					bd.Margin = new Thickness(0, 0, 2, 0);
					bd.BorderThickness = new Thickness(1);
					bd.CornerRadius = new CornerRadius(5);
					bd.BorderBrush = isSelected ? Brushes.LightGray : Brushes.White;

					bd.VerticalAlignment = System.Windows.VerticalAlignment.Center;
					bd.Width = 21;
					bd.Height = 16;

					TextBlock tb = new TextBlock();
					tb.Text = "Only";

					tb.Foreground = isSelected ? Brushes.LightGray : Brushes.White;
					tb.TextAlignment = TextAlignment.Center;
					tb.VerticalAlignment = System.Windows.VerticalAlignment.Center;
					tb.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
					tb.FontSize = 8;

					bd.Child = tb;

					spIcons.Children.Add(bd);

					//Image img = GetIconImage(Properties.Resources.icon_default);
					//img.VerticalAlignment = System.Windows.VerticalAlignment.Center;
					//img.Margin = new Thickness(10, 0, 0, 0);

					//spIcons.Children.Add(img);
				}

				if ((t.show_flag & (int)ShowFlag.Show_Event) > 0 && (t.show_flag & (int)ShowFlag.Show_NonEvent) == 0)
				{
					Border bd = new Border();
					bd.Margin = new Thickness(0, 0, 2, 0);
					bd.BorderThickness = new Thickness(1);
					bd.CornerRadius = new CornerRadius(5);
					bd.BorderBrush = isSelected ? Brushes.LightGray : Brushes.White;

					bd.VerticalAlignment = System.Windows.VerticalAlignment.Center;
					bd.Width = 44;
					bd.Height = 16;

					TextBlock tb = new TextBlock();
					tb.Text = "Event Only";

					tb.Foreground = isSelected ? Brushes.LightGray : Brushes.White;
					tb.TextAlignment = TextAlignment.Center;
					tb.VerticalAlignment = System.Windows.VerticalAlignment.Center;
					tb.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
					tb.FontSize = 8;

					bd.Child = tb;

					spIcons.Children.Add(bd);
				}
				else if ((t.show_flag & (int)ShowFlag.Show_Event) > 0)
				{
					Border bd = new Border();
					bd.Margin = new Thickness(0, 0, 2, 0);
					bd.BorderThickness = new Thickness(1);
					bd.CornerRadius = new CornerRadius(5);
					bd.BorderBrush = isSelected ? Brushes.LightGray : Brushes.White;

					bd.VerticalAlignment = System.Windows.VerticalAlignment.Center;
					bd.Width = 24;
					bd.Height = 16;

					TextBlock tb = new TextBlock();
					tb.Text = "Event";

					tb.Foreground = isSelected ? Brushes.LightGray : Brushes.White;
					tb.TextAlignment = TextAlignment.Center;
					tb.VerticalAlignment = System.Windows.VerticalAlignment.Center;
					tb.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
					tb.FontSize = 8;

					bd.Child = tb;

					spIcons.Children.Add(bd);
				}
			}
			catch { }
             */
            #endregion
        }

		[System.Runtime.InteropServices.DllImport("gdi32.dll")]
		public static extern bool DeleteObject(IntPtr hObject);

		private System.Windows.Controls.Image GetIconImage(System.Drawing.Bitmap image)
		{
			IntPtr hBitmap = IntPtr.Zero;
			System.Windows.Controls.Image iconf = null;
			try
			{
				hBitmap = image.GetHbitmap();

				iconf = new System.Windows.Controls.Image();
				iconf.Source = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(hBitmap,
											IntPtr.Zero, new Int32Rect(0, 0, image.Width, image.Height),
											BitmapSizeOptions.FromEmptyOptions());
				iconf.Width = 16;
				iconf.Height = 16;

				return iconf;
			}
			catch { iconf = null; }
			finally
			{
				if (hBitmap != IntPtr.Zero) DeleteObject(hBitmap);
			}

			return iconf;
		}


		/// <summary>
		/// 요일 문자열 반환
		/// </summary>
		/// <param name="dayofweek"></param>
		/// <returns></returns>
		private String DayOfWeekString(int dayofweek)
		{
			String sReturn = Properties.Resources.labelAllDaysOfWeek;

			if (dayofweek == 127) return sReturn;

			sReturn = String.Empty;

			if (0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_MON))
				sReturn += (Properties.Resources.labelMon + " ");
			if (0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_TUE))
				sReturn += (Properties.Resources.labelTue + " ");
			if (0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_WED))
				sReturn += (Properties.Resources.labelWed + " ");
			if (0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_THU))
				sReturn += (Properties.Resources.labelThu + " ");
			if (0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_FRI))
				sReturn += (Properties.Resources.labelFri + " ");
			if (0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_SAT))
				sReturn += (Properties.Resources.labelSat + " ");
			if (0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_SUN))
				sReturn += (Properties.Resources.labelSun + " ");
			return sReturn;
		}

		/// <summary>
		/// 툴팁 생성
		/// </summary>
		public void UpdateToolTip()
		{
			try
			{
				Task task = this.DataContext as Task;

// 				string sDownloadInfo = "";
				Config cfg = Config.GetConfig;

                #region 다운로드 정보
                int nDownloaded = 0;

                if (tblSummaryDownloaded == null)
                {
                    nDownloaded = tblDownloaded == null ? 0 : tblDownloaded.Length;
                }
                else
                {
                    nDownloaded = tblSummaryDownloaded.player_cnt;
                }

				if (nDownloaded > totalPlayers) nDownloaded = totalPlayers;

				//this.Opacity = nDownloaded != totalPlayers ? 0.5 : 1;

                this.IsDownloaded = (nDownloaded == totalPlayers);

                #endregion

                string pname = "";
				string gname = "";
				try
				{
					if(!String.IsNullOrEmpty(task.pid) && !task.pid.Contains("-1"))
					{
						XmlNode pnode = ServerDataCache.GetInstance.GetPlayer(task.pid);
						pname = pnode.Attributes["name"].Value;
					}
				}
				catch {}
				try
				{
					XmlNode gnode = ServerDataCache.GetInstance.GetGroupByGID(task.gid);
					gname = gnode.Attributes["name"].Value;
				}
				catch {}

                #region Touch - CmdTouchDefaultProgram 추가로 수정
                /* 수정전
                if (task.starttimeofday == task.endtimeofday)
                    textToolTip.Text = String.Format(Properties.Resources.tooltipTaskItem, task.description, task.pid.Contains("-1") ? Properties.Resources.labelNone : (String.IsNullOrEmpty(pname) ? task.pid : pname), String.IsNullOrEmpty(gname) ? task.gid : gname, task.type == TaskCommands.CmdDefaultProgram ? Properties.Resources.labelNone : TimeConverter.ConvertFromUTP(task.TaskStart).ToLocalTime().ToString(), task.type == TaskCommands.CmdDefaultProgram ? Properties.Resources.labelNone : TimeConverter.ConvertFromUTP(task.TaskEnd).ToLocalTime().ToString(), task.type == TaskCommands.CmdDefaultProgram ? task.duration.ToString() : Properties.Resources.labelNone, nDownloaded, totalPlayers);
                else
                    textToolTip.Text = String.Format(Properties.Resources.tooltipLinkedTaskItem, task.description, task.pid.Contains("-1") ? Properties.Resources.labelNone : (String.IsNullOrEmpty(pname) ? task.pid : pname), String.IsNullOrEmpty(gname) ? task.gid : gname, task.type == TaskCommands.CmdDefaultProgram ? Properties.Resources.labelNone : TimeConverter.ConvertFromUTP(task.TaskStart).ToLocalTime().ToLongDateString(), task.type == TaskCommands.CmdDefaultProgram ? Properties.Resources.labelNone : TimeConverter.ConvertFromUTP(task.TaskEnd).ToLocalTime().ToLongDateString(), String.Format("{0} - {1}", TimeSpan.FromSeconds(task.starttimeofday).ToString(), TimeSpan.FromSeconds(task.endtimeofday).ToString()), DayOfWeekString(task.daysofweek), task.type == TaskCommands.CmdDefaultProgram ? task.duration.ToString() : Properties.Resources.labelNone, nDownloaded, totalPlayers);
                */

                //bmwe3
                //동기화 스켸줄이면 분기문 처리로 M/S를 넣도록 수정할 것.

                

                
                    if (task.type == TaskCommands.CmdSubtitles)
                    {
                        if (task.starttimeofday == task.endtimeofday)
                            TextToolTip = String.Format(Properties.Resources.tooltipSubtitleTaskItem, task.description, (String.IsNullOrEmpty(task.pid) || task.pid.Contains("-1")) ? Properties.Resources.labelNone : (String.IsNullOrEmpty(pname) ? task.pid : pname), String.IsNullOrEmpty(gname) ? task.gid : gname, TimeConverter.ConvertFromUTP(task.TaskStart)/*.ToLocalTime()*/.ToString(), TimeConverter.ConvertFromUTP(task.TaskEnd)/*.ToLocalTime()*/.ToString(), task.duration.ToString(), task.AllowDuplication ? Properties.Resources.labelYes : Properties.Resources.labelNo, task.isAd ? Properties.Resources.labelYes : Properties.Resources.labelNo, String.IsNullOrEmpty(task.MetaTags) ? Properties.Resources.labelNone : task.MetaTags, GetPlayConstrait(task.show_flag), nDownloaded, totalPlayers);
                        else
                            TextToolTip = String.Format(Properties.Resources.tooltipLinkedSubtitleTaskItem, task.description, (String.IsNullOrEmpty(task.pid) || task.pid.Contains("-1")) ? Properties.Resources.labelNone : (String.IsNullOrEmpty(pname) ? task.pid : pname), String.IsNullOrEmpty(gname) ? task.gid : gname, TimeConverter.ConvertFromUTP(task.TaskStart)/*.ToLocalTime()*/.ToLongDateString(), TimeConverter.ConvertFromUTP(task.TaskEnd)/*.ToLocalTime()*/.ToLongDateString(), String.Format("{0} - {1}", TimeSpan.FromSeconds(task.starttimeofday).ToString(), TimeSpan.FromSeconds(task.endtimeofday).ToString()), DayOfWeekString(task.daysofweek), task.duration.ToString(), task.AllowDuplication ? Properties.Resources.labelYes : Properties.Resources.labelNo, task.isAd ? Properties.Resources.labelYes : Properties.Resources.labelNo, String.IsNullOrEmpty(task.MetaTags) ? Properties.Resources.labelNone : task.MetaTags, GetPlayConstrait(task.show_flag), nDownloaded, totalPlayers);

                    }
                    else if (task.type == TaskCommands.CmdControlSchedule)
                    {
                        if (task.starttimeofday == task.endtimeofday)
                            TextToolTip = String.Format(Properties.Resources.tooltipControlTaskItem, task.description, (String.IsNullOrEmpty(task.pid) || task.pid.Contains("-1")) ? Properties.Resources.labelNone : (String.IsNullOrEmpty(pname) ? task.pid : pname), String.IsNullOrEmpty(gname) ? task.gid : gname, TimeConverter.ConvertFromUTP(task.TaskStart)/*.ToLocalTime()*/.ToString(), TimeConverter.ConvertFromUTP(task.TaskEnd)/*.ToLocalTime()*/.ToString(), task.duration.ToString(), task.AllowDuplication ? Properties.Resources.labelYes : Properties.Resources.labelNo, task.isAd ? Properties.Resources.labelYes : Properties.Resources.labelNo, String.IsNullOrEmpty(task.MetaTags) ? Properties.Resources.labelNone : task.MetaTags, GetPlayConstrait(task.show_flag), nDownloaded, totalPlayers);
                        else
                            TextToolTip = String.Format(Properties.Resources.tooltipLinkedControlTaskItem, task.description, (String.IsNullOrEmpty(task.pid) || task.pid.Contains("-1")) ? Properties.Resources.labelNone : (String.IsNullOrEmpty(pname) ? task.pid : pname), String.IsNullOrEmpty(gname) ? task.gid : gname, TimeConverter.ConvertFromUTP(task.TaskStart)/*.ToLocalTime()*/.ToLongDateString(), TimeConverter.ConvertFromUTP(task.TaskEnd)/*.ToLocalTime()*/.ToLongDateString(), String.Format("{0} - {1}", TimeSpan.FromSeconds(task.starttimeofday).ToString(), TimeSpan.FromSeconds(task.endtimeofday).ToString()), DayOfWeekString(task.daysofweek), task.duration.ToString(), task.AllowDuplication ? Properties.Resources.labelYes : Properties.Resources.labelNo, task.isAd ? Properties.Resources.labelYes : Properties.Resources.labelNo, String.IsNullOrEmpty(task.MetaTags) ? Properties.Resources.labelNone : task.MetaTags, GetPlayConstrait(task.show_flag), nDownloaded, totalPlayers);

                    }
                    else
                    {
                        string mheader = "";
                        if (((task.show_flag & (int)ShowFlag.Show_SyncMaster) > 0 || (task.show_flag & (int)ShowFlag.Show_SyncSlave) > 0))
                        {
                            mheader = "Y";

                            if (task.starttimeofday == task.endtimeofday)
                                TextToolTip = String.Format(Properties.Resources.tooltipSyncTaskItem, task.description, (String.IsNullOrEmpty(task.pid) || task.pid.Contains("-1")) ? Properties.Resources.labelNone : (String.IsNullOrEmpty(pname) ? task.pid : pname), String.IsNullOrEmpty(gname) ? task.gid : gname, TimeConverter.ConvertFromUTP(task.TaskStart)/*.ToLocalTime()*/.ToString(), TimeConverter.ConvertFromUTP(task.TaskEnd)/*.ToLocalTime()*/.ToString(), task.duration.ToString(), task.AllowDuplication ? Properties.Resources.labelYes : Properties.Resources.labelNo, task.isAd ? Properties.Resources.labelYes : Properties.Resources.labelNo, String.IsNullOrEmpty(task.MetaTags) ? Properties.Resources.labelNone : task.MetaTags, GetPlayConstrait(task.show_flag), nDownloaded, totalPlayers,mheader);
                            else
                                TextToolTip = String.Format(Properties.Resources.tooltipLinkedSyncTaskItem, task.description, (String.IsNullOrEmpty(task.pid) || task.pid.Contains("-1")) ? Properties.Resources.labelNone : (String.IsNullOrEmpty(pname) ? task.pid : pname), String.IsNullOrEmpty(gname) ? task.gid : gname, TimeConverter.ConvertFromUTP(task.TaskStart)/*.ToLocalTime()*/.ToLongDateString(), TimeConverter.ConvertFromUTP(task.TaskEnd)/*.ToLocalTime()*/.ToLongDateString(), String.Format("{0} - {1}", TimeSpan.FromSeconds(task.starttimeofday).ToString(), TimeSpan.FromSeconds(task.endtimeofday).ToString()), DayOfWeekString(task.daysofweek), task.duration.ToString(), task.AllowDuplication ? Properties.Resources.labelYes : Properties.Resources.labelNo, task.isAd ? Properties.Resources.labelYes : Properties.Resources.labelNo, String.IsNullOrEmpty(task.MetaTags) ? Properties.Resources.labelNone : task.MetaTags, GetPlayConstrait(task.show_flag), nDownloaded, totalPlayers, mheader);
                        }
                        else
                        {

                            if (task.starttimeofday == task.endtimeofday)
                                TextToolTip = String.Format(Properties.Resources.tooltipTaskItem, task.description, (String.IsNullOrEmpty(task.pid) || task.pid.Contains("-1")) ? Properties.Resources.labelNone : (String.IsNullOrEmpty(pname) ? task.pid : pname), String.IsNullOrEmpty(gname) ? task.gid : gname, TimeConverter.ConvertFromUTP(task.TaskStart)/*.ToLocalTime()*/.ToString(), TimeConverter.ConvertFromUTP(task.TaskEnd)/*.ToLocalTime()*/.ToString(), task.duration.ToString(), task.AllowDuplication ? Properties.Resources.labelYes : Properties.Resources.labelNo, task.isAd ? Properties.Resources.labelYes : Properties.Resources.labelNo, String.IsNullOrEmpty(task.MetaTags) ? Properties.Resources.labelNone : task.MetaTags, GetPlayConstrait(task.show_flag), nDownloaded, totalPlayers);
                            else
                                TextToolTip = String.Format(Properties.Resources.tooltipLinkedTaskItem, task.description, (String.IsNullOrEmpty(task.pid) || task.pid.Contains("-1")) ? Properties.Resources.labelNone : (String.IsNullOrEmpty(pname) ? task.pid : pname), String.IsNullOrEmpty(gname) ? task.gid : gname, TimeConverter.ConvertFromUTP(task.TaskStart)/*.ToLocalTime()*/.ToLongDateString(), TimeConverter.ConvertFromUTP(task.TaskEnd)/*.ToLocalTime()*/.ToLongDateString(), String.Format("{0} - {1}", TimeSpan.FromSeconds(task.starttimeofday).ToString(), TimeSpan.FromSeconds(task.endtimeofday).ToString()), DayOfWeekString(task.daysofweek), task.duration.ToString(), task.AllowDuplication ? Properties.Resources.labelYes : Properties.Resources.labelNo, task.isAd ? Properties.Resources.labelYes : Properties.Resources.labelNo, String.IsNullOrEmpty(task.MetaTags) ? Properties.Resources.labelNone : task.MetaTags, GetPlayConstrait(task.show_flag), nDownloaded, totalPlayers);
                        }



                    }
               
               
                #endregion

            }
			catch
			{
				TextToolTip = "";
			}
		}

		/// <summary>
		/// 재생 조건을 문자열로 반환한다.
		/// </summary>
		/// <param name="Showflag"></param>
		/// <returns></returns>
		private String GetPlayConstrait(int Showflag)
		{
			String sReturn = String.Empty;
			if ((Showflag & (int)ShowFlag.Show_NonEvent) > 0)
			{
				sReturn = Properties.Resources.labelAdConstraintNonEvent;
			}

			if ((Showflag & (int)ShowFlag.Show_Event) > 0)
			{
				if (String.IsNullOrEmpty(sReturn))
					sReturn = Properties.Resources.labelAdConstraintEvent;
				else
					sReturn = String.Format("{0}, {1}", sReturn, Properties.Resources.labelAdConstraintEvent);
			}

			if (String.IsNullOrEmpty(sReturn))
			{
				sReturn = Properties.Resources.labelAdConstraintNone;
			}

			return sReturn;
		}

		private void UserControl_SizeChanged(object sender, SizeChangedEventArgs e)
		{
			double dHeight = this.ActualHeight;
				
			if(dHeight > 12.0)
			{
				text.FontSize = 12.0;
			}
			else if(dHeight <= 0)
			{
				text.FontSize = 8.0;
			}
			else
			{
				text.FontSize = dHeight;
			}
		}

		private void UserControl_Unloaded(object sender, RoutedEventArgs e)
		{ 
		}

		#region Drag Event
		private void LThumb_DragDelta(object sender, System.Windows.Controls.Primitives.DragDeltaEventArgs e)
		{
			try
			{
				Task item = this.DataContext as Task;
				if (!item.is_accessable) return;

				if (!ServerDataCache.GetInstance.HasPermission("UPDATE SCHEDULE"))
					return;

				double dDelta = e.HorizontalChange;
				if (IsEditableLeftSide && Math.Abs(dDelta) >= ValuePer10M)
				{
					int count = Convert.ToInt32(Math.Abs(dDelta) % ValuePer10M);
					double value = dDelta > 0 ? count * ValuePer10M : -1 * count * ValuePer10M;
					long timespan = dDelta > 0 ? count * 600 : count * -1 * 600;

					//	Width가 5 Pixel 아래로 내려가선 안된다.
					if (this.Width - value > 5)
					{
						item.TaskStart += timespan;

						if (item.starttimeofday != item.endtimeofday)
						{
							item.starttimeofday += (int)timespan;
						}

						Canvas.SetLeft(this, Canvas.GetLeft(this) + value);
						this.Width += -1*value;
						_isModified = true;
						
						UpdateToolTip();

						ToolTip tooltip = this.ToolTip as ToolTip;
						tooltip.IsOpen = true;
						
						//	수정이 된 경우
						if(HorizontalItemChanged != null) HorizontalItemChanged(this, (EventArgs)e);
					}
				}

			}
			catch
			{
				this.Width = 5;
			}

			e.Handled = true;
		}

		private void RThumb_DragDelta(object sender, System.Windows.Controls.Primitives.DragDeltaEventArgs e)
		{
			try
			{
				Task item = this.DataContext as Task;
				if (!item.is_accessable) return;

				if (!ServerDataCache.GetInstance.HasPermission("UPDATE SCHEDULE"))
					return;

				double dDelta = e.HorizontalChange;
				if (IsEditableRightSide && Math.Abs(dDelta) >= ValuePer10M)
				{
					int count = Convert.ToInt32(Math.Abs(dDelta) % ValuePer10M);
					double value = dDelta > 0 ? count * ValuePer10M : -1 * count * ValuePer10M;
					long timespan = dDelta > 0 ? count * 600 : count * -1 * 600;
					//	Width가 5 Pixel 아래로 내려가선 안된다.
					if (this.Width + value > 5)
					{
						this.Width += value;

						item.TaskEnd += timespan;

						if (item.starttimeofday != item.endtimeofday)
						{
							item.endtimeofday += (int)timespan;
						}

						_isModified = true;

						UpdateToolTip();

						ToolTip tooltip = this.ToolTip as ToolTip;
						tooltip.IsOpen = true;

						//	수정이 된 경우
						if (HorizontalItemChanged != null) HorizontalItemChanged(this, (EventArgs)e);
					}
				}
			}
			catch
			{
				this.Width = 5;
			}

			e.Handled = true;
		}

		private void Thumb_DragDelta(object sender, System.Windows.Controls.Primitives.DragDeltaEventArgs e)
		{
			try
			{
				Task item = this.DataContext as Task;
				if (!item.is_accessable) return;

				if (!ServerDataCache.GetInstance.HasPermission("UPDATE SCHEDULE"))
					return;

				double dVDelta = e.VerticalChange;
				double dDelta = e.HorizontalChange;
				
				if (Math.Abs(dVDelta) >= (this.Height * 1.5))
				{
					if (VerticalDraggingStarted != null) VerticalDraggingStarted(this, e);
					/*
					Canvas.SetTop(this, Canvas.GetTop(this) + dVDelta);
					_isModified = true;

					UpdateToolTip();

					ToolTip tooltip = this.ToolTip as ToolTip;
					tooltip.IsOpen = true;
					*/
				}
				else if (IsMovable && Math.Abs(dDelta) >= ValuePer10M)
				{
					int count = Convert.ToInt32(Math.Abs(dDelta) % ValuePer10M);
					double value = dDelta > 0 ? count * ValuePer10M : -1 * count * ValuePer10M;
					long timespan = dDelta > 0 ? count * 600 : count * -1 * 600;
					item.TaskStart += timespan;
					item.TaskEnd += timespan;

					if(item.starttimeofday != item.endtimeofday)
					{
						item.starttimeofday += (int)timespan;
						item.endtimeofday += (int)timespan;
					}

					Canvas.SetLeft(this, Canvas.GetLeft(this) + value);
					_isModified = true;

					UpdateToolTip();

					ToolTip tooltip = this.ToolTip as ToolTip;
					tooltip.IsOpen = true;

					//	수정이 된 경우
					if (HorizontalItemChanged != null) HorizontalItemChanged(this, (EventArgs)e);

				}
			}
			catch
			{
				this.Width = 5;
			}

			e.Handled = true;
		}

		private void RThumb_DragCompleted(object sender, System.Windows.Controls.Primitives.DragCompletedEventArgs e)
		{
			try
			{
				Task item = this.DataContext as Task;
				if (!item.is_accessable) return;

				if (!ServerDataCache.GetInstance.HasPermission("UPDATE SCHEDULE"))
					return;

				ToolTip tooltip = this.ToolTip as ToolTip;
				tooltip.IsOpen = false;

			}
			catch
			{

			}
// 			if(DragCompleted) DragCompleted(this, e);

			e.Handled = true;

		}

		private void LThumb_DragCompleted(object sender, System.Windows.Controls.Primitives.DragCompletedEventArgs e)
		{
			try
			{
				Task item = this.DataContext as Task;
				if (!item.is_accessable) return;

				if (!ServerDataCache.GetInstance.HasPermission("UPDATE SCHEDULE"))
					return;

				ToolTip tooltip = this.ToolTip as ToolTip;
				tooltip.IsOpen = false;
			}
			catch
			{

			}

// 			if(DragCompleted) DragCompleted(this, e);
	
			e.Handled = true;

		}

		private void Thumb_DragCompleted(object sender, System.Windows.Controls.Primitives.DragCompletedEventArgs e)
		{
			try
			{
				Task item = this.DataContext as Task;
				if (!item.is_accessable) return;

				if (!ServerDataCache.GetInstance.HasPermission("UPDATE SCHEDULE"))
					return;


				ToolTip tooltip = this.ToolTip as ToolTip;
				tooltip.IsOpen = false;
			}
			catch
			{

			}

			if(VerticalItemOrderChanged != null)
			{
				VerticalItemOrderChanged(this, e);
			}

// 			if(DragCompleted) DragCompleted(this, e);
// 
			e.Handled = true;
		}
		#endregion

		private void UserControl_PreviewKeyDown(object sender, KeyEventArgs e)
		{
			if (System.Windows.Input.Keyboard.GetKeyStates(Key.LeftCtrl) == KeyStates.Down)
			{
				if (e.Key == System.Windows.Input.Key.C)
				{
					try
					{
						Task item = this.DataContext as Task;
						Clipboard.SetText(item.description + "\t" + TimeSpan.FromSeconds(item.duration));
					}
					catch {}
				}
			}
		}

	}
}
