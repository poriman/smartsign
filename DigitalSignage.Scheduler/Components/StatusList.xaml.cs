﻿using System;
using System.Windows.Interop;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Reflection;

using NLog;

namespace DigitalSignage.Scheduler
{
    /// <summary>
    /// Interaction logic for GroupTree.xaml
    /// </summary>
    public partial class StatusList : Grid
    {

        #region Events
        public delegate void SelectionChanged(object sender, TreeViewItem newSelection);
        public event SelectionChanged OnSelectionChanged = null;
        #endregion

        private static Logger logger = LogManager.GetCurrentClassLogger();

        public StatusList()
        {
            InitializeComponent();
        }

        public int AddItem( string icon, string name, string text )
        {
            ListViewItem item = new ListViewItem();
            
            Grid grid = new Grid();
            ColumnDefinition column = new ColumnDefinition();
            grid.ColumnDefinitions.Add(column);
            column = new ColumnDefinition();
            grid.ColumnDefinitions.Add(column);

            BitmapImage bi = new BitmapImage();
            bi.BeginInit();
            bi.UriSource = new UriBuilder(icon).Uri;
            bi.EndInit();
            Image iconf = new Image();
            iconf.Source = bi;
            iconf.Width = 32;
            iconf.Height = 32;
            Grid.SetColumn(iconf, 0);
            grid.Children.Add(iconf);

            Label namef = new Label();
            namef.Content = name;
            namef.FontWeight = FontWeights.Bold;
            namef.FontSize = namef.FontSize + 1;
            Grid.SetColumn(namef, 1);
            grid.Children.Add(namef);

            Label textf = new Label();
            textf.Content = text;
            textf.Margin = new Thickness(0, 18, 0, 0);
            Grid.SetColumn(textf, 1);
            grid.Children.Add(textf);

            item.Content = grid;

            return list.Items.Add(item);
        }

        public int AddItem( System.Drawing.Bitmap icon, string name, string text)
        {
            ListViewItem item = new ListViewItem();

            Grid grid = new Grid();
            ColumnDefinition column = new ColumnDefinition();
            grid.ColumnDefinitions.Add(column);
            column = new ColumnDefinition();
            grid.ColumnDefinitions.Add(column);

            Image iconf = new Image();
            BitmapSource bitmapSource = Imaging.CreateBitmapSourceFromHBitmap( icon.GetHbitmap(), IntPtr.Zero,
                new Int32Rect(0, 0, icon.Width, icon.Height), BitmapSizeOptions.FromEmptyOptions());
            iconf.Source = bitmapSource;
            iconf.Width = 32;
            iconf.Height = 32;
            Grid.SetColumn(iconf, 0);
            grid.Children.Add(iconf);

            Label namef = new Label();
            namef.Content = name;
            namef.FontWeight = FontWeights.Bold;
            namef.FontSize = namef.FontSize + 1;
            Grid.SetColumn(namef, 1);
            grid.Children.Add(namef);

            Label textf = new Label();
            textf.Content = text;
            textf.Margin = new Thickness(0, 18, 0, 0);
            Grid.SetColumn(textf, 1);
            grid.Children.Add(textf);

            item.Content = grid;

            return list.Items.Add(item);
        }

        public void UpdateItem(int id, string icon, string name, string text)
        {
            ListViewItem item = (ListViewItem)list.Items[id];
            Grid grid = (Grid)item.Content;

            if (icon != null)
            {
                BitmapImage bi = new BitmapImage();
                bi.BeginInit();
                bi.UriSource = new UriBuilder(icon).Uri;
                bi.EndInit();
                Image iconf = (Image)grid.Children[0];
                iconf.Source = bi;
            }
            if (name != null)
                ((Label)grid.Children[1]).Content = name;
            if (text != null)
                ((Label)grid.Children[2]).Content = text;
        }

        public void UpdateItem(int id, System.Drawing.Bitmap icon, string name, string text)
        {
            ListViewItem item = (ListViewItem)list.Items[id];
            Grid grid = (Grid)item.Content;

            if (icon != null)
            {
                Image iconf = (Image)grid.Children[0]; 
                BitmapSource bitmapSource = Imaging.CreateBitmapSourceFromHBitmap(icon.GetHbitmap(), IntPtr.Zero,
                                new Int32Rect(0, 0, icon.Width, icon.Height), BitmapSizeOptions.FromEmptyOptions());
                iconf.Source = bitmapSource;
            }
            if (name != null)
                ((Label)grid.Children[1]).Content = name;
            if (text != null)
                ((Label)grid.Children[2]).Content = text;
        }

		public int SelectedIndex
		{
			get { return list.SelectedIndex; }
			set { list.SelectedIndex = value; }
		}
    }
}
