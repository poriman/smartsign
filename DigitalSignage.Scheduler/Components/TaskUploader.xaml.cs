﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;
using System.Windows.Threading;
using DigitalSignage.Common;
using System.Collections.ObjectModel;

using System.Xml;
using System.ComponentModel;

namespace DigitalSignage.Scheduler
{
	/// <summary>
	/// Interaction logic for TaskUploader.xaml
	/// </summary>
	public partial class TaskUploader : Window
	{
		private string m_prgName;
		private Config m_config = null;
		private Collection<XmlNode> m_rowGroups = null;
        private String m_strGroup = "";
		private Guid m_duuid;
		
		private ProgramUploader uploader = new ProgramUploader();
		private BackgroundWorker bwUpload;


		public Collection<ScreenPropertyLoader> UploadedScreens
		{
			get
			{
				return uploader.LoadedScreenProperties;
			}
		}

		public TaskUploader(string programName, Config cfg, ref Collection<XmlNode> group, Guid duuid)
		{
			m_prgName = programName;
			m_config = cfg;
			m_rowGroups = group;
			m_duuid = duuid;

			uploader.OnSetProgress = new EventHandler(InitialProgress);
			uploader.OnStepIt = new EventHandler(StepIt);
			uploader.OnUpdateLabel = new StatusEventHandler(UpdateLabel);
			uploader.OnUpdateTitle += new StatusEventHandler(UpdateTitle);

			InitializeComponent();
		}

        public TaskUploader(string programName, Config cfg, ref String group, Guid duuid)
        {
            m_prgName = programName;
            m_config = cfg;
            m_strGroup = group;
            m_duuid = duuid;

            uploader.OnSetProgress = new EventHandler(InitialProgress);
            uploader.OnStepIt = new EventHandler(StepIt);
            uploader.OnUpdateLabel = new StatusEventHandler(UpdateLabel);
            uploader.OnUpdateTitle += new StatusEventHandler(UpdateTitle);

            InitializeComponent();
        }
	
		void StepIt(Object obj, EventArgs e)
		{
			StepIt();
		}
		
		void InitialProgress(Object obj, EventArgs e)
		{
			FTPFunctions ftp = obj as FTPFunctions;
			if(ftp != null)
			{
				InitialProgress(ftp.ProgressCount);
			}
		}
		void UpdateLabel(Object obj, StatusEventArgs e)
		{
			if (e.OriginalSource != null)
			{
				UpdateLabel(e.OriginalSource as String);
			}
		}
		void UpdateTitle(Object obj, StatusEventArgs e)
		{
			if (e.OriginalSource != null)
			{
				UpdateTitle(e.OriginalSource as String);
			}
		}
		
		private void UploadTask()
		{
			bwUpload = new BackgroundWorker();
			bwUpload.DoWork += new DoWorkEventHandler(bwUpload_DoWork);
			bwUpload.WorkerSupportsCancellation = true;
			bwUpload.RunWorkerAsync();
		}

		void bwUpload_DoWork(object sender, DoWorkEventArgs e)
		{
			try
			{
				if (!uploader.PrepareUpload(ref m_prgName, m_config, m_duuid))
				{
					if (!String.IsNullOrEmpty(uploader.LastErrorMessage)) MessageBox.Show(uploader.LastErrorMessage, Properties.Resources.titleMessageBox);
					Finish(false);
					return;
				}

				XmlNode[] groups = new XmlNode[m_rowGroups.Count];

				m_rowGroups.CopyTo(groups, 0);
				foreach (XmlNode group in groups)
				{
					bool bRetry = true;

					while (bRetry)
					{
						using (new DigitalSignage.Common.WaitCursor())
						{

							if (Guid.Empty == uploader.UploadProgram(m_config, group, m_duuid))
							{

								MessageBoxResult result = MessageBox.Show(String.Format(Properties.Resources.mbErrorUpload, uploader.LastErrorMessage), Properties.Resources.titleMessageBox, MessageBoxButton.YesNoCancel);

								switch (result)
								{
									case MessageBoxResult.Yes:
										bRetry = true;
										break;
									case MessageBoxResult.No:
										m_rowGroups.Remove(group);

										bRetry = false;
										break;
									case MessageBoxResult.Cancel:

										bRetry = false;
										try
										{
											foreach (XmlNode delgroup in m_rowGroups)
											{
												uploader.DeleteProgram(m_config, delgroup, m_duuid);
											}
										}
										catch { }
										throw new Exception("CANCEL");
									// 										break;
								}
							}
							else
							{
								break;
							}
						}
					}
				}
				Finish(true);
			}
			catch (Exception ex)
			{
				if (!ex.Message.Equals("CANCEL"))
				{
					MessageBox.Show(e.ToString());
				}
				Finish(false);
			}
		}

		private void Window_Loaded(object sender, RoutedEventArgs e)
		{
			UploadTask();
		}

		#region Async routines
		delegate void finishCB(bool bExitCode);
		public void Finish(bool bExitCode)
		{
			try
			{
				if (!this.Dispatcher.CheckAccess())
				{
					this.Dispatcher.BeginInvoke(DispatcherPriority.Render,
					   new finishCB(Finish), bExitCode);
				}
				else
				{
					this.DialogResult = bExitCode;
					Close();
				}
			}
			catch (Exception /*e*/)
			{
// 				MessageBox.Show(e.Message);
			}
		}

		delegate void InitialProgressCB(int nProgressCount);
		void InitialProgress(int nProgressCount)
		{
			try
			{
				if (!this.Dispatcher.CheckAccess())
				{
					this.Dispatcher.BeginInvoke(DispatcherPriority.Render,
					   new InitialProgressCB(InitialProgress), nProgressCount);
				}
				else
				{
					progressBarCopyProgress.Maximum = nProgressCount;
					progressBarCopyProgress.Value = 0;
				}
			}
			catch (Exception e)
			{
				MessageBox.Show(e.Message);
			}
		}

		delegate void StepItCB();
		void StepIt()
		{
			try
			{
				if (!this.Dispatcher.CheckAccess())
				{
					this.Dispatcher.BeginInvoke(DispatcherPriority.Render,
					   new StepItCB(StepIt));
				}
				else
				{
					progressBarCopyProgress.Value++;
				}
			}
			catch (Exception e)
			{
				MessageBox.Show(e.Message);
			}
		}
		delegate void UpdateTitleLabelCB(String statusLabel);
		void UpdateTitle(String statusLabel)
		{
			try
			{
				if (!this.Dispatcher.CheckAccess())
				{
					this.Dispatcher.BeginInvoke(DispatcherPriority.Render,
					   new UpdateTitleLabelCB(UpdateTitle), statusLabel);
				}
				else
				{
					lbStatus.Content = statusLabel;
				}
			}
			catch (Exception e)
			{
				MessageBox.Show(e.Message);
			}
		}
		delegate void UpdateLabelCB(String statusLabel);
		void UpdateLabel(String statusLabel)
		{
			try
			{
				if (!this.Dispatcher.CheckAccess())
				{
					this.Dispatcher.BeginInvoke(DispatcherPriority.Render,
					   new UpdateLabelCB(UpdateLabel), statusLabel);
				}
				else
				{
					String content;
					String[] sarray = statusLabel.Split('\\');
					if(sarray.Length > 3)
					{
						content = sarray[0] + "\\..\\" + sarray[sarray.Length - 1];
					}
					else
					{
						content = statusLabel;
					}

					lbStatusContent.Content = content;
				}
			}
			catch (Exception e)
			{
				MessageBox.Show(e.Message);
			}
		}

		#endregion  

		private void Window_Unloaded(object sender, RoutedEventArgs e)
		{
			try
			{
				if (bwUpload != null)
				{
					if(bwUpload.IsBusy) bwUpload.CancelAsync();
					bwUpload.Dispose();
					bwUpload = null;
				}
			}
			catch
			{
			}
		}

		private void btnCancel_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				if (bwUpload != null)
				{
					if (bwUpload.IsBusy) bwUpload.CancelAsync();
					bwUpload.Dispose();
					bwUpload = null;

					try
					{
						foreach (XmlNode delgroup in m_rowGroups)
						{
							uploader.DeleteProgram(m_config, delgroup, m_duuid);
						}
					}
					catch{}

					Finish(false);
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}
		}

        public void RemoveTemporaryFiles()
        {
            String sExecutingPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            String tempDir = sExecutingPath + "\\SchedulerData\\temp\\";

            FTPFunctions.RecursiveDelete(tempDir, true);
        }
	}
}
