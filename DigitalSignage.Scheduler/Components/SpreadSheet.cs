﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DigitalSignage.Scheduler
{
    public partial class SpreadSheet : UserControl
    {
        public SpreadSheet()
        {
            InitializeComponent();

            axSpreadsheet1.DisplayWorkbookTabs = false;
            axSpreadsheet1.DisplayToolbar = false;
        }

        public AxMicrosoft.Office.Interop.Owc11.AxSpreadsheet GetInstance
        {
            get
            {
                return this.axSpreadsheet1;
            }
        }

        public void ExportTo()
        {
            if(axSpreadsheet1 != null)
                axSpreadsheet1.Export();

            
        }
    }
}
