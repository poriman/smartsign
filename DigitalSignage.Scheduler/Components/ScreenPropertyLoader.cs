﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigitalSignage.Common;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace DigitalSignage.Scheduler
{
	public class ScreenPropertyLoader
	{
		public bool IsAd = false;
		public int PlayConstrait = (int)ShowFlag.Show_NonEvent;
		public String MetaTags = String.Empty;
		public DateTime StartDateTime = DateTime.MinValue;
		public DateTime EndDateTime = DateTime.MaxValue;
		public int ScreenWidth = 800;
		public int ScreenHeight = 600;
		public int ScreenPlaytime = 0;
		public long ScreenID = DateTime.UtcNow.Ticks;
		public String ScreenName = String.Empty;
        public String LocalThumbPath = String.Empty;

		public ScreenPropertyLoader()
		{
			IsAd = false;
			PlayConstrait = (int)ShowFlag.Show_NonEvent;
			MetaTags = String.Empty;
			StartDateTime = DateTime.MinValue;
			EndDateTime = DateTime.MaxValue;

            LocalThumbPath = String.Empty;
		}

		public ScreenPropertyLoader(string sBinFilePath)
		{
			try
			{
				FileInfo fi = new FileInfo(sBinFilePath);
				ScreenName = fi.Name;

				if (sBinFilePath.ToLower().EndsWith(".bin"))
				{
					using (FileStream fileStream = new FileStream(sBinFilePath, FileMode.Open, FileAccess.Read))
					{
						BinaryFormatter binaryFormatter = new BinaryFormatter();
						List<Dictionary<string, object>> propertiesSet = new List<Dictionary<string, object>>();

						#region 광고 여부 추출
						try
						{
							propertiesSet = binaryFormatter.Deserialize(fileStream) as List<Dictionary<string, object>>;
							IsAd = (bool)propertiesSet[0]["isAd"];
						}
						catch
						{
							IsAd = false;
						}
						#endregion

						#region 재생 조건 추출
						try
						{
							PlayConstrait = (int)propertiesSet[0]["showFlag"];

						}
						catch
						{
							PlayConstrait = (int)ShowFlag.Show_NonEvent;
						}
						#endregion

						#region 광고 시간 추출
						try
						{
							StartDateTime = (DateTime)propertiesSet[0]["AdStartTime"];
							EndDateTime = (DateTime)propertiesSet[0]["AdEndTime"];
						}
						catch
						{
							StartDateTime = DateTime.MinValue;
							EndDateTime = DateTime.MaxValue;
						}
						#endregion

						#region 광고 메타태그 추출
						try
						{
							MetaTags = (String)propertiesSet[0]["MetaTags"];
						}
						catch
						{
							MetaTags = String.Empty;
						}
						#endregion

						#region 스크린 ID 추출
						try
						{
							ScreenID = Convert.ToInt64(propertiesSet[0]["ScreenID"]);
						}
						catch
						{
						}
						#endregion

						#region 스크린 Width
						try
						{
							ScreenWidth = (int)propertiesSet[0]["Width"];
						}
						catch
						{
						}
						#endregion

						#region 스크린 Height
						try
						{
							ScreenHeight = (int)propertiesSet[0]["Height"];
						}
						catch
						{
						}
						#endregion

						#region 스크린 재생 시간
						try
						{
							ScreenPlaytime = (int)((TimeSpan)propertiesSet[0]["PlayTime"]).TotalSeconds;
						}
						catch
						{
						}
						#endregion

						fileStream.Close();
					}
				}
				else if (sBinFilePath.ToLower().EndsWith(".smil"))
				{
					#region SMIL
					Smil.Library.Smil smil = Smil.Library.Smil.LoadObjectFromSmilFile(sBinFilePath);

					#region 스크린 Width
					try
					{
						ScreenWidth = smil.Width();
					}
					catch
					{
					}
					#endregion

					#region 스크린 Height
					try
					{
						ScreenHeight = smil.Height();
					}
					catch
					{
					}
					#endregion

					foreach (Smil.Library.Meta meta in smil.head.arrMetaCollection)
					{
						if (meta.name.Equals("isAd"))
						{
							IsAd = Convert.ToBoolean(meta.content);
						}

						#region 재생 조건 추출
						else if (meta.name.Equals("showFlag"))
						{
							PlayConstrait = Convert.ToInt32(meta.content);
						}
						#endregion

						#region 광고 시간 추출
						else if (meta.name.Equals("AdStartTime"))
						{
							StartDateTime = Convert.ToDateTime(meta.content);
						}
						else if (meta.name.Equals("AdEndTime"))
						{
							EndDateTime = Convert.ToDateTime(meta.content);
						}
						#endregion

						#region 광고 메타태그 추출
						else if (meta.name.Equals("MetaTags"))
						{
							MetaTags = meta.content;
						}
						#endregion

						#region 스크린 ID
						else if (meta.name.Equals("ScreenID"))
						{
                            try
                            {
                                ScreenID = Convert.ToInt64(meta.content);
                            }
                            catch { ScreenID = -1; }
						}
						#endregion

						#region 재생 시간
						try
						{
							
							ScreenPlaytime = (int)(smil.TotalDuration().TotalSeconds);
						}
						catch
						{
						}
						#endregion

					}
					#endregion
				}
				else
					throw new Exception();

				#region 스크린 Thumbnail Path
				try
				{
					String sThumbFile = String.Format("{0}\\thumb.png", fi.DirectoryName);
					FileInfo fiThumb = new FileInfo(sThumbFile);
					if (fiThumb.Exists)
					{
						LocalThumbPath = sThumbFile;
					}
				}
				catch
				{
					LocalThumbPath = String.Empty;
				}
				#endregion
				
			}
			catch
			{
				IsAd = false;
				PlayConstrait = (int)ShowFlag.Show_NonEvent;
				MetaTags = String.Empty;
				StartDateTime = DateTime.MinValue;
				EndDateTime = DateTime.MaxValue;
			}
		}
	}
}
