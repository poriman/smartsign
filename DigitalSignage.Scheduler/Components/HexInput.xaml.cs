﻿using System;
using System.Collections;
using System.Windows.Interop;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Reflection;
using System.Text;

using NLog;

namespace DigitalSignage.Scheduler
{
    /// <summary>
    /// Interaction logic for GroupTree.xaml
    /// </summary>
    public partial class HexInput : Grid
    {
        private byte[] dataArray = null;
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public HexInput()
        {
            InitializeComponent();
        }

        public byte[] data
        {
            get
            {
                if (parseArrayToData())
                    return dataArray;
                else
                    return null;
            }
            set
            {
                dataArray = value;
                updateView();
            }
        }

        private void updateView()
        {
            if (dataArray == null) return;
            StringBuilder sb = new StringBuilder();
            foreach (byte hex in dataArray ) sb.Append(hex.ToString("x2")+" ");
            dataUI.Text = sb.ToString().ToLower();
        }

        private Boolean parseArrayToData()
        {
            ArrayList res = new ArrayList();
            int prev = -1, cur;
            foreach ( char val in dataUI.Text )
            {
                cur = -1;
                if ((val >= '0') && (val <= '9'))
                    cur = val-0x30;
                if ((val >= 'a') && (val <= 'f'))
                    cur = val-'a'+10;
                if ((val >= 'A') && (val <= 'F'))
                    cur = val - 'A' + 10;
                if (cur < 0) continue;
                if (prev == -1)
                    prev = cur;
                else
                {
                    res.Add((prev << 4) | cur);
                    prev = -1;
                }
            }
            dataArray = new byte[res.Count];
            for (int i = 0; i < res.Count; i++)
            {
                int val = (int)res[i];
                dataArray[i] = (byte)val;
            }
            return true;
        }
    }
}
