﻿namespace DigitalSignage.Scheduler
{
    partial class SpreadSheet
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SpreadSheet));
            this.axSpreadsheet1 = new AxMicrosoft.Office.Interop.Owc11.AxSpreadsheet();
            ((System.ComponentModel.ISupportInitialize)(this.axSpreadsheet1)).BeginInit();
            this.SuspendLayout();
            // 
            // axSpreadsheet1
            // 
            this.axSpreadsheet1.DataSource = null;
            this.axSpreadsheet1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.axSpreadsheet1.Enabled = true;
            this.axSpreadsheet1.Location = new System.Drawing.Point(0, 0);
            this.axSpreadsheet1.Name = "axSpreadsheet1";
            this.axSpreadsheet1.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axSpreadsheet1.OcxState")));
            this.axSpreadsheet1.Size = new System.Drawing.Size(640, 480);
            this.axSpreadsheet1.TabIndex = 0;
            // 
            // SpreadSheet
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.axSpreadsheet1);
            this.Name = "SpreadSheet";
            this.Size = new System.Drawing.Size(640, 480);
            ((System.ComponentModel.ISupportInitialize)(this.axSpreadsheet1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private AxMicrosoft.Office.Interop.Owc11.AxSpreadsheet axSpreadsheet1;
    }
}
