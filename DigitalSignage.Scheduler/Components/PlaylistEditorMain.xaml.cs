﻿using System;
using System.Xml;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Interop;
using System.Windows.Media.Animation;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

using NLog;

using DigitalSignage.Common;
using DigitalSignage.Controls;
using DigitalSignage.PlayControls;

namespace DigitalSignage.Scheduler
{
	/// <summary>
	/// Interaction logic for PlaylistEditorMain.xaml
	/// </summary>
	public partial class PlaylistEditorMain : Grid
	{
		class ItemData
		{
			public int ItemType; // 0 - screen, 1 - group
			public int Hours;
			public int Minutes;
			public int Seconds;
			public int RepeatCount;
			public string ProjectName;
			public XmlNode Node;
		}

//         ListViewDragDropManager<program> dragMgr;
		private static Logger logger = LogManager.GetCurrentClassLogger();
		private Config cfg = null;
		private XmlDocument document = new XmlDocument();
		private string fileName = "";
		private Window parentWindow = null;
        private program SelectScreen = null;
		public Config Cfg
		{
			set { cfg = value; }
		}

		public Window ParentWindow
		{
			set { parentWindow = value; }
		}

		public PlaylistEditorMain()
		{
			InitializeComponent();

//             this.dragMgr = new ListViewDragDropManager<program>(PlayListBox);
//             this.dragMgr.ListView = PlayListBox;
//             this.dragMgr.ShowDragAdorner = true;

			XmlNode main = document.CreateNode(XmlNodeType.XmlDeclaration, "", "");
			document.AppendChild(main);
			main = document.CreateNode(XmlNodeType.Element, "playlist", "");
			document.AppendChild(main);
		}

        #region Count routines        

        private TimeSpan getGroupDuration(XmlNode group)
        {
            if (group.Attributes["time"] != null)
                return TimeConverter.StrToTime(group.Attributes["time"].Value);
            if (group.LocalName.Equals("group"))
            {
                if (group.Attributes["repeat"] == null)
                    return new TimeSpan(0);
                int rpt = Int32.Parse(group.Attributes["repeat"].Value);
                TimeSpan sum= new TimeSpan(0);
                foreach (XmlNode child in group.ChildNodes)
                {
                    sum = sum.Add(getGroupDuration(child));
                }
                return new TimeSpan(sum.Ticks * rpt);
            }
            return new TimeSpan(0);
        }

        private void calculateGroupDuration( TreeViewItem group )
        {
            PlayListItem pi = group.Header as PlayListItem;
            ItemData data = group.DataContext as ItemData;
            if (data.RepeatCount < 1) return;
            if (data.ItemType != 1) return;
            if (pi == null) return;
            try
            {
                TimeSpan time = getGroupDuration(data.Node);
                data.Hours = time.Hours; data.Minutes = time.Minutes; data.Seconds = time.Seconds;
            }
            catch (Exception err2)
            {
				logger.Error(err2 + "");
			}
            pi.SetDuration(data.Hours, data.Minutes, data.Seconds);
        }
        #endregion

        private void deleteUI_Click(object sender, RoutedEventArgs e)
        {
            //delete current item
            try
            {
                deleteItem();
            }
            catch (Exception err)
            {
                logger.Error(err + " " + err.StackTrace);
            }
        }

        private void addUI_Click(object sender, RoutedEventArgs e)
        {
			try
			{
				addNewItem();
			}
			catch
			{
			}
        }

		private void dataUI_KeyUp(object sender, KeyEventArgs e)
		{
			if (e.Key == Key.Insert)
				addNewItem();
			if (e.Key == Key.Delete)
				deleteItem();
		}

        private void upItem_Click(object sender, RoutedEventArgs e)
        {
            if (PlayListBox.SelectedItem != null)
            {
                program screen = (program)PlayListBox.SelectedItem;
                ChangeItem(screen, true);
            }
        }

        private void downItem_Click(object sender, RoutedEventArgs e)
        {
            if (PlayListBox.SelectedItem != null)
            {
                program screen = (program)PlayListBox.SelectedItem;
                ChangeItem(screen, false);
            }
        }

        private void ChangeItem(program data, bool isUp)
        {
            try
            {
                PlayListArry task = PlayListBox.ItemsSource as PlayListArry;
                int index = 0;
                bool isSelect = false;
                foreach (program screen in task)
                {
                    if (screen == data)
                    {
                        isSelect = true;
                        break;
                    }
                    index++;
                }
                if (isSelect)
                {
                    if (isUp)
                    {
                        if (index == 0) return;
                        task.RemoveAt(index);
                        index = index - 1;
                        
                    }
                    else
                    {
                        if (index == task.Count - 1) return;
                        task.RemoveAt(index);
                        index = index + 1;
                    }
                    
                    task.Insert(index , data);
                    PlayListBox.SelectedIndex = index;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex + " " + ex.StackTrace);
            }
        }

        public void addNewItem()
        {
            XmlNodeList list = document.SelectNodes("//child::screen");
            if(list != null && list.Count > 30)
            {
                MessageBox.Show(Properties.Resources.mbErrorAddScreen, Properties.Resources.titleMessageBox, MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            program screen = new program();

            XmlNode newItem = document.CreateNode(XmlNodeType.Element, "screen", "");
            XmlAttribute atr = document.CreateAttribute("ID");
            screen.FilePath = atr.Value = "New screen";
            newItem.Attributes.Append(atr);
            atr = document.CreateAttribute("time");
            try
            {
                screen.Time = TimeConverter.StrToTime(atr.Value = "00:00:00");
            }
            catch (Exception err2)
            {
                screen.Time = new TimeSpan(0);
                logger.Error(err2 + "");
            }

            #region 광고 노드 추가
            try
            {
                XmlAttribute attr1 = document.CreateAttribute("IsAd");
                attr1.Value = "N";
                screen.IsAd = false;
                newItem.Attributes.Append(attr1);
            }
            catch { }
            try
            {
                XmlAttribute attr1 = document.CreateAttribute("PlayConstrait");
                attr1.Value = "0";
                screen.PlayConstraint = 0;
                newItem.Attributes.Append(attr1);
            }
            catch { }
            try
            {
                XmlAttribute attr1 = document.CreateAttribute("AdStartTime");
                attr1.Value = "0";
                screen.AdStartTime = DateTime.MinValue;
                newItem.Attributes.Append(attr1);
            }
            catch { }
            try
            {
                XmlAttribute attr1 = document.CreateAttribute("AdEndTime");
                attr1.Value = "0";
                screen.AdEndTime = DateTime.MaxValue;
                newItem.Attributes.Append(attr1);
            }
            catch { }
            try
            {
                XmlAttribute attr1 = document.CreateAttribute("MetaTags");
                screen.MetaTag = attr1.Value = "";
                newItem.Attributes.Append(attr1);
            }
            catch { }

            #endregion

            screen.ID = System.IO.Path.GetFileName(screen.FilePath);
            screen.ThumbNail = getProjectIcon(screen.FilePath);

            newItem.Attributes.Append(atr);
            atr = document.CreateAttribute("selected");
            atr.Value = "1";
            newItem.Attributes.Append(atr);
            document.DocumentElement.AppendChild(newItem);

            screen.NODE = newItem;

            try
            {
                PlayListArry arry = PlayListBox.ItemsSource as PlayListArry;

                if (arry == null)
                    arry = new PlayListArry();

                arry.Add(screen);

                this.PlayListBox.ItemsSource = arry;
            }
            catch (System.Exception /*e*/)
            {
            	
            }


//             this.PlayListBox.ItemsSource = null;
//             LoadChilds(document.DocumentElement);
        }

        protected void LoadChilds(XmlNode mainnode)
        {
            PlayListArry arry = new PlayListArry();

            foreach (XmlNode node in mainnode.ChildNodes)
            {
                program screen = new program();
                screen.NODE = node;

                if (node.LocalName.Equals("screen"))
                {
                    screen.FilePath = node.Attributes["ID"].Value;
                    try
                    {
                        screen.Time = TimeConverter.StrToTime(node.Attributes["time"].Value);
                    }
                    catch (Exception err2)
                    {
                        screen.Time = new TimeSpan(0);
						logger.Error(err2 + "");
                    }

                    #region 광고 관련
                    try
                    {
                        screen.IsAd = Convert.ToBoolean(node.Attributes["IsAd"].Value); 
                    }
                    catch { screen.IsAd = false; }

                    try
                    {
                        screen.PlayConstraint = Convert.ToInt32(node.Attributes["PlayConstrait"].Value);
                    }
                    catch { screen.PlayConstraint = 0; }

                    try
                    {
                        screen.AdStartTime = Convert.ToDateTime(node.Attributes["AdStartTime"].Value);
                    }
                    catch { screen.AdStartTime = DateTime.MinValue; }

                    try
                    {
                        screen.IsAd = node.Attributes["IsAd"].Value.Equals("Y");
                    }
                    catch { screen.IsAd = false; }

                    try
                    {
                        screen.AdEndTime = Convert.ToDateTime(node.Attributes["AdEndTime"].Value);
                    }
                    catch { screen.AdEndTime = DateTime.MaxValue; }

                    try
                    {
                        screen.MetaTag = node.Attributes["MetaTags"].Value;
                    }
                    catch { screen.MetaTag = String.Empty; }

                    #endregion

                }
                screen.ID = System.IO.Path.GetFileName(screen.FilePath);
                screen.ThumbNail = getProjectIcon(screen.FilePath);

                arry.Add(screen);
            }

            PlayListBox.ItemsSource = arry;
        }

        public void deleteItem()
        {
            if (SelectScreen != null)
            {
                try
                {
                    XmlNode mainnode = document.DocumentElement;
                    PlayListArry task = PlayListBox.ItemsSource as PlayListArry;
                    int index = 0; bool isSelect = false;
                    foreach (program screen in task)
                    {
                        if (screen == SelectScreen)
                        {
                            mainnode.RemoveChild(screen.NODE);
                            isSelect = true;
                            break;
                        }
                        index++;
                    }
                    if (isSelect)
                    {
                       task.RemoveAt(index);
                    }
                }
                catch (Exception ex)
                {
                    logger.Error(ex + " " + ex.StackTrace);
                }
            }
        }

        private void PlayListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                if (PlayListBox.SelectedItem == null)
                {
                    SelectScreen = null;
                    removeItem.IsEnabled = false;
                    return;
                }
                removeItem.IsEnabled = true;
                program screen = (program)PlayListBox.SelectedItem;
                SelectScreen = screen;
            }
            catch { }
        }

        private BitmapSource getProjectIcon(string fname)
        {
			try
			{
				String thumbname = System.IO.Path.GetDirectoryName(fname) + "\\thumb.png";
				if (System.IO.File.Exists(thumbname))
				{
					System.Drawing.Bitmap icon = new System.Drawing.Bitmap(thumbname);
					return Imaging.CreateBitmapSourceFromHBitmap(icon.GetHbitmap(), IntPtr.Zero,
						new Int32Rect(0, 0, icon.Width, icon.Height), BitmapSizeOptions.FromEmptyOptions());
				}
				else
				{
// 					thumbname = System.IO.Path.GetDirectoryName(fileName) + "\\" + System.IO.Path.GetDirectoryName(fname) + "\\thumb.png";
// 					if (System.IO.File.Exists(thumbname))
// 					{
// 						System.Drawing.Bitmap icon = new System.Drawing.Bitmap(thumbname);
// 						return Imaging.CreateBitmapSourceFromHBitmap(icon.GetHbitmap(), IntPtr.Zero,
// 							new Int32Rect(0, 0, icon.Width, icon.Height), BitmapSizeOptions.FromEmptyOptions());
// 					}
// 					else
// 						throw new Exception("");

                    System.Drawing.Bitmap icon = Properties.Resources.notfound;
                    return Imaging.CreateBitmapSourceFromHBitmap(icon.GetHbitmap(), IntPtr.Zero,
                        new Int32Rect(0, 0, icon.Width, icon.Height), BitmapSizeOptions.FromEmptyOptions());				

				}
			}
			catch
			{
				System.Drawing.Bitmap icon = Properties.Resources.notfound;
				return Imaging.CreateBitmapSourceFromHBitmap(icon.GetHbitmap(), IntPtr.Zero,
					new Int32Rect(0, 0, icon.Width, icon.Height), BitmapSizeOptions.FromEmptyOptions());				
			}
        }

        private void filePath_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                System.Windows.Forms.OpenFileDialog ofd = new System.Windows.Forms.OpenFileDialog();
				ofd.Filter = Properties.Resources.labelFilter;
                
				System.Windows.Forms.DialogResult res = ofd.ShowDialog();
                if (ofd.FileName.Equals("")) return;

                ContentControl targetContentCtrl = (ContentControl)e.OriginalSource;
                program targetitem = (program)targetContentCtrl.DataContext;
                program screen = new program();

                screen = targetitem;
                screen.ID = System.IO.Path.GetFileName(ofd.FileName);

                XmlNode node = screen.NODE;

                XmlAttribute atr = node.Attributes["ID"];
                if (atr != null)
                {
                    atr = document.CreateAttribute("ID");
                    node.Attributes.Append(atr);
                }
                atr.Value = ofd.FileName;

                try
                {
                    ScreenPropertyLoader loaded_Screen = new ScreenPropertyLoader(ofd.FileName);

                    screen.Time = TimeSpan.FromSeconds(loaded_Screen.ScreenPlaytime);
                    atr = node.Attributes["time"];
                    if (atr != null)
                    {
                        atr = document.CreateAttribute("time");
                        node.Attributes.Append(atr);
                    }
                    atr.Value = screen.strTime;

                    #region 광고 관련
                    atr = node.Attributes["IsAd"];
                    if (atr != null)
                    {
                        atr = document.CreateAttribute("IsAd");
                        node.Attributes.Append(atr);
                    }
                    atr.Value = loaded_Screen.IsAd.ToString();
                    screen.IsAd = loaded_Screen.IsAd;

                    atr = node.Attributes["PlayConstrait"];
                    if (atr != null)
                    {
                        atr = document.CreateAttribute("PlayConstrait");
                        node.Attributes.Append(atr);
                    }
                    atr.Value = loaded_Screen.PlayConstrait.ToString();
                    screen.PlayConstraint = loaded_Screen.PlayConstrait;

                    atr = node.Attributes["AdStartTime"];
                    if (atr != null)
                    {
                        atr = document.CreateAttribute("AdStartTime");
                        node.Attributes.Append(atr);
                    }
                    atr.Value = loaded_Screen.StartDateTime.ToString();
                    screen.AdStartTime = loaded_Screen.StartDateTime;

                    atr = node.Attributes["AdEndTime"];
                    if (atr != null)
                    {
                        atr = document.CreateAttribute("AdEndTime");
                        node.Attributes.Append(atr);
                    }
                    atr.Value = loaded_Screen.EndDateTime.ToString();
                    screen.AdEndTime = loaded_Screen.EndDateTime;

                    atr = node.Attributes["MetaTags"];
                    if (atr != null)
                    {
                        atr = document.CreateAttribute("MetaTags");
                        node.Attributes.Append(atr);
                    }
                    screen.MetaTag = atr.Value = loaded_Screen.MetaTags;

                    #endregion

                    e.Handled = true;

                }
                catch
                { }

                screen.FilePath = ofd.FileName;
                screen.ThumbNail = getProjectIcon(ofd.FileName);
                SetItems(targetitem, screen);
            }
            catch (Exception err)
            {
                logger.Error(err + " " + err.StackTrace);
            }
        }

        private bool SetItems(program oldData, program newData)
        {
            try
            {
                PlayListArry task = PlayListBox.ItemsSource as PlayListArry;
                int index = 0;
                bool isSelect = false;
                foreach (program screen in task)
                {
                    if (screen == oldData)
                    {
                        isSelect = true;
                        break;
                    }
                    index++;
                }
                if (isSelect)
                {
                    task.RemoveAt(index);
                    task.Insert(index, newData);
                    return true;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex + " " + ex.StackTrace);
            }
            return false;
        }

        private void _durationUI_SelectedTimeChanged(object sender, AC.AvalonControlsLibrary.Controls.TimeSelectedChangedRoutedEventArgs e)
        {
            try
            {
                AC.AvalonControlsLibrary.Controls.TimePicker ctrl = (AC.AvalonControlsLibrary.Controls.TimePicker)sender;
                program screen = (program)ctrl.DataContext;

                XmlAttribute atr = screen.NODE.Attributes["time"];
                if (atr == null)
                {
                    atr = document.CreateAttribute("time");
                    screen.NODE.Attributes.Append(atr);
                }
                atr.Value = TimeConverter.TimeToStr(ctrl.SelectedHour, ctrl.SelectedMinute, ctrl.SelectedSecond);
                screen.Time = ctrl.SelectedTime;
            }
            catch
            { }
        }

        #region 메뉴

        private void newUI_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                fileName = "";
                PlayListBox.ItemsSource = null;
                document = new XmlDocument();
                XmlNode main = document.CreateNode(XmlNodeType.XmlDeclaration, "", "");
                document.AppendChild(main);
                main = document.CreateNode(XmlNodeType.Element, "playlist", "");
                document.AppendChild(main);

				playBtnUI.IsEnabled = true;
				addItem.IsEnabled = true;
				saveUI.IsEnabled = true;
            }
            catch (Exception err)
            {
                logger.Error(err + " " + err.StackTrace);
            }

        }  

        private void loadUI_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                System.Windows.Forms.OpenFileDialog ofd = new System.Windows.Forms.OpenFileDialog();
                ofd.Filter = "Playlist files|*.ipl;*.xml";
                System.Windows.Forms.DialogResult res = ofd.ShowDialog();
                if (res == System.Windows.Forms.DialogResult.OK)
                {
					fileName = ofd.FileName;
                    LoadData(ofd.FileName);

					playBtnUI.IsEnabled = true;
					addItem.IsEnabled = true;
					saveUI.IsEnabled = true;
				}
            }
            catch (Exception err)
            {
                logger.Error(err + " " + err.StackTrace);
				fileName = "";
            }
        }

        private void saveUI_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                SaveData(true);
            }
            catch (Exception err)
            {
                logger.Error(err + " " + err.StackTrace);
            }
        }

        private void playBtnUI_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if(SaveData(true))
				{
					PlaylistPlayerPreview dlg = new PlaylistPlayerPreview();
					dlg.Title = Properties.Resources.titlePreview;
					dlg.Owner = parentWindow;
					dlg.Show();
					dlg.Play(fileName);
				}

            }
            catch (Exception err)
            {
                logger.Error(err + " " + err.StackTrace);
            }
        }

        private void LoadData(string filename)
        {
            document = new XmlDocument();
            document.Load(filename);
			#region 재생 목록 파일 검증
			if (null == document.SelectSingleNode("child::playlist") ||
					null == document.SelectSingleNode("descendant::screen"))
			{
				MessageBox.Show(Properties.Resources.mbErrorPlaylistFile, Properties.Resources.titleMessageBox, MessageBoxButton.OK, MessageBoxImage.Error);
				throw new Exception();
			}
			#endregion

            PlayListBox.ItemsSource = null;
            LoadChilds(document.DocumentElement);
        }

        private bool ValidCheckForSave()
        {
            try
            {
                PlayListArry task = PlayListBox.ItemsSource as PlayListArry;
                TimeSpan ts = new TimeSpan(0, 0, 0, 0);
                foreach (program screen in task)
                {
                    if (screen.Time == ts)
                        return false;
                }
            }
            catch (Exception ex)
            {
				logger.Error(ex + "");
			}
            return true;
        }

        private bool SaveNewDocument()
        {
            try
            {
                document = new XmlDocument();
                XmlNode main = document.CreateNode(XmlNodeType.XmlDeclaration, "", "");
                document.AppendChild(main);
                main = document.CreateNode(XmlNodeType.Element, "playlist", "");
                document.AppendChild(main);

                PlayListArry task = PlayListBox.ItemsSource as PlayListArry;

                bool bIsAd = false;
                int nPlayConstrait = 0;
                DateTime dtStartTime = DateTime.MinValue;
                DateTime dtEndTime = DateTime.MaxValue;
                String sMetaTags = String.Empty;

                foreach (program screen in task)
                {
                    XmlNode newItem = document.CreateNode(XmlNodeType.Element, "screen", "");
                    XmlAttribute atr = document.CreateAttribute("ID");
                    atr.Value = screen.FilePath;
                    newItem.Attributes.Append(atr);
                    atr = document.CreateAttribute("time");
                    atr.Value = screen.strTime;
                    newItem.Attributes.Append(atr);
                    atr = document.CreateAttribute("selected");
                    atr.Value = "1";
                    newItem.Attributes.Append(atr);

                    #region 광고 노드 추가
                    try
                    {
                        XmlAttribute attr1 = document.CreateAttribute("IsAd");
                        attr1.Value = screen.IsAd.ToString();
                        newItem.Attributes.Append(attr1);
                    }
                    catch { }
                    try
                    {
                        XmlAttribute attr1 = document.CreateAttribute("PlayConstrait");
                        attr1.Value = screen.PlayConstraint.ToString();
                        newItem.Attributes.Append(attr1);
                    }
                    catch { }
                    try
                    {
                        XmlAttribute attr1 = document.CreateAttribute("AdStartTime");
                        attr1.Value = screen.AdStartTime.ToString();
                        newItem.Attributes.Append(attr1);
                    }
                    catch { }
                    try
                    {
                        XmlAttribute attr1 = document.CreateAttribute("AdEndTime");
                        attr1.Value = screen.AdEndTime.ToString();
                        newItem.Attributes.Append(attr1);
                    }
                    catch { }
                    try
                    {
                        XmlAttribute attr1 = document.CreateAttribute("MetaTags");
                        attr1.Value = screen.MetaTag;
                        newItem.Attributes.Append(attr1);
                    }
                    catch { }

                    if (screen.IsAd)
                    {
                        if (bIsAd)
                        {
                            nPlayConstrait |= screen.PlayConstraint;
                            if (dtStartTime > screen.AdStartTime)
                                dtStartTime = screen.AdStartTime;
                            if (dtEndTime < screen.AdEndTime)
                                dtEndTime = screen.AdEndTime;

                            if (!String.IsNullOrEmpty(screen.MetaTag))
                            {
                                MetaTagHelper source = new MetaTagHelper(sMetaTags);
                                sMetaTags = source.Combine(new MetaTagHelper(screen.MetaTag));
                            }
                        }
                        else
                        {
                            //  처음 들어옴
                            bIsAd = true;
                            nPlayConstrait = screen.PlayConstraint;
                            dtStartTime = screen.AdStartTime;
                            dtEndTime = screen.AdEndTime;
                            sMetaTags = screen.MetaTag;
                        }
                    }

                    #endregion

                    document.DocumentElement.AppendChild(newItem);

                    XmlAttribute attrCount = document.CreateAttribute("Count");
                    attrCount.Value = task.Count.ToString();

                    XmlAttribute attr2 = document.CreateAttribute("IsAd");
                    attr2.Value = bIsAd.ToString();

                    XmlAttribute attr3 = document.CreateAttribute("PlayConstrait");
                    attr3.Value = nPlayConstrait.ToString();

                    XmlAttribute attr4 = document.CreateAttribute("AdStartTime");
                    attr4.Value = dtStartTime.ToString();

                    XmlAttribute attr5 = document.CreateAttribute("AdEndTime");
                    attr5.Value = dtEndTime.ToString();

                    XmlAttribute attr6 = document.CreateAttribute("MetaTags");
                    attr6.Value = sMetaTags.ToString();

                    main.Attributes.Append(attrCount);
                    main.Attributes.Append(attr2);
                    main.Attributes.Append(attr3);
                    main.Attributes.Append(attr4);
                    main.Attributes.Append(attr5);
                    main.Attributes.Append(attr6);

                }
                return true;
            }
            catch { }
            return false;
        }

        private bool SaveData(bool bResultMsg)
        {
			if (false == ValidCheckForSave())
			{
				if (bResultMsg) MessageBox.Show(Properties.Resources.mbErrorSaveFailed, Properties.Resources.titleMessageBox);
				return false;
			}

            if (false == SaveNewDocument() || false == SetTotalDuration())
            {
				if (bResultMsg) MessageBox.Show(Properties.Resources.mbErrorSaveDuration, Properties.Resources.titleMessageBox);
				return false;
            }

            if (fileName.Equals(""))
            {
                System.Windows.Forms.SaveFileDialog fd = new System.Windows.Forms.SaveFileDialog();
                fd.Filter = "Playlist files|*.ipl";
                if (fd.ShowDialog() == System.Windows.Forms.DialogResult.Cancel)
					return false;

                fileName = fd.FileName;
            }
            try
            {
                document.Save(fileName);
				LoadData(fileName);

            }
            catch (Exception ee)
            {
				if (bResultMsg) MessageBox.Show(String.Format(Properties.Resources.messageFailed, fileName), Properties.Resources.titleMessageBox);
                statusUI.Text = ee.Message;
                ((Storyboard)statusUI.Resources["Down"]).Begin(statusUI);
				return false;
            }

			if (bResultMsg) MessageBox.Show(String.Format(Properties.Resources.messageSaved, fileName), Properties.Resources.titleMessageBox);

			return true;
        }

        private bool SetTotalDuration()
        {
            bool bRet = true;
            try
            {
                TimeSpan ts = new TimeSpan(0, 0, 0, 0);
                XmlNode rootNode = null;
                foreach (XmlNode node in document.ChildNodes)
                {
                    if (true == node.LocalName.Contains("playlist"))
                    {
                        rootNode = node;
                    }
                }

                foreach (XmlNode node in rootNode)
                {
                    if (true == node.LocalName.Contains("screen"))
                    {
                        ts += getGroupDuration(node);
                    }
                }

                XmlAttribute atr = rootNode.Attributes["time"];

                if (atr == null)
                {
                    atr = document.CreateAttribute("time");
                    rootNode.Attributes.Append(atr);
                }
                if (ts.TotalSeconds == 0)
                    bRet = false;

                atr.Value = TimeConverter.TimeToStr(ts.Hours, ts.Minutes, ts.Seconds);
            }
            catch (Exception)
            {
                bRet = false;
            }
            return bRet;
        }

        #endregion 메뉴


    }
}
