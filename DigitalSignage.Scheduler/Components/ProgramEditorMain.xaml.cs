﻿using System;
using System.Xml;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Interop;
using System.Windows.Media.Animation;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Reflection;

using NLog;

using DigitalSignage.Controls;
using DigitalSignage.Common;
using DigitalSignage.PlayControls;

namespace DigitalSignage.Scheduler
{
	/// <summary>
	/// Interaction logic for ProgramEditorMain.xaml
	/// </summary>
	public partial class ProgramEditorMain : Grid
	{
		class ItemData
		{
			public int ItemType; // 0 - screen, 1 - group
			public int Hours;
			public int Minutes;
			public int Seconds;
			public int RepeatCount;
			public string ProjectName;
			public XmlNode Node;
		}
		private static Logger logger = LogManager.GetCurrentClassLogger();
		private Config cfg = null;
		private bool lockDuration = false;
		private XmlDocument document = new XmlDocument();
		private string fileName = "";
		private Window parentWindow = null;
		private TimeSpan totalDuration = new TimeSpan(0, 0, 0, 0);

		private Size szScreen = new Size(800, 600);
		public Config Cfg
		{
			set { cfg = value; }
		}
		public Window ParentWindow
		{
			set { parentWindow = value; }
		}
		public ProgramEditorMain()
		{
			InitializeComponent();

			TableUI.GridSplitterDragDelta += new TableDrawer.GridSplitterDragDeltaEvent(TableUI_GridSplitterDragDelta);

			XmlNode main = document.CreateNode(XmlNodeType.XmlDeclaration, "", "");
			document.AppendChild(main);
			main = document.CreateNode(XmlNodeType.Element, "program", "");
			XmlAttribute atr = document.CreateAttribute("ID");
			atr.Value = "NewProgram";
			main.Attributes.Append(atr);
			
			XmlAttribute atr2 = document.CreateAttribute("time");
			atr2.Value = TimeConverter.TimeToStr(totalDuration.Hours, totalDuration.Minutes, totalDuration.Seconds);
			main.Attributes.Append(atr2);


			document.AppendChild(main);
			main = document.CreateNode(XmlNodeType.Element, "pane", "");
			document.DocumentElement.AppendChild(main);
			TableUI.Document = document;
		}

		void TableUI_GridSplitterDragDelta(object sender, System.Windows.Controls.Primitives.DragDeltaEventArgs e)
		{
			if(TableUI.SelectedItems != null && TableUI.SelectedItems.Count > 0)
			{
				Border ui = TableUI.SelectedItems[0] as Border;
				if (ui == null)
					return;

				UpdateSize(ui);
			}
		}

		void UpdateSize(Border selected)
		{
			if (selected == null)
				return;

			Size szItem = TableUI.ActualSize(selected);

			String sText = String.Format("{0}X{1} / ({2}, {3})", szScreen.Width, szScreen.Height, Convert.ToInt32(szScreen.Width * szItem.Width / 100), Convert.ToInt32(szScreen.Height * szItem.Height / 100));
			labelSizeInfo.Text = sText;
		}

		#region Count routines
		private BitmapSource getProjectIcon(string fname, bool group)
		{
			if (group)
			{
				System.Drawing.Bitmap icon = Properties.Resources.ftpoff;
				return Imaging.CreateBitmapSourceFromHBitmap(icon.GetHbitmap(), IntPtr.Zero,
					new Int32Rect(0, 0, icon.Width, icon.Height), BitmapSizeOptions.FromEmptyOptions());
			}
			if (System.IO.File.Exists(fname))
			{
				System.Drawing.Bitmap icon = Properties.Resources.ftpon;
				return Imaging.CreateBitmapSourceFromHBitmap(icon.GetHbitmap(), IntPtr.Zero,
					new Int32Rect(0, 0, icon.Width, icon.Height), BitmapSizeOptions.FromEmptyOptions());
			}
			else
			{
				System.Drawing.Bitmap icon = Properties.Resources.notfound;
				return Imaging.CreateBitmapSourceFromHBitmap(icon.GetHbitmap(), IntPtr.Zero,
					new Int32Rect(0, 0, icon.Width, icon.Height), BitmapSizeOptions.FromEmptyOptions());
			}
		}

		private string TimeToStr(int hour, int minute, int seconds)
		{
			string res = "";
			if (hour < 10)
				res += "0" + hour;
			else
				res += hour;
			res += ":";
			if (minute < 10)
				res += "0" + minute;
			else
				res += minute;
			res += ":";
			if (seconds < 10)
				res += "0" + seconds;
			else
				res += seconds;
			return res;
		}

		private TimeSpan StrToTime(string time)
		{
			string[] data = time.Split(':');
			if (data.Length != 3) throw new Exception("Wrong time format(1)");
			int hour = 0, min = 0, sec = 0;
			hour = Int32.Parse(data[0]);
			min = Int32.Parse(data[1]);
			sec = Int32.Parse(data[2]);
			return new TimeSpan(hour, min, sec); ;
		}

		private TimeSpan getGroupDuration(XmlNode group)
		{
			if (group.Attributes["time"] != null)
				return StrToTime(group.Attributes["time"].Value);
			if (group.LocalName.Equals("group"))
			{
				if (group.Attributes["repeat"] == null)
					return new TimeSpan(0);
				int rpt = Int32.Parse(group.Attributes["repeat"].Value);
				TimeSpan sum = new TimeSpan(0);
				foreach (XmlNode child in group.ChildNodes)
				{
					sum = sum.Add(getGroupDuration(child));
				}
				return new TimeSpan(sum.Ticks * rpt);
			}
			return new TimeSpan(0);
		}

		private void calculateGroupDuration(TreeViewItem group)
		{
			PlayListItem pi = group.Header as PlayListItem;
			ItemData data = group.DataContext as ItemData;
			if (data.RepeatCount < 1) return;
			if (data.ItemType != 1) return;
			if (pi == null) return;
			try
			{
				TimeSpan time = getGroupDuration(data.Node);
				data.Hours = time.Hours; data.Minutes = time.Minutes; data.Seconds = time.Seconds;
			}
			catch (Exception err2)
			{
				logger.Error(err2 + "");
			}
			pi.SetDuration(data.Hours, data.Minutes, data.Seconds);
		}
		#endregion

		private void newUI_Click(object sender, RoutedEventArgs e)
		{
			fileName = "";
			document = new XmlDocument();
			
			NewTemplateSelector dlg = new NewTemplateSelector();
			bool? bReturn = dlg.ShowDialog();

			if (bReturn.HasValue == false || bReturn.Value == false)
				return;

			//	비교할 페이지 사이즈를 저장한다.
			szScreen = dlg.ScreenSize;

			//	선택된 Template Index를 가져온다.
			NewTemplateSelector.Template nSelectedTemplate = dlg.SelectedIndex;

			XmlNode main = document.CreateNode(XmlNodeType.XmlDeclaration, "", "");
			document.AppendChild(main);
			main = document.CreateNode(XmlNodeType.Element, "program", "");
			XmlAttribute atr = document.CreateAttribute("ID");
			atr.Value = "NewProgram";
			main.Attributes.Append(atr);

			XmlAttribute atr2 = document.CreateAttribute("time");
			atr2.Value = TimeConverter.TimeToStr(totalDuration.Hours, totalDuration.Minutes, totalDuration.Seconds);
			main.Attributes.Append(atr2);


			document.AppendChild(main);
			main = document.CreateNode(XmlNodeType.Element, "pane", "");
			document.DocumentElement.AppendChild(main);
			TableUI.Document = document;

			TableUI.IsAssociated = true;
			TableUI.ScaleRate = szScreen.Width / szScreen.Height;
			//	Template을 생성한다.
			MakeTemplate(nSelectedTemplate);

			playUI.IsEnabled = true;
			saveUI.IsEnabled = true;
			TableUI.IsEnabled = true;
		}

		private void MakeTemplate(NewTemplateSelector.Template nSelected)
		{
			try
			{
				switch(nSelected)
				{
					case NewTemplateSelector.Template.Normal:	//	기본 템플릿
						break;
					case NewTemplateSelector.Template.Vertical_1on1:	//	세로 1:1
						MakePanes(GetPane(GetMainNode(), 0), 2, 1);
						break;
					case NewTemplateSelector.Template.Horizontal_1on1:	//	가로 1:1
						MakePanes(GetPane(GetMainNode(), 0), 1, 2);
						break;
					case NewTemplateSelector.Template.Horizontal_1on2:	//	가로 1:2
						{
							XmlNode node = MakePanes(GetPane(GetMainNode(), 0), 1, 2);
							MakePanes(GetPane(node, 1), 2, 1);
						}
						break;
					case NewTemplateSelector.Template.Horizontal_2on1:	//	가로 2:1
						{
							XmlNode node = MakePanes(GetPane(GetMainNode(), 0), 1, 2);
							MakePanes(GetPane(node, 0), 2, 1);				
						}
						break;
					case NewTemplateSelector.Template.Vertical_1on2:	//	세로 1:2
						{
							XmlNode node = MakePanes(GetPane(GetMainNode(), 0), 2, 1);
							MakePanes(GetPane(node, 1), 1, 2);
						}
						break;
					case NewTemplateSelector.Template.Vertical_2on1:	//	세로 2:1
						{
							XmlNode node = MakePanes(GetPane(GetMainNode(), 0), 2, 1);
							MakePanes(GetPane(node, 0), 1, 2);
						}
						break;
					case NewTemplateSelector.Template.Horizontal_2on2:	//	2:2
						MakePanes(GetPane(GetMainNode(), 0), 2, 2);
						break;
					case NewTemplateSelector.Template.Vertical_1on1on1:	//	세로 1:1:1
						MakePanes(GetPane(GetMainNode(), 0), 3, 1);
						break;
					case NewTemplateSelector.Template.Horizontal_1on1on1:	//	가로 1:1:1
						MakePanes(GetPane(GetMainNode(), 0), 1, 3);
						break;
				}
			}
			catch (Exception e)
			{
				MessageBox.Show(e.Message);
			}
		}

		private void loadUI_Click(object sender, RoutedEventArgs e)
		{
			// loading playlist from XML file
			try
			{
				System.Windows.Forms.OpenFileDialog ofd = new System.Windows.Forms.OpenFileDialog();
				ofd.Filter = "Program files|*.ipr;*.xml";
				System.Windows.Forms.DialogResult res = ofd.ShowDialog();
				if (res == System.Windows.Forms.DialogResult.OK)
				{
					LoadData(ofd.FileName);
					fileName = ofd.FileName;

					playUI.IsEnabled = true;
					saveUI.IsEnabled = true;
					TableUI.IsEnabled = true;
				}
			}
			catch (Exception err)
			{
				logger.Error(err + " " + err.StackTrace);
			}
		}

		private void LoadData(string filename)
		{
			fileName = "";

			document = new XmlDocument();
			document.Load(filename);
			#region 프로그램 파일 검증
			if (null == document.SelectSingleNode("child::program") ||
				null == document.SelectSingleNode("descendant::pane") || null == document.SelectSingleNode("descendant::pane").Attributes["playlist"])
			{
				MessageBox.Show(Properties.Resources.mbErrorProgramFile, Properties.Resources.titleMessageBox, MessageBoxButton.OK, MessageBoxImage.Error);
				throw new Exception();
			}
			#endregion
			//dataUI.Items.Clear();
			//LoadChilds(document.DocumentElement, dataUI.Items, 0 );
			TableUI.Document = document;
			saveUI.IsEnabled = true;
// 			this.Title = "Program editor - " + document.DocumentElement.Attributes["ID"].Value;
		}

		private bool SaveData()
		{
			try
			{
				if (fileName.Equals(""))
				{
					System.Windows.Forms.SaveFileDialog fd = new System.Windows.Forms.SaveFileDialog();
					fd.Filter = "Program files|*.ipr";
					if (fd.ShowDialog() == System.Windows.Forms.DialogResult.Cancel)
						return false;
					fileName = fd.FileName;
				}
				TableUI.SaveSizes();

				//	시간 추출
				UpdateProgramDuration();

                // 광고 태그 관련
                UpdateProgramAdTags();


                document.Save(fileName);

				MessageBox.Show(String.Format(Properties.Resources.messageSaved, fileName), Properties.Resources.titleMessageBox);
			}
			catch
			{
				return false;
			}
			return true;
		}

        void UpdateProgramAdTags()
        {
            XmlNode p_node = document.SelectSingleNode("child::program");
            XmlNodeList panes = document.SelectNodes("descendant::pane");
            if (p_node != null && panes != null)
            {
                int nTotalScreenCount = 0;
                bool bIsAd = false;
                int nPlayConstrait = 0;
                DateTime dtStartTime = DateTime.MinValue;
                DateTime dtEndTime = DateTime.MaxValue;
                String sMetaTags = String.Empty;

                foreach (XmlNode node in panes)
                {
                    #region 광고 관련
                    try
                    {
                        nTotalScreenCount += Convert.ToInt32(node.Attributes["ScreenCount"].Value);
                    }
                    catch { }

                    try
                    {
                        if (Convert.ToBoolean(node.Attributes["IsAd"].Value))
                        {
                            int currPlayConstrait = Convert.ToInt32(node.Attributes["PlayConstrait"].Value);
                            DateTime currAdStartTime = Convert.ToDateTime(node.Attributes["AdStartTime"].Value);
                            DateTime currAdEndTime = Convert.ToDateTime(node.Attributes["AdEndTime"].Value);
                            String currMetaTags = node.Attributes["MetaTags"].Value;
                            if (bIsAd)
                            {
                                nPlayConstrait |= currPlayConstrait;
                                if (dtStartTime > currAdStartTime)
                                    dtStartTime = currAdStartTime;
                                if (dtEndTime < currAdEndTime)
                                    dtEndTime = currAdEndTime;

                                if (!String.IsNullOrEmpty(currMetaTags))
                                {
                                    MetaTagHelper source = new MetaTagHelper(sMetaTags);
                                    sMetaTags = source.Combine(new MetaTagHelper(currMetaTags));
                                }
                            }
                            else
                            {
                                bIsAd = true;
                                nPlayConstrait = currPlayConstrait;
                                dtStartTime = currAdStartTime;
                                dtEndTime = currAdEndTime;
                                sMetaTags = currMetaTags;
                            }
                        }
                    }
                    catch { }
                    #endregion
                }
 
                XmlAttribute atr = p_node.Attributes["IsAd"];
                if (atr != null)
                {
                    atr = document.CreateAttribute("IsAd");
                    p_node.Attributes.Append(atr);
                }
                atr.Value = bIsAd.ToString();
 
                atr = p_node.Attributes["PlayConstrait"];
                if (atr != null)
                {
                    atr = document.CreateAttribute("PlayConstrait");
                    p_node.Attributes.Append(atr);
                }
                atr.Value = nPlayConstrait.ToString();

                atr = p_node.Attributes["AdStartTime"];
                if (atr != null)
                {
                    atr = document.CreateAttribute("AdStartTime");
                    p_node.Attributes.Append(atr);
                }
                atr.Value = dtStartTime.ToString();

                atr = p_node.Attributes["AdEndTime"];
                if (atr != null)
                {
                    atr = document.CreateAttribute("AdEndTime");
                    p_node.Attributes.Append(atr);
                }
                atr.Value = dtEndTime.ToString();

                atr = p_node.Attributes["MetaTags"];
                if (atr != null)
                {
                    atr = document.CreateAttribute("MetaTags");
                    p_node.Attributes.Append(atr);
                }
                atr.Value = sMetaTags;

                atr = p_node.Attributes["TotalScreenCount"];
                if (atr != null)
                {
                    atr = document.CreateAttribute("TotalScreenCount");
                    p_node.Attributes.Append(atr);
                }
                atr.Value = nTotalScreenCount.ToString();

                atr = p_node.Attributes["PaneCount"];
                if (atr != null)
                {
                    atr = document.CreateAttribute("PaneCount");
                    p_node.Attributes.Append(atr);
                }
                atr.Value = panes.Count.ToString();
            }
        }

		private void saveUI_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				SaveData();
			}
			catch (Exception err)
			{
				logger.Error(err + " " + err.StackTrace);
			}
		}

		private void mergeUI_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				mergeGroup();
			}
			catch (Exception err)
			{
				logger.Error(err + " " + err.StackTrace);
			}
		}

		private void durationUI_SelectedTimeChanged(object sender, AC.AvalonControlsLibrary.Controls.TimeSelectedChangedRoutedEventArgs e)
		{
			try
			{
				if (lockDuration) return;
				SetDuration(durationUI.SelectedHour, durationUI.SelectedMinute, durationUI.SelectedSecond);
			}
			catch (Exception err)
			{
				logger.Error(err + " " + err.StackTrace);
			}
		}

		private void splitUI_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				//split currently selected pane
				splitPane();
			}
			catch (Exception err)
			{
				logger.Error(err + " " + err.StackTrace);
			}
		}
		private void loadPlaylist()
		{
			try
			{
                Border item = TableUI.SelectedItems[0] as Border;
                if (item == null) return;

                System.Windows.Forms.OpenFileDialog ofd = new System.Windows.Forms.OpenFileDialog();
                ofd.Filter = "Supported files(.ipl, .bin, .xml)|*.ipl;*.xml;*.bin";
                System.Windows.Forms.DialogResult res = ofd.ShowDialog();
                if (ofd.FileName.Equals("")) return;

                String playlistName = ofd.FileName;
                //	hsshin 확장자가 BIN인경우는 중간 ipr과 ipl을 만들어줘야한다.
                if (playlistName.ToLower().EndsWith(".bin"))
                {
                    String sExecutingPath = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

                    TimeSpan ts = GetDurationFromBin(playlistName);

                    if(ts.TotalSeconds == 0)
                    {
                        MessageBox.Show(Properties.Resources.mbErrorSaveDuration, Properties.Resources.titleMessageBox, MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }

                    if (!MakeIPLFile(sExecutingPath, ref playlistName, ts))
                    {
                        MessageBox.Show(Properties.Resources.mbErrorPlaylistFile, Properties.Resources.titleMessageBox, MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }

                }

                #region 재생 목록 파일 검증
                XmlDocument testDocument = new XmlDocument();
                testDocument.Load(playlistName);
                XmlNode playlist = null;
                if (null == (playlist = testDocument.SelectSingleNode("child::playlist")) ||
                    null == testDocument.SelectSingleNode("descendant::screen"))
                {
                    MessageBox.Show(Properties.Resources.mbErrorPlaylistFile, Properties.Resources.titleMessageBox, MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                #endregion

                fileUI.Text = playlistName;

                XmlNode node = item.DataContext as XmlNode;

                XmlAttribute atr = node.Attributes["playlist"];
                if (atr == null)
                {
                    atr = document.CreateAttribute("playlist");
                    node.Attributes.Append(atr);
                }
                atr.Value = playlistName;

                #region 광고 관련
                atr = node.Attributes["ScreenCount"];
                if (atr == null)
                {
                    atr = document.CreateAttribute("ScreenCount");
                    node.Attributes.Append(atr);
                }

                try
                {
                    atr.Value = playlist.Attributes["Count"].Value;
                }
                catch { atr.Value = "1"; }

                atr = node.Attributes["IsAd"];
                if (atr == null)
                {
                    atr = document.CreateAttribute("IsAd");
                    node.Attributes.Append(atr);
                }

                try
                {
                    atr.Value = playlist.Attributes["IsAd"].Value;
                }
                catch { atr.Value = false.ToString(); }

                atr = node.Attributes["PlayConstrait"];
                if (atr == null)
                {
                    atr = document.CreateAttribute("PlayConstrait");
                    node.Attributes.Append(atr);
                }

                try
                {
                    atr.Value = playlist.Attributes["PlayConstrait"].Value;
                }
                catch { atr.Value = Convert.ToString((int)DigitalSignage.Common.ShowFlag.Show_NonEvent); }

                atr = node.Attributes["AdStartTime"];
                if (atr == null)
                {
                    atr = document.CreateAttribute("AdStartTime");
                    node.Attributes.Append(atr);
                }

                try
                {
                    atr.Value = playlist.Attributes["AdStartTime"].Value;
                }
                catch { atr.Value = DateTime.MinValue.ToString(); }

                atr = node.Attributes["AdEndTime"];
                if (atr == null)
                {
                    atr = document.CreateAttribute("AdEndTime");
                    node.Attributes.Append(atr);
                }

                try
                {
                    atr.Value = playlist.Attributes["AdEndTime"].Value;
                }
                catch { atr.Value = DateTime.MaxValue.ToString(); }

                atr = node.Attributes["MetaTags"];
                if (atr == null)
                {
                    atr = document.CreateAttribute("MetaTags");
                    node.Attributes.Append(atr);
                }

                try
                {
                    atr.Value = playlist.Attributes["MetaTags"].Value;
                }
                catch { atr.Value = String.Empty; }

                #endregion

                TableUI.SaveSizes();
                TableUI.Document = document;
			}
			catch (Exception err)
			{
				logger.Error(err + " " + err.StackTrace);
			}
		}

        private bool MakeIPLFile(string sExecutingPath, ref string programName, TimeSpan ts)
        {
            try
            {

                //	PlayList파일을 만들어준다
                String sIPLFormat = "<?xml version=\"1.0\"?><playlist time=\"{0}\"><screen time=\"{0}\" selected=\"1\" ID=\"{1}\"/></playlist>";
                String sIPL = String.Format(sIPLFormat, TimeConverter.TimeToStr(ts.Hours, ts.Minutes, ts.Seconds), programName);
                FileInfo fi = new FileInfo(programName);
                {
                    string filepath = sExecutingPath + String.Format("\\SchedulerData\\temp\\{0}_{1}.ipl", fi.Name, DateTime.Now.Ticks);
                   
                    if (!Directory.Exists(sExecutingPath + "\\SchedulerData\\temp\\"))
                        Directory.CreateDirectory(sExecutingPath + "\\SchedulerData\\temp\\");

                    System.IO.StreamWriter fsPL = new System.IO.StreamWriter(filepath, false);

                    if (fsPL != null)
                    {
                        fsPL.Write(sIPL);
                        fsPL.Flush();
                        fsPL.Close();
                        fsPL.Dispose();
                    }
                    programName = filepath;
                    return true;
                }

            }
            catch (Exception e)
            {
                MessageBox.Show(Properties.Resources.errorMakeIPLFile + "\n" + e.Message);
                return false;
            }

            //return false;
        }

        private TimeSpan GetDurationFromBin(string programName)
        {
            //	Dration 추출
            TimeSpan ts;
            FileStream fileStream = new FileStream(programName, FileMode.Open, FileAccess.Read);
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            List<Dictionary<string, object>> propertiesSet = new List<Dictionary<string, object>>();

            try
            {
                propertiesSet = binaryFormatter.Deserialize(fileStream) as List<Dictionary<string, object>>;
                ts = (TimeSpan)propertiesSet[0]["PlayTime"];
                if (ts.TotalSeconds == 0)
                    throw new Exception("a day looping");
            }
            catch
            {
                ts = TimeSpan.Zero;
            }
            fileStream.Close();

            return ts;
        }

		private void fileSelectUI_Click(object sender, RoutedEventArgs e)
		{
			loadPlaylist();
		}
		private TimeSpan LargestDuration(XmlNode node)
		{
			TimeSpan ts = new TimeSpan(0, 0, 0, 0);
			foreach(XmlNode child in node.ChildNodes)
			{
				if(child.LocalName.Contains("pane"))
				{
					try
					{
						XmlDocument doc = new XmlDocument();
						doc.Load(Convert.ToString(child.Attributes["playlist"].Value));
		
						foreach(XmlNode playlist in doc.ChildNodes)
						{
							if(playlist.LocalName.Contains("playlist"))
							{
								TimeSpan tsPlaylist = TimeConverter.StrToTime(playlist.Attributes["time"].Value);
								if(ts.TotalSeconds < tsPlaylist.TotalSeconds)
								{
									ts = tsPlaylist;
									break;
								}
							}
						}
					}
					catch(Exception)
					{

					}
				}
				else if(child.LocalName.Contains("grid"))
				{
					TimeSpan returnTS = LargestDuration(child);
					if(returnTS.TotalSeconds > ts.TotalSeconds)
					{
						ts = returnTS;
					}
				}
			}

			return ts;

		}
		private void UpdateProgramDuration()
		{
			TimeSpan ts = new TimeSpan(0, 0, 0, 0);
			XmlNode programNode = null;
			try
			{
				foreach (XmlNode child in document.ChildNodes)
				{
					try
					{
						if (child.LocalName.Contains("program"))
						{
							programNode = child;

							ts = LargestDuration(programNode);							
							
							break;
						}
					}
					catch (Exception)
					{
						ts = new TimeSpan(0, 0, 0, 0);
					}
				}
			}
			catch (Exception)
			{
				MessageBox.Show("Ex2");
			}
			finally
			{
				if(programNode != null)
				{
					XmlAttribute atr = null;
					try
					{
						programNode.Attributes["time"].Value = TimeConverter.TimeToStr(ts.Hours, ts.Minutes, ts.Seconds);
					}
					catch(Exception)
					{
						atr = document.CreateAttribute("time");
						atr.Value = TimeConverter.TimeToStr(ts.Hours, ts.Minutes, ts.Seconds);
						programNode.Attributes.Append(atr);
					}
				}
			}


		}

		private void splitPane()
		{
			Border item = TableUI.SelectedItems[0] as Border;
			if (item == null) return;
			XmlNode node = item.DataContext as XmlNode;
			XmlNode parent = node.ParentNode;
			XmlNode newNode = document.CreateElement("grid");
			XmlAttribute atr = document.CreateAttribute("rows");
			logger.Info("1");
			atr.Value = "2";
			newNode.Attributes.Append(atr);
			atr = document.CreateAttribute("columns");
			atr.Value = "2";
			newNode.Attributes.Append(atr);
			logger.Info("2");
			// inserting grid
			// and removing old pane
			parent.ReplaceChild(newNode, node);
			node = newNode;
			logger.Info("3");
			for (int i = 0; i < 4; i++)
			{
				newNode = document.CreateElement("pane");
				node.AppendChild(newNode);
			}
			logger.Info("4");
			TableUI.Document = document;
		}

		private void mergeGroup()
		{
			Grid item = TableUI.SelectedItems[0] as Grid;
			if (item == null) return;
			if (MessageBox.Show(Properties.Resources.mbMergePanes, Properties.Resources.titleMessageBox, MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
				return;
			XmlNode node = item.DataContext as XmlNode;
			XmlNode parent = node.ParentNode;
			XmlNode newNode = document.CreateElement("pane");
			// inserting pane
			// and removing old grid
			parent.ReplaceChild(newNode, node);
			// update image
			TableUI.Document = document;
		}

		private void SetDuration(int hours, int min, int sec)
		{
			Border item = TableUI.SelectedItems[0] as Border;
			if (item == null) return;
			XmlNode node = item.DataContext as XmlNode;
			XmlAttribute atr = node.Attributes["duration"];
			if (atr == null)
			{
				atr = document.CreateAttribute("duration");
				node.Attributes.Append(atr);
			}
			atr.Value = TimeConverter.TimeToStr(hours, min, sec);
		}

		private void TableUI_SelectedItemChanged(object sender, System.Collections.ArrayList selectedItems)
		{
			try
			{
				if (selectedItems.Count < 1)
				{
					splitUI.Visibility = Visibility.Collapsed;
					mergeUI.Visibility = Visibility.Collapsed;
					sepSplit.Visibility = Visibility.Collapsed;
					splitUI.IsEnabled = false;
					mergeUI.IsEnabled = false;

					colsUI.Visibility = Visibility.Hidden;
					colsLUI.Visibility = Visibility.Hidden;
					rowsLUI.Visibility = Visibility.Hidden;
					rowsUI.Visibility = Visibility.Hidden;
					durationLUI.Visibility = Visibility.Hidden;
					labelDurationinfo.Visibility = Visibility.Hidden;
					labelSizeHeader.Visibility = Visibility.Hidden;
					labelSizeInfo.Visibility = Visibility.Hidden;

					durationUI.Visibility = Visibility.Hidden;
					fileLUI.Visibility = Visibility.Hidden;
					fileUI.Visibility = Visibility.Hidden;
				}
				else
				{
					if (selectedItems[0].GetType().Equals(typeof(Border)))
					{
						// selected pane item
						fileSUI.IsEnabled = true;
						fileSUI.Visibility = Visibility.Visible;

						splitUI.Visibility = Visibility.Visible;
						mergeUI.Visibility = Visibility.Collapsed;
						sepSplit.Visibility = Visibility.Visible;

						splitUI.IsEnabled = true;
						mergeUI.IsEnabled = false;

						durationLUI.Visibility = Visibility.Visible;
						labelDurationinfo.Visibility = Visibility.Visible;
// 						durationUI.Visibility = Visibility.Visible;
						fileLUI.Visibility = Visibility.Visible;
						fileUI.Visibility = Visibility.Visible;
						labelSizeHeader.Visibility = Visibility.Visible;
						labelSizeInfo.Visibility = Visibility.Visible;

						colsUI.Visibility = Visibility.Hidden;
						colsLUI.Visibility = Visibility.Hidden;
						rowsLUI.Visibility = Visibility.Hidden;
						rowsUI.Visibility = Visibility.Hidden;

						//
						Border item = TableUI.SelectedItems[0] as Border;
						if (item == null) return;

						//	Item Size Update
						UpdateSize(item);

						XmlNode node = item.DataContext as XmlNode;

						if (node.Attributes["duration"] != null)
						{
							lockDuration = true;
							TimeSpan duration = TimeConverter.StrToTime(node.Attributes["duration"].Value);
							durationUI.SelectedHour = duration.Hours;
							durationUI.SelectedMinute = duration.Minutes;
							durationUI.SelectedSecond = duration.Seconds;
							lockDuration = false;
						}
						else
						{
							lockDuration = true;
							durationUI.SelectedHour = 0;
							durationUI.SelectedMinute = 0;
							durationUI.SelectedSecond = 0;
							lockDuration = false;
						}
						if (node.Attributes["playlist"] != null)
						{
							fileUI.Text = node.Attributes["playlist"].Value;
							
							try
							{
								XmlDocument doc = new XmlDocument();
								doc.Load(fileUI.Text);
								foreach(XmlNode playlist in doc.ChildNodes)
								{
									if(playlist.LocalName.Contains("playlist"))
									{
										labelDurationinfo.Text = playlist.Attributes["time"].Value;
										break;
									}
								}
							}
							catch(Exception)
							{
								labelDurationinfo.Text = "00:00:00";
							}
							
						}
						else
							fileUI.Text = "";

					}
					else
					{
						// selected group item
						splitUI.Visibility = Visibility.Collapsed;
						mergeUI.Visibility = Visibility.Visible;
						sepSplit.Visibility = Visibility.Visible;

						splitUI.IsEnabled = false;
						mergeUI.IsEnabled = true;

						durationLUI.Visibility = Visibility.Hidden;
						durationUI.Visibility = Visibility.Hidden;
						fileLUI.Visibility = Visibility.Hidden;
						fileSUI.Visibility = Visibility.Hidden;
						fileUI.Visibility = Visibility.Hidden;

						colsUI.Visibility = Visibility.Visible;
						colsUI.Text = ((XmlNode)((Grid)TableUI.SelectedItems[0]).DataContext).Attributes["columns"].Value;
						colsLUI.Visibility = Visibility.Visible;
						rowsLUI.Visibility = Visibility.Visible;
						rowsUI.Visibility = Visibility.Visible;
						rowsUI.Text = ((XmlNode)((Grid)TableUI.SelectedItems[0]).DataContext).Attributes["rows"].Value;
					}
				}
			}
			catch (Exception err)
			{
				logger.Error(err + " " + err.StackTrace);
			}
		}

		private XmlNode GetMainNode()
		{
			try
			{
				foreach (XmlNode node in document.ChildNodes)
				{
					if (node.LocalName.Contains("program"))
						return node;
				}
				return null;
			}
			catch
			{
				return null;
			}
		}
		private XmlNode GetPane(XmlNode node, int index)
		{
			try
			{
				return node.ChildNodes[index];
			}
			catch
			{
				return null;
			}
			
		}
		private XmlNode MakeGrid(XmlNode node)
		{
			try
			{
				if (node != null)
				{
					XmlNode newGrid = document.CreateElement("grid");

					XmlAttribute attRows = document.CreateAttribute("rows");
					attRows.Value = "1";
					XmlAttribute attCols = document.CreateAttribute("columns");
					attCols.Value = "1";

					newGrid.Attributes.Append(attRows);
					newGrid.Attributes.Append(attCols);

					node.AppendChild(newGrid);

					return newGrid;
				}

				return null;
			}
			catch
			{
				return null;
			}
		}

		private XmlNode MakePanes(XmlNode node, int rows, int cols)
		{
			try
			{
				if (node != null)
				{
					XmlNode parentNode = node.ParentNode;

					XmlNode newGrid = document.CreateElement("grid");

					XmlAttribute attRows = document.CreateAttribute("rows");
					attRows.Value = rows + "";
					XmlAttribute attCols = document.CreateAttribute("columns");
					attCols.Value = cols + "";

					newGrid.Attributes.Append(attRows);
					newGrid.Attributes.Append(attCols);

					for (int i = 0; i < rows * cols; i++)
					{
						XmlNode newNode = document.CreateElement("pane");
						newGrid.AppendChild(newNode);
					}

					parentNode.ReplaceChild(newGrid, node);

					TableUI.Document = document;
					
					return newGrid;
				}
				return null;
			}
			catch(Exception e)
			{
				MessageBox.Show(e.Message);
				return null;
			}
		}

		private void colsUI_KeyUp(object sender, KeyEventArgs e)
		{
			try
			{
				if (e.Key != Key.Enter) return;
				int newCol = Int32.Parse(colsUI.Text);
				if ((newCol > 0) && (newCol < 4))
				{
					Grid item = TableUI.SelectedItems[0] as Grid;
					if (item == null) return;
					XmlNode node = item.DataContext as XmlNode;
					int rows = Int32.Parse(node.Attributes["rows"].Value);
					int cols = Int32.Parse(node.Attributes["columns"].Value);

					int paneCnt = newCol * rows;
					if (paneCnt > node.ChildNodes.Count)
					{
						//appending panes
						for (int i = node.ChildNodes.Count; i < paneCnt; i++)
						{
							XmlNode newNode = document.CreateElement("pane");
							node.AppendChild(newNode);
						}
					}
					if (paneCnt < node.ChildNodes.Count)
					{
						//removing panes
						while (node.ChildNodes.Count > paneCnt)
						{
							node.RemoveChild(node.ChildNodes[node.ChildNodes.Count - 1]);
						}
					}
					node.Attributes["columns"].Value = newCol + "";
					// clearing column sizes
					if (node.Attributes["colsizes"] != null)
						node.Attributes.Remove(node.Attributes["colsizes"]);
					// update image
					TableUI.Document = document;
				}
			}
			catch (Exception err)
			{
				logger.Error(err + " " + err.StackTrace);
			}
		}

		private void rowsUI_KeyUp(object sender, KeyEventArgs e)
		{
			try
			{
				if (e.Key != Key.Enter) return;
				int newCol = Int32.Parse(rowsUI.Text);
				if ((newCol > 0) && (newCol < 4))
				{
					Grid item = TableUI.SelectedItems[0] as Grid;
					if (item == null) return;
					XmlNode node = item.DataContext as XmlNode;
					int rows = Int32.Parse(node.Attributes["rows"].Value);
					int cols = Int32.Parse(node.Attributes["columns"].Value);

					int paneCnt = newCol * cols;
					if (paneCnt > node.ChildNodes.Count)
					{
						//appending panes
						for (int i = node.ChildNodes.Count; i < paneCnt; i++)
						{
							XmlNode newNode = document.CreateElement("pane");
							node.AppendChild(newNode);
						}
					}
					if (paneCnt < node.ChildNodes.Count)
					{
						//removing panes
						while (node.ChildNodes.Count > paneCnt)
						{
							node.RemoveChild(node.ChildNodes[node.ChildNodes.Count - 1]);
						}
					}
					node.Attributes["rows"].Value = newCol + "";
					// clearing row sizes
					if (node.Attributes["rowsizes"] != null)
						node.Attributes.Remove(node.Attributes["rowsizes"]);
					// update image
					TableUI.Document = document;
				}
			}
			catch (Exception err)
			{
				logger.Error(err + " " + err.StackTrace);
			}
		}

		private void playUI_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				if(SaveData())
				{
					ProgramPlayerPreview dlg = new ProgramPlayerPreview();
					dlg.Title = Properties.Resources.titlePreview;
					dlg.Owner = parentWindow;
					dlg.Width = szScreen.Width;
					dlg.Height = szScreen.Height;
					dlg.Show();
					dlg.Play(fileName);
				}
			}
			catch (Exception err)
			{
				logger.Error(err + " " + err.StackTrace);
			}
		}

		private void TableUI_PreviewMouseDown(object sender, MouseButtonEventArgs e)
		{
			if (e.LeftButton == MouseButtonState.Pressed && e.ClickCount == 2)
			{
				loadPlaylist();
			}
		}
	}
}
