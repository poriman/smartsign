﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using DigitalSignage.ServerDatabase;
using DigitalSignage.Common;
using System.Collections.ObjectModel;
using System.Xml;
using System.IO;

namespace DigitalSignage.Scheduler
{
	/// <summary>
	/// Interaction logic for ReportViewer.xaml
	/// </summary>
	public partial class ReportViewer : Window
	{
        SpreadSheet _owcSheet = null;

        /// <summary>
        /// 그룹 노드가 찍혀있는지
        /// </summary>
        bool HasCheckGroup
        {
            get
            {
                Collection<TreeViewItem> items = groupBox.CheckedItems;
                if (items != null)
                {
                    foreach (TreeViewItem item in items)
                    {
                        XmlNode node = item.DataContext as XmlNode;

                        if (node.LocalName.Equals("group")) return true;
                    }
                }
                else
                    return false;

                return false;
            }
        }

        /// <summary>
        /// 생성자
        /// </summary>
        /// <param name="pNode"></param>
		public ReportViewer(XmlNode pNode)
		{
			InitializeComponent();
            MakeOWC();
            MakeYear();
            MakeMonth();

			DateTime now = DateTime.Now;

            dtSelectDate.SelectedDateTime = new DateTime(now.Year, now.Month, now.Day, 0, 0, 0);

			groupBox.HasPopupMenu = false;
			groupBox.SelectedItem = pNode;

            cbReportType.SelectedIndex = 0;
        }

        /// <summary>
        /// 생성자
        /// </summary>
        /// <param name="pNode"></param>
        /// <param name="selected"></param>
        public ReportViewer(XmlNode pNode, Task selected)
        {
            InitializeComponent();
            MakeOWC();
            MakeYear();
            MakeMonth();

            DateTime now = DateTime.Now;
            dtSelectDate.SelectedDateTime = new DateTime(now.Year, now.Month, now.Day, 0, 0, 0);

            groupBox.HasPopupMenu = false;
            groupBox.SelectedItem = pNode;

            cbReportType.SelectedIndex = 0;
        }

        /// <summary>
        /// SpreadSheet Object Web Component 생성
        /// </summary>
        void MakeOWC()
        {
            try
            {
                excelHost.Child = _owcSheet = new SpreadSheet();
            }
            catch 
            {
                MessageBox.Show(Properties.Resources.mbErrorOWC, Properties.Resources.titleReportViewer, MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        /// <summary>
        /// 년도 생성
        /// </summary>
        void MakeYear()
        {
            DateTime now = DateTime.Now;
            cbSelectYear.Items.Clear();
            cbSelectYear.Items.Add(now.Year);
            cbSelectYear.Items.Add(now.Year - 1);
            cbSelectYear.Items.Add(now.Year - 2);
            cbSelectYear.Items.Add(now.Year - 3);

            cbSelectYear.SelectedIndex = 0;
        }

        /// <summary>
        /// 월 생성
        /// </summary>
        void MakeMonth()
        {
            DateTime now = DateTime.Now;
            cbSelectMonth.Items.Clear();

            for (int i = 1; i <= 12; ++i)
                cbSelectMonth.Items.Add(i);

            cbSelectMonth.SelectedIndex = now.Month - 1;
        }

        private void cbReportType_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
			{
				gridFilter.Visibility = System.Windows.Visibility.Collapsed;
				gridReportDateUnit.Visibility = System.Windows.Visibility.Collapsed;
                gridSelectDate.Visibility = System.Windows.Visibility.Collapsed;
                gridSelectMonth.Visibility = System.Windows.Visibility.Collapsed;
                gridSelectYear.Visibility = System.Windows.Visibility.Collapsed;
                gridTargetGroup.Visibility = System.Windows.Visibility.Collapsed;
                gridTarget.Visibility = System.Windows.Visibility.Collapsed;
                cbReportUnit.SelectedIndex = -1;

                switch (cbReportType.SelectedIndex)
                {
                    case 0: /// 재생 통계
                        gridReportDateUnit.Visibility = System.Windows.Visibility.Visible;
                        cbReportUnit.SelectedIndex = 0;
						gridFilter.Visibility = System.Windows.Visibility.Visible;
                        break;
                    case 1: /// 타겟 광고 고객 현황
                        gridReportDateUnit.Visibility = System.Windows.Visibility.Visible;
                        gridTargetGroup.Visibility = System.Windows.Visibility.Visible;
                        cbReportUnit.SelectedIndex = 0;
                        break;
                    case 2: /// 일일 스케줄 현황
                        gridSelectDate.Visibility = System.Windows.Visibility.Visible;
                        gridTarget.Visibility = System.Windows.Visibility.Visible;
                        break;
                    case 3: /// 현재 스케줄 현황
                        gridTarget.Visibility = System.Windows.Visibility.Visible;
                        break;
                    case 4: /// DID 운영 현황
                        gridSelectDate.Visibility = System.Windows.Visibility.Visible;
                        gridTarget.Visibility = System.Windows.Visibility.Visible;
                        break;
					case 5: /// 플레이어 목록 현황
						break;
					case 6: /// 스케줄 목록 현황
						gridTarget.Visibility = System.Windows.Visibility.Visible;
						break;
                    case 7: /// 일일 플레이어 요약 현황
                        gridSelectDate.Visibility = System.Windows.Visibility.Visible;
                        gridTarget.Visibility = System.Windows.Visibility.Visible;
                        break;
                }
            }
            catch { }
        }

        private void cbReportUnit_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                switch (cbReportUnit.SelectedIndex)
                {
                    case 0: /// 일 단위
                        gridSelectDate.Visibility = System.Windows.Visibility.Visible;
                        gridSelectMonth.Visibility = System.Windows.Visibility.Collapsed;
                        gridSelectYear.Visibility = System.Windows.Visibility.Collapsed;
                        break;
                    case 1: /// 월 단위
                        gridSelectDate.Visibility = System.Windows.Visibility.Collapsed;
                        gridSelectMonth.Visibility = System.Windows.Visibility.Visible;
                        gridSelectYear.Visibility = System.Windows.Visibility.Visible;
                        break;
                    case 2: /// 년 단위
                        gridSelectDate.Visibility = System.Windows.Visibility.Collapsed;
                        gridSelectMonth.Visibility = System.Windows.Visibility.Collapsed;
                        gridSelectYear.Visibility = System.Windows.Visibility.Visible;
                        break;
                }
            }
            catch { }
        }

		/// <summary>
		/// 닫기 버튼 클릭
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void okButton_Click(object sender, RoutedEventArgs e)
		{
			this.Close();
		}


        private void btnExport_Click(object sender, RoutedEventArgs e)
		{
			this.IsEnabled = false;

			try
			{
				using (new DigitalSignage.Common.WaitCursor())
				{
                    if (_owcSheet != null)
                    {
                        _owcSheet.ExportTo();
                    }
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(String.Format(Properties.Resources.mbErrorOWC, ex.Message), Properties.Resources.titleReportViewer, MessageBoxButton.OK, MessageBoxImage.Error);
			}

			this.IsEnabled = true;

		}

		private void btnSaveToFile_Click(object sender, RoutedEventArgs e)
		{
			System.Windows.Forms.SaveFileDialog sfd = new System.Windows.Forms.SaveFileDialog();
			sfd.Title = Properties.Resources.labelSaveToFile;
			sfd.Filter = Properties.Resources.labelSaveLogFilter;
			sfd.OverwritePrompt = false;

			if (sfd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
			{
				this.IsEnabled = false;

				try
				{
					using (new DigitalSignage.Common.WaitCursor())
					{
                        if (_owcSheet != null)
                        {
                            _owcSheet.GetInstance.Export(sfd.FileName, Microsoft.Office.Interop.Owc11.SheetExportActionEnum.ssExportActionNone, Microsoft.Office.Interop.Owc11.SheetExportFormat.ssExportXMLSpreadsheet);
                            MessageBox.Show(Properties.Resources.mbSuccessCopyToFile, Properties.Resources.titleLogShower, MessageBoxButton.OK, MessageBoxImage.Information);
                        }
                        else
                        {
                            MessageBox.Show(String.Format(Properties.Resources.mbErrorCopyToFile, "Unknown Error."), Properties.Resources.titleLogShower, MessageBoxButton.OK, MessageBoxImage.Error);
                        }
					}
				}
				catch (Exception ex)
				{
					MessageBox.Show(String.Format(Properties.Resources.mbErrorCopyToFile, ex.Message), Properties.Resources.titleLogShower, MessageBoxButton.OK, MessageBoxImage.Error);

				} 
			}

			this.IsEnabled = true;
		}

        private bool ValidationCheck(ReportType type)
        {
            switch (cbReportType.SelectedIndex)
            {
                case 0: /// 재생 통계
                    break;
                case 1: /// 타겟 광고 고객 현황
                    break;
                case 2: /// 일일 스케줄 현황
                case 3: /// 현재 스케줄 현황
                case 4: /// DID 운영 현황
                case 7:
                    {
                        #region PlayerIDS
                        Collection<String> arrPids = new Collection<string>();
                        Collection<TreeViewItem> items = groupBox.CheckedItems;
                        if (items == null)
                        {
                            MessageBox.Show(Properties.Resources.mbSelectPlayerOrGroup, Properties.Resources.titleMessageBox, MessageBoxButton.OK, MessageBoxImage.Error);
                            groupBox.Focus();
                            return false;
                        }
                        else
                        {
                            foreach (TreeViewItem item in items)
                            {
                                XmlNode node = item.DataContext as XmlNode;

                                if (node.LocalName.Equals("player"))
                                {
                                    if (!arrPids.Contains(node.Attributes["pid"].Value))
                                        arrPids.Add(node.Attributes["pid"].Value);
                                }
                                else
                                {
                                    foreach (XmlNode player in node.SelectNodes("descendant::player[attribute::del_yn=\"N\"]"))
                                    {
                                        if (!arrPids.Contains(player.Attributes["pid"].Value))
                                            arrPids.Add(player.Attributes["pid"].Value);
                                    }
                                }
                            }
                        }
                        if (arrPids.Count == 0)
                        {
                            MessageBox.Show(Properties.Resources.mbSelectPlayerOrGroup, Properties.Resources.titleMessageBox, MessageBoxButton.OK, MessageBoxImage.Error);
                            groupBox.Focus();
                            return false;
                        }
                        #endregion
                    }
                    break;
            }

            if (gridSelectDate.Visibility == System.Windows.Visibility.Visible && (DateTime.Now - dtSelectDate.SelectedDateTime.Value).Days < 1)
            {
                MessageBox.Show(Properties.Resources.mbErrorReportConstraitDate, Properties.Resources.titleReportViewer, MessageBoxButton.OK, MessageBoxImage.Error);
                dtSelectDate.Focus();
                return false;
            }

            return true;
        }

        private void btnMakeReport_Click(object sender, RoutedEventArgs e)
        {
            ReportType type = GetReportType();

            /// 유효성 체크
            if (!ValidationCheck(type)) return;

            _owcSheet.Enabled = false;
            try
            {
                using (new DigitalSignage.Common.WaitCursor())
                {
                    /// 헤더 생성
                    MakeHeader(type);

                    _owcSheet.GetInstance.Refresh();
                }
            }
            catch { }
            _owcSheet.Enabled = true;
        }

        /// <summary>
        /// 헤더 생성
        /// </summary>
        /// <param name="type"></param>
        private void MakeHeader(ReportType type)
        {
            try
            {
                String SheetName = String.Empty;
                DateTime dtSelected = DateTime.Now;
                switch (type)
                {
                    case ReportType.DailyPlayReport:
                        SheetName = "P_DAY";
                        dtSelected = dtSelectDate.SelectedDateTime.Value;
                        break;
                    case ReportType.MonthlyPlayReport:
                        SheetName = "P_MONTH";
                        dtSelected = new DateTime(Convert.ToInt32(cbSelectYear.Text), Convert.ToInt32(cbSelectMonth.Text), 1);
                        break;
                    case ReportType.YearlyPlayReport:
                        dtSelected = new DateTime(Convert.ToInt32(cbSelectYear.Text), 1, 1);
                        SheetName = "P_YEAR";
                        break;
                    case ReportType.DailyTargetAdReport:
                        SheetName = "T_DAY";
                        dtSelected = dtSelectDate.SelectedDateTime.Value;
                        break;
                    case ReportType.MonthlyTargetAdReport:
                        SheetName = "T_MONTH";
                         dtSelected = new DateTime(Convert.ToInt32(cbSelectYear.Text), Convert.ToInt32(cbSelectMonth.Text), 1);
                       break;
                    case ReportType.YearlyTargetAdReport:
                        dtSelected = new DateTime(Convert.ToInt32(cbSelectYear.Text), 1, 1);
                        SheetName = "T_YEAR";
                        break;
                    case ReportType.DailyScheduleReport:
                        SheetName = "S_DAY";
                        dtSelected = dtSelectDate.SelectedDateTime.Value;
                        break;
                    case ReportType.CurrentScheduleReport:
                        SheetName = "S_CURRENT";
                        dtSelected = DateTime.Now;
                        break;
                    case ReportType.RunningReport:
                        SheetName = "R_DAY";
                        dtSelected = dtSelectDate.SelectedDateTime.Value;
                        break;
                    case ReportType.PlayerListReport:
                        SheetName = "P_LIST";
                        break;
					case ReportType.ScheduleListReport:
						SheetName = "S_LIST";
						break;
                    case ReportType.PlayerSummaryReport:
                        SheetName = "P_SUMMARY";
                        dtSelected = dtSelectDate.SelectedDateTime.Value;
                        break;
                }

                _owcSheet.GetInstance.Cells.Clear();
                Microsoft.Office.Interop.Owc11.Worksheet ws = _owcSheet.GetInstance.ActiveSheet;
                Microsoft.Office.Interop.Excel.Application app = null;
                Microsoft.Office.Interop.Excel.Workbook wb1 = null;
                try
                {

                    app = new Microsoft.Office.Interop.Excel.Application();
                    app.DisplayAlerts = false;

                    wb1 = app.Workbooks.Open(Config.GetConfig.AppData + "ReportTemplate.xlsx");
                    if (wb1 == null)
                    {
                        MessageBox.Show(String.Format(Properties.Resources.mbErrorReportFile, "Report File is not exist."), Properties.Resources.titleReportViewer, MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }

                    Microsoft.Office.Interop.Excel.Worksheet sheet = wb1.Sheets[SheetName] as Microsoft.Office.Interop.Excel.Worksheet;
                    sheet.Activate();

                    #region 컨텐트 채우기
                    /// 컨텐트 생성
                    switch (cbReportType.SelectedIndex)
                    {
                        case 0: /// 재생 통계

                            if (cbReportUnit.SelectedIndex == 0)
                                DrawDailyPlayReport(sheet);
                            else if (cbReportUnit.SelectedIndex == 1)
                                DrawMonthlyPlayReport(sheet);
                            else DrawYearlyPlayReport(sheet);

                            break;
                        case 1: /// 타겟 광고 고객 현황

                            if (cbReportUnit.SelectedIndex == 0)
                                DrawDailyTargetReport(sheet);
                            else if (cbReportUnit.SelectedIndex == 1)
                                DrawMonthlyTargetReport(sheet);
                            else DrawYearlyTargetReport(sheet);

                            break;
                        case 2: /// 일일 스케줄 현황
                            DrawDailyScheduleReport(sheet);
                            break;
                        case 3: /// 현재 스케줄 현황
                            DrawCurrentScheduleReport(sheet);
                            break;
                        case 4: /// DID 운영 현황
                            DrawRunningReport(sheet);
                            break;
                        case 5: /// 플레이어 목록 현황
                            DrawPlayListReport(sheet);
                            break;
						case 6: ///	스케줄 목록 현황
							DrawScheduleListReport(sheet);
							break;
                        case 7: /// 일별 플레이어 상태 요약 현황
                            DrawPlayerSummaryReport(sheet);
                            break;

                    }
                    #endregion

                    sheet.Cells.Select();
                    sheet.Cells.Copy();
                    System.Threading.Thread.Sleep(500);
                    ws.Cells.Paste();
                    System.Threading.Thread.Sleep(500);
                    ws.Cells[2, 1] = dtSelected;

                }
                catch (Exception exExcel)
                {
                    MessageBox.Show(String.Format(Properties.Resources.mbErrorExcel, exExcel.Message), Properties.Resources.titleReportViewer, MessageBoxButton.OK, MessageBoxImage.Error);
                }
                finally
                {
                    try
                    {
                        if (wb1 != null)
                        {
                            wb1.Saved = true;
                            wb1.Close();
                            wb1 = null;
                        }
                    }
                    catch { }
                    try
                    {
                        if (app != null)
                        {
                            app.CutCopyMode = Microsoft.Office.Interop.Excel.XlCutCopyMode.xlCopy;
                            app.Quit();
                            app = null;
                        }
                    }
                    catch { }
                }
                
              
            }
            catch (Exception ex) 
            {
                MessageBox.Show(String.Format(Properties.Resources.mbErrorExcel, ex.Message), Properties.Resources.titleReportViewer, MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }


        /// <summary>
        /// 재생 조건을 문자열로 변환
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        private String ConvertPlayConstraitToString(ServerDataSet.playreportsRow row)
        {
            String sReturn = String.Empty;

            if (row.s_is_ad.Equals("Y"))
            {
                sReturn = Properties.Resources.labelReportPlayTypeAd;
                if ((row.s_screen_type & (int)ShowFlag.Show_Event) > 0)
                {
                    sReturn += String.Format("/{0}", Properties.Resources.labelReportPlayTypeEvent);
                }

                if (row.s_meta_tag.Contains(Properties.Resources.labelReportPlayTypeEntries)) sReturn += String.Format("/{0}", Properties.Resources.labelReportPlayTypeEntries);
                else if (row.s_meta_tag.Contains(Properties.Resources.labelReportPlayTypeCoupon)) sReturn += String.Format("/{0}", Properties.Resources.labelReportPlayTypeCoupon);
            }
            else sReturn = Properties.Resources.labelReportPlayTypeNormal; 

            return sReturn;
        }

		/// <summary>
		/// 재생 조건을 문자열로 변환
		/// </summary>
		/// <param name="row"></param>
		/// <returns></returns>
		private String ConvertPlayConstraitToString(ServerDataSet.tasksRow row)
		{
			String sReturn = String.Empty;

			if (row.is_ad.Equals("Y"))
			{
				sReturn = Properties.Resources.labelReportPlayTypeAd;
				if ((row.show_flag & (int)ShowFlag.Show_Event) > 0)
				{
					sReturn += String.Format("/{0}", Properties.Resources.labelReportPlayTypeEvent);
				}

				if (row.meta_tag.Contains(Properties.Resources.labelReportPlayTypeEntries)) sReturn += String.Format("/{0}", Properties.Resources.labelReportPlayTypeEntries);
				else if (row.meta_tag.Contains(Properties.Resources.labelReportPlayTypeCoupon)) sReturn += String.Format("/{0}", Properties.Resources.labelReportPlayTypeCoupon);
			}
			else sReturn = Properties.Resources.labelReportPlayTypeNormal;

			return sReturn;
		}

        /// <summary>
        /// 일일 플레이어의 상태 보고서를 그린다.
        /// </summary>
        /// <param name="sheet"></param>
        private void DrawPlayerSummaryReport(Microsoft.Office.Interop.Excel.Worksheet sheet)
        {
            int nRow = 5;

            #region PlayerIDS
            Collection<String> arrPids = new Collection<string>();
            Collection<TreeViewItem> items = groupBox.CheckedItems;
            if (items == null)
            {
                MessageBox.Show(Properties.Resources.mbSelectPlayerOrGroup, Properties.Resources.titleMessageBox, MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            else
            {
                foreach (TreeViewItem item in items)
                {
                    XmlNode node = item.DataContext as XmlNode;

                    if (node.LocalName.Equals("player"))
                    {
                        if (!arrPids.Contains(node.Attributes["pid"].Value))
                            arrPids.Add(node.Attributes["pid"].Value);
                    }
                    else
                    {
                        foreach (XmlNode player in node.SelectNodes("descendant::player[attribute::del_yn=\"N\"]"))
                        {
                            if (!arrPids.Contains(player.Attributes["pid"].Value))
                                arrPids.Add(player.Attributes["pid"].Value);
                        }
                    }
                }
            }
            if (arrPids.Count == 0)
            {
                MessageBox.Show(Properties.Resources.mbSelectPlayerOrGroup, Properties.Resources.titleMessageBox, MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            #endregion

            DateTime dtSelected = dtSelectDate.SelectedDateTime.Value;
            using (ServerDataSet.summary_playersDataTable dt = Config.GetConfig.ServerLogList.GetPlayerSummaryDataByYYYYMMDD(arrPids.ToArray(), dtSelected.Year, dtSelected.Month, dtSelected.Day))
            {
                if (dt == null) return;

                foreach (ServerDataSet.summary_playersRow row in dt.Rows)
                {
                    #region 빈 로우 추가
                    /// 빈 로우 추가
                    Microsoft.Office.Interop.Excel.Range r1 = sheet.Rows[nRow] as Microsoft.Office.Interop.Excel.Range;
                    r1.Select();
                    r1.Copy();
                    sheet.Rows[nRow + 1].Insert(Microsoft.Office.Interop.Excel.XlInsertShiftDirection.xlShiftDown);
                    sheet.Rows[nRow + 1].PasteSpecial();
                    #endregion


                    sheet.Cells[nRow, 1] = ServerDataCache.GetInstance.GetGroupNameByPID(row.pid);
                    sheet.Cells[nRow, 2] = ServerDataCache.GetInstance.GetPlayerName(row.pid);
                    sheet.Cells[nRow, 3] = row.pid;
                    if (row.beginconndt == -1) sheet.Cells[nRow, 4] = "-";
                    else sheet.Cells[nRow, 4] = (new DateTime(row.beginconndt)).ToLocalTime();
                    if (row.lastconndt == -1) sheet.Cells[nRow, 5] = "-";
                    else sheet.Cells[nRow, 5] = (new DateTime(row.lastconndt)).ToLocalTime();
                    sheet.Cells[nRow, 6] = row.memoryusage;
                    sheet.Cells[nRow, 7] = row.cpurate;
                    sheet.Cells[nRow, 8] = row.freespace/1024;
                    sheet.Cells[nRow, 9] = row.volume;

                    int major = (row.versionH >> 16) & 0xFFFF;
                    int minor = row.versionH & 0xFFFF;
                    int build = (row.versionL >> 16) & 0xFFFF;
                    int revision = row.versionL & 0xFFFF;
                    sheet.Cells[nRow, 10] = String.Format("{0}.{1}.{2}.{3}", major, minor, build, revision);

                    nRow++;
                }

                sheet.Rows[nRow].Delete();
                sheet.Rows[nRow].Delete();

            }
        }
        /// <summary>
        /// 일일스케줄 보고서를 그린다.
        /// </summary>
        /// <param name="sheet"></param>
        private void DrawDailyScheduleReport(Microsoft.Office.Interop.Excel.Worksheet sheet)
        {
            int nRow = 5;
            int nCol = 5;

            #region PlayerIDS
            Collection<String> arrPids = new Collection<string>();
            Collection<TreeViewItem> items = groupBox.CheckedItems;
            if (items == null)
            {
                MessageBox.Show(Properties.Resources.mbSelectPlayerOrGroup, Properties.Resources.titleMessageBox, MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            else
            {
                foreach (TreeViewItem item in items)
                {
                    XmlNode node = item.DataContext as XmlNode;

                    if (node.LocalName.Equals("player"))
                    {
                        if (!arrPids.Contains(node.Attributes["pid"].Value))
                            arrPids.Add(node.Attributes["pid"].Value);
                    }
                    else
                    {
                        foreach (XmlNode player in node.SelectNodes("descendant::player[attribute::del_yn=\"N\"]"))
                        {
                            if (!arrPids.Contains(player.Attributes["pid"].Value))
                                arrPids.Add(player.Attributes["pid"].Value);
                        }
                    }
                }
            }
            if (arrPids.Count == 0)
            {
                MessageBox.Show(Properties.Resources.mbSelectPlayerOrGroup, Properties.Resources.titleMessageBox, MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            #endregion

            DateTime dtSelected = dtSelectDate.SelectedDateTime.Value;
            using (ServerDataSet.report_time_scheduleDataTable dt = Config.GetConfig.ServerLogList.GetScheduleReportDataByYYYYMMDD(arrPids.ToArray(), dtSelected.Year, dtSelected.Month, dtSelected.Day))
            {
                if (dt == null) return;

                string prev_pid = String.Empty;
                foreach (ServerDataSet.report_time_scheduleRow row in dt.Rows)
                {
                    if (String.IsNullOrEmpty(prev_pid))
                    {
                        #region 빈 로우 추가
                        /// 빈 로우 추가
                        Microsoft.Office.Interop.Excel.Range r1 = sheet.Rows[String.Format("{0}:{1}", nRow, nRow + 1)] as Microsoft.Office.Interop.Excel.Range;
                        r1.Select();
                        r1.Copy();
                        sheet.Rows[nRow + 2].Insert(Microsoft.Office.Interop.Excel.XlInsertShiftDirection.xlShiftDown);
                        sheet.Rows[nRow + 2].PasteSpecial();
                        #endregion

                        sheet.Cells[nRow, 1] = ServerDataCache.GetInstance.GetGroupNameByPID(row.pid);
						sheet.Cells[nRow, 2] = ServerDataCache.GetInstance.GetPlayerName(row.pid);
						sheet.Cells[nRow, 3] = row.pid;

                        prev_pid = row.pid;
                    }
                    else if (!prev_pid.Equals(row.pid))
                    {
                        nRow += 2;

                        #region 빈 로우 추가
                        /// 빈 로우 추가
                        Microsoft.Office.Interop.Excel.Range r1 = sheet.Rows[String.Format("{0}:{1}", nRow, nRow + 1)] as Microsoft.Office.Interop.Excel.Range;
                        r1.Select();
                        r1.Copy();
                        sheet.Rows[nRow + 2].Insert(Microsoft.Office.Interop.Excel.XlInsertShiftDirection.xlShiftDown);
                        sheet.Rows[nRow + 2].PasteSpecial();
                        #endregion

                        sheet.Cells[nRow, 1] = ServerDataCache.GetInstance.GetGroupNameByPID(row.pid);
                        sheet.Cells[nRow, 2] = ServerDataCache.GetInstance.GetPlayerName(row.pid);
						sheet.Cells[nRow, 3] = row.pid;

                        prev_pid = row.pid;
                    }

                    sheet.Cells[row.type == (int)TaskCommands.CmdProgram ? nRow + 1 : nRow, nCol + Convert.ToInt32(row.H24)] = Convert.ToInt32(row.CNT);
                }

                if (dt.Rows.Count > 0) nRow += 2;

                sheet.Rows[nRow].Delete();
                sheet.Rows[nRow].Delete();
            }
        }

        /// <summary>
        /// 플레이어 목록을 추출한다.
        /// </summary>
        /// <param name="sheet"></param>
        private void DrawPlayListReport(Microsoft.Office.Interop.Excel.Worksheet sheet)
        {
            try
            {
                int nRow = 5;

                XmlNode rootGroup = ServerDataCache.GetInstance.GetRootGroupNode();

                if (rootGroup == null)
                {
                    MessageBox.Show(Properties.Resources.mbNoResult, Properties.Resources.titleReportViewer, MessageBoxButton.OK, MessageBoxImage.Information);
                    return;
                }

                XmlNodeList nodelistPlayers = rootGroup.SelectNodes("//child::player[attribute::del_yn=\"N\"]");
                XmlNode g_node = null;

                foreach (XmlNode p_node in nodelistPlayers)
                {
                    if (p_node.ParentNode != g_node)
                        g_node = p_node.ParentNode;

                    #region 빈 로우 추가
                    /// 빈 로우 추가
                    Microsoft.Office.Interop.Excel.Range r1 = sheet.Rows[nRow] as Microsoft.Office.Interop.Excel.Range;
                    r1.Select();
                    r1.Copy();
                    sheet.Rows[nRow + 1].Insert(Microsoft.Office.Interop.Excel.XlInsertShiftDirection.xlShiftDown);
                    sheet.Rows[nRow + 1].PasteSpecial();
                    #endregion

                    sheet.Cells[nRow, 1] = g_node.Attributes["name"].Value;
                    sheet.Cells[nRow, 2] = p_node.Attributes["name"].Value;
                    sheet.Cells[nRow, 3] = p_node.Attributes["pid"].Value;

                    nRow++;
                }

                sheet.Rows[nRow].Delete();
            }
            catch { }

        }

		/// <summary>
		/// 현재 스케줄 목록을 추출한다.
		/// </summary>
		/// <param name="sheet"></param>
		private void DrawScheduleListReport(Microsoft.Office.Interop.Excel.Worksheet sheet)
		{
			int nRow = 5;

			Collection<String> arrPids = new Collection<string>();
			XmlNode node = groupBox.SelectedItem as XmlNode;
			if (node == null)
			{
				MessageBox.Show(Properties.Resources.mbSelectPlayerOrGroup, Properties.Resources.titleMessageBox, MessageBoxButton.OK, MessageBoxImage.Error);
				return;
			}

			String gid = String.Empty;
			String pid = String.Empty;

			String GroupName = String.Empty;
			String PlayerName = String.Empty;

			if (node.LocalName.Equals("player"))
			{
				gid = node.Attributes["gid"].Value;
				pid = node.Attributes["pid"].Value;

				GroupName = ServerDataCache.GetInstance.GetGroupNameByPID(pid);
				PlayerName = node.Attributes["name"].Value;
			}
			else
			{
				gid = node.Attributes["gid"].Value;
				pid = "-";

				GroupName = node.Attributes["name"].Value;
				PlayerName = "-";

			}

			sheet.Cells[nRow, 1] = GroupName;
			sheet.Cells[nRow, 3] = PlayerName;
			sheet.Cells[nRow, 5] = pid;

			nRow = 8;

			DateTime now = DateTime.Now;
			long dayStart = TimeConverter.ConvertToUTP((new DateTime(now.Year, now.Month, now.Day, 0, 0, 0)).ToUniversalTime());
			long dayEnd = dayStart + 3599;
			using (ServerDataSet.tasksDataTable dt = Config.GetConfig.ServerTasksList.GetTasksFrom(dayStart, dayEnd))
			{
				//	Server상의 모든 상위 gid를 뽑아온다.
				Collection<String> arrServerGids = new Collection<String>();

				string sReturn = Config.GetConfig.ServerGroupsList.GetGroupListByGID(gid);

				if (!String.IsNullOrEmpty(sReturn))
				{
					string[] arrReturns = sReturn.Split('|');

					foreach (String sGid in arrReturns)
					{
						arrServerGids.Add(sGid);
					}
				}

				int nIndex = 1;

				foreach (ServerDataSet.tasksRow task in dt.OrderByDescending(e => e.priority))
				{
					if (task.type == (int)TaskCommands.CmdProgram || task.type == (int)TaskCommands.CmdDefaultProgram)
					{
						if ((arrServerGids.Contains(task.gid) && String.IsNullOrEmpty(task.pid)) || task.pid == pid)
						{
							if (CheckDayOfWeek(task.daysofweek))
							{
								String sRange = String.Empty;
								if (task.type == (int)TaskCommands.CmdProgram)
								{
									if (task.starttimeofday != task.endtimeofday)
									{
										sRange = String.Format("{0} ~ {1}", TimeSpan.FromSeconds(task.starttimeofday), TimeSpan.FromSeconds(task.endtimeofday));
									}
									else
									{
										sRange = String.Format("{0} ~ {1}", TimeConverter.ConvertFromUTP(task.starttime).ToLocalTime().ToShortDateString(), TimeConverter.ConvertFromUTP(task.endtime).ToLocalTime().ToShortDateString());
									}

								}
								else if (task.type == (int)TaskCommands.CmdDefaultProgram)
								{
									sRange = Properties.Resources.comboitemDefaultSchedule;
								}
								else continue;

								/// 빈 로우 추가
								Microsoft.Office.Interop.Excel.Range r1 = sheet.Rows[nRow] as Microsoft.Office.Interop.Excel.Range;
								r1.Select();
								r1.Copy();
								sheet.Rows[nRow + 1].Insert(Microsoft.Office.Interop.Excel.XlInsertShiftDirection.xlShiftDown);
								sheet.Rows[nRow + 1].PasteSpecial();

								sheet.Cells[nRow, 1] = nIndex++;
								sheet.Cells[nRow, 2] = task.name;
								sheet.Cells[nRow, 3] = sRange;
								sheet.Cells[nRow, 4] = task.duration;
								sheet.Cells[nRow, 5] = ConvertPlayConstraitToString(task);

								nRow++;
							}
						}
						
					}
				}
			}

			sheet.Rows[nRow].Delete();
			sheet.Rows[nRow].Delete();

		}

        /// <summary>
        /// DID 운영 보고서를 그린다.
        /// </summary>
        /// <param name="sheet"></param>
        private void DrawRunningReport(Microsoft.Office.Interop.Excel.Worksheet sheet)
        {
            int nRow = 5;
            int nCol = 4;

            #region PlayerIDS
            Collection<String> arrPids = new Collection<string>();
            Collection<TreeViewItem> items = groupBox.CheckedItems;
            if (items == null)
            {
                MessageBox.Show(Properties.Resources.mbSelectPlayerOrGroup, Properties.Resources.titleMessageBox, MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            else
            {
                foreach (TreeViewItem item in items)
                {
                    XmlNode node = item.DataContext as XmlNode;

                    if (node.LocalName.Equals("player"))
                    {
                        if (!arrPids.Contains(node.Attributes["pid"].Value))
                            arrPids.Add(node.Attributes["pid"].Value);
                    }
                    else
                    {
                        foreach (XmlNode player in node.SelectNodes("descendant::player[attribute::del_yn=\"N\"]"))
                        {
                            if (!arrPids.Contains(player.Attributes["pid"].Value))
                                arrPids.Add(player.Attributes["pid"].Value);
                        }
                    }
                }
            }
            if (arrPids.Count == 0)
            {
                MessageBox.Show(Properties.Resources.mbSelectPlayerOrGroup, Properties.Resources.titleMessageBox, MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            #endregion

            DateTime dtSelected = dtSelectDate.SelectedDateTime.Value;
            using (ServerDataSet.report_running_timeDataTable dt = Config.GetConfig.ServerLogList.GetRunningReportDataByYYYYMMDD(arrPids.ToArray(), dtSelected.Year, dtSelected.Month, dtSelected.Day))
            {
                if (dt == null) return;

                string prev_pid = String.Empty;
                foreach (ServerDataSet.report_running_timeRow row in dt.Rows)
                {
                    if (String.IsNullOrEmpty(prev_pid))
                    {
                        #region 빈 로우 추가
                        /// 빈 로우 추가
                        Microsoft.Office.Interop.Excel.Range r1 = sheet.Rows[nRow] as Microsoft.Office.Interop.Excel.Range;
                        r1.Select();
                        r1.Copy();
                        sheet.Rows[nRow + 1].Insert(Microsoft.Office.Interop.Excel.XlInsertShiftDirection.xlShiftDown);
                        sheet.Rows[nRow + 1].PasteSpecial();
                        #endregion
        
                        sheet.Cells[nRow, 1] = ServerDataCache.GetInstance.GetGroupNameByPID(row.pid);
                        sheet.Cells[nRow, 2] = ServerDataCache.GetInstance.GetPlayerName(row.pid);
						sheet.Cells[nRow, 3] = row.pid;

                        prev_pid = row.pid;
                    }
                    else if (!prev_pid.Equals(row.pid))
                    {
                        nRow++;

                        #region 빈 로우 추가
                        /// 빈 로우 추가
                        Microsoft.Office.Interop.Excel.Range r1 = sheet.Rows[nRow] as Microsoft.Office.Interop.Excel.Range;
                        r1.Select();
                        r1.Copy();
                        sheet.Rows[nRow + 1].Insert(Microsoft.Office.Interop.Excel.XlInsertShiftDirection.xlShiftDown);
                        sheet.Rows[nRow + 1].PasteSpecial();
                        #endregion

                        sheet.Cells[nRow, 1] = ServerDataCache.GetInstance.GetGroupNameByPID(row.pid);
                        sheet.Cells[nRow, 2] = ServerDataCache.GetInstance.GetPlayerName(row.pid);
						sheet.Cells[nRow, 3] = row.pid;

                        prev_pid = row.pid;
                    }

                    sheet.Cells[nRow, nCol + Convert.ToInt32(row.H24)] = Convert.ToInt32(row.TM);
                }

                if (dt.Rows.Count > 0) nRow++;

                sheet.Rows[nRow].Delete();
                sheet.Rows[nRow].Delete();
            }
        }

        /// <summary>
        /// 재생 통계 보고서를 그린다.
        /// </summary>
        private void DrawDailyPlayReport(Microsoft.Office.Interop.Excel.Worksheet sheet)
        {
            int nRow = 5;
            int nCol = 5;
            DateTime dtSelected = dtSelectDate.SelectedDateTime.Value;
            using (ServerDataSet.playreportsDataTable dt = Config.GetConfig.ServerLogList.GetPlayReportDataByYYYYMMDD(dtSelected.Year, dtSelected.Month, dtSelected.Day))
            {
                if (dt == null) return;

				string sFilterText = cbReportFilter.Text.ToLower();
				string prev_uuid = String.Empty; 
                
                foreach (ServerDataSet.playreportsRow row in dt.Rows)
                {
					if (!row.s_name.ToLower().Contains(sFilterText)) continue;

                    if (String.IsNullOrEmpty(prev_uuid))
                    {
                        
                        #region 빈 로우 추가
                        /// 빈 로우 추가
                        Microsoft.Office.Interop.Excel.Range r1 = sheet.Rows[nRow] as Microsoft.Office.Interop.Excel.Range;
                        r1.Select();
                        r1.Copy();
                        sheet.Rows[nRow + 1].Insert(Microsoft.Office.Interop.Excel.XlInsertShiftDirection.xlShiftDown);
                        sheet.Rows[nRow + 1].PasteSpecial();
                        #endregion

						sheet.Cells[nRow, 1] = row.s_name.Replace(".bin", "");
						if (row.s_is_ad == "Y") sheet.Cells[nRow, 2] = String.Format("{0} ~ {1}", TimeConverter.ConvertFromUTP(row.s_starttime).ToLocalTime().ToShortDateString(), TimeConverter.ConvertFromUTP(row.s_endtime).ToLocalTime().ToShortDateString());
                        else sheet.Cells[nRow, 2] = "-";

                        sheet.Cells[nRow, 3] = row.s_playtime;
                        sheet.Cells[nRow, 4] = ConvertPlayConstraitToString(row);

                        prev_uuid = row.s_id.ToString();
                    }
                    else if (!prev_uuid.Equals(row.s_id.ToString()))
                    {
                        nRow++;

                        #region 빈 로우 추가
                        /// 빈 로우 추가
                        Microsoft.Office.Interop.Excel.Range r1 = sheet.Rows[nRow] as Microsoft.Office.Interop.Excel.Range;
                        r1.Select();
                        r1.Copy();
                        sheet.Rows[nRow + 1].Insert(Microsoft.Office.Interop.Excel.XlInsertShiftDirection.xlShiftDown);
                        sheet.Rows[nRow + 1].PasteSpecial();
                        #endregion

						sheet.Cells[nRow, 1] = row.s_name.Replace(".bin", "");
						if (row.s_is_ad == "Y") sheet.Cells[nRow, 2] = String.Format("{0} ~ {1}", TimeConverter.ConvertFromUTP(row.s_starttime).ToLocalTime().ToShortDateString(), TimeConverter.ConvertFromUTP(row.s_endtime).ToLocalTime().ToShortDateString());
                        else sheet.Cells[nRow, 2] = "-";
                        sheet.Cells[nRow, 3] = row.s_playtime;
                        sheet.Cells[nRow, 4] = ConvertPlayConstraitToString(row);

                        prev_uuid = row.s_id.ToString(); ;
                    }

                    sheet.Cells[nRow, nCol + Convert.ToInt32(row.H24)] = Convert.ToInt32(row.CNT);
                }

                if (dt.Rows.Count > 0) nRow++;
                
                sheet.Rows[nRow].Delete();
                sheet.Rows[nRow].Delete();
            }
        }

        /// <summary>
        /// 재생 통계 보고서를 그린다.
        /// </summary>
        private void DrawMonthlyPlayReport(Microsoft.Office.Interop.Excel.Worksheet sheet)
        {
            int nRow = 5;
            int nCol = 5;
            int nDays = DateTime.DaysInMonth(Convert.ToInt32(cbSelectYear.Text), Convert.ToInt32(cbSelectMonth.Text));

            using (ServerDataSet.playreportsDataTable dt = Config.GetConfig.ServerLogList.GetPlayReportDataByYYYYMM(Convert.ToInt32(cbSelectYear.Text), Convert.ToInt32(cbSelectMonth.Text)))
            {
                if (dt == null) return;

				string sFilterText = cbReportFilter.Text.ToLower();
				string prev_uuid = String.Empty;
                foreach (ServerDataSet.playreportsRow row in dt.Rows)
                {
					if (!row.s_name.ToLower().Contains(sFilterText)) continue;

                    if (String.IsNullOrEmpty(prev_uuid))
                    {
                        #region 빈 로우 추가
                        /// 빈 로우 추가
                        Microsoft.Office.Interop.Excel.Range r1 = sheet.Rows[nRow] as Microsoft.Office.Interop.Excel.Range;
                        r1.Select();
                        r1.Copy();
                        sheet.Rows[nRow + 1].Insert(Microsoft.Office.Interop.Excel.XlInsertShiftDirection.xlShiftDown);
                        sheet.Rows[nRow + 1].PasteSpecial();
                        #endregion

						sheet.Cells[nRow, 1] = row.s_name.Replace(".bin", "");
						if (row.s_is_ad == "Y") sheet.Cells[nRow, 2] = String.Format("{0} ~ {1}", TimeConverter.ConvertFromUTP(row.s_starttime).ToLocalTime().ToShortDateString(), TimeConverter.ConvertFromUTP(row.s_endtime).ToLocalTime().ToShortDateString());
                        else sheet.Cells[nRow, 2] = "-";

                        sheet.Cells[nRow, 3] = row.s_playtime;
                        sheet.Cells[nRow, 4] = ConvertPlayConstraitToString(row);

                        prev_uuid = row.s_id.ToString();
                    }
                    else if (!prev_uuid.Equals(row.s_id.ToString()))
                    {
                        nRow++;

                        #region 빈 로우 추가
                        /// 빈 로우 추가
                        Microsoft.Office.Interop.Excel.Range r1 = sheet.Rows[nRow] as Microsoft.Office.Interop.Excel.Range;
                        r1.Select();
                        r1.Copy();
                        sheet.Rows[nRow + 1].Insert(Microsoft.Office.Interop.Excel.XlInsertShiftDirection.xlShiftDown);
                        sheet.Rows[nRow + 1].PasteSpecial();
                        #endregion

						sheet.Cells[nRow, 1] = row.s_name.Replace(".bin", "");
						if (row.s_is_ad == "Y") sheet.Cells[nRow, 2] = String.Format("{0} ~ {1}", TimeConverter.ConvertFromUTP(row.s_starttime).ToLocalTime().ToShortDateString(), TimeConverter.ConvertFromUTP(row.s_endtime).ToLocalTime().ToShortDateString());
                        else sheet.Cells[nRow, 2] = "-";
                        sheet.Cells[nRow, 3] = row.s_playtime;
                        sheet.Cells[nRow, 4] = ConvertPlayConstraitToString(row);

                        prev_uuid = row.s_id.ToString();
                    }
                    
                    int nCnt = Convert.ToInt32(sheet.Cells[nRow, nCol + Convert.ToInt32(row.DD) - 1].Value2);
                    sheet.Cells[nRow, nCol + Convert.ToInt32(row.DD) - 1] = nCnt + Convert.ToInt32(row.CNT);
                }

                if (dt.Rows.Count > 0) nRow++;

                sheet.Rows[nRow].Delete();
                sheet.Rows[nRow].Delete();

                for (int i = nDays; i < 31; ++i)
                {
                    sheet.Columns[nCol + nDays].Delete();
                }
            }
        }

        /// <summary>
        /// 재생 통계 보고서를 그린다.
        /// </summary>
        private void DrawYearlyPlayReport(Microsoft.Office.Interop.Excel.Worksheet sheet)
        {
            int nRow = 5;
            int nCol = 5;
            using (ServerDataSet.playreportsDataTable dt = Config.GetConfig.ServerLogList.GetPlayReportDataByYYYY(Convert.ToInt32(cbSelectYear.Text)))
            {
                if (dt == null) return;

				string sFilterText = cbReportFilter.Text.ToLower();
                string prev_uuid = String.Empty;
                foreach (ServerDataSet.playreportsRow row in dt.Rows)
                {
					if (!row.s_name.ToLower().Contains(sFilterText)) continue;

                    if (String.IsNullOrEmpty(prev_uuid))
                    {
                        #region 빈 로우 추가
                        /// 빈 로우 추가
                        Microsoft.Office.Interop.Excel.Range r1 = sheet.Rows[nRow] as Microsoft.Office.Interop.Excel.Range;
                        r1.Select();
                        r1.Copy();
                        sheet.Rows[nRow + 1].Insert(Microsoft.Office.Interop.Excel.XlInsertShiftDirection.xlShiftDown);
                        sheet.Rows[nRow + 1].PasteSpecial();
                        #endregion

                        sheet.Cells[nRow, 1] = row.s_name.Replace(".bin", "");
						if (row.s_is_ad == "Y") sheet.Cells[nRow, 2] = String.Format("{0} ~ {1}", TimeConverter.ConvertFromUTP(row.s_starttime).ToLocalTime().ToShortDateString(), TimeConverter.ConvertFromUTP(row.s_endtime).ToLocalTime().ToShortDateString());
                        else sheet.Cells[nRow, 2] = "-";

                        sheet.Cells[nRow, 3] = row.s_playtime;
                        sheet.Cells[nRow, 4] = ConvertPlayConstraitToString(row);

                        prev_uuid = row.s_id.ToString();
                    }
                    else if (!prev_uuid.Equals(row.s_id.ToString()))
                    {
                        nRow++;

                        #region 빈 로우 추가
                        /// 빈 로우 추가
                        Microsoft.Office.Interop.Excel.Range r1 = sheet.Rows[nRow] as Microsoft.Office.Interop.Excel.Range;
                        r1.Select();
                        r1.Copy();
                        sheet.Rows[nRow + 1].Insert(Microsoft.Office.Interop.Excel.XlInsertShiftDirection.xlShiftDown);
                        sheet.Rows[nRow + 1].PasteSpecial();
                        #endregion

						sheet.Cells[nRow, 1] = row.s_name.Replace(".bin", "");
						if (row.s_is_ad == "Y") sheet.Cells[nRow, 2] = String.Format("{0} ~ {1}", TimeConverter.ConvertFromUTP(row.s_starttime).ToLocalTime().ToShortDateString(), TimeConverter.ConvertFromUTP(row.s_endtime).ToLocalTime().ToShortDateString());
                        else sheet.Cells[nRow, 2] = "-";
                        sheet.Cells[nRow, 3] = row.s_playtime;
                        sheet.Cells[nRow, 4] = ConvertPlayConstraitToString(row);

                        prev_uuid = row.s_id.ToString();
                    }

                    int nCnt = Convert.ToInt32(sheet.Cells[nRow, nCol + Convert.ToInt32(row.MM) - 1].Value2);
                    sheet.Cells[nRow, nCol + Convert.ToInt32(row.MM) - 1] = nCnt + Convert.ToInt32(row.CNT);
                }

                if (dt.Rows.Count > 0) nRow++;

                sheet.Rows[nRow].Delete();
                sheet.Rows[nRow].Delete();
            }
        }

        /// <summary>
        /// 타겟 광고 고객 반응 보고서를 그린다.
        /// </summary>
        private void DrawDailyTargetReport(Microsoft.Office.Interop.Excel.Worksheet sheet)
        {
            int nRow = 5;
            int nCol = 2;
            DateTime dtSelected = dtSelectDate.SelectedDateTime.Value;

            sheet.Cells[3, 1] = cbTargetGroup.Text;

            using (ServerDataSet.targetreportsDataTable dt = Config.GetConfig.ServerLogList.GetTargetReportDataByYYYYMMDD(dtSelected.Year, dtSelected.Month, dtSelected.Day, cbTargetGroup.Text))
            {
                if (dt == null) return;

                string prev_value = String.Empty;
                foreach (ServerDataSet.targetreportsRow row in dt.Rows)
                {
                    if (String.IsNullOrEmpty(prev_value))
                    {
                        #region 빈 로우 추가
                        /// 빈 로우 추가
                        Microsoft.Office.Interop.Excel.Range r1 = sheet.Rows[nRow] as Microsoft.Office.Interop.Excel.Range;
                        r1.Select();
                        r1.Copy();
                        sheet.Rows[nRow + 1].Insert(Microsoft.Office.Interop.Excel.XlInsertShiftDirection.xlShiftDown);
                        sheet.Rows[nRow + 1].PasteSpecial();
                        #endregion

                        sheet.Cells[nRow, 1] = row.r_value;

                        prev_value = row.r_value;
                    }
                    else if (!prev_value.Equals(row.r_value))
                    {
                        nRow++;

                        #region 빈 로우 추가
                        /// 빈 로우 추가
                        Microsoft.Office.Interop.Excel.Range r1 = sheet.Rows[nRow] as Microsoft.Office.Interop.Excel.Range;
                        r1.Select();
                        r1.Copy();
                        sheet.Rows[nRow + 1].Insert(Microsoft.Office.Interop.Excel.XlInsertShiftDirection.xlShiftDown);
                        sheet.Rows[nRow + 1].PasteSpecial();
                        #endregion

                        sheet.Cells[nRow, 1] = row.r_value;

                        prev_value = row.r_value;
                    }

                    sheet.Cells[nRow, nCol + Convert.ToInt32(row.H24)] = Convert.ToInt32(row.CNT);
                }

                if (dt.Rows.Count > 0) nRow++;

                sheet.Rows[nRow].Delete();
                sheet.Rows[nRow].Delete();
            }
        }

        /// <summary>
        /// 타겟 광고 고객 반응 보고서를 그린다.
        /// </summary>
        private void DrawMonthlyTargetReport(Microsoft.Office.Interop.Excel.Worksheet sheet)
        {
            int nRow = 5;
            int nCol = 2;
            
            sheet.Cells[3, 1] = cbTargetGroup.Text;
            int nDays = DateTime.DaysInMonth(Convert.ToInt32(cbSelectYear.Text), Convert.ToInt32(cbSelectMonth.Text));

            using (ServerDataSet.targetreportsDataTable dt = Config.GetConfig.ServerLogList.GetTargetReportDataByYYYYMM(Convert.ToInt32(cbSelectYear.Text), Convert.ToInt32(cbSelectMonth.Text), cbTargetGroup.Text))
            {
                if (dt == null) return;

                string prev_value = String.Empty;
                foreach (ServerDataSet.targetreportsRow row in dt.Rows)
                {
                    if (String.IsNullOrEmpty(prev_value))
                    {
                        #region 빈 로우 추가
                        /// 빈 로우 추가
                        Microsoft.Office.Interop.Excel.Range r1 = sheet.Rows[nRow] as Microsoft.Office.Interop.Excel.Range;
                        r1.Select();
                        r1.Copy();
                        sheet.Rows[nRow + 1].Insert(Microsoft.Office.Interop.Excel.XlInsertShiftDirection.xlShiftDown);
                        sheet.Rows[nRow + 1].PasteSpecial();
                        #endregion

                        sheet.Cells[nRow, 1] = row.r_value;

                        prev_value = row.r_value;
                    }
                    else if (!prev_value.Equals(row.r_value))
                    {
                        nRow++;

                        #region 빈 로우 추가
                        /// 빈 로우 추가
                        Microsoft.Office.Interop.Excel.Range r1 = sheet.Rows[nRow] as Microsoft.Office.Interop.Excel.Range;
                        r1.Select();
                        r1.Copy();
                        sheet.Rows[nRow + 1].Insert(Microsoft.Office.Interop.Excel.XlInsertShiftDirection.xlShiftDown);
                        sheet.Rows[nRow + 1].PasteSpecial();
                        #endregion

                        sheet.Cells[nRow, 1] = row.r_value;

                        prev_value = row.r_value;
                    }

                    int nCnt = Convert.ToInt32(sheet.Cells[nRow, nCol + Convert.ToInt32(row.DD) - 1].Value2);
                    sheet.Cells[nRow, nCol + Convert.ToInt32(row.DD) - 1] = nCnt + Convert.ToInt32(row.CNT);
                }

                if (dt.Rows.Count > 0) nRow++;

                sheet.Rows[nRow].Delete();
                sheet.Rows[nRow].Delete();

                for (int i = nDays; i < 31; ++i)
                {
                    sheet.Columns[nCol + nDays].Delete();
                }
            }
        }

        /// <summary>
        /// 타겟 광고 고객 반응 보고서를 그린다.
        /// </summary>
        private void DrawYearlyTargetReport(Microsoft.Office.Interop.Excel.Worksheet sheet)
        {
            int nRow = 5;
            int nCol = 2;
            
            sheet.Cells[3, 1] = cbTargetGroup.Text;
            
            using (ServerDataSet.targetreportsDataTable dt = Config.GetConfig.ServerLogList.GetTargetReportDataByYYYY(Convert.ToInt32(cbSelectYear.Text), cbTargetGroup.Text))
            {
                if (dt == null) return;

                string prev_value = String.Empty;
                foreach (ServerDataSet.targetreportsRow row in dt.Rows)
                {
                    if (String.IsNullOrEmpty(prev_value))
                    {
                        #region 빈 로우 추가
                        /// 빈 로우 추가
                        Microsoft.Office.Interop.Excel.Range r1 = sheet.Rows[nRow] as Microsoft.Office.Interop.Excel.Range;
                        r1.Select();
                        r1.Copy();
                        sheet.Rows[nRow + 1].Insert(Microsoft.Office.Interop.Excel.XlInsertShiftDirection.xlShiftDown);
                        sheet.Rows[nRow + 1].PasteSpecial();
                        #endregion

                        sheet.Cells[nRow, 1] = row.r_value;

                        prev_value = row.r_value;
                    }
                    else if (!prev_value.Equals(row.r_value))
                    {
                        nRow++;

                        #region 빈 로우 추가
                        /// 빈 로우 추가
                        Microsoft.Office.Interop.Excel.Range r1 = sheet.Rows[nRow] as Microsoft.Office.Interop.Excel.Range;
                        r1.Select();
                        r1.Copy();
                        sheet.Rows[nRow + 1].Insert(Microsoft.Office.Interop.Excel.XlInsertShiftDirection.xlShiftDown);
                        sheet.Rows[nRow + 1].PasteSpecial();
                        #endregion

                        sheet.Cells[nRow, 1] = row.r_value;

                        prev_value = row.r_value;
                    }

                    int nCnt = Convert.ToInt32(sheet.Cells[nRow, nCol + Convert.ToInt32(row.MM) - 1].Value2);
                    sheet.Cells[nRow, nCol + Convert.ToInt32(row.MM) - 1] = nCnt + Convert.ToInt32(row.CNT);
                }

                if (dt.Rows.Count > 0) nRow++;

                sheet.Rows[nRow].Delete();
                sheet.Rows[nRow].Delete();
            }
        }
       

        /// <summary>
        /// 현재 스케줄 현황
        /// </summary>
        private void DrawCurrentScheduleReport(Microsoft.Office.Interop.Excel.Worksheet sheet)
        {
            int nRow = 5;

            #region PlayerIDS
            Collection<String> arrPids = new Collection<string>();
            Collection<TreeViewItem> items = groupBox.CheckedItems;
            if (items == null)
            {
                MessageBox.Show(Properties.Resources.mbSelectPlayerOrGroup, Properties.Resources.titleMessageBox, MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            else
            {
                foreach (TreeViewItem item in items)
                {
                    XmlNode node = item.DataContext as XmlNode;

                    if (node.LocalName.Equals("player"))
                    {
                        if (!arrPids.Contains(node.Attributes["pid"].Value))
                            arrPids.Add(node.Attributes["pid"].Value);
                    }
                    else
                    {
                        foreach (XmlNode player in node.SelectNodes("descendant::player[attribute::del_yn=\"N\"]"))
                        {
                            if (!arrPids.Contains(player.Attributes["pid"].Value))
                                arrPids.Add(player.Attributes["pid"].Value);
                        }
                    }
                }
            }
            if (arrPids.Count == 0)
            {
                MessageBox.Show(Properties.Resources.mbSelectPlayerOrGroup, Properties.Resources.titleMessageBox, MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            #endregion

            long current = TimeConverter.ConvertToUTP(DateTime.UtcNow);
            using (ServerDataSet.tasksDataTable dt = Config.GetConfig.ServerTasksList.GetTasksFrom(current, current + 5))
            {
                foreach (String pid in arrPids)
                {
                    String PlayerName = ServerDataCache.GetInstance.GetPlayerName(pid);
                    XmlNode gnode = ServerDataCache.GetInstance.GetGroupByPID(pid);
                    int nCntRollingSched = 0;
                    int nCntTimeSched = 0;

                    int nCntRollingAd = 0;
                    int nCntTimeAd = 0;

                    if (gnode != null)
                    {
                        String GroupName = gnode.Attributes["name"].Value;

                        //	Server상의 모든 상위 gid를 뽑아온다.
                        Collection<String> arrServerGids = new Collection<String>();

                        string sReturn = Config.GetConfig.ServerGroupsList.GetGroupListByGID(gnode.Attributes["gid"].Value);

                        if (!String.IsNullOrEmpty(sReturn))
                        {
                            string[] arrReturns = sReturn.Split('|');

                            foreach (String sGid in arrReturns)
                            {
                                arrServerGids.Add(sGid);
                            }
                        }

                        foreach (ServerDataSet.tasksRow task in dt.Rows)
                        {
                            if (task.type == (int)TaskCommands.CmdProgram || task.type == (int)TaskCommands.CmdDefaultProgram)
                            {
                                if ((arrServerGids.Contains(task.gid) && String.IsNullOrEmpty(task.pid)) || task.pid == pid)
                                {
                                    if ((CheckContainTimeofDay(task.starttimeofday, task.endtimeofday)
                                        && CheckDayOfWeek(task.daysofweek)))
                                    {
                                        if (task.type == (int)TaskCommands.CmdProgram)
                                        {
                                            if (task.is_ad.Equals("Y"))
                                                nCntTimeAd++;
                                            else nCntTimeSched++;
                                        }
                                        else if (task.type == (int)TaskCommands.CmdDefaultProgram)
                                        {
                                            if (task.is_ad.Equals("Y"))
                                                nCntRollingAd++;
                                            else nCntRollingSched++;
                                        }
                                    }
                                }
                            }
                        }

                        /// 빈 로우 추가
                        Microsoft.Office.Interop.Excel.Range r1 = sheet.Rows[nRow] as Microsoft.Office.Interop.Excel.Range;
                        r1.Select();
                        r1.Copy();
                        sheet.Rows[nRow + 1].Insert(Microsoft.Office.Interop.Excel.XlInsertShiftDirection.xlShiftDown);
                        sheet.Rows[nRow + 1].PasteSpecial();

                        sheet.Cells[nRow, 1] = GroupName;
                        sheet.Cells[nRow, 2] = PlayerName;
						sheet.Cells[nRow, 3] = pid;
						sheet.Cells[nRow, 4] = nCntRollingSched;
						sheet.Cells[nRow, 5] = nCntRollingAd;
                        sheet.Cells[nRow, 6] = nCntTimeSched;
                        sheet.Cells[nRow, 7] = nCntTimeAd;

                        nRow++;
                    }

                }
            }

            sheet.Rows[nRow].Delete();
            sheet.Rows[nRow].Delete();

        }

        /// <summary>
        /// 요일 체크
        /// </summary>
        /// <param name="dayofweek"></param>
        /// <returns></returns>
        private bool CheckDayOfWeek(int dayofweek)
        {
            DayOfWeek dow = DateTime.Today.DayOfWeek;
            if (dow == DayOfWeek.Monday)
                return 0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_MON);
            else if (dow == DayOfWeek.Tuesday)
                return 0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_TUE);
            else if (dow == DayOfWeek.Wednesday)
                return 0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_WED);
            else if (dow == DayOfWeek.Thursday)
                return 0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_THU);
            else if (dow == DayOfWeek.Friday)
                return 0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_FRI);
            else if (dow == DayOfWeek.Saturday)
                return 0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_SAT);
            else if (dow == DayOfWeek.Sunday)
                return 0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_SUN);

            return false;
        }

        /// <summary>
        /// 현재 시간이 TimeOfDay에 포함되는지
        /// </summary>
        /// <param name="nStartTime"></param>
        /// <param name="nEndTime"></param>
        /// <returns></returns>
        private bool CheckContainTimeofDay(int nStartTime, int nEndTime)
        {
            //	둘다 같은 값이라면 Whole Day로 간주
            if (nStartTime == nEndTime) return true;

            DateTime dtLocal = DateTime.Now;
            int nCurr = dtLocal.Second + (dtLocal.Minute * 60) + (dtLocal.Hour * 3600);

            if (nStartTime < nEndTime)
            {
                //	시간대가 하루에 국한되는 경우

                return nCurr >= nStartTime && nCurr < nEndTime;
            }
            else
            {
                //	시간대가 오늘부터 다음 날로 넘어 간 경우
                return nCurr < nStartTime || nCurr >= nEndTime;
            }
        }

        /// <summary>
        /// 총 보고서 종류
        /// </summary>
        enum ReportType
        {
            /// <summary>
            /// 시간대별 재생 현황
            /// </summary>
            DailyPlayReport,
            /// <summary>
            /// 일별 재생 현황
            /// </summary>
            MonthlyPlayReport,
            /// <summary>
            /// 월별 재생 현황
            /// </summary>
            YearlyPlayReport,
            /// <summary>
            /// 시간대별 타겟 광고 고객 현황
            /// </summary>
            DailyTargetAdReport,
            /// <summary>
            /// 일별 타겟 광고 고객 현황
            /// </summary>
            MonthlyTargetAdReport,
            /// <summary>
            /// 월별 타겟 광고 고객 현황
            /// </summary>
            YearlyTargetAdReport,
            /// <summary>
            /// 일일 스케줄 현황
            /// </summary>
            DailyScheduleReport,
            /// <summary>
            /// 현재 스케줄 현황
            /// </summary>
            CurrentScheduleReport,
            /// <summary>
            /// DID 운영 보고서
            /// </summary>
            RunningReport,
            /// <summary>
            /// 플레이어 목록 보고서
            /// </summary>
            PlayerListReport,
			/// <summary>
			/// 현재 스케줄 목록 보고서
			/// </summary>
			ScheduleListReport,
            /// <summary>
            /// 일별 플레이어 상태 요약 보고서
            /// </summary>
            PlayerSummaryReport
        };

        /// <summary>
        /// 선택된 보고서 타입을 반환한다.
        /// </summary>
        /// <returns></returns>
        ReportType GetReportType()
        {
            switch (cbReportType.SelectedIndex)
            {
                case 0: /// 재생 통계
                    if (cbReportUnit.SelectedIndex == 0)
                        return ReportType.DailyPlayReport;
                    else if (cbReportUnit.SelectedIndex == 1)
                        return ReportType.MonthlyPlayReport;
                    else
                        return ReportType.YearlyPlayReport;
                case 1: /// 타겟 광고 고객 현황
                    if (cbReportUnit.SelectedIndex == 0)
                        return ReportType.DailyTargetAdReport;
                    else if (cbReportUnit.SelectedIndex == 1)
                        return ReportType.MonthlyTargetAdReport;
                    else
                        return ReportType.YearlyTargetAdReport;
                case 2: /// 일일 스케줄 현황
                    return ReportType.DailyScheduleReport;
                case 3: /// 현재 스케줄 현황
                    return ReportType.CurrentScheduleReport;
                case 4: /// DID 운영 현황
                    return ReportType.RunningReport;
                case 5: /// 플레이어 목록 현황
                    return ReportType.PlayerListReport;
				case 6: /// 현재 스케줄 목록 현황
					return ReportType.ScheduleListReport;
                case 7: /// 일별 플레이어 상태 요약 현황
                    return ReportType.PlayerSummaryReport;
            }

            return ReportType.RunningReport;
        }
	}
}
