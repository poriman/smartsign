﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Threading;
using System.Windows.Threading;
using DigitalSignage.Common;
using System.Collections.ObjectModel;
using System.Xml;
using System.Reflection;


namespace DigitalSignage.Scheduler
{
    /// <summary>
    /// ContentsUpDownDlg.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class ContentsUpDownDlg : Window
    {
        private string m_path;
        private Config m_config = null;
        private string m_userid = "";
        private String m_rowGroup = null;
        private Thread m_thProcess = null;
        private bool m_isUpload;
        private string m_contentsPath = "";
        private List<string> m_download_fileList;
        Scheduler.Template.Wizard.ContensUpDn UpandDownLoader = new DigitalSignage.Scheduler.Template.Wizard.ContensUpDn();

		/// <summary>
		/// 로드된 페이지 목록을 가져온다 
		/// </summary>
		public Collection<ScreenPropertyLoader> LoadedScreenProperties = null;

        public ContentsUpDownDlg(Config cfg, String group, string dir, string userid, bool isUpload, List<string> filelist)
        {
            m_config = cfg;
            m_rowGroup = group;
            m_path = dir;
            m_userid = userid;
            m_isUpload = isUpload;
            if (m_isUpload == true)
            {
                this.Title = Properties.Resources.titleContentsupload;
            }
            else
            {
                this.Title = Properties.Resources.titleContentsDownload;
                m_download_fileList = filelist;
                m_contentsPath = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\Contents\\";
            }
            UpandDownLoader.OnSetProgress = new EventHandler(InitialProgress);
            UpandDownLoader.OnStepIt = new EventHandler(StepIt);
            UpandDownLoader.OnUpdateLabel = new StatusEventHandler(UpdateLabel);
            UpandDownLoader.OnUpdateTitle += new StatusEventHandler(UpdateTitle);

            InitializeComponent();
        }

        void StepIt(Object obj, EventArgs e)
        {
            StepIt();
        }

        void InitialProgress(Object obj, EventArgs e)
        {
            FTPFunctions ftp = obj as FTPFunctions;
            if (ftp != null)
            {
                InitialProgress(ftp.ProgressCount);
            }
        }
        void UpdateLabel(Object obj, StatusEventArgs e)
        {
            if (e.OriginalSource != null)
            {
                UpdateLabel(e.OriginalSource as String);
            }
        }
        void UpdateTitle(Object obj, StatusEventArgs e)
        {
            if (e.OriginalSource != null)
            {
                UpdateTitle(e.OriginalSource as String);
            }
        }

        private void UploadContents()
        {
            try
            {
                bool bRetry = true;

                while (bRetry)
                {
                    using (new DigitalSignage.Common.WaitCursor())
                    {
                        UpandDownLoader.USER_ID = m_userid;
                        
                        if (false == UpandDownLoader.ContentsUpload(m_config, m_rowGroup, m_path))
                        {
                            MessageBoxResult result = MessageBox.Show(String.Format(Properties.Resources.mbErrorUpload, UpandDownLoader.LastErrorMessage), Properties.Resources.titleMessageBox, MessageBoxButton.YesNoCancel);

                            switch (result)
                            {
                                case MessageBoxResult.Yes:
                                    bRetry = true;
                                    break;
                                case MessageBoxResult.No:
                                    bRetry = false;
                                    break;
                                case MessageBoxResult.Cancel:

                                    bRetry = false;
                                    throw new Exception("CANCEL");
                                    //break;
                            }
                        }
                        else
                        {
                            break;
                        }
                    }
                }
                UpandDownLoader.RemoveTemporaryFiles();
                Finish(true);
            }
            catch (Exception e)
            {
                if (!e.Message.Equals("CANCEL"))
                {
                    MessageBox.Show(e.ToString());
                }
                UpandDownLoader.RemoveTemporaryFiles();
                Finish(false);
            }
        }

        private void DownloadContents()
        {
            try
            {
                bool bRetry = true;

                while (bRetry)
                {
                    using (new DigitalSignage.Common.WaitCursor())
                    {
                        UpandDownLoader.USER_ID = m_userid;
                        foreach (string file in m_download_fileList)
                        {
                            if (false == UpandDownLoader.ContentsDownload(m_config, m_rowGroup, m_contentsPath, file))
                            {
                                MessageBoxResult result = MessageBox.Show(String.Format(Properties.Resources.mbErrorUpload, UpandDownLoader.LastErrorMessage), Properties.Resources.titleMessageBox, MessageBoxButton.YesNoCancel);

                                switch (result)
                                {
                                    case MessageBoxResult.Yes:
                                        bRetry = true;
                                        break;
                                    case MessageBoxResult.No:
                                        bRetry = false;
                                        break;
                                    case MessageBoxResult.Cancel:

                                        bRetry = false;
                                        throw new Exception("CANCEL");
                                        break;
                                }
                            }
                        }
                        break;
                    }
                }
                UpandDownLoader.RemoveTemporaryFiles();
                Finish(true);
            }
            catch (Exception e)
            {
                if (!e.Message.Equals("CANCEL"))
                {
                    MessageBox.Show(e.ToString());
                }
                UpandDownLoader.RemoveTemporaryFiles();
                Finish(false);
            }
        }


        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (m_isUpload)
                m_thProcess = new Thread(this.UploadContents);
            else
                m_thProcess = new Thread(this.DownloadContents);

            m_thProcess.Start();
        }

        #region Async routines
        delegate void finishCB(bool bExitCode);
        public void Finish(bool bExitCode)
        {
            try
            {
                if (!this.Dispatcher.CheckAccess())
                {
                    this.Dispatcher.BeginInvoke(DispatcherPriority.Render,
                       new finishCB(Finish), bExitCode);
                }
                else
                {
                    this.DialogResult = bExitCode;
                    Close();
                }
            }
            catch (Exception e)
            {
                string errmsg = e.Message.ToString();
            }
        }

        delegate void InitialProgressCB(int nProgressCount);
        void InitialProgress(int nProgressCount)
        {
            try
            {
                if (!this.Dispatcher.CheckAccess())
                {
                    this.Dispatcher.BeginInvoke(DispatcherPriority.Render,
                       new InitialProgressCB(InitialProgress), nProgressCount);
                }
                else
                {
                    progressBarCopyProgress.Maximum = nProgressCount;
                    progressBarCopyProgress.Value = 0;
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        delegate void StepItCB();
        void StepIt()
        {
            try
            {
                if (!this.Dispatcher.CheckAccess())
                {
                    this.Dispatcher.BeginInvoke(DispatcherPriority.Render,
                       new StepItCB(StepIt));
                }
                else
                {
                    progressBarCopyProgress.Value++;
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }
        delegate void UpdateTitleLabelCB(String statusLabel);
        void UpdateTitle(String statusLabel)
        {
            try
            {
                if (!this.Dispatcher.CheckAccess())
                {
                    this.Dispatcher.BeginInvoke(DispatcherPriority.Render,
                       new UpdateTitleLabelCB(UpdateTitle), statusLabel);
                }
                else
                {
                    lbStatus.Content = statusLabel;
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }
        delegate void UpdateLabelCB(String statusLabel);
        void UpdateLabel(String statusLabel)
        {
            try
            {
                if (!this.Dispatcher.CheckAccess())
                {
                    this.Dispatcher.BeginInvoke(DispatcherPriority.Render,
                       new UpdateLabelCB(UpdateLabel), statusLabel);
                }
                else
                {
                    String content;
                    String[] sarray = statusLabel.Split('\\');
                    if (sarray.Length > 3)
                    {
                        content = sarray[0] + "\\..\\" + sarray[sarray.Length - 1];
                    }
                    else
                    {
                        content = statusLabel;
                    }

                    lbStatusContent.Content = content;
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }

        #endregion

        private void Window_Unloaded(object sender, RoutedEventArgs e)
        {
            try
            {
                if (m_thProcess != null && m_thProcess.IsAlive)
                    m_thProcess.Abort();
            }
            catch
            {
            }
        }
    }
}
