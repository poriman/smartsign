﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using DigitalSignage.Common;
using System.Collections.ObjectModel;
using System.Xml;
using System.IO;
using NLog;

namespace DigitalSignage.Scheduler
{
	/// <summary>
	/// Interaction logic for NPCUResult.xaml
	/// </summary>
	public partial class NPCUResult : Window
	{
		private static Logger logger = LogManager.GetCurrentClassLogger();
		XmlDocument xmlDoc = new XmlDocument();
		bool bIsLoaded = false;

		public NPCUResult(String sTitle, String sResponseXml)
		{
			InitializeComponent();

			try
			{
				this.Title = sTitle;

				xmlDoc.LoadXml(sResponseXml);
				bIsLoaded = true;
			}
			catch { bIsLoaded = false; }
		}
		private void btnOK_Click(object sender, RoutedEventArgs e)
        {
			this.DialogResult = true;
        }

		private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
			this.DialogResult = false;
		}

        
		private void Window_Loaded(object sender, RoutedEventArgs e)        
        {
			try
			{
				XmlNodeList list = xmlDoc.SelectNodes("//child::Response");
				lvResult.ItemsSource = list;
				int nSuccess = 0;
				int nFailed = 0;
				int nNotConn = 0;
				int nTimeout = 0;
				int nEtc = 0;

				foreach (XmlNode node in list)
				{
					try
					{
						int nCode = Convert.ToInt32(node.Attributes["res_cd"].Value);

						switch (nCode)
						{
							case 0:
								nSuccess++;
								break;
							case 1:
								nNotConn++;
								nFailed++;
								break;
							case 99:
								nFailed++;
								nTimeout++;
								break;
							default:
								nFailed++;
								nEtc++;
								break;
						}

						tbEtc.Text = nEtc.ToString();
						tbFailed.Text = nFailed.ToString();
						tbNotConn.Text = nNotConn.ToString();
						tbSuccess.Text = nSuccess.ToString();
						tbTimeout.Text = nTimeout.ToString();
						tbTotal.Text = list.Count.ToString();
					}
					catch { }
				}
				
				bIsLoaded = true;

			}
			catch { bIsLoaded = false; }
			if (!bIsLoaded)
				MessageBox.Show(Properties.Resources.mbNoResult, Properties.Resources.titlePowerManagement, MessageBoxButton.OK, MessageBoxImage.Information);
        }
		
		private void g_nmColumn_Click(object sender, RoutedEventArgs e)
		{

		}
		private void p_nmColumn_Click(object sender, RoutedEventArgs e)
		{

		}
		private void pidColumn_Click(object sender, RoutedEventArgs e)
		{

		}
		private void res_cdColumn_Click(object sender, RoutedEventArgs e)
		{

		}
	}
}
