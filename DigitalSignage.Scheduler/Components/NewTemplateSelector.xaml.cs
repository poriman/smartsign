﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DigitalSignage.Scheduler
{
	/// <summary>
	/// Interaction logic for NewTemplateSelector.xaml
	/// </summary>
	public partial class NewTemplateSelector : Window
	{
		public enum Template
		{
			Normal = 0,
			Vertical_1on1,
			Horizontal_1on1,
			Horizontal_1on2,
			Horizontal_2on1,
			Vertical_1on2,
			Vertical_2on1,
			Horizontal_2on2,
			Vertical_1on1on1,
			Horizontal_1on1on1
		}
		public NewTemplateSelector()
		{
			InitializeComponent();

			Select = this.panel.Children[0];
		}

		private double dresValue = (double)(3.0 / 4.0);
		private bool isInt = true;

		private Size _screensize = new Size(800, 600);
		public Size ScreenSize
		{
			get { return _screensize; }
			set { _screensize = value; }
		}

		private object _selected = null;
		private int _selectionIndex = 0;
		private object Select
		{
			set
			{
				int nIndex = 0;
				foreach(Border border in panel.Children)
				{
					if (border.Equals(value))
					{
						_selected = value;
						border.Background = (LinearGradientBrush)Application.Current.Resources["SelectionBrush"];
						_selectionIndex = nIndex;
					}
					else
						border.Background = (LinearGradientBrush)Application.Current.Resources["NormalBrush"];
					
					nIndex++;
				}
			}
		}
		public Template SelectedIndex
		{
			get { return (Template)_selectionIndex; }
		}

		private void resList_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			SetWidth();
		}

		private void IsConnect_Checked(object sender, RoutedEventArgs e)
		{
			if (this.IsConnect != null)
			{
				if (this.IsConnect.IsChecked == true)
					SetWidth();
			}
		}

		private void SetWidth()
		{
			if (resList == null)
				return;

			switch (resList.SelectedIndex)
			{
				case 0:
					dresValue = (double)(4.0 / 3.0);
					break;
				case 1:
					dresValue = (double)(3.0 / 4.0);
					break;
				case 2:
					dresValue = (double)(16.0 / 9.0);
					break;
				case 3:
					dresValue = (double)(9.0 / 16.0);
					break;
				default:
					dresValue = (double)(3.0 / 4.0);
					break;
			}

			if (this.heightBox != null && this.IsConnect.IsChecked == true)
			{
				try
				{
					this.heightBox.Text = ((int)(double.Parse(widthBox.Text) * dresValue)).ToString();
				}
				catch
				{}
			}
		}

		private void widthBox_TextChanged(object sender, TextChangedEventArgs e)
		{
			if (this.heightBox != null && this.IsConnect.IsChecked == true && isInt)
			{
				try
				{
					this.heightBox.Text = ((int)(double.Parse(widthBox.Text) * dresValue)).ToString();
				}
				catch
				{

				}
			}
		}

		private void widthBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
		{
			int digit = 0;
			if (!Int32.TryParse(e.Text, out digit))
			{
				e.Handled = true;
				isInt = false;
			}
			else
				isInt = true;
		}

		private void TemplateItem_Click(object sender, MouseButtonEventArgs e)
		{
			Select = sender;
		}

		private void btnOk_Click(object sender, RoutedEventArgs e)
		{

			try
			{
				ScreenSize = new Size(Convert.ToDouble(widthBox.Text), Convert.ToDouble(heightBox.Text));
				if (ScreenSize.Width == 0 || ScreenSize.Height == 0)
					throw new Exception();
			}
			catch
			{
				ScreenSize = new Size(800, 600);

			}
			this.DialogResult = true;
			this.Close();
		}

		private void btnCancel_Click(object sender, RoutedEventArgs e)
		{
			this.DialogResult = false;
			this.Close();
		}

		void tb_GotKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
		{
			try
			{
				TextBox tb = sender as TextBox;
				if (tb != null) tb.SelectAll();
				else
				{
					PasswordBox pb = sender as PasswordBox;
					pb.SelectAll();
				}

				e.Handled = true;
			}
			catch { }
		}
	}
}
