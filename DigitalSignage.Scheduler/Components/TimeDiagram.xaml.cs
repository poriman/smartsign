﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;

using DigitalSignage.Common;
using DigitalSignage.Controls;
using System.Collections.ObjectModel;
using System.Linq;
using DigitalSignage.ServerDatabase;
using System.Xml;

namespace DigitalSignage.Scheduler
{
    /// <summary>
    /// Interaction logic for TimeDiagram
    /// </summary>
    public partial class TimeDiagram : System.Windows.Controls.UserControl
	{
		const double GAP_OF_TASKS = 1.2;

		private object tree = null;
		private int cntPlayers = 1;

		private DateTime day;
		private ArrayList tasksList;
		private double hourLength;
		private TaskItem prevSelection = null;



        #region 조준영: 하나 이상 선택 삭제        
        private List<TaskItem> selectedTaskItems = new List<TaskItem>();
        #endregion

        /// <summary>
		/// 스케줄 모음
		/// </summary>
		Collection<TaskItem> arrDefaultTask = new Collection<TaskItem>();
		Collection<TaskItem> arrProgramTask = new Collection<TaskItem>();
		Collection<TaskItem> arrSubtitleTask = new Collection<TaskItem>();
		Collection<TaskItem> arrControlTask = new Collection<TaskItem>();
		private Collection<TaskItem> arrAllTasks = new Collection<TaskItem>();        
		
		public Collection<TaskItem> TaskItems
		{
			get { return arrAllTasks; }           
		}

        public ServerDataSet.downloadmasterDataTable DownloadInfoTable = null;
        public ServerDataSet.DownloadSummaryDataTable DownloadSummaryTable = null;

		#region Drag 관련
		private bool _dragStarted = false;
        DigitalSignage.Scheduler.DragAdorner dragAdorner = null;
		Point ptMouseDown;
		#endregion

		#region Events
		public delegate void SelectionChanged(object sender, Task newSelection);
		public event SelectionChanged OnSelectionChanged = null;

		public delegate void ItemDoubleclicked(object sender, Task selectedTask);
		public event ItemDoubleclicked OnItemDoubleClicked = null;

		public delegate void CallContextMenu(object sender, MouseButtonEventArgs e);
		public event CallContextMenu OnContextMenu = null;
		#endregion

		#region DependencyProperty

		public Boolean IsModified
		{
			get { return (Boolean)GetValue(IsModifiedProperty); }
			set { SetValue(IsModifiedProperty, value); }
		}

		public static readonly DependencyProperty IsModifiedProperty =
			DependencyProperty.Register("IsModified", typeof(Boolean), typeof(TimeDiagram),
			new UIPropertyMetadata((Boolean)false));



		#endregion

		#region restore GridLength

		RowDefinition rd_default = new RowDefinition();
		RowDefinition rd_time = new RowDefinition();
		RowDefinition rd_subtitle = new RowDefinition();
		RowDefinition rd_control = new RowDefinition();

		#endregion

		public TimeDiagram()
		{
			InitializeComponent();
			rd_default.MinHeight = rd_time.MinHeight = rd_subtitle.MinHeight = rd_control.MinHeight = 30;

			//	Data load
			rd_default.Height = new GridLength(Config.GetConfig.RepeatSchedValue, GridUnitType.Star);
			rd_time.Height = new GridLength(Config.GetConfig.TimeSchedValue, GridUnitType.Star);
			rd_subtitle.Height = new GridLength(Config.GetConfig.SubtitleSchedValue, GridUnitType.Star);
			rd_control.Height = new GridLength(Config.GetConfig.ControlSchedValue, GridUnitType.Star);
		}

		private Collection<Grid> arrBodyList = new Collection<Grid>();

		private void initializeUI()
		{
			arrBodyList.Clear();            

			bool bHasDefault = Config.GetConfig.IsShowRepeatationSchedule;
			bool bHasProgram = Config.GetConfig.IsShowTimeSchedule;
			bool bHasSubtitle = Config.GetConfig.IsAvailableSubtitleSchedule && Config.GetConfig.IsShowSubtitleSchedule;
			bool bHasControl = Config.GetConfig.IsAvailableControlSchedule && Config.GetConfig.IsShowControlSchedule;

			gridHeader.RowDefinitions.Clear();

			RowDefinition magin = new RowDefinition();
			magin.Height = new GridLength(30, GridUnitType.Pixel);
			gridHeader.RowDefinitions.Add(magin);

			int nCountRows = 1;

			if (bHasDefault)
			{
				edDefaultTask.Visibility = Visibility.Visible;
				arrBodyList.Add(edDefaultTask);
				gridHeader.RowDefinitions.Add(rd_default);
				Grid.SetRow(edDefaultTask, nCountRows);

				RowDefinition splitter = new RowDefinition();
				splitter.Height = new GridLength(3, GridUnitType.Pixel);
				gridHeader.RowDefinitions.Add(splitter);

				nCountRows += 2;
			}
			else
				edDefaultTask.Visibility = Visibility.Collapsed;

			if (bHasProgram)
			{
				edVideoTask.Visibility = Visibility.Visible;

				arrBodyList.Add(edVideoTask);
				gridHeader.RowDefinitions.Add(rd_time);
				Grid.SetRow(edVideoTask, nCountRows);

				RowDefinition splitter = new RowDefinition();
				splitter.Height = new GridLength(3, GridUnitType.Pixel);
				gridHeader.RowDefinitions.Add(splitter);

				nCountRows += 2;
			}
			else
				edVideoTask.Visibility = Visibility.Collapsed;

			if (bHasSubtitle)
			{
				edSubtileTask.Visibility = Visibility.Visible;

				arrBodyList.Add(edSubtileTask);
				gridHeader.RowDefinitions.Add(rd_subtitle);
				Grid.SetRow(edSubtileTask, nCountRows);

				RowDefinition splitter = new RowDefinition();
				splitter.Height = new GridLength(3, GridUnitType.Pixel);
				gridHeader.RowDefinitions.Add(splitter);

				nCountRows += 2;
			}
			else
				edSubtileTask.Visibility = Visibility.Collapsed;

			if (bHasControl)
			{
				edControlTask.Visibility = Visibility.Visible;

				arrBodyList.Add(edControlTask);

				gridHeader.RowDefinitions.Add(rd_control);
				Grid.SetRow(edControlTask, nCountRows);

				nCountRows += 2;
			}
			else
				edControlTask.Visibility = Visibility.Collapsed;

			RowDefinition bottommagin = new RowDefinition();
			bottommagin.MinHeight = bottommagin.MaxHeight = 20;
			bottommagin.Height = new GridLength(20, GridUnitType.Pixel);
			gridHeader.RowDefinitions.Add(bottommagin);

			UpdateLayout();

		}

		private bool prepareTasks(ArrayList tasks)
		{
			if (tasks == null) return false;

			arrDefaultTask.Clear();
			arrProgramTask.Clear();
			arrSubtitleTask.Clear();
			arrControlTask.Clear();          
			arrAllTasks.Clear();

			DateTime cDay = new DateTime(day.Year, day.Month, day.Day, 0, 0, 0);
			long daystart = TimeConverter.ConvertToUTP(cDay.ToUniversalTime());
			cDay = new DateTime(day.Year, day.Month, day.Day, 23, 59, 59);
			long dayend = TimeConverter.ConvertToUTP(cDay.ToUniversalTime());

			foreach (Task task in tasks)
			{
				// checking
				if ((task.TaskEnd < daystart) || (task.TaskStart > dayend)) continue;

				//	요일 체크
				if (!CheckDayOfWeek(task.daysofweek, cDay.DayOfWeek)) continue;


				TaskItem rect = null;

				if (task.type == TaskCommands.CmdProgram)
				{
					if (!Config.GetConfig.IsShowTimeSchedule) continue;

					rect = new TaskItem();
					arrProgramTask.Add(rect);
					arrAllTasks.Add(rect);

					rect.HorizontalItemChanged += new TaskItem.ItemChanged(OnTaskItemChanged);
					rect.VerticalDraggingStarted += new TaskItem.DraggingStarted(rect_VerticalDraggingStarted);
					
					canvasTimeSched.Children.Add(rect);
				}
				else if (task.type == TaskCommands.CmdSubtitles)
				{
					if (!Config.GetConfig.IsAvailableSubtitleSchedule || !Config.GetConfig.IsShowSubtitleSchedule)
						continue;

					rect = new TaskItem();
					arrSubtitleTask.Add(rect);
					arrAllTasks.Add(rect);

					rect.HorizontalItemChanged += new TaskItem.ItemChanged(OnTaskItemChanged);
					rect.VerticalDraggingStarted += new TaskItem.DraggingStarted(rect_VerticalDraggingStarted);
					
					canvasSubtitleSched.Children.Add(rect);
				}
                else if (task.type == TaskCommands.CmdDefaultProgram || task.type == TaskCommands.CmdTouchDefaultProgram)
                {
                    if (!Config.GetConfig.IsShowRepeatationSchedule) continue;

                    rect = new TaskItem();
                    arrDefaultTask.Add(rect);
					arrAllTasks.Add(rect);

					rect.IsEditableLeftSide = rect.IsEditableRightSide = false;
					
					canvasRollingSched.Children.Add(rect);
                }
                else if (task.type == TaskCommands.CmdControlSchedule)
				{
					if (!Config.GetConfig.IsAvailableControlSchedule || !Config.GetConfig.IsShowControlSchedule)
						continue;

					rect = new TaskItem();
					arrControlTask.Add(rect);
					arrAllTasks.Add(rect);

					if (task.IsVolumeControlTask)
					{
						rect.IsEditableLeftSide = rect.IsEditableRightSide = true;
						rect.HorizontalItemChanged += new TaskItem.ItemChanged(OnTaskItemChanged);
					}
					else rect.IsEditableLeftSide = rect.IsEditableRightSide = false;

					canvasControlSched.Children.Add(rect);
				}
				else
					continue;

				if (SelectedTask != null && task.uuid == SelectedTask.uuid)
				{
					prevSelection = rect;
                    rect.IsGroup = ((String.IsNullOrEmpty(task.pid) || task.pid == "-1"));
				}
				else
				{
					rect.DataContext = task;
					rect.IsGroup = ((String.IsNullOrEmpty(task.pid) || task.pid == "-1"));
				}

				rect.Border = Brushes.White;
				rect.Text = task.description;

				long taskStart = task.TaskStart;
				long taskEnd = task.TaskEnd;

				#region 시작 시간, 끝시간 체크
				if(task.starttimeofday != task.endtimeofday)
				{
					taskStart = taskStart > daystart + task.starttimeofday ? 
						taskStart : daystart + task.starttimeofday;

					taskEnd = taskEnd < daystart + task.endtimeofday ?
						taskEnd : daystart + task.endtimeofday;
				}
				#endregion


				if (taskStart < daystart)
				{
					taskStart = daystart - 900;
					rect.IsEditableLeftSide = false;
				}

				if (taskEnd > dayend)
				{
					taskEnd = dayend + 900;
					rect.IsEditableRightSide = false;
				}

				try
				{
					rect.Width = (taskEnd - taskStart) * hourLength / 3600;
				}
				catch { rect.Width = 20; }

				double left = (hourLength / 2) + (taskStart - daystart) * hourLength / 3600;
				Canvas.SetLeft(rect, left);

				//	Task별로 정보를 구성한다.
				XmlNode node = tree as XmlNode;

				if (node != null)
				{
					if (node.LocalName.Equals("player"))
					{
                        try
                        {
                            rect.tblSummaryDownloaded = DownloadSummaryTable.Single(row => row.uuid.Equals(task.uuid.ToString()));
                        }
                        catch
                        {
                            try
                            {
                                rect.tblDownloaded = (ServerDatabase.ServerDataSet.downloadmasterRow[])DownloadInfoTable.Select(String.Format("uuid = '{0}'", task.uuid.ToString()));
                            }
                            catch { rect.tblDownloaded = null; }
                        }

						rect.totalPlayers = 1;
					}
					else if (node.LocalName.Equals("group"))
					{
                        try
                        {
                            rect.tblSummaryDownloaded = DownloadSummaryTable.Single(row => row.uuid.Equals(task.uuid.ToString()));
                        }
                        catch
                        {

                            try
                            {
                                rect.tblDownloaded = (ServerDatabase.ServerDataSet.downloadmasterRow[])DownloadInfoTable.Select(String.Format("uuid = '{0}'", task.uuid.ToString()));
                            }
                            catch { rect.tblDownloaded = null; }
                        }

						rect.totalPlayers = cntPlayers;
					}

				}

				//	Task 데이터를 저장하고 Tooltip을 내부적으로 생성한다
				rect.DataContextWithMakeToolTip = task;
				//	10분을 움직이기위해 필요한 Pixel을 넣어준다
				rect.ValuePer10M = hourLength / 6;

				rect.PreviewMouseLeftButtonDown += new MouseButtonEventHandler(rectLeftMouseDown);
				rect.PreviewMouseLeftButtonUp += new MouseButtonEventHandler(rectLeftMouseUp);
				rect.PreviewMouseRightButtonDown += new MouseButtonEventHandler(rectLeftMouseDown);
				rect.MouseDoubleClick += new MouseButtonEventHandler(rect_MouseDoubleClick);
//				mainCanvas.Children.Add(rect);
			}

			return true;
		}

		void rect_MouseDoubleClick(object sender, MouseButtonEventArgs e)
		{
			if (!ServerDataCache.GetInstance.HasPermission("UPDATE SCHEDULE"))
				return;

			TaskItem item = sender as TaskItem;
			if(OnItemDoubleClicked != null)
			{
				OnItemDoubleClicked(sender, item == null ? null : item.DataContext as Task);
				e.Handled = true;
			}
		}


		/// <summary>
		/// 요일 체크
		/// </summary>
		/// <param name="dayofweek"></param>
		/// <param name="selectedDayOfWeek"></param>
		/// <returns></returns>
		private bool CheckDayOfWeek(int dayofweek, DayOfWeek selectedDayOfWeek)
		{
			DayOfWeek dow = selectedDayOfWeek;
			if (dow == DayOfWeek.Monday)
				return 0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_MON);
			else if (dow == DayOfWeek.Tuesday)
				return 0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_TUE);
			else if (dow == DayOfWeek.Wednesday)
				return 0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_WED);
			else if (dow == DayOfWeek.Thursday)
				return 0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_THU);
			else if (dow == DayOfWeek.Friday)
				return 0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_FRI);
			else if (dow == DayOfWeek.Saturday)
				return 0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_SAT);
			else if (dow == DayOfWeek.Sunday)
				return 0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_SUN);

			return false;
		}

		private void buildUI()
		{
			mainCanvas.Children.Clear(); // !!!!! this should be optimized
			canvasControlSched.Children.Clear();
			canvasTimeSched.Children.Clear();
			canvasRollingSched.Children.Clear();
			canvasSubtitleSched.Children.Clear();
			
			// 1. draw hour lines
			hourLength = (ActualWidth - 110 - 2) / 26;
			int lstep = 1;
			if (hourLength < 36) lstep = 2;
			if (hourLength < 18) lstep = 4;
			for (int i = 0; i < 25; i = i + lstep)
			{
				// line
				Line line = new Line();
				line.X1 = (hourLength / 2) + i * hourLength;
				line.X2 = line.X1;
				line.Y1 = 27 + this.Margin.Top;
				line.Y2 = 31 + this.Margin.Top/*ActualHeight - this.Margin.Bottom*/;
				line.Stroke = Brushes.Gray;
				line.StrokeThickness = 1.0;
				mainCanvas.Children.Add(line);
				// header
				TextBlock header = new TextBlock();
				header.Foreground = /*Brushes.White*/Brushes.Black;
				header.Text = /*(i < 10 ? "0" + i : i + "")*/ i + "시";
				Canvas.SetLeft(header, line.X1 - 18);
				Canvas.SetTop(header, 3 + Margin.Top);
				mainCanvas.Children.Add(header);
			}

			/// 현재 시간 조명
			DateTime now = DateTime.Now;

			if(this.Day.Year == now.Year &&
				this.Day.Month == now.Month &&
				this.Day.Day == now.Day)
			{
				AddHourLineInCanvas(now.Hour, now.Minute, ref canvasControlSched);
                AddHourLineInCanvas(now.Hour, now.Minute, ref canvasTimeSched);
                AddHourLineInCanvas(now.Hour, now.Minute, ref canvasRollingSched);
                AddHourLineInCanvas(now.Hour, now.Minute, ref canvasSubtitleSched);
			}			
			drawTasks(tasksList);
		}

		/// <summary>
        /// 현재 시간에 맞는 라인을 HIGHLIGHT 한다
		/// </summary>
		/// <param name="nHour"></param>
		/// <param name="nMinute"></param>
		/// <param name="canvas"></param>
		void AddHourLineInCanvas(int nHour, int nMinute, ref Canvas canvas)
		{
			int nCurrHour = nHour;

			//int nCurrMinute = DateTime.Now.Minute;

			Line currTimeline = new Line();
			currTimeline.Stroke = Brushes.Red;
			currTimeline.StrokeThickness = 2.0;
			currTimeline.Opacity = 0.5;

            double dleft = (hourLength / 2) + nCurrHour * hourLength + (nMinute * hourLength/60)/* + (nCurrMinute >= 30 ? (hourLength / 2) : 0)*/;

			currTimeline.X1 = dleft;
			currTimeline.X2 = dleft;
			currTimeline.Y1 = 0;
			currTimeline.Y2 = mainCanvas.ActualHeight;

			canvas.Children.Add(currTimeline);
		}


		private void drawTasks(ArrayList tasks)
		{
			if (tasks == null) return;

			try
			{
				//DateTime dtCurr = DateTime.Now;
				//TLB_default.SetCurrentTime(dtCurr.Hour, dtCurr.Minute);
				//TLB_time.SetCurrentTime(dtCurr.Hour, dtCurr.Minute);
				//TLB_subtitle.SetCurrentTime(dtCurr.Hour, dtCurr.Minute);
				//TLB_control.SetCurrentTime(dtCurr.Hour, dtCurr.Minute);

				if (prepareTasks(tasks))
				{
					arrangeDefaultTask();
					arrangeProgramTask();
					arrangeSubtitleTask();
					arrangeControlTask();

					drawProgramTaskDuplications();
				}

                if (prevSelection != null)
                {
                    Select(prevSelection);
                }
			}
            catch (Exception ex) { Console.WriteLine(ex.ToString()); }
		}

		public void OnTaskItemChanged(object sender, EventArgs e)
		{
			IsModified = true;

			try
			{
				switch ((sender as TaskItem).TaskType)
				{
					case TaskCommands.CmdProgram:
						arrangeProgramTask();
						drawProgramTaskDuplications();
						break;
					//case TaskCommands.CmdDefaultProgram:
					//case TaskCommands.CmdTouchDefaultProgram:
					//    arrangeDefaultTask();
					//break;
					case TaskCommands.CmdSubtitles:
						arrangeSubtitleTask();
						break;
					case TaskCommands.CmdControlSchedule:
						arrangeControlTask();
						break;
				}
			}
			catch {}

		}

		private void PriorityChanged(TaskItem task, Point pt)
		{
			try
			{
				if (task.TaskType == TaskCommands.CmdProgram)
				{
					pt = mainCanvas.TranslatePoint(pt, canvasTimeSched);

					#region 시간 스케줄 우선순위 정리
					int oldPrior = ((Task)task.DataContext).priority;

					Canvas.SetTop(task, pt.Y);
					
					var ordered = arrProgramTask.OrderBy(p => Canvas.GetTop(p));

					int nGroupPriority = 10000;
					int nUserPriority = 0;
					foreach (TaskItem item in ordered)
					{
						Task obj = item.DataContext as Task;
						if ((String.IsNullOrEmpty(obj.pid) || obj.pid.Equals("-1")))
						{
							nGroupPriority -= 100;
							nUserPriority = 0;
							obj.priority = nGroupPriority + nUserPriority;
						}
						else
						{
							--nUserPriority;
							obj.priority = nGroupPriority + nUserPriority;
						}
						this.IsModified = item.IsModified = true;
					}
					arrangeProgramTask();

					int newPrior = ((Task)task.DataContext).priority;

					//	예전과 비교해서 순서가 변경되었는지 검사
					if (this.IsModified == false && oldPrior == newPrior)
					{
						//	바뀌지 않음..
						foreach (TaskItem ti in arrProgramTask)
						{
							ti.IsModified = false;
						}

						this.IsModified = false;
					}
					else
						this.IsModified = true;
					#endregion
				}
				else if (task.TaskType == TaskCommands.CmdSubtitles)
				{
					pt = mainCanvas.TranslatePoint(pt, canvasSubtitleSched);

					#region 자막 스케줄 우선순위 정리
					int oldPrior = ((Task)task.DataContext).priority;

					Canvas.SetTop(task, pt.Y);

					var ordered = arrSubtitleTask.OrderBy(p => Canvas.GetTop(p));

					int nGroupPriority = 10000;
					int nUserPriority = 0;
					foreach (TaskItem item in ordered)
					{
						Task obj = item.DataContext as Task;
						if ((String.IsNullOrEmpty(obj.pid) || obj.pid.Equals("-1")))
						{
							nGroupPriority -= 100;
							nUserPriority = 0;
							obj.priority = nGroupPriority + nUserPriority;
						}
						else
						{
							--nUserPriority;
							obj.priority = nGroupPriority + nUserPriority;
						}
						this.IsModified = item.IsModified = true;
					}
					arrangeSubtitleTask();

					int newPrior = ((Task)task.DataContext).priority;

					//	예전과 비교해서 순서가 변경되었는지 검사
					if (this.IsModified == false && oldPrior == newPrior)
					{
						//	바뀌지 않음..
						foreach (TaskItem ti in arrSubtitleTask)
						{
							ti.IsModified = false;
						}

						this.IsModified = false;
					}
					else
						this.IsModified = true;
					#endregion
				}
				else if (task.TaskType == TaskCommands.CmdDefaultProgram || task.TaskType == TaskCommands.CmdTouchDefaultProgram)
				{
					pt = mainCanvas.TranslatePoint(pt, canvasRollingSched);
					
					#region 반복 스케줄 우선순위
					int oldPrior = ((Task)task.DataContext).priority;
					Canvas.SetTop(task, pt.Y);
					var ordered = arrDefaultTask.OrderBy(p => Canvas.GetTop(p));

					int nGroupPriority = 0;
					int nUserPriority = 0;
					foreach (TaskItem item in ordered)
					{
						Task obj = item.DataContext as Task;
						if ((String.IsNullOrEmpty(obj.pid) || obj.pid.Equals("-1")))
						{
							nGroupPriority -= 100;
							nUserPriority = 0;
							obj.priority = nGroupPriority + nUserPriority;
						}
						else
						{
							--nUserPriority;
							obj.priority = nGroupPriority + nUserPriority;
						}
						this.IsModified = item.IsModified = true;
					}
					arrangeDefaultTask();

					int newPrior = ((Task)task.DataContext).priority;

					//	예전과 비교해서 순서가 변경되었는지 검사
					if (this.IsModified == false && oldPrior == newPrior)
					{
						//	바뀌지 않음..
						foreach (TaskItem ti in arrDefaultTask)
						{
							ti.IsModified = false;
						}

						this.IsModified = false;
					}
					else
						this.IsModified = true;
					#endregion
				}
			}
			catch (Exception ee)
			{
				MessageBox.Show(ee.Message);
			}
		}
		//	그룹스케줄일경우 10단위로 변경된다.
		//	사용자 스케줄일경우 1단위로 변경된다.
		public void OnPriorityChanged(object sender, System.Windows.Controls.Primitives.DragCompletedEventArgs e)
		{
			TaskItem task = sender as TaskItem;

			Double oldTop = Canvas.GetTop(task);
			Point pt = new Point(Canvas.GetLeft(task), Canvas.GetTop(task) + e.VerticalChange);

			PriorityChanged(task, pt);

			e.Handled = true;
		}

		/// <summary>
		/// 
		/// </summary>
		private void arrangeDefaultTask()
		{
			if (arrDefaultTask.Count > 0)
			{

				var ordered = arrDefaultTask.OrderByDescending(p => ((Task)p.DataContext).priority);

				double dTaskHeight = 0;
				if (Config.GetConfig.UseScrollInTimeline) dTaskHeight = 25;
				else
					dTaskHeight = edDefaultTask.ActualHeight / Convert.ToDouble(arrDefaultTask.Count * GAP_OF_TASKS);
				
				int i = 0;
				double dMaxBottom = 0;
				foreach (TaskItem item in ordered)
				{
					double dTop = 5 + Convert.ToDouble(i) * dTaskHeight * GAP_OF_TASKS;
//					Canvas.SetTop(item, 35 + Convert.ToDouble(i) * dTaskHeight * GAP_OF_TASKS);
					Canvas.SetTop(item, dTop);
					item.Height = dTaskHeight;
					i++;

					if (dMaxBottom < dTop) dMaxBottom = dTop;
				}

				if (ordered.Count() > 0 && Config.GetConfig.UseScrollInTimeline)
				{
					canvasRollingSched.Height = 5 + dMaxBottom + dTaskHeight;
				}
				else canvasRollingSched.Height = Double.NaN;
			}
		}

		/// <summary>
		/// 중복 선을 그리기
		/// </summary>
		private void drawProgramTaskDuplications()
		{
			if (arrProgramTask.Count > 0)
			{
				var ordered = arrProgramTask.OrderByDescending(p => ((Task)p.DataContext).priority);

				#region 중복 범위 초기화
				foreach (TaskItem taskitem in ordered)
				{
					taskitem.Duplications.Clear();
				}
				#endregion

				#region 중복 범위 설정

				foreach (TaskItem taskitem in ordered)
				{
					Task task = taskitem.DataContext as Task;

					//	중복을 허용한다면 검색할 필요없음
					if (task.AllowDuplication) continue;

					double srcLeft = Canvas.GetLeft(taskitem);
					double srcRight = srcLeft + taskitem.Width;

					bool bPrev = true;

					foreach (TaskItem comp in ordered)
					{
						if (comp.Equals(taskitem))
						{
							bPrev = false;
							continue;
						}


						Task comptask = comp.DataContext as Task;

						if (bPrev && !comptask.AllowDuplication) continue;

						if (task.HasDuplicationTime(comptask, this.day))
						{
							double descLeft = Canvas.GetLeft(comp);
							double descRight = descLeft + comp.Width;

							comp.Duplications.Add(new TaskItem.LineScope(Math.Max(srcLeft, descLeft) - descLeft,
								Math.Min(srcRight, descRight) - descLeft));
						}

						comp.RedrawDuplications();
					}

				}
				#endregion
			}
		}

		/// <summary>
		/// 
		/// </summary>
		private void arrangeProgramTask()
		{
			if (arrProgramTask.Count > 0)
			{
				var ordered = arrProgramTask.OrderByDescending(p => ((Task)p.DataContext).priority);

				#region Top Index 설정
				Collection<int> arrTopIndex = new Collection<int>();

				int nIndex = 0;
				foreach (TaskItem taskitem in ordered)
				{
					Task task = taskitem.DataContext as Task;
					int nDuplicationCount = 0;

					int nCount = 0;
					foreach (TaskItem comp in ordered)
					{
						if (comp.Equals(taskitem))
						{
							arrTopIndex.Add(nDuplicationCount);
							nCount++;
							break;
						}

						Task comptask = comp.DataContext as Task;

						if (task.HasDuplicationTime(comptask, this.day))
						{
							if(nDuplicationCount < arrTopIndex[nCount] + 1)
								nDuplicationCount = arrTopIndex[nCount] + 1;
						}
						nCount++;
					}

					if (nDuplicationCount > nIndex) nIndex = nDuplicationCount;
				}

				nIndex++;

				#endregion

				double dTaskHeight = 0;

				if (Config.GetConfig.UseScrollInTimeline) dTaskHeight = 25;
				else dTaskHeight = edVideoTask.ActualHeight / Convert.ToDouble(nIndex * GAP_OF_TASKS);

				double dMaxBottom = 0;
				int i = 0;
				foreach (TaskItem item in ordered)
				{
					Task task = item.DataContext as Task;

					double dTop = 5 + Convert.ToDouble(arrTopIndex[i]/*arrSep.IndexOf(String.Format(task.gid + "|" + task.pid))*/) * dTaskHeight * GAP_OF_TASKS;
//					Canvas.SetTop(item, 35 + 4 + edDefaultTask.ActualHeight + Convert.ToDouble(arrTopIndex[i]/*arrSep.IndexOf(String.Format(task.gid + "|" + task.pid))*/) * dTaskHeight * GAP_OF_TASKS);
					Canvas.SetTop(item, dTop);
					item.Height = dTaskHeight;
					i++;

					if (dMaxBottom < dTop) dMaxBottom = dTop;
				}

				if (ordered.Count() > 0 && Config.GetConfig.UseScrollInTimeline)
				{
					canvasTimeSched.Height = 5 + dMaxBottom + dTaskHeight;
				}
				else canvasTimeSched.Height = Double.NaN;
				/*

				var ordered = arrProgramTask.OrderByDescending(p => ((Task)p.DataContext).priority);

				double dTaskHeight = edVideoTask.ActualHeight / Convert.ToDouble(arrProgramTask.Count * GAP_OF_TASKS);
				int i = 0;
				foreach (TaskItem item in ordered)
				{
					Canvas.SetTop(item, 35 + 4 + edDefaultTask.ActualHeight + Convert.ToDouble(i) * dTaskHeight * GAP_OF_TASKS);
					item.Height = dTaskHeight;
					i++;
				}
				*/

			}
		}
		/// <summary>
		/// 
		/// </summary>
		private void arrangeSubtitleTask()
		{
			if (arrSubtitleTask.Count > 0)
			{
				var ordered = arrSubtitleTask.OrderByDescending(p => ((Task)p.DataContext).priority);

				#region Top Index 설정
				Collection<int> arrTopIndex = new Collection<int>();

				int nIndex = 0;
				foreach (TaskItem taskitem in ordered)
				{
					Task task = taskitem.DataContext as Task;
					int nDuplicationCount = 0;

					int nCount = 0;
					foreach (TaskItem comp in ordered)
					{
						if (comp.Equals(taskitem))
						{
							arrTopIndex.Add(nDuplicationCount);
							nCount++;
							break;
						}

						Task comptask = comp.DataContext as Task;

						if (task.HasDuplicationTime(comptask, this.day))
						{
							if (nDuplicationCount < arrTopIndex[nCount] + 1)
								nDuplicationCount = arrTopIndex[nCount] + 1;
						}
						nCount++;
					}

					if (nDuplicationCount > nIndex) nIndex = nDuplicationCount;
				}

				nIndex++;

				#endregion

				double dTaskHeight = 0;
				
				if (Config.GetConfig.UseScrollInTimeline) dTaskHeight = 25;
				else dTaskHeight = edSubtileTask.ActualHeight / Convert.ToDouble(nIndex * GAP_OF_TASKS);

				double dMaxBottom = 0;
				int i = 0;
				foreach (TaskItem item in ordered)
				{
					Task task = item.DataContext as Task;

					double dTop = 5 + Convert.ToDouble(arrTopIndex[i]/*arrSep.IndexOf(String.Format(task.gid + "|" + task.pid))*/) * dTaskHeight * GAP_OF_TASKS;
//					Canvas.SetTop(item, 35 + 4 + 4 + edDefaultTask.ActualHeight + edVideoTask.ActualHeight + Convert.ToDouble(arrTopIndex[i]/*arrSep.IndexOf(String.Format(task.gid + "|" + task.pid))*/) * dTaskHeight * GAP_OF_TASKS);
					Canvas.SetTop(item, dTop);
					item.Height = dTaskHeight;
					i++;

					if (dMaxBottom < dTop) dMaxBottom = dTop;
				}

				if (ordered.Count() > 0 && Config.GetConfig.UseScrollInTimeline)
				{
					canvasSubtitleSched.Height = 5 + dMaxBottom + dTaskHeight;
				}
				else canvasSubtitleSched.Height = Double.NaN;
				/*
				var ordered = arrSubtitleTask.OrderByDescending(p => ((Task)p.DataContext).priority);

				double dTaskHeight = edSubtileTask.ActualHeight / Convert.ToDouble(arrSubtitleTask.Count * GAP_OF_TASKS);
				int i = 0;
				foreach (TaskItem item in ordered)
				{
					Canvas.SetTop(item, 35 + 4 + 4 + edDefaultTask.ActualHeight + edVideoTask.ActualHeight + Convert.ToDouble(i) * dTaskHeight * GAP_OF_TASKS);
					item.Height = dTaskHeight;
					i++;
				}
				*/
			}
		}

		private void arrangeControlTask()
		{
			if (arrControlTask.Count > 0)
			{
				var ordered = arrControlTask.OrderByDescending(p => ((Task)p.DataContext).priority);

				#region Top Index 설정
				Collection<int> arrTopIndex = new Collection<int>();

				int nIndex = 0;
				foreach (TaskItem taskitem in ordered)
				{
					Task task = taskitem.DataContext as Task;
					int nDuplicationCount = 0;

					int nCount = 0;
					foreach (TaskItem comp in ordered)
					{
						if (comp.Equals(taskitem))
						{
							arrTopIndex.Add(nDuplicationCount);
							nCount++;
							break;
						}

						Task comptask = comp.DataContext as Task;

						if (task.HasDuplicationTime(comptask, this.day))
						{
							if (nDuplicationCount < arrTopIndex[nCount] + 1)
								nDuplicationCount = arrTopIndex[nCount] + 1;
						}
						nCount++;
					}

					if (nDuplicationCount > nIndex) nIndex = nDuplicationCount;
				}

				nIndex++;

				#endregion

				double dTaskHeight = 0;

				if (Config.GetConfig.UseScrollInTimeline) dTaskHeight = 25;
				else dTaskHeight = edControlTask.ActualHeight / Convert.ToDouble(nIndex * GAP_OF_TASKS);

				double dMaxBottom = 0;
				int i = 0;
				foreach (TaskItem item in ordered)
				{
					Task task = item.DataContext as Task;
					double dTop = 5 + Convert.ToDouble(arrTopIndex[i]/*arrSep.IndexOf(String.Format(task.gid + "|" + task.pid))*/) * dTaskHeight * GAP_OF_TASKS;
//					Canvas.SetTop(item, 35 + 4 + 4 + 4 + edDefaultTask.ActualHeight + edVideoTask.ActualHeight + edSubtileTask.ActualHeight + Convert.ToDouble(arrTopIndex[i]/*arrSep.IndexOf(String.Format(task.gid + "|" + task.pid))*/) * dTaskHeight * GAP_OF_TASKS);
					Canvas.SetTop(item, dTop);
					item.Height = dTaskHeight;
					i++;

					if (dMaxBottom < dTop) dMaxBottom = dTop;

				}

				if (ordered.Count() > 0 && Config.GetConfig.UseScrollInTimeline)
				{
					canvasControlSched.Height = 5 + dMaxBottom + dTaskHeight;
				}
				else canvasControlSched.Height = Double.NaN;

				/*
				var ordered = arrControlTask.OrderByDescending(p => ((Task)p.DataContext).priority);

				double dTaskHeight = edControlTask.ActualHeight / Convert.ToDouble(arrControlTask.Count * GAP_OF_TASKS);
				int i = 0;
				foreach (TaskItem item in ordered)
				{
					Canvas.SetTop(item, 35 + 4 + 4 + 4 + edDefaultTask.ActualHeight + edVideoTask.ActualHeight + edSubtileTask.ActualHeight + Convert.ToDouble(i) * dTaskHeight * GAP_OF_TASKS);
					item.Height = dTaskHeight;
					i++;
				}
				*/
			}
		}

		private void mainCanvas_Loaded(object sender, RoutedEventArgs e)
		{
			UpdateLayout();
			initializeUI();
			buildUI();
		}

		public void Rebuild()
		{
			initializeUI();
			buildUI();
			if (prevSelection != null)
			{
				Task selected = prevSelection.DataContext as Task;
				prevSelection.IsGroup = (String.IsNullOrEmpty(selected.pid) || selected.pid == "-1");



                #region 조준영: 하나 이상 선택 삭제
                foreach (TaskItem titem in this.selectedTaskItems)
                {
                    Task task = titem.DataContext as Task;
                    titem.IsGroup = (String.IsNullOrEmpty(task.pid) || task.pid == "-1");
                }
                this.selectedTaskItems.Clear();
                #endregion

                Deselect();

			}
		}

        /// <summary>
        /// 선택
        /// </summary>
        /// <param name="item"></param>
        void Select(TaskItem item)
        {
            if ((Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control)
            {
                item.Select(item);
            }
            else
            {
                foreach (TaskItem i in arrAllTasks)
                {
                    i.Select(item);
                }
            }

            if (OnSelectionChanged != null)
                OnSelectionChanged(this, null);

			System.Windows.Forms.Application.DoEvents();
        }

        void Deselect()
        {
            foreach (TaskItem t in arrAllTasks)
            {
                t.Deselect();
            }
            prevSelection = null;

            if (OnSelectionChanged != null)
                OnSelectionChanged(this, null);

			System.Windows.Forms.Application.DoEvents();
        }

		#region events handlers
		private void rectLeftMouseDown(object sender, MouseButtonEventArgs args)
		{
			try
			{
				if (args != null && args.ClickCount < 2)
				{

					TaskItem rect = sender as TaskItem;
					if (rect == null)
					{
						return;
					}

					#region 조준영 : 하나 이상 선택 삭제
					if ((Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control)
					{
						if (this.selectedTaskItems.Contains(rect) == true)
						{
							Task curr = (Task)rect.DataContext;
							rect.IsGroup = (String.IsNullOrEmpty(curr.pid) || curr.pid == "-1");
							this.selectedTaskItems.Remove(rect);
							return;
						}

					}
					else
					{
						if (args.RightButton == MouseButtonState.Pressed)
						{
							if (this.selectedTaskItems.Contains(rect) == true)
							{
								return;
							}
						}

						if (prevSelection != null)
						{
                            /* 원본 code.
                            Task curr = (Task)prevSelection.DataContext;
                            // 				prevSelection.Background = (String.IsNullOrEmpty(curr.pid) || curr.pid == "-1") ? Brushes.Tomato : Brushes.RoyalBlue;
                            prevSelection.IsGroup = (String.IsNullOrEmpty(curr.pid) || curr.pid == "-1");
                            */
                            foreach (TaskItem item in this.selectedTaskItems)
							{
								Task curr = (Task)item.DataContext;
								item.IsGroup = (String.IsNullOrEmpty(curr.pid) || curr.pid == "-1");
							}
							this.selectedTaskItems.Clear();
						}
					}
					#endregion

                    Select(rect);

					prevSelection = rect;
					this.selectedTaskItems.Add(rect);//조준영 : 하나 이상 선택 삭제

					if (OnSelectionChanged != null)
						OnSelectionChanged(this, (Task)rect.DataContext);

					if (args.RightButton == MouseButtonState.Pressed)
					{
						args.Handled = true;
						return;
					}

					if (SelectedTask.type == TaskCommands.CmdDefaultProgram || SelectedTask.type == TaskCommands.CmdTouchDefaultProgram)
					{
						_dragStarted = true;
						this.ptMouseDown = MouseUtilities.GetMousePosition(this);
						if (ServerDataCache.GetInstance.HasPermission("UPDATE SCHEDULE"))
							prepareDrag(rect);
					}
					else {

						if (SelectedTask.type == TaskCommands.CmdProgram || SelectedTask.type == TaskCommands.CmdSubtitles)
						{
							this.ptMouseDown = MouseUtilities.GetMousePosition(this);
							//					if (ServerDataCache.GetInstance.HasPermission("UPDATE SCHEDULE"))
							//						prepareDrag(rect);
						}
						return;
					}

					
				}
				else if (args != null && args.ClickCount >= 2)
				{
					rect_MouseDoubleClick(sender, args);
				}

			}
			catch 
			{
			}

			args.Handled = true;
			
		}

		void rect_VerticalDraggingStarted(object sender, EventArgs e)
		{
			TaskItem rect = sender as TaskItem;

			if (_dragStarted == false && rect != null)
			{
				_dragStarted = true;

				if (ServerDataCache.GetInstance.HasPermission("UPDATE SCHEDULE"))
					prepareDrag(rect);
			}
		}

		private void rectLeftMouseUp(object sender, MouseButtonEventArgs args)
		{
			if (args != null && args.ClickCount < 2)
			{
				_dragStarted = false;

				args.Handled = true;
			}
		}

		private void rectMouseLeave(object sender, MouseEventArgs args)
		{
			try
			{
				TaskItem rect = sender as TaskItem;
				if (rect == null) return;
				if (prevSelection != null)
					if (prevSelection != null)
					{
						Task curr = (Task)prevSelection.DataContext;
						prevSelection.IsGroup = (String.IsNullOrEmpty(curr.pid) || curr.pid == "-1");
                        // 					prevSelection.Background = (String.IsNullOrEmpty(curr.pid) || curr.pid == "-1") ? Brushes.Tomato : Brushes.RoyalBlue;

					}

                Deselect();
			}
			catch 
			{
			}

			args.Handled = true;

		}

		private void mainCanvas_SizeChanged(object sender, SizeChangedEventArgs e)
		{
			buildUI();
		}


		private void TimeDiagram_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
		{
			if (OnContextMenu != null) OnContextMenu(sender, e);

			e.Handled = true;
		}

		#endregion

		#region properties

		public object SelectedTreeObject
		{
			get { return tree; }
			set
			{
				tree = value;

				try
				{
					XmlNode group = tree as XmlNode;
					if (null != group && group.LocalName.Equals("group"))
					{
						cntPlayers = group.SelectNodes("descendant::player[attribute::del_yn=\"N\"]").Count;
					}
					else
						cntPlayers = 1;
				}
				catch
				{
					cntPlayers = 0;
				}
			}
		}
		public DateTime Day
		{
			get { return day; }
			set { day = value; }
		}

		public ArrayList Tasks
		{
			get { return tasksList; }
			set
			{
				tasksList = value;
				IsModified = false;
			}
		}

		public Task SelectedTask
		{
			get
			{
				if (prevSelection == null)
					return null;
				return (Task)prevSelection.DataContext;
			}
		}

        //조준영 : 하나 이상 선택 삭제
        public List<Task> SelectedTasks
        {
            get
            {
                if (this.selectedTaskItems.Count == 0)
                    return null;

                return this.selectedTaskItems.Select(o => (Task)o.DataContext).ToList();
            }
        }
		#endregion

		#region Drag&Drop

		private void prepareDrag(TaskItem rect)
		{
			dragAdorner = new DragAdorner(this, rect.RenderSize, new VisualBrush(rect));
			dragAdorner.Opacity = 0.7;
			AdornerLayer layer = AdornerLayer.GetAdornerLayer(this);
			layer.Add(dragAdorner);

			DragDropEffects allowedEffects = DragDropEffects.Move | DragDropEffects.Link;
			DragDrop.DoDragDrop(this, rect, allowedEffects);
			System.Windows.Forms.Application.DoEvents();

			if (layer != null)
			{
				try
				{
					layer.Remove(dragAdorner);
					dragAdorner = null;
				}
				catch
				{ }
			}
		}
		private void UserControl_DragEnter(object sender, DragEventArgs e)
		{
			if (this.dragAdorner != null)
				this.dragAdorner.Visibility = Visibility.Visible;

			UpdateDragAdornerLocation();

			e.Handled = true;
		}

		private void UserControl_DragLeave(object sender, DragEventArgs e)
		{
			if (this.dragAdorner != null)
				this.dragAdorner.Visibility = Visibility.Collapsed;
			e.Handled = true;
		}

		private void UserControl_DragOver(object sender, DragEventArgs e)
		{
			e.Effects = DragDropEffects.Move;
			UpdateDragAdornerLocation();
			e.Handled = true;

		}
		private void UserControl_Drop(object sender, DragEventArgs e)
		{
			e.Effects = DragDropEffects.None;

			if (!e.Data.GetDataPresent(typeof(TaskItem)))
			{
				_dragStarted = false;
				e.Handled = true;
				return;
			}

			TaskItem data = e.Data.GetData(typeof(TaskItem)) as TaskItem;
			if (data == null)
			{
				_dragStarted = false;
				e.Handled = true;
				return;
			}
			
			PriorityChanged(data, new Point(this.dragAdorner.OffsetLeft, this.dragAdorner.OffsetTop)/*e.GetPosition(mainCanvas)*/);
			_dragStarted = false;
			e.Handled = true;
		}

		private void UpdateDragAdornerLocation()
		{
			try
			{
				if (dragAdorner != null)
				{
					Point ptCursor = MouseUtilities.GetMousePosition(this);


					// 4/13/2007 - Made the top offset relative to the item being dragged.
					TaskItem item = prevSelection;
					Point itemLoc = item.TranslatePoint(new Point(0, 0), this);

					if (item.TaskType == TaskCommands.CmdProgram)
					{
						double left = itemLoc.X;
						double top = itemLoc.Y + ptCursor.Y - this.ptMouseDown.Y;

						Point limit = edVideoTask.TranslatePoint(new Point(0, 0), this);

						if (limit.Y - 10 > top)
							top = limit.Y - 10;
						if (limit.Y + edVideoTask.ActualHeight + 10 < top)
							top = limit.Y + edVideoTask.ActualHeight + 10;

						this.dragAdorner.SetOffsets(left, top);
					}
					else if (item.TaskType == TaskCommands.CmdSubtitles)
					{
						double left = itemLoc.X;
						double top = itemLoc.Y + ptCursor.Y - this.ptMouseDown.Y;

						Point limit = edSubtileTask.TranslatePoint(new Point(0, 0), this);

						if (limit.Y - 10 > top)
							top = limit.Y - 10;
						if (limit.Y + edSubtileTask.ActualHeight + 10 < top)
							top = limit.Y + edSubtileTask.ActualHeight + 10;

						this.dragAdorner.SetOffsets(left, top);
					}
					else
					{

						double left = itemLoc.X;
						double top = itemLoc.Y + ptCursor.Y - this.ptMouseDown.Y;

						Point limit = edDefaultTask.TranslatePoint(new Point(0, 0), this);

						if (limit.Y - 10 > top)
							top = limit.Y - 10;
						if (limit.Y + edDefaultTask.ActualHeight + 10 < top)
							top = limit.Y + edDefaultTask.ActualHeight + 10;

						this.dragAdorner.SetOffsets(left, top);
					}
				}
			}
			catch { }
		}
		#endregion

		private void Body_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{
            if (e.ClickCount == 2)
            {
                RowDefinition def = gridHeader.RowDefinitions[Grid.GetRow(sender as Grid)];

                if (def.Height.Value == 85)
                {
                    foreach (Grid item in arrBodyList)
                    {
                        RowDefinition def_item = gridHeader.RowDefinitions[Grid.GetRow(item)];
                        def_item.Height = new GridLength(10, GridUnitType.Star);
                    }
                }
                else
                {
                    foreach (Grid item in arrBodyList)
                    {
                        RowDefinition def_item = gridHeader.RowDefinitions[Grid.GetRow(item)];
                        def_item.Height = new GridLength(10, GridUnitType.Star);
                    }

                    def.Height = new GridLength(85, GridUnitType.Star);

                }

                UpdateLayout();
                buildUI();
            }
            else
            {
                Deselect();

                selectedTaskItems.Clear();
            }
			e.Handled = true;
		}

		private void GridSplitter_DragDelta(object sender, System.Windows.Controls.Primitives.DragDeltaEventArgs e)
		{
			UpdateLayout();
			buildUI();
			e.Handled = true;
		}

		public void SaveConfig()
		{
			//	Data save
			Config.GetConfig.RepeatSchedValue = rd_default.Height.Value;
			Config.GetConfig.TimeSchedValue = rd_time.Height.Value;
			Config.GetConfig.SubtitleSchedValue = rd_subtitle.Height.Value;
			Config.GetConfig.ControlSchedValue = rd_control.Height.Value;
		}
	}
}