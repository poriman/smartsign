﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigitalSignage.Scheduler
{
	static class VNCLauncher
	{
		/// <summary>
		/// VNC Viewer 실행
		/// </summary>
		/// <param name="ip"></param>
		/// <param name="port"></param>
		/// <param name="auth_password"></param>
		/// <param name="bToolbar"></param>
		/// <param name="bStatusbar"></param>
		/// <param name="bViewOnly"></param>
		/// <param name="bContextMenu"></param>
		/// <param name="percentOfClientViewer"></param>
		/// <returns></returns>
		static public System.Diagnostics.Process RunVncViewer(string ip, string port, string auth_password, bool bToolbar, bool bStatusbar, bool bViewOnly, bool bContextMenu, int percentOfClientViewer)
		{

			String sFilePath = AppDomain.CurrentDomain.BaseDirectory;
			System.Diagnostics.ProcessStartInfo psi = new System.Diagnostics.ProcessStartInfo();
			psi.FileName = sFilePath + @"\vncviewer.exe";
			psi.UseShellExecute = true;
			psi.WorkingDirectory = sFilePath;
			psi.Arguments = String.Format("-connect {0}::{1} -password {2} -quickoption 3 -nohotkeys", ip, port, auth_password);
			//			-quality 0 -nohotkeys -noauto
			// 			" -quality 0 -autoacceptincoming -nohotkeys"
			if (!bToolbar) psi.Arguments += " -notoolbar";
			if (!bStatusbar) psi.Arguments += " -nostatus";
			if (bViewOnly) psi.Arguments += " -viewonly";
			if (!bContextMenu) psi.Arguments += " -restricted";

			psi.Arguments += String.Format(" -scale {0}/100", percentOfClientViewer > 100 ? 100 : percentOfClientViewer);

			return System.Diagnostics.Process.Start(psi);
		}

        /// <summary>
        /// VNC Viewer 실행
        /// </summary>
        /// <param name="connid"></param>
        /// <param name="ip"></param>
        /// <param name="port"></param>
        /// <param name="auth_password"></param>
        /// <param name="bToolbar"></param>
        /// <param name="bStatusbar"></param>
        /// <param name="bViewOnly"></param>
        /// <param name="bContextMenu"></param>
        /// <param name="percentOfClientViewer"></param>
        /// <returns></returns>
        static public System.Diagnostics.Process RunVncViewer(string connid, string ip, string port, string auth_password, bool bToolbar, bool bStatusbar, bool bViewOnly, bool bContextMenu, int percentOfClientViewer)
        {

            String sFilePath = AppDomain.CurrentDomain.BaseDirectory;
            System.Diagnostics.ProcessStartInfo psi = new System.Diagnostics.ProcessStartInfo();
            psi.FileName = sFilePath + @"\vncviewer.exe";
            psi.UseShellExecute = true;
            psi.WorkingDirectory = sFilePath;
            psi.Arguments = String.Format("-connect ID:{0} -proxy {1}:{2} -quickoption 3 -nohotkeys", connid, ip, port);
            //			-quality 0 -nohotkeys -noauto
            // 			" -quality 0 -autoacceptincoming -nohotkeys"
            if (!bToolbar) psi.Arguments += " -notoolbar";
            if (!bStatusbar) psi.Arguments += " -nostatus";
            if (bViewOnly) psi.Arguments += " -viewonly";
            if (!bContextMenu) psi.Arguments += " -restricted";

            psi.Arguments += String.Format(" -scale {0}/100", percentOfClientViewer > 100 ? 100 : percentOfClientViewer);

            return System.Diagnostics.Process.Start(psi);
        }

		/// <summary>
		/// VNC Viewer 실행
		/// </summary>
		/// <param name="ip"></param>
		/// <param name="port"></param>
		/// <param name="proxyhost"></param>
		/// <param name="proxyport"></param>
		/// <param name="auth_password"></param>
		/// <param name="bToolbar"></param>
		/// <param name="bStatusbar"></param>
		/// <param name="bViewOnly"></param>
		/// <param name="bContextMenu"></param>
		/// <param name="percentOfClientViewer"></param>
		/// <returns></returns>
		static public System.Diagnostics.Process RunVncViewer(string ip, string port, string proxyhost, string proxyport, string auth_password, bool bToolbar, bool bStatusbar, bool bViewOnly, bool bContextMenu, int percentOfClientViewer)
		{

			String sFilePath = AppDomain.CurrentDomain.BaseDirectory;
			System.Diagnostics.ProcessStartInfo psi = new System.Diagnostics.ProcessStartInfo();
			psi.FileName = sFilePath + @"\vncviewer.exe";
			psi.UseShellExecute = true;
			psi.WorkingDirectory = sFilePath;
			psi.Arguments = String.Format("-connect {0}::{1} -proxy {2}:{3} -password {4} -quickoption 3 -nohotkeys", ip, port, proxyhost, proxyport, auth_password);
			//			-quality 0 -nohotkeys -noauto
			// 			" -quality 0 -autoacceptincoming -nohotkeys"
			if (!bToolbar) psi.Arguments += " -notoolbar";
			if (!bStatusbar) psi.Arguments += " -nostatus";
			if (bViewOnly) psi.Arguments += " -viewonly";
			if (!bContextMenu) psi.Arguments += " -restricted";

			psi.Arguments += String.Format(" -scale {0}/100", percentOfClientViewer > 100 ? 100 : percentOfClientViewer);

			return System.Diagnostics.Process.Start(psi);
		}
	}
}
