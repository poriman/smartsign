﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Markup;
using System.IO;

using WPFDesigner;
using System.Globalization;
using DigitalSignage.Common;
using System.Windows.Controls.Primitives;

namespace DigitalSignage.Scheduler
{
    /// <summary>
    /// Provides an editing of flow documents
    /// </summary>
    public partial class TextEditor : Window
    {
        System.Windows.Forms.ColorDialog cd;
        string loadedDoc;

        ScrollTextInfo _sti, _loaded;

        bool bInitial = false;

        string rssConfirmed = String.Empty;

        public bool IsRssUI
        {
            get { return tabRss.Visibility == System.Windows.Visibility.Visible; }
            set
            {
                if (value)
                {
                    tabRss.Visibility = System.Windows.Visibility.Visible;
                    tabSubtitle.Visibility = System.Windows.Visibility.Collapsed;
                    tabRss.IsSelected = true;
                    tbRssURL.Focus();
                }
                else
                {
                    tabSubtitle.IsSelected = true;
                    tabRss.Visibility = System.Windows.Visibility.Collapsed;
                    tabSubtitle.Visibility = System.Windows.Visibility.Visible;
                    te_RichTextBox.Focus();
                }
            }
        }

        public TextEditor()
        {
            InitializeComponent();
            _sti = new ScrollTextInfo();
            cd = new System.Windows.Forms.ColorDialog();
            cd.AnyColor = true;
            cd.FullOpen = true;

            backgroundLabel.MouseDown += new MouseButtonEventHandler(backgroundLabel_MouseDown);
            fontColorLabel.MouseDown += new MouseButtonEventHandler(fontColorLabel_MouseDown);


            //	FontFamily Setting

            comboBoxFontFamily.ItemsSource = Fonts.SystemFontFamilies;
            
            st.RssLoadErrorEvent += new EventHandler(st_RssLoadErrorEvent);
        }



        /// <summary>
        /// Provides a background setup
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void backgroundLabel_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Label cb = sender as Label;
            
            BackgroundSetter bs = new BackgroundSetter();
            bs.Load(backgroundLabel.Background.Clone());
            if (bs.ISApply)
            {
                backgroundLabel.Background = bs.GetContent();
                _sti.background = backgroundLabel.Background;
                UpdateRss();
            }
            
        }

        void fontColorLabel_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Label cb = sender as Label;
            
            BackgroundSetter bs = new BackgroundSetter();
            bs.Load(fontColorLabel.Background.Clone());
            if (bs.ISApply)
            {
                fontColorLabel.Background = bs.GetContent();
                _sti.foreground = fontColorLabel.Background;
                UpdateRss();
            }
            
        }

        void st_RssLoadErrorEvent(object sender, EventArgs e)
        {
            try
            {
                st.Stop();

                MessageBox.Show(Properties.Resources.mbErrorRSSValidation, Properties.Resources.titleMessageBox, MessageBoxButton.OK, MessageBoxImage.Error);
            }
            catch { }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Init();
        }

        //Setups text editor
        void Init()
        {
			te_FontFamily.ItemsSource = Fonts.SystemFontFamilies;


            try
            {
                te_FontFamily.SelectedItem = new FontFamily("NanumGothic");
            }
            catch
            {
                te_FontFamily.SelectedIndex = 0;
            }

            try
            {
                te_FontSize.SelectedIndex = te_FontSize.Items.Count - 3;
            }
            catch 
            {
                te_FontSize.SelectedIndex = te_FontSize.Items.Count - 1;
            }

			//te_FontFamily.SelectedIndex = 0;

            te_RichTextBox.SpellCheck.IsEnabled = true;
            te_RichTextBox.SpellCheck.SpellingReform = SpellingReform.PreAndPostreform;

            //Initiating color dialog that is used for setting color
            //of currently selected text
            cd = new System.Windows.Forms.ColorDialog();
            cd.AnyColor = true;
            cd.FullOpen = true;

			te_RichTextBox.Document.Foreground = Brushes.Black;
            te_RichTextBox.Document.Background = new SolidColorBrush(Color.FromArgb(0, 255,255,255));

            te_FontColor.MouseDown += new MouseButtonEventHandler(te_FontColor_MouseDown);
            te_backgroundLabel.MouseDown += new MouseButtonEventHandler(te_backgroundLabel_MouseDown);

            bInitial = true;
        }

        void te_backgroundLabel_MouseDown(object sender, MouseButtonEventArgs e)
        {
            BackgroundSetter bs = new BackgroundSetter();
            bs.Load(te_backgroundLabel.Background == null ? Brushes.Transparent : te_backgroundLabel.Background);
            if (bs.ISApply)
            {
                te_backgroundLabel.Background = bs.GetContent();

                if (!bInitial) te_RichTextBox.Document.Background = te_backgroundLabel.Background;
                
                te_RichTextBox.Selection.ApplyPropertyValue(TextElement.BackgroundProperty, te_backgroundLabel.Background);

            }
        }

        void te_FontColor_MouseDown(object sender, MouseButtonEventArgs e)
        {
            BackgroundSetter bs = new BackgroundSetter();
            bs.Load(te_FontColor.Background);
            if (bs.ISApply)
            {
                te_FontColor.Background = bs.GetContent();
                if (!bInitial) te_RichTextBox.Document.Foreground = te_FontColor.Background;
                
                te_RichTextBox.Selection.ApplyPropertyValue(TextElement.ForegroundProperty, te_FontColor.Background);
            }
        }

        //Handling the context menu clicks
        private void MenuItem_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            switch (((MenuItem)sender).Name)
            {
                case "te_Copy":
                    {
                        te_RichTextBox.Copy();
                        break;
                    }
                case "te_Cut":
                    {
                        te_RichTextBox.Cut();
                        break;
                    }
                case "te_Paste":
                    {
                        te_RichTextBox.Paste();
                        break;
                    }
                default: break;
            }
        }

        //Loads a document to a text editor
        public void Load(string doc)
        {
            loadedDoc = doc;
            FlowDocument fd = FlowDocumentManager.DeserializeFlowDocument(loadedDoc);
            if (fd.Name != "RSS")
            {
                te_RichTextBox.Document = FlowDocumentManager.DeserializeFlowDocument(loadedDoc);
                te_backgroundLabel.Background = te_RichTextBox.Document.Background;
                if (te_RichTextBox.Document.Blocks.Count > 0)
                {
                    te_FontColor.Background = te_RichTextBox.Document.Blocks.FirstBlock.Foreground;
                }

            }
            else if (fd.Name == "RSS")
            {
                try
                {
                    if (fd.Blocks.Count > 0) {
                        fontColorLabel.Background = fd.Blocks.FirstBlock.Foreground;
                    }
                    tbRssURL.Text = (new TextRange(fd.ContentStart, fd.ContentEnd).Text).Trim(new char[] {'\r', '\n'});

                    try
                    {
                        comboBoxFontFamily.SelectedItem =  fd.FontFamily;
                    }
                    catch
                    {
                        comboBoxFontFamily.SelectedIndex = 0;
                    }

                    try
                    {
                        foreach (ComboBoxItem item in comboBoxFontSize.Items)
                        {
                            if (fd.FontSize.ToString().Equals(item.Content.ToString()))
                            {
                                item.IsSelected = true;
                                break;
                            }

                        }

                    }
                    catch
                    {
                        comboBoxFontSize.SelectedIndex = 5;
                    }

                    tabRss.Focus();
                }
                catch { }
            }
            ShowDialog();
        }

        //Returns an edited document
        public string GetDocument()
        {
            return loadedDoc;
        }

        //Unloads text editor
        public void Unload()
        {
            this.Close();
        }

        //Change font size of selected text
        private void te_FontSize_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string s = ((ComboBoxItem)te_FontSize.SelectedValue).Content.ToString();
            if (!bInitial) te_RichTextBox.Document.FontSize = Double.Parse(s);
           
            te_RichTextBox.Selection.ApplyPropertyValue(TextElement.FontSizeProperty, Double.Parse(s));
        }

        //Change font family of selected text
        private void te_FontFamily_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {
            if (!bInitial) te_RichTextBox.Document.FontFamily = (FontFamily)te_FontFamily.SelectedItem;
			
            te_RichTextBox.Selection.ApplyPropertyValue(TextElement.FontFamilyProperty, (FontFamily)te_FontFamily.SelectedItem/*((ComboBoxItem)te_FontFamily.SelectedValue).DataContext*/);
        }

        private void closeButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void okButton_Click(object sender, RoutedEventArgs e)
        {
            if (tabRss.IsSelected)
            {
                if (tbRssURL.Text.Equals(rssConfirmed))
                {
                    
                    //Run run = new Run(tbRssURL.Text);
                    FlowDocument doc = new FlowDocument();
                    doc.FontSize = _sti.FontSize;
                    doc.FontFamily = _sti.FontFamily;
                    doc.FontStyle = _sti.FontStyle;
                    doc.FontFamily = _sti.FontFamily;
                    doc.Foreground = _sti.foreground;
                    doc.FontWeight = _sti.FontWeight;
                    doc.Name = "RSS";
                    doc.Blocks.Add(new Paragraph(new Run(tbRssURL.Text)));
                    loadedDoc = FlowDocumentManager.SerializeFlowDocument(doc);
                    
                    Close();
                }
                else
                {
                    MessageBox.Show(Properties.Resources.RssStartMessage, Properties.Resources.titleMessageBox, MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                loadedDoc = FlowDocumentManager.SerializeFlowDocument(te_RichTextBox.Document);
                Close();
            }
        }


        void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox cb = sender as ComboBox;

            if (_sti == null) return;

            if (cb.SelectedItem == null)
            {
                return;
            }

            switch (cb.Name)
            {
                case "comboBoxFontFamily":
                    {
                        _sti.FontFamily = (FontFamily)cb.SelectedItem;
                        // 						_sti.FontFamily = ((ComboBoxItem)cb.SelectedItem).FontFamily;
                        break;
                    }
                case "comboBoxFontFamilyD":
                    {
                        _sti.DescFontFamily = (FontFamily)cb.SelectedItem;
                        // 						_sti.DescFontFamily = ((ComboBoxItem)cb.SelectedItem).FontFamily;
                        break;
                    }
                case "comboBoxFontSize":
                    {
                        String sSize = ((ComboBoxItem)cb.SelectedItem).Content.ToString();
                        _sti.FontSize = double.Parse(sSize);
                        break;
                    }
                case "comboBoxFontSizeD":
                    {
                        String sSize = ((ComboBoxItem)cb.SelectedItem).Content.ToString();
                        _sti.DescFontSize = double.Parse(sSize);
                        break;
                    }
                default: break;
            }

            UpdateRss();
        }

        private void scrollTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            _sti.content = tbRssURL.Text;
        }


        private void UpdateRss()
        {
            st.FeedUrl = _sti.content.ToString();
            st.Direction = _sti.direction;
            st.Speed = _sti.speed;
            ((ITextContainer)st).SetFontFamily(_sti.FontFamily);
            ((ITextContainer)st).SetFontBrush(_sti.foreground);
            ((ITextContainer)st).SetFontSize(_sti.FontSize);
            ((ITextContainer)st).SetFontWeight(_sti.FontWeight);
        }

        private void startScrollingButton_Click(object sender, RoutedEventArgs e)
        {
            _sti.content = tbRssURL.Text;

            _sti.background = backgroundLabel.Background;
            _sti.foreground = fontColorLabel.Background;

            UpdateRss();
            st.Direction = ScrollTextDirection.Flip;
            st.Start(new TimeSpan(0, 5, 0));
            rssConfirmed = tbRssURL.Text;
        }

        private void stopScrollingButton_Click(object sender, RoutedEventArgs e)
        {
            st.Stop();
        }

        private void te_Bold_Checked(object sender, RoutedEventArgs e)
        {
            ToggleButton btn = (ToggleButton)sender;
            _sti.FontWeight = FontWeights.Bold;
            st.TBFontWeight = FontWeights.Bold;
        }

        private void te_Bold_Unchecked(object sender, RoutedEventArgs e)
        {
            ToggleButton btn = (ToggleButton)sender;
            _sti.FontWeight = FontWeights.Normal;
            st.TBFontWeight = FontWeights.Normal;
        }

        private void te_Italic_Checked(object sender, RoutedEventArgs e)
        {
            ToggleButton btn = (ToggleButton)sender;
            
            _sti.FontStyle = FontStyles.Italic;
            st.TBFontStyle = FontStyles.Italic;
        }

        private void te_Italic_Unchecked(object sender, RoutedEventArgs e)
        {
            ToggleButton btn = (ToggleButton)sender;

            _sti.FontStyle = FontStyles.Normal;
            st.TBFontStyle = FontStyles.Normal;
        }

        private void applyButton_Click(object sender, RoutedEventArgs e)
        {
            loadedDoc = FlowDocumentManager.SerializeFlowDocument(te_RichTextBox.Document);
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

		private void te_RichTextBox_SelectionChanged(object sender, RoutedEventArgs e)
		{
			try
			{
				if (!te_RichTextBox.Selection.IsEmpty)
				{
					var fontFamily = te_RichTextBox.Selection.GetPropertyValue(TextElement.FontFamilyProperty);

					te_FontFamily.SelectedItem = fontFamily;

					// Set font size combo
					var fontSize = te_RichTextBox.Selection.GetPropertyValue(TextElement.FontSizeProperty);
					foreach (ComboBoxItem item in te_FontSize.Items)
					{
						if (fontSize.ToString().Equals(item.Content.ToString()))
						{
							item.IsSelected = true;
							break;
						}

					}

                    te_FontColor.Background = (Brush)te_RichTextBox.Selection.GetPropertyValue(TextElement.ForegroundProperty);
                    te_backgroundLabel.Background = (Brush)te_RichTextBox.Selection.GetPropertyValue(TextElement.BackgroundProperty);
                }
			}
			catch
			{

			}
		}
    }
}