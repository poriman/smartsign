﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;
using System.Windows.Threading;
using DigitalSignage.Common;
using System.Collections.ObjectModel;

using System.Xml;
using System.ComponentModel;

namespace DigitalSignage.Scheduler
{
	/// <summary>
	/// Interaction logic for TaskDownloader.xaml
	/// </summary>
	public partial class TaskDownloader : Window
	{
		private string m_dest;
		private Config m_config = null;
		private Guid m_duuid;
		private XmlNode m_group;

		private BackgroundWorker bwDownload;
		private ProgramUploader uploader = new ProgramUploader();

		public TaskDownloader(Config cfg, XmlNode group, Guid duuid, String dest_path)
		{
			m_dest = dest_path;
			m_config = cfg;
			m_group = group;
			m_duuid = duuid;

			uploader.OnSetProgress = new EventHandler(InitialProgress);
			uploader.OnStepIt = new EventHandler(StepIt);
			uploader.OnUpdateLabel = new StatusEventHandler(UpdateLabel);
			uploader.OnUpdateTitle += new StatusEventHandler(UpdateTitle);

			InitializeComponent();
		}

	
		void StepIt(Object obj, EventArgs e)
		{
			StepIt();
		}
		
		void InitialProgress(Object obj, EventArgs e)
		{
			FTPFunctions ftp = obj as FTPFunctions;
			if(ftp != null)
			{
				InitialProgress(ftp.ProgressCount);
			}
		}
		void UpdateLabel(Object obj, StatusEventArgs e)
		{
			if (e.OriginalSource != null)
			{
				UpdateLabel(e.OriginalSource as String);
			}
		}
		void UpdateTitle(Object obj, StatusEventArgs e)
		{
			if (e.OriginalSource != null)
			{
				UpdateTitle(e.OriginalSource as String);
			}
		}
		
		private void DownloadTask()
		{
			bwDownload = new BackgroundWorker();
			bwDownload.DoWork += new DoWorkEventHandler(bwDownload_DoWork);
			bwDownload.WorkerSupportsCancellation = true;
			bwDownload.RunWorkerAsync();
		}

		void bwDownload_DoWork(object sender, DoWorkEventArgs e)
		{
			try
			{
				while (true)
				{
					if (!uploader.DownloadProgram(m_config, m_group, m_duuid, m_dest))
					{
						MessageBoxResult result = MessageBox.Show(String.Format(Properties.Resources.mbErrorDownload, uploader.LastErrorMessage), Properties.Resources.titleMessageBox, MessageBoxButton.YesNo);
						
						switch(result)
						{
							case MessageBoxResult.Yes: continue;
							case MessageBoxResult.No: break;
						}
						uploader.RemoveLocalFiles(m_dest);
						Finish(false);
						return;
					}
					else break;

				}
				Finish(true);
				return;
			}
			catch
			{
				uploader.RemoveLocalFiles(m_dest);
				Finish(false);
			}
		}

		private void Window_Loaded(object sender, RoutedEventArgs e)
		{
			DownloadTask();
		}

		#region Async routines
		delegate void finishCB(bool bExitCode);
		public void Finish(bool bExitCode)
		{
			try
			{
				if (!this.Dispatcher.CheckAccess())
				{
					this.Dispatcher.BeginInvoke(DispatcherPriority.Render,
					   new finishCB(Finish), bExitCode);
				}
				else
				{
					this.DialogResult = bExitCode;
					Close();
				}
			}
			catch (Exception /*e*/)
			{
// 				MessageBox.Show(e.Message);
			}
		}

		delegate void InitialProgressCB(int nProgressCount);
		void InitialProgress(int nProgressCount)
		{
			try
			{
				if (!this.Dispatcher.CheckAccess())
				{
					this.Dispatcher.BeginInvoke(DispatcherPriority.Render,
					   new InitialProgressCB(InitialProgress), nProgressCount);
				}
				else
				{
					progressBarCopyProgress.Maximum = nProgressCount;
					progressBarCopyProgress.Value = 0;
				}
			}
			catch (Exception e)
			{
				MessageBox.Show(e.Message);
			}
		}

		delegate void StepItCB();
		void StepIt()
		{
			try
			{
				if (!this.Dispatcher.CheckAccess())
				{
					this.Dispatcher.BeginInvoke(DispatcherPriority.Render,
					   new StepItCB(StepIt));
				}
				else
				{
					progressBarCopyProgress.Value++;
				}
			}
			catch (Exception e)
			{
				MessageBox.Show(e.Message);
			}
		}
		delegate void UpdateTitleLabelCB(String statusLabel);
		void UpdateTitle(String statusLabel)
		{
			try
			{
				if (!this.Dispatcher.CheckAccess())
				{
					this.Dispatcher.BeginInvoke(DispatcherPriority.Render,
					   new UpdateTitleLabelCB(UpdateTitle), statusLabel);
				}
				else
				{
					lbStatus.Content = statusLabel;
				}
			}
			catch (Exception e)
			{
				MessageBox.Show(e.Message);
			}
		}
		delegate void UpdateLabelCB(String statusLabel);
		void UpdateLabel(String statusLabel)
		{
			try
			{
				if (!this.Dispatcher.CheckAccess())
				{
					this.Dispatcher.BeginInvoke(DispatcherPriority.Render,
					   new UpdateLabelCB(UpdateLabel), statusLabel);
				}
				else
				{
					String content;
					String[] sarray = statusLabel.Split('\\');
					if(sarray.Length > 3)
					{
						content = sarray[0] + "\\..\\" + sarray[sarray.Length - 1];
					}
					else
					{
						content = statusLabel;
					}

					lbStatusContent.Content = content;
				}
			}
			catch (Exception e)
			{
				MessageBox.Show(e.Message);
			}
		}

		#endregion  

		private void Window_Unloaded(object sender, RoutedEventArgs e)
		{
			try
			{
				if (bwDownload != null)
				{
					if (bwDownload.IsBusy) bwDownload.CancelAsync();
					bwDownload.Dispose();
					bwDownload = null;
				}
			}
			catch
			{
			}
		}

		private void btnCancel_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				if (bwDownload != null)
				{
					if (bwDownload.IsBusy) bwDownload.CancelAsync();
					bwDownload.Dispose();
					bwDownload = null;

					uploader.RemoveLocalFiles(m_dest);
					Finish(false);
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}
		}
	}
}
