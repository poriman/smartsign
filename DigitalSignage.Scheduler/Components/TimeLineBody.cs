﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DigitalSignage.Scheduler
{
	public class TimeLineBody : ItemsControl
	{
		static TimeLineBody()
		{
			DefaultStyleKeyProperty.OverrideMetadata(typeof(TimeLineBody), new FrameworkPropertyMetadata(typeof(TimeLineBody)));
		}
	}
}
