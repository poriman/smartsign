﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Xml;

namespace DigitalSignage.Scheduler
{
	public enum iVisionFileType { Screen, Playlist, Program, Unknown };

	/// <summary>
	/// 업로드할 프로그램을 만들어주는 헬퍼 클래스
	/// </summary>
	public class ivisionFileHelper
	{
		public ivisionFileHelper()
		{
			Initialize();
		}

		#region Private Data Member
		string _programfile;
		string _lastErrorMessage;
		Collection<string> _arrLoadedPlaylists = null;
		Collection<string> _arrLoadedScreens = null;

		XmlDocument _document = null;
		#endregion

		#region Properties
		
		/// <summary>
		/// 마지막 오류 정보를 반환한다.
		/// </summary>
		public String LastErrorMessage { get { return String.IsNullOrEmpty(_lastErrorMessage) ? "" : _lastErrorMessage;}}

		/// <summary>
		/// 로드된 페이지 목록을 가져온다 
		/// </summary>
		public Collection<ScreenPropertyLoader> LoadedScreenProperties = new Collection<ScreenPropertyLoader>();

		#endregion


		#region Functions
		
		#region Private
		/// <summary>
		/// 초기화
		/// </summary>
		void Initialize()
		{
			_arrLoadedPlaylists = new Collection<string>();
			_arrLoadedScreens = new Collection<string>();

			LoadedScreenProperties.Clear();
	
			_programfile = "";
			_lastErrorMessage= "";
			_document = null;
		}

		/// <summary>
		/// Xml을 분석한다.
		/// </summary>
		/// <param name="filepath"></param>
		/// <returns></returns>
		bool LoadXml(string filepath)
		{
			try
			{
				_document = new XmlDocument();
				_document.Load(filepath);
				return true;
			}
			catch
			{
				_lastErrorMessage =  Properties.Resources.mbErrorFileNotFound;
				return false;
			}		
		}
		#endregion

		/// 프로그램 파일을 분석하여 윈하는 위치에 Program을 생성한다.
		/// </summary>
		/// <param name="filepath">로드할 프로그램 파일</param>
		/// <param name="descpath"></param>
		/// <param name="isNewTouchFile"></param>
		/// <returns>False면 오류 발생, True면 성공</returns>
		public bool MakeProgram(String filepath, String descpath, bool isNewTouchFile)
		{
			try
			{
				//	해당 파일이 실제 존재하는지 체크한다.
				if (!System.IO.File.Exists(filepath))
					throw new Exception(Properties.Resources.mbErrorFileNotFound);

				//	프로그램 파일인지 검사한다.
				if (VerifyFile(filepath) != iVisionFileType.Program)
					throw new Exception(Properties.Resources.mbErrorProgramFile);

				//	프로그램 파일 위치를 저장한다.
				_programfile = System.IO.Path.GetDirectoryName(filepath) + "\\";

				#region 프로그램 파일 만들기
				//	pane들을 얻어와서 플레이리스트를 추출한다.
				XmlNodeList list = _document.SelectNodes("//child::pane");
				if (list == null) throw new Exception(Properties.Resources.mbErrorProgramFile);

				foreach(XmlNode node in list)
				{
					String playlist = node.Attributes["playlist"].Value;
					if(!System.IO.File.Exists(playlist))
					{
						playlist = _programfile + playlist;
						if(!System.IO.File.Exists(playlist))
						{
							throw new Exception(Properties.Resources.errorVerifyIPRFile);
						}
					}

					if(!_arrLoadedPlaylists.Contains(playlist))
					{
						_arrLoadedPlaylists.Add(playlist);
					}

					node.Attributes["playlist"].Value = String.Format("playlist{0}.xml", _arrLoadedPlaylists.IndexOf(playlist) + 1);
			
				}
				_document.Save(String.Format("{0}\\program.xml", descpath));
				#endregion
				int nCnt = 0;
				foreach(string playlist in _arrLoadedPlaylists)
				{
					try
					{
						XmlDocument xmllist = new XmlDocument();
						xmllist.Load(playlist);

						XmlNodeList screenlist = xmllist.SelectNodes("//child::screen");
						if (screenlist == null) throw new Exception(Properties.Resources.mbErrorPlaylistFile);

						//	플레이리스트 기본폴더를 지정한다.
						string playlistfile = System.IO.Path.GetDirectoryName(playlist) + "\\";
						foreach (XmlNode screenNode in screenlist)
						{
							string screen = screenNode.Attributes["ID"].Value;
							
							if (!System.IO.File.Exists(screen))
							{
								screen = playlistfile + screen;
								if (!System.IO.File.Exists(screen))
								{
									throw new Exception(Properties.Resources.errorVerifyIPLFile);
								}

							}

							ScreenPropertyLoader loaded_screen = new ScreenPropertyLoader(screen);
							LoadedScreenProperties.Add(loaded_screen);

							if (!_arrLoadedScreens.Contains(screen))
							{
								_arrLoadedScreens.Add(screen);
								string descscreen = isNewTouchFile ? 
									descpath : String.Format("{0}\\{1}\\", descpath, loaded_screen.ScreenID);

								if (!DigitalSignage.Common.FTPFunctions.RecursiveCopy(System.IO.Path.GetDirectoryName(screen), descscreen))
								{
									throw new Exception(Properties.Resources.mbErrorCopyFailed);
								}
							}

							screenNode.Attributes["ID"].Value = isNewTouchFile ? System.IO.Path.GetFileName(screen) : String.Format("{0}\\{1}", loaded_screen.ScreenID,
								System.IO.Path.GetFileName(screen));
						}
						xmllist.Save(String.Format("{0}\\playlist{1}.xml", descpath, ++nCnt));

					}
					catch
					{
						throw new Exception(Properties.Resources.errorVerifyIPRFile);
					}


				}
			
				return true;
			}
			catch (System.Exception e)
			{
				_lastErrorMessage = e.Message;
				return false;
			}
		}

		/// <summary>
		/// 해당 프로그램이 어떤 타입의 파일인지 분석한다.
		/// </summary>
		/// <param name="filepath">로드한 파일</param>
		/// <returns>iVisionFileType 열거자가 반환된다.</returns>
		public iVisionFileType VerifyFile(String filepath)
		{
			try
			{
				//	해당 파일이 실제 존재하는지 체크한다.
				if (!System.IO.File.Exists(filepath))
				{
					throw new Exception(Properties.Resources.mbErrorFileNotFound);
				}

				if (System.IO.Path.GetExtension(filepath).ToLower().Equals(".bin"))
						return iVisionFileType.Screen;

				if (LoadXml(filepath))
				{
					if (_document.SelectSingleNode("child::program") != null &&
						_document.SelectSingleNode("descendant::pane") != null &&
						_document.SelectSingleNode("descendant::pane").Attributes["playlist"] != null)
						return iVisionFileType.Program;
					else if (_document.SelectSingleNode("child::playlist") != null &&
						 _document.SelectSingleNode("descendant::screen") != null)
						return iVisionFileType.Playlist;
					else return iVisionFileType.Unknown;
				}
				else
					return iVisionFileType.Unknown;

			}
			catch (System.Exception e)
			{
				_lastErrorMessage = e.Message;
				return iVisionFileType.Unknown;
			}
		}

		#endregion



	}
}
