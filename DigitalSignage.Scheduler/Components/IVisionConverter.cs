﻿using System;
using System.Windows.Data;
using System.Globalization;
using System.Collections.ObjectModel;
using DigitalSignage.Common;
using System.Windows.Controls;
using System.Windows.Media.Effects;
using System.Windows.Media;
using System.ComponentModel;

namespace DigitalSignage.Scheduler
{
	class VersionConvert : IValueConverter
	{
		#region IValueConverter 멤버

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			try
			{
				int major = (System.Convert.ToInt32(value) >> 16) & 0xFFFF;
				int minor = System.Convert.ToInt32(value) & 0xFFFF;

				return String.Format("{0}.{1}", major, minor);
			}
			catch (Exception ex)
			{
				string aa = ex.ToString();
				return Properties.Resources.labelUnknown;
			}
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return Binding.DoNothing;
		}

		#endregion
	}

	class VersionWithStateConvert : IMultiValueConverter
	{
		#region IValueConverter 멤버

		public object Convert(object[] value, Type targetType, object parameter, CultureInfo culture)
		{
			try
			{
                /*
				if (System.Convert.ToInt32(value[0]) == 0)
				{
					return Properties.Resources.labelUnknown;
				}
				else*/
				{
					try
					{
						int major = (System.Convert.ToInt32(value[1]) >> 16) & 0xFFFF;
						int minor = System.Convert.ToInt32(value[1]) & 0xFFFF;
						int build = (System.Convert.ToInt32(value[2]) >> 16) & 0xFFFF;
						int revision = System.Convert.ToInt32(value[2]) & 0xFFFF;

						return String.Format("{0}.{1}.{2}.{3}", major, minor, build, revision);
					}
					catch (Exception ex)
					{
						string aa = ex.ToString();
						return Properties.Resources.labelUnknown;
					}

				}
			}
			catch
			{
				return Properties.Resources.labelUnknown;
			}
		}

		public object[] ConvertBack(object value, Type[] targetType, object parameter, CultureInfo culture)
		{
			return null;
		}

		#endregion
	}

	class UptimeConvert : IValueConverter
	{
		#region IValueConverter 멤버

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			try
			{
				TimeSpan ts = new TimeSpan(0, System.Convert.ToInt32(value), 0);

				return ts.ToString();

			}
			catch
			{
				return Properties.Resources.labelUnknown;
			}
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return Binding.DoNothing;
		}

		#endregion
	}

	class UptimeWithStateConvert : IMultiValueConverter
	{
		#region IValueConverter 멤버

		public object Convert(object[] value, Type targetType, object parameter, CultureInfo culture)
		{
			try
			{
				if (System.Convert.ToInt32(value[0]) == 0)
				{
					return Properties.Resources.labelUnknown;
				}
				else
				{
					TimeSpan ts = new TimeSpan(0, System.Convert.ToInt32(value[1]), 0);

					return ts.ToString();

				}
			}
			catch
			{
				return Properties.Resources.labelUnknown;
			}
		}

		public object[] ConvertBack(object value, Type[] targetType, object parameter, CultureInfo culture)
		{
			return null;
		}

		#endregion
	}

	class CurrProgress : IValueConverter
	{
		#region IValueConverter 멤버

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			String data = value as String;

			if (String.IsNullOrEmpty(data))
				return 0;

			try
			{
				string[] arr = data.Split('/');
				double dValue = System.Convert.ToDouble(arr[0]);
				return dValue;
			}
			catch
			{
				return 0;
			}
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return Binding.DoNothing;
		}

		#endregion
	}

	class CurrProgressText : IMultiValueConverter
	{
		#region IValueConverter 멤버

		public object Convert(object[] value, Type targetType, object parameter, CultureInfo culture)
		{
			try
			{
				if (System.Convert.ToInt32(value[0]) == 0)
				{
					return Properties.Resources.labelUnknown;
				}
				else
				{
					String data = value[1] as String;

					if (String.IsNullOrEmpty(data) || data.Equals("none"))
						return Properties.Resources.labelNone;

					try
					{
						string[] arr = data.Split('/');

						if (arr.Length == 0 || String.IsNullOrEmpty(arr[1]) || arr[1].Equals("none"))
							return Properties.Resources.labelNone;

						return System.Convert.ToString(arr[1]);
					}
					catch
					{
						return Properties.Resources.labelNone;
					}
				}
			}
			catch
			{
				return Properties.Resources.labelUnknown;
			}
		}

		public object[] ConvertBack(object value, Type[] targetType, object parameter, CultureInfo culture)
		{
			return null;
		}

		#endregion
	}

	class MonitorNameConvert : IMultiValueConverter
	{
		#region IValueConverter 멤버

		public object Convert(object[] value, Type targetType, object parameter, CultureInfo culture)
		{
			try
			{
				if (System.Convert.ToInt32(value[0]) == 0)
				{
					return Properties.Resources.labelUnknown;
				}
				else
				{
					String sValue = value[1] as String;
					if (String.IsNullOrEmpty(sValue)) return Properties.Resources.labelUnknown;

					return sValue.Equals("-1") || sValue.Equals("None") ? Properties.Resources.labelUnknown : sValue;

				}
			}
			catch
			{
				return Properties.Resources.labelUnknown;
			}
		}

		public object[] ConvertBack(object value, Type[] targetType, object parameter, CultureInfo culture)
		{
			return null;
		}

		#endregion
	}

	class MonitorSourceConvert : IMultiValueConverter
	{
		#region IValueConverter 멤버

		public object Convert(object[] value, Type targetType, object parameter, CultureInfo culture)
		{
			try
			{
				if (System.Convert.ToInt32(value[0]) == 0)
				{
					return Properties.Resources.labelUnknown;
				}
				else
				{
					String sValue = value[1] as String;
					if (String.IsNullOrEmpty(sValue)) return Properties.Resources.labelUnknown;

					return sValue.Equals("-1") || sValue.Equals("-") ? Properties.Resources.labelUnknown : sValue;
				}
			}
			catch
			{
				return Properties.Resources.labelUnknown;
			}
		}

		public object[] ConvertBack(object value, Type[] targetType, object parameter, CultureInfo culture)
		{
			return null;
		}

		#endregion
	}
	
	class CurrScheduleConvert : IMultiValueConverter
	{
		#region IValueConverter 멤버

		public object Convert(object[] value, Type targetType, object parameter, CultureInfo culture)
		{
			try
			{
				if (System.Convert.ToInt32(value[0]) == 0)
				{
					return Properties.Resources.labelUnknown;
				}
				else
				{
					if (String.IsNullOrEmpty(value[1] as string)) return Properties.Resources.labelNone;

					return value[1].ToString().Equals("unknown") ? Properties.Resources.labelNone : value[1].ToString();
				}
			}
			catch
			{
				return Properties.Resources.labelUnknown;
			}
		}

		public object[] ConvertBack(object value, Type[] targetType, object parameter, CultureInfo culture)
		{
			return null;
		}

		#endregion
	}

	class AllProgress1 : IValueConverter
	{
		#region IValueConverter 멤버

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			String data = value as String;

			if (String.IsNullOrEmpty(data))
				return 0;

			try
			{
				string[] arr = data.Split('/');

				return System.Convert.ToInt32(arr[0]);
			}
			catch
			{
				return 0;
			}
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return Binding.DoNothing;
		}

		#endregion
	}

	class AllProgress2 : IValueConverter
	{
		#region IValueConverter 멤버

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			String data = value as String;

			if (String.IsNullOrEmpty(data))
				return 0;

			try
			{
				string[] arr = data.Split('/');

				return System.Convert.ToInt32(arr[1]);
			}
			catch
			{
				return 0;
			}
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return Binding.DoNothing;
		}

		#endregion
	}

	class PlayerConvert : IValueConverter
	{
		#region IValueConverter 멤버

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			try
			{
				return ServerDataCache.GetInstance.GetPlayerName(System.Convert.ToString(value));
			}
			catch { }

			return value;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return Binding.DoNothing;
		}

		#endregion
	}

	class GroupConvert : IValueConverter
	{
		#region IValueConverter 멤버

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			try
			{
				return ServerDataCache.GetInstance.GetGroupNameByPID(System.Convert.ToString(value));
			}
			catch { }

			return value;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return Binding.DoNothing;
		}

		#endregion
	}

	class NPCUResponseCodeConvert : IValueConverter
	{
		#region IValueConverter 멤버

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			try
			{
				return GetNPCUMessage(System.Convert.ToString(value));
			}
			catch { }

			return value;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return Binding.DoNothing;
		}

		String GetNPCUMessage(String response_cd)
		{

			if (response_cd.Equals("0"))
			{
				return Properties.Resources.comboLogResultSuccess;
			}
			else if (response_cd.Equals("1") || response_cd.Equals("2"))
			{
				return String.Format("{0} [{1}]", Properties.Resources.comboLogResultFailed, Properties.Resources.labelNPCU_01_02);
			}
			else if (response_cd.Equals("6"))
			{
				return String.Format("{0} [{1}]", Properties.Resources.comboLogResultFailed, Properties.Resources.labelNPCU_06);
			}
			else if (response_cd.Equals("7"))
			{
				return String.Format("{0} [{1}]", Properties.Resources.comboLogResultFailed, Properties.Resources.labelNPCU_07);
			}
			else if (response_cd.Equals("8"))
			{
				return String.Format("{0} [{1}]", Properties.Resources.comboLogResultFailed, Properties.Resources.labelNPCU_08);
			}
			else if (response_cd.Equals("98"))
			{
				return String.Format("{0} [{1}]", Properties.Resources.comboLogResultFailed, Properties.Resources.labelNPCU_98);
			}
			else if (response_cd.Equals("99"))
			{
				return String.Format("{0} [{1}]", Properties.Resources.comboLogResultFailed, Properties.Resources.labelNPCU_99);
			}
			else
			{
				return Properties.Resources.labelNPCU_00;
			}
		}

		#endregion
	}

	class LogCodeConvert : IValueConverter
	{
		#region IValueConverter 멤버

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			int data = System.Convert.ToInt32(value);

			//	hsshin Error로그와 컨버팅
			
			return ErrorCmd.GetErrorMsg(data);
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return Binding.DoNothing;
		}

		#endregion
	}

	class LogAdConvert : IValueConverter
	{
		#region IValueConverter 멤버

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			try
			{
				int data = System.Convert.ToInt32(value);

				if ((data & (int)LogType.LogType_Ad) > 0) return Properties.Resources.comboLogTypeAd;
				else return Properties.Resources.comboLogTypeNonAd;

			}
			catch { }

			return Properties.Resources.comboLogTypeNonAd;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return Binding.DoNothing;
		}

		#endregion
	}

	class LogEventConvert : IValueConverter
	{
		#region IValueConverter 멤버

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			try
			{
				int data = System.Convert.ToInt32(value);

				if ((data & (int)LogType.LogType_Event) > 0) return Properties.Resources.comboLogTypeEvent;
				else return Properties.Resources.comboLogTypeGeneral;

			}
			catch { }

			return Properties.Resources.comboLogTypeGeneral;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return Binding.DoNothing;
		}

		#endregion
	}

	class LogSuccessConvert : IValueConverter
	{
		#region IValueConverter 멤버

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			try
			{
				int data = System.Convert.ToInt32(value);

				if ((data & (int)LogType.LogType_Failed) > 0) return Properties.Resources.comboLogResultFailed;
				else return Properties.Resources.comboLogResultSuccess;

			}
			catch { }

			return Properties.Resources.comboLogResultSuccess;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return Binding.DoNothing;
		}

		#endregion
	}

	class NameConvert : IMultiValueConverter
	{
		#region IValueConverter 멤버

		public object Convert(object[] value, Type targetType, object parameter, CultureInfo culture)
		{
			try
			{
				if ((int)((int)value[1] & (int)LogType.LogType_Connection) > 0)
				{
					if ((int)value[2] == 0)
						return "CONNECT";
					else return "DISCONNECT";
				}
				else return value[0];
			}
			catch { }

			return "";
		}

		public object[] ConvertBack(object value, Type[] targetType, object parameter, CultureInfo culture)
		{
			return null;
		}

		#endregion
	}

	class DurationConvert : IValueConverter
	{
		#region IValueConverter 멤버

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			try
			{
				long lSecond = System.Convert.ToInt64(value);
                if(lSecond < 0) return "-";

				String sReturn = String.Empty;
				TimeSpan ts = TimeSpan.FromSeconds(lSecond);

				if (ts.TotalDays >= 1)
					sReturn += String.Format("{0}일 ", ts.Days);
				if (ts.TotalHours >= 1)
					sReturn += String.Format("{0}시간 ", ts.Hours);
				if (ts.TotalMinutes >= 1)
					sReturn += String.Format("{0}분 ", ts.Minutes);

				sReturn += String.Format("{0}초", ts.Seconds);

				return sReturn;

			}
			catch { }

			return "-";
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return Binding.DoNothing;
		}

		#endregion
	}

	class DurationMultiConvert : IMultiValueConverter
	{
		#region IValueConverter 멤버

		public object Convert(object[] value, Type targetType, object parameter, CultureInfo culture)
		{
			try
			{
				int second = System.Convert.ToInt32(value[1]) - System.Convert.ToInt32(value[0]);
                if (second < 0) return "-";

				String sReturn = String.Empty;
				TimeSpan ts = TimeSpan.FromSeconds(second);

				if (ts.TotalDays >= 1)
					sReturn += String.Format("{0}일 ", ts.Days);
				if (ts.TotalHours >= 1)
				    sReturn += String.Format("{0}시간 ", ts.Hours);
				if (ts.TotalMinutes >= 1)
				    sReturn += String.Format("{0}분 ", ts.Minutes);

				sReturn += String.Format("{0}초", ts.Seconds);

				return sReturn;

			}
			catch { }

			return "-";
		}

		public object[] ConvertBack(object value, Type[] targetType, object parameter, CultureInfo culture)
		{
			return null;
		}

		#endregion
	}

	class LogCategoryConvert : IValueConverter
	{
		#region IValueConverter 멤버

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			try
			{
				int data = System.Convert.ToInt32(value);

				switch (data)
				{
					case (int)LogCategory.CatProcceed:
						return Properties.Resources.checkPlay;
					case (int)LogCategory.CatError:
						return Properties.Resources.checkError;
					case (int)LogCategory.CatDownload:
						return Properties.Resources.checkDownload;
				}

			}
			catch{}

			return "NONE";
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return Binding.DoNothing;
		}

		#endregion
	}

	class LogTypeConvert : IValueConverter
	{
		#region IValueConverter 멤버

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			try
			{
				int data = System.Convert.ToInt32(value);

				switch (data)
				{
					case (int)TaskCommands.CmdProgram:
						return Properties.Resources.labelProgramTask;
                    case (int)TaskCommands.CmdTouchDefaultProgram:
					case (int)TaskCommands.CmdDefaultProgram:
						return Properties.Resources.labelDefaultTask;
					case (int)TaskCommands.CmdSubtitles:
						return Properties.Resources.labelSubtitleTask;
					case (int)TaskCommands.CmdControlSchedule:
						return Properties.Resources.labelControlTask;
				}

			}
			catch { }
			return TaskCommands.CmdUndefined.ToString();
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return Binding.DoNothing;
		}

		#endregion
	}

    class LastConnectedConvert : IMultiValueConverter
    {
        #region IValueConverter 멤버

        public object Convert(object[] value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                if (System.Convert.ToInt32(value[0]) == 0)
                {
                    TimeSpan ts = DateTime.UtcNow - new DateTime(System.Convert.ToInt64(value[1]));
                    return ts.ToString(@"d\.hh\:mm\:ss");
                }
                else
                {
                    return "-";
                }
            }
            catch
            {
				return Properties.Resources.labelUnknown;
            }
        }

        public object[] ConvertBack(object value, Type[] targetType, object parameter, CultureInfo culture)
        {
            return null;
        }

        #endregion
    }

	class DataWithStateConvert : IMultiValueConverter
	{
		#region IValueConverter 멤버

		public object Convert(object[] value, Type targetType, object parameter, CultureInfo culture)
		{
			try
			{/*
				if (System.Convert.ToInt32(value[0]) == 0)
				{
					return Properties.Resources.labelUnknown;
				}
				else*/
				{
					return System.Convert.ToString(value[1]);
				}
			}
			catch
			{
				return Properties.Resources.labelUnknown;
			}
		}

		public object[] ConvertBack(object value, Type[] targetType, object parameter, CultureInfo culture)
		{
			return null;
		}

		#endregion
	}

	class UTPTimeConvert : IValueConverter
	{
		#region IValueConverter 멤버

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			try
			{
				long data = System.Convert.ToInt64(value);

				return TimeConverter.ConvertFromUTP(data).ToLocalTime().ToString();

			}
			catch
			{
				return "";
			}
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return Binding.DoNothing;
		}

		#endregion
	}

	class StatusConvert : IValueConverter
	{
		#region IValueConverter 멤버

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (value != null)
			{
				string path = value.ToString();
				return (path.Equals("1")) ? "OnLine" : "OffLine";
			}
			else
			{
				return "OffLine";
			}
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return Binding.DoNothing;
		}

		#endregion
	}

	class AvailableWoLConvert : IValueConverter
	{
		#region IValueConverter 멤버

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			try
			{
				if (!ServerDataCache.GetInstance.IsScheduleHandlerEnabled) return System.Windows.Visibility.Collapsed;

				int path = System.Convert.ToInt32(value);
				return ((path & (int)PowerMntType.WoL) == (int)PowerMntType.WoL) ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed;
			}
			catch { return System.Windows.Visibility.Collapsed; }
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return Binding.DoNothing;
		}

		#endregion
	}

	class AvailableAMTConvert : IValueConverter
	{
		#region IValueConverter 멤버

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			try
			{
				if (!ServerDataCache.GetInstance.IsScheduleHandlerEnabled) return System.Windows.Visibility.Collapsed;

				int path = System.Convert.ToInt32(value);
				return ((path & (int)PowerMntType.AMT) == (int)PowerMntType.AMT) ? System.Windows.Visibility.Visible : System.Windows.Visibility.Collapsed;
			}
			catch { return System.Windows.Visibility.Collapsed; }
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return Binding.DoNothing;
		}

		#endregion
	}

	class RateWithStateConvert : IMultiValueConverter
	{
		#region IValueConverter 멤버

		public object Convert(object[] value, Type targetType, object parameter, CultureInfo culture)
		{
			try
			{
                /*
				if (System.Convert.ToInt32(value[0]) == 0)
				{
					return Properties.Resources.labelUnknown;
				}
				else*/
				{
					int rate = System.Convert.ToInt32(value[1]);

					if (rate >= 0)
					{

						return rate + " %";
					}
					throw new Exception();
				}
			}
			catch
			{
				return Properties.Resources.labelUnknown;
			}
		}

		public object[] ConvertBack(object value, Type[] targetType, object parameter, CultureInfo culture)
		{
			return null;
		}

		#endregion
	}

	class RateConvert : IValueConverter
	{
		#region IValueConverter 멤버

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			try
			{
				int rate = System.Convert.ToInt32(value);

				if (rate >= 0)
				{
					
					return rate + " %";
				}
				throw new Exception();
			}
			catch
			{
				return Properties.Resources.labelUnknown;
			}
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			return Binding.DoNothing;
		}

		#endregion
	}

	class FreeSpaceConvert : IMultiValueConverter
	{
		#region IValueConverter 멤버

		public object Convert(object[] value, Type targetType, object parameter, CultureInfo culture)
		{
			try
			{/*
				if (System.Convert.ToInt32(value[0]) == 0)
				{
					return Properties.Resources.labelUnknown;
				}
				else*/
				{
					if (value[1] != null)
					{
						string size = value[1].ToString().ToLower();
						return DigitalSignage.Scheduler.Window1.ConvertDisplayFileSize(size);
					}
					else
					{
						return Properties.Resources.labelUnknown;
					}
				}
			}
			catch
			{
				return Properties.Resources.labelUnknown;
			}
		}

		public object[] ConvertBack(object value, Type[] targetType, object parameter, CultureInfo culture)
		{
			return null;
		}

		#endregion
	}

	class StatusImageWithStateConvert : IMultiValueConverter
	{
		#region IValueConverter 멤버

		public object Convert(object[] value, Type targetType, object parameter, CultureInfo culture)
		{
			ImageSourceConverter conv = new ImageSourceConverter();
			try
			{
				string path = @"pack://application:,,,/images/icon_status_off.png";

				if (System.Convert.ToInt32(value[0]) == 0)
				{
					return conv.ConvertFromString(path);
				}
				else
				{
					if (value[1] != null)
					{
						string str_Status = value[1].ToString().ToLower();
						if (str_Status.Equals("1") || str_Status.Equals("true"))
						{
							path = @"pack://application:,,,/images/icon_status_on.png";
						}
						else
						{
							path = @"pack://application:,,,/images/icon_status_off.png";
						}

						return conv.ConvertFromString(path);
					}
					else
					{
						return conv.ConvertFromString(path);
					}
				}
			}
			catch
			{
				return conv.ConvertFromString(@"pack://application:,,,/images/icon_status_off.png");
			}
		}

		public object[] ConvertBack(object value, Type[] targetType, object parameter, CultureInfo culture)
		{
			return null;
		}

		#endregion
	}

    class StatusImageConvert : IValueConverter
    {

        #region IValueConverter 멤버

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                string str_Status = value.ToString().ToLower();
                string path = "";
                if (str_Status.Equals("1") || str_Status.Equals("true"))
                {
                    path = @"images/icon_status_on.png";
                }
				else if (String.IsNullOrEmpty(str_Status) || str_Status.Equals("-") || str_Status.Equals("-1"))
				{
					path = @"images/icon_status_none.png";
				}
				else
				{
					path = @"images/icon_status_off.png";
				}

                return path;
            }
            else
            {
				return @"images/icon_status_none.png";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }

        #endregion
    }

    class TreeSelectConvert : IValueConverter
    {

        #region IValueConverter 멤버

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                bool bvalue = (bool)value;
                if (bvalue == true)
                {
                    SolidColorBrush sb = new SolidColorBrush(Color.FromRgb(255,153,51));
                    return sb;
                }
                else
                    return Brushes.Black;
            }

            return Brushes.Black;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }

        #endregion
    }
}
