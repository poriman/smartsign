﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using NLog;
using System.Xml;

using Intel.vPro.AMT;
using System.ComponentModel;
using System.Windows.Threading;


namespace DigitalSignage.Scheduler
{
	/// <summary>
	/// Interaction logic for UserControl1.xaml
	/// </summary>
	public partial class AMTDetailsDlg : Window
	{
		private static Logger logger = LogManager.GetCurrentClassLogger();

		private  object _lockObject = new object();

		BackgroundWorker _bwRefresher = null;

		DispatcherTimer _tmRefresher = null;
		
		private XmlNode player_node = null;

		public AMTDetailsDlg(XmlNode p_node)
		{
			InitializeComponent();

			player_node = p_node;
		}

		private PowerState _currState = PowerState.UNKNOWN;
		
		#region Event Handler
		private void btnOK_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				if (_bwRefresher != null)
				{
					_bwRefresher.CancelAsync();
					_bwRefresher = null;
				}
				if (_tmRefresher != null)
				{
					_tmRefresher.Stop();
					_tmRefresher = null;
				}

				this.Close();
			}
			catch (Exception err)
			{
				logger.Error(err + "");
			}
		}

		private void btnCancel_Click(object sender, RoutedEventArgs e)
		{
			this.Close();
		}

		void tb_GotKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
		{
			try
			{
				TextBox tb = sender as TextBox;
				if (tb != null) tb.SelectAll();
				else
				{
					PasswordBox pb = sender as PasswordBox;
					pb.SelectAll();
				}

				e.Handled = true;
			}
			catch { }
		}
		#endregion

		private void Window_Loaded(object sender, RoutedEventArgs e)
		{
			try
			{
				this.playerName.Text = player_node.Attributes["name"].Value;
				this.ipAddr.Text = player_node.Attributes["_ipv4addr"].Value;

				RefreshStatus();
			}
			catch (Exception ex){ logger.Error(ex);}
		}

		private void RefreshStatus()
		{
			if (_bwRefresher == null)
			{
				_bwRefresher = new BackgroundWorker();

				_bwRefresher.WorkerSupportsCancellation = true;
				_bwRefresher.DoWork += new DoWorkEventHandler(_bwRefresher_DoWork);
				_bwRefresher.RunWorkerCompleted += new RunWorkerCompletedEventHandler(_bwRefresher_RunWorkerCompleted);
			}

			if (_tmRefresher == null)
			{
				_tmRefresher = new DispatcherTimer();
				_tmRefresher.Interval = TimeSpan.FromSeconds(30);
				_tmRefresher.Tick += new EventHandler(_tmRefresher_Tick);
				_tmRefresher.Start();
			}

			lock (_lockObject)
			{
				if (!_bwRefresher.IsBusy) _bwRefresher.RunWorkerAsync();
			}
		}

		void _tmRefresher_Tick(object sender, EventArgs e)
		{
			this.RefreshStatus();
		}

		void _bwRefresher_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			if (!e.Cancelled)
			{
				try
				{
					PowerState ps = (PowerState)e.Result;
					this.state.Text = ps.ToString();

					if (_currState != ps)
					{
						InsertText(String.Format(Properties.Resources.labelChangedStatus, ps));
						_currState = ps;
					}
				}
				catch { }
			}
			else
			{
				if (_tmRefresher != null)
				{
					_tmRefresher.Stop();
					_tmRefresher = null;
				}
			}
		}

		void _bwRefresher_DoWork(object sender, DoWorkEventArgs e)
		{
			try
			{
				lock (_lockObject)
				{
					if (e.Cancel == true) return;

					CVProCtrl vPro = new CVProCtrl();
					e.Result = vPro.GetPowerState(player_node.Attributes["_ipv4addr"].Value,
						player_node.Attributes["_auth_id"].Value,
						player_node.Attributes["_auth_pass"].Value);
				}
			}
			catch { e.Result = PowerState.UNKNOWN; }
		}

		private void btnRefresh_Click(object sender, RoutedEventArgs e)
		{
			RefreshStatus();
		}

		private void btnCommand_Click(object sender, RoutedEventArgs e)
		{
			//if (rbPowerOff.IsChecked == true)
			//{

			//}
			//else if (rbPowerOn.IsChecked == true)
			//{

			//}
			//else if (rbReboot.IsChecked == true)
			//{
			//    throw new NotImplementedException();
			//}
			//else if (rbRemoteViewer.IsChecked == true)
			//{
			//}

		}

		private void PowerOff_Click(object sender, RoutedEventArgs e)
		{
			lock (_lockObject)
			{
				CVProCtrl vPro = new CVProCtrl();
				if (vPro.SetPowerOff(player_node.Attributes["_ipv4addr"].Value,
				player_node.Attributes["_auth_id"].Value,
				player_node.Attributes["_auth_pass"].Value))
				{
					InsertText(String.Format(Properties.Resources.labelRunCommand, Properties.Resources.labelCtrlPowerOff));
				}
				else
				{
					InsertText(String.Format(Properties.Resources.labelFailedCommand, Properties.Resources.labelCtrlPowerOff));
				}
			}
			//	바로 보내봤자 변경된 스테이터스가 안넘어오더라..
			//			RefreshStatus();
		}
		private void PowerOn_Click(object sender, RoutedEventArgs e)
		{
			lock (_lockObject)
			{
				CVProCtrl vPro = new CVProCtrl();
				if(vPro.SetPowerOn(player_node.Attributes["_ipv4addr"].Value,
				player_node.Attributes["_auth_id"].Value,
				player_node.Attributes["_auth_pass"].Value))
				{
					InsertText(String.Format(Properties.Resources.labelRunCommand, Properties.Resources.labelCtrlPowerOn));
				}
				else
				{
					InsertText(String.Format(Properties.Resources.labelFailedCommand, Properties.Resources.labelCtrlPowerOn));
				}
			}
			//	바로 보내봤자 변경된 스테이터스가 안넘어오더라..
			//			RefreshStatus();
		}
		private void RemoteViewer_Click(object sender, RoutedEventArgs e)
		{
			bool bRedirectProxy = false;
			if (ServerDataCache.GetInstance.IsRunningProxyServer)
			{
				MessageBoxResult re = MessageBox.Show(Properties.Resources.mbProxy, Properties.Resources.titleMessageBox, MessageBoxButton.YesNoCancel, MessageBoxImage.Question);

				if (re == MessageBoxResult.Cancel)
					return;
				else if (re == MessageBoxResult.Yes) bRedirectProxy = true;
				else bRedirectProxy = false;
			}

			if (bRedirectProxy)
			{
				String hostip = Config.GetConfig.ServerPath.Replace("tcp://", "").Replace(":888", "").TrimEnd('/');
				//	프록시 서버를 거침
				if (null != VNCLauncher.RunVncViewer(player_node.Attributes["_ipv4addr"].Value,
					"5900", hostip, "5901", player_node.Attributes["_auth_pass"].Value, true, true, false, false, 50))
				{
					InsertText(String.Format(Properties.Resources.labelRunCommand, Properties.Resources.menuitemRemoteView));
				}
				else
				{
					InsertText(String.Format(Properties.Resources.labelFailedCommand, Properties.Resources.menuitemRemoteView));
				}
			}
			else
			{
				//	프록시 서버를 거치지 않음
				if (null != VNCLauncher.RunVncViewer(player_node.Attributes["_ipv4addr"].Value,
					"5900", player_node.Attributes["_auth_pass"].Value, true, true, false, false, 50))
				{
					InsertText(String.Format(Properties.Resources.labelRunCommand, Properties.Resources.menuitemRemoteView));
				}
				else
				{
					InsertText(String.Format(Properties.Resources.labelFailedCommand, Properties.Resources.menuitemRemoteView));
				}
			}
			//	바로 보내봤자 변경된 스테이터스가 안넘어오더라..
			//			RefreshStatus();
		}

		private void InsertText(string sMessage)
		{
			if (String.IsNullOrEmpty(this.tbSummary.Text))
			{
				this.tbSummary.Text = sMessage;
			}
			else
			{
				this.tbSummary.Text = (sMessage + "\r\n" + this.tbSummary.Text);
			}
		}

		private void Window_Unloaded(object sender, RoutedEventArgs e)
		{
			try
			{
				if (_bwRefresher != null)
				{
					_bwRefresher.CancelAsync();
					_bwRefresher = null;
				}
				if (_tmRefresher != null)
				{
					_tmRefresher.Stop();
					_tmRefresher = null;
				}

			}
			catch (Exception err)
			{
				logger.Error(err + "");
			}
		}
	}
}
