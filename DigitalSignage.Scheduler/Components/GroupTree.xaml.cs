﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Reflection;

using NLog;
using DigitalSignage.Common;
// using DigitalSignage.DataBase;

using System.Drawing;
using System.Windows.Interop;
using System.Collections.ObjectModel;

using System.Xml;
using System.Xml.XPath;

namespace DigitalSignage.Scheduler
{
    /// <summary>
    /// Interaction logic for GroupTree.xaml
    /// </summary>
    public partial class GroupTree : Grid
    {

#region Custom Commands
        public static RoutedUICommand AddGroup = new RoutedUICommand("Add new group", "AddGroup", typeof(GroupTree));
        public static RoutedUICommand EditGroup = new RoutedUICommand("Edit group", "EditGroup", typeof(GroupTree));
        public static RoutedUICommand DeleteGroup = new RoutedUICommand("Delete group", "DeleteGroup", typeof(GroupTree));
#endregion

#region Events
        public delegate void SelectionChanged(object sender, TreeViewItem newSelection);
        public event SelectionChanged OnSelectionChanged = null;

		public event RoutedEventHandler event_AddLimitPlayers;
		public event RoutedEventHandler event_DeleteLimitPlayers;

		public event RoutedEventHandler event_User;
		public event RoutedEventHandler event_FindPlayers;
		public event RoutedEventHandler event_TaskWizard;
		public event RoutedEventHandler event_NewTask;
		public event RoutedEventHandler event_Subtitle;
		public event RoutedEventHandler event_DefaultTask;
		public event RoutedEventHandler event_EditPlayer;
		public event RoutedEventHandler event_DeletePlayer;
		public event RoutedEventHandler event_ViewScreen;
		public event RoutedEventHandler event_AMTDetails;
		public event RoutedEventHandler event_Control;

		public event RoutedEventHandler event_PowerOn;
		public event RoutedEventHandler event_PowerOff;
		public event RoutedEventHandler event_Restart;


#endregion Events

        private Window1 parentWnd;
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private Config cfg = null ;
        private bool showPlayers = true;
        private bool hsaPopupMenu = true;
		public bool HasPopupMenu
		{
			get { return hsaPopupMenu; }
			set { hsaPopupMenu = value; }
		}

        private int ExpandDepth
        {
            get
            {
                try
                {
                    int nDepth = Properties.Settings.Default.Expand_Depth;
                    return nDepth;
                }
                catch { return 2; }
            }
        }      
        
		/// <summary>
		/// 세부 정보를 볼수 잇는지 체크
		/// </summary>
		private bool showDetail = false;
		public bool ShowDetailInformation
		{
			get { return showDetail; }
			set { showDetail = value; }
		}

		private bool bCheckBox = false;
		public bool HasCheckBox
		{
			get { return bCheckBox; }
			set { bCheckBox = value; }
		}

		public void TreeState(bool bExpand, int nMaxDepth)
		{
			foreach(TreeViewItem item in GroupTreeView.Items)
			{
				item.IsExpanded = bExpand;
				if(item.HasItems)
				{
					ChildTreeState(item, bExpand, nMaxDepth, 1);
				}
			}
		}

        public void TreeState(bool bExpand)
        {
            foreach (TreeViewItem item in GroupTreeView.Items)
            {
                item.IsExpanded = bExpand;
                if (item.HasItems)
                {
                    ChildTreeState(item, bExpand);
                }
            }
        }

        private void ChildTreeState(TreeViewItem parent_item, bool bExpand, int nMaxDepth, int nDepth)
		{
            
			foreach (TreeViewItem item in parent_item.Items)
			{
                if(nMaxDepth > nDepth) item.IsExpanded = bExpand;

				if (item.HasItems)
				{
					ChildTreeState(item, bExpand, nMaxDepth, nDepth + 1);
				}
			}
		}

		private void ChildTreeState(TreeViewItem parent_item, bool bExpand)
		{
            
			foreach (TreeViewItem item in parent_item.Items)
			{
				item.IsExpanded = bExpand;

				if (item.HasItems)
				{
					ChildTreeState(item, bExpand);
				}
			}
		}

		public Collection<TreeViewItem> CheckedItems
		{
			get 
			{
				Collection<TreeViewItem> arrChecked = new Collection<TreeViewItem>();

				ItemCollection items = GroupTreeView.Items;

				foreach(TreeViewItem item in items)
				{
					GroupTreeItem gtv = item.Header as GroupTreeItem;                    

					if (gtv != null && gtv.IsChecked)
						arrChecked.Add(item);
					else if (item.HasItems)
					{
						GetCheckedItem(item, ref arrChecked);
					}

				}

				return arrChecked.Count == 0 ? null : arrChecked;
			}
		}

		private void GetCheckedItem(TreeViewItem parent, ref Collection<TreeViewItem> items)
		{
			if (parent == null)
				return;

			foreach (TreeViewItem item in parent.Items)
			{
				GroupTreeItem gtv = item.Header as GroupTreeItem;

				if (gtv != null && gtv.IsChecked)
					items.Add(item);
				else if (item.HasItems)
				{
					GetCheckedItem(item, ref items);
				}
			}

		}

		public void SetCheckedItem(TreeViewItem parent, ref Collection<XmlNode> items)
		{
			if (items.Count == 0)
				return;
            
			//	일단 parent 아이템 체크 검사
			XmlNode node = parent.DataContext as XmlNode;

			bool bContains = false;

			foreach (XmlNode n in items)
			{
				if (!node.LocalName.Equals(n.LocalName)) continue;

				if ((node.LocalName.Equals("player") && node.Attributes["pid"].Value.Equals(n.Attributes["pid"].Value)) ||
					(node.LocalName.Equals("group") && node.Attributes["gid"].Value.Equals(n.Attributes["gid"].Value)))
				{
					bContains = true;
					break;
				}
			}

			if (bContains)
			{
				GroupTreeItem gtv = parent.Header as GroupTreeItem;

				if (gtv != null)
				{
					gtv.IsChecked = true;
					parent.BringIntoView();
				}
			}

			//	하위 아이템이 있으면 재귀호출
			if (parent.HasItems)
			{
				foreach (TreeViewItem item in parent.Items)
				{
					SetCheckedItem(item, ref items);
				}
			}
		}

        public GroupTree()
        {
            InitializeComponent();
            // changing selection style
        }

        public void SetParent(Window1 parent, Config config )
        {
            this.parentWnd = parent;
            cfg = config;
            UpdateTree();
        }

        public void SetParent(Config config)
        {
            cfg = config;
            hsaPopupMenu = false;
            UpdateTree();
        }    

		public void UpdateTree()
		{
			using (new DigitalSignage.Common.WaitCursor())
			{  
                String unique_id = String.Empty;               
                
				//item.Clear();

				ServerDataCache objCache = ServerDataCache.GetInstance;
				showPlayers = showPlayers && objCache.HasPermission("VIEW PLAYER");

				try
				{
					int nTotalOnline = 0;

                    if (GroupTreeView.HasItems)
                    {
                        //UpdateTree(null, objCache.NetworkTree.ChildNodes[0], out nTotalOnline);
                        UpdateTree(null, SortTreeviewNode(objCache.NetworkTree.DocumentElement), out nTotalOnline);
                    }                    
                    else
                    {
                        //MakeTree(null, objCache.NetworkTree.ChildNodes[0], out nTotalOnline);
                        MakeTree(null, SortTreeviewNode(objCache.NetworkTree.DocumentElement), out nTotalOnline);
                       
                    }                       
				}
				catch { }
			}
		}

        public XmlNode SortTreeviewNode(XmlNode rootNode)
        {
            SortElements(rootNode);

            foreach (XmlNode childNode in rootNode.ChildNodes)
            {
                SortTreeviewNode(childNode);
            }
           
            return rootNode;
        }

        /// <summary>
        /// Treeview 노드들의 이름을 비교하여 오름차순으로 정렬하는 함수(버블정렬)
        /// </summary>
        /// <param name="node">Treeview 아이템</param>
        public void SortElements(XmlNode node)
        {
            bool bChanged = true;

            while (bChanged)
            {
                bChanged = false;

                for (int i = 1; i < node.ChildNodes.Count; i++)
                {
                    if (node.ChildNodes[i].LocalName != node.ChildNodes[i - 1].LocalName)
                    {
                        if (String.Compare(node.ChildNodes[i].LocalName, node.ChildNodes[i - 1].LocalName) < 0)
                        {
                            node.InsertBefore(node.ChildNodes[i], node.ChildNodes[i - 1]);
                            bChanged = true;
                        }
                    }
                    else if (String.Compare(node.ChildNodes[i].Attributes["name"].Value, node.ChildNodes[i - 1].Attributes["name"].Value, false) < 0)
                    {
                        //Replace:
                        node.InsertBefore(node.ChildNodes[i], node.ChildNodes[i - 1]);
                        bChanged = true;
                    }
                }
            }
        }        

        /// <summary>
        /// 트리 업데이트
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="parent_node"></param>
        /// <param name="cntOnline"></param>
        /// <returns></returns>
        public int UpdateTree(TreeViewItem parent, XmlNode parent_node, out int cntOnline)
        {
            bool bSuperAdmin = ServerDataCache.GetInstance.HasPermission("ADMINISTRATOR");

            cntOnline = 0;
            int cntPlayers = 0;

            ItemCollection colTrees = null;
            if (parent == null) colTrees = GroupTreeView.Items;
            else colTrees = parent.Items;
            int idxCurrNode = 0;

            foreach (XmlNode node in parent_node.ChildNodes)
            {
                TreeSelectConvert converter = new TreeSelectConvert();
                Binding bind = new Binding("IsSelected");
                bind.Converter = converter;
                
                if (node.LocalName == "group" && node.Attributes["del_yn"].Value.Equals("N"))
                {
                    String header = "";
                    String tooltip = "";
                    String name = node.Attributes["name"].Value;
                    String gid = node.Attributes["gid"].Value;

                    bool bHasNode = false;
 
                    #region 노드 업데이트 : 노드가 있다면 갱신한다.
                    foreach (TreeViewItem ti in colTrees)
                    {
                        try
                        {
                            XmlNode desc_node = ti.DataContext as XmlNode;
                            if (desc_node != null && desc_node.LocalName == "group")
                            {
                                if (gid.Equals(desc_node.Attributes["gid"].Value))
                                {
                                    ti.DataContext = node;

                                    int cntOnlineInGroup = 0;
                                    int cntPlayersInGroup = UpdateTree(ti, node, out cntOnlineInGroup);

                                    if (!showDetail)
                                    {
                                        header = String.Format("{0}", name);
                                        tooltip = null;
                                    }
                                    else
                                    {
                                        header = String.Format("{0} ({1}/{2})", name, cntOnlineInGroup, cntPlayersInGroup);
                                        tooltip = String.Format(Properties.Resources.tooltipGroupItem, name, String.Format("{0}/{1}", cntOnlineInGroup, cntPlayersInGroup));
                                    }
                                    GroupTreeItem gti = ti.Header as GroupTreeItem;

                                    gti.Text = header;

                                    if (showDetail) gti.TextToolTip = tooltip;

                                    cntOnline += cntOnlineInGroup;
                                    cntPlayers += cntPlayersInGroup;

                                    bHasNode = true;

                                    break;
                                }
                            }
                        }
                        catch { }
                    }
            #endregion

                    if (!bHasNode)
                    {
                        #region 노드 생성 : 노드가 없다면 생성해준다
                        TreeViewItem gi = new TreeViewItem();
                        GroupTreeItem ggti = new GroupTreeItem(true, header, bCheckBox);

                        int cntOnlineInGroup = 0;
                        int cntPlayersInGroup = UpdateTree(gi, node, out cntOnlineInGroup);

                        if (!showDetail)
                        {
                            header = String.Format("{0}", name);
                            tooltip = null;
                        }
                        else
                        {
                            header = String.Format("{0} ({1}/{2})", name, cntOnlineInGroup, cntPlayersInGroup);
                            tooltip = String.Format(Properties.Resources.tooltipGroupItem, name, String.Format("{0}/{1}", cntOnlineInGroup, cntPlayersInGroup));
                        }

                        ggti.Text = header;
                        ggti.DataContext = gi;
                        bind.Source = gi;
                        gi.SetBinding(TreeViewItem.ForegroundProperty, bind);

                        gi.FontWeight = System.Windows.FontWeights.Bold;
                        gi.Header = ggti;
                        gi.DataContext = node;

                        if (showDetail) ggti.TextToolTip = tooltip;

                        /// ybkim 
                        if (colTrees.Count - 1 < idxCurrNode)
                        {
                            colTrees.Add(gi);
                        }
                        else
                        {
                            colTrees.Insert(idxCurrNode, gi);
                        }


                        cntOnline += cntOnlineInGroup;
                        cntPlayers += cntPlayersInGroup;
                        #endregion
                    }
                    idxCurrNode++;
 
                }
                else if (showPlayers && node.LocalName == "player" && node.Attributes["del_yn"].Value.Equals("N"))
                {
                    String header;
                    String tooltip;
                    String name = node.Attributes["name"].Value;
                    String pid = node.Attributes["pid"].Value;
                    bool bOnline = node.Attributes["state"].Value.Equals("1");

                    if (bSuperAdmin)
                    {
                        if (!showDetail) header = String.Format("{0}", name);
                        else header = String.Format("{0} ({1})", name, pid);
                    }
                    else header = String.Format("{0}", name);

                    tooltip = String.Format(Properties.Resources.tooltipPlayerItem, name, pid, bOnline ? Properties.Resources.labelFTPOn : Properties.Resources.labelFTPOff);

                    bool bHasNode = false;

                    #region 노드 업데이트 : 노드가 있다면 갱신한다.
                    foreach (TreeViewItem ti in colTrees)
                    {
                        try
                        {
                            XmlNode desc_node = ti.DataContext as XmlNode;
                            if (desc_node != null && desc_node.LocalName == "player")
                            {
                                if (pid.Equals(desc_node.Attributes["pid"].Value))
                                {
                                    GroupTreeItem gti = ti.Header as GroupTreeItem;

                                    gti.Text = header;
                                    if (showDetail) gti.TextToolTip = tooltip;

                                    ti.DataContext = node;

                                    if (node.Attributes["state"].Value.Equals("1")) cntOnline++;

                                    cntPlayers++;

                                    bHasNode = true;

                                    break;
                                }
                            }
                        }
                        catch { }
                    }
                    #endregion

                    if (!bHasNode)
                    {
                        #region 노드 생성 : 노드가 없다면 생성해준다

                        TreeViewItem pi = new TreeViewItem();
                        GroupTreeItem pgti = new GroupTreeItem(false, header, bCheckBox);

                        pgti.DataContext = pi;
                        if (showDetail) pgti.TextToolTip = tooltip;

                        bind.Source = pi;
                        pi.SetBinding(TreeViewItem.ForegroundProperty, bind);

                        pi.FontWeight = System.Windows.FontWeights.Normal;
                        pi.Header = pgti;
                        pi.DataContext = node;


                        /// ybkim 
                        if (colTrees.Count - 1 < idxCurrNode)
                        {
                            colTrees.Add(pi);
                        }
                        else
                        {
                            colTrees.Insert(idxCurrNode, pi);
                        }

                        if (node.Attributes["state"].Value.Equals("1")) cntOnline++;

                        cntPlayers++;
                        #endregion
                    }
 
                    idxCurrNode++;
                }
            }

            List<TreeViewItem> arrWillBeDeleted = new List<TreeViewItem>();

            foreach (TreeViewItem ti in colTrees)
            {
                bool bDelete = true;
                foreach (XmlNode node in parent_node.ChildNodes)
                {
                    if (node.Equals(ti.DataContext))
                    {
                        bDelete = false;
                        break;
                    }
                }

                if (bDelete)
                {
                    arrWillBeDeleted.Add(ti);
                }
            }

            foreach (TreeViewItem del in arrWillBeDeleted)
            {
                colTrees.Remove(del);
            }

            return cntPlayers;
        }

		/// <summary>
		/// 트리를 만든다.
		/// </summary>
		/// <param name="parent">부모 트리</param>
		/// <param name="parent_node">부모 XML 노드</param>
		/// <returns>하위의 플레이어 개수</returns>
        public int MakeTree(TreeViewItem parent, XmlNode parent_node, out int cntOnline)
        {
            bool bSuperAdmin = ServerDataCache.GetInstance.HasPermission("ADMINISTRATOR");
            cntOnline = 0;
            int cntPlayers = 0;

            foreach (XmlNode node in parent_node.ChildNodes)
            {
                TreeSelectConvert converter = new TreeSelectConvert();
                Binding bind = new Binding("IsSelected");
                bind.Converter = converter;

                if (node.LocalName == "group" && node.Attributes["del_yn"].Value.Equals("N"))
                {
                    String header = "";
                    String tooltip = "";
                    String name = node.Attributes["name"].Value;

                    TreeViewItem gi = new TreeViewItem();
                    GroupTreeItem ggti = new GroupTreeItem(true, header, bCheckBox);                                        

                    int cntOnlineInGroup = 0;
                    int cntPlayersInGroup = MakeTree(gi, node, out cntOnlineInGroup);

                    if (!showDetail)
                    {
                        header = String.Format("{0}", name);
                        tooltip = null;
                    }
                    else
                    {
                        header = String.Format("{0} ({1}/{2})", name, cntOnlineInGroup, cntPlayersInGroup);
                        tooltip = String.Format(Properties.Resources.tooltipGroupItem, name, String.Format("{0}/{1}", cntOnlineInGroup, cntPlayersInGroup));
                    }

                    ggti.Text = header;
                    ggti.DataContext = gi;
                    bind.Source = gi;
                    gi.SetBinding(TreeViewItem.ForegroundProperty, bind);

                    gi.FontWeight = System.Windows.FontWeights.Bold;
                    gi.Header = ggti;
                    gi.DataContext = node;

                    if (showDetail) ggti.TextToolTip = tooltip;

                    if (parent == null)
                    {
                        GroupTreeView.Items.Add(gi);                        
                    }
                    else
                    {
                        parent.Items.Add(gi);      
                    }                                    

                    cntOnline += cntOnlineInGroup;
                    cntPlayers += cntPlayersInGroup;
                }
                else if (showPlayers && node.LocalName == "player" && node.Attributes["del_yn"].Value.Equals("N"))
                {
                    String header;
                    String tooltip;
                    String name = node.Attributes["name"].Value;
                    String pid = node.Attributes["pid"].Value;
                    bool bOnline = node.Attributes["state"].Value.Equals("1");

                    if (bSuperAdmin)
                    {
                        if (!showDetail) header = String.Format("{0}", name);
                        else header = String.Format("{0} ({1})", name, pid);
                    }
                    else header = String.Format("{0}", name);

                    tooltip = String.Format(Properties.Resources.tooltipPlayerItem, name, pid, bOnline ? Properties.Resources.labelFTPOn : Properties.Resources.labelFTPOff);

                    TreeViewItem pi = new TreeViewItem();
                    GroupTreeItem pgti = new GroupTreeItem(false, header, bCheckBox);

                    pgti.DataContext = pi;
                    if (showDetail) pgti.TextToolTip = tooltip;

                    bind.Source = pi;
                    pi.SetBinding(TreeViewItem.ForegroundProperty, bind);

                    pi.FontWeight = System.Windows.FontWeights.Normal;
                    pi.Header = pgti;
                    pi.DataContext = node;

                    if (parent == null)
                    {
                        GroupTreeView.Items.Add(pi);
                    }
                    else
                    {
                        parent.Items.Add(pi);
                    }                       

                    if (node.Attributes["state"].Value.Equals("1")) cntOnline++;

                    cntPlayers++;
                }
            }

            TreeState(true, ExpandDepth);

            return cntPlayers;
        }

        #region Properties

        public bool ShowPlayers
        {
            get
            {
                return showPlayers;
            }
            set
            {
                showPlayers = value;
                UpdateTree();
            }
        }

        public Object SelectedItem
        {
            get
            {
                TreeViewItem item = (TreeViewItem)GroupTreeView.SelectedItem;
                if ( item == null )
                    return null;
                else
                    return item.DataContext;
            }
            set
            {
				try
				{
					if(value as XmlNode != null)
					{
						XmlNode nodeValue = value as XmlNode;
						// changing selection
						DependencyObject dObject = null;

						for (int i = 0; i < GroupTreeView.Items.Count; i++)
						{
							XmlNode node = ((TreeViewItem)GroupTreeView.Items[i]).DataContext as XmlNode;

							try
							{
								if (node.LocalName.Equals("player") && nodeValue.LocalName.Equals("player") && node.Attributes["pid"].Value.Equals(nodeValue.Attributes["pid"].Value))
								{
									dObject = (DependencyObject)GroupTreeView.Items[i];
									break;
								}
								else if (node.LocalName.Equals("group") && nodeValue.LocalName.Equals("group") && node.Attributes["gid"].Value.Equals(nodeValue.Attributes["gid"].Value))
								{
									dObject = (DependencyObject)GroupTreeView.Items[i];
									break;
								}
							}
							catch { }

							if (!node.LocalName.Equals("group"))
								continue;

							if (SelectTree((TreeViewItem)GroupTreeView.Items[i], nodeValue, ref dObject)) break;
						}
						if (dObject == null) return;

						//uncomment the following line if UI updates are unnecessary
						((TreeViewItem)dObject).IsSelected = true;
						((TreeViewItem)dObject).IsExpanded = true;
						((TreeViewItem)dObject).BringIntoView();

						if (HasCheckBox)
						{
							try
							{
								((GroupTreeItem)((TreeViewItem)dObject).Header).IsChecked = true;
							}
							catch { }
						}

						MethodInfo selectMethod =
						   typeof(TreeViewItem).GetMethod("Select",
						   BindingFlags.NonPublic | BindingFlags.Instance);

						selectMethod.Invoke(dObject, new object[] { true });
					}
					else
					{
						Collection<XmlNode> nodes = value as Collection<XmlNode>;
						if(nodes != null)
						{
							this.SetCheckedItem(GroupTreeView.Items[0] as TreeViewItem, ref nodes);
						}
					}
				}
				catch { }				

			}
        }

        public XmlNode SelectedGroup
        {
            get
            {
                XmlNode item = SelectedItem as XmlNode;
                if (item == null) return null;
				if (item.LocalName.Equals("group"))
                    return item;
                else
                    return null;

            }
            set
            {
                try
                {
                    // changing selection
                    DependencyObject dObject = null;

					for (int i = 0; i < GroupTreeView.Items.Count; i++)
					{
						XmlNode node = ((TreeViewItem)GroupTreeView.Items[i]).DataContext as XmlNode;
						if (!node.LocalName.Equals("group"))
							continue;

						try
						{
							if (value.LocalName.Equals("group") && node.Attributes["gid"].Value.Equals(value.Attributes["gid"].Value))
							{
								dObject = (DependencyObject)GroupTreeView.Items[i];
								break;
							}
						}
						catch { }
						
						if (SelectGroup((TreeViewItem)GroupTreeView.Items[i], value, ref dObject)) break;
					}
					if (dObject == null) return;

                    //uncomment the following line if UI updates are unnecessary
                    ((TreeViewItem)dObject).IsSelected = true;
					((TreeViewItem)dObject).BringIntoView();

                    MethodInfo selectMethod =
                       typeof(TreeViewItem).GetMethod("Select",
                       BindingFlags.NonPublic | BindingFlags.Instance);

                    selectMethod.Invoke(dObject, new object[] { true });
                }
                catch { }
            }
        }

		bool SelectTree(TreeViewItem parent_item, XmlNode _value, ref DependencyObject dObject)
		{
			foreach (TreeViewItem item in parent_item.Items)
			{
				XmlNode node = item.DataContext as XmlNode;

				if (node.LocalName.Equals("group"))
				{
					if (_value.LocalName.Equals("group") && node.Attributes["gid"].Value.Equals(_value.Attributes["gid"].Value))
					{
						dObject = (DependencyObject)item;
						return true;
					}
					else
					{
						if (SelectTree(item, _value, ref dObject)) return true;
					}

				}
				else
				{
					if (_value.LocalName.Equals("player") && node.Attributes["pid"].Value.Equals(_value.Attributes["pid"].Value))
					{
						dObject = (DependencyObject)item;
						return true;
					}
				}
			}
			dObject = null;

			return false;
		}

		bool SelectGroup(TreeViewItem parent_item, XmlNode _value, ref DependencyObject dObject)
		{
			foreach (TreeViewItem item in parent_item.Items)
			{
				XmlNode node = item.DataContext as XmlNode;

				if (node.LocalName.Equals("group"))
				{
					if (_value.LocalName.Equals("group") && node.Attributes["gid"].Value.Equals(_value.Attributes["gid"].Value))
					{
						dObject = (DependencyObject)item;
						return true;
					}
					else
					{
						if (SelectGroup(item, _value, ref dObject)) return true;
					}

				}
				else continue;

			}
			dObject = null;

			return false;
		}

        public string SelectPlayerID
        {
            get
            {
				if (SelectedItem == null) return "-1";
				XmlNode node = SelectedItem as XmlNode;

				if (node != null && node.LocalName.Equals("player"))
					return node.Attributes["pid"].Value;

//                 Object item = SelectedItem;
//                 if (item == null) return "-1";
//                 if (item.GetType().Equals(typeof(DataSet.playersRow)))
//                     return ((DataSet.playersRow)item).pid;
                else
                    return "-1";
            }
        }

		public string SelectedGroupGID
        {
            get
            {
                if (SelectedItem == null) return "-1";
				XmlNode node = SelectedItem as XmlNode;

				if (node != null && node.LocalName.Equals("group"))
					return node.Attributes["gid"].Value;
//                 if (item.GetType().Equals(typeof(DataSet.groupsRow)))
//                     return ((DataSet.groupsRow)item).gid;
                else
                    return "-1";

            }
            set
            {
                try
                {
                    // changing selection
                    DependencyObject dObject = null;

					XmlNode _value = ServerDataCache.GetInstance.GetGroupByGID(value);

					for (int i = 0; i < GroupTreeView.Items.Count; i++)
					{
						XmlNode node = ((TreeViewItem)GroupTreeView.Items[i]).DataContext as XmlNode;
						if (!node.LocalName.Equals("group"))
							continue;

						if (node.Attributes["gid"].Value.Equals(value))
						{
							dObject = (DependencyObject)GroupTreeView.Items[i];
							break;
						}

						if (SelectGroup((TreeViewItem)GroupTreeView.Items[i], _value, ref dObject)) break;

					}
					
					if (dObject == null) return;

                    //uncomment the following line if UI updates are unnecessary
                    ((TreeViewItem)dObject).IsSelected = true;
					((TreeViewItem)dObject).BringIntoView();
					

                    MethodInfo selectMethod =
                       typeof(TreeViewItem).GetMethod("Select",
                       BindingFlags.NonPublic | BindingFlags.Instance);

                    selectMethod.Invoke(dObject, new object[] { true });
                }
                catch { }
            }
        }

        /*public TreeViewItem selectedItem
        {
            get { return ()GroupTreeView.SelectedItem; }
            set { }
        }*/
        #endregion

		#region BitmapResourceToImage
		[System.Runtime.InteropServices.DllImport("gdi32.dll")]
		public static extern bool DeleteObject(IntPtr hObject);

		private System.Windows.Controls.Image GetIconImage(System.Drawing.Bitmap image)
		{
			IntPtr hBitmap = IntPtr.Zero;
			System.Windows.Controls.Image iconf = null;
			try
			{
				hBitmap = image.GetHbitmap();

				iconf = new System.Windows.Controls.Image();
				iconf.Source = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(hBitmap,
											IntPtr.Zero, new Int32Rect(0, 0, image.Width, image.Height),
											BitmapSizeOptions.FromEmptyOptions());
				iconf.Width = 16;
				iconf.Height = 16;

				return iconf;
			}
			catch { iconf = null; }
			finally
			{
				if (hBitmap != IntPtr.Zero) DeleteObject(hBitmap);
			}

			return iconf;
		}
		#endregion

		#region event handlers
		private void GPTree_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
 
            try
            {
				TreeViewItem item = null;
				try
				{
					Type source = e.Source.GetType();
					if (source != null && source.Equals(typeof(GroupTreeItem)))
					{
						GroupTreeItem gti = e.Source as GroupTreeItem;
						item = gti.DataContext as TreeViewItem;
						}
					else if (source != null && source.Equals(typeof(TreeViewItem)))
					{
						item = e.Source as TreeViewItem;
					}

				}
				catch
				{
				}

				bool bIsGroup = false;
				XmlNode node = null;

                if (item != null)
                {
                    if (item.IsSelected == false)
                        item.IsSelected = true;

					node = item.DataContext as XmlNode;
					bIsGroup = node.LocalName.Equals("group");
                }

				if (hsaPopupMenu == false)
					return;

				ContextMenu contextMenu = new System.Windows.Controls.ContextMenu();
                contextMenu.Items.Clear();

				Separator sp = null;
				MenuItem mi = null;

				ServerDataCache sdc = ServerDataCache.GetInstance;

				mi = new MenuItem();
				mi.Header = Properties.Resources.menuitemRefresh;
				mi.Icon = GetIconImage(Properties.Resources.network_group);
				mi.IsEnabled = true;
				mi.Click += new RoutedEventHandler(mi_TreeRefreshClick);
				contextMenu.Items.Add(mi);

                sp = new Separator();
                contextMenu.Items.Add(sp);

				//	Task 
				if (item != null && item.Header.ToString().Length > 0)
				{
					bool perm_CreateTask = sdc.HasPermission("CREATE SCHEDULE");
                    if (cfg.IsAvailableComplexPane)
                    {
                        mi = new MenuItem();
                        mi.Header = Properties.Resources.menuitemTaskWizard;
                        mi.Icon = GetIconImage(Properties.Resources.icon_task_wizard);
                        mi.IsEnabled = perm_CreateTask;
                        mi.Click += new RoutedEventHandler(OnClick_TaskWizard);
                        contextMenu.Items.Add(mi);
                    }

					mi = new MenuItem();
					mi.Header = Properties.Resources.menuitemAddDefaultSchedule;
					mi.IsEnabled = perm_CreateTask;
					mi.Icon = GetIconImage(Properties.Resources.icon_default);
					mi.Click += new RoutedEventHandler(OnClick_DefaultTask);
					contextMenu.Items.Add(mi);
					mi = new MenuItem();
					mi.Header = Properties.Resources.menuitemAddTaskProgram;
					mi.Icon = GetIconImage(Properties.Resources.icon_add_program_task);
					mi.IsEnabled = perm_CreateTask;
					mi.Click += new RoutedEventHandler(OnClick_NewTask);
					contextMenu.Items.Add(mi);

					if (cfg.IsAvailableSubtitleSchedule)
					{
						mi = new MenuItem();
						mi.Header = Properties.Resources.menuitemAddSubtitle;
						mi.IsEnabled = perm_CreateTask;
						mi.Icon = GetIconImage(Properties.Resources.icon_add_subtitle_task);
						mi.Click += new RoutedEventHandler(OnClick_Subtitle);
						contextMenu.Items.Add(mi);
					}

					if (cfg.IsAvailableControlSchedule)
					{
						mi = new MenuItem();
						mi.Header = Properties.Resources.menuitemAddControl;
						mi.IsEnabled = perm_CreateTask;
                        mi.Icon = GetIconImage(Properties.Resources.icon_add_control_task);
						mi.Click += new RoutedEventHandler(OnClick_Control);
						contextMenu.Items.Add(mi);
					}

					sp = new Separator();
					contextMenu.Items.Add(sp);
				}

				//	Group
                mi = new MenuItem();
				mi.Header = Properties.Resources.menuitemAddGroup;
				mi.Icon = GetIconImage(Properties.Resources.icon_add_group);
				mi.Command = GroupTree.AddGroup;
				mi.IsEnabled = bIsGroup && sdc.HasPermission("CREATE GROUP");
                mi.CommandTarget = this;
                contextMenu.Items.Add(mi);

				if (item != null && item.Header.ToString().Length > 0)
				{
					if (bIsGroup)
					{
						mi = new MenuItem();
						mi.Header = Properties.Resources.menuitemEditGroup;
						mi.Command = GroupTree.EditGroup;
						mi.CommandTarget = item;
						mi.Icon = GetIconImage(Properties.Resources.icon_edit_group);
						mi.IsEnabled = sdc.HasPermission("UPDATE GROUP");
						contextMenu.Items.Add(mi);

                        if (node.Attributes["gid"].Value != Config.ROOT_GROUP_ID) // we can't delete root group
						{
							mi = new MenuItem();
							mi.Header = Properties.Resources.menuitemDeleteGroup;
							mi.Command = GroupTree.DeleteGroup;
							mi.CommandTarget = item;
							mi.Icon = GetIconImage(Properties.Resources.icon_delete_group);
							mi.IsEnabled = sdc.HasPermission("DELETE GROUP");
							contextMenu.Items.Add(mi);
						}
					}
					sp = new Separator();
					contextMenu.Items.Add(sp);

				}

				//	Player
				// SE인 경우 플레이어가 여러개일 수 없기 때문에 포함되지 않는다.
				if(cfg.ProductCode != DigitalSignage.SerialKey.SerialKey._ProductCode.StandaloneEdition)
				{
					mi = new MenuItem();
					mi.Header = Properties.Resources.menuitemAddPlayer;
					mi.Icon = GetIconImage(Properties.Resources.icon_add_player);
					mi.IsEnabled = bIsGroup && sdc.HasPermission("CREATE PLAYER");
					mi.Click += new RoutedEventHandler(mi_Click);
					contextMenu.Items.Add(mi);
				}

				if (item != null && item.Header.ToString().Length > 0)
				{
					if (!bIsGroup)
					{
						mi = new MenuItem();
						mi.Header = Properties.Resources.menuitemEditPlayer;
						mi.IsEnabled = sdc.HasPermission("UPDATE PLAYER");
						mi.Icon = GetIconImage(Properties.Resources.icon_edit_player);
						mi.Click += new RoutedEventHandler(OnClick_EditPlayer);
						contextMenu.Items.Add(mi);

						mi = new MenuItem();
						mi.Header = Properties.Resources.menuitemDeletePlayer;
						mi.IsEnabled = sdc.HasPermission("DELETE PLAYER");
						mi.Icon = GetIconImage(Properties.Resources.icon_delete_player);
						mi.Click += new RoutedEventHandler(OnClick_DeletePlayer);
						contextMenu.Items.Add(mi);
					}
					else
					{
						if (cfg.ProductCode != DigitalSignage.SerialKey.SerialKey._ProductCode.StandaloneEdition && 
							sdc.HasPermission("ADMINISTRATOR"))
						{
							sp = new Separator();
							contextMenu.Items.Add(sp);

							mi = new MenuItem();
							mi.Header = Properties.Resources.menuitemLimitPlayers;
							mi.Icon = GetIconImage(Properties.Resources.icon_LimitRegistration);
							mi.Click += new RoutedEventHandler(mi_AddLimitPlayersClick);
							mi.IsEnabled = sdc.HasPermission("ADMINISTRATOR");
							contextMenu.Items.Add(mi);

							mi = new MenuItem();
							mi.Header = Properties.Resources.menuitemDeleteLimitPlayers;
							mi.Icon = GetIconImage(Properties.Resources.icon_LimitRemove);
							mi.Click += new RoutedEventHandler(mi_DeleteLimitPlayersClick);
							mi.IsEnabled = sdc.HasPermission("ADMINISTRATOR");
							contextMenu.Items.Add(mi);

						}
					}

					if (ServerDataCache.GetInstance.IsRunningNPCUServer)
					{
						sp = new Separator();
						contextMenu.Items.Add(sp);

						mi = new MenuItem();
						mi.Header = Properties.Resources.labelPowerControl;
						mi.Icon = GetIconImage(Properties.Resources.power_on);
						mi.IsEnabled = sdc.HasPermission("ADMINISTRATOR");
						//            mi.Icon = GetIconImage(Properties.Resources.zoom_in16);
						contextMenu.Items.Add(mi);

						MenuItem submi = new MenuItem();
						submi.Header = Properties.Resources.labelCtrlPowerOn;
						submi.Icon = GetIconImage(Properties.Resources.power_on);
						submi.IsEnabled = sdc.HasPermission("ADMINISTRATOR");
						submi.Click += new RoutedEventHandler(submi_p_on_Click);
						mi.Items.Add(submi);

						submi = new MenuItem();
						submi.Header = Properties.Resources.labelCtrlPowerOff;
						submi.Icon = GetIconImage(Properties.Resources.power_off);
						submi.IsEnabled = sdc.HasPermission("ADMINISTRATOR");
						submi.Click += new RoutedEventHandler(submi_p_off_Click);
						mi.Items.Add(submi);

						submi = new MenuItem();
						submi.Header = Properties.Resources.labelCtrlReboot;
						submi.Icon = GetIconImage(Properties.Resources.power_restart);
						submi.IsEnabled = sdc.HasPermission("ADMINISTRATOR");
						submi.Click += new RoutedEventHandler(submi_restart_Click);
						mi.Items.Add(submi);
					}
				}

                if (!bIsGroup)
                {
                    sp = new Separator();
                    contextMenu.Items.Add(sp);

                    mi = new MenuItem();
                    mi.Header = Properties.Resources.menuitemRemoteView;
                    mi.IsEnabled = true;
                    mi.Icon = GetIconImage(Properties.Resources.icon_preview);
                    mi.Click += new RoutedEventHandler(OnClick_ViewScreen);
                    contextMenu.Items.Add(mi);
                }

				if (!bIsGroup && ServerDataCache.GetInstance.IsScheduleHandlerEnabled)
				{
					try
					{

						int path = System.Convert.ToInt32(node.Attributes["_mnttype"].Value);
						bool bIsAMT = ((path & (int)PowerMntType.AMT) == (int)PowerMntType.AMT);

						sp = new Separator();
						contextMenu.Items.Add(sp);
						
						mi = new MenuItem();
						mi.Header = Properties.Resources.menuitemAMTDetails;
						mi.IsEnabled = bIsAMT && sdc.HasPermission("ADMINISTRATOR");
						mi.Icon = GetIconImage(Properties.Resources.zoom_in16);
						mi.Click += new RoutedEventHandler(OnClick_AMTDetails);
						contextMenu.Items.Add(mi);
					}
					catch { }
		
				}

				sp = new Separator();
				contextMenu.Items.Add(sp);

				//	User Management

				mi = new MenuItem();
				mi.Header = Properties.Resources.menuitemUserManagement;
				mi.Icon = GetIconImage(Properties.Resources.icon_user_management);
				mi.IsEnabled = sdc.HasPermission("VIEW USER");
				mi.Click += new RoutedEventHandler(OnClick_User);
				contextMenu.Items.Add(mi);


                contextMenu.UpdateLayout();
                contextMenu.IsOpen = true;
            }
            catch (Exception err)
            {
                logger.Error(err + "");
            }
        }


        void mi_Click(object sender, RoutedEventArgs e)
        {
            Window1.AddPlayer.Execute(null, null);
        }

		void mi_TreeRefreshClick(object sender, RoutedEventArgs e)
		{
			UpdateTree();
		}		

		void mi_AddLimitPlayersClick(object sender, RoutedEventArgs e)
		{
			if (event_AddLimitPlayers != null)
				event_AddLimitPlayers(this, e);
		}

		void mi_DeleteLimitPlayersClick(object sender, RoutedEventArgs e)
		{
			if (event_DeleteLimitPlayers != null)
				event_DeleteLimitPlayers(this, e);
		}

		internal void OnClick_User(object sender, RoutedEventArgs e)
        {
            if (event_User != null)
                event_User(this, e);
        }

		internal void OnClick_FindPlayers(object sender, RoutedEventArgs e)
        {
            if (event_FindPlayers != null)
                event_FindPlayers(this, e);
        }

		internal void OnClick_TaskWizard(object sender, RoutedEventArgs e)
		{
			if (event_TaskWizard != null)
				event_TaskWizard(this, e);
		}
		
		internal void OnClick_NewTask(object sender, RoutedEventArgs e)
        {
            if (event_NewTask != null)
                event_NewTask(this, e);
        }

		internal void OnClick_Subtitle(object sender, RoutedEventArgs e)
        {
            if (event_Subtitle != null)
                event_Subtitle(this, e);
        }
		internal void OnClick_Control(object sender, RoutedEventArgs e)
		{
			if (event_Control != null)
				event_Control(this, e);
		}
		internal void OnClick_DefaultTask(object sender, RoutedEventArgs e)
        {
			if (event_DefaultTask != null)
				event_DefaultTask(this, e);
        }

		internal void OnClick_EditPlayer(object sender, RoutedEventArgs e)
        {
            if (event_EditPlayer != null)
                event_EditPlayer(this, e);
        }

		internal void OnClick_DeletePlayer(object sender, RoutedEventArgs e)
        {
            if (event_DeletePlayer != null)
                event_DeletePlayer(this, e);
        }

		internal void OnClick_ViewScreen(object sender, RoutedEventArgs e)
        {
            if (event_ViewScreen != null)
                event_ViewScreen(this, e);
        }

		internal void OnClick_AMTDetails(object sender, RoutedEventArgs e)
        {
            if (event_AMTDetails != null)
                event_AMTDetails(this, e);
        }

        void submi_p_on_Click(object sender, RoutedEventArgs e)
        {
            if (event_PowerOn != null)
                event_PowerOn(this, e);
        }

        void submi_p_off_Click(object sender, RoutedEventArgs e)
        {
            if (event_PowerOff != null)
                event_PowerOff(this, e);
        }

        void submi_restart_Click(object sender, RoutedEventArgs e)
        {
            if (event_Restart != null)
                event_Restart(this, e);
        }

        private void GPTree_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            try
            {
                if (OnSelectionChanged != null)
                    OnSelectionChanged(this, (TreeViewItem)((TreeView)sender).SelectedItem);                
            }
            catch (Exception err)
            {
                logger.Error(err + "");
            }
        }
        #endregion

        #region Group Commands
        public void CmdAddGroup(object sender, ExecutedRoutedEventArgs e)
        {
            try
            {
                //get default FTP server
				XmlNode parent_group_node = ServerDataCache.GetInstance.GetGroupByGID(SelectedGroupGID);

				if (parent_group_node != null)
				{
					NewGroupDlg dlg = new NewGroupDlg(Properties.Resources.menuitemAddGroup, SelectedGroupGID, false);

                    dlg.GroupID = Config.ROOT_GROUP_ID;
					dlg.GroupSource = parent_group_node.Attributes["source"].Value;
					dlg.GroupUser = parent_group_node.Attributes["username"].Value;
					dlg.GroupPassword = parent_group_node.Attributes["password"].Value;
					dlg.GroupName = Properties.Resources.labelDefaultGroupName;
					dlg.Owner = parentWnd;
					dlg.ShowDialog();

                    if (dlg.GroupName == null || dlg.DialogResult != true) return; //user has canceled operation

					using (new DigitalSignage.Common.WaitCursor())
					{

						int depth = Convert.ToInt32(parent_group_node.Attributes["depth"].Value) + 1;

						dlg.RegisterDownloadTime();
						if (null != (dlg.GroupID = cfg.ServerDataCacheList.AddGroup(dlg.GroupName, dlg.GroupSource, dlg.GroupUser, dlg.GroupPassword, dlg.Parent_GroupID, dlg.IsInherited ? "Y" : "N", depth, "0000000", dlg.GroupPath)))
						{
							try
							{
								///	그룹 생성 기록
								cfg.ServerLogList.InsertDetailLog(ServerDataCache.GetInstance.Connected_UserName, (int)(LogType.LogType_Managing | LogType.LogType_Proceed), "", "GROUP_CREATE", dlg.GroupName, 0, 0, TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()));
							}
							catch { }

							ServerDataCache.GetInstance.RefreshNetworkTreeInfo();
							UpdateTree();
						}
						else
						{
							try
							{
								///	그룹 생성 기록
								cfg.ServerLogList.InsertDetailLog(ServerDataCache.GetInstance.Connected_UserName, (int)(LogType.LogType_Managing | LogType.LogType_Failed), "", "GROUP_CREATE", dlg.GroupName, 0, 0, TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()));
							}
							catch { }
						}
					}

				}
            }
            catch (Exception err)
            {
                logger.Error(err + "");
            }

			e.Handled = true;
		}

        public void CmdEditGroup(object sender, ExecutedRoutedEventArgs e)
        {
			try
            {
                TreeViewItem item = (TreeViewItem)GroupTreeView.SelectedItem;
                if (item == null) return;
				XmlNode row = item.DataContext as XmlNode;
                //get default FTP server
				NewGroupDlg dlg = new NewGroupDlg(Properties.Resources.menuitemEditGroup, row.Attributes["parent_gid"].Value, true);

				dlg.GroupID = row.Attributes["gid"].Value;
				dlg.GroupSource = row.Attributes["source"].Value;
				dlg.GroupName = row.Attributes["name"].Value;
                dlg.GroupPassword = row.Attributes["password"].Value;
                dlg.GroupPath = row.Attributes["path"].Value;
				dlg.GroupUser = row.Attributes["username"].Value;
				dlg.IsInherited = row.Attributes["is_inherited"].Value.Equals("Y");

                dlg.Owner = this.parentWnd;
                dlg.ShowDialog();
                if (dlg.GroupName == null || dlg.DialogResult != true) return; //user has canceled operation

				using (new DigitalSignage.Common.WaitCursor())
				{

					int depth = 1;

					try
					{
						depth = cfg.ServerDataCacheList.GetGroupDepth(dlg.Parent_GroupID);
					}
					catch { }

					dlg.RegisterDownloadTime();

                    if (cfg.ServerDataCacheList.UpdateGroup(dlg.GroupID, dlg.GroupName, dlg.GroupSource, dlg.GroupUser, dlg.GroupPassword, dlg.Parent_GroupID, dlg.IsInherited ? "Y" : "N", depth, dlg.GroupPath))
					{
						try
						{
							///	그룹 수정 기록
							cfg.ServerLogList.InsertDetailLog(ServerDataCache.GetInstance.Connected_UserName, (int)(LogType.LogType_Managing | LogType.LogType_Proceed), "", "GROUP_UPDATE", dlg.GroupName, 0, 0, TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()));
						}
						catch { }

						ServerDataCache.GetInstance.RefreshNetworkTreeInfo();
						UpdateTree();
					}
					else
					{
						try
						{
							///	그룹 수정 기록
							cfg.ServerLogList.InsertDetailLog(ServerDataCache.GetInstance.Connected_UserName, (int)(LogType.LogType_Managing | LogType.LogType_Failed), "", "GROUP_UPDATE", dlg.GroupName, 0, 0, TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()));
						}
						catch { }
					}


				}
            }
            catch (Exception err)
            {
                logger.Error(err + "");
            }

			e.Handled = true;
		}

        public void CmdDeleteGroup(object sender, ExecutedRoutedEventArgs e)
        {
            try
            {
                TreeViewItem item = (TreeViewItem)GroupTreeView.SelectedItem;
                if (item == null) return;
                XmlNode row = item.DataContext as XmlNode;
                if ((row == null) || !row.LocalName.Equals("group") || (row.Attributes["gid"].Value == Config.ROOT_GROUP_ID)) // this is main group, we can't delete them
                    return;

				if (ServerDataCache.GetInstance.HasChild(row, true))
				{
					MessageBox.Show(Properties.Resources.mbErrorDeleteGroup, Properties.Resources.titleMessageBox);
					return;
				}

				MessageBoxResult res = MessageBox.Show(String.Format(Properties.Resources.mbDelete, row.Attributes["name"].Value),
					Properties.Resources.titleMessageBox, MessageBoxButton.YesNo);
                if (res == MessageBoxResult.Yes)
                {
					using (new DigitalSignage.Common.WaitCursor())
					{

						if (cfg.ServerDataCacheList.DeleteGroup(row.Attributes["gid"].Value))
						{
							try
							{
								///	그룹 삭제 기록
								cfg.ServerLogList.InsertDetailLog(ServerDataCache.GetInstance.Connected_UserName, (int)(LogType.LogType_Managing | LogType.LogType_Proceed), "", "GROUP_DELETE", row.Attributes["name"].Value, 0, 0, TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()));
							}
							catch { }
							ServerDataCache.GetInstance.RefreshNetworkTreeInfo();
							UpdateTree();
						}
						else
						{
							try
							{
								///	그룹 삭제 기록
								cfg.ServerLogList.InsertDetailLog(ServerDataCache.GetInstance.Connected_UserName, (int)(LogType.LogType_Managing | LogType.LogType_Failed), "", "GROUP_DELETE", row.Attributes["name"].Value, 0, 0, TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()));
							}
							catch { }
						}
					}
                }
            }
            catch (Exception err)
            {
                logger.Error(err + "");
            }

			e.Handled = true;
		}
        #endregion
    }
}
