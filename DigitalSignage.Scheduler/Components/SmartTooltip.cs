﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;

namespace DigitalSignage.Scheduler
{
	public class SmartTooltip : ToolTip
	{
        public SmartTooltip()
		{
            //DefaultStyleKeyProperty.OverrideMetadata(typeof(ToolTip), new FrameworkPropertyMetadata(typeof(ToolTip)));
		}

        #region ToolTipText

        public static readonly DependencyProperty ToolTipTextProperty =
            DependencyProperty.Register("ToolTipText", typeof(string), typeof(SmartTooltip),
				new FrameworkPropertyMetadata((string)string.Empty));

        public string ToolTipText
		{
            get { return (string)GetValue(ToolTipTextProperty); }
            set { 
                SetValue(ToolTipTextProperty, value); 
                DrawToolTip(value);
            }
		}

        void DrawToolTip(String sText)
        {

            using (System.IO.StringReader sr = new System.IO.StringReader(sText))
            {
                String sLine = null;
                StackPanel sp = new StackPanel();
                sp.VerticalAlignment = System.Windows.VerticalAlignment.Stretch;
                sp.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
                sp.Orientation = Orientation.Vertical;
                sp.Margin = new Thickness(10, 5, 10, 5);
                int nIndex = 0;

                while ((sLine = sr.ReadLine()) != null)
                {
                    String[] arrSplit = sLine.Split(new String[] { "\\t" }, StringSplitOptions.None);
                    if (arrSplit.Length == 2)
                    {
                        StackPanel sp1 = new StackPanel();
                        sp1.Orientation = Orientation.Horizontal;
                        sp1.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
                        sp1.MinHeight = 20;
                        sp1.MaxHeight = 100;

                        TextBlock title = new TextBlock();
                        title.MinWidth = 80;
                        title.Foreground = this.Foreground;

                        title.Text = arrSplit[0].Trim();
                        title.TextAlignment = TextAlignment.Left;
                        title.FontWeight = FontWeights.Bold;

                        title.VerticalAlignment = System.Windows.VerticalAlignment.Stretch;
                        title.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;

                        TextBlock cont = new TextBlock();
                        cont.Margin = new Thickness(0, 0, 0, 2);
                        cont.Foreground = nIndex == 0 ? (Brush)Application.Current.Resources["ToolTipHighlightForegroundBrush"] : this.Foreground;

                        cont.Text = arrSplit[1].Trim();
                        cont.TextAlignment = TextAlignment.Left;
                        cont.VerticalAlignment = System.Windows.VerticalAlignment.Stretch;
                        cont.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
                        cont.MaxWidth = 300;
                        cont.FontWeight = nIndex == 0 ? FontWeights.Bold : FontWeights.Normal; 
                        cont.TextWrapping = TextWrapping.Wrap;

                        sp1.Children.Add(title);
                        sp1.Children.Add(cont);

                        sp.Children.Add(sp1);

                    }
                    else
                    {
                        StackPanel sp1 = new StackPanel();
                        sp1.Orientation = Orientation.Horizontal;
                        sp1.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
                        sp1.MinHeight = 20;
                        sp1.MaxHeight = 100;

                        TextBlock title = new TextBlock();
                        title.Foreground = this.Foreground;
                        title.Margin = new Thickness(0, 0, 0, 2);

                        title.Text = sLine.Trim();
                        title.TextAlignment = TextAlignment.Left;
                        //title.FontWeight = FontWeights.Bold;
                        title.VerticalAlignment = System.Windows.VerticalAlignment.Stretch;
                        title.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
                        title.MaxWidth = 400;
                        title.TextWrapping = TextWrapping.Wrap;

                        sp1.Children.Add(title);
                        sp.Children.Add(sp1);
                    }

                    this.Content = sp;
                    nIndex++;
                }

                sr.Dispose();
            }
            //

        }

		#endregion
	}
}
