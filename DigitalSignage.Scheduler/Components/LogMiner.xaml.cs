﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using DigitalSignage.ServerDatabase;
using DigitalSignage.Common;
using System.Collections.ObjectModel;
using System.Xml;
using System.IO;

namespace DigitalSignage.Scheduler
{
	/// <summary>
	/// Interaction logic for LogMiner.xaml
	/// </summary>
	public partial class LogMiner : Window
	{
		RequestLogs currRequest = null;

		ServerDataSet.tblLogsDataTable dtAll = null;
		string ordering = "DESC";
		string column = "reg_dt";

        /// <summary>
        /// 그룹 노드가 찍혀있는지
        /// </summary>
        bool HasCheckGroup
        {
            get
            {
                Collection<TreeViewItem> items = groupBox.CheckedItems;
                if (items != null)
                {
                    foreach (TreeViewItem item in items)
                    {
                        XmlNode node = item.DataContext as XmlNode;

                        if (node.LocalName.Equals("group")) return true;
                    }
                }
                else
                    return false;

                return false;
            }
        }

        Task selectedSchedule = null;
        Task SelectedTask
        {
            get { return selectedSchedule; }
            set
            {
                if (null != (selectedSchedule = value))
                {
                    rdScheduleName.Height = new GridLength(50);
                    gridScheduleName.Visibility = System.Windows.Visibility.Visible;

                    switch (selectedSchedule.type)
                    {
                        case TaskCommands.CmdDefaultProgram:
                        case TaskCommands.CmdTouchDefaultProgram:
                            cbLogScheduleType.SelectedIndex = 0;
                            break;
                        case TaskCommands.CmdProgram:
                            cbLogScheduleType.SelectedIndex = 1;
                            break;
                        case TaskCommands.CmdSubtitles:
                            cbLogScheduleType.SelectedIndex = 2;
                            break;
                        case TaskCommands.CmdControlSchedule:
                            cbLogScheduleType.SelectedIndex = 3;
                            break;
                    }

                    if(selectedSchedule.isAd)
                    {
                        if ((selectedSchedule.show_flag & (int)ShowFlag.Show_NonEvent) == 0)
                        {
                            /// 일반 재생 아님
                            citemNonEvent.IsEnabled = false;
                            citemEvent.IsSelected = true;
                        }
                        else if ((selectedSchedule.show_flag & (int)ShowFlag.Show_Event) == 0)
                        {
                            /// 이벤트가 아님
                            citemEvent.IsEnabled = false;

                            citemNonEvent.IsSelected = true;
                        }
                        citemNonAd.IsEnabled = false;

                        citemAd.IsSelected = true;
                    }
                    else
                    {
                        citemAd.IsEnabled = citemEvent.IsEnabled = false;

                        citemNonAd.IsSelected = citemNonEvent.IsSelected = true;
                    }
                    gridSchedule.IsEnabled = false;
                    cbiConnection.IsEnabled = false;
                    tbScheduleName.Text = selectedSchedule.description;
                }
                else
                {
                    rdScheduleName.Height = new GridLength(0);
                    gridScheduleName.Visibility = System.Windows.Visibility.Collapsed;
                    tbScheduleName.Text = String.Empty;
                }
            }
        }

		public LogMiner(XmlNode pNode)
		{
			InitializeComponent();

			DateTime now = DateTime.Now;
			dtStart.SelectedDateTime = new DateTime(now.Year, now.Month, now.Day, 0, 0, 0);
			dtEnd.SelectedDateTime = new DateTime(now.Year, now.Month, now.Day, 23, 59, 59);

			groupBox.HasPopupMenu = false;
			groupBox.SelectedItem = pNode;

            SelectedTask = null;
		}

        public LogMiner(XmlNode pNode, Task selected)
        {
            InitializeComponent();

            DateTime now = DateTime.Now;
            dtStart.SelectedDateTime = new DateTime(now.Year, now.Month, now.Day, 0, 0, 0);
            dtEnd.SelectedDateTime = new DateTime(now.Year, now.Month, now.Day, 23, 59, 59);

            groupBox.HasPopupMenu = false;
            groupBox.SelectedItem = pNode;

            SelectedTask = selected;
        }

		private void durationColumn_Click(object sender, RoutedEventArgs e)
		{
			SortColumn(sender, "duration");
			e.Handled = true;
		}
		private void nameColumn_Click(object sender, RoutedEventArgs e)
		{
			SortColumn(sender, "name");
			e.Handled = true;
		}
		private void codeColumn_Click(object sender, RoutedEventArgs e)
		{
			SortColumn(sender, "errorcd");
			e.Handled = true;
		}
		private void reg_dtColumn_Click(object sender, RoutedEventArgs e)
		{
			SortColumn(sender, "reg_dt");
			e.Handled = true;
		}
		private void descriptionColumn_Click(object sender, RoutedEventArgs e)
		{
			SortColumn(sender, "description");
			e.Handled = true;
		}
		private void typeColumn_Click(object sender, RoutedEventArgs e)
		{
			SortColumn(sender, "logcd");
			e.Handled = true;
		}
		private void pidColumn_Click(object sender, RoutedEventArgs e)
		{
			SortColumn(sender, "pid");
			e.Handled = true;
		}
        private void tagsColumn_Click(object sender, RoutedEventArgs e)
		{
			SortColumn(sender, "tags");
			e.Handled = true;
		}

		private void SortColumn(object sender, string gvCol)
		{
			try
			{
				using (new DigitalSignage.Common.WaitCursor())
				{
					if (dtAll != null && lvLogs != null)
					{
						GridViewColumnHeaderEx ex = sender as GridViewColumnHeaderEx;

						foreach (GridViewColumn col in gvLogs.Columns)
						{
							if (ex.Equals(col.Header))
								ex.Sort();
							else
							{
								GridViewColumnHeaderEx tmp = col.Header as GridViewColumnHeaderEx;
								if (tmp != null) tmp.UnSort();
							}
						}
						ServerDataSet.tblLogsDataTable tbl = dtAll;

						if (!ex.IsSorting) lvLogs.DataContext = dtAll;
						else if (ex.IsAscending)
						{
							ordering = "ASC";
							column = gvCol;
							lvLogs.DataContext = UpdateLogListByFiltering(tbl);
						}
						else
						{
							ordering = "DESC";
							column = gvCol;
							lvLogs.DataContext = UpdateLogListByFiltering(tbl);
						}

						ListItemChecker();
					}
				}
			}
			catch { }
		}

		private void LogsListBox_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.Key == System.Windows.Input.Key.C)
			{
				if (Keyboard.Modifiers == ModifierKeys.Control)
				{
					CopySelectedLogsToClipboard();
					e.Handled = true;
				}
			}
		}

		private void CopySelectedLogsToClipboard()
		{
			try
			{
				if (lvLogs.SelectedItems != null)
				{
					string sClipboard = string.Empty;
					foreach (ServerDataSet.tblLogsRow item in lvLogs.SelectedItems)
					{
						sClipboard += GetLogInfo(item, "\t");
						sClipboard += "\r\n";
					}
					if (!String.IsNullOrEmpty(sClipboard))
					{
						Clipboard.Clear();
						Clipboard.SetText(sClipboard, TextDataFormat.Text);
					}
				}
			}
			catch (System.Exception ex)
			{
				MessageBox.Show(ex.Message, Properties.Resources.titleMessageBox, MessageBoxButton.OK, MessageBoxImage.Error);
			}
		}

		/// <summary>
		/// 클립보드에 사용할 해더 정보를 기록한다.
		/// </summary>
		/// <returns></returns>
		private string MakeHeader(String seperator)
		{
			String sReturn = String.Empty;
			//	등록 시간
			sReturn += hdRegTime.Content + seperator;
			//	플레이어 이름
			sReturn += hdTarget.Content + seperator;
			//	플레이어 ID
			sReturn += hdPlayerID.Content + seperator;
			//	처리 내역
			sReturn += hdIsSuccess.Content + seperator;
            if (GridViewColumnVisibilityManager.GetIsVisible(colTags))
            {
                //	요청된 태그
                sReturn += hdTags.Content + seperator;
            }
            
            //	스케줄 이름
			sReturn += hdName.Content + seperator;

            if (GridViewColumnVisibilityManager.GetIsVisible(colDuration))
            {
                //	재생 시간
                sReturn += hdDuration.Content + seperator;
            }

			if (GridViewColumnVisibilityManager.GetIsVisible(colIsAd))
			{
				//	광고 여부
				sReturn += hdIsAd.Content + seperator;
			}

			if (GridViewColumnVisibilityManager.GetIsVisible(colIsEvent))
			{
				//	이벤트 여부
				sReturn += hdIsEvent.Content + seperator;
			}
			//	상세
			sReturn += hdDescription.Content + seperator;
			//	에러 코드
			sReturn += hdCode.Content + seperator;

			//	로그 범위 정보
			sReturn += Properties.Resources.colLogRange + seperator;

			if (GridViewColumnVisibilityManager.GetIsVisible(colIsAd))
			{
				//	스케줄 항목
				sReturn += Properties.Resources.colSchedCategory + seperator;
			}

			return sReturn;
		}
		/// <summary>
		/// 로그 정보를 파싱하여 정보를 변환한다.
		/// </summary>
		/// <param name="node"></param>
		/// <param name="seperator"></param>
		/// <returns></returns>
		private String GetLogInfo(ServerDataSet.tblLogsRow node, String seperator)
		{
			try
			{
				String sReturn = String.Empty;

				//	등록 시간
				sReturn += TimeConverter.ConvertFromUTP(node.reg_dt).ToLocalTime().ToString() + seperator;

				//	플레이어 이름
				sReturn += ServerDataCache.GetInstance.GetPlayerName(node.pid) + seperator;

				//	플레이어 ID
				sReturn += node.pid + seperator;

				//	처리 내역
				if ((node.logcd & (int)LogType.LogType_Failed) > 0)
				{
					sReturn += Properties.Resources.comboLogResultFailed + seperator;
				}
				else
				{
					sReturn += Properties.Resources.comboLogResultSuccess + seperator;
				}

                if (GridViewColumnVisibilityManager.GetIsVisible(colTags))
                {
                    //	요청된 태그
                    sReturn += node.tags + seperator;
                }

				//	스케줄 이름
				if ((int)(node.logcd & (int)LogType.LogType_Connection) > 0)
				{
					sReturn += (node.errorcd == 0 ? "CONNECT" : "DISCONNECT") + seperator;
				}
				else sReturn += node.name + seperator;

				//	재생 시간
                if (GridViewColumnVisibilityManager.GetIsVisible(colDuration))
                {
                    sReturn += node.duration.ToString() + seperator;
                }

				if (GridViewColumnVisibilityManager.GetIsVisible(colIsAd))
				{
					//	광고 여부
					if ((node.logcd & (int)LogType.LogType_Ad) > 0)
					{
						sReturn += Properties.Resources.comboLogTypeAd + seperator;
					}
					else
					{
						sReturn += Properties.Resources.comboLogTypeNonAd + seperator;
					}
				}

				if (GridViewColumnVisibilityManager.GetIsVisible(colIsEvent))
				{
					//	이벤트 여부
					if ((node.logcd & (int)LogType.LogType_Event) > 0)
					{
						sReturn += Properties.Resources.comboLogTypeEvent + seperator;
					}
					else
					{
						sReturn += Properties.Resources.comboLogTypeGeneral + seperator;
					}
				}

				//	상세
				sReturn += node.description + seperator;

				//	에러 코드
				sReturn += ErrorCmd.GetErrorMsg(node.errorcd) + seperator;

				//	로그 범위 정보
				if ((node.logcd & (int)LogType.LogType_Connection) > 0)
				{
					sReturn += Properties.Resources.comboLogTypeConnection + seperator;
				}
				else if ((node.logcd & (int)LogType.LogType_Download) > 0)
				{
					sReturn += Properties.Resources.comboLogTypeDownload + seperator;
				}
				else
				{
					sReturn += Properties.Resources.comboLogTypePlay + seperator;
				}

				if (GridViewColumnVisibilityManager.GetIsVisible(colIsAd))
				{
					//	스케줄 항목
					if ((node.logcd & (int)LogType.LogType_RollingSched) > 0)
					{
						sReturn += Properties.Resources.comboitemDefaultSchedule + seperator;
					}
					else if ((node.logcd & (int)LogType.LogType_TimeSched) > 0)
					{
						sReturn += Properties.Resources.comboitemProgram + seperator;
					}
					else if ((node.logcd & (int)LogType.LogType_SubtitleSched) > 0)
					{
						sReturn += Properties.Resources.comboitemSubtitle + seperator;
					}
					else if ((node.logcd & (int)LogType.LogType_ControlSched) > 0)
					{
						sReturn += Properties.Resources.comboitemProgram + seperator;
					}
					else if ((node.logcd & (int)LogType.LogType_UrgentSched) > 0)
					{
						sReturn += Properties.Resources.comboitemUrgent + seperator;
					}
					else
					{
						sReturn += TaskCommands.CmdUndefined.ToString() + seperator;
					}
				}

				return sReturn;
			}
			catch
			{
				return String.Empty;
			}
		}

		/// <summary>
		/// 닫기 버튼 클릭
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void okButton_Click(object sender, RoutedEventArgs e)
		{
			this.Close();
		}


		private bool MakeConditions(out string[] sPids, out int[] nlogTypes)
		{
			Collection<String> arrPids = new Collection<string>();
			Collection<int> arrLogTypes = new Collection<int>();

			#region PlayerIDS
			Collection<TreeViewItem> items = groupBox.CheckedItems;
			if (items == null)
			{
				sPids = null;
				nlogTypes = null;
				MessageBox.Show(Properties.Resources.mbSelectPlayerOrGroup, Properties.Resources.titleMessageBox, MessageBoxButton.OK, MessageBoxImage.Error);

				return false;

			}
			else
			{
				foreach (TreeViewItem item in items)
				{
					XmlNode node = item.DataContext as XmlNode;

					if (node.LocalName.Equals("player"))
					{
						if (!arrPids.Contains(node.Attributes["pid"].Value))
							arrPids.Add(node.Attributes["pid"].Value);
					}
					else
					{
						foreach (XmlNode player in node.SelectNodes("descendant::player[attribute::del_yn=\"N\"]"))
						{
							if (!arrPids.Contains(player.Attributes["pid"].Value))
								arrPids.Add(player.Attributes["pid"].Value);
						}
					}
				}
			}
			if (arrPids.Count == 0)
			{
				sPids = null;
				nlogTypes = null;

				MessageBox.Show(Properties.Resources.mbSelectPlayerOrGroup, Properties.Resources.titleMessageBox, MessageBoxButton.OK, MessageBoxImage.Error);
				return false;
			}
			#endregion

			int nLogType = 0;
			switch (cbLogCategory.SelectedIndex)
			{
				case 0: //	접속
					nLogType |= (int)LogType.LogType_Connection;
					break;

				case 1: //	재생
					
					switch(cbLogScheduleType.SelectedIndex)
					{
						case 0:	//	반복
							nLogType |= (int) LogType.LogType_RollingSched;
							break;
						case 1: //	시간
							nLogType |= (int) LogType.LogType_TimeSched;
							break;
						case 2: //	자막
							nLogType |= (int) LogType.LogType_SubtitleSched;
							break;
						case 3: //	제어
							nLogType |= (int) LogType.LogType_ControlSched;
							break;
					}

					break;

				case 2: //	다운로드
					nLogType |= (int)LogType.LogType_Download;

					switch (cbLogScheduleType.SelectedIndex)
					{
						case 0:	//	반복
							nLogType |= (int)LogType.LogType_RollingSched;
							break;
						case 1: //	시간
							nLogType |= (int)LogType.LogType_TimeSched;
							break;
						case 2: //	자막
							nLogType |= (int)LogType.LogType_SubtitleSched;
							break;
						case 3: //	제어
							nLogType |= (int)LogType.LogType_ControlSched;
							break;
					}
					break;
                case 3: //  타겟 광고
                    nLogType |= (int)LogType.LogType_TargetAd;
                    arrLogTypes.Add(nLogType);
                    
                    nlogTypes = arrLogTypes.ToArray();
			        sPids = arrPids.ToArray();
                    
                    return true;
			}

			if (cbLogCategory.SelectedIndex != 0) //	접속이 아닌 경우
			{
                /// 반복 / 시간 스케줄
                if (cbLogScheduleType.SelectedIndex == 0 || cbLogScheduleType.SelectedIndex == 1)
                {
                    if (cbLogScheduleType2.SelectedIndex == 0)	//	모두
                    {
                        arrLogTypes.Add(nLogType);
                        arrLogTypes.Add(nLogType | (int)LogType.LogType_Event);

                    }
                    else if (cbLogScheduleType2.SelectedIndex == 1)	//	일반
                    {
                        arrLogTypes.Add(nLogType);
                    }
                    else //	이벤트
                    {
                        arrLogTypes.Add(nLogType | (int)LogType.LogType_Event);
                    }

                    if (cbLogAd.SelectedIndex == 0)	//	모두
                    {
                        for (int i = arrLogTypes.Count - 1; i >= 0; --i)
                        {
                            arrLogTypes.Add(arrLogTypes[i] | (int)LogType.LogType_Ad);
                        }
                    }
                    else if (cbLogAd.SelectedIndex == 1)	//	광고
                    {
                        for (int i = arrLogTypes.Count - 1; i >= 0; --i)
                        {
                            arrLogTypes[i] |= (int)LogType.LogType_Ad;
                        }
                    }
                    else //	비광고
                    {
                        //	처리 없음
                    }
                }
                else
                {
                    /// 자막 / 제어 스케줄
                    arrLogTypes.Add(nLogType);
                }
			}

            //	성공 / 실패 부분
            if (cbLogDetail.SelectedIndex == 0) // 모두
            {
                if (arrLogTypes.Count == 0) arrLogTypes.Add(nLogType);

                for (int i = arrLogTypes.Count - 1; i >= 0; --i)
                {
                    arrLogTypes.Add(arrLogTypes[i] | (int)LogType.LogType_Failed);
                    arrLogTypes.Add(arrLogTypes[i] | (int)LogType.LogType_Proceed);
                    arrLogTypes[i] |= (int)LogType.LogType_Processing;
                }
            }
            else if (cbLogDetail.SelectedIndex == 1) //	성공 
            {
                if (arrLogTypes.Count == 0) arrLogTypes.Add(nLogType);

                for (int i = arrLogTypes.Count - 1; i >= 0; --i)
                {
                    arrLogTypes.Add(arrLogTypes[i] | (int)LogType.LogType_Proceed);
                    arrLogTypes[i] |= (int)LogType.LogType_Processing;
                }
            }
            else if (cbLogDetail.SelectedIndex == 2) // 실패
            {
                if (arrLogTypes.Count == 0) arrLogTypes.Add(nLogType);

                for (int i = arrLogTypes.Count - 1; i >= 0; --i)
                {
                    arrLogTypes[i] |= (int)LogType.LogType_Failed;
                }
            }
            
			nlogTypes = arrLogTypes.ToArray();
			sPids = arrPids.ToArray();

			return true;
		}

        private bool ValidationCheck()
        {
            try
            {
                if (dtStart.SelectedDateTime.Value.AddMonths(3) < dtEnd.SelectedDateTime.Value)
                {
                    MessageBox.Show(Properties.Resources.mbErrorConstraitThreeMonths, Properties.Resources.titleLogShower, MessageBoxButton.OK, MessageBoxImage.Warning);
                    dtStart.Focus();
                    return false;
                }
                else if (dtStart.SelectedDateTime.Value.AddMonths(1) < dtEnd.SelectedDateTime.Value && HasCheckGroup)
                {
                    MessageBox.Show(Properties.Resources.mbErrorConstraitOneMonths, Properties.Resources.titleLogShower, MessageBoxButton.OK, MessageBoxImage.Warning);
                    dtStart.Focus();
                    return false;
                }

                return true;
            }
            catch { return false; }
        }

		private void btnSearch_Click(object sender, RoutedEventArgs e)
		{
            if (!ValidationCheck()) return;

			this.IsEnabled = false;

			try
			{
                TimeSpan ts = dtEnd.SelectedDateTime.Value - dtStart.SelectedDateTime.Value;

				String[] PIDs;
				int[] LogTypes;
				if (MakeConditions(out PIDs, out LogTypes))
				{
					using (new DigitalSignage.Common.WaitCursor())
					{
						#region Request Logs 구성
						currRequest = new RequestLogs();
						currRequest.PlayerIds = PIDs;
						currRequest.Codes = LogTypes;

                        DateTime tempdt = dtStart.SelectedDateTime.Value;
                        DateTime startdt = new DateTime(tempdt.Year, tempdt.Month, tempdt.Day, 0,0,0);
                        tempdt = dtEnd.SelectedDateTime.Value;
                        DateTime enddt = new DateTime(tempdt.Year, tempdt.Month, tempdt.Day, 23,59,59);

                        currRequest.From = TimeConverter.ConvertToUTP(startdt.ToUniversalTime());
                        currRequest.To = TimeConverter.ConvertToUTP(enddt.ToUniversalTime());
						currRequest.Offset = 0;
						#endregion

                        if(SelectedTask == null)
						    dtAll = Config.GetConfig.ServerLogList.GetDetailLogsFromTo(currRequest.PlayerIds, currRequest.Codes,
							    currRequest.From, currRequest.To, currRequest.Limit, currRequest.Offset);
                        else
                            dtAll = Config.GetConfig.ServerLogList.GetDetailLogsFromTo(SelectedTask.uuid.ToString(), currRequest.PlayerIds, currRequest.Codes,
                                currRequest.From, currRequest.To, currRequest.Limit, currRequest.Offset);
						
						FillReturnedData();

						ShowAndHideColumnByCondition();

						lvLogs.DataContext = UpdateLogListByFiltering(dtAll);

						ListItemChecker();

					}
				}
			}
			catch { }

			this.IsEnabled = true;
		}

		/// <summary>
		/// 서버로 부터 받은 데이터의 빈공간을 일정한 규칙으로 채운다.
		/// </summary>
		private void FillReturnedData()
		{
			if (dtAll != null)
			{
				foreach (ServerDataSet.tblLogsRow row in dtAll)
				{
                    try
                    {
                        if ((int)(row.logcd & (int)LogType.LogType_Connection) > 0)
                        {

                            //row.reg_dt = row.end_dt;

                            if (row.errorcd == (int)ErrorCodes.IS_ERROR_CONN_DISCONNECTED ||
                                row.errorcd == (int)ErrorCodes.IS_ERROR_CONN_TIMEOUT_DISCONNECTED_BY_SERVER)
                                row.name = "DISCONNECT";
                            else
                                row.name = "CONNECT";
                        }
                        else
                            row.reg_dt = row.start_dt;

                        row.duration = row.end_dt - row.start_dt;
                        
                        if (row.duration < 0) row.duration = -1;
                    }
                    catch { }
				}
			}
		}

		private void RequestAdditionLogs()
		{
			this.IsEnabled = false;

			try
			{
				using (new DigitalSignage.Common.WaitCursor())
				{
					if (dtAll != null && currRequest != null)
					{
						int nCount = dtAll.Rows.Count;
						if (nCount % currRequest.Limit == 0)
						{
							currRequest.Offset += currRequest.Limit;
                            ServerDatabase.ServerDataSet.tblLogsDataTable logTemp = null;

                            if(SelectedTask == null)
							    logTemp = Config.GetConfig.ServerLogList.GetDetailLogsFromTo(currRequest.PlayerIds, currRequest.Codes,
								    currRequest.From, currRequest.To, currRequest.Limit, currRequest.Offset);
                            else
                                logTemp = Config.GetConfig.ServerLogList.GetDetailLogsFromTo(SelectedTask.uuid.ToString(), currRequest.PlayerIds, currRequest.Codes,
								    currRequest.From, currRequest.To, currRequest.Limit, currRequest.Offset);

							if (logTemp != null && logTemp.Rows.Count > 0)
							{
								dtAll.Merge(logTemp);

                                FillReturnedData();

								ShowAndHideColumnByCondition();

								lvLogs.DataContext = UpdateLogListByFiltering(dtAll);

								ListItemChecker();
							}
						}
					}
				}
			}
			catch { }

			this.IsEnabled = true;
		}

		/// <summary>
		/// 검색 결과를 갱신한다.
		/// </summary>
		private void UpdateSearchResultLabel()
		{
			String sLabel = String.Empty;

			if (dtAll != null && currRequest != null)
			{
				int nCount = dtAll.Rows.Count;
				bool bHasMoreRow = nCount != 0 && (nCount % currRequest.Limit == 0);

				if (String.IsNullOrEmpty(tbSearchBox.Text))
				{
					///	필터되지 않음
					sLabel = String.Format(Properties.Resources.labelSearchRows, nCount);
				}
				else
				{
					///	필터 됨
					sLabel = String.Format(Properties.Resources.labelSearchMatchs, lvLogs.Items.Count, nCount);
				}

				if (bHasMoreRow) sLabel += String.Format(" {0}", Properties.Resources.labelMoreRowExist);
			}

			lbSearchResult.Content = sLabel;
		}

		private void tbSearchBox_TextChanged(object sender, TextChangedEventArgs e)
		{
			if (!tbSearchBox.Text.Equals(hdName.Filter))
			{
				using (new DigitalSignage.Common.WaitCursor())
				{

					hdName.Filter = tbSearchBox.Text;

					int selected = lvLogs.SelectedIndex;
					lvLogs.DataContext = UpdateLogListByFiltering(dtAll);
					lvLogs.SelectedIndex = selected;

					ListItemChecker();
				}
			}
		}

		private ServerDataSet.tblLogsRow[] UpdateLogListByFiltering(ServerDataSet.tblLogsDataTable dt)
		{
			if (dt != null)
			{
				String sFilterExpression = String.Empty;

				if (!String.IsNullOrEmpty(hdName.Filter as String))
					sFilterExpression += String.Format("name LIKE '*{0}*' OR description LIKE '*{0}*'", hdName.Filter);

				return dt.Select(sFilterExpression, String.Format("{0} {1}", column, ordering)) as ServerDataSet.tblLogsRow[];

			}

			return null;

		}

		private void ShowAndHideColumnByCondition()
		{
			try
			{
                if (!gvLogs.Columns.Contains(colTags)) gvLogs.Columns.Insert(3, colTags);
                if (!gvLogs.Columns.Contains(colDuration)) gvLogs.Columns.Insert(4, colDuration);
                if (!gvLogs.Columns.Contains(colIsAd)) gvLogs.Columns.Insert(5, colIsAd);
                if (!gvLogs.Columns.Contains(colIsEvent)) gvLogs.Columns.Insert(6, colIsEvent);
                if (!gvLogs.Columns.Contains(colDescription)) gvLogs.Columns.Insert(7, colDescription);

				switch (cbLogCategory.SelectedIndex)
				{
					case 0:	//	접속
						hdName.Content = Properties.Resources.colName;
						hdDuration.Content = Properties.Resources.colConnectedTime;
                        GridViewColumnVisibilityManager.SetIsVisible(colTags, false);
                        GridViewColumnVisibilityManager.SetIsVisible(colDuration, false);
						GridViewColumnVisibilityManager.SetIsVisible(colIsAd, false);
						GridViewColumnVisibilityManager.SetIsVisible(colIsEvent, false);
						GridViewColumnVisibilityManager.SetIsVisible(colDescription, false);
						break;
					case 1: //	재생
						hdName.Content = Properties.Resources.colScheduleName;
						hdDuration.Content = Properties.Resources.colDuration;
                        GridViewColumnVisibilityManager.SetIsVisible(colTags, false);
                        GridViewColumnVisibilityManager.SetIsVisible(colDuration, true);
						GridViewColumnVisibilityManager.SetIsVisible(colIsAd, true);
						GridViewColumnVisibilityManager.SetIsVisible(colIsEvent, true);
						GridViewColumnVisibilityManager.SetIsVisible(colDescription, true);
						break;
					case 2: //	다운로드
						hdName.Content = Properties.Resources.colScheduleName;
						hdDuration.Content = Properties.Resources.colDownloadTime;
                        GridViewColumnVisibilityManager.SetIsVisible(colTags, false);
                        GridViewColumnVisibilityManager.SetIsVisible(colDuration, true);
						GridViewColumnVisibilityManager.SetIsVisible(colIsAd, true);
						GridViewColumnVisibilityManager.SetIsVisible(colIsEvent, true);
						GridViewColumnVisibilityManager.SetIsVisible(colDescription, true);
						break;
                    case 3: //  타겟 광고
						hdName.Content = Properties.Resources.colScheduleName;
						hdDuration.Content = Properties.Resources.colDuration;
                        GridViewColumnVisibilityManager.SetIsVisible(colTags, true);
                        GridViewColumnVisibilityManager.SetIsVisible(colDuration, true);
						GridViewColumnVisibilityManager.SetIsVisible(colIsAd, false);
						GridViewColumnVisibilityManager.SetIsVisible(colIsEvent, false);
						GridViewColumnVisibilityManager.SetIsVisible(colDescription, true);
                        break;

				}
			}
			catch { }
		}

		private void ListItemChecker()
		{
			lbResult.Visibility = lvLogs.Items.Count == 0 ? Visibility.Visible : Visibility.Hidden;
			UpdateSearchResultLabel();
		}

		private void cbLogCategory_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			try
			{
				switch (cbLogCategory.SelectedIndex)
				{
					case 0:	//	접속
						gridSchedule.Visibility = Visibility.Collapsed;
						gridScheduleType2.Visibility = Visibility.Collapsed;
						gridLogAd.Visibility = Visibility.Collapsed;
                        gridLogDetail.Visibility = Visibility.Visible;
						break;
					case 1: //	재생
					case 2: //	다운로드
						gridSchedule.Visibility = Visibility.Visible;
                        gridSchedule.IsEnabled = selectedSchedule == null;
						gridScheduleType2.Visibility = Visibility.Visible;
						gridLogAd.Visibility = Visibility.Visible;
                        gridLogDetail.Visibility = Visibility.Visible;
						break;
                    case 3: //  타겟 광고
						gridSchedule.Visibility = Visibility.Collapsed;
						gridScheduleType2.Visibility = Visibility.Collapsed;
						gridLogAd.Visibility = Visibility.Collapsed;
                        gridLogDetail.Visibility = Visibility.Collapsed;
						break;

				}
			}
			catch { }
		}

		private void cbLogScheduleType_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			try
			{
				switch (cbLogScheduleType.SelectedIndex)
				{
					case 0:	//	반복
					case 1: //	시간
						gridScheduleType2.Visibility = Visibility.Visible;
						gridLogAd.Visibility = Visibility.Visible;
						break;
					case 2: //	자막
					case 3: //	제어
						gridScheduleType2.Visibility = Visibility.Collapsed;
						gridLogAd.Visibility = Visibility.Collapsed;
						break;
				}
			}
			catch { }
		}



		private void btnSaveToClipboard_Click(object sender, RoutedEventArgs e)
		{
			this.IsEnabled = false;

			try
			{
				using (new DigitalSignage.Common.WaitCursor())
				{
					string sClipboard = MakeHeader("\t");
					sClipboard += "\r\n";
					foreach (ServerDataSet.tblLogsRow item in lvLogs.Items)
					{
						sClipboard += GetLogInfo(item, "\t");
						sClipboard += "\r\n";
					}
					if (!String.IsNullOrEmpty(sClipboard))
					{
						Clipboard.Clear();
						Clipboard.SetText(sClipboard, TextDataFormat.Text);
					}
				}
				MessageBox.Show(Properties.Resources.mbSuccessCopyToClipboard, Properties.Resources.titleLogShower, MessageBoxButton.OK, MessageBoxImage.Information);
			}
			catch (Exception ex)
			{
				MessageBox.Show(String.Format(Properties.Resources.mbErrorCopyToClipboard1, ex.Message), Properties.Resources.titleLogShower, MessageBoxButton.OK, MessageBoxImage.Error);
				
			}

			this.IsEnabled = true;

		}

		private void btnSaveToFile_Click(object sender, RoutedEventArgs e)
		{
			System.Windows.Forms.SaveFileDialog sfd = new System.Windows.Forms.SaveFileDialog();
			sfd.Title = Properties.Resources.labelSaveToFile;
			sfd.Filter = Properties.Resources.labelSaveLogFilter;
			sfd.OverwritePrompt = false;

			if (sfd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
			{
				this.IsEnabled = false;

				try
				{
					using (new DigitalSignage.Common.WaitCursor())
					{

						if (sfd.FilterIndex != 1)
						{
							using (StreamWriter tw = new StreamWriter(sfd.FileName, false))
							{
								///	Header 기록
								tw.WriteLine(MakeHeader("\t"));

								///	내용 기록
								foreach (ServerDataSet.tblLogsRow item in lvLogs.Items)
								{
									tw.WriteLine(GetLogInfo(item, "\t"));
								}
								tw.Close();
								tw.Dispose();
							}
						}
						else
						{
							Microsoft.Office.Interop.Excel.Application app = new Microsoft.Office.Interop.Excel.Application();
							app.Visible = false;

							Microsoft.Office.Interop.Excel.Workbook wb = app.Workbooks.Add(Type.Missing);
							Microsoft.Office.Interop.Excel.Worksheet ws = wb.Sheets["Sheet1"];

							#region Header 기록
							string sHeader = MakeHeader("\t");
							string[] arrHeader = sHeader.Split('\t');

							for (int i = 1; i <= arrHeader.Length; ++i)
							{
								ws.Cells[1, i] = arrHeader[i - 1];
							}
							ws.UsedRange.Font.Bold = true;
							ws.UsedRange.BorderAround(Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous, Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin, Microsoft.Office.Interop.Excel.XlColorIndex.xlColorIndexAutomatic, Type.Missing);

							#endregion

							#region 내용 기록
							int nRow = 2;
							foreach (ServerDataSet.tblLogsRow item in lvLogs.Items)
							{
								string contents = GetLogInfo(item, "\t");

								string[] arrContents = contents.Split('\t');

								for (int cell = 1; cell <= arrContents.Length; ++cell)
								{
									ws.Cells[nRow, cell] = arrContents[cell - 1];
								}

								nRow++;
							}
							#endregion

							ws.UsedRange.EntireColumn.AutoFit();
							ws.UsedRange.BorderAround(Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous, Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin, Microsoft.Office.Interop.Excel.XlColorIndex.xlColorIndexAutomatic, Type.Missing);

							wb.SaveAs(sfd.FileName, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing,
								Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlShared,
								Microsoft.Office.Interop.Excel.XlSaveConflictResolution.xlLocalSessionChanges, Type.Missing, Type.Missing, Type.Missing, Type.Missing);

							wb.Close(false, sfd.FileName, Type.Missing);
							wb = null;

							//if (app != null) // 작업후 프로세스가 남는 경우를 방지하기 위해서...
							//{
							//    System.Diagnostics.Process[] pProcess;
							//    pProcess = System.Diagnostics.Process.GetProcessesByName("Excel");
							//    pProcess[0].Kill();
							//}

							app = null;


						}
					}
					MessageBox.Show(Properties.Resources.mbSuccessCopyToFile, Properties.Resources.titleLogShower, MessageBoxButton.OK, MessageBoxImage.Information);
				}
				catch (Exception ex)
				{
					MessageBox.Show(String.Format(Properties.Resources.mbErrorCopyToFile, ex.Message), Properties.Resources.titleLogShower, MessageBoxButton.OK, MessageBoxImage.Error);

				} 
			}

			this.IsEnabled = true;
		}

		private void lvLogs_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if (lvLogs.SelectedIndex == lvLogs.Items.Count - 1)
			{
				RequestAdditionLogs();
			}
		}

		/// <summary>
		/// 로그 요청 클래스
		/// </summary>
		class RequestLogs
		{
			public string[] PlayerIds = null;
			public int[] Codes = null;
			public long From = 0;
			public long To = 0;

			public int Limit = 1000;
			public int Offset = 0;
		}

		System.Windows.Controls.Primitives.ScrollBar currVSB = null;

		private void lvLogs_Scroll(object sender, System.Windows.Controls.Primitives.ScrollEventArgs e)
		{
			if (currVSB == null)
			{
				currVSB = e.OriginalSource as System.Windows.Controls.Primitives.ScrollBar;

				if (currVSB != null && currVSB.Orientation == Orientation.Vertical)
				{
					currVSB.ValueChanged += new RoutedPropertyChangedEventHandler<double>(currVSB_ValueChanged);
				}
				else currVSB = null;
			}
		}

		void currVSB_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
		{
			try
			{
				if (currVSB.Value == currVSB.Maximum)
				{
					RequestAdditionLogs();
				}
			}
			catch { }
		}

        private void btnThreeDays_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DateTime dt = dtEnd.SelectedDateTime.Value;
				dtStart.SelectedDateTime = dtStart.MonthView.ViewDateTime = dt.AddDays(-3);
				
            }
            catch { }
        }
        private void btnFiftyDays_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DateTime dt = dtEnd.SelectedDateTime.Value;
				dtStart.SelectedDateTime = dtStart.MonthView.ViewDateTime = dt.AddDays(-15);
            }
            catch { }
        }
        private void btnOneMonth_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DateTime dt = dtEnd.SelectedDateTime.Value;
				dtStart.SelectedDateTime = dtStart.MonthView.ViewDateTime = dt.AddMonths(-1);
            }
            catch { }
        }
        private void btnThreeMonths_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                DateTime dt = dtEnd.SelectedDateTime.Value;
				dtStart.SelectedDateTime = dtStart.MonthView.ViewDateTime = dt.AddMonths(-3);
            }
            catch { }
        }

	}
}
