﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DigitalSignage.Scheduler
{
	public class TimeLineHeader : Control
	{
		static TimeLineHeader()
		{
			DefaultStyleKeyProperty.OverrideMetadata(typeof(TimeLineHeader), new FrameworkPropertyMetadata(typeof(TimeLineHeader)));
		}

		#region Header Name

		public static readonly DependencyProperty HeaderNameProperty =
			DependencyProperty.Register("HeaderName", typeof(string), typeof(TimeLineHeader),
				new FrameworkPropertyMetadata((string)string.Empty));

		public string HeaderName
		{
			get { return (string)GetValue(HeaderNameProperty); }
			set { SetValue(HeaderNameProperty, value); }
		}

        public static readonly DependencyProperty DescriptionVisibilityProperty =
            DependencyProperty.Register("DescriptionVisibility", typeof(Visibility), typeof(TimeLineHeader),
                new FrameworkPropertyMetadata((Visibility)Visibility.Collapsed));

        public Visibility DescriptionVisibility
		{
            get { return (Visibility)GetValue(DescriptionVisibilityProperty); }
            set { SetValue(DescriptionVisibilityProperty, value); }
		}
		#endregion
	}
}
