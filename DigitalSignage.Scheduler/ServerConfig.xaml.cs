﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;

using NLog;

using DigitalSignage.Common;

namespace DigitalSignage.Scheduler
{
    /// <summary>
    /// Interaction logic for NewDlgItem.xaml
    /// </summary>
    public partial class ServerConfig : Window
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        public bool result = false;
		BroadcastClient client = null;

		bool _bSearch = false;

        public ServerConfig(bool bDoSearch)
        {
            InitializeComponent();
			_bSearch = bDoSearch;
        }


        #region properties
        public String Hostname
        {
            get
            {
                return "tcp://"+hostname.Text+":888/";
            }
            set
            {
				try
				{
					string serverAddress = value;
					serverAddress = serverAddress.Substring(serverAddress.IndexOf("//")+2);
					serverAddress = serverAddress.Substring(0, serverAddress.IndexOf(":"));
					hostname.Text = serverAddress;
				}
				catch
				{
					logger.Error("The hostname will be set because of exceptions.\r\nValue = " + value);
					hostname.Text = value;
				}
            }
        }
        #endregion

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
			tbProduct.Text = Config.GetConfig.ProductCode.ToString();

			client = new BroadcastClient();

			client.BeginBroadCast += new EventHandler(client_BeginBroadCast);
			client.EndBroadCast += new EventHandler(client_EndBroadCast);
			client.RecvMessage += new SocketEventHandler(client_RecvMessage);
			client.Initialiaze();

			if(_bSearch)
			{
				btnSearch_Click(this, null);
			}

        }

		void tb_GotKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
		{
			try
			{
				TextBox tb = sender as TextBox;
				if (tb != null) tb.SelectAll();
				else
				{
					PasswordBox pb = sender as PasswordBox;
					pb.SelectAll();
				}

				e.Handled = true;
			}
			catch { }
		}
		public bool CheckConnection(string hostname, bool show_dialog)
		{
			try
			{
				SerialKey.NESerialKey._ProductCode localcode = Config.GetConfig.ProductCode;

				IDataCacheList serverDataCacheList = (IDataCacheList)Activator.GetObject(
					typeof(IDataCacheList), "tcp://" + hostname + ":888/DataCacheHost/rt");

				SerialKey.NESerialKey._ProductCode code = serverDataCacheList.GetProductCode();
				if (code != localcode && localcode != DigitalSignage.SerialKey.SerialKey._ProductCode.DebugEdition)
				{
					if (show_dialog) MessageBox.Show(String.Format(Properties.Resources.mbCannotConnectToServiceCauseProductCode, localcode, code));
					throw new Exception(Properties.Resources.exceptionIncorrectAnser);
				}
				if(show_dialog) MessageBox.Show(Properties.Resources.mbServerCheckSuccess);

				return true;
			}
			catch (Exception err)
			{
				logger.Info(err + "");
			}
			return false;
		}
		public bool CheckConnection(string hostname)
		{
			return CheckConnection(hostname, false);
		}
		#region Button Events
		private void btnCheckConnection_Click(object sender, RoutedEventArgs e)
        {
            // checking connection to server
            // try to open remote CmdReceiver and get server version
            try
            {

				if (!CheckConnection(this.hostname.Text, true))
				{
					throw new Exception(); 
				}
            }
            catch
            {
				MessageBox.Show(Properties.Resources.mbServerCheckFailed);
            }
        }

		private void btnOk_Click(object sender, RoutedEventArgs e)
		{
			if (CheckConnection(hostname.Text, true))
			{
				this.Close();
				result = true;
			}
			else
			{
				MessageBox.Show(Properties.Resources.mbServerCheckFailed);
			}
		}
		private void btnCancel_Click(object sender, RoutedEventArgs e)
		{
			this.Close();
		}
		#endregion

		private void btnSearch_Click(object sender, RoutedEventArgs e)
		{
			client.BroadCast();
		}

		void client_RecvMessage(object sender, SocketEventArgs e)
		{
			System.Windows.Forms.MethodInvoker method = delegate
			{
				string ipAddress = e.IPEndpoint.Address.ToString();

				bool bContains = false;
				foreach (ComboBoxItem item in hostname.Items)
				{
					if (((String)item.Content).Equals(ipAddress))
					{
						bContains = true;
						break;
					}
				}

				if (!bContains)
				{
					ComboBoxItem item = new ComboBoxItem();
					item.Content = ipAddress;
					item.DataContext = e.ReceivedData;
					item.ToolTip = e.ReceivedData;

					hostname.Items.Add(item);

					if (String.IsNullOrEmpty(hostname.Text)) hostname.SelectedIndex = 0;
				}
			};

			if (!this.Dispatcher.CheckAccess())
			{
				try
				{
					this.Dispatcher.BeginInvoke(method);
				}
				catch (Exception exx)
				{
					logger.Error(exx + "");
				}
			}
			else
			{
				method.Invoke();
			}
		}

		void client_EndBroadCast(object sender, EventArgs e)
		{
			System.Windows.Forms.MethodInvoker method = delegate
			{
				busyAnimation.Visibility = Visibility.Collapsed;
				try
				{ if(String.IsNullOrEmpty(hostname.Text)) hostname.SelectedIndex = 0; }
				catch {}
			};

			if (!this.Dispatcher.CheckAccess())
			{
				try
				{
					this.Dispatcher.BeginInvoke(method);
				}
				catch (Exception exx)
				{
					logger.Error(exx + "");
				}
			}
			else
			{
				method.Invoke();
			} 
		}

		void client_BeginBroadCast(object sender, EventArgs e)
		{
			System.Windows.Forms.MethodInvoker method = delegate
			{
				busyAnimation.Visibility = Visibility.Visible;
			};

			if (!this.Dispatcher.CheckAccess())
			{
				try
				{
					this.Dispatcher.BeginInvoke(method);
				}
				catch (Exception exx)
				{
					logger.Error(exx + "");
				}
			}
			else
			{
				method.Invoke();
			} 
		}

		private void Window_Unloaded(object sender, RoutedEventArgs e)
		{
			if(client != null)
			{
				client.Dispose();
				client = null;
			}
		}
    }
}
