﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using DigitalSignage.Common;

namespace DigitalSignage.Scheduler.ViewModels
{
    public class ViewModelBase : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public virtual void OnPropertyChanged(params string[] propertyNames)
        {
            if (PropertyChanged == null)
                return;

            foreach (string propertyName in propertyNames)
            {
                try
                {
                    PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
                }
                catch { }
            }
        }

        public bool IsDesignTime
        {
            get
            {
                return (App.Current == null) || (App.Current.GetType() == typeof(App));
            }
        }

        public void Dispose()
        {
            this.OnDispose();
        }

        protected virtual void OnDispose()
        {
        }

        ~ViewModelBase()
        {
            string msg = string.Format("{0} ({1}) Finalized", this.GetType().Name, this.GetHashCode());
            System.Diagnostics.Debug.WriteLine(msg);
        }
    }
}
