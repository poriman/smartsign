﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;

using DigitalSignage.ServerDatabase;
using DigitalSignage.Common;

using NLog;
using System.Xml;

namespace DigitalSignage.Scheduler
{
    /// <summary>
    /// Interaction logic for NewDlgItem.xaml
    /// </summary>
    public partial class NewPlayerDlg : Window, INotifyPropertyChanged
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        public bool result = false;
		Config cfgConnection = null;
		bool bIsModify = false;
        string groupID;
        public NewPlayerDlg(Config cfg , bool bModifyMode)
        {
			cfgConnection = cfg;
            InitializeComponent();
            groupBox.SetParent(null, cfg);
			PlayerName = Properties.Resources.labelDefaultPlayerName;
			this.Title = bModifyMode ? Properties.Resources.menuitemEditPlayer : Properties.Resources.menuitemAddPlayer;
// 			PlayerGroup = null;
			if (true == (bIsModify = bModifyMode))
			{
				pid.IsReadOnly = true;
			}
			else{
				PlayerID = "0000000";
				pid.Visibility = Visibility.Collapsed;
				rdFirst1.Height = new GridLength(0);
				rdFirst2.Height = new GridLength(0);

			}

            name.SelectAll();
            name.Focus();
/*
			panelPowerManagement.Visibility =
				ServerDataCache.GetInstance.IsScheduleHandlerEnabled ? Visibility.Visible : Visibility.Hidden;
	*/			
        }

		void groupBox_OnSelectionChanged(object sender, TreeViewItem newSelection)
		{
			if(lstTimeZone.Items.Count > 0)
			{
				lstTimeZone.Items.Clear();
			}
		}

		private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
			this.DialogResult = false;
            this.Close();
        }

		private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            // converting hostname to an ip address
            try
            {
				
				if (!bIsModify)
				{
					//	새로 생성하는 경우에는 서버에서 플레이어의 개수 제한을 검사해야 한다.
					if (!cfgConnection.ServerPlayersList.IsAvailableAddPlayer()) throw new Exception(Properties.Resources.mbErrorRejectAddPlayer);

					if (!cfgConnection.ServerPlayersList.IsAvailableAddPlayer(GROUPID)) throw new Exception(Properties.Resources.mbErrorRejectAddPlayer);

					using (new DigitalSignage.Common.WaitCursor())
					{
						//try
						//{
						//    int nMntType = 0;
						//    if (IsAMTChecked) nMntType |= (int)PowerMntType.AMT;
						//    if (IsWoLChecked) nMntType |= (int)PowerMntType.WoL;

						//    if (null == (PlayerID = cfgConnection.ServerDataCacheList.AddPlayer(GROUPID, PlayerName, "", nMntType, AuthID, AuthPass))) throw new Exception(Properties.Resources.mbCheckLog);
						//}
						//catch
						//{
						if (null == (PlayerID = cfgConnection.ServerDataCacheList.AddPlayer(GROUPID, PlayerName, "")))
						{
							try
							{
								///	플레이어 생성 기록
								cfgConnection.ServerLogList.InsertDetailLog(ServerDataCache.GetInstance.Connected_UserName, (int)(LogType.LogType_Managing | LogType.LogType_Failed), "", "PLAYER_CREATE", PlayerName, 0, 0, TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()));
							}
							catch { } 
							throw new Exception(Properties.Resources.mbCheckLog);
						}
						else
						{
							try
							{
								///	플레이어 생성 기록
								cfgConnection.ServerLogList.InsertDetailLog(ServerDataCache.GetInstance.Connected_UserName, (int)(LogType.LogType_Managing | LogType.LogType_Proceed), "", "PLAYER_CREATE", PlayerName, 0, 0, TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()));
							}
							catch { }
						}
						//}

					}
				}

				try
				{
					using (new DigitalSignage.Common.WaitCursor())
					{
						cfgConnection.ServerDownloadTimer.DeleteTimesFromPID(PlayerID);

						if (bDownloadCheck.IsChecked.HasValue && bDownloadCheck.IsChecked.Value == true)
						{
							foreach (String item in lstTimeZone.Items)
							{
								String[] arrstartEnd = item.Split('-');

								cfgConnection.ServerDownloadTimer.InsertDownloadTimeToPlayer(PlayerID, arrstartEnd[0], arrstartEnd[1]);
							}
						}
					}
				}
				catch (Exception ex)
				{
					MessageBox.Show(ex.Message, Properties.Resources.titleMessageBox, MessageBoxButton.OK);
				}

            }
			catch (Exception error)
			{
				MessageBox.Show(error.Message, Properties.Resources.titleMessageBox, MessageBoxButton.OK);
                return;
			}
            if (PlayerGroup == null)
            {
				MessageBox.Show(Properties.Resources.mbSelectPlayersGroup, Properties.Resources.titleMessageBox, MessageBoxButton.OK);
                return;
            }

			result = true;
			this.DialogResult = true;

            this.Close();
        }

        #region properties
        public string PlayerName
        {
            get { return name.Text; }
            set { name.Text = value; OnPropertyChanged("PlayerName"); }
        }
		public string PlayerID
		{
			get {
				return pid.Text;
			}
			set {
				pid.Text = value;
				
				OnPropertyChanged("PlayerID"); 
			}
		}

		public string AuthID
		{
			get
			{
				return tbAMTAuthID.Text;
			}
			set
			{
				tbAMTAuthID.Text = value;
				OnPropertyChanged("AuthID"); 
			}
		}

		public string AuthPass
		{
			get
			{
				return tbAMTAuthPass.Password;
			}
			set
			{
				tbAMTAuthPass.Password = value;
				OnPropertyChanged("AuthPass");
			}
		}

		public bool IsWoLChecked
		{
			get
			{
				if (bHasWoL.IsChecked == null) return false;
				return bHasWoL.IsChecked.Value;
			}
			set
			{
				bHasWoL.IsChecked = value;
				OnPropertyChanged("IsWoLChecked");
			}
		}

		public bool IsAMTChecked
		{
			get
			{
				if (bHasAMT.IsChecked == null) return false;
				return bHasAMT.IsChecked.Value;
			}
			set
			{
				bHasAMT.IsChecked = value;
				OnPropertyChanged("IsAMTChecked");
			}
		}

        public XmlNode PlayerGroup
        {
            get { return groupBox.SelectedGroup; }
            set 
            {
                if ( value != null )
                    groupBox.SelectedGroup = value;
                OnPropertyChanged("PlayerGroup"); 
            }
        }

		public string PlayerGroupGID
        {
            get { return groupBox.SelectedGroup.Attributes["gid"].Value; }
            set
            {
                if (value != null)
                    groupBox.SelectedGroupGID = value;
                OnPropertyChanged("PlayerGroup");
            }
        }

        public string GROUPID
        {
            set { groupID = value; }
            get { return groupID;}
        }

        public string PlayerComPort
        {
            get { return COMportUI.Text; }
            set { COMportUI.Text = value; OnPropertyChanged("PlayerComPort"); }
        }

        public int PlayerComBitrate
        {
            get 
            { 
                int[] rates = { 1200, 2400, 4800, 9600, 19200, 38400, 57600, 115200 };
                return rates[COMportSpeedUI.SelectedIndex]; 
            }
            set 
            {
                int id = 5;
                int[] rates = { 1200, 2400, 4800, 9600, 19200, 38400, 57600, 115200 };
                for (int i = 0; i < rates.Length; i++)
                    if (value == rates[i]) id = i;
                COMportSpeedUI.SelectedIndex = id;
            }
        }

        public int PlayerComBits
        {
            get 
            {
                int[] bits = { 1, 2 };
                return bits[COMportBitsUI.SelectedIndex]; 
            }
            set 
            {
                int id = 0;
                int[] bits = { 1, 2 };
                for (int i = 0; i < bits.Length; i++)
                    if (value == bits[i]) id = i;
                COMportBitsUI.SelectedIndex = id;
            }
        }

        #endregion

        #region INotify
        public event PropertyChangedEventHandler PropertyChanged;                
        protected void OnPropertyChanged(string propertyName)                    
        {                                                                        
           if (this.PropertyChanged != null)                                    
              PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }        
        #endregion

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
			try
			{
				groupBox.HasPopupMenu = false;

				groupBox.UpdateTree();
				groupBox.SelectedGroupGID = GROUPID;
			}
			catch { }

            if (bIsModify)
            {
				try
				{
					//	다운로드 진행 시간대 검색.
					ServerDataSet.DownloadTime_Players_ASNDataTable dt = cfgConnection.ServerDownloadTimer.GetDataByPID(PlayerID);

					if (dt.Rows.Count > 0)
					{
						bDownloadCheck.IsChecked = true;

						foreach (ServerDataSet.DownloadTime_Players_ASNRow row in dt.Rows)
						{
							lstTimeZone.Items.Add(row.starttime + "-" + row.endtime);
						}

					}
					else
					{
						bDownloadCheck.IsChecked = false;
					}

				}
				catch { }

            }
        }

		private void Button_Add_Times(object sender, RoutedEventArgs e)
		{
			TimeSpan tsstart = new TimeSpan(18, 0, 0);
			TimeSpan tsend = new TimeSpan(21, 0, 0);
			SelectTimeZone dlg_timezone = new SelectTimeZone(tsstart, tsend, true);
			dlg_timezone.Owner = this;

			#region group id 
			try
			{
				string gid = groupBox.SelectedGroupGID;

				//	다운로드 진행 시간대 검색.
				ServerDataSet.DownloadTime_Groups_ASNDataTable dt = cfgConnection.ServerDownloadTimer.GetDataByParents(gid);

				if (dt != null && dt.Rows.Count > 0)
				{
					foreach (ServerDataSet.DownloadTime_Groups_ASNRow row in dt.Rows)
					{
						string[] arrStart = row.starttime.Split(':');
						string[] arrEnd = row.endtime.Split(':');

						int startH = Convert.ToInt32(arrStart[0]);
						int startM = Convert.ToInt32(arrStart[1]);
						int startS = Convert.ToInt32(arrStart[2]);

						int endH = Convert.ToInt32(arrEnd[0]);
						int endM = Convert.ToInt32(arrEnd[1]);
						int endS = Convert.ToInt32(arrEnd[2]);

						TimeSpan tsStart = new TimeSpan(startH, startM, startS);
						TimeSpan tsEnd = new TimeSpan(endH, endM, endS);

						dlg_timezone.AddGroupTimeScope(tsStart, tsEnd);
					}

				}

			}
			catch { }

			#endregion

			dlg_timezone.ShowDialog();

			if(dlg_timezone.DialogResult.HasValue && dlg_timezone.DialogResult.Value == true)
			{
				lstTimeZone.Items.Add(dlg_timezone.StartTime.ToString() + "-" + dlg_timezone.EndTime.ToString());
			}

		}

		private void Button_Delete_Times(object sender, RoutedEventArgs e)
		{
			if(lstTimeZone.SelectedItem != null)
			{
				lstTimeZone.Items.Remove(lstTimeZone.SelectedItem);
			}
		}

		void tb_GotKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
		{
			try
			{
				TextBox tb = sender as TextBox;
				if (tb != null) tb.SelectAll();
				else
				{
					PasswordBox pb = sender as PasswordBox;
					pb.SelectAll();
				}

				e.Handled = true;
			}
			catch { }
		}
    }
}
