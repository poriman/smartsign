﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DigitalSignage.ServerDatabase;
using DigitalSignage.Common;

namespace DigitalSignage.Scheduler
{
    /// <summary>
    /// Interaction logic for NewDlgItem.xaml
    /// </summary>
    public partial class NewGroupDlg : Window
    {
        public string GroupID = Config.ROOT_GROUP_ID;
		public string GroupName = Properties.Resources.labelDefaultGroupName;
        public string GroupSource = "localhost";
        public string GroupUser = "";
        public string GroupPassword = "";
        public string GroupPath = "";
		public string Parent_GroupID = "-1";
		public bool IsInherited = true;

		private bool IsModifyMode = false;

		public NewGroupDlg(string title, string _parent_gid, bool bIsModifyMode)
		{
			InitializeComponent();
			
			this.Title = title;
			this.Parent_GroupID = _parent_gid;

			if(!(IsModifyMode = bIsModifyMode))
			{
				gid.Visibility = Visibility.Hidden;
			}
		}

        public new bool? ShowDialog()
        {
            name.Text = GroupName;
            source.Text = GroupSource;
            name.SelectAll();
            name.Focus();
			
			gid.Text = GroupID;
			gid.IsReadOnly = IsModifyMode;

            source.Text = GroupSource;
            path.Text = GroupPath;
            user.Text = GroupUser;
            password.Password = GroupPassword;
			
			cbInherited.IsChecked = IsInherited;

            if (String.IsNullOrEmpty(this.Parent_GroupID))
            {
                cbInherited.IsEnabled = false;
            }

			RefreshInheriteCheck();

			if(IsModifyMode)
			{
				try
				{
					//	다운로드 진행 시간대 검색.
					ServerDataSet.DownloadTime_Groups_ASNDataTable dt = Config.GetConfig.ServerDownloadTimer.GetDataByGID(GroupID);

					if (dt.Rows.Count > 0)
					{
						bDownloadCheck.IsChecked = true;

						foreach (ServerDataSet.DownloadTime_Groups_ASNRow row in dt.Rows)
						{
							lstTimeZone.Items.Add(row.starttime + "-" + row.endtime);
						}

					}
					else
					{
						bDownloadCheck.IsChecked = false;
					}
				}
				catch { }

			}


            return base.ShowDialog();
        }

		private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            GroupName = null;
			this.DialogResult = false;
            this.Close();
        }

		private void btnOk_Click(object sender, RoutedEventArgs e)
        {
			if(String.IsNullOrEmpty(name.Text))
			{
				MessageBox.Show(Properties.Resources.mbGroupName, Properties.Resources.titleMessageBox);
				return;
			}
			
			GroupID = gid.Text;
            GroupName = name.Text;
            GroupSource = source.Text;
            GroupPassword = password.Password;
            GroupPath = path.Text;
            GroupUser = user.Text;

//             if (GroupSource == "127.0.0.1")
//             {
// 				MessageBox.Show(Properties.Resources.mbFTPAddressFailed, Properties.Resources.titleMessageBox);
// 				return;
// //                 if (MessageBox.Show("FTP server will be not accessible from remote computer. Continue?",
// //                     "Group editor", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.No)
// //                     return;
// 
//             }
			this.DialogResult = true;
            this.Close();
        }

		public bool RegisterDownloadTime()
		{
			try
			{
				Config.GetConfig.ServerDownloadTimer.DeleteTimesFromGID(GroupID);

				if (bDownloadCheck.IsChecked.HasValue && bDownloadCheck.IsChecked.Value == true)
				{
					foreach (String item in lstTimeZone.Items)
					{
						String[] arrstartEnd = item.Split('-');

						Config.GetConfig.ServerDownloadTimer.InsertDownloadTimeToGroup(GroupID, arrstartEnd[0], arrstartEnd[1]);
					}
				}

				return true;
			}
			catch { }

			return false;

		}

		private bool CheckValidGID(String gid)
		{
			Config cfg = Config.GetConfig;

			String sReturn = cfg.ServerDataCacheList.GetGroupNode(gid);

			return String.IsNullOrEmpty(sReturn);
		}

		private void Button_Add_Times(object sender, RoutedEventArgs e)
		{
			Config cfgConnection = Config.GetConfig;

			TimeSpan tsstart = new TimeSpan(18, 0, 0);
			TimeSpan tsend = new TimeSpan(21, 0, 0);
			SelectTimeZone dlg_timezone = new SelectTimeZone(tsstart, tsend, true);
			dlg_timezone.Owner = this;

			#region group id
			try
			{
				//	다운로드 진행 시간대 검색.
				ServerDataSet.DownloadTime_Groups_ASNDataTable dt = cfgConnection.ServerDownloadTimer.GetDataByParents(GroupID);

				if (dt != null && dt.Rows.Count > 0)
				{
					foreach (ServerDataSet.DownloadTime_Groups_ASNRow row in dt.Rows)
					{
						string[] arrStart = row.starttime.Split(':');
						string[] arrEnd = row.endtime.Split(':');

						int startH = Convert.ToInt32(arrStart[0]);
						int startM = Convert.ToInt32(arrStart[1]);
						int startS = Convert.ToInt32(arrStart[2]);

						int endH = Convert.ToInt32(arrEnd[0]);
						int endM = Convert.ToInt32(arrEnd[1]);
						int endS = Convert.ToInt32(arrEnd[2]);

						TimeSpan tsStart = new TimeSpan(startH, startM, startS);
						TimeSpan tsEnd = new TimeSpan(endH, endM, endS);

						dlg_timezone.AddGroupTimeScope(tsStart, tsEnd);
					}

				}

			}
			catch { }

			#endregion

			dlg_timezone.ShowDialog();

			if (dlg_timezone.DialogResult.HasValue && dlg_timezone.DialogResult.Value == true)
			{
				lstTimeZone.Items.Add(dlg_timezone.StartTime.ToString() + "-" + dlg_timezone.EndTime.ToString());
			}
		}

		private void Button_Delete_Times(object sender, RoutedEventArgs e)
		{
			if (lstTimeZone.SelectedItem != null)
			{
				lstTimeZone.Items.Remove(lstTimeZone.SelectedItem);
			}
		}

		void tb_GotKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
		{
			try
			{
				TextBox tb = sender as TextBox;
				if (tb != null) tb.SelectAll();
				else
				{
					PasswordBox pb = sender as PasswordBox;
					pb.SelectAll();
				}

				e.Handled = true;
			}
			catch { }
		}

		private void cbInherited_Checked(object sender, RoutedEventArgs e)
		{
			RefreshInheriteCheck();
		}

		private void RefreshInheriteCheck()
		{
			try
			{
				bool bSuperAdmin = ServerDataCache.GetInstance.HasPermission("ADMINISTRATOR");

				if(!bSuperAdmin)
				{
					tbAddress.Visibility = tbUser.Visibility = tbPwd.Visibility = tbPath.Visibility = source.Visibility = user.Visibility = password.Visibility = path.Visibility = cbInherited.IsChecked.Value ? Visibility.Hidden : Visibility.Visible;

				}
				source.IsEnabled = user.IsEnabled = password.IsEnabled = path.IsEnabled = !cbInherited.IsChecked.Value;

				if (IsInherited = cbInherited.IsChecked.Value)
				{
					Config cfg = Config.GetConfig;

					if(bSuperAdmin || cbInherited.IsChecked.Value)
					{
						String sReturn = cfg.ServerDataCacheList.GetInheritedFTPInfo(Parent_GroupID);
						if (!String.IsNullOrEmpty(sReturn))
						{
							String[] arrFTPInfo = sReturn.Split('|');

							source.Text = arrFTPInfo[0];
							user.Text = arrFTPInfo[1];
							password.Password = arrFTPInfo[2];
							path.Text = arrFTPInfo[3];
						}
					}
				}
				else if(!bSuperAdmin)
				{
					source.Text = "";
					user.Text = "";
					password.Password = "";
					path.Text = "";
				}
			}
			catch { }
		}

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (e.Cancel) DialogResult = false;
        }
    }
}
