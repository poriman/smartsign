﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

using System.Net;
using System.Net.Sockets;
using System.Threading;

using NLog;

using DigitalSignage.Common;
using System.Xml;

namespace DigitalSignage.Scheduler
{
    /// <summary>
    /// Interaction logic for NewDlgItem.xaml
    /// </summary>
    public partial class FTPStatuses : Window
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private Config cfg = null;
        private ArrayList servers = new ArrayList();
        private ArrayList users = new ArrayList();
        private ArrayList passwords = new ArrayList();
        private ArrayList ids = new ArrayList();
		private ArrayList groupnames = new ArrayList();

        public FTPStatuses(Config config)
        {
            InitializeComponent();
            cfg = config;
            Start();
        }

        #region Event handlers
        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

        }
        #endregion

        #region Network routines
        public void Start()
        {
			ServerDataCache sdc = ServerDataCache.GetInstance;
			XmlNodeList list = sdc.NetworkTree.SelectNodes("//child::group[attribute::del_yn=\"N\"]");
			bool bSuperAdmin = sdc.HasPermission("ADMINISTRATOR");

			int nIndex = 0;
			foreach(XmlNode node in list)
			{
				groupnames.Add(node.Attributes["name"].Value);
				servers.Add(cfg.AnalyzeSource(node.Attributes["source"].Value));
				users.Add(node.Attributes["username"].Value);
				passwords.Add(node.Attributes["password"].Value);
				String sText;
				if (bSuperAdmin) sText = node.Attributes["name"].Value + " (" + node.Attributes["source"].Value + ")";
				else sText = node.Attributes["name"].Value;
				ids.Add(status.AddItem(Properties.Resources.ftpoff, sText, Properties.Resources.labelFTPOff));
				
				Thread s = new Thread(new ParameterizedThreadStart(this.Scan));
				s.Start(nIndex++);
			}


        }

		public void Scan(object nIndex)
		{
			try
			{
				int i = (int)nIndex;

				FTPFunctions ftp = new FTPFunctions((string)servers[i], "", (string)users[i], (string)passwords[i], cfg.FTP_Timeout);
				if (ftp.CheckState())
					SetOnlineState((int)ids[i]);
			}
			catch (Exception e)
			{
				logger.Error(e + "");
			}
			finally
			{
			}
		}

        #endregion

        #region Async routines
        delegate void setOnlineStateCB( int id );
        public void SetOnlineState( int id )
        {
            try
            {
                if (!this.Dispatcher.CheckAccess())
                {
                    this.Dispatcher.BeginInvoke(DispatcherPriority.Render,
                       new setOnlineStateCB(SetOnlineState), id );
                }
                else
                {
					status.UpdateItem(id, Properties.Resources.ftpon, null, Properties.Resources.labelFTPOn);
                }
            }
            catch (Exception e)
            {
                logger.Error(e + "");
            }
        }
        #endregion  

    }
}
