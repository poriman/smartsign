﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;

using DigitalSignage.DataBase;
using DigitalSignage.Common;

namespace DigitalSignage.Scheduler
{
    /// <summary>
    /// Interaction logic for TimeDiagram
    /// </summary>
    public partial class TimeDiagram : UserControl
    {
        private DateTime day;
        private ArrayList tasksList;
        private double hourLength;
        private Rectangle prevSelection = null;

        #region Events
        public delegate void SelectionChanged(object sender, Task newSelection);
        public event SelectionChanged OnSelectionChanged = null;
        #endregion

        public TimeDiagram()
        {
            InitializeComponent();
        }

        private void buildUI()
        {
            mainCanvas.Children.Clear(); // !!!!! this should be optimized

            // 1. draw hour lines
            hourLength = (ActualWidth - 100) / 26;
            int lstep = 1;
            if ( hourLength < 36) lstep = 2;
            if (hourLength < 18) lstep = 4;
            for (int i = 0; i < 25; i = i +lstep )
            {
                // line
                Line line = new Line();
                line.X1 = (hourLength / 2) + i * hourLength;
                line.X2 = line.X1;
                line.Y1 = 25;
                line.Y2 = ActualHeight-25;
                line.Stroke = Brushes.Gray;
                line.StrokeThickness = 1.0;
                mainCanvas.Children.Add(line);
                // header
                TextBlock header = new TextBlock();
                header.Foreground = Brushes.White;
                header.Text = (i < 10 ? "0" + i : i + "") + ":00";
                Canvas.SetLeft(header, line.X1 - 20);
                Canvas.SetTop(header, 3);
                mainCanvas.Children.Add(header);
            }
            drawTasks(tasksList);

        }

        private void drawTasks( ArrayList tasks )
        {
            if (tasks == null) return;
            DateTime cDay = new DateTime( day.Year, day.Month, day.Day, 0, 0,0);
            long daystart = TimeConverter.ConvertToUTP(cDay.ToUniversalTime());
            cDay = new DateTime(day.Year, day.Month, day.Day, 23, 59, 59); 
            long dayend = TimeConverter.ConvertToUTP(cDay.ToUniversalTime());
            for (int i = 0; i < tasks.Count; i++)
            {
                Task task = tasks[i] as Task;
                // checking
                if ((task.TaskEnd < daystart) || (task.TaskStart > dayend)) continue;
                if ( task.type != TaskCommands.CmdScreen ) continue;

                Rectangle rect = new Rectangle();
                rect.Height = 30;
                rect.Stroke = Brushes.White;
                rect.Fill = Brushes.LightBlue;

                if (task.TaskStart < daystart) task.TaskStart = daystart - 900;
                if (task.TaskEnd > dayend) task.TaskEnd = dayend + 900;

                rect.Width = (task.TaskEnd - task.TaskStart) * hourLength / 3600;
                double left = (hourLength / 2) + (task.TaskStart - daystart) * hourLength / 3600;
                Canvas.SetLeft(rect, left);
                if ( task.type == TaskCommands.CmdScreen )
                    Canvas.SetTop(rect, 25);
                rect.DataContext = task;
                //rect.MouseEnter += new MouseEventHandler( rectMouseEnter );
                rect.MouseLeftButtonUp += new MouseButtonEventHandler( rectMouseEnter);
                //rect.MouseLeave += new MouseEventHandler(rectMouseLeave);
                mainCanvas.Children.Add(rect);
            }
        }

        private void mainCanvas_Loaded(object sender, RoutedEventArgs e)
        {
            UpdateLayout();
            buildUI();
        }

        public void Rebuild()
        {
            buildUI();
            if (prevSelection != null)
            {
                prevSelection = null;
                if (OnSelectionChanged != null)
                    OnSelectionChanged(this, null);
            }
            //drawTasks(tasksList);
        }

        #region events handlers
        private void rectMouseEnter(object sender, MouseButtonEventArgs args)
        {
           Rectangle rect = sender as Rectangle;
           if ( rect == null ) return;
           if (prevSelection != null)
              prevSelection.Fill = Brushes.LightBlue;
           rect.Fill = Brushes.White;
           prevSelection = rect;
           if (OnSelectionChanged != null)
              OnSelectionChanged(this, (Task)rect.DataContext);
        }

        private void rectMouseLeave(object sender, MouseEventArgs args)
        {
           Rectangle rect = sender as Rectangle;
           if (rect == null) return;
           if (prevSelection != null)
              prevSelection.Fill = Brushes.LightBlue;
           prevSelection = null;
           if (OnSelectionChanged != null)
              OnSelectionChanged(this, null);
        }

        private void mainCanvas_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            buildUI();
        }
        #endregion

        #region properties
        public DateTime Day
        {
            get { return day; }
            set { day = value; }
        }

        public ArrayList Tasks
        {
            get { return tasksList; }
            set { tasksList = value; }
        }

        public Task SelectedTask
        {
           get 
           {
              if (prevSelection == null)
                 return null;
              return (Task)prevSelection.DataContext; 
           }
        }
        #endregion

    }
}
