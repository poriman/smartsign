﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Collections;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Controls.Primitives;
using System.Globalization;
using System.Windows.Threading;
using System.Threading;

using DigitalSignage.Common;
using DigitalSignage.ServerDatabase;

using DigitalSignage.Scheduler.Cultures;
using Microsoft.Windows.Controls.Ribbon;

using NLog;
using NLog.Targets;

using System.Drawing;

using System.Windows.Interop;

using WinInterop = System.Windows.Interop;
using System.Runtime.InteropServices;
using System.Xml;
using System.Collections.ObjectModel;
using System.Net;
using System.IO;
using DigitalSignage.PlayControls;
using System.Linq;
using System.Xml.XPath;
using System.Runtime.Serialization.Formatters.Binary;
using System.ComponentModel;
using System.Configuration;

namespace DigitalSignage.Scheduler
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
	public partial class Window1 : RibbonWindow
    {

        private static Logger logger = LogManager.GetCurrentClassLogger();
        private Config cfg;
		public ContextMenu StatusCMenu;

// 		DataSet.playersDataTable status_players = null;
		XmlNodeList status_players = null;

		string order_players = "pid asc";

        Type tp_sort = typeof(String);

        private Collection<Task> _arrCopiesList = new Collection<Task>();

        #region 하나 이상 선택 삭제
        public delegate void DeleteContentFromStorageDelegate(List<Task> DeleteTasks);//조준영 : 하나 이상 선택 삭제
        static EventWaitHandle waitSyncHandler = new AutoResetEvent(false);//조준영 : 하나 이상 선택 삭제
        #endregion

		#region WINAPI
		/// <summary>
		/// POINT aka POINTAPI
		/// </summary>
		[StructLayout(LayoutKind.Sequential)]
		public struct POINT
		{
			/// <summary>
			/// x coordinate of point.
			/// </summary>
			public int x;
			/// <summary>
			/// y coordinate of point.
			/// </summary>
			public int y;

			/// <summary>
			/// Construct a point of coordinates (x,y).
			/// </summary>
			public POINT(int x, int y)
			{
				this.x = x;
				this.y = y;
			}
		}

		[StructLayout(LayoutKind.Sequential)]
		public struct MINMAXINFO
		{
			public POINT ptReserved;
			public POINT ptMaxSize;
			public POINT ptMaxPosition;
			public POINT ptMinTrackSize;
			public POINT ptMaxTrackSize;
		};

		/// <summary>
		/// </summary>
		[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
		public class MONITORINFO
		{
			/// <summary>
			/// </summary>            
			public int cbSize = Marshal.SizeOf(typeof(MONITORINFO));

			/// <summary>
			/// </summary>            
			public RECT rcMonitor = new RECT();

			/// <summary>
			/// </summary>            
			public RECT rcWork = new RECT();

			/// <summary>
			/// </summary>            
			public int dwFlags = 0;
		}


		/// <summary> Win32 </summary>
		[StructLayout(LayoutKind.Sequential, Pack = 0)]
		public struct RECT
		{
			/// <summary> Win32 </summary>
			public int left;
			/// <summary> Win32 </summary>
			public int top;
			/// <summary> Win32 </summary>
			public int right;
			/// <summary> Win32 </summary>
			public int bottom;

			/// <summary> Win32 </summary>
			public static readonly RECT Empty = new RECT();

			/// <summary> Win32 </summary>
			public int Width
			{
				get { return Math.Abs(right - left); }  // Abs needed for BIDI OS
			}
			/// <summary> Win32 </summary>
			public int Height
			{
				get { return bottom - top; }
			}

			/// <summary> Win32 </summary>
			public RECT(int left, int top, int right, int bottom)
			{
				this.left = left;
				this.top = top;
				this.right = right;
				this.bottom = bottom;
			}


			/// <summary> Win32 </summary>
			public RECT(RECT rcSrc)
			{
				this.left = rcSrc.left;
				this.top = rcSrc.top;
				this.right = rcSrc.right;
				this.bottom = rcSrc.bottom;
			}

			/// <summary> Win32 </summary>
			public bool IsEmpty
			{
				get
				{
					// BUGBUG : On Bidi OS (hebrew arabic) left > right
					return left >= right || top >= bottom;
				}
			}
			/// <summary> Return a user friendly representation of this struct </summary>
			public override string ToString()
			{
				if (this == RECT.Empty) { return "RECT {Empty}"; }
				return "RECT { left : " + left + " / top : " + top + " / right : " + right + " / bottom : " + bottom + " }";
			}

			/// <summary> Determine if 2 RECT are equal (deep compare) </summary>
			public override bool Equals(object obj)
			{
				if (!(obj is Rect)) { return false; }
				return (this == (RECT)obj);
			}

			/// <summary>Return the HashCode for this struct (not garanteed to be unique)</summary>
			public override int GetHashCode()
			{
				return left.GetHashCode() + top.GetHashCode() + right.GetHashCode() + bottom.GetHashCode();
			}


			/// <summary> Determine if 2 RECT are equal (deep compare)</summary>
			public static bool operator ==(RECT rect1, RECT rect2)
			{
				return (rect1.left == rect2.left && rect1.top == rect2.top && rect1.right == rect2.right && rect1.bottom == rect2.bottom);
			}

			/// <summary> Determine if 2 RECT are different(deep compare)</summary>
			public static bool operator !=(RECT rect1, RECT rect2)
			{
				return !(rect1 == rect2);
			}


		}

		[DllImport("user32")]
		internal static extern bool GetMonitorInfo(IntPtr hMonitor, MONITORINFO lpmi);

		/// <summary>
		/// 
		/// </summary>
		[DllImport("User32")]
		internal static extern IntPtr MonitorFromWindow(IntPtr handle, int flags);


		void Window_SourceInitialized(object sender, EventArgs e)
		{
			System.IntPtr handle = (new WinInterop.WindowInteropHelper(this)).Handle;
			WinInterop.HwndSource.FromHwnd(handle).AddHook(new WinInterop.HwndSourceHook(WindowProc));
		}

		private static System.IntPtr WindowProc(
			  System.IntPtr hwnd,
			  int msg,
			  System.IntPtr wParam,
			  System.IntPtr lParam,
			  ref bool handled)
		{
			switch (msg)
			{
				case 0x0024:/* WM_GETMINMAXINFO */
					WmGetMinMaxInfo(hwnd, lParam);
					handled = true;
					break;
			}

			return (System.IntPtr)0;
		}

		private static void WmGetMinMaxInfo(System.IntPtr hwnd, System.IntPtr lParam)
		{
			MINMAXINFO mmi = (MINMAXINFO)Marshal.PtrToStructure(lParam, typeof(MINMAXINFO));

			// Adjust the maximized size and position to fit the work area of the correct monitor
			int MONITOR_DEFAULTTONEAREST = 0x00000002;
			System.IntPtr monitor = MonitorFromWindow(hwnd, MONITOR_DEFAULTTONEAREST);

			if (monitor != System.IntPtr.Zero)
			{
				try
				{
					MONITORINFO monitorInfo = new MONITORINFO();
					GetMonitorInfo(monitor, monitorInfo);

					RECT rcWorkArea = monitorInfo.rcWork;
					RECT rcMonitorArea = monitorInfo.rcMonitor;

					int MARGIN = rcWorkArea.left - mmi.ptMaxPosition.x;

					mmi.ptMaxPosition.x = Math.Abs(rcWorkArea.left - rcMonitorArea.left) - MARGIN;
					mmi.ptMaxPosition.y = Math.Abs(rcWorkArea.top - rcMonitorArea.top) - MARGIN;
					mmi.ptMaxSize.x = Math.Abs(rcWorkArea.right - rcWorkArea.left) + MARGIN * 2;
					mmi.ptMaxSize.y = Math.Abs(rcWorkArea.bottom - rcWorkArea.top) + (MARGIN > 2 ? 2 : MARGIN);
				}
				catch { }
			}

			Marshal.StructureToPtr(mmi, lParam, true);
		}
		
		#endregion

		private void InitializeLogger()
		{
			String sExecutingPath = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
			string appData = sExecutingPath + "\\SchedulerData\\";
			if (!Directory.Exists(appData))
				Directory.CreateDirectory(appData);

			try
			{
				// configuring log output
				FileTarget target = new FileTarget();
				target.Layout = "${longdate}\t[${level}]\t[${callsite}]\t${message}";
				//             target.Layout = "${longdate} ${logger} ${message}";
				target.FileName = appData + "Manager.Log.txt";
				target.ArchiveFileName = appData + "archives/Manager.Log.{#####}.txt";
				// 			target.ArchiveAboveSize = 256 * 1024 * 10; // archive files greater than 2560 KB
				target.MaxArchiveFiles = 31;
				target.ArchiveEvery = FileArchivePeriod.Day;// FileTarget.ArchiveEveryMode.Day;
				target.ArchiveNumbering = ArchiveNumberingMode.Sequence;// FileTarget.ArchiveNumberingMode.Sequence;
				// this speeds up things when no other processes are writing to the file
				target.ConcurrentWrites = true;
				NLog.Config.SimpleConfigurator.ConfigureForTargetLogging(target, LogLevel.Debug);

			}
			catch { }
		}

		//	TimeLine 과 연동할 컨텍스트 메뉴
		private void TimeDiagram_ContextMenu(object sender, MouseButtonEventArgs e)
		{
			ServerDataCache sdc = ServerDataCache.GetInstance;

			ContextMenu contextMenu = new ContextMenu();
			
			Separator sp = null;
			MenuItem mi = null;

			bool perm_CreateTask = sdc.HasPermission("CREATE SCHEDULE");
			//	Task 
            if (cfg.IsAvailableComplexPane)
            {
                mi = new MenuItem();
                mi.Header = Properties.Resources.menuitemTaskWizard;
                mi.Icon = GetIconImage(Properties.Resources.icon_task_wizard);
                mi.IsEnabled = perm_CreateTask;
                mi.Click += new RoutedEventHandler(GPTree_OnClickTaskWizard);
                contextMenu.Items.Add(mi);
            }

			mi = new MenuItem();
			mi.Header = Properties.Resources.menuitemAddDefaultSchedule;
			mi.Icon = GetIconImage(Properties.Resources.icon_default);
			mi.IsEnabled = perm_CreateTask;
			mi.Click += new RoutedEventHandler(GPTree_OnClickAddDefault);
			contextMenu.Items.Add(mi); mi = new MenuItem();

			mi = new MenuItem();
			mi.Header = Properties.Resources.menuitemAddTaskProgram;
			mi.Icon = GetIconImage(Properties.Resources.icon_add_program_task);
			mi.IsEnabled = perm_CreateTask;
			mi.Click += new RoutedEventHandler(GPTree_OnClickNewTask);
			contextMenu.Items.Add(mi);

			if (cfg.IsAvailableSubtitleSchedule)
			{
				mi = new MenuItem();
				mi.Header = Properties.Resources.menuitemAddSubtitle;
				mi.Icon = GetIconImage(Properties.Resources.icon_add_subtitle_task);
				mi.IsEnabled = perm_CreateTask;
				mi.Click += new RoutedEventHandler(GPTree_OnClickSubtitle);
				contextMenu.Items.Add(mi);
			}

			if (cfg.IsAvailableControlSchedule)
			{
				mi = new MenuItem();
				mi.Header = Properties.Resources.menuitemAddControl;
                mi.Icon = GetIconImage(Properties.Resources.icon_add_control_task);
				mi.IsEnabled = perm_CreateTask;
				mi.Click += new RoutedEventHandler(GPTree_OnClickControl);
				contextMenu.Items.Add(mi);
			}

			sp = new Separator();
			contextMenu.Items.Add(sp);

			bool hasSelectedTask = tasksDiagram.SelectedTask != null;

			if(hasSelectedTask &&
                (tasksDiagram.SelectedTask.type == TaskCommands.CmdDefaultProgram || tasksDiagram.SelectedTask.type == TaskCommands.CmdTouchDefaultProgram || 
				tasksDiagram.SelectedTask.type == TaskCommands.CmdProgram))
			{
				mi = new MenuItem();
				mi.Header = Properties.Resources.menuitemPreview;
				mi.Icon = GetIconImage(Properties.Resources.icon_preview);
				mi.IsEnabled = hasSelectedTask && sdc.HasPermission("VIEW SCHEDULE");
				mi.Click += new RoutedEventHandler(OnPreviewTask);
				contextMenu.Items.Add(mi);

				mi = new MenuItem();
				mi.Header = Properties.Resources.menuitemExport;
				mi.Icon = GetIconImage(Properties.Resources.icon_export);
				mi.IsEnabled = hasSelectedTask && sdc.HasPermission("UPDATE SCHEDULE");
				mi.Click += new RoutedEventHandler(OnExportTask);
				contextMenu.Items.Add(mi);

				sp = new Separator();
				contextMenu.Items.Add(sp);
			}

			mi = new MenuItem();
			mi.Header = Properties.Resources.labelCopy;
			mi.Icon = GetIconImage(Properties.Resources.icon_copy);
			mi.IsEnabled = perm_CreateTask && hasSelectedTask;
			mi.Click += new RoutedEventHandler(mi_CopyClick);
			contextMenu.Items.Add(mi);

			mi = new MenuItem();
			mi.Header = Properties.Resources.labelPaste;
			mi.Icon = GetIconImage(Properties.Resources.icon_paste);
			mi.IsEnabled = perm_CreateTask && _arrCopiesList.Count > 0;
			mi.Click += new RoutedEventHandler(mi_PasteClick);
			contextMenu.Items.Add(mi);

			sp = new Separator();
			contextMenu.Items.Add(sp);

            mi = new MenuItem();
            mi.Header = Properties.Resources.menuitemEditTask;
            mi.Icon = GetIconImage(Properties.Resources.icon_edit_task);
            mi.IsEnabled = hasSelectedTask && sdc.HasPermission("UPDATE SCHEDULE");
            mi.Click += new RoutedEventHandler(GPTree_OnClickEditTask);
            contextMenu.Items.Add(mi);

			mi = new MenuItem();
			mi.Header = Properties.Resources.menuitemDeleteTask;
            mi.Icon = GetIconImage(Properties.Resources.icon_delete_task);
			mi.IsEnabled = hasSelectedTask && sdc.HasPermission("DELETE SCHEDULE");
			mi.Click += new RoutedEventHandler(GPTree_OnClickDeleteTask);
			contextMenu.Items.Add(mi);

            MenuItem delmi = mi;

            sp = new Separator();
            contextMenu.Items.Add(sp);

            //	Task 
            mi = new MenuItem();
            mi.Header = Properties.Resources.menuitemShowSchedLog;
            mi.Icon = GetIconImage(Properties.Resources._16_log_schedule);
            mi.IsEnabled = tasksDiagram.SelectedTask != null && sdc.HasPermission("ADMINISTRATOR");
			
            mi.Click += new RoutedEventHandler(CmdShowLogsBySchedule);
            contextMenu.Items.Add(mi);

            #region 조준영: 하나 이상 선택 삭제           
            if (tasksDiagram.SelectedTasks != null && tasksDiagram.SelectedTasks.Count > 1)//마지막 mi 가" 스케줄 삭제" MenuItem이다. 
            {
                foreach (object item in contextMenu.Items)
                {
                    MenuItem menuitem = item as MenuItem;
                    if (menuitem != null && menuitem.Equals(delmi) == false)
                        menuitem.IsEnabled = false;
                }
            }
            #endregion

			contextMenu.UpdateLayout();


			contextMenu.IsOpen = true;
			e.Handled = true;
            
		}

		void mi_CopyClick(object sender, RoutedEventArgs e)
		{
			try
			{
				_arrCopiesList.Clear();
				if (this.tasksDiagram.SelectedTasks != null)
				{
					foreach (Task t in this.tasksDiagram.SelectedTasks)
					{
						_arrCopiesList.Add(t);
					}
				}
				else
				{
					_arrCopiesList.Add(this.tasksDiagram.SelectedTask);
				}

				e.Handled = true;
			}
			catch { }
		}

		void mi_PasteClick(object sender, RoutedEventArgs e)
		{
			try
			{
				XmlNode node = this.GPTree.SelectedItem as XmlNode;

				if (node == null) return;

				foreach(Task item in _arrCopiesList)
				{
					Task newItem = new Task();
					newItem.uuid = Guid.NewGuid();

					newItem.daysofweek = item.daysofweek;
					newItem.description = item.description;
					newItem.duration = item.duration;
					newItem.duuid = item.duuid;
					newItem.TaskStart = item.TaskStart;
					newItem.TaskEnd = item.TaskEnd;
					newItem.starttimeofday = item.starttimeofday;
					newItem.endtimeofday = item.endtimeofday;
					newItem.type = item.type;
					newItem.isAd = item.isAd;
					newItem.MetaTags = item.MetaTags;
					newItem.show_flag = item.show_flag;

					/*	hsshin 중복 체크 루틴 제거

                    bool isCheckTimeScopeValue = TaskMaker.CheckTimeScope(newItem, GPTree.SelectedItem);
					if (item.type != TaskCommands.CmdTouchDefaultProgram && item.type != TaskCommands.CmdDefaultProgram && false == isCheckTimeScopeValue)
					{
						MessageBox.Show(Properties.Resources.mbAlreadyUsed, Properties.Resources.titleMessageBox, MessageBoxButton.OK, MessageBoxImage.Stop);
						return;
					}
					*/

					XmlNode gnode = null;
					if (node != null && node.LocalName.Equals("group"))
					{
						gnode = node;
						newItem.gid = node.Attributes["gid"].Value;
						newItem.pid = null/*"-1"*/;
					}
					else
					{
						gnode = node.ParentNode;

						newItem.pid = node.Attributes["pid"].Value;
						newItem.gid = node.Attributes["gid"].Value;

					}

					try
					{
						FTPFunctions ftp = new FTPFunctions(gnode.Attributes["source"].Value, gnode.Attributes["path"].Value, gnode.Attributes["username"].Value, gnode.Attributes["password"].Value, 5000);

						if (!ftp.FTPHasDirectory(item.duuid.ToString()))
						{
							MessageBoxResult res = MessageBox.Show(Properties.Resources.mbNoDirectoryInFTP, Properties.Resources.titleMessageBox, MessageBoxButton.YesNo, MessageBoxImage.Warning);

							if (res == MessageBoxResult.Yes)
								return;
						}
					}
					catch
					{
						MessageBox.Show(Properties.Resources.mbAccessDeniedFTP, Properties.Resources.titleMessageBox, MessageBoxButton.OK, MessageBoxImage.Error);
						return;
					}



					newItem.guuid = Guid.NewGuid();
					newItem.is_accessable = true;
					newItem.priority = item.priority;
// 					newItem.starttimeofday = item.starttimeofday;
					newItem.state = TaskState.StateWait;
// 					newItem.TaskEnd = item.TaskEnd;
// 					newItem.TaskStart = item.TaskStart;
// 					newItem.type = item.type;

					if (-1 != cfg.ServerTasksList.AddTask(newItem))
					{
						#region Screen 매핑 등록
						try
						{
							using (ServerDatabase.ServerDataSet.screensDataTable dt = cfg.ServerTasksList.GetScreensByUuid(item.uuid.ToString()))
							{
								if (dt != null)
								{
									int nIndex = 0;
									ServerDataCache sdc = ServerDataCache.GetInstance;

									foreach (ServerDatabase.ServerDataSet.screensRow row in dt.Rows)
									{
										String sThumbnailURL = String.Empty;
										if (sdc.IsSupportedThumbnail)
										{
											/// 섬네일 등록
											String sThumbName = String.Format("{0}.png", row.s_id);
											sThumbnailURL = sdc.ThumbnailGetUrl +
													(sdc.ThumbnailGetUrl.EndsWith("/") ? "" : "/") + sThumbName;
										}

										cfg.ServerTasksList.InsertTaskAndScreenMappingDetail(row.s_id, newItem.uuid.ToString(), nIndex++, sThumbnailURL);
									}
								}
								dt.Dispose();
							}

						}
						catch { }
						#endregion

						try
						{
							///	스케줄 생성 기록
							cfg.ServerLogList.InsertDetailLog(ServerDataCache.GetInstance.Connected_UserName, (int)(LogType.LogType_Managing | LogType.LogType_Proceed), newItem.uuid.ToString(), "SCHEDULE_CREATE", newItem.description, 0, 0, TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()));
						}
						catch { }
					}
					else
					{
						try
						{
							///	스케줄 생성 기록
							cfg.ServerLogList.InsertDetailLog(ServerDataCache.GetInstance.Connected_UserName, (int)(LogType.LogType_Managing | LogType.LogType_Failed), newItem.uuid.ToString(), "SCHEDULE_CREATE", newItem.description, 0, 0, TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()));
						}
						catch { }
					}
				}
				this.updateTasksList(GPTree.SelectedItem);
			}
			catch
			{
				
			}
		}

		[System.Runtime.InteropServices.DllImport("gdi32.dll")]
		public static extern bool DeleteObject(IntPtr hObject);

		private System.Windows.Controls.Image GetIconImage(System.Drawing.Bitmap image)
		{
			IntPtr hBitmap = IntPtr.Zero;
			System.Windows.Controls.Image iconf = null;
			try
			{
				hBitmap = image.GetHbitmap();

				iconf = new System.Windows.Controls.Image();
				iconf.Source = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(hBitmap,
											IntPtr.Zero, new Int32Rect(0, 0, image.Width, image.Height),
											BitmapSizeOptions.FromEmptyOptions());
				iconf.Width = 16;
				iconf.Height = 16;

				return iconf;
			}
			catch { iconf = null; }
			finally
			{
				if (hBitmap != IntPtr.Zero) DeleteObject(hBitmap);
			}

			return iconf;
		}
		#region Custom Commands

		public static RoutedUICommand AddPlayer = new RoutedUICommand("Add new player", "AddPlayer", typeof(Window1));
		public static RoutedUICommand EditPlayer = new RoutedUICommand("Edit player configuration", "EditPlayer", typeof(Window1));
		public static RoutedUICommand DeletePlayer = new RoutedUICommand("Delete player", "DeletePlayer", typeof(Window1));

		public static RoutedUICommand AddGroup = new RoutedUICommand("Add new Group", "AddGroup", typeof(Window1));
		public static RoutedUICommand EditGroup = new RoutedUICommand("Edit Group configuration", "EditGroup", typeof(Window1));
		public static RoutedUICommand DeleteGroup = new RoutedUICommand("Delete Group", "DeleteGroup", typeof(Window1));

		public static RoutedUICommand TaskWizard = new RoutedUICommand("Task Wizard", "TaskWizard", typeof(Window1));
		public static RoutedUICommand AddTask = new RoutedUICommand("Add new Task", "AddTask", typeof(Window1));
		public static RoutedUICommand AddSubtitle = new RoutedUICommand("Add Subtitle", "AddSubtitle", typeof(Window1));
		public static RoutedUICommand EditTask = new RoutedUICommand("Edit Task configuration", "EditTask", typeof(Window1));
		public static RoutedUICommand DeleteTask = new RoutedUICommand("Delete Task", "DeleteTask", typeof(Window1));

		public static RoutedUICommand AddDefault = new RoutedUICommand("Add Default Task", "AddDefault", typeof(Window1));
		public static RoutedUICommand AddControl = new RoutedUICommand("Add Control Task", "AddControl", typeof(Window1));

		public static RoutedUICommand ShowProgram = new RoutedUICommand("Show/Hide", "ShowProgram", typeof(Window1));
		public static RoutedUICommand ShowDefault = new RoutedUICommand("Show/Hide", "ShowDefault", typeof(Window1));
		public static RoutedUICommand ShowSubtitle = new RoutedUICommand("Show/Hide", "ShowSubtitle", typeof(Window1));
		public static RoutedUICommand ShowControl = new RoutedUICommand("Show/Hide", "ShowControl", typeof(Window1));

		public static RoutedUICommand AddUser = new RoutedUICommand("Add new User", "AddUser", typeof(Window1));
		public static RoutedUICommand EditUser = new RoutedUICommand("Edit User", "EditUser", typeof(Window1));
		public static RoutedUICommand DeleteUser = new RoutedUICommand("Delete User", "DeleteUser", typeof(Window1));

        public static RoutedUICommand NewTemplate = new RoutedUICommand("New Screen", "NewTemplate", typeof(Window1));
        public static RoutedUICommand EditTemplate = new RoutedUICommand("Edit Screen", "EditTemplate", typeof(Window1));

		public static RoutedUICommand AddLimitPlayers = new RoutedUICommand("Add Limit Players", "AddLimitPlayers", typeof(Window1));
		public static RoutedUICommand DeleteLimitPlayers = new RoutedUICommand("Delete Limit Players", "DeleteLimitPlayers", typeof(Window1));

		#endregion

		private System.Windows.Forms.Timer _timerNetworkRefresher;
		private System.Windows.Forms.Timer _timerStatusTabRefresher;
		private System.Windows.Forms.Timer _timerTimelineRefresher;

		private bool ConnectToServer()
		{
            cfg.ProductCode = SerialKey.SerialKey._ProductCode.NetworkEdition;
			switch (cfg.ProductCode)
			{
				case DigitalSignage.SerialKey.SerialKey._ProductCode.EmbededEdition:
					throw new NotImplementedException();
					//break;
				case DigitalSignage.SerialKey.SerialKey._ProductCode.Menuboard:
				case DigitalSignage.SerialKey.SerialKey._ProductCode.StandaloneEdition:
/*					{
						ServerConfig scd;
						do
						{
							scd = new ServerConfig(true);
// 							scd.Hostname = cfg.ServerPath;
							scd.ShowDialog();

							if (scd.result == true)
							{
								cfg.ServerPath = scd.Hostname;
								cfg.ConnectToServer();
								if(cfg.ServerFound)
								{
									SerialKey.NESerialKey._ProductCode server_code = GetProductCodeFromServer();
									if (server_code != cfg.ProductCode)
									{
										MessageBox.Show(String.Format(Properties.Resources.mbCannotConnectToServiceCauseProductCode, cfg.ProductCode, server_code));
										cfg.ServerFound = false;
									}
								}
							}
							else if (scd.result == false)
								throw new Exception(Properties.Resources.exceptionConnectCancel);

						} while (!cfg.ServerFound);


						break;
					}*/
				case DigitalSignage.SerialKey.SerialKey._ProductCode.NetworkEdition:
					{
						if (cfg.ServerFound)
						{
							SerialKey.NESerialKey._ProductCode server_code = GetProductCodeFromServer();
							if (server_code != cfg.ProductCode)
							{
								MessageBox.Show(String.Format(Properties.Resources.mbCannotConnectToServiceCauseProductCode, cfg.ProductCode, server_code));
								cfg.ServerFound = false;
							}
						}

						if(!cfg.ServerFound)
						{
							ServerConfig scd;

							do
							{
								MessageBox.Show(Properties.Resources.mbCannotConnectToService2, Properties.Resources.titleMessageBox);
								scd = new ServerConfig(true);
								if (!cfg.ServerPath.Equals(Config.SAAS_DOMAIN))
									scd.Hostname = cfg.ServerPath;
	
								scd.ShowDialog();

								if (scd.result == true)
								{
									cfg.ServerPath = scd.Hostname;
									cfg.ConnectToServer();
									if (cfg.ServerFound)
									{
										SerialKey.NESerialKey._ProductCode server_p_code = GetProductCodeFromServer();
										if (server_p_code != cfg.ProductCode)
										{
											MessageBox.Show(String.Format(Properties.Resources.mbCannotConnectToServiceCauseProductCode, cfg.ProductCode, server_p_code));
											cfg.ServerFound = false;
										}
									}
								}
								else if (scd.result == false)
									throw new Exception(Properties.Resources.exceptionConnectCancel);

							} while (!cfg.ServerFound);
						}

						return true;
					}
				case DigitalSignage.SerialKey.SerialKey._ProductCode.SoftwareAsaService:
					cfg.ServerPath = Config.SAAS_DOMAIN;
					if (!cfg.ConnectToServer())
					{
						MessageBox.Show(Properties.Resources.mbCannotConnectToService, Properties.Resources.titleMessageBox);
						throw new Exception(Properties.Resources.exceptionConnectCancel);
					}

					return true;
				case DigitalSignage.SerialKey.SerialKey._ProductCode.DebugEdition:
					{
						ServerConfig scd;

						do
						{
							scd = new ServerConfig(true);
							if (!cfg.ServerPath.Equals(Config.SAAS_DOMAIN))
								scd.Hostname = cfg.ServerPath;

							scd.ShowDialog();

							if (scd.result == true)
							{
								cfg.ServerPath = scd.Hostname;
								cfg.ConnectToServer();
							}
							else if (scd.result == false)
								throw new Exception(Properties.Resources.exceptionConnectCancel);

						} while (!cfg.ServerFound);

						return true;
					}
                case DigitalSignage.SerialKey.SerialKey._ProductCode.SpecialEdition:
                    {
                        if (!cfg.ServerFound)
                        {
                            ServerConfig scd;

                            do
                            {
                                MessageBox.Show(Properties.Resources.mbCannotConnectToService2, Properties.Resources.titleMessageBox);
                                scd = new ServerConfig(true);
                                if (!cfg.ServerPath.Equals(Config.SAAS_DOMAIN))
                                    scd.Hostname = cfg.ServerPath;

                                scd.ShowDialog();

                                if (scd.result == true)
                                {
                                    cfg.ServerPath = scd.Hostname;
                                    cfg.ConnectToServer();
                                }
                                else if (scd.result == false)
                                    throw new Exception(Properties.Resources.exceptionConnectCancel);

                            } while (!cfg.ServerFound);
                        }

                        return true;
                    }
				case DigitalSignage.SerialKey.SerialKey._ProductCode.Undefined:
					MessageBox.Show(Properties.Resources.mbErrorUndefinedProduct, Properties.Resources.titleMessageBox, MessageBoxButton.OK, MessageBoxImage.Error);
					throw new Exception(Properties.Resources.exceptionConnectCancel);
			}
			return false;
		}

		/// <summary>
		/// 매니저 UpldateURL 변경
		/// </summary>
		private void CleanUpdateURL()
		{
			try
			{
				DigitalSignage.SerialKey.SerialKey._ProductCode pcode = cfg.ProductCode;

				XmlDocument updater_config = new XmlDocument();
				updater_config.Load(AppDomain.CurrentDomain.BaseDirectory + "ManagerUpdater.exe.config");

				XmlNode node = updater_config.SelectSingleNode("//child::setting[attribute::name=\"UpdateURL\"]");
				XmlNode nodevalue = node.SelectSingleNode("child::value");

				if (pcode == DigitalSignage.SerialKey.SerialKey._ProductCode.SoftwareAsaService)
				{
					nodevalue.ChildNodes[0].Value = "http://update.myivision.com/ivision21/SaaS/Manager/Default/";
				}
				else if (pcode == DigitalSignage.SerialKey.SerialKey._ProductCode.NetworkEdition)
				{
					nodevalue.ChildNodes[0].Value = "http://update.myivision.com/ivision21/NE/Manager/Default/";
				}
				updater_config.Save(AppDomain.CurrentDomain.BaseDirectory + "ManagerUpdater.exe.config");

			}
			catch { }
		}

        public Window1()
        {
			Properties.Resources.Culture = Properties.Settings.Default.DefaultCulture;
			CultureResources.ChangeCulture(Properties.Settings.Default.DefaultCulture);
			Thread.CurrentThread.CurrentCulture = Properties.Resources.Culture;
			Thread.CurrentThread.CurrentUICulture = Properties.Resources.Culture;

			InitializeComponent();

			InitializeLogger();
            
            #region 언어 메뉴
            try
            {
                foreach (CultureInfo cinfo in CultureResources.SupportedCultures)
                {
                    MenuItem item = new MenuItem();
                    item.Header = cinfo;
                    item.IsChecked = Properties.Resources.Culture.Equals(cinfo);

                    menuLanguages.Items.Add(item);
                }
            }
            catch 
            {
            	
            }

            #endregion


			//CleanUpdateURL();

			try
            {
                /*
				TrialChecker checker = new TrialChecker();
				if(!checker.IsValid)
				{
					MessageBox.Show(checker.LastMessage);

					this.Visibility = Visibility.Hidden;
					Application.Current.Shutdown();
				}
                */

				//	그룹 정보를 표시한다
                GPTree.ShowDetailInformation = true;

				cfg = Config.GetConfig;

				if(!ConnectToServer())
				{
					throw new Exception();
				}

				while (!ServerDataCache.GetInstance.IsConnected) //while we are not authorized
                {
                    LoginDlg dlg = new LoginDlg(cfg, cfg.GetConfigStr("user", ""));
                    dlg.ShowDialog();
                    if (!dlg.result)
                    {
                        throw new Exception(Properties.Resources.exceptionLoginCancel);
                    }
                    else
                    {
						using (new DigitalSignage.Common.WaitCursor())
						{

							if (ServerDataCache.GetInstance.Initialize(dlg.login, dlg.password))
							{
								cfg.SetConfigStr("user", dlg.login);
								userID.Text = dlg.login;

								// 로그인 성공 로그인 정보 송출
								SendConnectionInfo();
                                GetProductCodeFromServer();
								UpdateMetaTagInformation();
								break;
							}
							else
							{
								try
								{
									///	접속 로그 기록
									cfg.ServerLogList.InsertDetailLog(this.userID.Text, (int)(LogType.LogType_Managing | LogType.LogType_Connection | LogType.LogType_Failed), "", "", "", 0, 0, TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()));
								}
								catch { }
								MessageBox.Show(Properties.Resources.mbFailedLogin, Properties.Resources.titleMessageBox);

							}
						}
                    }
                }



// 				contextMenu = FindResource("contextMenu") as ContextMenu;

 
				cleanDynMenu();
				status.Text = "Server version " + ((cfg.ServerVersion >> 48) & 0xFFFF) + "." +
                    ((cfg.ServerVersion >> 32) & 0xFFFF) + "." +
                    ((cfg.ServerVersion >> 16) & 0xFFFF) + "." + (cfg.ServerVersion & 0xFFFF);
                UpdateLayout();
                InitStatusTabRefresh();
				InitTimelineRefresh();
                InitNetworkBrowserRefresh();

				GPTree.event_TaskWizard += new RoutedEventHandler(GPTree_OnClickTaskWizard);
				GPTree.event_NewTask += new RoutedEventHandler(GPTree_OnClickNewTask);
				GPTree.event_Subtitle += new RoutedEventHandler(GPTree_OnClickSubtitle);
				GPTree.event_Control += new RoutedEventHandler(GPTree_OnClickControl);
				GPTree.event_DefaultTask += new RoutedEventHandler(GPTree_OnClickAddDefault);
				GPTree.event_User += new RoutedEventHandler(GPTree_User);
				GPTree.event_EditPlayer += new RoutedEventHandler(GPTree_EditPlayer);
				GPTree.event_DeletePlayer += new RoutedEventHandler(GPTree_DeletePlayer);
				GPTree.event_AddLimitPlayers += new RoutedEventHandler(GPTree_event_AddLimitPlayers);
				GPTree.event_DeleteLimitPlayers += new RoutedEventHandler(GPTree_event_DeleteLimitPlayers);
                GPTree.event_ViewScreen += new RoutedEventHandler(GPTree_event_VNCViewer);
                GPTree.event_AMTDetails += new RoutedEventHandler(GPTree_event_AMTDetails);

				GPTree.event_PowerOn += new RoutedEventHandler(GPTree_event_PowerOn);
				GPTree.event_PowerOff += new RoutedEventHandler(GPTree_event_PowerOff);
				GPTree.event_Restart += new RoutedEventHandler(GPTree_event_Restart);

				//	hsshin 타임라인에 컨텍스트 메뉴 추가
				tasksDiagram.OnContextMenu += new TimeDiagram.CallContextMenu(TimeDiagram_ContextMenu);

				tasksDiagram.OnItemDoubleClicked += new TimeDiagram.ItemDoubleclicked(tasksDiagram_OnItemDoubleClicked);

				//	hsshin status ListView에 컨텍스트 메뉴 추가
				statusListBox.MouseRightButtonUp += new MouseButtonEventHandler(statusListBox_ContextMenu);

				this.SourceInitialized += new EventHandler(Window_SourceInitialized);

				return;
            }
            catch (Exception e)
            {
                logger.Error(e + "");
				this.Visibility = Visibility.Hidden;
				Application.Current.Shutdown();
            }            
		}

        private void InitStatusTabRefresh()
        {
			_timerStatusTabRefresher = new System.Windows.Forms.Timer();
			_timerStatusTabRefresher.Interval = 10000;
			_timerStatusTabRefresher.Tick += new EventHandler(_timerStatusTabRefresher_Tick);
            _timerStatusTabRefresher.Start();
        }

        private void InitNetworkBrowserRefresh()
        {
            _timerNetworkRefresher = new System.Windows.Forms.Timer();
            _timerNetworkRefresher.Interval = 30000;
            _timerNetworkRefresher.Tick += new EventHandler(_timerNetworkRefresher_Tick);
            _timerNetworkRefresher.Start();
        }

		private void InitTimelineRefresh()
		{
			_timerTimelineRefresher = new System.Windows.Forms.Timer();
			_timerTimelineRefresher.Interval = 300000;
			_timerTimelineRefresher.Tick += new EventHandler(_timerTimelineRefresher_Tick);
			_timerTimelineRefresher.Start();
		}

		void _timerTimelineRefresher_Tick(object sender, EventArgs e)
		{
			_timerTimelineRefresher.Stop();
			if(btnCommit.Visibility != Visibility.Visible)
			{
				updateTasksList(GPTree.SelectedItem);
			}
			_timerTimelineRefresher.Start();
		}


        void _timerNetworkRefresher_Tick(object sender, EventArgs e)
        {
            _timerNetworkRefresher.Stop();
            try
            {
                GPTree.UpdateTree();
            }
            catch { }
            _timerNetworkRefresher.Start();
        }


        public static string ConvertDisplayFileSize(string size)
        {
            double dSize = Double.Parse(size);

            double convertSize = (double)0;
            NumberFormatInfo nfi = new CultureInfo("en-US", false).NumberFormat;
            nfi.NumberDecimalDigits = 2;

            string retValue = "";
            try
            {
                if (dSize >= (1024 * 1024))
                {
                    convertSize = dSize / (1024 * 1024);
                    retValue = convertSize.ToString("N", nfi) + " GB";
                }
                else if (dSize >= 1024 )
                {
                    convertSize = dSize / 1024;
                    retValue = convertSize.ToString("N", nfi) + " MB";
                }
                else
                {
                    convertSize = dSize;
                    retValue = convertSize.ToString("N", nfi) + " KB";
                }
            }
            catch (Exception ex)
            {
                retValue = size;
                Console.WriteLine(ex.StackTrace);
            }

            return retValue;
        }


		/// <summary>
		/// Changing the UI culture of Scheduler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void menuLanguages_Selected(object sender, RoutedEventArgs e)
		{
			MenuItem parent = e.Source as MenuItem;
			MenuItem selected = e.OriginalSource as MenuItem;
			CultureInfo info = selected.Header as CultureInfo;

			if (Properties.Resources.Culture != null && !Properties.Resources.Culture.Equals(info))
			{
				Thread.CurrentThread.CurrentCulture = info;
				Thread.CurrentThread.CurrentUICulture = info;
				CultureResources.ChangeCulture(info);

				//	Setting 파일에 저장
				Properties.Settings.Default.DefaultCulture = info;
				Properties.Settings.Default.Save();

                #region 메뉴 체크 로직
                try
                {
                    foreach (MenuItem item in ((RibbonDropDownButton)sender).Items)
                    {
                        item.IsChecked = info.Equals(item.Header);
                    }
                }
                catch { }
                #endregion

				tasksDate.OnApplyTemplate();
				if (tasksDate.MonthView != null)
					tasksDate.MonthView.OnApplyTemplate();
				
// 				selected.IsChecked = true;
			}			
		}

        /// <summary>
        /// Fires every 5 seconds (see InitStatusTabRefresh() method that initializes the timer) and refreshes the players list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void _timerStatusTabRefresher_Tick(object sender, EventArgs e)
        {
			_timerStatusTabRefresher.Stop();
			ServerDataCache sdc = ServerDataCache.GetInstance;
			if (!sdc.HasPermission("VIEW PLAYER")) return;

            if (!GPTree.SelectedGroupGID.Equals("-1"))
                updatePlayersList(GPTree.SelectedGroupGID);
            else
                updatePlayersList2(GPTree.SelectPlayerID);
			_timerStatusTabRefresher.Start();
        }

        private void GPTree_User(object sender, RoutedEventArgs e)
        {
			CmdUserManagement(sender, e as ExecutedRoutedEventArgs);

        }

		private void GPTree_EditPlayer(object sender, RoutedEventArgs e)
        {
			this.CmdEditPlayer(sender, e);
		}

		private void GPTree_DeletePlayer(object sender, RoutedEventArgs e)
        {
			this.CmdDeletePlayer(sender, e);
		}

		private void GPTree_ViewScreen(object sender, RoutedEventArgs e)
        {
			this.CmdRemoteView(sender, e);
		}

		void GPTree_event_AMTDetails(object sender, RoutedEventArgs e)
		{
			this.CmdAMTDetails(sender, e);
		}

        void GPTree_event_VNCViewer(object sender, RoutedEventArgs e)
        {
            this.CmdVNCViewer(sender, e);
        }

		void GPTree_OnClickSubtitle(object sender, RoutedEventArgs e)
        {
			tSubtilesUI_Click(sender, e);
		}
		void GPTree_OnClickControl(object sender, RoutedEventArgs e)
		{
			CmdAddControl(sender, e);
		}

		void GPTree_OnClickAddDefault(object sender, RoutedEventArgs e)
		{
			CmdAddDefault(sender, e);
		}

		void GPTree_OnClickNewTask(object sender, RoutedEventArgs e)
        {
			CmdAddTask(sender, e);
		}
		void GPTree_OnClickEditTask(object sender, RoutedEventArgs e)
		{
			CmdEditTask(sender, e);
		}
		void GPTree_OnClickDeleteTask(object sender, RoutedEventArgs e)
		{
			CmdDeleteTask(sender, e);
		}
		void GPTree_OnClickTaskWizard(object sender, RoutedEventArgs e)
		{
			CmdTaskWizard(sender, e);
		}

		void OnPreviewTask(object sender, RoutedEventArgs e)
		{
			CmdPreviewTask(sender, e);
		}

		void OnExportTask(object sender, RoutedEventArgs e)
		{
			CmdExportTask(sender, e);
		}

		void GPTree_event_Restart(object sender, RoutedEventArgs e)
		{
			XmlNode selected_node = GPTree.SelectedItem as XmlNode;

			if (selected_node != null)
			{
				if (selected_node.LocalName == "player")
				{
					MessageBox.Show(GetNPCUMessage(Properties.Resources.labelCtrlReboot, Config.GetConfig.ServerPlayersList.NPCUControl(selected_node.Attributes["pid"].Value, "2")), Properties.Resources.titleMessageBox, MessageBoxButton.OK, MessageBoxImage.Information);
				}
				else
				{
					//	그룹인 경우
					try
					{
						using (DigitalSignage.Common.WaitCursor wc = new DigitalSignage.Common.WaitCursor())
						{
							ArrayList arrPids = new ArrayList();

							XmlNodeList arrPlayers = ServerDataCache.GetInstance.GetPlayerList(selected_node.Attributes["gid"].Value, false);

							if (arrPlayers != null)
							{
								foreach (XmlNode node in arrPlayers)
								{
									arrPids.Add(node.Attributes["pid"].Value);
								}

								MessageBoxResult mbr = MessageBox.Show(String.Format(Properties.Resources.mbQuestionPowerControl, arrPids.Count, Properties.Resources.labelRestart), Properties.Resources.titleMessageBox, MessageBoxButton.OKCancel, MessageBoxImage.Question);

								if (mbr == MessageBoxResult.OK)
								{
									String[] arrPidToString = arrPids.ToArray(typeof(String)) as String[];
									String sReturn = Config.GetConfig.ServerPlayersList.NPCUControlByPIDArray(arrPidToString, "2");

									wc.Dispose();

									NPCUResult npcu = new NPCUResult(String.Format(Properties.Resources.titlePowerManagement, Properties.Resources.labelRestart), sReturn);
									npcu.ShowDialog();
								}
							}
						}
						e.Handled = true;

					}
					catch (Exception err)
					{
						logger.Error(err + "");
					}
				}
			}
		}

		void GPTree_event_PowerOff(object sender, RoutedEventArgs e)
		{
			XmlNode selected_node = GPTree.SelectedItem as XmlNode;

			if (selected_node != null)
			{
				if (selected_node.LocalName == "player")
				{
					MessageBox.Show(GetNPCUMessage(Properties.Resources.labelCtrlPowerOff, Config.GetConfig.ServerPlayersList.NPCUControl(selected_node.Attributes["pid"].Value, "0")), Properties.Resources.titleMessageBox, MessageBoxButton.OK, MessageBoxImage.Information);
				}
				else
				{
					//	그룹인 경우
					try
					{
						using (DigitalSignage.Common.WaitCursor wc = new DigitalSignage.Common.WaitCursor())
						{
							ArrayList arrPids = new ArrayList();

							XmlNodeList arrPlayers = ServerDataCache.GetInstance.GetPlayerList(selected_node.Attributes["gid"].Value, false);

							if (arrPlayers != null)
							{
								foreach (XmlNode node in arrPlayers)
								{
									arrPids.Add(node.Attributes["pid"].Value);
								}

								MessageBoxResult mbr = MessageBox.Show(String.Format(Properties.Resources.mbQuestionPowerControl, arrPids.Count, Properties.Resources.labelPowerOff), Properties.Resources.titleMessageBox, MessageBoxButton.OKCancel, MessageBoxImage.Question);

								if (mbr == MessageBoxResult.OK)
								{
									String[] arrPidToString = arrPids.ToArray(typeof(String)) as String[];
									String sReturn = Config.GetConfig.ServerPlayersList.NPCUControlByPIDArray(arrPidToString, "0");

									wc.Dispose();

									NPCUResult npcu = new NPCUResult(String.Format(Properties.Resources.titlePowerManagement, Properties.Resources.labelPowerOff), sReturn);
									npcu.ShowDialog();
								}
							}
						}
						e.Handled = true;

					}
					catch (Exception err)
					{
						logger.Error(err + "");
					}
				}
			}
		}

		void GPTree_event_PowerOn(object sender, RoutedEventArgs e)
		{

			XmlNode selected_node = GPTree.SelectedItem as XmlNode;
          
			if (selected_node != null)
			{
				if (selected_node.LocalName == "player")
				{
					MessageBox.Show(GetNPCUMessage(Properties.Resources.labelCtrlPowerOn, Config.GetConfig.ServerPlayersList.NPCUControl(selected_node.Attributes["pid"].Value, "1")), Properties.Resources.titleMessageBox, MessageBoxButton.OK, MessageBoxImage.Information);
				}
				else
				{
					//	그룹인 경우
					try
					{
						using (DigitalSignage.Common.WaitCursor wc = new DigitalSignage.Common.WaitCursor())
						{
							ArrayList arrPids = new ArrayList();

							XmlNodeList arrPlayers = ServerDataCache.GetInstance.GetPlayerList(selected_node.Attributes["gid"].Value, false);

							if (arrPlayers != null)
							{
								foreach (XmlNode node in arrPlayers)
								{
									arrPids.Add(node.Attributes["pid"].Value);
								}

								MessageBoxResult mbr = MessageBox.Show(String.Format(Properties.Resources.mbQuestionPowerControl, arrPids.Count, Properties.Resources.labelPowerOn), Properties.Resources.titleMessageBox, MessageBoxButton.OKCancel, MessageBoxImage.Question);

								if (mbr == MessageBoxResult.OK)
								{
									String[] arrPidToString = arrPids.ToArray(typeof(String)) as String[];
									String sReturn = Config.GetConfig.ServerPlayersList.NPCUControlByPIDArray(arrPidToString, "1");

									wc.Dispose();

									NPCUResult npcu = new NPCUResult(String.Format(Properties.Resources.titlePowerManagement, Properties.Resources.labelPowerOn), sReturn);
									npcu.ShowDialog();
								}
							}
						}
						e.Handled = true;

					}
					catch (Exception err)
					{
						logger.Error(err + "");
					}
				}
			}

		}

		void GPTree_event_DeleteLimitPlayers(object sender, RoutedEventArgs e)
		{
			if (MessageBoxResult.Yes == MessageBox.Show(Properties.Resources.mbDeleteLimitPlayers, Properties.Resources.titleMessageBox, MessageBoxButton.YesNo, MessageBoxImage.Warning))
				cfg.ServerPlayersList.DeleteLimitInfo(GPTree.SelectedGroupGID);
			e.Handled = true;

		}

		void GPTree_event_AddLimitPlayers(object sender, RoutedEventArgs e)
		{
			LimitPlayersDlg dlg = new LimitPlayersDlg(cfg);
			dlg.PlayerGroup = GPTree.SelectedGroup;
			dlg.ShowDialog();
			e.Handled = true;
		}

		private void LoadConfig()
		{
			try
			{
				Config cfg = Config.GetConfig;
				String value = cfg.GetConfigStr("tab_timeline", "60");
				rdef_timeline.Height = new GridLength(Convert.ToDouble(value), GridUnitType.Star);
				value = cfg.GetConfigStr("tab_status", "40");
				rdef_status.Height = new GridLength(Convert.ToDouble(value), GridUnitType.Star);
				value = cfg.GetConfigStr("tab_pbrowser", "200");
				cdef_pbrowser.Width = new GridLength(Convert.ToDouble(value), GridUnitType.Pixel);

				value = cfg.GetConfigStr("use_timeline_scroll", "False");
				cbShowScroll.IsChecked = cfg.UseScrollInTimeline = Convert.ToBoolean(value);
	
			}
			catch { }
		}
		private void SaveConfig()
		{
			Config cfg = Config.GetConfig;
			cfg.SetConfigStr("tab_timeline", rdef_timeline.Height.Value.ToString());
			cfg.SetConfigStr("tab_status", rdef_status.Height.Value.ToString());
			cfg.SetConfigStr("tab_pbrowser", cdef_pbrowser.Width.Value.ToString());
			cfg.SetConfigStr("use_timeline_scroll", cbShowScroll.IsChecked.Value.ToString());

			tasksDiagram.SaveConfig();

		}

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
			SaveConfig();

//             cfg.SetConfigInt("width", (int)this.Width);
//             cfg.SetConfigInt("height", (int)this.Height);
//             cfg.SetConfigInt("left", (int)this.Left);
//             cfg.SetConfigInt("top", (int)this.Top);

            if (_timerStatusTabRefresher != null)
            {
                _timerStatusTabRefresher.Stop();
				_timerStatusTabRefresher.Dispose();
                _timerStatusTabRefresher = null;
            }

			if (_timerTimelineRefresher != null)
			{
				_timerTimelineRefresher.Stop();
				_timerTimelineRefresher.Dispose();
				_timerTimelineRefresher = null;
			}

            if (_timerNetworkRefresher != null)
			{
                _timerNetworkRefresher.Stop();
                _timerNetworkRefresher.Dispose();
                _timerNetworkRefresher = null;
			}
        }

        private void restoreParams()
        {
// 			this.Width = cfg.GetConfigInt("width", 800);
//             this.Height = cfg.GetConfigInt("height", 600);
//             this.Left = cfg.GetConfigInt("left", 100);
//             this.Top = cfg.GetConfigInt("top", 100);
// 			
// 			if (this.Top < 0)
// 				this.Top = 0;
// 			if (this.Top < 0)
// 				this.Top = 0;


			ServerDataCache sdc = ServerDataCache.GetInstance;
			tUsers.IsEnabled = sdc.HasPermission("VIEW USER");

			tTaskWizard.IsEnabled = tDefaultTask.IsEnabled = tSubtitleTask.IsEnabled = tNewTask.IsEnabled = tNewPlayer.IsEnabled = sdc.HasPermission("CREATE SCHEDULE");
			tEditTask.IsEnabled = sdc.HasPermission("UPDATE TASK");
			tDeleteTask.IsEnabled = sdc.HasPermission("DELETE TASK");

			tNewGroup.IsEnabled = sdc.HasPermission("CREATE GROUP");
			tEditGroup.IsEnabled = sdc.HasPermission("UPDATE GROUP");
			tDeleteGroup.IsEnabled = sdc.HasPermission("DELETE GROUP");

			tNewPlayer.IsEnabled = sdc.HasPermission("CREATE PLAYER");
			tEditPlayer.IsEnabled = sdc.HasPermission("UPDATE PLAYER");
			tDeletePlayer.IsEnabled = sdc.HasPermission("DELETE PLAYER");

			tNewPlayer.Visibility = (cfg.ProductCode != DigitalSignage.SerialKey.SerialKey._ProductCode.StandaloneEdition) ? Visibility.Visible : Visibility.Collapsed;
        }

        private void cleanDynMenu()
        {
            // deleting all object from toolbar, except four default
//             while (dynMenu.Items.Count > 7)
//                 dynMenu.Items.RemoveAt(7);


			ServerDataCache sdc = ServerDataCache.GetInstance;

// 			tUsers.IsEnabled = sdc.HasPermission("VIEW USER");
// 
// 			tTaskWizard.IsEnabled = tDefaultTask.IsEnabled = tSubtitleTask.IsEnabled = tNewTask.IsEnabled = tNewPlayer.IsEnabled = sdc.HasPermission("CREATE SCHEDULE");
// 			tEditTask.IsEnabled = sdc.HasPermission("UPDATE TASK");
// 			tDeleteTask.IsEnabled = sdc.HasPermission("DELETE TASK");
// 
// 			tNewGroup.IsEnabled = sdc.HasPermission("CREATE GROUP");
// 			tEditGroup.IsEnabled = sdc.HasPermission("UPDATE GROUP");
// 			tDeleteGroup.IsEnabled = sdc.HasPermission("DELETE GROUP");
// 
// 			tNewPlayer.IsEnabled = sdc.HasPermission("CREATE PLAYER");
// 			tEditPlayer.IsEnabled = sdc.HasPermission("UPDATE PLAYER");
// 			tDeletePlayer.IsEnabled = sdc.HasPermission("DELETE PLAYER"); 

			tUsers.IsEnabled = sdc.HasPermission("VIEW USER");

			tTaskWizard.IsEnabled = tDefaultTask.IsEnabled = tSubtitleTask.IsEnabled = tNewTask.IsEnabled = tControlTask.IsEnabled = sdc.HasPermission("CREATE SCHEDULE");
			tEditTask.IsEnabled = false;
			tDeleteTask.IsEnabled = false;

			tNewGroup.IsEnabled = false;
			tEditGroup.IsEnabled = false;
			tDeleteGroup.IsEnabled = false;

			tNewPlayer.IsEnabled = false;
            tEditPlayer.IsEnabled = false;
			tDeletePlayer.IsEnabled = false;

			tLogs.IsEnabled = tReports.IsEnabled = tPowerManagement.IsEnabled = sdc.HasPermission("ADMINISTRATOR");

			tPowerManagement.Visibility = ServerDataCache.GetInstance.IsRunningNPCUServer ? Visibility.Visible : Visibility.Collapsed;
        }

        #region Menu events

        private void CmdFTP(object sender, ExecutedRoutedEventArgs e)
        {
            try
            {
                FTPStatuses dlg = new FTPStatuses(cfg);
                dlg.Owner = this;
                dlg.ShowDialog();
				RibbonMain.Focus();

            }
            catch (Exception err)
            {
                logger.Error(err + "");
            }
        }

        private void CmdShowLogs(object sender, ExecutedRoutedEventArgs e)
        {
            try
            {
                XmlNode row = this.GPTree.SelectedItem as XmlNode;

                LogMiner dlg = new LogMiner(row);
                dlg.ShowDialog();

                //ShowLogDialog dlg = new ShowLogDialog(cfg, row.Attributes["pid"].Value, row.Attributes["name"].Value);
                //dlg.Owner = this;
                //dlg.ShowDialog();
            }
            catch (Exception err)
            {
                logger.Error(err + "");
            }


            RibbonMain.Focus();
        }

        private void CmdShowReports(object sender, ExecutedRoutedEventArgs e)
        {
            try
            {
                XmlNode row = this.GPTree.SelectedItem as XmlNode;

                ReportViewer dlg = new ReportViewer(row);
                dlg.ShowDialog();

                //ShowLogDialog dlg = new ShowLogDialog(cfg, row.Attributes["pid"].Value, row.Attributes["name"].Value);
                //dlg.Owner = this;
                //dlg.ShowDialog();
            }
            catch (Exception err)
            {
                logger.Error(err + "");
            }


            RibbonMain.Focus();
        }
        #endregion

        #region Event handlers

        private void MenuItem_Click_4(object sender, RoutedEventArgs e)
        {
            try
            {
				
				//FindPlayers dlg = new FindPlayers(cfg);
				//dlg.TargetObject = null/*GPTree.SelectedGroup*/;
				//dlg.Owner = this;
				//dlg.ShowDialog();
				//RibbonMain.Focus();

                GPTree.UpdateTree();
            }
            catch (Exception err)
            {
                logger.Error(err + "");
            }
        }

        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            
			using (new DigitalSignage.Common.WaitCursor())
			{
				try
				{
					GPTree.SetParent(this, cfg);
					GPTree.UpdateTree();
					tasksDate.SelectedDateTime = DateTime.Today;
					updateTasksList(GPTree.SelectedItem);

					this.WindowState = WindowState.Maximized;
					RibbonMain.Focus();

					cbShowProgram.IsChecked = cfg.IsShowTimeSchedule;
					cbShowDefault.IsChecked = cfg.IsShowRepeatationSchedule;
					cbShowControl.IsChecked = cfg.IsShowControlSchedule;
					cbShowSubtitle.IsChecked = cfg.IsShowSubtitleSchedule;
					cbShowScroll.IsChecked = cfg.UseScrollInTimeline;

					if (!cfg.IsAvailableSubtitleSchedule)
					{
						cbShowSubtitle.Visibility = Visibility.Collapsed;
						tSubtitleTask.Visibility = Visibility.Collapsed;
					}

					if (!cfg.IsAvailableControlSchedule)
					{
						cbShowControl.Visibility = Visibility.Collapsed;
						tControlTask.Visibility = Visibility.Collapsed;
					}

                    if (!cfg.IsAvailableComplexPane)
                    {
                        tComplexPane.Visibility = Visibility.Collapsed;
                        tTaskWizard.Visibility = Visibility.Collapsed;
                    }

                    if (!cfg.IsAvailableTemplate)
                    {
                        tTemplate.Visibility = Visibility.Collapsed;
                    }

					LoadConfig();
				}
				catch (Exception err)
				{
					logger.Error(err + "");
				}
			}
        }

		#region Show/Hide routines
		private void CmdShowAndHide(object sender, RoutedEventArgs e)
		{
			Config con = Config.GetConfig;
			con.IsShowControlSchedule = cbShowControl.IsChecked.Value;
			con.IsShowTimeSchedule = cbShowProgram.IsChecked.Value;
			con.IsShowSubtitleSchedule = cbShowSubtitle.IsChecked.Value;
			con.IsShowRepeatationSchedule = cbShowDefault.IsChecked.Value;
			con.UseScrollInTimeline = cbShowScroll.IsChecked.Value;

			tasksDiagram.Rebuild();
		}

		#endregion
        private void startFTP(object sender, RoutedEventArgs e)
        {
            try
            {
                // get assembly path
                string strAppDir = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetModules()[0].FullyQualifiedName);
                Process process = new Process();
                process.StartInfo.FileName = strAppDir + "\\slimftpd.exe";
                process.StartInfo.WorkingDirectory = strAppDir;
                process.Start();
            }
            catch (Exception err)
            {
				MessageBox.Show(Properties.Resources.mbCannotStartFTP + err.Message);
                logger.Error(err + "");
            }
        }

         #endregion

        #region Tree routines
        private void GPTree_SelectedItemChanged(object sender, TreeViewItem item)
        {
            try
            {
				updateTasksList(GPTree.SelectedItem);

                cleanDynMenu();

                if (item == null) return;

				ServerDataCache sdc = ServerDataCache.GetInstance;

				bool viewPermission = sdc.HasPermission("VIEW PLAYER");

				XmlNode row = item.DataContext as XmlNode;

				if (row != null)
				{

					if (row.LocalName.Equals("group"))
					{
						tNewGroup.IsEnabled = sdc.HasPermission("CREATE GROUP");
						tNewPlayer.IsEnabled = sdc.HasPermission("CREATE PLAYER");

						tEditGroup.IsEnabled = sdc.HasPermission("UPDATE GROUP");

                        if (!row.Attributes["gid"].Value.Equals(Config.ROOT_GROUP_ID)) // we can't delete root group
						{
							tDeleteGroup.IsEnabled = sdc.HasPermission("DELETE GROUP");
						}
						else tDeleteGroup.IsEnabled = false;

						if (viewPermission) updatePlayersList(row.Attributes["gid"].Value);

					}
					else if(row.LocalName.Equals("player"))
					{
						tEditPlayer.IsEnabled = sdc.HasPermission("UPDATE PLAYER");
						tDeletePlayer.IsEnabled = sdc.HasPermission("DELETE PLAYER");

						if (viewPermission) updatePlayersList2(row.Attributes["pid"].Value);                       
					}
					
					return;
				}
            }
            catch (Exception err)
            {
                logger.Error(err + "");
            }
        }

        void mi_Click(object sender, RoutedEventArgs e)
        {
            //throw new NotImplementedException();
			//RemoteMonitorDlg dlg = new RemoteMonitorDlg(cfg, ((DataSet.playersRow)GPTree.SelectedItem).host);
			//dlg.Owner = this;
			//dlg.ShowDialog();
			//RibbonMain.Focus();

        }

		private void CmdAddGroup(object sender, RoutedEventArgs e)
        {
            try
            {
                GPTree.CmdAddGroup(null, null);
            }
            catch (Exception err)
            {
                logger.Error(err + "");
            }
        }

		private void CmdEditGroup(object sender, RoutedEventArgs e)
        {
            try
            {
                GPTree.CmdEditGroup(null, null);
            }
            catch (Exception err)
            {
                logger.Error(err + "");
            }
        }

        private void CmdDeleteGroup(object sender, RoutedEventArgs e)
        {
            try
            {
                GPTree.CmdDeleteGroup(null, null);
            }
            catch (Exception err)
            {
                logger.Error(err + "");
            }
        }
        #endregion

        #region Status box routines

		/// <summary>
		/// 접속 필터
		/// </summary>
		enum FilterConnections
		{
			/// <summary>
			/// 필터 없음
			/// </summary>
			None,
			/// <summary>
			/// 온라인만 보기
			/// </summary>
			Online,
			/// <summary>
			/// 오프라인만 보기
			/// </summary>
			Offline
		}

		private void statusListBox_ContextMenu(object sender, MouseButtonEventArgs e)
        {
			StatusCMenu = new System.Windows.Controls.ContextMenu();
			ServerDataCache sdc = ServerDataCache.GetInstance;

			MenuItem mi = null;
            
			mi = new MenuItem();
			mi.Header = Properties.Resources.labelView;
			mi.Icon = GetIconImage(Properties.Resources.zoom_in16);
			StatusCMenu.Items.Add(mi);

			MenuItem submi = new MenuItem();
			submi.Header = Properties.Resources.labelAllPlayer;
			submi.IsCheckable = true;
			submi.IsChecked = colState.Filter == null;
			submi.Click += new RoutedEventHandler(submi_None_Click);
			mi.Items.Add(submi);

			submi = new MenuItem();
			submi.Header = Properties.Resources.labelOnlyOnline;
			submi.IsCheckable = true;
			submi.IsChecked = FilterConnections.Online.Equals(colState.Filter);
			submi.Click += new RoutedEventHandler(submi_Online_Click);
			mi.Items.Add(submi);

			submi = new MenuItem();
			submi.Header = Properties.Resources.labelOnlyOffline;
			submi.IsCheckable = true;
			submi.IsChecked = FilterConnections.Offline.Equals(colState.Filter);
			submi.Click += new RoutedEventHandler(submi_Offline_Click);
			mi.Items.Add(submi);
			
			Separator sp = new Separator();
			mi.Items.Add(sp);

			submi = new MenuItem();
			submi.Header = Properties.Resources.labelShowFilter;
			submi.IsCheckable = true;
			submi.IsChecked = rdFilter.Height == new GridLength(35);
			submi.Click += new RoutedEventHandler(submi_Filter_Click);
			mi.Items.Add(submi);

			//	Task 
			sp = new Separator();
			StatusCMenu.Items.Add(sp);

			bool perm_CreateTask = sdc.HasPermission("CREATE SCHEDULE");
			if (cfg.IsAvailableComplexPane)
			{
				mi = new MenuItem();
				mi.Header = Properties.Resources.menuitemTaskWizard;
				mi.Icon = GetIconImage(Properties.Resources.icon_task_wizard);
				mi.IsEnabled = perm_CreateTask;
				mi.Click += new RoutedEventHandler(this.CmdTaskWizard);
				StatusCMenu.Items.Add(mi);
			}

			mi = new MenuItem();
			mi.Header = Properties.Resources.menuitemAddDefaultSchedule;
			mi.IsEnabled = perm_CreateTask;
			mi.Icon = GetIconImage(Properties.Resources.icon_default);
			mi.Click += new RoutedEventHandler(this.CmdAddDefault);
			StatusCMenu.Items.Add(mi);
			mi = new MenuItem();
			mi.Header = Properties.Resources.menuitemAddTaskProgram;
			mi.Icon = GetIconImage(Properties.Resources.icon_add_program_task);
			mi.IsEnabled = perm_CreateTask;
			mi.Click += new RoutedEventHandler(this.CmdAddTask);
			StatusCMenu.Items.Add(mi);

			if (cfg.IsAvailableSubtitleSchedule)
			{
				mi = new MenuItem();
				mi.Header = Properties.Resources.menuitemAddSubtitle;
				mi.IsEnabled = perm_CreateTask;
				mi.Icon = GetIconImage(Properties.Resources.icon_add_subtitle_task);
				mi.Click += new RoutedEventHandler(this.CmdAddSubtitle);
				StatusCMenu.Items.Add(mi);
			}

			if (cfg.IsAvailableControlSchedule)
			{
				mi = new MenuItem();
				mi.Header = Properties.Resources.menuitemAddControl;
				mi.IsEnabled = perm_CreateTask;
				mi.Icon = GetIconImage(Properties.Resources.icon_add_control_task);
				mi.Click += new RoutedEventHandler(this.CmdAddControl);
				StatusCMenu.Items.Add(mi);
			}

			sp = new Separator();
			StatusCMenu.Items.Add(sp);

			mi = new MenuItem();
			mi.Header = Properties.Resources.menuitemEditPlayer;
			mi.IsEnabled = sdc.HasPermission("UPDATE PLAYER");
			mi.Icon = GetIconImage(Properties.Resources.icon_edit_player);
			mi.Click += new RoutedEventHandler(this.CmdEditPlayer);
			StatusCMenu.Items.Add(mi);

			mi = new MenuItem();
			mi.Header = Properties.Resources.menuitemDeletePlayer;
			mi.IsEnabled = sdc.HasPermission("DELETE PLAYER");
			mi.Icon = GetIconImage(Properties.Resources.icon_delete_player);
			mi.Click += new RoutedEventHandler(this.CmdDeletePlayer);
			StatusCMenu.Items.Add(mi);
			

            #region NPCU Command

			if (ServerDataCache.GetInstance.IsRunningNPCUServer)
			{
				sp = new Separator();
				StatusCMenu.Items.Add(sp);

				mi = new MenuItem();
				mi.Header = Properties.Resources.labelPowerControl;
				mi.IsEnabled = sdc.HasPermission("ADMINISTRATOR");
				mi.Icon = GetIconImage(Properties.Resources.power_on);
				//            mi.Icon = GetIconImage(Properties.Resources.zoom_in16);
				StatusCMenu.Items.Add(mi);

				submi = new MenuItem();
				submi.Header = Properties.Resources.labelCtrlPowerOn;
				submi.IsEnabled = sdc.HasPermission("ADMINISTRATOR");
				submi.Icon = GetIconImage(Properties.Resources.power_on);
				submi.Click += new RoutedEventHandler(submi_p_on_Click);
				mi.Items.Add(submi);

				submi = new MenuItem();
				submi.Header = Properties.Resources.labelCtrlPowerOff;
				submi.IsEnabled = sdc.HasPermission("ADMINISTRATOR");
				submi.Icon = GetIconImage(Properties.Resources.power_off);
				submi.Click += new RoutedEventHandler(submi_p_off_Click);
				mi.Items.Add(submi);

				submi = new MenuItem();
				submi.Header = Properties.Resources.labelCtrlReboot;
				submi.IsEnabled = sdc.HasPermission("ADMINISTRATOR");
				submi.Icon = GetIconImage(Properties.Resources.power_restart);
				submi.Click += new RoutedEventHandler(submi_restart_Click);
				mi.Items.Add(submi);

			}
            #endregion

            sp = new Separator();
            StatusCMenu.Items.Add(sp);

            mi = new MenuItem();
            mi.Header = Properties.Resources.menuitemRemoteView;
            mi.IsEnabled = true;
            mi.Icon = GetIconImage(Properties.Resources.icon_preview);
            mi.Click += new RoutedEventHandler(statusList_OnRemoveView);
            StatusCMenu.Items.Add(mi);

			sp = new Separator();
			StatusCMenu.Items.Add(sp);

			if (ServerDataCache.GetInstance.IsScheduleHandlerEnabled)
			{
				try
				{
					XmlNode node = ((ListBox)statusListBox).SelectedItem as XmlNode;

					int path = System.Convert.ToInt32(node.Attributes["_mnttype"].Value);
					bool bIsAMT = ((path & (int)PowerMntType.AMT) == (int)PowerMntType.AMT);

					mi = new MenuItem();
					mi.Header = Properties.Resources.menuitemAMTDetails;
					mi.IsEnabled = bIsAMT && sdc.HasPermission("ADMINISTRATOR");
					mi.Icon = GetIconImage(Properties.Resources.zoom_in16);
					mi.Click += new RoutedEventHandler(statusList_OnAMTDetails);
					StatusCMenu.Items.Add(mi);

					Separator sp1 = new Separator();
					StatusCMenu.Items.Add(sp1);
				}
				catch { }

			}

			//	Log 
			mi = new MenuItem();
			mi.Header = Properties.Resources.menuitemLog;
            mi.Icon = GetIconImage(Properties.Resources._16_log_view);
			mi.IsEnabled = (statusListBox.SelectedItem != null) && sdc.HasPermission("ADMINISTRATOR");
			mi.Click += new RoutedEventHandler(statusList_OnStatusDialog);
			StatusCMenu.Items.Add(mi);

			StatusCMenu.UpdateLayout();
			StatusCMenu.IsOpen = true;

			e.Handled = true;

        }

		String GetNPCUMessage(String request_name, String response_cd)
        {
            
            if (response_cd.Equals("0"))
            {
                return String.Format(Properties.Resources.mbSuccessNPCU, request_name);
            }
            else if (response_cd.Equals("1") || response_cd.Equals("2"))
            {
                return String.Format(Properties.Resources.mbErrorNPCU, request_name, response_cd, Properties.Resources.labelNPCU_01_02);
            }
            else if (response_cd.Equals("6"))
            {
                return String.Format(Properties.Resources.mbErrorNPCU, request_name, response_cd, Properties.Resources.labelNPCU_06);
            }
            else if (response_cd.Equals("7"))
            {
                return String.Format(Properties.Resources.mbErrorNPCU, request_name, response_cd, Properties.Resources.labelNPCU_07);
            }
            else if (response_cd.Equals("8"))
            {
                return String.Format(Properties.Resources.mbErrorNPCU, request_name, response_cd, Properties.Resources.labelNPCU_08);
            }
            else if (response_cd.Equals("98"))
            {
                return String.Format(Properties.Resources.mbErrorNPCU, request_name, response_cd, Properties.Resources.labelNPCU_98);
            }
            else if (response_cd.Equals("99"))
            {
                return String.Format(Properties.Resources.mbErrorNPCU, request_name, response_cd, Properties.Resources.labelNPCU_99);
            }
            else
            {
                return String.Format(Properties.Resources.mbErrorNPCU, request_name, response_cd, Properties.Resources.labelNPCU_00);
            }
        }

        void submi_p_on_Click(object sender, RoutedEventArgs e)
        {
            XmlNode node = ((ListBox)statusListBox).SelectedItem as XmlNode;
			if (node != null)
			{
                MessageBox.Show(GetNPCUMessage(Properties.Resources.labelCtrlPowerOn, Config.GetConfig.ServerPlayersList.NPCUControl(node.Attributes["pid"].Value, "1")), Properties.Resources.titleMessageBox, MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        void submi_p_off_Click(object sender, RoutedEventArgs e)
        {
            XmlNode node = ((ListBox)statusListBox).SelectedItem as XmlNode;
            if (node != null)
            {
                MessageBox.Show(GetNPCUMessage(Properties.Resources.labelCtrlPowerOff, Config.GetConfig.ServerPlayersList.NPCUControl(node.Attributes["pid"].Value, "0")), Properties.Resources.titleMessageBox, MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        void submi_restart_Click(object sender, RoutedEventArgs e)
        {
            XmlNode node = ((ListBox)statusListBox).SelectedItem as XmlNode;
            if (node != null)
            {
                MessageBox.Show(GetNPCUMessage(Properties.Resources.labelCtrlReboot, Config.GetConfig.ServerPlayersList.NPCUControl(node.Attributes["pid"].Value, "2")), Properties.Resources.titleMessageBox, MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }


		void submi_Filter_Click(object sender, RoutedEventArgs e)
		{
			if (rdFilter.Height == new GridLength(35))
			{
				rdFilter.Height = new GridLength(0);
				colName.Filter = String.Empty;
			}
			else
			{
				rdFilter.Height = new GridLength(35);
				colName.Filter = tbFilter.Text;
			}

			int selected = statusListBox.SelectedIndex;
			UpdatePlayerListBySorting(order_players);
			statusListBox.SelectedIndex = selected;
		}

		void submi_None_Click(object sender, RoutedEventArgs e)
		{
			colState.Filter = null;

			int selected = statusListBox.SelectedIndex;
			UpdatePlayerListBySorting(order_players);
			statusListBox.SelectedIndex = selected;
		}

		void submi_Online_Click(object sender, RoutedEventArgs e)
		{
			colState.Filter = FilterConnections.Online;

			int selected = statusListBox.SelectedIndex;
			UpdatePlayerListBySorting(order_players);
			statusListBox.SelectedIndex = selected;
		}

		void submi_Offline_Click(object sender, RoutedEventArgs e)
		{
			colState.Filter = FilterConnections.Offline;

			int selected = statusListBox.SelectedIndex;
			UpdatePlayerListBySorting(order_players);
			statusListBox.SelectedIndex = selected;
		}

		private void CopySelectedPlayerlistToClipboard()
		{
			try
			{
				if (statusListBox.SelectedItems != null)
				{
					string sClipboard = string.Empty;
					foreach (XmlNode item in statusListBox.SelectedItems)
					{
						sClipboard += GetPlayerInfo(item, "\t");
						sClipboard += "\r\n";
					}
					if (!String.IsNullOrEmpty(sClipboard))
					{
						Clipboard.Clear();
						Clipboard.SetText(sClipboard, TextDataFormat.Text);
					}
				}
			}
			catch (System.Exception ex)
			{
				MessageBox.Show(ex.Message, Properties.Resources.titleMessageBox, MessageBoxButton.OK, MessageBoxImage.Error);
			}
		}

		private void mi_CopyClipboardClick(object sender, RoutedEventArgs e)
		{
			CopySelectedPlayerlistToClipboard();
			e.Handled = true;

		}
		/// <summary>
		/// 단말 XML을 파싱하여 정보를 변환한다.
		/// </summary>
		/// <param name="node"></param>
		/// <param name="seperator"></param>
		/// <returns></returns>
		private String GetPlayerInfo(XmlNode node, String seperator)
		{
			try
			{
				String sReturn = String.Empty;

				sReturn += node.Attributes["pid"].Value + seperator;
				sReturn += node.ParentNode.Attributes["name"].Value + seperator;
				sReturn += node.Attributes["name"].Value + seperator;
				sReturn += node.Attributes["host"].Value + seperator;
                sReturn += node.Attributes["_ipv4addr"].Value + seperator;

				try
				{
					sReturn += (node.Attributes["state"].Value.Equals("0") ? "OffLine" : "OnLine") + seperator;
				}
				catch
				{
					sReturn += "OffLine" + seperator;
				}
				try
				{
					sReturn += DigitalSignage.Scheduler.Window1.ConvertDisplayFileSize(node.Attributes["freespace"].Value) + seperator;
				}
				catch 
				{
					sReturn += "Unknown" + seperator;
				}
				
				try
				{
					string h = node.Attributes["versionH"].Value;
					string l = node.Attributes["versionL"].Value;
					int major = (System.Convert.ToInt32(h) >> 16) & 0xFFFF;
					int minor = System.Convert.ToInt32(h) & 0xFFFF;
					int build = (System.Convert.ToInt32(l) >> 16) & 0xFFFF;;
					int revision = System.Convert.ToInt32(l) & 0xFFFF;;
					sReturn += String.Format("{0}.{1}.{2}.{3}", major, minor, build, revision) + seperator;
				}
				catch 
				{
					sReturn += "Unknown" + seperator;
				}


				sReturn += String.Format("{0}%", node.Attributes["cpurate"].Value) + seperator;
				sReturn += String.Format("{0}%", node.Attributes["memoryusage"].Value) + seperator;
				sReturn += node.Attributes["curr_screen"].Value + seperator;
				sReturn += node.Attributes["curr_subtitle"].Value + seperator;

				return sReturn;
			}
			catch 
			{
				return String.Empty;
			}
		}

        private void statusList_OnRemoveView(object sender, EventArgs e)
        {
            try
            {
                XmlNode node = ((ListBox)statusListBox).SelectedItem as XmlNode;
                if (node != null)
                {
                    string pwd = node.Attributes["_auth_pass"].Value;
                    if (String.IsNullOrEmpty(pwd)) pwd = "Test12#$";

                    bool bRedirectProxy = false;
                    if (ServerDataCache.GetInstance.IsRunningProxyServer)
                    {
                        MessageBoxResult re = MessageBox.Show(Properties.Resources.mbProxy, Properties.Resources.titleMessageBox, MessageBoxButton.YesNoCancel, MessageBoxImage.Question);

                        if (re == MessageBoxResult.Cancel)
                            return;
                        else if (re == MessageBoxResult.Yes) bRedirectProxy = true;
                        else bRedirectProxy = false;
                    }

                    if (bRedirectProxy)
                    {
                        Random rand = new Random(DateTime.Now.Millisecond);
                        string sRandCode = rand.Next(1, 99999).ToString();

                        String hostip = Config.GetConfig.ServerPath.Replace("tcp://", "").Replace(":888", "").TrimEnd('/');
                        //	프록시 서버를 거침


                        if (null != VNCLauncher.RunVncViewer(sRandCode, hostip, "5901", "", true, true, false, true, 50))
                        {
                            Config.GetConfig.ServerPlayersList.RunRemoteControl(sRandCode, node.Attributes["pid"].Value);

                        }
                        else
                        {
                            MessageBox.Show(String.Format(Properties.Resources.labelFailedCommand, Properties.Resources.menuitemRemoteView));
                        }
                    }
                    else
                    {
                        //	프록시 서버를 거치지 않음
                        if (null != VNCLauncher.RunVncViewer(node.Attributes["_ipv4addr"].Value,
                            "5900", pwd, true, true, false, true, 50))
                        {
                        }
                        else
                        {
                            MessageBox.Show(String.Format(Properties.Resources.labelFailedCommand, Properties.Resources.menuitemRemoteView));
                        }
                    }
                }

            }
            catch { }
        }

		private void statusList_OnAMTDetails(object sender, EventArgs e)
		{
			XmlNode node = ((ListBox)statusListBox).SelectedItem as XmlNode;
			if (node != null)
			{
				AMTDetailsDlg amtDetailes = new AMTDetailsDlg(node);
				amtDetailes.ShowDialog();
			}
		}

        private void CmdShowLogsBySchedule(object sender, EventArgs e)
        {
            try
            {
                LogMiner dlg = new LogMiner(GPTree.SelectedItem as XmlNode, tasksDiagram.SelectedTask);
                dlg.ShowDialog();

            }
            catch (Exception err)
            {
                logger.Error(err + "");
            }


            RibbonMain.Focus();
            statusListBox.Focus();
            return;

        }

		private void statusList_OnStatusDialog(object sender, EventArgs e)
		{
			try
			{
				XmlNode row = ((ListBox)statusListBox).SelectedItem as XmlNode;

				LogMiner dlg = new LogMiner(row);
				dlg.ShowDialog();

				//ShowLogDialog dlg = new ShowLogDialog(cfg, row.Attributes["pid"].Value, row.Attributes["name"].Value);
				//dlg.Owner = this;
				//dlg.ShowDialog();
			}
			catch (Exception err)
			{
				logger.Error(err + "");
			}

			
			RibbonMain.Focus();
			statusListBox.Focus();
			return;

		}

        private void SelectedItemChanged(object sender, EventArgs e)
        {
            updateTasksList(GPTree.SelectedItem);
        }

		private void PreDate_Click(object sender, RoutedEventArgs e)
        {
            try
            {
				DateTime PrevDateTime = tasksDate.SelectedDateTime != null ? tasksDate.SelectedDateTime.Value : DateTime.Now;
                DateTime newValue = PrevDateTime.AddDays(-1);
                tasksDate.SelectedDateTime = new DateTime(newValue.Year, newValue.Month, newValue.Day, newValue.Hour, newValue.Minute, newValue.Second);
//                 updateTasksList(GPTree.SelectedItem);

				e.Handled = true;
			}
            catch (Exception err)
            {
                logger.Error(err + "");
            }
        }

        private void NextDate_Click(object sender, RoutedEventArgs e)
        {
            try
            {
				DateTime FwDateTime = tasksDate.SelectedDateTime != null ? tasksDate.SelectedDateTime.Value : DateTime.Now;
                DateTime newValue = FwDateTime.AddDays(1);
                tasksDate.SelectedDateTime = new DateTime(newValue.Year, newValue.Month, newValue.Day, newValue.Hour, newValue.Minute, newValue.Second);
				
//tasksDate.SelectedDateTime = FwDateTime.AddDays(1);
//                 updateTasksList(GPTree.SelectedItem);
				e.Handled = true;
            }
            catch (Exception err)
            {
                logger.Error(err + "");
            }
        }
        #endregion

        #region template 
        private void CmdNewTemplate(object sender, RoutedEventArgs e)
        {
            Scheduler.Template.Wizard.NewTemplateWizard wizard = new DigitalSignage.Scheduler.Template.Wizard.NewTemplateWizard();
            
            if (wizard.ShowDialog() == true)
            {
            }
			e.Handled = true;
        }
        private void CmdEditTemplate(object sender, RoutedEventArgs e)
        {

            System.Windows.Forms.OpenFileDialog fileDlg = new System.Windows.Forms.OpenFileDialog();
            fileDlg.Filter = "Screen|*.bin";
            String sExecutingPath = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            fileDlg.InitialDirectory = sExecutingPath + "\\TScreen\\";

            if (fileDlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                DirectoryInfo info = new DirectoryInfo(fileDlg.FileName);
                string path = fileDlg.FileName.Substring(0, fileDlg.FileName.IndexOf(info.Name));

                if (File.Exists(path + "\\media_files\\Template_File.swf"))
                {
                    Scheduler.Template.Wizard.NewTemplateWizard wizard = new Scheduler.Template.Wizard.NewTemplateWizard(info.FullName);
                    if (wizard.ShowDialog() == true)
                    {
                    }
                }            
                
            }

			e.Handled = true;
        }

        #endregion template

        #region Players routines
        private void CmdAddPlayer(object sender, RoutedEventArgs e)
        {
            try
            {
                NewPlayerDlg dlg = new NewPlayerDlg(cfg, false);
                dlg.Owner = this;
				dlg.GROUPID = GPTree.SelectedGroupGID;
                dlg.PlayerGroup = GPTree.SelectedGroup;
                dlg.ShowDialog();

				RibbonMain.Focus();

                if (dlg.result != true) return;

				using (new DigitalSignage.Common.WaitCursor())
				{
					ServerDataCache.GetInstance.RefreshNetworkTreeInfo();

                    if (GPTree.SelectedGroup != null)
                    {
                        updatePlayersList(GPTree.SelectedGroup.Attributes["gid"].Value);
                    }
					
					GPTree.UpdateTree();
				}

				e.Handled = true;
            }
            catch (Exception err)
            {
                logger.Error(err + " " + err.StackTrace);
				MessageBox.Show(Properties.Resources.mbCheckLog);
            }
        }

        private void CmdEditPlayer(object sender, RoutedEventArgs e)
        {
            try
            {
				#region 타겟 선정
				object target = null;
				try
				{
					if (this.StatusCMenu.Equals(((MenuItem)sender).Parent)) target = ((ListBox)statusListBox).SelectedItem as XmlNode;
					else target = GPTree.SelectedItem;
				}
				catch { target = GPTree.SelectedItem; }
				#endregion


				XmlNode row = target as XmlNode;
                if (row == null) return;

				string oldGid;
                NewPlayerDlg dlg = new NewPlayerDlg(cfg, true);
				dlg.PlayerID = row.Attributes["pid"].Value;
				dlg.PlayerName = row.Attributes["name"].Value;
				dlg.PlayerGroupGID = oldGid = row.Attributes["gid"].Value;
				dlg.GROUPID = row.Attributes["gid"].Value;

				try
				{
					dlg.AuthID = row.Attributes["_auth_id"].Value;
					dlg.AuthPass = row.Attributes["_auth_pass"].Value;

					int mntype = Convert.ToInt32(row.Attributes["_mnttype"].Value);
					dlg.IsAMTChecked = ((mntype & 1) == 1);
					dlg.IsWoLChecked = ((mntype & 2) == 2);

				}
				catch { }
				
                dlg.Owner = this;
                dlg.PlayerGroup = GPTree.SelectedGroup;
                dlg.PlayerComPort = "";
                dlg.PlayerComBitrate = 0;
                dlg.PlayerComBits = 0;
				dlg.ShowDialog();
				RibbonMain.Focus();

                if (dlg.result != true) return;
				
				if (!oldGid.Equals(dlg.PlayerGroupGID) &&
					MessageBoxResult.No == MessageBox.Show(Properties.Resources.mbWarnGroupMove, Properties.Resources.titleMessageBox, MessageBoxButton.YesNo, MessageBoxImage.Warning))
				{
					return ;
				}

				using (new DigitalSignage.Common.WaitCursor())
				{
					//try
					//{
					//    int nMntType = 0;
					//    if (dlg.IsAMTChecked) nMntType |= (int)PowerMntType.AMT;
					//    if (dlg.IsWoLChecked) nMntType |= (int)PowerMntType.WoL;

					//    cfg.ServerDataCacheList.UpdatePlayer(dlg.PlayerID, dlg.PlayerGroupGID, dlg.PlayerName, nMntType, dlg.AuthID, dlg.AuthPass);
					//}
					//catch 
					//{
					if (cfg.ServerDataCacheList.UpdatePlayer(dlg.PlayerID, dlg.PlayerGroupGID, dlg.PlayerName))
					{
						try
						{
							///	단말 수정 기록
							cfg.ServerLogList.InsertDetailLog(ServerDataCache.GetInstance.Connected_UserName, (int)(LogType.LogType_Managing | LogType.LogType_Proceed), "", "PLAYER_UPDATE", dlg.PlayerName, 0, 0, TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()));
						}
						catch { } 
					}
					else
					{
						try
						{
							///	단말 수정 기록
							cfg.ServerLogList.InsertDetailLog(ServerDataCache.GetInstance.Connected_UserName, (int)(LogType.LogType_Managing | LogType.LogType_Failed), "", "PLAYER_UPDATE", dlg.PlayerName, 0, 0, TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()));
						}
						catch { }
					}
					//}

					ServerDataCache.GetInstance.RefreshNetworkTreeInfo();

					GPTree.UpdateTree();
					e.Handled = true;
				}
            }
            catch (Exception err)
            {
                logger.Error(err + "");
				MessageBox.Show(Properties.Resources.mbCheckLog);
            }
        }

        private void CmdDeletePlayer(object sender, RoutedEventArgs e)
        {
            try
            {
				#region 타겟 선정
				object target = null;
				try
				{
					if (this.StatusCMenu.Equals(((MenuItem)sender).Parent)) target = ((ListBox)statusListBox).SelectedItem as XmlNode;
					else target = GPTree.SelectedItem;
				}
				catch { target = GPTree.SelectedItem; }
				#endregion


				XmlNode row = target as XmlNode;
                if (row == null) return;

				MessageBoxResult res = MessageBox.Show(String.Format(Properties.Resources.mbDeletePlayer, row.Attributes["name"].Value),
                    Properties.Resources.menuitemDeletePlayer, MessageBoxButton.YesNo);
                if (res == MessageBoxResult.Yes)
                {
					using (new DigitalSignage.Common.WaitCursor())
					{
						if (cfg.ServerDataCacheList.DeletePlayer(row.Attributes["pid"].Value))
						{
							try
							{
								///	단말 수정 기록
								cfg.ServerLogList.InsertDetailLog(ServerDataCache.GetInstance.Connected_UserName, (int)(LogType.LogType_Managing | LogType.LogType_Proceed), "", "PLAYER_DELETE", row.Attributes["name"].Value, 0, 0, TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()));
							}
							catch { }

							ServerDataCache.GetInstance.RefreshNetworkTreeInfo();

							GPTree.UpdateTree();
						}
						else
						{
							try
							{
								///	단말 수정 기록
								cfg.ServerLogList.InsertDetailLog(ServerDataCache.GetInstance.Connected_UserName, (int)(LogType.LogType_Managing | LogType.LogType_Failed), "", "PLAYER_DELETE", row.Attributes["name"].Value, 0, 0, TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()));
							}
							catch { } 
						}

					}
               }
				e.Handled = true;
            }
            catch (Exception err)
            {
                logger.Error(err + "");
            }
        }

        private void updatePlayersList(string gid)
        {
			using (new DigitalSignage.Common.WaitCursor())
			{
				try
				{
					int selected = statusListBox.SelectedIndex;

					//ServerDataCache.GetInstance.UpdatePlayersInGroup(gid);
					XmlNodeList nodelist = ServerDataCache.GetInstance.GetPlayerList(gid, false);

					status_players = nodelist;

					UpdatePlayerListBySorting(order_players);
					statusListBox.SelectedIndex = selected;
				}
				catch (Exception err)
				{
					logger.Error(err + "");
				}
			}
        }

        private void updatePlayersList2(string pid)
        {
			using (new DigitalSignage.Common.WaitCursor())
			{
				try
				{
					if (String.IsNullOrEmpty(pid) || pid == "-1") return;

					int selected = statusListBox.SelectedIndex;
					// lets get players list for current group

					//ServerDataCache.GetInstance.UpdatePlayer(pid);
					XmlNodeList nodelist = ServerDataCache.GetInstance.GetPlayerToReturnList(pid);

					status_players = nodelist;

					UpdatePlayerListBySorting(order_players);
					statusListBox.SelectedIndex = selected;
				}
				catch (Exception err)
				{
					logger.Error(err + "");
				}
			}
        }

        private void updateTasksList(Object root)
        {
			using (new DigitalSignage.Common.WaitCursor())
			{
				try
				{

					if (root == null)
					{
						System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
						return;
					}

					XmlNode row = root as XmlNode;

					//	상위 gid를 모두 뽑아온다.
					Collection<String> arrGids = new Collection<String>();
					XmlNode iter = row;
					while (iter != null)
					{
						if (iter.LocalName.Equals("group"))
						{
							arrGids.Add(iter.Attributes["gid"].Value);
						}
						iter = iter.ParentNode;
					}

					//	Server상의 모든 상위 gid를 뽑아온다.
					Collection<String> arrServerGids = new Collection<String>();
					iter = row;
					if (iter != null)
					{
						string sReturn = cfg.ServerGroupsList.GetGroupListByGID(iter.Attributes["gid"].Value);

						if (!String.IsNullOrEmpty(sReturn))
						{
							string[] arrReturns = sReturn.Split('|');

							foreach (String sGid in arrReturns)
							{
								arrServerGids.Add(sGid);
							}
						}
					}

					if (tasksDiagram == null)
					{
						System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
						return;
					}

					DateTime cs = tasksDate.SelectedDateTime != null ? tasksDate.SelectedDateTime.Value : DateTime.Now;
					cs = new DateTime(cs.Year, cs.Month, cs.Day, 0, 0, 0);

					long start = TimeConverter.ConvertToUTP(cs.ToUniversalTime());
					cs = new DateTime(cs.Year, cs.Month, cs.Day, 23, 59, 59);
					long end = TimeConverter.ConvertToUTP(cs.ToUniversalTime());

					// lets get players list for current group
					using (ServerDataSet.tasksDataTable tasks = cfg.ServerTasksList.GetTasksFrom(start, end))
					{

						ServerDataCache sdc = ServerDataCache.GetInstance;
						bool bIsPossibleViewParentGroup = sdc.HasPermission("VIEW PARENT SCHEDULE");

						// ok, now group them
						Dictionary<System.Guid, long[]> timings = new Dictionary<Guid, long[]>();
						ArrayList tasksList = new ArrayList();
						for (int i = 0; i < tasks.Rows.Count; i++)
						{
							Task task = new Task((ServerDataSet.tasksRow)tasks.Rows[i]);


							if (row != null && row.LocalName.Equals("group") && (!arrServerGids.Contains(task.gid) || (arrServerGids.Contains(task.gid) && !String.IsNullOrEmpty(task.pid))))
								continue;
							if (row != null && row.LocalName.Equals("player") && (!arrServerGids.Contains(task.gid) || (arrServerGids.Contains(task.gid) && !String.IsNullOrEmpty(task.pid) && task.pid != row.Attributes["pid"].Value)))
							{
								if (task.pid != row.Attributes["pid"].Value) continue;
							}

							//bool bIs_accessable = false;
							if (!arrGids.Contains(task.gid))
							{
								//////////////////////////////////////////////////////////////////////////
								// pid가 있는경우는 단말 스케줄인데 그룹 정보가 없는경우 이므로 표시를 하지 말자
								if (!String.IsNullOrEmpty(task.pid) && !task.pid.Equals("-1")) continue;

								//	만약 상위 그룹을 볼수 있는 권한이없다면 X
								if (!bIsPossibleViewParentGroup)
									continue;
								task.is_accessable = false;
							}
							else
							{
								task.is_accessable = true;
							}

							if (timings.ContainsKey(task.guuid))
							{
								long[] set = timings[task.guuid];
								if (task.TaskStart < set[0])
									set[0] = task.TaskStart;
								if (task.TaskEnd > set[1])
									set[1] = task.TaskEnd;
								timings.Remove(task.guuid);
								timings.Add(task.guuid, set);
							}
							else
							{
								long[] set = new long[2];
								set[0] = task.TaskStart;
								set[1] = task.TaskEnd;
								timings.Add(task.guuid, set);
								tasksList.Add(task);
							}
						}
						for (int i = 0; i < tasksList.Count; i++)
						{
							Task task = (Task)tasksList[i];
							long[] set = timings[task.guuid];
							task.TaskStart = set[0];
							task.TaskEnd = set[1];
						}

						// and show
                        
                        if(null == (tasksDiagram.DownloadSummaryTable = GetDownloadSummary(root, tasksList)))
                        {
                            tasksDiagram.DownloadInfoTable = GetDownloadInformation(root, tasksList);
                        }

						tasksDiagram.SelectedTreeObject = root;
						tasksDiagram.Tasks = tasksList;
						tasksDiagram.Day = cs;
						tasksDiagram.Rebuild();
						tasks.Dispose();
					}
				}
				catch (Exception err)
				{
					logger.Error(err + "");
				}
			}

        }
        private ServerDataSet.DownloadSummaryDataTable GetDownloadSummary(Object seleted, ArrayList tasks)
        {
            using (new DigitalSignage.Common.WaitCursor())
            {
                Collection<string> pids = new Collection<string>();
                Collection<string> uuids = new Collection<string>();
                try
                {
                    #region 단말 ID
                    XmlNode row = seleted as XmlNode;

                    if (row.LocalName.Equals("player"))
                    {
                        pids.Add(row.Attributes["pid"].Value);
                    }
                    else
                    {
                        XmlNodeList nodelist = ServerDataCache.GetInstance.GetPlayerList(row.Attributes["gid"].Value, false);
                        if (nodelist != null)
                        {
                            foreach (XmlNode player in nodelist)
                            {
                                pids.Add(player.Attributes["pid"].Value);
                            }
                        }
                    }
                    #endregion

                    foreach (Task task in tasks)
                    {
                        uuids.Add(task.uuid.ToString());
                    }

                    if (uuids.Count == 0 || pids.Count == 0) return null;

                    return cfg.ServerDownloadList.GetDownloadCountFromUuid(uuids.ToArray(), pids.ToArray());

                }
                catch (Exception e)
                {
                    logger.Error(e.ToString());
                }
            }
            return null;
        }

		private ServerDataSet.downloadmasterDataTable GetDownloadInformation(Object seleted, ArrayList tasks)
		{
			using (new DigitalSignage.Common.WaitCursor())
			{
				Collection<string> pids = new Collection<string>();
				Collection<string> uuids = new Collection<string>();
				try
				{
					#region 단말 ID
					XmlNode row = seleted as XmlNode;

					if (row.LocalName.Equals("player"))
					{
						pids.Add(row.Attributes["pid"].Value);
					}
					else
					{
						XmlNodeList nodelist = ServerDataCache.GetInstance.GetPlayerList(row.Attributes["gid"].Value, false);
						if (nodelist != null)
						{
							foreach (XmlNode player in nodelist)
							{
								pids.Add(player.Attributes["pid"].Value);
							}
						}
					}
					#endregion

					foreach (Task task in tasks)
					{
						uuids.Add(task.uuid.ToString());
					}
					
					if (uuids.Count == 0 || pids.Count == 0) return null;

					return cfg.ServerDownloadList.GetDownloadInformation(uuids.ToArray(), pids.ToArray());

				}
				catch (Exception e)
				{
					logger.Error(e.ToString());
				}
			}
			return null;
		}
		
        private void statusListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
        }
        #endregion

        #region Tasks routines
		private void CmdAddSubtitle(object sender, RoutedEventArgs e)
		{
			tSubtilesUI_Click(sender, e);
		}

		private void CmdTaskWizard(object sender, RoutedEventArgs e)
		{
			try
			{
				#region 타겟 선정
				object target = null;
				try
				{
					if (this.StatusCMenu.Equals(((MenuItem)sender).Parent)) target = ((ListBox)statusListBox).SelectedItem as XmlNode;
					else target = GPTree.SelectedItem;
				}
				catch { target = GPTree.SelectedItem; }
				#endregion

				TaskWizard dlg = new TaskWizard(cfg);

				dlg.TargetObject = target;

				dlg.ApplyDate = this.tasksDate.SelectedDateTime.Value;

				if (true == dlg.ShowDialog())
				{
					updateTasksList(GPTree.SelectedItem);
				}
				e.Handled = true;

				RibbonMain.Focus();

			}
			catch(Exception err)
			{
				logger.Error(err + "");
			}
		}


		private void CmdRemoteView(object sender, RoutedEventArgs e)
		{
			try
			{
				XmlNode node = GPTree.SelectedItem as XmlNode;
				if (node != null)
				{
					VNCLauncher.RunVncViewer(node.Attributes["_ipv4addr"].Value,
						"5900", node.Attributes["_auth_pass"].Value, true, true, false, true, 50);
				}
				e.Handled = true;
			}
			catch { }

		}

		private void CmdAMTDetails(object sender, RoutedEventArgs e)
		{

			try
			{
				XmlNode node = GPTree.SelectedItem as XmlNode;
				if (node != null)
				{
					AMTDetailsDlg amtDetailes = new AMTDetailsDlg(node);
					amtDetailes.ShowDialog();
				}

				e.Handled = true;
			}
			catch { }
		}

        private void CmdVNCViewer(object sender, RoutedEventArgs e)
        {

            try
            {
                XmlNode node = GPTree.SelectedItem as XmlNode;
                if (node != null)
                {
                    string pwd = node.Attributes["_auth_pass"].Value;
                    if (String.IsNullOrEmpty(pwd)) pwd = "Test12#$";

                    bool bRedirectProxy = false;
                    if (ServerDataCache.GetInstance.IsRunningProxyServer)
                    {
                        MessageBoxResult re = MessageBox.Show(Properties.Resources.mbProxy, Properties.Resources.titleMessageBox, MessageBoxButton.YesNoCancel, MessageBoxImage.Question);

                        if (re == MessageBoxResult.Cancel)
                            return;
                        else if (re == MessageBoxResult.Yes) bRedirectProxy = true;
                        else bRedirectProxy = false;
                    }

                    if (bRedirectProxy)
                    {
                        Random rand = new Random(DateTime.Now.Millisecond);
                        string sRandCode = rand.Next(1, 99999).ToString();

                        String hostip = Config.GetConfig.ServerPath.Replace("tcp://", "").Replace(":888", "").TrimEnd('/');
                        //	프록시 서버를 거침


                        if (null != VNCLauncher.RunVncViewer(sRandCode, hostip, "5901", "", true, true, false, true, 50))
                        {
                            Config.GetConfig.ServerPlayersList.RunRemoteControl(sRandCode, node.Attributes["pid"].Value);
                        }
                        else
                        {
                            MessageBox.Show(String.Format(Properties.Resources.labelFailedCommand, Properties.Resources.menuitemRemoteView));
                        }
                    }
                    else
                    {
                        //	프록시 서버를 거치지 않음
                        if (null != VNCLauncher.RunVncViewer(node.Attributes["_ipv4addr"].Value,
                            "5900", pwd, true, true, false, true, 50))
                        {
                        }
                        else
                        {
                            MessageBox.Show(String.Format(Properties.Resources.labelFailedCommand, Properties.Resources.menuitemRemoteView));
                        }
                    }
                }

                e.Handled = true;
            }
            catch { }
        }


		/// <summary>
		/// 반복 스케줄 추가
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void CmdAddDefault(object sender, RoutedEventArgs e)
		{
			try
			{
				#region 타겟 선정
				object target = null;
				try
				{
					if (this.StatusCMenu.Equals(((MenuItem)sender).Parent)) target = ((ListBox)statusListBox).SelectedItem as XmlNode;
					else target = GPTree.SelectedItem;
				}
				catch { target = GPTree.SelectedItem; }
				#endregion

				NewCmdTaskDlg dlg = new NewCmdTaskDlg(cfg, 2);
				dlg.Owner = this;
				dlg.TargetObject = target;

				dlg.ApplyDate = this.tasksDate.SelectedDateTime.Value;

				if (true == dlg.ShowDialog())
				{
					updateTasksList(GPTree.SelectedItem);
				}

				e.Handled = true;
				RibbonMain.Focus();

			}
			catch (Exception err)
			{
				logger.Error(err + "" + err.StackTrace);
				MessageBox.Show(Properties.Resources.mbCheckLog);
			}
		}

		/// <summary>
		/// 시간 스케줄 추가
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void CmdAddTask(object sender, RoutedEventArgs e)
		{
			try
			{
				#region 타겟 선정
				object target = null;
				try
				{
					if (this.StatusCMenu.Equals(((MenuItem)sender).Parent)) target = ((ListBox)statusListBox).SelectedItem as XmlNode;
					else target = GPTree.SelectedItem;
				}
				catch { target = GPTree.SelectedItem; }
				#endregion

				NewCmdTaskDlg dlg = new NewCmdTaskDlg(cfg, 0);
				//NewTaskDlg dlg = new NewTaskDlg(cfg, null);
				dlg.Owner = this;

				dlg.TargetObject = target;

				dlg.ApplyDate = this.tasksDate.SelectedDateTime.Value;
				if (true == dlg.ShowDialog())
				{
					updateTasksList(GPTree.SelectedItem);
				}

				e.Handled = true;

				RibbonMain.Focus();

			}
			catch (Exception err)
			{
				logger.Error(err + "" + err.StackTrace);
				MessageBox.Show(Properties.Resources.mbCheckLog);
			}
		}

		/// <summary>
		///  제어 스케줄 추가
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void CmdAddControl(object sender, RoutedEventArgs e)
		{
			try
			{
				#region 타겟 선정
				object target = null;
				try
				{
					if(this.StatusCMenu.Equals(((MenuItem)sender).Parent)) target = ((ListBox)statusListBox).SelectedItem as XmlNode;
					else target = GPTree.SelectedItem;
				}
				catch { target = GPTree.SelectedItem; }
				#endregion

				NewCmdTaskDlg dlg = new NewCmdTaskDlg(cfg, 3);
				dlg.Owner = this;
				dlg.TargetObject = target;

				dlg.ApplyDate = this.tasksDate.SelectedDateTime.Value;

				if (true == dlg.ShowDialog())
				{
					updateTasksList(GPTree.SelectedItem);
				}

				e.Handled = true;

				RibbonMain.Focus();


			}
			catch (Exception err)
			{
				logger.Error(err + "" + err.StackTrace);
				MessageBox.Show(Properties.Resources.mbCheckLog);
			}
		}

		/// <summary>
		/// 스케줄 가져오기
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
        private void CmdExportTask(object sender, RoutedEventArgs e)
        {
            try
            {
                Task task = tasksDiagram.SelectedTask;

				if (task == null || !task.is_accessable)
				{
					MessageBox.Show(Properties.Resources.mbErrorPermission,
						Properties.Resources.menuitemEditTask, MessageBoxButton.OK, MessageBoxImage.Error);
                    e.Handled = true;
					return;
				}
                if (task.type == TaskCommands.CmdDefaultProgram || task.type == TaskCommands.CmdTouchDefaultProgram ||
					task.type == TaskCommands.CmdProgram ||
					task.type == TaskCommands.CmdSubtitles)
				{
					XmlNode node = GPTree.SelectedItem as XmlNode;
					if (node != null)
					{
						XmlNode groupnode = node.LocalName.Equals("group") ? node : ServerDataCache.GetInstance.GetGroupByGID(node.Attributes["gid"].Value);
						
						System.Windows.Forms.FolderBrowserDialog dlg = new System.Windows.Forms.FolderBrowserDialog();
                        //FolderBrowser.ExtendedFolderBrowser dlg = new FolderBrowser.ExtendedFolderBrowser();
                        
						if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
						{
							String sExecutingPath = dlg.SelectedPath + "\\" + task.duuid.ToString() + "\\";
							TaskDownloader downloader = new TaskDownloader(cfg, groupnode, task.duuid, sExecutingPath);
							if(true == downloader.ShowDialog())
							{
// 								try
// 								{
// 									foreach (string file in System.IO.Directory.GetFiles(sExecutingPath))
// 									{
// 										if(file.Contains("program"))
// 										{
// 											string newname = sExecutingPath + System.IO.Path.GetFileNameWithoutExtension(file) + ".ipr";
// 											System.IO.File.Move(file, newname);
// 										}
// 										else if(file.Contains("playlist"))
// 										{
// 											string newname = sExecutingPath + System.IO.Path.GetFileNameWithoutExtension(file) + ".ipl";
// 											System.IO.File.Move(file, newname);
// 										}
// 									}
// 								}
// 								catch
// 								{
// 								}

							}


						}
					}

				}
				e.Handled = true;

            }
            catch (Exception err)
            {
                logger.Error(err + " " + err.StackTrace);
                //MessageBox.Show("An error happened during last operation. Check the logs for a description.");
            }
        }

		private void CmdPreviewTask(object sender, RoutedEventArgs e)
		{
			try
			{
				Task task = tasksDiagram.SelectedTask;

				if (task == null)
				{
					MessageBox.Show(Properties.Resources.mbErrorPermission,
						Properties.Resources.menuitemEditTask, MessageBoxButton.OK, MessageBoxImage.Error);

					e.Handled = true;

					return;
				}

                if (task.type == TaskCommands.CmdDefaultProgram || task.type == TaskCommands.CmdTouchDefaultProgram ||
					task.type == TaskCommands.CmdProgram)
				{
					String sExecutingPath = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "\\SchedulerData\\temp\\" + task.duuid.ToString() + "\\";

					XmlNode node = GPTree.SelectedItem as XmlNode;
					if(node != null)
					{
						XmlNode groupnode = node.LocalName.Equals("group") ? node : ServerDataCache.GetInstance.GetGroupByGID(node.Attributes["gid"].Value);
						TaskDownloader downloader = new TaskDownloader(cfg, groupnode, task.duuid, sExecutingPath);
						if(true == downloader.ShowDialog())
						{
							ProgramPlayerPreview dlg = new ProgramPlayerPreview();
							dlg.Title = Properties.Resources.titlePreview;
							dlg.Owner = this;
							try
							{
								int nWidth, nHeight;
								ExtractScreenSize(sExecutingPath, out nWidth, out nHeight);
								dlg.Width = nWidth;
								dlg.Height = nHeight;
							}
							catch 
							{
								dlg.Width = 400;
								dlg.Height = 300;								
							}
							dlg.ResizeMode = ResizeMode.NoResize;
							dlg.Show();
							dlg.Play(sExecutingPath + "program.xml");
						}
					}
				}
				e.Handled = true;

			}
			catch (Exception err)
			{
				logger.Error(err + " " + err.StackTrace);
				//MessageBox.Show("An error happened during last operation. Check the logs for a description.");
			}
		}

		private void ExtractScreenSize(String sExecutingPath, out int nWidth, out int nHeight)
		{
			String sScreenPath = sExecutingPath + "\\1";
			int width = 400, height = 300;

			#region 스크린의 사이즈 얻어오는 루틴
			foreach(String sFile in System.IO.Directory.GetFiles(sScreenPath))
			{
				System.IO.FileInfo finfo = new System.IO.FileInfo(sFile);

				if(finfo.Extension.ToLower().Equals(".bin"))
				{
					try
					{
						FileStream fileStream = new FileStream(finfo.FullName, FileMode.Open, FileAccess.Read);
						BinaryFormatter binaryFormatter = new BinaryFormatter();
						List<Dictionary<string, object>> propertiesSet = new List<Dictionary<string, object>>();

						try
						{
							propertiesSet = binaryFormatter.Deserialize(fileStream) as List<Dictionary<string, object>>;
							width = Convert.ToInt32(propertiesSet[0]["Width"]);
							height = Convert.ToInt32(propertiesSet[0]["Height"]);
						}
						catch
						{
						}
						fileStream.Close();

					}
					catch
					{

					}
					break;
				}
			}
			#endregion

			#region 페이지 크기와 비교하여 미리보기 사이즈 조절
			int nScreenWidth = System.Windows.Forms.SystemInformation.PrimaryMonitorSize.Width;
			int nScreenHeight = System.Windows.Forms.SystemInformation.PrimaryMonitorSize.Height;

			if (nScreenWidth > width && nScreenHeight > height)
			{
				nWidth = width;
				nHeight = height;
			}
			else
			{
				int nMaxOne = Math.Max(nScreenWidth, nScreenHeight);
				if (width > height)
				{
					nWidth = (nMaxOne / 2);
					nHeight = (int)(nWidth / ((double)width / (double)height));
				}
				else
				{
					nHeight = (nMaxOne / 2);
					nWidth = (int)(nHeight / ((double)height / (double)width));
				}
			}
			#endregion
		}

		void tasksDiagram_OnItemDoubleClicked(object sender, Task selectedTask)
		{
			try
			{
				if (selectedTask == null || !selectedTask.is_accessable)
				{
					MessageBox.Show(Properties.Resources.mbErrorPermission,
						Properties.Resources.menuitemEditTask, MessageBoxButton.OK, MessageBoxImage.Error);

					return;
				}
				if (selectedTask.type == TaskCommands.CmdControlSchedule)
				{
					MessageBox.Show(Properties.Resources.mbErrorEditControlTask);
					return;
				}


				/*
				NewTaskDlg dlg = new NewTaskDlg(cfg, task.guuid.ToString());
				dlg.Owner = this;
				*/

				NewCmdTaskDlg dlg = new NewCmdTaskDlg(cfg, 0);
				dlg.WillEditTask = selectedTask;
				dlg.Owner = this;

				if(true == dlg.ShowDialog())
				{
					updateTasksList(GPTree.SelectedItem);
				}
				RibbonMain.Focus();

			}
			catch (Exception err)
			{
				logger.Error(err + " " + err.StackTrace);
				//MessageBox.Show("An error happened during last operation. Check the logs for a description.");
			}
		}

		private void CmdEditTask(object sender, RoutedEventArgs e)
		{
			try
			{
				Task task = tasksDiagram.SelectedTask;

				if (task == null || !task.is_accessable)
				{
					MessageBox.Show(Properties.Resources.mbErrorPermission,
						Properties.Resources.menuitemEditTask, MessageBoxButton.OK, MessageBoxImage.Error);

					return;
				}
				if (task.type == TaskCommands.CmdControlSchedule)
				{
					MessageBox.Show(Properties.Resources.mbErrorEditControlTask);
					return;
				}


				/*
				NewTaskDlg dlg = new NewTaskDlg(cfg, task.guuid.ToString());
				dlg.Owner = this;
				*/

				NewCmdTaskDlg dlg = new NewCmdTaskDlg(cfg, 0);
				dlg.WillEditTask = task;
				dlg.Owner = this;

				if(true == dlg.ShowDialog())
				{
					updateTasksList(GPTree.SelectedItem);
				}
				e.Handled = true;

				RibbonMain.Focus();

			}
			catch (Exception err)
			{
				logger.Error(err + " " + err.StackTrace);
				//MessageBox.Show("An error happened during last operation. Check the logs for a description.");
			}
		}

		private bool DeleteContentFromStorage(Guid duuid, string gid)
		{
			try
			{

				XmlNode node = ServerDataCache.GetInstance.GetGroupByGID(gid);
				if (node == null)
					return false;

				if (cfg.ServerTasksList.CanDeleteByDuuid(duuid.ToString(), node.Attributes["source"].Value, node.Attributes["path"].Value))
				{

					FTPFunctions ftp = new FTPFunctions(cfg.AnalyzeSource(node.Attributes["source"].Value), node.Attributes["path"].Value, node.Attributes["username"].Value, node.Attributes["password"].Value, cfg.FTP_Timeout);
					ftp.FTPRecursiveDelete(duuid.ToString());
					
					return true;
				}
				else
				{
					//	같은 duuid의 Task가 존재하므로 FTP에 삭제하지 않는다.
					return true;
				}

			}
			catch {}

			//	실패한 경우 버전이 다른경우이므로 호환성을 위해 성공으로 처리한다.
			return true;
		}

        private void CmdDeleteTask(object sender, RoutedEventArgs e)
        {
            try
            {
				if (tasksDiagram.SelectedTask == null)
					return;

				if (!tasksDiagram.SelectedTask.is_accessable)
				{
					MessageBox.Show(Properties.Resources.mbErrorPermission, 
						Properties.Resources.menuitemDeleteTask, MessageBoxButton.OK, MessageBoxImage.Error);

					e.Handled = true;
					return;
				}

                MessageBoxResult res = (tasksDiagram.SelectedTask.pid == null ? MessageBox.Show(Properties.Resources.mbDeleteGroupTask,
                    Properties.Resources.menuitemDeleteTask, MessageBoxButton.YesNo) : MessageBox.Show(Properties.Resources.mbDeleteTask,
                    Properties.Resources.menuitemDeleteTask, MessageBoxButton.YesNo));              

				if (res == MessageBoxResult.Yes)
				{
                    if (tasksDiagram.SelectedTasks.Count == 1)
                    {
                        Task task = tasksDiagram.SelectedTask;
                        if (task == null) return;

                        try
                        {
                            int nLinkedCount = cfg.ServerTasksList.GetTasksGroupCount(task.guuid.ToString());

                            if (nLinkedCount > 1)
                            {
                                if (MessageBoxResult.No == MessageBox.Show(String.Format(Properties.Resources.mbDeleteLinkedTask, nLinkedCount),
                                    Properties.Resources.menuitemDeleteTask, MessageBoxButton.YesNo, MessageBoxImage.Warning))
                                    return;
                            }
                        }
                        catch
                        {
                        }

                        if (!DeleteContentFromStorage(task.duuid, task.gid))
                        {
                            MessageBox.Show(Properties.Resources.mbErrorFailedToDeleteFromFTP,
                                Properties.Resources.menuitemDeleteTask, MessageBoxButton.OK, MessageBoxImage.Error);
                        }
                        else
                        {
							if (-1 != cfg.ServerTasksList.DeleteTask(task))
							{
								try
								{
									///	스케줄 삭제 기록
									cfg.ServerLogList.InsertDetailLog(ServerDataCache.GetInstance.Connected_UserName, (int)(LogType.LogType_Managing | LogType.LogType_Proceed), task.uuid.ToString(), "SCHEDULE_DELETE", task.description, 0, 0, TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()));
								}
								catch { }
							}
							else
							{
								try
								{
									///	스케줄 삭제 기록
									cfg.ServerLogList.InsertDetailLog(ServerDataCache.GetInstance.Connected_UserName, (int)(LogType.LogType_Managing | LogType.LogType_Failed), task.uuid.ToString(), "SCHEDULE_DELETE", task.description, 0, 0, TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()));
								}
								catch { }
							}
                            updateTasksList(GPTree.SelectedItem);
                        }
                    }
                    else if (tasksDiagram.SelectedTasks.Count > 1)//하나 이상 선택 삭제
                    {
                        List<Task> deletedTask = new List<Task>();
                        DeleteContentFromStorageDelegate DeleteContentFromStorageEventHandler = new DeleteContentFromStorageDelegate(DeleteSchedule);
                        DeleteContentFromStorageEventHandler.BeginInvoke(tasksDiagram.SelectedTasks, new AsyncCallback(CompletedDeleteContentFromStorageCallback), DeleteContentFromStorageEventHandler);
                        waitSyncHandler.WaitOne();
                        updateTasksList(GPTree.SelectedItem);
                    }
					e.Handled = true;
				}
            }
            catch (Exception err)
            {
                logger.Error(err + "");
            }
        }

        /// <summary>
        /// 조준영 : 하나 이상 선택 삭제
        /// </summary>
        /// <param name="taskList"></param>
        private void DeleteSchedule(List<Task> taskList)
        {
            try
            {
                foreach (Task delTask in taskList)
                {
                    if (delTask == null) continue;
                    if (!DeleteContentFromStorage(delTask.duuid, delTask.gid))
                    {
                        MessageBox.Show(Properties.Resources.mbErrorFailedToDeleteFromFTP,
                            Properties.Resources.menuitemDeleteTask, MessageBoxButton.OK, MessageBoxImage.Error);
                        return;
                    }
                    else
                    {
						if (-1 != cfg.ServerTasksList.DeleteTask(delTask))
						{
							try
							{
								///	스케줄 삭제 기록
								cfg.ServerLogList.InsertDetailLog(ServerDataCache.GetInstance.Connected_UserName, (int)(LogType.LogType_Managing | LogType.LogType_Proceed), delTask.uuid.ToString(), "SCHEDULE_DELETE", delTask.description, 0, 0, TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()));
							}
							catch { }
						}
						else
						{
							try
							{
								///	스케줄 삭제 기록
								cfg.ServerLogList.InsertDetailLog(ServerDataCache.GetInstance.Connected_UserName, (int)(LogType.LogType_Managing | LogType.LogType_Failed), delTask.uuid.ToString(), "SCHEDULE_DELETE", delTask.description, 0, 0, TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()));
							}
							catch { }
						}
						//updateTasksList(GPTree.SelectedItem);
					}
                }
            }
            catch { }
            finally
            {
                waitSyncHandler.Set();
            }
        }

        /// <summary>
        /// cjy_add : 묶음삭제 기능
        /// </summary>
        /// <param name="ar"></param>
        private static void CompletedDeleteContentFromStorageCallback(IAsyncResult ar)
        {
            try
            {
                DeleteContentFromStorageDelegate s1 = (DeleteContentFromStorageDelegate)ar.AsyncState;
                s1.EndInvoke(ar);

            }
            catch
            {
            }
        }

        private void taskSelectionChanged(object sender, Task task)
        {
            try
            {
                cleanDynMenu();

				if (task == null) return;
				
				ServerDataCache sdc = ServerDataCache.GetInstance;

                tEditTask.IsEnabled = sdc.HasPermission("UPDATE SCHEDULE");
                tDeleteTask.IsEnabled = sdc.HasPermission("DELETE SCHEDULE");               
            }
            catch (Exception err)
            {
                logger.Error(err + "");
            }
            return;
        }

        #endregion

        #region User routines
        private void CmdAddUser(object sender, ExecutedRoutedEventArgs e)
        {
            try
            {
                EditUsers dlg = new EditUsers(cfg, null, -1, false);
                dlg.Owner = this;
                dlg.ShowDialog();
				e.Handled = true;

				RibbonMain.Focus();

            }
            catch (Exception err)
            {
                logger.Error(err + "");
                MessageBox.Show(Properties.Resources.mbCheckLog);
            }
        }

        private void CmdEditUser(object sender, ExecutedRoutedEventArgs e)
        {
            try
            {
            }
            catch (Exception err)
            {
                logger.Error(err + "");
				//MessageBox.Show(Properties.Resources.mbCheckLog);
            }
        }

        private void CmdDeleteUser(object sender, ExecutedRoutedEventArgs e)
        {
            try
            {
            }
            catch (Exception err)
            {
                logger.Error(err + "");
            }
        }
        #endregion

        private void MenuItem_Click_5(object sender, RoutedEventArgs e)
        {
            try
            {
				#region 타겟 선정
				object target = null;
				try
				{
					if (this.StatusCMenu.Equals(((MenuItem)sender).Parent)) target = ((ListBox)statusListBox).SelectedItem as XmlNode;
					else target = GPTree.SelectedItem;
				}
				catch { target = GPTree.SelectedItem; }
				#endregion

                NewCmdTaskDlg dlg = new NewCmdTaskDlg(cfg, 2);
                dlg.Owner = this;
				
				dlg.TargetObject = target;
				
				dlg.ApplyDate = this.tasksDate.SelectedDateTime.Value;
				if (true == dlg.ShowDialog())
				{
					updateTasksList(GPTree.SelectedItem);
				}
				e.Handled = true;
				RibbonMain.Focus();
            }
            catch (Exception err)
            {
                logger.Error(err + " " + err.StackTrace);
				MessageBox.Show(Properties.Resources.mbCheckLog);
            }
        }

        private void CmdPlayListEditor(object sender, ExecutedRoutedEventArgs e)
        {
            // open PlayList editor
            try
            {
                PlayListEditor dlg = new PlayListEditor(cfg);
                dlg.Owner = this;
                dlg.ShowDialog();
				e.Handled = true;
				RibbonMain.Focus();

            }
            catch (Exception err)
            {
                logger.Error(err + " " + err.StackTrace);
				MessageBox.Show(Properties.Resources.mbCheckLog);
            }
        }

		private void CmdProgramEditor(object sender, ExecutedRoutedEventArgs e)
        {
            // open Program editor
            try
            {
                ProgramEditor dlg = new ProgramEditor(cfg);
                dlg.Owner = this;
                dlg.ShowDialog();
				e.Handled = true;

				RibbonMain.Focus();

            }
            catch (Exception err)
            {
                logger.Error(err + " " + err.StackTrace);
				MessageBox.Show(Properties.Resources.mbCheckLog);
            }
        }

        private void tSubtilesUI_Click(object sender, RoutedEventArgs e)
        {
            // open Program editor
            try
            {
				#region 타겟 선정
				object target = null;
				try
				{
					if (this.StatusCMenu.Equals(((MenuItem)sender).Parent)) target = ((ListBox)statusListBox).SelectedItem as XmlNode;
					else target = GPTree.SelectedItem;
				}
				catch { target = GPTree.SelectedItem; }
				#endregion

                NewCmdTaskDlg dlg = new NewCmdTaskDlg(cfg, 1);
                dlg.Owner = this;

				dlg.TargetObject = target;

				dlg.ApplyDate = this.tasksDate.SelectedDateTime.Value;
				if (true == dlg.ShowDialog())
				{
					updateTasksList(GPTree.SelectedItem);
				}
				e.Handled = true;

				RibbonMain.Focus();

			}
            catch (Exception err)
            {
                logger.Error(err + " " + err.StackTrace);
				MessageBox.Show(Properties.Resources.mbCheckLog);
            }
        }

        private void Window_PreviewKeyDown(object sender, KeyEventArgs e)
        {

			ServerDataCache sdc = ServerDataCache.GetInstance;
			bool perm_CreateTask = sdc.HasPermission("CREATE SCHEDULE");

			if (e.Key == Key.Delete)
			{
				if (sdc.HasPermission("DELETE SCHEDULE"))
				{
					CmdDeleteTask(null, null);
				}
				e.Handled = true;
			}
			else if (e.Key == Key.C && e.KeyboardDevice.Modifiers == ModifierKeys.Control && perm_CreateTask)
			{
				try
				{

					//	텍스트 복사
					if (tasksDiagram.SelectedTask != null)
					{
						Task item = tasksDiagram.SelectedTask as Task;
						Clipboard.SetText(((String.IsNullOrEmpty(item.pid) || item.pid == "-1") ? Properties.Resources.labelGroupUser : Properties.Resources.labelPlayerUser) + "\t" + item.description + "\t" + TimeSpan.FromSeconds(item.duration));
					}
				}
				catch
				{
				}
				mi_CopyClick(null, null);
				//e.Handled = true;
			}
			else if (e.Key == Key.V && e.KeyboardDevice.Modifiers == ModifierKeys.Control && perm_CreateTask)
			{
				mi_PasteClick(null, null);
				e.Handled = true;
			}
		}

		private void Rollback_Click(object sender, RoutedEventArgs e)
		{
			if (MessageBoxResult.Yes == MessageBox.Show(Properties.Resources.mbRollback, Properties.Resources.titleMessageBox, MessageBoxButton.YesNo))
			{
				updateTasksList(GPTree.SelectedItem);
				tasksDiagram.IsModified = false;

                // 메뉴버튼 재활성화
                ChangeMenuBtnState(GPTree.SelectedItem);
			}
		}

		private bool CheckOverlapTime(TaskItem item)
		{
			bool bRet = false;
			try
			{
				Task origin = item.DataContext as Task;

				if (origin.IsVolumeControlTask) return true;

				foreach (TaskItem taskitem in tasksDiagram.TaskItems)
				{
					if (taskitem == null) continue;
					else if (item.Equals(taskitem)) continue;
					
					Task task = taskitem.DataContext as Task;

                    if (task.gid == origin.gid && task.pid == origin.pid && task.type == origin.type && task.type != TaskCommands.CmdDefaultProgram && task.type != TaskCommands.CmdTouchDefaultProgram)
					{
						//	원본이 반복 패턴이 있는지 검사
						if (origin.starttimeofday != origin.endtimeofday)
						{
							//	대상이 반복 패턴이 있는지 검사
							if (task.starttimeofday != task.endtimeofday)
							{
								//	요일 중복 검색
								if ((origin.daysofweek & origin.daysofweek) > 0)
								{
									// 요일이 중복된다면 시간대 검색
									if ((origin.starttimeofday == task.starttimeofday && origin.endtimeofday == task.starttimeofday) || 
										(origin.starttimeofday <= task.starttimeofday && origin.endtimeofday > task.starttimeofday) ||
										(origin.starttimeofday < task.endtimeofday && origin.endtimeofday >= task.endtimeofday) ||
										(task.starttimeofday <= origin.starttimeofday && task.endtimeofday > origin.starttimeofday) ||
										(task.starttimeofday < origin.endtimeofday && task.endtimeofday >= origin.endtimeofday))
									{
										//	시간대가 중복된다면
										return bRet = false;
									}

									else bRet = true;
								}
								else return bRet = false;
							}
							else
							{
								//	대상이 반복 패턴이 없는 경우
								DateTime dtStart = TimeConverter.ConvertFromUTP(task.TaskStart).ToLocalTime();
								DateTime dtEnd = TimeConverter.ConvertFromUTP(task.TaskEnd).ToLocalTime();
								//aaa더해야해
								DateTime dtSrcStart = TimeConverter.ConvertFromUTP(origin.TaskStart).ToLocalTime();
								DateTime dtSrcEnd = TimeConverter.ConvertFromUTP(origin.TaskEnd).ToLocalTime();

								dtSrcStart = new DateTime(dtSrcStart.Year, dtSrcStart.Month, dtSrcStart.Day) + TimeSpan.FromSeconds(origin.starttimeofday);
								dtSrcEnd = new DateTime(dtSrcEnd.Year, dtSrcEnd.Month, dtSrcEnd.Day) + TimeSpan.FromSeconds(origin.endtimeofday);

								DateTime start = dtSrcStart;
								DateTime end = new DateTime(dtSrcStart.Year, dtSrcStart.Month, dtSrcStart.Day) + TimeSpan.FromSeconds(origin.endtimeofday);

								while (start <= dtSrcEnd)
								{
									if ((origin.daysofweek & (int)start.DayOfWeek) == 0)
									{
										start += TimeSpan.FromDays(1);
										end += TimeSpan.FromDays(1);

										continue;
									}

									if ((start <= dtStart && dtStart < end) || (start < dtEnd && dtEnd <= end) ||
										(dtStart <= start && start < dtEnd) || (dtStart < end && end <= dtEnd))
									{
										return bRet = false;
									}
									start += TimeSpan.FromDays(1);
									end += TimeSpan.FromDays(1);
								}
							}

						}
						else
						{
							//	반복 패턴이 없다면
							//	대상이 반복 패턴이 있는지 검사
							if (task.starttimeofday != task.endtimeofday)
							{
								//	원본이 반복 패턴이 없고 대상이 반복패턴이 있는경우
								DateTime dtStart = TimeConverter.ConvertFromUTP(origin.TaskStart).ToLocalTime();
								DateTime dtEnd = TimeConverter.ConvertFromUTP(origin.TaskEnd).ToLocalTime();
								//aaa더해야해
								DateTime dtDescStart = TimeConverter.ConvertFromUTP(task.TaskStart).ToLocalTime();
								DateTime dtDescEnd = TimeConverter.ConvertFromUTP(task.TaskEnd).ToLocalTime();

								dtDescStart = new DateTime(dtDescStart.Year, dtDescStart.Month, dtDescStart.Day) + TimeSpan.FromSeconds(task.starttimeofday);
								dtDescEnd = new DateTime(dtDescEnd.Year, dtDescEnd.Month, dtDescEnd.Day) + TimeSpan.FromSeconds(task.endtimeofday);

								DateTime start = dtDescStart;
								DateTime end = new DateTime(dtDescStart.Year, dtDescStart.Month, dtDescStart.Day) + TimeSpan.FromSeconds(task.endtimeofday);

								while (start <= dtDescEnd)
								{
									if ((task.daysofweek & (int)start.DayOfWeek) == 0)
									{
										start += TimeSpan.FromDays(1);
										end += TimeSpan.FromDays(1);

										continue;
									}

									if ((start <= dtStart && dtStart < end) || (start < dtEnd && dtEnd <= end) ||
										(dtStart <= start && start < dtEnd) || (dtStart < end && end <= dtEnd))
									{
										return bRet = false;
									}
									start += TimeSpan.FromDays(1);
									end += TimeSpan.FromDays(1);
								}

								bRet = true;
							}
							else
							{
								if (origin.TaskStart >= task.TaskStart && origin.TaskStart <= (task.TaskEnd - 1))
								{
									return bRet = false;
								}
								else if(origin.TaskStart <= task.TaskStart && origin.TaskEnd >= (task.TaskEnd - 1))
								{
									return bRet = false;
								}
								else if ((origin.TaskEnd - 1) >= task.TaskStart && (origin.TaskEnd - 1) <= (task.TaskEnd - 1))
								{
									return bRet = false;
								}
								else if (origin.TaskStart >= task.TaskStart && (origin.TaskEnd - 1) <= (task.TaskEnd - 1))
								{
									return bRet = false;
								}
								else bRet = true;
							}

						}
					}
// 
// 					Rect comp = new Rect(Canvas.GetLeft(task), Canvas.GetTop(task),
// 					task.ActualWidth - 1,
// 					task.ActualHeight);
// 
// 					if(origin.IntersectsWith(comp))
// 					{
// 						return false;
// 					}
				}
				bRet = true;
			}
			catch
			{

			}
			return bRet;

		}

		private void btnCommit_Click(object sender, RoutedEventArgs e)		
        {
			//	중복 허용 불가 로직 삭제
			//foreach (UIElement element in tasksDiagram.Diagram.Children)
			//{
			//    TaskItem item = element as TaskItem;

			//    if (item == null || !item.IsModified) continue;

			//    if (false == CheckOverlapTime(item))
			//    {
			//        MessageBox.Show(Properties.Resources.mbAlreadyUsed, Properties.Resources.titleMessageBox, MessageBoxButton.OK, MessageBoxImage.Warning);

			//        e.Handled = true;
			//        return;
			//    }
			//}

			if(MessageBoxResult.Yes == MessageBox.Show(Properties.Resources.mbCommit, Properties.Resources.titleMessageBox, MessageBoxButton.YesNo))
			{                     
				using (new DigitalSignage.Common.WaitCursor())
				{
					foreach (TaskItem task in tasksDiagram.TaskItems)
					{
						if (task != null && task.IsModified)
						{
							task.IsModified = false;

							try
							{
								Task item = task.DataContext as Task;

								item.state = TaskState.StateEdit;
								if (-1 != cfg.ServerTasksList.UpdateTask(item))
								{
									try
									{
										///	스케줄 수정 기록
										cfg.ServerLogList.InsertDetailLog(ServerDataCache.GetInstance.Connected_UserName, (int)(LogType.LogType_Managing | LogType.LogType_Proceed), item.uuid.ToString(), "SCHEDULE_UPDATE", item.description, 0, 0, TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()));
									}
									catch { }
								}
								else
								{
									try
									{
										    ///	스케줄 수정 기록
										cfg.ServerLogList.InsertDetailLog(ServerDataCache.GetInstance.Connected_UserName, (int)(LogType.LogType_Managing | LogType.LogType_Failed), item.uuid.ToString(), "SCHEDULE_UPDATE", item.description, 0, 0, TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()));
									}
									catch { }
								}
							}
							catch (Exception ee)
							{
								MessageBox.Show(ee.Message);
							}
						}
					}

					tasksDiagram.IsModified = false;
					tasksDiagram.Rebuild();

                    // 메뉴버튼 재활성화
                    ChangeMenuBtnState(GPTree.SelectedItem);
				}              
			}
		}

        /// <summary>
        /// 타임라인 반영하기, 되돌리기 버튼 클릭 시 선택된 트리아이템에 따라 
        /// 메뉴버튼을 재활성화 시키는 함수
        /// </summary>
        /// <param name="selected">선택된 트리아이템</param>
        private void ChangeMenuBtnState(object selected)
        {
            XmlNode row = selected as XmlNode;
            string strName = row.Name;
            string strParentName = row.ParentNode.Name;          

            if (strName.ToLower().Equals("group") && strParentName.ToLower().Equals("networktree"))
            {
                // 루트그룹 일때 : 단말추가, 그룹추가, 그룹편집 버튼 활성화 
                tNewPlayer.IsEnabled = true;
                tNewGroup.IsEnabled = true;
                tEditGroup.IsEnabled = true;
            }
            else if (strName.ToLower().Equals("group"))
            {
                // 그룹 일때 : 단말추가, 그룹추가, 그룹편집, 그룹삭제 버튼 활성화
                tNewPlayer.IsEnabled = true;
                tNewGroup.IsEnabled = true;
                tEditGroup.IsEnabled = true;
                tDeleteGroup.IsEnabled = true;
            }
            else if (strName.ToLower().Equals("player"))
            {
                // 단말 일때 : 단말편집, 단말삭제 버튼 활성화
                 tEditPlayer.IsEnabled = true;
                 tDeletePlayer.IsEnabled = true;
            }
            else
            {
            }
        }

		private void CmdLogin(object sender, ExecutedRoutedEventArgs e)
		{
            System.Windows.Forms.Application.Restart();
            System.Windows.Application.Current.Shutdown();
            //try
            //{
            //    do
            //    {
            //        LoginDlg dlg = new LoginDlg(cfg, cfg.GetConfigStr("user", ""));
            //        dlg.Owner = this;
            //        dlg.ShowDialog();
            //        RibbonMain.Focus();

            //        if (!dlg.result)
            //        {
            //            GPTree.GroupTreeView.Visibility = Visibility.Collapsed;
            //            tasksDiagram.Visibility = Visibility.Collapsed;
            //            statusListBox.Visibility = Visibility.Collapsed;
            //            MessageBox.Show("로그인 후 사용하세요");
            //            throw new Exception(Properties.Resources.exceptionLoginCancel);
            //        }
            //        else
            //        {
            //            statusListBox.DataContext = null;

            //            if (!ServerDataCache.GetInstance.Initialize(dlg.login, dlg.password))
            //            {
            //                try
            //                {
            //                    ///	접속 로그 기록
            //                    cfg.ServerLogList.InsertDetailLog(this.userID.Text, (int)(LogType.LogType_Managing | LogType.LogType_Connection | LogType.LogType_Failed), "", "", "", 0, 0, TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()));
            //                }
            //                catch { }

            //                MessageBox.Show(Properties.Resources.mbFailedLogin, Properties.Resources.titleMessageBox);
            //                continue;
            //            }
            //            else
            //            {
            //                try
            //                {
            //                    ///	접속 로그 기록
            //                    cfg.ServerLogList.InsertDetailLog(this.userID.Text, (int)(LogType.LogType_Managing | LogType.LogType_Connection | LogType.LogType_Proceed), "", "", "", 0, 0, TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()));
            //                }
            //                catch { }
            //            }

            //            using (new DigitalSignage.Common.WaitCursor())
            //            {
            //                cfg.SetConfigStr("user", dlg.login);
            //                restoreParams();
            //                GPTree.SetParent(this, cfg);
            //                GPTree.ShowDetailInformation = true;
            //                GPTree.ShowPlayers = true;

            //                GPTree.UpdateLayout();
            //                GPTree.SelectedItem = null;
            //                tasksDiagram.Tasks = null;
            //                tasksDiagram.SelectedTreeObject = null;
            //                tasksDiagram.Rebuild();

            //                userID.Text = dlg.login;
            //                GPTree.GroupTreeView.Visibility = Visibility.Visible;
            //                tasksDiagram.Visibility = Visibility.Visible;
            //                statusListBox.Visibility = Visibility.Visible;
            //                // 로그인 성공 로그인 정보 송출
            //                SendConnectionInfo();
            //                GetProductCodeFromServer();
            //                UpdateMetaTagInformation();

            //            }
            //            break;

            //        }
            //    } while (true);

            //}
            //catch (Exception ee)
            //{
            //    logger.Error(ee.Message);
            //}

		}

		private void CmdExit(object sender, ExecutedRoutedEventArgs e)
		{
			this.Close();
		}
		private void CmdAbout(object sender, ExecutedRoutedEventArgs e)
		{
			AboutManager about = new AboutManager();
			about.Owner = this;
			about.ShowDialog();

			this.RibbonMain.Focus();
		}

		private void CmdPowerManagement(object sender, ExecutedRoutedEventArgs e)
		{
			try
			{
				using (DigitalSignage.Common.WaitCursor wc = new DigitalSignage.Common.WaitCursor())
				{
					ArrayList arrPids = new ArrayList();
					XmlNodeList arrPlayers = ServerDataCache.GetInstance.NetworkTree.SelectNodes("//child::player[attribute::del_yn=\"N\"]");

					if (arrPlayers != null)
					{
						foreach (XmlNode node in arrPlayers)
						{
							if (node.Attributes["state"].Value != "1")
							{
								arrPids.Add(node.Attributes["pid"].Value);
							}
						}

						MessageBoxResult mbr = MessageBox.Show(String.Format(Properties.Resources.mbQuestionPowerControlAll, arrPids.Count), Properties.Resources.titleMessageBox, MessageBoxButton.OKCancel, MessageBoxImage.Question);

						if (mbr == MessageBoxResult.OK)
						{
							String[] arrPidToString = arrPids.ToArray(typeof(String)) as String[];
							String sReturn = Config.GetConfig.ServerPlayersList.NPCUControlByPIDArray(arrPidToString, "2");

							wc.Dispose();

							NPCUResult npcu = new NPCUResult(String.Format(Properties.Resources.titlePowerManagement, Properties.Resources.labelOfflineRestart), sReturn);
							npcu.ShowDialog();
						}
					}
				}
			}
			catch (Exception err)
			{
				logger.Error(err + "");
			}
		}

		private void CmdUserManagement(object sender, ExecutedRoutedEventArgs e)
		{
			try
			{
				UsersDlg dlg = new UsersDlg(cfg);
				dlg.Owner = this;
				dlg.ShowDialog();
				RibbonMain.Focus();

			}
			catch (Exception err)
			{
				logger.Error(err + "");
			}
		}

		static object objLock = new object();

		private void UpdatePlayerListBySorting(string sort_keywords)
		{
			lock (objLock)
			{
				if (String.IsNullOrEmpty(sort_keywords))
					return;
				if (status_players == null)
					return;

				String[] arrKey = sort_keywords.Split(' ');

				IEnumerable<XmlNode> array = status_players.Cast<XmlNode>();

				if (array != null)
				{
					if (arrKey.Length == 2)
					{
						array = SortColumnWithoutChange(array, arrKey[0]);
					}
					else if (arrKey.Length == 4)
					{
						array = SortColumnWithoutChange(array, arrKey[0], arrKey[2]);
					}

					statusListBox.DataContext = UpdatePlayerListByFiltering(array);
				}
				else
					statusListBox.DataContext = status_players;

				try
				{
					tbFilterState.Text = String.Format(Properties.Resources.labelFilterFormat, status_players.Count, statusListBox.Items.Count);
				}
				catch
				{
					tbFilterState.Text = String.Format(Properties.Resources.labelFilterFormat, "-", "-");
				}
			}
		}

		private IEnumerable<XmlNode> UpdatePlayerListByFiltering(IEnumerable<XmlNode> nodelist)
		{
			IEnumerable<XmlNode> nodes = null;
			if (nodelist != null)
			{
				if (colState.Filter == null) nodes = nodelist;
				else if (colState.Filter.Equals(FilterConnections.Online)) nodes = nodelist.Where(item => item.Attributes["state"].Value.Equals("1"));
				else if (colState.Filter.Equals(FilterConnections.Offline)) nodes = nodelist.Where(item => item.Attributes["state"].Value.Equals("0"));
				else nodes = nodelist;

				if (!String.IsNullOrEmpty(colName.Filter as String))
					return nodes.Where(item => (item.Attributes["name"].Value.Contains(colName.Filter as String) || item.Attributes["pid"].Value.ToLower().Contains((colName.Filter as String).ToLower())));
				
			}

			return nodes;
		}

		private IEnumerable<XmlNode> SortColumnWithoutChange(IEnumerable<XmlNode> nodelist, string column)
		{
			IEnumerable<XmlNode> nodes = null;

			using (new DigitalSignage.Common.WaitCursor())
			{
				try
				{
					bool bAscending = true;

					if (order_players.Equals(String.Format("{0} asc", column)))
						bAscending = true;
					else
						bAscending = false;

					if (status_players != null)
					{
                        if (bAscending) nodes = nodelist.OrderBy(item => Convert.ChangeType(item.Attributes[column].Value, tp_sort));
                        else nodes = nodelist.OrderByDescending(item => Convert.ChangeType(item.Attributes[column].Value, tp_sort));

					}
				}
				catch
				{
				}
			}

			return nodes;
		}

		private IEnumerable<XmlNode> SortColumnWithoutChange(IEnumerable<XmlNode> nodelist, string column1, string column2)
		{
			IEnumerable<XmlNode> nodes = null;

			using (new DigitalSignage.Common.WaitCursor())
			{

				try
				{
					bool bAscending = true;

					if (order_players.Contains(column1))
					{
						if (order_players.Contains("asc"))
							bAscending = true;
						else
							bAscending = false;
					}
					else
						bAscending = true;

					if (nodelist != null)
					{
						if (bAscending)
						{
							nodes = nodelist.OrderBy(item => (String)(item.Attributes[column1].Value)).ThenBy(item => (String)(item.Attributes[column2].Value));
						}
						else nodes = nodelist.OrderByDescending(item => (String)(item.Attributes[column2].Value)).ThenByDescending(item => (String)(item.Attributes[column1].Value));

					}
				}
				catch
				{
				}
			}

			return nodes;
		}

		private void SortColumn(object sender, string column, Type change_tp)
		{
			try
			{
				using (new DigitalSignage.Common.WaitCursor())
				{
					GridViewColumnHeaderEx ex = sender as GridViewColumnHeaderEx;

					foreach (GridViewColumn col in gvStatus.Columns)
					{
						if (ex.Equals(col.Header))
							ex.Sort();
						else
						{
							GridViewColumnHeaderEx tmp = col.Header as GridViewColumnHeaderEx;
							if(tmp != null) tmp.UnSort();
						}
					}
                    
					IEnumerable<XmlNode> nodes = status_players.Cast<XmlNode>();

					if (!ex.IsSorting) statusListBox.DataContext = status_players;
					else if (ex.IsAscending)
					{
                        statusListBox.DataContext = UpdatePlayerListByFiltering(nodes.OrderBy(item => Convert.ChangeType(item.Attributes[column].Value, tp_sort = change_tp)));
						order_players = String.Format("{0} asc", column);
					}
					else
					{
                        statusListBox.DataContext = UpdatePlayerListByFiltering(nodes.OrderByDescending(item => Convert.ChangeType(item.Attributes[column].Value, tp_sort = change_tp)));
						order_players = String.Format("{0} desc", column);
					}
				}

				
			}
			catch { }

			/*
			using (new DigitalSignage.Common.WaitCursor())
			{
				try
				{
					bool bAscending = true;

					if (order_players.Equals(String.Format("{0} asc", column)))
					{
						order_players = String.Format("{0} desc", column);
						bAscending = false;
					}
					else
					{
						order_players = String.Format("{0} asc", column);
						bAscending = true;
					}
					if (status_players != null)
					{
						IEnumerable<XmlNode> nodes = status_players.Cast<XmlNode>();

						if (bAscending) statusListBox.DataContext = nodes.OrderBy(item => (String)(item.Attributes[column].Value));
						else statusListBox.DataContext = nodes.OrderByDescending(item => (String)(item.Attributes[column].Value));

					}
					// 				statusListBox.DataContext = status_players.Select("", order_players);
				}
				catch
				{
				}
			}
			*/
		}

		private void SortColumn(object sender, string column1, string column2)
		{
			try
			{
				using (new DigitalSignage.Common.WaitCursor())
				{
					GridViewColumnHeaderEx ex = sender as GridViewColumnHeaderEx;

					foreach (GridViewColumn col in gvStatus.Columns)
					{
						if (ex.Equals(col.Header))
							ex.Sort();
						else
						{
							GridViewColumnHeaderEx tmp = col.Header as GridViewColumnHeaderEx;
							if (tmp != null) tmp.UnSort();
						}
					}
					IEnumerable<XmlNode> nodes = status_players.Cast<XmlNode>();

					if (!ex.IsSorting) statusListBox.DataContext = status_players;
					else if (ex.IsAscending) 
					{
                        tp_sort = typeof(String);

						statusListBox.DataContext = UpdatePlayerListByFiltering(nodes.OrderBy(item => (String)(item.Attributes[column1].Value)).ThenBy(item => (String)(item.Attributes[column2].Value)));
						order_players = String.Format("{0} asc, {1} asc", column2, column1);
					}
					else 
					{
                        tp_sort = typeof(String);
                        
                        statusListBox.DataContext = UpdatePlayerListByFiltering(nodes.OrderByDescending(item => (String)(item.Attributes[column2].Value)).ThenByDescending(item => (String)(item.Attributes[column1].Value)));
						order_players = String.Format("{0} desc, {1} desc", column2, column1);
					}
				}
			}
			catch { }
			/*
			using (new DigitalSignage.Common.WaitCursor())
			{

				try
				{
					bool bAscending = true;

					if (order_players.Contains(column1))
					{
						if (order_players.StartsWith(column1))
						{
							order_players = String.Format("{0} desc, {1} desc", column2, column1);
							bAscending = false;
						}
						else
						{
							order_players = String.Format("{0} asc, {1} asc", column1, column2);
							bAscending = true;
						}
					}
					else
					{
						order_players = String.Format("{0} asc, {1} asc", column1, column2);
						bAscending = true;
					}

					if (status_players != null)
					{
						IEnumerable<XmlNode> nodes = status_players.Cast<XmlNode>();

						if (bAscending) 
						{
							statusListBox.DataContext = nodes.OrderBy(item => (String)(item.Attributes[column1].Value)).ThenBy(item => (String)(item.Attributes[column2].Value));
						}
						else statusListBox.DataContext = nodes.OrderByDescending(item => (String)(item.Attributes[column2].Value)).ThenByDescending(item => (String)(item.Attributes[column1].Value));

					}
				}
				catch
				{
				}
			}
			*/
		}
		private void colName_Click(object sender, RoutedEventArgs e)
		{
			
			SortColumn(sender, "name", typeof(String));
		}

		private void colHostname_Click(object sender, RoutedEventArgs e)
		{
            SortColumn(sender, "host", typeof(String));
		}

        private void colIPAddress_Click(object sender, RoutedEventArgs e)
        {
            SortColumn(sender, "_ipv4addr", typeof(String));
        }

        private void colPlayerID_Click(object sender, RoutedEventArgs e)
		{
            SortColumn(sender, "pid", typeof(String));
		}

		private void colState_Click(object sender, RoutedEventArgs e)
		{
            SortColumn(sender, "state", typeof(String));
		}

		private void colCurrScreen_Click(object sender, RoutedEventArgs e)
		{
            SortColumn(sender, "curr_screen", typeof(String));
		}

		private void colCurrSubtitle_Click(object sender, RoutedEventArgs e)
		{
            SortColumn(sender, "curr_subtitle", typeof(String));
		}

		private void colAllDownload_Click(object sender, RoutedEventArgs e)
		{
            SortColumn(sender, "all_d_progress", typeof(String));
		}

		private void colCurrDownload_Click(object sender, RoutedEventArgs e)
		{
            SortColumn(sender, "curr_d_progress", typeof(String));
		}

		private void colFreeSpace_Click(object sender, RoutedEventArgs e)
		{
            SortColumn(sender, "freespace", typeof(Int32));
		}

		private void colUptime_Click(object sender, RoutedEventArgs e)
		{
            SortColumn(sender, "uptime", typeof(Int64));
		}

        private void colLastConn_Click(object sender, RoutedEventArgs e)
		{
            SortColumn(sender, "_timestamp", typeof(Int64));
		}

		private void colCPURate_Click(object sender, RoutedEventArgs e)
		{
            SortColumn(sender, "cpurate", typeof(Int32));
		}

		private void colMemoryUsageRate_Click(object sender, RoutedEventArgs e)
		{
            SortColumn(sender, "memoryusage", typeof(Int32));
		}

		private void colVersion_Click(object sender, RoutedEventArgs e)
		{
			SortColumn(sender, "versionH", "versionL");
		}

		private void colDevice_Click(object sender, RoutedEventArgs e)
		{
            SortColumn(sender, "_serial_device_name", typeof(String));
		}

		private void colSource_Click(object sender, RoutedEventArgs e)
		{
            SortColumn(sender, "_serial_stat_source", typeof(String));
		}

		private void colVolume_Click(object sender, RoutedEventArgs e)
		{
            SortColumn(sender, "_serial_stat_volume", typeof(Int32));
		}
        private void colPCVolume_Click(object sender, RoutedEventArgs e)
        {
            SortColumn(sender, "curr_volume", typeof(Int32));
        }

		private SerialKey.NESerialKey._ProductCode GetProductCodeFromServer()
		{
			return cfg.ServerDataCacheList.GetProductCode();
		}
		private void SendConnectionInfo()
		{
			try
			{
				string addr1 = "", addr2 = "", addr3 = "";
				String MachineName = System.Environment.MachineName;

				IPHostEntry host_entry = Dns.GetHostByName(Dns.GetHostName());

				foreach (IPAddress ip in host_entry.AddressList)
				{
					if (String.IsNullOrEmpty(addr1))
					{
						addr1 = ip.ToString();
					}
					else if (String.IsNullOrEmpty(addr2))
					{
						addr2 = ip.ToString();
					}
					else if (String.IsNullOrEmpty(addr3))
					{
						addr3 = ip.ToString();
					}
					else break;
				}

				Assembly myAssembly = Assembly.GetExecutingAssembly();
				AssemblyName myAssemblyName = myAssembly.GetName();

				/////	로그인 Detail 기록
				//cfg.ServerUsersList.AddConnectionInfo(, MachineName, addr1, addr2, addr3, myAssemblyName.Version.ToString());
				
				///	접속 로그 기록
				cfg.ServerLogList.InsertDetailLog(this.userID.Text, (int)(LogType.LogType_Managing | LogType.LogType_Connection | LogType.LogType_Proceed), "", "", String.Format("(USERID={0})(HOST={1})(IP={2})(VERSION={3})",this.userID.Text, MachineName, addr1, myAssemblyName.Version.ToString()), 0, 0, TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()));
				
			}
			catch (Exception ex)
			{
				logger.Error(ex.ToString());
			}
		}

		public void UpdateMetaTagInformation()
		{
			string sSettingPath = AppDomain.CurrentDomain.BaseDirectory + @"settings\";
			string sMetaTagFilePath = sSettingPath + "meta_tags.xml";

			long ts = -10000;
			try
			{
				XmlDocument doc = new XmlDocument();
				doc.Load(sMetaTagFilePath);

				XmlNode node = doc.SelectSingleNode("//child:tag_groups");
				ts = Convert.ToInt64(node.Attributes["ts"].Value);
			}
			catch
			{
			}

			try
			{
				String sReturn = cfg.ServerDataCacheList.GetMetaTags(ts);

                if (!String.IsNullOrEmpty(sReturn))
                {
                    XmlDocument doc = new XmlDocument();

                    doc.LoadXml(sReturn);

                    if (!System.IO.Directory.Exists(sSettingPath))
                        System.IO.Directory.CreateDirectory(sSettingPath);

                    doc.Save(sMetaTagFilePath);
                }
			}
			catch { }
		}


        public static int[] GetCustomColors()
        {
            try
            {
                string[] customcolors = ConfigurationManager.AppSettings.Get("CustomColors").Split(',');
                if (customcolors.Length > 0)
                {
                    int[] arr = new int[customcolors.Length];
                    int index = 0;
                    foreach (string str in customcolors)
                    {
                        if (str.Length == 0)
                            return null;
                        arr[index] = int.Parse(str);
                        index++;
                    }

                    return arr;
                }
            }
            catch (Exception ex)
            {
                string errmsg = ex.Message.ToString();
                //  System.Windows.MessageBox.Show(errmsg);
            }
            return null;
        }

        public static void SetAppSeting(string key, string value)
        {
            try
            {
                ConfigurationManager.AppSettings.Set(key, value);


                String path = Assembly.GetExecutingAssembly().GetModules()[0].FullyQualifiedName + ".config";
//                 string path = GetRootPath() + "WPFDesigner.exe.config";
                if (File.Exists(path))
                {
                    XmlDocument _doc = new XmlDocument();
                    _doc.Load(path);
                    XmlNode appSetting = _doc.SelectSingleNode("//appSettings");
                    foreach (XmlNode node in appSetting.ChildNodes)
                    {
                        if (node.Attributes["key"].InnerText.Equals(key))
                        {
                            node.Attributes["value"].InnerText = value;
                            break;
                        }
                    }
                    _doc.Save(path);
                }
            }
            catch (Exception ex)
            {
                string errmsg = ex.Message.ToString();
                //  System.Windows.MessageBox.Show(errmsg);
            }
        }

		private void statusListBox_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.Key == System.Windows.Input.Key.C)
			{
				if (Keyboard.Modifiers == ModifierKeys.Control)
				{
					CopySelectedPlayerlistToClipboard();
					e.Handled = true;
				}
			}
		}
		
		private void GridViewColumnHeader_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
		{
			ContextMenu menu = new ContextMenu();
			menu.Items.Clear();

			Separator sp = null;
			MenuItem mi = null;

			mi = new MenuItem();
			mi.Header = Properties.Resources.menuitemTaskWizard;
			mi.Icon = GetIconImage(Properties.Resources.icon_task_wizard);
			mi.Click += new RoutedEventHandler(GPTree_OnClickTaskWizard);
			menu.Items.Add(mi);

			sp = new Separator();

			menu.Items.Add(sp);

			mi = new MenuItem();
			mi.Header = "필터";
			menu.Items.Add(mi);

			if (gvStatus.Columns != null)
			{
				foreach (GridViewColumn col in gvStatus.Columns)
				{
					GridViewColumnHeaderEx header = col.Header as GridViewColumnHeaderEx;
					if (header != null && header.CanFilter)
					{
						//if(col == 
						MenuItem mii = new MenuItem();

						mii.Header = Convert.ToString(header.Content);
						mii.DataContext = col;
						mii.IsCheckable = true;
						mii.IsChecked = col.Width == 0 ? false : false;
						mii.Click += new RoutedEventHandler(mii_View_Click);
						mi.Items.Add(mii);						
					}
				}
			}

			MenuItem mi2 = new MenuItem();
			mi2.Header = "정렬";
			menu.Items.Add(mi2);

			//menu.IsOpen = true;

			e.Handled = true;
		}

		void mii_View_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				MenuItem mi = sender as MenuItem;
				GridViewColumn col = mi.DataContext as GridViewColumn;

				if (mi.IsChecked == true)
				{
					gvStatus.Columns.Add(col);
				}
				else
				{
					gvStatus.Columns.Remove(col);
				}
			}
			catch { }
		}

		private void GridViewColumnHeader_MouseDoubleClick(object sender, MouseButtonEventArgs e)
		{
			
			e.Handled = true;
		}

		private void tbFilter_TextChanged(object sender, TextChangedEventArgs e)
		{
			if (!tbFilter.Text.Equals(colName.Filter))
			{
				colName.Filter = tbFilter.Text;

				int selected = statusListBox.SelectedIndex;
				UpdatePlayerListBySorting(order_players);
				statusListBox.SelectedIndex = selected;
			}
		}

       private void CmdServerIP(object sender, ExecutedRoutedEventArgs e)
        {
            try
            {
                cfg.ServerFound = false;
                MessageBox.Show("IP변경시 재시작 됩니다.");
                ConnectToServer();
                System.Windows.Forms.Application.Restart();
                System.Windows.Application.Current.Shutdown();

            }
            catch (Exception err)
            {
                logger.Error(err + "");
            }
        }


        private void btnSearchItem_Click(object sender, RoutedEventArgs e)
        {
            string strPlayerName = String.Empty;
            strPlayerName = tBSearch.Text.ToString();

            TreeViewItem parent = GPTree.GroupTreeView.Items[0] as TreeViewItem;

            if (strPlayerName != "")
            {
                SearchTreeItem(parent, strPlayerName);
            }           
        }

        private void OnKeyDownHandler(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                string strPlayerName = String.Empty;
                strPlayerName = tBSearch.Text.ToString();

                TreeViewItem parent = GPTree.GroupTreeView.Items[0] as TreeViewItem;                

                if (strPlayerName != "")
                {
                    SearchTreeItem(parent, strPlayerName);
                }
            }
        }

        /// <summary>
        /// 2014.02.17. YBKIM 
        /// Tree에서 찾을 키워드와 동일하거나 포함된 요소를 찾아 선택하는 함수
        /// </summary>
        /// <param name="selected">NULL 인 경우, 트리의 선택된 요소에 상관 없이 처음부터 검색, TREEITEM 요소인 경우 트리의 선택된 요소 다음 부터 검색</param>
        /// <param name="strPlayer">찾을 키워드</param>
        private void SearchTreeItem(TreeViewItem selected, string strPlayer)
        {
            TreeViewItem parent = (selected == null ? GPTree.GroupTreeView.Items[0] as TreeViewItem : selected);
            ItemCollection nodes = parent.Items;

            bool bFindSelectedItems = (parent == GPTree.GroupTreeView.SelectedItem) || (null == GPTree.GroupTreeView.SelectedItem) || (selected == null);
        
            TreeViewItem finded = BrowseNodes(nodes, strPlayer, ref bFindSelectedItems);

            if (finded != null)
            {
                GPTree.SelectedItem = finded;
                finded.IsSelected = true;
                finded.BringIntoView();
            }
            else
            {
                if (MessageBoxResult.Yes == MessageBox.Show(Properties.Resources.mbSearch, Properties.Resources.titleMessageBox, MessageBoxButton.YesNo))
                {
                    SearchTreeItem(null, strPlayer);
                }
            }
        }

        private TreeViewItem BrowseNodes(ItemCollection nodes, string searchKeyword, ref bool bStartFind)
        {
            foreach (TreeViewItem node in nodes)
            {
                GroupTreeItem gti = node.Header as GroupTreeItem;

                if (gti == null)
                {
                    return null;
                }
                else if (bStartFind && gti.Text.Contains(searchKeyword))
                {
                    return node;
                }
                else
                {
                    if (bStartFind == false)
                    {
                        TreeViewItem selecteditem = GPTree.GroupTreeView.SelectedItem as TreeViewItem;

                        if (selecteditem == node)
                        {
                            bStartFind = true;
                        } 
                    }

                    TreeViewItem item = BrowseNodes(node.Items, searchKeyword, ref bStartFind);

                    if(item == null)
                        continue;
                    else
                        return item;
                }
           }

            return null;
        }

        public IEnumerable<TreeViewItem> RecursiveNodeList
        {
            get
            {
                TreeViewItem parent = GPTree.GroupTreeView.Items[0] as TreeViewItem;
                ItemCollection nodes = parent.Items;
                //return BrowseNodes(nodes, tBSearch.Text).ToList();
                return null;
            }
        }             

        //public void ExpandAll()
        //{
        //    foreach (TreeViewItem item in RecursiveNodeList)
        //    {
        //        item.IsExpanded = false;
        //        string s = ((GroupTreeItem)item.Header).Text;
        //        GroupTreeItem gti = item.Header as GroupTreeItem;
        //        string strGroupName = gti.Text;
        //    }
        //}
	}
}
