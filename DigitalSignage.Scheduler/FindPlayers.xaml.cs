﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

using System.Net;
using System.Net.Sockets;
using System.Threading;

using NLog;

using DigitalSignage.DataBase;

namespace DigitalSignage.Scheduler
{
    /// <summary>
    /// Interaction logic for NewDlgItem.xaml
    /// </summary>
    public partial class FindPlayers : Window
    {
        private int playersUdpPort = 1889;
        private int localUdpPort = 1890;
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private bool process = false;
        private Thread udpThread;
        private Config cfg = null;
        private DataSet.groupsRow target = null;

        public FindPlayers(Config config )
        {
            InitializeComponent();
            cfg = config;
        }

        #region Event handlers
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                listBox.Items.Clear();
                //Starting the UDP Server thread.
                process = true;
                udpThread = new Thread(new ThreadStart(Start));
                udpThread.Start();
            }
            catch (Exception err)
            {
                logger.Error(err + "");
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            listBox.SelectAll();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            listBox.UnselectAll();
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            try
            {
                if (target == null)
                {
                    MessageBox.Show("Select group, please.");
                    return;
                }
                DataSet.playersDataTable table = new DataSet.playersDataTable();
                for ( int i = 0; i < listBox.SelectedItems.Count; i ++ )
                {
                    ListViewItem item = (ListViewItem)listBox.SelectedItems[i];
                    string[] data = ((String)item.DataContext).Split(',');
                    DataSet.playersRow row = table.NewplayersRow();
                    int pos = data[3].IndexOf(":");
                    if (pos < 0)
                        row.host = data[3];
                    else
                        row.host = data[3].Substring(0, pos);
                    row.pid = "0000000";
                    row.gid = target.gid;
                    row.name = data[2];
                    row.port = "COM1";
                    row.ComSpeed = 38400;
                    row.ComBits = 1;
                    table.AddplayersRow(row);
                }
                cfg.ServerPlayersList.Add( target.gid, table );
                this.Close();
            }
            catch (Exception err)
            {
                logger.Error(err + "");
            }
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        public void Stop()
        {
            process = false;
            udpThread.Abort();
        }

        private void Button_Click_5(object sender, RoutedEventArgs e)
        {
            try
            {
                GroupPicker dlg = new GroupPicker(cfg, false);
                dlg.Owner = this;
                dlg.ShowDialog();
                if (dlg.result != null)
                {
                    TargetObject = (DataSet.groupsRow)dlg.result;
                }
            }
            catch (Exception err)
            {
				logger.Error(err + "");
			}
        }
        #endregion

        #region Network routines
        public void Start()
        {
            SetSearchState(false);
            Socket s = null;
            string query = "DigitalSignageEchoRequest";
            process = true;
            int recv = 0;
            IPEndPoint point = new IPEndPoint( IPAddress.Broadcast, playersUdpPort );
            IPEndPoint localPoint = new IPEndPoint( 0, localUdpPort );
            try
            {
                // Create a UDP socket
                s = new Socket(point.Address.AddressFamily, SocketType.Dgram, ProtocolType.Udp);
                s.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Broadcast, 1);
                s.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReceiveTimeout, 4000);
                s.Bind(localPoint);
                s.SendTo(System.Text.Encoding.ASCII.GetBytes(query.ToCharArray()), point);
                while (process)
                {
                    Byte[] received = new Byte[256];
                    IPEndPoint tmpIpEndPoint = new IPEndPoint(0, localUdpPort );
                    EndPoint remoteEP = (tmpIpEndPoint);
                    recv = s.ReceiveFrom(received, ref remoteEP);
                    String dataReceived = System.Text.Encoding.ASCII.GetString(received, 0, recv);

                    if ( dataReceived.StartsWith("DigitalSignagePlayer" ))
                    {
                        AddPlayerToList( dataReceived+","+remoteEP.ToString());
                    }
                    logger.Info(dataReceived);
                }
            }
            catch (Exception e)
            {
                logger.Error(e + "");
            }
            finally
            {
                if (s != null)
                {
                    s.Shutdown(SocketShutdown.Both);
                    s.Close();
                }
            }
            SetSearchState(true);
        }
        #endregion

        #region Async routines
        delegate void setSearchStateCB( bool state );
        public void SetSearchState(bool state)
        {
            try
            {
                if (!this.Dispatcher.CheckAccess())
                {
                    this.Dispatcher.BeginInvoke(DispatcherPriority.Send,
                       new setSearchStateCB(SetSearchState), state);
                }
                else
                {
                    searchButton.IsEnabled = state;
                }
            }
            catch (Exception e)
            {
                logger.Error(e + "");
            }
        }

        delegate void addPlayerCB(string player);
        public void AddPlayerToList(string player)
        {
            try
            {
                if (!this.Dispatcher.CheckAccess())
                {
                    this.Dispatcher.BeginInvoke(DispatcherPriority.Send,
                       new addPlayerCB(AddPlayerToList), player);
                }
                else
                {
                    string[] data = player.Split(',');
                    int pos = data[3].IndexOf(":");
                    if (pos > 0)
                        data[3] = data[3].Substring(0, pos);
                    DataSet.playersDataTable players = cfg.ServerPlayersList.FindPlayerByHost(data[3]);
                    if (players.Rows.Count > 0) return;

                    //searchButton.IsEnabled = state;
                    ListViewItem item = new ListViewItem();

                    item.Content = data[2]+" on "+data[3];
                    item.DataContext = player;
                    listBox.Items.Add(item);
                }
            }
            catch (Exception e)
            {
                logger.Error(e + "");
            }
        }
        #endregion  



        public DataSet.groupsRow TargetObject
        {
            get
            {
                return target;
            }
            set
            {
                if (value == null)
                {
                    target = null;
                    groupName.Text = "";
                }
                else
                {
                    target = value;
                    groupName.Text = ((DataSet.groupsRow)value).name;
                }
            }
        }
    }
}
