﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

using System.Net;
using System.Net.Sockets;
using System.Threading;

using NLog;

using DigitalSignage.DataBase;
using DigitalSignage.Common;

namespace DigitalSignage.Scheduler
{
    /// <summary>
    /// Interaction logic for NewDlgItem.xaml
    /// </summary>
    public partial class NewCmdDlg : Window
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        public Boolean result = false;
        public byte[] dataArray = null;
        public string name = "New command";
        //private Config cfg = null;

        public NewCmdDlg( DataSet.RSCommandsRow data )
        {
            InitializeComponent();
            if (data != null)
            {
                name = data.name;
                dataArray = data.data;
            }
        }

        #region Event handlers
        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            try
            {
                result = false;
                this.Close();
            }
            catch (Exception err)
            {
                logger.Error(err + "");
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                nameUI.Text = name;
                dataUI.data = dataArray;    
            }
            catch (Exception err)
            {
				logger.Error(err + "");
            }
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            try
            {
                if (nameUI.Text.Equals(""))
                {
                    MessageBox.Show("Please, enter command name", "Scheduler", 
                        MessageBoxButton.OK, MessageBoxImage.Stop);
                    return;
                }
                name = nameUI.Text;
                dataArray = dataUI.data;

                result = true;
                this.Close();
            }
            catch (Exception err)
            {
                logger.Error(err + "");
            }
        }
        #endregion
    }
}
