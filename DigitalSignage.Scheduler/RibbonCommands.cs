﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Windows.Controls.Ribbon;
using System.Windows.Media;
using System.Windows;

namespace DigitalSignage.Scheduler
{
	public static class RibbonCommands
	{
		private static String ConvertSpaceToReturn(String sText)
		{
// 			string sReturn = "";
// 			string[] array = sText.Split(' ');
// 
// 			foreach(string piece in array)
// 			{
// 				sReturn += (piece + '\n');
// 			}
// 
// 			return sReturn.TrimEnd('\n');			
			return sText;
		}

		#region File Commands
        public static RibbonCommand ServerIP
        {
            get
            {
                RibbonCommand command = (RibbonCommand)Application.Current.Resources["rServerIP"];
                command.LabelTitle = Properties.Resources.menuitemServerIP;
                command.ToolTipTitle = Properties.Resources.menuitemServerIP;
                command.ToolTipDescription = Properties.Resources.descServerIP;
                return command;
            }
        }

		public static RibbonCommand Login
		{
			get
			{
				RibbonCommand command = (RibbonCommand)Application.Current.Resources["rLogin"];
				command.LabelTitle = Properties.Resources.menuitemLogIn;
				command.ToolTipTitle = Properties.Resources.menuitemLogIn;
				command.ToolTipDescription = Properties.Resources.descLogIn;
				return command;
			}
		}


		public static RibbonCommand Title
		{
			get
			{
				RibbonCommand command = (RibbonCommand)Application.Current.Resources["rTitle"];
				command.LabelTitle = Properties.Resources.menuitemExit;
				command.ToolTipTitle = Properties.Resources.menuitemExit;
				command.ToolTipDescription = Properties.Resources.descExit;
				return command;
			}
		}
		
		public static RibbonCommand Exit
		{
			get
			{
				RibbonCommand command = (RibbonCommand)Application.Current.Resources["rExit"];
				command.LabelTitle = Properties.Resources.menuitemExit;
				command.ToolTipTitle = Properties.Resources.menuitemExit;
				command.ToolTipDescription = Properties.Resources.descExit;
				return command;
			}
		}
		public static RibbonCommand FTP
		{
			get
			{
				RibbonCommand command = (RibbonCommand)Application.Current.Resources["rFTP"];
                command.LabelTitle = Properties.Resources.ribbonFtp;
				command.ToolTipTitle = Properties.Resources.menuitemFtp;
				command.ToolTipDescription = Properties.Resources.descFtp;
				return command;
			}
		}

		public static RibbonCommand ConnectGroup
		{
			get
			{
				RibbonCommand command = (RibbonCommand)Application.Current.Resources["rConnectGroup"];
				command.LabelTitle = Properties.Resources.menuGroupConnect;
				command.ToolTipTitle = Properties.Resources.menuGroupConnect;
				return command;
			}
		}
		public static RibbonCommand ManagementGroup
		{
			get
			{
				RibbonCommand command = (RibbonCommand)Application.Current.Resources["rManagementGroup"];
				command.LabelTitle = Properties.Resources.menuGroupManagement;
				command.ToolTipTitle = Properties.Resources.menuGroupManagement;
				return command;
			}
		}
		public static RibbonCommand ContentManagementGroup
		{
			get
			{
				RibbonCommand command = (RibbonCommand)Application.Current.Resources["rContentManagementGroup"];
				command.LabelTitle = Properties.Resources.menuGroupContent;
				command.ToolTipTitle = Properties.Resources.menuGroupContent;
				return command;
			}
		}
		public static RibbonCommand ScheduleGroup
		{
			get
			{
				RibbonCommand command = (RibbonCommand)Application.Current.Resources["rScheduleGroup"];
				command.LabelTitle = Properties.Resources.menuGroupScheduling;
				command.ToolTipTitle = Properties.Resources.menuGroupScheduling;
				return command;
			}
		}

		public static RibbonCommand PlayerGroup
		{
			get
			{
				RibbonCommand command = (RibbonCommand)Application.Current.Resources["rPlayerGroup"];
				command.LabelTitle = Properties.Resources.menuGroupPlayer;
				command.ToolTipTitle = Properties.Resources.menuGroupPlayer;
				return command;
			}
		}
		public static RibbonCommand GroupGroup
		{
			get
			{
				RibbonCommand command = (RibbonCommand)Application.Current.Resources["rGroupGroup"];
				command.LabelTitle = Properties.Resources.menuGroupGroup;
				command.ToolTipTitle = Properties.Resources.menuGroupGroup;
				return command;
			}
		}
		public static RibbonCommand UserGroup
		{
			get
			{
				RibbonCommand command = (RibbonCommand)Application.Current.Resources["rUserGroup"];
				command.LabelTitle = Properties.Resources.menuGroupUser;
				command.ToolTipTitle = Properties.Resources.menuGroupUser;
				return command;
			}
		}

		public static RibbonCommand ShowHideGroup
		{
			get
			{
				RibbonCommand command = (RibbonCommand)Application.Current.Resources["rShowHideGroup"];
				command.LabelTitle = Properties.Resources.menuShowHide;
				command.ToolTipTitle = Properties.Resources.menuShowHide;
				return command;
			}
		}
		public static RibbonCommand TaskWizard
		{
			get
			{
				RibbonCommand command = (RibbonCommand)Application.Current.Resources["rTaskWizard"];
				command.LabelTitle = ConvertSpaceToReturn(Properties.Resources.menuitemTaskWizard);
				command.ToolTipTitle = Properties.Resources.menuitemTaskWizard;
				command.ToolTipDescription = Properties.Resources.descTaskWizard;
				return command;
			}
		}

		public static RibbonCommand DefaultTask
		{
			get
			{
				RibbonCommand command = (RibbonCommand)Application.Current.Resources["rDefaultTask"];
				command.LabelTitle = ConvertSpaceToReturn(Properties.Resources.ribbonAddDefaultSchedule);
                command.ToolTipTitle = Properties.Resources.menuitemAddDefaultSchedule;
				command.ToolTipDescription = Properties.Resources.descAddDefaultSchedule;
				return command;
			}
		}

		public static RibbonCommand SubtitleTask
		{
			get
			{
				RibbonCommand command = (RibbonCommand)Application.Current.Resources["rSubtitleTask"];
				command.LabelTitle = ConvertSpaceToReturn(Properties.Resources.ribbonAddSubtitle);
                command.ToolTipTitle = Properties.Resources.menuitemAddSubtitle;
				command.ToolTipDescription = Properties.Resources.descAddSubtitle;
				return command;
			}
		}

		public static RibbonCommand ProgramTask
		{
			get
			{
				RibbonCommand command = (RibbonCommand)Application.Current.Resources["rProgramTask"];
				command.LabelTitle = ConvertSpaceToReturn(Properties.Resources.ribbonAddTaskProgram);
                command.ToolTipTitle = Properties.Resources.menuitemAddTaskProgram;
				command.ToolTipDescription = Properties.Resources.descAddTaskProgram;
				return command;
			}
		}

		public static RibbonCommand ControlTask
		{
			get
			{
				RibbonCommand command = (RibbonCommand)Application.Current.Resources["rControlTask"];
				command.LabelTitle = ConvertSpaceToReturn(Properties.Resources.ribbonAddControl);
                command.ToolTipTitle = Properties.Resources.menuitemAddControl;
				command.ToolTipDescription = Properties.Resources.descAddControl;
				return command;
			}
		}
		public static RibbonCommand ShowDefault
		{
			get
			{
				RibbonCommand command = (RibbonCommand)Application.Current.Resources["rShowDefault"];
				command.LabelTitle = ConvertSpaceToReturn(Properties.Resources.menuShowDefault);
				command.ToolTipTitle = Properties.Resources.menuShowDefault;
				command.ToolTipDescription = Properties.Resources.menuShowDefault;
				return command;
			}
		}
		public static RibbonCommand ShowProgram
		{
			get
			{
				RibbonCommand command = (RibbonCommand)Application.Current.Resources["rShowProgram"];
				command.LabelTitle = ConvertSpaceToReturn(Properties.Resources.menuShowProgram);
				command.ToolTipTitle = Properties.Resources.menuShowProgram;
				command.ToolTipDescription = Properties.Resources.menuShowProgram;
				return command;
			}
		}
		public static RibbonCommand ShowSubtitle
		{
			get
			{
				RibbonCommand command = (RibbonCommand)Application.Current.Resources["rShowSubtitle"];
				command.LabelTitle = ConvertSpaceToReturn(Properties.Resources.menuShowSubtitle);
				command.ToolTipTitle = Properties.Resources.menuShowSubtitle;
				command.ToolTipDescription = Properties.Resources.menuShowSubtitle;
				return command;
			}
		}
		public static RibbonCommand ShowControl
		{
			get
			{
				RibbonCommand command = (RibbonCommand)Application.Current.Resources["rShowControl"];
				command.LabelTitle = ConvertSpaceToReturn(Properties.Resources.menuShowControl);
				command.ToolTipTitle = Properties.Resources.menuShowControl;
				command.ToolTipDescription = Properties.Resources.menuShowControl;
				return command;
			}
		}
		public static RibbonCommand ShowScroll
		{
			get
			{
				RibbonCommand command = (RibbonCommand)Application.Current.Resources["rShowScroll"];
				command.LabelTitle = ConvertSpaceToReturn(Properties.Resources.menuShowScroll);
				command.ToolTipTitle = Properties.Resources.menuShowScroll;
				command.ToolTipDescription = Properties.Resources.menuShowScroll;
				return command;
			}
		}
		public static RibbonCommand EditTask
		{
			get
			{
				RibbonCommand command = (RibbonCommand)Application.Current.Resources["rEditTask"];
				command.LabelTitle = ConvertSpaceToReturn(Properties.Resources.menuitemEditTask);
				command.ToolTipTitle = Properties.Resources.menuitemEditTask;
				command.ToolTipDescription = Properties.Resources.descEditTask;
				return command;
			}
		}

		public static RibbonCommand DeleteTask
		{
			get
			{
				RibbonCommand command = (RibbonCommand)Application.Current.Resources["rDeleteTask"];
				command.LabelTitle = ConvertSpaceToReturn(Properties.Resources.menuitemDeleteTask);
				command.ToolTipTitle = Properties.Resources.menuitemDeleteTask;
				command.ToolTipDescription = Properties.Resources.descDeleteTask;
				return command;
			}
		}

		public static RibbonCommand AddPlayer
		{
			get
			{
				RibbonCommand command = (RibbonCommand)Application.Current.Resources["rAddPlayer"];
				command.LabelTitle = Properties.Resources.menuitemAddPlayer;
				command.ToolTipTitle = Properties.Resources.menuitemAddPlayer;
				command.ToolTipDescription = Properties.Resources.descAddPlayer;
				return command;
			}
		}
		public static RibbonCommand EditPlayer
		{
			get
			{
				RibbonCommand command = (RibbonCommand)Application.Current.Resources["rEditPlayer"];
				command.LabelTitle = Properties.Resources.menuitemEditPlayer;
				command.ToolTipTitle = Properties.Resources.menuitemEditPlayer;
				command.ToolTipDescription = Properties.Resources.descEditPlayer;
				return command;
			}
		}
		public static RibbonCommand DeletePlayer
		{
			get
			{
				RibbonCommand command = (RibbonCommand)Application.Current.Resources["rDeletePlayer"];
				command.LabelTitle = Properties.Resources.menuitemDeletePlayer;
				command.ToolTipTitle = Properties.Resources.menuitemDeletePlayer;
				command.ToolTipDescription = Properties.Resources.descDeletePlayer;
				return command;
			}
		}

		public static RibbonCommand AddGroup
		{
			get
			{
				RibbonCommand command = (RibbonCommand)Application.Current.Resources["rAddGroup"];
				command.LabelTitle = ConvertSpaceToReturn(Properties.Resources.ribbonAddGroup);
				command.ToolTipTitle = Properties.Resources.menuitemAddGroup;
				command.ToolTipDescription = Properties.Resources.descAddGroup;
				return command;
			}
		}

		public static RibbonCommand EditGroup
		{
			get
			{
				RibbonCommand command = (RibbonCommand)Application.Current.Resources["rEditGroup"];
                command.LabelTitle = ConvertSpaceToReturn(Properties.Resources.ribbonEditGroup);
				command.ToolTipTitle = Properties.Resources.menuitemEditGroup;
				command.ToolTipDescription = Properties.Resources.descEditGroup;
				return command;
			}
		}

		public static RibbonCommand DeleteGroup
		{
			get
			{
				RibbonCommand command = (RibbonCommand)Application.Current.Resources["rDeleteGroup"];
                command.LabelTitle = ConvertSpaceToReturn(Properties.Resources.ribbonDeleteGroup);
				command.ToolTipTitle = Properties.Resources.menuitemDeleteGroup;
				command.ToolTipDescription = Properties.Resources.descDeleteGroup;
				return command;
			}
		}

		public static RibbonCommand EditPlaylist
		{
			get
			{
				RibbonCommand command = (RibbonCommand)Application.Current.Resources["rEditPlaylist"];
				command.LabelTitle = Properties.Resources.menuitemEdPlaylist;
				command.ToolTipTitle = Properties.Resources.menuitemEdPlaylist;
				command.ToolTipDescription = Properties.Resources.descEdPlaylist;
				return command;
			}
		}

		public static RibbonCommand EditProgram
		{
			get
			{
				RibbonCommand command = (RibbonCommand)Application.Current.Resources["rEditProgram"];
				command.LabelTitle = Properties.Resources.menuitemEdProgram;
				command.ToolTipTitle = Properties.Resources.menuitemEdProgram;
				command.ToolTipDescription = Properties.Resources.descEdProgram;
				return command;
			}
		}

		public static RibbonCommand UserManagement
		{
			get
			{
				RibbonCommand command = (RibbonCommand)Application.Current.Resources["rUserManagement"];
				command.LabelTitle = Properties.Resources.menuitemUserManagement;
				command.ToolTipTitle = Properties.Resources.menuitemUserManagement;
				command.ToolTipDescription = Properties.Resources.descUserManagement;
				return command;
			}
		}

		public static RibbonCommand PowerManagement
		{
			get
			{
				RibbonCommand command = (RibbonCommand)Application.Current.Resources["rPowerManagement"];
				command.LabelTitle = Properties.Resources.menuitemPowerManagement;
				command.ToolTipTitle = Properties.Resources.menuitemPowerManagement;
				command.ToolTipDescription = Properties.Resources.descPowerManagement;
				return command;
			}
		}

		public static RibbonCommand LanguageSelector
		{
			get
			{
				RibbonCommand command = (RibbonCommand)Application.Current.Resources["rLanguageSelector"];
				command.LabelTitle = Properties.Resources.menuitemLanguages;
				command.ToolTipTitle = Properties.Resources.menuitemLanguages;
				command.ToolTipDescription = Properties.Resources.descLanguages;
				return command;
			}
		}

		public static RibbonCommand About
		{
			get
			{
				RibbonCommand command = (RibbonCommand)Application.Current.Resources["rAbout"];
				command.LabelTitle = Properties.Resources.titleAbout;
				command.ToolTipTitle = Properties.Resources.titleAbout;
				command.ToolTipDescription = Properties.Resources.descAbout;
				return command;
			}
		}

        public static RibbonCommand TemplateManagementGroup
        {
            get
            {
                RibbonCommand command = (RibbonCommand)Application.Current.Resources["rTemplateManagementGroup"];
                command.LabelTitle = Properties.Resources.menuitemTemplate;
                command.ToolTipTitle = Properties.Resources.menuitemTemplate;
                return command;
            }
        }

        public static RibbonCommand NewTemplate
        {
            get
            {
                RibbonCommand command = (RibbonCommand)Application.Current.Resources["rNewTemplate"];
                command.LabelTitle = Properties.Resources.menuitemNewTemplate;
                command.ToolTipTitle = Properties.Resources.menuitemNewTemplate;
                command.ToolTipDescription = Properties.Resources.DescNewTemplate;
                return command;
            }
        }

        public static RibbonCommand EditTemplate
        {
            get
            {
                RibbonCommand command = (RibbonCommand)Application.Current.Resources["rEditTemplate"];
                command.LabelTitle = Properties.Resources.menuitemEditTemplate;
                command.ToolTipTitle = Properties.Resources.menuitemEditTemplate;
                command.ToolTipDescription = Properties.Resources.DescEditTemplate;
                return command;
            }
        }

        public static RibbonCommand ReportGroup
        {
            get
            {
                RibbonCommand command = (RibbonCommand)Application.Current.Resources["rReportGroup"];
                command.LabelTitle = Properties.Resources.menuitemReportGroup;
                command.ToolTipTitle = Properties.Resources.descReportGroup;
                return command;
            }
        }

        public static RibbonCommand ViewLogs
        {
            get
            {
                RibbonCommand command = (RibbonCommand)Application.Current.Resources["rViewLogs"];
                command.LabelTitle = Properties.Resources.menuitemViewLogs;
                command.ToolTipTitle = Properties.Resources.menuitemViewLogs;
                command.ToolTipDescription = Properties.Resources.descViewLogs;
                return command;
            }
        }

        public static RibbonCommand ViewReports
        {
            get
            {
                RibbonCommand command = (RibbonCommand)Application.Current.Resources["rViewReports"];
                command.LabelTitle = Properties.Resources.menuitemReports;
                command.ToolTipTitle = Properties.Resources.menuitemReports;
                command.ToolTipDescription = Properties.Resources.descViewReports;
                return command;
            }
        }

		#endregion
	}
}