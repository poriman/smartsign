﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

using System.Net;
using System.Net.Sockets;
using System.Threading;

using NLog;


namespace DigitalSignage.Scheduler
{
    /// <summary>
    /// Interaction logic for NewDlgItem.xaml
    /// </summary>
    public partial class LoginDlg : Window
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        public bool result = false;
        private Config cfg = null;
        public String login;
        public String password;

        public LoginDlg(Config config, String user )
        {
            InitializeComponent();
            cfg = config;
            userLogin.Text = user;
            userPassword.Focus();

            //this.userPassword.Password = "dlsxpffldks.";//by_JunYoung_Touchflow : 테스트를 위해 입력.
        }

        private void saveData()
        {
            login = userLogin.Text;
            password = userPassword.Password;
            result = true;
        }


		private void btnOK_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                saveData();
                this.Close();
            }
            catch (Exception err)
            {
                logger.Error(err + "");
            }
        }

		private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

		void tb_GotKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
		{
			try
			{
				TextBox tb = sender as TextBox;
				if (tb != null) tb.SelectAll();
				else
				{
					PasswordBox pb = sender as PasswordBox;
					pb.SelectAll();
				}

				e.Handled = true;
			}
			catch { }
		}
    }
}
