﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigitalSignage.Common;

namespace DigitalSignage.Scheduler
{
    public class ErrorCmd
    {
        static public string GetErrorMsg(int icode)
        {
            string strError = "";
			
            switch (icode)
            {
				case 0:
					strError = "N/A";
					break;
				case (int)ErrorCodes.IS_ERROR_INVALID_PSR_FILE_NAME:
                    strError = "파일 이름 오류";
                    break;
                case (int)ErrorCodes.IS_ERROR_INVALID_PSR_OBJECT_NAME:
                    strError = "객체 명 오류";
                    break;
                case (int)ErrorCodes.IS_ERROR_FATAL:
                    strError = "치명적 오류";
                    break;
                case (int)ErrorCodes.IS_ERROR_SYSTEM_ERROR:
                    strError = "시스템 오류";
                    break;
                case (int)ErrorCodes.IS_ERROR_INTERNAL:
                    strError = "내부 처리 오류";
                    break;
                case (int)ErrorCodes.IS_ERROR_OUT_OF_MEMORY:
                    strError = "메모리 생성 오류";
                    break;
                case (int)ErrorCodes.IS_ERROR_UNSUPPT_OSVERSION:
                    strError = "버전 실패";
                    break;
                case (int)ErrorCodes.IS_ERROR_VERSION_NOT_FOUND:
                    strError = "버전 조회 실패";
                    break;
                case (int)ErrorCodes.IS_ERROR_UNDEFINED:
                    strError = "정의 되지않는 요청";
                    break;
                case (int)ErrorCodes.IS_ERROR_ELEMENT_NOT_FOUND:
                    strError = "다운로드 엘리먼트 찾기 실패";
                    break;
                case (int)ErrorCodes.IS_ERROR_PATH_NOT_FOUND:
                    strError = "존재하지 않는 경로";
                    break;
                case (int)ErrorCodes.IS_ERROR_FILE_NOT_FOUND:
                    strError = "존재 하지 않는 파일";
                    break;
                case (int)ErrorCodes.IS_ERROR_CONTENT_NOT_FOUND:
                    strError = "존재 하지 않는 컨텐츠";
                    break;
                case (int)ErrorCodes.IS_ERROR_UNSUPPT_CONTENT:
                    strError = "지원 하지 않는 컨텐츠";
                    break;
                case (int)ErrorCodes.IS_ERROR_CONTENT_CRASHED:
                    strError = "손상된 컨텐츠";
                    break;
                case (int)ErrorCodes.IS_ERROR_INVALID_PROGRAM_ID:
                    strError = "프로그램 아이디 오류";
                    break;
                case (int)ErrorCodes.IS_ERROR_DATA_NOT_FOUND:
                    strError = "테이터 찾기 오류";
                    break;
                case (int)ErrorCodes.IS_ERROR_CODEC_NOT_FOUND:
                    strError = "코덱 찾기 실패";
                    break;
                case (int)ErrorCodes.IS_ERROR_CONTENT_EXIST:
                    strError = "존재 하지 않는 컨텐츠 다운로드 요청";
                    break;
                case (int)ErrorCodes.IS_ERROR_SCHE_NOT_FOUND:
                    strError = "스케쥴 파일 찾기 실패";
                    break;
                case (int)ErrorCodes.IS_ERROR_INVALID_SCHE_NAME:
                    strError = "스케쥴 파일명 오류";
                    break;
                case (int)ErrorCodes.IS_ERROR_DOWNELEMENT_NOT_FOUND:
                    strError = "다운로드 항목 찾기 실패";
                    break;
                case (int)ErrorCodes.IS_ERROR_CONTENT_NOT_INSERVER:
                    strError = "서버에 존재하지 않는 컨텐츠 요청";
                    break;
                case (int)ErrorCodes.IS_ERROR_UNSUPPT_COMMAND:
                    strError = "지원 하지않는 명령 실행";
                    break;
                case (int)ErrorCodes.IS_ERROR_THREAD_ALREADY_RUN:
                    strError = "쓰레드 생성중";
                    break;
                case (int)ErrorCodes.IS_ERROR_DOC_ALREADY_OPEN:
                    strError = "쓰레드 실행중";
                    break;
                case (int)ErrorCodes.IS_ERROR_TIMEOUT:
                    strError = "명령 실행에 대한 타임 아웃";
                    break;
                case (int)ErrorCodes.IS_ERROR_PROCESS_ALREADY_RUN:
                    strError = "실행중인 프로세스 재실행";
                    break;
                case (int)ErrorCodes.IS_ERROR_TERMINATION:
                    strError = "프로세스 종료중";
                    break;
                case (int)ErrorCodes.IS_ERROR_COMMAND_UNDONE:
                    strError = "요청된 명령실행 미완료";
                    break;
                case (int)ErrorCodes.IS_ERROR_CODE_NOT_DEFINED:
                    strError = "정의 되지 않는 코드값 수신";
                    break;
                case (int)ErrorCodes.IS_ERROR_INVALID_PLAYLIST:
                    strError = "실행 할수 없는 task 실행 요청";
                    break;
                case (int)ErrorCodes.IS_ERROR_CONNECT_FAIL:
                    strError = "서버 연결 실패";
                    break;
                case (int)ErrorCodes.IS_ERROR_LINE_DISCONNECT:
                    strError = "네트워크 끊김";
                    break;
                case (int)ErrorCodes.IS_ERROR_SERVER_NOT_FOUND:
                    strError = "서버 아이피 또는 URL오류";
                    break;
                case (int)ErrorCodes.IS_ERROR_NO_MSG_DEFINE:
                    strError = "알수 없는 서버 수신 메시지";
                    break;
                case (int)ErrorCodes.IS_ERROR_CONN_SESSION_FAIL:
                    strError = "서버 세션연결 실패 또는 연결 끊김";
                    break;
                case (int)ErrorCodes.IS_ERROR_CONNECT_UNINITIAL:
                    strError = "서버연결 초기화 하지 않은 상태에서 통신실행";
                    break;
                case (int)ErrorCodes.IS_ERROR_INVALID_AUTHORIZATION:
                    strError = "서버 인증 실패";
                    break;
                case (int)ErrorCodes.IS_ERROR_INVALID_SERVICE:
                    strError = "알수없는 서비스 요청";
                    break;
                case (int)ErrorCodes.IS_ERROR_LOGIN_FAIL:
                    strError = "서버 로그인 실패";
                    break;
                case (int)ErrorCodes.IS_ERROR_INVALID_DST_PATH:
                    strError = "적용 패스 지정오류";
                    break;
                case (int)ErrorCodes.IS_ERROR_INVALID_DST_FILE:
                    strError = "적용 파일 지정 오류";
                    break;
                case (int)ErrorCodes.IS_STBLOG_STOP_BY_USER:
                    strError = "사용자 메뉴에 의한 프로그램 종료";
                    break;
                case (int)ErrorCodes.IS_STBLOG_STOP_BY_UNKNOWN_COMMAND:
                    strError = "알수 없는 명령으로 프로그램 종료";
                    break;
                case (int)ErrorCodes.IS_STBLOG_STOP_ABNORMAL:
                    strError = "비정상적인 서브프로세스 종료";
                    break;
                case (int)ErrorCodes.IS_STBLOG_STOP_BY_AGENT:
                    strError = "응답이 없는 서브프로세스를 에이전트가 재시작 시킴";
                    break;
				case (int)ErrorCodes.IS_ERROR_CONN_DISCONNECTED:
					strError = "플레이어에 의한 서버와 연결 종료";
					break;
				case (int)ErrorCodes.IS_ERROR_CONN_DUPLICATED_PLAYER:
					strError = "중복된 플레이어 ID로 접속됨";
					break;
				case (int)ErrorCodes.IS_ERROR_CONN_EXCESS_OF_ALLOWED_MAX_PLAYER:
					strError = "접속 가능한 플레이어 개수를 초과하여 접속이 제한됨";
					break;
				case (int)ErrorCodes.IS_ERROR_CONN_NO_PLAYER:
					strError = "플레이어 ID가 일치하지 않음";
					break;
				case (int)ErrorCodes.IS_ERROR_CONN_TIMEOUT_DISCONNECTED_BY_SERVER:
					strError = "플레이어가 응답이 없어 서버에 의해 접속 끊김";
					break;
				default:
					strError = "알 수 없는 에러";
					break;
            }

            return strError;
        }

    }
}
