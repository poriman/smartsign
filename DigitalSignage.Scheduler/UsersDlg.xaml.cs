﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.ComponentModel;
using System.Collections;
using NLog;
using System.Data;
using System.Xml;
using DigitalSignage.Common;


namespace DigitalSignage.Scheduler
{
    /// <summary>
    /// Interaction logic for NewDlgItem.xaml
    /// </summary>
    public partial class UsersDlg : Window
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private Config cfg = null;
        GridViewColumnHeader _lastHeaderClicked = null;
        ListSortDirection _lastDirection = ListSortDirection.Ascending;

        public UsersDlg(Config config)
        {
            InitializeComponent();
            
            cfg = config;
            loadData();
            usersList.ContextMenu = new ContextMenu();
            usersList.MouseRightButtonUp += new MouseButtonEventHandler(usersList_MouseRightButtonUp);
        }
        void GridViewColumnHeaderClickedHandler(object sender, RoutedEventArgs e)
        {

            GridViewColumnHeader headerClicked = e.OriginalSource as GridViewColumnHeader;
            ListSortDirection direction;

            if (headerClicked.Role != GridViewColumnHeaderRole.Padding)
            {
                if (headerClicked != _lastHeaderClicked)
                {
                    direction = ListSortDirection.Ascending;
                }
                else
                {
                    if (_lastDirection == ListSortDirection.Ascending)
                    {
                        direction = ListSortDirection.Descending;
                    }
                    else
                    {
                        direction = ListSortDirection.Ascending;
                    }
                }
                Sort(GetBindingPath(headerClicked), direction, (sender as ListView).Items);
                _lastHeaderClicked = headerClicked;
                _lastDirection = direction;
            }
            
        }
        public string GetBindingPath(GridViewColumnHeader Target)
        {
            string header = string.Empty;
            if (Target.Column.Header.ToString() == "사용자 ID")
                header= "ID";
            else if (Target.Column.Header.ToString() == "역할 이름")
                header= "Role";
            return header;
            //Column에서 직접 바인딩할 경우 아래 소스 사용(현 템플릿 TextBlock사용)
            //if (Target == null || Target.Column == null || Target.Column.DisplayMemberBinding == null) return null;

            //GridViewColumn GridViewColumn = Target.Column;
            //if (GridViewColumn.DisplayMemberBinding == null) return null;

            //Binding Binding = Target.Column.DisplayMemberBinding as Binding;
            //return Binding.Path.Path.ToString();
        }
        private void Sort(string sortBy, ListSortDirection direction, object ItemsSource)
        {
            ICollectionView dataView = CollectionViewSource.GetDefaultView(ItemsSource);
            dataView.SortDescriptions.Clear();
            SortDescription sd = new SortDescription("Role", direction);
            dataView.SortDescriptions.Add(sd);
            dataView.Refresh();
        }


        void usersList_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            ContextMenu menu = usersList.ContextMenu;

            menu.Items.Clear();

            MenuItem mi = null;

            //	추가 
            mi = new MenuItem();
            mi.Header = Properties.Resources.btnAdd;
            mi.IsEnabled = true;
            mi.Click += new RoutedEventHandler(btnClickAdd);
            menu.Items.Add(mi);

            if (usersList.SelectedItem != null)
            {
                XmlNode row = ((ListViewItem)usersList.SelectedItem).DataContext as XmlNode;

                //	수정 
                mi = new MenuItem();
                mi.Header = Properties.Resources.btnEdit;
                mi.IsEnabled = true;
                mi.Click += new RoutedEventHandler(btnClickEdit);
                menu.Items.Add(mi);

                //	삭제 
                mi = new MenuItem();
                mi.Header = Properties.Resources.btnDelete;
                mi.IsEnabled = true;
                mi.Click += new RoutedEventHandler(btnClickDelete);
                menu.Items.Add(mi);

                menu.Items.Add(new Separator());

                //	활성 여부 
                bool is_enabled = row.Attributes["is_enabled"].Value.Equals("Y");

                mi = new MenuItem();
                mi.Header = Properties.Resources.labelUserEnabled;
                mi.IsEnabled = true;

                MenuItem enabled = new MenuItem();
                enabled.Header = Properties.Resources.labelEnabled;
                enabled.IsChecked = is_enabled;
                enabled.Click += new RoutedEventHandler(btnClickEnabled);
                mi.Items.Add(enabled);

                MenuItem disabled = new MenuItem();
                disabled.Header = Properties.Resources.labelDisabled;
                disabled.IsChecked = !is_enabled;
                disabled.Click += new RoutedEventHandler(btnClickDisabled);
                mi.Items.Add(disabled);

                menu.Items.Add(mi);

            }

            menu.UpdateLayout();
            menu.IsOpen = true;

            e.Handled = true;
        }

        private void loadData()
        {
            try
            {
				usersList.Items.Clear();
                ServerDataCache sdc = ServerDataCache.GetInstance;
                sdc.RefreshUserTreeInfo();

                btnAdd.IsEnabled = sdc.HasPermission("CREATE USER");
                btnEdit.IsEnabled = sdc.HasPermission("UPDATE USER");
                btnDelete.IsEnabled = sdc.HasPermission("DELETE USER");
                XmlNodeList list = sdc.GetUserList();

                foreach (XmlNode node in list)
                {
                    UserListViewItem item = new UserListViewItem();
                    if (node.Attributes["is_enabled"].Value.Equals("N"))
                        continue;
                    string item_name = node.Attributes["name"].Value;
                    string item_enabled = node.Attributes["is_enabled"].Value.Equals("Y") ? Properties.Resources.labelEnabled : Properties.Resources.labelDisabled;
                    string item_roles = "";

                    String sHeader = node.Attributes["name"].Value;
                    string roles = "";

                    if (!node.Attributes["is_enabled"].Value.Equals("Y"))
                    {
                        item.Foreground = Brushes.DarkGray;
                    }
                    try
                    {

                        foreach (XmlNode rolenode in node.ChildNodes)
                        {

                            if (rolenode.LocalName.Equals("role") && rolenode.Attributes["is_enabled"].Value == "Y")
                            {
                                roles += String.Format("({0})", rolenode.Attributes["role_name"].Value);
                                item_roles += String.Format("{0}, ", rolenode.Attributes["role_name"].Value);
                            }
                        }
                    }
                    catch { }

                    #region Smart ToolTip

                    SmartTooltip tooltip = new SmartTooltip();
                    tooltip.BorderBrush = Brushes.Black;
                    tooltip.BorderThickness = new Thickness(1);
                    tooltip.Background = Application.Current.Resources["ToolTipBackgroundBrush"] as Brush;
                    tooltip.Foreground = Application.Current.Resources["ToolTipForegroundBrush"] as Brush;
                    tooltip.ToolTipText = String.Format(Properties.Resources.tooltipUserItem, item_name, item_roles.TrimEnd(new char[2] { ',', ' ' }), item_enabled);
                    #endregion;
                    item.DataContext = node;
                    item.ID = sHeader;
                    item.Role = roles;
                    item.Content = new UserListDataSource() { ID = sHeader, Role = roles };

                    item.ToolTip = tooltip;
                    usersList.Items.Add(item);
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
        }

  

        #region Event handlers
        private void btnClickClose(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnClickEdit(object sender, RoutedEventArgs e)
        {
            try
            {
                if (usersList.SelectedItem == null)
                {
                    MessageBox.Show(Properties.Resources.mbSelectUser);
                    return;	//선택해 주세요 추가해야함
                }

                long user_id = Convert.ToInt64(ServerDataCache.GetInstance.CurrentUser.Attributes["id"].Value);

                EditUsers dlg = new EditUsers(cfg, (XmlNode)((ListViewItem)usersList.SelectedItem).DataContext, user_id, true);
                dlg.Owner = this;
                dlg.ShowDialog();

                loadData();
            }
            catch (Exception err)
            {
                logger.Error(err + "");
            }
        }

        private void btnClickDelete(object sender, RoutedEventArgs e)
        {
            try
            {

                if (usersList.SelectedItem == null)
                {
                    MessageBox.Show(Properties.Resources.mbSelectUser);
                    return;	//선택해 주세요 추가해야함
                }
                XmlNode row = ((ListViewItem)usersList.SelectedItem).DataContext as XmlNode;

                if (row.Equals(ServerDataCache.GetInstance.CurrentUser)) // 자신은 삭제 불가능하다.
                {
                    MessageBox.Show(Properties.Resources.mbErrorDeleteAdmin);
                    return;
                }

                MessageBoxResult ret = MessageBox.Show(String.Format(Properties.Resources.mbDeleteUser, row.Attributes["name"].Value), Properties.Resources.titleMessageBox, MessageBoxButton.YesNo);

                if (ret == MessageBoxResult.Yes)
                {
                    if (cfg.ServerUsersList.DeleteUser(Convert.ToInt64(row.Attributes["id"].Value)))
                    {
                        try
                        {
                            ///	사용자 삭제 기록
                            cfg.ServerLogList.InsertDetailLog(ServerDataCache.GetInstance.Connected_UserName, (int)(LogType.LogType_Managing | LogType.LogType_Proceed), "", "USER_DELETE", row.Attributes["name"].Value, 0, 0, TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()));
                        }
                        catch { }
                    }
                    else
                    {
                        try
                        {
                            ///	사용자 삭제 기록
                            cfg.ServerLogList.InsertDetailLog(ServerDataCache.GetInstance.Connected_UserName, (int)(LogType.LogType_Managing | LogType.LogType_Failed), "", "USER_DELETE", row.Attributes["name"].Value, 0, 0, TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()));
                        }
                        catch { }
                    }
                    loadData();
                }
            }
            catch (Exception err)
            {
                logger.Error(err + "");
            }
        }

        private void btnClickAdd(object sender, RoutedEventArgs e)
        {
            try
            {
                long user_id = Convert.ToInt64(ServerDataCache.GetInstance.CurrentUser.Attributes["id"].Value);

                EditUsers dlg = new EditUsers(cfg, null, user_id, false);
                dlg.Owner = this;
                dlg.ShowDialog();
                loadData();
            }
            catch (Exception err)
            {
                logger.Error(err + "");
            }
        }
        private void btnClickDisabled(object sender, RoutedEventArgs e)
        {
            try
            {
                if (usersList.SelectedItem == null)
                {
                    MessageBox.Show(Properties.Resources.mbSelectUser);
                    return;	//선택해 주세요 추가해야함
                }
                XmlNode row = ((ListViewItem)usersList.SelectedItem).DataContext as XmlNode;

                if (row.Equals(ServerDataCache.GetInstance.CurrentUser)) // 자신은 삭제 불가능하다.
                {
                    MessageBox.Show(Properties.Resources.mbErrorModifyStatus);
                    return;
                }

                bool bEnabled = row.Attributes["is_enabled"].Value.Equals("Y");

                if (!bEnabled) return;

                long user_id = Convert.ToInt64(row.Attributes["id"].Value);

                if (cfg.ServerUsersList.ModifyEnabled(user_id, false))
                {
                    loadData();
                }
            }
            catch (Exception err)
            {
                logger.Error(err + "");
            }
        }

        private void btnClickEnabled(object sender, RoutedEventArgs e)
        {
            try
            {
                if (usersList.SelectedItem == null)
                {
                    MessageBox.Show(Properties.Resources.mbSelectUser);
                    return;	//선택해 주세요 추가해야함
                }
                XmlNode row = ((ListViewItem)usersList.SelectedItem).DataContext as XmlNode;

                if (row.Equals(ServerDataCache.GetInstance.CurrentUser)) // 자신은 삭제 불가능하다.
                {
                    MessageBox.Show(Properties.Resources.mbErrorModifyStatus);
                    return;
                
				}
				bool bEnabled = row.Attributes["is_enabled"].Value.Equals("Y");

				if (bEnabled) return;

				long user_id = Convert.ToInt64(row.Attributes["id"].Value);
				
				if(cfg.ServerUsersList.ModifyEnabled(user_id, true))
				{
					loadData();
				}
			}
			catch (Exception err)
			{
				logger.Error(err + "");
			}
		}
        #endregion
    }

    public class UserListViewItem : ListViewItem
    {
        string strID, strRole;

        public string ID
        {
            set { strID = value; }
            get { return strID; }
        }

        public string Role
        {
            set { strRole = value; }
            get { return strRole; }
        }
    }
    
    public class UserListDataSource 
    {
        string strID, strRole;

        public string ID
        {
            set { strID = value; }
            get { return strID; }
        }

        public string Role
        {
            set { strRole = value; }
            get { return strRole; }
        }
    }
}