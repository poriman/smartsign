﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Xml;
using System.Windows.Media.Imaging;

namespace DigitalSignage.Scheduler
{
    public class program
    {
        BitmapSource thumbnail;
        TimeSpan time;
        String id;
        string filePath;
        XmlNode Node;

        public XmlNode NODE
        {
            set { Node = value; }
            get { return Node; }
        }
        public String ID
        {
            set { id = value; }
            get { return id; }
        }
        public BitmapSource ThumbNail
        {
            set { thumbnail = value; }
            get { return thumbnail; }
        }
        public TimeSpan Time
        {
            set { time = value; }
            get { return time; }
        }
        public string strTime
        {
            get { return time.ToString(); }
        }
        public string FilePath
        {
            set{filePath = value;}
            get{return filePath;}
        }

        #region 광고 관련

        public bool IsAd { get; set; }
        public int PlayConstraint { get; set; }
        public DateTime AdStartTime { get; set; }
        public DateTime AdEndTime { get; set; }
        public String MetaTag { get; set; }

        #endregion
    }

    public class PlayListArry : ObservableCollection<program>
    {
        public PlayListArry() : base() { }
    }

    class PlaylistLoader
    {
        public PlayListArry Load(XmlNode mainnode)
        {
            try
            {
            }
            catch { }
            return null;
        }
    }
}
