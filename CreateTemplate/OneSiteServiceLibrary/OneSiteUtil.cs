﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Configuration;
using System.Xml;

namespace MenuBoardService
{
    public class OneSiteUtil
    {

        private const string MOVIE = "03";
        private const string IMAGE = "05";
        private const string FLASH = "02";
        private const string PPT = "10";
        public const string ERROR = "ERROR";
        public const string MESSAGE = "MESSAGE";
        public const string CONFIG_DIR = "CONFIG_DIR";
        public const string UPDATE_DIR = "UPDATE_DIR";

        public const string XML_DIRECTORY = "XML_DIRECTORY";
        public const string TEMP_DIRECTORY = "TEMP_DIRECTORY";
        public const string SCHEDULE_DIRECTORY = "SCHEDULE_DIRECTORY";
        public const string CONTENT_DIRECTORY = "CONTENT_DIRECTORY";
        public const string THUMBNAIL_DIRECTORY = "THUMBNAIL_DIRECTORY";
        public const string SYNC_DIRECTORY = "SYNC_DIRECTORY";


        public const string DISPLAY_WIDTH = "DISPLAY_WIDTH";
        public const string DISPLAY_HEIGHT = "DISPLAY_HEIGTH";
        public const string CONTENT_XML_FILENAME = "CONTENT_XML_FILENAME";
        public const string PROGRAM_XML_FILENAME = "PROGRAM_XML_FILENAME";
        public const string DATA_XML_FILENAME = "DATA_XML_FILENAME";
        public const string TEMPLATE_XML_FILENAME = "TEMPLATE_XML_FILENAME";
        public const string MENU_XML_FILENAME = "MENU_XML_FILENAME";
        public const string PLAYLIST_XML_FILENAME = "PLAYLIST_XML_FILENAME";
        public const string SUBTITLE_XML_FILENAME = "SUBTITLE_XML_FILENAME";
        public const string DEFAULT_PLAYLIST_XML_FILENAME = "DEFAULT_PLAYLIST_XML_FILENAME";
        public const string TIME_PLAYLIST_XML_FILENAME = "TIME_PLAYLIST_XML_FILENAME";
        public const string WEBSERVICE_REFDATA_LIST = "webservice_refdata_list";
        public const string Perfix_thumanil = "TH_";

        public const string Perfix_Program_thumanil = "TH_P_";


        public const string LOGSDIRECTORY = "LOGSDIRECTORY";
        public const string DEBUG = "DEBUG";

        public static string getFileType(string filename)
        {
            string[] images = new string[] { ".PCX", ".PNG", ".PSP", ".TIF", ".TIFF", ".WMF", ".JPG", ".GIF", ".BMP", ".JIF", ".JPEG", ".LWF" };

            string[] avi = new string[] { ".MOV", ".MPEG", ".MPG", ".WMA", ".WMV", ".ASF", ".AVI" };

            string ppt = ".PPT";

            string flash = ".SWF";

            string fileExtension = Path.GetExtension(filename).ToUpper();
            
            if (ContainsInArray(fileExtension, images))
            {
                return IMAGE;
            }
            else if (ContainsInArray(fileExtension, avi))
            {
                return MOVIE;
            }
            else if (fileExtension.Equals(flash))
            {
                return FLASH;
            }
            else if (fileExtension.Equals(ppt))
            {
                return PPT;
            }
            else
            {
                return fileExtension;
            }
        }

        public static string getDBXml(string source)
        {

            if (source.Equals("playlist"))
            {
                return Path.Combine(getDirectoryAppConfig(XML_DIRECTORY), getAppConfig(PLAYLIST_XML_FILENAME));
            }
            else if (source.Equals("program_list"))
            {
                return Path.Combine(getDirectoryAppConfig(XML_DIRECTORY), getAppConfig(PROGRAM_XML_FILENAME));
            }
            else if (source.Equals("data_list"))
            {
                return Path.Combine(getDirectoryAppConfig(XML_DIRECTORY), getAppConfig(DATA_XML_FILENAME));
            }
            else if (source.Equals("template_list"))
            {
                return Path.Combine(getDirectoryAppConfig(XML_DIRECTORY), getAppConfig(TEMPLATE_XML_FILENAME));
            }
            else if (source.Equals("menu_list"))
            {
                return Path.Combine(getDirectoryAppConfig(XML_DIRECTORY), getAppConfig(MENU_XML_FILENAME));
            }
            else if (source.Equals("contents_list"))
            {
                return Path.Combine(getDirectoryAppConfig(XML_DIRECTORY), getAppConfig(CONTENT_XML_FILENAME));
            }
            else if (source.Equals("subtitles"))
            {
                return Path.Combine(getDirectoryAppConfig(XML_DIRECTORY), getAppConfig(SUBTITLE_XML_FILENAME));
            }
            else
            {
                throw new System.IO.FileNotFoundException("해당 파일을 Config 정보가 정의 되지 않았습니다.", source);
            }
        }


        private static bool ContainsInArray(string fileExtension, Array array)
        {
            bool _result = false;

            for (int i = 0; i < array.Length; i++)
            {
                if (array.GetValue(i).Equals(fileExtension))
                {
                    _result = true;
                    break;
                }
            }

            return _result;
        }


        /// <summary>
        /// App.config에 세팅된 로그 관련 Directory 값을 가져 온다.
        /// </summary>
        /// <param name="logType"></param>
        /// <returns>string</returns>
        public static string getLogsDirectory(string logType)
        {

            //로그 파일의 위치는 Dll을 참조하는 App.config에  
            //  <appSettings>
            //      <add key="Logs" value="logs"/>
            //  </appSettings>
            //해당 정보를 읽어서 로그 Directory를 생성 하고 하위에 날짜 별로 기록을 하도록 한다.
            string filename = AppDomain.CurrentDomain.BaseDirectory + ConfigurationManager.AppSettings.Get(LOGSDIRECTORY);

            if (!Directory.Exists(filename))
            {
                Directory.CreateDirectory(filename);
            }

            filename = Path.Combine(filename , logType + "-" + DateTime.Now.ToString("yyyy-MM-dd") + ".XML");

            return filename;
        }


        /// <summary>
        /// directory 관련 세팅 정보를 가져 온다.
        /// </summary>
        /// <returns></returns>
        public static string getDirectoryAppConfig(string key)
        {

            //로그 파일의 위치는 Dll을 참조하는 App.config에  
            //  <appSettings>
            //      <add key="Xml" value="XmlData"/>
            //  </appSettings>
            //해당 정보를 읽어서 로그 Directory를 생성 하고 하위에 날짜 별로 기록을 하도록 한다.
            string filename = AppDomain.CurrentDomain.BaseDirectory + ConfigurationManager.AppSettings.Get(key);

            if (!Directory.Exists(filename))
            {
                Directory.CreateDirectory(filename);
            }

            return filename;
        }

        
        /// <summary>
        /// Application 세팅 관련 정보를 가져 온다
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string getAppConfig(string key)
        {

            //로그 파일의 위치는 Dll을 참조하는 App.config에  
            //  <appSettings>
            //      <add key="DisplayWidth" value="1024"/>
            //  </appSettings>
            //해당 정보를 읽어서 DisplayWidth 정보를 가져 온다.
            string _key = ConfigurationManager.AppSettings.Get(key);

            return _key;
        }

        public static string getMediainfo(string key)
        {
            try
            {
                if (File.Exists(Path.Combine(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, OneSiteUtil.getAppConfig(OneSiteUtil.CONFIG_DIR)), "MediaInfo.xml")))
                {

                    XmlDocument _doc = new XmlDocument();

                    _doc.Load(Path.Combine(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, OneSiteUtil.getAppConfig(OneSiteUtil.CONFIG_DIR)), "MediaInfo.xml"));

                    XmlNode mediainfo1 = _doc.SelectSingleNode("//media-machine");

                    return mediainfo1.Attributes[key].Value;
                }
                else
                {
                    return "";
                }
            }
            catch (Exception e)
            {
                OneSiteLogsWriter.write(e);
                return "";
            }
        }

        public static XmlNode getMediaNode()
        {
            XmlDocument _doc = new XmlDocument();

            _doc.Load(Path.Combine(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, OneSiteUtil.getAppConfig(OneSiteUtil.CONFIG_DIR)), "MediaInfo.xml"));

            return _doc.SelectSingleNode("//media-info");
        }

        public static string getExternalDataFileName()
        {
            try
            {
                if (File.Exists(Path.Combine(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, OneSiteUtil.getAppConfig(OneSiteUtil.CONFIG_DIR)), "MediaInfo.xml")))
                {

                    XmlDocument _doc = new XmlDocument();

                    _doc.Load(Path.Combine(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, OneSiteUtil.getAppConfig(OneSiteUtil.CONFIG_DIR)), "MediaInfo.xml"));

                    XmlNode mediainfo1 = _doc.SelectSingleNode("//media-file");
                    XmlNodeList nodeList = mediainfo1.ChildNodes;
                    for(int i = 0; i < nodeList.Count;i++)
                    {
                        XmlNode node = nodeList.Item(i);
                        if (node.Attributes["name"].Value.Equals("external_data"))
                        {
                            return node.InnerText;
                        }
                    }
                    return "";
                }
                else
                {
                    return "";
                }
            }
            catch (Exception e)
            {
                OneSiteLogsWriter.write(e);
                return "";
            }
        }
    }

    public static class OneSiteMessageStore
    {
        private static string message = "";

        private static string nodeId = "";

        public static void SetNodeId(string value)
        {
            nodeId = value;
        }

        public static string GetNodeId()
        {
            return nodeId;
        }

        public static void SetMessage(string msg)
        {
            message = msg;

            //OneSiteLogsWriter.write(msg);
        }

        public static string GetMessage()
        {
            return message;
        }
    }
}
