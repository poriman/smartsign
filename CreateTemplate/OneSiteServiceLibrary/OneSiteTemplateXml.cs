﻿using System;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Collections;
using System.Text;
using Wilson.XmlDbClient;
using System.Data;
using System.Data.Common;

namespace MenuBoardService
{
    class OneSiteTemplateXml
    {
        public static string ErrorMessage = "";

        public static string template_id = "";

        //프로그램 등록 또는 프로그램 수정
        public static bool CreateOrUpdateTemplate(template_update _update)
        {
            //해당 프로그램을 Xml 파일에 Serialize 
            //string template_id = "";

            if (_update.job_type.Equals("DELETE"))
            {

                template_id = _update.template.id;

                //if (CheckTemplateUsing(template_id))
                //{
                //    ErrorMessage = "사용중인 프로그램은 삭제 할 수 없습니다.";
                //}
                //else
                {
                    //program_list에서 해당 프로그램을 삭제 한다.

                    if (!DeleteInTemplatelist(_update.template))
                    {
                        ErrorMessage = "해당 프로그램이 이미 삭제 되었거나, 다른 프로그램이 해당 자원을 사용하여 접근이 거부 되었습니다.";
                    }
                    else
                    {
                        return true;
                    }
                }

            }
            else
            {
                template_id = _update.template.id;

                if (CheckTemplateUsing(template_id))
                {
                    if (UpdateInTemplatelist(_update.template) && UpdateInTemplatelist(_update.template))
                    {
                        return true;
                    }
                }
                else
                {
                    if (UpdateInTemplatelist(_update.template))
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        //프로그램 사용여부 확인 
        public static bool CheckTemplateUsing(string template_id)
        {
            if (File.Exists(OneSiteUtil.getDBXml("template_list")))
            {

                XmlDbConnection xmldbconn = new XmlDbConnection();
                try
                {
                    xmldbconn.ConnectionString = OneSiteUtil.getDBXml("template_list");

                    XmlDbCommand xmldbcmd = new XmlDbCommand();
                    xmldbcmd.Connection = xmldbconn;

                    xmldbcmd.CommandText = "SELECT id from template where id='" + template_id + "'";

                    XmlDbDataAdapter da = new XmlDbDataAdapter(xmldbcmd);
                    DataSet ds_program = new DataSet();

                    da.Fill(ds_program);

                    if (ds_program.Tables[0].Rows.Count > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception e)
                {
                    OneSiteLogsWriter.write(e);
                }
                finally
                {
                    xmldbconn.Close();
                }

                return false;
            }
            else
            {
                return false;
            }
        }

        //template List에서 정보 수정
        public static bool UpdateInTemplatelist(template _template)
        {
            try
            {
                OneSiteXmlSerializer serializer = new OneSiteXmlSerializer(typeof(template_list));

                template_list _templatelists = list();


                int program_length = -1;

                //최초에 NULL 인 경우에 처리
                if (_templatelists == null)
                {
                    program_length = 0;
                    _templatelists = new template_list();
                    _templatelists.last_update_time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                }
                else if (_templatelists.template == null)
                {
                    program_length = 0;
                }
                else
                {
                    program_length = _templatelists.template.Length;
                }


                bool exists = false;

                for (int i = 0; i < program_length; i++)
                {
                    if (_templatelists.template[i].id.Equals(_template.id))
                    {
                        _templatelists.template[i] = _template;

                        exists = true;
                    }
                }

                if (!exists)
                {
                    //추가 작업 수행

                    template_list new_templatelists = new template_list();

                    new_templatelists.last_update_time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                    template[] new_program_array = new template[program_length + 1];

                    if (program_length > 0)
                    {
                        _templatelists.template.CopyTo(new_program_array, 0);
                    }

                    new_program_array[program_length] = _template;

                    new_templatelists.template = new_program_array;

                    //변경된 프로그램 목록을 기록한다.
                    serializer.OneSiteDirectSerialize(OneSiteUtil.getDBXml("template_list"), new_templatelists);
                }
                else
                {

                    //변경된 프로그램 목록을 기록한다.
                    serializer.OneSiteDirectSerialize(OneSiteUtil.getDBXml("template_list"), _templatelists);
                }

                return true;
            }
            catch (Exception e)
            {
                OneSiteLogsWriter.write(e);
                return false;
            }
        }

        //template List에서 프로그램 삭제
        public static bool DeleteInTemplatelist(template _template)
        {
            try
            {
                OneSiteXmlSerializer serializer = new OneSiteXmlSerializer(typeof(template_list));

                template_list _templatelists = list();

                if (_templatelists != null && _templatelists.template != null)
                {

                    bool exists = false;

                    for (int i = 0; i < _templatelists.template.Length; i++)
                    {
                        if (_templatelists.template[i].id.Equals(_template.id))
                        {
                            exists = true;
                        }
                    }

                    if (exists)
                    {
                        //추가 작업 수행

                        template_list new_templatelists = new template_list();

                        new_templatelists.last_update_time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                        template[] new_template_array = new template[_templatelists.template.Length - 1];

                        new_templatelists.template = new_template_array;

                        int new_index = 0;

                        for (int i = 0; i < _templatelists.template.Length; i++)
                        {
                            if (!_templatelists.template[i].id.Equals(_template.id))
                            {
                                new_template_array[new_index] = _templatelists.template[i];
                                new_index++;
                            }
                        }

                        new_templatelists.template = new_template_array;

                        //변경된 프로그램 목록을 기록한다.
                        serializer.OneSiteDirectSerialize(OneSiteUtil.getDBXml("template_list"), new_templatelists);

                        //프로그램 XML 파일 삭제
                        //File.Delete(Path.Combine(OneSiteUtil.getDirectoryAppConfig(OneSiteUtil.SCHEDULE_DIRECTORY), _template.id + ".xml"));

                        //프로그램 섬네일 파일 삭제
                       // File.Delete(Path.Combine(OneSiteUtil.getDirectoryAppConfig(OneSiteUtil.THUMBNAIL_DIRECTORY), OneSiteUtil.Perfix_Program_thumanil + _template.id + ".jpg"));

                        return true;
                    }
                    else
                    {
                        //Console.WriteLine("해당 프로그램이 리스트에 없습니다. ID={0}", _template.id);
                        return false;
                    }

                }
                return false;
            }
            catch (Exception e)
            {
                OneSiteLogsWriter.write(e);
                //Console.WriteLine("프로그램 리스트에서 프로그램 삭제 Error. ID={0}", _template.id);
                return false;
            }
        }

        public static template_list list()
        {
            try
            {
                OneSiteXmlSerializer serializer = new OneSiteXmlSerializer(typeof(template_list));

                template_list _templatelists = (template_list)serializer.OneSiteDirectDeSerialize(OneSiteUtil.getDBXml("template_list"));

                return _templatelists;
            }
            catch
            {
                return null;
            }
        }

        public static template_list list(string path)
        {
            try
            {
                OneSiteXmlSerializer serializer = new OneSiteXmlSerializer(typeof(template_list));

                template_list _templatelists = (template_list)serializer.OneSiteDirectDeSerialize(path);

                return _templatelists;
            }
            catch
            {
                return null;
            }
        }
    }
}
