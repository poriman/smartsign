﻿using System;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Collections;
using System.Text;
using Wilson.XmlDbClient;
using System.Data;
using System.Data.Common;
using System.Threading;

namespace MenuBoardService
{
    public class OneSitePlaylistXml
    {
        public static string ErrorMessage = "";

        public static bool CheckVaildTime(playlist _update_list)
        {
            if (_update_list.sch_type.Equals("T"))
            {
                bool ischeck = false;

                int istart_day = 0;

                int iend_day = 0;

                int iin_start_day = Int32.Parse(_update_list.start_day.Replace("-", ""));

                int iin_end_day = Int32.Parse(_update_list.end_day.Replace("-", "")); ;

                int istart_time = 0;

                int iend_time = 0;

                int iin_start_time = Int32.Parse(_update_list.start_time.Replace(":", ""));

                int iin_end_time = Int32.Parse(_update_list.end_time.Replace(":", ""));

                if (File.Exists(OneSiteUtil.getDBXml("playlist")))
                {

                    XmlDbConnection xmldbconn = new XmlDbConnection();

                    try
                    {

                        xmldbconn.ConnectionString = OneSiteUtil.getDBXml("playlist");

                        XmlDbCommand xmldbcmd = new XmlDbCommand();
                        xmldbcmd.Connection = xmldbconn;

                        if (_update_list.sch_id != null && _update_list.sch_id != "")
                        {
                            xmldbcmd.CommandText = "SELECT start_time,end_time,start_day, end_day from playlist where sch_type='T' and apply_status='Y' and sch_id <> '" + _update_list.sch_id + "'";
                        }
                        else
                        {
                            xmldbcmd.CommandText = "SELECT start_time,end_time,start_day, end_day from playlist where sch_type='T' and apply_status='Y'";
                        }
                        XmlDbDataAdapter da = new XmlDbDataAdapter(xmldbcmd);
                        DataSet ds_playlist = new DataSet();

                        da.Fill(ds_playlist);
                        ischeck = true;
                        xmldbconn.Close();

                        for (int i = 0; i < ds_playlist.Tables[0].Rows.Count; i++)
                        {
                            istart_time = Int32.Parse(ds_playlist.Tables[0].Rows[i]["start_time"].ToString().Replace(":", ""));

                            iend_time = Int32.Parse(ds_playlist.Tables[0].Rows[i]["end_time"].ToString().Replace(":", ""));

                            istart_day = Int32.Parse(ds_playlist.Tables[0].Rows[i]["start_day"].ToString().Replace("-", ""));

                            iend_day = Int32.Parse(ds_playlist.Tables[0].Rows[i]["end_day"].ToString().Replace("-", ""));

                            //추가되는 시간 지정이 슬롯이 작은 경우에는 켭치는 부분만 체크
                            if (istart_time < iin_start_time && iin_start_time < iend_time)
                            {
                                if (false == CheckVaildDay(iin_start_day, iin_end_day, istart_day, iend_day))
                                    return false;
                            }
                            else if (istart_time < iin_end_time && iin_end_time < iend_time)
                            {
                                if (false == CheckVaildDay(iin_start_day, iin_end_day, istart_day, iend_day))
                                    return false;
                            }
                            //추가되는 시간 지정이 슬롯이 큰은 경우에는 켭치는 부분만 체크
                            else if (iin_start_time < istart_time && istart_time < iin_end_time)
                            {
                                if (false == CheckVaildDay(iin_start_day, iin_end_day, istart_day, iend_day))
                                    return false;
                            }
                            else if (iin_start_time < iend_time && iend_time < iin_start_time)
                            {
                                if (false == CheckVaildDay(iin_start_day, iin_end_day, istart_day, iend_day))
                                    return false;
                            }
                        }

                        return true;
                    }
                    catch (Exception e)
                    {
                        OneSiteLogsWriter.write(e);
                    }
                    finally
                    {
                        if (ischeck == false)
                            xmldbconn.Close();
                    }

                    return false;

                }
                else
                {
                    return true;
                }
            }
            else
            {
                return true;
            }
        }

        private static bool CheckVaildDay(int iin_start_day, int iin_end_day, int istart_day, int iend_day)
        {            
            try
            {  
                //추가되는 시간 지정이 슬롯이 작은 경우에는 켭치는 부분만 체크
                if (istart_day < iin_start_day && iin_start_day < iend_day)
                {
                    return false;
                }
                else if (istart_day < iin_end_day && iin_end_day < iend_day)
                {
                    return false;
                }
                //추가되는 시간 지정이 슬롯이 큰은 경우에는 켭치는 부분만 체크
                else if (iin_start_day < istart_day && istart_day < iin_end_day)
                {
                    return false;
                }
                else if (iin_start_day < iend_day && iend_day < iin_start_day)
                {
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                OneSiteLogsWriter.write(e);
            }

            return false;
        }

        //스케줄 삭제 가능 여부 확인
        public static bool CheckDeletePossiable(playlist _playlist)
        {
            if (_playlist.sch_type.Equals("D"))
            {
                if (File.Exists(OneSiteUtil.getDBXml("playlist")))
                {

                    XmlDbConnection xmldbconn = new XmlDbConnection();

                    try
                    {

                        xmldbconn.ConnectionString = OneSiteUtil.getDBXml("playlist");

                        XmlDbCommand xmldbcmd = new XmlDbCommand();
                        xmldbcmd.Connection = xmldbconn;

                        xmldbcmd.CommandText = "SELECT sch_id from playlist where sch_type='D' and apply_status='Y'";

                        XmlDbDataAdapter da = new XmlDbDataAdapter(xmldbcmd);
                        DataSet ds_playlist = new DataSet();

                        da.Fill(ds_playlist);

                        //Default 스케줄이면서 적용의 스케줄 아이디를 찾아서 현재 삭제하려는 아이디와 동일한 경우는 삭제 할수 없음
                        for (int i = 0; i < ds_playlist.Tables[0].Rows.Count; i++)
                        {
                            if (ds_playlist.Tables[0].Rows[i]["sch_id"].ToString().Equals(_playlist.sch_id))
                            {
                                ErrorMessage = "적용중인 기본 재생 목록을 삭제 할 수 없습니다. ";
                                return false;
                            }
                        }

                        return true;
                    }
                    catch (Exception e)
                    {

                        OneSiteLogsWriter.write(e);
                    }
                    finally
                    {
                        xmldbconn.Close();
                    }

                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return true;
            }
        }

        //기본 스케줄 여부 확인
        public static bool IsDefaultSchedule(string  sch_id)
        {
            if (File.Exists(OneSiteUtil.getDBXml("playlist")))
            {

                XmlDbConnection xmldbconn = new XmlDbConnection();

                try
                {

                    xmldbconn.ConnectionString = OneSiteUtil.getDBXml("playlist");

                    XmlDbCommand xmldbcmd = new XmlDbCommand();
                    xmldbcmd.Connection = xmldbconn;

                    xmldbcmd.CommandText = "SELECT sch_id from playlist where sch_type='D' and apply_status='Y'";

                    XmlDbDataAdapter da = new XmlDbDataAdapter(xmldbcmd);
                    DataSet ds_playlist = new DataSet();

                    da.Fill(ds_playlist);

                    //Default 스케줄이면서 적용의 스케줄 아이디를 찾아서 현재 삭제하려는 아이디와 동일한 경우는 삭제 할수 없음
                    for (int i = 0; i < ds_playlist.Tables[0].Rows.Count; i++)
                    {
                        if (ds_playlist.Tables[0].Rows[i]["sch_id"].ToString().Equals(sch_id))
                        {                            
                            return true;
                        }
                    }

                }
                catch (Exception e)
                {
                    OneSiteLogsWriter.write(e);
                }
                finally
                {
                    xmldbconn.Close();
                }

                return false;
            }
            else
            {
                return false;
            }
        }

        public static playlists list()
        {
            try
            {
                OneSiteXmlSerializer serializer = new OneSiteXmlSerializer(typeof(playlists));

                playlists list = (playlists)serializer.OneSiteDirectDeSerialize(OneSiteUtil.getDBXml("playlist"));

                return list;
            }
            catch
            {
                return null;
            }
        }

        public static bool CreateOrUpdatePlaylist(playlist_update _playlist_update)
        {
            
            try
            {
                string sch_id = "";

                if (_playlist_update.job_type.Equals("DELETE"))
                {
                    //삭제의 경우 현재 적용 되어있는 Default Schedule은 삭제 하지 못한다.
                    if (CheckDeletePossiable(_playlist_update.playlist))
                    {
                       //삭제 가능한 것은 삭제를 하도록 한다.... 

                        if (DeleteSchedule(_playlist_update.playlist))
                        {
                            return true;
                        }
                        else
                        {

                            return false;
                        }
                    }
                }
                else
                {
                    if (_playlist_update.job_type.Equals("INSERT"))
                    {
                        sch_id = DateTime.Now.ToString("yyyyMMddHHmmssffff");
                        _playlist_update.playlist.sch_id = sch_id;
                    }
                    else
                    {
                        sch_id = _playlist_update.playlist.sch_id;
                    }

                    //수정 삭제의 경우에 시간이 겹치는 부분이 있는가 확인 한다.

                    if (!CheckVaildTime(_playlist_update.playlist))
                    {
                        ErrorMessage = "시간지정 재생: 목록중 시간이 중복 되는 부분이 있습니다.";
                        return false;
                    }
                    else
                    {
                        //재생목록 수정 수행
                        if (UpdateSchedule(_playlist_update.playlist))
                        {
                            return true;
                        }
                    }
                }

            }
            catch
            {
                ErrorMessage = "재생목록 수정/등록 작업이 실패하였습니다.";
            }

            return false;
        }

        public static bool DeleteSchedule(playlist _playlist)
        {
            //해당 목록에서 삭제후에 다시 
            try
            {

                OneSiteXmlSerializer serializer = new OneSiteXmlSerializer(typeof(playlists));

                playlists _list = list();

                if (_list != null)
                {

                    bool exists = false;

                    for (int i = 0; i < _list.playlist.Length; i++)
                    {
                        if (_list.playlist[i].sch_id.Equals(_playlist.sch_id))
                        {
                            exists = true;
                        }
                    }

                    if (exists)
                    {
                        //작업 수행

                        playlists new_list = new playlists();

                        new_list.last_update_time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                        playlist[] new_array = new playlist[_list.playlist.Length - 1];

                        new_list.playlist = new_array;

                        int new_index = 0;

                        for (int i = 0; i < _list.playlist.Length; i++)
                        {
                            if (!_list.playlist[i].sch_id.Equals(_playlist.sch_id))
                            {
                                new_list.playlist[new_index] = _list.playlist[i];
                                new_index++;
                            }
                        }

                        new_list.playlist = new_array;

                        //변경된 프로그램 목록을 기록한다.
                        string path = OneSiteUtil.getDBXml("playlist");
                        if (false == serializer.OneSiteDirectSerialize(path, new_list))
                            throw new System.IO.IOException();

                        return true;
                    }
                    else
                    {
                        ErrorMessage = "해당 재생 목록이 리스트에 없습니다.";
                        return false;
                    }
                }

                ErrorMessage = "재생 목록 리스트가 없습니다";
                return false;
            }
            catch(Exception e)
            {
                OneSiteLogsWriter.write(e);
            }

            return false;
        }

        public static bool UpdateSchedule(playlist _playlist)
        {
            try
            {
                OneSiteXmlSerializer serializer = new OneSiteXmlSerializer(typeof(playlists));

                playlists _list = list();

                bool exists = false;

                int apply_index = -1;

                int update_index = -1;

                int playlists_length = -1;

                //최초에 NULL 인 경우에 처리
                if (_list == null)
                {
                    playlists_length = 0;
                    _list = new playlists();
                    _list.last_update_time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                }
                else if (_list.playlist == null)
                {
                    playlists_length = 0;
                }
                else
                {
                    playlists_length = _list.playlist.Length;
                }

                for (int i = 0; i < playlists_length; i++)
                {
                    if (_list.playlist[i].sch_id.Equals(_playlist.sch_id))
                    {
                        //_list.playlist[i] = _playlist;

                        update_index = i;

                        exists = true;
                    }

                    if (_list.playlist[i].sch_type.Equals("D") && _list.playlist[i].apply_status.Equals("Y"))
                    {
                        apply_index = i;
                    }
                }

                if (!exists)
                {
                    //재생목록 추가 작업-- 신규 등록

                    playlists new_list = new playlists();

                    if (_playlist.sch_type.Equals("D") && _playlist.apply_status.Equals("Y"))
                    {
                        //새롭게 추가되는 재생 목록이 Default 스케줄로 등록 되어야 하므로 기존의 default 스케줄 적용 상태를 'N'으로 적용 한다.

                        if (apply_index > -1)
                        {
                            _list.playlist[apply_index].apply_status = "N";
                        }
                    }

                    new_list.last_update_time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                    playlist[] new_array = new playlist[playlists_length + 1];

                    if (playlists_length > 0)
                    {
                        _list.playlist.CopyTo(new_array, 0);
                    }

                    new_array[playlists_length] = _playlist;

                    new_list.playlist = new_array;

                    if (_playlist.sch_type.Equals("T"))
                    {
                        if (!CreatePlFile(_playlist))
                        {
                            return false;
                        }
                    }

                    string path = OneSiteUtil.getDBXml("playlist");
                    if (false == serializer.OneSiteDirectSerialize(path, new_list))
                        throw new System.IO.IOException();

                    return CreateTempFile(new_list);
                }
                else
                {
                    //재생목록 수정작업 수행

                    //수정 작업은 삭제후 다시 넣는 작업과 동일 


                    //일간 --> 시간으로 변경시 고려 사항
                    //해당 스케줄이 default 스케줄이 아니어야 한다. 그외의 경우에는 문제 없음
                    if (IsDefaultSchedule(_playlist.sch_id) && _playlist.sch_type.Equals("T"))
                    {
                        ErrorMessage = "기본 스케줄을 시간지정 스케줄로 변경 할 수 없습니다.";
                        return false;
                    }
                    else
                    {
                        //if (CheckDeletePossiable(_playlist))
                        //{
                            if (_playlist.sch_type.Equals("D") && _playlist.apply_status.Equals("Y"))
                            {
                                //새롭게 추가되는 재생 목록이 Default 스케줄로 등록 되어야 하므로 기존의 default 스케줄 적용 상태를 'N'으로 적용 한다.

                                if (apply_index > -1)
                                {
                                    _list.playlist[apply_index].apply_status = "N";
                                }
                            }

                            _list.playlist[update_index] = _playlist;

                            string path = OneSiteUtil.getDBXml("playlist");
                            if (false == serializer.OneSiteDirectSerialize(path, _list))
                                throw new System.IO.IOException();

                            //현재의 재생목록을 수정 했으면 temp 폴더로 복사해야 하는 파일을 생성 한다.

                            if (_playlist.sch_type.Equals("T"))
                            {
                                if (!CreatePlFile(_playlist))
                                {
                                    return false;
                                }
                            }

                            return CreateTempFile(_list);
                    }
                }
            }
            catch (Exception e)
            {
                OneSiteLogsWriter.write(e);
            }

            return false;
        }


        public static bool CreatePlFile(playlist _playlist)
        {
            try
            {
                playlists timeSchedulelist = new playlists();

                playlist timeSchedule = new playlist();

                timeSchedule.sch_id = _playlist.sch_id;
                timeSchedule.desc = _playlist.desc;
                timeSchedule.cnt = _playlist.cnt;
                timeSchedule.sch_type = _playlist.sch_type;
                timeSchedule.start_day = _playlist.start_day;
                timeSchedule.end_day = _playlist.end_day;
                timeSchedule.start_time = _playlist.start_time;
                timeSchedule.end_time = _playlist.end_time;
                timeSchedule.apply_status = _playlist.apply_status;
                timeSchedule.loop_yn = _playlist.loop_yn;
                timeSchedule.last_prg = _playlist.last_prg;
                timeSchedule.total_size = _playlist.total_size;
                timeSchedule.total_playtime = _playlist.total_playtime;

                if (_playlist.program != null)
                {
                    for (int k = 0; k < _playlist.program.Length; k++)
                    {
                        //<program id="200802201038272500" is_ad="N" play_time="24" ad_schedule_id="UNKNOWN" />
                        program _program = new program();

                        _program.id = _playlist.program[k].id;
                        _program.is_ad = "N";
                        _program.play_time = _playlist.program[k].play_time;
                        _program.ad_schedule_id = "UNKNOWN";

                        AddProgram(timeSchedule, _program);
                    }
                }

                playlist[] _timeScheduleArray = new playlist[1];

                _timeScheduleArray[0] = timeSchedule;

                timeSchedulelist.playlist = _timeScheduleArray;

                //해당 내용을 schedule 폴더에 기록 한다.

                OneSiteXmlSerializer time_serializer = new OneSiteXmlSerializer(typeof(playlists));
                string path = Path.Combine(OneSiteUtil.getDirectoryAppConfig(OneSiteUtil.SCHEDULE_DIRECTORY), "PL_" + _playlist.sch_id+".xml");
                
                if (false == time_serializer.OneSiteDirectSerialize(path, timeSchedulelist))
                    throw new System.IO.IOException();

                return true;

            }
            catch
            {
                ErrorMessage = "기본 스케줄, 시간지정 스케줄 스케줄 폴더에 생성 실패";
                return false;
            }
        }

        public static bool CreateTempFile(playlists _playlists)
        {
            try
            {
                //재생목록에서 해당 리스트를 생성 하여 temp 폴더에 기록 한다.

                //1. Default 스케줄 생성

                playlist defaultSchedule = new playlist();

                playlists timeSchedulelist = new playlists();


                for (int i = 0; i < _playlists.playlist.Length; i++)
                {
                    if (_playlists.playlist[i].apply_status.Equals("Y") && _playlists.playlist[i].sch_type.Equals("D"))
                    {
                        defaultSchedule.sch_id = _playlists.playlist[i].sch_id;
                        defaultSchedule.desc = _playlists.playlist[i].desc;
                        defaultSchedule.cnt = _playlists.playlist[i].cnt;
                        defaultSchedule.sch_type = _playlists.playlist[i].sch_type;
                        defaultSchedule.start_day = _playlists.playlist[i].start_day;
                        defaultSchedule.end_day = _playlists.playlist[i].end_day;
                        defaultSchedule.start_time = _playlists.playlist[i].start_time;
                        defaultSchedule.end_time = _playlists.playlist[i].end_time;
                        defaultSchedule.apply_status = _playlists.playlist[i].apply_status;
                        defaultSchedule.loop_yn = _playlists.playlist[i].loop_yn;
                        defaultSchedule.last_prg = _playlists.playlist[i].last_prg;
                        defaultSchedule.total_size = _playlists.playlist[i].total_size;
                        defaultSchedule.total_playtime = _playlists.playlist[i].total_playtime;

                        if (_playlists.playlist[i].program != null)
                        {
                            for (int j = 0; j < _playlists.playlist[i].program.Length; j++)
                            {
                                //<program id="200802201038272500" is_ad="N" play_time="24" ad_schedule_id="UNKNOWN" />
                                program _program = new program();

                                _program.id = _playlists.playlist[i].program[j].id;
                                _program.is_ad = "N";
                                _program.play_time = _playlists.playlist[i].program[j].play_time;
                                _program.ad_schedule_id = "UNKNOWN";

                                AddProgram(defaultSchedule, _program);
                            }
                        }
                    }
                    else if (_playlists.playlist[i].apply_status.Equals("Y") && _playlists.playlist[i].sch_type.Equals("T"))
                    {

                        playlist timeSchedule = new playlist();

                        timeSchedule.sch_id = _playlists.playlist[i].sch_id;
                        timeSchedule.desc = _playlists.playlist[i].desc;
                        timeSchedule.cnt = _playlists.playlist[i].cnt;
                        timeSchedule.sch_type = _playlists.playlist[i].sch_type;
                        timeSchedule.start_day = _playlists.playlist[i].start_day;
                        timeSchedule.end_day = _playlists.playlist[i].end_day;
                        timeSchedule.start_time = _playlists.playlist[i].start_time;
                        timeSchedule.end_time = _playlists.playlist[i].end_time;
                        timeSchedule.apply_status = _playlists.playlist[i].apply_status;
                        timeSchedule.loop_yn = _playlists.playlist[i].loop_yn;
                        timeSchedule.last_prg = _playlists.playlist[i].last_prg;
                        timeSchedule.total_size = _playlists.playlist[i].total_size;
                        timeSchedule.total_playtime = _playlists.playlist[i].total_playtime;

                        if (_playlists.playlist[i].program != null)
                        {
                            for (int k = 0; k < _playlists.playlist[i].program.Length; k++)
                            {
                                //<program id="200802201038272500" is_ad="N" play_time="24" ad_schedule_id="UNKNOWN" />
                                program _program = new program();

                                _program.id = _playlists.playlist[i].program[k].id;
                                _program.is_ad = "N";
                                _program.play_time = _playlists.playlist[i].program[k].play_time;
                                _program.ad_schedule_id = "UNKNOWN";

                                AddProgram(timeSchedule, _program);
                            }
                        }

                        //새로 추가된 시간지정 play를 타임 스케줄 리스트에 추가
                        if (timeSchedulelist.playlist != null && timeSchedulelist.playlist.Length > 0)
                        {
                            playlist[] new_array = new playlist[timeSchedulelist.playlist.Length + 1];

                            timeSchedulelist.playlist.CopyTo(new_array, 0);

                            new_array[timeSchedulelist.playlist.Length] = timeSchedule;

                            timeSchedulelist.playlist = new_array;

                        }
                        else
                        {
                            playlist[] new_array = new playlist[1];

                            new_array[0] = timeSchedule;

                            timeSchedulelist.playlist = new_array;

                        }
                    }
                }

                //해당 내용을 temp 폴더에 기록 한다.

                OneSiteXmlSerializer de_serializer = new OneSiteXmlSerializer(typeof(playlist));
                string path = Path.Combine(OneSiteUtil.getDirectoryAppConfig(OneSiteUtil.TEMP_DIRECTORY), OneSiteUtil.getAppConfig(OneSiteUtil.DEFAULT_PLAYLIST_XML_FILENAME));
                if (false == de_serializer.OneSiteDirectSerialize(path, defaultSchedule))
                    throw new System.IO.IOException();                

                OneSiteXmlSerializer time_serializer = new OneSiteXmlSerializer(typeof(playlists));
                path = Path.Combine(OneSiteUtil.getDirectoryAppConfig(OneSiteUtil.SCHEDULE_DIRECTORY), OneSiteUtil.getAppConfig(OneSiteUtil.TIME_PLAYLIST_XML_FILENAME));
                
                if (false == time_serializer.OneSiteDirectSerialize(path, timeSchedulelist))
                    throw new System.IO.IOException();                                  

                return true;

            }
            catch
            {
                ErrorMessage = "기본 스케줄, 시간지정 스케줄 스케줄 폴더에 생성 실패";

                return false;
            }
        }

        public static void AddProgram(playlist _playlist, program _program)
        {
            if (_playlist.program != null && _playlist.program.Length > 0)
            {
                program[] new_array = new program[_playlist.program.Length + 1];

                _playlist.program.CopyTo(new_array, 0);

                new_array[_playlist.program.Length] = _program;

                _playlist.program = new_array;
            }
            else
            {
                program[] new_array = new program[1];

                new_array[0] = _program;

                _playlist.program = new_array;
            }
        }
    }
}
