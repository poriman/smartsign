﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ServiceModel;
using System.IO;
using System.Diagnostics;
using System.Collections;
using System.Xml;
using System.Runtime.InteropServices;

namespace MenuBoardService
{
    [ServiceBehavior(InstanceContextMode=InstanceContextMode.Single , ConcurrencyMode= ConcurrencyMode.Multiple)]
    public class OneSiteService : IOneSiteService
    {
        //JOB TYPE 속성 정의
        private const string INSERT_JOB = "INSERT";
        private const string UPDATE_JOB = "UPDATE";
        private const string DELETE_JOB = "DELETE";
        private const string SCHED_NOT_USE = "NOTUSE";
        private const string SCHED_USE = "USE";


        [DllImport("kernel32.dll")]

        extern static private void GetSystemTime(ref SYSTEMTIME systemtime);


        [DllImport("kernel32.dll")]

        extern static private bool SetSystemTime(ref SYSTEMTIME systemtime);

        struct SYSTEMTIME
        {
            public ushort wYear;

            public ushort wMonth;

            public ushort wDayOfWeek;

            public ushort wDay;

            public ushort wHour;

            public ushort wMinute;

            public ushort wSecond;

            public ushort wMilliseconds;
        } 

        public service_query_result GetServiceQuery(service_query query)
        {
            string node_id = query.node_id;
            string node_type = query.node_type;

            service_query_result query_result = new service_query_result();
            query_result.process_result = "E";
            query_result.process_msg = "정의 되지 않은 명령입니다.";

            if (node_type.Equals("contents_list"))
            {
                if (node_id.Equals("ALL"))
                {
                    contents_list current_contentlists = OneSiteContentsXml.list();

                    if (current_contentlists != null && current_contentlists.contents != null && current_contentlists.contents.Length > 0)
                    {
                        query_result.contents_list = current_contentlists;
                        query_result.process_result = "S";
                        query_result.process_msg = "";
                    }
                    else
                    {
                        query_result.process_result = "N";
                        query_result.process_msg = "Contents List 목록이 없습니다.";
                    }
                }
            }
            else if (node_type.Equals("program_list"))
            {
                if (node_id.Equals("ALL"))
                {
                    program_list list = OneSiteProgramXml.list();

                    if (list != null && list.program != null && list.program.Length > 0)
                    {
                        query_result.program_list = list;
                        query_result.process_result = "S";
                        query_result.process_msg = "";
                    }
                    else
                    {
                        query_result.process_result = "N";
                        query_result.process_msg = "Program List 목록이 없습니다.";
                    }
                }

            }
            else if (node_type.Equals("data_list"))
            {
                if (node_id.Equals("ALL"))
                {
                    data_list list = OneSiteDataXml.list();

                    if (list != null && list.dataField != null && list.dataField.Length > 0)
                    {
                        query_result.data_list = list;
                        query_result.process_result = "S";
                        query_result.process_msg = "";
                    }
                    else
                    {
                        query_result.process_result = "N";
                        query_result.process_msg = "Program List 목록이 없습니다.";
                    }
                }

            }
            else if (node_type.Equals("template_list"))
            {
                if (node_id.Equals("ALL"))
                {
                    template_list list = OneSiteTemplateXml.list();

                    if (list != null && list.template != null && list.template.Length > 0)
                    {
                        query_result.template_list = list;
                        query_result.process_result = "S";
                        query_result.process_msg = "";
                    }
                    else
                    {
                        query_result.process_result = "N";
                        query_result.process_msg = "템플릿 리스트 목록이 없습니다.";
                    }
                }

            }
            else if (node_type.Equals("menu_list"))
            {
                if (node_id.Equals("ALL"))
                {
                    menus list = OneSiteMenusXml.list();

                    if (list != null && list.menu != null && list.menu.Length > 0)
                    {
                        query_result.menus = list;
                        query_result.process_result = "S";
                        query_result.process_msg = "";
                    }
                    else
                    {
                        query_result.process_result = "N";
                        query_result.process_msg = "템플릿 리스트 목록이 없습니다.";
                    }
                }

            }
            else if (node_type.Equals("playlists"))
            {
                if (node_id.Equals("ALL"))
                {
                    playlists list = OneSitePlaylistXml.list();

                    if (list != null && list.playlist != null && list.playlist.Length > 0)
                    {
                        query_result.playlists = list;
                        query_result.process_result = "S";
                        query_result.process_msg = "";
                    }
                    else
                    {
                        query_result.process_result = "N";
                        query_result.process_msg = "Program List 목록이 없습니다.";
                    }
                }
            }
            else if (node_type.Equals("subtitles"))
            {
                subtitles current_subtitles = OneSiteSubtitle.list();

                if (current_subtitles != null && current_subtitles.subtitle != null && current_subtitles.subtitle.Length > 0)
                {
                    query_result.subtitles = current_subtitles;
                    query_result.process_result = "S";
                    query_result.process_msg = "";
                }
                else
                {
                    query_result.process_result = "N";
                    query_result.process_msg = "자막 목록 없습니다.";
                }

            }

            return query_result;
        }

        public update_result ContentUpdate(contents_update _update)
        {
            update_result _update_result = new update_result();
            _update_result.process_result = "E";
            _update_result.process_msg = "정의되지 않은 명령입니다.";


            if (OneSiteContentsXml.CreateOrUpdateContent(_update))
            {
                _update_result.process_result = "S";
                _update_result.process_msg = "Success";
            }
            else
            {
                _update_result.process_result = "E";
                _update_result.process_msg = OneSiteContentsXml.ErrorMessage;
            }

            return _update_result;
        }

        public update_result ProgramUpdate(program_update _update)
        {
            update_result _update_result = new update_result();

            if (OneSiteProgramXml.CreateOrUpdateProgram(_update))
            {
                _update_result.process_result = "S";
                _update_result.process_msg = OneSiteProgramXml.program_id;
            }
            else
            {
                _update_result.process_result = "E";
                _update_result.process_msg = OneSiteProgramXml.ErrorMessage;
            }

            return _update_result;
        }

        public update_result DataUpdate(data_update _update)
        {
            update_result _update_result = new update_result();

            if (OneSiteDataXml.CreateOrUpdateData(_update))
            {
                _update_result.process_result = "S";
                _update_result.process_msg = OneSiteDataXml.data_id;
            }
            else
            {
                _update_result.process_result = "E";
                _update_result.process_msg = OneSiteDataXml.ErrorMessage;
            }

            return _update_result;
        }

        public update_result TemplateUpdate(template_update _update)
        {
            update_result _update_result = new update_result();

            if (OneSiteTemplateXml.CreateOrUpdateTemplate(_update))
            {
                _update_result.process_result = "S";
                _update_result.process_msg = OneSiteTemplateXml.template_id;
            }
            else
            {
                _update_result.process_result = "E";
                _update_result.process_msg = OneSiteProgramXml.ErrorMessage;
            }

            return _update_result;
        }

        public update_result MenuUpdate(menus_update _update)
        {
            update_result _update_result = new update_result();

            if (OneSiteMenusXml.CreateOrUpdateMenu(_update))
            {
                _update_result.process_result = "S";
                _update_result.process_msg = "메뉴 업데이트 성공";
            }
            else
            {
                _update_result.process_result = "E";
                _update_result.process_msg = OneSiteProgramXml.ErrorMessage;
            }

            return _update_result;
        }

        public update_result PlaylistUpdate(playlist_update _update)
        {
            update_result _update_result = new update_result();


            if (OneSitePlaylistXml.CreateOrUpdatePlaylist(_update))
            {
                _update_result.process_result = "S";
                _update_result.process_msg = "Success";
            }
            else
            {
                _update_result.process_result = "E";
                _update_result.process_msg = OneSitePlaylistXml.ErrorMessage;
            }
            
            
            return _update_result;
        }

        public update_result SubtitleUpdate(subtitle_update _update)
        {
            update_result _update_result = new update_result();

            if (OneSiteSubtitle.CreateOrUpdateSubtitle(_update))
            {
                    _update_result.process_result = "S";
                    _update_result.process_msg = "Success";
            }
            else
            {
                _update_result.process_result = "E";
                _update_result.process_msg = OneSiteSubtitle.ErrorMessage;
            }

            return _update_result;
        }

        public update_result SubtitleUiUpdate(subtitle_ui_update _update)
        {
            update_result _update_result = new update_result();
            _update_result.process_result = "S";
            _update_result.process_msg = "Success";
            return _update_result;
        }

        public bool SetStbTime(String dateString)
        {
            //해당 Date정보로 시간 정보를 변경 한다. 

            //Console.WriteLine("입력 값 {0}", dateString);

            //Console.WriteLine("DateTime {0}", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss ffff"));

            if (dateString != null && dateString.Length > 17)
            {
                string year = dateString.Substring(0, 4), month = dateString.Substring(4, 2),
                       day = dateString.Substring(6, 2), hour = dateString.Substring(8, 2), min = dateString.Substring(10, 2), sec = dateString.Substring(12, 2), m_sec = dateString.Substring(14, 3);

                int iHour = Int16.Parse(hour);

                SYSTEMTIME systemtime = new SYSTEMTIME();

                GetSystemTime(ref systemtime);

                systemtime.wYear = UInt16.Parse(year);
                systemtime.wMonth = UInt16.Parse(month);
                systemtime.wDay = UInt16.Parse(day);

                if (iHour >= 9)
                {
                    systemtime.wHour = (UInt16)(UInt16.Parse(hour) - 9);
                }
                else
                {
                    systemtime.wHour = (UInt16)(UInt16.Parse(hour) + 15);
                }

                systemtime.wMinute = UInt16.Parse(min);
                systemtime.wSecond = UInt16.Parse(sec);
                systemtime.wMilliseconds = UInt16.Parse(m_sec);

                //systemtime.wDayOfWeek =  DateTime.Now.DayOfWeek

                if (SetSystemTime(ref systemtime))
                {
                    //Console.WriteLine("변경");
                    return true;
                }
                else
                {
                    //Console.WriteLine("변경하지 못함");
                    return false;
                }
            }
            else
            {
                //Console.WriteLine("Error");
                return false;
            }
        }

        public template_list GetTemplateList(string path)
        {
            return OneSiteTemplateXml.list(path);
        }

        public bool SubtractStbTime(String timeString)
        {

            //Console.WriteLine("현재 시간 {0}", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss ffff"));

            //Console.WriteLine("입력 값 : {0}", timeString);

            if (timeString != null && timeString.Length > 5)
            {
                //Console.WriteLine("빼기 실행이전: {0}",DateTime.Now.ToString("yyyyMMddHHmmssffff"));
                //Console.WriteLine("빼기 값 : {0}",timeString);

                string sub_sec = timeString.Substring(0, 2), sub_m_sec = timeString.Substring(2, 3);

                int iSec = Int16.Parse(sub_sec);

                int iMSec = Int16.Parse(sub_m_sec);

                DateTime t_current = DateTime.Now;
                DateTime a_current = DateTime.Now;

                //초단위 더하기
                t_current.AddSeconds(iSec);

                //밀리세컨 더하기
                t_current.AddMilliseconds(iMSec);

                //빼야하는 시간 간격 구하기
                TimeSpan span = t_current.Subtract(a_current);

                
                //현재 설정된 Time 구하기
                SYSTEMTIME systemtime = new SYSTEMTIME();

                GetSystemTime(ref systemtime);

                //현재 시간에서 해당 시간을 뺀 시간 생성 하기

                DateTime current = DateTime.Now.Subtract(span);

                string dateString = current.ToString("yyyyMMddHHmmssffff");

                string year = dateString.Substring(0, 4), month = dateString.Substring(4, 2),
                        day = dateString.Substring(6, 2), hour = dateString.Substring(8, 2), min = dateString.Substring(10, 2), sec = dateString.Substring(12, 2), m_sec = dateString.Substring(14, 3);

                int iHour = Int16.Parse(hour);

                systemtime.wYear = UInt16.Parse(year);
                systemtime.wMonth = UInt16.Parse(month);
                systemtime.wDay = UInt16.Parse(day);

                if (iHour >= 9)
                {
                    systemtime.wHour = (UInt16)(UInt16.Parse(hour) - 9);
                }
                else
                {
                    systemtime.wHour = (UInt16)(UInt16.Parse(hour) + 15);
                }

                systemtime.wMinute = UInt16.Parse(min);
                systemtime.wSecond = UInt16.Parse(sec);
                systemtime.wMilliseconds = UInt16.Parse(m_sec);

                if (SetSystemTime(ref systemtime))
                {
                    //Console.WriteLine("변경 시간 {0}", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss ffff"));
                    return true;
                }
                else
                {
                    //Console.WriteLine("Error");
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public bool AddStbTime(String timeString)
        {
            //Console.WriteLine("현재 시간 {0}", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss ffff"));

            //Console.WriteLine("입력 값 : {0}", timeString);

            if (timeString != null && timeString.Length > 5)
            {
                //Console.WriteLine("더하기 실행이전: {0}", DateTime.Now.ToString("yyyyMMddHHmmssffff"));
                //Console.WriteLine("더하기 값 : {0}", timeString);

                string add_sec = timeString.Substring(0, 2), add_m_sec = timeString.Substring(2, 3);

                int iSec = Int16.Parse(add_sec);

                int iMSec = Int16.Parse(add_m_sec);

                //현재 설정된 Time 구하기
                SYSTEMTIME systemtime = new SYSTEMTIME();

                GetSystemTime(ref systemtime);

                DateTime current = DateTime.Now;

                //초단위 더하기
                current.AddSeconds(iSec);

                //밀리세컨 더하기
                current.AddMilliseconds(iMSec);

                string dateString = current.ToString("yyyyMMddHHmmssffff");

                string year = dateString.Substring(0, 4), month = dateString.Substring(4, 2),
                        day = dateString.Substring(6, 2), hour = dateString.Substring(8, 2), min = dateString.Substring(10, 2), sec = dateString.Substring(12, 2), m_sec = dateString.Substring(14, 3);


                int iHour = Int16.Parse(hour);

                systemtime.wYear = UInt16.Parse(year);
                systemtime.wMonth = UInt16.Parse(month);
                systemtime.wDay = UInt16.Parse(day);

                if (iHour >= 9)
                {
                    systemtime.wHour = (UInt16)(UInt16.Parse(hour) - 9);
                }
                else
                {
                    systemtime.wHour = (UInt16)(UInt16.Parse(hour) + 15);
                }

                systemtime.wMinute = UInt16.Parse(min);
                systemtime.wSecond = UInt16.Parse(sec);
                systemtime.wMilliseconds = UInt16.Parse(m_sec);

                if (SetSystemTime(ref systemtime))
                {
                    //Console.WriteLine("변경 시간 {0}", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss ffff"));
                    return true;
                }
                else
                {
                    //Console.WriteLine("Error");
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public bool SyncXmlFile()
        {
            try
            {
                service_query qur = new service_query();
                qur.node_id = "ALL";
                qur.node_type = "program_list";

                service_query_result svc_result = GetServiceQuery(qur);
                program_list programlist = svc_result.program_list;
                foreach (program pro in programlist.program)
                {
                    string fileName = "", fileSize = "0";
                    service_query squr = new service_query();
                    squr.node_id = "ALL";
                    squr.node_type = "template_list";

                    service_query_result temp_result = GetServiceQuery(squr);
                    template_list templatelist = temp_result.template_list;
                    foreach (template _template in templatelist.template)
                    {
                        if (pro.tp_type.Equals(_template.id) && !_template.ex_file_nm.Equals(""))
                        {
                            fileName = _template.ex_file_nm;
                            break;
                        }
                    }

                    if (fileName != "")
                    {
                        string path = Path.Combine(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, OneSiteUtil.getAppConfig(OneSiteUtil.CONTENT_DIRECTORY)), fileName);
                        if (!File.Exists(path))
                            continue;

                        System.IO.FileInfo fileInfo = new System.IO.FileInfo(path);
                        fileSize = fileInfo.Length.ToString();

                        field[] _field = pro.fields;
                        _field[0].contents.file_nm = fileName;
                        _field[0].contents.file_size = fileSize;

                        pro.fields = _field;

                        program_update _update = new program_update();
                        _update.job_type = "UPDATE";
                        _update.program = pro;

                        update_result result = ProgramUpdate(_update);
                        if (result.process_result.Equals("E"))
                        {
                        }
                    }
                }
                //플레이 리스트 업데이트
                {
                    playlist t_plylist = GetPlaylist ("D")[0];
                    t_plylist.program = GetCurrentPlaylist(t_plylist.program, int.Parse(t_plylist.cnt));
                    playlist_update _update = new playlist_update();
                    _update.job_type = "UPDATE";
                    _update.playlist = t_plylist;
                    update_result result = PlaylistUpdate(_update);
                }
                //타임 스테쥴 리스트 업데이트
                {
                    List<playlist> PList = GetPlaylist("T");
                    foreach (MenuBoardService.playlist _playlist in PList)
                    {
                        _playlist.program = GetCurrentPlaylist(_playlist.program, int.Parse(_playlist.cnt));
                        playlist_update _update = new playlist_update();
                        _update.job_type = "UPDATE";
                        _update.playlist = _playlist;
                        MenuBoardService.update_result result = PlaylistUpdate(_update);
                    }
                }
            }
            catch (Exception e)
            {
                OneSiteLogsWriter.write(e);
            }
            return false;
        }
        private List<playlist> GetPlaylist(string type)
        {
            try
            {
                List<playlist> PList = new List<playlist>();
                service_query plqur = new service_query();
                plqur.node_id = "ALL";
                plqur.node_type = "playlists";

                service_query_result plresult = GetServiceQuery(plqur);
                playlists playlist = plresult.playlists;
                foreach (playlist _plylist in playlist.playlist)
                    if (_plylist.apply_status.Equals("Y") && _plylist.sch_type.Equals(type))
                    {
                        PList.Add( _plylist);
                    }

                return PList;
            }
            catch (Exception e)
            {
                OneSiteLogsWriter.write(e);
            }
            return null;
        }

        private program[] GetCurrentPlaylist(program[] tempPrograme, int index)
        {
            try
            {
                program[] _programe = new program[index];
                service_query qur = new service_query();
                qur.node_id = "ALL";
                qur.node_type = "program_list";
                int count = 0;
                service_query_result svc_result = GetServiceQuery(qur);
                program_list programlist = svc_result.program_list;
                foreach (program _pro in tempPrograme)
                {
                    foreach (program curPro in programlist.program)
                    {
                        if (_pro.id == curPro.id)
                        {
                            _programe[count] = _pro;
                            count++;
                            break;
                        }
                    }
                }

                return _programe;
            }
            catch (Exception e)
            {
                OneSiteLogsWriter.write(e);
            }
            return null;
        }

        public StbInfo GetStbInfo()
        {
            StbInfo _StbInfo = new StbInfo();

            string _path = Path.GetPathRoot(AppDomain.CurrentDomain.BaseDirectory);

            DriveInfo _info = new DriveInfo(_path);

            //시스템 정보를 얻어 오는 부분을 넣도록 한다.

            _StbInfo.total_disk_capacity = _info.TotalSize.ToString();
            _StbInfo.total_free_capacity = _info.AvailableFreeSpace.ToString();

            //세탑 정보에 추가로 config 내용을 추가하여 넘겨 주도록 한다.

            if (File.Exists(Path.Combine(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, OneSiteUtil.getAppConfig(OneSiteUtil.CONFIG_DIR)), "MediaInfo.xml")))
            {

                XmlDocument _doc = new XmlDocument();

                _doc.Load(Path.Combine(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, OneSiteUtil.getAppConfig(OneSiteUtil.CONFIG_DIR)), "MediaInfo.xml"));

                XmlNode mediainfo1 = _doc.SelectSingleNode("//media-machine");

                _StbInfo.product = mediainfo1.Attributes["product"].Value;
                _StbInfo.modename = mediainfo1.Attributes["modename"].Value;
                _StbInfo.inch = mediainfo1.Attributes["inch"].Value;
                _StbInfo.form = mediainfo1.Attributes["form"].Value;

                mediainfo1 = _doc.SelectSingleNode("//media-display");

                _StbInfo.top = mediainfo1.Attributes["top"].Value;
                _StbInfo.left = mediainfo1.Attributes["left"].Value;
                _StbInfo.width = mediainfo1.Attributes["width"].Value;
                _StbInfo.height = mediainfo1.Attributes["height"].Value;

                _doc = null;

                mediainfo1 = null;
            }
            else
            {
                _StbInfo.product = "";
                _StbInfo.modename = "";
                _StbInfo.inch = "";
                _StbInfo.form = "";
                _StbInfo.width = "";
                _StbInfo.height = "";
            }

            return _StbInfo;
        }


        public update_result UpdateStbInfo(StbInfo _info)
        {
            update_result _update_result = new update_result();
            _update_result.process_result = "S";
            _update_result.process_msg = "Success";

            try
            {
                XmlDocument _doc = new XmlDocument();

                _doc.Load(Path.Combine(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, OneSiteUtil.getAppConfig(OneSiteUtil.CONFIG_DIR)), "MediaInfo.xml"));

                XmlNode mediadispInfo = _doc.SelectSingleNode("//media-display");

                mediadispInfo.Attributes["top"].Value = _info.top;
                mediadispInfo.Attributes["left"].Value = _info.left;
                mediadispInfo.Attributes["width"].Value = _info.width;
                mediadispInfo.Attributes["height"].Value = _info.height;

                XmlNode mediaInfo = _doc.SelectSingleNode("//media-machine");

                mediaInfo.Attributes["product"].Value = _info.product;
                mediaInfo.Attributes["modename"].Value = _info.modename;
                mediaInfo.Attributes["inch"].Value = _info.inch;
                mediaInfo.Attributes["form"].Value = _info.form;

                _doc.Save(Path.Combine(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, OneSiteUtil.getAppConfig(OneSiteUtil.CONFIG_DIR)), "MediaInfo.xml"));

                return _update_result;
            }
            catch (Exception e)
            {
                _update_result.process_result = "S";
                _update_result.process_msg = e.Message;

                OneSiteLogsWriter.write(e);
                return _update_result;
            }

        }        

        public VersionInfos GetVersionInfo()
        {

            VersionInfos _versions = new VersionInfos();

            if (File.Exists(Path.Combine(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, OneSiteUtil.getAppConfig(OneSiteUtil.CONFIG_DIR)), "MediaInfo.xml")))
            {
                XmlDocument _doc = new XmlDocument();

                _doc.Load(Path.Combine(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, OneSiteUtil.getAppConfig(OneSiteUtil.CONFIG_DIR)), "MediaInfo.xml"));

                XmlNode mediainfo1 = _doc.SelectSingleNode("//media-info");

                _versions.mainversion = mediainfo1.Attributes["version"].Value;
            }

            string[] files = Directory.GetFiles(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "bin"));

            ArrayList filesList = new ArrayList();

            foreach (string filename in files)
            {
                if (Path.GetExtension(filename).ToUpper().Equals(".DLL") || Path.GetExtension(filename).ToUpper().Equals(".EXE"))
                {
                    FileVersionInfo versioninfo = FileVersionInfo.GetVersionInfo(filename);

                    VersionInfo _v = new VersionInfo();

                    _v.fileName = Path.GetFileName(filename);
                    _v.fileVersion = versioninfo.FileVersion;

                    filesList.Add(_v);
                }
            }

            VersionInfo[] r_version = new VersionInfo[filesList.Count];

            for (int i=0 ; i < filesList.Count ; i++)
            {
                r_version[i] = (VersionInfo)filesList[i];
            }

            _versions.fileversions = r_version;

            filesList = null;

            return _versions;
        }

        public void Reboot(string key)
        {
            string rebootKey = key;

            if (rebootKey.Contains("dlsxpffldkstltmxpawm!@#"))
            {
                string mainVersion = rebootKey.Replace("dlsxpffldkstltmxpawm!@#", "");

                if (mainVersion.Length > 0)
                {
                    //메인 버전이 온 경우에는 해당 버전을 업데이트 후에 리부팅 하도록 한다.

                    XmlDocument _doc = new XmlDocument();

                    _doc.Load(Path.Combine(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, OneSiteUtil.getAppConfig(OneSiteUtil.CONFIG_DIR)), "MediaInfo.xml"));

                    XmlNode mediainfo1 = _doc.SelectSingleNode("//media-info");

                    mediainfo1.Attributes["version"].Value = mainVersion;

                    _doc.Save(Path.Combine(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, OneSiteUtil.getAppConfig(OneSiteUtil.CONFIG_DIR)), "MediaInfo.xml"));

                    _doc = null;

                    mediainfo1 = null;
                }

                System.Diagnostics.Process.Start("ShutDown", "-r -t 00");
            }            
        }

        //입력된 IP에 대하여 자원의 싱크를 맞추는 기능을 제공 한다.
        public bool SyncStb(string ips)
        {
            try
            {
                string[] remote_ip = ips.Split(new string[] { "#" }, StringSplitOptions.RemoveEmptyEntries);

                string path = OneSiteUtil.getDirectoryAppConfig(OneSiteUtil.SYNC_DIRECTORY);

                for (int i = 0; i < remote_ip.Length; i++)
                {
                    File.Create(Path.Combine(path, remote_ip[i]));
                }
            }
            catch
            {
                Directory.Delete(OneSiteUtil.getDirectoryAppConfig(OneSiteUtil.SYNC_DIRECTORY), true);

                return false;
            }

            return true;
        }

        public bool SyncCompleteCheck()
        {

            string path = OneSiteUtil.getDirectoryAppConfig(OneSiteUtil.SYNC_DIRECTORY);

            DirectoryInfo directory = new DirectoryInfo(path);

            if (directory.GetFiles().Length> 0)
            {
                return false;
            }
            else
            {
                return true;
            }
            
        }
    }

    public class OneSiteFileTransferService : IOneSiteFileTransferService
    {
        public RemoteFileInfo DownloadFile(DownloadRequest request)
        {
            try
            {
                //섬네일 다운로드와 일반 파일 다운로드로 구분 한다.

                //-------원본 파일 다운로드 -----------------//
                if (request.Type.Equals("0"))
                {
                    // get some info about the input file
                    string content_path = OneSiteUtil.getDirectoryAppConfig(OneSiteUtil.CONTENT_DIRECTORY);

                    string filePath = System.IO.Path.Combine(content_path, request.FileName);
                    System.IO.FileInfo fileInfo = new System.IO.FileInfo(filePath);

                    // check if exists
                    if (!fileInfo.Exists) throw new System.IO.FileNotFoundException("File not found", request.FileName);

                    // open stream
                    System.IO.FileStream stream = new System.IO.FileStream(filePath, System.IO.FileMode.Open, System.IO.FileAccess.Read);

                    // return result
                    RemoteFileInfo result = new RemoteFileInfo();
                    result.FileName = request.FileName;
                    result.Length = fileInfo.Length;
                    result.Type = request.Type;
                    result.FileByteStream = stream;
                    return result;
                    // after returning to the client download starts. Stream remains open and on server and the client reads it, although the execution of this method is completed.
                }
                else if (request.Type.Equals("2"))
                {
                    throw new System.IO.FileNotFoundException("업데이트 파일은 타입은 다운로드 할수 없습니다.", request.FileName);
                }
                    // 외부 테이터 다운로드....
                else if (request.Type.Equals("4"))
                {
                    string sch_path = OneSiteUtil.getDirectoryAppConfig(OneSiteUtil.SCHEDULE_DIRECTORY);
                    string filePath = System.IO.Path.Combine(sch_path, OneSiteUtil.getExternalDataFileName());

                    System.IO.FileInfo fileInfo = new System.IO.FileInfo(filePath);
                    if (!fileInfo.Exists) 
                        throw new System.IO.FileNotFoundException("File not found", request.FileName);

                    // open stream
                    System.IO.FileStream stream = new System.IO.FileStream(filePath, System.IO.FileMode.Open, System.IO.FileAccess.Read);

                    // return result
                    RemoteFileInfo result = new RemoteFileInfo();
                    result.FileName = OneSiteUtil.Perfix_thumanil + request.FileName + "jpg";
                    result.Length = fileInfo.Length;
                    result.Type = request.Type;
                    result.FileByteStream = stream;
                    return result;
                }
                else if (request.Type.Equals("5"))
                {
                    // get some info about the input file
                    string content_path = OneSiteUtil.getDirectoryAppConfig(OneSiteUtil.CONTENT_DIRECTORY);

                    string filePath = System.IO.Path.Combine(content_path, request.FileName);
                    System.IO.FileInfo fileInfo = new System.IO.FileInfo(filePath);

                    // check if exists
                    if (!fileInfo.Exists) throw new System.IO.FileNotFoundException("File not found", request.FileName);

                    // open stream
                    System.IO.FileStream stream = new System.IO.FileStream(filePath, System.IO.FileMode.Open, System.IO.FileAccess.Read);

                    // return result
                    RemoteFileInfo result = new RemoteFileInfo();
                    result.FileName = request.FileName;
                    result.Length = fileInfo.Length;
                    result.Type = request.Type;
                    result.FileByteStream = stream;
                    return result;
                    // after returning to the client download starts. Stream remains open and on server and the client reads it, although the execution of this method is completed.
                }
                //-------- 섬네일 파일 다운로드----------------// 
                else
                {

                    string thumbnail_path = OneSiteUtil.getDirectoryAppConfig(OneSiteUtil.THUMBNAIL_DIRECTORY);

                    //-------- 컨텐트 섬네일 파일 다운로드 Path----------------// 
                    string filePath = System.IO.Path.Combine(thumbnail_path, OneSiteUtil.Perfix_thumanil + request.FileName + ".jpg");

                    //-------- 프로그램 섬네일 파일 다운로드 Path----------------//
                    if (request.Type.Equals("3"))
                    {
                        filePath = System.IO.Path.Combine(thumbnail_path, OneSiteUtil.Perfix_Program_thumanil + request.FileName + ".jpg");
                    }


                    System.IO.FileInfo fileInfo = new System.IO.FileInfo(filePath);

                    // report start
                    //Console.WriteLine("섬네일 Sending stream " + filePath + " to client");
                    //Console.WriteLine("Size " + fileInfo.Length);

                    // check if exists
                    if (!fileInfo.Exists) throw new System.IO.FileNotFoundException("File not found", request.FileName);

                    // open stream
                    System.IO.FileStream stream = new System.IO.FileStream(filePath, System.IO.FileMode.Open, System.IO.FileAccess.Read);

                    // return result
                    RemoteFileInfo result = new RemoteFileInfo();
                    result.FileName = OneSiteUtil.Perfix_thumanil + request.FileName + "jpg";
                    result.Length = fileInfo.Length;
                    result.Type = request.Type;
                    result.FileByteStream = stream;
                    return result;
                }
            }
            catch (System.IO.FileNotFoundException e)
            {
                OneSiteLogsWriter.write(e);

                return null;
            }
            catch (Exception e)
            {
                OneSiteLogsWriter.write(e);

                return null;
            }
        }

        public UploadResult SyncUpload(RemoteFileInfo request)
        {
            //request.Type 새로운 정의
            // Type  0 Contents
            // Type  1 Thumbnail
            // Type  2 schedule file
            // Type  3 SERVICE XML file

            UploadResult result = new UploadResult();
            result.FileName = "0";

            string content_path = "", save_file_name = request.FileName;
           

            if (request.Type.Equals("0"))
            {
                content_path = OneSiteUtil.getDirectoryAppConfig(OneSiteUtil.CONTENT_DIRECTORY);
            }
            else if (request.Type.Equals("1"))
            {
                content_path = OneSiteUtil.getDirectoryAppConfig(OneSiteUtil.THUMBNAIL_DIRECTORY);
            }
            else if (request.Type.Equals("2"))
            {
                content_path = OneSiteUtil.getDirectoryAppConfig(OneSiteUtil.SCHEDULE_DIRECTORY);
            }
            else if (request.Type.Equals("3"))
            {
                content_path = OneSiteUtil.getDirectoryAppConfig(OneSiteUtil.XML_DIRECTORY);
            }
            else if (request.Type.Equals("4"))
            {
                return result;
            }            

            //해당 파일의 이름과 사이즈가 동일한 파일이 있는 경우에는 업로드 할 필요가 없다.

            if (File.Exists(Path.Combine(content_path, save_file_name)) && (request.Type.Equals("0") || request.Type.Equals("1")))
            {
                FileInfo f_info = new FileInfo(Path.Combine(content_path, save_file_name));

                if (f_info.Length.Equals(request.Length))
                {
                    //동일한 파일은 업로드 할 필요 없다.

                    result.FileName="1";

                    return result;
                }
                else
                {
                    //파일 업로드
                    if (Upload(request.FileByteStream, Path.Combine(content_path, save_file_name)))
                        result.FileName = "1";
                }
            }
            else
            {
                //파일업로드
                if (Upload(request.FileByteStream, Path.Combine(content_path, save_file_name)))
                    result.FileName = "1";
            }

            return result;
        }


        public bool Upload(Stream inputStream, string save_path)
        {
            try
            {
                int chunkSize = 2048;
                byte[] buffer = new byte[chunkSize];

                if (File.Exists(save_path))
                {
                    File.Delete(save_path);
                }

                using (System.IO.FileStream writeStream = new System.IO.FileStream(save_path, System.IO.FileMode.CreateNew, System.IO.FileAccess.ReadWrite))
                {
                    do
                    {
                        // read bytes from input stream
                        int bytesRead = inputStream.Read(buffer, 0, chunkSize);
                        if (bytesRead == 0) break;

                        // write bytes to output stream
                        writeStream.Write(buffer, 0, bytesRead);
                    } while (true);

                    //report end
                    //Console.WriteLine("File Sync {0} Done", save_path);

                    writeStream.Close();
                }

                return true;
            }
            catch (IOException ex)
            {
                OneSiteLogsWriter.write(ex);

                return false;
            }
        }

        public UploadResult UploadFile(RemoteFileInfo request)
        {
            // report start

            OneSiteLogsWriter.write("Start uploading " + request.FileName);
            OneSiteLogsWriter.write("Size " + request.Length);

            //type에 따라서 섬네일 경우와 원본 파일 경우에 따라서 틀려 진다.

            try
            {

                if (request.Type.Equals("0"))
                {
                    //원본 이미지 파일 업로드인 경우
                    string content_path = OneSiteUtil.getDirectoryAppConfig(OneSiteUtil.CONTENT_DIRECTORY);

                    string save_file_name = DateTime.Now.ToString("yyyyMMddHHmmssffff") + System.IO.Path.GetExtension(request.FileName);

                    int chunkSize = 2048;
                    byte[] buffer = new byte[chunkSize];

                    using (System.IO.FileStream writeStream = new System.IO.FileStream(System.IO.Path.Combine(content_path, save_file_name), System.IO.FileMode.CreateNew, System.IO.FileAccess.Write))
                    {
                        do
                        {
                            // read bytes from input stream
                            int bytesRead = request.FileByteStream.Read(buffer, 0, chunkSize);
                            if (bytesRead == 0) break;

                            // write bytes to output stream
                            writeStream.Write(buffer, 0, bytesRead);
                        } while (true);

                        // report end
                        //Console.WriteLine("Done!");

                        writeStream.Close();
                    }

                    UploadResult result = new UploadResult();
                    result.FileName = save_file_name;

                    return result;
                }
                else if (request.Type.Equals("1") || request.Type.Equals("3"))
                {
                    //---- 컨텐트 섬네일 파일의 업로드인 경우 ----------//
                    string thumbnail_path = OneSiteUtil.getDirectoryAppConfig(OneSiteUtil.THUMBNAIL_DIRECTORY);

                    string save_file_name = OneSiteUtil.Perfix_thumanil + request.FileName + ".jpg";

                    //---- 프로그램 섬네일 파일의 업로드인 경우 ----------//
                    if (request.Type.Equals("3"))
                    {
                        save_file_name = OneSiteUtil.Perfix_Program_thumanil + request.FileName + ".jpg";

                        File.Delete(Path.Combine(thumbnail_path, save_file_name));
                    }

                    int chunkSize = 2048;
                    byte[] buffer = new byte[chunkSize];

                    using (System.IO.FileStream writeStream = new System.IO.FileStream(System.IO.Path.Combine(thumbnail_path, save_file_name), System.IO.FileMode.CreateNew, System.IO.FileAccess.Write))
                    {
                        //Exception 처리 해야 합니다.

                        do
                        {
                            // read bytes from input stream
                            int bytesRead = request.FileByteStream.Read(buffer, 0, chunkSize);
                            if (bytesRead == 0) break;

                            // write bytes to output stream
                            writeStream.Write(buffer, 0, bytesRead);
                        } while (true);

                        // report end
                        //Console.WriteLine("Thumbnail Done!");

                        writeStream.Close();
                    }

                    UploadResult result = new UploadResult();
                    result.FileName = save_file_name;

                    return result;
                }
                else if (request.Type.Equals("5"))
                {
                    //원본 이미지 파일 업로드인 경우
                    string content_path = OneSiteUtil.getDirectoryAppConfig(OneSiteUtil.CONTENT_DIRECTORY);

                    string save_file_name = request.FileName;
                    string filePath = System.IO.Path.Combine(content_path, save_file_name);
                    if (File.Exists(filePath))
                        File.Delete(filePath);

                    int chunkSize = 2048;
                    byte[] buffer = new byte[chunkSize];

                    using (System.IO.FileStream writeStream = new System.IO.FileStream(filePath, System.IO.FileMode.CreateNew, System.IO.FileAccess.Write))
                    {
                        do
                        {
                            // read bytes from input stream
                            int bytesRead = request.FileByteStream.Read(buffer, 0, chunkSize);
                            if (bytesRead == 0) break;

                            // write bytes to output stream
                            writeStream.Write(buffer, 0, bytesRead);
                        } while (true);

                        // report end
                        //Console.WriteLine("Done!");

                        writeStream.Close();
                    }

                    UploadResult result = new UploadResult();
                    result.FileName = save_file_name;

                    return result;
                }
                else
                {
                    string update_path = "";
                    if (request.Type.Equals("4"))
                    {
                        //외부 테이터 업로드시 스케쥴 폴더에 파일 이름 그대로 복사함.
                        update_path = OneSiteUtil.getDirectoryAppConfig(OneSiteUtil.SCHEDULE_DIRECTORY);
                    }
                    else //업데이트 파일 업로드인 경우
                    {
                        update_path = OneSiteUtil.getDirectoryAppConfig(OneSiteUtil.UPDATE_DIR);
                    }

                    string save_file_name = request.FileName;
                    if (File.Exists(Path.Combine(update_path, save_file_name)))
                    {
                        File.Delete(Path.Combine(update_path, save_file_name));
                    }

                    int chunkSize = 2048;
                    byte[] buffer = new byte[chunkSize];

                    using (System.IO.FileStream writeStream = new System.IO.FileStream(System.IO.Path.Combine(update_path, save_file_name), System.IO.FileMode.CreateNew, System.IO.FileAccess.Write))
                    {
                        do
                        {
                            // read bytes from input stream
                            int bytesRead = request.FileByteStream.Read(buffer, 0, chunkSize);
                            if (bytesRead == 0) break;

                            // write bytes to output stream
                            writeStream.Write(buffer, 0, bytesRead);
                        } while (true);

                        // report end
                        //Console.WriteLine("Done!");

                        writeStream.Close();
                    }

                    UploadResult result = new UploadResult();
                    result.FileName = save_file_name;

                    return result;
                }
                

            }
            catch (Exception e)
            {
                OneSiteLogsWriter.write(e);

                return null;
            }
        }

        public FileExitsRequest FileExits(FileExitsRequest request)
        {
            FileExitsRequest result = new FileExitsRequest();
            result.FileName = request.FileName;
            result.Type = request.Type;
            result.HasFile = false;

            try
            {
                string _path = "";
                /*
                 * 0 : contents 폴더
                 * 1 : shcedule 폴더
                 * 2 : SERVICE_XML 폴더
                 * 3 : thumbnails 폴더
                 * */
                if (request.Type.Equals("0"))
                {
                    _path = OneSiteUtil.getDirectoryAppConfig(OneSiteUtil.CONTENT_DIRECTORY);
                }
                else if (request.Type.Equals("1"))
                {
                    _path = OneSiteUtil.getDirectoryAppConfig(OneSiteUtil.SCHEDULE_DIRECTORY);
                }
                else if (request.Type.Equals("2"))
                {
                    _path = OneSiteUtil.getDirectoryAppConfig(OneSiteUtil.XML_DIRECTORY);
                }
                else if (request.Type.Equals("3"))
                {
                    _path = OneSiteUtil.getDirectoryAppConfig(OneSiteUtil.THUMBNAIL_DIRECTORY);
                }
                else
                    return result;

                string filePath = System.IO.Path.Combine(_path, request.FileName);
                System.IO.FileInfo fileInfo = new System.IO.FileInfo(filePath);

                // check if exists
                if (!File.Exists(filePath))
                    throw new System.IO.FileNotFoundException("File not found", request.FileName);

                result.HasFile = true;

                return result;
            }
            catch (System.IO.FileNotFoundException e)
            {
                OneSiteLogsWriter.write(e);
                return result;
            }
            catch (Exception e)
            {
                OneSiteLogsWriter.write(e);
                return result;
            }

        }
    }
}
