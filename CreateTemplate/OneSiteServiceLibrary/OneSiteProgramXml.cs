﻿using System;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Collections;
using System.Text;
using Wilson.XmlDbClient;
using System.Data;
using System.Data.Common;
using System.Threading;

namespace MenuBoardService
{
    public class OneSiteProgramXml
    {

        public static string ErrorMessage = "";

        public static string program_id = "";

        //프로그램 등록 또는 프로그램 수정
        public static bool CreateOrUpdateProgram(program_update _update)
        {
            //해당 프로그램을 Xml 파일에 Serialize 
            //string program_id = "";

            if (_update.job_type.Equals("DELETE"))
            {

                program_id = _update.program.id;

                if (CheckProgramUsing(program_id))
                {
                    ErrorMessage = "사용중인 프로그램은 삭제 할 수 없습니다.";
                    return false;
                }
                else
                {
                    //program_list에서 해당 프로그램을 삭제 한다.

                    if (!DeleteInProgramlist(_update.program))
                    {
                        ErrorMessage = "해당 프로그램이 이미 삭제 되었거나, 다른 프로그램이 해당 자원을 사용하여 접근이 거부 되었습니다.";
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
                
            }
            else
            {
                //if (_update.job_type.Equals("INSERT"))
                //{
                //    program_id = DateTime.Now.ToString("yyyyMMddHHmmssffff");
                //    _update.program.id = program_id;
                //}
                //else
                {
                    program_id = _update.program.id;
                }

                string savePath = Path.Combine(OneSiteUtil.getDirectoryAppConfig(OneSiteUtil.SCHEDULE_DIRECTORY), program_id + ".xml");

                OneSiteXmlSerializer program_serializer = new OneSiteXmlSerializer(typeof(program));

                if (program_serializer.OneSiteDirectSerialize(savePath, _update.program))
                {
                    if (CheckProgramUsing(program_id))
                    {
                        if (UpdateInPlaylist(_update.program) && UpdateInProgramlist(_update.program))
                        {
                            return true;
                        }
                    }
                    else
                    {
                        if (UpdateInProgramlist(_update.program))
                        {
                            return true;
                        }
                    }

                }
            }

            return false;
        }

        //프로그램 사용여부 확인 
        public static bool CheckProgramUsing(string program_id)
        {
            if (File.Exists(OneSiteUtil.getDBXml("playlist")))
            {

                XmlDbConnection xmldbconn = new XmlDbConnection();
                try
                {
                    xmldbconn.ConnectionString = OneSiteUtil.getDBXml("playlist");

                    XmlDbCommand xmldbcmd = new XmlDbCommand();
                    xmldbcmd.Connection = xmldbconn;

                    xmldbcmd.CommandText = "SELECT name from program where id='" + program_id + "'";

                    XmlDbDataAdapter da = new XmlDbDataAdapter(xmldbcmd);
                    DataSet ds_program = new DataSet();

                    da.Fill(ds_program);

                    if (ds_program.Tables[0].Rows.Count > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception e)
                {
                    OneSiteLogsWriter.write(e);
                }
                finally
                {
                    xmldbconn.Close();
                }

                return false;
            }
            else
            {
                return false;
            }
        }

        //playlist에서 프로그램 정보 수정
        public static bool UpdateInPlaylist(program _program)
        {
            try
            {
                OneSiteXmlSerializer serializer = new OneSiteXmlSerializer(typeof(playlists));

                playlists current_playlists = OneSitePlaylistXml.list();

                if (current_playlists != null && current_playlists.playlist.Length > 0)
                {
                    for (int i = 0; i < current_playlists.playlist.Length; i++)
                    {

                        int play_time_change = 0;

                        if (current_playlists.playlist[i].program != null && current_playlists.playlist[i].program.Length > 0)
                        {
                            for (int j = 0; j < current_playlists.playlist[i].program.Length; j++)
                            {
                                if (_program.id.Equals(current_playlists.playlist[i].program[j].id))
                                {
                                    //재생시간이 변경 되는 내용을 추가를 해야 한다
                                    play_time_change = play_time_change + int.Parse(current_playlists.playlist[i].program[j].play_time) - int.Parse(_program.play_time);

                                    //변경된 프로그램으로 해당 내용을 변경 한다.
                                    current_playlists.playlist[i].program[j] = _program;
                                }
                            }
                            //변경된 재생시간을 playlist에 반영을 한다.
                            play_time_change = int.Parse(current_playlists.playlist[i].total_playtime) - play_time_change;

                            current_playlists.playlist[i].total_playtime = play_time_change.ToString();
                        }
                    }
                }

                //변경된 프로그램 목록을 기록한다.
                string path = OneSiteUtil.getDBXml("playlist");
                int count = 0;
                bool isApply = false;
                while (isApply == false)
                {
                    try
                    {
                        isApply = serializer.OneSiteDirectSerialize(path, current_playlists);
                    }
                    catch
                    {
                        Thread.Sleep(200);
                        if (File.Exists(path))
                            File.Delete(path);
                    }
                    if (count++ > 2)
                        throw new System.IO.IOException();
                }
                return true;
            }
            catch (Exception e)
            {
                OneSiteLogsWriter.write(e);
                return false;
            }
        }

        //program List에서 정보 수정
        public static bool UpdateInProgramlist(program _program)
        {
            try
            {
                OneSiteXmlSerializer serializer = new OneSiteXmlSerializer(typeof(program_list));

                program_list _programlists = list();


                int program_length = -1;

                //최초에 NULL 인 경우에 처리
                if (_programlists == null)
                {
                    program_length = 0;
                    _programlists = new program_list();
                    _programlists.last_update_time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                }
                else if (_programlists.program == null)
                {
                    program_length = 0;
                }
                else
                {
                    program_length = _programlists.program.Length;
                }


                bool exists = false;

                for (int i = 0; i < program_length; i++)
                {
                    if (_programlists.program[i].id.Equals(_program.id))
                    {
                        _programlists.program[i] = _program;

                        exists = true;
                    }
                }

                if (!exists)
                {
                    //추가 작업 수행

                    program_list new_programlists = new program_list();

                    new_programlists.last_update_time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                    program[] new_program_array = new program[program_length + 1];

                    if (program_length > 0)
                    {
                        _programlists.program.CopyTo(new_program_array, 0);
                    }

                    new_program_array[program_length] = _program;

                    new_programlists.program = new_program_array;

                    //변경된 프로그램 목록을 기록한다.
                    serializer.OneSiteDirectSerialize(OneSiteUtil.getDBXml("program_list"), new_programlists);
                }
                else
                {

                    //변경된 프로그램 목록을 기록한다.
                    serializer.OneSiteDirectSerialize(OneSiteUtil.getDBXml("program_list"), _programlists);
                }

                return true;
            }
            catch (Exception e)
            {
                OneSiteLogsWriter.write(e);
                return false;
            }
        }

        //program List에서 프로그램 삭제
        public static bool DeleteInProgramlist(program _program)
        {
            try
            {
                OneSiteXmlSerializer serializer = new OneSiteXmlSerializer(typeof(program_list));

                program_list _programlists = list();

                if (_programlists != null && _programlists.program != null)
                {

                    bool exists = false;

                    for (int i = 0; i < _programlists.program.Length; i++)
                    {
                        if (_programlists.program[i].id.Equals(_program.id))
                        {
                            exists = true;
                        }
                    }

                    if (exists)
                    {
                        //추가 작업 수행

                        program_list new_programlists = new program_list();

                        new_programlists.last_update_time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                        program[] new_program_array = new program[_programlists.program.Length - 1];

                        new_programlists.program = new_program_array;

                        int new_index = 0;

                        for (int i = 0; i < _programlists.program.Length; i++)
                        {
                            if (!_programlists.program[i].id.Equals(_program.id))
                            {
                                new_program_array[new_index] = _programlists.program[i];
                                new_index++;
                            }
                        }

                        new_programlists.program = new_program_array;

                        //변경된 프로그램 목록을 기록한다.
                        serializer.OneSiteDirectSerialize(OneSiteUtil.getDBXml("program_list"), new_programlists);

                        //프로그램 XML 파일 삭제
                        File.Delete(Path.Combine(OneSiteUtil.getDirectoryAppConfig(OneSiteUtil.SCHEDULE_DIRECTORY),_program.id+".xml"));

                        //프로그램 외부테이터 XML 파일 삭제
                        File.Delete(Path.Combine(OneSiteUtil.getDirectoryAppConfig(OneSiteUtil.CONTENT_DIRECTORY), _program.ex_filenm));

                        //프로그램 섬네일 파일 삭제
                        File.Delete(Path.Combine(OneSiteUtil.getDirectoryAppConfig(OneSiteUtil.THUMBNAIL_DIRECTORY), OneSiteUtil.Perfix_Program_thumanil + _program.id + ".jpg"));

                        return true;
                    }
                    else
                    {
                        //Console.WriteLine("해당 프로그램이 리스트에 없습니다. ID={0}", _program.id);
                        return false;
                    }

                }
                return false;
            }
            catch (Exception e)
            {
                OneSiteLogsWriter.write(e);
                //Console.WriteLine("프로그램 리스트에서 프로그램 삭제 Error. ID={0}", _program.id);
                return false;
            }
        }

        public static program_list list()
        {
            try
            {
                OneSiteXmlSerializer serializer = new OneSiteXmlSerializer(typeof(program_list));

                program_list _programlists = (program_list)serializer.OneSiteDirectDeSerialize(OneSiteUtil.getDBXml("program_list"));

                return _programlists;
            }
            catch
            {
                return null;
            }
        }
    }
}
