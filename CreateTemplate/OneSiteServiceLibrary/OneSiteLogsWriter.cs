﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Xml;
using System.IO;


namespace MenuBoardService
{
    public class OneSiteLogsWriter
    {

        #region "Exception Write"
        /// 
        /// Exception Write
        /// 

        public static void write(Exception ErrorExc)
        {
            try
            {
                string sErrorType = ErrorExc.GetType().Name.ToLower();

                string filename = OneSiteUtil.getLogsDirectory(OneSiteUtil.ERROR);

                XmlDocument xmlDoc = new XmlDocument();
                //Check file exsist or not
                try
                {
                    xmlDoc.Load(filename);
                }
                catch (System.IO.FileNotFoundException)
                {

                    //if file is not found, create a new xml file

                    XmlTextWriter xmlWriter = new XmlTextWriter(filename, System.Text.Encoding.UTF8);
                    xmlWriter.Formatting = Formatting.Indented;
                    xmlWriter.WriteProcessingInstruction("xml", "version='1.0' encoding='UTF-8'");
                    xmlWriter.WriteStartElement("ErrorLog");
                    xmlWriter.Close();
                    xmlDoc.Load(filename);
                }

                XmlNode root = xmlDoc.DocumentElement;
                XmlElement Data = xmlDoc.CreateElement("ErrorData");

                root.AppendChild(Data);

                Data.SetAttribute("Type", sErrorType);
                Data.SetAttribute("DateTime", DateTime.Now.ToString());
                Data.SetAttribute("Type", ErrorExc.GetType().Name);
                Data.SetAttribute("Description", ErrorExc.Message.ToString());
                Data.SetAttribute("Trace", ErrorExc.StackTrace.ToString());
                Data.SetAttribute("Source", ErrorExc.Source.ToString());
                xmlDoc.Save(filename);

            }
            catch (Exception ex1)
            {
                //Send To Message Page..
                OneSiteLogsWriter.write(ex1);
            }
        }
        #endregion

        #region "Message Write"
        /// 
        /// Message Write
        /// 

        public static void write(string message)
        {
            //메시지 기록은 DEBUG 모드에서만 기록 하도록 합니다.
            if (OneSiteUtil.getAppConfig(OneSiteUtil.DEBUG).Equals("true"))
            {
                try
                {

                    //Console.WriteLine(message);

                    string filename = OneSiteUtil.getLogsDirectory(OneSiteUtil.MESSAGE);

                    XmlDocument xmlDoc = new XmlDocument();
                    //Check file exsist or not
                    try
                    {
                        xmlDoc.Load(filename);
                    }
                    catch (System.IO.FileNotFoundException)
                    {

                        //if file is not found, create a new xml file

                        XmlTextWriter xmlWriter = new XmlTextWriter(filename, System.Text.Encoding.UTF8);
                        xmlWriter.Formatting = Formatting.Indented;
                        xmlWriter.WriteProcessingInstruction("xml", "version='1.0' encoding='UTF-8'");
                        xmlWriter.WriteStartElement("MessageLog");
                        xmlWriter.Close();
                        xmlDoc.Load(filename);
                    }

                    XmlNode root = xmlDoc.DocumentElement;
                    XmlElement Data = xmlDoc.CreateElement("MessageData");

                    root.AppendChild(Data);

                    Data.SetAttribute("DateTime", DateTime.Now.ToString());
                    Data.SetAttribute("Messaeg", message);
                    xmlDoc.Save(filename);
                }
                catch (Exception ex1)
                {
                    //Send To Message Page..
                    OneSiteLogsWriter.write(ex1);
                }
            }
        }

        #endregion
    }
}

