﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace MenuBoardService
{
    //XmlSerializer를 상속 받아서 해당 파일 생성시 백업 파일 형태로 파일을 생성을 하고
    //commit function을 두어서 해당 파일을 실제 path에 쓰도록 한다.
    //rollback function은 임시로 기록한 파일을 삭제를 한다.
    public class OneSiteXmlSerializer : XmlSerializer
    {
        private string save_path = "";
        private string tmp_path = "";

        public OneSiteXmlSerializer(Type type):base(type)
        {
            
        }

        public bool OneSiteSerialize(string savePath , object o)
        {
            StreamWriter writer = null;

            bool result = true;

            try
            {
                save_path = savePath;
                tmp_path = save_path + ".tmp";
                rollback();

                writer = new StreamWriter(tmp_path, false, Encoding.UTF8);

                base.Serialize(writer, o);
            }
            catch (Exception e)
            {
                result = false;
                OneSiteLogsWriter.write(e);
                OneSiteLogsWriter.write("파일 기록 하지 못하였습니다.PATH=" + save_path);
                OneSiteMessageStore.SetMessage("파일 기록 하지 못하였습니다.PATH=" + save_path);
            }
            finally
            {
                if (writer != null) writer.Close();
            }

            return result;
        }

        public bool OneSiteDirectSerialize(string savePath, object o)
        {
            StreamWriter writer = null;
            save_path = savePath;
            bool result = true;
            tmp_path = save_path + ".tmp";
            rollback();
            try
            {

                writer = new StreamWriter(tmp_path, false, Encoding.UTF8);

                base.Serialize(writer, o);
            }
            catch (Exception e)
            {
                result = false;
                OneSiteLogsWriter.write(e);
                OneSiteLogsWriter.write("파일 기록 하지 못하였습니다.PATH=" + save_path);
                OneSiteMessageStore.SetMessage("파일 기록 하지 못하였습니다.PATH=" + save_path);
            }
            finally
            {
                if (writer != null) writer.Close();
                if (result == true)
                {
                    result = commit();
                }

                if (result == true && File.Exists(tmp_path))
                {
                    result = rollback();
                }
            }

            return result;
        }

        public object OneSiteDirectDeSerialize(string path)
        {
            StreamReader reader = null;

            object result = null;

            if (File.Exists(path))
            {

                try
                {
                    reader = new StreamReader(path, Encoding.UTF8);

                    result = base.Deserialize(reader);

                }
                catch (Exception e)
                {
                    result = false;
                    OneSiteLogsWriter.write(e);
                    OneSiteMessageStore.SetMessage("파일 열기 Error");
                }
                finally
                {
                    if (reader != null) reader.Close();
                }

                return result;
            }
            else
            {
                return result;
            }
        }

        public bool commit()
        {
            try
            {
                File.Delete(save_path);
                File.Move(tmp_path, save_path);
                return true;
            }
            catch{}

            return false;
        }

        public bool rollback()
        {
            try
            {
                if (File.Exists(tmp_path))
                    File.Delete(tmp_path);

                return true;
            }
            catch { }
            return false;
        }
    }
}
