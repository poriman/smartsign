﻿using System;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Collections;
using System.Text;
using Wilson.XmlDbClient;
using System.Data;
using System.Data.Common;

namespace MenuBoardService
{
    public class OneSiteContentsXml
    {
        public static string ErrorMessage = "";

        public static bool CreateOrUpdateContent(contents_update _update)
        {

            //해당 프로그램을 Xml 파일에 Serialize 
            string contents_id = "";

            if (_update.job_type.Equals("DELETE"))
            {

                if (CheckContentsUsing(Path.GetFileNameWithoutExtension(_update.contents.file_nm)))
                {
                    ErrorMessage = "사용중인 컨텐트는 삭제 할 수 없습니다.";
                }
                else
                {
                    if (!DeleteContentList(_update.contents))
                    {
                        ErrorMessage = "해당 컨텐트가 이미 삭제 되었거나, 다른 프로그램이 해당 자원을 사용하여 접근이 거부 되었습니다.";
                    }
                    else
                    {
                        return true;
                    }
                }
            }
            else
            {
                if (_update.job_type.Equals("INSERT"))
                {
                    contents_id = Path.GetFileNameWithoutExtension(_update.contents.file_nm);
                    _update.contents.id = contents_id;
                }
                else
                {
                    contents_id = _update.contents.id;
                }

                if (!UpdateContentList(_update.contents))
                {
                    ErrorMessage = "다른 프로그램이 해당 자원을 사용하여 접근이 거부 되었습니다.";
                }
                else
                {
                    return true;
                }

            }

            return false;
        }

        public static bool UpdateContentList(contents _contents)
        {
            try
            {
                OneSiteXmlSerializer serializer = new OneSiteXmlSerializer(typeof(contents_list));

                contents_list _contentlists = list();

                int contents_length = -1;

                //최초에 NULL 인 경우에 처리
                if (_contentlists == null)
                {
                    contents_length = 0;
                    _contentlists = new contents_list();
                    _contentlists.last_update_time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                }
                else if (_contentlists.contents == null)
                {
                    contents_length = 0;
                }
                else
                {
                    contents_length = _contentlists.contents.Length;
                }

                bool exists = false;

                _contents.id = Path.GetFileNameWithoutExtension(_contents.file_nm);

                _contents.cont_type = Path.GetExtension(_contents.file_nm).ToUpper().Substring(1);

                _contents.thumbnail_nm = OneSiteUtil.Perfix_thumanil + _contents.id + ".jpg";



                for (int i = 0; i < contents_length; i++)
                {
                    if (_contentlists.contents[i].id.Equals(Path.GetFileNameWithoutExtension(_contents.file_nm)))
                    {
                        _contentlists.contents[i] = _contents;
                        exists = true;
                    }
                }

                if (!exists)
                {
                    //추가 작업 수행

                    contents_list new_contentslists = new contents_list();

                    new_contentslists.last_update_time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                    contents[] new_contents_array = new contents[contents_length + 1];

                    if (contents_length > 0)
                    {
                        _contentlists.contents.CopyTo(new_contents_array, 0);
                    }
                    
                    new_contents_array[contents_length] = _contents;

                    new_contentslists.contents = new_contents_array;

                    //추가된 컨텐트 목록을 기록한다.
                    serializer.OneSiteDirectSerialize(OneSiteUtil.getDBXml("contents_list"), new_contentslists);
                }
                else
                {
                    //변경된 컨텐트 목록을 기록한다.
                    serializer.OneSiteDirectSerialize(OneSiteUtil.getDBXml("contents_list"), _contentlists);
                }

                return true;
            }
            catch (Exception e)
            {
                OneSiteLogsWriter.write(e);
                //Console.WriteLine("컨텐트 리스트에서 컨텐트 등록 Error. ID={0}", _contents.id);
                return false;
            }
        }

        public static bool DeleteContentList(contents _contents)
        {
            try
            {
                OneSiteXmlSerializer serializer = new OneSiteXmlSerializer(typeof(contents_list));

                contents_list _contentlists = list();

                if (_contentlists != null)
                {

                    bool exists = false;

                    for (int i = 0; i < _contentlists.contents.Length; i++)
                    {
                        if (_contentlists.contents[i].id.Equals(Path.GetFileNameWithoutExtension(_contents.file_nm)))
                        {
                            exists = true;
                        }
                    }

                    if (exists)
                    {
                        //추가 작업 수행

                        contents_list new_contentslists = new contents_list();

                        new_contentslists.last_update_time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                        contents[] new_contents_array = new contents[_contentlists.contents.Length - 1];

                        new_contentslists.contents = new_contents_array;

                        int new_index = 0;

                        for (int i = 0; i < _contentlists.contents.Length; i++)
                        {
                            if (!_contentlists.contents[i].id.Equals(Path.GetFileNameWithoutExtension(_contents.file_nm)))
                            {
                                new_contents_array[new_index] = _contentlists.contents[i];
                                new_index++;
                            }
                        }

                        new_contentslists.contents = new_contents_array;

                        //변경된 컨텐트 목록을 기록한다.
                        serializer.OneSiteDirectSerialize(OneSiteUtil.getDBXml("contents_list"), new_contentslists);

                        //해당 Contents에 원본 파일과 thumbnail 이미지 삭제

                        File.Delete(Path.Combine(OneSiteUtil.getDirectoryAppConfig(OneSiteUtil.CONTENT_DIRECTORY), _contents.id + "." + _contents.cont_type.ToLower()));
                        File.Delete(Path.Combine(OneSiteUtil.getDirectoryAppConfig(OneSiteUtil.THUMBNAIL_DIRECTORY), _contents.thumbnail_nm));


                        return true;
                    }
                    else
                    {
                        ErrorMessage =  "해당 컨텐트가 리스트에 없습니다";
                        return false;
                    }

                }

                ErrorMessage = "컨텐트 리스트에 없습니다";
                return false;
            }
            catch (Exception e)
            {
                OneSiteLogsWriter.write(e);
                //Console.WriteLine("컨텐트 리스트에서 컨텐트 삭제 Error. ID={0}", _contents.id);
                return false;
            }
        }

        //해당 Contents가 프로그램에 사용중인가 확인.
        public static bool CheckContentsUsing(string contents_id)
        {
            if (File.Exists(OneSiteUtil.getDBXml("program_list")))
            {
                XmlDbConnection xmldbconn = new XmlDbConnection();

                try
                {
                    xmldbconn.ConnectionString = OneSiteUtil.getDBXml("program_list");

                    XmlDbCommand xmldbcmd = new XmlDbCommand();
                    xmldbcmd.Connection = xmldbconn;

                    xmldbcmd.CommandText = "SELECT id from contents where id='" + contents_id + "'";

                    XmlDbDataAdapter da = new XmlDbDataAdapter(xmldbcmd);
                    DataSet ds_contents = new DataSet();

                    da.Fill(ds_contents);

                    if (ds_contents.Tables[0].Rows.Count > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception e)
                {
                    OneSiteLogsWriter.write(e);
                }
                finally
                {
                    xmldbconn.Close();
                }

                return false;
            }
            else
            {
                return false;
            }
        }

        public static contents_list list()
        {
            try
            {
                OneSiteXmlSerializer serializer = new OneSiteXmlSerializer(typeof(contents_list));

                contents_list _contentlists = (contents_list)serializer.OneSiteDirectDeSerialize(OneSiteUtil.getDBXml("contents_list"));

                return _contentlists;
            }
            catch
            {
                return null;
            }
        }
    }
}
