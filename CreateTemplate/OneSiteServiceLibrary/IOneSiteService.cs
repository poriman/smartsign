﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ServiceModel;
using System.Runtime.Serialization;

namespace MenuBoardService
{

    // The following settings must be added to your configuration file in order for 
    // the new WCF service item added to your project to work correctly.
    // A WCF service consists of a contract (defined below), 
    // a class which implements that interface, and configuration 
    // entries that specify behaviors and endpoints associated with 
    // that implementation (see <system.serviceModel> in your application
    // configuration file).

    [ServiceContract()]
    public interface IOneSiteService
    {
        
        [OperationContract]
        service_query_result GetServiceQuery(service_query query);

        [OperationContract]
        update_result ContentUpdate(contents_update _update);

        //Authoring Tool 추가에 따른 프로그램 관련 내용 추가 - 2008/03/24
        [OperationContract]
        update_result ProgramUpdate(program_update _update);

        [OperationContract]
        update_result DataUpdate(data_update _update);

        [OperationContract]
        update_result PlaylistUpdate(playlist_update _update);

        [OperationContract]
        update_result SubtitleUpdate(subtitle_update _update);

        [OperationContract]
        update_result SubtitleUiUpdate(subtitle_ui_update _update);

        [OperationContract]
        update_result MenuUpdate(menus_update _update);

        [OperationContract]
        update_result TemplateUpdate(template_update _update);

        [OperationContract]
        template_list GetTemplateList(string path);

        [OperationContract]
        StbInfo GetStbInfo();

        [OperationContract]
        bool SetStbTime(String dateString);

        [OperationContract]
        bool AddStbTime(String timeString);

        [OperationContract]
        bool SyncXmlFile();

        [OperationContract]
        bool SubtractStbTime(String timeString);

        [OperationContract]
        update_result UpdateStbInfo(StbInfo _info);

        [OperationContract]
        VersionInfos GetVersionInfo();

        [OperationContract]
        void Reboot(string key);

        [OperationContract]
        bool SyncStb(string ips);

        [OperationContract]
        bool SyncCompleteCheck();
    }

    [DataContract]
    public class StbInfo
    {
        //멥버 

        string _total_disk_capacity, _total_free_capacity, _product, _modename, _inch, _form, _width, _height, _left, _top;

        [DataMember(Order = 0, Name = "total_size", IsRequired = false)]
        public string total_disk_capacity
        {
            get { return _total_disk_capacity; }

            set { _total_disk_capacity = value; }
        }

        [DataMember(Order = 1, Name = "free_size", IsRequired = false)]
        public string total_free_capacity
        {
            get { return _total_free_capacity; }

            set { _total_free_capacity = value; }
        }


        [DataMember(Order = 2, Name = "product", IsRequired = false)]
        public string product
        {
            get { return _product; }

            set { _product = value; }
        }


        [DataMember(Order = 3, Name = "modename", IsRequired = false)]
        public string modename
        {
            get { return _modename; }

            set { _modename = value; }
        }


        [DataMember(Order = 4, Name = "inch", IsRequired = false)]
        public string inch
        {
            get { return _inch; }

            set { _inch = value; }
        }


        [DataMember(Order = 5, Name = "form", IsRequired = false)]
        public string form
        {
            get { return _form; }

            set { _form = value; }
        }


        [DataMember(Order = 6, Name = "width", IsRequired = false)]
        public string width
        {
            get { return _width; }

            set { _width = value; }
        }

        [DataMember(Order = 7, Name = "height", IsRequired = false)]
        public string height
        {
            get { return _height; }

            set { _height = value; }
        }

        [DataMember(Order = 8, Name = "left", IsRequired = false)]
        public string left
        {
            get { return _left; }

            set { _left = value; }
        }

        [DataMember(Order = 8, Name = "top", IsRequired = false)]
        public string top
        {
            get { return _top; }

            set { _top = value; }
        }

    }

    [DataContract]
    public class VersionInfo
    {
        string _filename , _fileversion;

        [DataMember(Order=0,Name="file_name",IsRequired=false)]
        public string fileName
        {
            get {return _filename;}

            set { _filename = value ;}
        }
        [DataMember(Order=1,Name="file_version",IsRequired=false)]
        public string fileVersion
        {
            get {return _fileversion;}

            set { _fileversion = value ;}
        }
    }

    [DataContract]
    public class VersionInfos
    {
        string _mainversion;

        VersionInfo[] _fileversions;

        [DataMember(Order = 0, Name = "mainversion", IsRequired = false)]
        public string mainversion
        {
            get { return _mainversion; }

            set { _mainversion = value; }
        }

        [DataMember(Order = 1, Name = "fileversions", IsRequired = false)]
        public VersionInfo[] fileversions
        {
            get { return _fileversions; }

            set { _fileversions = value; }
        }
    }

    [ServiceContract]
    public interface IOneSiteFileTransferService
    {
        [OperationContract]
        UploadResult SyncUpload(RemoteFileInfo request);
        
        [OperationContract]
        UploadResult UploadFile(RemoteFileInfo request);

        [OperationContract]
        RemoteFileInfo DownloadFile(DownloadRequest request);

        [OperationContract]
        FileExitsRequest FileExits(FileExitsRequest request);
    }

    [MessageContract]
    public class UploadResult
    {
        [MessageBodyMember]
        public string FileName;
    }


    [MessageContract]
    public class DownloadRequest
    {
        [MessageBodyMember]
        public string FileName;
        [MessageBodyMember]
        public string Type;
    }

    [MessageContract]
    public class FileExitsRequest
    {
        [MessageBodyMember]
        public string FileName;
        [MessageBodyMember]
        public string Type;
        [MessageBodyMember]
        public bool HasFile;
    }

    [MessageContract]
    public class RemoteFileInfo : IDisposable
    {
        [MessageHeader(MustUnderstand = true)]
        public string FileName;

        [MessageHeader(MustUnderstand = true)]
        public long Length;

        [MessageHeader(MustUnderstand = true)]
        public string Type;

        [MessageBodyMember(Order = 1)]
        public System.IO.Stream FileByteStream;

        public void Dispose()
        {
            // close stream when the contract instance is disposed. this ensures that stream is closed when file download is complete, since download procedure is handled by the client and the stream must be closed on server.
            // thanks Bhuddhike! http://blogs.thinktecture.com/buddhike/archive/2007/09/06/414936.aspx
            if (FileByteStream != null)
            {
                try
                {
                    FileByteStream.Close();
                    FileByteStream = null;
                }
                catch (Exception ex) 
                {
                    string msgerr = ex.Message;
                    //Console.WriteLine(ex.Message);
                }
            }
        }
    }
}

