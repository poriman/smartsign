﻿using System;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Collections;
using System.Text;
using Wilson.XmlDbClient;
using System.Data;
using System.Data.Common;

namespace MenuBoardService
{
    public class OneSiteSubtitle
    {
        //자막 등록 , 자막 수정 , 자막 삭제 , 현재 자막 목록 쿼리 기능 구현

        public static string ErrorMessage = "";

        //해당 자막이 존재 하는가 확인
        public static bool ExistSubtitle(string subtitle_id)
        {
            if (File.Exists(OneSiteUtil.getDBXml("subtitles")))
            {

                XmlDbConnection xmldbconn = new XmlDbConnection();

                try
                {
                    xmldbconn.ConnectionString = OneSiteUtil.getDBXml("subtitles");

                    XmlDbCommand xmldbcmd = new XmlDbCommand();
                    xmldbcmd.Connection = xmldbconn;

                    xmldbcmd.CommandText = "SELECT id from subtitle where id='" + subtitle_id + "'";

                    XmlDbDataAdapter da = new XmlDbDataAdapter(xmldbcmd);
                    DataSet ds_subtitle = new DataSet();

                    da.Fill(ds_subtitle);

                    if (ds_subtitle.Tables[0].Rows.Count > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception e)
                {
                    OneSiteLogsWriter.write(e);
                }
                finally
                {
                    xmldbconn.Close();
                }

                return false;
            }
            else
            {
                return false;
            }
        }

        public static subtitles list()
        {
            try
            {
                OneSiteXmlSerializer serializer = new OneSiteXmlSerializer(typeof(subtitles));

                subtitles list = (subtitles)serializer.OneSiteDirectDeSerialize(OneSiteUtil.getDBXml("subtitles"));

                return list;
            }
            catch
            {
                return null;
            }
        }


        public static bool CreateOrUpdateSubtitle(subtitle_update _update)
        {

            //해당 프로그램을 Xml 파일에 Serialize 
            string subtitle_id = "";

            if (_update.job_type.Equals("DELETE"))
            {

                if (!DeleteSubtitles(_update.subtitle))
                {
                    ErrorMessage = "해당 자막이 이미 삭제 되었거나, 다른 프로그램이 해당 자원을 사용하여 접근이 거부 되었습니다.";
                }
                else
                {
                    return true;
                }
            }
            else
            {
                if (_update.job_type.Equals("INSERT"))
                {
                    subtitle_id = DateTime.Now.ToString("yyyyMMddHHmmssffff");
                    _update.subtitle.id = subtitle_id;
                }
                else
                {
                    subtitle_id = _update.subtitle.id;
                }

                if (!UpdateSubtitleList(_update.subtitle))
                {
                    ErrorMessage = "다른 프로그램이 해당 자원을 사용하여 접근이 거부 되었습니다.";
                }
                else
                {
                    return true;
                }

            }

            return false;
        }

        public static bool UpdateSubtitleList(subtitle _subtitle)
        {
            try
            {
                OneSiteXmlSerializer serializer = new OneSiteXmlSerializer(typeof(subtitles));

                subtitles _list = list();

                int subtitle_length = -1;

                //최초에 NULL 인 경우에 처리
                if (_list == null)
                {
                    subtitle_length = 0;
                    _list = new subtitles();
                    _list.update_time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                }
                else if (_list.subtitle == null)
                {
                    subtitle_length = 0;
                }
                else
                {
                    subtitle_length = _list.subtitle.Length;
                }


                bool exists = false;

                for (int i = 0; i < subtitle_length; i++)
                {
                    if (_list.subtitle[i].id.Equals(_subtitle.id))
                    {
                        _list.subtitle[i] = _subtitle;
                        exists = true;
                    }
                }

                if (!exists)
                {
                    //추가 작업 수행

                    subtitles new_list = new subtitles();

                    new_list.update_time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                    subtitle[] new_array = new subtitle[subtitle_length + 1];

                    if (subtitle_length > 0)
                    {
                        _list.subtitle.CopyTo(new_array, 0);
                    }

                    new_array[subtitle_length] = _subtitle;

                    new_list.subtitle = new_array;

                    //추가된 자막 목록을 기록한다.
                    serializer.OneSiteDirectSerialize(OneSiteUtil.getDBXml("subtitles"), new_list);
                    serializer.OneSiteDirectSerialize(Path.Combine(OneSiteUtil.getDirectoryAppConfig(OneSiteUtil.TEMP_DIRECTORY), OneSiteUtil.getAppConfig(OneSiteUtil.SUBTITLE_XML_FILENAME)), new_list);

                }
                else
                {
                    //변경된 자막 목록을 기록한다.
                    serializer.OneSiteDirectSerialize(OneSiteUtil.getDBXml("subtitles"), _list);
                    serializer.OneSiteDirectSerialize(Path.Combine(OneSiteUtil.getDirectoryAppConfig(OneSiteUtil.TEMP_DIRECTORY), OneSiteUtil.getAppConfig(OneSiteUtil.SUBTITLE_XML_FILENAME)), _list);
                }

                return true;
            }
            catch (Exception e)
            {
                OneSiteLogsWriter.write(e);
                //Console.WriteLine("자막 리스트에서 자막 수정  Error. ID={0}", _subtitle.id);
                return false;
            }
        }

        public static bool DeleteSubtitles(subtitle _subtitle)
        {
            try
            {
                OneSiteXmlSerializer serializer = new OneSiteXmlSerializer(typeof(subtitles));

                subtitles _list = list();

                if (_list != null)
                {

                    bool exists = ExistSubtitle(_subtitle.id);

                    if (exists)
                    {
                        //추가 작업 수행

                        subtitles new_list = new subtitles();

                        new_list.update_time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                        subtitle[] new_array = new subtitle[_list.subtitle.Length - 1];

                        new_list.subtitle = new_array;

                        int new_index = 0;

                        for (int i = 0; i < _list.subtitle.Length; i++)
                        {
                            if (!_list.subtitle[i].id.Equals(_subtitle.id))
                            {
                                new_array[new_index] = _list.subtitle[i];
                                new_index++;
                            }
                        }

                        new_list.subtitle = new_array;

                        //변경된 자막 목록을 기록한다.
                        serializer.OneSiteDirectSerialize(OneSiteUtil.getDBXml("subtitles"), new_list);
                        serializer.OneSiteDirectSerialize(Path.Combine(OneSiteUtil.getDirectoryAppConfig(OneSiteUtil.TEMP_DIRECTORY), OneSiteUtil.getAppConfig(OneSiteUtil.SUBTITLE_XML_FILENAME)), new_list);

                        return true;
                    }
                    else
                    {
                        //Console.WriteLine("해당 자막정보가 리스트에 없습니다. ID={0}", _subtitle.id);
                        return false;
                    }

                }

                return false;
            }
            catch (Exception e)
            {
                OneSiteLogsWriter.write(e);
                //Console.WriteLine("자막 리스트에서 자막 삭제 Error. ID={0}", _subtitle.id);
                return false;
            }
        }
    }
}
