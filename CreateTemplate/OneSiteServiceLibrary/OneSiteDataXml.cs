﻿using System;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Collections;
using System.Text;
using Wilson.XmlDbClient;
using System.Data;
using System.Data.Common;

namespace MenuBoardService
{
    class OneSiteDataXml
    {
        public static string ErrorMessage = "";

        public static string data_id = "";

        public static bool ExistData(string dataField_id)
        {
            if (File.Exists(OneSiteUtil.getDBXml("data_list")))
            {
                XmlDbConnection xmldbconn = new XmlDbConnection();

                try
                {
                    xmldbconn.ConnectionString = OneSiteUtil.getDBXml("data_list");

                    XmlDbCommand xmldbcmd = new XmlDbCommand();
                    xmldbcmd.Connection = xmldbconn;

                    xmldbcmd.CommandText = "SELECT id from datafield where id='" + dataField_id + "'";

                    XmlDbDataAdapter da = new XmlDbDataAdapter(xmldbcmd);
                    DataSet ds_data = new DataSet();

                    da.Fill(ds_data);

                    if (ds_data.Tables[0].Rows.Count > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception e)
                {
                    OneSiteLogsWriter.write(e);
                }
                finally
                {
                    xmldbconn.Close();
                }

                return false;
            }
            else
            {
                return false;
            }
        }

        public static data_list list()
        {
            try
            {
                OneSiteXmlSerializer serializer = new OneSiteXmlSerializer(typeof(menus));

                data_list list = (data_list)serializer.OneSiteDirectDeSerialize(OneSiteUtil.getDBXml("data_list"));

                return list;
            }
            catch
            {
                return null;
            }
        }

        //프로그램 등록 또는 프로그램 수정
        public static bool CreateOrUpdateData(data_update _update)
        {
            if (_update.job_type.Equals("DELETE"))
            {
                if (!DeleteDataField(_update.dataField))
                {
                    ErrorMessage = "해당 자원이 이미 삭제 되었거나, 다른 프로그램이 해당 자원을 사용하여 접근이 거부 되었습니다.";
                }
                else
                {
                    return true;
                }
            }
            else
            {
                data_id = _update.dataField.id;

                if (!UpdateDataList(_update.dataField))
                {
                    ErrorMessage = "다른 프로그램이 해당 자원을 사용하여 접근이 거부 되었습니다.";
                }
                else
                {
                    return true;
                }

            }

            return false;
        }

        public static bool UpdateDataList(datafield _data)
        {
            try
            {
                OneSiteXmlSerializer serializer = new OneSiteXmlSerializer(typeof(menus));

                data_list _list = list();

                int data_length = -1;

                //최초에 NULL 인 경우에 처리
                if (_list == null)
                {
                    data_length = 0;
                    _list = new data_list();
                    _list.last_update_time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                }
                else if (_list.dataField == null)
                {
                    data_length = 0;
                }
                else
                {
                    data_length = _list.dataField.Length;
                }


                bool exists = false;

                for (int i = 0; i < data_length; i++)
                {
                    if (_list.dataField[i].id.Equals(_data.id))
                    {
                        _list.dataField[i] = _data;
                        exists = true;
                    }
                }

                if (!exists)
                {
                    //추가 작업 수행

                    data_list new_list = new data_list();

                    new_list.last_update_time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                    datafield[] new_array = new datafield[data_length + 1];

                    if (data_length > 0)
                    {
                        _list.dataField.CopyTo(new_array, 0);
                    }

                    new_array[data_length] = _data;

                    new_list.dataField = new_array;

                    //추가된 자막 목록을 기록한다.
                    serializer.OneSiteDirectSerialize(OneSiteUtil.getDBXml("data_list"), new_list);

                }
                else
                {
                    //변경된 자막 목록을 기록한다.
                    serializer.OneSiteDirectSerialize(OneSiteUtil.getDBXml("data_list"), _list);

                }

                return true;
            }
            catch (Exception e)
            {
                OneSiteLogsWriter.write(e);
                return false;
            }
        }

        public static bool DeleteDataField(datafield _data)
        {
            try
            {
                OneSiteXmlSerializer serializer = new OneSiteXmlSerializer(typeof(data_list));

                data_list _list = list();

                if (_list != null)
                {

                    bool exists = ExistData(_data.id);

                    if (exists)
                    {
                        //추가 작업 수행

                        data_list new_list = new data_list();

                        new_list.last_update_time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                        datafield[] new_array = new datafield[_list.dataField.Length - 1];

                        new_list.dataField = new_array;

                        int new_index = 0;

                        for (int i = 0; i < _list.dataField.Length; i++)
                        {
                            if (!_list.dataField[i].id.Equals(_data.id))
                            {
                                new_array[new_index] = _list.dataField[i];
                                new_index++;
                            }
                        }

                        new_list.dataField = new_array;

                        //변경된 자막 목록을 기록한다.
                        serializer.OneSiteDirectSerialize(OneSiteUtil.getDBXml("data_list"), new_list);
                        //if (_menu.menu_img != null && _menu.menu_img.Length > 0)
                        //    File.Delete(Path.Combine(OneSiteUtil.getDirectoryAppConfig(OneSiteUtil.CONTENT_DIRECTORY), _menu.menu_img));

                        return true;
                    }
                    else
                    {
                        return false;
                    }

                }

                return false;
            }
            catch (Exception e)
            {
                OneSiteLogsWriter.write(e);
                return false;
            }
        }
    }
}
