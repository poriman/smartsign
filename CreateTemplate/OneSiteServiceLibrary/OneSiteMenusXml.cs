﻿using System;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Collections;
using System.Text;
using Wilson.XmlDbClient;
using System.Data;
using System.Data.Common;

namespace MenuBoardService
{
    public class OneSiteMenusXml
    {
        public static string ErrorMessage = "";

        //해당 메뉴가 존재 하는가 확인
        public static bool ExistMenus (string menu_id)
        {
            if (File.Exists(OneSiteUtil.getDBXml("menu_list")))
            {

                XmlDbConnection xmldbconn = new XmlDbConnection();

                try
                {
                    xmldbconn.ConnectionString = OneSiteUtil.getDBXml("menu_list");

                    XmlDbCommand xmldbcmd = new XmlDbCommand();
                    xmldbcmd.Connection = xmldbconn;

                    xmldbcmd.CommandText = "SELECT id from menu where id='" + menu_id + "'";

                    XmlDbDataAdapter da = new XmlDbDataAdapter(xmldbcmd);
                    DataSet ds_menu = new DataSet();

                    da.Fill(ds_menu);

                    if (ds_menu.Tables[0].Rows.Count > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception e)
                {
                    OneSiteLogsWriter.write(e);
                }
                finally
                {
                    xmldbconn.Close();
                }

                return false;
            }
            else
            {
                return false;
            }
        }

        public static menus list()
        {
            try
            {
                OneSiteXmlSerializer serializer = new OneSiteXmlSerializer(typeof(menus));

                menus list = (menus)serializer.OneSiteDirectDeSerialize(OneSiteUtil.getDBXml("menu_list"));

                return list;
            }
            catch
            {
                return null;
            }
        }

        public static bool CreateOrUpdateMenu(menus_update _update)
        {

            //해당 프로그램을 Xml 파일에 Serialize 
            string menu_id = "";

            if (_update.job_type.Equals("DELETE"))
            {

                if (!DeleteMenus(_update.menus))
                {
                    ErrorMessage = "해당 메뉴가 이미 삭제 되었거나, 다른 프로그램이 해당 자원을 사용하여 접근이 거부 되었습니다.";
                }
                else
                {
                    return true;
                }
            }
            else
            {
                menu_id = _update.menus.id;

                if (!UpdateMenuList(_update.menus))
                {
                    ErrorMessage = "다른 프로그램이 해당 자원을 사용하여 접근이 거부 되었습니다.";
                }
                else
                {
                    return true;
                }

            }

            return false;
        }
        public static bool UpdateMenuList(menu _menu)
        {
            try
            {
                OneSiteXmlSerializer serializer = new OneSiteXmlSerializer(typeof(menus));

                menus _list = list();

                int menu_length = -1;

                //최초에 NULL 인 경우에 처리
                if (_list == null)
                {
                    menu_length = 0;
                    _list = new menus();
                    _list.update_time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                }
                else if (_list.menu == null)
                {
                    menu_length = 0;
                }
                else
                {
                    menu_length = _list.menu.Length;
                }


                bool exists = false;

                for (int i = 0; i < menu_length; i++)
                {
                    if (_list.menu[i].id.Equals(_menu.id))
                    {
                        _list.menu[i] = _menu;
                        exists = true;
                    }
                }

                if (!exists)
                {
                    //추가 작업 수행

                    menus new_list = new menus();

                    new_list.update_time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                    menu[] new_array = new menu[menu_length + 1];

                    if (menu_length > 0)
                    {
                        _list.menu.CopyTo(new_array, 0);
                    }

                    new_array[menu_length] = _menu;

                    new_list.menu = new_array;

                    //추가된 자막 목록을 기록한다.
                    serializer.OneSiteDirectSerialize(OneSiteUtil.getDBXml("menu_list"), new_list);
                   
                }
                else
                {
                    //변경된 자막 목록을 기록한다.
                    serializer.OneSiteDirectSerialize(OneSiteUtil.getDBXml("menu_list"), _list);
                  
                }

                return true;
            }
            catch (Exception e)
            {
                OneSiteLogsWriter.write(e);
                return false;
            }
        }

        public static bool DeleteMenus(menu _menu)
        {
            try
            {
                OneSiteXmlSerializer serializer = new OneSiteXmlSerializer(typeof(menus));

                menus _list = list();

                if (_list != null)
                {

                    bool exists = ExistMenus(_menu.id);

                    if (exists)
                    {
                        //추가 작업 수행

                        menus new_list = new menus();

                        new_list.update_time = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                        menu[] new_array = new menu[_list.menu.Length - 1];

                        new_list.menu = new_array;

                        int new_index = 0;

                        for (int i = 0; i < _list.menu.Length; i++)
                        {
                            if (!_list.menu[i].id.Equals(_menu.id))
                            {
                                new_array[new_index] = _list.menu[i];
                                new_index++;
                            }
                        }

                        new_list.menu = new_array;

                        //변경된 자막 목록을 기록한다.
                        serializer.OneSiteDirectSerialize(OneSiteUtil.getDBXml("menu_list"), new_list);
                        if(_menu.menu_img != null && _menu.menu_img.Length > 0)
                            File.Delete(Path.Combine(OneSiteUtil.getDirectoryAppConfig(OneSiteUtil.CONTENT_DIRECTORY), _menu.menu_img));

                        return true;
                    }
                    else
                    {
                        return false;
                    }

                }

                return false;
            }
            catch (Exception e)
            {
                OneSiteLogsWriter.write(e);
                //Console.WriteLine("메뉴리스트에서 메뉴 삭제 Error. ID={0}", _subtitle.id);
                return false;
            }
        }
    }
}
