﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Globalization;
using System.IO;
using MenuBoardService;

namespace MenuBoard
{
    public partial class Form1 : Form
    {
        private int m_iFontSelIndex;
        private int m_iPromSelIndex;
        private bool m_isNewTemplate = true;
        public Form1()
        {
            InitializeComponent();

            TB_exFileName.Text = "";
            txt_recommandCnt.Text = "0";
            this.cb_logo.SelectedIndex = 0;
            this.cb_weather.SelectedIndex = 0;
            this.cb_backimage.SelectedIndex = 1;

            check_value01.Checked = false;
            check_value02.Checked = false;
            check_value03.Checked = false;
            check_value04.Checked = false;
            check_value05.Checked = false;
            check_value06.Checked = false;
            check_value07.Checked = false;
            check_value08.Checked = false;
            check_value09.Checked = false;
            check_value10.Checked = false;
            check_value11.Checked = false;
            check_value12.Checked = false;
            check_value13.Checked = false;
            check_value14.Checked = false;
            check_value15.Checked = false;
            check_value16.Checked = false;
            check_value17.Checked = false;
            check_value18.Checked = false;
            check_value19.Checked = false;
            check_value20.Checked = false;

            CreateFolder();
            AddHearder();
            AddTemplateItme();
        }

        private void CreateFolder()
        {
            try
            {
                string path = Path.GetDirectoryName(Application.ExecutablePath) + "\\Contents\\";
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                path = Path.GetDirectoryName(Application.ExecutablePath) + "\\thumbnails\\";
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                path = Path.GetDirectoryName(Application.ExecutablePath) + "\\SERVICE_XML\\";
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
            }
            catch (Exception ex)
            {
                OneSiteLogsWriter.write(ex);
            }
        }


        public template SelectTemplateData
        {
            get
            {
                foreach (ListViewItem curItem in this.Template_listView.SelectedItems)
                {
                    template curTag = (template)curItem.Tag;
                    return curTag;
                }

                return null;
            }
        }

        public bool ISTemplateListViewSelected
        {
            get
            {
                if (this.Template_listView.SelectedItems != null && this.Template_listView.SelectedItems.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public setting SelectFontData
        {
            get
            {
                foreach (ListViewItem curItem in this.Font_listView.SelectedItems)
                {
                    m_iFontSelIndex = curItem.Index;
                    setting curTag = (setting)curItem.Tag;
                    return curTag;
                }

                return null;
            }
        }

        public bool IsFontListSelected
        {
            get
            {
                if (this.Font_listView.SelectedItems != null && this.Font_listView.SelectedItems.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public promotion SelectPromotionData
        {
            get
            {
                foreach (ListViewItem curItem in this.ProMosion_listView.SelectedItems)
                {
                    m_iPromSelIndex = curItem.Index;
                    promotion curTag = (promotion)curItem.Tag;
                    return curTag;
                }

                return null;
            }
        }

        public bool IsPromotionListSelected
        {
            get
            {
                if (this.ProMosion_listView.SelectedItems != null && this.ProMosion_listView.SelectedItems.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        private void AddHearder()
        {
            this.Template_listView.View = View.Details;
            this.Template_listView.Columns.Add("순번", 100, HorizontalAlignment.Center);
            this.Template_listView.Columns.Add("이름", 100, HorizontalAlignment.Center);
            this.Template_listView.Columns.Add("종류", 100, HorizontalAlignment.Center);

            Font_listView.View = View.Details;
            this.Font_listView.Columns.Add("순번", 100, HorizontalAlignment.Center);
            this.Font_listView.Columns.Add("메뉴 이름", 100, HorizontalAlignment.Center);
            this.Font_listView.Columns.Add("코드", 100, HorizontalAlignment.Center);
            this.Font_listView.Columns.Add("타입", 100, HorizontalAlignment.Center);
            this.Font_listView.Columns.Add("최대 글자 수", 100, HorizontalAlignment.Center);

            this.ProMosion_listView.View = View.Details;
            this.ProMosion_listView.Columns.Add("순번", 100, HorizontalAlignment.Center);
            this.ProMosion_listView.Columns.Add("이름", 100, HorizontalAlignment.Center);
            this.ProMosion_listView.Columns.Add("코드", 100, HorizontalAlignment.Center);
            this.ProMosion_listView.Columns.Add("종류", 100, HorizontalAlignment.Center);
        }

        private void AddItem()
        {
        }

        private void ReNumberList()
        {
            for (int i = 1; i - 1 < this.Template_listView.Items.Count; i++)
            {
                this.Template_listView.Items[i - 1].SubItems[0].Text = i.ToString();
            }
        }

        private void AddTemplateItme()
        {
            try
            {
                Template_listView.Items.Clear();
                this.Template_listView.View = View.Details;
                TemplateListLoader loader = new TemplateListLoader();
                template_list list = loader.Load();

                ListViewItem.ListViewSubItem[] subItems;
                ListViewItem item = null;

                for (int i = 0; i < list.template.Length; i++)
                {
                    int viewIdx = this.Template_listView.Items.Count + 1;

                    template _template = list.template[i];
                    item = new ListViewItem(viewIdx.ToString());
                    subItems = new ListViewItem.ListViewSubItem[]
                    { 
                        new ListViewItem.ListViewSubItem(item, _template.name),
                        new ListViewItem.ListViewSubItem(item, _template.type),
                    };

                    item.SubItems.AddRange(subItems);
                    item.Tag = _template;
                    this.Template_listView.Items.Add(item);
                }

                this.Template_listView.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
            }
            catch (Exception ex)
            {
                string errmsg = ex.Message;
            }
        }

        private void Template_listView_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.ISTemplateListViewSelected)
            {
                m_isNewTemplate = false;
                AddTemplateData(this.SelectTemplateData);
            }
            else
            {
                Clear();
            }
        }

        private void AddTemplateData(template data)
        {
            if (data != null)
            {
                m_isNewTemplate = false;
                this.txt_Name.Text = data.name;
                this.TB_Count.Text = data.count;
                this.txt_recommandCnt.Text = data.recommandcount;
                this.TB_Decs.Text = data.desc;
                TB_exFileName.Text = data.ex_file_nm;
                this.TB_Name.Text = data.file_nm;
                this.TB_ThumbnailPath.Text = data.thumbnail_nm;
                this.cb_Form.SelectedItem = GetDeFormType(data.form);
                this.cb_TemplateType.SelectedItem = GetDeType(data.type);
                this.cb_menuType.SelectedIndex = data.isused.Equals("true") ? 1 : 0;

                this.text_butimage.Text = data.buttimg;
                this.cb_logo.SelectedIndex = (data.logo != null && data.logo.Equals("use")) ? 0 : 1;
                this.cb_backimage.SelectedIndex = (data.backImage != null && data.backImage.Equals("use")) ? 0 : 1;
                this.cb_weather.SelectedIndex = (data.weather != null && data.weather.Equals("use")) ? 0 : 1;

                check_value01.Checked = GetDataCategory (data.category, 1);
                check_value02.Checked = GetDataCategory(data.category, 2);
                check_value03.Checked = GetDataCategory(data.category, 3);
                check_value04.Checked = GetDataCategory(data.category, 4);
                check_value05.Checked = GetDataCategory(data.category, 5);
                check_value06.Checked = GetDataCategory(data.category, 6);
                check_value07.Checked = GetDataCategory(data.category, 7);
                check_value08.Checked = GetDataCategory(data.category, 8);
                check_value09.Checked = GetDataCategory(data.category, 9);
                check_value10.Checked = GetDataCategory(data.category, 10);
                check_value11.Checked = GetDataCategory(data.category, 11);
                check_value12.Checked = GetDataCategory(data.category, 12);
                check_value13.Checked = GetDataCategory(data.category, 13);
                check_value14.Checked = GetDataCategory(data.category, 14);
                check_value15.Checked = GetDataCategory(data.category, 15);
                check_value16.Checked = GetDataCategory(data.category, 16);
                check_value17.Checked = GetDataCategory(data.category, 17);
                check_value18.Checked = GetDataCategory(data.category, 18);
                check_value19.Checked = GetDataCategory(data.category, 19);
                check_value20.Checked = GetDataCategory(data.category, 20);

                AddFontData(data.settings);
                AddPromotion(data.promotions);
            }
        }

        private string GetEnType(string value)
        {
            try
            {
                switch (value)
                {
                    case "메뉴":
                        return "M";
                    case "프로모션":
                        return "P";
                    case "메뉴 + 프로모션":
                        return "T";
                    case "리스트타입":
                        return "W";
                }
            }
            catch (Exception ex)
            {
                string errmsg = ex.Message.ToString();
            }
            return "P";
        }

        private string GetDeType(string value)
        {
            try
            {
                switch (value)
                {
                    case "M":
                        return "메뉴";
                    case "P":
                        return "프로모션";
                    case "T":
                        return "메뉴 + 프로모션";
                    case "W":
                        return "리스트타입";
                }
            }
            catch (Exception ex)
            {
                string errmsg = ex.Message.ToString();
            }

            return "프로모션";
        }

        private string GetEnFormType(string value)
        {
            try
            {
                switch (value)
                {
                    case "가로형":
                        return "Width";
                    case "세로형":
                        return "Height";
                }
            }
            catch (Exception ex)
            {
                string errmsg = ex.Message.ToString();
            }

            return "Height";
        }

        private string GetDeFormType(string value)
        {
            try
            {
                switch (value)
                {
                    case "Width":
                        return "가로형";
                    case "Height":
                        return "세로형";
                }
            }
            catch (Exception ex)
            {
                string errmsg = ex.Message.ToString();
            }

            return "세로형";
        }

        private void AddFontData(setting[] fonts)
        {
            try
            {
                Font_listView.Items.Clear();

                ListViewItem.ListViewSubItem[] subItems;
                ListViewItem item = null;

                for (int i = 0; i < fonts.Length; i++)
                {
                    int viewIdx = this.Font_listView.Items.Count + 1;

                    setting _setting = fonts[i];

                    item = new ListViewItem(viewIdx.ToString());
                    subItems = new ListViewItem.ListViewSubItem[]
                    { 
                        new ListViewItem.ListViewSubItem(item, _setting.id),
                        new ListViewItem.ListViewSubItem(item, (_setting.code == null)?"":_setting.code),
                        new ListViewItem.ListViewSubItem(item, GetTypeName(_setting.type)),
                        new ListViewItem.ListViewSubItem(item, _setting.max_count_cd),
                    };

                    item.SubItems.AddRange(subItems);
                    item.Tag = (object)_setting;
                    this.Font_listView.Items.Add(item);
                }

                this.Font_listView.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
            }
            catch (Exception ex)
            {
                string errmsg = ex.Message;
            }
        }


        private void AddPromotion(promotion[] data)
        {
            try
            {
                ProMosion_listView.Items.Clear();

                ListViewItem.ListViewSubItem[] subItems;
                ListViewItem item = null;

                for (int i = 0; i < data.Length; i++)
                {
                    int viewIdx = this.ProMosion_listView.Items.Count + 1;

                    promotion _data = data[i];

                    item = new ListViewItem(viewIdx.ToString());
                    subItems = new ListViewItem.ListViewSubItem[]
                    { 
                        new ListViewItem.ListViewSubItem(item, _data.name),
                        new ListViewItem.ListViewSubItem(item, _data.code),
                        new ListViewItem.ListViewSubItem(item, GetTypeName(_data.cont_type)),
                    };

                    item.SubItems.AddRange(subItems);
                    item.Tag = (object)_data;
                    this.ProMosion_listView.Items.Add(item);
                }

                this.ProMosion_listView.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
            }
            catch (Exception ex)
            {
                string errmsg = ex.Message;
            }
        }

        private void Clear()
        {
            m_isNewTemplate = true;
            this.txt_Name.Text = "";
            this.TB_Count.Text = "0";
            this.txt_recommandCnt.Text = "0";
            this.TB_Decs.Text = "";
            this.TB_exFileName.Text = "";
            this.TB_Name.Text = "";
            this.TB_ThumbnailPath.Text = "";
            this.cb_TemplateType.SelectedIndex = -1;
            this.cb_menuType.SelectedIndex = 0;
            this.cb_Form.SelectedIndex = -1;
            this.Font_listView.Items.Clear();
            this.cb_logo.SelectedIndex = 0;
            this.cb_weather.SelectedIndex = 0;
            this.cb_backimage.SelectedIndex = 1;
            this.ProMosion_listView.Items.Clear();
            this.text_butimage.Text = "";

            check_value01.Checked = false;
            check_value02.Checked = false;
            check_value03.Checked = false;
            check_value04.Checked = false;
            check_value05.Checked = false;
            check_value06.Checked = false;
            check_value07.Checked = false;
            check_value08.Checked = false;
            check_value09.Checked = false;
            check_value10.Checked = false;
            check_value11.Checked = false;
            check_value12.Checked = false;
            check_value13.Checked = false;
            check_value14.Checked = false;
            check_value15.Checked = false;
            check_value16.Checked = false;
            check_value17.Checked = false;
            check_value18.Checked = false;
            check_value19.Checked = false;
            check_value20.Checked = false;
        }

        private bool CheckData()
        {
            if (this.txt_Name.Text.Length == 0)
            {
                MessageBox.Show("템플릿 이름을 입력 하세요.");
                this.txt_Name.Focus();
                return false;
            }
            if (this.TB_Name.Text.Length == 0)
            {
                MessageBox.Show("플래쉬 파일 이름을 입력 하세요.");
                this.TB_Name.Focus();
                return false;
            }
            if (TB_exFileName.Text.Length == 0)
            {
                MessageBox.Show("외부 테이터 파일 이름을 입력 하세요.");
                return false;
            }
            if (this.TB_ThumbnailPath.Text.Length == 0)
            {
                MessageBox.Show("썸네일 경로을 설정 하세요.");
                return false;
            }
            if (this.text_butimage.Text.Length == 0)
            {
                MessageBox.Show("버튼 이미지를 설정 하십시오.");
                return false;
            }

            if (this.TB_Count.Text.Length == 0)
            {
                MessageBox.Show("개수을 입력 하세요.");
                this.TB_Name.Focus();
                return false;
            }
            if (!IsNumeric(TB_Count.Text))
            {
                MessageBox.Show("숫자만 입력 하세요.");
                this.TB_Count.Focus();
                return false;
            }

            if (this.txt_recommandCnt.Text.Length == 0)
            {
                MessageBox.Show("개수을 입력 하세요.");
                this.txt_recommandCnt.Focus();
                return false;
            }

            if (!IsNumeric(this.txt_recommandCnt.Text))
            {
                MessageBox.Show("숫자만 입력 하세요.");
                this.txt_recommandCnt.Focus();
                return false;
            }

            if (cb_Form.SelectedIndex < 0)
            {
                MessageBox.Show("디스플레이 유형을 선택 하십시오.");
                return false;
            }

            if (cb_TemplateType.SelectedIndex < 0)
            {
                MessageBox.Show("종류을 선택 하십시오.");
                return false;
            }

            return true;
        }

        public static bool IsNumeric(string value)
        {
            try
            {
                foreach (char _char in value)
                {
                    if (!Char.IsNumber(_char))
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                string errmsg = ex.Message;
            }

            return true;
        }

        private void Font_listView_DoubleClick(object sender, EventArgs e)
        {
            FontApply();
        }

        private void FontApply()
        {
            try
            {
                bool isNew = false;
                int index = -1;
                CreateMenuColumDlg dlg = new CreateMenuColumDlg();
                setting _font = new setting();
                if (SelectFontData != null)
                {
                    dlg.Name = SelectFontData.id;
                    dlg.Type = GetTypeName(SelectFontData.type);
                    dlg.FontCount = SelectFontData.max_count_cd;
                    dlg.Code = SelectFontData.code;
                    index = m_iFontSelIndex;
                }
                else
                {
                    dlg.Name = "";
                    dlg.Type = "텍스트";
                    dlg.FontCount = "0";
                    dlg.Code = "";
                    isNew = true;
                } 

                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    _font.id            = dlg.Name;
                    _font.max_count_cd  =dlg.FontCount;
                    _font.type          =  GetType(dlg.Type);
                    _font.code          = dlg.Code;

                    if (!isNew)
                    {
                        Font_listView.Items.RemoveAt(index);
                    }
                    else
                    {
                        index = Font_listView.Items.Count;
                    }
                    {
                        ListViewItem.ListViewSubItem[] subItems;
                        ListViewItem item = null;

                        {
                            int count = index + 1;
                            item = new ListViewItem(count.ToString());
                            subItems = new ListViewItem.ListViewSubItem[]
                        { 
                            new ListViewItem.ListViewSubItem(item, _font.id),
                            new ListViewItem.ListViewSubItem(item, (_font.code == null)?"":_font.code),
                            new ListViewItem.ListViewSubItem(item, GetTypeName(_font.type)),
                            new ListViewItem.ListViewSubItem(item, _font.max_count_cd),
                        };

                            item.SubItems.AddRange(subItems);
                            item.Tag = (object)_font;
                            if (!isNew)
                                this.Font_listView.Items.Insert(index, item);
                            else
                                this.Font_listView.Items.Add(item);
                        }

                        this.Font_listView.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
                    }
                }
            }
            catch (Exception ex)
            {
                string errmsg = ex.Message;
            }
        }

        private void ProMosion_listView_DoubleClick(object sender, EventArgs e)
        {
            ProMotionApply();
        }

        private void ProMotionApply()
        {
            try
            {
                CreatePromotion dlg = new CreatePromotion();
                promotion data = new promotion();
                int index = -1; bool isNew = true;
                if (this.SelectPromotionData != null)
                {
                    isNew = false;
                    index = m_iPromSelIndex;
                    data = SelectPromotionData;
                    dlg._Name = data.name;
                    dlg.Type = GetTypeName(data.cont_type);
                    dlg.CODE = data.code;
                    dlg.MAXCOUNT = data.fontsize;
                    if (data.fontsize != null)
                        dlg.FONTSIZE = data.fontsize;
                    if (data.fontcolor != null)
                        dlg.FONTCOLOR = data.fontcolor;
                }
                else
                {
                    dlg.FONTSIZE = "20";
                    dlg.MAXCOUNT = "0";
                }

                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    data.name = dlg._Name;
                    data.cont_type = GetType(dlg.Type);
                    data.code = dlg.CODE;
                    if (dlg.Type.Equals("텍스트"))
                    {
                        data.fontcolor = dlg.FONTCOLOR;
                        data.fontsize = dlg.MAXCOUNT;
                    }
                    if (!isNew)
                    {
                        this.ProMosion_listView.Items.RemoveAt(index);
                    }
                    else
                    {
                        index = this.ProMosion_listView.Items.Count;
                    }
                    {
                        ListViewItem.ListViewSubItem[] subItems;
                        ListViewItem item = null;

                        {
                            int count = index + 1;
                            item = new ListViewItem(count.ToString());
                            subItems = new ListViewItem.ListViewSubItem[]
                        { 
                            new ListViewItem.ListViewSubItem(item, data.name),
                            new ListViewItem.ListViewSubItem(item, data.code),
                            new ListViewItem.ListViewSubItem(item, GetTypeName(data.cont_type)),
                        };

                            item.SubItems.AddRange(subItems);
                            item.Tag = (object)data;
                            if (!isNew)
                                this.ProMosion_listView.Items.Insert(index, item);
                            else
                                this.ProMosion_listView.Items.Add(item);
                        }

                        this.ProMosion_listView.AutoResizeColumns(ColumnHeaderAutoResizeStyle.HeaderSize);
                    }
                }
            }
            catch (Exception ex)
            {
                string errmsg = ex.Message;
            }
        }

        private string GetType(string type)
        {
            switch (type)
            {
                case "I":
                case "이미지":
                    return "I";
                case "M":
                case "동영상":
                    return "M";
                case "F":
                case "플래쉬":
                    return "F";
            }
            return "T";
        }

        private string GetTypeName(string type)
        {
            switch (type)
            {
                case "I":
                case "이미지":
                    return "이미지";
                case "M":
                case "동영상":
                    return "동영상";
                case "F":
                case "플래쉬":
                    return "플래쉬";
            }

            return "텍스트";
        }

        private void BTN_imagePath_Click(object sender, EventArgs e)
        {
            try{
                
                OpenFileDialog fileDlg = new OpenFileDialog();
                string images = "*.PNG;*.TIF;*.WMF;*.JPG;*.GIF;*.BMP;*.JPEG;*.EMF";
                string avi = "*.MOV;*.MPEG;*.MPG;*.WMA;*.WMV;*.ASF;*.FLV;*.AVI";
                string flash = "*.SWF";

                fileDlg.Title = "컨텐츠 추가";
                fileDlg.Filter = string.Format("{3} ({0};{1};{2})|{0};{1};{2}|{4} ({0})|{0}|{5} (*.SWF)|*.SWF|{6} (*.PPT)|*.PPT|{7} ({2})|{2}|{8}|*.*",
                                                avi, flash, images,
                                                "지원 파일",
                                                "동영상 파일",
                                                "플레쉬 파일",
                                                "파워 포인터 파일",
                                                "이미지 파일",
                                                "모든 파일");

                if (fileDlg.ShowDialog() == DialogResult.OK)
                {
                    FileInfo Fileinfo = new FileInfo(fileDlg.FileName);
                    string newpath = Path.GetDirectoryName(Application.ExecutablePath) + "\\Contents\\" + Fileinfo.Name;
                    File.Copy(fileDlg.FileName, newpath, true);
                    this.TB_ThumbnailPath.Text = Fileinfo.Name;
                    this.Update();
                }
            }
            catch (Exception ex)
            {
                string errmsg = ex.Message;
            }
        }

        private void BTN_NewSave_Click(object sender, EventArgs e)
        {
            m_isNewTemplate = true;
            Save();
        }

        private void btn_Save_Click(object sender, EventArgs e)
        {
            Save();
        }

        private string GetCategory()
        {
            try
            {
                return string.Format("{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}{10}{11}{12}{13}{14}{15}{16}{17}{18}{19}",
                                    check_value01.Checked ? 1 : 0, check_value02.Checked ? 1 : 0, check_value03.Checked ? 1 : 0, check_value04.Checked ? 1 : 0, check_value05.Checked ? 1 : 0,
                                    check_value06.Checked ? 1 : 0, check_value07.Checked ? 1 : 0, check_value08.Checked ? 1 : 0, check_value09.Checked ? 1 : 0, check_value10.Checked ? 1 : 0,
                                    check_value11.Checked ? 1 : 0, check_value12.Checked ? 1 : 0, check_value13.Checked ? 1 : 0, check_value14.Checked ? 1 : 0, check_value15.Checked ? 1 : 0,
                                    check_value16.Checked ? 1 : 0, check_value17.Checked ? 1 : 0, check_value18.Checked ? 1 : 0, check_value19.Checked ? 1 : 0, check_value20.Checked ? 1 : 0);
            }
            catch
            {
            }
            return "00000000000000000000";
        }

        private bool GetDataCategory(string data, int index)
        {
            try
            {
                if (data != null && data.Length == 20)
                {
                    return string.Format("{0}",data[index - 1]) == "1";
                }
            }
            catch{
            }
            return false;
        }

        private bool Save()
        {
            try
            {
                if (CheckData())
                {
                    TemplateListLoader loader = new TemplateListLoader();
                    template[] arry = new template[1];
                    template Data = new template();
                    if (SelectTemplateData != null)
                        Data = SelectTemplateData;

                    if (m_isNewTemplate)
                    {
                        Data.id = DateTime.Now.ToString("yyyyMMddHHmmssffff");                        
                    }

                    Data.name = this.txt_Name.Text;
                    Data.file_nm = this.TB_Name.Text;
                    Data.ex_file_nm = TB_exFileName.Text;
                    Data.count = this.TB_Count.Text;
                    Data.recommandcount = this.txt_recommandCnt.Text;
                    Data.form = GetEnFormType(this.cb_Form.SelectedItem.ToString());
                    Data.desc = this.TB_Decs.Text;
                    Data.thumbnail_nm = this.TB_ThumbnailPath.Text;
                    Data.type = GetEnType(this.cb_TemplateType.SelectedItem.ToString());
                    Data.isused = (this.cb_menuType.SelectedIndex == 0)?"false" : "true";
                    
                    Data.backImage = this.cb_backimage.SelectedItem.ToString();
                    Data.weather = this.cb_weather.SelectedItem.ToString();
                    Data.logo = this.cb_logo.SelectedItem.ToString();
                    Data.buttimg = this.text_butimage.Text;
                    Data.category = GetCategory();
                    Data.backImagepath = "";

                    setting[] fontArry = null;
                    if (Font_listView.Items.Count > 0)
                    {
                        fontArry = new setting[this.Font_listView.Items.Count];
                        int index = 0;
                        foreach (ListViewItem curItem in this.Font_listView.Items)
                        {
                            fontArry[index++] = (setting)curItem.Tag;
                        }
                        Data.settings = fontArry;
                    }
                    else
                        Data.settings = null;

                    promotion[] proArry = null;
                    if (ProMosion_listView.Items.Count > 0)
                    {
                        proArry = new promotion[ProMosion_listView.Items.Count];
                        int index = 0;
                        foreach (ListViewItem curItem in this.ProMosion_listView.Items)
                        {
                            proArry[index++] = (promotion)curItem.Tag;
                        }
                        Data.promotions = proArry;
                    }
                    else
                        Data.promotions = null;

                    arry[0] = Data;
                    if (m_isNewTemplate)
                    {
                        loader.InsertTemplate(arry);
                    }
                    else
                    {
                        loader.UpdateTemplate(arry);
                    }

                    AddTemplateItme();
                    Clear();
                }
            }
            catch(Exception ex)
            {
                string errmsg = ex.Message;
                return false;
            }

            return true;
        }

        private void Menu_Font_Add_Click(object sender, EventArgs e)
        {
            FontApply();
        }

        private void Menu_Font_Modify_Click(object sender, EventArgs e)
        {
            if (SelectFontData != null)
            {
                FontApply();
            }
        }

        private void Menu_Font_Delete_Click(object sender, EventArgs e)
        {
            if (SelectFontData != null)
            {
                Font_listView.Items.RemoveAt(m_iFontSelIndex);
            }
        }

        private void Menu_PM_Add_Click(object sender, EventArgs e)
        {
            ProMotionApply();
        }

        private void Menu_PM_Modify_Click(object sender, EventArgs e)
        {
            if (SelectPromotionData != null)
            {
                ProMotionApply();
            }
        }

        private void Menu_PM_Delete_Click(object sender, EventArgs e)
        {
            if (SelectPromotionData != null)
            {
                this.ProMosion_listView.Items.RemoveAt(this.m_iPromSelIndex);
            }
        }

        private void btn_Cancel_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void cb_Form_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void BTN_AddFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog fileDlg = new OpenFileDialog();
            string images = "*.swf";
            fileDlg.Title = "플래쉬 추가";
            fileDlg.Filter = string.Format("플래쉬 파일({0})|{0}", images);

            if (fileDlg.ShowDialog() == DialogResult.OK)
            {
                FileInfo Fileinfo = new FileInfo(fileDlg.FileName);               
                string newpath = Path.GetDirectoryName(Application.ExecutablePath) + "\\Contents\\" + Fileinfo.Name;
                File.Copy(fileDlg.FileName, newpath, true);

                TB_Name.Text = Fileinfo.Name;
            }
        }

        private void BTN_Add_Template_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog fileDlg = new OpenFileDialog();
                string images = "*.xml";
                fileDlg.Title = "xml 추가";
                fileDlg.Filter = string.Format("템플릿 파일({0})|{0}", images);

                if (fileDlg.ShowDialog() == DialogResult.OK)
                {
                    OneSiteService sv = new OneSiteService();
                    template_list _templatelists = sv.GetTemplateList(fileDlg.FileName);
                    TemplateListLoader loader = new TemplateListLoader();

                    template[] arry = new template[_templatelists.template.Length];
                    int index = 0;
                    foreach (template _template in _templatelists.template)
                    {
                       // _template.id = DateTime.Now.ToString("yyyyMMddHHmmssffff"); ;
                        arry[index++] = _template;
                        
                    }

                    loader.InsertTemplate(arry);
                    AddTemplateItme();
                    Clear();

                    FileInfo Fileinfo = new FileInfo(fileDlg.FileName);
                    
                    string rootPath = Path.GetDirectoryName(Application.ExecutablePath);

                    string contPath = rootPath + "\\Contents\\";
                    if (!Directory.Exists(contPath))
                    {
                        Directory.CreateDirectory(contPath);
                    }

                    DirectoryInfo dirConts = new DirectoryInfo(Fileinfo.DirectoryName + "\\Contents");
                    foreach (FileInfo _file in dirConts.GetFiles())
                    {
                        File.Copy(_file.FullName, contPath + _file.Name, true);
                    }

                    string thumPath = rootPath + "\\thumbnails\\";
                    if (!Directory.Exists(thumPath))
                    {
                        Directory.CreateDirectory(thumPath);
                    }
                    DirectoryInfo dirThum = new DirectoryInfo(Fileinfo.DirectoryName + "\\thumbnails");
                    foreach (FileInfo _file in dirThum.GetFiles())
                    {
                        File.Copy(_file.FullName, thumPath + _file.Name, true);
                    }

                }
            }
            catch (Exception ex)
            {
                OneSiteLogsWriter.write(ex);
            }
        }

        private void cb_menuType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cb_menuType.SelectedIndex == 1)
            {
                lb_Header.Visible = true;
                txt_recommandCnt.Visible = true;
            }
            else
            {
                lb_Header.Visible = false;
                txt_recommandCnt.Visible = false;
            }
        }

        private void but_Delete_Click(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.Show("삭제 하시겠습니까?", "템플릿 삭제", MessageBoxButtons.YesNo) == DialogResult.No)
                    return;

                TemplateListLoader loader = new TemplateListLoader();
                template[] arry = new template[1];
                template Data = new template();
                arry[0] = SelectTemplateData;
                loader.DeleteTemplate(arry);
                {
                    AddTemplateItme();
                    Clear();
                }
            }
            catch
            {
            }
        }

        private void but_image_Click(object sender, EventArgs e)
        {
            try
            {

                OpenFileDialog fileDlg = new OpenFileDialog();
                string images = "*.PNG;*.TIF;*.WMF;*.JPG;*.GIF;*.BMP;*.JPEG;*.EMF";
                string avi = "*.MOV;*.MPEG;*.MPG;*.WMA;*.WMV;*.ASF;*.FLV;*.AVI";
                string flash = "*.SWF";

                fileDlg.Title = "컨텐츠 추가";
                fileDlg.Filter = string.Format("{3} ({0};{1};{2})|{0};{1};{2}|{4} ({0})|{0}|{5} (*.SWF)|*.SWF|{6} (*.PPT)|*.PPT|{7} ({2})|{2}|{8}|*.*",
                                                avi, flash, images,
                                                "지원 파일",
                                                "동영상 파일",
                                                "플레쉬 파일",
                                                "파워 포인터 파일",
                                                "이미지 파일",
                                                "모든 파일");

                if (fileDlg.ShowDialog() == DialogResult.OK)
                {
                    FileInfo Fileinfo = new FileInfo(fileDlg.FileName);
                    string newpath = Path.GetDirectoryName(Application.ExecutablePath) + "\\Contents\\" + Fileinfo.Name;
                    File.Copy(fileDlg.FileName, newpath, true);
                    this.text_butimage.Text = Fileinfo.Name;
                    this.Update();
                }
            }
            catch (Exception ex)
            {
                string errmsg = ex.Message;
            }
        }
    }
}
