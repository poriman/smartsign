﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MenuBoardService;

namespace MenuBoard
{
    class TemplateListLoader
    {
        public template_list Load()
        {
            try
            {
                OneSiteService sv = new OneSiteService();
                service_query qur = new service_query();
                qur.node_id = "ALL";
                qur.node_type = "template_list";

                service_query_result svc_result = sv.GetServiceQuery(qur);
                template_list templatelist = svc_result.template_list;

                return templatelist;
            }
            catch (Exception ex)
            {
                string errMsg = ex.Message;
            }

            return null;
        }

        public bool InsertTemplate(template[] _templates)
        {
            try
            {
                OneSiteService sv = new OneSiteService();
                foreach (template _template in _templates)
                {
                    template_update _update = new template_update();
                    _update.job_type = "INSERT";
                    _update.template = _template;

                    update_result result = sv.TemplateUpdate(_update);
                }
            }
            catch (Exception ex)
            {
                string errMsg = ex.Message;
            }

            return false;
        }

        public bool UpdateTemplate(template[] _templates)
        {
            try
            {
                OneSiteService sv = new OneSiteService();
                foreach (template _template in _templates)
                {
                    template_update _update = new template_update();
                    _update.job_type = "UPDATE";
                    _update.template = _template;

                    update_result result = sv.TemplateUpdate(_update);
                }
            }
            catch (Exception ex)
            {
                string errMsg = ex.Message;
            }
            return false;
        }

        public bool DeleteTemplate(template[] _templates)
        {
            try
            {
                OneSiteService sv = new OneSiteService();
                foreach (template _template in _templates)
                {
                    template_update _update = new template_update();
                    _update.job_type = "DELETE";
                    _update.template = _template;

                    update_result result = sv.TemplateUpdate(_update);
                }
            }
            catch (Exception ex)
            {
                string errMsg = ex.Message;
            }
            return false;
        }
    }
}
