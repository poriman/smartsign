﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MenuBoard
{
    public partial class CreateMenuColumDlg : Form
    {
        public CreateMenuColumDlg()
        {
            InitializeComponent();
            cb_type.SelectedIndex = 0;
            tb_fontCount.Text = "0";
        }
        public string Name
        {
            set { tb_menuName.Text = value; }
            get { return tb_menuName.Text; }
        }

        public string FontCount
        {
            set { tb_fontCount.Text = value; }
            get { return tb_fontCount.Text; }
        }

        public string Type
        {
            set { cb_type.SelectedItem = value; }
            get { return cb_type.SelectedItem.ToString(); }
        }

        public string Code
        {
            set { tb_code.Text = value; }
            get { return tb_code.Text; }
        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            if (this.tb_menuName.Text.Length == 0)
            {
                MessageBox.Show("이름을 입력 하십시오.!");
                return;
            }

            if (cb_type.SelectedIndex == -1)
            {
                MessageBox.Show("종류를 선택 하십시오.");
                return;
            }

            if (this.tb_fontCount.Text.Length == 0 )
            {
                MessageBox.Show("최대 글자수를 입력 하십시오.!");
                return;
            }

            if (this.tb_code.Text.Length == 0)
            {
                MessageBox.Show("코드를 입력 하십시오.!");
                return;
            }

            this.DialogResult = DialogResult.OK;
        }

        private void btn_cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void cb_type_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cb_type.SelectedIndex == 0)
            {
                tb_fontCount.ReadOnly = false;
            }
            else
                tb_fontCount.ReadOnly = true;

        }
    }
}
