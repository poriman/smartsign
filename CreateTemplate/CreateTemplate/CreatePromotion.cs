﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Globalization;

namespace MenuBoard
{
    public partial class CreatePromotion : Form
    {
        private string m_Name;
        private string m_Type;
        private string m_Code;
        private string m_size;
        private string m_color;
        private string m_maxcount;

        public string MAXCOUNT
        {
            set 
            { 
                m_maxcount = value;
                tb_maxcount.Text = value;
            }
            get { return m_maxcount; }
        }

        private Color m_FontColor = Color.Black;

        public CreatePromotion()
        {
            InitializeComponent();            
        }

        public string FONTCOLOR
        {
            set 
            { 
                m_color = value;
                int R = Byte.Parse(m_color.Substring(0, 2), NumberStyles.HexNumber);
                int G = Byte.Parse(m_color.Substring(2, 2), NumberStyles.HexNumber);
                int B = Byte.Parse(m_color.Substring(4, 2), NumberStyles.HexNumber);
                m_FontColor = Color.FromArgb(R, G, B);
                this.BTN_FontColor.BackColor = m_FontColor;
                this.BTN_FontColor.Update();
            }
            get 
            {
                m_color = string.Format("{0:X2}{1:X2}{2:X2}", BTN_FontColor.BackColor.R, BTN_FontColor.BackColor.G, BTN_FontColor.BackColor.B);
                return m_color;
            }
        }

        public string FONTSIZE
        {
            set 
            { 
                m_size = value;
                this.CB_FontSize.SelectedItem = value;
                CB_FontSize.Update();
            }
            get 
            {
                m_size = this.CB_FontSize.SelectedItem.ToString();
                return m_size;
            }
        }

        public string _Name
        {
            set { m_Name = value; TB_Name.Text = value; }
            get { return m_Name; }
        }

        public string Type
        {
            set { m_Type = GetType(value); cb_type.SelectedItem = m_Type; }
            get { return m_Type; }
        }

        public string CODE
        {
            set { m_Code = value; TB_Code.Text = value; }
            get { return m_Code; }
        }

        private string GetType(string type)
        {
            switch (type)
            {
                case "I":
                case "이미지":
                    return "이미지";
                case "M":
                case "동영상":
                    return "동영상";
                case "F":
                case "플래쉬":
                    return "플래쉬";
            }
            return "텍스트";
        }

        private void btn_save_Click(object sender, EventArgs e)
        {
            if (TB_Name.Text.Length == 0)
            {
                MessageBox.Show("이름을 입력 하십시오.!");
                TB_Name.Focus();
                return;
            }

            if (cb_type.SelectedIndex == -1)
            {
                MessageBox.Show("종류를 선택 하십시오.");
                cb_type.Focus();
                return;
            }

            if (TB_Code.Text.Length == 0)
            {
                MessageBox.Show("코드을 입력 하십시오.!");
                TB_Code.Focus();
                return;
            }
            if (this.cb_type.SelectedItem.ToString().Equals("텍스트"))
            {
                if (tb_maxcount.Text.Length == 0)
                {
                    MessageBox.Show("최대 글자 수를 입력 하십시오.");
                    tb_maxcount.Focus();
                    return;
                }
                else if (tb_maxcount.Text.Length > 0 && false !=tb_maxcount.Text.Equals("0") && false ==Form1.IsNumeric(tb_maxcount.Text))
                {
                    MessageBox.Show("숫자만 가능 합니다. 다시입력 하십시오.");
                    tb_maxcount.Focus();
                    return;
                }
            }

            _Name = TB_Name.Text;
            Type = cb_type.SelectedItem.ToString();
            CODE = TB_Code.Text;

            this.DialogResult = DialogResult.OK;
        }

        private void cb_type_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.cb_type.SelectedItem.ToString().Equals("텍스트"))
            {
                this.CB_FontSize.Enabled = true;
                this.BTN_FontColor.Enabled = true;
                tb_maxcount.Enabled = true;
            }
            else
            {
                this.CB_FontSize.Enabled = false;
                this.BTN_FontColor.Enabled = false;
                tb_maxcount.Enabled = false;
            }
        }

        private void BTN_FontColor_Click(object sender, EventArgs e)
        {
            try
            {
                using (ColorDialog dlg = new ColorDialog())
                {
                    dlg.Color = m_FontColor;
                    if (dlg.ShowDialog() == DialogResult.OK)
                    {
                        m_FontColor = dlg.Color;
                        this.BTN_FontColor.BackColor = dlg.Color;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Error");
            }
        }
    }
}
