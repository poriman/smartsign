﻿namespace MenuBoard
{
    partial class Form1
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다.
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.cb_TemplateType = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.TB_Count = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.TB_ThumbnailPath = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.Font_listView = new System.Windows.Forms.ListView();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.Menu_Font_Add = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_Font_Modify = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_Font_Delete = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.check_value20 = new System.Windows.Forms.CheckBox();
            this.check_value19 = new System.Windows.Forms.CheckBox();
            this.check_value18 = new System.Windows.Forms.CheckBox();
            this.check_value17 = new System.Windows.Forms.CheckBox();
            this.check_value16 = new System.Windows.Forms.CheckBox();
            this.check_value15 = new System.Windows.Forms.CheckBox();
            this.check_value14 = new System.Windows.Forms.CheckBox();
            this.check_value13 = new System.Windows.Forms.CheckBox();
            this.check_value12 = new System.Windows.Forms.CheckBox();
            this.check_value11 = new System.Windows.Forms.CheckBox();
            this.check_value10 = new System.Windows.Forms.CheckBox();
            this.check_value09 = new System.Windows.Forms.CheckBox();
            this.check_value08 = new System.Windows.Forms.CheckBox();
            this.check_value07 = new System.Windows.Forms.CheckBox();
            this.check_value06 = new System.Windows.Forms.CheckBox();
            this.check_value05 = new System.Windows.Forms.CheckBox();
            this.check_value04 = new System.Windows.Forms.CheckBox();
            this.check_value03 = new System.Windows.Forms.CheckBox();
            this.check_value02 = new System.Windows.Forms.CheckBox();
            this.check_value01 = new System.Windows.Forms.CheckBox();
            this.but_image = new System.Windows.Forms.Button();
            this.text_butimage = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.cb_backimage = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.cb_weather = new System.Windows.Forms.ComboBox();
            this.but_Delete = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.cb_logo = new System.Windows.Forms.ComboBox();
            this.lb_Header = new System.Windows.Forms.Label();
            this.cb_menuType = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txt_recommandCnt = new System.Windows.Forms.TextBox();
            this.BTN_Add_Template = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.txt_Name = new System.Windows.Forms.TextBox();
            this.BTN_AddFile = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.TB_exFileName = new System.Windows.Forms.TextBox();
            this.BTN_imagePath = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.cb_Form = new System.Windows.Forms.ComboBox();
            this.BTN_NewSave = new System.Windows.Forms.Button();
            this.TB_Decs = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.TB_Name = new System.Windows.Forms.TextBox();
            this.btn_Cancel = new System.Windows.Forms.Button();
            this.btn_Save = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.ProMosion_listView = new System.Windows.Forms.ListView();
            this.menuStrip2 = new System.Windows.Forms.MenuStrip();
            this.Menu_PM_Add = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_PM_Modify = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_PM_Delete = new System.Windows.Forms.ToolStripMenuItem();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.Template_listView = new System.Windows.Forms.ListView();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.splitContainer3 = new System.Windows.Forms.SplitContainer();
            this.groupBox1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.menuStrip2.SuspendLayout();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.splitContainer3.Panel1.SuspendLayout();
            this.splitContainer3.Panel2.SuspendLayout();
            this.splitContainer3.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(95, 102);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "종류";
            // 
            // cb_TemplateType
            // 
            this.cb_TemplateType.FormattingEnabled = true;
            this.cb_TemplateType.Items.AddRange(new object[] {
            "메뉴",
            "프로모션",
            "메뉴 + 프로모션",
            "리스트타입"});
            this.cb_TemplateType.Location = new System.Drawing.Point(131, 98);
            this.cb_TemplateType.Name = "cb_TemplateType";
            this.cb_TemplateType.Size = new System.Drawing.Size(123, 20);
            this.cb_TemplateType.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(261, 101);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(57, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "메뉴 개수";
            // 
            // TB_Count
            // 
            this.TB_Count.Location = new System.Drawing.Point(324, 97);
            this.TB_Count.Name = "TB_Count";
            this.TB_Count.Size = new System.Drawing.Size(54, 21);
            this.TB_Count.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(55, 146);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 12);
            this.label3.TabIndex = 4;
            this.label3.Text = "썸네일 이름";
            // 
            // TB_ThumbnailPath
            // 
            this.TB_ThumbnailPath.Location = new System.Drawing.Point(132, 140);
            this.TB_ThumbnailPath.Name = "TB_ThumbnailPath";
            this.TB_ThumbnailPath.ReadOnly = true;
            this.TB_ThumbnailPath.Size = new System.Drawing.Size(192, 21);
            this.TB_ThumbnailPath.TabIndex = 5;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.Font_listView);
            this.groupBox1.Controls.Add(this.menuStrip1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(513, 191);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "반복 메뉴 설정";
            // 
            // Font_listView
            // 
            this.Font_listView.FullRowSelect = true;
            this.Font_listView.GridLines = true;
            this.Font_listView.Location = new System.Drawing.Point(3, 41);
            this.Font_listView.MultiSelect = false;
            this.Font_listView.Name = "Font_listView";
            this.Font_listView.Size = new System.Drawing.Size(505, 167);
            this.Font_listView.TabIndex = 0;
            this.Font_listView.UseCompatibleStateImageBehavior = false;
            this.Font_listView.View = System.Windows.Forms.View.Details;
            this.Font_listView.DoubleClick += new System.EventHandler(this.Font_listView_DoubleClick);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Menu_Font_Add,
            this.Menu_Font_Modify,
            this.Menu_Font_Delete});
            this.menuStrip1.Location = new System.Drawing.Point(3, 17);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(507, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // Menu_Font_Add
            // 
            this.Menu_Font_Add.Name = "Menu_Font_Add";
            this.Menu_Font_Add.Size = new System.Drawing.Size(43, 20);
            this.Menu_Font_Add.Text = "추가";
            this.Menu_Font_Add.Click += new System.EventHandler(this.Menu_Font_Add_Click);
            // 
            // Menu_Font_Modify
            // 
            this.Menu_Font_Modify.Name = "Menu_Font_Modify";
            this.Menu_Font_Modify.Size = new System.Drawing.Size(43, 20);
            this.Menu_Font_Modify.Text = "수정";
            this.Menu_Font_Modify.Click += new System.EventHandler(this.Menu_Font_Modify_Click);
            // 
            // Menu_Font_Delete
            // 
            this.Menu_Font_Delete.Name = "Menu_Font_Delete";
            this.Menu_Font_Delete.Size = new System.Drawing.Size(43, 20);
            this.Menu_Font_Delete.Text = "삭제";
            this.Menu_Font_Delete.Click += new System.EventHandler(this.Menu_Font_Delete_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.groupBox4);
            this.groupBox2.Controls.Add(this.but_image);
            this.groupBox2.Controls.Add(this.text_butimage);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.cb_backimage);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.cb_weather);
            this.groupBox2.Controls.Add(this.but_Delete);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.cb_logo);
            this.groupBox2.Controls.Add(this.lb_Header);
            this.groupBox2.Controls.Add(this.cb_menuType);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.txt_recommandCnt);
            this.groupBox2.Controls.Add(this.BTN_Add_Template);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.txt_Name);
            this.groupBox2.Controls.Add(this.BTN_AddFile);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.TB_exFileName);
            this.groupBox2.Controls.Add(this.BTN_imagePath);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.cb_Form);
            this.groupBox2.Controls.Add(this.BTN_NewSave);
            this.groupBox2.Controls.Add(this.TB_Decs);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.TB_Name);
            this.groupBox2.Controls.Add(this.btn_Cancel);
            this.groupBox2.Controls.Add(this.btn_Save);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.TB_ThumbnailPath);
            this.groupBox2.Controls.Add(this.cb_TemplateType);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.TB_Count);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(0, 0);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(513, 330);
            this.groupBox2.TabIndex = 8;
            this.groupBox2.TabStop = false;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.check_value20);
            this.groupBox4.Controls.Add(this.check_value19);
            this.groupBox4.Controls.Add(this.check_value18);
            this.groupBox4.Controls.Add(this.check_value17);
            this.groupBox4.Controls.Add(this.check_value16);
            this.groupBox4.Controls.Add(this.check_value15);
            this.groupBox4.Controls.Add(this.check_value14);
            this.groupBox4.Controls.Add(this.check_value13);
            this.groupBox4.Controls.Add(this.check_value12);
            this.groupBox4.Controls.Add(this.check_value11);
            this.groupBox4.Controls.Add(this.check_value10);
            this.groupBox4.Controls.Add(this.check_value09);
            this.groupBox4.Controls.Add(this.check_value08);
            this.groupBox4.Controls.Add(this.check_value07);
            this.groupBox4.Controls.Add(this.check_value06);
            this.groupBox4.Controls.Add(this.check_value05);
            this.groupBox4.Controls.Add(this.check_value04);
            this.groupBox4.Controls.Add(this.check_value03);
            this.groupBox4.Controls.Add(this.check_value02);
            this.groupBox4.Controls.Add(this.check_value01);
            this.groupBox4.Location = new System.Drawing.Point(6, 236);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(500, 88);
            this.groupBox4.TabIndex = 77;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "카테고리 설정";
            // 
            // check_value20
            // 
            this.check_value20.AutoSize = true;
            this.check_value20.Location = new System.Drawing.Point(195, 66);
            this.check_value20.Name = "check_value20";
            this.check_value20.Size = new System.Drawing.Size(36, 16);
            this.check_value20.TabIndex = 96;
            this.check_value20.Text = "20";
            this.check_value20.UseVisualStyleBackColor = true;
            // 
            // check_value19
            // 
            this.check_value19.AutoSize = true;
            this.check_value19.Location = new System.Drawing.Point(140, 66);
            this.check_value19.Name = "check_value19";
            this.check_value19.Size = new System.Drawing.Size(36, 16);
            this.check_value19.TabIndex = 95;
            this.check_value19.Text = "19";
            this.check_value19.UseVisualStyleBackColor = true;
            // 
            // check_value18
            // 
            this.check_value18.AutoSize = true;
            this.check_value18.Location = new System.Drawing.Point(73, 66);
            this.check_value18.Name = "check_value18";
            this.check_value18.Size = new System.Drawing.Size(36, 16);
            this.check_value18.TabIndex = 94;
            this.check_value18.Text = "18";
            this.check_value18.UseVisualStyleBackColor = true;
            // 
            // check_value17
            // 
            this.check_value17.AutoSize = true;
            this.check_value17.Location = new System.Drawing.Point(10, 66);
            this.check_value17.Name = "check_value17";
            this.check_value17.Size = new System.Drawing.Size(36, 16);
            this.check_value17.TabIndex = 93;
            this.check_value17.Text = "17";
            this.check_value17.UseVisualStyleBackColor = true;
            // 
            // check_value16
            // 
            this.check_value16.AutoSize = true;
            this.check_value16.Location = new System.Drawing.Point(429, 44);
            this.check_value16.Name = "check_value16";
            this.check_value16.Size = new System.Drawing.Size(36, 16);
            this.check_value16.TabIndex = 92;
            this.check_value16.Text = "16";
            this.check_value16.UseVisualStyleBackColor = true;
            // 
            // check_value15
            // 
            this.check_value15.AutoSize = true;
            this.check_value15.Location = new System.Drawing.Point(373, 45);
            this.check_value15.Name = "check_value15";
            this.check_value15.Size = new System.Drawing.Size(36, 16);
            this.check_value15.TabIndex = 91;
            this.check_value15.Text = "15";
            this.check_value15.UseVisualStyleBackColor = true;
            // 
            // check_value14
            // 
            this.check_value14.AutoSize = true;
            this.check_value14.Location = new System.Drawing.Point(312, 45);
            this.check_value14.Name = "check_value14";
            this.check_value14.Size = new System.Drawing.Size(36, 16);
            this.check_value14.TabIndex = 90;
            this.check_value14.Text = "14";
            this.check_value14.UseVisualStyleBackColor = true;
            // 
            // check_value13
            // 
            this.check_value13.AutoSize = true;
            this.check_value13.Location = new System.Drawing.Point(253, 44);
            this.check_value13.Name = "check_value13";
            this.check_value13.Size = new System.Drawing.Size(48, 16);
            this.check_value13.TabIndex = 89;
            this.check_value13.Text = "교통";
            this.check_value13.UseVisualStyleBackColor = true;
            // 
            // check_value12
            // 
            this.check_value12.AutoSize = true;
            this.check_value12.Location = new System.Drawing.Point(195, 44);
            this.check_value12.Name = "check_value12";
            this.check_value12.Size = new System.Drawing.Size(48, 16);
            this.check_value12.TabIndex = 88;
            this.check_value12.Text = "유통";
            this.check_value12.UseVisualStyleBackColor = true;
            // 
            // check_value11
            // 
            this.check_value11.AutoSize = true;
            this.check_value11.Location = new System.Drawing.Point(140, 44);
            this.check_value11.Name = "check_value11";
            this.check_value11.Size = new System.Drawing.Size(60, 16);
            this.check_value11.TabIndex = 87;
            this.check_value11.Text = "음식점";
            this.check_value11.UseVisualStyleBackColor = true;
            // 
            // check_value10
            // 
            this.check_value10.AutoSize = true;
            this.check_value10.Location = new System.Drawing.Point(73, 44);
            this.check_value10.Name = "check_value10";
            this.check_value10.Size = new System.Drawing.Size(72, 16);
            this.check_value10.TabIndex = 86;
            this.check_value10.Text = "공공장소";
            this.check_value10.UseVisualStyleBackColor = true;
            // 
            // check_value09
            // 
            this.check_value09.AutoSize = true;
            this.check_value09.Location = new System.Drawing.Point(10, 44);
            this.check_value09.Name = "check_value09";
            this.check_value09.Size = new System.Drawing.Size(60, 16);
            this.check_value09.TabIndex = 85;
            this.check_value09.Text = "박물관";
            this.check_value09.UseVisualStyleBackColor = true;
            // 
            // check_value08
            // 
            this.check_value08.AutoSize = true;
            this.check_value08.Location = new System.Drawing.Point(429, 19);
            this.check_value08.Name = "check_value08";
            this.check_value08.Size = new System.Drawing.Size(48, 16);
            this.check_value08.TabIndex = 84;
            this.check_value08.Text = "종교";
            this.check_value08.UseVisualStyleBackColor = true;
            // 
            // check_value07
            // 
            this.check_value07.AutoSize = true;
            this.check_value07.Location = new System.Drawing.Point(373, 20);
            this.check_value07.Name = "check_value07";
            this.check_value07.Size = new System.Drawing.Size(48, 16);
            this.check_value07.TabIndex = 83;
            this.check_value07.Text = "숙박";
            this.check_value07.UseVisualStyleBackColor = true;
            // 
            // check_value06
            // 
            this.check_value06.AutoSize = true;
            this.check_value06.Location = new System.Drawing.Point(312, 20);
            this.check_value06.Name = "check_value06";
            this.check_value06.Size = new System.Drawing.Size(48, 16);
            this.check_value06.TabIndex = 82;
            this.check_value06.Text = "의료";
            this.check_value06.UseVisualStyleBackColor = true;
            // 
            // check_value05
            // 
            this.check_value05.AutoSize = true;
            this.check_value05.Location = new System.Drawing.Point(253, 20);
            this.check_value05.Name = "check_value05";
            this.check_value05.Size = new System.Drawing.Size(48, 16);
            this.check_value05.TabIndex = 81;
            this.check_value05.Text = "정부";
            this.check_value05.UseVisualStyleBackColor = true;
            // 
            // check_value04
            // 
            this.check_value04.AutoSize = true;
            this.check_value04.Location = new System.Drawing.Point(195, 20);
            this.check_value04.Name = "check_value04";
            this.check_value04.Size = new System.Drawing.Size(48, 16);
            this.check_value04.TabIndex = 80;
            this.check_value04.Text = "교육";
            this.check_value04.UseVisualStyleBackColor = true;
            // 
            // check_value03
            // 
            this.check_value03.AutoSize = true;
            this.check_value03.Location = new System.Drawing.Point(140, 20);
            this.check_value03.Name = "check_value03";
            this.check_value03.Size = new System.Drawing.Size(48, 16);
            this.check_value03.TabIndex = 79;
            this.check_value03.Text = "사내";
            this.check_value03.UseVisualStyleBackColor = true;
            // 
            // check_value02
            // 
            this.check_value02.AutoSize = true;
            this.check_value02.Location = new System.Drawing.Point(73, 20);
            this.check_value02.Name = "check_value02";
            this.check_value02.Size = new System.Drawing.Size(48, 16);
            this.check_value02.TabIndex = 78;
            this.check_value02.Text = "금융";
            this.check_value02.UseVisualStyleBackColor = true;
            // 
            // check_value01
            // 
            this.check_value01.AutoSize = true;
            this.check_value01.Location = new System.Drawing.Point(10, 20);
            this.check_value01.Name = "check_value01";
            this.check_value01.Size = new System.Drawing.Size(48, 16);
            this.check_value01.TabIndex = 77;
            this.check_value01.Text = "여가";
            this.check_value01.UseVisualStyleBackColor = true;
            // 
            // but_image
            // 
            this.but_image.Location = new System.Drawing.Point(328, 162);
            this.but_image.Name = "but_image";
            this.but_image.Size = new System.Drawing.Size(50, 20);
            this.but_image.TabIndex = 36;
            this.but_image.Text = "...";
            this.but_image.UseVisualStyleBackColor = true;
            this.but_image.Click += new System.EventHandler(this.but_image_Click);
            // 
            // text_butimage
            // 
            this.text_butimage.Location = new System.Drawing.Point(132, 162);
            this.text_butimage.Name = "text_butimage";
            this.text_butimage.ReadOnly = true;
            this.text_butimage.Size = new System.Drawing.Size(192, 21);
            this.text_butimage.TabIndex = 35;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(55, 168);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(69, 12);
            this.label13.TabIndex = 34;
            this.label13.Text = "버튼 이미지";
            // 
            // cb_backimage
            // 
            this.cb_backimage.FormattingEnabled = true;
            this.cb_backimage.Items.AddRange(new object[] {
            "use",
            "unused"});
            this.cb_backimage.Location = new System.Drawing.Point(400, 183);
            this.cb_backimage.Name = "cb_backimage";
            this.cb_backimage.Size = new System.Drawing.Size(76, 20);
            this.cb_backimage.TabIndex = 33;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(326, 190);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(69, 12);
            this.label12.TabIndex = 32;
            this.label12.Text = "배경 이미지";
            // 
            // cb_weather
            // 
            this.cb_weather.FormattingEnabled = true;
            this.cb_weather.Items.AddRange(new object[] {
            "use",
            "unused"});
            this.cb_weather.Location = new System.Drawing.Point(249, 183);
            this.cb_weather.Name = "cb_weather";
            this.cb_weather.Size = new System.Drawing.Size(69, 20);
            this.cb_weather.TabIndex = 31;
            // 
            // but_Delete
            // 
            this.but_Delete.Location = new System.Drawing.Point(401, 116);
            this.but_Delete.Name = "but_Delete";
            this.but_Delete.Size = new System.Drawing.Size(105, 29);
            this.but_Delete.TabIndex = 30;
            this.but_Delete.Text = "삭제";
            this.but_Delete.UseVisualStyleBackColor = true;
            this.but_Delete.Click += new System.EventHandler(this.but_Delete_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(214, 190);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(29, 12);
            this.label10.TabIndex = 28;
            this.label10.Text = "날씨";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(67, 190);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(57, 12);
            this.label11.TabIndex = 26;
            this.label11.Text = "로고 유무";
            // 
            // cb_logo
            // 
            this.cb_logo.FormattingEnabled = true;
            this.cb_logo.Items.AddRange(new object[] {
            "use",
            "unused"});
            this.cb_logo.Location = new System.Drawing.Point(132, 183);
            this.cb_logo.Name = "cb_logo";
            this.cb_logo.Size = new System.Drawing.Size(76, 20);
            this.cb_logo.TabIndex = 27;
            // 
            // lb_Header
            // 
            this.lb_Header.AutoSize = true;
            this.lb_Header.Location = new System.Drawing.Point(261, 123);
            this.lb_Header.Name = "lb_Header";
            this.lb_Header.Size = new System.Drawing.Size(57, 12);
            this.lb_Header.TabIndex = 25;
            this.lb_Header.Text = "헤더 정보";
            this.lb_Header.Visible = false;
            // 
            // cb_menuType
            // 
            this.cb_menuType.FormattingEnabled = true;
            this.cb_menuType.Items.AddRange(new object[] {
            "메뉴",
            "설문조사"});
            this.cb_menuType.Location = new System.Drawing.Point(131, 119);
            this.cb_menuType.Name = "cb_menuType";
            this.cb_menuType.Size = new System.Drawing.Size(123, 20);
            this.cb_menuType.TabIndex = 24;
            this.cb_menuType.SelectedIndexChanged += new System.EventHandler(this.cb_menuType_SelectedIndexChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(55, 123);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(69, 12);
            this.label9.TabIndex = 22;
            this.label9.Text = "템플릿 타입";
            // 
            // txt_recommandCnt
            // 
            this.txt_recommandCnt.Location = new System.Drawing.Point(324, 118);
            this.txt_recommandCnt.Name = "txt_recommandCnt";
            this.txt_recommandCnt.Size = new System.Drawing.Size(54, 21);
            this.txt_recommandCnt.TabIndex = 23;
            this.txt_recommandCnt.Visible = false;
            // 
            // BTN_Add_Template
            // 
            this.BTN_Add_Template.Location = new System.Drawing.Point(400, 9);
            this.BTN_Add_Template.Name = "BTN_Add_Template";
            this.BTN_Add_Template.Size = new System.Drawing.Size(105, 29);
            this.BTN_Add_Template.TabIndex = 21;
            this.BTN_Add_Template.Text = "템플릿 추가";
            this.BTN_Add_Template.UseVisualStyleBackColor = true;
            this.BTN_Add_Template.Click += new System.EventHandler(this.BTN_Add_Template_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(55, 16);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(69, 12);
            this.label8.TabIndex = 19;
            this.label8.Text = "템플릿 이름";
            // 
            // txt_Name
            // 
            this.txt_Name.Location = new System.Drawing.Point(130, 11);
            this.txt_Name.Name = "txt_Name";
            this.txt_Name.Size = new System.Drawing.Size(248, 21);
            this.txt_Name.TabIndex = 20;
            // 
            // BTN_AddFile
            // 
            this.BTN_AddFile.Location = new System.Drawing.Point(328, 33);
            this.BTN_AddFile.Name = "BTN_AddFile";
            this.BTN_AddFile.Size = new System.Drawing.Size(50, 20);
            this.BTN_AddFile.TabIndex = 18;
            this.BTN_AddFile.Text = "...";
            this.BTN_AddFile.UseVisualStyleBackColor = true;
            this.BTN_AddFile.Click += new System.EventHandler(this.BTN_AddFile_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(43, 59);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(81, 12);
            this.label7.TabIndex = 16;
            this.label7.Text = "외부 파일이름";
            // 
            // TB_exFileName
            // 
            this.TB_exFileName.Location = new System.Drawing.Point(130, 55);
            this.TB_exFileName.Name = "TB_exFileName";
            this.TB_exFileName.Size = new System.Drawing.Size(248, 21);
            this.TB_exFileName.TabIndex = 17;
            // 
            // BTN_imagePath
            // 
            this.BTN_imagePath.Location = new System.Drawing.Point(328, 140);
            this.BTN_imagePath.Name = "BTN_imagePath";
            this.BTN_imagePath.Size = new System.Drawing.Size(50, 20);
            this.BTN_imagePath.TabIndex = 15;
            this.BTN_imagePath.Text = "...";
            this.BTN_imagePath.UseVisualStyleBackColor = true;
            this.BTN_imagePath.Click += new System.EventHandler(this.BTN_imagePath_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(31, 81);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(93, 12);
            this.label6.TabIndex = 13;
            this.label6.Text = "디스플레이 유형";
            // 
            // cb_Form
            // 
            this.cb_Form.FormattingEnabled = true;
            this.cb_Form.Items.AddRange(new object[] {
            "가로형",
            "세로형"});
            this.cb_Form.Location = new System.Drawing.Point(131, 77);
            this.cb_Form.Name = "cb_Form";
            this.cb_Form.Size = new System.Drawing.Size(247, 20);
            this.cb_Form.TabIndex = 14;
            this.cb_Form.SelectedIndexChanged += new System.EventHandler(this.cb_Form_SelectedIndexChanged);
            // 
            // BTN_NewSave
            // 
            this.BTN_NewSave.Location = new System.Drawing.Point(400, 44);
            this.BTN_NewSave.Name = "BTN_NewSave";
            this.BTN_NewSave.Size = new System.Drawing.Size(105, 29);
            this.BTN_NewSave.TabIndex = 12;
            this.BTN_NewSave.Text = "다른 이름 저장";
            this.BTN_NewSave.UseVisualStyleBackColor = true;
            this.BTN_NewSave.Click += new System.EventHandler(this.BTN_NewSave_Click);
            // 
            // TB_Decs
            // 
            this.TB_Decs.Location = new System.Drawing.Point(131, 207);
            this.TB_Decs.Multiline = true;
            this.TB_Decs.Name = "TB_Decs";
            this.TB_Decs.Size = new System.Drawing.Size(375, 32);
            this.TB_Decs.TabIndex = 11;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(95, 211);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 12);
            this.label5.TabIndex = 10;
            this.label5.Text = "설명";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(27, 37);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(97, 12);
            this.label4.TabIndex = 8;
            this.label4.Text = "플래쉬 파일 이름";
            // 
            // TB_Name
            // 
            this.TB_Name.Location = new System.Drawing.Point(130, 33);
            this.TB_Name.Name = "TB_Name";
            this.TB_Name.ReadOnly = true;
            this.TB_Name.Size = new System.Drawing.Size(194, 21);
            this.TB_Name.TabIndex = 9;
            // 
            // btn_Cancel
            // 
            this.btn_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btn_Cancel.Location = new System.Drawing.Point(402, 149);
            this.btn_Cancel.Name = "btn_Cancel";
            this.btn_Cancel.Size = new System.Drawing.Size(105, 29);
            this.btn_Cancel.TabIndex = 7;
            this.btn_Cancel.Text = "종료";
            this.btn_Cancel.UseVisualStyleBackColor = true;
            this.btn_Cancel.Click += new System.EventHandler(this.btn_Cancel_Click);
            // 
            // btn_Save
            // 
            this.btn_Save.Location = new System.Drawing.Point(400, 81);
            this.btn_Save.Name = "btn_Save";
            this.btn_Save.Size = new System.Drawing.Size(105, 29);
            this.btn_Save.TabIndex = 6;
            this.btn_Save.Text = "저장";
            this.btn_Save.UseVisualStyleBackColor = true;
            this.btn_Save.Click += new System.EventHandler(this.btn_Save_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.ProMosion_listView);
            this.groupBox3.Controls.Add(this.menuStrip2);
            this.groupBox3.Location = new System.Drawing.Point(0, 0);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(513, 213);
            this.groupBox3.TabIndex = 9;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "고정 데이터";
            // 
            // ProMosion_listView
            // 
            this.ProMosion_listView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ProMosion_listView.FullRowSelect = true;
            this.ProMosion_listView.GridLines = true;
            this.ProMosion_listView.Location = new System.Drawing.Point(3, 41);
            this.ProMosion_listView.Name = "ProMosion_listView";
            this.ProMosion_listView.Size = new System.Drawing.Size(507, 169);
            this.ProMosion_listView.TabIndex = 0;
            this.ProMosion_listView.UseCompatibleStateImageBehavior = false;
            this.ProMosion_listView.View = System.Windows.Forms.View.Details;
            this.ProMosion_listView.DoubleClick += new System.EventHandler(this.ProMosion_listView_DoubleClick);
            // 
            // menuStrip2
            // 
            this.menuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Menu_PM_Add,
            this.Menu_PM_Modify,
            this.Menu_PM_Delete});
            this.menuStrip2.Location = new System.Drawing.Point(3, 17);
            this.menuStrip2.Name = "menuStrip2";
            this.menuStrip2.Size = new System.Drawing.Size(507, 24);
            this.menuStrip2.TabIndex = 1;
            this.menuStrip2.Text = "menuStrip2";
            // 
            // Menu_PM_Add
            // 
            this.Menu_PM_Add.Name = "Menu_PM_Add";
            this.Menu_PM_Add.Size = new System.Drawing.Size(43, 20);
            this.Menu_PM_Add.Text = "추가";
            this.Menu_PM_Add.Click += new System.EventHandler(this.Menu_PM_Add_Click);
            // 
            // Menu_PM_Modify
            // 
            this.Menu_PM_Modify.Name = "Menu_PM_Modify";
            this.Menu_PM_Modify.Size = new System.Drawing.Size(43, 20);
            this.Menu_PM_Modify.Text = "수정";
            this.Menu_PM_Modify.Click += new System.EventHandler(this.Menu_PM_Modify_Click);
            // 
            // Menu_PM_Delete
            // 
            this.Menu_PM_Delete.Name = "Menu_PM_Delete";
            this.Menu_PM_Delete.Size = new System.Drawing.Size(43, 20);
            this.Menu_PM_Delete.Text = "삭제";
            this.Menu_PM_Delete.Click += new System.EventHandler(this.Menu_PM_Delete_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.Template_listView);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(752, 708);
            this.splitContainer1.SplitterDistance = 235;
            this.splitContainer1.TabIndex = 10;
            // 
            // Template_listView
            // 
            this.Template_listView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Template_listView.FullRowSelect = true;
            this.Template_listView.GridLines = true;
            this.Template_listView.Location = new System.Drawing.Point(0, 0);
            this.Template_listView.MultiSelect = false;
            this.Template_listView.Name = "Template_listView";
            this.Template_listView.Size = new System.Drawing.Size(235, 708);
            this.Template_listView.TabIndex = 0;
            this.Template_listView.UseCompatibleStateImageBehavior = false;
            this.Template_listView.View = System.Windows.Forms.View.Details;
            this.Template_listView.SelectedIndexChanged += new System.EventHandler(this.Template_listView_SelectedIndexChanged);
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.groupBox2);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.splitContainer3);
            this.splitContainer2.Size = new System.Drawing.Size(513, 708);
            this.splitContainer2.SplitterDistance = 330;
            this.splitContainer2.TabIndex = 10;
            // 
            // splitContainer3
            // 
            this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer3.Location = new System.Drawing.Point(0, 0);
            this.splitContainer3.Name = "splitContainer3";
            this.splitContainer3.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer3.Panel1
            // 
            this.splitContainer3.Panel1.Controls.Add(this.groupBox1);
            // 
            // splitContainer3.Panel2
            // 
            this.splitContainer3.Panel2.Controls.Add(this.groupBox3);
            this.splitContainer3.Size = new System.Drawing.Size(513, 374);
            this.splitContainer3.SplitterDistance = 191;
            this.splitContainer3.TabIndex = 10;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(752, 708);
            this.Controls.Add(this.splitContainer1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "템플릿 생성";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.menuStrip2.ResumeLayout(false);
            this.menuStrip2.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.ResumeLayout(false);
            this.splitContainer3.Panel1.ResumeLayout(false);
            this.splitContainer3.Panel2.ResumeLayout(false);
            this.splitContainer3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cb_TemplateType;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TB_Count;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox TB_ThumbnailPath;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ListView Font_listView;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btn_Cancel;
        private System.Windows.Forms.Button btn_Save;
        private System.Windows.Forms.ListView ProMosion_listView;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TextBox TB_Decs;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox TB_Name;
        private System.Windows.Forms.ListView Template_listView;
        private System.Windows.Forms.Button BTN_NewSave;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.SplitContainer splitContainer3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cb_Form;
        private System.Windows.Forms.Button BTN_imagePath;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem Menu_Font_Add;
        private System.Windows.Forms.ToolStripMenuItem Menu_Font_Modify;
        private System.Windows.Forms.ToolStripMenuItem Menu_Font_Delete;
        private System.Windows.Forms.MenuStrip menuStrip2;
        private System.Windows.Forms.ToolStripMenuItem Menu_PM_Add;
        private System.Windows.Forms.ToolStripMenuItem Menu_PM_Modify;
        private System.Windows.Forms.ToolStripMenuItem Menu_PM_Delete;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox TB_exFileName;
        private System.Windows.Forms.Button BTN_AddFile;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txt_Name;
        private System.Windows.Forms.Button BTN_Add_Template;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txt_recommandCnt;
        private System.Windows.Forms.ComboBox cb_menuType;
        private System.Windows.Forms.Label lb_Header;
        private System.Windows.Forms.ComboBox cb_weather;
        private System.Windows.Forms.Button but_Delete;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox cb_logo;
        private System.Windows.Forms.ComboBox cb_backimage;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button but_image;
        private System.Windows.Forms.TextBox text_butimage;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.CheckBox check_value01;
        private System.Windows.Forms.CheckBox check_value10;
        private System.Windows.Forms.CheckBox check_value09;
        private System.Windows.Forms.CheckBox check_value08;
        private System.Windows.Forms.CheckBox check_value07;
        private System.Windows.Forms.CheckBox check_value06;
        private System.Windows.Forms.CheckBox check_value05;
        private System.Windows.Forms.CheckBox check_value04;
        private System.Windows.Forms.CheckBox check_value03;
        private System.Windows.Forms.CheckBox check_value02;
        private System.Windows.Forms.CheckBox check_value20;
        private System.Windows.Forms.CheckBox check_value19;
        private System.Windows.Forms.CheckBox check_value18;
        private System.Windows.Forms.CheckBox check_value17;
        private System.Windows.Forms.CheckBox check_value16;
        private System.Windows.Forms.CheckBox check_value15;
        private System.Windows.Forms.CheckBox check_value14;
        private System.Windows.Forms.CheckBox check_value13;
        private System.Windows.Forms.CheckBox check_value12;
        private System.Windows.Forms.CheckBox check_value11;
    }
}

