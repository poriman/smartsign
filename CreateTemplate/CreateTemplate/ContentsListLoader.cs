﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using MenuBoardService;

namespace MenuBoard
{  
    class ContentsListLoader
    {
        public bool InsertContents(contents[] _contents)
        {
            try
            {
                OneSiteService sv = new OneSiteService();
                foreach (contents _con in _contents)
                {
                    contents_update _update = new contents_update();
                    _update.job_type = "INSERT";
                    _update.contents = _con;

                    update_result result = sv.ContentUpdate(_update);
                }
                return true;
            }
            catch (Exception ex)
            {
                string errMsg = ex.Message;
            }

            return false;
        }

        public bool UpdateContents(contents[] _contents)
        {
            try
            {
                OneSiteService sv = new OneSiteService();
                foreach (contents _con in _contents)
                {
                    contents_update _update = new contents_update();
                    _update.job_type = "UPDATE";
                    _update.contents = _con;

                    update_result result = sv.ContentUpdate(_update);
                }

                return true;
            }
            catch (Exception ex)
            {
                string errMsg = ex.Message;
            }
            return false;
        }

        public bool DeleteContents(contents[] _contents)
        {
            try
            {
                OneSiteService sv = new OneSiteService();
                foreach (contents _con in _contents)
                {
                    contents_update _update = new contents_update();
                    _update.job_type = "DELETE";
                    _update.contents = _con;

                    update_result result = sv.ContentUpdate(_update);
                }

                return true;
            }
            catch (Exception ex)
            {
                string errMsg = ex.Message;
            }
            return false;
        }
    }
}
