﻿namespace MenuBoard
{
    partial class MFontDlg
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다.
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MFontDlg));
            this.BTN_Save = new System.Windows.Forms.Button();
            this.BTN_Cancel = new System.Windows.Forms.Button();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.StripCB_Font = new System.Windows.Forms.ToolStripComboBox();
            this.StripCB_FontSize = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.StripBTN_FontColor = new System.Windows.Forms.ToolStripButton();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.CB_Type = new System.Windows.Forms.ToolStripComboBox();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // BTN_Save
            // 
            this.BTN_Save.Location = new System.Drawing.Point(184, 148);
            this.BTN_Save.Name = "BTN_Save";
            this.BTN_Save.Size = new System.Drawing.Size(85, 28);
            this.BTN_Save.TabIndex = 0;
            this.BTN_Save.Text = "저장";
            this.BTN_Save.UseVisualStyleBackColor = true;
            this.BTN_Save.Click += new System.EventHandler(this.BTN_Save_Click);
            // 
            // BTN_Cancel
            // 
            this.BTN_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.BTN_Cancel.Location = new System.Drawing.Point(275, 148);
            this.BTN_Cancel.Name = "BTN_Cancel";
            this.BTN_Cancel.Size = new System.Drawing.Size(85, 28);
            this.BTN_Cancel.TabIndex = 1;
            this.BTN_Cancel.Text = "취소";
            this.BTN_Cancel.UseVisualStyleBackColor = true;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StripCB_Font,
            this.StripCB_FontSize,
            this.toolStripSeparator1,
            this.StripBTN_FontColor,
            this.CB_Type});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(365, 27);
            this.toolStrip1.TabIndex = 2;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // StripCB_Font
            // 
            this.StripCB_Font.Name = "StripCB_Font";
            this.StripCB_Font.Size = new System.Drawing.Size(121, 26);
            this.StripCB_Font.ToolTipText = "폰트설정";
            this.StripCB_Font.SelectedIndexChanged += new System.EventHandler(this.StripCB_Font_SelectedIndexChanged);
            // 
            // StripCB_FontSize
            // 
            this.StripCB_FontSize.Items.AddRange(new object[] {
            "8",
            "9",
            "10",
            "11",
            "12",
            "14",
            "16",
            "18",
            "20",
            "22",
            "24",
            "26",
            "28",
            "36",
            "41",
            "48",
            "72"});
            this.StripCB_FontSize.Name = "StripCB_FontSize";
            this.StripCB_FontSize.Size = new System.Drawing.Size(75, 26);
            this.StripCB_FontSize.ToolTipText = "폰트크기 설정";
            this.StripCB_FontSize.SelectedIndexChanged += new System.EventHandler(this.StripCB_FontSize_SelectedIndexChanged);
            this.StripCB_FontSize.TextChanged += new System.EventHandler(this.StripCB_FontSize_TextChanged);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 26);
            // 
            // StripBTN_FontColor
            // 
            this.StripBTN_FontColor.BackColor = System.Drawing.Color.Black;
            this.StripBTN_FontColor.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.StripBTN_FontColor.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.StripBTN_FontColor.ForeColor = System.Drawing.Color.White;
            this.StripBTN_FontColor.Image = ((System.Drawing.Image)(resources.GetObject("StripBTN_FontColor.Image")));
            this.StripBTN_FontColor.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.StripBTN_FontColor.Name = "StripBTN_FontColor";
            this.StripBTN_FontColor.Size = new System.Drawing.Size(25, 24);
            this.StripBTN_FontColor.Text = "A";
            this.StripBTN_FontColor.ToolTipText = "글자 색";
            this.StripBTN_FontColor.Click += new System.EventHandler(this.StripBTN_FontColor_Click);
            // 
            // textBox1
            // 
            this.textBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.textBox1.Location = new System.Drawing.Point(0, 27);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(365, 116);
            this.textBox1.TabIndex = 3;
            // 
            // CB_Type
            // 
            this.CB_Type.Items.AddRange(new object[] {
            "텍스트",
            "이미지",
            "이미지+텍스트"});
            this.CB_Type.Name = "CB_Type";
            this.CB_Type.Size = new System.Drawing.Size(121, 26);
            // 
            // MFontDlg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.ClientSize = new System.Drawing.Size(365, 179);
            this.ControlBox = false;
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.BTN_Cancel);
            this.Controls.Add(this.BTN_Save);
            this.Name = "MFontDlg";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "폰트 설정";
            this.Load += new System.EventHandler(this.MFontDlg_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BTN_Save;
        private System.Windows.Forms.Button BTN_Cancel;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripComboBox StripCB_Font;
        private System.Windows.Forms.ToolStripComboBox StripCB_FontSize;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.ToolStripButton StripBTN_FontColor;
        private System.Windows.Forms.ToolStripComboBox CB_Type;
    }
}