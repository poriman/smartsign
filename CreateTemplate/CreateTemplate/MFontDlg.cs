﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;


namespace MenuBoard
{
    public partial class MFontDlg : Form
    {
        private Font m_Font = null;
        private Color m_FontColor = Color.Black;
        private int m_iEffect = 0;
        private string m_type;

        public MFontDlg()
        {
            InitializeComponent();
            CB_Type.SelectedIndex = 0;
        }

        public Font SetFont
        {
            set
            {
                m_Font = value;
            }

            get
            {
                return m_Font;
            }
        }

        public Color SetColor
        {
            set
            {
                m_FontColor = value;
            }
            get
            {
                return m_FontColor;
            }
        }

        public int SetEffect
        {
            set
            {
                m_iEffect = value;
            }
            get
            {
                return m_iEffect;
            }
        }

        public string GetText
        {
            get
            {
                return textBox1.Text;
            }
            set
            {
                textBox1.Text = value;
            }
        }

        public string TYPE
        {
            set 
            {
                m_type = value; 
                CB_Type.SelectedItem = value;
            }
            get 
            {
                m_type = CB_Type.SelectedItem.ToString();
                return m_type;
            }
        }


        private void FontApply()
        {
            textBox1.Font = m_Font;
            textBox1.ForeColor = m_FontColor;
            textBox1.Refresh();
        }

        private void MFontDlg_Load(object sender, EventArgs e)
        {
            try
            {
                /*
                foreach (FontFamily family in FontFamily.Families)
                {
                    this.StripCB_Font.Items.Add(family.Name);
                }
                 * HY중고딕, HY헤드라인M, HY신명조, 돋움체
                */
                string[] fonts = new string[] { "HY중고딕", "HY헤드라인M", "HY신명조", "돋움체" };

                foreach (string  font in fonts)
                {
                    this.StripCB_Font.Items.Add(font);
                }

                this.StripCB_Font.SelectedItem = m_Font.Name;
                StripCB_FontSize.SelectedItem = m_Font.Size.ToString();
                StripBTN_FontColor.BackColor = SetColor;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Error");
            }
        }

        private void StripCB_Font_SelectedIndexChanged(object sender, EventArgs e)
        {
            // font
            try
            {
                FontFamily newFamily = new FontFamily(StripCB_Font.SelectedItem.ToString());
                m_Font = new Font(newFamily, m_Font.Size, m_Font.Style);
                FontApply();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Error");
            }
        }

        private void StripCB_FontSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                float newSize = Convert.ToSingle(StripCB_FontSize.SelectedItem.ToString());
                m_Font = new Font(m_Font.FontFamily, newSize, m_Font.Style);
                FontApply();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Error");
            }
        }

        private void StripCB_FontSize_TextChanged(object sender, EventArgs e)
        {
            // font size custom
            try
            {
                float newSize = Convert.ToSingle(StripCB_FontSize.Text);
                m_Font = new Font(m_Font.FontFamily, newSize, m_Font.Style);
                FontApply();                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Error");
            }
        }

        private void StripBTN_Bold_Click(object sender, EventArgs e)
        {
            try
            {
                FontStyle newFontStyle = m_Font.Style ^ FontStyle.Bold;
                m_Font = new Font(m_Font.FontFamily, m_Font.Size, newFontStyle);
                FontApply();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Error");
            }
        }

        private void StripBTN_Italic_Click(object sender, EventArgs e)
        {
            try
            {
                FontStyle newFontStyle = m_Font.Style ^ FontStyle.Italic;
                m_Font = new Font(m_Font.FontFamily, m_Font.Size, newFontStyle);
                FontApply();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Error");
            }

        }

        private void StripBTN_FontColor_Click(object sender, EventArgs e)
        {
            try
            {
                using (ColorDialog dlg = new ColorDialog())
                {
                    dlg.Color = m_FontColor;
                    if (dlg.ShowDialog() == DialogResult.OK)
                    {
                        m_FontColor = dlg.Color;
                        StripBTN_FontColor.BackColor = dlg.Color;
                        FontApply();
                    }
                }                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString(), "Error");
            }
        }

        private void StripCB_Effect_SelectedIndexChanged(object sender, EventArgs e)
        {
        }

        private void BTN_Save_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Length == 0)
            {
                MessageBox.Show("텍스트를 삽입 하십시오.");
                return;
            }
            this.DialogResult = DialogResult.OK;
        }
        
    }
}