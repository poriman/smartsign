﻿using System;
using System.IO;
using System.Linq;
// using System.Collections.Generic;
// using System.Collections;
using System.Xml;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
//using System.Windows.Shapes;
using System.Windows.Threading;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Markup;
using System.Windows.Forms;
using System.Windows.Media.Animation;
using System.Drawing;

using System.Collections;

using DigitalSignage.Common;

// logging routines
using NLog;
using NLog.Targets;

// RSS routines
using Sloppycode.net;

using WPFDesigner;
using System.Threading;

// .NET remoting
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;
using System.Runtime.Remoting.Channels.Tcp;
using DigitalSignage.PlayControls;
using System.Windows.Controls.Primitives;
using DigitalSignage.TouchPlayer;
using System.Reflection;

namespace DigitalSignage.Player
{
	public class CmdAliveReceiver : MarshalByRefObject, ICmdReceiver
	{
		Logger logger = LogManager.GetCurrentClassLogger();
		public Object ProcessExpandCommand(CmdExReceiverCommands command)
		{
			try
			{
				TimerLoop tm = TimerLoop.GetInstance;

				switch (command)
				{
					case CmdExReceiverCommands.CmdExCurrentScreen:
						return tm.CurrentScreen;
					case CmdExReceiverCommands.CmdExCurrentSubtitle:
						return tm.CurrentSubtitle;
					case CmdExReceiverCommands.CmdExIsPlayerOn:
						return tm.IsPlayerVisible;
					case CmdExReceiverCommands.CmdExSerialStatusPower:
						return tm.SerialPortDeviceStatus(SerialPortController.SerialPortStatus.statusPower);
						break;
					case CmdExReceiverCommands.CmdExSerialStatusSource:
						return tm.SerialPortDeviceStatus(SerialPortController.SerialPortStatus.statusSource);
						break;
					case CmdExReceiverCommands.CmdExSerialStatusVolume:
						return tm.SerialPortDeviceStatus(SerialPortController.SerialPortStatus.statusVolume);
						break;
				}
			}
			catch (Exception e)
			{
				logger.Error(e + "");
			}
			return null;
		}

		public long ProcessSimpleCommand(CmdReceiverCommands cmd)
		{
			Logger logger = LogManager.GetCurrentClassLogger();
			long res = -1; //not implemented

			try
			{
				Config cfg = Config.GetConfig;

				switch (cmd)
				{
					case CmdReceiverCommands.CmdEcho:
						res = new Random(128).Next(128);
						break;
					case CmdReceiverCommands.CmdGetVersion:
						res = cfg.Version;
						break;
					case CmdReceiverCommands.CmdKillPlayer:
// 						System.Windows.Application.Current.MainWindow.Close();
						System.Windows.Application.Current.Shutdown();
						res = 0;
						break;
				}
			}
			catch (Exception e)
			{
				logger.Error(e + "");
			}
			return res;
		}
	}

    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
	public partial class MainWindow : Window
    {
        #region Touch
        private ITouchPlayer touchPlayer;
        private TouchScreenAgent.TSAgent tsAgent;
        #endregion
        

        private static Logger logger = LogManager.GetCurrentClassLogger();
        private Config cfg = Config.GetConfig;
		private iVisionLogHelper iVisionLog = iVisionLogHelper.GetInstance;
		
//        private DispatcherTimer dt;
//         private UDPListener udpListener;
        //private WPFDesigner.DesignElementsManager designManager;
        TimerLoop tm;

        private PlayerDef primaryPlayer = null;
        private Canvas primaryBuffer = null;
        private ScaleTransform primaryScale = null;

        private PlayerDef backPlayer = null;
        private Canvas backBuffer = null;
        private ScaleTransform backScale = null;
        
        private static object lockSubtitle = new object();

		Mutex _mutex = null;

		#region fade-in-out routines
		bool fade_start = true;
		bool fade_end = true;
		#endregion

		bool is_window_mode = false;

		#region 제어스케줄
		bool is_visible_player = true;

        delegate bool PlayerVisibleCB(bool isVisible);
        bool PlayerVisible(bool isVisible)
        {
            if (!this.Dispatcher.CheckAccess())
            {
                try
                {
                    DispatcherOperation o = this.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                       new PlayerVisibleCB(PlayerVisible), isVisible);

                    o.Wait();

                    return o.Result == null ? false : (bool)o.Result;
                }
                catch (Exception e)
                {
                    logger.Error(e + "");
                    return false;
                }
            }
            else
            {
                is_visible_player = isVisible;
                this.Visibility = is_visible_player ? Visibility.Visible : Visibility.Hidden;

                if (is_visible_player) this.Activate();

                return true;
            }
        }

		public bool IsPlayerVisible
		{
			get
			{
				return is_visible_player;
			}
			set
			{
                PlayerVisible(value);
				//is_visible_player = value;
				//this.Visibility = is_visible_player ? Visibility.Visible : Visibility.Hidden;
			}
		}
		#endregion


		public MainWindow()
        {
            try
            {
				InitializeLogger();

				TrialChecker checker = new TrialChecker();
				if (!checker.IsValid)
				{
					System.Windows.MessageBox.Show(checker.LastMessage);
					System.Windows.Application.Current.Shutdown();
				} 
				
				InitializeComponent();

				#region Mutex - 프로그램 한개만 뜨도록
				bool bCreate = false;
				_mutex = new Mutex(true, "DigitalSignage.Player", out bCreate);
				if (!bCreate)
				{
					System.Windows.Application.Current.Shutdown();
					return;
				}
				#endregion


//				PrimaryUI.ProgramLoadCompleted += new ProgramLoadCompletedHandler(evProgramLoadCompleted);
//				BackUI.ProgramLoadCompleted += new ProgramLoadCompletedHandler(evProgramLoadCompleted);
	


				registerService();

				DigitalSignage.PlayAgent.Task t = new DigitalSignage.PlayAgent.Task();
				t.type = TaskCommands.CmdInitPlayer;
				t.uuid = DigitalSignage.PlayAgent.Task.TaskInitPlayer;
                t.pid = cfg.PlayerId;
				t.gid = cfg.GroupId;
				t.duuid = DigitalSignage.PlayAgent.Task.TaskInitPlayer;
				t.guuid = DigitalSignage.PlayAgent.Task.TaskInitPlayer;
				t.description = "CmdInitPlayer";

				t.TaskStart = TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()); // run just now
				if (-1 == cfg.localTasksList.AddTask(t))
				{
					////	추가가 되지않음.
					cfg.localTasksList.CleanTask(Task.TaskInitPlayer);
					//	다시 추가 시도
					cfg.localTasksList.AddTask(t);
				}

				//	hsshin 숨김

				if (cfg.IsManualToPlayer)
				{

					this.WindowState = WindowState.Normal;
					this.WindowStartupLocation = WindowStartupLocation.Manual;

					if (is_window_mode = cfg.ShowWindowEdge)
					{
						System.Windows.Forms.Cursor.Show();
						this.WindowStyle = WindowStyle.ThreeDBorderWindow;
						this.ResizeMode = ResizeMode.CanResizeWithGrip;
						this.ShowInTaskbar = true;
					}
					else
					{
						this.WindowStyle = WindowStyle.None;
						this.ResizeMode = ResizeMode.NoResize;
						this.ShowInTaskbar = false;
						this.Topmost = cfg.WindowTopMost;
					}

					this.Left = (double)cfg.WindowXPos;
					this.Top = (double)cfg.WindowYPos;
					this.Width = (double)cfg.WindowWidth;
					this.Height = (double)cfg.WindowHeight;
				}
				else
				{
					//System.Windows.Forms.Cursor.Hide();//조준영 테스트

					this.WindowState = WindowState.Maximized;
					this.WindowStyle = WindowStyle.None;
					this.ResizeMode = ResizeMode.NoResize;
					this.ShowInTaskbar = false;
				}
				try
				{
					//	hsshin 제어 스케줄
					this.IsPlayerVisible = cfg.IsPlayerOn;

					fade_start = cfg.FadeStart;
					fade_end = cfg.FadeEnd;
				}
				catch
				{

				}

				logger.Info("Starting Player...");

                tm = TimerLoop.GetInstance;
                tm.Start(this);

				return;
            }
            catch (Exception e)
            {
                logger.Error(e + "");
            }
            this.Close();
        }

		private void InitializeLogger()
		{
			String sExecutingPath = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
			string appData = sExecutingPath + "\\PlayerData\\";
			if (!Directory.Exists(appData))
				Directory.CreateDirectory(appData);

			try
			{
				// configuring log output
				FileTarget target = new FileTarget();
				target.Layout = "${longdate}\t[${level}]\t[${callsite}]\t${message}";
				//             target.Layout = "${longdate} ${logger} ${message}";
				target.FileName = appData + "Player.Log.txt";
				target.ArchiveFileName = appData + "archives/Player.Log.{#####}.txt";
				// 			target.ArchiveAboveSize = 256 * 1024 * 10; // archive files greater than 2560 KB
				target.MaxArchiveFiles = 31;
				target.ArchiveEvery = FileArchivePeriod.Day;// FileTarget.ArchiveEveryMode.Day;
				target.ArchiveNumbering = ArchiveNumberingMode.Sequence;// FileTarget.ArchiveNumberingMode.Sequence;
				// this speeds up things when no other processes are writing to the file
				target.ConcurrentWrites = true;
				NLog.Config.SimpleConfigurator.ConfigureForTargetLogging(target, LogLevel.Debug);

			}
			catch { }
		}

		private int registerService()
		{
			try
			{
				// creating providers chunks
				BinaryServerFormatterSinkProvider provider = new BinaryServerFormatterSinkProvider();
				provider.TypeFilterLevel = System.Runtime.Serialization.Formatters.TypeFilterLevel.Full;
				ClientIPProvider ipProvider = new ClientIPProvider();
				provider.Next = ipProvider;

				// regestering channel
				Hashtable props = new Hashtable();
				props.Add("port", 1889);
				TcpChannel channel = new TcpChannel(props, null, provider);
				ChannelServices.RegisterChannel(channel, false);

				// resgestering services
				RemotingConfiguration.RegisterWellKnownServiceType(
				  typeof(CmdAliveReceiver),
				  "CmdReceiverHost/rt",
				  WellKnownObjectMode.Singleton);

				logger.Info(".NET remoting handlers registered successfully");
				// 				udpListener = new UDPListener();
				//                 logger.Info("UDP listener started");
				return 0;
			}
			catch (Exception e)
			{
				logger.Error(e + "");
			}
			return -1;
		}

//		void evProgramLoadCompleted(object sender, ProgramLoadCompletedArgs e)
//		{
//			SwapGrid();
//		}

		delegate void SwapGridCB();
		void SwapGrid()
		{
			if (!this.Dispatcher.CheckAccess())
			{
				try
				{
					DispatcherOperation o = this.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
					   new SwapGridCB(SwapGrid));

					return;
				}
				catch (Exception e)
				{
					logger.Error(e + "");
					return ;
				}
			}
			else
			{
				try
				{
					//BackUI.Visibility = Visibility.Visible;
					//PrimaryUI.Visibility = Visibility.Collapsed;

					//PrimaryUI.Stop();

					//this.BeginInit();
					//ProgramPlayer swap = PrimaryUI;
					//PrimaryUI = BackUI;
					//BackUI = swap;
					//this.EndInit();
				}
				catch (Exception e)
				{
					logger.Error(e.Message/* + " " + e.StackTrace*/);

				}
			}
		}

        #region Async routines
        delegate bool loadProjectCB(string filename);
        public bool loadProject(String projectName)
        {
			if (!is_visible_player)
				return true;
			
			if (!this.Dispatcher.CheckAccess())
            {
                try
                {
                    DispatcherOperation o = this.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                       new loadProjectCB(loadProject), projectName);

					o.Wait();

					return o.Result == null ? false : (bool)o.Result;
                }
                catch (Exception e)
                {
                    logger.Error(e + "");
					return false;
                }
            }
            else
            {
				try
				{
					if (projectName.Equals(""))
					{
						PrimaryUI.Stop();
						return true;
					}

// 					PrimaryUI.Stop();
// 
// 					PrimaryUI.Load(projectName);
// 
// 					PrimaryUI.IsFadeIn = fade_start;
// 					PrimaryUI.IsFadeOut = fade_end;
                    // 					PrimaryUI.Start();

                    #region Touch - ITouchPlayer 인스턴스 생성.

                    PrimaryUI.ChangedNonTouchScreenHandler = new ChangedNonTouchScreenDelegate(ChangedNonTouchScreenDelegateFunc);
                    BackUI.ChangedNonTouchScreenHandler = new ChangedNonTouchScreenDelegate(ChangedNonTouchScreenDelegateFunc);

                    if (touchPlayer == null)
                    {
                        touchPlayer = TouchPlayerWindow.TheOnlyTSAgent;
                        touchPlayer.SetTouchWindowSize(new System.Windows.Size(this.ActualWidth, this.ActualHeight));
                        touchPlayer.TouchPlayerProgramPath = projectName;
                        touchPlayer.ChangedTouchPageHandler = new ChangedTouchPageDelegate(ChangedTouchPage);
                        touchPlayer.SetPlayStateTouchProjectHandler = new SetPlayStateTouchProjectDelegate(SetPlayStateTouchProject);
                        touchPlayer.RaisedUserTouchHandler = new RaisedUserTouchDelegate(RaisedUserTouch);
                        ProgramPlayer.LoadTouchProjectHandler = new DigitalSignage.PlayControls.LoadTouchProjectDelegate(LoadTouchProject);
                        ProgramPlayer.PlayTouchProjectHandler = new DigitalSignage.PlayControls.PlayTouchProjectDelegate(touchPlayer.PlayTouchProject);
                        ProgramPlayer.CloseTouchProjectHandler = new DigitalSignage.PlayControls.CloseTouchProjectDelegate(touchPlayer.CloseTouchProject);
                        touchPlayer.SetUserTouchWindow(this);
                        touchPlayer.SetOnScreenKeyboardWindow(this);
                        touchPlayer.IsTimeScheduleProgram = false;
                        
                    }
                    touchPlayer.CloseMsgWindow();
                    #endregion

                    #region Touch - 시간 스케줄일 경우 UserTouch Window 설정
                    TimerLoop timerLoop = TimerLoop.GetInstance;
                    if (timerLoop != null && timerLoop.CurrentScreen != null)
                    {
                        if (timerLoop.CurrentScreen.type == TaskCommands.CmdProgram)//시간스케줄일 경우.
                        {
                            touchPlayer.ScheduleType = ScheduleType.TimeSchedule;
                            touchPlayer.IsTimeScheduleProgram = true;
                            touchPlayer.IsTouchWindow = false;
                            touchPlayer.ShowHideUserTouchWindow(false);
                            touchPlayer.ShowHideOnScreenKeyboardWindow(false);
                        }
                        else
                        {
                            touchPlayer.IsTimeScheduleProgram = false;
                            if (timerLoop.CurrentScreen.type == TaskCommands.CmdDefaultProgram)
                            {
                                touchPlayer.ScheduleType = ScheduleType.DefaultSchedule;
                                touchPlayer.IsTouchWindow = true;
                                touchPlayer.ShowHideUserTouchWindow(true);
                                touchPlayer.ShowHideOnScreenKeyboardWindow(false);                                
                            }
                            else
                            {
                                touchPlayer.ScheduleType = ScheduleType.DefaultTouchSchedule;
                                touchPlayer.IsTouchWindow = false;
                                touchPlayer.ShowHideUserTouchWindow(false);
                                touchPlayer.ShowHideOnScreenKeyboardWindow(true);                                
                            }
                            
                        }
                        //if (TimerLoop.CurrentTaskCommand == TaskCommands.CmdProgram)//시간스케줄일 경우.
                        //    this.ShowTouchWindow(false);
                        //else
                        //    this.ShowTouchWindow(true);
                    }
                    #endregion

                    BackUI.Load(projectName, is_window_mode);

					BackUI.IsFadeIn = fade_start;
					BackUI.IsFadeOut = fade_end;
					BackUI.Start();

					BackUI.Visibility = Visibility.Visible;
					PrimaryUI.Visibility = Visibility.Collapsed;

					PrimaryUI.Stop();

					this.BeginInit();
					ProgramPlayer swap = PrimaryUI;
					PrimaryUI = BackUI;
					BackUI = swap;
					this.EndInit();

                    #region Touch
                    //if (tsAgent == null)
                    //{
                        //tsAgent = TouchScreenAgent.TSAgent.TheOnlyTSAgent;
                        //tsAgent.ChangePaneScreen = new DigitalSignage.TouchScreenAgent.TSAgent.ChangePaneScreenDelegate(ChangedTouchPageDelegateFunc);
                        //tsAgent.InitTouchScreenAgent(projectName, new System.Windows.Size(this.ActualWidth, this.ActualHeight));
                        //tsAgent.CreateTouchScreenForm(PrimaryUI, PrimaryUI.TSA_GetPlaylistPlayerItem(), this);
                    //}

                    #endregion

                    return true;
				}
				catch (iVisionException ive)
				{
					iVisionLog.ScreenErrorLog(projectName, ive.ErrorCode, ive.DetailMessage);

					logger.Error(String.Format("iVisionException Code : {0}, Description : {1}", ive.ErrorCode, ive.DetailMessage));
					logger.Error(ive.StackTrace);
					return false;
				}
				catch (IOException ioe)
				{
					logger.Error(ioe.Message/* + " " + e.StackTrace*/);

					FileInfo info = new FileInfo(projectName);
					if(info != null)
					{
						if(!info.Exists)
						{
							iVisionLog.ScreenErrorLog(info.Name, ErrorCodes.IS_ERROR_CONTENT_NOT_FOUND, ioe.Message);
						}
						else
						{
							iVisionLog.ScreenErrorLog(info.Name, ErrorCodes.IS_ERROR_CONTENT_CRASHED, ioe.Message);
						}

					}
					logger.Error(ioe.StackTrace);

					return false;
					
				}
				catch (Exception e)
				{
					logger.Error(e.Message/* + " " + e.StackTrace*/);

					FileInfo info = new FileInfo(projectName);
					if(info != null)
					{
						iVisionLog.ScreenErrorLog(info.Name, ErrorCodes.IS_ERROR_INTERNAL, e.Message);
					}

					logger.Error(e.StackTrace);

				}

				return false;
            }

			return true;
        }

        public bool setSubtitle(String projectName)
        {
			if (!is_visible_player)
				return true;

			if (!this.Dispatcher.CheckAccess())
			{
				try
				{
					DispatcherOperation o = this.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
					   new loadProjectCB(setSubtitle), projectName);

					o.Wait();

					return o.Result == null ? false : (bool)o.Result;
				}
				catch (Exception e)
				{
					logger.Error(e + "");
					return false;
				}
			}
			else
			{
				try
				{
                    lock (lockSubtitle)
                    {

						if (popSubtiles != null)
						{
							try
							{
								popSubtiles.IsOpen = false;
								popSubtiles.Child = null;
							}
							catch
							{
							}

							gridMain.Children.Remove(popSubtiles);

							popSubtiles = null;
						}

                        if (String.IsNullOrEmpty(projectName))
                            return true;

                        popSubtiles = new Popup();
                        popSubtiles.AllowsTransparency = true;
                        // 					popSubtiles.Placement = PlacementMode.AbsolutePoint;
                        popSubtiles.Placement = PlacementMode.RelativePoint;
                        popSubtiles.PlacementTarget = this;
                        Canvas cvPopup = new Canvas();
                        popSubtiles.Child = cvPopup;

                        FlowDocument fdoc = null;
                        using (TextReader tr = new StreamReader(projectName))
                        {
                            fdoc = FlowDocumentManager.DeserializeFlowDocument(tr.ReadToEnd());
                            tr.Close();
                            tr.Dispose();
                        }

                        double length = 0;
                        double fontheight = 0;

                        // 					float linwith = (float)1.1;
                        foreach (Block block in fdoc.Blocks)
                        {
                            foreach (Inline _line in block.ContentStart.Paragraph.Inlines)
                            {
                                TextRange flowDocSelection = new TextRange(_line.ContentStart, _line.ContentEnd);
                                length += flowDocSelection.Text.Length * _line.FontSize;
                                if (fontheight < _line.FontSize) fontheight = _line.FontSize;
                            }
                        }
                        fontheight *= 1.5;
                        length += 20;

                        System.Windows.Controls.RichTextBox rtbText = new System.Windows.Controls.RichTextBox();
                        rtbText.Document = fdoc;

                        rtbText.Background = System.Windows.Media.Brushes.Transparent;
                        rtbText.BorderThickness = new Thickness(0);

                        popSubtiles.Width = this.ActualWidth;
                        rtbText.Width = length;
                        popSubtiles.Height = rtbText.Height = fontheight;

                        rtbText.VerticalAlignment = VerticalAlignment.Center;
                        rtbText.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;

                        cvPopup.Children.Add(rtbText);


                        DoubleAnimation animation = new DoubleAnimation();

                        animation.Duration = new Duration(CalculateDurationFromSpeed(length, (int)cfg.SubSpeed));

                        // 					if (length > this.ActualWidth)
                        // 						animation.Duration = new Duration(new TimeSpan((long)(length) * 110000));
                        // 					else
                        // 						animation.Duration = new Duration(new TimeSpan((long)(this.ActualWidth) * 110000));
                        // 				animation.Duration = new Duration(new TimeSpan((long)(length + this.ActualWidth) * 110000));

                        animation.SpeedRatio = 1.0;
                        animation.RepeatBehavior = RepeatBehavior.Forever;
                        animation.By = 200.0;
                        animation.From = this.ActualWidth + 10;
                        animation.To = -length - 10;
                        rtbText.BeginAnimation(Canvas.LeftProperty, animation);

                        double dVerticalOffset = this.ActualHeight - fontheight - 10;

                        if (cfg.IsManualToSubtitle)
                        {
                            dVerticalOffset = (double)cfg.SubPositionY;
                        }

                        popSubtiles.VerticalOffset = dVerticalOffset;
                        popSubtiles.IsOpen = true;

                        gridMain.Children.Add(popSubtiles);

                        iVisionLog.SubtitleStartLog(projectName);
                    }
				}
				catch (iVisionException ive)
				{
					iVisionLog.ScreenErrorLog(projectName, ive.ErrorCode, ive.DetailMessage);
					return false;
				}
				catch (Exception e)
				{
					iVisionLog.ScreenErrorLog(projectName, ErrorCodes.IS_ERROR_INTERNAL, e.Message);
					return false;
				}

			}
			return true;
        }

		TimeSpan CalculateDurationFromSpeed(double length, int speed)
		{
			TimeSpan ts = new TimeSpan(0, 0, 0, 0, (int)(((length + this.ActualWidth) / speed) * 1000));
			return ts;
		}

        public void SetStatusText(string text)
        {
            /*try
            {
                if (!this.Dispatcher.CheckAccess())
                {
                    this.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                       new loadProjectCB(SetStatusText), text );
                }
                else
                {
                    status.Content = text;
                }
            }
            catch (Exception e)
            {
                logger.Error(e + "");
            }*/
        }
        #endregion

        #region Event handlers
        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            //System.Windows.Forms.Cursor.Hide();//조준영 테스트
//             subsTextUI.Margin = new Thickness(0, 20, 0, 60);
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            System.Windows.Forms.Cursor.Show();
			
			if (PrimaryUI != null)
			{
				PrimaryUI.Stop();
			}
			
//             if (udpListener != null)
//             {
//                 udpListener.Stop();
//                 udpListener = null;
//             }
            if (tm != null)
            {
                tm.Stop();
                tm = null;
            }

			if(primaryBuffer != null)
			{
				primaryBuffer = null;
			}

			if (backBuffer != null)
			{
				backBuffer = null;
			}

			if (_mutex != null)
				_mutex.ReleaseMutex();

			logger.Info("Ending Player...");

            if (touchPlayer != null)
            {
                touchPlayer.CloseTouchPlayer();
            }
        }

        #endregion

        #region Touch

        /// <summary>
        /// Touch Screen로 부터 Page 변경 이벤트가 발생시 호출된다.
        /// </summary>
        /// <param name="paneScreenID"></param>
        /// <param name="paneScreenView"></param>
        private void ChangedTouchPageDelegateFunc(string paneScreenID, object paneScreenView, bool isLinked, bool isScreenRepeat)
        {
            try
            {
                PrimaryUI.TSA_PlaylistStart(paneScreenID, paneScreenView, isLinked, isScreenRepeat);
            }
            catch (Exception ex)
            {
            }
        }

        /// <summary>
        /// Non Touch Screen으로부터 Page 변경 이벤트가 발생시 호출된다.
        /// </summary>
        /// <param name="paneScreenID"></param>
        /// <param name="playlistPlayer"></param>
        private void ChangedNonTouchScreenDelegateFunc(string paneScreenID, object playlistPlayer)
        {
            tsAgent.DisplayTSView(paneScreenID, playlistPlayer);
        }

        private void ChangedTouchPage(UIElement changedPage, UIElement playerScreen)
        {
            try
            {
                PrimaryUI.TSA_PlaylistStart(changedPage, playerScreen);
            }
            catch (Exception ex)
            {
            }
        }

        private void SetPlayStateTouchProject(bool isTouchPlayState)
        {
            try
            {
                PrimaryUI.SetPlayStateTouchProject(isTouchPlayState);
                TimerLoop.IsPauseDefaultProgram = isTouchPlayState;
            }
            catch (Exception ex)
            {
            }
        }

        private void RaisedUserTouch()
        {
            try
            {
                TimerLoop tm = TimerLoop.GetInstance;
                tm.PlayTouchRollingTask();
            }
            catch (Exception ex)
            {
            }
        }

        private UIElement LoadTouchProject(string touchProjectName, Grid playerGrid)
        {
            TimerLoop timerLoop = TimerLoop.GetInstance;            
            return touchPlayer.LoadTouchProject(touchProjectName, playerGrid, timerLoop.CurrentScreen.duration);
        }
        #endregion

        protected override void OnRenderSizeChanged(SizeChangedInfo sizeInfo)
        {
            base.OnRenderSizeChanged(sizeInfo);

            if (touchPlayer != null)
            {                
                touchPlayer.SizeChangedScreen(sizeInfo.NewSize);
            }            
        }

    }
}