﻿using System;
using System.Collections;
using System.Runtime.InteropServices;
using System.Threading;

using DigitalSignage.Common;
// using DigitalSignage.DataBase;

using NLog;

using DigitalSignage.PlayAgent;

namespace DigitalSignage.Player
{
    class TimerLoop
    {
		private iVisionLogHelper iVisionLog = iVisionLogHelper.GetInstance;

        private static Logger logger = LogManager.GetCurrentClassLogger();
		private Config config = null;

		//	Lock 오브젝트
		private static readonly object semaphore = new object();
		private readonly object lockScreen = new object();

		public bool IsPlayerVisible
		{
			get { return parent.IsPlayerVisible; }
		}
		
		private static TimerLoop instance = null;
		
		public static TimerLoop GetInstance
		{
			get
			{
				lock (semaphore)
				{
					if (instance == null)
					{
						instance = new TimerLoop();
					}
					return instance;
				}
			}
		}

		//	hsshin DefaultSchedlue List
		private TaskPlaylist _defaultScreenPlaylist = new TaskPlaylist();

		private TaskPlaylist _backscreenPlaylist = new TaskPlaylist();

		private TaskPlaylist _subtitlePlaylist = new TaskPlaylist();

		private TaskPlaylist _volumeControlPlaylist = new TaskPlaylist();

        private bool processing_flag;
        private MainWindow parent;
		private DigitalSignage.PlayAgent.Task currentVolumeControl = null;
		private DigitalSignage.PlayAgent.Task currentScreen = null;
		private DigitalSignage.PlayAgent.Task currentSub = null;
		private DigitalSignage.PlayAgent.Task previewSub = null;		//	이전 서브타이틀
		private Thread control = null;

		//	hsshin
		private Thread thDefaultScreenPlayer = null;

		public DigitalSignage.PlayAgent.Task CurrentVolumeControl
		{
			get { return currentVolumeControl; }
		}
		public DigitalSignage.PlayAgent.Task CurrentScreen
		{
			get { return currentScreen; }
		}
		public DigitalSignage.PlayAgent.Task CurrentSubtitle
		{
			get { return currentSub; }
		}

		//	Log Db
		private DigitalSignage.ClientDataBase.LogsDataSetTableAdapters.client_logsTableAdapter client_logsTA = null;

        public int Start( MainWindow parent )
        {
            this.parent = parent;
            config = Config.GetConfig;

			iVisionLog.GeneralLog = WriteInfo;
			iVisionLog.GeneralLogError = WriteError;

			iVisionLog.ScreenLogStarted = ScreenStarted;
			iVisionLog.ScreenLogEnded = ScreenEnded;
			iVisionLog.ScreenLogError = ScreenErrorOccurred;
			
			iVisionLog.SubtitleLogStarted = SubtitleStarted;
			iVisionLog.SubtitleLogEnded = SubtitleEnded;
			iVisionLog.SubtitleLogError = SubtitleErrorOccurred;

            processing_flag = true;

            //starting control thread
			control = new Thread(this.controlThread);
			control.Priority = ThreadPriority.BelowNormal;
            control.Start();
		
            return 0;
        }

		/// <summary>
		/// TimerLoop를 정지시킨다
		/// </summary>
		/// <returns>reserved</returns>
        public int Stop()
        {
            processing_flag = false;

			if (thDefaultScreenPlayer != null && thDefaultScreenPlayer.IsAlive)
			{
				thDefaultScreenPlayer.Abort();
				thDefaultScreenPlayer = null;
			}

            return 0;
        }

		/// <summary>
		/// 반복 스케줄을 수행할 스레드를 생성한다.
		/// </summary>
		private void defaultScreenTimer(object param)
		{
			try
			{
				long duration = Convert.ToInt64(param);

				//0.2초 보정시간을 주자 - 뺐음
				Thread.Sleep((int)(duration * 1000) - 200);

				lock (lockScreen)
				{
					if (currentScreen != null && currentScreen.type == TaskCommands.CmdDefaultProgram)
					{
						PlayNextDefaultScreenTask();
					}
					else
						logger.Error("Current screen is not 'Default' task.");
				}

			}
			catch
			{
			}
		}

		/// <summary>
		/// 1초마다 Task루틴을 수행할 스레드를 생성한다.
		/// </summary>
        private void controlThread()
        {
			logger.Info("Starting Timeloop thread...");

			config.localTasksList.ResetRunningTasks();
			logger.Info("Running tasks are updated to 'Wait' state.");

			while (processing_flag)
            {
                //starting task starter thread
//                 Thread ts = new Thread(this.runTasks);
//                 ts.Priority = ThreadPriority.BelowNormal;
//                 ts.Start();
				runTasks();

                Thread.Sleep(1000);
            }

			logger.Info("Ending Timeloop thread...");
        }

		#region 반복 스케줄 처리
		/// <summary>
		/// 반복 스케줄 처리
		/// </summary>
		/// <param name="task">해당 Task</param>
		private void RollingTask(DigitalSignage.PlayAgent.Task task)
		{
			if (task.state == TaskState.StateWait)
			{
				logger.Info("Task " + task.ToString());

				logger.Info(String.Format("[CmdDefaultProgram][{0}] StateWait", task.description));

				if (task.TaskEnd >= TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()))
				{
					_defaultScreenPlaylist.AddTask(task);
					config.localTasksList.ChangeTaskState(TaskState.StateRunning, task.uuid);
					//	아무것도 플레이 중이 아니면 재생해준다.
					if (currentScreen == null/* && false == _defaultScreenPlaylist.IsStarted*/)
					{
						PlayNextDefaultScreenTask();
					}
					else if (currentScreen != null && currentScreen.uuid == task.uuid)
					{
						PlayNextDefaultScreenTask();
					}
					return ;
				}
				else
				{
					//	이미 시간이 지났는데 wait로 들어온 경우라면
					//	사용자가 Edit Task를 통해 변경한 경우이다.
					//	재생 지연 목록에서 요청된 Task를 제거해주고,
					//	현재 플레이중이라면 종료를 해준 후 다음 스크린을 재생한다.
					logger.Info("[" + task.description + " (" + task.uuid + ")] End time is wrong. It will be canceled.");
					config.localTasksList.ChangeTaskState(TaskState.StateProcessed, task.uuid);

					_defaultScreenPlaylist.RemoveTask(task);
					iVisionLog.GeneralLogError(task.description, task.type, ErrorCodes.IS_ERROR_INVALID_TIME_SCOPE, "This schedule is alreay ended.");

					if (currentScreen != null && currentScreen.uuid == task.uuid)
					{
						PlayNextDefaultScreenTask();
					}
				}

			}
			else if (task.state == TaskState.StateEdit)
			{
				logger.Info("Task " + task.ToString());

				logger.Info(String.Format("[CmdDefaultProgram][{0}] StateEdit.", task.description));

				//	적용중인 Task가 있다면 지워준다 
				_defaultScreenPlaylist.RemoveTask(task);

				//	만약 현재 재생하고 있다면!!
				if (currentScreen != null && currentScreen.uuid == task.uuid)
				{
					// 다음 Task를 재생하자
					PlayNextDefaultScreenTask();
				}
				if (!IsDownloadComplete(task.duuid.ToString()))
				{
					logger.Info(String.Format("[CmdDefaultProgram][{0}] state has been modified as 'Download'", task.description));
					config.localTasksList.ChangeTaskState(TaskState.StateDownload, task.uuid);
				}
				else
					config.localTasksList.ChangeTaskState(TaskState.StateWait, task.uuid);


			}
			else if (task.state == TaskState.StateRunning)
			{
				if (task.TaskEnd <= TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()))
				{
					logger.Info("Task " + task.ToString());

					//	스케줄 종료시간 보다 현재 시간이 더 크다면
					logger.Info(String.Format("[CmdDefaultProgram][{0}] StateRunning.", task.description));

					_defaultScreenPlaylist.RemoveTask(task);
					config.localTasksList.ChangeTaskState(TaskState.StateProcessed, task.uuid);

					if (currentScreen != null && currentScreen.uuid == task.uuid)
					{
						PlayNextDefaultScreenTask();
					}
				}
				else
					config.localTasksList.ChangeTaskState(TaskState.StateRunning, task.uuid);

				return ;

			}
			else if (task.state == TaskState.StateCanceled)
			{
				logger.Info("Task " + task.ToString());

				logger.Info(String.Format("[CmdDefaultProgram][{0}] StateCanceled.", task.description));

				_defaultScreenPlaylist.RemoveTask(task);
				config.localTasksList.ChangeTaskState(TaskState.StateProcessed, task.uuid);

				if (currentScreen != null && currentScreen.uuid == task.uuid)
				{
					PlayNextDefaultScreenTask();
				}

			}
			else
			{
				logger.Error("[CmdDefaultProgram] Unknown TaskState : " + task.state.ToString());
			}

			return ;
		}
		#endregion

		#region 제어 스케줄 처리
		/// <summary>
		/// 제어 스케줄 처리
		/// </summary>
		/// <param name="task"></param>
		private void ControlTask(DigitalSignage.PlayAgent.Task task)
		{
			if (task.state == TaskState.StateWait)
			{
				if (task.TaskEnd > TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()))
				{
					//	현재 시간보다 스케줄 종료 시간이 더 크다면

					//	1. 일일 내 시간 검사
					//	2. 요일 검사
					if (false == (CheckContainTimeofDay(task.starttimeofday, task.endtimeofday) && CheckDayOfWeek(task.daysofweek)))
					{
						//	현재 시간과 비교하여 하루 시간 대와 요일에 맞지 않는다면.
						config.localTasksList.ChangeTaskState(TaskState.StateWait, task.uuid);
						return ;
					}
					logger.Info("Task " + task.ToString());

					logger.Info(String.Format("[CmdControlSchedule][{0}] StateWait.", task.description));

					TaskControlSchedules command = ControlScheduleHelper.GetKindOfCommand(task.description);
					switch (command)
					{
						case TaskControlSchedules.CtrlVolume:	//	볼륨인 경우 적용 범위(START-END)가 존재한다.
							{
								config.localTasksList.ChangeTaskState(TaskState.StateRunning, task.uuid);

								if (currentVolumeControl != null && currentVolumeControl.type == TaskCommands.CmdControlSchedule && currentVolumeControl.priority > task.priority)
								{
									logger.Info("[" + task.description + " (" + task.uuid + ")] Added to playlist. It is lower priority than current.");
									_volumeControlPlaylist.Push(task);
									break;
								}

								if (currentVolumeControl != null && currentVolumeControl.type == TaskCommands.CmdControlSchedule && currentVolumeControl.uuid != task.uuid)
									_volumeControlPlaylist.Push(currentVolumeControl);

								if (false == PlayControlSchedule(task))
									PlayNextStoredVolumeControlTask();

								break;
							}
						default:
							config.localTasksList.ChangeTaskState(TaskState.StateRunning, task.uuid);

							ControlScheduleHelper.PlayControlSchedule(task.description, task.duuid);
							break;
					}
				}
				else
				{

					logger.Info("[" + task.description + " (" + task.uuid + ")] End time is wrong. It will be canceled.");
					config.localTasksList.ChangeTaskState(TaskState.StateProcessed, task.uuid);

					TaskControlSchedules command = ControlScheduleHelper.GetKindOfCommand(task.description);
					switch (command)
					{
						case TaskControlSchedules.CtrlVolume:	//	볼륨인 경우 적용 범위(START-END)가 존재한다.
							_volumeControlPlaylist.RemoveTask(task);

							if (currentVolumeControl != null && currentVolumeControl.uuid == task.uuid)
							{
								PlayNextStoredVolumeControlTask();
							}

							break;
					}

					iVisionLog.GeneralLogError(task.description, task.type, ErrorCodes.IS_ERROR_INVALID_TIME_SCOPE, "This schedule is already ended.");
				}
			}
			else if (task.state == TaskState.StateEdit)
			{
				logger.Info("Task " + task.ToString());
				logger.Info(String.Format("[CmdControlSchedule][{0}] StateEdit.", task.description));

				TaskControlSchedules command = ControlScheduleHelper.GetKindOfCommand(task.description);
				switch (command)
				{
					case TaskControlSchedules.CtrlVolume:	//	볼륨인 경우 적용 범위(START-END)가 존재한다.
						//	적용중인 Task가 있다면 지워준다 
						_volumeControlPlaylist.RemoveTask(task);

						//	만약 현재 재생하고 있다면!!
						if (currentVolumeControl != null && currentVolumeControl.uuid == task.uuid)
						{
							// 다음 Task를 재생하자
							PlayNextStoredVolumeControlTask();
						}
						break;
				}

				config.localTasksList.ChangeTaskState(TaskState.StateWait, task.uuid);
			}
			else if (task.state == TaskState.StateRunning)
			{
				if (task.TaskEnd <= TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()))
				{
					//	스케줄 종료시간 보다 현재 시간이 더 크다면

					logger.Info("[" + task.description + " (" + task.uuid + ")] will be ended.");
					config.localTasksList.ChangeTaskState(TaskState.StateProcessed, task.uuid);

					TaskControlSchedules command = ControlScheduleHelper.GetKindOfCommand(task.description);

					switch (command)
					{
						case TaskControlSchedules.CtrlVolume:	//	볼륨인 경우 적용 범위(START-END)가 존재한다.
							_volumeControlPlaylist.RemoveTask(task);

							if ((currentVolumeControl != null) && (task.uuid.Equals(currentVolumeControl.uuid)))
							{
								PlayNextStoredVolumeControlTask();
							}
							break;
						default:
							break;
					}
				}
				else
				{
					//	현재 시간보다 스케줄 종료 시간이 더 크다면

					//	1. 일일 내 시간 검사
					//	2. 요일 검사
					if (false == (CheckContainTimeofDay(task.starttimeofday, task.endtimeofday) && CheckDayOfWeek(task.daysofweek)))
					{
						logger.Info("[" + task.description + " (" + task.uuid + ")] will be ended.");

						//	현재 시간과 비교하여 하루 시간 대와 요일에 맞지 않는다면.
						config.localTasksList.ChangeTaskState(TaskState.StateWait, task.uuid);

						TaskControlSchedules command = ControlScheduleHelper.GetKindOfCommand(task.description);

						switch (command)
						{
							case TaskControlSchedules.CtrlVolume:	//	볼륨인 경우 적용 범위(START-END)가 존재한다.
								_volumeControlPlaylist.RemoveTask(task);

								if ((currentVolumeControl != null) && (task.uuid.Equals(currentVolumeControl.uuid)))
								{
									PlayNextStoredVolumeControlTask();
								}
								break;
							default:
								break;
						}
					}
					else
						config.localTasksList.ChangeTaskState(TaskState.StateRunning, task.uuid);

				}

			}
			else if (task.state == TaskState.StateCanceled)
			{
				config.localTasksList.ChangeTaskState(TaskState.StateProcessed, task.uuid);
				logger.Info("Changing status to [StateCanceled]. (UUID=" + task.uuid + ")");

				TaskControlSchedules command = ControlScheduleHelper.GetKindOfCommand(task.description);

				switch (command)
				{
					case TaskControlSchedules.CtrlVolume:	//	볼륨인 경우 적용 범위(START-END)가 존재한다.
						_volumeControlPlaylist.RemoveTask(task);

						if ((currentVolumeControl != null) && (task.uuid.Equals(currentVolumeControl.uuid)))
						{
							PlayNextStoredVolumeControlTask();
						}
						break;
					default:
						break;
				}

			}
			else
			{
				logger.Error("[CmdDefaultProgram] Unknown TaskState : " + task.state.ToString());
			}
		}
		#endregion

		#region 프로그램 스케줄 처리
		/// <summary>
		/// 프로그램 스케줄 처리
		/// </summary>
		/// <param name="task"></param>
		/// <param name="tasks"></param>
		void ProgramTask(DigitalSignage.PlayAgent.Task task, DigitalSignage.PlayAgent.Task[] tasks)
		{
			if (task.state == TaskState.StateWait)
			{
				if (task.TaskEnd > TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()))
				{
					//	현재 시간보다 스케줄 종료 시간이 더 크다면

					//	1. 일일 내 시간 검사
					//	2. 요일 검사
					if (false == (CheckContainTimeofDay(task.starttimeofday, task.endtimeofday) && CheckDayOfWeek(task.daysofweek)))
					{
						//	현재 시간과 비교하여 하루 시간 대와 요일에 맞지 않는다면.
						config.localTasksList.ChangeTaskState(TaskState.StateWait, task.uuid);
						return ;
					}
					logger.Info("Task " + task.ToString());

					logger.Info(String.Format("[CmdProgram][{0}] StateWait.", task.description));

					config.localTasksList.ChangeTaskState(TaskState.StateRunning, task.uuid);

					lock (lockScreen)
					{
						if (currentScreen != null && currentScreen.type == TaskCommands.CmdProgram && currentScreen.priority > task.priority)
						{
							logger.Info("[" + task.description + " (" + task.uuid + ")] Added to playlist. It is lower priority than current.");
							_backscreenPlaylist.Push(task);
							return ;
						}

						if (currentScreen != null && currentScreen.type == TaskCommands.CmdProgram && currentScreen.uuid != task.uuid)
							_backscreenPlaylist.Push(currentScreen);

						if (false == PlayScreenTask(task))
							PlayNextStoredScreenTask();
					}
				}
				else
				{
					//	이미 시간이 지났는데 wait로 들어온 경우라면
					//	사용자가 Edit Task를 통해 변경한 경우이다.
					//	재생 지연 목록에서 요청된 Task를 제거해주고,
					//	현재 플레이중이라면 종료를 해준 후 다음 스크린을 재생한다.
					logger.Info("[" + task.description + " (" + task.uuid + ")] End time is wrong. It will be canceled.");
					config.localTasksList.ChangeTaskState(TaskState.StateProcessed, task.uuid);

					_backscreenPlaylist.RemoveTask(task);
					// 									iVisionLog.GeneralLogError(task.description, task.type, ErrorCodes.IS_ERROR_INVALID_TIME_SCOPE, "This schedule is alreay ended.");

					lock (lockScreen)
					{
						if (currentScreen != null && currentScreen.uuid == task.uuid)
						{
							PlayNextStoredScreenTask();
						}
					}
				}
			}
			else if (task.state == TaskState.StateEdit)
			{
				logger.Info("Task " + task.ToString());

				logger.Info(String.Format("[CmdProgram][{0}] StateEdit.", task.description));

				//	적용중인 Task가 있다면 지워준다 
				_backscreenPlaylist.RemoveTask(task);

				lock (lockScreen)
				{
					//	만약 현재 재생하고 있다면!!
					if (currentScreen != null && currentScreen.uuid == task.uuid)
					{
						// 다음 Task를 재생하자
						PlayNextStoredScreenTask();
					}
				}
				if (!IsDownloadComplete(task.duuid.ToString()))
				{
					logger.Info(String.Format("[CmdProgram][{0}] state has been modified as 'Download'.", task.description));

					config.localTasksList.ChangeTaskState(TaskState.StateDownload, task.uuid);
				}
				else
					config.localTasksList.ChangeTaskState(TaskState.StateWait, task.uuid);
			}
			else if (task.state == TaskState.StateRunning)
			{
				if (task.TaskEnd <= TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()))
				{
					//	스케줄 종료시간 보다 현재 시간이 더 크다면
					logger.Info("[" + task.description + " (" + task.uuid + ")] will be ended.");

					config.localTasksList.ChangeTaskState(TaskState.StateProcessed, task.uuid);
					_backscreenPlaylist.RemoveTask(task);

					bool bConnectedScreen = IsConnectedScreenTask(tasks, task.TaskEnd);
					if ((currentScreen != null) && (task.uuid.Equals(currentScreen.uuid)))
					{
						if (false == bConnectedScreen)
							PlayNextStoredScreenTask();
						else
							currentScreen = null;

					}
				}
				else
				{
					//	현재 시간보다 스케줄 종료 시간이 더 크다면

					//	1. 일일 내 시간 검사
					//	2. 요일 검사
					if (false == (CheckContainTimeofDay(task.starttimeofday, task.endtimeofday) && CheckDayOfWeek(task.daysofweek)))
					{
						//	현재 시간과 비교하여 하루 시간 대와 요일에 맞지 않는다면.
						config.localTasksList.ChangeTaskState(TaskState.StateWait, task.uuid);
						_backscreenPlaylist.RemoveTask(task);

						if ((currentScreen != null) && (task.uuid.Equals(currentScreen.uuid)))
						{
							PlayNextStoredScreenTask();
						}
						return ;
					}
					else
						config.localTasksList.ChangeTaskState(TaskState.StateRunning, task.uuid);

				}


			}
			else if (task.state == TaskState.StateCanceled)
			{
				logger.Info("Task " + task.ToString());

				logger.Info(String.Format("[CmdProgram][{0}] StateCanceled.", task.description));

				config.localTasksList.ChangeTaskState(TaskState.StateProcessed, task.uuid);
				_backscreenPlaylist.RemoveTask(task);

				bool bConnectedScreen = IsConnectedScreenTask(tasks, task.TaskEnd);

				if ((currentScreen != null) && (task.uuid.Equals(currentScreen.uuid)) &&
					false == bConnectedScreen)
				{
					PlayNextStoredScreenTask();
				}
				else if ((currentScreen != null) && (task.uuid.Equals(currentScreen.uuid)) &&
					bConnectedScreen == true)
				{
					currentScreen = null;

				}

			}
		}
		#endregion

		private void SubtitleTask(DigitalSignage.PlayAgent.Task task)
		{
			if (task.state == TaskState.StateWait)
			{
				if (task.TaskEnd > TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()))
				{
					//	현재 시간보다 스케줄 종료 시간이 더 크다면

					//	1. 일일 내 시간 검사
					//	2. 요일 검사
					if (false == (CheckContainTimeofDay(task.starttimeofday, task.endtimeofday) && CheckDayOfWeek(task.daysofweek)))
					{
						//	현재 시간과 비교하여 하루 시간 대와 요일에 맞지 않는다면.
						config.localTasksList.ChangeTaskState(TaskState.StateWait, task.uuid);
						return ;
					}
					logger.Info("Task " + task.ToString());

					logger.Info(String.Format("[CmdSubtitles][{0}] StateWait", task.description));

					config.localTasksList.ChangeTaskState(TaskState.StateRunning, task.uuid);

					if (currentSub != null && currentSub.priority > task.priority)
					{
						logger.Info("[" + task.description + " (" + task.uuid + ")] Added to playlist. It is lower priority than current.");
						_subtitlePlaylist.AddTask(task);
						return ;
					}

					if (currentSub != null && currentSub.uuid != task.uuid)
					{
						_subtitlePlaylist.AddTask(currentSub);
					}

					if (false == PlaySubtitleTask(task))
					{
						PlayNextStoredSubtitleTask();
					}
				}
				else
				{
					//	이미 시간이 지났는데 wait로 들어온 경우라면
					//	사용자가 Edit Task를 통해 변경한 경우이다.
					//	재생 지연 목록에서 요청된 Task를 제거해주고,
					//	현재 플레이중이라면 종료를 해준 후 다음 스크린을 재생한다.
					logger.Info("[" + task.description + " (" + task.uuid + ")] End time is wrong. It will be canceled.");
					config.localTasksList.ChangeTaskState(TaskState.StateProcessed, task.uuid);

					_subtitlePlaylist.RemoveTask(task);
					// 									iVisionLog.GeneralLogError(task.description, task.type, ErrorCodes.IS_ERROR_INVALID_TIME_SCOPE, "This schedule is alreay ended.");

					if (currentSub != null && currentSub.uuid == task.uuid)
					{
						PlayNextStoredSubtitleTask();
					}
				}
			}
			else if (task.state == TaskState.StateEdit)
			{
				logger.Info("Task " + task.ToString());

				logger.Info(String.Format("[CmdSubtitles][{0}] StateEdit", task.description));

				_subtitlePlaylist.RemoveTask(task);

				if ((currentSub != null) && (task.uuid.Equals(currentSub.uuid)))
				{
					PlayNextStoredSubtitleTask();
				}

				if (!IsDownloadComplete(task.duuid.ToString()))
				{
					logger.Info("state has been modified as 'Download'");
					config.localTasksList.ChangeTaskState(TaskState.StateDownload, task.uuid);
				}
				else
					config.localTasksList.ChangeTaskState(TaskState.StateWait, task.uuid);


			}
			if (task.state == TaskState.StateRunning)
			{
				if (task.TaskEnd <= TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()))
				{
					//	스케줄 종료시간 보다 현재 시간이 더 크다면
					logger.Info("[" + task.description + " (" + task.uuid + ")] will be ended.");

					config.localTasksList.ChangeTaskState(TaskState.StateProcessed, task.uuid);
					_subtitlePlaylist.RemoveTask(task);

					if ((currentSub != null) && (task.uuid.Equals(currentSub.uuid)))
					{
						PlayNextStoredSubtitleTask();
					}
				}
				else
				{
					//	현재 시간보다 스케줄 종료 시간이 더 크다면

					//	1. 일일 내 시간 검사
					//	2. 요일 검사
					if (false == (CheckContainTimeofDay(task.starttimeofday, task.endtimeofday) && CheckDayOfWeek(task.daysofweek)))
					{
						logger.Info("[" + task.description + " (" + task.uuid + ")] will be ended.");

						//	현재 시간과 비교하여 하루 시간 대와 요일에 맞지 않는다면.
						config.localTasksList.ChangeTaskState(TaskState.StateWait, task.uuid);
						_subtitlePlaylist.RemoveTask(task);

						if ((currentSub != null) && (task.uuid.Equals(currentSub.uuid)))
						{
							PlayNextStoredSubtitleTask();
						}
						return ;
					}
					else
						config.localTasksList.ChangeTaskState(TaskState.StateRunning, task.uuid);

				}

			}
			if (task.state == TaskState.StateCanceled)
			{
				logger.Info("Task " + task.ToString());

				logger.Info(String.Format("[CmdSubtitles][{0}] StateCanceled", task.description));

				_subtitlePlaylist.RemoveTask(task);

				config.localTasksList.ChangeTaskState(TaskState.StateProcessed, task.uuid);

				if (currentSub != null && currentSub.uuid.Equals(task.uuid))
				{
					PlayNextStoredSubtitleTask();
				}
			}
		}

		/// <summary>
		/// Task 관련 업무를 수행할 스레드 함수. 로컬 DB를 쿼리해 현재 이용가능한 데이터를 처리한다. 
		/// </summary>
        private void runTasks()
        {
            try
            {
                // reading task list
				DigitalSignage.PlayAgent.Task[] tasks = config.localTasksList.GetActiveTask();
                if (tasks == null)
                {
                    logger.Info("No active tasks");
                    return; // nothing new
                }

                for (int i = 0; i < tasks.Length; i++)
                {
                    //running task
                    switch ((TaskCommands)tasks[i].type)
                    {
						case TaskCommands.CmdGetRefData:
							logger.Info("[CmdGetRefData] is started.");
							config.localTasksList.ChangeTaskState(TaskState.StateDownload, tasks[i].uuid);
							break;
                        case TaskCommands.CmdInitPlayer:
							logger.Info("[CmdInitPlayer] is started.");
							// reset tasks, that are marked as running
							config.localTasksList.ResetRunningTasks();
							logger.Info("Running tasks are updated to 'Wait' state.");

							//	볼륨을 예전 볼륨으로 되돌린다.
							RestoreVolume();

							break;

						case TaskCommands.CmdNextDefaultProgram:
							logger.Info("[CmdNextDefaultProgram] is started.");
							{
								if (currentScreen != null && currentScreen.type == TaskCommands.CmdDefaultProgram)
								{
									PlayNextDefaultScreenTask();
								}
								else
								{
									logger.Error("[CmdNextDefaultProgram] Current screen is not 'Default' task.");
								}
							}
							break;
						case TaskCommands.CmdDefaultProgram:
							
							RollingTask(tasks[i]);
							break;

						case TaskCommands.CmdControlSchedule:
							
							ControlTask(tasks[i]);
							break;

						case TaskCommands.CmdProgram:

							ProgramTask(tasks[i], tasks);
                            break;

						case TaskCommands.CmdSubtitles:

							SubtitleTask(tasks[i]);
							break;

                        case TaskCommands.CmdEcho:
                        case TaskCommands.CmdUndefined:
#if DEBUG
                        logger.Info( "Undefined task runned :" + tasks[i].uuid.ToString());
#endif
                            break;
						case TaskCommands.CmdSynchronizeNow:
                            break;
                        case TaskCommands.CmdClearStorage:
                            {
								logger.Info("[CmdClearStorage] is started.");
								
								Thread ts = new Thread(this.clearStorage);
                                ts.Priority = ThreadPriority.BelowNormal;
                                ts.Start();
                            }
                            break;
						case TaskCommands.CmdReturnStatus:
							{
							}
							break;

                        case TaskCommands.CmdWriteRS232:
                            {
								logger.Info("[CmdWriteRS232] is started.");

								Thread ts = new Thread(delegate()
                                  { 
                                     SendRS232.SendTask( tasks[i].duuid );
                                  });
                               ts.Priority = ThreadPriority.Normal;
                               ts.Start();
			                   config.localTasksList.ChangeTaskState(TaskState.StateProcessed, tasks[i].uuid);
                            }
                            break;
                        default:
                            logger.Info("Unknown task type");
                            break;
                    }
                }
            }
            catch (Exception err)
            {
                logger.Error(err + " "+err.StackTrace);
            }
        }

		/// <summary>
		/// 기본스케줄 리스트를 생성한다.
		/// </summary>
		private void InitDefaultScheduleList()
		{
			//	스크린 추가

			//	스크린 재생
			if(currentScreen == null)
			{
				PlayScreenTask(_defaultScreenPlaylist.Next);
			}
		}

		/// <summary>
		/// 지정된 시간에 DefaultScreen을 교체해주기 위한 Task
		/// </summary>
		/// <param name="duration">지속 시간(초)</param>
		private void RegisterNextDefaultScreen(long duration)
		{
			//	default Screen Player thread
			try
			{
				Thread temp = null;
				if (thDefaultScreenPlayer != null && thDefaultScreenPlayer.IsAlive)
					temp = thDefaultScreenPlayer;

				thDefaultScreenPlayer = new Thread(this.defaultScreenTimer);
				thDefaultScreenPlayer.Priority = ThreadPriority.BelowNormal;
				thDefaultScreenPlayer.Start(duration);
				
				if (temp != null) temp.Abort();

			}
			catch
			{
			}
				

/* *
			DigitalSignage.PlayAgent.Task t = new DigitalSignage.PlayAgent.Task();
			t.pid = config.PlayerId;
			t.gid = config.GroupId;
			t.type = TaskCommands.CmdNextDefaultProgram;
			t.uuid = DigitalSignage.PlayAgent.Task.TaskDefaultScreen;
			t.TaskStart = TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()) + duration - 1;
			t.description = "CmdNextDefaultProgram";
			t.duration = duration;

			InsertOrReplaceTask(t);
**/
		}

		/// <summary>
		/// 다음 기본 스케줄을 재생한다
		/// </summary>
		private void PlayNextDefaultScreenTask()
		{
			DigitalSignage.PlayAgent.Task next = null;

			int nRetCount = 0;
			do
			{
				next = _defaultScreenPlaylist.Next;
			}
			while (false == PlayScreenTask(next) && nRetCount++ <= _defaultScreenPlaylist.Count);

			if (next != null) RegisterNextDefaultScreen(next.duration);

		}

		/// <summary>
		/// 저장된 다음 볼륨 조절 스케줄을 적용한다.
		/// </summary>
		private void PlayNextStoredVolumeControlTask()
		{
			DigitalSignage.PlayAgent.Task t = _volumeControlPlaylist.First;

			while (t != null && !PlayControlSchedule(t))
			{
				t = _volumeControlPlaylist.NextToEnd;
			}

			if (t == null)
			{
				//	볼륨을 되돌린다.
				RestoreVolume();
				currentVolumeControl = null;
			}

// 			DigitalSignage.PlayAgent.Task t = null;
// 
// 			while (null != (t = _volumeControlPlaylist.Pop()))
// 			{
// 				if (t.TaskEnd <= TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()))
// 					continue;
// 
// 				if (!PlayControlSchedule(t))
// 					continue;
// 
// 				break;
// 			}
// 
// 			if (t == null)
// 			{
// 				//	볼륨을 되돌린다.
// 				RestoreVolume();
// 				currentVolumeControl = null;
// 			}
		}

		private bool PlayControlSchedule(DigitalSignage.PlayAgent.Task task)
		{
			if (task != null)
			{
				//	시간 및 요일 검색
				if (!CheckContainStartAndEnd(task.TaskStart, task.TaskEnd) ||
					!CheckDayOfWeek(task.daysofweek) ||
					!CheckContainTimeofDay(task.starttimeofday, task.endtimeofday))
				{
					return false;
				}

				TaskControlSchedules command = ControlScheduleHelper.GetKindOfCommand(task.description);
				switch (command)
				{
					case TaskControlSchedules.CtrlVolume:
						{
							StoreCurrentVolume();
							if (ControlScheduleHelper.PlayControlSchedule(task.description, task.duuid))
							{
								currentVolumeControl = task;
							}

							break;
						}
					default:
						ControlScheduleHelper.PlayControlSchedule(task.description, task.duuid);
						break;
				}

				return true;
			}
			else
			{
				//	unload screen
				currentVolumeControl = null;
				return false;
			}
		}

		/// <summary>
		/// 저장해둔 볼륨 정보가 없다면 현재 볼륨을 저장해 둔다.
		/// </summary>
		private void StoreCurrentVolume()
		{
			int nWVol = config.StoredWaveVolume;
			int nMVol = config.StoredMasterVolume;
			if (nWVol == -1)
			{
				config.StoredWaveVolume = PCControl.GetWaveVolume();
			}
			if (nMVol == -1)
			{
				config.StoredMasterVolume = PCControl.GetMasterVolume();
			}
		}

		/// <summary>
		/// 저장해둔 볼륨이 있다면 볼륨을 되돌리고 저장정보를 초기화한다..
		/// </summary>
		private void RestoreVolume()
		{
			int nMVol = config.StoredMasterVolume;
			int nWVol = config.StoredWaveVolume;
			if (nWVol != -1)
			{
				if (PCControl.SetWaveVolume(nWVol))
				{
					logger.Info(String.Format("The wave volume has been restored to {0}.", nWVol));
					config.StoredWaveVolume = -1;
				}
			}
			if (nMVol != -1)
			{
				if (PCControl.SetMasterVolume(nMVol))
				{
					logger.Info(String.Format("The master volume has been restored to {0}.", nMVol));
					config.StoredMasterVolume = -1;
				}
			}
		}

		/// <summary>
		/// 저장된 다음순서 스크린을 재생한다
		/// </summary>
		private void PlayNextStoredScreenTask()
		{
			DigitalSignage.PlayAgent.Task t = _backscreenPlaylist.First;

			while(t != null && !PlayScreenTask(t))
			{
				t = _backscreenPlaylist.NextToEnd;
			}

			if (t == null)
			{
				PlayNextDefaultScreenTask();
			}

// 			while (null != (t = _backscreenPlaylist.Pop()))
// 			{
// 				if (t.TaskEnd <= TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()))
// 					continue;
// 
// 				if (!PlayScreenTask(t))
// 					continue;
// 
// 				break;
// 			}
// 
		}

		private void PlayNextStoredSubtitleTask()
		{
			DigitalSignage.PlayAgent.Task t = _subtitlePlaylist.First;

			while (!PlaySubtitleTask(t))
			{
				t = _subtitlePlaylist.NextToEnd;
			}


// 			DigitalSignage.PlayAgent.Task t = null;
// 
// 			while (null != (t = _subtitlePlaylist.Pop()))
// 			{
// 				if (t.TaskEnd <= TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()))
// 					continue;
// 				break;
// 			}
// 			PlaySubtitleTask(t);

		}
		/// <summary>
		/// 스크린를 즉시 재생한다.
		/// </summary>
		/// <param name="task">재생할 작업</param>
		private bool PlayScreenTask(DigitalSignage.PlayAgent.Task task)
		{
			if (task != null)
			{
				//	시간 및 요일 검색
				if (!CheckContainStartAndEnd(task.TaskStart, task.TaskEnd) || 
					!CheckDayOfWeek(task.daysofweek) ||
					!CheckContainTimeofDay(task.starttimeofday, task.endtimeofday))
				{
					return false;
				}

				logger.Info("[" + task.description + " (" + task.uuid + ")] will be started.");

				String sFilePath = config.AppData + "xaml\\" + task.duuid.ToString() + "\\program.xml";
				logger.Info("Loading [" + sFilePath + "]");
				if(parent.loadProject(sFilePath))
				{
					currentScreen = task;
					return true;
				}

				return false;
			}
			else
			{
				//	unload screen
				parent.loadProject("");
				currentScreen = null;
				return true;
			}
		}

		private bool PlaySubtitleTask(DigitalSignage.PlayAgent.Task task)
		{
			if(task != null)
			{
				//	시간 및 요일 검색
				if (!CheckContainStartAndEnd(task.TaskStart, task.TaskEnd) ||
					!CheckDayOfWeek(task.daysofweek) ||
					!CheckContainTimeofDay(task.starttimeofday, task.endtimeofday))
				{
					return false;
				}

				String sFilePath = config.AppData + "xaml\\" + task.duuid.ToString() + "\\program.xml";
				logger.Info("Loading [" + sFilePath + "]");
				if(parent.setSubtitle(sFilePath))
				{
					previewSub = currentSub;
					currentSub = task;
					return true;
				}
				return false;
			}
			else
			{
				parent.setSubtitle(""); //unload project
				previewSub = null;
				currentSub = null;
				return true;
			}
		}
		/// <summary>
		/// Task 리스트 중 endtime이 시작시간과 연결되어있는지 검사한다.
		/// </summary>
		/// <param name="tasks">검사할 Task 리스트</param>
		/// <param name="endtime">기준이될 끝 시간</param>
		/// <returns>만약 true라면 연결된 Task가 있다는 것이고, false면 없다는 것.</returns>
		private bool IsConnectedScreenTask(DigitalSignage.PlayAgent.Task[] tasks, long endtime)
		{
			foreach (DigitalSignage.PlayAgent.Task task in tasks)
			{
				if (task.type == TaskCommands.CmdScreen && task.TaskStart == endtime)
					return true;
			}
			return false;
		}

		/// <summary>
		/// Task가 중복되는 uuid를 가질 수 없기 때문에 중복된다면 지우고 추가해준다.
		/// </summary>
		/// <param name="task">추가할 Task</param>
		/// <returns>Reserved</returns>
		private int InsertOrReplaceTask(DigitalSignage.PlayAgent.Task task)
		{
			int nRet = -1;
			try
			{
				if (-1 == (nRet = config.localTasksList.AddTask(task)))
				{
					////	추가가 되지않음.
					config.localTasksList.CleanTask(task.uuid);
					//	다시 추가 시도
					nRet = config.localTasksList.AddTask(task);
				}
			}
			catch(Exception e)
			{
				logger.Error(e.Message);
			}
			return nRet;
		}

		/// <summary>
		/// local 저장소를 정리한다.
		/// </summary>
        private void clearStorage()
        {
            try
            {
                Cleaner cleaner = new Cleaner();
                cleaner.deleteLostProjects();
                cleaner.deleteOldProjects();
            }
            catch (Exception e)
            {
                logger.Error(e.ToString());
            }
        }

		/// <summary>
		/// 요일 체크
		/// </summary>
		/// <param name="dayofweek"></param>
		/// <returns></returns>
		private bool CheckDayOfWeek(int dayofweek)
		{
			DayOfWeek dow = DateTime.Today.DayOfWeek;
			if (dow == DayOfWeek.Monday)
				return 0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_MON);
			else if (dow == DayOfWeek.Tuesday)
				return 0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_TUE);
			else if (dow == DayOfWeek.Wednesday)
				return 0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_WED);
			else if (dow == DayOfWeek.Thursday)
				return 0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_THU);
			else if (dow == DayOfWeek.Friday)
				return 0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_FRI);
			else if (dow == DayOfWeek.Saturday)
				return 0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_SAT);
			else if (dow == DayOfWeek.Sunday)
				return 0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_SUN);
			else
			{
				logger.Error("Exception : invalid DayOfWeek. : " + dayofweek.ToString());
			}
			return false;
		}

		/// <summary>
		/// 시작과 끝시간이 현재 시간에 들어오는지 체크
		/// </summary>
		/// <param name="start"></param>
		/// <param name="end"></param>
		/// <returns></returns>
		private bool CheckContainStartAndEnd(long start, long end)
		{
			long currTime = TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime());
			return (start <= currTime && end > currTime);
		
		}

		/// <summary>
		/// 현재 시간이 TimeOfDay에 포함되는지
		/// </summary>
		/// <param name="nStartTime"></param>
		/// <param name="nEndTime"></param>
		/// <returns></returns>
		private bool CheckContainTimeofDay(int nStartTime, int nEndTime)
		{
			//	둘다 같은 값이라면 Whole Day로 간주
			if (nStartTime == nEndTime) return true;

			DateTime dtLocal = DateTime.Now;
			int nCurr = dtLocal.Second + (dtLocal.Minute * 60) + (dtLocal.Hour * 3600);

			if(nStartTime < nEndTime)
			{
				//	시간대가 하루에 국한되는 경우

				return nCurr >= nStartTime && nCurr < nEndTime;
			}
			else
			{
				//	시간대가 오늘부터 다음 날로 넘어 간 경우
				return nCurr < nStartTime || nCurr >= nEndTime;				
			}
		}


		/// <summary>
		/// 다운로드 완료되었는지 체크
		/// </summary>
		/// <param name="duuid"></param>
		/// <returns></returns>
		bool IsDownloadComplete(string duuid)
		{
			try
			{
				string sDest = config.AppData + "xaml\\" + duuid + "\\downloaded";

				System.IO.FileInfo fi = new System.IO.FileInfo(sDest);

				if (fi == null) return false;

				return fi.Exists;
			}
			catch (Exception e)
			{
				logger.Error(e.ToString());
				return false;
			}
		}
		#region Logging

		private void SubtitleStarted(string sName)
		{
			try
			{
				if (client_logsTA == null)
					client_logsTA = config.logdbLayer.CreateClientLogDA();

				client_logsTA.InsertLogs(sName, currentSub.description, "Started", (int)TaskCommands.CmdSubtitles, (int)LogCategory.CatProcceed, 0, TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()));

			}
			catch (Exception e)
			{
				logger.Error(e.Message);
			}
		}

		private void SubtitleEnded(string sName)
		{
			try
			{
				if (client_logsTA == null)
					client_logsTA = config.logdbLayer.CreateClientLogDA();

				client_logsTA.InsertLogs(sName, currentSub.description, "Ended", (int)TaskCommands.CmdSubtitles, (int)LogCategory.CatProcceed, 0, TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()));
			}
			catch (Exception e)
			{
				logger.Error(e.Message);
			}
		}

		private void SubtitleErrorOccurred(string sName, int code, string description)
		{
			try
			{
				if (client_logsTA == null)
					client_logsTA = config.logdbLayer.CreateClientLogDA();

				client_logsTA.InsertLogs(sName, currentSub.description, description, (int)TaskCommands.CmdSubtitles, (int)LogCategory.CatError, code, TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()));
			}
			catch (Exception e)
			{
				logger.Error(e.Message);
			}
		}

		private void ScreenStarted(string sName)
		{
			try
			{
				if (client_logsTA == null)
					client_logsTA = config.logdbLayer.CreateClientLogDA();

				client_logsTA.InsertLogs(sName, currentScreen.description, "Started", (int)currentScreen.type, (int)LogCategory.CatProcceed, 0, TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()));

			}
			catch (Exception e)
			{
				logger.Error(e.Message);
			}
		}

		private void ScreenEnded(string sName)
		{
			try
			{
				if (client_logsTA == null)
					client_logsTA = config.logdbLayer.CreateClientLogDA();

				client_logsTA.InsertLogs(sName, currentScreen.description, "Ended", (int)currentScreen.type, (int)LogCategory.CatProcceed, 0, TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()));
			}
			catch (Exception e)
			{
				logger.Error(e.Message);
			}
		}

		private void ScreenErrorOccurred(string sName, int code, string description)
		{
			try
			{
				if (client_logsTA == null)
					client_logsTA = config.logdbLayer.CreateClientLogDA();

				client_logsTA.InsertLogs(sName, currentScreen.description, description, (int)currentScreen.type, (int)LogCategory.CatError, code, TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()));
			}
			catch (Exception e)
			{
				logger.Error(e.Message);
			}
		}

		private void WriteInfo(string title, TaskCommands type, String message)
		{
			try
			{
				if (client_logsTA == null)
					client_logsTA = config.logdbLayer.CreateClientLogDA();

				client_logsTA.InsertLogs("", title, message, (int)type, (int)LogCategory.CatProcceed, 0, TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()));
			}
			catch (Exception e)
			{
				logger.Error(e.Message);
			}
		}

		private void WriteError(string title, TaskCommands type, ErrorCodes code, String message)
		{
			try
			{
				if (client_logsTA == null)
					client_logsTA = config.logdbLayer.CreateClientLogDA();

				client_logsTA.InsertLogs("", title, message, (int)type, (int)LogCategory.CatError, (int)code, TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()));
			}
			catch (Exception e)
			{
				logger.Error(e.Message);
			}
		}
		#endregion
	}
}
