﻿using System;
using System.Collections;
using System.Runtime.InteropServices;
using System.Threading;
using System.Collections.ObjectModel;

using DigitalSignage.Common;
// using DigitalSignage.DataBase;

using NLog;

using DigitalSignage.PlayAgent;
using SerialPortController;

namespace DigitalSignage.Player
{
    class TimerLoop
	{
		#region Properties

		/// <summary>
		/// DB Logging Helper
		/// </summary>
		private iVisionLogHelper iVisionLog = iVisionLogHelper.GetInstance;

		/// <summary>
		/// Serial Port Control Instance
		/// </summary>
		ISerialPortControl _serialPortObject = null;

		/// <summary>
		/// 로컬 로깅 Instance
		/// </summary>
        private static Logger logger = LogManager.GetCurrentClassLogger();
		/// <summary>
		/// Configration Instance
		/// </summary>
		private Config config = null;

		#region Lock Object
		/// <summary>
		/// Lock 오브젝트
		/// </summary>
		private static readonly object semaphore = new object();
		/// <summary>
		/// Screen Lock 오브젝트
		/// </summary>
		private readonly object lockScreen = new object();
		#endregion

		/// <summary>
		/// 플레이어의 Visible 상태
		/// </summary>
		public bool IsPlayerVisible
		{
			get { return parent.IsPlayerVisible; }
        }

        #region Touch - 반복 스케줄 진행 제한 
        private static bool isPauseDefaultProgram;
        public static bool IsPauseDefaultProgram 
        {
            get { return isPauseDefaultProgram; }
            set { isPauseDefaultProgram = value; 
                if (value == false) 
                    waitDefaultProgramHandler.Set(); 
            }
        }
        public static EventWaitHandle waitDefaultProgramHandler = new AutoResetEvent(false);       
        #endregion
       
        private static TimerLoop instance = null;
		
		public static TimerLoop GetInstance
		{
			get
			{
				lock (semaphore)
				{
					if (instance == null)
					{
						instance = new TimerLoop();
					}
					return instance;
				}
			}
		}

		#region 스케줄 재생 List 
		/// <summary>
		/// 기본 반복 스케줄 재생 List
		/// </summary>
		private TaskPlaylist _defaultScreenPlaylist = new TaskPlaylist();
		/// <summary>
		/// 시간 스케줄 재생 List
		/// </summary>
		private TaskPlaylist _timeScreenPlaylist = new TaskPlaylist();
		/// <summary>
		/// 자막 스케줄 재생 List
		/// </summary>
		private TaskPlaylist _subtitlePlaylist = new TaskPlaylist();
		/// <summary>
		/// PC 볼륨 컨트롤 재생 List
		/// </summary>
		private TaskPlaylist _volumeControlPlaylist = new TaskPlaylist();
		/// <summary>
		/// Serial 볼륨 컨트롤 재생 List
		/// </summary>
		private TaskPlaylist _serialvolumeControlPlaylist = new TaskPlaylist();
		#endregion

		#region 스케줄 관리 List
		/// <summary>
		/// 시간 스케줄 관리 List
		/// </summary>
		private TaskPlaylist _managedtimeScreenPlaylist = new TaskPlaylist();
		/// <summary>
		/// 자막 스케줄 관리 List
		/// </summary>
		private TaskPlaylist _managedsubtitlePlaylist = new TaskPlaylist();
		/// <summary>
		/// 제어 스케줄 관리 List
		/// </summary>
		private TaskPlaylist _controlPlaylist = new TaskPlaylist();
		#endregion

		private bool processing_flag;
        private MainWindow parent;
		private DigitalSignage.PlayAgent.Task currentVolumeControl = null;
		private DigitalSignage.PlayAgent.Task currentSerialVolumeControl = null;
		private DigitalSignage.PlayAgent.Task currentScreen = null;
		private DigitalSignage.PlayAgent.Task currentSub = null;
		private DigitalSignage.PlayAgent.Task previewSub = null;		//	이전 서브타이틀
		private Thread control = null;

		//	hsshin
		private Thread thDefaultScreenPlayer = null;
		
		/// <summary>
		/// 현재 적용 중인 PC 볼륨 스케줄
		/// </summary>
		public DigitalSignage.PlayAgent.Task CurrentVolumeControl
		{
			get { return currentVolumeControl; }
		}

		/// <summary>
		/// 현재 적용중인 Serial 볼륨 스케줄
		/// </summary>
		public DigitalSignage.PlayAgent.Task CurrentSerialVolumeControl
		{
			get { return currentSerialVolumeControl; }
		}
		/// <summary>
		/// 현재 재생 중인 스크린 스케줄
		/// </summary>
		public DigitalSignage.PlayAgent.Task CurrentScreen
		{
			get { return currentScreen; }
		}
		/// <summary>
		/// 현재 자막 스케줄
		/// </summary>
		public DigitalSignage.PlayAgent.Task CurrentSubtitle
		{
			get { return currentSub; }
		}

		/// <summary>
		/// Log Db Adapter
		/// </summary>
		private DigitalSignage.ClientDataBase.LogsDataSetTableAdapters.client_logsTableAdapter client_logsTA = null;

		#endregion


		/// <summary>
		/// TimerLoop /시작
		/// </summary>
		/// <param name="parent"></param>
		/// <returns></returns>
		public int Start( MainWindow parent )
        {
            this.parent = parent;
            config = Config.GetConfig;

			iVisionLog.GeneralLog = WriteInfo;
			iVisionLog.GeneralLogError = WriteError;

			iVisionLog.ScreenLogStarted = ScreenStarted;
			iVisionLog.ScreenLogEnded = ScreenEnded;
			iVisionLog.ScreenLogError = ScreenErrorOccurred;
			
			iVisionLog.SubtitleLogStarted = SubtitleStarted;
			iVisionLog.SubtitleLogEnded = SubtitleEnded;
			iVisionLog.SubtitleLogError = SubtitleErrorOccurred;

            processing_flag = true;

			#region 시리얼 포트 연결
			try
			{
				SerialPortControlFactory factory = new SerialPortControlFactory();
				_serialPortObject = factory.CreateController(config.SerialPortDevice);
				if (_serialPortObject != null) _serialPortObject.Initialize();
			}
			catch (System.Exception e)
			{
				logger.Error(e.ToString());
			}
			#endregion

            //starting control thread
			control = new Thread(this.controlThread);
			control.Priority = ThreadPriority.Highest;
            control.Start();

            IsPauseDefaultProgram = false;
            return 0;
        }

		/// <summary>
		/// TimerLoop를 정지시킨다
		/// </summary>
		/// <returns>reserved</returns>
        public int Stop()
        {
            processing_flag = false;

			if (thDefaultScreenPlayer != null && thDefaultScreenPlayer.IsAlive)
			{
				thDefaultScreenPlayer.Abort();
				thDefaultScreenPlayer = null;
			}

            return 0;
        }

		/// <summary>
		/// 스크린 스케줄을 수행할 스레드를 생성한다.
		/// </summary>
		private void defaultScreenTimer(object param)
		{
			try
			{
				long duration = Convert.ToInt64(param);

				//0.1초 보정시간을 주자 - 뺐음
				Thread.Sleep((int)(duration * 1000) - 100);
				logger.Debug("Screen Load Started!");

				lock (lockScreen)
				{
                    #region Touch - Program의 스케줄 시간 종료시 (Sleep으로 스케줄 시간까지대기) Touch 실행 플래그가 true이면 대기.
                    waitDefaultProgramHandler.WaitOne();
                    #endregion

					//	hsshin 반복기능을 시간스케줄도 같이 쓰도록 구성
                    if (currentScreen != null && (currentScreen.type == TaskCommands.CmdDefaultProgram || currentScreen.type == TaskCommands.CmdTouchDefaultProgram))
					{
						PlayNextDefaultScreenTask();
					}
					else if (currentScreen != null && (currentScreen.type == TaskCommands.CmdProgram))
					{
						PlayNextTimeScreenTask();
					}
					else
						logger.Error("Current screen is not 'Default' task.");
				}

			}
			catch
			{
			}
		}

		/// <summary>
		/// 1초마다 Task루틴을 수행할 스레드를 생성한다.
		/// </summary>
        private void controlThread()
        {
			logger.Info("Starting Timeloop thread...");

			try
			{
				//	볼륨을 예전 볼륨으로 되돌린다.
				RestoreVolume();
				RestoreSerialVolume();
			}
			catch { }
			config.localTasksList.ResetRunningTasks();
			logger.Info("Running tasks are updated to 'Wait' state.");

            ControlScheduleHelper.GetInstance.OnChangeStatus += new ChangeStatusEventHandler(ControlScheduleHelper_OnChangeStatus);
            
			while (processing_flag)
            {
                //starting task starter thread
//                 Thread ts = new Thread(this.runTasks);
//                 ts.Priority = ThreadPriority.BelowNormal;
//                 ts.Start();
				runTasks();

                Thread.Sleep(1000);
            }

			logger.Info("Ending Timeloop thread...");
        }

        /// <summary>
        /// Touch
        /// </summary>
        public void PlayTouchRollingTask()
        {            
            for (int i = 0; i < _defaultScreenPlaylist.Count; i++)
            {
                DigitalSignage.PlayAgent.Task next = _defaultScreenPlaylist.Next;
                if (next == null)
                    continue;
                
                if (next.type == TaskCommands.CmdTouchDefaultProgram)
                {
                    if (PlayScreenTask(next) == true)
                    {
                        RegisterNextScreen(next.duration);
                        break;
                    }
                }
            }            
        
        }

		/// <summary>
		/// 데이터베이스 상에 State 가 Processed 또는 Processing이면 변경한다. 왜냐하면 
		/// PlayAgent에서 State를 변경했을 경우 공교롭게 엎어쓸수도 있기때문이다. 
		/// </summary>
		/// <param name="willChange"></param>
		/// <param name="uuid"></param>
		void ChangeTaskState(TaskState willChange, Guid uuid)
		{
			try
			{
				TaskState state = (TaskState)config.localTasksList.GetTaskState(uuid);
				logger.Debug(String.Format("Current DB State = {0}, Will Change = {1}, uuid = {2}",state, willChange, uuid));

				if (state == TaskState.StateProcessed || state == TaskState.StateProcessing)
				{
					config.localTasksList.ChangeTaskState(willChange, uuid);
				}
				else
				{
					logger.Info("TaskState는 변경 되지 않음. 서버에서 메시지가 변경됨.");
				}
			}
			catch (System.Exception e)
			{
				logger.Error(e.Message);
			}
		}

		public object SerialPortDeviceStatus(SerialPortStatus status)
		{
			if(_serialPortObject == null)
			{
				return null;
			}

			return _serialPortObject.Status(status);
		}

		#region 반복 스케줄 처리
		/// <summary>
		/// 반복 스케줄 처리
		/// </summary>
		/// <param name="task">해당 Task</param>
		private void RollingTask(DigitalSignage.PlayAgent.Task task)
		{
			if (task.state == TaskState.StateWait)
			{
				logger.Info("Task " + task.ToString());

				logger.Info(String.Format("[CmdDefaultProgram][{0}] StateWait", task.description));

				if (task.TaskEnd >= TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()))
				{
					_defaultScreenPlaylist.AddTask(task);
					ChangeTaskState(TaskState.StateRunning, task.uuid);
					//	아무것도 플레이 중이 아니면 재생해준다.
					if (currentScreen == null/* && false == _defaultScreenPlaylist.IsStarted*/)
					{
						PlayNextDefaultScreenTask();
                        //waitCurrentScreenPlayHandler.WaitOne();
                    }
                    #region Touch : 처음 Player 실행시 Task 리스트의 모든 Task를 실행하여 주석 처리 함 - 확인 필요.
                    /*
                    else if (currentScreen != null && currentScreen.uuid == task.uuid)
                    {
                        PlayNextDefaultScreenTask();
                    }
                    */
                    #endregion
                    return ;
				}
				else
				{
					//	이미 시간이 지났는데 wait로 들어온 경우라면
					//	사용자가 Edit Task를 통해 변경한 경우이다.
					//	재생 지연 목록에서 요청된 Task를 제거해주고,
					//	현재 플레이중이라면 종료를 해준 후 다음 스크린을 재생한다.
					logger.Info("[" + task.description + " (" + task.uuid + ")] End time is wrong. It will be canceled.");
					ChangeTaskState(TaskState.StateProcessed, task.uuid);

					_defaultScreenPlaylist.RemoveTask(task);
					iVisionLog.GeneralLogError(task.description, task.type, ErrorCodes.IS_ERROR_INVALID_TIME_SCOPE, "This schedule is alreay ended.");

					if (currentScreen != null && currentScreen.uuid == task.uuid)
					{
						PlayNextDefaultScreenTask();
					}
				}

			}
			else if (task.state == TaskState.StateEdit)
			{
				logger.Info("Task " + task.ToString());

				logger.Info(String.Format("[CmdDefaultProgram][{0}] StateEdit.", task.description));

				//	적용중인 Task가 있다면 지워준다 
				_defaultScreenPlaylist.RemoveTask(task);

				//	만약 현재 재생하고 있다면!!
				if (currentScreen != null && currentScreen.uuid == task.uuid)
				{
					// 다음 Task를 재생하자
					PlayNextDefaultScreenTask();
				}
				if (!IsDownloadComplete(task.duuid.ToString()))
				{
					logger.Info(String.Format("[CmdDefaultProgram][{0}] state has been modified as 'Download'", task.description));
					ChangeTaskState(TaskState.StateDownload, task.uuid);
				}
				else
					ChangeTaskState(TaskState.StateWait, task.uuid);


			}
			else if (task.state == TaskState.StateRunning)
			{
				if (task.TaskEnd <= TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()))
				{
					logger.Info("Task " + task.ToString());

					//	스케줄 종료시간 보다 현재 시간이 더 크다면
					logger.Info(String.Format("[CmdDefaultProgram][{0}] StateRunning.", task.description));

					_defaultScreenPlaylist.RemoveTask(task);
					ChangeTaskState(TaskState.StateProcessed, task.uuid);

					if (currentScreen != null && currentScreen.uuid == task.uuid)
					{
						PlayNextDefaultScreenTask();
					}
				}
// 				else
// 					ChangeTaskState(TaskState.StateRunning, task.uuid);

				return ;

			}
			else if (task.state == TaskState.StateCanceled)
			{
				logger.Info("Task " + task.ToString());

				logger.Info(String.Format("[CmdDefaultProgram][{0}] StateCanceled.", task.description));

				_defaultScreenPlaylist.RemoveTask(task);
				ChangeTaskState(TaskState.StateProcessed, task.uuid);

				if (currentScreen != null && currentScreen.uuid == task.uuid)
				{
					PlayNextDefaultScreenTask();
				}

			}
			else
			{
				logger.Error("[CmdDefaultProgram] Unknown TaskState : " + task.state.ToString());
			}

			return ;
		}
		#endregion

		#region 제어 스케줄 처리

		/// <summary>
		/// 프로그램 스크린의 재생/종료을 담당한다.
		/// </summary>
		private void RunControlTask()
		{
			Collection<DigitalSignage.PlayAgent.Task> _arrWillRemove = new Collection<DigitalSignage.PlayAgent.Task>();

			DigitalSignage.PlayAgent.Task next = _controlPlaylist.First;

			//	현재 스케줄이 제어 스케줄이고 시간대가 표출 끝났다면
			if ((currentVolumeControl != null) && currentVolumeControl.type == TaskCommands.CmdControlSchedule)
			{
				if (!(CheckContainTimeofDay(currentVolumeControl.starttimeofday, currentVolumeControl.endtimeofday)
					&& CheckDayOfWeek(currentVolumeControl.daysofweek)))
				{
					logger.Info(String.Format("[CmdControlSchedule][{0}] 볼륨 스케줄 종료.", currentVolumeControl.description));
					currentVolumeControl = null;
				}
			}

			if ((currentSerialVolumeControl != null) && currentSerialVolumeControl.type == TaskCommands.CmdControlSchedule)
			{
				if (!(CheckContainTimeofDay(currentSerialVolumeControl.starttimeofday, currentSerialVolumeControl.endtimeofday)
					&& CheckDayOfWeek(currentSerialVolumeControl.daysofweek)))
				{
					logger.Info(String.Format("[CmdControlSchedule][{0}] 볼륨 스케줄 종료.", currentSerialVolumeControl.description));
					currentSerialVolumeControl = null;
				}
			}


			do
			{
				if (next == null) continue;

				//	1. 일일 내 시간 검사
				//	2. 요일 검사
				if (CheckContainTimeofDay(next.starttimeofday, next.endtimeofday) && CheckDayOfWeek(next.daysofweek))
				{
					//	현재 시간과 비교하여 하루 시간 대와 요일에 맞는다면..

					TaskControlSchedules command = ControlScheduleHelper.GetInstance.GetKindOfCommand(next.description);
					switch (command)
					{
						case TaskControlSchedules.CtrlVolume:	//	볼륨인 경우 적용 범위(START-END)가 존재한다.

							if ((currentVolumeControl != null) && (currentVolumeControl.uuid.Equals(next.uuid))) continue;

							if ((currentVolumeControl != null) &&
								(currentVolumeControl.type == TaskCommands.CmdControlSchedule) &&
								next.priority >= currentVolumeControl.priority)
							{
								// 재생 중인 스크린이 프로그램 스크린이고 우선순위가 재생 중인 스크린보다 높다면
								if (PlayControlSchedule(next))
								{
									logger.Info(String.Format("[CmdControlSchedule][{0}] 제어 스케줄 시작.", next.description));
								}
							}
							else if (currentVolumeControl == null)
							{
								// 재생 중인 스크린이 없거나 프로그램 스크린이 아닌경우

								if (PlayControlSchedule(next))
								{
									logger.Info(String.Format("[CmdControlSchedule][{0}] 제어 스케줄 시작.", next.description));
								}
							}
							break;
						case TaskControlSchedules.CtrlSerialVolume:	//	볼륨인 경우 적용 범위(START-END)가 존재한다.

							if ((currentSerialVolumeControl != null) && (currentSerialVolumeControl.uuid.Equals(next.uuid))) continue;

							if ((currentSerialVolumeControl != null) &&
								(currentSerialVolumeControl.type == TaskCommands.CmdControlSchedule) &&
								next.priority >= currentSerialVolumeControl.priority)
							{
								// 재생 중인 스크린이 프로그램 스크린이고 우선순위가 재생 중인 스크린보다 높다면
								if (PlayControlSchedule(next))
								{
									logger.Info(String.Format("[CmdControlSchedule][{0}] 제어 스케줄 시작.", next.description));
								}
							}
							else if (currentSerialVolumeControl == null)
							{
								// 재생 중인 스크린이 없거나 프로그램 스크린이 아닌경우

								if (PlayControlSchedule(next))
								{
									logger.Info(String.Format("[CmdControlSchedule][{0}] 제어 스케줄 시작.", next.description));
								}
							}
							break;
						default:
							if (PlayControlSchedule(next))
							{
								logger.Info(String.Format("[CmdControlSchedule][{0}] 제어 스케줄 시작.", next.description));
								if(next.starttimeofday == next.endtimeofday)
								{
									// 반복 패턴이 없는 경우
									_arrWillRemove.Add(next);
								}
							}

							break;
					}

				}

			}
			while ((next = _controlPlaylist.NextToEnd) != null);

			if (currentVolumeControl == null)
			{
				RestoreVolume();
			}
			if (currentSerialVolumeControl == null)
			{
				RestoreSerialVolume();
			}

			try
			{
				//	처리된 제어 스케줄을 제거한다. 
				foreach (DigitalSignage.PlayAgent.Task t in _arrWillRemove)
				{
					_controlPlaylist.RemoveTask(t);
				}
			}
			catch {}

		}

		/// <summary>
		/// 제어 스케줄 처리
		/// </summary>
		/// <param name="task"></param>
		private void ControlTask(DigitalSignage.PlayAgent.Task task)
		{
			if (task.state == TaskState.StateWait)
			{
				if (task.TaskEnd > TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()))
				{
					//	현재 시간보다 스케줄 종료 시간이 더 크다면

					_controlPlaylist.Push(task);

					logger.Info("Task " + task.ToString());

					logger.Info(String.Format("[CmdControlSchedule][{0}] StateWait.", task.description));

					ChangeTaskState(TaskState.StateRunning, task.uuid);
				}
				else
				{

					logger.Info("[" + task.description + " (" + task.uuid + ")] End time is wrong. It will be canceled.");
					ChangeTaskState(TaskState.StateProcessed, task.uuid);

					_controlPlaylist.RemoveTask(task);

					TaskControlSchedules command = ControlScheduleHelper.GetInstance.GetKindOfCommand(task.description);
					switch (command)
					{
						case TaskControlSchedules.CtrlVolume:	//	볼륨인 경우 적용 범위(START-END)가 존재한다.
							_volumeControlPlaylist.RemoveTask(task);

							if (currentVolumeControl != null && currentVolumeControl.uuid == task.uuid)
							{
								currentVolumeControl = null;
							}

							break;
						case TaskControlSchedules.CtrlSerialVolume:	//	볼륨인 경우 적용 범위(START-END)가 존재한다.
							_serialvolumeControlPlaylist.RemoveTask(task);

							if (currentSerialVolumeControl != null && currentSerialVolumeControl.uuid == task.uuid)
							{
								currentSerialVolumeControl = null;
							}

							break;
					}

					iVisionLog.GeneralLogError(task.description, task.type, ErrorCodes.IS_ERROR_INVALID_TIME_SCOPE, "This schedule is already ended.");
				}
			}
			else if (task.state == TaskState.StateEdit)
			{
				logger.Info("Task " + task.ToString());
				logger.Info(String.Format("[CmdControlSchedule][{0}] StateEdit.", task.description));

				_controlPlaylist.RemoveTask(task);

				TaskControlSchedules command = ControlScheduleHelper.GetInstance.GetKindOfCommand(task.description);
				switch (command)
				{
					case TaskControlSchedules.CtrlVolume:	//	볼륨인 경우 적용 범위(START-END)가 존재한다.
						//	적용중인 Task가 있다면 지워준다 
						_volumeControlPlaylist.RemoveTask(task);

						//	만약 현재 재생하고 있다면!!
						if (currentVolumeControl != null && currentVolumeControl.uuid == task.uuid)
						{
// 							// 다음 Task를 재생하자
							currentVolumeControl = null;
						}
						break;
					case TaskControlSchedules.CtrlSerialVolume:	//	볼륨인 경우 적용 범위(START-END)가 존재한다.
						//	적용중인 Task가 있다면 지워준다 
						_serialvolumeControlPlaylist.RemoveTask(task);

						//	만약 현재 재생하고 있다면!!
						if (currentSerialVolumeControl != null && currentSerialVolumeControl.uuid == task.uuid)
						{
							// 							// 다음 Task를 재생하자
							currentSerialVolumeControl = null;
						}
						break;
				}

				ChangeTaskState(TaskState.StateWait, task.uuid);
			}
			else if (task.state == TaskState.StateRunning)
			{
				if (task.TaskEnd <= TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()))
				{
					//	스케줄 종료시간 보다 현재 시간이 더 크다면

					logger.Info("[" + task.description + " (" + task.uuid + ")] will be ended.");
					ChangeTaskState(TaskState.StateProcessed, task.uuid);
					_controlPlaylist.RemoveTask(task);

					TaskControlSchedules command = ControlScheduleHelper.GetInstance.GetKindOfCommand(task.description);

					switch (command)
					{
						case TaskControlSchedules.CtrlVolume:	//	볼륨인 경우 적용 범위(START-END)가 존재한다.
							_volumeControlPlaylist.RemoveTask(task);

							if ((currentVolumeControl != null) && (task.uuid.Equals(currentVolumeControl.uuid)))
							{
								currentVolumeControl = null;
							}
							break;
						case TaskControlSchedules.CtrlSerialVolume:	//	볼륨인 경우 적용 범위(START-END)가 존재한다.
							//	적용중인 Task가 있다면 지워준다 
							_serialvolumeControlPlaylist.RemoveTask(task);

							//	만약 현재 재생하고 있다면!!
							if (currentSerialVolumeControl != null && currentSerialVolumeControl.uuid == task.uuid)
							{
								// 							// 다음 Task를 재생하자
								currentSerialVolumeControl = null;
							}
							break;
						default:
							break;
					}
				}
				else
				{
					//	현재 시간보다 스케줄 종료 시간이 더 크다면

					ChangeTaskState(TaskState.StateRunning, task.uuid);

				}

			}
			else if (task.state == TaskState.StateCanceled)
			{
				ChangeTaskState(TaskState.StateProcessed, task.uuid);
				logger.Info("Changing status to [StateCanceled]. (UUID=" + task.uuid + ")");

				_controlPlaylist.RemoveTask(task);

				TaskControlSchedules command = ControlScheduleHelper.GetInstance.GetKindOfCommand(task.description);

				switch (command)
				{
					case TaskControlSchedules.CtrlVolume:	//	볼륨인 경우 적용 범위(START-END)가 존재한다.
						_volumeControlPlaylist.RemoveTask(task);

						if ((currentVolumeControl != null) && (task.uuid.Equals(currentVolumeControl.uuid)))
						{
							currentVolumeControl = null;
						}
						break;
					case TaskControlSchedules.CtrlSerialVolume:	//	볼륨인 경우 적용 범위(START-END)가 존재한다.
						//	적용중인 Task가 있다면 지워준다 
						_serialvolumeControlPlaylist.RemoveTask(task);

						//	만약 현재 재생하고 있다면!!
						if (currentSerialVolumeControl != null && currentSerialVolumeControl.uuid == task.uuid)
						{
							// 							// 다음 Task를 재생하자
							currentSerialVolumeControl = null;
						}
						break;
					default:
						break;
				}

			}
			else
			{
				logger.Error("[CmdControlSchedule] Unknown TaskState : " + task.state.ToString());
			}
		}
		#endregion
		
		#region 프로그램 스케줄 처리

		/// <summary>
		/// 프로그램 스크린의 재생/종료을 담당한다.
		/// </summary>
		private void RunProgramTask()
		{

			DigitalSignage.PlayAgent.Task next = _managedtimeScreenPlaylist.First;

			do
			{

				if (next == null) continue;

				//	1. 일일 내 시간 검사
				//	2. 요일 검사
				if (CheckContainTimeofDay(next.starttimeofday, next.endtimeofday) && CheckDayOfWeek(next.daysofweek))
				{
					//	현재 시간과 비교하여 하루 시간 대와 요일에 맞는다면..

					if (0 < _timeScreenPlaylist.AddTask(next))
					{
						logger.Info(String.Format("[CmdProgram][{0}] 스크린 시작.", next.description));
					}
				}
				else
				{
					if (_timeScreenPlaylist.RemoveTask(next))
					{
						logger.Info(String.Format("[CmdProgram][{0}] 스크린 종료.", next.description));
					}

				}

			}
			while ((next = _managedtimeScreenPlaylist.NextToEnd) != null);

			if(_timeScreenPlaylist.Count > 0 && 
				currentScreen != null && (currentScreen.type == TaskCommands.CmdDefaultProgram || currentScreen.type == TaskCommands.CmdTouchDefaultProgram))
			{
				PlayNextTimeScreenTask();
			}
			/*
			if (currentScreen == null)
			{
				PlayNextDefaultScreenTask();
			}*/
		}

		/// <summary>
		/// 프로그램 스케줄 처리
		/// </summary>
		/// <param name="task"></param>
		/// <param name="tasks"></param>
		void ProgramTask(DigitalSignage.PlayAgent.Task task, DigitalSignage.PlayAgent.Task[] tasks)
		{
			if (task.state == TaskState.StateWait)
			{
				if (task.TaskEnd > TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()))
				{
					//	현재 시간보다 스케줄 종료 시간이 더 크다면

					_managedtimeScreenPlaylist.Push(task);

					logger.Info("Task " + task.ToString());

					logger.Info(String.Format("[CmdProgram][{0}] StateWait.", task.description));

					ChangeTaskState(TaskState.StateRunning, task.uuid);

				}
				else
				{
					//	이미 시간이 지났는데 wait로 들어온 경우라면
					//	사용자가 Edit Task를 통해 변경한 경우이다.
					//	재생 지연 목록에서 요청된 Task를 제거해주고,
					//	현재 플레이중이라면 종료를 해준 후 다음 스크린을 재생한다.
					logger.Info("[" + task.description + " (" + task.uuid + ")] End time is wrong. It will be canceled.");
					ChangeTaskState(TaskState.StateProcessed, task.uuid);

					_managedtimeScreenPlaylist.RemoveTask(task);
					_timeScreenPlaylist.RemoveTask(task);

					if (currentScreen != null && currentScreen.uuid == task.uuid)
					{
						PlayNextTimeScreenTask();
					}

					/*
					lock (lockScreen)
					{
						if (currentScreen != null && currentScreen.uuid == task.uuid)
						{
							currentScreen = null;
						}
					}
					*/
// 									iVisionLog.GeneralLogError(task.description, task.type, ErrorCodes.IS_ERROR_INVALID_TIME_SCOPE, "This schedule is alreay ended.");

				}
			}
			else if (task.state == TaskState.StateEdit)
			{
				logger.Info("Task " + task.ToString());

				logger.Info(String.Format("[CmdProgram][{0}] StateEdit.", task.description));

				//	적용중인 Task가 있다면 지워준다 
				_managedtimeScreenPlaylist.RemoveTask(task);
				_timeScreenPlaylist.RemoveTask(task);

				if (currentScreen != null && currentScreen.uuid == task.uuid)
				{
					PlayNextTimeScreenTask();
				}

				/*
				lock(lockScreen)
				{
					if (currentScreen != null && currentScreen.uuid == task.uuid)
					{
						currentScreen = null;
					}
				}
				*/

				if (!IsDownloadComplete(task.duuid.ToString()))
				{
					logger.Info(String.Format("[CmdProgram][{0}] state has been modified as 'Download'.", task.description));

					ChangeTaskState(TaskState.StateDownload, task.uuid);
				}
				else
					ChangeTaskState(TaskState.StateWait, task.uuid);
			}
			else if (task.state == TaskState.StateRunning)
			{
				if (task.TaskEnd <= TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()))
				{
					//	스케줄 종료시간 보다 현재 시간이 더 크다면
					logger.Info("[" + task.description + " (" + task.uuid + ")] will be ended.");

					ChangeTaskState(TaskState.StateProcessed, task.uuid);

					_managedtimeScreenPlaylist.RemoveTask(task);
					_timeScreenPlaylist.RemoveTask(task);

					if (currentScreen != null && currentScreen.uuid == task.uuid)
					{
						PlayNextTimeScreenTask();
					}

					/*
					lock (lockScreen)
					{
						if (currentScreen != null && currentScreen.uuid == task.uuid)
						{
							currentScreen = null;
						}
					}
					*/
				}
				else
				{
					//	현재 시간보다 스케줄 종료 시간이 더 크다면
					ChangeTaskState(TaskState.StateRunning, task.uuid);
				}


			}
			else if (task.state == TaskState.StateCanceled)
			{
				logger.Info("Task " + task.ToString());

				logger.Info(String.Format("[CmdProgram][{0}] StateCanceled.", task.description));

				ChangeTaskState(TaskState.StateProcessed, task.uuid);
				_managedtimeScreenPlaylist.RemoveTask(task);
				_timeScreenPlaylist.RemoveTask(task);

				if (currentScreen != null && currentScreen.uuid == task.uuid)
				{
					PlayNextTimeScreenTask();
				}

				/*
				lock (lockScreen)
				{
					if (currentScreen != null && currentScreen.uuid == task.uuid)
					{
						currentScreen = null;
					}
				}
				*/
			}
		}
		#endregion

		#region 자막 스케줄 처리

		/// <summary>
		/// 자막 스케줄의 재생/종료을 담당한다.
		/// </summary>
		private void RunSubtitleTask()
		{
			DigitalSignage.PlayAgent.Task next = _managedsubtitlePlaylist.First;

			do
			{

				if (next == null) continue;

				//	1. 일일 내 시간 검사
				//	2. 요일 검사
				if (CheckContainTimeofDay(next.starttimeofday, next.endtimeofday) && CheckDayOfWeek(next.daysofweek))
				{
					//	현재 시간과 비교하여 하루 시간 대와 요일에 맞는다면..

					if (0 < _subtitlePlaylist.AddTask(next))
					{
						logger.Info(String.Format("[CmdSubtitles][{0}] 자막 시작.", next.description));
					}
				}
				else
				{
					if (_subtitlePlaylist.RemoveTask(next))
					{
						logger.Info(String.Format("[CmdSubtitles][{0}] 자막 종료.", next.description));
					}

				}

			}
			while ((next = _managedsubtitlePlaylist.NextToEnd) != null);

			if (currentSub == null)
			{
				PlayNextSubtitle();
			}
		}

		private void SubtitleTask(DigitalSignage.PlayAgent.Task task)
		{
			if (task.state == TaskState.StateWait)
			{
				if (task.TaskEnd > TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()))
				{
					//	현재 시간보다 스케줄 종료 시간이 더 크다면

					_managedsubtitlePlaylist.AddTask(task);
					
					logger.Info("Task " + task.ToString());

					logger.Info(String.Format("[CmdSubtitles][{0}] StateWait.", task.description));

					ChangeTaskState(TaskState.StateRunning, task.uuid);
				}
				else
				{
					//	이미 시간이 지났는데 wait로 들어온 경우라면
					//	사용자가 Edit Task를 통해 변경한 경우이다.
					//	재생 지연 목록에서 요청된 Task를 제거해주고,
					//	현재 플레이중이라면 종료를 해준 후 다음 스크린을 재생한다.
					logger.Info("[" + task.description + " (" + task.uuid + ")] End time is wrong. It will be canceled.");
					ChangeTaskState(TaskState.StateProcessed, task.uuid);

					_managedsubtitlePlaylist.RemoveTask(task);
					_subtitlePlaylist.RemoveTask(task);

					if (currentSub != null && currentSub.uuid == task.uuid)
					{
						PlayNextSubtitle();
					}
				}
			}
			else if (task.state == TaskState.StateEdit)
			{
				logger.Info("Task " + task.ToString());

				logger.Info(String.Format("[CmdSubtitles][{0}] StateEdit.", task.description));

				//	적용중인 Task가 있다면 지워준다 
				_managedsubtitlePlaylist.RemoveTask(task);
				_subtitlePlaylist.RemoveTask(task);

				if (currentSub != null && currentSub.uuid == task.uuid)
				{
					PlayNextSubtitle();
				}

				if (!IsDownloadComplete(task.duuid.ToString()))
				{
					logger.Info(String.Format("[CmdSubtitles][{0}] state has been modified as 'Download'.", task.description));

					ChangeTaskState(TaskState.StateDownload, task.uuid);
				}
				else
					ChangeTaskState(TaskState.StateWait, task.uuid);
			}
			if (task.state == TaskState.StateRunning)
			{
				if (task.TaskEnd <= TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()))
				{
					//	스케줄 종료시간 보다 현재 시간이 더 크다면
					logger.Info("[" + task.description + " (" + task.uuid + ")] will be ended.");

					ChangeTaskState(TaskState.StateProcessed, task.uuid);

					_managedsubtitlePlaylist.RemoveTask(task);
					_subtitlePlaylist.RemoveTask(task);

					if (currentSub != null && currentSub.uuid == task.uuid)
					{
						PlayNextSubtitle();
					}
				}
				else
				{
					//	현재 시간보다 스케줄 종료 시간이 더 크다면
					ChangeTaskState(TaskState.StateRunning, task.uuid);

				}

			}
			if (task.state == TaskState.StateCanceled)
			{
				logger.Info("Task " + task.ToString());

				logger.Info(String.Format("[CmdSubtitles][{0}] StateCanceled.", task.description));

				ChangeTaskState(TaskState.StateProcessed, task.uuid);

				_managedsubtitlePlaylist.RemoveTask(task);
				_subtitlePlaylist.RemoveTask(task);

				if (currentSub != null && currentSub.uuid == task.uuid)
				{
					PlayNextSubtitle();
				}

			}
		}
#endregion

		/// <summary>
		/// Task 관련 업무를 수행할 스레드 함수. 로컬 DB를 쿼리해 현재 이용가능한 데이터를 처리한다. 
		/// </summary>
        private void runTasks()
        {
            try
            {
                // reading task list
				DigitalSignage.PlayAgent.Task[] tasks = config.localTasksList.GetActiveTask();
                if (tasks == null)
                {
                    logger.Info("No active tasks");
                    return; // nothing new
                }

                for (int i = 0; i < tasks.Length; i++)
                {
                    //running task
                    switch ((TaskCommands)tasks[i].type)
                    {
						case TaskCommands.CmdGetRefData:
							logger.Info("[CmdGetRefData] is started.");
							ChangeTaskState(TaskState.StateDownload, tasks[i].uuid);
							break;
                        case TaskCommands.CmdInitPlayer:
							logger.Info("[CmdInitPlayer] is started.");
							break;

						case TaskCommands.CmdNextDefaultProgram:
							logger.Info("[CmdNextDefaultProgram] is started.");
							{
								if (currentScreen != null && (currentScreen.type == TaskCommands.CmdDefaultProgram || currentScreen.type == TaskCommands.CmdTouchDefaultProgram))
								{
									PlayNextDefaultScreenTask();
								}
								else
								{
									logger.Error("[CmdNextDefaultProgram] Current screen is not 'Default' task.");
								}
							}
							break;
                        case TaskCommands.CmdTouchDefaultProgram:
						case TaskCommands.CmdDefaultProgram:
							
							RollingTask(tasks[i]);
							break;

						case TaskCommands.CmdControlSchedule:
							
							ControlTask(tasks[i]);
							break;

						case TaskCommands.CmdProgram:

							ProgramTask(tasks[i], tasks);
                            break;

						case TaskCommands.CmdSubtitles:

							SubtitleTask(tasks[i]);
							break;

                        case TaskCommands.CmdEcho:
                        case TaskCommands.CmdUndefined:
#if DEBUG
                        logger.Info( "Undefined task runned :" + tasks[i].uuid.ToString());
#endif
                            break;
						case TaskCommands.CmdSynchronizeNow:
							logger.Info("[CmdSynchronizeNow] is started.");
							try
							{
								String[] arrDesc = tasks[i].description.Split('|');
								config.LastSyncFromFile = Convert.ToInt64(arrDesc[1]);

								config.AddSynchronizeTasks(TimeConverter.ConvertToUTP(DateTime.Now.AddDays(7).ToUniversalTime()), 
									TimeConverter.ConvertToUTP(DateTime.Now.AddMonths(-1).ToUniversalTime()));
							}
							catch (System.Exception ex)
							{
								logger.Error(ex.ToString());
							}
							
							//config.LastSyncFromFile = 

                            break;
                        case TaskCommands.CmdClearStorage:
                            {
								logger.Info("[CmdClearStorage] is started.");
								
								Thread ts = new Thread(this.clearStorage);
                                ts.Priority = ThreadPriority.BelowNormal;
                                ts.Start();

                                AddCleanTaskAfter();
                            }
                            break;
						case TaskCommands.CmdReturnStatus:
							{
							}
							break;

                        case TaskCommands.CmdWriteRS232:
                            {
								logger.Info("[CmdWriteRS232] is started.");

								Thread ts = new Thread(delegate()
                                  { 
                                     SendRS232.SendTask( tasks[i].duuid );
                                  });
                               ts.Priority = ThreadPriority.Normal;
                               ts.Start();
			                   ChangeTaskState(TaskState.StateProcessed, tasks[i].uuid);
                            }
                            break;
                        default:
                            logger.Info("Unknown task type");
                            break;
                    }
                }

				//////////////////////////////////////////////////////////////////////////
				// 프로그램 Task 감시
				RunProgramTask();
				RunSubtitleTask();
				RunControlTask();

            }
            catch (Exception err)
            {
                logger.Error(err + " "+err.StackTrace);
            }
        }

        public void AddCleanTaskAfter()
        {
            try
            {
                // clean storage
                DigitalSignage.PlayAgent.Task t = new DigitalSignage.PlayAgent.Task();
                t.type = TaskCommands.CmdClearStorage;
                t.uuid = DigitalSignage.PlayAgent.Task.TaskClean;
                t.pid = config.PlayerId;
                t.gid = config.GroupId;
                t.description = "CmdClearStorage";
                t.TaskStart = TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()) + 3600;
                t.TaskEnd = t.TaskStart + 600 * 2;
                if (-1 == config.localTasksList.AddTask(t))
                {
                    ////	추가가 되지않음.
                    config.localTasksList.CleanTask(DigitalSignage.PlayAgent.Task.TaskClean);
                    //	다시 추가 시도
                    config.localTasksList.AddTask(t);
                }

            }
            catch (Exception e)
            {
                logger.Error(e.ToString() + "");
            }
        }

        void ControlScheduleHelper_OnChangeStatus(object Sender, ChangeStatusEventArgs e)
        {
            try
            {
                switch(e.control)
                {
                    case TaskControlSchedules.CtrlPlayerOn:
                        
                        parent.IsPlayerVisible = true;
                        logger.Info("Control Player On");
                        break;
                    case TaskControlSchedules.CtrlPlayerOff:
                        parent.IsPlayerVisible = false;
                        logger.Info("Control Player Off");
                        break;
                }

            }
            catch (System.Exception err)
            {
                logger.Error("Control Player Error!" + err.Message);
            }
        }

		/// <summary>
		/// 기본스케줄 리스트를 생성한다.
		/// </summary>
		private void InitDefaultScheduleList()
		{
			//	스크린 추가

			//	스크린 재생
			if(currentScreen == null)
			{
				PlayScreenTask(_defaultScreenPlaylist.Next);
			}
		}

		/// <summary>
		/// 지정된 시간에 DefaultScreen을 교체해주기 위한 Task
		/// </summary>
		/// <param name="duration">지속 시간(초)</param>
		private void RegisterNextScreen(long duration)
		{
			//	default Screen Player thread
			try
			{
				Thread temp = null;
				if (thDefaultScreenPlayer != null && thDefaultScreenPlayer.IsAlive)
					temp = thDefaultScreenPlayer;

				thDefaultScreenPlayer = new Thread(this.defaultScreenTimer);
				thDefaultScreenPlayer.Priority = ThreadPriority.Highest;
				thDefaultScreenPlayer.Start(duration);
				
				if (temp != null) temp.Abort();

			}
			catch
			{
			}
				

/* *
			DigitalSignage.PlayAgent.Task t = new DigitalSignage.PlayAgent.Task();
			t.pid = config.PlayerId;
			t.gid = config.GroupId;
			t.type = TaskCommands.CmdNextDefaultProgram;
			t.uuid = DigitalSignage.PlayAgent.Task.TaskDefaultScreen;
			t.TaskStart = TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()) + duration - 1;
			t.description = "CmdNextDefaultProgram";
			t.duration = duration;

			InsertOrReplaceTask(t);
**/
		}

        Timer timer = null;

		/// <summary>
		/// 다음 기본 스케줄을 재생한다
		/// </summary>
		public void PlayNextDefaultScreenTask()
        {
			#region Touch  - IsPauseDefaultProgram 플래그 에 따라 DefaultProgram 대기 여부 결정
			if (timer == null)
			{
				timer = new Timer(delegate(object o)
				{

					if (IsPauseDefaultProgram == false/* || this.currentScreen == null*/)
						waitDefaultProgramHandler.Set();
				}, null, 0, 10);
			}
			waitDefaultProgramHandler.WaitOne();

			#endregion

			DigitalSignage.PlayAgent.Task next = null;

			int nRetCount = 0;
			do
			{
				next = _defaultScreenPlaylist.Next;
			}
			while (false == PlayScreenTask(next) && nRetCount++ <= _defaultScreenPlaylist.Count);

			if (next != null)
				RegisterNextScreen(next.duration);
		}

		/// <summary>
		/// 다음 시간 스케줄을 재생한다.
		/// </summary>
		public void PlayNextTimeScreenTask()
		{
			#region Touch  - IsPauseDefaultProgram 플래그 에 따라 DefaultProgram 대기 여부 결정
			if (timer == null)
			{
				timer = new Timer(delegate(object o)
				{

					if (IsPauseDefaultProgram == false/* || this.currentScreen == null*/)
						waitDefaultProgramHandler.Set();
				}, null, 0, 10);
			}
			waitDefaultProgramHandler.WaitOne();
			#endregion

			DigitalSignage.PlayAgent.Task next = null;

			int nRetCount = 0;
			do
			{
				next = _timeScreenPlaylist.Next;
			}
			while (false == PlayScreenTask(next) && nRetCount++ <= _timeScreenPlaylist.Count);

			if (next != null)
				RegisterNextScreen(next.duration);
			else
				PlayNextDefaultScreenTask();

		}

		/// <summary>
		/// 저장된 다음 볼륨 조절 스케줄을 적용한다.
		/// </summary>
		private void PlayNextStoredVolumeControlTask()
		{
			DigitalSignage.PlayAgent.Task t = null;

			while (null != (t = _volumeControlPlaylist.Pop()))
			{
				if (t.TaskEnd <= TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()))
					continue;

				if (!PlayControlSchedule(t))
					continue;

				break;
			}

			if (t == null)
			{
				//	볼륨을 되돌린다.
				RestoreVolume();
				RestoreSerialVolume();

				currentVolumeControl = null;
				currentSerialVolumeControl = null;
			}
		}

		private bool PlayControlSchedule(DigitalSignage.PlayAgent.Task task)
		{
			if (task != null)
			{
				logger.Info("[" + task.description + " (" + task.uuid + ")] will be started.");
				if (!CheckDayOfWeek(task.daysofweek))
				{
					logger.Info("[" + task.description + " (" + task.uuid + ")(Day Of Week : " + task.daysofweek + ")] invaild Day of week.");
					return false;
				}
				try
				{
					bool bSuccess = false;

					TaskControlSchedules command = ControlScheduleHelper.GetInstance.GetKindOfCommand(task.description);
					switch (command)
					{
						case TaskControlSchedules.CtrlVolume:
							{
								StoreCurrentVolume();
								if (bSuccess = ControlScheduleHelper.GetInstance.PlayControlSchedule(task.description, task.duuid))
								{
									currentVolumeControl = task;
								}

								break;
							}
						case TaskControlSchedules.CtrlSyncSchedule:
							{
// 								config.localTasksList.CleanAllTasks();
								config.LastSyncFromFile = 0;

								logger.Info(command.ToString() + " : " + " LastSyncFromFile = 0");
								break;
							}
						case TaskControlSchedules.CtrlSerialAuto:
							{
								logger.Info(command.ToString() + " : " +
									(bSuccess = _serialPortObject.Auto()).ToString());

								break;
							}
						case TaskControlSchedules.CtrlSerialPowerOff:
							{
								logger.Info(command.ToString() + " : " +
									(bSuccess = _serialPortObject.PowerOff()).ToString());
		
								break;
							}
						case TaskControlSchedules.CtrlSerialPowerOn:
							{
								logger.Info(command.ToString() + " : " +
									(bSuccess = _serialPortObject.PowerOn()).ToString());

								break;
							}
						case TaskControlSchedules.CtrlSerialSourceDVI:
							{
								logger.Info(command.ToString() + " : " +
									(bSuccess = _serialPortObject.Source(SourceMode.sourceDVI)).ToString());
								break;
							}
						case TaskControlSchedules.CtrlSerialSourceRGB:
							{
								logger.Info(command.ToString() + " : " +
									(bSuccess = _serialPortObject.Source(SourceMode.sourceRGB)).ToString());
								break;
							}
						case TaskControlSchedules.CtrlSerialSourceTV:
							{
								logger.Info(command.ToString() + " : " +
									(bSuccess = _serialPortObject.Source(SourceMode.sourceGeneral)).ToString());
								break;
							}
						case TaskControlSchedules.CtrlSerialVolume:
							{
								String[] arrVolumes = task.description.Split('[')[1].TrimEnd(']').Split('|');

								logger.Info("SET DEVICE VOLUME NOW : " + arrVolumes[0]);

								logger.Info(command.ToString() + " : " +
									(bSuccess = _serialPortObject.Volume(Convert.ToInt32(arrVolumes[0]))).ToString());
								break;
							}
						case TaskControlSchedules.CtrlSerialVolumeNow:
							{
								String[] arrVolumes = task.description.Split('[')[1].TrimEnd(']').Split('|');

								logger.Info("SET DEVICE VOLUME NOW : " + arrVolumes[0]);

								logger.Info(command.ToString() + " : " +
									(bSuccess = _serialPortObject.Volume(Convert.ToInt32(arrVolumes[0]))).ToString());
								break;
							}
						default:
							bSuccess = ControlScheduleHelper.GetInstance.PlayControlSchedule(task.description, task.duuid);
							break;
					}

					iVisionLogHelper.GetInstance.GeneralLog(command.ToString(), TaskCommands.CmdControlSchedule, bSuccess.ToString());

				}
				catch (NullReferenceException /*nullerr*/)
				{
					logger.Info("There is no Serial Port Device.");
					return true;
				}
				catch (System.NotImplementedException)
				{
					logger.Info("There is no Volume Function in the Serial Port Device.");
					return true;
				}
				catch (System.Exception e)
				{
					logger.Error(e.ToString());
					return false;
				}

				return true;
			}
			else
			{
				//	unload screen
				currentVolumeControl = null;
				return false;
			}
		}

		/// <summary>
		/// 저장해둔 볼륨 정보가 없다면 현재 볼륨을 저장해 둔다.
		/// </summary>
		private void StoreCurrentVolume()
		{
			int nWVol = config.StoredWaveVolume;
			int nMVol = config.StoredMasterVolume;
			if (nWVol == -1)
			{
				config.StoredWaveVolume = PCControl.GetWaveVolume();
			}
			if (nMVol == -1)
			{
				config.StoredMasterVolume = PCControl.GetMasterVolume();
			}
		}

		/// <summary>
		/// 저장해둔 볼륨이 있다면 볼륨을 되돌리고 저장정보를 초기화한다..
		/// </summary>
		private void RestoreVolume()
		{
			try
			{
				int nMVol = config.StoredMasterVolume;
				int nWVol = config.StoredWaveVolume;
				if (nWVol != -1)
				{
					if (PCControl.SetWaveVolume(nWVol))
					{
						logger.Info(String.Format("The wave volume has been restored to {0}.", nWVol));
						config.StoredWaveVolume = -1;
					}
				}
				if (nMVol != -1)
				{
					if (PCControl.SetMasterVolume(nMVol))
					{
						logger.Info(String.Format("The master volume has been restored to {0}.", nMVol));
						config.StoredMasterVolume = -1;
					}
				}
			}
            catch (System.Exception e)
            {
                logger.Error(e.Message);	
            }

		}

		/// <summary>
		/// 저장해둔 볼륨이 있다면 볼륨을 되돌리고 저장정보를 초기화한다..
		/// </summary>
		private void RestoreSerialVolume()
		{
			try
			{
				int nSVol = config.StoredSerialVolume;

				if (nSVol != -1)
				{
					if (_serialPortObject.Volume(nSVol))
					{
						logger.Info(String.Format("The serial volume has been restored to {0}.", nSVol));
						config.StoredSerialVolume = -1;
					}
				}
			}
			catch (System.NullReferenceException)
			{
			}
			catch (System.NotImplementedException)
			{
			}
			catch (System.Exception e)
			{
				logger.Error(e.Message);
			}

		}
		/// <summary>
		/// 저장된 다음순서 스크린을 재생한다
		/// </summary>
		private void PlayNextStoredScreenTask()
		{
			DigitalSignage.PlayAgent.Task t = null;

			while (null != (t = _managedtimeScreenPlaylist.Pop()))
			{
				if (t.TaskEnd <= TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()))
					continue;

				if (!PlayScreenTask(t))
					continue;

				break;
			}

			if (t == null)
			{
				PlayNextDefaultScreenTask();
			}
		}

		private void PlayNextStoredSubtitleTask()
		{
			DigitalSignage.PlayAgent.Task t = null;

			while (null != (t = _subtitlePlaylist.Pop()))
			{
				if (t.TaskEnd <= TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()))
					continue;
				break;
			}
			PlaySubtitleTask(t);

		}
		/// <summary>
		/// 스크린를 즉시 재생한다.
		/// </summary>
		/// <param name="task">재생할 작업</param>
		private bool PlayScreenTask(DigitalSignage.PlayAgent.Task task)
		{
			if (task != null)
			{
				logger.Info("[" + task.description + " (" + task.uuid + ")] will be started.");
				if(!CheckDayOfWeek(task.daysofweek))
				{
					logger.Info("[" + task.description + " (" + task.uuid +")(Day Of Week : "+ task.daysofweek +")] invaild Day of week.");
					return false;
				}

				String sFilePath = config.AppData + "xaml\\" + task.duuid.ToString() + "\\program.xml";
				logger.Info("Loading [" + sFilePath + "]");

                DigitalSignage.PlayAgent.Task tempTask = currentScreen;
				currentScreen = task;
                
				if(parent.loadProject(sFilePath))
				{
					logger.Debug("Default Screen Load Finishied!");
//					currentScreen = task;
					return true;
				}

                currentScreen = tempTask;
				return false;
			}
			else
			{
				//	unload screen
				parent.loadProject("");
				currentScreen = null;
				return false;
			}
		}

		/// <summary>
		/// 다음 자막을 재생한다.
		/// </summary>
		/// <returns></returns>
		public bool PlayNextSubtitle()
		{
			if (_subtitlePlaylist != null && _subtitlePlaylist.Count > 0)
			{
				PlayAgent.Task t = _subtitlePlaylist.Next;
				bool bRet = PlaySubtitleTask(t);
				if (bRet)
				{
					currentSub = t;
				}
				else
					currentSub = null;

				return bRet;
			}
			else
			{
				PlaySubtitleTask(null);
				currentSub = null;

				return true;
			}
		}

		private bool PlaySubtitleTask(DigitalSignage.PlayAgent.Task task)
		{
			if(task != null)
			{
				logger.Info("[" + task.description + " (" + task.uuid + ")] will be started.");
				if (!CheckDayOfWeek(task.daysofweek))
				{
					logger.Info("[" + task.description + " (" + task.uuid + ")(Day Of Week : " + task.daysofweek + ")] invaild Day of week.");
					return false;
				}

				String sFilePath = config.AppData + "xaml\\" + task.duuid.ToString() + "\\program.xml";
				logger.Info("Loading [" + sFilePath + "]");

				previewSub = currentSub;
				currentSub = task;

				if (parent.setSubtitle(sFilePath))
				{
					return true;
				}
				else
				{
					currentSub = previewSub;
					return false;
				}
			}
			else
			{
				parent.setSubtitle(""); //unload project
				previewSub = null;
				currentSub = null;
				return false;
			}
		}
		/// <summary>
		/// Task 리스트 중 endtime이 시작시간과 연결되어있는지 검사한다.
		/// </summary>
		/// <param name="tasks">검사할 Task 리스트</param>
		/// <param name="endtime">기준이될 끝 시간</param>
		/// <returns>만약 true라면 연결된 Task가 있다는 것이고, false면 없다는 것.</returns>
		private bool IsConnectedScreenTask(DigitalSignage.PlayAgent.Task[] tasks, long endtime)
		{
			foreach (DigitalSignage.PlayAgent.Task task in tasks)
			{
				if (task.type == TaskCommands.CmdScreen && task.TaskStart == endtime)
					return true;
			}
			return false;
		}

		/// <summary>
		/// Task가 중복되는 uuid를 가질 수 없기 때문에 중복된다면 지우고 추가해준다.
		/// </summary>
		/// <param name="task">추가할 Task</param>
		/// <returns>Reserved</returns>
		private int InsertOrReplaceTask(DigitalSignage.PlayAgent.Task task)
		{
			int nRet = -1;
			try
			{
				if (-1 == (nRet = config.localTasksList.AddTask(task)))
				{
					////	추가가 되지않음.
					config.localTasksList.CleanTask(task.uuid);
					//	다시 추가 시도
					nRet = config.localTasksList.AddTask(task);
				}
			}
			catch(Exception e)
			{
				logger.Error(e.Message);
			}
			return nRet;
		}

		/// <summary>
		/// local 저장소를 정리한다.
		/// </summary>
        private void clearStorage()
        {
            try
            {
                Cleaner cleaner = new Cleaner();
                cleaner.deleteLostProjects();
                cleaner.deleteOldProjects();
            }
            catch (Exception e)
            {
                logger.Error(e.ToString());
            }
        }

		/// <summary>
		/// 요일 체크
		/// </summary>
		/// <param name="dayofweek"></param>
		/// <returns></returns>
		private bool CheckDayOfWeek(int dayofweek)
		{
			DayOfWeek dow = DateTime.Today.DayOfWeek;
			if (dow == DayOfWeek.Monday)
				return 0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_MON);
			else if (dow == DayOfWeek.Tuesday)
				return 0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_TUE);
			else if (dow == DayOfWeek.Wednesday)
				return 0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_WED);
			else if (dow == DayOfWeek.Thursday)
				return 0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_THU);
			else if (dow == DayOfWeek.Friday)
				return 0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_FRI);
			else if (dow == DayOfWeek.Saturday)
				return 0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_SAT);
			else if (dow == DayOfWeek.Sunday)
				return 0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_SUN);
			else
			{
				logger.Error("Exception : invalid DayOfWeek. : " + dayofweek.ToString());
			}
			return false;
		}

		/// <summary>
		/// 현재 시간이 TimeOfDay에 포함되는지
		/// </summary>
		/// <param name="nStartTime"></param>
		/// <param name="nEndTime"></param>
		/// <returns></returns>
		private bool CheckContainTimeofDay(int nStartTime, int nEndTime)
		{
			//	둘다 같은 값이라면 Whole Day로 간주
			if (nStartTime == nEndTime) return true;

			DateTime dtLocal = DateTime.Now;
			int nCurr = dtLocal.Second + (dtLocal.Minute * 60) + (dtLocal.Hour * 3600);

			if(nStartTime < nEndTime)
			{
				//	시간대가 하루에 국한되는 경우

				return nCurr >= nStartTime && nCurr < nEndTime;
			}
			else
			{
				//	시간대가 오늘부터 다음 날로 넘어 간 경우
				return nCurr < nStartTime || nCurr >= nEndTime;				
			}
		}


		/// <summary>
		/// 다운로드 완료되었는지 체크
		/// </summary>
		/// <param name="duuid"></param>
		/// <returns></returns>
		bool IsDownloadComplete(string duuid)
		{
			try
			{
				string sDest = config.AppData + "xaml\\" + duuid + "\\downloaded";

				System.IO.FileInfo fi = new System.IO.FileInfo(sDest);

				if (fi == null) return false;

				return fi.Exists;
			}
			catch (Exception e)
			{
				logger.Error(e.ToString());
				return false;
			}
		}
		#region Logging

		private void SubtitleStarted(string sName)
		{
			try
			{
				if (client_logsTA == null)
					client_logsTA = config.logdbLayer.CreateClientLogDA();

				client_logsTA.InsertLogs(sName, currentSub.description, "Started", (int)TaskCommands.CmdSubtitles, (int)LogCategory.CatProcceed, 0, "N", TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()));

			}
			catch (Exception e)
			{
				logger.Error(e.Message);
			}
		}

		private void SubtitleEnded(string sName)
		{
			try
			{
				if (client_logsTA == null)
					client_logsTA = config.logdbLayer.CreateClientLogDA();

				client_logsTA.InsertLogs(sName, currentSub.description, "Ended", (int)TaskCommands.CmdSubtitles, (int)LogCategory.CatProcceed, 0, "N", TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()));
			}
			catch (Exception e)
			{
				logger.Error(e.Message);
			}
		}

		private void SubtitleErrorOccurred(string sName, int code, string description)
		{
			try
			{
				if (client_logsTA == null)
					client_logsTA = config.logdbLayer.CreateClientLogDA();

				client_logsTA.InsertLogs(sName, currentSub.description, description, (int)TaskCommands.CmdSubtitles, (int)LogCategory.CatError, code, "N", TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()));
			}
			catch (Exception e)
			{
				logger.Error(e.Message);
			}
		}

		private void ScreenStarted(string sName)
		{
			try
			{
				if (client_logsTA == null)
					client_logsTA = config.logdbLayer.CreateClientLogDA();

				client_logsTA.InsertLogs(sName, currentScreen.description, "Started", (int)currentScreen.type, (int)LogCategory.CatProcceed, 0, currentScreen.isAd ? "Y" : "N", TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()));

			}
			catch (Exception e)
			{
				logger.Error(e.Message);
			}
		}

		private void ScreenEnded(string sName)
		{
			try
			{
				if (client_logsTA == null)
					client_logsTA = config.logdbLayer.CreateClientLogDA();

				client_logsTA.InsertLogs(sName, currentScreen.description, "Ended", (int)currentScreen.type, (int)LogCategory.CatProcceed, 0, currentScreen.isAd ? "Y" : "N", TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()));
			}
			catch (Exception e)
			{
				logger.Error(e.Message);
			}
		}

		private void ScreenErrorOccurred(string sName, int code, string description)
		{
			try
			{
				if (client_logsTA == null)
					client_logsTA = config.logdbLayer.CreateClientLogDA();

				client_logsTA.InsertLogs(sName, currentScreen.description, description, (int)currentScreen.type, (int)LogCategory.CatError, code, currentScreen.isAd ? "Y" : "N", TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()));
			}
			catch (Exception e)
			{
				logger.Error(e.Message);
			}
		}

		private void WriteInfo(string title, TaskCommands type, String message)
		{
			try
			{
				if (client_logsTA == null)
					client_logsTA = config.logdbLayer.CreateClientLogDA();

				client_logsTA.InsertLogs("", title, message, (int)type, (int)LogCategory.CatProcceed, 0, "N", TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()));
			}
			catch (Exception e)
			{
				logger.Error(e.Message);
			}
		}

		private void WriteError(string title, TaskCommands type, ErrorCodes code, String message)
		{
			try
			{
				if (client_logsTA == null)
					client_logsTA = config.logdbLayer.CreateClientLogDA();

				client_logsTA.InsertLogs("", title, message, (int)type, (int)LogCategory.CatError, (int)code, "N", TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()));
			}
			catch (Exception e)
			{
				logger.Error(e.Message);
			}
		}
		#endregion
	}
}
