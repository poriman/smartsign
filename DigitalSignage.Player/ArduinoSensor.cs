﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Ports;
using System.Windows.Forms;
using NLog;

namespace DigitalSignage.Player
{
    /// <summary>
    /// 감지 센서 
    /// </summary>
    public class ArduinoSensor
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        SerialPort _port = null;

        private int sensor_distance = 80;

        /// <summary>
        /// 생성자
        /// </summary>
        public ArduinoSensor()
        {
        }

        public bool Accessed
        {
            get;
            set;
        }

        public event ArduinoEventHandler AccessOccured = null;

        /// <summary>
        /// 초기화
        /// </summary>
        /// <param name="ComPort"></param>
        /// <param name="BoudRate"></param>
        /// <param name="SensorValue"></param>
        /// <returns></returns>
        public bool Initialize(String ComPort, int BoudRate, int SensorValue)
        {
            Uninitialize();

            try
            {
                sensor_distance = SensorValue;

                _port = new SerialPort(ComPort, BoudRate);
                _port.Open();
                _port.DataReceived += new SerialDataReceivedEventHandler(_port_DataReceived);
                return _port.IsOpen;
            }
            catch { return false; }
        }

        private string _lastData = "";

        void _port_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            string data = _port.ReadExisting();//센서로부터 받은 데이타
            _lastData += data;
            string[] allData = _lastData.Split('\n');
            _lastData = allData[allData.Length - 1];
            foreach (var word in allData.Take(allData.Length - 1))
            {
                string[] word2 = word.Split('\t', '\r').Where(o => string.IsNullOrEmpty(o) == false).ToArray();

                foreach (var strValue in word2.Take(word2.Length))
                {
                    if (strValue.ToLower().Contains("sensor") == true)
                    {
                        try
                        {
                            string temp = strValue.Substring(strValue.IndexOf('=') + 1);
                            temp = temp.Trim();
                            int dist = int.Parse(temp);

                            //logger.Debug("Arduino Value({0}), Distance({1})", dist, sensor_distance);
                            this.Accessed = dist > sensor_distance;

                            if (AccessOccured != null)
                            {
                                AccessOccured(this, new ArduinoEventArgs(this.Accessed));
                            }
                        }
                        catch
                        {
                            continue;
                        }
                    }
                    else if (strValue.Contains("output") == true)
                    {
                        continue;
                    }
                }
            }
        }

        

        /// <summary>
        /// 감지 센서 동작 시작 명령 
        /// </summary>
        /// <returns></returns>
        public bool Start()
        {
            if (_port != null && _port.IsOpen)
            {
                _port.Write("A");

                return true;
            }

            return false;
        }

        /// <summary>
        /// 감지 센서 동작 종료 명령 
        /// </summary>
        /// <returns></returns>
        public bool Stop()
        {
            if (_port != null && _port.IsOpen)
            {
                _port.Write("B");

                return true;
            }

            return false;
        }

        public void Uninitialize()
        {
            if (_port != null)
            {
                _port.DataReceived -= _port_DataReceived;
                _port.Close();
                _port.Dispose();
                _port = null;
            }
        }
    }
	public delegate void ArduinoEventHandler(object sender, ArduinoEventArgs e);

	public class ArduinoEventArgs : EventArgs
	{
		public bool IsAccessed
		{
			get;
			set;
		}

		public ArduinoEventArgs(bool _isaccessed)
		{
			IsAccessed = _isaccessed;
		}
	}
}
