﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using WPFDesigner;

namespace DigitalSignage.Player
{
    public class PlayerDef
    {
        public Dictionary<UIElement, PlaylistPlayer> GeneralPlayList;

        public PlayerDef(Dictionary<UIElement, PlaylistPlayer> pl)
        {
            GeneralPlayList = new Dictionary<UIElement, PlaylistPlayer>(pl);
        }

        public void StartPlayer()
        {
            foreach (UIElement key in GeneralPlayList.Keys)
            {
                GeneralPlayList[key].Start(TimeSpan.Zero);
            }
        }

        public void StopPlayer()
        {
            foreach (UIElement key in GeneralPlayList.Keys)
            {
                GeneralPlayList[key].Stop();
            }
        }
    }
}
