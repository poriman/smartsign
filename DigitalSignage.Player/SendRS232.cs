﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.IO;
using System.IO.Ports;
using NLog;

namespace DigitalSignage.Player
{
	/// <summary>
	/// RS232C 호출 로직
	/// </summary>
	public class SendRS232
	{
		public static void SendTask( System.Guid task )
		{
			Config cfg = Config.GetConfig;
			// opening port
			string filename = cfg.AppData + "xaml\\" + task.ToString() + "\\command.bin";
			SerialPort port = new SerialPort( cfg.RS232Port, cfg.RS232Speed, Parity.None, 
			cfg.RS232Bits, StopBits.One);
			port.Open();
			// reading command
			FileInfo fi = new FileInfo(filename);
			byte[] buf = new byte[ fi.Length ];
			BinaryReader sr = new BinaryReader(new FileStream( filename,
			FileMode.Open, FileAccess.Read)); 
			sr.Read( buf, 0, (int)fi.Length );
			port.Write( buf, 0, (int)fi.Length );
			port.Close();
		}
	}
}
