﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigitalSignage.PlayAgent;
using System.Collections.ObjectModel;

namespace DigitalSignage.Player
{
	class TaskPlaylist
	{
		Collection<Task> tasks = new Collection<Task>();
		Task currtask = null;
		bool bStarted = false;

        Task backuptask = null;

		private readonly object objLock = new object();

        int nCurrIndex = 0;

		#region Properties
		public int Count
		{
			get{
				lock (objLock)
				{
					return tasks.Count;
				}
			}

		}

        /// <summary>
        /// 현재 스케줄을 백업한다. 
        /// </summary>
        public void BackupTask()
        {
            backuptask = currtask;
        }

        /// <summary>
        /// 백업된 스케줄로 현재스케줄을 복구한다.
        /// </summary>
        public void RestoreTask()
        {
            if (backuptask != null) currtask = backuptask;
        }

        /// <summary>
        /// 일반 시간 스케줄이 하나라도 있는지 검사
        /// </summary>
        /// <returns></returns>
        public Task GetSingleTaskForNonEvent()
        {
            lock (objLock)
            {
                try
                {
                    var task = tasks.First(t => ((t.show_flag & (int)DigitalSignage.Common.ShowFlag.Show_NonEvent) > 0));

                    if (task == null) return null;

                    return task as Task;
                }
                catch { return null; }
            }
        }

		/// <summary>
		/// 중복을 허용하지 않는 Task 하나를 반환한다.
		/// </summary>
		/// <returns></returns>
		public Task GetSingleTaskForNonDuplication()
		{
			lock (objLock)
			{
				var task = tasks.First(t => t.AllowDuplication == false);
				if (task == null) return null;

				return task as Task;
			}
		}

        public List<Task> GetTasksForNonDuplication()
        {
            lock (objLock)
            {
                var task = tasks.Where(t => t.AllowDuplication == false);
                if (task == null) return null;

                return task.ToList();
            }
        }

		/// <summary>
		/// 이벤트 재생 여부에 따라 중복을 허용하지 않는 Task 하나를 반환한다.
		/// </summary>
		/// <param name="bEventPlay">이벤트 재생 여부</param>
		/// <returns></returns>
		public Task GetSingleTaskForNonDuplication(bool bEventPlay)
		{
			lock (objLock)
			{
				try
				{
					int nShowFlag = bEventPlay ? (int)DigitalSignage.Common.ShowFlag.Show_Event : (int)DigitalSignage.Common.ShowFlag.Show_NonEvent;
					var task = tasks.First(t => ((t.AllowDuplication == false) &&
						(t.show_flag & nShowFlag) > 0));

					if (task == null) return null;

					return task as Task;
				}
				catch (Exception ex) {
					Console.WriteLine(ex.ToString());
					return null; }
			}
		}
        
        public void RestoreFirst()
        {
            lock (objLock)
            {
                currtask = null;
            }
        }

		public Task First
		{
			get{
				lock (objLock)
				{
					bStarted = true;

					if (tasks.Count > 0)
						return currtask = tasks[0];
					else
					{
						bStarted = false;
						return null;
					}
				}
			}
		}

		public Task NextToEnd
		{
			get
			{
				lock (objLock)
				{
					bStarted = true;

					if (currtask == null && tasks.Count > 0)
						return currtask = tasks[0];
					else if (currtask == null && tasks.Count == 0)
					{
						bStarted = false;
						return null;
					}
					Task comp = null;
					for (int i = 0; i < tasks.Count; ++i)
					{
						if (tasks[i].uuid == currtask.uuid)
						{
							try
							{
								comp = tasks[i + 1];
								break;
							}
							catch
							{
								comp = null;
							}
						}
					}
					return currtask = comp;
				}
			}
		}

		public Task Next
		{
			get{
				bStarted = true;

				if (currtask == null && tasks.Count > 0)
					return currtask = tasks[0];
				else if (currtask == null && tasks.Count == 0)
				{
					bStarted = false;
					return null;
				}
				Task comp = null;
				for (int i = 0; i < tasks.Count; ++i)
				{
					if (tasks[i].uuid == currtask.uuid)
					{
						try
						{
							comp = tasks[i+1];
							break;
						}
						catch
						{
							comp = tasks[0];
						}
					}
				}

				return currtask = comp;
			}
		}
		public Task Current
		{
			get{
				return currtask;
			}
		}
		public Task Previous
		{
			get
			{
				lock (objLock)
				{

					bStarted = true;

					if (currtask == null && tasks.Count > 0)
						return currtask = tasks[0];
					else if (currtask == null && tasks.Count == 0)
					{
						bStarted = false;
						return null;
					}
					Task comp = null;
					for (int i = 0; i < tasks.Count; ++i)
					{
						if (tasks[i].uuid == currtask.uuid)
						{
							try
							{
								comp = tasks[i - 1];
								break;
							}
							catch
							{
								comp = tasks[tasks.Count - 1];
							}
						}
					}

					return currtask = comp;
				}
			}
		}

		public bool IsStarted
		{
			get {
				return bStarted; 
			}
			set {
				bStarted = value;
			}
		}
		#endregion

		#region Functions

		/// <summary>
        /// 고유 ID를 통해 스케줄을 가져온다.
		/// </summary>
		/// <param name="uuid"></param>
		/// <param name="isSetCurrent"></param>
		/// <returns></returns>
        public Task FindTask(string uuid, bool isSetCurrent = false)
		{
			try
			{
                Task task = tasks.First(t => uuid.Equals(t.uuid.ToString()));
                if (isSetCurrent && task != null) currtask = task;
                return task;
			}
			catch { }

			return null;
		}

        /// <summary>
        /// 그룹 스케줄 ID를 통해 스케줄을 가져온다.
        /// </summary>
        /// <param name="guuid"></param>
        /// <param name="isSetCurrent"></param>
        /// <returns></returns>
        public Task FindTaskByGuuid(string guuid, bool isSetCurrent = false)
        {
            try
            {
                Task task = tasks.First(t => guuid.Equals(t.guuid.ToString()));
                if (isSetCurrent && task != null) currtask = task;
                return task;
            }
            catch { }

            return null;
        }
	
        /// <summary>
		/// 작업을 추가한다
		/// </summary>
		/// <param name="task"></param>
		/// <returns>추가된 경우는 추가된 개수, 이미 있는 경우는 0, 실패한 경우는 -1</returns>
        public int AddTask(Task task)
        {
            return AddTask(task, false);
        }


        /// <summary>
        /// 작업을 추가한다.
        /// </summary>
        /// <param name="task"></param>
        /// <param name="canAddDup">이미 있는 작업도 추가가 가능한지 여부</param>
        /// <returns></returns>
		private int AddTask(Task task, bool canAddDup)
		{
			try
			{
				int nRet = 1;

				lock (objLock)
				{
                    if (!canAddDup)
                    {
                        //	이미 있는 작업이라면 삭제를 해주고
                        for (int i = tasks.Count - 1; i >= 0; i--)
                        {
                            if (tasks[i].uuid == task.uuid)
                            {
                                tasks.RemoveAt(i);
                                nRet = 0;
                                break;
                            }
                        }
                    }

					bool bAdd = false;
					for (int i = 0; i < tasks.Count; ++i)
					{
						if (tasks[i].priority <= task.priority)
						{
							tasks.Insert(i, task);
							bAdd = true;
							break;
						}
					}

					//	추가한다.
					if (!bAdd)
						tasks.Add(task);
				}

				return nRet;
			}
			catch { return -1; }
		}

		/// <summary>
		/// 작업을 삭제한다
		/// </summary>
		/// <param name="task"></param>
		public bool RemoveTask(Task task)
		{
			lock (objLock)
			{
				for (int i = tasks.Count - 1; i >= 0; i--)
				{
					if (tasks[i].uuid == task.uuid)
					{
						tasks.RemoveAt(i);
						return true;
					}
				}
			}
			return false;
		}

        /// <summary>
        /// 모든 작업을 삭제한다.
        /// </summary>
        /// <returns></returns>
        public bool RemoveAllTasks()
        {
            lock (objLock)
            {
                tasks.Clear();
            }
            return true;
        }

		/// <summary>
		/// Task를 넣는다
		/// </summary>
		/// <param name="task">넣을 Task</param>
		public void Push(Task task)
		{
			AddTask(task);
		}

		/// <summary>
		/// Task를 뺀다
		/// </summary>
		/// <returns>빠진 Task</returns>
		public Task Pop()
		{
			try
			{
				lock (objLock)
				{
					Task popTask = tasks[0];
					tasks.RemoveAt(0);
					return popTask;
				}
			}
			catch(Exception e)
			{
				return null;
			}
		}

        /// <summary>
        /// 같은 Task가 존재 하는지
        /// </summary>
        /// <param name="task"></param>
        /// <returns></returns>
        public bool Contain(Task task)
        {
			try
			{
                lock (objLock)
                {
                    foreach (Task t in tasks)
                    {
                        if (t.uuid == task.uuid)
                            return true;
                    }
                }
            }
            catch 
            {
            }

            return false;
        }

        /// <summary>
        /// 다음으로 재생하기위해 현재 Task을 대상의 이전 Task 등록한다.
        /// </summary>
        /// <param name="task"></param>
        /// <returns></returns>
        public bool PrepareToNextTaskForPlaying(Task task)
        {
            bool bRet = false;
            try
            {
                lock (objLock)
                {
                    foreach (Task t in tasks)
                    {
                        if (t.uuid == task.uuid)
                        {
                            bRet = true;
                            currtask = t;
                            break;
                        }
                    }
                }
                if (bRet) currtask = this.Previous;
            }
            catch { }
            return bRet;
        }
		#endregion
	}
}
