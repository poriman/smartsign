﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Media.Animation;
using System.Windows.Forms;

namespace DigitalSignage.Player
{
    /// <summary>
    /// Interaction logic for LoadingWindow.xaml
    /// </summary>
    public partial class LoadingWindow : Window
    {
        public int DelayTime = 1000;
        private bool bIsAutoClosing = true;
        public LoadingWindow(bool bAutoClosing = true)
        {
            bIsAutoClosing = bAutoClosing;

            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (bIsAutoClosing)
            {
                startAnimation_Completed(this, null);
            }

        }

        public void CloseWindow()
        {
            try
            {
                startAnimation_Completed(this, null);
            }
            catch { }
        }

        /*
        void tmClosing_Tick(object sender, EventArgs e)
        {
            if (nCount++ > 0)
            {
                tmClosing.Stop();
                tmClosing.Dispose();

                //더블 애니메이션 하나 설정 했다. 
                DoubleAnimation MyDoubleAnimation = new DoubleAnimation();
                MyDoubleAnimation.From = 1;
                MyDoubleAnimation.To = 0;

                MyDoubleAnimation.Duration = new Duration(TimeSpan.FromSeconds(0.5));
                MyDoubleAnimation.FillBehavior = FillBehavior.Stop;
                MyDoubleAnimation.Completed += new EventHandler(MyDoubleAnimation_Completed);
                this.BeginAnimation(Window.OpacityProperty, MyDoubleAnimation);
            }
        }
        */

        void MyDoubleAnimation_Completed(object sender, EventArgs e)
        {
            this.Close();
        }

        private void startAnimation_Completed(object sender, EventArgs e)
        {
            //더블 애니메이션 하나 설정 했다. 
            DoubleAnimation MyDoubleAnimation = new DoubleAnimation();
            MyDoubleAnimation.From = 1;
            MyDoubleAnimation.To = 0;
            MyDoubleAnimation.By = 0.3;

            MyDoubleAnimation.Duration = new Duration(TimeSpan.FromMilliseconds(DelayTime < 500 ? 500 : DelayTime));
            MyDoubleAnimation.FillBehavior = FillBehavior.Stop;
            MyDoubleAnimation.Completed += new EventHandler(MyDoubleAnimation_Completed);
            this.BeginAnimation(Window.OpacityProperty, MyDoubleAnimation);
        }
    }
}
