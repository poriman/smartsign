﻿
using System;
using System.IO;
using System.Collections;
using DigitalSignage.PlayAgent;
using NLog;
using System.Collections.ObjectModel;

namespace DigitalSignage.Player
{
    public class Cleaner
    {
        private Config config = Config.GetConfig;
		private static Logger logger = LogManager.GetCurrentClassLogger();


        public int deleteLostProjects()
        {
            ArrayList directories = new ArrayList();
            // get projects list from storage
            String[] subdirs = Directory.GetDirectories(config.AppData + "\\xaml\\");
            foreach (String dir in subdirs)
                directories.Add(System.IO.Path.GetFileName( dir) );
            // check list
            Task[] tasks = config.localTasksList.GetAllTasks();
			
			//	hsshin 기본 Task는 지우지 않는다.
			string defaulttask = Task.TaskDefaultScreen.ToString();
			directories.Remove(defaulttask);

            for (int i = 0; i < tasks.Length; i++)
            {
                string taskduuid = tasks[i].duuid.ToString();
                directories.Remove(taskduuid);
            }
            // deleting unlinked ones
			for (int i = 0; i < directories.Count; i++)
			{
				logger.Info(config.AppData + "\\xaml\\" + directories[i]);
				DigitalSignage.Common.FTPFunctions.RecursiveDelete(config.AppData + "\\xaml\\" + directories[i], true);
			} 
			return 0;
        }

        public int deleteOldProjects()
        {
            // get all projects that was ended
			Task[] tasks = config.localTasksList.GetAllTasks();
            // delete them from stoprage and local database

			Collection<string> arrAlive = new Collection<string>();
			Collection<string> arrWillRemove = new Collection<string>();

            foreach ( Task task in tasks )
            {
                bool isComDefaultProgram = (task.type == DigitalSignage.Common.TaskCommands.CmdDefaultProgram || task.type == DigitalSignage.Common.TaskCommands.CmdTouchDefaultProgram);

                if (isComDefaultProgram == true && (task.state == DigitalSignage.Common.TaskState.StateProcessed || task.state == DigitalSignage.Common.TaskState.StateCanceled))
				{
					arrWillRemove.Add(task.duuid.ToString());
				}
				else if (isComDefaultProgram != true && config.localTasksList.CanDeleteTask(task.duuid.ToString()))
				{
					//	hsshin 1주일 전 프록램은 아직지우지않는다.
					if (task.TaskEnd > (int)DigitalSignage.Common.TimeConverter.ConvertToUTP(DateTime.UtcNow.AddDays(-7)))
					{
						logger.Info(task.duuid.ToString() + " : This task will delete in 7 days.");

						arrAlive.Add(task.duuid.ToString());

						continue;
					}

					arrWillRemove.Add(task.duuid.ToString());
				}
				else
				{
					arrAlive.Add(task.duuid.ToString());

					continue;
				}

                config.localTasksList.CleanTask( task );
            }

			foreach (string duuid in arrWillRemove)
			{
				if (!arrAlive.Contains(duuid))
				{
					logger.Info(config.AppData + "\\xaml\\" + duuid);
					DigitalSignage.Common.FTPFunctions.RecursiveDelete(config.AppData + "\\xaml\\" + duuid, true);
				}
			}

            return 0;
        }
    }
}
