﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NLog;
using System.Xml;
using DigitalSignage.Common;

namespace DigitalSignage.Player
{
	class ControlScheduleHelper
	{
        private static ControlScheduleHelper _instance = null;

        public static ControlScheduleHelper GetInstance
        {
            get {
                if (_instance == null)
                {
                    _instance = new ControlScheduleHelper();
                }
                return _instance;
            }
        }
		private static Logger logger = LogManager.GetCurrentClassLogger();

        public event ChangeStatusEventHandler OnChangeStatus = null;

		public bool PlayControlSchedule(PlayAgent.Task task)
		{
            String sTitle = task.description;
			try
			{
                iVisionLogHelper.GetInstance.GeneralLog(sTitle, task.uuid.ToString(), TaskCommands.CmdControlSchedule, "");

                TaskControlSchedules ControlSched = GetKindOfCommand(task.description);
				switch (ControlSched)
				{
					case TaskControlSchedules.CtrlReboot:
						PCControl.Reboot();
						break;
					case TaskControlSchedules.CtrlPowerOff:
						PCControl.PowerOff();
						break;
					case TaskControlSchedules.CtrlPlayerOn:
						Config.GetConfig.IsPlayerOn = true;
// 						Config.GetConfig.KillPlayer();
						break;
					case TaskControlSchedules.CtrlPlayerOff:
                        Config.GetConfig.IsPlayerOn = false;
// 						Config.GetConfig.KillPlayer();
						break;
					case TaskControlSchedules.CtrlVolume:
						{
							String[] arrVolumes = sTitle.Split('[')[1].TrimEnd(']').Split('|');

							logger.Info("SET MASTER VOLUME NOW : " + arrVolumes[0]);
							logger.Info("SET WAVE VOLUME NOW : " + arrVolumes[1]);
							PCControl.SetMasterVolume(Convert.ToInt32(arrVolumes[0]));
							PCControl.SetWaveVolume(Convert.ToInt32(arrVolumes[1]));
						}
						break;
					case TaskControlSchedules.CtrlVolumeImmediately:
						{
							String[] arrVolumes = sTitle.Split('[')[1].TrimEnd(']').Split('|');

							logger.Info("SET MASTER VOLUME NOW : " + arrVolumes[0]);
							logger.Info("SET WAVE VOLUME NOW : " + arrVolumes[1]);
							PCControl.SetMasterVolume(Convert.ToInt32(arrVolumes[0]));
							PCControl.SetWaveVolume(Convert.ToInt32(arrVolumes[1]));
						}
						break;
                    case TaskControlSchedules.CtrlEventVolume:
                        {
							String[] arrVolumes = sTitle.Split('[')[1].TrimEnd(']').Split('|');

                            logger.Info("SET EVENT VOLUME : " + arrVolumes[0]);
                            Config.GetConfig.StoredEventVolume = Convert.ToInt32(arrVolumes[0]);
                        }
                        break;
                    case TaskControlSchedules.CtrlSubtitle:
                        {
                            String[] arrSubtitles = sTitle.Split('[')[1].TrimEnd(']').Split('|');

                            if (arrSubtitles[0] == "Y")
                            {
                                Config.GetConfig.IsTransparentToSubtitle = true;
                                Config.GetConfig.SubSpeed = Convert.ToInt32(arrSubtitles[1]);
                            }
                            else
                            {
                                Config.GetConfig.IsTransparentToSubtitle = false;
                                Config.GetConfig.SubBackcolor = arrSubtitles[1];
                                Config.GetConfig.SubSpeed = Convert.ToInt32(arrSubtitles[2]);
                            }
                        }
                        break;
                    case TaskControlSchedules.CtrlFileTransmit:
                        {
                            /// File[testNetworkAdapter.exe],Run[Y],Param[/s],Folder[8],Subpath[Contents]
                            MetaTagHelper helper = new MetaTagHelper(task.MetaTags);

                            String sDescFolder = GetiVisionSpecialFolder((FileTransferInfo)Convert.ToInt32(helper.DataDictionary["Folder"]));

                            if(!String.IsNullOrEmpty(helper.DataDictionary["Subpath"] as String))
                            {
                                sDescFolder += helper.DataDictionary["Subpath"].ToString().Replace('/', '\\');
                                if(!sDescFolder.EndsWith("\\")) sDescFolder += "\\";
                            }

                            String SSrcFileName = String.Format(@"{0}PlayerData\Xaml\{1}\{2}", GetiVisionSpecialFolder(FileTransferInfo.FILE_IVISION), task.duuid, helper.DataDictionary["File"]);

                            bool bSuccess = false;
                            int nRetry = 0;
                            do
                            {
                                try
                                {
                                    String sDescFileName = String.Format(@"{0}{1}", sDescFolder, helper.DataDictionary["File"]);
                                    logger.Info("파일 복사 시도중 ({0}) > ({1})", SSrcFileName, sDescFileName);
                                    System.IO.File.Copy(SSrcFileName, sDescFileName, true);
                                    logger.Info("파일 복사 성공: {0}", sDescFileName);

                                    bSuccess = true;

                                    break;
                                }
                                catch (Exception err) { logger.Error("파일 복사 실패 {1}: {0}", err.Message, nRetry); }
                                System.Threading.Thread.Sleep(1000);
                            } while (nRetry++ > 3);

                            if(helper.DataDictionary["Run"].Equals("Y"))
                            {
                                String sExcuteFileName = SSrcFileName;
                                if (bSuccess)
                                {
                                    sExcuteFileName = sDescFolder + helper.DataDictionary["File"];
                                }

                                System.Diagnostics.Process.Start(sExcuteFileName, helper.DataDictionary["Param"].ToString());
                                logger.Info("파일 실행 성공: {0}", sExcuteFileName);
                            }
                            
                            
                        }
                        break;
					case TaskControlSchedules.CtrlUndefined:
						iVisionLogHelper.GetInstance.GeneralLogError(sTitle, task.uuid.ToString(), TaskCommands.CmdControlSchedule, ErrorCodes.IS_ERROR_UNSUPPT_COMMAND, "No supported command.");
						return false;
				}

                if (OnChangeStatus != null) OnChangeStatus(null, new ChangeStatusEventArgs(ControlSched, null));

				return true;
			}
			catch (System.IO.FileNotFoundException fnfex)
			{
				logger.Error(fnfex.ToString());
				iVisionLogHelper.GetInstance.GeneralLogError(sTitle, task.uuid.ToString(), TaskCommands.CmdControlSchedule, ErrorCodes.IS_ERROR_CONTENT_NOT_FOUND, fnfex.Message);
			}
			catch (System.IO.IOException ioex)
			{
				logger.Error(ioex.ToString());
                iVisionLogHelper.GetInstance.GeneralLogError(sTitle, task.uuid.ToString(), TaskCommands.CmdControlSchedule, ErrorCodes.IS_ERROR_CONTENT_CRASHED, ioex.Message);
			}
			catch (Exception ex)
			{
				logger.Error(ex.ToString());
                iVisionLogHelper.GetInstance.GeneralLogError(sTitle, task.uuid.ToString(), TaskCommands.CmdControlSchedule, ErrorCodes.IS_ERROR_UNDEFINED, ex.Message);
			}

			return false;
		}

        /// <summary>
        /// i-Vision 스페셜 폴더를 가져온다.
        /// </summary>
        /// <param name="folder"></param>
        /// <returns></returns>
        public String GetiVisionSpecialFolder(FileTransferInfo folder)
        {
            switch (folder)
            {
                case FileTransferInfo.FILE_DESKTOP:
                    return Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory);
                case FileTransferInfo.FILE_INSTALLDRIVE:
                    return String.Format("{0}:\\", System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location).Substring(0,1));
                case FileTransferInfo.FILE_IVISION:
                    return System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\";
                case FileTransferInfo.FILE_MYDOCUMENT:
                    return Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                case FileTransferInfo.FILE_REFDATA:
                    return System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\PlayerData\\RefData\\";
                case FileTransferInfo.FILE_TEMP:
                    return Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
            }

            return String.Empty;
        }
		public int GetValue(String sTitle)
		{
			try
			{
				String sVolume = sTitle.Split('[')[1].TrimEnd(']');
				return Convert.ToInt32(sVolume);
			}
			catch
			{

			}
			return -1;
		}

		public TaskControlSchedules GetKindOfCommand(String sTitle)
		{
			try
			{
				TaskControlSchedules command = TaskControlSchedules.CtrlUndefined;

				if (sTitle.Contains(TaskControlSchedules.CtrlReboot.ToString()))
				{
					command = TaskControlSchedules.CtrlReboot;
				}
				else if (sTitle.Contains(TaskControlSchedules.CtrlPowerOff.ToString()))
				{
					command = TaskControlSchedules.CtrlPowerOff;
				}
				else if (sTitle.Contains(TaskControlSchedules.CtrlPlayerOn.ToString()))
				{
					command = TaskControlSchedules.CtrlPlayerOn;
				}
				else if (sTitle.Contains(TaskControlSchedules.CtrlPlayerOff.ToString()))
				{
					command = TaskControlSchedules.CtrlPlayerOff;
				}
				else if (sTitle.Contains(TaskControlSchedules.CtrlVolumeImmediately.ToString()))
				{
					command = TaskControlSchedules.CtrlVolumeImmediately;
				}
				else if (sTitle.Contains(TaskControlSchedules.CtrlVolume.ToString()))
				{
					command = TaskControlSchedules.CtrlVolume;
				}
				else if (sTitle.Contains(TaskControlSchedules.CtrlSerialAuto.ToString()))
				{
					command = TaskControlSchedules.CtrlSerialAuto;
				}
				else if (sTitle.Contains(TaskControlSchedules.CtrlSerialPowerOff.ToString()))
				{
					command = TaskControlSchedules.CtrlSerialPowerOff;
				}
				else if (sTitle.Contains(TaskControlSchedules.CtrlSerialPowerOn.ToString()))
				{
					command = TaskControlSchedules.CtrlSerialPowerOn;
				}
				else if (sTitle.Contains(TaskControlSchedules.CtrlSerialSourceDVI.ToString()))
				{
					command = TaskControlSchedules.CtrlSerialSourceDVI;
				}
				else if (sTitle.Contains(TaskControlSchedules.CtrlSerialSourceComponent.ToString()))
				{
					command = TaskControlSchedules.CtrlSerialSourceComponent;
				}
				else if (sTitle.Contains(TaskControlSchedules.CtrlSerialSourceRGB.ToString()))
				{
					command = TaskControlSchedules.CtrlSerialSourceRGB;
				}
				else if (sTitle.Contains(TaskControlSchedules.CtrlSerialSourceTV.ToString()))
				{
					command = TaskControlSchedules.CtrlSerialSourceTV;
				}
				else if (sTitle.Contains(TaskControlSchedules.CtrlSerialVolume.ToString()))
				{
					command = TaskControlSchedules.CtrlSerialVolume;
				}
				else if (sTitle.Contains(TaskControlSchedules.CtrlSerialVolumeNow.ToString()))
				{
					command = TaskControlSchedules.CtrlSerialVolumeNow;
				}
				else if (sTitle.Contains(TaskControlSchedules.CtrlSyncSchedule.ToString()))
				{
					command = TaskControlSchedules.CtrlSyncSchedule;
				}
                else if (sTitle.Contains(TaskControlSchedules.CtrlEventVolume.ToString()))
                {
                    command = TaskControlSchedules.CtrlEventVolume;
                }
                else if (sTitle.Contains(TaskControlSchedules.CtrlSubtitle.ToString()))
                {
                    command = TaskControlSchedules.CtrlSubtitle;
                }
                else if (sTitle.Contains(TaskControlSchedules.CtrlFileTransmit.ToString()))
                {
                    command = TaskControlSchedules.CtrlFileTransmit;
                }
                else
                {
                    command = TaskControlSchedules.CtrlUndefined;
                }

				return command;
			}
			catch {}

			return TaskControlSchedules.CtrlUndefined;
		}

		public TaskControlSchedules GetKindOfCommand(Guid duuid)
		{
			try
			{
				String sFilePath = Config.GetConfig.AppData + "xaml\\" + duuid.ToString() + "\\command.xml";
				logger.Info("Loading [" + sFilePath + "]");

				XmlDocument doc = new XmlDocument();
				doc.Load(sFilePath);

				XmlNode nodeControl = doc.SelectSingleNode("Control");

				return (TaskControlSchedules)(Convert.ToInt32(nodeControl.Attributes["ctrl_type"].Value));
			}
			catch {}

			return TaskControlSchedules.CtrlUndefined;
		}
		private bool LoadControlSchedule (PlayAgent.Task task)
		{
            String sTitle = task.description;
			try
			{
                String sFilePath = Config.GetConfig.AppData + "xaml\\" + task.duuid.ToString() + "\\command.xml";
				logger.Info("Loading [" + sFilePath + "]");

				XmlDocument doc = new XmlDocument();
				doc.Load(sFilePath);

				XmlNode nodeControl = doc.SelectSingleNode("Control");

				TaskControlSchedules ControlSched = (TaskControlSchedules)(Convert.ToInt32(nodeControl.Attributes["ctrl_type"].Value));
				logger.Info("Control Schedule Type : " + ControlSched.ToString());

				switch (ControlSched)
				{
					case TaskControlSchedules.CtrlReboot:
						PCControl.Reboot();
						break;
					case TaskControlSchedules.CtrlPowerOff:
						PCControl.PowerOff();
						break;
					case TaskControlSchedules.CtrlPlayerOn:
						break;
					case TaskControlSchedules.CtrlPlayerOff:
						break;
					case TaskControlSchedules.CtrlVolume:
						PCControl.SetWaveVolume(Convert.ToInt32(nodeControl.Attributes["ctrl_value"].Value));
						break;
					case TaskControlSchedules.CtrlVolumeImmediately:
						PCControl.SetWaveVolume(Convert.ToInt32(nodeControl.Attributes["ctrl_value"].Value));
						break;
					case TaskControlSchedules.CtrlUndefined:
						iVisionLogHelper.GetInstance.GeneralLogError(sTitle, task.uuid.ToString(), TaskCommands.CmdControlSchedule, ErrorCodes.IS_ERROR_UNSUPPT_COMMAND, "No supported command.");
						return false;
						break;
				}

                iVisionLogHelper.GetInstance.GeneralLog(sTitle, task.uuid.ToString(), TaskCommands.CmdControlSchedule, "");
				return true;
			}
			catch (System.IO.FileNotFoundException fnfex)
			{
				logger.Error(fnfex.ToString());
                iVisionLogHelper.GetInstance.GeneralLogError(sTitle, task.uuid.ToString(), TaskCommands.CmdControlSchedule, ErrorCodes.IS_ERROR_CONTENT_NOT_FOUND, fnfex.Message);
			}
			catch (System.IO.IOException ioex)
			{
				logger.Error(ioex.ToString());
                iVisionLogHelper.GetInstance.GeneralLogError(sTitle, task.uuid.ToString(), TaskCommands.CmdControlSchedule, ErrorCodes.IS_ERROR_CONTENT_CRASHED, ioex.Message);
			}
			catch (Exception ex)
			{
				logger.Error(ex.ToString());
                iVisionLogHelper.GetInstance.GeneralLogError(sTitle, task.uuid.ToString(), TaskCommands.CmdControlSchedule, ErrorCodes.IS_ERROR_UNDEFINED, ex.Message);
			}

			return false;

		}
	}
    public delegate void ChangeStatusEventHandler(object Sender, ChangeStatusEventArgs e);

    public class ChangeStatusEventArgs : EventArgs
    {
        public TaskControlSchedules control = TaskControlSchedules.CtrlUndefined;
        public object objStatus = null;

        public ChangeStatusEventArgs(TaskControlSchedules con, object objStat)
        {
            control = con;
            objStatus = objStatus;
        }
    }
}
