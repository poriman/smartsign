﻿using System;
using System.IO;
using System.Linq;
// using System.Collections.Generic;
// using System.Collections;
using System.Xml;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
//using System.Windows.Shapes;
using System.Windows.Threading;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Markup;
using System.Windows.Forms;
using System.Windows.Media.Animation;
using System.Drawing;

using System.Collections;

using DigitalSignage.Common;
using DigitalSignage.DataBase;

// logging routines
using NLog;
// RSS routines
using Sloppycode.net;

using WPFDesigner;
using System.Threading;

// .NET remoting
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;
using System.Runtime.Remoting.Channels.Tcp;
using DigitalSignage.PlayControls;

namespace DigitalSignage.Player
{
	public class CmdAliveReceiver : MarshalByRefObject, ICmdReceiver
	{
		Logger logger = LogManager.GetCurrentClassLogger();
		public Object ProcessExpandCommand(CmdExReceiverCommands command)
		{
			try
			{
				TimerLoop tm = TimerLoop.GetInstance;

				switch (command)
				{
					case CmdExReceiverCommands.CmdExCurrentScreen:
						return tm.CurrentScreen;
						break;
					case CmdExReceiverCommands.CmdExCurrentSubtitle:
						return tm.CurrentSubtitle;
						break;
				}
			}
			catch (Exception e)
			{
				logger.Error(e + "");
			}
			return null;
		}

		public long ProcessSimpleCommand(CmdReceiverCommands cmd)
		{
			Logger logger = LogManager.GetCurrentClassLogger();
			long res = -1; //not implemented

			try
			{
				Config cfg = Config.GetConfig;

				switch (cmd)
				{
					case CmdReceiverCommands.CmdEcho:
						res = new Random(128).Next(128);
						break;
					case CmdReceiverCommands.CmdGetVersion:
						res = cfg.Version;
						break;
					case CmdReceiverCommands.CmdKillPlayer:
// 						System.Windows.Application.Current.MainWindow.Close();
						System.Windows.Application.Current.Shutdown();
						res = 0;
						break;
				}
			}
			catch (Exception e)
			{
				logger.Error(e + "");
			}
			return res;
		}
	}

    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
	public partial class MainWindow : Window
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private Config cfg = Config.GetConfig;
		private iVisionLogHelper iVisionLog = iVisionLogHelper.GetInstance;
		
//        private DispatcherTimer dt;
//         private UDPListener udpListener;
        //private WPFDesigner.DesignElementsManager designManager;
        TimerLoop tm;

        private PlayerDef primaryPlayer = null;
        private Canvas primaryBuffer = null;
        private ScaleTransform primaryScale = null;

        private PlayerDef backPlayer = null;
        private Canvas backBuffer = null;
        private ScaleTransform backScale = null;

		Mutex _mutex = null;

		#region fade-in-out routines
		bool fade_start = true;
		bool fade_end = true;
		#endregion

		public MainWindow()
        {
            try
            {
				InitializeComponent();

				#region Mutex - 프로그램 한개만 뜨도록
				bool bCreate = false;
				_mutex = new Mutex(true, "DigitalSignage.Player", out bCreate);
				if (!bCreate)
				{
					System.Windows.Application.Current.Shutdown();
					return;
				}
				#endregion

				registerService();
				
				Task t = new Task();
				t.type = TaskCommands.CmdInitPlayer;
                t.uuid = Task.TaskInitPlayer;
                t.pid = cfg.PlayerId;
				t.gid = cfg.GroupId;
                t.duuid = Task.TaskInitPlayer;
                t.guuid = Task.TaskInitPlayer;
				t.description = "CmdInitPlayer";

				t.TaskStart = TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()); // run just now
				if (-1 == cfg.localTasksList.AddTask(t))
				{
					////	추가가 되지않음.
					cfg.localTasksList.CleanTask(Task.TaskInitPlayer);
					//	다시 추가 시도
					cfg.localTasksList.AddTask(t);
				}

				//	hsshin 숨김
				System.Windows.Forms.Cursor.Hide();

				try
				{
					fade_start = Convert.ToBoolean(System.Configuration.ConfigurationSettings.AppSettings["fade_start"]);
					fade_end = Convert.ToBoolean(System.Configuration.ConfigurationSettings.AppSettings["fade_end"]);
				}
				catch
				{

				}

				logger.Info("Starting Player...");

                tm = TimerLoop.GetInstance;
                tm.Start(this);

				return;
            }
            catch (Exception e)
            {
                logger.Error(e + "");
            }
            this.Close();
        }

		private int registerService()
		{
			try
			{
				// creating providers chunks
				BinaryServerFormatterSinkProvider provider = new BinaryServerFormatterSinkProvider();
				provider.TypeFilterLevel = System.Runtime.Serialization.Formatters.TypeFilterLevel.Full;
				ClientIPProvider ipProvider = new ClientIPProvider();
				provider.Next = ipProvider;

				// regestering channel
				Hashtable props = new Hashtable();
				props.Add("port", 1889);
				TcpChannel channel = new TcpChannel(props, null, provider);
				ChannelServices.RegisterChannel(channel, false);

				// resgestering services
				RemotingConfiguration.RegisterWellKnownServiceType(
				  typeof(CmdAliveReceiver),
				  "CmdReceiverHost/rt",
				  WellKnownObjectMode.Singleton);

				logger.Info(".NET remoting handlers registered successfully");
				// 				udpListener = new UDPListener();
				//                 logger.Info("UDP listener started");
				return 0;
			}
			catch (Exception e)
			{
				logger.Error(e + "");
			}
			return -1;
		}

        #region Async routines
        delegate bool loadProjectCB(string filename);
        public bool loadProject(String projectName)
        {
            if (!this.Dispatcher.CheckAccess())
            {
                try
                {
                    this.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                       new loadProjectCB(loadProject), projectName);
                }
                catch (Exception e)
                {
                    logger.Error(e + "");
					return false;
                }
            }
            else
            {
				try
				{
					if (projectName.Equals(""))
					{
						PrimaryUI.Stop();
						return true;
					}

					BackUI.Load(projectName);

					BackUI.IsFadeIn = fade_start;
					BackUI.IsFadeOut = fade_end;
					BackUI.Start();

					
					BackUI.Opacity = 1.0;
					PrimaryUI.Opacity = 0.0;

					PrimaryUI.Stop();

					this.BeginInit();
					ProgramPlayer swap = PrimaryUI;
					PrimaryUI = BackUI;
					BackUI = swap;

					BackUI.Children.Clear();
					this.EndInit();

					System.Windows.Forms.Cursor.Hide();
					
					System.GC.Collect();
				}
				catch (iVisionException ive)
				{
					iVisionLog.ScreenErrorLog(projectName, ive.ErrorCode, ive.DetailMessage);

					logger.Error(String.Format("iVisionException Code : {0}, Description : {1}", ive.ErrorCode, ive.DetailMessage));
					logger.Error(ive.StackTrace);
					return false;
				}
				catch (IOException ioe)
				{
					logger.Error(ioe.Message/* + " " + e.StackTrace*/);

					FileInfo info = new FileInfo(projectName);
					if(info != null)
					{
						if(!info.Exists)
						{
							iVisionLog.ScreenErrorLog(info.Name, ErrorCodes.IS_ERROR_CONTENT_NOT_FOUND, ioe.Message);
						}
						else
						{
							iVisionLog.ScreenErrorLog(info.Name, ErrorCodes.IS_ERROR_CONTENT_CRASHED, ioe.Message);
						}

					}

					logger.Error(ioe.StackTrace);

					return false;
					
				}
				catch (Exception e)
				{
					logger.Error(e.Message/* + " " + e.StackTrace*/);

					FileInfo info = new FileInfo(projectName);
					if(info != null)
					{
						iVisionLog.ScreenErrorLog(info.Name, ErrorCodes.IS_ERROR_INTERNAL, e.Message);
					}

					logger.Error(e.StackTrace);

				}

				return false;
            }

			return true;
        }

        public bool setSubtitle(String projectName)
        {
            if (!this.Dispatcher.CheckAccess())
            {
                try
                {
                    this.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
					   new loadProjectCB(setSubtitle), projectName);
                }
                catch (Exception e)
                {
                    logger.Error(e + "");
					return false;
                }
            }
            else
            {
                try
                {
                    if (projectName.Equals(""))
                    {
                        //	hsshin null이 먹지 않기때문에 임시로 Clear를 집어넣음;;;
                        //	subsTextUI.Document = null;
                        subsTextUI.Document.Blocks.Clear();
                        DoubleAnimation emptyanimation = new DoubleAnimation();
                        subtilesTransformUI.BeginAnimation(TranslateTransform.XProperty, emptyanimation);
                        logger.Info("Unloading subtitle.");

                        return true;
                    }
                    //subsTextUI.Margin = new Thickness( this.ActualWidth, 0, 0, 0 );

                    double length = 0;
                    {
                        System.Windows.Controls.RichTextBox richTextBox = new System.Windows.Controls.RichTextBox();
                        TextReader treader = new StreamReader(projectName);
                        FlowDocument document = FlowDocumentManager.DeserializeFlowDocument(treader.ReadToEnd());
                        float linwith = (float)1.1;
                        foreach (Block block in document.Blocks)
                        {
                            foreach (Inline _line in block.ContentStart.Paragraph.Inlines)
                            {
                                TextRange flowDocSelection = new TextRange(_line.ContentStart, _line.ContentEnd);
                                length += flowDocSelection.Text.Length * _line.FontSize * linwith;
                            }
                        }
                        treader.Close();
                        length += 10;
                    }

                    subsTextUI.Width = length;

                    TextReader tr = new StreamReader(projectName);
                    subsTextUI.Document = FlowDocumentManager.DeserializeFlowDocument(tr.ReadToEnd());
                    subsTextUI.UpdateLayout();

                    Canvas.SetTop(subsTextUI, this.ActualHeight - subsTextUI.ActualHeight - 10);
                    tr.Close();

                    DoubleAnimation animation = new DoubleAnimation();
                    if (length > this.ActualWidth)
                        animation.Duration = new Duration(new TimeSpan((long)(length) * 110000));
                    else
                        animation.Duration = new Duration(new TimeSpan((long)(ActualWidth) * 110000));

                    animation.SpeedRatio = 1.0;
                    animation.RepeatBehavior = RepeatBehavior.Forever;
                    animation.By = 20.0;
                    animation.From = this.ActualWidth + 10;
                    animation.To = -subsTextUI.ActualWidth - 10;
                    subtilesTransformUI.BeginAnimation(TranslateTransform.XProperty, animation);
                    logger.Info("Loading subtitle from " + projectName + " " + animation.From + "-" + animation.To);
					
					iVisionLog.SubtitleStartLog(projectName);
                }
				catch (iVisionException ive)
				{
					iVisionLog.ScreenErrorLog(projectName, ive.ErrorCode, ive.DetailMessage);
					return false;
				}
                catch (Exception e)
                {
                    iVisionLog.ScreenErrorLog(projectName, ErrorCodes.IS_ERROR_INTERNAL, e.Message);
					return false;
                }

            }
			return true;
        }

        public void SetStatusText(string text)
        {
            /*try
            {
                if (!this.Dispatcher.CheckAccess())
                {
                    this.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                       new loadProjectCB(SetStatusText), text );
                }
                else
                {
                    status.Content = text;
                }
            }
            catch (Exception e)
            {
                logger.Error(e + "");
            }*/
        }
        #endregion

        #region Event handlers
        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.Cursor.Hide();
            subsTextUI.Margin = new Thickness(0, 20, 0, 60);
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            System.Windows.Forms.Cursor.Show();
			
			if (PrimaryUI != null)
			{
				PrimaryUI.Stop();
			}
			
//             if (udpListener != null)
//             {
//                 udpListener.Stop();
//                 udpListener = null;
//             }
            if (tm != null)
            {
                tm.Stop();
                tm = null;
            }

			if(primaryBuffer != null)
			{
				primaryBuffer = null;
			}

			if (backBuffer != null)
			{
				backBuffer = null;
			}

			if (_mutex != null)
				_mutex.ReleaseMutex();

			logger.Info("Ending Player...");
        }

        #endregion
    }
}