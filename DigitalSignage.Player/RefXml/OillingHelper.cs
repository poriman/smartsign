﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.ComponentModel;

namespace DigitalSignage.Player.RefXml
{
    [XmlRoot(ElementName = "Oilling", Namespace="www.inteiilansys.co.kr"), Serializable]
    public class Oilling
    {
        [XmlAttribute(AttributeName = "ts")]
        public String timestamp { get; set; }

        [XmlAttribute(AttributeName = "oil_cd")]
        public String code { get; set; }

        [XmlAttribute(AttributeName = "oiling_tp")]
        public String type { get; set; }

        [XmlAttribute(AttributeName = "total_liter")]
        public String total_liter { get; set; }

        [XmlAttribute(AttributeName = "money_per_liter")]
        public String money_per_liter { get; set; }

        [XmlAttribute(AttributeName = "total_money")]
        public String total_money { get; set; }

        [XmlAttribute(AttributeName = "curr_liter")]
        public String curr_liter { get; set; }

        [XmlAttribute(AttributeName = "curr_money")]
        public String curr_money { get; set; }

        [XmlAttribute(AttributeName = "finished")]
        public bool finished { get; set; }

        /*
        [XmlElement(ElementName = "liter", IsNullable = false)]
        public OillingLiter liter { get; set; }
        [XmlElement(ElementName = "money", IsNullable = false)]
        public OillingMoney money { get; set; }
        */
        public Oilling()
        {
            timestamp = DateTime.Now.ToString();
            finished = false;
            code = "0660";
            type = "0";
            total_liter = "50.0";
            total_money = "100000";
            money_per_liter = "2000";
        }
    }
    /*
    [XmlRoot(ElementName = "money"), Serializable]
    public class OillingMoney
    {
        public String money { get; set; }
    }

    [XmlRoot(ElementName = "liter"), Serializable]
    public class OillingLiter
    {
        public String liter { get; set; }
    }
     */
}
