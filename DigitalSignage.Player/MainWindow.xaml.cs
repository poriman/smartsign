﻿using System;
using System.IO;
using System.Linq;
// using System.Collections.Generic;
// using System.Collections;
using System.Xml;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
//using System.Windows.Shapes;
using System.Windows.Threading;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Markup;
using System.Windows.Forms;
using System.Windows.Media.Animation;
using System.Drawing;

using System.Collections;

using DigitalSignage.Common;

// logging routines
using NLog;
using NLog.Targets;

using WPFDesigner;
using System.Threading;

// .NET remoting
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;
using System.Runtime.Remoting.Channels.Tcp;
using DigitalSignage.PlayControls;
using System.Windows.Controls.Primitives;
using System.Reflection;
using System.Configuration;

using DigitalSignage.EventReciever;
using System.Collections.Generic;
using DigitalSignage.Player.RefXml;
using System.Xml.Serialization;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Windows.Interop;
using System.Diagnostics;
using DigitalSignage.Controls;


namespace DigitalSignage.Player
{
	public class CmdAliveReceiver : MarshalByRefObject, IPlayerCmdReceiver
	{
		Logger logger = LogManager.GetCurrentClassLogger();
		public Object ProcessExpandCommand(CmdExReceiverCommands command)
		{
			try
			{
				TimerLoop tm = TimerLoop.GetInstance;
                
				switch (command)
				{
					case CmdExReceiverCommands.CmdExCurrentScreen:
						return tm.CurrentScreen;
					case CmdExReceiverCommands.CmdExCurrentSubtitle:
						return tm.CurrentSubtitle;
					case CmdExReceiverCommands.CmdExIsPlayerOn:
						return tm.IsPlayerVisible;
					case CmdExReceiverCommands.CmdExSerialStatusPower:
						return tm.SerialPortDeviceStatus(SerialPortController.SerialPortStatus.statusPower);
						break;
					case CmdExReceiverCommands.CmdExSerialStatusSource:
						return tm.SerialPortDeviceStatus(SerialPortController.SerialPortStatus.statusSource);
						break;
					case CmdExReceiverCommands.CmdExSerialStatusVolume:
						return tm.SerialPortDeviceStatus(SerialPortController.SerialPortStatus.statusVolume);
						break;
				}
			}
			catch (Exception e)
			{
				logger.Error(e + "");
			}
			return null;
		}

        public bool SendControlCommand(CmdExReceiverCommands command, int nValue)
        {
            try
            {
                TimerLoop tm = TimerLoop.GetInstance;

                switch (command)
                {
                    case CmdExReceiverCommands.CmdExSerialStatusPower:
                        if (nValue == 0) return tm.GetSerialPortObject().PowerOff();
                        else return tm.GetSerialPortObject().PowerOn();
                    case CmdExReceiverCommands.CmdExSerialStatusVolume:
                        return tm.GetSerialPortObject().Volume(nValue);
                }
            }
            catch (Exception e)
            {
                logger.Error(e + "");
            }
            return false;
        }

		public long ProcessSimpleCommand(CmdReceiverCommands cmd)
		{
			Logger logger = LogManager.GetCurrentClassLogger();
			long res = -1; //not implemented

			try
			{
				Config cfg = Config.GetConfig;
				TimerLoop tm = TimerLoop.GetInstance;

				switch (cmd)
				{
					case CmdReceiverCommands.CmdEcho:
						res = tm.IsPlaying ? new Random(128).Next(128) : -1;
						break;
					case CmdReceiverCommands.CmdGetVersion:
						res = cfg.Version;
						break;
					case CmdReceiverCommands.CmdKillPlayer:
// 						System.Windows.Application.Current.MainWindow.Close();
						System.Windows.Application.Current.Shutdown();
						res = 0;
						break;
				}
			}
			catch (Exception e)
			{
				logger.Error(e + "");
			}
			return res;
		}

        public bool SendEvent(String sEventValue)
        {
            if(!String.IsNullOrEmpty(sEventValue))
            {
                logger.Info("Event 수신: {0}", sEventValue);

                String sCode = sEventValue;
                String sValue = String.Empty;

                String[] arrSplit = sEventValue.Split('|');

                sCode = arrSplit[0];
                if(arrSplit.Length > 1)
                    sValue = arrSplit[1];

                if (sCode.ToLower().Contains("screen enabled"))
                {
                    TimerLoop.GetInstance.GetParent.LoadingVisible(false, false);
                }/* Screen Disabled */
                else if (sCode.ToLower().Contains("screen disabled"))
                {
                    TimerLoop.GetInstance.GetParent.LoadingVisible(true, false);
                }
                else if (sCode.ToLower().Contains("screen stop"))
                {
                    TimerLoop.GetInstance.GetParent.LoadingVisible(false, false);
                    TimerLoop.GetInstance.PausePlaying();
                }
                else if (sCode.ToLower().Contains("scheduled screen play"))
                {
                    TimerLoop.GetInstance.GetParent.LoadingVisible(false, false);
                    
                    if (TimerLoop.GetInstance.IsUrgentEvent)
                    {
                        TimerLoop.PlayerStatus = TimerLoop.PlayStatus.Played;
                        TimerLoop.GetInstance.CancelUrgentEvent();
                    }
                    else
                    {
                        TimerLoop.PlayerStatus = TimerLoop.PlayStatus.Played;
                        TimerLoop.GetInstance.NextScreen();
                    }

                }
                else if (sCode.ToLower().Contains("sync move"))
                {
                    TimerLoop.GetInstance.EventSyncMoveProcess(sCode, sValue);
                }

                return true;
            }
            return false;
        }

        public bool SendSyncSchedule(String sGroupID)
        {
            return TimerLoop.GetInstance.PlaySyncSchedule(sGroupID);
        }

        public bool SendEventResponse(String sMetaTags)
        {
            try
            {
                logger.Info("Event Responsed from server: " + sMetaTags);
                if (!String.IsNullOrEmpty(sMetaTags))
                {
                    TimerLoop tm = TimerLoop.GetInstance;
                    int nFirstSep = sMetaTags.IndexOf('|');
                    String sID = sMetaTags.Substring(0, nFirstSep).TrimEnd(new char[] {' ', '|'});
                    return tm.MakeMetatagEvent(sID, Config.GetConfig.LastEventRequestInfo, sMetaTags.Substring(nFirstSep + 1));
                }
            }
            catch (Exception e)
            {
                Config.GetConfig.WriteTargetAdError(Config.GetConfig.CurrentRelayID, TargetAd_ErrorType.TargetAd_Client_ToLower, (int)AdTarget_Client_ErrorCodes.ErrorInvalidData);
                logger.Error(e + "");
            }
            return false;
        }

        /// <summary>
        /// 스크린 캡쳐 요청
        /// </summary>
        /// <returns></returns>
        public byte[] GetScreenCapture()
        {
            try
            {
                TimerLoop tm = TimerLoop.GetInstance;
                return tm.GetParent.captureScreen(320, 240);
                
            }
            catch { }

            return null;
        }



	}

    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
	public partial class MainWindow : Window
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

		#region 프로그램 상태 관련 이벤트
		/// <summary>
		/// 프로그램 상태
		/// </summary>
		public enum PlayerStatus
		{
			/// <summary>
			/// 로딩 중
			/// </summary>
			Loading,
			/// <summary>
			/// 재생 중
			/// </summary>
			Playing,
			/// <summary>
			/// 닫힘
			/// </summary>
			Closed,
			/// <summary>
			/// 실패
			/// </summary>
			Failed,
			/// <summary>
			/// 지연됨
			/// </summary>
			Pending
		}

		public PlayerStatus StatusOfPlayer = PlayerStatus.Closed;
		#endregion

        private bool isPrevLoading = Config.GetConfig.PreLoadingMode;

		/// <summary>
		/// 이벤트 리시버
		/// </summary>
		private EventReciever.EventReciever eventListener = null;
		
		private Config cfg = Config.GetConfig;
		private iVisionLogHelper iVisionLog = iVisionLogHelper.GetInstance;
		
        TimerLoop tm;
        private Canvas primaryBuffer = null;
        private Canvas backBuffer = null;

        Window popSubtiles = null;

        private static object lockSubtitle = new object();

        Mutex _mutex = null;

		#region fade-in-out routines
		bool fade_start = true;
		bool fade_end = true;
		#endregion

		bool is_window_mode = false;

        


        #region 중앙대 hsjang
        bool is_started = true;
        Thread Research_File = null;
        #endregion

        #region 제어스케줄
        bool is_visible_player = true;

        delegate bool PlayerVisibleCB(bool isVisible);
        bool PlayerVisible(bool isVisible)
        {
            if (!this.Dispatcher.CheckAccess())
            {
                try
                {
                    DispatcherOperation o = this.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                       new PlayerVisibleCB(PlayerVisible), isVisible);

                    o.Wait();

                    return o.Result == null ? false : (bool)o.Result;
                }
                catch (Exception e)
                {
                    logger.Error(e + "");
                    return false;
                }
            }
            else
            {
                is_visible_player = isVisible;
                this.Visibility = is_visible_player ? Visibility.Visible : Visibility.Hidden;
                
                if(is_visible_player) this.Activate();
                
                return true;
            }
        }

		public bool IsPlayerVisible
		{
			get
			{
				return is_visible_player;
			}
            set
            {
                PlayerVisible(value);
            }
		}
		#endregion


        private LoadingWindow window = null;

        delegate bool LoadingVisibleCB(bool isVisible, bool UseFadeDelaySetting = true);
        public bool LoadingVisible(bool isVisible, bool UseFadeDelaySetting = true)
        {
            if (UseFadeDelaySetting == false || Properties.Settings.Default.FadeInDelayTime > 0)
            {
                if (!this.Dispatcher.CheckAccess())
                {
                    try
                    {
                        DispatcherOperation o = this.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                           new LoadingVisibleCB(LoadingVisible), isVisible, UseFadeDelaySetting);

                        o.Wait();

                        return o.Result == null ? false : (bool)o.Result;
                    }
                    catch (Exception e)
                    {
                        logger.Error(e + "");
                        return false;
                    }
                }
                else
                {
                    if (isVisible)
                    {

                        if (window == null)
                        {
                            logger.Info("로딩 윈도우 생성 ({0},{1},{2},{3})", this.Left, this.Top, this.ActualWidth, this.ActualHeight);
                            window = new LoadingWindow(false);
                            window.Width = this.ActualWidth;
                            window.Height = this.ActualHeight;
                            window.Left = this.Left < 0 ? 0 : this.Left;
                            window.Top = this.Top < 0 ? 0 : this.Top;
                            window.DelayTime = Properties.Settings.Default.FadeInDelayTime;

                            window.Show();
                        }
                    }
                    else
                    {
                        if (window != null)
                        {
                            window.CloseWindow();
                            window = null;
                        }
                    }
                }
            }

            return true;
        }

        DateTime dtLastUpdateFiles = DateTime.MinValue;

        /// <summary>
        /// 지정폴더에 있는 파일의 종류 리턴
        /// </summary>
        /// <returns></returns>
        private string MediaType(FileInfo finfo)
        {
            if (finfo.Extension == ".jpg" || finfo.Extension == ".bmp" || finfo.Extension == ".gif")
                return "image";
            else if (finfo.Extension == ".avi" || finfo.Extension == ".mp4" || finfo.Extension == ".wmv" || finfo.Extension == ".mkv")
                return "video";
            else if (finfo.Extension == ".smil")
                return "smil";
            return "unkown";
        }

        /// <summary>
        /// 지정폴더 업데이트 검사 스레드(중앙대)
        /// </summary>
        private void Thread_ResearchFile()
        {
            string FilePath = Properties.Settings.Default.Search_File;

            if (String.IsNullOrEmpty(FilePath)) return;

            try
            {
                if (Directory.Exists(FilePath) != true)
                {
                    Directory.CreateDirectory(FilePath);
                }
            }
            catch { }

            while (is_started)
            {
                try
                {
                    DirectoryInfo dinfo = new DirectoryInfo(FilePath);
                    System.IO.FileInfo[] files;

                    bool IsUpdate = false;
                    #region 1. 새로 만들어야 할지 검사
                    FileInfo f = new FileInfo(FilePath+"contents.xml");
                    if (dtLastUpdateFiles<f.LastWriteTime)
                        IsUpdate = true;
                    #endregion

                    #region 2. 새로 만들어야 한다면 Smil / Playlist / Program 목록 갱신
                    if (IsUpdate)
                    { 
                        //  SMIL 파일 모두 지우기
                        files = dinfo.GetFiles("*.smil", SearchOption.AllDirectories);
                        foreach (System.IO.FileInfo file in files)
                        {
                            // 만약 ReadOnly 속성이 있는 파일이 있다면 지울때 에러가 나므로 속성을 Normal로 바꿔놓는다.
                            if (file.Attributes == FileAttributes.ReadOnly)
                                file.Attributes = FileAttributes.Normal;
                            file.Delete();
                        }

                        string playtime = string.Empty;
                        
                        #region Playlist 만들기 / Program 만들기 / SMIL 만들기

                        XmlDocument doc = new XmlDocument();
                        XmlElement elPlaylist = doc.CreateElement("playlist");

                        foreach (FileInfo finfo in dinfo.GetFiles())
                        {
                            MakeSpecialScreen(finfo, ref elPlaylist, ref doc);
                        }
                        doc.AppendChild(elPlaylist);

                        if (doc.HasChildNodes && elPlaylist.HasChildNodes)
                        {
                            doc.Save(Config.GetConfig.AppData + "usbplaylist.xml");

                            XmlDocument doc2 = new XmlDocument();
                            doc2.LoadXml("<?xml version=\"1.0\"?><program> <pane playlist=\"usbplaylist.xml\" duration=\"23:59:59\" /> </program>");
                            doc2.Save(Config.GetConfig.AppData + "usbprogram.xml");

                            //  플레이어 재생
                            tm.IsUSBPlay = true;
                            tm.CurrentRemovalDrive = "a";
                            tm.AddUsbTask();
                            tm.PlayNextUSBScreenTask();
                        }
                        else
                        {
                            tm.IsUSBPlay = false;
                            tm.PlayNextUrgentScreenTask();
                        }
                        
                        #endregion


                        dtLastUpdateFiles = DateTime.Now;
                        IsUpdate = false;
                    }

                    #endregion
                    
                }
                catch (Exception ex) { ex.ToString(); }
                finally
                {
                    Thread.Sleep(Properties.Settings.Default.researchfile_Time);
                }
            }
        }

        private void MakeSpecialScreen(FileInfo finfo, ref XmlElement elPlaylist, ref XmlDocument doc)
        {
            try
            {
                XmlElement elScreen = doc.CreateElement("screen");
                XmlNodeList nodeList = null;
                TimeSpan duration = TimeSpan.FromSeconds(30);
                  string smilName = "";
                XmlDocument docSmil = new XmlDocument();
                if (MediaType(finfo) == "image")
                {
                    docSmil.Load(AppDomain.CurrentDomain.BaseDirectory + "PlayerData/image.xml");
                    nodeList = docSmil.GetElementsByTagName("img");
                    duration = TimeSpan.FromSeconds(Properties.Settings.Default.ImageTime);
                }
                if (MediaType(finfo) == "video")
                {
                    docSmil.Load(AppDomain.CurrentDomain.BaseDirectory + "PlayerData/video.xml");
                    nodeList = docSmil.GetElementsByTagName("video");

                    try
                    {
                        //FilgraphManager filterGraph = new FilgraphManager();
                        //filterGraph.RenderFile(finfo.FullName);
                        //IMediaPosition pos = filterGraph as IMediaPosition;
                        //duration = new TimeSpan(0, 0, (int)pos.Duration);
                        
                        //WindowsMediaPlayerClass wmp = new WindowsMediaPlayerClass();
                        //IWMPMedia media = wmp.newMedia(finfo.FullName);
                        //int nPlaytime = Convert.ToInt32(media.duration);
                        int nPlaytime = 60;
                        duration = new TimeSpan(0, 0, nPlaytime > 0 ? nPlaytime - 1 : nPlaytime);
                        //wmp.close();
                        //wmp = null;
                    }
                    catch
                    {
                        duration = TimeSpan.FromSeconds(30);
                    }
                }

                foreach (XmlNode node in nodeList)
                {
                    node.Attributes["src"].InnerText = finfo.FullName;
                    node.Attributes["dur"].InnerText = String.Format("{0}s", (int)duration.TotalSeconds);
                    docSmil.Save(smilName = (Properties.Settings.Default.Search_File + finfo.Name.Substring(0, finfo.Name.IndexOf(".")) + ".smil"));
                }

                XmlAttribute attr1 = doc.CreateAttribute("time");
                attr1.Value = duration.ToString();
                XmlAttribute attr2 = doc.CreateAttribute("selected");
                attr2.Value = "1";
                XmlAttribute attr3 = doc.CreateAttribute("ID");
                attr3.Value = smilName;

                elScreen.Attributes.Append(attr1);
                elScreen.Attributes.Append(attr2);
                elScreen.Attributes.Append(attr3);

                elPlaylist.AppendChild(elScreen);
            }
            catch { }
        }


		public MainWindow()
        {
            try
            {
				InitializeLogger();

                /*
				TrialChecker checker = new TrialChecker();
				if (!checker.IsValid)
				{
					System.Windows.MessageBox.Show(checker.LastMessage);
					System.Windows.Application.Current.Shutdown();
				}
                 * */
				
				InitializeComponent();

				logger.Info("Create And Check Mutex!");

				PrimaryUI.ProgramLoadCompleted += new ProgramLoadCompletedHandler(Event_ProgramLoadCompleted);
				BackUI.ProgramLoadCompleted += new ProgramLoadCompletedHandler(Event_ProgramLoadCompleted);
				#region Mutex - 프로그램 한개만 뜨도록
				bool bCreate = false;
				_mutex = new Mutex(true, "DigitalSignage.Player", out bCreate);
				if (!bCreate)
				{
					logger.Info("Player is already exist!");

					System.Windows.Application.Current.Shutdown();
					return;
				}
				#endregion



				logger.Info("Registing Service!");

				registerService();
				
				cfg = new Config();
				cfg.init();


				DigitalSignage.PlayAgent.Task t = new DigitalSignage.PlayAgent.Task();
				t.type = TaskCommands.CmdInitPlayer;
				t.uuid = DigitalSignage.PlayAgent.Task.TaskInitPlayer;
                t.pid = cfg.PlayerId;
				t.gid = cfg.GroupId;
				t.duuid = DigitalSignage.PlayAgent.Task.TaskInitPlayer;
				t.guuid = DigitalSignage.PlayAgent.Task.TaskInitPlayer;
				t.description = "CmdInitPlayer";

				t.TaskStart = TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()); // run just now
				if (-1 == cfg.localTasksList.AddTask(t))
				{
					////	추가가 되지않음.
					cfg.localTasksList.CleanTask(Task.TaskInitPlayer);
					//	다시 추가 시도
					cfg.localTasksList.AddTask(t);
				}

				//	hsshin 숨김

				if (cfg.IsManualToPlayer)
				{

					this.WindowState = WindowState.Normal;
					this.WindowStartupLocation = WindowStartupLocation.Manual;

					if (is_window_mode = cfg.ShowWindowEdge)
					{
						System.Windows.Forms.Cursor.Show();
						this.WindowStyle = WindowStyle.ThreeDBorderWindow;
						this.ResizeMode = ResizeMode.CanResizeWithGrip;
 
						this.ShowInTaskbar = true;
					}
					else
					{
						this.WindowStyle = WindowStyle.None;
						this.ResizeMode = ResizeMode.NoResize;
						this.ShowInTaskbar = false;
                        System.Windows.Forms.Cursor.Hide();
					}

					this.Left = (double)cfg.WindowXPos;
					this.Top = (double)cfg.WindowYPos;
					this.Width = (double)cfg.WindowWidth;
					this.Height = (double)cfg.WindowHeight;
				}
				else
				{
					System.Windows.Forms.Cursor.Hide();

					this.WindowState = WindowState.Maximized;
					this.WindowStyle = WindowStyle.None;
					this.ResizeMode = ResizeMode.NoResize;
					this.ShowInTaskbar = false;
				}
				try
				{
					//	hsshin 제어 스케줄
					this.IsPlayerVisible = cfg.IsPlayerOn;

					fade_start = cfg.FadeStart;
					fade_end = cfg.FadeEnd;

                    this.Topmost = cfg.WindowTopMost;

				}
				catch
				{

                }

                #region 중앙대 - 특정 폴더 컨텐츠 탐색 기능
                Research_File = new Thread(Thread_ResearchFile);
                Research_File.IsBackground = true;
                Research_File.Start();
                #endregion

				/* //Transition 효과 
                #region Transition Effect

                Timeline.DesiredFrameRateProperty.OverrideMetadata( typeof(Timeline), 
                    new FrameworkPropertyMetadata { DefaultValue = 40 });

                object appValue = ConfigurationManager.AppSettings["TransitionEffect"];
                if (appValue != null)
                {
                    currentTransitionType = (TransitionEffect.TransitionType)Enum.Parse(typeof(TransitionEffect.TransitionType), Convert.ToString(appValue));
                }
                
                switch (currentTransitionType)
                {
                    case TransitionEffect.TransitionType.Default:
                        PrimaryUI.ProgramLoadCompleted += new ProgramLoadCompletedHandler(Event_ProgramLoadCompleted);
                        BackUI.ProgramLoadCompleted += new ProgramLoadCompletedHandler(Event_ProgramLoadCompleted);

                        transition = new TransitionEffect.Transition(currentTransitionType);
                        break;
                    case TransitionEffect.TransitionType.Slide:
                        transition = new TransitionEffect.SlideTransition(currentTransitionType);
                        break;
                    case TransitionEffect.TransitionType.Slide2:
                        transition = new TransitionEffect.SlideTransition2(currentTransitionType);
                        break;
                    default:
                        transition = new TransitionEffect.Transition(currentTransitionType);
                        break;
                }
                this.transition.SetGrid(this, this.screenGrid, this.PrimaryUI, this.BackUI);//Transition(전환) 효과를 위한 layout 재 배치 시킨

                #endregion
				*/

				//	이벤트 리스너 시작

                int port = 8888;
                string method = "tcp";

                try
                {
                    port = Properties.Settings.Default.Event_Port;
                }
                catch { }

                try
                {
                    method = Properties.Settings.Default.Event_Method;
                }
                catch { }

                eventListener = new EventReciever.EventReciever(method, port);
				eventListener.Start();
				eventListener.OnEventOccured += new RecieverEventHandler(eventListener_OnEventOccured);
				logger.Info("Started Event Listener...");

                tm = TimerLoop.GetInstance;
                tm.Start(this);

				
				logger.Info("Starting Player...");

				this.PreviewKeyDown += new System.Windows.Input.KeyEventHandler(MainWindow_PreviewKeyDown);
                this.SourceInitialized += new EventHandler(MainWindow_SourceInitialized);
                this.Closing += new CancelEventHandler(MainWindow_Closing);
                this.Loaded += new RoutedEventHandler(MainWindow_Loaded);

                return;
            }
            catch (Exception e)
            {
                logger.Error(e + "");
            }

            this.Close();
        }

        void GetInstance_OSDChanged(object sender, OSDChangedEventArgs e)
        {
            DispatcherOperation disop = Dispatcher.BeginInvoke(DispatcherPriority.Send, new Action(delegate
			{
                DelayTime.Text = e.OSDText;
            }));

            disop.Wait();
        }
        
        void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                /// USB 재생 목록 검색
                FindScreensInRemovalDrives();

                if (Properties.Settings.Default.ShowOSD)
                {
                    subsPopup.Visibility = System.Windows.Visibility.Visible;
                    subsPopup.Placement = System.Windows.Controls.Primitives.PlacementMode.AbsolutePoint;

                    subsPopup.HorizontalOffset = this.Left;
                    subsPopup.VerticalOffset = this.Top;
                    subsPopup.IsOpen = true;

                    /// 이벤트 연결
                    OSDObject.GetInstance.OSDChanged += new OSDChangeEventHandler(GetInstance_OSDChanged);
                }

            }
            catch { }
        }

        void MainWindow_Closing(object sender, CancelEventArgs e)
        {
            try
            {
                IntPtr windowHandle = (new WindowInteropHelper(this)).Handle;
                HwndSource src = HwndSource.FromHwnd(windowHandle);
                src.RemoveHook(new HwndSourceHook(this.WndProc));
            }
            catch { }
        }

        void MainWindow_SourceInitialized(object sender, EventArgs e)
        {
            try
            {
                IntPtr windowHandle = (new WindowInteropHelper(this)).Handle;
                HwndSource src = HwndSource.FromHwnd(windowHandle);
                src.AddHook(new HwndSourceHook(WndProc));
            }
            catch { }
        }

        private IntPtr WndProc(IntPtr hWnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            UInt32 WM_DEVICECHANGE = 0x0219;
            UInt32 DBT_DEVUP_VOLUME = 0x02;
            UInt32 DBT_DEVICEARRIVAL = 0x8000;
            UInt32 DBT_DEVICEREMOVECOMPLETE = 0x8004;

            if ((msg == WM_DEVICECHANGE) &&
                (wParam.ToInt32() == DBT_DEVICEARRIVAL))
            {
                int devType = Marshal.ReadInt32(lParam, 4);
                if (devType == DBT_DEVUP_VOLUME)
                {
                    //  USB 추가
                    FindScreensInRemovalDrives();
                }
            }
            if ((msg == WM_DEVICECHANGE) &&
                (wParam.ToInt32() == DBT_DEVICEREMOVECOMPLETE))
            {
                int devType = Marshal.ReadInt32(lParam, 4);
                if (devType == DBT_DEVUP_VOLUME)
                {
                    //  USB 제거
                    OnRemovealDrives();
                }
            }

            return IntPtr.Zero;
        }

        /// <summary>
        /// USB에서 스크린 찾아 재생하기
        /// </summary>
        private void FindScreensInRemovalDrives()
        {
            try
            {
                foreach (string d in System.IO.Directory.GetLogicalDrives())
                {
                    System.IO.DriveInfo dinfo = new DriveInfo(d);
                    if (dinfo.DriveType == DriveType.Removable)
                    {
                        foreach (String directory in System.IO.Directory.GetDirectories(dinfo.RootDirectory.FullName))
                        {
                            if (directory.ToLower().EndsWith("screens"))
                            {
                                if (MakeUSBPlaylist(directory))
                                {
                                    tm.IsUSBPlay = true;
                                    tm.CurrentRemovalDrive = d;
                                    tm.AddUsbTask();
                                    tm.PlayNextUSBScreenTask();
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex) { logger.Error(ex.ToString()); }
        }

        private bool MakeUSBPlaylist(String sDirectory)
        {
            XmlDocument doc = new XmlDocument();

            XmlElement elPlaylist = doc.CreateElement("playlist");

            foreach (String d in System.IO.Directory.GetDirectories(sDirectory))
            {
                MakeUSBScreen(d, ref elPlaylist, ref doc);
            }

            doc.AppendChild(elPlaylist);

            if (doc.HasChildNodes)
            {
                doc.Save(Config.GetConfig.AppData + "usbplaylist.xml");
                
                XmlDocument doc2 = new XmlDocument();
                doc2.LoadXml("<?xml version=\"1.0\"?><program> <pane playlist=\"usbplaylist.xml\" duration=\"23:59:59\" /> </program>");
                doc2.Save(Config.GetConfig.AppData + "usbprogram.xml");
                
                return true;
            }
            return false;
        }

        private void MakeUSBScreen(String sDirectory, ref XmlElement elPlaylist, ref XmlDocument doc)
        {
            try
            {
                XmlElement elScreen = doc.CreateElement("screen");

                foreach (String f in System.IO.Directory.GetFiles(sDirectory))
                {
                    if (f.ToLower().EndsWith(".smil"))
                    {
                        FileInfo finfo = new FileInfo(f);

                        TimeSpan tsDur = TimeSpan.FromSeconds(0);
                        try
                        {
                            XmlDocument screenDoc = new XmlDocument();
                            screenDoc.Load(f);
                            XmlNamespaceManager nsmgr = new XmlNamespaceManager(screenDoc.NameTable);
                            nsmgr.AddNamespace("sm", "http://www.w3.org/ns/SMIL");
                            XmlNode par_node = screenDoc.SelectSingleNode("//child::sm:par", nsmgr);

                            String sDur = par_node.Attributes["dur"].Value;
                            tsDur = TimeSpan.FromSeconds(Convert.ToDouble(sDur.Replace("s", "")));
                        }
                        catch (Exception ex) { logger.Error(ex.Message); }

                        XmlAttribute attr1 = doc.CreateAttribute("time");
                        attr1.Value = tsDur.ToString();
                        XmlAttribute attr2 = doc.CreateAttribute("selected");
                        attr2.Value = "1";
                        XmlAttribute attr3 = doc.CreateAttribute("ID");
                        attr3.Value = f;

                        elScreen.Attributes.Append(attr1);
                        elScreen.Attributes.Append(attr2);
                        elScreen.Attributes.Append(attr3);

                        elPlaylist.AppendChild(elScreen);
                    }
                }
            }
            catch { }
        }

        /// <summary>
        /// 드라이브 변경 이벤트
        /// </summary>
        private void OnRemovealDrives()
        {
            if (tm.IsUSBPlay)
            {
                bool bRet = true;
                foreach (string d in System.IO.Directory.GetLogicalDrives())
                {
                    if (d == tm.CurrentRemovalDrive)
                    {
                        bRet = false;
                    }
                }

                if (bRet)
                {
                    tm.IsUSBPlay = false;
                    tm.PlayNextUrgentScreenTask();
                }
            }
        }

		void MainWindow_PreviewKeyDown(object sender, System.Windows.Input.KeyEventArgs e)
		{
			if (e.Key == Key.E && e.KeyboardDevice.Modifiers == ModifierKeys.Control)
			{
				this.Close();
			}
		}

        #region Event 관련

        BackgroundWorker _bwRestoreThread = null;
        BackgroundWorker _bwOillingThread = null;
        DateTime _dtTouchStarted = DateTime.Now;
        DateTime _dtOillingStarted = DateTime.Now;
        Oilling oilling_info_xml = null;

        String sLastEventMessage = String.Empty;
        int nLastEventModuleID = 0;

        void _bwRestoreThread_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                while (!_bwRestoreThread.CancellationPending)
                {
                    if ((DateTime.Now - _dtTouchStarted).TotalMinutes > 3)
                    {
                        tm.RestoreEventVolume();
                        tm.GatheringPromotionTasks = false;

                        logger.Error("고객 터치 TIMEOUT:고객 터치 후 응답이 3분 동안 오지 않아서 임의로 이벤트 볼륨을 복구합니다.");

                        e.Result = "TIMEOUTED";
                        break;
                    }
                    Thread.Sleep(1000);
                }
            }
            catch { }
        }


        void _bwOillingThread_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                while (!_bwOillingThread.CancellationPending)
                {
                    if ((DateTime.Now - _dtOillingStarted).TotalMinutes > 5)
                    {
                        EndOilling(nLastEventModuleID, sLastEventMessage);
                        logger.Error("주유 에러:주유 종료 메시지가 5분 동안 오지 않아서 임의로 종료됩니다.");

                        e.Result = "TIMEOUTED";
                        break;
                    }
                    Thread.Sleep(1000);
                }
            }
            catch { }
        }

        /// <summary>
        /// 주유 시작
        /// </summary>
        /// <param name="sEventData"></param>
        void StartOilling(String sEventData)
        {
            long lRealyID = Config.GetConfig.CurrentRelayID = Config.GetConfig.MakeRelayID();
            Dictionary<String, String> arrValues = ParseODTValue(sEventData);
            if (arrValues != null)
            {
                /// 주유 시작 타임아웃 스레드 생성
                _dtTouchStarted = DateTime.Now;

                if (_bwOillingThread != null)
                {
                    _bwOillingThread.CancelAsync();
                    try
                    {
                        _bwOillingThread.Dispose();
                    }
                    catch { }
                    finally
                    {
                        _bwOillingThread = null;
                    }
                }

                _bwOillingThread = new BackgroundWorker();
                _bwOillingThread.WorkerSupportsCancellation = true;

                _bwOillingThread.DoWork += new DoWorkEventHandler(_bwOillingThread_DoWork);

                #region RefXML 만들기
                try
                {
                    oilling_info_xml = new Oilling();
                    oilling_info_xml.timestamp = DateTime.Now.ToString();
                    oilling_info_xml.code = arrValues["유종코드"];
                    oilling_info_xml.type = arrValues["주유설정"];
                    oilling_info_xml.total_liter = arrValues["리터"];
                    oilling_info_xml.total_money = arrValues["금액"];
                    oilling_info_xml.money_per_liter = arrValues["단가"];
                    oilling_info_xml.finished = false;

                    oilling_info_xml.curr_money = "0";
                    oilling_info_xml.curr_liter = "0.0";

                    XmlSerializer ser = new XmlSerializer(typeof(Oilling));
                    // A FileStream is used to write the file.
                    if (!System.IO.Directory.Exists(Config.GetConfig.AppData + "RefData\\"))
                        System.IO.Directory.CreateDirectory(Config.GetConfig.AppData + "RefData\\");

                    using (FileStream fs = new FileStream(Config.GetConfig.AppData + "RefData\\oilling.xml", FileMode.Create))
                    {
                        ser.Serialize(fs, oilling_info_xml);
                        fs.Close();
                    }

                }
                catch (Exception exp)
                {
                    logger.Error(exp);
                }
                #endregion

                /// 요청 패킷 정보 [주유소코드]|[카드번호]|[유종코드]
                String cardNumber = arrValues["카드번호"];
                if (!String.IsNullOrEmpty(cardNumber) && !cardNumber.Contains("0000000000000000"))
                {
                    Config.GetConfig.LastEventRequestInfo = String.Format("{0}|{1}|{2}", arrValues["주유소코드"], cardNumber, arrValues["유종코드"]);
                    try
                    {
                        IAgentCmdReceiver agentCmdReceiver = (IAgentCmdReceiver)Activator.GetObject(
                            typeof(IAgentCmdReceiver), "tcp://127.0.0.1:1888/CmdReceiverHost/rt");
                        agentCmdReceiver.SendEvent(String.Format("{0}|{1}", lRealyID, Config.GetConfig.LastEventRequestInfo));

                        /// 최대 타겟광고 12회 실시
                        tm.LimitUrgentScreenCount = 12;

                        /// 주유 시작 타이머 가동
                        _dtOillingStarted = DateTime.Now;
                        _bwOillingThread.RunWorkerAsync();
                    }
                    catch
                    {
                        Config.GetConfig.WriteTargetAdError(lRealyID, TargetAd_ErrorType.TargetAd_Client_ToUpper, (int)AdTarget_Client_ErrorCodes.ErrorRemotingConnection);
                    }
                }
                else
                {
                    logger.Info("GS 포인트 카드 정보가 없어서 타겟 광고 이벤트는 제외됩니다.");
                }
            }
            else
            {
                Config.GetConfig.WriteTargetAdError(lRealyID, TargetAd_ErrorType.TargetAd_Client_ToUpper, (int)AdTarget_Client_ErrorCodes.ErrorInvalidData);
            }
        }


        /// <summary>
        /// 주유 중
        /// </summary>
        /// <param name="sEventData"></param>
        void Oilling(String sEventData)
        {
            #region RefXML 만들기
            try
            {
                Dictionary<String, String> arrValues = ParseODTValue(sLastEventMessage = sEventData);
                if (arrValues != null)
                {
                    _dtTouchStarted = DateTime.Now;
                    _dtOillingStarted = DateTime.Now;

                    if (oilling_info_xml == null) oilling_info_xml = new RefXml.Oilling();

                    oilling_info_xml.timestamp = DateTime.Now.ToString();

                    oilling_info_xml.curr_money = arrValues["금액"];
                    oilling_info_xml.curr_liter = arrValues["리터"];

                    XmlSerializer ser = new XmlSerializer(typeof(Oilling));
                    // A FileStream is used to write the file.
                    using (FileStream fs = new FileStream(Config.GetConfig.AppData + "RefData\\oilling.xml", FileMode.Create))
                    {
                        ser.Serialize(fs, oilling_info_xml);
                        fs.Close();
                    }
                }

            }
            catch (Exception exp)
            {
                logger.Error(exp);
            }
            #endregion

        }

        /// <summary>
        /// 주유 종료
        /// </summary>
        /// <param name="sEventData"></param>
        void EndOilling(int nEventModuleID, String sEventData)
        {
            try
            {
                if (_bwRestoreThread != null)
                {
                    _bwRestoreThread.CancelAsync();
                    try
                    {
                        _bwRestoreThread.Dispose();
                    }
                    catch { }
                    finally
                    {
                        _bwRestoreThread = null;
                    }
                }
            }
            catch { }

            try
            {
                if (_bwOillingThread != null)
                {
                    _bwOillingThread.CancelAsync();
                    try
                    {
                        _bwOillingThread.Dispose();
                    }
                    catch { }
                    finally
                    {
                        _bwOillingThread = null;
                    }
                }
            }
            catch { }

            #region RefXML 만들기
            try
            {
                Dictionary<String, String> arrValues = ParseODTValue(sEventData);
                if (arrValues != null)
                {
                    if (oilling_info_xml == null) oilling_info_xml = new RefXml.Oilling();

                    oilling_info_xml.timestamp = DateTime.Now.ToString();
                    oilling_info_xml.finished = true;
                    oilling_info_xml.curr_money = arrValues["금액"];
                    oilling_info_xml.curr_liter = arrValues["리터"];

                    XmlSerializer ser = new XmlSerializer(typeof(Oilling));
                    // A FileStream is used to write the file.
                    using (FileStream fs = new FileStream(Config.GetConfig.AppData + "RefData\\oilling.xml", FileMode.Create))
                    {
                        ser.Serialize(fs, oilling_info_xml);
                        fs.Close();
                    }
                }

            }
            catch (Exception exp)
            {
                logger.Error(exp);
            }
            finally
            {
                //oilling_info_xml = null;
            }
            #endregion

            tm.GatheringPromotionTasks = false;
            if (tm.HasCouponPromotion && tm.HasEntriesPromotion)
            {
                /// 응모/쿠폰 존재
                eventListener.SendMessage(nLastEventModuleID, 703, "");
				
				tm.UrgentCouponPromotions();
            }
            else if (tm.HasEntriesPromotion)
            {
                /// 응모만 존재
                eventListener.SendMessage(nLastEventModuleID, 701, "");
				
				tm.UrgentEntriesPromotions();
			}
            else if (tm.HasCouponPromotion)
            {
                /// 쿠폰만 존재
                eventListener.SendMessage(nLastEventModuleID, 702, "");
				
				tm.UrgentCouponPromotions();
			}
            else
            {
                /// 프로모션 없음
                eventListener.SendMessage(nLastEventModuleID, 700, "");

				///	2회 후 종료 설정
				tm.LimitUrgentScreenCount = 2;

            }

            /// 2. 타겟 광고가 있는 경우 볼륨
            tm.RestoreEventVolume();
        }

        #endregion


		/// <summary>
		/// 이벤트 리스너 이벤트 처리 부
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		void eventListener_OnEventOccured(object sender, RecieverEventArgs e)
		{
			try
			{
				switch (e.EventCode)
				{
					case 1:	// evcode_PrevSchedule
						tm.PreviousScreen();
						eventListener.SendMessage(e.EventModuleID, 1, "Ok"); 
						break;
					case 2:	// evcode_NextSchedule
						tm.NextScreen();
						eventListener.SendMessage(e.EventModuleID, 2, "Ok"); 
						break;
					case 3: // evcode_GetVolume
						int nVol = PCControl.GetMasterVolume();
						eventListener.SendMessage(e.EventModuleID, 3, nVol.ToString()); 
						break;
					case 4: // evcode_SetVolume
						PCControl.SetMasterVolume(Convert.ToInt32(e.EventValues));
						eventListener.SendMessage(e.EventModuleID, 4, "Ok"); 
						break;
					case 5: // evcode_EventQuery
                        tm.LimitUrgentScreenCount = 2;
                        Config.GetConfig.LastEventRequestInfo = "1000|12345|660";
                        if (!tm.MakeMetatagEvent("1", Config.GetConfig.LastEventRequestInfo, e.EventValues))
						{
							logger.Debug("There are no matching tags in active tasks.");
							eventListener.SendMessage(e.EventModuleID, 5, "Failed action Or No matching tags");
						}
						else
							eventListener.SendMessage(e.EventModuleID, 5, "Success");
						break;
                    case 6: // evcode_SOMO_Data
                        {
                            tm.LimitUrgentScreenCount = 2;
                            Config.GetConfig.LastEventRequestInfo = "1000|12345|660";
                            long lRealyID = Config.GetConfig.CurrentRelayID = Config.GetConfig.MakeRelayID();
                            try
                            {
                                IAgentCmdReceiver agentCmdReceiver = (IAgentCmdReceiver)Activator.GetObject(
                                    typeof(IAgentCmdReceiver), "tcp://127.0.0.1:1888/CmdReceiverHost/rt");
                                agentCmdReceiver.SendEvent(String.Format("{0}|{1}", lRealyID, e.EventValues));
                            }
                            catch
                            {
                                Config.GetConfig.WriteTargetAdError(lRealyID, TargetAd_ErrorType.TargetAd_Client_ToUpper, (int)AdTarget_Client_ErrorCodes.ErrorRemotingConnection);
                            }
                            eventListener.SendMessage(e.EventModuleID, 6, "Ok");
                        }
                        break;
                    ///   KFunc1: 응모 클릭
                    case 400:
                        {
                            /// 1. 모바일 연동 
                            /// 2. 연동 기록 로깅
                            if (!String.IsNullOrEmpty(e.EventValues))
                            {
                                EventDataHelper dataHelper = EventDataHelper.MakeEventDataDictionary(e.EventValues);

                                String sReturn = null;
                                if(!String.IsNullOrEmpty(sReturn = dataHelper.GetValue("키데이터")))
                                {
                                    tm.RegisterEntriesPromotion(sReturn, "");
                                    return;
                                }
                            }

                            logger.Error("응모 데이터에 유효한 키데이터가 존재하지 않습니다.");
                        }
                        break;
                    ///   KFunc2: 쿠폰 클릭
                    case 401:
                        {
                            /// 1. 모바일 연동 
                            /// 2. 연동 기록 로깅
                            if (!String.IsNullOrEmpty(e.EventValues))
                            {
                                EventDataHelper dataHelper = EventDataHelper.MakeEventDataDictionary(e.EventValues);

                                String sReturn = null;
                                if (!String.IsNullOrEmpty(sReturn = dataHelper.GetValue("키데이터")))
                                {
                                    tm.RegisterCouponPromotion(sReturn, "");
                                    return;
                                }
                            }

                            logger.Error("쿠폰 데이터에 유효한 키데이터가 존재하지 않습니다.");
                        }
                        break;
                    ///   D0: 주유허가 전 노즐 Down
                    case 500:
                        /// 처리 없음
                        break;
                    ///   D1: 주유허가 전 노즐 Up
                    case 501:
                        /// 처리 없음
                        break;
                    ///   D2: 주유허가 후 노즐 Down
                    case 502:
                        /// 처리 없음
                        break;
                    ///   D3: 주유허가 후 노즐 Up
                    case 503:
                        /// 처리 없음
                        break;
                    ///   D4: 주유 중
                    case 504:
                        /// 1. 계량 정보 파일 갱신
                        Oilling(e.EventValues);

                        break;
                    ///   D5: 주유 종료
                    case 505:
                        /// 1. EVENT QUEUE 종료 및 응모, 쿠폰 이벤트 조회 후 전달
                        EndOilling(nLastEventModuleID = e.EventModuleID, e.EventValues);

                        break;
                    ///   D6: 고객 ODT 조작 시작
                    case 506:
                        tm.LimitUrgentScreenCount = 0;
                        /// 1. 볼륨 조절 로직 수행
                        tm.SetEventVolume();

                        _dtTouchStarted = DateTime.Now;
                        if (_bwRestoreThread != null)
                        {
                            _bwRestoreThread.CancelAsync();
                            _bwRestoreThread = null;
                        }
                        _bwRestoreThread = new BackgroundWorker();
                        _bwRestoreThread.WorkerSupportsCancellation = true;
                        _bwRestoreThread.DoWork += new DoWorkEventHandler(_bwRestoreThread_DoWork);

                        /// 2. EVENT QUEUE 생성 및 초기화
                        tm.GatheringPromotionTasks = true;

                        _bwRestoreThread.RunWorkerAsync();

                        break;
                    ///   M0: 주유 시작 (카드 정보)
                    case 600:
                        /// 1. 카드 정보 있는 경우 타겟 광고 수행
                        {
                            /// 이벤트 큐가 실행되지 않았으면 실행
                            if (tm.GatheringPromotionTasks == false)
                                 tm.GatheringPromotionTasks = true;

                            nLastEventModuleID = e.EventModuleID;
                            StartOilling(e.EventValues);
                        }
                        break;
                    case 2001:
                        {
                            logger.Info("중기청: ({0}) 스케줄 시작!", e.EventValues);
                            tm.PlayTask(e.EventValues, false, false);

                            break;
                        }
				}
			}
			catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
		}

        /// <summary>
        /// ODT에서 넘어온 데이터를 파싱한다.
        /// </summary>
        /// <param name="values"></param>
        /// <returns></returns>
        private Dictionary<String, String> ParseODTValue(String values)
        {
            if (String.IsNullOrEmpty(values)) return null;

            Dictionary<String, String> arrDictionary = new Dictionary<string, string>();
            String[] arrTemp = values.Split(')');
            foreach (string temp in arrTemp)
            {
                try
                {
                    string[] arrKeyValue = temp.Trim(new char[] { '(', ' ' }).Split('=');
                    arrDictionary.Add(arrKeyValue[0], arrKeyValue[1]);
                }
                catch { }
            }

            return arrDictionary;
        }

		private void InitializeLogger()
		{
			String sExecutingPath = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
			string appData = sExecutingPath + "\\PlayerData\\";
			if (!Directory.Exists(appData))
				Directory.CreateDirectory(appData);

			try
			{
				// configuring log output
				FileTarget target = new FileTarget();
				target.Layout = "${longdate}\t[${level}]\t[${callsite}]\t${message}";
				//             target.Layout = "${longdate} ${logger} ${message}";
				target.FileName = appData + "Player.Log.txt";
				target.ArchiveFileName = appData + "archives/Player.Log.{#####}.txt";
				// 			target.ArchiveAboveSize = 256 * 1024 * 10; // archive files greater than 2560 KB
				target.MaxArchiveFiles = 31;
				target.ArchiveEvery = FileArchivePeriod.Day;// FileTarget.ArchiveEveryMode.Day;
				target.ArchiveNumbering = ArchiveNumberingMode.Sequence;// FileTarget.ArchiveNumberingMode.Sequence;
				// this speeds up things when no other processes are writing to the file
				target.ConcurrentWrites = true;
				NLog.Config.SimpleConfigurator.ConfigureForTargetLogging(target, LogLevel.Debug);

			}
			catch { }
		}
		static TcpChannel channel = null;

		private int unregisterService()
		{
			try
			{
				if(channel != null)
				{
					ChannelServices.UnregisterChannel(channel);
					channel = null;
					logger.Info(".NET remoting handlers unregistered!");
				}
			}
			catch (System.Exception ex)
			{
				logger.Error(ex.Message);
				return -1;
			}

			return 0;
		}
		private int registerService()
		{
			try
			{
				// creating providers chunks
				BinaryServerFormatterSinkProvider provider = new BinaryServerFormatterSinkProvider();
				provider.TypeFilterLevel = System.Runtime.Serialization.Formatters.TypeFilterLevel.Full;
				ClientIPProvider ipProvider = new ClientIPProvider();
				provider.Next = ipProvider;

				// regestering channel
				Hashtable props = new Hashtable();
				props.Add("port", 1889);
				channel = new TcpChannel(props, null, provider);
				ChannelServices.RegisterChannel(channel, false);

				// resgestering services
				RemotingConfiguration.RegisterWellKnownServiceType(
				  typeof(CmdAliveReceiver),
				  "CmdReceiverHost/rt",
				  WellKnownObjectMode.Singleton);

				logger.Info(".NET remoting handlers registered successfully");
				// 				udpListener = new UDPListener();
				//                 logger.Info("UDP listener started");
				return 0;
			}
			catch (Exception e)
			{
				logger.Error(e + "");
			}
			return -1;
		}

        #region Async routines

		void Event_ProgramLoadCompleted(object sender, ProgramLoadCompletedArgs e)
		{
			try
			{
                /*
                if (e.Status == ProgramStatus.Playing)
                {
                    DispatcherOperation disopChanged = Dispatcher.BeginInvoke(DispatcherPriority.Send, new Action(delegate
                        {
                            Grid.SetZIndex(BackUI, 1);
                            Grid.SetZIndex(PrimaryUI, 0);

                            this.UpdateLayout();
                            
                        }
                    ));

                    disopChanged.Wait();
                }
                */

				DispatcherOperation disop = Dispatcher.BeginInvoke(DispatcherPriority.Send, new Action(delegate
					{
						if (e.Status == ProgramStatus.Playing)
						{
                            //Thread.Sleep(100);
                            Grid.SetZIndex(BackUI, 1);
                            Grid.SetZIndex(PrimaryUI, 0);

                            this.UpdateLayout();

                            //Thread.Sleep(1000);

                            if (isPrevLoading)
                            {

                                PrimaryUI.Stop();
								
                                logger.Debug("Default Screen Load Back Panel Stoped!");
                            }
                            //Thread.Sleep(1000);
                            
							this.BeginInit();
							ProgramPlayer swap = PrimaryUI;
							PrimaryUI = BackUI;
							BackUI = swap;
							//this.transition.AnimateTransition(this.ChangeSlideDexter, ref PrimaryUI, ref BackUI, isPrimaryUIHanded);//Transition 효과 
							this.EndInit();
							logger.Debug("Default Screen Load Panel Swapped!");

                            StatusOfPlayer = PlayerStatus.Playing;

                            //Thread.Sleep(1000);
                        }
						else if (e.Status == ProgramStatus.Failed)
						{
							logger.Error("Program Loading failed!!");
							BackUI.Stop();

							StatusOfPlayer = PlayerStatus.Failed;
						}
						else if (e.Status == ProgramStatus.Pending)
						{
							try
							{
								logger.Error(String.Format("SmartSign Player is pending to stop screen. SmartSign Player will be restarted! : status ({0})", ProgramStatus.Pending));

								try
								{
									tm.Stop();
								}
								catch { }

								this.ShutDownPlayer();
							}
							catch (System.Exception ex)
							{
								logger.Error(ex.ToString());
							}

							StatusOfPlayer = PlayerStatus.Pending;
						}
					}
				));

				DispatcherOperationStatus dispstatus = disop.Wait(TimeSpan.FromSeconds(60));
				if (dispstatus == DispatcherOperationStatus.Pending)
				{
					try
					{
						logger.Error(String.Format("SmartSign Player is pending to play screen. SmartSign Player will be restarted! : status ({0})", disop.Status));

						try
						{
							tm.Stop();
							disop.Abort();
						}
						catch { }

						this.ShutDownPlayer();
					}
					catch (System.Exception ex)
					{
						logger.Error(ex.ToString());
						StatusOfPlayer = PlayerStatus.Failed;
					}
				}
			}
			catch (System.Exception ex)
			{
				logger.Error(ex.ToString());
				StatusOfPlayer = PlayerStatus.Failed;
			}
			catch { 
				logger.Error("Unhandled Exception Occurs!");
				StatusOfPlayer = PlayerStatus.Failed;
			}

            //LoadingVisible(false);
            
        }
		delegate void ActivatePlayerCB();
		public void ActivatePlayer()
		{
			Dispatcher.Invoke(DispatcherPriority.Normal, new Action(delegate
				{
					try
					{
                        this.Topmost = true;
						this.Activate();
                        this.Topmost = false;
					}
					catch (System.Exception ex)
					{
						logger.Error(ex.ToString());
					}
				}
			));

		}

		delegate void ShutDownPlayerCB();
		public void ShutDownPlayer()
		{
            try
            {
                Config.GetConfig.KillOwnProcess();
            }
            catch { }

			Dispatcher.Invoke(DispatcherPriority.Normal, new Action(delegate
				{
					try
					{
						this.Close();
					}
					catch (System.Exception ex)
					{
						logger.Error(ex.ToString());
					}
				}
			));

		}

        private bool KillPowerpoint()
        {
            Process[] processlist = Process.GetProcessesByName("POWERPNT");

            if (processlist != null)
            {
                foreach (Process proc in processlist)
                {
                    try
                    {
                        String fname = System.IO.Path.GetFileName(proc.MainModule.FileName).ToLower();
                        if (fname.Equals("powerpnt.exe"))
                        {
                            try
                            {
                                proc.Kill();
                                proc.Dispose();
                                logger.Info("Founded 'POWERPNT.exe' process. It will be closed.");
                                return true;
                            }
                            catch (Exception ex)
                            {

                                logger.Error("ModuleName=" + fname + ", " + ex.Message);
                            }
                        }
                    }
                    catch (Exception err)
                    {

                        logger.Error(err.Message);
                    }
                }
            }

            return false;
        }

        #region Window API
        public const int WM_SYSCOMMAND = 0x0112;
        public const int SC_CLOSE = 0xF060;

        [DllImport("user32.dll")]
        public static extern IntPtr FindWindow(string className, string windowName);
        [DllImport("user32.dll")]
        public static extern int SendMessage(IntPtr hwnd, int wMsg, int wParam, int lParam);
        #endregion

        private bool KillPowerpointErrorMessage()
        {
            try
            {
                IntPtr handle = FindWindow(null, "Microsoft PowerPoint");
                {
                    if (handle != null && handle.ToInt32() > 0)
                    {
                        logger.Info("Founded 'Microsoft PowerPoint' Message Window. Will be closed.");
                        SendMessage(handle, WM_SYSCOMMAND, SC_CLOSE, 0);

                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }

            return false;
        }
        delegate byte[] captureScreenCB(int nCx, int nCy);
        public byte[] captureScreen(int nCx, int nCy)
        {
            if (!this.Dispatcher.CheckAccess())
            {
                try
                {
                    DispatcherOperation o = this.Dispatcher.BeginInvoke(new captureScreenCB(captureScreen), DispatcherPriority.Send,
                       new object [] {nCx, nCy});

					DispatcherOperationStatus disp = o.Wait(TimeSpan.FromSeconds(60));
					if (disp == DispatcherOperationStatus.Pending)
					{
						try
						{
							logger.Error(String.Format("i-Vision Player is pending to Capture screen. i-Vision Player will be restarted! : status ({0})", o.Status));
							
							try
							{
								tm.Stop();
								o.Abort();
							}
							catch {}

							this.ShutDownPlayer();
						}
						catch (System.Exception ex)
						{
							logger.Error(ex.ToString());
						}
					}

                    return o.Result == null ? null : (byte[])o.Result;
                }
                catch (Exception e)
                {
                    logger.Error(e + "");
					return null;
                }
            }
            else
            {
                try
				{
                    using (Bitmap bmp = new Bitmap((int)this.ActualWidth, (int)this.ActualHeight, System.Drawing.Imaging.PixelFormat.Format32bppArgb))
                    {
                        using (Graphics g = Graphics.FromImage(bmp))
                        {
                            System.Windows.Point pos = this.PointToScreen(new System.Windows.Point(0, 0));
                            g.CopyFromScreen(new System.Drawing.Point((int)pos.X, (int)pos.Y), new System.Drawing.Point(0, 0), new System.Drawing.Size((int)this.ActualWidth, (int)this.ActualHeight),
                                CopyPixelOperation.SourceCopy);
                            g.Dispose();
                        }

                        using (System.Drawing.Image img = bmp.GetThumbnailImage(nCx, nCy, null, IntPtr.Zero))
                        {
                            using (MemoryStream s = new MemoryStream(1024))
                            {

                                img.Save(s, System.Drawing.Imaging.ImageFormat.Png);
                                img.Dispose();

                                return s.GetBuffer();
                            }
                        }
                    }
                    
                }
				catch (Exception e)
				{
					logger.Error(e.Message/* + " " + e.StackTrace*/);
				}

                return null;
            }
        }


        delegate bool loadProjectCB(string filename);
        public bool loadProject(String projectName)
        {
			if (!is_visible_player)
				return true;

			if (!this.Dispatcher.CheckAccess())
            {
                try
                {
					int nWaitTime = 0;
					while (StatusOfPlayer == PlayerStatus.Loading)
					{
						System.Windows.Forms.Application.DoEvents();
						Thread.Sleep(10);
						nWaitTime += 10;

						if (nWaitTime > 5000)
							break;
					}
                    /*
                    if (!isPrevLoading)
                    {
                        DispatcherOperation disopChanged = Dispatcher.BeginInvoke(DispatcherPriority.Send, new Action(delegate
                            {
                                //Grid.SetZIndex(BackUI, 1);
                                //Grid.SetZIndex(PrimaryUI, 0);
                                PrimaryUI.Stop();
                                logger.Debug("Default Screen Load Back Panel Stoped!");

                                this.UpdateLayout();

                            }
                        ));

                        disopChanged.Wait();
                    }
                     * */

					DispatcherOperation o = this.Dispatcher.BeginInvoke(DispatcherPriority.Send,
                       new loadProjectCB(loadProject), projectName);
					DispatcherOperationStatus disp = o.Wait(TimeSpan.FromSeconds(60));
					if (disp == DispatcherOperationStatus.Pending)
					{
						try
						{
                            if(KillPowerpoint())
                                logger.Info("power point Killed!");

                            if (KillPowerpointErrorMessage())
                                logger.Info("power point ErrorMessage killed!");

							logger.Error(String.Format("SmartSign Player is pending to play screen. SmartSign Player will be restarted! : status ({0})", o.Status));
							
							try
							{
								tm.Stop();
								o.Abort();
							}
							catch {}

							this.ShutDownPlayer();
						}
						catch (System.Exception ex)
						{
							logger.Error(ex.ToString());
						}
					}

					return o.Result == null ? false : (bool)o.Result;
                }
                catch (Exception e)
                {
                    logger.Error(e + "");
					return false;
                }
            }
            else
            {
				try
				{
					StatusOfPlayer = PlayerStatus.Loading;

					if (projectName.Equals(""))
					{
						PrimaryUI.Stop();
						StatusOfPlayer = PlayerStatus.Closed;
						return true;
					}

					if (!isPrevLoading)
					{
						//Thread.Sleep(100);

						PrimaryUI.Stop();
						logger.Debug("Default Screen Load Back Panel Stoped!");
					}
					else
					{
						logger.Debug("Default Screen Paused!");
						PrimaryUI.Pause();
					}

					BackUI.Load(projectName, is_window_mode);
					logger.Debug("Default Screen Load Loaded!");

					BackUI.IsFadeIn = fade_start;
					BackUI.IsFadeOut = fade_end;
					BackUI.Start();
					logger.Debug("Default Screen Load Started!");


					/*
					#region //Transition 효과 
                    if (this.transition.TransitionType == TransitionEffect.TransitionType.Default)
                    {
                        //touchPlayer.ExportToPng(PrimaryUI);

                        BackUI.Load(projectName, is_window_mode);
                        logger.Debug("Default Screen Load Loaded!");

                        BackUI.IsFadeIn = fade_start;
                        BackUI.IsFadeOut = fade_end;
                        BackUI.Start();
                        logger.Debug("Default Screen Load Started!");                        
                    }
                    else
                    {
                        if (isFirstStartApp == true)
                        {
                            //BackUI.Visibility = System.Windows.Visibility.Hidden;
                            PrimaryUI.Load(projectName, is_window_mode);
                            PrimaryUI.Start();                            
                            isFirstStartApp = false;
                            isPrimaryUIHanded = true;                            
                        }
                        else
                        {
                            if (isPrimaryUIHanded == true)
                            {
                                //PrimaryUI.Visibility = System.Windows.Visibility.Hidden;
                                BackUI.Load(projectName, is_window_mode);
                                BackUI.Start();
                            }
                            else
                            {
                                //BackUI.Visibility = System.Windows.Visibility.Hidden;
                                PrimaryUI.Load(projectName, is_window_mode);
                                PrimaryUI.Start();
                            }
                            this.BeginInit();
                            this.transition.AnimateTransition(this.ChangeSlideDexter, ref PrimaryUI, ref BackUI, isPrimaryUIHanded);
                            //AnimateGrids(this.ChangeSlideDexter, PrimaryUI.ActualWidth, BackUI.ActualWidth);
                            this.EndInit();
                        }
                    }
					#endregion 
					*/
                    #region Touch
                    //if (tsAgent == null)
                    //{
                        //tsAgent = TouchScreenAgent.TSAgent.TheOnlyTSAgent;
                        //tsAgent.ChangePaneScreen = new DigitalSignage.TouchScreenAgent.TSAgent.ChangePaneScreenDelegate(ChangedTouchPageDelegateFunc);
                        //tsAgent.InitTouchScreenAgent(projectName, new System.Windows.Size(this.ActualWidth, this.ActualHeight));
                        //tsAgent.CreateTouchScreenForm(PrimaryUI, PrimaryUI.TSA_GetPlaylistPlayerItem(), this);
                    //}

                    #endregion

                    return true;
				}
				catch (iVisionException ive)
				{
					iVisionLog.ScreenErrorLog(projectName, ive.ErrorCode, ive.DetailMessage);

					logger.Error(String.Format("iVisionException Code : {0}, Description : {1}", ive.ErrorCode, ive.DetailMessage));
					logger.Error(ive.StackTrace);
					StatusOfPlayer = PlayerStatus.Failed;
					return false;
				}
				catch (IOException ioe)
				{
					logger.Error(ioe.Message/* + " " + e.StackTrace*/);

					FileInfo info = new FileInfo(projectName);
					if(info != null)
					{
						if(!info.Exists)
						{
							iVisionLog.ScreenErrorLog(info.Name, ErrorCodes.IS_ERROR_CONTENT_NOT_FOUND, ioe.Message);
						}
						else
						{
							iVisionLog.ScreenErrorLog(info.Name, ErrorCodes.IS_ERROR_CONTENT_CRASHED, ioe.Message);
						}

					}
					logger.Error(ioe.StackTrace);

					StatusOfPlayer = PlayerStatus.Failed;
					return false;
					
				}
				catch (Exception e)
				{
					logger.Error(e.Message/* + " " + e.StackTrace*/);

					FileInfo info = new FileInfo(projectName);
					if(info != null)
					{
						iVisionLog.ScreenErrorLog(info.Name, ErrorCodes.IS_ERROR_INTERNAL, e.Message);
                        iVisionLog.ScreenErrorLog(info.Name, ErrorCodes.IS_ERROR_INTERNAL, e.StackTrace);
					}

					logger.Error(e.StackTrace);

					StatusOfPlayer = PlayerStatus.Failed;
				}

				return false;
            }

			return true;
        }

        private void UpdateRss()
        {
            

        }

		DateTime _subtitleStarted = DateTime.Today;
		string _sSubtitleName = String.Empty;
        public bool setSubtitle(String projectName)
        {
			if (!is_visible_player)
				return true;

			if (!this.Dispatcher.CheckAccess())
			{
				try
				{
					lock (lockSubtitle)
                    {

						DispatcherOperation o = this.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
						   new loadProjectCB(setSubtitle), projectName);

						o.Wait();

						return o.Result == null ? false : (bool)o.Result;
					}
				}
				catch (Exception e)
				{
					logger.Error(e + "");
					return false;
				}
			}
			else
			{
                Window oldSubtitles = null;

				try
				{


                    if (String.IsNullOrEmpty(projectName))
                    {
                        CloseSubtitle(ref popSubtiles);
                        return true;
                    }
					FileInfo fi = new FileInfo(projectName);
					if (!fi.Exists) return false;

					_sSubtitleName = fi.Name;

                    bool bIsTransparent = cfg.IsTransparentToSubtitle;

                    oldSubtitles = popSubtiles;

                    popSubtiles = new Window();
                    popSubtiles.ShowInTaskbar = false;
                    popSubtiles.Topmost = true;
                    popSubtiles.Owner = this;
                    popSubtiles.ResizeMode = System.Windows.ResizeMode.NoResize;
                    popSubtiles.WindowState = System.Windows.WindowState.Normal;
                    popSubtiles.WindowStyle = System.Windows.WindowStyle.None;
                    
                    //popSubtiles.AllowsTransparency = true;
                    popSubtiles.AllowsTransparency = bIsTransparent;
                    
                    // 					popSubtiles.Placement = PlacementMode.AbsolutePoint;
                    
                    Canvas cvPopup = new Canvas();
					if (bIsTransparent == false)
					{
						String color = cfg.SubBackcolor;
						popSubtiles.Background = cvPopup.Background = new SolidColorBrush(System.Windows.Media.Color.FromRgb(Convert.ToByte(color.Substring(1, 2), 16), Convert.ToByte(color.Substring(3, 2), 16), Convert.ToByte(color.Substring(5, 2), 16)));
					}
					else
					{
						popSubtiles.Background = cvPopup.Background = System.Windows.Media.Brushes.Transparent;
					}
                    popSubtiles.Content = cvPopup;

                    FlowDocument fdoc = null;
                    using (TextReader tr = new StreamReader(projectName))
                    {
                        fdoc = FlowDocumentManager.DeserializeFlowDocument(tr.ReadToEnd());
                        tr.Close();
                        tr.Dispose();
                    }

                    double length = 0;
                    double fontheight = 0;

                    foreach (Block block in fdoc.Blocks)
                    {
                        foreach (Inline _line in block.ContentStart.Paragraph.Inlines)
                        {
                            TextRange flowDocSelection = new TextRange(_line.ContentStart, _line.ContentEnd);
                            FormattedText ft_text = new FormattedText(flowDocSelection.Text, System.Globalization.CultureInfo.CurrentCulture, System.Windows.FlowDirection.LeftToRight, new Typeface(_line.FontFamily, _line.FontStyle, _line.FontWeight, _line.FontStretch), _line.FontSize, _line.Foreground);

							length += ft_text.WidthIncludingTrailingWhitespace;
                            if (fontheight < ft_text.Height) fontheight = ft_text.Height;
                        }
                    }
                    RssLine st = new RssLine();
                    length += 100;
                    if (fdoc.Name == "RSS")
                    {

                        TextRange flowDocSelection = new TextRange(fdoc.Blocks.FirstOrDefault().ContentStart.Paragraph.Inlines.FirstOrDefault().ContentStart, fdoc.Blocks.FirstOrDefault().ContentStart.Paragraph.Inlines.FirstOrDefault().ContentEnd);
                        st.FeedUrl = flowDocSelection.Text;
                        st.Speed = Convert.ToInt32(cfg.SubSpeed);
                        st.Margin = new Thickness(20);
                        st.Direction = ScrollTextDirection.Flip;
                        
                        ((ITextContainer)st).SetFontFamily(fdoc.FontFamily);
                        ((ITextContainer)st).SetFontBrush(fdoc.Foreground);
                        ((ITextContainer)st).SetFontSize(fdoc.FontSize);
                        ((ITextContainer)st).SetFontWeight(fdoc.FontWeight);
                        //((ITextContainer)st).SetFontStyle(fdoc.FontStyle);
                        st.Background = System.Windows.Media.Brushes.Transparent;
                        st.BorderThickness = new Thickness(0);

                        popSubtiles.Width = this.ActualWidth;
                        st.Width = this.ActualWidth-40;
                        popSubtiles.Height = fontheight * 1.5;
                        st.Height = fontheight * 1.2;
                        int canvasTop = Convert.ToInt32(popSubtiles.Height - st.Height);
                        Canvas.SetTop(st, canvasTop / 2);
                        st.VerticalAlignment = VerticalAlignment.Center;
                        st.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
                        
                        cvPopup.Children.Add(st);
                        st.Start(new TimeSpan(0, 5, 0));
                    }else{
                        

                        System.Windows.Controls.RichTextBox rtbText = new System.Windows.Controls.RichTextBox();
					    rtbText.Document = fdoc;
                    
                    
                        rtbText.Background = System.Windows.Media.Brushes.Transparent;
                        rtbText.BorderThickness = new Thickness(0);

                        popSubtiles.Width = this.ActualWidth;
                        rtbText.Width = length;
                        popSubtiles.Height = fontheight * 1.5;
                        rtbText.Height = fontheight * 1.2;
                        double canvasTop = popSubtiles.Height - rtbText.Height;
                        Canvas.SetTop(rtbText, canvasTop / 2);
                        rtbText.VerticalAlignment = VerticalAlignment.Center;
                        rtbText.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
                    
                        cvPopup.Children.Add(rtbText);


                        DoubleAnimation animation = new DoubleAnimation();

                        animation.Duration = new Duration(CalculateDurationFromSpeed(length, (int)cfg.SubSpeed));

                        animation.SpeedRatio = 1.0;
                        animation.RepeatBehavior = new RepeatBehavior(1);
                        animation.By = 200.0;
                        animation.From = this.ActualWidth + 10;
                        animation.To = -length - 10;
					    animation.Completed += new EventHandler(animation_Completed);
                        rtbText.BeginAnimation(Canvas.LeftProperty, animation);
                    }
                    double dVerticalOffset = this.ActualHeight - popSubtiles.Height;

                    if (cfg.IsManualToSubtitle)
                    {
                        dVerticalOffset = (double)cfg.SubPositionY;
                    }
                    
                    double dLeft = this.Left < 0 ? 0 : this.Left;
                    double dTop = this.Top < 0 ? 0 : this.Top;
                    popSubtiles.Left = dLeft;
                    popSubtiles.Top = dVerticalOffset + dTop;
                    popSubtiles.Show();

                    logger.Debug(String.Format("Main Actual Size:{0}, Size:{1}, Popup Actual Size:{2}, Size:{3}",this.ActualWidth, this.Width, popSubtiles.ActualWidth, popSubtiles.Width));
                    if (popSubtiles.Width != this.ActualWidth)
                    {
                        popSubtiles.Width = this.ActualWidth;
                        logger.Debug("Subtitle Popup Actual size = " + this.ActualWidth);
                    }

					_subtitleStarted = DateTime.UtcNow;
//					iVisionLog.SubtitleStartLog(projectName, /*Hsshin Event 정의*/false);

                    CloseSubtitle(ref oldSubtitles);
                  
				}
				catch (iVisionException ive)
				{
					iVisionLog.SubtitleErrorLog(_sSubtitleName, ive.ErrorCode, ive.DetailMessage);

                    CloseSubtitle(ref popSubtiles);
                    if (oldSubtitles != null)
                        popSubtiles = oldSubtitles;

					return false;
				}
				catch (Exception e)
				{
					iVisionLog.SubtitleErrorLog(_sSubtitleName, ErrorCodes.IS_ERROR_INTERNAL, e.Message);
                    
                    CloseSubtitle(ref popSubtiles);
                    if (oldSubtitles != null)
                        popSubtiles = oldSubtitles;
                    
                    return false;
				}

			}
			return true;
        }

        /// <summary>
        /// 자막 창 닫기
        /// </summary>
        /// <param name="oldSubtitles"></param>
        void CloseSubtitle(ref Window oldSubtitles)
        {
            try
            {
                if (oldSubtitles != null)
                {
                    try
                    {
                        oldSubtitles.Close();
                        oldSubtitles = null;
                    }
                    catch
                    {
                        oldSubtitles = null;
                    }

                    oldSubtitles = null;
                }
            }
            catch { oldSubtitles = null; }
        }

		void animation_Completed(object sender, EventArgs e)
		{
			try
			{
				if(iVisionLog.SubtitleFinishied != null)
				{
					iVisionLog.SubtitleFinishied(_sSubtitleName, TimeConverter.ConvertToUTP(_subtitleStarted.ToUniversalTime()), TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()));
				}
			}
			catch { }
			if (tm != null)
			{
				tm.PlayNextSubtitle();
			}
		}

		TimeSpan CalculateDurationFromSpeed(double length, int speed)
		{
			TimeSpan ts = new TimeSpan(0, 0, 0, 0, (int)(((length + this.ActualWidth) / speed) * 1000));
			return ts;
		}

        public void SetStatusText(string text)
        {
            /*try
            {
                if (!this.Dispatcher.CheckAccess())
                {
                    this.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                       new loadProjectCB(SetStatusText), text );
                }
                else
                {
                    status.Content = text;
                }
            }
            catch (Exception e)
            {
                logger.Error(e + "");
            }*/
        }
        #endregion

        #region Event handlers
        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.Cursor.Hide();
//             subsTextUI.Margin = new Thickness(0, 20, 0, 60);
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            is_started = false;
            
            
			unregisterService();

            System.Windows.Forms.Cursor.Show();

            if (popSubtiles != null)
            {
                try
                {
                    popSubtiles.Close();
                }
                catch { }
                popSubtiles = null;
            }

			if (PrimaryUI != null)
			{
				PrimaryUI.Stop();
			}
			if (BackUI != null)
			{
				BackUI.Stop();
			}
			
//             if (udpListener != null)
//             {
//                 udpListener.Stop();
//                 udpListener = null;
//             }
            if (tm != null)
            {
                tm.Stop();
                tm = null;
            }

            if (Research_File != null)
            {
                try
                {
                    Research_File.Abort();
                }
                catch { }
                finally { Research_File = null; }

            }

			if(primaryBuffer != null)
			{
				primaryBuffer = null;
			}

			if (backBuffer != null)
			{
				backBuffer = null;
			}

			if (_mutex != null)
				_mutex.ReleaseMutex();

            if (eventListener != null)
            {
                eventListener.Stop();
            }
			logger.Info("Stoped Event Listener");

			logger.Info("Ending Player...");
        }

        #endregion

        #region Touch

        /// <summary>
        /// Touch Screen로 부터 Page 변경 이벤트가 발생시 호출된다.
        /// </summary>
        /// <param name="paneScreenID"></param>
        /// <param name="paneScreenView"></param>
        private void ChangedTouchPageDelegateFunc(string paneScreenID, object paneScreenView, bool isLinked, bool isScreenRepeat)
        {
            try
            {
                PrimaryUI.TSA_PlaylistStart(paneScreenID, paneScreenView, isLinked, isScreenRepeat);
            }
            catch (Exception ex)
            {
            }
        }

        /// <summary>
        /// Non Touch Screen으로부터 Page 변경 이벤트가 발생시 호출된다.
        /// </summary>
        /// <param name="paneScreenID"></param>
        /// <param name="playlistPlayer"></param>
        private void ChangedNonTouchScreenDelegateFunc(string paneScreenID, object playlistPlayer)
        {
            //tsAgent.DisplayTSView(paneScreenID, playlistPlayer);
        }

        private void ChangedTouchPage(UIElement changedPage, UIElement playerScreen)
        {
            try
            {
                PrimaryUI.TSA_PlaylistStart(changedPage, playerScreen);
            }
            catch (Exception ex)
            {
            }
        }

        private void SetPlayStateTouchProject(bool isTouchPlayState, Grid PlaylistPlayer)
        {
			try
			{
				TimerLoop timerLoop = TimerLoop.GetInstance;
                if (timerLoop.CurrentScreen != null)
                {
                    if (timerLoop.CurrentScreen.type != TaskCommands.CmdProgram)
                    {
                        //PrimaryUI.SetPlayStateTouchProject(isTouchPlayState);
                        TimerLoop.IsPauseDefaultProgram = isTouchPlayState;
                    }
                    else// 시간스케줄일 경우 강제 false 설정
                    {
                        TimerLoop.IsPauseDefaultProgram = false;
                    }
                }
                else
                {
                    TimerLoop.IsPauseDefaultProgram = isTouchPlayState;
                }
			}
			catch (Exception ex)
			{
                logger.Debug(ex.Message);
			}
        }

        private void RaisedUserTouch()
        {
            try
            {
				TimerLoop timerLoop = TimerLoop.GetInstance;
				if (timerLoop.CurrentScreen.type != TaskCommands.CmdProgram)
				{
					TimerLoop tm = TimerLoop.GetInstance;
					tm.PlayTouchRollingTask();
				}
				else// 시간스케줄일 경우 강제 false 설정
				{
					TimerLoop.IsPauseDefaultProgram = false;
				}
            }
            catch (Exception ex)
            {
            }
        }
        #endregion

        protected override void OnRenderSizeChanged(SizeChangedInfo sizeInfo)
        {
            base.OnRenderSizeChanged(sizeInfo);
         
        }
    }
}