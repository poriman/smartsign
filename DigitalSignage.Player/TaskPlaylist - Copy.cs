﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DigitalSignage.PlayAgent;
using System.Collections.ObjectModel;

namespace DigitalSignage.Player
{
	class TaskPlaylist
	{
		Collection<Task> tasks = new Collection<Task>();
		Task currtask = null;
		bool bStarted = false;

		#region Properties
		public int Count
		{
			get{
				return tasks.Count;
			}

		}

		public Task First
		{
			get{
				bStarted = true;

				if (tasks.Count > 0)
					return currtask = tasks[0];
				else 
				{
					bStarted = false;
					return null;
				}
			}
		}

		public Task Next
		{
			get{
				bStarted = true;

				if (currtask == null && tasks.Count > 0)
					return currtask = tasks[0];
				else if (currtask == null && tasks.Count == 0)
				{
					bStarted = false;
					return null;
				}
				Task comp = null;
				for (int i = 0; i < tasks.Count; ++i)
				{
					if (tasks[i].uuid == currtask.uuid)
					{
						try
						{
							comp = tasks[i+1];
							break;
						}
						catch
						{
							comp = tasks[0];
						}
					}
				}

				return currtask = comp;
			}
		}
		public Task Current
		{
			get{
				return currtask;
			}
		}
		public Task Previous
		{
			get
			{
				bStarted = true;

				if (currtask == null && tasks.Count > 0)
					return currtask = tasks[0];
				else if (currtask == null && tasks.Count == 0)
				{
					bStarted = false;
					return null;
				}
				Task comp = null;
				for (int i = 0; i < tasks.Count; ++i)
				{
					if (tasks[i].uuid == currtask.uuid)
					{
						try
						{
							comp = tasks[i - 1];
							break;
						}
						catch
						{
							comp = tasks[tasks.Count - 1];
						}
					}
				}

				return comp;
			}
		}

		public bool IsStarted
		{
			get {
				return bStarted; 
			}
			set {
				bStarted = value;
			}
		}
		#endregion

		#region Functions
	
		/// <summary>
		/// 작업을 추가한다
		/// </summary>
		/// <param name="task"></param>
		public void AddTask(Task task)
		{
			//	이미 있는 작업이라면 삭제를 해주고
			for (int i = tasks.Count - 1; i >= 0; i--)
			{
				if (tasks[i].uuid == task.uuid)
				{
					tasks.RemoveAt(i);
					break;
				}
			}

			bool bAdd = false;
			for(int i = 0 ; i < tasks.Count; ++i)
			{
				if(tasks[i].priority <= task.priority)
				{
					tasks.Insert(i, task);
					bAdd = true;
					break;
				}
			}

			//	추가한다.
			if (!bAdd)
				tasks.Add(task);
		}
	
		/// <summary>
		/// 작업을 삭제한다
		/// </summary>
		/// <param name="task"></param>
		public void RemoveTask(Task task)
		{
			for(int i = tasks.Count - 1 ; i >= 0 ; i--)
			{
				if(tasks[i].uuid == task.uuid)
				{
					tasks.RemoveAt(i);
					break;
				}
			}
		}

		/// <summary>
		/// Task를 넣는다
		/// </summary>
		/// <param name="task">넣을 Task</param>
		public void Push(Task task)
		{
			AddTask(task);
		}

		/// <summary>
		/// Task를 뺀다
		/// </summary>
		/// <returns>빠진 Task</returns>
		public Task Pop()
		{
			try
			{
				Task popTask = tasks[0];
				tasks.RemoveAt(0);
				return popTask;
			}
			catch(Exception e)
			{
				return null;
			}
		}
		#endregion
	}
}
