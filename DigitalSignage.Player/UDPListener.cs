﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using NLog;

namespace DigitalSignage.Player
{
    class UDPListener
    {
        private bool process = false;
        private Thread udpThread;
        private int udpPort = 1889;
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private Config cfg = Config.GetConfig;

        public UDPListener()
        {
            try
            {
                //Starting the UDP Server thread.
                process = true;
                udpThread = new Thread(new ThreadStart(Start));
                udpThread.Priority = ThreadPriority.BelowNormal;
                udpThread.Start();
            }
            catch (Exception e)
            {
                logger.Error(e + "");
                udpThread.Abort();
            }
        }

        public void Start()
        {
            //IPHostEntry localHostEntry;
            try
            {
                int recv = 0;
                //Create a UDP socket.
                Socket soUdp = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
                IPEndPoint localIpEndPoint = new IPEndPoint(0, udpPort);
                soUdp.Bind(localIpEndPoint);
                soUdp.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReceiveTimeout, 2000);
                while (process)
                {
                    Byte[] received = new Byte[256];
                    IPEndPoint tmpIpEndPoint = new IPEndPoint(0, udpPort);
                    EndPoint remoteEP = (tmpIpEndPoint);
                    try
                    {
                        recv = soUdp.ReceiveFrom(received, ref remoteEP);
                        String dataReceived = System.Text.Encoding.ASCII.GetString(received, 0, recv);
                        logger.Info("Received " + dataReceived);
                        if (dataReceived.Equals("DigitalSignageEchoRequest"))
                        {
                            String returningString = "DigitalSignagePlayer," + ((cfg.Version >> 48) & 0xFFFF) + "." +
                                ((cfg.Version >> 32) & 0xFFFF) + "." +
                                ((cfg.Version >> 16) & 0xFFFF) + "." + (cfg.Version & 0xFFFF) + "," + 
                                System.Windows.Forms.SystemInformation.ComputerName;
                            Byte[] returningByte = System.Text.Encoding.ASCII.GetBytes(returningString.ToCharArray());
                            logger.Info("Send answer to " + remoteEP.ToString());
                            soUdp.SendTo(returningByte, remoteEP);
                        }
                    }
                    catch (Exception e)
                    {
                        //logger.Info(e + "");
                    }
                }
            }
            catch (Exception e)
            {
                logger.Error(e + "");
            }
        }

        public void Stop()
        {
            process = false;
            udpThread.Abort();
        }
    }
}
