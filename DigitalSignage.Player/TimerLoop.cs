﻿using System;
using System.Collections;
using System.Runtime.InteropServices;
using System.Threading;
using System.Collections.ObjectModel;

using DigitalSignage.Common;
// using DigitalSignage.DataBase;

using NLog;

using DigitalSignage.PlayAgent;
using SerialPortController;
using System.Collections.Generic;
using DigitalSignage.PlayerSync;
using System.Diagnostics;
using System.IO;

namespace DigitalSignage.Player
{
    class TimerLoop
	{
		#region Properties

        /// <summary>
        /// 동기화 오브젝트
        /// </summary>
        private UdpSync _syncObject = null;

        private object _lockSync = new object();

        /// <summary>
        /// 싱크 스케줄 갱신 이력
        /// </summary>
        private String dtSyncTask = DateTime.MinValue.ToString("yyyyMMddHHmmss");
 
		/// <summary>
		/// DB Logging Helper
		/// </summary>
		private iVisionLogHelper iVisionLog = iVisionLogHelper.GetInstance;

		/// <summary>
		/// Serial Port Control Instance
		/// </summary>
		ISerialPortControl _serialPortObject = null;

        ArduinoSensor _sensorObject = null;

		/// <summary>
		/// 로컬 로깅 Instance
		/// </summary>
        private static Logger logger = LogManager.GetCurrentClassLogger();
		/// <summary>
		/// Configration Instance
		/// </summary>
		private Config config = null;

		#region ETRI 재생/중지 기능
		/// <summary>
		/// 재생 상태
		/// </summary>
		public enum PlayStatus
		{
			/// <summary>
			/// 정지 중
			/// </summary>
			Stopped,
			/// <summary>
			/// 재생 중
			/// </summary>
			Played,
			/// <summary>
			/// 중지 중
			/// </summary>
			Paused
		};

		/// <summary>
		/// 재생 상태
		/// </summary>
		public static PlayStatus PlayerStatus = PlayStatus.Stopped;

		#endregion

        /// <summary>
        /// 현재 연결되어 재생중인 USB 드라이브
        /// </summary>
        public String CurrentRemovalDrive = String.Empty;

        /// <summary>
        /// USB 작업을 추가한다.
        /// </summary>
        /// <returns></returns>
        public bool AddUsbTask()
        {
            PlayAgent.Task t = new PlayAgent.Task();
            t.description = "USB PLAY";
            t.duration = 3600;
            t.type = TaskCommands.CmdUsbSchedule;

            _usbScreenPlaylist.AddTask(t);

            return true;
        }

        /// <summary>
        /// USB 재생 여부
        /// </summary>
        public bool IsUSBPlay = false;

        #region GS 프로모션 기능

        /// <summary>
        /// 프로모션 Piece
        /// </summary>
        class Promotion
        {
            public long ScreenID = -1;
            public PlayAgent.Task PromotionTask = null;

            public Promotion(long screen_id, PlayAgent.Task t)
            {
                ScreenID = screen_id;
                PromotionTask = t;
            }
        }

        /// <summary>
        /// 프로모션 이벤트 큐
        /// </summary>
        private Collection<Promotion> queuePromotion = new Collection<Promotion>();

        /// <summary>
        /// 선택된 쿠폰 이벤트
        /// </summary>
        Promotion selectedCouponPromotion = null;
        /// <summary>
        /// 선택된 응모 이벤트
        /// </summary>
        Promotion selectedEntriesPromotion = null;

        /// <summary>
        /// 응모 프로모션 등록
        /// </summary>
        /// <param name="cell_phone"></param>
        /// <param name="reserved"></param>
        /// <returns></returns>
        public bool RegisterEntriesPromotion(string cell_phone, string reserved)
        {
            if (selectedEntriesPromotion != null)
            {
                return Config.GetConfig.WritePromotionInfo(selectedEntriesPromotion.ScreenID, cell_phone, reserved);
            }
            else logger.Error("저장된 응모 프로모션이 없습니다.");

            return false;
        }

        /// <summary>
        /// 쿠폰 프로모션 등록
        /// </summary>
        /// <param name="cell_phone"></param>
        /// <param name="reserved"></param>
        /// <returns></returns>
        public bool RegisterCouponPromotion(string cell_phone, string reserved)
        {
            if (selectedCouponPromotion != null)
            {
                return Config.GetConfig.WritePromotionInfo(selectedCouponPromotion.ScreenID, cell_phone, reserved);
            }
            else logger.Error("저장된 쿠폰 프로모션이 없습니다.");

            return false;
        }

        /// <summary>
        /// 쿠폰 이벤트 존재 여부
        /// </summary>
        public bool HasCouponPromotion
        {
            get { return selectedCouponPromotion != null; }
        }
        /// <summary>
        /// 응모 이벤트 존재 여부
        /// </summary>
        public bool HasEntriesPromotion
        {
            get { return selectedEntriesPromotion != null; }
        }
        /// <summary>
        /// 프로모션 스케줄 수집 여부
        /// </summary>
        private bool _bGatheringPromotionTasks = false;

        /// <summary>
        /// 프로모션 스케줄 수집 여부
        /// </summary>
        public bool GatheringPromotionTasks
        {
            get { return _bGatheringPromotionTasks; }
            set
            {
                bool bOld = _bGatheringPromotionTasks;
                _bGatheringPromotionTasks = value;

                ///  수집을 완료하고 프로모션 존재 여부를 생성한다.
                if (bOld == true && value == false)
                {
                    PickUpPromotion(false); 
                }
                else if(value == true)
                {
                    ///  초기화
                    selectedCouponPromotion = null;
                    selectedEntriesPromotion = null;
                    queuePromotion.Clear();
                }
            }
        }

        /// <summary>
        /// 프로모션 정보를 추출한다.
        /// </summary>
        /// <param name="bPickUpFirst">첫 정보를 추출할것인지 마지막 정보를 추출할 것인지</param>
        private void PickUpPromotion(bool bPickUpFirst)
        {
            selectedCouponPromotion = null;
            selectedEntriesPromotion = null;

            foreach (Promotion p in queuePromotion)
            {
                if (!p.PromotionTask.isAd) continue;

                if (p.PromotionTask.MetaTags.Contains("응모"))
                {
                    if (!bPickUpFirst) selectedEntriesPromotion = p;
                    else if (selectedEntriesPromotion == null) selectedEntriesPromotion = p;
                }
                else if (p.PromotionTask.MetaTags.Contains("쿠폰"))
                {
                    if (!bPickUpFirst) selectedCouponPromotion = p;
                    else if (selectedCouponPromotion == null) selectedCouponPromotion = p;
                }
            }
        }

		/// <summary>
		/// 긴급 쿠폰 스케줄 적용
		/// </summary>
		public void UrgentCouponPromotions()
		{
			try
			{
				if (selectedCouponPromotion != null)
				{
					_urgentScreenPlaylist.RemoveAllTasks();
					_urgentScreenPlaylist.AddTask(selectedCouponPromotion.PromotionTask);

					logger.Debug("Adding Urgent Coupon schedule: " + selectedCouponPromotion.ScreenID);
					LimitUrgentScreenCount = 1;
				}
			}
			catch { }
		}

		/// <summary>
		/// 긴급 응모 스케줄 적용
		/// </summary>
		public void UrgentEntriesPromotions()
		{
			try
			{
				if (selectedEntriesPromotion != null)
				{
					_urgentScreenPlaylist.RemoveAllTasks();
					_urgentScreenPlaylist.AddTask(selectedEntriesPromotion.PromotionTask);

					logger.Debug("Adding Urgent Entries schedule: " + selectedEntriesPromotion.ScreenID);
					LimitUrgentScreenCount = 1;
				}
			}
			catch { }
		}

        /// <summary>
        /// 긴급 스케줄 재생 종료를 위한 제한 설정 (-1인 경우 제한 없음) 
        /// </summary>
        public int LimitUrgentScreenCount = 0;


        #endregion

        #region Lock Object
        /// <summary>
		/// Lock 오브젝트
		/// </summary>
		private static readonly object semaphore = new object();
		/// <summary>
		/// Screen Lock 오브젝트
		/// </summary>
		private readonly object lockScreen = new object();
		#endregion

		/// <summary>
		/// 플레이어의 Visible 상태
		/// </summary>
		public bool IsPlayerVisible
		{
			get { return parent.IsPlayerVisible; }
        }

        public MainWindow GetParent
        {
            get { return this.parent; }
        }

		bool bIsUrgent = false;
		/// <summary>
		/// 긴급 이벤트인지 
		/// </summary>
		public bool IsUrgentEvent
		{
			get { return bIsUrgent; }
			private set { bIsUrgent = value; }
		}
        private long lEventID = -1;
        /// <summary>
        /// 이벤트 발생 시 이벤트 ID
        /// </summary>
        public long EventID
        {
            get { return lEventID; }
            set { lEventID = value; }
        }


        #region Touch - 반복 스케줄 진행 제한 
        private static bool isPauseDefaultProgram;
        public static bool IsPauseDefaultProgram 
        {
            get { return isPauseDefaultProgram; }
            set { isPauseDefaultProgram = value; 
                if (value == false) 
                    waitDefaultProgramHandler.Set(); 
            }
        }
        public static EventWaitHandle waitDefaultProgramHandler = new AutoResetEvent(false);       
        #endregion
       
        private static TimerLoop instance = null;
		
		public static TimerLoop GetInstance
		{
			get
			{
				lock (semaphore)
				{
					if (instance == null)
					{
						instance = new TimerLoop();
					}
					return instance;
				}
			}
		}

		#region 스케줄 재생 List 

        /// <summary>
        /// USB 스케줄 관리 List
        /// </summary>
        private TaskPlaylist _usbScreenPlaylist = new TaskPlaylist();
		/// <summary>
		/// 기본 반복 스케줄 재생 List
		/// </summary>
		private TaskPlaylist _defaultScreenPlaylist = new TaskPlaylist();
		/// <summary>
		/// 시간 스케줄 재생 List
		/// </summary>
		private TaskPlaylist _timeScreenPlaylist = new TaskPlaylist();
		/// <summary>
		/// 긴급 스케줄 재생 List
		/// </summary>
		private TaskPlaylist _urgentScreenPlaylist = new TaskPlaylist();
		/// <summary>
		/// 자막 스케줄 재생 List
		/// </summary>
		private TaskPlaylist _subtitlePlaylist = new TaskPlaylist();
		/// <summary>
		/// PC 볼륨 컨트롤 재생 List
		/// </summary>
		private TaskPlaylist _volumeControlPlaylist = new TaskPlaylist();
		/// <summary>
		/// Serial 볼륨 컨트롤 재생 List
		/// </summary>
		private TaskPlaylist _serialvolumeControlPlaylist = new TaskPlaylist();
		#endregion

		#region 스케줄 관리 List
		/// <summary>
		/// 시간 스케줄 관리 List
		/// </summary>
		private TaskPlaylist _managedtimeScreenPlaylist = new TaskPlaylist();
		/// <summary>
		/// 자막 스케줄 관리 List
		/// </summary>
		private TaskPlaylist _managedsubtitlePlaylist = new TaskPlaylist();
		/// <summary>
		/// 제어 스케줄 관리 List
		/// </summary>
		private TaskPlaylist _controlPlaylist = new TaskPlaylist();
		#endregion

		private bool processing_flag;
        private MainWindow parent;
		private DigitalSignage.PlayAgent.Task currentVolumeControl = null;
		private DigitalSignage.PlayAgent.Task currentSerialVolumeControl = null;
		private DigitalSignage.PlayAgent.Task currentScreen = null;
		private DigitalSignage.PlayAgent.Task prevScreen = null;
		private DigitalSignage.PlayAgent.Task currentSub = null;
		private DigitalSignage.PlayAgent.Task previewSub = null;		//	이전 서브타이틀
		private Thread control = null;

		//	hsshin
		private Thread thDefaultScreenPlayer = null;

        private Thread thSensorModule = null;

		public bool IsPlaying
		{
			get { return processing_flag; }
		}

		
		/// <summary>
		/// 현재 적용 중인 PC 볼륨 스케줄
		/// </summary>
		public DigitalSignage.PlayAgent.Task CurrentVolumeControl
		{
			get { return currentVolumeControl; }
		}

		/// <summary>
		/// 현재 적용중인 Serial 볼륨 스케줄
		/// </summary>
		public DigitalSignage.PlayAgent.Task CurrentSerialVolumeControl
		{
			get { return currentSerialVolumeControl; }
		}
		/// <summary>
		/// 현재 재생 중인 스크린 스케줄
		/// </summary>
		public DigitalSignage.PlayAgent.Task CurrentScreen
		{
			get { return currentScreen; }
		}
		/// <summary>
		/// 현재 자막 스케줄
		/// </summary>
		public DigitalSignage.PlayAgent.Task CurrentSubtitle
		{
			get { return currentSub; }
		}

		/// <summary>
		/// Log Db Adapter
		/// </summary>
		private DigitalSignage.ClientDataBase.LogsDataSetTableAdapters.client_logsTableAdapter client_logsTA = null;

		#endregion


		/// <summary>
		/// TimerLoop /시작
		/// </summary>
		/// <param name="parent"></param>
		/// <returns></returns>
		public int Start( MainWindow parent )
        {
            this.parent = parent;
            config = Config.GetConfig;

			iVisionLog.GeneralLog = WriteInfo;
			iVisionLog.GeneralLogError = WriteError;

			iVisionLog.ScreenLogStarted = ScreenStarted;
			iVisionLog.ScreenLogEnded = ScreenEnded;
			iVisionLog.ScreenFinishied = ScreenProcceed;
			iVisionLog.ScreenLogError = ScreenErrorOccurred;
			
			iVisionLog.SubtitleLogStarted = SubtitleStarted;
			iVisionLog.SubtitleLogEnded = SubtitleEnded;
			iVisionLog.SubtitleFinishied = SubtitleProcceed;
			iVisionLog.SubtitleLogError = SubtitleErrorOccurred;

            processing_flag = true;

			#region 시리얼 포트 연결
			try
			{
				SerialPortControlFactory factory = new SerialPortControlFactory();
				_serialPortObject = factory.CreateController(config.SerialPortDevice);
				if (_serialPortObject != null) _serialPortObject.Initialize(config.ComPort);
			}
			catch (System.Exception e)
			{
				logger.Error(e.ToString());
			}
			#endregion

            #region 센서 연결

            thSensorModule = new Thread(this.procSensor);
            thSensorModule.Start();


            #endregion

            #region 동기화 오브젝트 생성

            try
            {
                _syncObject = UdpSync.GetInstance;
                _syncObject.AddSyncServer(config.PlayerId, Properties.Settings.Default.SyncPort);
                _syncObject.OnStartSchedule += new StartScheduleEventHandler(_syncObject_OnStartSchedule);
            }
            catch (Exception ex) { logger.Error(ex.ToString()); }
            #endregion

            //starting control thread
			control = new Thread(this.controlThread);
			control.Priority = ThreadPriority.Highest;
            control.Start();

            IsPauseDefaultProgram = false;
            return 0;
        }

        bool IsMonitorOn = false;
        DateTime dtAccessedTime = DateTime.Now;
        void procSensor()
        {
            bool bUseSensor = config.UseSensor;
            int nMonitorOffLimitTS = config.SensorLimitTS;

            logger.Info("감지 센서 체크 모듈 시작!");
            logger.Info("감지 센서 사용 여부 ({0})", bUseSensor);
            logger.Info("감지 센서 비접근 시 모니터 OFF 기준 초 ({0})", nMonitorOffLimitTS);

            int nCntCheckUseSensor = 0;
            dtAccessedTime = DateTime.Now;

            while (processing_flag)
            {
                if (bUseSensor)
                {
                    DateTime dtNow = DateTime.Now;

                    if (_sensorObject == null)
                    {
                        _sensorObject = new ArduinoSensor();
                        logger.Debug("감지센서 모듈 생성: COMPort({0}), BaudRate ({1}), Sensor Value ({2})", config.SensorPort, 9600, config.SensorValue);
                        if (_sensorObject.Initialize(config.SensorPort, 9600, config.SensorValue))
                        {
                            _sensorObject.Start();
                            logger.Debug("감지센서 모듈 시작됨");
                        }
                        else
                            _sensorObject = null;
                    }

                    if (_sensorObject != null && _sensorObject.Accessed)
                    {
                        dtAccessedTime = dtNow;
                        try
                        {
                            String[] arrTempIDs = config.SensorTempIDs.Split('|');

                            if (arrTempIDs.Length > 0)
                            {
                                DateTime dtLocalNow = DateTime.Now;
                                String sStartDT = dtLocalNow.ToString("yyyyMMddHHmmss");
                                String sEndDT = (dtLocalNow + TimeSpan.FromSeconds(nMonitorOffLimitTS)).ToString("yyyyMMddHHmmss");
                                foreach (string sTempID in arrTempIDs)
                                {
                                    #region 이벤트 신호 발생
                                    //config.localEventsList.InsertOrReplaceSignal(7, Convert.ToInt32(sTempID), config.PlayerId, sStartDT, sEndDT);
                                    logger.Debug("감지센서 이벤트 발생: {0}, {1}, {2}, {3}, {4}", 7, Convert.ToInt32(sTempID), config.PlayerId, sStartDT, sEndDT);
                                    #endregion
                                }
                            }
                            Thread.Sleep(2000);

                        }
                        catch { }

                    }

                }

                if (!bUseSensor && ++nCntCheckUseSensor > 600)
                {
                    nCntCheckUseSensor = 0;
                    /// 설정 파일에서 감지센서 사용 여부를 체크한다.
                    bUseSensor = config.UseSensor;
                    nMonitorOffLimitTS = config.SensorLimitTS;

                    if (bUseSensor)
                    {
                        logger.Info("감지 센서 사용 여부 ({0})", bUseSensor);
                        logger.Info("감지 센서 비접근 시 모니터 OFF 기준 초 ({0})", nMonitorOffLimitTS);
                    }

                }

                Thread.Sleep(100);
            }
        }

        /*
        /// <summary>
        /// 시간 동기화 로직 수행
        /// </summary>
        void procTimeSych()
        {
            int tp = Config.GetConfig.TimeSyncType;
            int nCount = 0;

            logger.Info("시간 동기화 스레드 시작.");
                        
            while (processing_flag)
            {
                try
                {
                    /// 한시간 마다 TimeSyncType 갱신하기
                    if (nCount++ > 360000)
                    {
                        nCount = 0;
                        tp = Config.GetConfig.TimeSyncType;

                        try
                        {
                            GC.Collect();
                        }
                        catch { }

                    }

                    DateTime dtLocal = DateTime.Now;
                    if (dtLocal.Second == 0)
                    {
                        /// 동기화 로직 수행하기
                        if ((tp & (int)TimeSyn.EveryHour_0) > 0 && dtLocal.Minute == 0)
                        {
                            logger.Info("매시 00분 시간 동기화 발생!!");
                            FirstScreen();
                            Thread.Sleep(60000);
                            continue;
                        }

                        if ((tp & (int)TimeSyn.EveryHour_10) > 0 && dtLocal.Minute == 10)
                        {
                            logger.Info("매시 10분 시간 동기화 발생!!");
                            FirstScreen();
                            Thread.Sleep(60000);
                            continue;
                        }

                        if ((tp & (int)TimeSyn.EveryHour_20) > 0 && dtLocal.Minute == 20)
                        {
                            logger.Info("매시 20분 시간 동기화 발생!!");
                            FirstScreen();
                            Thread.Sleep(60000);
                            continue;
                        }

                        if ((tp & (int)TimeSyn.EveryHour_30) > 0 && dtLocal.Minute == 30)
                        {
                            logger.Info("매시 30분 시간 동기화 발생!!");
                            FirstScreen();
                            Thread.Sleep(60000);
                            continue;
                        }

                        if ((tp & (int)TimeSyn.EveryHour_40) > 0 && dtLocal.Minute == 40)
                        {
                            logger.Info("매시 40분 시간 동기화 발생!!");
                            FirstScreen();
                            Thread.Sleep(60000);
                            continue;
                        }

                        if ((tp & (int)TimeSyn.EveryHour_50) > 0 && dtLocal.Minute == 50)
                        {
                            logger.Info("매시 50분 시간 동기화 발생!!");
                            FirstScreen();
                            Thread.Sleep(60000);
                            continue;
                        }
                    }
                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message);
                }

                Thread.Sleep(10);
            }
            logger.Info("시간 동기화 스레드 종료.");

        }
         */

		/// <summary>
		/// TimerLoop를 정지시킨다
		/// </summary>
		/// <returns>reserved</returns>
        public int Stop()
        {
            processing_flag = false;

			if (thDefaultScreenPlayer != null && thDefaultScreenPlayer.IsAlive)
			{
				thDefaultScreenPlayer.Abort();
				thDefaultScreenPlayer = null;
			}


            if (thSensorModule != null && thSensorModule.IsAlive)
            {
                thSensorModule.Abort();
                thSensorModule = null;
            }

            if (_sensorObject != null)
            {
                try
                {
                    _sensorObject.Stop();
                }
                catch { }
                _sensorObject.Uninitialize();
            }

            try
            {
                if (_serialPortObject != null)
                {
                    _serialPortObject.Uninitialize();
                }
            }
            catch { }

            return 0;
        }

		/// <summary>
		/// 스크린 스케줄을 수행할 스레드를 생성한다.
		/// </summary>
		private void defaultScreenTimer(object param)
		{
			try
			{
 				long duration = Convert.ToInt64(param);

                #region 스케줄 시작시간을 통한 다음 스케줄 재생 대기

//                double startingdelayseconds = TimeSpan.FromMilliseconds(ScreenStartInfoInSchedule.GetInstance.PauseInStarting).TotalSeconds + 0.10;

                while ((DateTime.Now - ScreenStartInfoInSchedule.GetInstance.ScheduleStartDT).TotalSeconds + 0.20/*startingdelayseconds*/ < duration)
                {
                    Thread.Sleep(10);
                }

                GetParent.LoadingVisible(true);

                try
                {
                    /// 슬레이브인 경우 Ending Delay 만큼 대기
                    if (ScreenStartInfoInSchedule.GetInstance.IsSyncSlave)
                    {
                        ///  스크린 멈춤 이벤트 전달 
                        ScreenStartInfoInSchedule.GetInstance.OnScreenPaused(null);
                        Thread.Sleep(Properties.Settings.Default.PauseEndingDelayInSyncSlave);
                    }
                }
                catch { }


                #endregion

                lock (_lockSync)
                {
                    SelectedAdPlaying = false;

                    logger.Debug("시작 시간과 종료 시간 비교에 따른 스케줄 변경: {0} - {1}초", ScreenStartInfoInSchedule.GetInstance.ScheduleStartDT, (DateTime.Now - ScreenStartInfoInSchedule.GetInstance.ScheduleStartDT).TotalSeconds);

                    /*
                    //0.2초 보정시간을 주자 - 뺐음
                    Thread.Sleep((int)(duration * 1000) - 100);
                    */
                    lock (lockScreen)
                    {
                        #region Touch - Program의 스케줄 시간 종료시 (Sleep으로 스케줄 시간까지대기) Touch 실행 플래그가 true이면 대기.
                        waitDefaultProgramHandler.WaitOne();
                        #endregion

                        ///	현재 재생 상태가 Pause이면 다음 스크린으로 넘어가지 않고 대기한다.
                        while (PlayerStatus == PlayStatus.Paused)
                        {
                            Thread.Sleep(100);
                        }

                        //	hsshin 반복기능을 시간스케줄도 같이 쓰도록 구성
                        if (IsUSBPlay)
                        {
                            PlayNextUSBScreenTask();
                        }
                        else if (IsUrgentEvent)
                        {
                            /// 긴급 스케줄 실행
                            PlayNextUrgentScreenTask();
                        }
                        else if (currentScreen != null && (currentScreen.type == TaskCommands.CmdDefaultProgram || currentScreen.type == TaskCommands.CmdTouchDefaultProgram))
                        {
                            PlayNextDefaultScreenTask();
                        }
                        else if (currentScreen != null && (currentScreen.type == TaskCommands.CmdProgram))
                        {
                            PlayNextTimeScreenTask();
                        }
                        //else if (currentScreen != null && (currentScreen.type == TaskCommands.CmdUrgentProgram))
                        //{
                        //    PlayNextUrgertScreenTask();
                        //}
                        else
                            logger.Error("Current screen is not 'Default' task.");
                    }
                }

			}
			catch (ThreadAbortException /*thae*/)
			{
				logger.Error("Thread Abort");
			}
			catch (Exception ex)
			{
				logger.Error("Exception: " + ex.ToString());
			}
		}

		/// <summary>
		/// 1초마다 Task루틴을 수행할 스레드를 생성한다.
		/// </summary>
        private void controlThread()
        {
			logger.Info("Starting Timeloop thread...");
            
			try
			{
				//	볼륨을 예전 볼륨으로 되돌린다.
                RestoreEventVolume();
				RestoreVolume();
				RestoreSerialVolume();
			}
			catch { }

			config.localTasksList.ResetRunningTasks();

            ///싱크 스케줄 삭제
            config.localTasksList.DeleteSyncTaskList();

			logger.Info("Running tasks are updated to 'Wait' state.");

            ControlScheduleHelper.GetInstance.OnChangeStatus += new ChangeStatusEventHandler(ControlScheduleHelper_OnChangeStatus);

            bool bRunOnce = false;
            TimeSpan tsTerm = TimeSpan.FromSeconds(20);
            DateTime dtStarted = DateTime.Now;

            Thread.Sleep(Properties.Settings.Default.StartTaskDelayOnFirst);

			while (processing_flag)
            {
                //starting task starter thread
//                 Thread ts = new Thread(this.runTasks);
//                 ts.Priority = ThreadPriority.BelowNormal;
//                 ts.Start();
				runTasks();

                Thread.Sleep(500);
            }

			logger.Info("Ending Timeloop thread...");
        }

        /// <summary>
        /// Touch
        /// </summary>
        public void PlayTouchRollingTask()
        {				
            for (int i = 0; i < _defaultScreenPlaylist.Count; i++)
            {
                DigitalSignage.PlayAgent.Task next = _defaultScreenPlaylist.Next;
                if (next == null)
                    continue;
                
                if (next.type == TaskCommands.CmdTouchDefaultProgram)
                {
                    if (PlayScreenTask(next) == true)
                    {
                        RegisterNextScreen(next.duration);
                        break;
                    }
                }
            }            
        
        }

		/// <summary>
		/// 데이터베이스 상에 State 가 Processed 또는 Processing이면 변경한다. 왜냐하면 
		/// PlayAgent에서 State를 변경했을 경우 공교롭게 엎어쓸수도 있기때문이다. 
		/// </summary>
		/// <param name="willChange"></param>
		/// <param name="uuid"></param>
		void ChangeTaskState(TaskState willChange, Guid uuid)
		{
			try
			{
				TaskState state = (TaskState)config.localTasksList.GetTaskState(uuid);
				logger.Debug(String.Format("Current DB State = {0}, Will Change = {1}, uuid = {2}",state, willChange, uuid));

				if (state == TaskState.StateProcessed || state == TaskState.StateProcessing)
				{
					config.localTasksList.ChangeTaskState(willChange, uuid);
				}
				else
				{
					logger.Info("TaskState는 변경 되지 않음. 서버에서 메시지가 변경됨.");
				}
			}
			catch (System.Exception e)
			{
				logger.Error(e.Message);
			}
		}

		public object SerialPortDeviceStatus(SerialPortStatus status)
		{
			if (_serialPortObject == null || !_serialPortObject.IsOpen)
			{
				return null;
			}

			return _serialPortObject.Status(status);
		}

        public ISerialPortControl GetSerialPortObject()
        {
            if (_serialPortObject == null || !_serialPortObject.IsOpen)
            {
                return null;
            }

            return _serialPortObject;
        }

		#region 반복 스케줄 처리
		/// <summary>
		/// 반복 스케줄 처리
		/// </summary>
		/// <param name="task">해당 Task</param>
		private void RollingTask(DigitalSignage.PlayAgent.Task task)
		{
			if (task.state == TaskState.StateWait)
			{
				logger.Info("Task " + task.ToString());

				logger.Info(String.Format("[CmdDefaultProgram][{0}] StateWait", task.description));

				if (task.TaskEnd >= TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()))
				{
					_defaultScreenPlaylist.AddTask(task);
					ChangeTaskState(TaskState.StateRunning, task.uuid);
					//	아무것도 플레이 중이 아니면 재생해준다.
					if (currentScreen == null/* && false == _defaultScreenPlaylist.IsStarted*/)
					{
						PlayNextDefaultScreenTask();
                        //waitCurrentScreenPlayHandler.WaitOne();
                    }
                    #region Touch : 처음 Player 실행시 Task 리스트의 모든 Task를 실행하여 주석 처리 함 - 확인 필요.
                    /*
                    else if (currentScreen != null && currentScreen.uuid == task.uuid)
                    {
                        PlayNextDefaultScreenTask();
                    }
                    */
                    #endregion
                    return ;
				}
				else
				{
					//	이미 시간이 지났는데 wait로 들어온 경우라면
					//	사용자가 Edit Task를 통해 변경한 경우이다.
					//	재생 지연 목록에서 요청된 Task를 제거해주고,
					//	현재 플레이중이라면 종료를 해준 후 다음 스크린을 재생한다.
					logger.Info("[" + task.description + " (" + task.uuid + ")] End time is wrong. It will be canceled.");
					ChangeTaskState(TaskState.StateProcessed, task.uuid);

					_defaultScreenPlaylist.RemoveTask(task);
					iVisionLog.GeneralLogError(task.description, task.uuid.ToString(), task.type, ErrorCodes.IS_ERROR_INVALID_TIME_SCOPE, "This schedule is alreay ended.");

					if (currentScreen != null && currentScreen.uuid == task.uuid)
					{
						PlayNextDefaultScreenTask();
					}
				}

			}
			else if (task.state == TaskState.StateEdit)
			{
				logger.Info("Task " + task.ToString());

				logger.Info(String.Format("[CmdDefaultProgram][{0}] StateEdit.", task.description));

				//	적용중인 Task가 있다면 지워준다 
				_defaultScreenPlaylist.RemoveTask(task);

				//	만약 현재 재생하고 있다면!!
				if (currentScreen != null && currentScreen.uuid == task.uuid)
				{
					// 다음 Task를 재생하자
					PlayNextDefaultScreenTask();
				}
				if (!IsDownloadComplete(task.duuid.ToString()))
				{
					logger.Info(String.Format("[CmdDefaultProgram][{0}] state has been modified as 'Download'", task.description));
					ChangeTaskState(TaskState.StateDownload, task.uuid);
				}
				else
					ChangeTaskState(TaskState.StateWait, task.uuid);


			}
			else if (task.state == TaskState.StateRunning)
			{
				if (task.TaskEnd <= TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()))
				{
					logger.Info("Task " + task.ToString());

					//	스케줄 종료시간 보다 현재 시간이 더 크다면
					logger.Info(String.Format("[CmdDefaultProgram][{0}] StateRunning.", task.description));

					_defaultScreenPlaylist.RemoveTask(task);
					ChangeTaskState(TaskState.StateProcessed, task.uuid);

					if (currentScreen != null && currentScreen.uuid == task.uuid)
					{
						PlayNextDefaultScreenTask();
					}
				}
// 				else
// 					ChangeTaskState(TaskState.StateRunning, task.uuid);

				return ;

			}
			else if (task.state == TaskState.StateCanceled)
			{
				logger.Info("Task " + task.ToString());

				logger.Info(String.Format("[CmdDefaultProgram][{0}] StateCanceled.", task.description));

				_defaultScreenPlaylist.RemoveTask(task);
				ChangeTaskState(TaskState.StateProcessed, task.uuid);

				if (currentScreen != null && currentScreen.uuid == task.uuid)
				{
					PlayNextDefaultScreenTask();
				}

			}
			else
			{
				logger.Error("[CmdDefaultProgram] Unknown TaskState : " + task.state.ToString());
			}

			return ;
		}
		#endregion

		#region 제어 스케줄 처리

		/// <summary>
		/// 프로그램 스크린의 재생/종료을 담당한다.
		/// </summary>
		private void RunControlTask()
		{
			Collection<DigitalSignage.PlayAgent.Task> _arrWillRemove = new Collection<DigitalSignage.PlayAgent.Task>();

			DigitalSignage.PlayAgent.Task next = _controlPlaylist.First;

			//	현재 스케줄이 제어 스케줄이고 시간대가 표출 끝났다면
			if ((currentVolumeControl != null) && currentVolumeControl.type == TaskCommands.CmdControlSchedule)
			{
				if (!(CheckContainTimeofDay(currentVolumeControl.starttimeofday, currentVolumeControl.endtimeofday)
					&& CheckDayOfWeek(currentVolumeControl.daysofweek)))
				{
					logger.Info(String.Format("[CmdControlSchedule][{0}] 볼륨 스케줄 종료.", currentVolumeControl.description));
					currentVolumeControl = null;
				}
			}

			if ((currentSerialVolumeControl != null) && currentSerialVolumeControl.type == TaskCommands.CmdControlSchedule)
			{
				if (!(CheckContainTimeofDay(currentSerialVolumeControl.starttimeofday, currentSerialVolumeControl.endtimeofday)
					&& CheckDayOfWeek(currentSerialVolumeControl.daysofweek)))
				{
					logger.Info(String.Format("[CmdControlSchedule][{0}] 볼륨 스케줄 종료.", currentSerialVolumeControl.description));
					currentSerialVolumeControl = null;
				}
			}

            int nCount = 0;

			do
			{
				if (next == null) continue;

				//	1. 일일 내 시간 검사
				//	2. 요일 검사
				if (CheckContainTimeofDay(next.starttimeofday, next.endtimeofday) && CheckDayOfWeek(next.daysofweek))
				{
					//	현재 시간과 비교하여 하루 시간 대와 요일에 맞는다면..

					TaskControlSchedules command = ControlScheduleHelper.GetInstance.GetKindOfCommand(next.description);
					switch (command)
					{
						case TaskControlSchedules.CtrlVolume:	//	볼륨인 경우 적용 범위(START-END)가 존재한다.

							if ((currentVolumeControl != null) && (currentVolumeControl.uuid.Equals(next.uuid))) continue;

							if ((currentVolumeControl != null) &&
								(currentVolumeControl.type == TaskCommands.CmdControlSchedule) &&
								next.priority >= currentVolumeControl.priority)
							{
								// 재생 중인 스크린이 프로그램 스크린이고 우선순위가 재생 중인 스크린보다 높다면
								if (PlayControlSchedule(next))
								{
									logger.Info(String.Format("[CmdControlSchedule][{0}] 제어 스케줄 시작.", next.description));
								}
							}
							else if (currentVolumeControl == null)
							{
								// 재생 중인 스크린이 없거나 프로그램 스크린이 아닌경우

								if (PlayControlSchedule(next))
								{
									logger.Info(String.Format("[CmdControlSchedule][{0}] 제어 스케줄 시작.", next.description));
								}
							}
							break;
						case TaskControlSchedules.CtrlSerialVolume:	//	볼륨인 경우 적용 범위(START-END)가 존재한다.

							if ((currentSerialVolumeControl != null) && (currentSerialVolumeControl.uuid.Equals(next.uuid))) continue;

							if ((currentSerialVolumeControl != null) &&
								(currentSerialVolumeControl.type == TaskCommands.CmdControlSchedule) &&
								next.priority >= currentSerialVolumeControl.priority)
							{
								// 재생 중인 스크린이 프로그램 스크린이고 우선순위가 재생 중인 스크린보다 높다면
								if (PlayControlSchedule(next))
								{
									logger.Info(String.Format("[CmdControlSchedule][{0}] 제어 스케줄 시작.", next.description));
								}
							}
							else if (currentSerialVolumeControl == null)
							{
								// 재생 중인 스크린이 없거나 프로그램 스크린이 아닌경우

								if (PlayControlSchedule(next))
								{
									logger.Info(String.Format("[CmdControlSchedule][{0}] 제어 스케줄 시작.", next.description));
								}
							}
							break;
						default:
							if (PlayControlSchedule(next))
							{
								logger.Info(String.Format("[CmdControlSchedule][{0}] 제어 스케줄 시작.", next.description));
								if(next.starttimeofday == next.endtimeofday)
								{
									// 반복 패턴이 없는 경우
									_arrWillRemove.Add(next);
								}
							}

							break;
					}

				}

                if (nCount++ > _controlPlaylist.Count)
                {
                    next = null;
                    break;
                }

			}
			while ((next = _controlPlaylist.NextToEnd) != null);

			if (currentVolumeControl == null)
			{
				RestoreVolume();
			}
			if (currentSerialVolumeControl == null)
			{
				RestoreSerialVolume();
			}

			try
			{
				//	처리된 제어 스케줄을 제거한다. 
				foreach (DigitalSignage.PlayAgent.Task t in _arrWillRemove)
				{
					_controlPlaylist.RemoveTask(t);
				}
			}
			catch {}

		}

		/// <summary>
		/// 제어 스케줄 처리
		/// </summary>
		/// <param name="task"></param>
		private void ControlTask(DigitalSignage.PlayAgent.Task task)
		{
			if (task.state == TaskState.StateWait)
			{
				if (task.TaskEnd > TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()))
				{
					//	현재 시간보다 스케줄 종료 시간이 더 크다면

					_controlPlaylist.Push(task);

					logger.Info("Task " + task.ToString());

					logger.Info(String.Format("[CmdControlSchedule][{0}] StateWait.", task.description));

					ChangeTaskState(TaskState.StateRunning, task.uuid);
				}
				else
				{

					logger.Info("[" + task.description + " (" + task.uuid + ")] End time is wrong. It will be canceled.");
					ChangeTaskState(TaskState.StateProcessed, task.uuid);

					_controlPlaylist.RemoveTask(task);

					TaskControlSchedules command = ControlScheduleHelper.GetInstance.GetKindOfCommand(task.description);
					switch (command)
					{
						case TaskControlSchedules.CtrlVolume:	//	볼륨인 경우 적용 범위(START-END)가 존재한다.
							_volumeControlPlaylist.RemoveTask(task);

							if (currentVolumeControl != null && currentVolumeControl.uuid == task.uuid)
							{
								currentVolumeControl = null;
							}

							break;
						case TaskControlSchedules.CtrlSerialVolume:	//	볼륨인 경우 적용 범위(START-END)가 존재한다.
							_serialvolumeControlPlaylist.RemoveTask(task);

							if (currentSerialVolumeControl != null && currentSerialVolumeControl.uuid == task.uuid)
							{
								currentSerialVolumeControl = null;
							}

							break;
					}

					iVisionLog.GeneralLogError(task.description, task.uuid.ToString(), task.type, ErrorCodes.IS_ERROR_INVALID_TIME_SCOPE, "This schedule is already ended.");
				}
			}
			else if (task.state == TaskState.StateEdit)
			{
				logger.Info("Task " + task.ToString());
				logger.Info(String.Format("[CmdControlSchedule][{0}] StateEdit.", task.description));

				_controlPlaylist.RemoveTask(task);

				TaskControlSchedules command = ControlScheduleHelper.GetInstance.GetKindOfCommand(task.description);
				switch (command)
				{
					case TaskControlSchedules.CtrlVolume:	//	볼륨인 경우 적용 범위(START-END)가 존재한다.
						//	적용중인 Task가 있다면 지워준다 
						_volumeControlPlaylist.RemoveTask(task);

						//	만약 현재 재생하고 있다면!!
						if (currentVolumeControl != null && currentVolumeControl.uuid == task.uuid)
						{
// 							// 다음 Task를 재생하자
							currentVolumeControl = null;
						}
						break;
					case TaskControlSchedules.CtrlSerialVolume:	//	볼륨인 경우 적용 범위(START-END)가 존재한다.
						//	적용중인 Task가 있다면 지워준다 
						_serialvolumeControlPlaylist.RemoveTask(task);

						//	만약 현재 재생하고 있다면!!
						if (currentSerialVolumeControl != null && currentSerialVolumeControl.uuid == task.uuid)
						{
							// 							// 다음 Task를 재생하자
							currentSerialVolumeControl = null;
						}
						break;
				}

				ChangeTaskState(TaskState.StateWait, task.uuid);
			}
			else if (task.state == TaskState.StateRunning)
			{
				if (task.TaskEnd <= TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()))
				{
					//	스케줄 종료시간 보다 현재 시간이 더 크다면

					logger.Info("[" + task.description + " (" + task.uuid + ")] will be ended.");
					ChangeTaskState(TaskState.StateProcessed, task.uuid);
					_controlPlaylist.RemoveTask(task);

					TaskControlSchedules command = ControlScheduleHelper.GetInstance.GetKindOfCommand(task.description);

					switch (command)
					{
						case TaskControlSchedules.CtrlVolume:	//	볼륨인 경우 적용 범위(START-END)가 존재한다.
							_volumeControlPlaylist.RemoveTask(task);

							if ((currentVolumeControl != null) && (task.uuid.Equals(currentVolumeControl.uuid)))
							{
								currentVolumeControl = null;
							}
							break;
						case TaskControlSchedules.CtrlSerialVolume:	//	볼륨인 경우 적용 범위(START-END)가 존재한다.
							//	적용중인 Task가 있다면 지워준다 
							_serialvolumeControlPlaylist.RemoveTask(task);

							//	만약 현재 재생하고 있다면!!
							if (currentSerialVolumeControl != null && currentSerialVolumeControl.uuid == task.uuid)
							{
								// 							// 다음 Task를 재생하자
								currentSerialVolumeControl = null;
							}
							break;
						default:
							break;
					}
				}
				else
				{
					//	현재 시간보다 스케줄 종료 시간이 더 크다면

					ChangeTaskState(TaskState.StateRunning, task.uuid);

				}

			}
			else if (task.state == TaskState.StateCanceled)
			{
				ChangeTaskState(TaskState.StateProcessed, task.uuid);
				logger.Info("Changing status to [StateCanceled]. (UUID=" + task.uuid + ")");

				_controlPlaylist.RemoveTask(task);

				TaskControlSchedules command = ControlScheduleHelper.GetInstance.GetKindOfCommand(task.description);

				switch (command)
				{
					case TaskControlSchedules.CtrlVolume:	//	볼륨인 경우 적용 범위(START-END)가 존재한다.
						_volumeControlPlaylist.RemoveTask(task);

						if ((currentVolumeControl != null) && (task.uuid.Equals(currentVolumeControl.uuid)))
						{
							currentVolumeControl = null;
						}
						break;
					case TaskControlSchedules.CtrlSerialVolume:	//	볼륨인 경우 적용 범위(START-END)가 존재한다.
						//	적용중인 Task가 있다면 지워준다 
						_serialvolumeControlPlaylist.RemoveTask(task);

						//	만약 현재 재생하고 있다면!!
						if (currentSerialVolumeControl != null && currentSerialVolumeControl.uuid == task.uuid)
						{
							// 							// 다음 Task를 재생하자
							currentSerialVolumeControl = null;
						}
						break;
					default:
						break;
				}

			}
			else
			{
				logger.Error("[CmdControlSchedule] Unknown TaskState : " + task.state.ToString());
			}
		}
		#endregion

		#region 긴급 프로그램 스케줄 처리

		/// <summary>
		/// 긴급 프로그램 스크린의 재생을 담당한다.
		/// </summary>
		private void RunUrgentProgramTask()
		{
            if (IsUSBPlay) return;

			if (_urgentScreenPlaylist.Count > 0)
			{
				if (IsUrgentEvent == true) return;

				logger.Info(String.Format("[CmdUrgentProgram] 긴급 스크린 시작."));

                IsUrgentEvent = true;
                PlayNextUrgentScreenTask();
			}
		}

		/// <summary>
		/// 긴급 스케줄 처리
		/// </summary>
		/// <param name="task"></param>
		private void UrgentTask(DigitalSignage.PlayAgent.Task task)
		{
			if (task.state == TaskState.StateWait)
			{
				if (task.TaskEnd > TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()))
				{
                    if ((task.show_flag & (int)ShowFlag.Show_Infinity) == 0)
                    {
                        ChangeTaskState(TaskState.StateProcessed, task.uuid);
                    }

					//	현재 시간보다 스케줄 종료 시간이 더 크다면

					_urgentScreenPlaylist.Push(task);

                    logger.Info("Task " + task.ToString());

                    logger.Info(String.Format("[CmdUrgentProgram][{0}] StateWait.", task.description));

                    if (currentScreen != null && currentScreen.type != TaskCommands.CmdUrgentProgram && (currentScreen.type != TaskCommands.CmdUsbSchedule))
                    {
                        PlayNextUrgentScreenTask();
                    }
				}
			}
            else if (task.state == TaskState.StateEdit)
            {
                logger.Info("Task " + task.ToString());

                logger.Info(String.Format("[CmdUrgentProgram][{0}] StateEdit.", task.description));

                //	적용중인 Task가 있다면 지워준다 
                _urgentScreenPlaylist.RemoveTask(task);

                if (currentScreen != null && currentScreen.uuid == task.uuid)
                {
                    PlayNextUrgentScreenTask();
                }

                if (!IsDownloadComplete(task.duuid.ToString()))
                {
                    logger.Info(String.Format("[CmdUrgentProgram][{0}] state has been modified as 'Download'.", task.description));

                    ChangeTaskState(TaskState.StateDownload, task.uuid);
                }
                else
                    ChangeTaskState(TaskState.StateWait, task.uuid);
            }
			else if (task.state == TaskState.StateCanceled)
			{
				ChangeTaskState(TaskState.StateProcessed, task.uuid);
				logger.Info(String.Format("[CmdUrgentProgram][{0}] StateCanceled.", task.description));

				_urgentScreenPlaylist.RemoveTask(task);

				if (currentScreen != null && currentScreen.uuid == task.uuid)
				{
                    PlayNextUrgentScreenTask();
				}
			}
		}
		#endregion

		#region 프로그램 스케줄 처리

		/// <summary>
		/// 프로그램 스크린의 재생/종료을 담당한다.
		/// </summary>
		private void RunProgramTask()
		{
			DigitalSignage.PlayAgent.Task next = _managedtimeScreenPlaylist.First;

            int nCount = 0;
			do
			{

				if (next == null) continue;

				//	1. 일일 내 시간 검사
				//	2. 요일 검사
				if (CheckContainTimeofDay(next.starttimeofday, next.endtimeofday) && CheckDayOfWeek(next.daysofweek))
				{
					//	현재 시간과 비교하여 하루 시간 대와 요일에 맞는다면..

					if (0 < _timeScreenPlaylist.AddTask(next))
					{
						logger.Info(String.Format("[CmdProgram][{0}] 스크린 시작.", next.description));
					}
				}
				else
				{
					if (_timeScreenPlaylist.RemoveTask(next))
					{
						logger.Info(String.Format("[CmdProgram][{0}] 스크린 종료.", next.description));
					}

				}

                if(nCount++ > _managedtimeScreenPlaylist.Count)
                {
                    next = null;
                    break;
                }
			}
			while ((next = _managedtimeScreenPlaylist.NextToEnd) != null);

			if (_timeScreenPlaylist.Count > 0 &&
				(currentScreen == null || (currentScreen.type == TaskCommands.CmdDefaultProgram || currentScreen.type == TaskCommands.CmdTouchDefaultProgram)))
			{
				PlayNextTimeScreenTask();
			}
		}

		/// <summary>
		/// 프로그램 스케줄 처리
		/// </summary>
		/// <param name="task"></param>
		/// <param name="tasks"></param>
		void ProgramTask(DigitalSignage.PlayAgent.Task task, DigitalSignage.PlayAgent.Task[] tasks)
		{
			if (task.state == TaskState.StateWait)
			{
				if (task.TaskEnd > TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()))
				{
					//	현재 시간보다 스케줄 종료 시간이 더 크다면

					_managedtimeScreenPlaylist.Push(task);

					logger.Info("Task " + task.ToString());

					logger.Info(String.Format("[CmdProgram][{0}] StateWait.", task.description));

					ChangeTaskState(TaskState.StateRunning, task.uuid);

				}
				else
				{
					//	이미 시간이 지났는데 wait로 들어온 경우라면
					//	사용자가 Edit Task를 통해 변경한 경우이다.
					//	재생 지연 목록에서 요청된 Task를 제거해주고,
					//	현재 플레이중이라면 종료를 해준 후 다음 스크린을 재생한다.
					logger.Info("[" + task.description + " (" + task.uuid + ")] End time is wrong. It will be canceled.");
					ChangeTaskState(TaskState.StateProcessed, task.uuid);

					_managedtimeScreenPlaylist.RemoveTask(task);
					_timeScreenPlaylist.RemoveTask(task);

					if (currentScreen != null && currentScreen.uuid == task.uuid)
					{
						PlayNextTimeScreenTask();
					}

					/*
					lock (lockScreen)
					{
						if (currentScreen != null && currentScreen.uuid == task.uuid)
						{
							currentScreen = null;
						}
					}
					*/
// 									iVisionLog.GeneralLogError(task.description, task.type, ErrorCodes.IS_ERROR_INVALID_TIME_SCOPE, "This schedule is alreay ended.");

				}
			}
			else if (task.state == TaskState.StateEdit)
			{
				logger.Info("Task " + task.ToString());

				logger.Info(String.Format("[CmdProgram][{0}] StateEdit.", task.description));

				//	적용중인 Task가 있다면 지워준다 
				_managedtimeScreenPlaylist.RemoveTask(task);
				_timeScreenPlaylist.RemoveTask(task);

				if (currentScreen != null && currentScreen.uuid == task.uuid)
				{
					PlayNextTimeScreenTask();
				}

				/*
				lock(lockScreen)
				{
					if (currentScreen != null && currentScreen.uuid == task.uuid)
					{
						currentScreen = null;
					}
				}
				*/

				if (!IsDownloadComplete(task.duuid.ToString()))
				{
					logger.Info(String.Format("[CmdProgram][{0}] state has been modified as 'Download'.", task.description));

					ChangeTaskState(TaskState.StateDownload, task.uuid);
				}
				else
					ChangeTaskState(TaskState.StateWait, task.uuid);
			}
			else if (task.state == TaskState.StateRunning)
			{
				if (task.TaskEnd <= TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()))
				{
					//	스케줄 종료시간 보다 현재 시간이 더 크다면
					logger.Info("[" + task.description + " (" + task.uuid + ")] will be ended.");

					ChangeTaskState(TaskState.StateProcessed, task.uuid);

					_managedtimeScreenPlaylist.RemoveTask(task);
					_timeScreenPlaylist.RemoveTask(task);

					if (currentScreen != null && currentScreen.uuid == task.uuid)
					{
						PlayNextTimeScreenTask();
					}

					/*
					lock (lockScreen)
					{
						if (currentScreen != null && currentScreen.uuid == task.uuid)
						{
							currentScreen = null;
						}
					}
					*/
				}
				else
				{
					//	현재 시간보다 스케줄 종료 시간이 더 크다면
					ChangeTaskState(TaskState.StateRunning, task.uuid);
				}


			}
			else if (task.state == TaskState.StateCanceled)
			{
				logger.Info("Task " + task.ToString());

				logger.Info(String.Format("[CmdProgram][{0}] StateCanceled.", task.description));

				ChangeTaskState(TaskState.StateProcessed, task.uuid);
				_managedtimeScreenPlaylist.RemoveTask(task);
				_timeScreenPlaylist.RemoveTask(task);

				if (currentScreen != null && currentScreen.uuid == task.uuid)
				{
					PlayNextTimeScreenTask();
				}

				/*
				lock (lockScreen)
				{
					if (currentScreen != null && currentScreen.uuid == task.uuid)
					{
						currentScreen = null;
					}
				}
				*/
			}
		}
		#endregion

		#region 자막 스케줄 처리

		/// <summary>
		/// 자막 스케줄의 재생/종료을 담당한다.
		/// </summary>
		private void RunSubtitleTask()
		{
			DigitalSignage.PlayAgent.Task next = _managedsubtitlePlaylist.First;

            int nCount = 0;

			do
			{

				if (next == null) continue;

				//	1. 일일 내 시간 검사
				//	2. 요일 검사
				if (CheckContainTimeofDay(next.starttimeofday, next.endtimeofday) && CheckDayOfWeek(next.daysofweek))
				{
					//	현재 시간과 비교하여 하루 시간 대와 요일에 맞는다면..

					if (0 < _subtitlePlaylist.AddTask(next))
					{
						logger.Info(String.Format("[CmdSubtitles][{0}] 자막 시작.", next.description));
					}
				}
				else
				{
					if (_subtitlePlaylist.RemoveTask(next))
					{
						logger.Info(String.Format("[CmdSubtitles][{0}] 자막 종료.", next.description));
					}

				}

                if (nCount++ > _managedsubtitlePlaylist.Count)
                {
                    next = null;
                    break;
                }

			}
			while ((next = _managedsubtitlePlaylist.NextToEnd) != null);

			if (currentSub == null)
			{
				PlayNextSubtitle();
			}
		}

		private void SubtitleTask(DigitalSignage.PlayAgent.Task task)
		{
			if (task.state == TaskState.StateWait)
			{
				if (task.TaskEnd > TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()))
				{
					//	현재 시간보다 스케줄 종료 시간이 더 크다면

					_managedsubtitlePlaylist.AddTask(task);
					
					logger.Info("Task " + task.ToString());

					logger.Info(String.Format("[CmdSubtitles][{0}] StateWait.", task.description));

					ChangeTaskState(TaskState.StateRunning, task.uuid);
				}
				else
				{
					//	이미 시간이 지났는데 wait로 들어온 경우라면
					//	사용자가 Edit Task를 통해 변경한 경우이다.
					//	재생 지연 목록에서 요청된 Task를 제거해주고,
					//	현재 플레이중이라면 종료를 해준 후 다음 스크린을 재생한다.
					logger.Info("[" + task.description + " (" + task.uuid + ")] End time is wrong. It will be canceled.");
					ChangeTaskState(TaskState.StateProcessed, task.uuid);

					_managedsubtitlePlaylist.RemoveTask(task);
					_subtitlePlaylist.RemoveTask(task);

					if (currentSub != null && currentSub.uuid == task.uuid)
					{
						PlayNextSubtitle();
					}
				}
			}
			else if (task.state == TaskState.StateEdit)
			{
				logger.Info("Task " + task.ToString());

				logger.Info(String.Format("[CmdSubtitles][{0}] StateEdit.", task.description));

				//	적용중인 Task가 있다면 지워준다 
				_managedsubtitlePlaylist.RemoveTask(task);
				_subtitlePlaylist.RemoveTask(task);

				if (currentSub != null && currentSub.uuid == task.uuid)
				{
					PlayNextSubtitle();
				}

				if (!IsDownloadComplete(task.duuid.ToString()))
				{
					logger.Info(String.Format("[CmdSubtitles][{0}] state has been modified as 'Download'.", task.description));

					ChangeTaskState(TaskState.StateDownload, task.uuid);
				}
				else
					ChangeTaskState(TaskState.StateWait, task.uuid);
			}
			if (task.state == TaskState.StateRunning)
			{
				if (task.TaskEnd <= TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()))
				{
					//	스케줄 종료시간 보다 현재 시간이 더 크다면
					logger.Info("[" + task.description + " (" + task.uuid + ")] will be ended.");

					ChangeTaskState(TaskState.StateProcessed, task.uuid);

					_managedsubtitlePlaylist.RemoveTask(task);
					_subtitlePlaylist.RemoveTask(task);

					if (currentSub != null && currentSub.uuid == task.uuid)
					{
						PlayNextSubtitle();
					}
				}
				else
				{
					//	현재 시간보다 스케줄 종료 시간이 더 크다면
					ChangeTaskState(TaskState.StateRunning, task.uuid);

				}

			}
			if (task.state == TaskState.StateCanceled)
			{
				logger.Info("Task " + task.ToString());

				logger.Info(String.Format("[CmdSubtitles][{0}] StateCanceled.", task.description));

				ChangeTaskState(TaskState.StateProcessed, task.uuid);

				_managedsubtitlePlaylist.RemoveTask(task);
				_subtitlePlaylist.RemoveTask(task);

				if (currentSub != null && currentSub.uuid == task.uuid)
				{
					PlayNextSubtitle();
				}

			}
		}
#endregion

        DateTime dtLogging = DateTime.Now;
		/// <summary>
		/// Task 관련 업무를 수행할 스레드 함수. 로컬 DB를 쿼리해 현재 이용가능한 데이터를 처리한다. 
		/// </summary>
        private void runTasks()
        {
            try
            {
                DateTime dtNow = DateTime.Now;
                if (dtLogging + TimeSpan.FromMinutes(10) < dtNow)
                {
                    dtLogging = dtNow;

                    logger.Info("Running 'runTasks'...");
                }
                #region Sync 스케줄 갱신
                try
                {
                    if (config.localTasksList.HasUpdatedSyncTask(dtSyncTask))
                    {
                        logger.Debug("동기화 스케줄 갱신!");
                        ClientDataBase.DataSet.SYNC_TASKSDataTable dt = config.localTasksList.GetSyncTaskList();
                        logger.Debug("동기화 스케줄 갱신됨: 총 {0} 개", dt.Rows.Count);

                        foreach (ClientDataBase.DataSet.SYNC_TASKSRow row in dt.Rows)
                        {
                            if (row.DEL_YN == "N")
                            {
                                try
                                {
                                    if (String.IsNullOrEmpty(row.IP_ADDR)) continue;
                                    /// Sync 클라이언트 생성
                                    _syncObject.AddSyncClient(String.Format("{0}|{1}", row.PLAYERS_PID, row.GUUID), System.Net.IPAddress.Parse(row.IP_ADDR), Properties.Settings.Default.SyncPort);
                                }
                                catch (Exception errSync) { logger.Error(errSync.ToString()); }
                            }
                            else
                            {
                                _syncObject.RemoveSyncClient(String.Format("{0}|{1}", row.PLAYERS_PID, row.GUUID));
                            }

                            if (dtSyncTask.CompareTo(row.EDIT_DT) < 0)
                            {
                                dtSyncTask = row.EDIT_DT;
                            }
                        }
                    }
                }
                catch (Exception eSync) { logger.Error(eSync.ToString()); }
                #endregion

                // reading task list
				DigitalSignage.PlayAgent.Task[] tasks = config.localTasksList.GetActiveTask();
                if (tasks == null)
                {
                    logger.Info("No active tasks");
                    return; // nothing new
                }

                #region 오픈스크린 이벤트 스크린 등록
                if (File.Exists(Config.GetConfig.AppData + @"Smil\openscreen.smil"))
                {
                    try
                    {

                        File.Delete(Config.GetConfig.AppData + @"Smil\openscreen2.smil");
                    }
                    catch { }
                    File.Move(Config.GetConfig.AppData + @"Smil\openscreen.smil", Config.GetConfig.AppData + @"Smil\openscreen2.smil");

                    Smil.Library.Smil smilObject = Smil.Library.Smil.LoadObjectFromSmilFile(Config.GetConfig.AppData + @"Smil\openscreen2.smil");
                    TimeSpan tsDuration = smilObject.TotalDuration();

                    DigitalSignage.PlayAgent.Task t = new PlayAgent.Task();
                    t.duration = (int)tsDuration.TotalSeconds == 0 ? 86400 : (int)tsDuration.TotalSeconds;
                    t.type = TaskCommands.CmdUrgentProgram;
                    t.description = "추천 스크린 재생";
                    t.daysofweek = 127;
                    t.show_flag = 127;
                    IsUrgentEvent = true;

                    if (PlayScreenTask(t, Config.GetConfig.AppData + @"Smil\program.xml"))
                    {
                        RegisterNextScreen(t.duration);
                    }
                    else
                    {
                        PlayNextUrgentScreenTask();
                    }

                }
                #endregion


                for (int i = 0; i < tasks.Length; i++)
                {
                    //running task
                    switch ((TaskCommands)tasks[i].type)
                    {
						case TaskCommands.CmdGetRefData:
							logger.Info("[CmdGetRefData] is started.");
							ChangeTaskState(TaskState.StateDownload, tasks[i].uuid);
							break;
                        case TaskCommands.CmdInitPlayer:
							logger.Info("[CmdInitPlayer] is started.");
							break;

						case TaskCommands.CmdNextDefaultProgram:
							logger.Info("[CmdNextDefaultProgram] is started.");
							{
								NextScreen();
							}
							break;
                        case TaskCommands.CmdTouchDefaultProgram:
						case TaskCommands.CmdDefaultProgram:
							
							RollingTask(tasks[i]);
							break;

						case TaskCommands.CmdUrgentProgram:

							UrgentTask(tasks[i]);
							break;

						case TaskCommands.CmdControlSchedule:
							
							ControlTask(tasks[i]);
							break;

						case TaskCommands.CmdProgram:

							ProgramTask(tasks[i], tasks);
                            break;

						case TaskCommands.CmdSubtitles:

							SubtitleTask(tasks[i]);
							break;

                        case TaskCommands.CmdEcho:
                        case TaskCommands.CmdUndefined:
#if DEBUG
                        logger.Info( "Undefined task runned :" + tasks[i].uuid.ToString());
#endif
                            break;
						case TaskCommands.CmdSynchronizeNow:
							logger.Info("[CmdSynchronizeNow] is started.");
							try
							{
								String[] arrDesc = tasks[i].description.Split('|');
								config.LastSyncFromFile = Convert.ToInt64(arrDesc[1]);
                                config.LastSyncTaskSyncFromFile = DateTime.MinValue.ToString("yyyyMMddHHmmss");

								config.AddSynchronizeTasks(TimeConverter.ConvertToUTP(DateTime.Now.AddDays(7).ToUniversalTime()), 
									TimeConverter.ConvertToUTP(DateTime.Now.AddMonths(-1).ToUniversalTime()));
							}
							catch (System.Exception ex)
							{
								logger.Error(ex.ToString());
							}
							
							//config.LastSyncFromFile = 

                            break;
                        case TaskCommands.CmdClearStorage:
                            {
								logger.Info("[CmdClearStorage] is started.");
								
								Thread ts = new Thread(this.clearStorage);
                                ts.Priority = ThreadPriority.BelowNormal;
                                ts.Start();

                                AddCleanTaskAfter();
                            }
                            break;
						case TaskCommands.CmdReturnStatus:
							{
							}
							break;

                        case TaskCommands.CmdWriteRS232:
                            {
								logger.Info("[CmdWriteRS232] is started.");

								Thread ts = new Thread(delegate()
                                  { 
                                     SendRS232.SendTask( tasks[i].duuid );
                                  });
                               ts.Priority = ThreadPriority.Normal;
                               ts.Start();
			                   ChangeTaskState(TaskState.StateProcessed, tasks[i].uuid);
                            }
                            break;
                        default:
                            logger.Info("Unknown task type");
                            break;
                    }
                }

				//////////////////////////////////////////////////////////////////////////
				// 프로그램 Task 감시
				RunUrgentProgramTask();
				RunProgramTask();
				RunSubtitleTask();
				RunControlTask();

            }
            catch (Exception err)
            {
                logger.Error(err + " "+err.StackTrace);
            }
        }

        public void AddCleanTaskAfter()
        {
            try
            {
                // clean storage
                DigitalSignage.PlayAgent.Task t = new DigitalSignage.PlayAgent.Task();
                t.type = TaskCommands.CmdClearStorage;
                t.uuid = DigitalSignage.PlayAgent.Task.TaskClean;
                t.pid = config.PlayerId;
                t.gid = config.GroupId;
                t.description = "CmdClearStorage";
                t.TaskStart = TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()) + 3600;
                t.TaskEnd = t.TaskStart + 600 * 2;
                if (-1 == config.localTasksList.AddTask(t))
                {
                    ////	추가가 되지않음.
                    config.localTasksList.CleanTask(DigitalSignage.PlayAgent.Task.TaskClean);
                    //	다시 추가 시도
                    config.localTasksList.AddTask(t);
                }

            }
            catch (Exception e)
            {
                logger.Error(e.ToString() + "");
            }
        }

        void ControlScheduleHelper_OnChangeStatus(object Sender, ChangeStatusEventArgs e)
        {
            try
            {
                switch(e.control)
                {
                    case TaskControlSchedules.CtrlPlayerOn:
                        
                        parent.IsPlayerVisible = true;
                        logger.Info("Control Player On");
                        break;
                    case TaskControlSchedules.CtrlPlayerOff:
                        parent.IsPlayerVisible = false;
                        logger.Info("Control Player Off");
                        break;
                }

            }
            catch (System.Exception err)
            {
                logger.Error("Control Player Error!" + err.Message);
            }
        }

		/// <summary>
		/// 기본스케줄 리스트를 생성한다.
		/// </summary>
		private void InitDefaultScheduleList()
		{
			//	스크린 추가

			//	스크린 재생
			if(currentScreen == null)
			{
				PlayScreenTask(_defaultScreenPlaylist.Next);
			}
		}

		/// <summary>
		/// 멈추어진 재생을 시작한다.
		/// </summary>
		public void ResumePlaying()
		{
			//	default Screen Player thread
			try
			{
				PlayerStatus = PlayStatus.Played;

				PlayScreenTask(currentScreen);

				Thread temp = null;
				if (thDefaultScreenPlayer != null && thDefaultScreenPlayer.IsAlive)
					temp = thDefaultScreenPlayer;

				thDefaultScreenPlayer = new Thread(this.defaultScreenTimer);
				thDefaultScreenPlayer.Priority = ThreadPriority.BelowNormal;
				thDefaultScreenPlayer.Start(currentScreen.duration);

				if (temp != null) temp.Abort();

			}
			catch
			{

			}
		}

		/// <summary>
		/// 긴급 스케줄 재생을 멈춘다.
		/// </summary>
		public void CancelUrgentEvent()
		{
			if (!this.IsUrgentEvent) return;

            _urgentScreenPlaylist.RemoveAllTasks();
            this.IsUrgentEvent = false;
            this.LimitUrgentScreenCount = 0;

            this.NextScreen();

		}

		/// <summary>
		/// 재생을 멈춘다.
		/// </summary>
		public void PausePlaying()
		{
			try
			{
				PlayerStatus = PlayStatus.Paused;

				if (thDefaultScreenPlayer != null && thDefaultScreenPlayer.IsAlive)
				{
					thDefaultScreenPlayer.Abort();
				}

                #region 재생 시작 전 작업
                ScreenStartInfoInSchedule s = DigitalSignage.Common.ScreenStartInfoInSchedule.GetInstance;
                s.DownloadID = String.Empty;
                s.GroupScheduleID = String.Empty;
                s.ScheduleID = String.Empty;
                s.ScheduleName = String.Empty;
                s.ScheduleDuration = 0;
                s.ScheduleStartDT = s.ScreenStartDT = DateTime.Now;
                s.ShowFlag = 0;
                #endregion


				parent.loadProject("");
			}
			catch { }

			thDefaultScreenPlayer = null;

		}

		/// <summary>
		/// 지정된 시간에 DefaultScreen을 교체해주기 위한 Task
		/// </summary>
		/// <param name="duration">지속 시간(초)</param>
		private void RegisterNextScreen(long duration)
		{
			//	default Screen Player thread
			try
			{
				Thread temp = null;
				if (thDefaultScreenPlayer != null && thDefaultScreenPlayer.IsAlive)
					temp = thDefaultScreenPlayer;

				thDefaultScreenPlayer = new Thread(this.defaultScreenTimer);
				thDefaultScreenPlayer.Priority = ThreadPriority.BelowNormal;
				thDefaultScreenPlayer.Start(duration);
				
				if (temp != null) temp.Abort();

			}
			catch
			{
			}
				

/* *
			DigitalSignage.PlayAgent.Task t = new DigitalSignage.PlayAgent.Task();
			t.pid = config.PlayerId;
			t.gid = config.GroupId;
			t.type = TaskCommands.CmdNextDefaultProgram;
			t.uuid = DigitalSignage.PlayAgent.Task.TaskDefaultScreen;
			t.TaskStart = TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()) + duration - 1;
			t.description = "CmdNextDefaultProgram";
			t.duration = duration;

			InsertOrReplaceTask(t);
**/
		}

        Timer timer = null;

		/// <summary>
		/// 이전 기본 스케줄을 재생한다.
		/// </summary>
		public void PlayPrevDefaultScreenTask()
		{
			#region Touch  - IsPauseDefaultProgram 플래그 에 따라 DefaultProgram 대기 여부 결정
			if (timer == null)
			{
				timer = new Timer(delegate(object o)
				{

					if (IsPauseDefaultProgram == false/* || this.currentScreen == null*/)
						waitDefaultProgramHandler.Set();
				}, null, 0, 10);
			}
			waitDefaultProgramHandler.WaitOne();

			#endregion

			DigitalSignage.PlayAgent.Task prev = null;
			
			int nRetCount = 0;
			do
			{
				prev = _defaultScreenPlaylist.Previous;
			}
			while (false == PlayScreenTask(prev) && nRetCount++ <= _defaultScreenPlaylist.Count);

			if (prev != null)
				RegisterNextScreen(prev.duration);
		}

		/// <summary>
		/// 다음 기본 스케줄을 재생한다
		/// </summary>
		public void PlayNextDefaultScreenTask()
        {
            if (IsUrgentEvent) return;

            logger.Debug("Next Default Screen Task!!");

			#region Touch  - IsPauseDefaultProgram 플래그 에 따라 DefaultProgram 대기 여부 결정
			if (timer == null)
			{
				timer = new Timer(delegate(object o)
				{

                    if (IsPauseDefaultProgram == false/* || this.currentScreen == null*/)
                    {
                        waitDefaultProgramHandler.Set();
                    }

					/*	터치 호환성 테스트 필요 
					try
					{
						if (IsPauseDefaultProgram == false && 
							(currentScreen == null || currentScreen.type != TaskCommands.CmdTouchDefaultProgram)) 
							waitDefaultProgramHandler.Set();
					}
					catch { }
					 */

				}, null, 0, 10);
			}
			waitDefaultProgramHandler.WaitOne();

			#endregion

			DigitalSignage.PlayAgent.Task next = null;

			int nRetCount = 0;
			do
			{
				next = _defaultScreenPlaylist.Next;
			}
			while (false == PlayScreenTask(next) && nRetCount++ <= _defaultScreenPlaylist.Count);

			if (next != null)
				RegisterNextScreen(next.duration);
		}

		/// <summary>
		/// 이전 시간 스케줄을 재생한다.
		/// </summary>
		public void PlayPrevTimeScreenTask()
		{
			#region Touch  - IsPauseDefaultProgram 플래그 에 따라 DefaultProgram 대기 여부 결정
			if (timer == null)
			{
				timer = new Timer(delegate(object o)
				{

					if (IsPauseDefaultProgram == false/* || this.currentScreen == null*/)
						waitDefaultProgramHandler.Set();
				}, null, 0, 10);
			}
			waitDefaultProgramHandler.WaitOne();
			#endregion

			DigitalSignage.PlayAgent.Task prev = null;

			int nRetCount = 0;

			prev = _timeScreenPlaylist.GetSingleTaskForNonDuplication(false);

			if (prev == null)
			{

				do
				{
					prev = _timeScreenPlaylist.Previous;
				}
				while (false == PlayScreenTask(prev) && nRetCount++ <= _timeScreenPlaylist.Count);
			}
			else
			{
				//	중복 허용하지 않는 시간 스케줄을 재생한다.
				PlayScreenTask(prev);
			}

			if (prev != null)
				RegisterNextScreen(prev.duration);
			else
				PlayNextDefaultScreenTask();

		}

		/// <summary>
		/// 다음 시간 스케줄을 재생한다.
		/// </summary>
		public void PlayNextTimeScreenTask()
		{
            if (IsUrgentEvent) return;

            #region Touch  - IsPauseDefaultProgram 플래그 에 따라 DefaultProgram 대기 여부 결정
			if (timer == null)
			{
				timer = new Timer(delegate(object o)
				{

					if (IsPauseDefaultProgram == false/* || this.currentScreen == null*/)
						waitDefaultProgramHandler.Set();
				}, null, 0, 10);
			}
			waitDefaultProgramHandler.WaitOne();
			#endregion

			DigitalSignage.PlayAgent.Task next = null;

			int nRetCount = 0;

			next = _timeScreenPlaylist.GetSingleTaskForNonDuplication(false);

            if(next != null)
            {
                logger.Debug("Next Time Screen Task!!");

                //	중복 허용하지 않는 시간 스케줄을 재생한다.
                if (!PlayScreenTask(next))
                {
                    next = null;
                }
            }

			if (next == null)
			{
                bool bPlayed = false;
				do
				{
					next = _timeScreenPlaylist.Next;
				}
                while (false == (bPlayed = PlayScreenTask(next)) && nRetCount++ <= _timeScreenPlaylist.Count);

                if (!bPlayed)
                {
                    if (currentScreen == null || currentScreen.IsEventPlay || currentScreen.type != TaskCommands.CmdDefaultProgram)
                        PlayNextDefaultScreenTask();

                    return;
                }

                logger.Debug("Next Time Screen Task!!");
			}


			if (next != null)
				RegisterNextScreen(next.duration);
			else
				PlayNextDefaultScreenTask();

		}
        public void PlayNextUSBScreenTask()
        {
            logger.Debug("Next USB Screen Task!!");

            #region Touch  - IsPauseDefaultProgram 플래그 에 따라 DefaultProgram 대기 여부 결정
            if (timer == null)
            {
                timer = new Timer(delegate(object o)
                {

                    if (IsPauseDefaultProgram == false/* || this.currentScreen == null*/)
                        waitDefaultProgramHandler.Set();
                }, null, 0, 10);
            }
            waitDefaultProgramHandler.WaitOne();
            #endregion

            DigitalSignage.PlayAgent.Task next = null;

            int nRetCount = 0;
            int nMaxTryCount = _usbScreenPlaylist.Count;
            do
            {
                next = _usbScreenPlaylist.Next;

                if (next != null) logger.Debug("Trying USB Play: " + next.description + " " + next.type);
                else logger.Debug("USB Play Failed: null");

                if (next != null)
                {
                    if (false == PlayScreenTask(next, String.Format("{0}usbprogram.xml", config.AppData)))
                    {
                        next = null;
                        logger.Debug("PlayScreenTask is False : next = null");
                    }
                    else break;
                }
            }
            while (nRetCount++ <= nMaxTryCount);

            if (next != null)
                RegisterNextScreen(next.duration);
            else
            {
                logger.Info(String.Format("USB 스크린 종료."));
                _usbScreenPlaylist.RemoveAllTasks();
                IsUSBPlay = false;
                PlayNextUrgentScreenTask();
            }
        }

		public void PlayNextUrgentScreenTask()
		{
            logger.Debug("Next Urgent Screen Task!!");

            #region Touch  - IsPauseDefaultProgram 플래그 에 따라 DefaultProgram 대기 여부 결정
			if (timer == null)
			{
				timer = new Timer(delegate(object o)
				{

					if (IsPauseDefaultProgram == false/* || this.currentScreen == null*/)
						waitDefaultProgramHandler.Set();
				}, null, 0, 10);
			}
			waitDefaultProgramHandler.WaitOne();
			#endregion

            DigitalSignage.PlayAgent.Task next = null;

            int nRetCount = 0;
            int nMaxTryCount = _urgentScreenPlaylist.Count;
            do
            {
                next = _urgentScreenPlaylist.Next;

				if (next != null) logger.Debug("Trying Urgent Play: " + next.description + " " + next.type);
				else logger.Debug("Urgent Play Failed: null");

                if (next != null && (next.show_flag & (int)ShowFlag.Show_Infinity) == 0)
                {
                    /// 무한 반복이 아니라면 스케줄을 삭제한다.
                    _urgentScreenPlaylist.RemoveTask(next);
                }

                if (next != null)
                {
                    //	1. 일일 내 시간 검사
                    //	2. 요일 검사
                    if (CheckContainTimeofDay(next.starttimeofday, next.endtimeofday) && CheckDayOfWeek(next.daysofweek))
                    {
                        if (false == PlayScreenTask(next))
                        {
                            next = null;
                            logger.Debug("PlayScreenTask is False : next = null");
                        }
                        else
                        {
                            IsUrgentEvent = true;
                            if ((next.show_flag & (int)ShowFlag.Show_Infinity) == 0)
                            {
                                /// 무한 반복이 아니라면 다음 실행시 다시 재생하지 않도록 스케줄 DB에서 상태를 변경한다.
                                ChangeTaskState(TaskState.StateProcessed, next.uuid);
                                break;
                            }
                        }
                    }
                    else next = null;
                }
            }
            while (nRetCount++ <= nMaxTryCount);

            if (next != null)
                RegisterNextScreen(next.duration);
            else
            {
                logger.Info(String.Format("[CmdUrgentProgram] 긴급 스크린 종료."));
                _urgentScreenPlaylist.RemoveAllTasks();
                IsUrgentEvent = false;
                PlayNextTimeScreenTask();
            }
        }


		public void PlayNextUrgertScreenTask()
		{
            logger.Debug("Next Urgent Screen Task!!");

            #region Touch  - IsPauseDefaultProgram 플래그 에 따라 DefaultProgram 대기 여부 결정
			if (timer == null)
			{
				timer = new Timer(delegate(object o)
				{

					if (IsPauseDefaultProgram == false/* || this.currentScreen == null*/)
						waitDefaultProgramHandler.Set();
				}, null, 0, 10);
			}
			waitDefaultProgramHandler.WaitOne();
			#endregion

            DigitalSignage.PlayAgent.Task next = null;

            int nRetCount = 0;
            int nMaxTryCount = _urgentScreenPlaylist.Count;
            do
            {
                next = _urgentScreenPlaylist.Next;

				if (next != null) logger.Debug("Trying Urgent Play: " + next.description + " " + next.type);
				else logger.Debug("Urgent Play Failed: null");

                if (next != null && (next.show_flag & (int)ShowFlag.Show_Infinity) == 0)
                {
                    /// 무한 반복이 아니라면 스케줄을 삭제한다.
                    _urgentScreenPlaylist.RemoveTask(next);
                }

                if (next != null)
                {
                    //	1. 일일 내 시간 검사
                    //	2. 요일 검사
                    if (CheckContainTimeofDay(next.starttimeofday, next.endtimeofday) && CheckDayOfWeek(next.daysofweek))
                    {
                        if (false == PlayScreenTask(next))
                        {
                            next = null;
                            logger.Debug("PlayScreenTask is False : next = null");
                        }
                        else
                        {
                            IsUrgentEvent = true;
                            if ((next.show_flag & (int)ShowFlag.Show_Infinity) == 0)
                            {
                                /// 무한 반복이 아니라면 다음 실행시 다시 재생하지 않도록 스케줄 DB에서 상태를 변경한다.
                                ChangeTaskState(TaskState.StateProcessed, next.uuid);
                                break;
                            }
                        }
                    }
                    else next = null;
                }
            }
            while (nRetCount++ <= nMaxTryCount);

            if (next != null)
                RegisterNextScreen(next.duration);
            else
            {
                logger.Info(String.Format("[CmdUrgentProgram] 긴급 스크린 종료."));
                _urgentScreenPlaylist.RemoveAllTasks();
                IsUrgentEvent = false;
                /// GS Target 광고
                LimitUrgentScreenCount = 0;
                PlayNextTimeScreenTask();

            }
        }

		/// <summary>
		/// 저장된 다음 볼륨 조절 스케줄을 적용한다.
		/// </summary>
		private void PlayNextStoredVolumeControlTask()
		{
			DigitalSignage.PlayAgent.Task t = null;

			while (null != (t = _volumeControlPlaylist.Pop()))
			{
				if (t.TaskEnd <= TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()))
					continue;

				if (!PlayControlSchedule(t))
					continue;

				break;
			}

			if (t == null)
			{
				//	볼륨을 되돌린다.
				RestoreVolume();
				RestoreSerialVolume();

				currentVolumeControl = null;
				currentSerialVolumeControl = null;
			}
		}

		private bool PlayControlSchedule(DigitalSignage.PlayAgent.Task task)
		{
			if (task != null)
			{
				logger.Info("[" + task.description + " (" + task.uuid + ")] will be started.");
				if (!CheckDayOfWeek(task.daysofweek))
				{
					logger.Info("[" + task.description + " (" + task.uuid + ")(Day Of Week : " + task.daysofweek + ")] invaild Day of week.");
					return false;
				}
				try
				{
					bool bSuccess = false;

                    try
                    {
                        this.ControlSchedStarted(task);
                    }
                    catch { }

					TaskControlSchedules command = ControlScheduleHelper.GetInstance.GetKindOfCommand(task.description);
					switch (command)
					{
						case TaskControlSchedules.CtrlVolume:
							{
								StoreCurrentVolume();
								if (bSuccess = ControlScheduleHelper.GetInstance.PlayControlSchedule(task))
								{
									currentVolumeControl = task;
								}

								break;
							}
						case TaskControlSchedules.CtrlSyncSchedule:
							{
// 								config.localTasksList.CleanAllTasks();
								config.LastSyncFromFile = 0;
                                config.LastSyncTaskSyncFromFile = DateTime.MinValue.ToString("yyyyMMddHHmmss");

								logger.Info(command.ToString() + " : " + " LastSyncFromFile = 0");
								break;
							}
						case TaskControlSchedules.CtrlSerialAuto:
							{
								logger.Info(command.ToString() + " : " +
									(bSuccess = _serialPortObject.Auto()).ToString());

								break;
							}
						case TaskControlSchedules.CtrlSerialPowerOff:
							{
								logger.Info(command.ToString() + " : " +
									(bSuccess = _serialPortObject.PowerOff()).ToString());
		
								break;
							}
						case TaskControlSchedules.CtrlSerialPowerOn:
							{
								logger.Info(command.ToString() + " : " +
									(bSuccess = _serialPortObject.PowerOn()).ToString());

								break;
							}
						case TaskControlSchedules.CtrlSerialSourceComponent:
							{
								logger.Info(command.ToString() + " : " +
									(bSuccess = _serialPortObject.Source(SourceMode.sourceComponent)).ToString());
								break;
							}
						case TaskControlSchedules.CtrlSerialSourceDVI:
							{
								logger.Info(command.ToString() + " : " +
									(bSuccess = _serialPortObject.Source(SourceMode.sourceDVI)).ToString());
								break;
							}
                        case TaskControlSchedules.CtrlSerialSourceHDMI:
                            {
                                logger.Info(command.ToString() + " : " +
                                    (bSuccess = _serialPortObject.Source(SourceMode.sourceHDMI)).ToString());
                                break;
                            }
						case TaskControlSchedules.CtrlSerialSourceRGB:
							{
								logger.Info(command.ToString() + " : " +
									(bSuccess = _serialPortObject.Source(SourceMode.sourceRGB)).ToString());
								break;
							}
						case TaskControlSchedules.CtrlSerialSourceTV:
							{
								logger.Info(command.ToString() + " : " +
									(bSuccess = _serialPortObject.Source(SourceMode.sourceGeneral)).ToString());
								break;
							}
						case TaskControlSchedules.CtrlSerialVolume:
							{
								String[] arrVolumes = task.description.Split('[')[1].TrimEnd(']').Split('|');

								logger.Info("SET DEVICE VOLUME NOW : " + arrVolumes[0]);

								logger.Info(command.ToString() + " : " +
									(bSuccess = _serialPortObject.Volume(Convert.ToInt32(arrVolumes[0]))).ToString());
								break;
							}
						case TaskControlSchedules.CtrlSerialVolumeNow:
							{
								String[] arrVolumes = task.description.Split('[')[1].TrimEnd(']').Split('|');

								logger.Info("SET DEVICE VOLUME NOW : " + arrVolumes[0]);

								logger.Info(command.ToString() + " : " +
									(bSuccess = _serialPortObject.Volume(Convert.ToInt32(arrVolumes[0]))).ToString());
								break;
							}
						default:
							bSuccess = ControlScheduleHelper.GetInstance.PlayControlSchedule(task);
							break;
					}

				}
				catch (NullReferenceException /*nullerr*/)
				{
					logger.Info("There is no Serial Port Device.");
					return true;
				}
				catch (System.NotImplementedException)
				{
					logger.Info("There is no Volume Function in the Serial Port Device.");
					return true;
				}
				catch (System.Exception e)
				{
					logger.Error(e.ToString());
					return false;
				}

				return true;
			}
			else
			{
				//	unload screen
				currentVolumeControl = null;
				return false;
			}
		}

        /// <summary>
        /// 처음 스크린 재생
        /// </summary>
        public void FirstScreen()
        {
            logger.Info("First Screen Called!");

            try
            {
                if (IsUrgentEvent)
                {
                    _urgentScreenPlaylist.RestoreFirst();
                    PlayNextUrgentScreenTask();
                }
                else if (currentScreen != null && (currentScreen.type == TaskCommands.CmdProgram))
                {
                    _timeScreenPlaylist.RestoreFirst();
                    PlayNextTimeScreenTask();
                }
                else if (currentScreen != null && (currentScreen.type == TaskCommands.CmdDefaultProgram || currentScreen.type == TaskCommands.CmdTouchDefaultProgram))
                {
                    _defaultScreenPlaylist.RestoreFirst();
                    PlayNextDefaultScreenTask();
                }
                //else if (currentScreen != null && (currentScreen.type == TaskCommands.CmdUrgentProgram))
                //{
                //    PlayNextUrgertScreenTask();
                //}
            }
            catch (Exception ex)
            {
                logger.Error(String.Format("Next Event Error: {0}", ex.Message));
            }
        }

		/// <summary>
		/// 다음 스크린 재생
		/// </summary>
		public void NextScreen()
		{
			logger.Info("Next Screen Called!");

			try
			{
                if (IsUSBPlay)
                {
                    PlayNextUSBScreenTask();
                }
				else if (IsUrgentEvent)
				{
                    PlayNextUrgentScreenTask();
				}
                else if((currentScreen != null && currentScreen.type == TaskCommands.CmdUrgentProgram))
                {
                    PlayNextUrgentScreenTask();
                }
				else if (currentScreen != null && (currentScreen.type == TaskCommands.CmdDefaultProgram || currentScreen.type == TaskCommands.CmdTouchDefaultProgram))
				{
					PlayNextDefaultScreenTask();
				}
				else if (currentScreen != null && (currentScreen.type == TaskCommands.CmdProgram))
				{
					PlayNextTimeScreenTask();
				}
				//else if (currentScreen != null && (currentScreen.type == TaskCommands.CmdUrgentProgram))
				//{
				//    PlayNextUrgertScreenTask();
				//}
			}
			catch (Exception ex)
			{
				logger.Error(String.Format("Next Event Error: {0}", ex.Message));
			}
		}

		/// <summary>
		/// 이전 스크린 재생
		/// </summary>
		public void PreviousScreen()
		{
			logger.Info("Previous Screen Called!");

			try
			{
                if (IsUSBPlay)
                {
                    logger.Debug("Previous Screen is not supported in Usb.");
                }
				else if (IsUrgentEvent)
				{
					logger.Debug("Previous Screen is not supported in Event.");
				}
				else if (currentScreen != null && (currentScreen.type == TaskCommands.CmdDefaultProgram || currentScreen.type == TaskCommands.CmdTouchDefaultProgram))
				{
					PlayPrevDefaultScreenTask();
				}
				else if (currentScreen != null && (currentScreen.type == TaskCommands.CmdProgram))
				{
					PlayPrevTimeScreenTask();
				}
			}
			catch (Exception ex)
			{
				logger.Error(String.Format("Previous Event Error: {0}", ex.Message));
			}
		}

		/// <summary>
        /// Metatag와 일치하는 정보를 가져온다
		/// </summary>
        /// <param name="sEventID"></param>
		/// <param name="sRequest"></param>
		/// <param name="sResponse"></param>
		/// <returns></returns>
		public bool MakeMetatagEvent(String sEventID, String sRequest, String sResponse)
		{
			if (currentScreen != null && this.IsUrgentEvent /*currentScreen.type == TaskCommands.CmdUrgentProgram*/)
			{
				logger.Debug("Urgent Schedule is running.");
				return false;
			}

            try
            {
                EventID = Convert.ToInt64(sEventID);
                
                MetaTagHelper srcMetaTagHelper = new MetaTagHelper(sRequest, sResponse);

                logger.Debug("Making Src MetaTags");
                foreach (string key in srcMetaTagHelper.DataDictionary.Keys)
                {
                    logger.Debug(String.Format("키: {0}, 값: {1}", key, srcMetaTagHelper.DataDictionary[key]));
                }

                #region 시간 스케줄 조회
                _managedtimeScreenPlaylist.BackupTask();

                PlayAgent.Task item = _managedtimeScreenPlaylist.First;

                int nCount = 0;

                ///	시간 스케줄 태그 분석
                do
                {
                    if (item == null) break;

                    ///	이벤트 속성이 없다면 Skip
                    if ((item.show_flag & (int)ShowFlag.Show_Event) == 0)
                        continue;

                    try
                    {
                        MetaTagHelper DestMetaTagHelper = new MetaTagHelper(item.MetaTags);

                        if (srcMetaTagHelper.Contain(DestMetaTagHelper))
                        {
                            logger.Debug("Succeed Metatags : " + item.MetaTags);
                            PlayAgent.Task newtask = item.Clone() as PlayAgent.Task;
                            newtask.IsEventPlay = true;
                            _urgentScreenPlaylist.AddTask(newtask);
                        }
                    }
                    catch (Exception err)
                    {
                        logger.Error("Destination MetaTag Pasing Error: " + err.Message);
                    }

                    if (nCount++ > _managedtimeScreenPlaylist.Count)
                    {
                        item = null;
                        break;
                    }

                } while ((item = _managedtimeScreenPlaylist.NextToEnd) != null);

                _managedtimeScreenPlaylist.RestoreTask();
                #endregion

                #region 반복 스케줄 조회
                _defaultScreenPlaylist.BackupTask();

                ///	반복 스케줄 TAG 분석
                item = _defaultScreenPlaylist.First;

                nCount = 0;
                do
                {
                    if (item == null) break;

                    ///	이벤트 속성이 없다면 Skip
                    if ((item.show_flag & (int)ShowFlag.Show_Event) == 0)
                        continue;
                    try
                    {
                        MetaTagHelper DestMetaTagHelper = new MetaTagHelper(item.MetaTags);

                        if (srcMetaTagHelper.Contain(DestMetaTagHelper))
                        {
                            logger.Debug("Succeed Metatags : " + item.MetaTags);
                            PlayAgent.Task newtask = item.Clone() as PlayAgent.Task;
                            newtask.IsEventPlay = true;
                            _urgentScreenPlaylist.AddTask(newtask);
                        }
                    }
                    catch (Exception err)
                    {
                        logger.Error("Destination MetaTag Pasing Error: " + err.Message);
                    }

                    if (nCount++ > _defaultScreenPlaylist.Count)
                    {
                        item = null;
                        break;
                    }

                } while ((item = _defaultScreenPlaylist.NextToEnd) != null);

                _defaultScreenPlaylist.RestoreTask();
                #endregion

                try
                {
                    //  타겟 광고 성공 기록
                    Config.GetConfig.WriteTargetAdInfo(EventID, srcMetaTagHelper);
                }
                catch (Exception wte)
                {
                    logger.Error(wte.ToString());
                }

            }
            catch (Exception err)
            {
                logger.Error("Source MetaTag Pasing Error: " + err.Message);
            }

            _urgentScreenPlaylist.RestoreFirst();

			return _urgentScreenPlaylist.Count > 0;

		}

		/// <summary>
		/// Metatag와 일치하는 정보를 가져온다
		/// </summary>
		/// <param name="sResponse"></param>
		/// <returns></returns>
		public bool MakeMetatagEvent(String sResponse)
		{
			if (currentScreen != null && this.IsUrgentEvent /*currentScreen.type == TaskCommands.CmdUrgentProgram*/)
			{
				logger.Debug("Urgent Schedule is running.");
				return false;
			}

			try
			{
				EventID = DateTime.Now.Ticks;
				String sTrimedResponse = sResponse.Trim();

				MetaTagHelper srcMetaTagHelper = new MetaTagHelper();
				srcMetaTagHelper.EtriPrepare(sResponse);

				logger.Debug("Making Src MetaTags");
				foreach (string key in srcMetaTagHelper.DataDictionary.Keys)
				{
					logger.Debug(String.Format("키: {0}, 값: {1}", key, srcMetaTagHelper.DataDictionary[key]));
				}

				#region 시간 스케줄 조회
				_managedtimeScreenPlaylist.BackupTask();

				PlayAgent.Task item = _managedtimeScreenPlaylist.First;

				///	시간 스케줄 태그 분석
				do
				{
					if (item == null) break;

					///	이벤트 속성이 없다면 Skip
					if ((item.show_flag & (int)ShowFlag.Show_Event) == 0)
						continue;

					try
					{
						MetaTagHelper DestMetaTagHelper = new MetaTagHelper(item.MetaTags);

						if (srcMetaTagHelper.Contain(DestMetaTagHelper))
						{
							logger.Debug("Succeed Metatags : " + item.MetaTags);
							PlayAgent.Task newtask = item.Clone() as PlayAgent.Task;
							newtask.IsEventPlay = true;
							_urgentScreenPlaylist.AddTask(newtask);
						}
					}
					catch (Exception err)
					{
						logger.Error("Destination MetaTag Pasing Error: " + err.Message);
					}
				} while ((item = _managedtimeScreenPlaylist.NextToEnd) != null);

				_managedtimeScreenPlaylist.RestoreTask();
				#endregion

				#region 반복 스케줄 조회
				_defaultScreenPlaylist.BackupTask();

				///	반복 스케줄 TAG 분석
				item = _defaultScreenPlaylist.First;

				do
				{
					if (item == null) break;

					///	이벤트 속성이 없다면 Skip
					if ((item.show_flag & (int)ShowFlag.Show_Event) == 0)
						continue;
					try
					{
						MetaTagHelper DestMetaTagHelper = new MetaTagHelper(item.MetaTags);

						if (srcMetaTagHelper.Contain(DestMetaTagHelper))
						{
							logger.Debug("Succeed Metatags : " + item.MetaTags);
							PlayAgent.Task newtask = item.Clone() as PlayAgent.Task;
							newtask.IsEventPlay = true;
							_urgentScreenPlaylist.AddTask(newtask);
						}
					}
					catch (Exception err)
					{
						logger.Error("Destination MetaTag Pasing Error: " + err.Message);
					}
				} while ((item = _defaultScreenPlaylist.NextToEnd) != null);

				_defaultScreenPlaylist.RestoreTask();
				#endregion
			}
			catch (Exception err)
			{
				logger.Error("Source MetaTag Pasing Error: " + err.Message);
			}

			_urgentScreenPlaylist.RestoreFirst();

			this.LimitUrgentScreenCount = _urgentScreenPlaylist.Count;

			return _urgentScreenPlaylist.Count > 0;

		}

        /// <summary>
        /// 이벤트 볼륨을 재생한다.
        /// </summary>
        public void SetEventVolume()
        {
            try
            {
                int nMVol = config.StoredEventVolume;
                int nWVol = 100;
                if (nWVol != -1)
                {
                    if (PCControl.SetWaveVolume(nWVol))
                    {
                        logger.Info(String.Format("The wave volume has been set to {0}.", nWVol));
                    }
                }
                if (nMVol != -1)
                {
                    int nStored = Config.GetConfig.ProcessingEventVolume;
                    
                    if(nStored == -1)   Config.GetConfig.ProcessingEventVolume = PCControl.GetMasterVolume();

                    if (PCControl.SetMasterVolume(nMVol))
                    {
                        logger.Info(String.Format("The master volume has been set to {0}.", nMVol));
                    }
                }
            }
            catch (System.Exception e)
            {
                logger.Error(e.Message);
            }
        }

        /// <summary>
        /// 저장해둔 볼륨이 있다면 볼륨을 되돌리고 저장정보를 초기화한다..
        /// </summary>
        public void RestoreEventVolume()
        {
            try
            {
                int nMVol = config.ProcessingEventVolume;

                if (nMVol != -1)
                {
                    if (PCControl.SetMasterVolume(nMVol))
                    {
                        logger.Info(String.Format("The master volume has been restored to {0}.", nMVol));
                        config.ProcessingEventVolume = -1;
                    }
                }
            }
            catch (System.Exception e)
            {
                logger.Error(e.Message);
            }

        }

		/// <summary>
		/// 저장해둔 볼륨 정보가 없다면 현재 볼륨을 저장해 둔다.
		/// </summary>
        public void StoreCurrentVolume()
		{
			int nWVol = config.StoredWaveVolume;
			int nMVol = config.StoredMasterVolume;
			if (nWVol == -1)
			{
				config.StoredWaveVolume = PCControl.GetWaveVolume();
			}
			if (nMVol == -1)
			{
				config.StoredMasterVolume = PCControl.GetMasterVolume();
			}
		}

		/// <summary>
		/// 저장해둔 볼륨이 있다면 볼륨을 되돌리고 저장정보를 초기화한다..
		/// </summary>
        public void RestoreVolume()
		{
			try
			{
				int nMVol = config.StoredMasterVolume;
				int nWVol = config.StoredWaveVolume;
				if (nWVol != -1)
				{
					if (PCControl.SetWaveVolume(nWVol))
					{
						logger.Info(String.Format("The wave volume has been restored to {0}.", nWVol));
						config.StoredWaveVolume = -1;
					}
				}
				if (nMVol != -1)
				{
					if (PCControl.SetMasterVolume(nMVol))
					{
						logger.Info(String.Format("The master volume has been restored to {0}.", nMVol));
						config.StoredMasterVolume = -1;
					}
				}
			}
            catch (System.Exception e)
            {
                logger.Error(e.Message);	
            }

		}

		/// <summary>
		/// 저장해둔 볼륨이 있다면 볼륨을 되돌리고 저장정보를 초기화한다..
		/// </summary>
		private void RestoreSerialVolume()
		{
			try
			{
				int nSVol = config.StoredSerialVolume;

				if (nSVol != -1)
				{
					if (_serialPortObject.Volume(nSVol))
					{
						logger.Info(String.Format("The serial volume has been restored to {0}.", nSVol));
						config.StoredSerialVolume = -1;
					}
				}
			}
			catch (System.NullReferenceException)
			{
			}
			catch (System.NotImplementedException)
			{
			}
			catch (System.Exception e)
			{
				logger.Error(e.Message);
			}

		}
		/// <summary>
		/// 저장된 다음순서 스크린을 재생한다
		/// </summary>
		private void PlayNextStoredScreenTask()
		{
			DigitalSignage.PlayAgent.Task t = null;

			while (null != (t = _managedtimeScreenPlaylist.Pop()))
			{
				if (t.TaskEnd <= TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()))
					continue;

				if (!PlayScreenTask(t))
					continue;

				break;
			}

			if (t == null)
			{
				PlayNextDefaultScreenTask();
			}
		}

		private void PlayNextStoredSubtitleTask()
		{
			DigitalSignage.PlayAgent.Task t = null;

			while (null != (t = _subtitlePlaylist.Pop()))
			{
				if (t.TaskEnd <= TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()))
					continue;
				break;
			}
			PlaySubtitleTask(t);

		}

        /// <summary>
        /// 동기화 오브젝트 스케줄 시작
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void _syncObject_OnStartSchedule(object sender, StartScheduleEventArgs e)
        {

            /// 선택적 광고 모드인 경우 무시
            if (SelectedAdPlaying) return ;

            DateTime dtNow = DateTime.Now;
            TimeSpan tsSlaveAdjustTime = TimeSpan.FromMilliseconds(Properties.Settings.Default.SlaveAdjustTimeValue);
            /// 대기 모드
            if (e.sType == "S")
            {
                lock (_lockSync)
                {
                    TimeSpan dateDiff = dtNow - DigitalSignage.Common.TimeConverter.Convert17LengthStringToDateTime(e.sRecvDT);

                    ScreenStartInfoInSchedule.GetInstance.ScreenStartDT = ScreenStartInfoInSchedule.GetInstance.ScheduleStartDT = DigitalSignage.Common.TimeConverter.Convert17LengthStringToDateTime(e.sStartDT) + dateDiff + tsSlaveAdjustTime;
                    logger.Debug("동기화 스크린 시작시간: {0}", ScreenStartInfoInSchedule.GetInstance.ScreenStartDT.ToString("yyyyMMddHHmmssfff"));
                }

                //if (ScreenStartInfoInSchedule.GetInstance.GroupScheduleID != e.sID)
                {

                    PlayTaskFromGuuid(e.sID, true, false, false);
                    logger.Debug("동기화 슬레이브 시작: 상대방({0}), 나({1}), GUUID({2})", e.sRecvDT, dtNow.ToString("yyyyMMddHHmmssfff"), e.sID);
                } 
                

            }
            else if (e.sType == "P") /// 재생 모드
            {
            }
            else if (e.sType == "K") /// Keep Alive
            {
                logger.Debug("동기화 슬레이브 Keep Alive 체크: 상대방({0}), 나({1}), GUUID({2})", e.sRecvDT, dtNow.ToString("yyyyMMddHHmmssfff"), e.sID);

                lock (_lockSync)
                {
                    TimeSpan dateDiff = dtNow - DigitalSignage.Common.TimeConverter.Convert17LengthStringToDateTime(e.sRecvDT);

                    ScreenStartInfoInSchedule.GetInstance.ScreenStartDT = ScreenStartInfoInSchedule.GetInstance.ScheduleStartDT = DigitalSignage.Common.TimeConverter.Convert17LengthStringToDateTime(e.sStartDT) + dateDiff + tsSlaveAdjustTime;
                    logger.Debug("동기화 스크린 시작시간: {0}", ScreenStartInfoInSchedule.GetInstance.ScreenStartDT.ToString("yyyyMMddHHmmssfff"));
                }

                if (ScreenStartInfoInSchedule.GetInstance.GroupScheduleID != e.sID)
                {
                    PlayTaskFromGuuid(e.sID, true, false,false);
                    logger.Debug("동기화 슬레이브 시작: 상대방({0}), 나({1}), GUUID({2})", e.sRecvDT, dtNow.ToString("yyyyMMddHHmmssfff"), e.sID);
                }
            }
            else if (e.sType.StartsWith("E")) /// 오픈스크린 : 이벤트 처리 모드
            {
                try
                {
                    string[] arrSplit = e.sType.Split('|');

                    DigitalSignage.Common.OpenScreen.SyncScreenEventHelper.GetInstance.RunEvent(this, System.Text.UTF8Encoding.UTF8.GetString(Convert.FromBase64String(arrSplit[1])), arrSplit[2], arrSplit[3]);
                }
                catch { }
            }

        }

        /// <summary>
        /// 오픈스크린 : 동기화 이동 이벤트 처리 로직, 마스터인 경우에만 처리하도록 한다
        /// </summary>
        /// <param name="sCode"></param>
        /// <param name="sValue"></param>
        /// <returns></returns>
        public bool EventSyncMoveProcess(string sCode, string sValue)
        {
            bool bRet = false;

            if (ScreenStartInfoInSchedule.GetInstance.IsSyncMaster)
            {
                logger.Debug("- OpenScreen : 동기화 마스터 이벤트 분석 시작.");

                /// Sample: Region1:Left=30,Top=30;Region2:Left=30,Top=30
                string[] arrPacket = sValue.Split(';');

                foreach (string sPacket in arrPacket)
                {
                    string[] arrPieces = sPacket.Split(':');

                    string sName = arrPieces[0];
                    if (arrPieces.Length > 1)
                    {
                        string[] arrCommandPackets = arrPieces[1].Split(',');

                        foreach (string sCommandPackets in arrCommandPackets)
                        {
                            string[] arrCommands = arrPieces[1].Split('=');

                            if (arrCommands.Length > 1)
                            {
                                string sCommandKey = arrCommands[0];
                                string sCommandValue = arrCommands[1];

                                /// 처리로직 수행
                                ///  - sName의 컴포넌트를 sCommandKey 키를 통해 sCommandValue만큼 처리
                                ///  

                                /// TEST
                                //string sSlaveValue = (Convert.ToInt32(sCommandValue) - 1920).ToString();

                                string slavePacket = String.Format("E|{0}|{1}|{2}", Convert.ToBase64String(System.Text.UTF8Encoding.UTF8.GetBytes(sName)), sCommandKey, sCommandValue);
                                _syncObject.SendToSlaves(ScreenStartInfoInSchedule.GetInstance.GroupScheduleID, ScreenStartInfoInSchedule.GetInstance.ScreenStartDT.ToString("yyyyMMddHHmmssfff"), slavePacket);
                                logger.Debug("- OpenScreen : 슬래이브 목록에 명령 전송 완료 : {0}, {1}", ScreenStartInfoInSchedule.GetInstance.GroupScheduleID, slavePacket);

                                DigitalSignage.Common.OpenScreen.SyncScreenEventHelper.GetInstance.RunEvent(this, sName, sCommandKey, sCommandValue);
                                logger.Debug("- OpenScreen : 명령 값 처리 완료");
                            }
                            else
                            {
                                logger.Debug("- OpenScreen : 명령 값이 없습니다.");
                            }
                        }
                    }
                    else
                    {
                        logger.Debug("- OpenScreen : 명령 패킷이 없습니다.");

                    }

                }
            }


            return bRet;
            
        }

        public bool PlaySyncSchedule(String sGroupID)
        {
            if (File.Exists(String.Format(Config.GetConfig.AppData + @"Smil\{0}.smil", sGroupID)))
            {
                #region Sync 스케줄 갱신
                try
                {
                    if (config.localTasksList.HasUpdatedSyncTask(dtSyncTask))
                    {
                        logger.Debug("동기화 스케줄 갱신!");
                        ClientDataBase.DataSet.SYNC_TASKSDataTable dt = config.localTasksList.GetSyncTaskList();
                        logger.Debug("동기화 스케줄 갱신됨: 총 {0} 개", dt.Rows.Count);

                        foreach (ClientDataBase.DataSet.SYNC_TASKSRow row in dt.Rows)
                        {
                            if (row.DEL_YN == "N")
                            {
                                /// Sync 클라이언트 생성
                                _syncObject.AddSyncClient(String.Format("{0}|{1}", row.PLAYERS_PID, row.GUUID), System.Net.IPAddress.Parse(row.IP_ADDR), Properties.Settings.Default.SyncPort);
                            }
                            else
                            {
                                _syncObject.RemoveSyncClient(String.Format("{0}|{1}", row.PLAYERS_PID, row.GUUID));
                            }

                            if (dtSyncTask.CompareTo(row.EDIT_DT) < 0)
                            {
                                dtSyncTask = row.EDIT_DT;
                            }
                        }
                    }
                }
                catch (Exception eSync) { logger.Error(eSync.ToString()); }
                #endregion

                Smil.Library.Smil smilObject = Smil.Library.Smil.LoadObjectFromSmilFile(String.Format(Config.GetConfig.AppData + @"Smil\{0}.smil", sGroupID));
                TimeSpan tsDuration = smilObject.TotalDuration();

                DigitalSignage.PlayAgent.Task t = new PlayAgent.Task();
                t.duration = (int)tsDuration.TotalSeconds == 0 ? 86400 : (int)tsDuration.TotalSeconds;
                t.type = TaskCommands.CmdUrgentProgram;
                t.guuid = Guid.Parse(sGroupID);
                t.description = "추천 스크린 재생";
                t.daysofweek = 127;
                t.show_flag = 127 | (int)ShowFlag.Show_SyncMaster;

                IsUrgentEvent = true;

                if (PlayScreenTask(t, String.Format(Config.GetConfig.AppData + @"Smil\{0}.xml", sGroupID)))
                {
                    RegisterNextScreen(t.duration);
                }
                else
                {
                    PlayNextUrgentScreenTask();
                }

                return true;
            }

            return false;
        }

        /// <summary>
        /// 스케줄 재생
        /// </summary>
        /// <param name="guuid"></param>
        /// <param name="isSetCurrent"></param>
        /// <param name="isUnlimitRepeat"></param>
        /// <param name="isSetStartDT"></param>
        /// <returns></returns>
        public bool PlayTaskFromGuuid(string guuid, bool isSetCurrent = false, bool isUnlimitRepeat = true, bool isSetStartDT = true)
        {
            lock (_lockSync)
            {
                DigitalSignage.PlayAgent.Task t = null;

                string guuidToLower = guuid.ToLower();

                try
                {
                    logger.Info("재생 요청: {0}", guuid);

                    if (File.Exists(String.Format(Config.GetConfig.AppData + @"Smil\{0}.smil", guuid)))
                    {
                        logger.Info("오픈스크린 동기화 대상 파일 존재함: {0}", guuid);
                        Smil.Library.Smil smilObject = Smil.Library.Smil.LoadObjectFromSmilFile(String.Format(Config.GetConfig.AppData + @"Smil\{0}.smil", guuid));
                        TimeSpan tsDuration = smilObject.TotalDuration();

                        t = new PlayAgent.Task();
                        t.duration = (int)tsDuration.TotalSeconds == 0 ? 86400 : (int)tsDuration.TotalSeconds;
                        t.type = TaskCommands.CmdUrgentProgram;
                        t.description = "추천 스크린 재생";
                        t.guuid = Guid.Parse(guuid);
                        t.daysofweek = 127;
                        t.show_flag = 127 | (int)ShowFlag.Show_SyncSlave;

                        IsUrgentEvent = true;

                        if (PlayScreenTask(t, String.Format(Config.GetConfig.AppData + @"Smil\{0}.xml", guuid)))
                        {
                            RegisterNextScreen(t.duration);
                        }
                        else
                        {
                            PlayNextUrgentScreenTask();
                        }

                        return true;
                    }
                    else
                    {
                        /// 스케줄 조회
                        if (null == (t = _timeScreenPlaylist.FindTaskByGuuid(guuidToLower, isSetCurrent)))
                        {
                            if (null == (t = this._defaultScreenPlaylist.FindTaskByGuuid(guuidToLower, isSetCurrent)))
                            {
                                if (null == (t = _managedtimeScreenPlaylist.FindTaskByGuuid(guuidToLower, isSetCurrent)))
                                {
                                    return false;
                                }
                            }
                        }
                    }
                }
                catch (Exception ex) { logger.Error(ex.Message); }


                bool bRet = false;
                
                if (bRet = PlayScreenTask(t, false, isSetStartDT, true))
                {
                    if (isUnlimitRepeat)
                    {
                        try
                        {
                            if (thDefaultScreenPlayer != null && thDefaultScreenPlayer.IsAlive)
                            {
                                thDefaultScreenPlayer.Abort();
                            }
                        }
                        catch { }
                        thDefaultScreenPlayer = null;
                    }
                    else
                    {
                        try
                        {
                            RegisterNextScreen(t.duration );
                        }
                        catch { }
                    }
                    PlayerStatus = PlayStatus.Played;
                }

                return bRet;
            }
        }

        bool SelectedAdPlaying = false;
        /// <summary>
        /// 스케줄 재생
        /// </summary>
        /// <param name="guuid"></param>
        /// <param name="isSetCurrent"></param>
        /// <param name="isUnlimitRepeat"></param>
        /// <param name="isSetStartDT"></param>
        /// <returns></returns>
        public bool PlayTask(string guuid, bool isSetCurrent = false, bool isUnlimitRepeat = true, bool isSetStartDT = true)
        {
            lock (_lockSync)
            {
                DigitalSignage.PlayAgent.Task t = null;

                string guuidToLower = guuid.ToLower();

                try
                {
                    logger.Info("재생 요청: {0}", guuid);

                    /// 스케줄 조회
                    if (null == (t = _timeScreenPlaylist.FindTask(guuidToLower, isSetCurrent)))
                    {
                        if (null == (t = this._defaultScreenPlaylist.FindTask(guuidToLower, isSetCurrent)))
                        {
                            if (null == (t = _managedtimeScreenPlaylist.FindTask(guuidToLower, isSetCurrent)))
                            {
                                return false;
                            }
                        }
                    }
                }
                catch (Exception ex) { logger.Error(ex.Message); }


                bool bRet = false;

                if (bRet = PlayScreenTask(t, false, isSetStartDT, true))
                {
                    SelectedAdPlaying = true;

                    if (isUnlimitRepeat)
                    {
                        try
                        {
                            if (thDefaultScreenPlayer != null && thDefaultScreenPlayer.IsAlive)
                            {
                                thDefaultScreenPlayer.Abort();
                            }
                        }
                        catch { }
                        thDefaultScreenPlayer = null;
                    }
                    else
                    {
                        try
                        {
                            RegisterNextScreen(t.duration);
                        }
                        catch { }
                    }
                    PlayerStatus = PlayStatus.Played;
                }

                return bRet;
            }
        }

		/// <summary>
        /// 스케줄 재생
		/// </summary>
		/// <param name="uuid"></param>
		/// <param name="isSetCurrent"></param>
		/// <param name="isUnlimitRepeat"></param>
		/// <returns></returns>
        public bool PlayUrgentTask(string uuid, bool isSetCurrent = false, bool isUnlimitRepeat = true)
		{
            lock (_lockSync)
            {
                DigitalSignage.PlayAgent.Task t = null;

                string uuidToLower = uuid.ToLower();

                try
                {
                    logger.Info("재생 요청: {0}", uuid);

                    /// 스케줄 조회
                    if (null == (t = _timeScreenPlaylist.FindTask(uuidToLower, isSetCurrent)))
                    {
                        if (null == (t = this._defaultScreenPlaylist.FindTask(uuidToLower, isSetCurrent)))
                        {
                            if (null == (t = _managedtimeScreenPlaylist.FindTask(uuidToLower, isSetCurrent)))
                            {
                                return false;
                            }
                        }
                    }
                }
                catch (Exception ex) { logger.Error(ex.Message); }


                bool bRet = false;

                if (t != null)
                {
                    PlayAgent.Task newtask = t.Clone() as PlayAgent.Task;
                    newtask.IsEventPlay = true;

                    _urgentScreenPlaylist.AddTask(newtask);

                    _urgentScreenPlaylist.RestoreFirst();

                    this.LimitUrgentScreenCount = _urgentScreenPlaylist.Count;

                    bRet = _urgentScreenPlaylist.Count > 0;


                } return bRet;
            }
		}

        private void LoggingStackTrace()
        {
            #region 호출 스택 로그 찍기
            StackTrace stackTrace = new StackTrace();           // get call stack
            StackFrame[] stackFrames = stackTrace.GetFrames();  // get method calls (frames)

            // write call stack method names
            String strFrame = String.Empty;
            foreach (StackFrame stackFrame in stackFrames)
            {

                System.Reflection.MethodBase method = stackFrame.GetMethod();
                strFrame += (method.Name + " " + method.Module + " " + method.DeclaringType + " , ");
            }
            logger.Debug("호출스택:" + strFrame);
            #endregion
        }


        /// <summary>
        /// 스크린를 즉시 재생한다.
        /// </summary>
        /// <param name="task">재생할 작업</param>
        /// <param name="CheckCondition">조건 검사 여부</param>
        /// <param name="IsSetStartDT">시작 시간을 넣을지 여부</param>
        /// <param name="IsPlaySlaveSchedule">슬래이브 스케줄을 실행할지 여부</param>
        /// <returns></returns>
		private bool PlayScreenTask(DigitalSignage.PlayAgent.Task task, bool CheckCondition = true, bool IsSetStartDT = true, bool IsPlaySlaveSchedule = false)
		{
			if (task != null)
			{
                if (Properties.Settings.Default.IsPlaySlaveContent) IsPlaySlaveSchedule = true;

                if (IsPlaySlaveSchedule == false && task.IsSyncSlave) return false;

				// 재생 조건이 맞지 않으면 재생하지 않는다.
//				if ((task.show_flag & showFlag) == 0)
                //					return false;

                #region 오프라인 스킵 여부 조회
                /// 오프라인 메타태그를 조회하여 오프라인 시 재생을 스킵한다.
                if (!String.IsNullOrEmpty(task.MetaTags))
                {
                    try
                    {
                        MetaTagHelper tagHelper = new MetaTagHelper(task.MetaTags);
                        if (tagHelper.HasOfflineSkipTag())
                        {
                            bool bSkip = false;
                            try
                            {
                                var func = new Func<bool>(() =>
                                {
                                    try
                                    {
                                        /// 서버에게 이벤트 종료 메시지 날리기
                                        IAgentCmdReceiver agentCmdReceiver = (IAgentCmdReceiver)Activator.GetObject(
                                            typeof(IAgentCmdReceiver), "tcp://127.0.0.1:1888/CmdReceiverHost/rt");

                                        bSkip = !agentCmdReceiver.IsOnlineToServer();

                                        return true;
                                    }
                                    catch (Exception ex) { logger.Error(ex.ToString()); }

                                    return false;
                                });

                                if (!MethodHelper.Timeout(func, 5000))
                                {
                                    logger.Error("PlayAgent CmdReceiver is not responsed!");
                                }
                            }
                            catch { bSkip = true; }
                            if (bSkip)
                            {
                                logger.Info("Offline 이므로 재생이 스킵됩니다.");
                                return false;
                            }
                        }
                    }
                    catch { }
                }
                #endregion

                logger.Info("[" + task.description + " (" + task.uuid + ")] will be started.");

				if (CheckCondition)
				{
					if (!CheckDayOfWeek(task.daysofweek))
					{
						logger.Info("[" + task.description + " (" + task.uuid + ")(Day Of Week : " + task.daysofweek + ")] invaild Day of week.");
						return false;
					}
                }

                #region DEBUG 메모리 체크
                logger.Debug("현재 메모리 사용량: {0}%, 현재 CPU 사용율: {1}%", config.MemoryUsage, config.CpuRate);
                #endregion

                String sFilePath = config.AppData + "xaml\\" + task.duuid.ToString() + "\\program.xml";
				logger.Info("Loading [" + sFilePath + "]");

				DigitalSignage.PlayAgent.Task tempTask = prevScreen;
				prevScreen = currentScreen;
				currentScreen = task;

                #region 재생 시작 전 작업
                ScreenStartInfoInSchedule s = DigitalSignage.Common.ScreenStartInfoInSchedule.GetInstance;
                s.DownloadID = task.duuid.ToString();
                s.GroupScheduleID = task.guuid.ToString();
                s.ScheduleID = task.uuid.ToString();
                s.ScheduleName = task.description;
                s.ScheduleDuration = task.duration;
                if (IsSetStartDT)
                {
                    /// 싱크 스케줄인 경우 시작 딜레이 추가
                    if (task.IsSyncTask) s.ScheduleStartDT = s.ScreenStartDT = DateTime.Now + TimeSpan.FromMilliseconds(Properties.Settings.Default.PauseStartingDelayInSync);
                    else s.ScheduleStartDT = s.ScreenStartDT = DateTime.Now;
                }
                s.ShowFlag = task.show_flag;
                s.SyncThreshold = Properties.Settings.Default.SeekThreshold;
                s.IsUsingSeek = Properties.Settings.Default.IsUsingSeek;
                s.IsUsingFirstSeek = Properties.Settings.Default.IsUsingFirstSeek;
                s.DueTime = Properties.Settings.Default.DueTime;
                s.AdjustValue = Properties.Settings.Default.AdjustValue;
                if (s.IsSyncTask)
                {
                    s.PauseInStarting = Properties.Settings.Default.PauseStartingDelayInSync;
                }
                else s.PauseInStarting = 0;
  
                /// TCP 싱크 호출!
                if (task.IsSyncMaster)
                {
                    _syncObject.SendToSlaves(s.GroupScheduleID.ToString(), s.ScreenStartDT.ToString("yyyyMMddHHmmssfff"));
                    logger.Debug("동기화 마스터 시작: 나({0}), GUUID({1})", s.ScreenStartDT.ToString("yyyyMMddHHmmssfff"), s.GroupScheduleID.ToString());
                }


                #endregion

                if (parent.loadProject(sFilePath))
				{
					logger.Debug("Screen Load Finishied!");
                    
                    //if (task.type == TaskCommands.CmdEventSchedule) this.EventID = task.temp_id;

//					currentScreen = task;

					PlayerStatus = PlayStatus.Played;
					return true;
				}

				currentScreen = prevScreen;
				prevScreen = tempTask;
				return false;
			}
			else
			{
				//	unload screen

                #region 재생 시작 전 작업
                ScreenStartInfoInSchedule s = DigitalSignage.Common.ScreenStartInfoInSchedule.GetInstance;
                s.DownloadID = String.Empty;
                s.GroupScheduleID = String.Empty;
                s.ScheduleID = String.Empty;
                s.ScheduleName = String.Empty;
                s.ScheduleDuration = 0;
                if (IsSetStartDT) s.ScheduleStartDT = s.ScreenStartDT = DateTime.Now;
                s.ShowFlag = 0;
                #endregion

				parent.loadProject("");
				prevScreen = currentScreen;
				currentScreen = null;
				return false;
			}
		}

        /// <summary>
        /// 스크린를 즉시 재생한다.
        /// </summary>
        /// <param name="task">재생할 스크린</param>
        /// <param name="filepath">재생할 경로</param>
        /// <param name="IsSetStartDT">시작 시간을 넣을지 여부</param>
        /// <returns></returns>
        private bool PlayScreenTask(DigitalSignage.PlayAgent.Task task, string filepath, bool IsSetStartDT = true)
        {
            if (!String.IsNullOrEmpty(filepath))
            {
                #region 오프라인 스킵 여부 조회
                /// 오프라인 메타태그를 조회하여 오프라인 시 재생을 스킵한다.
                if (!String.IsNullOrEmpty(task.MetaTags))
                {
                    try
                    {
                        MetaTagHelper tagHelper = new MetaTagHelper(task.MetaTags);
                        if (tagHelper.HasOfflineSkipTag())
                        {
                            bool bSkip = false;
                            try
                            {
                                var func = new Func<bool>(() =>
                                {
                                    try
                                    {
                                        /// 서버에게 이벤트 종료 메시지 날리기
                                        IAgentCmdReceiver agentCmdReceiver = (IAgentCmdReceiver)Activator.GetObject(
                                            typeof(IAgentCmdReceiver), "tcp://127.0.0.1:1888/CmdReceiverHost/rt");

                                        bSkip = !agentCmdReceiver.IsOnlineToServer();

                                        return true;
                                    }
                                    catch (Exception ex) { logger.Error(ex.ToString()); }

                                    return false;
                                });

                                if (!MethodHelper.Timeout(func, 5000))
                                {
                                    logger.Error("PlayAgent CmdReceiver is not responsed!");
                                }


                            }
                            catch { bSkip = true; }

                            if (bSkip)
                            {
                                logger.Info("Offline 이므로 재생이 스킵됩니다.");
                                return false;
                            }
                        }
                    }
                    catch { }
                }
                #endregion

                #region DEBUG 메모리 체크
                logger.Debug("현재 메모리 사용량: {0}%", config.MemoryUsage);
                #endregion

                logger.Info("Loading [" + filepath + "]");

                DigitalSignage.PlayAgent.Task tempTask = prevScreen;
                prevScreen = currentScreen;
                currentScreen = task;

                #region 재생 시작 전 작업
                ScreenStartInfoInSchedule s = DigitalSignage.Common.ScreenStartInfoInSchedule.GetInstance;
                s.DownloadID = task.duuid.ToString();
                s.GroupScheduleID = task.guuid.ToString();
                s.ScheduleID = task.uuid.ToString();
                s.ScheduleName = task.description;
                s.ScheduleDuration = task.duration;
                if (IsSetStartDT)
                {
                    /// 싱크 스케줄인 경우 시작 딜레이 추가
                    if (task.IsSyncTask) s.ScheduleStartDT = s.ScreenStartDT = DateTime.Now + TimeSpan.FromMilliseconds(Properties.Settings.Default.PauseStartingDelayInSync);
                    else s.ScheduleStartDT = s.ScreenStartDT = DateTime.Now;
                } 
                s.ShowFlag = task.show_flag;
                s.SyncThreshold = Properties.Settings.Default.SeekThreshold;
                s.IsUsingSeek = Properties.Settings.Default.IsUsingSeek;
                s.IsUsingFirstSeek = Properties.Settings.Default.IsUsingFirstSeek;
                s.DueTime = Properties.Settings.Default.DueTime;
                s.AdjustValue = Properties.Settings.Default.AdjustValue;
                if (s.IsSyncTask)
                {
                    s.PauseInStarting = Properties.Settings.Default.PauseStartingDelayInSync;
                }
                else s.PauseInStarting = 0;

                /// TCP 싱크 호출!
                if (task.IsSyncMaster)
                {
                    _syncObject.SendToSlaves(s.GroupScheduleID.ToString(), s.ScreenStartDT.ToString("yyyyMMddHHmmssfff"));
                    logger.Debug("동기화 마스터 시작: 나({0}), GUUID({1})", s.ScreenStartDT.ToString("yyyyMMddHHmmssfff"), s.GroupScheduleID.ToString());
                }

                #endregion

                if (parent.loadProject(filepath))
                {
                    logger.Debug("Screen Load Finishied!");

                    return true;
                }

                currentScreen = prevScreen;
                prevScreen = tempTask;
                return false;
            }
            else
            {
                #region 재생 시작 전 작업
                ScreenStartInfoInSchedule s = DigitalSignage.Common.ScreenStartInfoInSchedule.GetInstance;
                s.DownloadID = String.Empty;
                s.GroupScheduleID = String.Empty;
                s.ScheduleID = String.Empty;
                s.ScheduleName = String.Empty;
                s.ScheduleDuration = 0;
                if (IsSetStartDT) s.ScheduleStartDT = s.ScreenStartDT = DateTime.Now;
                s.ShowFlag = 0;
                #endregion

                //	unload screen
                parent.loadProject("");
                prevScreen = currentScreen;
                currentScreen = null;
                return false;
            }
        }

		/// <summary>
		/// 다음 자막을 재생한다.
		/// </summary>
		/// <returns></returns>
		public bool PlayNextSubtitle()
		{
			if (_subtitlePlaylist != null && _subtitlePlaylist.Count > 0)
			{
				PlayAgent.Task temp = previewSub;
				previewSub = currentSub;
				currentSub =_subtitlePlaylist.Next;
				bool bRet = PlaySubtitleTask(currentSub);
				if (bRet)
				{
					return true;
				}
				else
				{
					currentSub = previewSub;
					previewSub = temp;
//					currentSub = null;
				}
				return bRet;
			}
			else
			{
				previewSub = currentSub;
				PlaySubtitleTask(null);
				currentSub = null;

				return true;
			}
		}

		private bool PlaySubtitleTask(DigitalSignage.PlayAgent.Task task)
		{
			if(task != null)
			{
				logger.Info("[" + task.description + " (" + task.uuid + ")] will be started.");
				if (!CheckDayOfWeek(task.daysofweek))
				{
					logger.Info("[" + task.description + " (" + task.uuid + ")(Day Of Week : " + task.daysofweek + ")] invaild Day of week.");
					return false;
				}

				String sFilePath = config.AppData + "xaml\\" + task.duuid.ToString() + "\\program.xml";
				logger.Info("Loading [" + sFilePath + "]");

				previewSub = currentSub;
				currentSub = task;

				if (parent.setSubtitle(sFilePath))
				{
					return true;
				}
				else
				{
					currentSub = previewSub;
					return false;
				}
			}
			else
			{
				parent.setSubtitle(""); //unload project
				previewSub = currentSub;
				currentSub = null;
				return false;
			}
		}
		/// <summary>
		/// Task 리스트 중 endtime이 시작시간과 연결되어있는지 검사한다.
		/// </summary>
		/// <param name="tasks">검사할 Task 리스트</param>
		/// <param name="endtime">기준이될 끝 시간</param>
		/// <returns>만약 true라면 연결된 Task가 있다는 것이고, false면 없다는 것.</returns>
		private bool IsConnectedScreenTask(DigitalSignage.PlayAgent.Task[] tasks, long endtime)
		{
			foreach (DigitalSignage.PlayAgent.Task task in tasks)
			{
				if (task.type == TaskCommands.CmdScreen && task.TaskStart == endtime)
					return true;
			}
			return false;
		}

		/// <summary>
		/// Task가 중복되는 uuid를 가질 수 없기 때문에 중복된다면 지우고 추가해준다.
		/// </summary>
		/// <param name="task">추가할 Task</param>
		/// <returns>Reserved</returns>
		private int InsertOrReplaceTask(DigitalSignage.PlayAgent.Task task)
		{
			int nRet = -1;
			try
			{
				if (-1 == (nRet = config.localTasksList.AddTask(task)))
				{
					////	추가가 되지않음.
					config.localTasksList.CleanTask(task.uuid);
					//	다시 추가 시도
					nRet = config.localTasksList.AddTask(task);
				}
			}
			catch(Exception e)
			{
				logger.Error(e.Message);
			}
			return nRet;
		}

		/// <summary>
		/// local 저장소를 정리한다.
		/// </summary>
        private void clearStorage()
        {
            try
            {
                Cleaner cleaner = new Cleaner();
                cleaner.deleteLostProjects();
                cleaner.deleteOldProjects();
            }
            catch (Exception e)
            {
                logger.Error(e.ToString());
            }
        }

		/// <summary>
		/// 요일 체크
		/// </summary>
		/// <param name="dayofweek"></param>
		/// <returns></returns>
		private bool CheckDayOfWeek(int dayofweek)
		{
			DayOfWeek dow = DateTime.Today.DayOfWeek;
			if (dow == DayOfWeek.Monday)
				return 0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_MON);
			else if (dow == DayOfWeek.Tuesday)
				return 0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_TUE);
			else if (dow == DayOfWeek.Wednesday)
				return 0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_WED);
			else if (dow == DayOfWeek.Thursday)
				return 0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_THU);
			else if (dow == DayOfWeek.Friday)
				return 0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_FRI);
			else if (dow == DayOfWeek.Saturday)
				return 0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_SAT);
			else if (dow == DayOfWeek.Sunday)
				return 0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_SUN);
			else
			{
				logger.Error("Exception : invalid DayOfWeek. : " + dayofweek.ToString());
			}
			return false;
		}

		/// <summary>
		/// 현재 시간이 TimeOfDay에 포함되는지
		/// </summary>
		/// <param name="nStartTime"></param>
		/// <param name="nEndTime"></param>
		/// <returns></returns>
		private bool CheckContainTimeofDay(int nStartTime, int nEndTime)
		{
			//	둘다 같은 값이라면 Whole Day로 간주
			if (nStartTime == nEndTime) return true;

			DateTime dtLocal = DateTime.Now;
			int nCurr = dtLocal.Second + (dtLocal.Minute * 60) + (dtLocal.Hour * 3600);

			if(nStartTime < nEndTime)
			{
				//	시간대가 하루에 국한되는 경우

				return nCurr >= nStartTime && nCurr < nEndTime;
			}
			else
			{
				//	시간대가 오늘부터 다음 날로 넘어 간 경우
				return nCurr < nStartTime || nCurr >= nEndTime;				
			}
		}


		/// <summary>
		/// 다운로드 완료되었는지 체크
		/// </summary>
		/// <param name="duuid"></param>
		/// <returns></returns>
		bool IsDownloadComplete(string duuid)
		{
			try
			{
				string sDest = config.AppData + "xaml\\" + duuid + "\\downloaded";

				System.IO.FileInfo fi = new System.IO.FileInfo(sDest);

				if (fi == null) return false;

				return fi.Exists;
			}
			catch (Exception e)
			{
				logger.Error(e.ToString());
				return false;
			}
		}
		#region Logging

		private void SubtitleStarted(string sName)
		{
			try
			{
				if (client_logsTA == null)
					client_logsTA = config.logdbLayer.CreateClientLogDA();

				long currTicks = TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime());
                client_logsTA.InsertLogs(Config.GetConfig.PlayerId, currentSub.uuid.ToString(), currentSub.description, sName, Config.MakeTaskLogType(false, currentSub.IsEventPlay, LogType.LogType_Processing, TaskCommands.CmdSubtitles, 0), 0, currTicks, currTicks, null, null);

			}
			catch (Exception e)
			{
				logger.Error(e.Message);
			}
		}

		private void SubtitleEnded(string sName)
		{
			try
			{
				if (client_logsTA == null)
					client_logsTA = config.logdbLayer.CreateClientLogDA();

				long currTicks = TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime());
                client_logsTA.InsertLogs(Config.GetConfig.PlayerId, previewSub.uuid.ToString(), previewSub.description, sName, Config.MakeTaskLogType(false, currentSub.IsEventPlay, LogType.LogType_Proceed, TaskCommands.CmdSubtitles, 0), 0, currTicks, currTicks, null, null);
			}
			catch (Exception e)
			{
				logger.Error(e.Message);
			}
		}

		private void SubtitleProcceed(string sName, long start_dt, long end_dt)
		{
			try
			{
				if (client_logsTA == null)
					client_logsTA = config.logdbLayer.CreateClientLogDA();

                client_logsTA.InsertLogs(Config.GetConfig.PlayerId, currentSub.uuid.ToString(), currentSub.description, sName, Config.MakeTaskLogType(false, currentSub.IsEventPlay, LogType.LogType_Proceed, TaskCommands.CmdSubtitles, 0), 0, start_dt, end_dt, null, null);

			}
			catch (Exception e)
			{
				logger.Error(e.Message);
			}
		}


		private void SubtitleErrorOccurred(string sName, int code, string description)
		{
			try
			{
				if (client_logsTA == null)
					client_logsTA = config.logdbLayer.CreateClientLogDA();

				long currTicks = TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime());
                client_logsTA.InsertLogs(Config.GetConfig.PlayerId, currentSub.uuid.ToString(), currentSub.description, String.Format("{0}: {1}", sName, description), Config.MakeTaskLogType(false, currentSub.IsEventPlay, LogType.LogType_Failed, TaskCommands.CmdSubtitles, 0), code, currTicks, currTicks, null, null);
			}
			catch (Exception e)
			{
				logger.Error(e.Message);
			}
		}

		private void ScreenStarted(string sName)
		{
			try
			{
				logger.Debug("Screend Started!!!");

				//if (client_logsTA == null)
				//	client_logsTA = config.logdbLayer.CreateClientLogDA();

                string sScreenName = String.Empty;
                long lScreenID = -1;
                long? lRelayID = null;
                if (currentScreen.IsEventPlay) lRelayID = this.EventID;

                try
                {
					String[] arrScreen = sName.Split('\\');
					if (arrScreen.Length == 1)
					{
						arrScreen = sName.Split('.');

						lScreenID = Convert.ToInt64(arrScreen[0]);
						sScreenName = sName;
					}
					else
					{
						lScreenID = Convert.ToInt64(arrScreen[0]);
						sScreenName = arrScreen[1];
					}
				}
                catch { sScreenName = sName; }

				try
				{
					/// 프로모션 Gathering
					if (_bGatheringPromotionTasks && (currentScreen.MetaTags.Contains("응모") || currentScreen.MetaTags.Contains("쿠폰")))
					{
						queuePromotion.Add(new Promotion(lScreenID, currentScreen));
						logger.Debug("Promotion Added: " + lScreenID);
					}
				}
				catch { }

				//long currTicks = TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime());
                //client_logsTA.InsertLogs(Config.GetConfig.PlayerId, currentScreen.uuid.ToString(), currentScreen.description, sScreenName, Config.MakeTaskLogType(currentScreen.isAd, currentScreen.IsEventPlay, LogType.LogType_Processing, currentScreen.type, 0), 0, currTicks, currTicks, lScreenID, lRelayID);

			}
			catch (Exception e)
			{
				logger.Error(e.Message);
			}
		}

		private void ScreenEnded(string sName)
		{
			try
			{
				//if (client_logsTA == null)
				//	client_logsTA = config.logdbLayer.CreateClientLogDA();

                string sScreenName = String.Empty;
                long lScreenID = -1;
                long? lRelayID = null;
                if (prevScreen.IsEventPlay) lRelayID = this.EventID;

                try
                {
					String[] arrScreen = sName.Split('\\');
					if (arrScreen.Length == 1)
					{
						arrScreen = sName.Split('.');

						lScreenID = Convert.ToInt64(arrScreen[0]);
						sScreenName = sName;
					}
					else
					{
						lScreenID = Convert.ToInt64(arrScreen[0]);
						sScreenName = arrScreen[1];
					}
				}
                catch { sScreenName = sName; }

				//long currTicks = TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime());
                //client_logsTA.InsertLogs(Config.GetConfig.PlayerId, prevScreen.uuid.ToString(), prevScreen.description, sScreenName, Config.MakeTaskLogType(prevScreen.isAd, prevScreen.IsEventPlay, LogType.LogType_Proceed, prevScreen.type, 0), 0, currTicks, currTicks, lScreenID, lRelayID);
			}
			catch (Exception e)
			{
				logger.Error(e.Message);
			}
		}

		private void ScreenProcceed(string sName, long start_dt, long end_dt)
		{
 
			try
			{
				if (client_logsTA == null)
					client_logsTA = config.logdbLayer.CreateClientLogDA();

                string sScreenName = String.Empty;
                long lScreenID = -1;
                long? lRelayID = null;
                if (prevScreen.IsEventPlay) lRelayID = this.EventID;

                try
                {
                    String[] arrScreen = sName.Split('\\');
					if (arrScreen.Length == 1)
					{
						arrScreen = sName.Split('.');

						lScreenID = Convert.ToInt64(arrScreen[0]);
						sScreenName = sName;
 					}
					else
					{
						lScreenID = Convert.ToInt64(arrScreen[0]);
						sScreenName = arrScreen[1];
					}
                }
                catch { sScreenName = sName; }
                
                client_logsTA.InsertLogs(Config.GetConfig.PlayerId, prevScreen.uuid.ToString(), prevScreen.description, sScreenName, Config.MakeTaskLogType(prevScreen.isAd, prevScreen.IsEventPlay, LogType.LogType_Proceed, prevScreen.type, 0), 0, start_dt, end_dt, lScreenID, lRelayID);

			}
			catch (Exception e)
			{
				logger.Error(e.Message);
			}
		}

		private void ScreenErrorOccurred(string sName, int code, string description)
		{
			try
			{
				if (client_logsTA == null)
					client_logsTA = config.logdbLayer.CreateClientLogDA();

                string sScreenName = String.Empty;
                long lScreenID = -1;
                long? lRelayID = null;
                if(currentScreen.IsEventPlay) lRelayID = this.EventID;

                try
                {
                    String[] arrScreen = sName.Split('\\');
                    lScreenID = Convert.ToInt64(arrScreen[0]);
                    sScreenName = arrScreen[1];
                }
                catch { sScreenName = sName; }

				long currTicks = TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime());
                client_logsTA.InsertLogs(Config.GetConfig.PlayerId, currentScreen.uuid.ToString(), currentScreen.description, String.Format("{0} : {1}", sScreenName, description), Config.MakeTaskLogType(currentScreen.isAd, currentScreen.IsEventPlay, LogType.LogType_Failed, currentScreen.type, 0), code, currTicks, currTicks, lScreenID, lRelayID);
			}
			catch (Exception e)
			{
				logger.Error(e.Message);
			}

			try
			{
				/*
				DigitalSignage.PlayAgent.Task t = new DigitalSignage.PlayAgent.Task();
				t.pid = config.PlayerId;
				t.gid = config.GroupId;
				t.type = TaskCommands.CmdNextDefaultProgram;
				t.uuid = DigitalSignage.PlayAgent.Task.TaskDefaultScreen;
				t.TaskStart = TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()) + 1;
				t.TaskEnd = t.TaskStart + 5;
				t.description = "CmdNextDefaultProgram";
				t.duration = 10;

				InsertOrReplaceTask(t);
				 * */
			}
			catch{}
		}

		private void ControlSchedStarted(PlayAgent.Task cTask)
		{
			try
			{
				if (client_logsTA == null)
					client_logsTA = config.logdbLayer.CreateClientLogDA();

				long currTicks = TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime());
				client_logsTA.InsertLogs(Config.GetConfig.PlayerId, cTask.uuid.ToString(), cTask.description, "", Config.MakeTaskLogType(false, false, LogType.LogType_Processing, TaskCommands.CmdControlSchedule, 0), 0, currTicks, currTicks, null, null);

			}
			catch (Exception e)
			{
				logger.Error(e.Message);
			}
		}

		private void ControlSchedEnded(PlayAgent.Task cTask)
		{
			try
			{
				if (client_logsTA == null)
					client_logsTA = config.logdbLayer.CreateClientLogDA();

				long currTicks = TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime());
                client_logsTA.InsertLogs(Config.GetConfig.PlayerId, cTask.uuid.ToString(), cTask.description, "", Config.MakeTaskLogType(false, false, LogType.LogType_Proceed, TaskCommands.CmdControlSchedule, 0), 0, currTicks, currTicks, null, null);
			}
			catch (Exception e)
			{
				logger.Error(e.Message);
			}
		}

		private void ControlSchedErrorOccurred(PlayAgent.Task cTask, int code, string description)
		{
			try
			{
				if (client_logsTA == null)
					client_logsTA = config.logdbLayer.CreateClientLogDA();

				long currTicks = TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime());
                client_logsTA.InsertLogs(Config.GetConfig.PlayerId, cTask.uuid.ToString(), cTask.description, description, Config.MakeTaskLogType(false, false, LogType.LogType_Failed, TaskCommands.CmdControlSchedule, 0), code, currTicks, currTicks, null, null);
			}
			catch (Exception e)
			{
				logger.Error(e.Message);
			}

		}


		private void WriteInfo(string title, string uuid, TaskCommands type, String message)
		{
			try
			{
				if (client_logsTA == null)
					client_logsTA = config.logdbLayer.CreateClientLogDA();

				long currTicks = TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime());
                client_logsTA.InsertLogs(Config.GetConfig.PlayerId, uuid, title, message, Config.MakeTaskLogType(false, false, LogType.LogType_Proceed, type, 0), 0, currTicks, currTicks, null, null);
			}
			catch (Exception e)
			{
				logger.Error(e.Message);
			}
		}

		private void WriteError(string title, string uuid, TaskCommands type, ErrorCodes code, String message)
		{
			try
			{
				if (client_logsTA == null)
					client_logsTA = config.logdbLayer.CreateClientLogDA();

				long currTicks = TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime());
                client_logsTA.InsertLogs(Config.GetConfig.PlayerId, uuid, title, message, Config.MakeTaskLogType(false, false, LogType.LogType_Failed, type, 0), (int)code, currTicks, currTicks, null, null);
			}
			catch (Exception e)
			{
				logger.Error(e.Message);
			}
		}
		#endregion
	}
}
