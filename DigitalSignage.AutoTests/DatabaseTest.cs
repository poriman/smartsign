﻿
using DigitalSignage.ServerDatabase;
using DigitalSignage.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using System;
using System.Security.Cryptography;
using System.Text;

namespace DigitalSignage.AutoTests
{
    
    
    /// <summary>
    ///This is a test class for CleanerTest and is intended
    ///to contain all CleanerTest Unit Tests
    ///</summary>
    [TestClass()]
	public class DatabaseTest
	{

// 		string connection_string = @"Provider=sqloledb; server=211.192.178.115; database=NEDatabase;uid=NE_user;pwd=ivisionNE;";
// 		string connection_string = @"dsn=ivisionNE;uid=NE_user;pwd=ivisionNE;";

 // 		string connection_string = @"dsn=MSiVision;uid=NE_user;pwd=ivisionNE;";

//		string connection_string = @"Driver=SQLite3 ODBC Driver;Database=C:\playlist.db3";
//         string connection_string = @"dsn=MyiVision;uid=platform;pwd=dlalwl;";
//		string connection_string = @"DSN=iVisionGS;uid=sa;pwd=dlsxpffldks.;";
		string connection_string = @"DSN=iVisionOracle;Dbq=PLATFORM;uid=SCOTT;pwd=tiger;";

		private TestContext testContextInstance;

		/// <summary>
		///Gets or sets the test context which provides
		///information about and functionality for the current test run.
		///</summary>
		public TestContext TestContext
		{
			get
			{
				return testContextInstance;
			}
			set
			{
				testContextInstance = value;
			}
		}

		#region Additional test attributes
		// 
		//You can use the following additional attributes as you write your tests:
		//
		//Use ClassInitialize to run code before running the first test in the class
		//[ClassInitialize()]
		//public static void MyClassInitialize(TestContext testContext)
		//{
		//}
		//
		//Use ClassCleanup to run code after all tests in a class have run
		//[ClassCleanup()]
		//public static void MyClassCleanup()
		//{
		//}
		//
		//Use TestInitialize to run code before running each test
		//[TestInitialize()]
		//public void MyTestInitialize()
		//{
		//}
		//
		//Use TestCleanup to run code after each test has run
		//[TestCleanup()]
		//public void MyTestCleanup()
		//{
		//}
		//
		#endregion



		/// <summary>
		///A test for DBConnectionTest
		///</summary>
        [TestMethod()]
		public void DBConnectionTest()
		{
/*
			DigitalSignage.SchedulerService.CacheObject ins = DigitalSignage.SchedulerService.CacheObject.GetInstance;
			ins.Initialize(connection_string);

			System.Xml.XmlNode node222 = ins.FindUserNode("admin", "21232f297a57a5a743894a0e4a801fc3");
			if (node222 == null)
				node222 = null;
			System.Xml.XmlNode node = ins.FindGroupNode("0002");
			System.Xml.XmlNode node2 = ins.FindPlayerNode("0010005");
			long tspt = DateTime.UtcNow.Ticks;

			System.Xml.XmlNodeList list1 = ins.FindModifiedGroups(tspt);
			int ncount1 = list1.Count;
			System.Xml.XmlNodeList list2 = ins.FindModifiedPlayers(tspt);
			int ncount2 = list2.Count;
			node.Attributes["_timestamp"].Value = DateTime.UtcNow.Ticks.ToString();
			node2.Attributes["_timestamp"].Value = DateTime.UtcNow.Ticks.ToString();
			System.Xml.XmlNodeList list3 = ins.FindModifiedGroups(tspt);
			int ncount3 = list3.Count;
			System.Xml.XmlNodeList list4 = ins.FindModifiedPlayers(tspt);
			int ncount4 = list4.Count;

			string sNode = node.OuterXml;
			string sNode2 = node2.OuterXml;
			*/
			DigitalSignage.ServerDatabase.DBLayer dblayer = new DigitalSignage.ServerDatabase.DBLayer();
			dblayer.openDB(connection_string);
			dblayer.InitializeConnectionPool(10);
			String sQuery = String.Empty;

			using (System.Data.Odbc.OdbcConnection con = dblayer.GetAvailableConnection())
			{
				
				 sQuery = String.Format("SELECT  id, name, hash, parent_id, role_ids, is_enabled FROM     users WHERE  (parent_id IS NULL) OR (parent_id = - 1)");

				 using (ServerDatabase.ServerDataSet.usersDataTable dt = new ServerDataSet.usersDataTable())
				 {

					 using (System.Data.Odbc.OdbcCommand comm = new System.Data.Odbc.OdbcCommand(sQuery, con))
					 {
						 using (System.Data.Odbc.OdbcDataReader dr = comm.ExecuteReader())
						 {

							 dt.Load(dr);



							 dr.Close();
							 dr.Dispose();
						 }

						 comm.Dispose();
					 }
				 }

/*				 using (ServerDatabase.ServerDataSet.rolesDataTable dt = new ServerDataSet.rolesDataTable())
				 {

					 using (System.Data.Odbc.OdbcCommand comm = new System.Data.Odbc.OdbcCommand(sQuery, con))
					 {
						 comm.Parameters.Add(new System.Data.Odbc.OdbcParameter("role_id", System.Data.Odbc.OdbcType.BigInt, 19, "role_id"));
						 comm.Parameters["role_id"].Value = 1;

						 using (System.Data.Odbc.OdbcDataReader dr = comm.ExecuteReader())
						 {

							 dt.Load(dr);



							 dr.Close();
							 dr.Dispose();
						 }

						 comm.Dispose();
					 }
				 }
 */
			}

			System.Xml.XmlDocument doc1 = new System.Xml.XmlDocument();
			SchedulerService.GenNetworkTree.ServerDBLayer = dblayer;
			System.Xml.XmlDocument doc = SchedulerService.GenNetworkTree.GenerateNetworkTree(doc1, "0000000000000");

			
			DigitalSignage.ServerDatabase.ServerDataSetTableAdapters.role_perm_ASNTableAdapter da = dblayer.Createrole_perm_ASN_DA();
			DigitalSignage.ServerDatabase.ServerDataSet.role_perm_ASNDataTable ta = da.GetPermissionsByRoleID(1);

			foreach (DigitalSignage.ServerDatabase.ServerDataSet.role_perm_ASNRow row in ta.Rows)
			{
				string s = row.ToString();

				int nCount = ta.Rows.Count;
			}
		}

		/// <summary>
		/// 패스워드 해시값 추출
		/// </summary>
		/// <param name="origin"></param>
		/// <returns></returns>
		private string GetHash(string origin)
		{
			if (String.IsNullOrEmpty(origin))
				return origin;

			StringBuilder sb = new StringBuilder();
			MD5 md5 = new MD5CryptoServiceProvider();
			Byte[] original;
			Byte[] encoded;
			original = ASCIIEncoding.Default.GetBytes(origin);
			encoded = md5.ComputeHash(original);
			foreach (byte hex in encoded) sb.Append(hex.ToString("x2"));
			return sb.ToString().ToLower();
		}

		/// <summary>
		///A test for DBConnectionTest
		///</summary>
        [TestMethod()]
        public void DBConnectionTest2()
        {
            string sMetaTags = "665562389757812500|0190619990004625|Y|1|72|02|135280|02|135280|000000000392000";
            int nFirstSep = sMetaTags.IndexOf('|');
            string sID = sMetaTags.Substring(0, nFirstSep).TrimEnd(new char[] {' ', '|'});
            MetaTagHelper helper = new MetaTagHelper("1000|12345|L1", sMetaTags.Substring(nFirstSep + 1));

            IContentsList serverContentsList = null;
            IResultDataList serverResultDataList = null;
           // IDataCacheList test = null;

            serverContentsList = (IContentsList)Activator.GetObject(
                typeof(IContentsList), "tcp://119.206.205.147:888/ContentsHost/rt");

			serverResultDataList = (IResultDataList)Activator.GetObject(
				typeof(IResultDataList), "tcp://119.206.205.147:888/ResultDataHost/rt");

			IDataCacheList serverCacheList = null;

			serverCacheList = (IDataCacheList)Activator.GetObject(
				typeof(IDataCacheList), "tcp://119.206.205.147:888/DataCacheHost/rt");


			IDataInfoList serverDataInfoList = null;
			serverDataInfoList = (IDataInfoList)Activator.GetObject(
	typeof(IDataInfoList), "tcp://119.206.205.147:888/DataInfoHost/rt");

            DBLayer dblayer = new DBLayer(); // TODO: Initialize to an appropriate value
            int expected = 0;
            int actual;
            // 			actual = dblayer.openDB(@"Provider=OleSQLite.SQLiteSource; Data Source=C:\DigitalSignage\playlist.db3");
            actual = dblayer.openDB(connection_string);
            dblayer.InitializeConnectionPool(1);

// 			ServerDatabase.ServerDataSetTableAdapters.DataInfoTableAdapter dta = dblayer.CreateDataInfoConnectDA();
// 			ServerDataSet.DataInfoDataTable dt = dta.GetDatabyUserid(1, "201005201421450078");
			string sReturn = serverCacheList.GetUserNode("admin", GetHash("dlsxpffldks."));
// 			string sReturn = serverCacheList.GetNetworkTreeByUser("admin");
			Assert.IsTrue(!String.IsNullOrEmpty(sReturn));

            //ServerDatabase.ServerDataSetTableAdapters.Data_FieldTableAdapter dft = dblayer.CreateDataFieldConnectDA();
            //ServerDatabase.ServerDataSet.Data_FieldDataTable table = dft.GetDatabyTemplateid("201003251603381044");
            //int count = table.Rows.Count;//201004081710380267

            //ServerDatabase.ServerDataSetTableAdapters.resultdatasTableAdapter dta = dblayer.CreateResultDataConnectDA();
            expected = 1;
            //actual = dta.AddResultDataValues(20100409, "0003010", "201004081710380267", "201003251603381044", "테스트1", "01|02|02|02|02|02|02|02|02|02|02|02|02|02|02|02|02|02|02|02");
            //ServerDataSet.resultdatasDataTable table = serverResultDataList.GetResultData(201004011, "0003010", "201004081710380267");//dta.GetResultDatabyFieldid
            //int count = table.Rows.Count;
            //serverResultDataList.AddResultData(20100412, "0003010", "201004081710380267", "15|15|05|04|05|06|07|08|09|10|11|12|13|14|15|16|17|18|19|20");
            //ServerDataSet.resultdatasDataTable _table = dta.CallResultData(20100411, "0003010", "201004081710380267");
            //int count = _table.Rows.Count;
            //dta.UpdateResData("11|12|03|04|05|06|07|08|09|10|11|12|13|14|15|16|17|18|19|20", 20100411, "0003010", "201004081710380267");
            //dta.Dispose();
           

           // ServerDatabase.ServerDataSetTableAdapters.ContentsTableAdapter da = dblayer.CreateContentsConnectDA();

            //expected = 1;
            //string id = "201002221439261762";
            //string type = "T";
            //string value = "수정 5555";
            //string _null = "";
           // actual = da.EditContents("중국산", "", "", "201004151347077911");//.AddDatainfo("3", 5, "010101", "하이테스트");

            //serverContentsList.AddContets("201004151347077917", "201004151347073547", "T", "아아아", "", "", "", "201004141610109576");
            //if (false == serverContentsList.UpdateContents("국내산6", "", "", "201004151347077911"))
            //{
            //    string err = "";
            //}
            //da.Dispose();
            //Assert.AreEqual(expected, actual);
            serverResultDataList.AddResultData(20100415, "0003020", "201004141612084960", "05|15|05|04|05|06|07|08|09|10|11|12|13|14|15|16|17|18|19|20");
            serverResultDataList.AddResultData(20100415, "0003020", "201004141612100828", "15|10|05|04|05|06|07|08|09|10|11|12|13|14|15|16|17|18|19|20");
            serverResultDataList.AddResultData(20100415, "0003020", "201004141612113513", "25|13|05|04|05|06|07|08|09|10|11|12|13|14|15|16|17|18|19|20");

            serverResultDataList.AddResultData(20100415, "0003021", "201004141612084960", "15|15|05|04|05|06|07|08|09|10|11|12|13|14|15|16|17|18|19|20");
            serverResultDataList.AddResultData(20100415, "0003021", "201004141612100828", "05|10|05|04|05|06|07|08|09|10|11|12|13|14|15|16|17|18|19|20");
            serverResultDataList.AddResultData(20100415, "0003021", "201004141612113513", "35|13|05|04|05|06|07|08|09|10|11|12|13|14|15|16|17|18|19|20");

            serverResultDataList.AddResultData(20100415, "0003022", "201004141612084960", "15|15|05|04|05|06|07|08|09|10|11|12|13|14|15|16|17|18|19|20");
            serverResultDataList.AddResultData(20100415, "0003022", "201004141612100828", "25|10|05|04|05|06|07|08|09|10|11|12|13|14|15|16|17|18|19|20");
            serverResultDataList.AddResultData(20100415, "0003022", "201004141612113513", "05|13|05|04|05|06|07|08|09|10|11|12|13|14|15|16|17|18|19|20");

            serverResultDataList.AddResultData(20100415, "0003023", "201004141612084960", "15|15|05|04|05|06|07|08|09|10|11|12|13|14|15|16|17|18|19|20");
            serverResultDataList.AddResultData(20100415, "0003023", "201004141612100828", "05|10|05|04|05|06|07|08|09|10|11|12|13|14|15|16|17|18|19|20");
            serverResultDataList.AddResultData(20100415, "0003023", "201004141612113513", "05|13|05|04|05|06|07|08|09|10|11|12|13|14|15|16|17|18|19|20");
            
            dblayer.CloseConnectionPool();
        }

// 		#region Player 관련
// 
// 		/// <summary>
// 		///A test for Player_update1
// 		///</summary>
// 		[TestMethod()]
// 		public void Player_add()
// 		{
// 
// 			int expected = 1;
// 			int actual;
// 
// 			DBLayer dblayer = new DBLayer();
// 			dblayer.openDB(connection_string);
// 
// 			actual = dblayer.CreatePlayersDA().AddPlayer("9999999", "0002", "유닛테스트중", "", "", 0, 0);
// 			Assert.AreEqual(expected, actual);
// 
// 		}
// 
// 		/// <summary>
// 		///A test for Player_update1
// 		///</summary>
// 		[TestMethod()]
// 		public void Player_update1()
// 		{
// 
// 			int expected = 1;
// 			int actual;
// 
// 			DBLayer dblayer = new DBLayer();
// 			dblayer.openDB(connection_string);
// 
// 			// 			actual = dblayer.CreatePlayersDA().Update1("0001", "name", "host", 0, 0, 0, 0, 0, 0, "0000001", "port", 0, 0, 0, 0, "all_d", "curr_d", "curr_scr", "curr_sub");
// 			// 			actual = dblayer.CreatePlayersDA().Update1("0001", "유닛테스트중_수정", "", 0, 0, 0, 0, 0, 0, "0000001", "", 0, 0, 0, 0, "", "", "", "");
// 			actual = dblayer.CreatePlayersDA().Update1("0001", "유닛테스트중_수정", "127.0.0.1", 0, 0, 0, 0, 0, "1234", "", "", "", "", 0, 0, 0, 0, 0, "0000001");
// 			Assert.AreEqual(expected, actual);
// 
// 		}
// 
// 		/// <summary>
// 		///A test for Player_status_on
// 		///</summary>
// 		[TestMethod()]
// 		public void Player_status_on()
// 		{
// 
// 			int expected = 1;
// 			int actual;
// 
// 			DBLayer dblayer = new DBLayer();
// 			dblayer.openDB(connection_string);
// 
// 			actual = dblayer.CreatePlayersDA().StateOnByPID("0000001");
// 			Assert.AreEqual(expected, actual);
// 
// 		}
// 
// 		/// <summary>
// 		///A test for Player_status_off
// 		///</summary>
// 		[TestMethod()]
// 		public void Player_status_off()
// 		{
// 
// 			int expected = 1;
// 			int actual;
// 
// 			DBLayer dblayer = new DBLayer();
// 			dblayer.openDB(connection_string);
// 
// 			actual = dblayer.CreatePlayersDA().StateOffByPID("0000001");
// 			Assert.AreEqual(expected, actual);
// 
// 		}
// 
// 		/// <summary>
// 		///A test for Player_reset_state
// 		///</summary>
// 		[TestMethod()]
// 		public void Player_reset_state()
// 		{
// 
// 			int expected = 0;
// 			int actual;
// 
// 			DBLayer dblayer = new DBLayer();
// 			dblayer.openDB(connection_string);
// 
// 			actual = dblayer.CreatePlayersDA().ResetState();
// 			Assert.AreNotEqual(expected, actual);
// 		}
// 
// 		/// <summary>
// 		///A test for Player_get_player_by_pid
// 		///</summary>
// 		[TestMethod()]
// 		public void Player_get_player_by_pid()
// 		{
// 
// 			int expected = 1;
// 			int actual;
// 
// 			DBLayer dblayer = new DBLayer();
// 			dblayer.openDB(connection_string);
// 
// 			actual = dblayer.CreatePlayersDA().GetPlayerByPID("0000001").Rows.Count;
// 			Assert.AreEqual(expected, actual);
// 		}
// 
// 		/// <summary>
// 		///A test for Player_get_player_by_gid
// 		///</summary>
// 		[TestMethod()]
// 		public void Player_get_player_by_gid()
// 		{
// 
// 			int expected = 0;
// 			int actual;
// 
// 			DBLayer dblayer = new DBLayer();
// 			dblayer.openDB(connection_string);
// 
// 			actual = dblayer.CreatePlayersDA().GetPlayersByGID("0001").Rows.Count;
// 			Assert.AreNotEqual(expected, actual);
// 		}
// 
// 		/// <summary>
// 		///A test for Player_change_status_by_pid
// 		///</summary>
// 		[TestMethod()]
// 		public void Player_change_status_by_pid()
// 		{
// 
// 			int expected = 1;
// 			int actual;
// 
// 			DBLayer dblayer = new DBLayer();
// 			dblayer.openDB(connection_string);
// 
// 			actual = dblayer.CreatePlayersDA().ChangeStatusInfoByPID(1, 0, 0, 0, 0, 0, 0, 0, "", "", "", "", "", "0000001");
// 			actual = dblayer.CreatePlayersDA().ChangeStatusInfoByPID(0, 0, 0, 0, 0, 0, 0, 0, "", "", "", "", "", "0000001");
// 			Assert.AreEqual(expected, actual);
// 		}
// 
// 		/// <summary>
// 		///A test for Player_change_group
// 		///</summary>
// 		[TestMethod()]
// 		public void Player_change_group()
// 		{
// 
// 			int expected = 1;
// 			int actual;
// 
// 			DBLayer dblayer = new DBLayer();
// 			dblayer.openDB(connection_string);
// 
        // 			actual = dblayer.CreatePlayersDA().ChangeGroup("0000000000000", "0002");
// 			Assert.AreEqual(expected, actual);
// 		}
// 
// 		/// <summary>
// 		///A test for Player_change_group
// 		///</summary>
// 		[TestMethod()]
// 		public void Player_delete()
// 		{
// 
// 			int expected = 1;
// 			int actual;
// 
// 			DBLayer dblayer = new DBLayer();
// 			dblayer.openDB(connection_string);
// 
// 			actual = dblayer.CreatePlayersDA().DeletePlayer("9999999");
// 			Assert.AreEqual(expected, actual);
// 		}
// 
// 		#endregion Player 관련
// 
// 		#region Group 관련
// 
// 		/// <summary>
// 		///A test for Group_add
// 		///</summary>
// 		[TestMethod()]
// 		public void Group_add()
// 		{
// 
// 			int expected = 1;
// 			int actual;
// 
// 			DBLayer dblayer = new DBLayer();
// 			dblayer.openDB(connection_string);
// 
// 			actual = dblayer.CreateGroupDA().AddGroup("9999", "0000000", "유닛테스트 그룹", "127.0.0.1", "test", "test", "");
// 			Assert.AreEqual(expected, actual);
// 		}
// 
// 		/// <summary>
// 		///A test for Group_delete
// 		///</summary>
// 		[TestMethod()]
// 		public void Group_delete()
// 		{
// 
// 			int expected = 1;
// 			int actual;
// 
// 			DBLayer dblayer = new DBLayer();
// 			dblayer.openDB(connection_string);
// 
// 			actual = dblayer.CreateGroupDA().DeleteGroup("9999");
// 			Assert.AreEqual(expected, actual);
// 		}
// 
// 		/// <summary>
// 		///A test for Group_get_by_gid
// 		///</summary>
// 		[TestMethod()]
// 		public void Group_get_by_gid()
// 		{
// 
// 			int expected = 1;
// 			int actual;
// 
// 			DBLayer dblayer = new DBLayer();
// 			dblayer.openDB(connection_string);
// 
// 			actual = dblayer.CreateGroupDA().GetDataByGID("0000").Rows.Count;
// 			Assert.AreEqual(expected, actual);
// 		}
// 
// 		/// <summary>
// 		///A test for Group_get_by_parent
// 		///</summary>
// 		[TestMethod()]
// 		public void Group_get_by_parent()
// 		{
// 
// 			int expected = 1;
// 			int actual;
// 
// 			DBLayer dblayer = new DBLayer();
// 			dblayer.openDB(connection_string);
// 
// 			actual = dblayer.CreateGroupDA().GetDataByParent("0000001").Rows.Count;
// 			Assert.AreEqual(expected, actual);
// 		}
// 
// 		/// <summary>
// 		///A test for Group_get_all
// 		///</summary>
// 		[TestMethod()]
// 		public void Group_get_all()
// 		{
// 
// 			int expected = 0;
// 			int actual;
// 
// 			DBLayer dblayer = new DBLayer();
// 			dblayer.openDB(connection_string);
// 
// 			actual = dblayer.CreateGroupDA().GetDataAll().Rows.Count;
// 			Assert.AreNotEqual(expected, actual);
// 		}
// 
// 		/// <summary>
// 		///A test for Group_get_all
// 		///</summary>
// 		[TestMethod()]
// 		public void Group_update()
// 		{
// 
// 			int expected = 1;
// 			int actual;
// 
// 			DBLayer dblayer = new DBLayer();
// 			dblayer.openDB(connection_string);
// 
// 			actual = dblayer.CreateGroupDA().UpdateGroup("", "test", "test", "0000000", "이름변경됨", "127.0.0.1", "0001");
// 			Assert.AreEqual(expected, actual);
// 		}
// 		#endregion Group 관련
// 
// 		#region User 관련
// 		/// <summary>
// 		///A test for User_get_data
// 		///</summary>
// 		[TestMethod()]
// 		public void User_get_data()
// 		{
// 			DBLayer dblayer = new DBLayer(); // TODO: Initialize to an appropriate value
// 			int expected = 0;
// 			int actual;
// 			dblayer.openDB(connection_string);
// 			actual = dblayer.CreateUsersDA().GetData().Rows.Count;
// 			Assert.AreNotEqual(expected, actual);
// 		}
// 
// 		/// <summary>
// 		///A test for User_add_user
// 		///</summary>
// 		[TestMethod()]
// 		public void User_add_delete_user()
// 		{
// 			DBLayer dblayer = new DBLayer(); // TODO: Initialize to an appropriate value
// 			int expected = 1;
// 			int actual;
// 			dblayer.openDB(connection_string);
// 			actual = dblayer.CreateUsersDA().AddUser("unittest", "unittest", 0, "0001", "0000001");
// 			Assert.AreEqual(expected, actual);
// 
// 			foreach (DataSet.usersRow row in dblayer.CreateUsersDA().GetData().Rows)
// 			{
// 				if(row.name.Equals("unittest"))
// 				{
// 					actual = dblayer.CreateUsersDA().DeleteUser((int)row.id);
// 					Assert.AreEqual(expected, actual);
// 					return;
// 				}
// 			}
// 			Assert.Fail();
// 		}
// 
// 		/// <summary>
// 		///A test for User_update_user
// 		///</summary>
// 		[TestMethod()]
// 		public void User_update_user()
// 		{
// 			DBLayer dblayer = new DBLayer(); // TODO: Initialize to an appropriate value
// 			int expected = 1;
// 			int actual;
// 			dblayer.openDB(connection_string);
// 			DataSet.usersRow row = dblayer.CreateUsersDA().GetData().Rows[0] as DataSet.usersRow;
// 			actual = dblayer.CreateUsersDA().UpdateUser("unittest", (int)row.level, row.gid, row.pid, (int)row.id);
// 			// 복구
// 			actual = dblayer.CreateUsersDA().UpdateUser(row.hash, (int)row.level, row.gid, row.pid, (int)row.id);
// 			Assert.AreEqual(expected, actual);
// 		}
// 
// 		/// <summary>
// 		///A test for User_check_user
// 		///</summary>
// 		[TestMethod()]
// 		public void User_check_user()
// 		{
// 			DBLayer dblayer = new DBLayer(); // TODO: Initialize to an appropriate value
// 			int expected = 0;
// 			int actual;
// 			dblayer.openDB(connection_string);
// 			actual = dblayer.CreateUsersDA().CheckUser("admin", "admin").Rows.Count;
// 			Assert.AreEqual(expected, actual);
// 		}
// 		#endregion User 관련
// 
// 		#region Task 관련
// 
// 		/// <summary>
// 		///A test for Task_get_data
// 		///</summary>
// 		[TestMethod()]
// 		public void Task_get_data()
// 		{
// 			DBLayer dblayer = new DBLayer(); // TODO: Initialize to an appropriate value
// 			int expected = 1;
// 			int actual;
// 			dblayer.openDB(connection_string);
// 
// 			//	Add
// 			string uuid = Guid.NewGuid().ToString();
// 			string guuid = Guid.NewGuid().ToString();
// 			string duuid = Guid.NewGuid().ToString();
// 			long ticks = TimeConverter.ConvertToUTP(DateTime.Now) - 10;
// 			actual = dblayer.CreateTaskDA().AddTask("0000001", "0001", 0, uuid, ticks, ticks - 1000, ticks + 1000,
// 				guuid, duuid, "Unit Test", 100, 127, 100, 0, 0);
// 
// 			Assert.AreEqual(expected, actual);
// 
// 			//	GetData
// 			ticks = TimeConverter.ConvertToUTP(DateTime.Now);
// 			actual = dblayer.CreateTaskDA().GetData(ticks, ticks).Rows.Count;
// 
// 			if(actual <= 0)
// 			{
// 				Assert.Fail();
// 			}
// 
// 			//	GetDownloadTasks
// 			ticks = TimeConverter.ConvertToUTP(DateTime.Now);
// 			actual = dblayer.CreateTaskDA().GetDownloadTasks(ticks + 10).Rows.Count;
// 
// 			if (actual < 0)
// 			{
// 				Assert.Fail();
// 			}
// 
// 			//	GetFinishedTasks
// 			ticks = TimeConverter.ConvertToUTP(DateTime.Now);
// 			actual = dblayer.CreateTaskDA().GetFinishedTasks(ticks + 10).Rows.Count;
// 
// 			if (actual < 0)
// 			{
// 				Assert.Fail();
// 			}
// 			long start = ticks - 100, end = ticks + 100;
// 
// 			//	GetTasksFrom
// 			ticks = TimeConverter.ConvertToUTP(DateTime.Now);
// 			actual = dblayer.CreateTaskDA().GetTasksFrom(start, end, start, end, start, end).Rows.Count;
// 
// 			if (actual <= 0)
// 			{
// 				Assert.Fail();
// 			}
// 
// 			//	GetTasksByDuuid
// 			ticks = TimeConverter.ConvertToUTP(DateTime.Now);
// 			actual = dblayer.CreateTaskDA().GetTasksByDuuid(duuid, 0).Rows.Count;
// 
// 			if (actual <= 0)
// 			{
// 				Assert.Fail();
// 			}
// 
// 			//	GetTasksGroup
// 			ticks = TimeConverter.ConvertToUTP(DateTime.Now);
// 			actual = dblayer.CreateTaskDA().GetTasksGroup(guuid).Rows.Count;
// 
// 			if (actual <= 0)
// 			{
// 				Assert.Fail();
// 			}
// 
// 			//	GetTasksBetweenStartEndByGroup
// 			ticks = TimeConverter.ConvertToUTP(DateTime.Now);
// 			actual = dblayer.CreateTaskDA().GetTasksBetweenStartEndByGroup(uuid, "0001", 0, start, uuid, "0001", 0, end, uuid, "0001", 0, start, end).Rows.Count;
// 
// 			if (actual <= 0)
// 			{
// 				Assert.Fail();
// 			}
// 
// 			//	GetTasksBetweenStartEndByPlayer
// 			ticks = TimeConverter.ConvertToUTP(DateTime.Now);
// 			actual = dblayer.CreateTaskDA().GetTasksBetweenStartEndByPlayer(uuid, 0, "0001", start, uuid, 0, "0001", end, uuid, 0, "0000001", start, uuid, 0, "0000001", end, uuid, 0, "0000001", start, end, uuid, 0, "0001", start, end).Rows.Count;
// 
// 			if (actual <= 0)
// 			{
// 				Assert.Fail();
// 			}
// 
// 			//	GetTasksBetweenStartEndOnlyGroup
// 			ticks = TimeConverter.ConvertToUTP(DateTime.Now);
// 			actual = dblayer.CreateTaskDA().GetTasksBetweenStartEndOnlyGroup(uuid, "0001", 0, start, uuid, "0001", 0, end, uuid, "0001", 0, start, end).Rows.Count;
// 
// 			if (actual <= 0)
// 			{
// 				Assert.Fail();
// 			}
// 
// 			//	GetTasksBetweenStartEndOnlyPlayer
// 			ticks = TimeConverter.ConvertToUTP(DateTime.Now);
// 			actual = dblayer.CreateTaskDA().GetTasksBetweenStartEndOnlyPlayer(uuid, 0, "0000001", start, uuid, 0, "0000001", end, uuid, 0, "0000001", start, end).Rows.Count;
// 
// 			if (actual <= 0)
// 			{
// 				Assert.Fail();
// 			}
// 
// 			//	Clean
// 			actual = dblayer.CreateTaskDA().CleanTask(uuid);
// 			Assert.AreEqual(expected, actual);
// 		}
// 
// 		/// <summary>
// 		///A test for Task_add_update_edit_remove_clean
// 		///</summary>
// 		[TestMethod()]
// 		public void Task_add_update_edit_remove_clean()
// 		{
// 			DBLayer dblayer = new DBLayer(); // TODO: Initialize to an appropriate value
// 			int expected = 1;
// 			int actual;
// 			dblayer.openDB(connection_string);
// 
// 			//	Add
// 			string uuid = Guid.NewGuid().ToString();
// 			string guuid = Guid.NewGuid().ToString();
// 			string duuid = Guid.NewGuid().ToString();
// 			long ticks = TimeConverter.ConvertToUTP(DateTime.Now);
// 			actual = dblayer.CreateTaskDA().AddTask("0000001", "0001", 0, uuid, ticks, ticks - 1000, ticks + 1000,
// 				guuid, duuid, "Unit Test", 100, 127, 100, 0, 0);
// 
// 			Assert.AreEqual(expected, actual);
// 
// 			//	Update
// 			ticks = TimeConverter.ConvertToUTP(DateTime.Now);
// 
// 			actual = dblayer.CreateTaskDA().UpdateTask("0000001", "0001", ticks - 1000, ticks + 1000, ticks, 0,
// 				"Unit Test - modified", duuid = Guid.NewGuid().ToString(), 100, 127, 100, 0, 0, 0, uuid);
// 			
// 			Assert.AreEqual(expected, actual);
// 
// 			//	Edit
// 			actual = dblayer.CreateTaskDA().EditTasks(0, 0, ticks - 1000, ticks + 1000, ticks, "Unit Test - edited", Guid.NewGuid().ToString(), 100, 127, 100, 0, 0, guuid, duuid, ticks - 1000, ticks + 1000);
// 
// 			Assert.AreEqual(expected, actual);
// 
// 			//	Remove
// 			actual = dblayer.CreateTaskDA().RemoveTask(TimeConverter.ConvertToUTP(DateTime.Now), guuid);
// 			Assert.AreEqual(expected, actual);
// 
// 			//	Clean
// 			actual = dblayer.CreateTaskDA().CleanTask(uuid);
// 			Assert.AreEqual(expected, actual);
// 
// // 			//	Cleans
// // 			actual = dblayer.CreateTaskDA().CleanTasks();
// 
// 		}
// 
// 		/// <summary>
// 		///A test for Task_add_with_state_clean
// 		///</summary>
// 		[TestMethod()]
// 		public void Task_add_with_state_clean()
// 		{
// 			DBLayer dblayer = new DBLayer(); // TODO: Initialize to an appropriate value
// 			int expected = 1;
// 			int actual;
// 			dblayer.openDB(connection_string);
// 
// 			//	Add
// 			string uuid = Guid.NewGuid().ToString();
// 			string guuid = Guid.NewGuid().ToString();
// 			long ticks = TimeConverter.ConvertToUTP(DateTime.Now);
// 			actual = dblayer.CreateTaskDA().AddTaskWithState("0000001", "0001", 0, 1, uuid, ticks, ticks - 1000, ticks + 1000,
// 				Guid.NewGuid().ToString(), Guid.NewGuid().ToString(), "Unit Test- add with state", 100, 127, 100, 0, 0);
// 
// 			Assert.AreEqual(expected, actual);
// 
// 			//	Clean
// 			actual = dblayer.CreateTaskDA().CleanTask(uuid);
// 			Assert.AreEqual(expected, actual);
// 
// 		}
// 
// 		/// <summary>
// 		///A test for Task_changes
// 		///</summary>
// 		[TestMethod()]
// 		public void Task_changes()
// 		{
// 			DBLayer dblayer = new DBLayer(); // TODO: Initialize to an appropriate value
// 			int expected = 1;
// 			int actual;
// 			dblayer.openDB(connection_string);
// 
// 			//	Add
// 			string uuid = Guid.NewGuid().ToString();
// 			string guuid = Guid.NewGuid().ToString();
// 			long ticks = TimeConverter.ConvertToUTP(DateTime.Now);
// 			actual = dblayer.CreateTaskDA().AddTaskWithState("0000001", "0001", 0, 1, uuid, ticks, ticks - 1000, ticks + 1000,
// 				guuid, Guid.NewGuid().ToString(), "Unit Test- Changes test", 100, 127, 100, 0, 0);
// 
// 			Assert.AreEqual(expected, actual);
// 
// 			//	Change Priority
// 			actual = dblayer.CreateTaskDA().ChangePriority(10, TimeConverter.ConvertToUTP(DateTime.Now), uuid);
// 			Assert.AreEqual(expected, actual);
// 
// 			//	Change Priority
// 			actual = dblayer.CreateTaskDA().ChangeTaskState(1, TimeConverter.ConvertToUTP(DateTime.Now), uuid);
// 			Assert.AreEqual(expected, actual);
// 
// 			//	ResetRunning
// 			actual = dblayer.CreateTaskDA().ResetRunning();
// 			Assert.AreEqual(expected, actual);
// 
// 			//	Clean
// 			actual = dblayer.CreateTaskDA().CleanTask(uuid);
// 			Assert.AreEqual(expected, actual);
// 
// 		}
// 		#endregion Task 관련
// 
// 		#region Log 관련
// 
// 		/// <summary>
// 		///A test for Log_logmaster
// 		///</summary>
// 		[TestMethod()]
// 		public void Log_logmaster()
// 		{
// 			DBLayer dblayer = new DBLayer(); // TODO: Initialize to an appropriate value
// 			int expected = 1;
// 			int actual;
// 			dblayer.openDB(connection_string);
// 			dblayer.InitializeConnectionPool(1);
// 			//	insert Master Data
// 			actual = dblayer.CreateLogMasterDA().InsertLogMasterData(1, TimeConverter.ConvertToUTP(DateTime.Now), 0, "0000001");
// // 			if (actual < 0)
// // 			{
// // 				Assert.Fail();
// // 			}
// 			Assert.AreEqual(expected, actual);
// 
// 			//	Get Log Master Data
// 
// 			ServerDataSet.logmasterDataTable table = dblayer.CreateLogMasterDA().GetLogMasterData("0000001", 0, 1);
// 			
// 			actual = table.Rows.Count;
// 			if (actual <= 0)
// 			{
// 				Assert.Fail();
// 			}
// 
// 			actual = dblayer.CreateLogMasterDA().UpdateLastUpdateDatetime(TimeConverter.ConvertToUTP(DateTime.Now), ((ServerDataSet.logmasterRow)table.Rows[0]).id);
// 			Assert.AreEqual(expected, actual);
// 
// 			dblayer.CloseConnectionPool();
// 		}
// 
// 
// 		/// <summary>
// 		///A test for Log_logmaster
// 		///</summary>
// 		[TestMethod()]
// 		public void Log_logs()
// 		{
// 			DBLayer dblayer = new DBLayer(); // TODO: Initialize to an appropriate value
// 			int expected = 1;
// 			int actual;
// 			dblayer.openDB(connection_string);
// 			dblayer.InitializeConnectionPool(1);
// 
// 
// 			//	Get Log Master Data
// 
// 			ServerDataSet.logmasterDataTable table = dblayer.CreateLogMasterDA().GetLogMasterData("0000001", 0, 1);
// 
// 			actual = table.Rows.Count;
// 			if (actual <= 0)
// 			{
// 				Assert.Fail();
// 			}
// 
// 			long master_id = ((ServerDataSet.logmasterRow)table.Rows[0]).id;
// 			long ticks = TimeConverter.ConvertToUTP(DateTime.Now);
// 			//	insert log
// 			actual = dblayer.CreateLogDA().InsertLog(0, master_id, ticks, "Unit Test - writing logs", "logs", "ok");
// 			Assert.AreEqual(expected, actual);
// 
// 			//	get logs by mater id
// 			actual = dblayer.CreateLogDA().GetLogsByMasterInfo(0, 1, "0000001").Rows.Count;
// 			if (actual <= 0)
// 			{
// 				Assert.Fail();
// 			}
// 
// 			//	get logs from to
// 			actual = dblayer.CreateLogDA().GetLogsFromTo(0, 1, "0000001", ticks - 100, ticks + 100).Rows.Count;
// 			if (actual <= 0)
// 			{
// 				Assert.Fail();
// 			}
// 
// 			//	delete before
// 			actual = dblayer.CreateLogDA().DeleteLogsBefore(ticks+100);
// 			Assert.AreEqual(expected, actual);
// 
// 			//	insert log
// 			actual = dblayer.CreateLogDA().InsertLog(0, master_id, ticks, "Unit Test - writing logs", "logs", "ok");
// 			Assert.AreEqual(expected, actual);
// 
// 			//	delete all
// 			actual = dblayer.CreateLogDA().DeleteAllLogs();
// 			Assert.AreEqual(expected, actual);
// 			dblayer.CloseConnectionPool();
// 		}
// 
// 		#endregion Log 관련
// 
// 		#region Download 관련
// 
// 		/// <summary>
// 		///A test for Download_Process
// 		///</summary>
// 		[TestMethod()]
// 		public void Download_Process()
// 		{
// 			DBLayer dblayer = new DBLayer(); // TODO: Initialize to an appropriate value
// 			int expected = 1;
// 			int actual;
// 			dblayer.openDB(connection_string);
// 
// 			string duuid = Guid.NewGuid().ToString();
// 
// 			//	insert Master Data
// 			actual = dblayer.CreateDownloadMasterDA().InsertDownloadedPlayer(duuid, "0010", "0000001", TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()));
// 			Assert.AreEqual(expected, actual);
// 
// 			DataBase.DataSet.downloadmasterDataTable table = dblayer.CreateDownloadMasterDA().GetDataByPID(duuid, "0010", "0000001");
// 			actual = table.Rows.Count;
// 			Assert.AreEqual(expected, actual);
// 
// 			actual = dblayer.CreateDownloadMasterDA().GetDataByGID(duuid, "0010").Rows.Count;
// 			Assert.AreEqual(expected, actual);
// 
// 			actual = dblayer.CreateDownloadMasterDA().UpdateRegDate(TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()), ((DataBase.DataSet.downloadmasterRow)table.Rows[0]).id);
// 	
// 			Assert.AreEqual(expected, actual);
// 		}
// 
// 		#endregion
// 
		#region Download Time 관련
		
		/// <summary>
		///A test for DownLoadTimeTest
		///</summary>
		[TestMethod()]
		public void DownLoadTimeTest()
		{
            //DBLayer dblayer = new DBLayer(); // TODO: Initialize to an appropriate value
            //int expected = 1;
            //int actual;
            //dblayer.openDB(connection_string);
            //dblayer.InitializeConnectionPool(1);
            ////	insert Master Data
            //ServerDatabase.ServerDataSet.downloadtimerDataTable dt = dblayer.CreateDownloadTimerDA().GetDataByPID("0000011");
            //actual = dt.Rows.Count;

            //Assert.AreEqual(expected, actual);
		}

		#endregion
		
	}
}
