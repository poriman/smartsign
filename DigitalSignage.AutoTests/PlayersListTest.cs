﻿using DigitalSignage.SchedulerService;
using Microsoft.VisualStudio.TestTools.UnitTesting;
// using DigitalSignage.DataBase;

namespace DigitalSignage.AutoTests
{
    
    
    /// <summary>
    ///This is a test class for PlayersListTest and is intended
    ///to contain all PlayersListTest Unit Tests
    ///</summary>
    [TestClass()]
    public class PlayersListTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for Add
        ///</summary>
        [TestMethod()]
        public void AddTest()
        {
            PlayersList target = new PlayersList(); // TODO: Initialize to an appropriate value
            string gid = "0000000000000"; // TODO: Initialize to an appropriate value
            DataSet.playersDataTable newData = new DataSet.playersDataTable();
            DataSet.playersRow row1 = (DataSet.playersRow)newData.NewRow();
            row1.ComBits = 1;
            row1.ComSpeed = 38400;
            row1.freespace = 123;
            row1.gid = "0000000000000";
            row1.host = "234.123.123.123";
            row1.name = "Test player";
            row1.pid = "0000000000001";
            row1.port = "COM1";
            row1.state = 0;
            row1.uptime = 256;
            row1.versionH = 9812;
            row1.versionL = 2387;
            newData.AddplayersRow(row1);
            Assert.AreEqual( 0, target.Add(gid, newData));
            newData = target.GetPlayersByGID("0000000000000");
            Assert.AreEqual(1, newData.Rows.Count);
            DataSet.playersRow row2 = newData.Rows[0] as DataSet.playersRow;
            Assert.AreNotEqual(null, row2);
            Assert.AreEqual(row1.ComBits, row2.ComBits);
            Assert.AreEqual(row1.ComSpeed, row2.ComSpeed);
            Assert.AreEqual(row1.gid, row2.gid);
            Assert.AreEqual(row1.host, row2.host);
            Assert.AreEqual(row1.name, row2.name);
            Assert.AreEqual(row1.port, row2.port);
            target.Update(row2.pid, "name2", "0000000000000", "123.123.123.132", "", "COM2", 19200, 1, 0, 0, 0, "", "", "", "");
        }
    }
}
