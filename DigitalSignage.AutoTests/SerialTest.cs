﻿using DigitalSignage.Player;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using DigitalSignage.SerialKey;
using DigitalSignage.Common;

namespace DigitalSignage.AutoTests
{


    /// <summary>
    ///This is a test class for CleanerTest and is intended
    ///to contain all CleanerTest Unit Tests
    ///</summary>
	[TestClass()]
	public class SerialKeyTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
		///A test for SerialKeyTest
        ///</summary>
        [TestMethod()]
        public void SerialKeyValidTest()
        {

// 			string sReturn = DigitalSignage.SchedulerService.SearchManager.GetInstance.IncreaseID("1111");
// 			Assert.AreEqual(sReturn, "1112");
// 			sReturn = DigitalSignage.SchedulerService.SearchManager.GetInstance.IncreaseID("A111");
// 			Assert.AreEqual(sReturn, "0112");
// 			sReturn = DigitalSignage.SchedulerService.SearchManager.GetInstance.IncreaseID("0001");
// 			Assert.AreEqual(sReturn, "0002");
// 			sReturn = DigitalSignage.SchedulerService.SearchManager.GetInstance.IncreaseID("9999");
// 			Assert.AreEqual(sReturn, "0000000000000");
// 			sReturn = DigitalSignage.SchedulerService.SearchManager.GetInstance.IncreaseID("9999999");
// 			Assert.AreEqual(sReturn, "0000000");
// 			sReturn = DigitalSignage.SchedulerService.SearchManager.GetInstance.IncreaseID("0000001");
// 			Assert.AreEqual(sReturn, "0000002");
// 			sReturn = DigitalSignage.SchedulerService.SearchManager.GetInstance.IncreaseID("5210001");
// 			Assert.AreEqual(sReturn, "5210002");
// 
// 			KeyChecker.GetInstance.SetProductType(NESerialKey._ProductCode.SoftwareAsaService);
// 
// 			NESerialKey._ProductCode code = KeyChecker.GetInstance.CheckProductType();
// 
// 			Assert.AreEqual(NESerialKey._ProductCode.SoftwareAsaService, (code & NESerialKey._ProductCode.SoftwareAsaService));
// 			Assert.AreEqual(NESerialKey._ProductCode.NetworkEdition, (code & NESerialKey._ProductCode.NetworkEdition));
// 			Assert.AreEqual(NESerialKey._ProductCode.StandaloneEdition, (code & NESerialKey._ProductCode.StandaloneEdition));
// 			Assert.AreEqual(NESerialKey._ProductCode.EmbededEdition, (code & NESerialKey._ProductCode.EmbededEdition));
// 
// 			
			string source = DateTime.UtcNow.Ticks.ToString("D19");
			string sCID1 = DigitalSignage.SerialKey.CharacterizedID.ConvertToCharacterized(source);
// 			string sCID2 = DigitalSignage.SerialKey.CharacterizedID.ConvertToCharacterized("0000002");
// 			string sCID3 = DigitalSignage.SerialKey.CharacterizedID.ConvertToCharacterized("0000003");
// 			string sCID4 = DigitalSignage.SerialKey.CharacterizedID.ConvertToCharacterized("0000004");

			Assert.AreEqual(source, DigitalSignage.SerialKey.CharacterizedID.ConvertToDeserializedID(sCID1));
// 			Assert.AreEqual("0000002", DigitalSignage.SerialKey.CharacterizedID.ConvertToDeserializedID(sCID2));
// 			Assert.AreEqual("0000003", DigitalSignage.SerialKey.CharacterizedID.ConvertToDeserializedID(sCID3));
// 			Assert.AreEqual("0000004", DigitalSignage.SerialKey.CharacterizedID.ConvertToDeserializedID(sCID4));

// 			Assert.AreEqual("0000001", DigitalSignage.SerialKey.CharacterizedID.ConvertToDeserializedID("GSBRYRERGQ971"));
		}
		[TestMethod()]
		public void SerialKeyValidTest2()
		{
			Assert.IsNull(DigitalSignage.SerialKey.CharacterizedID.ConvertToDeserializedID("GSBRYRERGQ972"));
			Assert.IsNull(DigitalSignage.SerialKey.CharacterizedID.ConvertToDeserializedID("GSBRYRERGQ973"));
			Assert.IsNull(DigitalSignage.SerialKey.CharacterizedID.ConvertToDeserializedID("GSBRYRERGQ974"));
			Assert.IsNull(DigitalSignage.SerialKey.CharacterizedID.ConvertToDeserializedID("GSBRYRERGQ975"));
			Assert.IsNull(DigitalSignage.SerialKey.CharacterizedID.ConvertToDeserializedID("GSBRYRERGQ976"));
			Assert.IsNull(DigitalSignage.SerialKey.CharacterizedID.ConvertToDeserializedID("GSBRYRERGQ977"));
			Assert.IsNull(DigitalSignage.SerialKey.CharacterizedID.ConvertToDeserializedID("GSBRYRERGQ978"));
			Assert.IsNull(DigitalSignage.SerialKey.CharacterizedID.ConvertToDeserializedID("GSBRYRERGQ979"));
			Assert.IsNull(DigitalSignage.SerialKey.CharacterizedID.ConvertToDeserializedID("GSBRYRERGQ980"));


// 			DigitalSignage.SerialKey.SerialKey key = new DigitalSignage.SerialKey.SerialKey();
// 
// 			int expected = 6;
// 			int actual;
// 			key.Initialize(6);
// 			key.SetByte(0,211);
// 			key.SetByte(1,192);
// 			key.SetByte(2,178);
// 			key.SetByte(3,120);
// 			key.SetBytes(4, BitConverter.GetBytes((short)1000));
// 
// 			String sReturn = key.MakeKey("ivision");
// 
// 			bool bRet = key.ParseKey(sReturn, "ivision");
// 			Assert.IsTrue(bRet, "Key is not valid");
// 
// 			Byte b1 = key.GetByte(0);
// 			Byte b2 = key.GetByte(1);
// 			Byte b3 = key.GetByte(2);
// 			Byte b4 = key.GetByte(3);
// 			Byte[] b5 = key.GetBytes(4, 2);
// 			int nb5 = BitConverter.ToInt16(b5, 0);
// 
// 			actual = sReturn.Length;
// 			Assert.AreEqual(expected, actual);
        }

    }
}
