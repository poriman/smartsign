﻿using DigitalSignage.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;

namespace DigitalSignage.AutoTests
{
    
    
    /// <summary>
    ///This is a test class for FTPFunctionsTest and is intended
    ///to contain all FTPFunctionsTest Unit Tests
    ///</summary>
    [TestClass()]
    public class FTPFunctionsTest
    {
		public static string root = "C:\\DownloadTest\\1번\\";
        public static string dst = "C:\\DownloadTest\\";

		#region 실서버
		public static string address = "119.206.205.149";
		public static string path = "homes/ivision";
		public static string id = "ivision";
		public static string pwd = "dlsxpffldks.";
		#endregion

		#region 테스트 서버
// 		public static string address = "211.192.178.120";
// 		public static string path = "";
// 		public static string id = "test";
// 		public static string pwd = "test";
		#endregion

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        [TestMethod()]
        public void FTPTest()
        {
            //FTPFunctions ftp = new FTPFunctions(address, path, id, pwd, 5000);
            //Assert.AreEqual(ftp.CheckState(), true);
            //// lets check the files
            //Assert.AreEqual(ftp.FTPRecursiveCopy(root, "00000000-0000-0000-0000-000000000000", "", true), true);
            //int nFTPCount = ftp.GetFTPRecursiveFileCount("00000000-0000-0000-0000-000000000000");
            //Assert.AreEqual(nFTPCount > 0, true);
            //Assert.AreEqual(ftp.FTPHasDirectory("00000000-0000-0000-0000-000000000000"), true);
            //Assert.AreEqual(ftp.FTPRecursiveDownload("00000000-0000-0000-0000-000000000000", dst), true);
            //Assert.AreEqual(ftp.FTPRecursiveDelete("00000000-0000-0000-0000-000000000000"), true);
			
			//Assert.AreEqual( File.Exists( dst+"\\media\\游资偏爱这些短线股.txt" ), true);
        }
// 
//         [TestMethod()]
//         public void UploadTest()
//         {
//             FTPFunctions ftp = new FTPFunctions("127.0.0.1", "", "test", "test");
//             Assert.AreEqual(ftp.FTPRecursiveCopy( root+ "tests\\FTPUnicodeFileNames",
//                 "00000004-e4f3-4c65-954c-fd143d5bf476", ""), true, "Can't upload project");
//         }
// 
//         /// <summary>
//         ///A test for FTPRecursiveDownload
//         ///</summary>
//         [TestMethod()]
//         public void FTPRecursiveDownloadTest()
//         {
//             FTPFunctions ftp = new FTPFunctions("127.0.0.1", "", "test", "test");
//             Assert.AreEqual(ftp.FTPRecursiveDownload( "00000004-e4f3-4c65-954c-fd143d5bf476", dst),
//                 true, "Can't upload project");
//         }
    }
}
