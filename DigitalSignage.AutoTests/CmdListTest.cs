﻿using DigitalSignage.SchedulerService;
using Microsoft.VisualStudio.TestTools.UnitTesting;
// using DigitalSignage.DataBase;

namespace DigitalSignage.AutoTests
{
    
    
    /// <summary>
    ///This is a test class for CmdListTest and is intended
    ///to contain all CmdListTest Unit Tests
    ///</summary>
    [TestClass()]
    public class CmdListTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for Delete
        ///</summary>
        [TestMethod()]
        public void DeleteTest()
        {
            DigitalSignage.Common.AuthTicket ticket = new DigitalSignage.Common.AuthTicket();
            ticket.name = "admin";
            ticket.passhash = "21232f297a57a5a743894a0e4a801fc3";
            CmdList target = new CmdList(); // TODO: Initialize to an appropriate value
            int id = 0; // TODO: Initialize to an appropriate value
            int expected = 0; // TODO: Initialize to an appropriate value
            int actual;
            actual = target.Delete(ticket, id);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for Get
        ///</summary>
        [TestMethod()]
        public void GetTest()
        {
            CmdList target = new CmdList(); // TODO: Initialize to an appropriate value
            int id = 0; // TODO: Initialize to an appropriate value
            DataSet.RSCommandsDataTable expected = null; // TODO: Initialize to an appropriate value
            DataSet.RSCommandsDataTable actual;
            actual = target.Get(id);
            Assert.AreEqual(expected, actual);
            Assert.Inconclusive("Verify the correctness of this test method.");
        }

        /// <summary>
        ///A test for GetCommands
        ///</summary>
        [TestMethod()]
        public void GetCommandsTest()
        {
            CmdList target = new CmdList(); // TODO: Initialize to an appropriate value
            DataSet.RSCommandsDataTable actual = null;
            actual = target.GetCommands();
            Assert.AreNotEqual(null, actual);
            Assert.AreEqual( 1, actual.Rows.Count);
        }

        /// <summary>
        ///A test for IsInUse
        ///</summary>
        [TestMethod()]
        public void IsInUseTest()
        {
            CmdList target = new CmdList(); // TODO: Initialize to an appropriate value
            int id = 0; // TODO: Initialize to an appropriate value
            bool expected = true; // TODO: Initialize to an appropriate value
            bool actual;
            actual = target.IsInUse(id);
            Assert.AreEqual(expected, actual);
        }

        /// <summary>
        ///A test for Update
        ///</summary>
        [TestMethod()]
        public void UpdateTest()
        {
            DigitalSignage.Common.AuthTicket ticket = new DigitalSignage.Common.AuthTicket();
            ticket.name = "admin";
            ticket.passhash = "21232f297a57a5a743894a0e4a801fc3";

            CmdList target = new CmdList();
            DataSet.RSCommandsDataTable data = new DataSet.RSCommandsDataTable();
            DataSet.RSCommandsRow row = (DataSet.RSCommandsRow)data.NewRow();
            row.id = 0;
            row.etime = 123;
            row.name = "Test task1";
            row.data = new byte[] { 10, 20, 30, 40, 50, 60, 70, 80, 90 };
            data.AddRSCommandsRow(row);
            Assert.AreEqual( target.Update( ticket, data), 0 );
            GetCommandsTest();
            DataSet.RSCommandsDataTable data2 = target.GetCommands();
            DataSet.RSCommandsRow row2 = data2.Rows[0] as DataSet.RSCommandsRow;
            Assert.AreEqual(row2.name, row.name);
            bool cmp = false;
            if ( row2.data.Length == row.data.Length )
            {
                cmp = true;
                for (int i = 0; i < row2.data.Length; i++)
                    if (row2.data[i] != row.data[i])
                        cmp = false;
            }
            Assert.AreEqual( true, cmp);
            row2.name = "New name";
            row2.data = new byte[] { 11, 22, 33, 44 };
            data2.AcceptChanges();
            Assert.AreEqual(target.Update(ticket, data2), 0);
            data2 = target.GetCommands();
            row = data2.Rows[0] as DataSet.RSCommandsRow;
            Assert.AreEqual(row2.name, row.name);
            cmp = false;
            if (row2.data.Length == row.data.Length)
            {
                cmp = true;
                for (int i = 0; i < row2.data.Length; i++)
                    if (row2.data[i] != row.data[i])
                        cmp = false;
            }
            Assert.AreEqual(true, cmp);
            data = target.Get((int)row.id);
            Assert.AreNotEqual(null, data);
            Assert.AreEqual(true, data.Rows.Count > 0);
            row = data.Rows[0] as DataSet.RSCommandsRow;
            DigitalSignage.SchedulerService.Config cfg = DigitalSignage.SchedulerService.Config.GetConfig;
            DigitalSignage.Common.Task newTask = new DigitalSignage.Common.Task();
            newTask.description = "Test command tasl";
            newTask.duuid = new System.Guid((int)row.id, (short)((row.etime & 0xFFFF0000) >> 16),
                            (short)(row.etime & 0xFFFF), new byte[] { 0, 0, 0, 0, 0, 0, 0, 0 });
            newTask.guuid = System.Guid.NewGuid();
            newTask.pid = "0000001";
            newTask.state = DigitalSignage.Common.TaskState.StateWait;
            newTask.TaskEnd = 123;
            newTask.TaskStart = 122;
            newTask.type = DigitalSignage.Common.TaskCommands.CmdWriteRS232;
            newTask.uuid = System.Guid.NewGuid();
            cfg.localTasksList.AddTask(ticket, newTask);
            Assert.AreEqual( true, target.IsInUse((int)row.id));
            Assert.AreEqual(1, target.Delete(ticket, (int)row.id));
            Assert.AreEqual( false, target.IsInUse((int)row.id));
        }
    }
}
