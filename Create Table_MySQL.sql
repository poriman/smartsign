
create database ivision21;

use mysql;

CREATE USER 'ivisionuser'@'%' IDENTIFIED BY 'ivision';
GRANT ALL ON ivision21.* TO 'ivisionuser'@'%';

use ivision21;


CREATE TABLE geo_master
(
	geocode INT PRIMARY KEY,
	x float,
	y float,
	postcode VARCHAR(9),
	address VARCHAR(256),
	ref_code VARCHAR(10)
);

CREATE TABLE client_logs (pid char(13), name VARCHAR(255), description VARCHAR(255), logcd int, error_code int, start_dt bigint, end_dt bigint, uuid CHAR(36));

CREATE TABLE groups (path VARCHAR(64), username VARCHAR(32), password VARCHAR(32), gid char(13) PRIMARY KEY, name VARCHAR(32), source VARCHAR(64), geocode int, parent_gid char(13) NULL, depth int NULL, is_inherited char(1) NULL DEFAULT 'N', del_yn char(1) NULL DEFAULT 'N');

ALTER TABLE groups
	ADD CONSTRAINT parent_gid_fkey
	FOREIGN KEY(parent_gid)
	REFERENCES groups(gid);

ALTER TABLE groups
	ADD CONSTRAINT g_ref_code_fkey
	FOREIGN KEY(geocode)
	REFERENCES geo_master(geocode);
	
CREATE TABLE players (pid char(13) PRIMARY KEY, gid char(13), name VARCHAR(255), host VARCHAR(128), state int, lastconndt bigint, versionH int, versionL int, del_yn char(1) default 'N', tz_info varchar(6), geocode int);

ALTER TABLE players
	ADD CONSTRAINT players_fkey
	FOREIGN KEY(gid)
	REFERENCES groups(gid);

ALTER TABLE players
	ADD CONSTRAINT p_ref_code_fkey
	FOREIGN KEY(geocode)
	REFERENCES geo_master(geocode);

CREATE TABLE `player_detailinfo` ( 
	`pid`      	char(13) UNIQUE,
	`_ipv4addr`	varchar(16) NULL,
	`_is_dhcp` 	varchar(2) NULL,
	`_macaddr` 	varchar(17) NULL,
	`_mnttype` 	int NULL,
	`_auth_id` 	varchar(50) NULL,
	`_auth_pass`	varchar(50) NULL
	);

ALTER TABLE `player_detailinfo`
	ADD CONSTRAINT `foreign_pid`
	FOREIGN KEY(`pid`)
	REFERENCES `players`(`pid`);


CREATE TABLE downloadtime (d_id bigint PRIMARY KEY, starttime char(8), endtime char(8));
CREATE TABLE dt_players_asn (d_id bigint, pid char(13), FOREIGN KEY(pid) REFERENCES players(pid), FOREIGN KEY(d_id) REFERENCES downloadtime(d_id), PRIMARY KEY (d_id, pid));
CREATE TABLE dt_groups_asn (d_id bigint, gid char(13), FOREIGN KEY(gid) REFERENCES groups(gid), FOREIGN KEY(d_id) REFERENCES downloadtime(d_id), PRIMARY KEY (d_id, gid));

CREATE TABLE relaylogs
(
	relay_id bigint PRIMARY KEY,
	created_ts bigint NOT NULL,
	pid char(13),
	client_up_ts bigint,
	app_srv_up_ts bigint,
	relay_srv_up_ts bigint,
	relay_srv_down_ts bigint,
	app_srv_down_ts bigint,
	client_down_ts bigint
);

ALTER TABLE relaylogs
	ADD CONSTRAINT foreign_r_pid
	FOREIGN KEY(pid)
	REFERENCES players(pid);

CREATE TABLE relaylogs_error
(
	id INTEGER AUTO_INCREMENT PRIMARY KEY,
	relay_id bigint,
	err_tp int,
	err_cd int,
	err_ts bigint
);

ALTER TABLE relaylogs_error
	ADD CONSTRAINT foreign_r_e_relay_id
	FOREIGN KEY(relay_id)
	REFERENCES relaylogs(relay_id);

CREATE TABLE relaylogs_error_defines
(
	err_tp int NOT NULL,
	err_cd int NOT NULL,
	err_message VARCHAR(255)
);

ALTER TABLE relaylogs_error_defines
	ADD PRIMARY KEY(err_tp, err_cd);

CREATE TABLE relaylogs_detail
(
	relay_id bigint,
	r_key VARCHAR(32),
	r_value VARCHAR(32)
);

ALTER TABLE relaylogs_detail
	ADD CONSTRAINT foreign_r_id
	FOREIGN KEY(relay_id)
	REFERENCES relaylogs(relay_id);

CREATE TABLE tasks (priority INT, endtimeofday INT, starttimeofday INT, daysofweek INT, duration INT, pid char(13), gid char(13), starttime BIGINT, endtime BIGINT, etime BIGINT, type INT, state INT, uuid char(36) PRIMARY KEY, guuid char(36), duuid char(36), name VARCHAR(255), is_ad char(1), meta_tag VARCHAR(255), show_flag int, tz_info varchar(6));
ALTER TABLE `tasks`
	ADD CONSTRAINT `task_g_fkey`
	FOREIGN KEY(`gid`)
	REFERENCES `groups`(`gid`);
ALTER TABLE `tasks`
	ADD CONSTRAINT `task_p_fkey`
	FOREIGN KEY(`pid`)
	REFERENCES `players`(`pid`);

CREATE INDEX idx_tasks ON tasks (starttime, endtime);

CREATE TABLE screens (s_id BIGINT PRIMARY KEY, s_starttime BIGINT, s_endtime BIGINT, s_width int, s_height int, s_playtime int, s_name VARCHAR(256), s_description VARCHAR(256), s_screen_type int, s_display_type int, s_is_ad char(1), s_meta_tag VARCHAR(100), s_delete_yn char(1) DEFAULT 'N', s_regdt BIGINT);

CREATE TABLE screentask_asn (s_id bigint, uuid char(36), s_index int, thumb_url varchar(256));

ALTER TABLE `screentask_asn`
	ADD CONSTRAINT `s_id`
	FOREIGN KEY(`s_id`)
	REFERENCES `screens`(`s_id`);

ALTER TABLE `screentask_asn`
	ADD CONSTRAINT `s_uuid`
	FOREIGN KEY(`uuid`)
	REFERENCES `tasks`(`uuid`);

CREATE TABLE promotions
(
	id			INTEGER AUTO_INCREMENT PRIMARY KEY,
	screen_id	BIGINT NOT NULL,
	receiver	VARCHAR(20) NOT NULL,
	reserved	VARCHAR(100),
	regdt		BIGINT NOT NULL,
	proceeddt	BIGINT
);

ALTER TABLE promotions
	ADD CONSTRAINT `p_sid`
	FOREIGN KEY(screen_id)
	REFERENCES screens(s_id);


CREATE TABLE playlogs
(
    pid char(13), 
    logcd int,
    uuid char(36),
    tasknm VARCHAR(255),
    screennm VARCHAR(255),
	screen_id bigint,
    errorcd int,
    start_dt bigint,
	end_dt bigint,
	relay_id bigint
);

ALTER TABLE playlogs
	ADD CONSTRAINT t_log_fkey
	FOREIGN KEY(uuid)
	REFERENCES tasks(uuid);

ALTER TABLE playlogs
	ADD CONSTRAINT p_log_fkey
	FOREIGN KEY(pid)
	REFERENCES players(pid);

ALTER TABLE playlogs
	ADD CONSTRAINT s_log_fkey
	FOREIGN KEY(screen_id)
	REFERENCES screens(s_id);

ALTER TABLE playlogs
	ADD CONSTRAINT p_log_r_fkey
	FOREIGN KEY(relay_id)
	REFERENCES relaylogs(relay_id);

CREATE INDEX idx_playlog ON playlogs (start_dt DESC, logcd, pid, relay_id);

CREATE TABLE downloadlogs
(
    pid char(13), 
    logcd int,
    uuid char(36),
    tasknm VARCHAR(255),
    contentnm VARCHAR(255),
    errorcd int,
    start_dt bigint,
	end_dt bigint
);

ALTER TABLE `downloadlogs`
	ADD CONSTRAINT `d_log_fkey`
	FOREIGN KEY(`pid`)
	REFERENCES `players`(`pid`);

CREATE INDEX idx_downloadlog ON downloadlogs (start_dt DESC, logcd, pid);

CREATE TABLE managerlogs
(
    usernm VARCHAR(255), 
    logcd int,
    uuid char(36),
    actionnm VARCHAR(255),
    actiondesc VARCHAR(255),
    errorcd int,
    reg_dt bigint
);

CREATE INDEX idx_managerlog ON managerlogs (reg_dt DESC, logcd);

CREATE TABLE connectionlogs
(
    pid char(13), 
    logcd int,
    errorcd int,
    start_dt bigint,
    end_dt bigint,
	reg_dt bigint
);

ALTER TABLE connectionlogs
	ADD PRIMARY KEY(pid, reg_dt);

ALTER TABLE connectionlogs
	ADD CONSTRAINT c_log_fkey
	FOREIGN KEY(pid)
	REFERENCES players(pid);

CREATE INDEX idx_connectionlog ON connectionlogs (reg_dt DESC, logcd, pid);

create table tbl_limitplayers (gid char(13), cntplayers int );

CREATE TABLE SYNC_TASKS(GUUID CHAR(36) NOT NULL, PLAYERS_PID CHAR(13) NOT NULL, SYNC_TP CHAR(1) NOT NULL, EDIT_DT VARCHAR(14) NULL, REG_DT VARCHAR(14) NOT NULL, DEL_YN CHAR(1) NOT NULL, PRIMARY KEY(GUUID, PLAYERS_PID), CONSTRAINT XFK_SYNC_TASKS_PLAYERS_PID FOREIGN KEY(PLAYERS_PID) REFERENCES PLAYERS(PID));

ALTER TABLE `tbl_limitplayers`
	ADD CONSTRAINT `limit_fkey`
	FOREIGN KEY(`gid`)
	REFERENCES `groups`(`gid`);

CREATE TABLE users (id INTEGER AUTO_INCREMENT PRIMARY KEY, name VARCHAR(255), hash VARCHAR(255), parent_id INTEGER, role_ids VARCHAR(255), is_enabled char(1) DEFAULT 'Y');

CREATE TABLE ivision_timestamps ( ts_key VARCHAR(50), ts_value BIGINT);

insert into ivision_timestamps (ts_key, ts_value) values ('meta_tag_groups', 1);
insert into ivision_timestamps (ts_key, ts_value) values ('relaylogs', 1);

CREATE TABLE meta_tags (m_id BIGINT PRIMARY KEY, g_id BIGINT, m_tagname VARCHAR(50), m_tagvalue VARCHAR(50), m_tagdescription VARCHAR(100));

CREATE TABLE meta_tag_groups (g_id BIGINT PRIMARY KEY, g_name VARCHAR(50), g_ui_type INTEGER, g_priority INTEGER);

insert into meta_tag_groups (g_id, g_name, g_ui_type, g_priority) VALUES (1, '성별', 0, 2);
insert into meta_tag_groups (g_id, g_name, g_ui_type, g_priority) VALUES (2, '연령', 0, 1);
insert into meta_tag_groups (g_id, g_name, g_ui_type, g_priority) VALUES (3, '유종', 0, 0);
insert into meta_tag_groups (g_id, g_name, g_ui_type, g_priority) VALUES (4, '주소', 2, 0);
insert into meta_tag_groups (g_id, g_name, g_ui_type, g_priority) VALUES (6, '프로모션', 3, 3);

insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (1, 1, '남자', 'S0', '');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (2, 1, '여자', 'S1', '');

insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (3, 2, '20세 이하', 'A20', '');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (4, 2, '21-25', 'A21', '');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (5, 2, '26-30', 'A26', '');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (6, 2, '31-35', 'A31', '');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (7, 2, '36-40', 'A36', '');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (8, 2, '41-45', 'A41', '');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (9, 2, '46-50', 'A46', '');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (10, 2, '51-55', 'A51', '');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (11, 2, '56-60', 'A56', '');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (12, 2, '61세 이상', 'A61', '');

insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (13, 3, '휘발유', 'L0660', '');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (14, 3, '고급 휘발유', 'L0610', '');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (15, 3, '경유', 'L1206', '');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (16, 3, '고급 경유', 'L1201', '');

insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (100,4,'강원도','200','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (101,4,'강원도 강릉시','210','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (102,4,'강원도 고성군','219','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (103,4,'강원도 동해시','240','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (104,4,'강원도 동해시','245','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (105,4,'강원도 삼척시','245','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (106,4,'강원도 속초시','217','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (107,4,'강원도 속초시','219','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (108,4,'강원도 양구군','255','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (109,4,'강원도 양양군','215','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (110,4,'강원도 영월군','230','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (111,4,'강원도 영월군','233','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (112,4,'강원도 원주시','220','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (113,4,'강원도 인제군','252','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (114,4,'강원도 인제군','250','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (115,4,'강원도 정선군','233','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (116,4,'강원도 철원군','269','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (117,4,'강원도 철원군','487','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (118,4,'강원도 철원군','209','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (119,4,'강원도 춘천시','200','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (120,4,'강원도 춘천시','250','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (121,4,'강원도 춘천시','209','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (122,4,'강원도 태백시','235','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (123,4,'강원도 평창군','232','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (124,4,'강원도 홍천군','250','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (125,4,'강원도 화천군','209','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (126,4,'강원도 횡성군','225','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (127,4,'경기도','400','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (128,4,'경기도 가평군','477','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (129,4,'경기도 고양시 덕양구','412','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (130,4,'경기도 고양시 덕양구','411','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (131,4,'경기도 고양시 일산동구','410','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (132,4,'경기도 고양시 일산서구','411','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (133,4,'경기도 과천시','427','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (134,4,'경기도 광명시','423','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (135,4,'경기도 광주시','464','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (136,4,'경기도 구리시','471','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (137,4,'경기도 군포시','435','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (138,4,'경기도 김포시','415','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (139,4,'경기도 남양주시','472','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (140,4,'경기도 남양주시','480','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (141,4,'경기도 남양주시','476','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (142,4,'경기도 동두천시','483','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (143,4,'경기도 동두천시','482','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (144,4,'경기도 동두천시','486','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (145,4,'경기도 부천시 소사구','422','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (146,4,'경기도 부천시 오정구','421','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (147,4,'경기도 부천시 원미구','420','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (148,4,'경기도 성남시 분당구','463','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (149,4,'경기도 성남시 수정구','461','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (150,4,'경기도 성남시 중원구','462','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (151,4,'경기도 수원시 권선구','441','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (152,4,'경기도 수원시 영통구','443','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (153,4,'경기도 수원시 장안구','440','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (154,4,'경기도 수원시 팔달구','442','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (155,4,'경기도 시흥시','429','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (156,4,'경기도 안산시 단원구','425','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (157,4,'경기도 안산시 상록구','426','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (158,4,'경기도 안성시','456','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (159,4,'경기도 안양시 동안구','431','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (160,4,'경기도 안양시 만안구','430','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (161,4,'경기도 양주시','482','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (162,4,'경기도 양주시','487','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (163,4,'경기도 양주시','480','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (164,4,'경기도 양주시','412','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (165,4,'경기도 양평군','476','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (166,4,'경기도 여주군','469','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (167,4,'경기도 여주군','476','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (168,4,'경기도 연천군','486','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (169,4,'경기도 연천군','482','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (170,4,'경기도 연천군','487','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (171,4,'경기도 연천군','480','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (172,4,'경기도 오산시','447','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (173,4,'경기도 용인시  기흥구','446','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (174,4,'경기도 용인시 기흥구','446','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (175,4,'경기도 용인시 수지구','448','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (176,4,'경기도 용인시 처인구','449','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (177,4,'경기도 의왕시','437','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (178,4,'경기도 의정부시','480','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (179,4,'경기도 이천시','467','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (180,4,'경기도 파주시','413','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (181,4,'경기도 파주시','411','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (182,4,'경기도 파주시','412','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (183,4,'경기도 파주시','482','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (184,4,'경기도 평택시','450','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (185,4,'경기도 평택시','459','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (186,4,'경기도 평택시','451','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (187,4,'경기도 포천시','487','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (188,4,'경기도 포천시','480','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (189,4,'경기도 포천시','482','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (190,4,'경기도 포천시','486','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (191,4,'경기도 하남시','465','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (192,4,'경기도 화성시','445','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (193,4,'경상남도','600','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (194,4,'경상남도 거제시','656','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (195,4,'경상남도 거창군','670','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (196,4,'경상남도 고성군','638','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (197,4,'경상남도 김해시','621','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (198,4,'경상남도 남해군','668','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (199,4,'경상남도 마산시','630','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (200,4,'경상남도 마산시','631','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (201,4,'경상남도 밀양시','627','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (202,4,'경상남도 사천시','664','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (203,4,'경상남도 산청군','666','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (204,4,'경상남도 양산시','626','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (205,4,'경상남도 의령군','636','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (206,4,'경상남도 진주시','660','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (207,4,'경상남도 진해시','645','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (208,4,'경상남도 창녕군','635','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (209,4,'경상남도 창원시','641','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (210,4,'경상남도 통영시','650','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (211,4,'경상남도 하동군','667','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (212,4,'경상남도 함안군','637','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (213,4,'경상남도 함양군','676','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (214,4,'경상남도 합천군','678','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (215,4,'경상북도','700','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (216,4,'경상북도 경산시','712','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (217,4,'경상북도 경주시','780','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (218,4,'경상북도 고령군','717','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (219,4,'경상북도 구미시','730','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (220,4,'경상북도 군위군','716','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (221,4,'경상북도 김천시','740','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (222,4,'경상북도 문경시','745','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (223,4,'경상북도 봉화군','755','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (224,4,'경상북도 상주시','742','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (225,4,'경상북도 성주군','719','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (226,4,'경상북도 안동시','760','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (227,4,'경상북도 영덕군','766','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (228,4,'경상북도 영양군','764','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (229,4,'경상북도 영주시','750','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (230,4,'경상북도 영천시','770','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (231,4,'경상북도 예천군','757','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (232,4,'경상북도 울릉군','799','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (233,4,'경상북도 울진군','767','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (234,4,'경상북도 의성군','769','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (235,4,'경상북도 청도군','714','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (236,4,'경상북도 청송군','763','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (237,4,'경상북도 칠곡군','718','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (238,4,'경상북도 포항시 남구','790','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (239,4,'경상북도 포항시 북구','791','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (240,4,'광주시','500','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (241,4,'광주시 광산구','506','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (242,4,'광주시 남구','503','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (243,4,'광주시 동구','501','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (244,4,'광주시 북구','500','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (245,4,'광주시 서구','502','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (246,4,'대구시','700','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (247,4,'대구시 남구','705','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (248,4,'대구시 달서구','704','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (249,4,'대구시 달성군','711','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (250,4,'대구시 동구','701','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (251,4,'대구시 북구','702','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (252,4,'대구시 서구','703','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (253,4,'대구시 수성구','706','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (254,4,'대구시 중구','700','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (255,4,'대전시','300','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (256,4,'대전시 대덕구','306','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (257,4,'대전시 동구','300','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (258,4,'대전시 서구','302','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (259,4,'대전시 유성구','305','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (260,4,'대전시 중구','301','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (261,4,'부산시','600','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (262,4,'부산시 강서구','618','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (263,4,'부산시 금정구','609','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (264,4,'부산시 기장군','619','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (265,4,'부산시 남구','608','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (266,4,'부산시 동구','601','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (267,4,'부산시 동래구','607','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (268,4,'부산시 부산진구','614','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (269,4,'부산시 북구','616','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (270,4,'부산시 사상구','617','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (271,4,'부산시 사하구','604','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (272,4,'부산시 서구','602','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (273,4,'부산시 수영구','613','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (274,4,'부산시 연제구','611','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (275,4,'부산시 영도구','606','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (276,4,'부산시 중구','600','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (277,4,'부산시 해운대구','612','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (278,4,'서울시','100','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (279,4,'서울시 강남구','135','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (280,4,'서울시 강동구','134','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (281,4,'서울시 강북구','142','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (282,4,'서울시 강서구','157','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (283,4,'서울시 관악구','151','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (284,4,'서울시 광진구','143','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (285,4,'서울시 구로구','152','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (286,4,'서울시 금천구','153','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (287,4,'서울시 노원구','139','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (288,4,'서울시 도봉구','132','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (289,4,'서울시 동대문구','130','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (290,4,'서울시 동작구','156','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (291,4,'서울시 마포구','121','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (292,4,'서울시 서대문구','120','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (293,4,'서울시 서초구','137','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (294,4,'서울시 성동구','133','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (295,4,'서울시 성북구','136','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (296,4,'서울시 송파구','138','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (297,4,'서울시 양천구','158','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (298,4,'서울시 영등포구','150','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (299,4,'서울시 용산구','140','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (300,4,'서울시 은평구','122','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (301,4,'서울시 은평구','412','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (302,4,'서울시 종로구','110','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (303,4,'서울시 중구','100','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (304,4,'서울시 중랑구','131','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (305,4,'울산시','600','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (306,4,'울산시 남구','680','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (307,4,'울산시 동구','682','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (308,4,'울산시 북구','683','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (309,4,'울산시 울주군','689','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (310,4,'울산시 중구','681','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (311,4,'인천시','400','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (312,4,'인천시 강화군','417','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (313,4,'인천시 계양구','407','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (314,4,'인천시 남구','402','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (315,4,'인천시 남동구','405','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (316,4,'인천시 동구','401','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (317,4,'인천시 부평구','403','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (318,4,'인천시 서구','404','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (319,4,'인천시 연수구','406','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (320,4,'인천시 옹진군','409','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (321,4,'인천시 중구','400','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (322,4,'전라남도','500','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (323,4,'전라남도 강진군','527','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (324,4,'전라남도 고흥군','548','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (325,4,'전라남도 곡성군','516','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (326,4,'전라남도 광양시','545','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (327,4,'전라남도 구례군','542','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (328,4,'전라남도 나주시','520','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (329,4,'전라남도 담양군','517','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (330,4,'전라남도 목포시','530','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (331,4,'전라남도 무안군','534','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (332,4,'전라남도 보성군','546','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (333,4,'전라남도 순천시','540','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (334,4,'전라남도 신안군','535','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (335,4,'전라남도 신안군','530','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (336,4,'전라남도 여수시','550','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (337,4,'전라남도 여수시','555','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (338,4,'전라남도 여수시','556','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (339,4,'전라남도 영광군','513','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (340,4,'전라남도 영암군','526','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (341,4,'전라남도 완도군','537','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (342,4,'전라남도 완도군','527','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (343,4,'전라남도 완도군','536','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (344,4,'전라남도 완도군','548','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (345,4,'전라남도 장성군','515','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (346,4,'전라남도 장흥군','529','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (347,4,'전라남도 진도군','539','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (348,4,'전라남도 함평군','525','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (349,4,'전라남도 해남군','536','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (350,4,'전라남도 화순군','519','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (351,4,'전라북도','500','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (352,4,'전라북도 고창군','585','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (353,4,'전라북도 고창군','579','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (354,4,'전라북도 군산시','573','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (355,4,'전라북도 김제시','576','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (356,4,'전라북도 남원시','590','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (357,4,'전라북도 무주군','568','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (358,4,'전라북도 부안군','579','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (359,4,'전라북도 순창군','595','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (360,4,'전라북도 완주군','565','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (361,4,'전라북도 익산시','570','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (362,4,'전라북도 임실군','566','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (363,4,'전라북도 장수군','597','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (364,4,'전라북도 전주시 덕진구','561','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (365,4,'전라북도 전주시 완산구','560','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (366,4,'전라북도 정읍시','580','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (367,4,'전라북도 진안군','567','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (368,4,'제주도','600','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (369,4,'제주도 서귀포시','697','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (370,4,'제주도 서귀포시','699','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (371,4,'제주도 제주시','690','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (372,4,'제주도 제주시','695','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (373,4,'충청남도','300','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (374,4,'충청남도 계룡시','321','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (375,4,'충청남도 공주시','314','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (376,4,'충청남도 금산군','312','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (377,4,'충청남도 논산시','320','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (378,4,'충청남도 당진군','343','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (379,4,'충청남도 보령시','355','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (380,4,'충청남도 부여군','323','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (381,4,'충청남도 서산시','356','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (382,4,'충청남도 서천군','325','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (383,4,'충청남도 아산시','336','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (384,4,'충청남도 연기군','339','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (385,4,'충청남도 예산군','340','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (386,4,'충청남도 천안시 동남구','330','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (387,4,'충청남도 천안시 서북구','331','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (388,4,'충청남도 청양군','345','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (389,4,'충청남도 태안군','357','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (390,4,'충청남도 홍성군','350','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (391,4,'충청북도 ','300','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (392,4,'충청북도 괴산군','367','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (393,4,'충청북도 단양군','395','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (394,4,'충청북도 보은군','376','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (395,4,'충청북도 영동군','370','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (396,4,'충청북도 옥천군','373','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (397,4,'충청북도 음성군','369','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (398,4,'충청북도 제천시','390','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (399,4,'충청북도 증평군','368','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (400,4,'충청북도 진천군','365','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (401,4,'충청북도 청원군','363','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (402,4,'충청북도 청주시 상당구','360','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (403,4,'충청북도 청주시 흥덕구','361','');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (404,4,'충청북도 충주시','380','');
																					
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (405, 6, '없음', '10000', '');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (406, 6, '응모', '10001', '');
insert into meta_tags (m_id, g_id, m_tagname, m_tagvalue, m_tagdescription) VALUES (407, 6, '쿠폰', '10002', '');
																						 
																						 
ALTER TABLE `meta_tags`																	 
	ADD CONSTRAINT `g_id`																 
	FOREIGN KEY(`g_id`)																	 
	REFERENCES `meta_tag_groups`(`g_id`);												 
																						 
CREATE TABLE roles (role_id INTEGER AUTO_INCREMENT PRIMARY KEY, role_name VARCHAR(255), role_level INTEGER, is_enabled char(1) DEFAULT 'Y');
																						 
CREATE TABLE permissions (perm_id INTEGER AUTO_INCREMENT PRIMARY KEY, perm_name VARCHAR(255), parent_perm_id INTEGER);

CREATE TABLE `datainfo` ( 
    `Id`         	varchar(18) NOT NULL,
    `User_id`    	BIGINT NULL,
    `Template_id`	varchar(18) NULL,
    `_Title`     	VARCHAR(255) NULL,
    PRIMARY KEY(Id)
);

CREATE TABLE `data_field` ( 
    `Id`         	varchar(18) NOT NULL,
    `DataInfo_id`	varchar(18) NULL,
    `Value1`     	VARCHAR(255) NULL,
    `Value2`     	VARCHAR(255) NULL,
    `Value3`     	VARCHAR(255) NULL,
    `Value4`     	VARCHAR(255) NULL,
    `Value5`     	VARCHAR(255) NULL,
    `Value6`     	VARCHAR(255) NULL,
    `Value7`     	VARCHAR(255) NULL,
    `Value8`     	VARCHAR(255) NULL,
    `Value9`     	VARCHAR(255) NULL,
    `Value10`   	VARCHAR(255) NULL,
    `Value11`    	VARCHAR(255) NULL,
    `Value12`    	VARCHAR(255) NULL,
    `Value13`    	VARCHAR(255) NULL,
    `Value14`    	VARCHAR(255) NULL,
    `Value15`    	VARCHAR(255) NULL,
    `Value16`    	VARCHAR(255) NULL,
    `Value17`    	VARCHAR(255) NULL,
    `Value18`    	VARCHAR(255) NULL,
    `Value19`    	VARCHAR(255) NULL,
    `Value20`    	VARCHAR(255) NULL,
    `Value21`    	VARCHAR(255) NULL,
    `Value22`    	VARCHAR(255) NULL,
    `Value23`    	VARCHAR(255) NULL,
    `Value24`    	VARCHAR(255) NULL,
    `Value25`    	VARCHAR(255) NULL,
    `Value26`    	VARCHAR(255) NULL,
    `Value27`    	VARCHAR(255) NULL,
    `Value28`    	VARCHAR(255) NULL,
    `Value29`    	VARCHAR(255) NULL,
    `Value30`    	VARCHAR(255) NULL,
    `Field_name` 	VARCHAR(255) NULL,
    PRIMARY KEY(Id)
);

CREATE TABLE `contents` ( 
    `id`           	varchar(25) NOT NULL,
    `Data_field_id`	varchar(25) NULL,
    `_Type`        	varchar(25) NULL,
    `_Value`       	VARCHAR(255) NULL,
    `File_nm`     	VARCHAR(255) NULL,
    `_Path`        	VARCHAR(255) NULL,
    `_Desc`        	VARCHAR(255) NULL,
    `Data_info_id` 	varchar(25) NULL,
    PRIMARY KEY(id)
);

CREATE TABLE `resultdatas` ( 
    `apply_date` 	BIGINT NULL,
    `playerid`   	varchar(25) NULL,
    `datafieldid`	varchar(25) NULL,
    `tempalteid` 	varchar(25) NULL,
    `data_desc`  	VARCHAR(255) NULL,
    `data_values`	VARCHAR(255) NULL 
);

CREATE TABLE `role_perm_ASN` ( 
	`role_id`	integer(11) NOT NULL,
	`perm_id`	integer(11) NOT NULL 
	);

ALTER TABLE `role_perm_ASN`
	ADD CONSTRAINT `role_id`
	FOREIGN KEY(`role_id`)
	REFERENCES `roles`(`role_id`);

ALTER TABLE `role_perm_ASN`
	ADD CONSTRAINT `perm_id`
	FOREIGN KEY(`perm_id`)
	REFERENCES `permissions`(`perm_id`);

ALTER TABLE role_perm_ASN
	ADD PRIMARY KEY(role_id, perm_id);


CREATE TABLE `user_player_ASN` ( 
	`user_id`	integer(11) NOT NULL,
	`pid`    	char(13) NOT NULL 
	);

ALTER TABLE user_player_ASN
	ADD PRIMARY KEY(user_id, pid);


ALTER TABLE `user_player_ASN`
	ADD CONSTRAINT `user_id`
	FOREIGN KEY(`user_id`)
	REFERENCES `users`(`id`);

ALTER TABLE `user_player_ASN`
	ADD CONSTRAINT `pid`
	FOREIGN KEY(`pid`)
	REFERENCES `players`(`pid`);

CREATE TABLE user_group_ASN (
	user_id integer(11) NOT NULL,
	gid char(13) NOT NULL);

ALTER TABLE user_group_ASN
	ADD PRIMARY KEY(user_id, gid);

ALTER TABLE user_group_ASN
	ADD CONSTRAINT `g_user_id`
	FOREIGN KEY(`user_id`)
	REFERENCES `users`(`id`);

ALTER TABLE `user_group_ASN`
	ADD CONSTRAINT `gid`
	FOREIGN KEY(`gid`)
	REFERENCES `groups`(`gid`);

insert into roles (role_name, role_level) VALUES ('SYSTEM ADMINISTRATOR', 0);
insert into roles (role_name, role_level) VALUES ('SERVICE ADMINISTRATOR', 1);
insert into roles (role_name, role_level) VALUES ('GROUP ADMINISTRATOR', 2);
insert into roles (role_name, role_level) VALUES ('PLAYER ADMINISTRATOR', 3);
insert into roles (role_name, role_level) VALUES ('OBSERVER', 3);
insert into roles (role_name, role_level) VALUES ('SCHEDULER', 3);
insert into roles (role_name, role_level) VALUES ('USER MANAGER', 0);
insert into roles (role_name, role_level) VALUES ('GROUP MANAGER', 3);
insert into roles (role_name, role_level) VALUES ('PLAYER MANAGER', 3);

insert into permissions (perm_name, parent_perm_id) VALUES ('ADMINISTRATOR', -1);
insert into permissions (perm_name, parent_perm_id) VALUES ('VIEW USER', -1);
insert into permissions (perm_name, parent_perm_id) VALUES ('CREATE USER', -1);
insert into permissions (perm_name, parent_perm_id) VALUES ('UPDATE USER', -1);
insert into permissions (perm_name, parent_perm_id) VALUES ('DELETE USER', -1);
insert into permissions (perm_name, parent_perm_id) VALUES ('VIEW PARENT GROUP', -1);
insert into permissions (perm_name, parent_perm_id) VALUES ('VIEW OWN GROUP', -1);
insert into permissions (perm_name, parent_perm_id) VALUES ('VIEW CHILD GROUP', -1);
insert into permissions (perm_name, parent_perm_id) VALUES ('VIEW PLAYER', -1);
insert into permissions (perm_name, parent_perm_id) VALUES ('CREATE PLAYER', -1);
insert into permissions (perm_name, parent_perm_id) VALUES ('CREATE GROUP', -1);
insert into permissions (perm_name, parent_perm_id) VALUES ('UPDATE PLAYER', -1);
insert into permissions (perm_name, parent_perm_id) VALUES ('UPDATE GROUP', -1);
insert into permissions (perm_name, parent_perm_id) VALUES ('DELETE PLAYER', -1);
insert into permissions (perm_name, parent_perm_id) VALUES ('DELETE GROUP', -1);
insert into permissions (perm_name, parent_perm_id) VALUES ('VIEW SCHEDULE', -1);
insert into permissions (perm_name, parent_perm_id) VALUES ('VIEW PARENT SCHEDULE', -1);
insert into permissions (perm_name, parent_perm_id) VALUES ('CREATE SCHEDULE', -1);
insert into permissions (perm_name, parent_perm_id) VALUES ('UPDATE SCHEDULE', -1);
insert into permissions (perm_name, parent_perm_id) VALUES ('DELETE SCHEDULE', -1);

/*롤과 퍼미션 연결*/
insert into role_perm_ASN (role_id, perm_id) VALUES (1,1);

insert into role_perm_ASN (role_id, perm_id) VALUES (2,2);
insert into role_perm_ASN (role_id, perm_id) VALUES (2,3);
insert into role_perm_ASN (role_id, perm_id) VALUES (2,4);
insert into role_perm_ASN (role_id, perm_id) VALUES (2,5);
insert into role_perm_ASN (role_id, perm_id) VALUES (2,7);
insert into role_perm_ASN (role_id, perm_id) VALUES (2,8);
insert into role_perm_ASN (role_id, perm_id) VALUES (2,9);
insert into role_perm_ASN (role_id, perm_id) VALUES (2,10);
insert into role_perm_ASN (role_id, perm_id) VALUES (2,11);
insert into role_perm_ASN (role_id, perm_id) VALUES (2,12);
insert into role_perm_ASN (role_id, perm_id) VALUES (2,13);
insert into role_perm_ASN (role_id, perm_id) VALUES (2,14);
insert into role_perm_ASN (role_id, perm_id) VALUES (2,15);
insert into role_perm_ASN (role_id, perm_id) VALUES (2,16);
insert into role_perm_ASN (role_id, perm_id) VALUES (2,17);
insert into role_perm_ASN (role_id, perm_id) VALUES (2,18);
insert into role_perm_ASN (role_id, perm_id) VALUES (2,19);
insert into role_perm_ASN (role_id, perm_id) VALUES (2,20);

insert into role_perm_ASN (role_id, perm_id) VALUES (3,2);
insert into role_perm_ASN (role_id, perm_id) VALUES (3,3);
insert into role_perm_ASN (role_id, perm_id) VALUES (3,4);
insert into role_perm_ASN (role_id, perm_id) VALUES (3,5);
insert into role_perm_ASN (role_id, perm_id) VALUES (3,7);
insert into role_perm_ASN (role_id, perm_id) VALUES (3,8);
insert into role_perm_ASN (role_id, perm_id) VALUES (3,9);
insert into role_perm_ASN (role_id, perm_id) VALUES (3,10);
insert into role_perm_ASN (role_id, perm_id) VALUES (3,11);
insert into role_perm_ASN (role_id, perm_id) VALUES (3,12);
insert into role_perm_ASN (role_id, perm_id) VALUES (3,13);
insert into role_perm_ASN (role_id, perm_id) VALUES (3,14);
insert into role_perm_ASN (role_id, perm_id) VALUES (3,15);
insert into role_perm_ASN (role_id, perm_id) VALUES (3,16);
insert into role_perm_ASN (role_id, perm_id) VALUES (3,17);
insert into role_perm_ASN (role_id, perm_id) VALUES (3,18);
insert into role_perm_ASN (role_id, perm_id) VALUES (3,19);
insert into role_perm_ASN (role_id, perm_id) VALUES (3,20);

insert into role_perm_ASN (role_id, perm_id) VALUES (4,2);
insert into role_perm_ASN (role_id, perm_id) VALUES (4,4);
insert into role_perm_ASN (role_id, perm_id) VALUES (4,9);
insert into role_perm_ASN (role_id, perm_id) VALUES (4,16);
insert into role_perm_ASN (role_id, perm_id) VALUES (4,17);
insert into role_perm_ASN (role_id, perm_id) VALUES (4,18);
insert into role_perm_ASN (role_id, perm_id) VALUES (4,19);
insert into role_perm_ASN (role_id, perm_id) VALUES (4,20);

insert into role_perm_ASN (role_id, perm_id) VALUES (5,6);
insert into role_perm_ASN (role_id, perm_id) VALUES (5,7);
insert into role_perm_ASN (role_id, perm_id) VALUES (5,8);
insert into role_perm_ASN (role_id, perm_id) VALUES (5,9);
insert into role_perm_ASN (role_id, perm_id) VALUES (5,16);
insert into role_perm_ASN (role_id, perm_id) VALUES (5,17);

insert into role_perm_ASN (role_id, perm_id) VALUES (6,7);
insert into role_perm_ASN (role_id, perm_id) VALUES (6,8);
insert into role_perm_ASN (role_id, perm_id) VALUES (6,9);
insert into role_perm_ASN (role_id, perm_id) VALUES (6,16);
insert into role_perm_ASN (role_id, perm_id) VALUES (6,17);
insert into role_perm_ASN (role_id, perm_id) VALUES (6,18);
insert into role_perm_ASN (role_id, perm_id) VALUES (6,19);
insert into role_perm_ASN (role_id, perm_id) VALUES (6,20);

insert into role_perm_ASN (role_id, perm_id) VALUES (7,2);
insert into role_perm_ASN (role_id, perm_id) VALUES (7,3);
insert into role_perm_ASN (role_id, perm_id) VALUES (7,4);
insert into role_perm_ASN (role_id, perm_id) VALUES (7,5);

insert into role_perm_ASN (role_id, perm_id) VALUES (8,7);
insert into role_perm_ASN (role_id, perm_id) VALUES (8,8);
insert into role_perm_ASN (role_id, perm_id) VALUES (8,11);
insert into role_perm_ASN (role_id, perm_id) VALUES (8,13);
insert into role_perm_ASN (role_id, perm_id) VALUES (8,16);

insert into role_perm_ASN (role_id, perm_id) VALUES (9,9);
insert into role_perm_ASN (role_id, perm_id) VALUES (9,10);
insert into role_perm_ASN (role_id, perm_id) VALUES (9,12);
insert into role_perm_ASN (role_id, perm_id) VALUES (9,14);

select * from roles;
select * from permissions;

update permissions SET parent_perm_id = -1;

INSERT INTO users (name,hash,role_ids, parent_id,is_enabled) VALUES ('admin','21232f297a57a5a743894a0e4a801fc3','1','-1','Y');

CREATE TABLE downloadmaster (uuid char(36) NOT NULL, pid char(13) NOT NULL, reg_date BIGINT);

ALTER TABLE downloadmaster
	ADD CONSTRAINT d_master_uuid
	FOREIGN KEY(uuid)
	REFERENCES tasks(uuid);

ALTER TABLE downloadmaster
	ADD CONSTRAINT d_master_pid
	FOREIGN KEY(pid)
	REFERENCES players(pid);

ALTER TABLE downloadmaster
	ADD PRIMARY KEY(uuid, pid);

INSERT INTO groups (gid,name,source,path,password, username,  depth) VALUES ('0000000000000','ROOT NETWORK','','', '', '', 0);

insert into user_group_asn (user_id, gid) values (1, '0000000000000');

CREATE TABLE report_normal_schedule
(
	YYYY                 CHAR(4)  NOT NULL ,
	MM                   CHAR(2)  NOT NULL ,
	DD                   CHAR(2)  NOT NULL ,
	CNT                  int  NULL ,
	pid                  char(13)  NOT NULL 
);


ALTER TABLE report_normal_schedule
	ADD CONSTRAINT XPKreport_normal_schedule PRIMARY KEY  NONCLUSTERED (pid ASC,YYYY ASC,MM ASC,DD ASC);


CREATE TABLE report_play
(
	YYYY                 CHAR(4)  NOT NULL ,
	MM                   CHAR(2)  NOT NULL ,
	DD                   CHAR(2)  NOT NULL ,
	H24                  CHAR(2)  NOT NULL ,
	CNT                  int  NULL ,
	s_id                 bigint  NOT NULL 
);

ALTER TABLE report_play
	ADD CONSTRAINT XPKreport_play PRIMARY KEY  NONCLUSTERED (s_id ASC,YYYY ASC,MM ASC,DD ASC,H24 ASC);

CREATE TABLE report_relay
(
	YYYY                 CHAR(4)  NOT NULL ,
	MM                   CHAR(2)  NOT NULL ,
	DD                   CHAR(2)  NOT NULL ,
	H24                  CHAR(2)  NOT NULL ,
	g_id                 bigint  NOT NULL ,
	CNT                  int  NULL ,
	r_key                VARCHAR(32)  NOT NULL 
);


ALTER TABLE report_relay
	ADD CONSTRAINT XPKreport_relay PRIMARY KEY  CLUSTERED (g_id ASC,r_key ASC,YYYY ASC,MM ASC,DD ASC,H24 ASC);


CREATE TABLE report_running_time
(
	pid                  char(13)  NOT NULL ,
	YYYY                 CHAR(4)  NOT NULL ,
	MM                   CHAR(2)  NOT NULL ,
	DD                   CHAR(2)  NOT NULL ,
	H24                  CHAR(2)  NOT NULL ,
	TM                   int  NULL 
);


ALTER TABLE report_running_time
	ADD CONSTRAINT XPKreport_running_time PRIMARY KEY  NONCLUSTERED (pid ASC,YYYY ASC,MM ASC,DD ASC,H24 ASC);


CREATE TABLE report_time_schedule
(
	pid                  char(13)  NOT NULL ,
	YYYY                 CHAR(4)  NOT NULL ,
	MM                   CHAR(2)  NOT NULL ,
	DD                   CHAR(2)  NOT NULL ,
	H24                  CHAR(2)  NOT NULL ,
	type		     int  NULL,
	CNT                  int  NULL 
);

CREATE TABLE summary_players
(
	pid			char(13)  NOT NULL ,
	YYYY		CHAR(4)  NOT NULL ,
	MM			CHAR(2)  NOT NULL ,
	DD			CHAR(2)  NOT NULL ,
	H24			CHAR(2)  NOT NULL ,
	gid			char(13) NOT NULL,
	name		VARCHAR(255), 
	host		VARCHAR(128), 
	state		int, 
	uptime		int,
	lastconndt	bigint, 
	versionH	int, 
	versionL	int,
	freespace	int,
	memoryusage int,
	cpurate		int,
	volume		int,
	serial_power VARCHAR(10),
	serial_source VARCHAR(50),
	serial_volume int
);

ALTER TABLE summary_players
	ADD CONSTRAINT XPKsummary_players PRIMARY KEY  NONCLUSTERED (pid ASC,YYYY ASC,MM ASC,DD ASC,H24 ASC);

ALTER TABLE report_time_schedule
	ADD CONSTRAINT XPKreport_time_schedule PRIMARY KEY  NONCLUSTERED (pid ASC,YYYY ASC,MM ASC,DD ASC,H24 ASC, type ASC);



ALTER TABLE report_normal_schedule
	ADD CONSTRAINT  R_38 FOREIGN KEY (pid) REFERENCES players(pid)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION;



ALTER TABLE report_play
	ADD CONSTRAINT  R_33 FOREIGN KEY (s_id) REFERENCES screens(s_id)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION;



ALTER TABLE report_relay
	ADD CONSTRAINT  R_36 FOREIGN KEY (g_id) REFERENCES meta_tag_groups(g_id)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION;



ALTER TABLE report_running_time
	ADD CONSTRAINT  R_34 FOREIGN KEY (pid) REFERENCES players(pid)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION;




ALTER TABLE report_time_schedule
	ADD CONSTRAINT  R_37 FOREIGN KEY (pid) REFERENCES players(pid)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION;
