﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.IO;

namespace DigitalSignage.PlayerSync
{
    /// <summary>
    /// 시작 스케줄 동기화 이벤트
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public delegate void StartScheduleEventHandler(object sender, StartScheduleEventArgs e);

    /// <summary>
    /// 시작 스케줄 동기화 이벤트 아규먼트
    /// </summary>
    public class StartScheduleEventArgs : EventArgs
    {
        public string sID = string.Empty;
        public string sStartDT = string.Empty;
        public string sRecvDT = string.Empty;
        /// <summary>
        /// S = 시작, K = 하트비트 체크
        /// </summary>
        public string sType = "S";

        public StartScheduleEventArgs(string id, string startDT, string recvDT, string tpPacket)
        {
            sID = id;
            sStartDT = startDT;
            sRecvDT = recvDT;
            sType = tpPacket;
        }
    }

    /// <summary>
    /// 동기화 서버
    /// </summary>
    public class SyncServer
    {
        private Thread m_serverThread;      
        private int m_port;
        private IPAddress m_clientIP = null;
        private bool m_IsRunning = false;

        public event StartScheduleEventHandler OnStartSchedule = null;

        //public string m_msg = null;
        private string m_guuid = null;
        private string m_startDT = null;
        private string m_recieveDT = null;
        private string m_type = null;

        public SyncServer(int port)
        {
            m_port = port;
        }

        public bool Start(int port, WriteMessageCallback d_WriteMessage)
        //public bool Start(int port)
        {
            if (m_IsRunning)
            {
                return false;
            }

            //WriteMessage = d_WriteMessage;
            
            m_IsRunning = true;

            m_port = port;
            m_serverThread = new Thread(new ThreadStart(ServerProc));
            m_serverThread.IsBackground = true;
            m_serverThread.Start();

            return m_IsRunning;

        }

        public bool Start()
        {
            if (m_IsRunning)
            {
                return false;
            }

            //WriteMessage = d_WriteMessage;

            //m_port = port;
            m_serverThread = new Thread(new ThreadStart(ServerProc));
            m_serverThread.IsBackground = true;
            m_serverThread.Start();
            m_IsRunning = true;

            return true;
        }

        public bool Stop()
        {
            if (!m_IsRunning)
            {
                return false;
            }

            m_serverThread.Abort();
            m_serverThread = null;
            m_IsRunning = false;

            return true;
        }


        public bool SendMessage(string message)
        {
            return true;
        } 

        private void ServerProc()
        {
            try
            {
                Listen();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Err :" + ex.ToString());
            }
        }

        private void Listen()
        {
            TcpListener tcpListener = null;

            try
            {
                tcpListener = new TcpListener(m_port);
                tcpListener.Start();
                
                Console.WriteLine("Wait for the client...");
                while (m_IsRunning)
                {
                    Socket clientSocket = tcpListener.AcceptSocket();
                    Thread th = new Thread(new ParameterizedThreadStart(thRecieve));
                    th.Start(clientSocket);
                }
                Console.WriteLine("Client connected...");
            }
            finally// (Exception ex)
            {
                if (tcpListener == null)
                {
                    tcpListener.Stop();
                }
                //Console.WriteLine("Err :" + ex.ToString());
            }
        }

        private void thRecieve(object sock)
        {
            Socket sockClient = sock as Socket;

            try
            {
                while (m_IsRunning)
                {
                    if (!sockClient.Connected)
                    {
                        return;
                    }
                    else
                    {
                        using (NetworkStream ns = new NetworkStream(sockClient))
                        {
                            using (StreamReader sr = new StreamReader(ns))
                            {
                                String message = sr.ReadLine();
                                string[] splitString = message.Split(',');
                                try
                                {
                                    m_guuid = splitString[0];
                                    m_startDT = splitString[1];
                                    m_recieveDT = splitString[2];
                                    m_type = splitString[3];
                                }
                                catch (Exception ex)
                                {

                                }
                                ///이벤트 전달
                                if (OnStartSchedule != null)
                                {
                                    OnStartSchedule(this, new StartScheduleEventArgs(m_guuid, m_startDT, m_recieveDT, m_type));
                                }
                            }
                        }
                    }
                    Thread.Sleep(100);
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                sockClient.Dispose();
            }
        }

        //private void Recieve()
        //{
        //    try
        //    {
        //        while (true)
        //        {
        //            if (!m_socket.Connected)
        //            {
        //                return;
        //            }
        //            else
        //            {
        //                string message = m_reader.ReadLine();
        //                m_msg = message;
        //                //---
        //                //TODO : 받은 내용 처리 부분 구현
        //                if (message != null)
        //                {
        //                    //WriteMessage(message);
        //                    SendMessage("Keep Alived!!");
        //                }
        //                //--
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {

        //    }
        //    finally
        //    {
        //        m_writer.Close();
        //        m_reader.Close();
        //        m_ns.Close();

        //        m_socket.Close();
        //        m_socket.Shutdown(SocketShutdown.Both);
        //    }
        //}
    }
}
