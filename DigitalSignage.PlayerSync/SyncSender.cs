﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Sockets;
using System.Net;
using System.Threading;
using NLog;

namespace DigitalSignage.PlayerSync
{
    public class SyncSender
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        private int m_port = 0;
        private IPAddress m_senderIP = null;
        private bool m_IsRunning = false;

        Socket sockclient = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
        EndPoint epRemote = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8888);

        public SyncSender(IPAddress ip, int port)
        {
            m_senderIP = ip;
            m_port = port;
        }

        public bool Start()
        {
            try
            {
                epRemote = new IPEndPoint(m_senderIP, m_port);
                m_IsRunning = true;

            }
            catch (Exception err)
            {
                return false;
            }

            return true;
        }

        public bool Stop()
        {
            if (!m_IsRunning)
            {
                return false;
            }

            sockclient.SendTo(ASCIIEncoding.UTF8.GetBytes("UDPSender is end."), epRemote);

            m_IsRunning = false;
            return true;
        }

        public bool SendMessage(string message)
        {
            if (!m_IsRunning)
                return false;
            try
            {
                sockclient.SendTo(ASCIIEncoding.UTF8.GetBytes(message), epRemote);
                logger.Debug("전달 메시지: {0}", message);

            }
            catch (Exception ex)
            {
                logger.Error("전달 중 에러: {0}", ex.ToString());
                return false;
            }
            return true;
        }
    }
}
