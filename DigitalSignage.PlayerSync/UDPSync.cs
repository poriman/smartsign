﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NLog;
using System.Threading;
using DigitalSignage.Common;
using System.Net;

namespace DigitalSignage.PlayerSync
{
    class ParamSender
    {
        public SyncSender m_sender;
        public string m_message;

        public ParamSender(SyncSender sender, string message)
        {
            m_sender = sender;
            m_message = message;
        }
    }

    public class UdpSync
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// 센더 딕셔너리
        /// </summary>
        private Dictionary<string, SyncSender> dicSender;
        /// <summary>
        /// 클라이언트 딕셔너리
        /// </summary>
        private Dictionary<string, SyncListener> dicListener;

        /// <summary>
        /// 서버 딕셔너리 접근자
        /// </summary>
        public Dictionary<string, SyncSender> SenderDictionaries { get { return dicSender; } }

        /// <summary>
        /// 클라이언트 딕셔너리 접근자
        /// </summary>
        public Dictionary<string, SyncListener> ClientDictionaries { get { return dicListener; } }

        public event StartScheduleEventHandler OnStartSchedule = null;

        /// <summary>
        /// 생성자
        /// </summary>
        private UdpSync()
        {
            dicSender = new Dictionary<string, SyncSender>();
            dicListener = new Dictionary<string, SyncListener>();
        }

        /// <summary>
        /// 싱글톤 객체를 위한 Lock 오브젝트
        /// </summary>
        private static object lockObject = new object();

        /// <summary>
        /// 싱글톤 객체
        /// </summary>
        private static UdpSync _instance = null;

        /// <summary>
        /// 싱글톤 객체 반환
        /// </summary>
        public static UdpSync GetInstance
        {
            get
            {
                lock (lockObject)
                {
                    if (_instance == null)
                        _instance = new UdpSync();

                    return _instance;
                }
            }
        }

        /// <summary>
        /// 슬래이브 목록에게 정보를 전달한다.
        /// </summary>
        /// <param name="guuid"></param>
        /// <param name="startDT"></param>
        /// <param name="eventArgs"></param>
        public void SendToSlaves(string guuid, string startDT, string eventArgs = "S")
        {
            //  AsyncSendToServer 기능을통해 보냄
            foreach (KeyValuePair<string, SyncSender> dicSend in dicSender)
            {
                if (dicSend.Key.Contains(guuid))
                {
                    string message = string.Format("{0},{1},{2},{3}", guuid, startDT, DateTime.Now.ToString("yyyyMMddHHmmssfff"), eventArgs);
                    logger.Debug("전달할 슬래이브 목록: {0} - {1}", dicSend.Key, guuid);

                    //if (dicSend.Value.ConnectionCheck())
                    {
                        SyncSender sender = dicSend.Value;
                        AsyncSendToServer(sender, message);
                    }
                }
            }
        }

        public void AddSyncClient(string id, IPAddress ip, int port)
        {
            if (!dicSender.ContainsKey(id))
            {
                try
                {
                    dicSender.Add(id, new SyncSender(ip, port));
                    dicSender[id].Start();
                }
                catch (Exception ex)
                {
                    logger.Error("Err :" + ex.ToString());
                }
            }
        }

        public void RemoveSyncClient(string id)
        {
            if (dicSender.ContainsKey(id))
            {
                try
                {
                    dicSender[id].Stop();
                    dicSender.Remove(id);
                }
                catch (Exception ex)
                {
                    logger.Error("Err :" + ex.ToString());
                }
            }
        }

        public void AddSyncServer(string id, int port)
        {
            if (!dicListener.ContainsKey(id))
            {
                try
                {
                    dicListener.Add(id, new SyncListener(port));
                    dicListener[id].Start();
                    dicListener[id].OnStartSchedule += new StartScheduleEventHandler(UdpSync_OnStartSchedule);
                }
                catch (Exception ex)
                {
                    logger.Error("Err :" + ex.ToString());
                }
            }
        }

        void UdpSync_OnStartSchedule(object sender, StartScheduleEventArgs e)
        {
            if (OnStartSchedule != null)
            {
                OnStartSchedule(this, e);
            }
        }

        public void RemoveSyncServer(string id)
        {
            if (dicListener.ContainsKey(id))
            {
                try
                {
                    dicListener[id].Stop();
                    dicListener.Remove(id);
                }
                catch (Exception ex)
                {
                    logger.Error("Err :" + ex.ToString());
                }
            }
        }


        private void AsyncSendToServer(SyncSender sender, string message)
        {
            Thread th = new Thread(new ParameterizedThreadStart(thSendMessage));
            th.Priority = ThreadPriority.Highest;
            th.Start(new ParamSender(sender, message));
        }

        private void thSendMessage(object param)
        {
            ParamSender sender = param as ParamSender;
            bool bFirst = true;

            long lPlaytime = ScreenStartInfoInSchedule.GetInstance.ScheduleDuration * 1000;

            logger.Debug("전달 쓰레드 생성: 전달할 총 시간({0}), 전달할 guuid({1})", lPlaytime, ScreenStartInfoInSchedule.GetInstance.GroupScheduleID);

            long lCurrDuration = 0;
            while (sender.m_message.Contains(ScreenStartInfoInSchedule.GetInstance.GroupScheduleID) && lCurrDuration < lPlaytime)
            {
                try
                {
                    if (bFirst)
                    {
                        sender.m_sender.SendMessage(sender.m_message);
                        bFirst = false;
                    }
                    else
                    {
                        sender.m_sender.SendMessage(String.Format("{0},{1},{2},K", ScreenStartInfoInSchedule.GetInstance.GroupScheduleID, ScreenStartInfoInSchedule.GetInstance.ScreenStartDT.ToString("yyyyMMddHHmmssfff"), DateTime.Now.ToString("yyyyMMddHHmmssfff")));
                    }
                    Thread.Sleep(4000);
                    lCurrDuration += 4000;
                }
                catch { }
            }
        }

    }
}
