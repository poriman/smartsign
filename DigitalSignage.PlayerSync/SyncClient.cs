﻿using System;
using System.Net.Sockets;
using System.Net;
using System.IO;
using System.Threading;
using NLog;

namespace DigitalSignage.PlayerSync
{
    public class SyncClient
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        private Socket m_socket;
        private Thread m_clientThread;  
        private int m_port = 0;
        private IPAddress m_serverIP = null;
        private bool m_IsRunning = false;

        private NetworkStream m_ns = null;
        private StreamReader m_reader = null;
        private StreamWriter m_writer = null;

        //private WriteMessageCallback WriteMessage = null;
        private TimerCallback m_tbc = null;
        private Timer m_tm;
        
        public SyncClient(IPAddress ip, int port)
        {
            m_serverIP = ip;
            m_port = port;
        }

        public bool Start(int port, WriteMessageCallback d_WriteMessage)
        //public bool Start(IPAddress ip, int port, WriteMessageCallback d_WriteMessage)
        //public bool Start(IPAddress ip, int port)
        {
            if (m_IsRunning == true)
            {
                return false;
            }

            //WriteMessage = d_WriteMessage;

            m_port = port;
            m_serverIP = IPAddress.Parse("127.0.0.1");

            m_clientThread = new Thread(new ThreadStart(ClientProc));
            m_clientThread.IsBackground = true;
            m_clientThread.Start();

            m_IsRunning = true;

            //SendMessage(DateTime.Now.ToString("yyyyMMddHHmmssfff"));

            return true;
        }

        public bool Start()
        //public bool Start(IPAddress ip, int port, WriteMessageCallback d_WriteMessage)
        //public bool Start(IPAddress ip, int port)
        {
            if (m_IsRunning == true)
            {
                return false;
            }

            m_clientThread = new Thread(new ThreadStart(ClientProc));
            m_clientThread.IsBackground = true;
            m_clientThread.Start();

            m_IsRunning = true;

            //SendMessage(DateTime.Now.ToString("yyyyMMddHHmmssfff"));

            return true;
        }

        public bool Stop()
        {
            if (!m_IsRunning)
            {
                return false;
            }

            if (m_tm != null)
            {
                m_tm.Dispose();
            }

            if (m_clientThread != null)
            {
                m_clientThread.Abort();
                m_clientThread = null;
            }

            m_IsRunning = false;
            return true;
        }

        public bool SendMessage(string message)
        {
            if (m_socket == null || !m_socket.Connected)
                return false;
            try
            {
                m_writer.WriteLine(message);
                m_writer.Flush();
                logger.Debug("전달 메시지: {0}", message); 

            }
            catch (Exception ex)
            {
                logger.Error("전달 중 에러: {0}", ex.ToString());
                return false;
            }
            return true;
        }

        private void ClientProc()
        {
                while (m_IsRunning)
                {
                    try
                    {
                        if(!ConnectionCheck())
                            Connect();

                        ////--Test용 코드---
                        //m_tbc = new TimerCallback(Callback_OnTimer);
                        //m_tm = new Timer(m_tbc, null, Timeout.Infinite, Timeout.Infinite);
                        //m_tm.Change(0, 1000);
                        ////----------------
                        Recieve();
                        Thread.Sleep(1000);
                    }
                    catch (Exception ex)
                    {
                        logger.Debug("Err :" + ex.ToString());
                        Thread.Sleep(10000);
                    }
                }

        }
        
        //테스트용 코드
        //void Callback_OnTimer(object state)
        //{
        //    if (m_socket.Connected)
        //    {
        //        SendMessage(DateTime.Now.ToString("yyyyMMddhhmmssfff"));
        //    }
        //    else
        //    {
        //        this.Stop();
        //    }
        //}

        private void Connect()
        {
            if (m_serverIP == null)
            {
                return;
            }
            else if (m_port < 0)
            {
                return;
            }

            m_socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            m_socket.Connect(new IPEndPoint(m_serverIP, m_port));

            try {
                if (m_socket.Connected)
                {
                    m_ns = new NetworkStream(m_socket);
                    m_reader = new StreamReader(m_ns);
                    m_writer = new StreamWriter(m_ns);
                }
            } catch(Exception ex) {
                logger.Debug("Err :" + ex.ToString());
            }
        }

        public bool ConnectionCheck()
        {
            if (m_socket == null || !m_socket.Connected)
            {
                return false;
            }
            return true;
        }

        private void Recieve()
        {
            try
            {
                while (m_IsRunning)
                {
                    if (!m_socket.Connected)
                    {
                        return;
                    }
                    else if (m_socket.Available > 0)
                    {
                        string message = m_reader.ReadLine();
                    }
                    Thread.Sleep(100);
                }
            }
            catch (Exception ex)
            {
                logger.Debug("Err : " + ex.Message);
            }
            finally
            {
                m_tm.Dispose();

                m_writer.Close();
                m_reader.Close();
                m_ns.Close();

                m_socket.Close();
                m_socket.Shutdown(SocketShutdown.Both);
                m_socket = null;
            }
            return;
        }

    }
}
