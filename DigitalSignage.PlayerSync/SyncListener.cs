﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Net;
using System.ComponentModel;
using System.Net.Sockets;
using NLog;

namespace DigitalSignage.PlayerSync
{

    public class SyncListener
    {
        /// <summary>
        /// 로컬 로깅 Instance
        /// </summary>
        private static Logger logger = LogManager.GetCurrentClassLogger();

        private Thread m_serverThread;      
        private int m_port;
        private IPAddress m_clientIP = null;
        private bool m_IsRunning = false;

        public event StartScheduleEventHandler OnStartSchedule = null;

        //public string m_msg = null;
        private string m_guuid = null;
        private string m_startDT = null;
        private string m_recieveDT = null;
        private string m_type = null;

        /// <summary>
        /// 리스너 소켓
        /// </summary>
        Socket sockListener = null;

        /// <summary>
        /// UDP 관련 EndPoint
        /// </summary>
        EndPoint remoteEndPoint = new IPEndPoint(IPAddress.Any, 0);

        bool isPrepared = false;
        bool isRunning = false;

        /// <summary>
        /// Recv 함수
        /// </summary>
        BackgroundWorker bwReceiver = null;

        /// <summary>
        /// 서버 소켓 관리 함수
        /// </summary>
        BackgroundWorker bwAccepter = null;

        public SyncListener(int port)
        {
            m_port = port;
        }

        public bool Start()
        {
            if (m_IsRunning)
            {
                return false;
            }
            m_IsRunning = true;

            logger.Info("Start Called!");

            m_serverThread = new Thread(new ThreadStart(ServerProc));
            m_serverThread.IsBackground = true;
            m_serverThread.Start();

            return true;
        }

        public bool Stop()
        {
            if (!m_IsRunning)
            {
                return false;
            }

            logger.Info("Stop Called!");
            m_IsRunning = false;
            
            m_serverThread.Abort();
            m_serverThread = null;


            return true;
        }

        private void ServerProc()
        {
            try
            {
                Listen();
            }
            catch (Exception ex)
            {
                logger.Error("Err :" + ex.ToString());
            }
        }

        private void Listen()
        {
            Socket udpListener = null;

            int nRet = 0;
            do
            {
                try
                {
                    udpListener = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);

                    IPEndPoint ep = new IPEndPoint(IPAddress.Any, m_port);
                    udpListener.Bind(ep);
                    udpListener.Blocking = true;     // The server socket is working in blocking mode     

                    logger.Info("Wait for the client...");
                    while (m_IsRunning)
                    {
                        if (udpListener != null && udpListener.IsBound)
                        {
                            byte[] arrBytes = new byte[1024];
                            int nRecved = 0;

                            /// <summary>
                            /// UDP 관련 EndPoint
                            /// </summary>
                            EndPoint remoteEndPoint = new IPEndPoint(IPAddress.Any, 0);

                            if (0 < (nRecved = udpListener.ReceiveFrom(arrBytes, ref remoteEndPoint)))
                            {
                                String Message = ASCIIEncoding.ASCII.GetString(arrBytes, 0, nRecved);

                                string[] splitString = Message.Split(',');
                                try
                                {
                                    m_guuid = splitString[0];
                                    m_startDT = splitString[1];
                                    m_recieveDT = splitString[2];
                                    m_type = splitString[3];

                                    if (OnStartSchedule != null)
                                    {
                                        OnStartSchedule(this, new StartScheduleEventArgs(m_guuid, m_startDT, m_recieveDT, m_type));
                                    }
                                }
                                catch (Exception ex)
                                {
                                    logger.Error(ex.ToString());
                                }
                                ///이벤트 전달
                            }
                        }

                    }

                    if (!m_IsRunning)
                    {
                        logger.Info("All Client connected...");
                        break;
                    }

                }
                catch (Exception ex)
                {
                    logger.Error(ex.ToString());
                }
                finally// (Exception ex)
                {
                    if (udpListener == null)
                    {
                        udpListener.Close();
                    }
                    //Console.WriteLine("Err :" + ex.ToString());
                }
                Thread.Sleep(1000);
            } while (nRet++ < 3);

            logger.Info("Listener Closed...");
        }
    }
}
