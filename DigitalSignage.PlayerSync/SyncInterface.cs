﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigitalSignage.PlayerSync
{
    interface SyncInterface
    {
        bool Start(int m_port, WriteMessageCallback d_WriteMessage);
        //bool Start(IPAddress ip, int port, WriteMessageCallback d_WriteMessage);

        bool Stop();

        bool SendMessage(string message);
    }
}
