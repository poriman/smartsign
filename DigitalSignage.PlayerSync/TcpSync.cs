﻿using System.Collections.Generic;
using System.Net;
using System.Threading;
using System;
using DigitalSignage.Common;
using NLog;

namespace DigitalSignage.PlayerSync
{
    public class ParamClient {
        public SyncClient m_client;
        public string m_message;

        public ParamClient(SyncClient client, string message)
        {
            m_client = client;
            m_message = message;
        }
    }

    /// <summary>
    /// TCP Sync 요소 기술 관리 Class
    /// </summary>
    public class TcpSync
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// 서버 딕셔너리
        /// </summary>
        private Dictionary<string, SyncServer> dicServer;
        /// <summary>
        /// 클라이언트 딕셔너리
        /// </summary>
        private Dictionary<string, SyncClient> dicClient;
        /// <summary>
        /// 쓰레드 딕셔너리
        /// </summary>
        private Dictionary<string, Thread> dicThread;

        /// <summary>
        /// 서버 딕셔너리 접근자
        /// </summary>
        public Dictionary<string, SyncServer> ServerDictionaries { get { return dicServer; } }

        /// <summary>
        /// 클라이언트 딕셔너리 접근자
        /// </summary>
        public Dictionary<string, SyncClient> ClientDictionaries { get { return dicClient; } }

        /// <summary>
        /// 쓰레드 딕셔너리 접근자
        /// </summary>
        public Dictionary<string, Thread> ThreadDictionaries { get { return dicThread; } }

        public event StartScheduleEventHandler OnStartSchedule = null;

        /// <summary>
        /// 생성자
        /// </summary>
        private TcpSync()
        {
            dicServer = new Dictionary<string, SyncServer>();
            dicClient = new Dictionary<string, SyncClient>();
        }

        /// <summary>
        /// 싱글톤 객체를 위한 Lock 오브젝트
        /// </summary>
        private static object lockObject = new object();

        /// <summary>
        /// 싱글톤 객체
        /// </summary>
        private static TcpSync _instance = null;

        /// <summary>
        /// 싱글톤 객체 반환
        /// </summary>
        public static TcpSync GetInstance
        {
            get
            {
                lock (lockObject)
                {
                    if (_instance == null)
                        _instance = new TcpSync();

                    return _instance;
                }
            }
        }

        /// <summary>
        /// 슬래이브 목록에게 정보를 전달한다.
        /// </summary>
        /// <param name="startedt"></param>
        public void SendToSlaves(string guuid, string startDT)
        {
            //  AsyncSendToServer 기능을통해 보냄
            foreach (KeyValuePair<string, SyncClient> dicSend in dicClient)
            {
                if(dicSend.Key.Contains(guuid))
                {
                    string message = string.Format("{0},{1},{2},S", guuid, startDT, DateTime.Now.ToString("yyyyMMddHHmmssfff"));
                    logger.Debug("전달할 슬래이브 목록: {0} - {1}", dicSend.Key, guuid); 
                    
                    if (dicSend.Value.ConnectionCheck())
                    {
                        SyncClient client = dicSend.Value;
                        AsyncSendToServer(client, message);
                    }
                }
            }
        }

        public void AddSyncClient(string id, IPAddress ip, int port)
        {
            if (!dicClient.ContainsKey(id))
            {
                try
                {
                    dicClient.Add(id, new SyncClient(ip, port));
                    dicClient[id].Start();
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Err :" + ex.ToString());
                }
            }
        }

        public void RemoveSyncClient(string id)
        {
			if (dicClient.ContainsKey(id))
            {
            	try
            	{
                	dicClient[id].Stop();
                	dicClient.Remove(id);
            	}
            	catch (Exception ex)
            	{
                	Console.WriteLine("Err :" + ex.ToString());
            	}
			}
        }

        public void AddSyncServer(string id, int port)
        {
            if (!dicServer.ContainsKey(id))
            {
                try
                {
                    dicServer.Add(id, new SyncServer(port));
                    dicServer[id].Start();
                    dicServer[id].OnStartSchedule += new StartScheduleEventHandler(TcpSync_OnStartSchedule);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Err :" + ex.ToString());
                }
            }
        }

        void TcpSync_OnStartSchedule(object sender, StartScheduleEventArgs e)
        {
            if (OnStartSchedule != null)
            {
                OnStartSchedule(this, e);
            }
        }

        public void RemoveSyncServer(string id)
        {
            if (dicServer.ContainsKey(id))
            {
                try
                {
                    dicServer[id].Stop();
                    dicServer.Remove(id);
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Err :" + ex.ToString());
                }
            }
        }

        private void AsyncSendToServer(SyncClient client, string message)
        {
            Thread th = new Thread(new ParameterizedThreadStart(thSendMessage));
            th.Start(new ParamClient(client,message));
        }

        private void thSendMessage(object param)
        {
            ParamClient client = param as ParamClient;
            bool bFirst = true;

            long lPlaytime = ScreenStartInfoInSchedule.GetInstance.ScheduleDuration * 1000;

            logger.Debug("전달 쓰레드 생성: 전달할 총 시간({0}), 전달할 guuid({1})", lPlaytime, ScreenStartInfoInSchedule.GetInstance.GroupScheduleID); 

            long lCurrDuration = 0;
            while (client.m_message.Contains(ScreenStartInfoInSchedule.GetInstance.GroupScheduleID) && lCurrDuration < lPlaytime)
            {
                try
                {
                    if (bFirst)
                    {
                        client.m_client.SendMessage(client.m_message);
                        bFirst = false;
                    }
                    else
                    {
                        client.m_client.SendMessage(String.Format("{0},{1},{2},K", ScreenStartInfoInSchedule.GetInstance.GroupScheduleID, ScreenStartInfoInSchedule.GetInstance.ScreenStartDT.ToString("yyyyMMddHHmmssfff"), DateTime.Now.ToString("yyyyMMddHHmmssfff")));
                    }
                    Thread.Sleep(4000);
                    lCurrDuration += 4000;
                }
                catch { }
            } 

        }
    }
}
