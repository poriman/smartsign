﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.IO;

namespace DigitalSignage.TouchScreenAgent.Model
{
    public class Program
    {
        public double DisplayWidth { get; set; }
        public double DisplayHeight { get; set; }
        public string FullPath { get; private set; }
        public string FileName { get; set; }
        public double XPosition { get; set; }
        public double YPosition { get; set; }

        public Program() : this("", new Size(0, 0))
        {            
        }

        public Program(string _programFullPath, Size size)
        {
            this.FullPath = _programFullPath;
            this.DisplayWidth = size.Width;
            this.DisplayHeight = size.Height;
        }

        public void InitProgram(string _programFullPath, Size size)
        {
            this.FullPath = _programFullPath;
            this.DisplayWidth = size.Width;
            this.DisplayHeight = size.Height;
        }

        public string ProgramDirectoryPath()
        {
            if (string.IsNullOrEmpty(FullPath) != true)
            {
                FileInfo fileinfo = new FileInfo(FullPath);
                if (fileinfo != null)
                {
                    DirectoryInfo dirInfo = fileinfo.Directory;
                    return dirInfo.FullName;
                }
            }

            return "";
        }
    }
}
