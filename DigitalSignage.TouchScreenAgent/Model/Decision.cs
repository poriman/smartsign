﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigitalSignage.TouchScreenAgent.Model
{
    public class Decision
    {
        public Guid SourceScreenID { get; set; }
        public Guid SourceScreenControlID { get; set; }
        public string SourceScreenControlValueType { get; set; }

        public List<TargetScreenInfo> DecisionTargetScreenInfos = new List<TargetScreenInfo>();
    }

    public class TargetScreenInfo
    {
        public Guid TargetScreenID { get; set; }
        public string Value1 { get; set; }
        public string Value1Condition { get; set; }
        public string Value2 { get; set; }
        public string Value2Condition { get; set; }
        public string ControlValueType { get; set; }
        public string ConditionType { get; set; }
    }
}
