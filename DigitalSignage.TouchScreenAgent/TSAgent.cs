﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml.Linq;
using DigitalSignage.TouchScreenAgent.Model;
using System.Windows;
using System.Windows.Controls;
using System.Xml;
using System.Windows.Markup;
using System.Windows.Media;
using System.Runtime.Remoting.Messaging;

namespace DigitalSignage.TouchScreenAgent
{
    public class TSAgent
    {
        public delegate void ChangePaneScreenDelegate(string paneScreenID, object paneScreenView, bool isLinked, bool isScreenRepeat);

        private UIElement MainTSOwner;
        private static readonly TSAgent theOneAndOnly;
        private string ProgramFullPath { get; set; }
        private Program program;
        private MainTSWindow mainTSWindow;
        private XElement RootXmlElement;

        public ChangePaneScreenDelegate ChangePaneScreen;

        public static TSAgent TheOnlyTSAgent
        {
            get { return theOneAndOnly; }
        }

        static TSAgent()
        {
            theOneAndOnly = new TSAgent();
        }

        private TSAgent()
        {            
        }
       
        public static bool IsSupportTouchSystem()
        {
            bool isSupport = true;

            return isSupport;
        }

        public static bool IsSupportTouchScreen(string _programFilePath)
        {
            bool isSupport = true;

            return isSupport;
        }

        /// <summary>
        /// 사용하지 않음.
        /// </summary>
        /// <param name="loadProgramFilePath"></param>
        /// <param name="windowSize"></param>
        /// <returns></returns>
        public bool InitTouchScreenAgent(string loadProgramFilePath, Size windowSize)
        {
            if (string.IsNullOrEmpty(loadProgramFilePath) == true)
                return false;

            ProgramFullPath = loadProgramFilePath;

            return LoadProgram(ProgramFullPath, windowSize.Width, windowSize.Height);
        }

        /// <summary>
        /// Program 파일 로드시 NEPlayer에서 호출된다.
        /// Main Touch Screen Widnow를 생성한다.
        /// 
        /// </summary>
        /// <param name="loadProgramFilePath"></param>
        private bool LoadProgram(string loadProgramFilePath, double width, double height)
        {
            if (CheckFileValidation(loadProgramFilePath) == false)
            {
                //MessageBox.Show("파일이 존재하지 않습니다.");
                return false;
            }

            try
            {
                if (program != null)
                    program = null;
                if (mainTSWindow != null)
                    mainTSWindow = null;

                program = new Program(loadProgramFilePath, new Size(width, height));
                mainTSWindow = new MainTSWindow(new Size(width, height));

                mainTSWindow.SendTargetTSScreenID = new MainTSWindow.SendTargetTSScreenIDDelegate(SendTargetTSScreenIDFunc);
                mainTSWindow.CreateTSWorkflowProcess(GetTSMappingFilePath(loadProgramFilePath));

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }            
        }

        private void SendTargetTSScreenIDFunc(Guid targetGuid, object ViewObj, bool IsNEScreenRepeat)
        {
            string mappingFilePath = this.GetTSMappingFilePath(this.ProgramFullPath);

            bool isLinked = true;
            string paneScreenID = this.GetPaneScreenIDFromMappingFile(mappingFilePath, targetGuid.ToString(), out isLinked);

            ChangePaneScreen(paneScreenID, ViewObj, isLinked, IsNEScreenRepeat);
        }

        private string GetTouchScreenIDFromMappingFile(string mappingFilePath, string psID)
        {
            XElement tsMappingXml = XElement.Load(mappingFilePath);

            IEnumerable<XElement> ScreenMappingList = tsMappingXml.Element("Mapping").Elements("ScreenMapping");

            foreach (XElement item in ScreenMappingList)
            {
                string PaneScreenID = item.Attribute("PaneScreenID").Value;
                string TouchScreenID = item.Attribute("TouchScreenID").Value;

                if (PaneScreenID.Equals(psID) == true)
                {
                    return TouchScreenID;
                }
            }

            return "";
        }

        private string GetPaneScreenIDFromMappingFile(string mappingFilePath, string tsID, out bool isLinked)
        {
            XElement tsMappingXml = XElement.Load(mappingFilePath);

            IEnumerable<XElement> ScreenMappingList = tsMappingXml.Element("Mapping").Elements("ScreenMapping");

            foreach (XElement item in ScreenMappingList)
            {
                string PaneScreenID = item.Attribute("PaneScreenID").Value;
                string TouchScreenID = item.Attribute("TouchScreenID").Value;
                isLinked = true;
                if (bool.TryParse(item.Attribute("IsLinked").Value, out isLinked) == false)
                    isLinked = true;

                if (TouchScreenID.Equals(tsID) == true)
                {
                    return PaneScreenID;
                }
            }
            isLinked = true;
            return "";
        }


        public string GetTSWorkflowDesignerFilePath(string loadProgramFilePath)
        {
            FileInfo fileInfo = new FileInfo(loadProgramFilePath);

            return loadProgramFilePath.Replace(fileInfo.Extension, ".tsview");
        }

        /// <summary>
        /// program.xml 파일 경로를 얻어서 동일 경로의 tamap 파일을 가져온다.
        /// </summary>
        /// <param name="loadProgramFilePath"></param>
        /// <returns></returns>
        public string GetTSMappingFilePath(string loadProgramFilePath)
        {
            if (File.Exists(loadProgramFilePath) == false)
                return "";

            XElement program = XElement.Load(loadProgramFilePath);
            XElement touchMapping = program.Element("touchmappingfile");

            FileInfo fileInfo = new FileInfo(loadProgramFilePath);
            string touchMappingFile = touchMapping.Attribute("source").Value;
            return loadProgramFilePath.Replace(fileInfo.Name, touchMappingFile);
        }
      
        /// <summary>
        /// Program 파일, 맵핑 파일, Workflow Design 파일 체크한다.
        /// </summary>
        /// <param name="programFilePath"></param>
        /// <returns></returns>
        private bool CheckFileValidation(string programFilePath)
        {
            try
            {
                string mappingFilePath = this.GetTSMappingFilePath(programFilePath);
                if (File.Exists(mappingFilePath) == false)
                    throw new Exception(mappingFilePath + " 파일이 존재하지 않습니다.");                

                return true;
            }
            catch (Exception ex)
            {
                //Console.WriteLine(ex.Message);
            }

            return false;
        }

        /// <summary>
        /// NEPlayer 화면에 Display되는 Program의 Pane 구성 정보를 분석하여 Main Touch Screen Widnow에 적용한다.
        /// </summary>
        /// <param name="grid">NEPlayer 화면을 구성하는 Root Grid</param>
        public void CreateTouchScreenForm(Grid grid, Dictionary<string, object> paneScreenItems, UIElement windowOwner)
        {
            if (grid != null)
            {
                MainTSOwner = windowOwner;

                CreateTouchScreenFormAsyncMethod(grid, paneScreenItems);
            }
        }

        private bool CreateTouchScreenFormAsyncMethod(Grid grid, Dictionary<string, object> paneScreenItems)
        {
            Grid tsGrid = CopyNEPlayerUI(grid, Grid.GetRow(grid), Grid.GetColumn(grid)) as Grid;


            if (this.mainTSWindow != null)
            {
                if (tsGrid != null)
                {
                    this.mainTSWindow.Content = tsGrid;
                    if (program == null)
                        throw new Exception("NE Player Program 인스턴스 오류!!!");

                    mainTSWindow.ShowDialog();
                }
            }
            
            return true;
        }
      

        public void DisplayTouchScreenView(string paneScreenID, object objView)
        {
            DisplayTSView(paneScreenID, objView);
        }

        /// <summary>
        /// NEPlayer의 Screen 구조로 TouchScreen 화면을 생성한다.
        /// </summary>
        /// <param name="orig"></param>
        /// <param name="row"></param>
        /// <param name="col"></param>
        /// <returns></returns>
        public static UIElement CopyNEPlayerUI(Grid orig, int row, int col)
        {
            if (orig != null && orig is Grid)
            {
                Grid grid = new Grid();
                grid.Tag = orig;//NEPlayer의 Screen과 서로 연관시킨다.

                //grid.ShowGridLines = true;
                grid.Margin = new Thickness(0);
                Grid.SetColumn(grid, col);
                Grid.SetRow(grid, row);
                grid.UpdateLayout();

                RowDefinitionCollection rowCollection = orig.RowDefinitions;
                foreach (RowDefinition rowdef in rowCollection)
                {
                    RowDefinition rowDefTemp = new RowDefinition();
                    rowDefTemp.Height = rowdef.Height;
                    grid.RowDefinitions.Add(rowDefTemp);
                }

                ColumnDefinitionCollection colCollection = orig.ColumnDefinitions;
                foreach (ColumnDefinition coldef in colCollection)
                {
                    ColumnDefinition colDefTemp = new ColumnDefinition();
                    colDefTemp.Width = coldef.Width;
                    grid.ColumnDefinitions.Add(colDefTemp);
                }

                if (orig.Children.Count > 0)
                {
                    foreach (UIElement child in orig.Children)
                    {
                        if (child is Grid)
                        {
                            Grid gridTemp =
                                CopyNEPlayerUI(child as Grid, Grid.GetRow(child), Grid.GetColumn(child)) as Grid;
                            grid.Children.Add(gridTemp);
                        }
                        else
                        {
                            Border item = new Border();

                            item.Tag = (child as Border).Child;//NEPlayer의 Screen과 서로 연관시킨다. (PlaylistPlayer 객체)
                            item.BorderThickness = new Thickness(0);
                            item.Margin = new Thickness(0);
                            item.ClipToBounds = true;
                            grid.Children.Add(item);
                            Grid.SetColumn(item, Grid.GetColumn(child));
                            Grid.SetRow(item, Grid.GetRow(child));
                            //item.Background = Brushes.Transparent;//이값을 주어야 클릭 이벤트 처리됨.
                            item.MouseDown += new System.Windows.Input.MouseButtonEventHandler(TouchScreenBorder_MouseDown);
                            
                            item.UpdateLayout();                            
                        }
                    }
                }

                return grid;
            }

            return null;
        }     

        static void TouchScreenBorder_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            Border border = sender as Border;
            if (border != null)
            {
                TSScreen tsScreen = border.Child as TSScreen;
                if (tsScreen != null)
                {                  
                    //Console.WriteLine("Mouse Down Pane Screen : {0}", tsScreen.Name);
                    tsScreen.ShowTSScreen();
                    tsScreen.RestartTouchScreenTimeout();
                }
            }
        }      

        private IEnumerable<Border> GetContents(Grid grid)
        {
            List<Border> borders = new List<Border>();
            IEnumerable<Border> borderCollection = grid.Children.OfType<Border>();
            borders.AddRange(borderCollection);
            
            IEnumerable<Grid> gridCollection = grid.Children.OfType<Grid>();
            foreach (Grid child in gridCollection)
            {
                borderCollection = child.Children.OfType<Border>();
                borders.AddRange(borderCollection);
            }

            return borders.ToArray();
        }

        /// <summary>
        /// 현재 NEPlayer에 로드되어 있는 모든 Pane Screen을 Display한다.
        /// </summary>
        /// <param name="tsItems">Pane Screen에 구성되어 있는 PlaylistPlayer 인스턴스와 ID의 집합</param>
        private void DisplayAllTSView(Dictionary<string, object> tsItems)
        {
            try
            {
                foreach (KeyValuePair<string, object> item in tsItems)
                {
                    DisplayTSView(item.Key, item.Value);
                }
            }
            catch (Exception ex)
            {
                //Console.WriteLine(ex.Message);
            }
        }

        /// <summary>
        /// 현재 NEPlayer에 로드되어 있는 하나의 Pane Screen을 Display한다.
        /// </summary>
        /// <param name="screenID">Playlist의 ID</param>
        /// <param name="playlistPlayer">Pnae Screen을 구성하는 PlaylistPlayer 인스턴스</param>
        public void DisplayTSView(string paneScreenID, object playlistPlayer)
        {
            try
            {                
                Canvas content = CreateTouchScreenUI(paneScreenID, playlistPlayer);                
                if (content != null)
                {
                    if (mainTSWindow.Content != null && mainTSWindow.Content is Grid)
                    { 
                        IEnumerable<Border> borders = GetContents(mainTSWindow.Content as Grid);

                        Border border = borders.Where(w => w.Tag.Equals(playlistPlayer) == true).SingleOrDefault();
                        if (border != null)
                        {
                            //content.Width = border.Width;
                            //content.Height = border.Height;

                            double scaleX = border.ActualWidth / content.Width;
                            double scaleY = border.ActualHeight / content.Height;
                            content.RenderTransform = new ScaleTransform(scaleX, scaleY);

                            content.Width = border.ActualWidth;
                            content.Height = border.ActualHeight;

                            border.Child = content;
                            
                            content.Tag = playlistPlayer;//터치스크린의 화면과 NEPlayer의 화면을 연결하기 위한 Tag 설정.
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }  

        private Canvas CreateTouchScreenUI(string _paneScreenID, object playlistPlayer)
        {
            Canvas content = null;
            string mappingFilePath = this.GetTSMappingFilePath(program.FullPath);
            string TouchScreenID = this.GetTouchScreenIDFromMappingFile(mappingFilePath, _paneScreenID);
            if ( string.IsNullOrEmpty(TouchScreenID) != true)
            {
                content = mainTSWindow.DrawTSView(TouchScreenID);
                return content;
            }

            return content;
        }

        private void LoadTouchScreenDesignInfo(string FullName)
        {
            try
            {
                XElement xmlData = XElement.Load(FullName);
            }
            catch (Exception ex)
            {
            }
        }        

        public static UIElement CloneElement(UIElement orig)
        {
            if (orig == null)
                return (null);

            string s = XamlWriter.Save(orig);
            StringReader stringReader = new StringReader(s);

            XmlReader xmlReader = XmlTextReader.Create(stringReader, new XmlReaderSettings());

            return (UIElement)XamlReader.Load(xmlReader);
        }
             
        public void PlayPreviewTouchUI(string FileName)
        {
           
        }

    }
}
