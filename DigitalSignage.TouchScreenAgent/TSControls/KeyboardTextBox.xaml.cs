﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DigitalSignage.TouchScreenAgent.TSControls
{
    /// <summary>
    /// Interaction logic for KeyboardTextBox.xaml
    /// </summary>
    public partial class KeyboardTextBox : TextBox
    {
        public KeyboardTextBox()
        {
            InitializeComponent();
            ARiaChoWpf.TouchScreenKeyboard.Keyboard.TouchScreenKeyboard.SetTouchScreenKeyboard(this, true);
        }

        protected override void OnLostFocus(RoutedEventArgs e)
        {
            
            base.OnLostFocus(e);
        }

        public void OnClose()
        {
            ARiaChoWpf.TouchScreenKeyboard.Keyboard.TouchScreenKeyboard.OnClose();
        }
    }
}
