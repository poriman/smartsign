﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DigitalSignage.TouchScreenAgent.TSControls
{
    /// <summary>
    /// Interaction logic for tuiNumberPanel.xaml
    /// </summary>
    public partial class NumberPanel : UserControl
    {
        private int startChar;
        private static Control _CurrentControl = null;
        private string _OldMessage;
        private bool _IsUserEnter;

        public string NumberText 
        {
            get 
            {
                if (_CurrentControl is TextBox)
                    return ((TextBox)_CurrentControl).Text;
                else
                    return "";
            }
            set 
            {
                if (_CurrentControl is TextBox)
                {                    
                    TextBox tb = (TextBox)_CurrentControl;
                    if (_IsUserEnter == false)
                    {
                        _IsUserEnter = true;
                        string strChar = value.Substring(tb.Text.Length, 1);
                        tb.Text = strChar;
                        tb.SelectionStart = strChar.Length;
                        tb.Focus();
                    }
                    else
                    {
                        string strChar = value.Substring(tb.Text.Length, 1);
                        tb.Text = tb.Text.Insert(startChar, strChar);

                        if (string.IsNullOrEmpty(tb.Text) != true)
                        {
                            tb.SelectionStart = startChar + 1;
                            tb.Focus();
                        }
                        else
                        {
                            tb.Text = _OldMessage;
                            _IsUserEnter = false;
                        }
                    }
                    
                }
            }
        }

        public static RoutedUICommand Cmd1 = new RoutedUICommand();
        public static RoutedUICommand Cmd2 = new RoutedUICommand();
        public static RoutedUICommand Cmd3 = new RoutedUICommand();
        public static RoutedUICommand Cmd4 = new RoutedUICommand();
        public static RoutedUICommand Cmd5 = new RoutedUICommand();
        public static RoutedUICommand Cmd6 = new RoutedUICommand();
        public static RoutedUICommand Cmd7 = new RoutedUICommand();
        public static RoutedUICommand Cmd8 = new RoutedUICommand();
        public static RoutedUICommand Cmd9 = new RoutedUICommand();
        public static RoutedUICommand Cmd0 = new RoutedUICommand();
        public static RoutedUICommand CmdBackspace = new RoutedUICommand();
        public static RoutedUICommand CmdLeftMove = new RoutedUICommand();
        public static RoutedUICommand CmdRightMove = new RoutedUICommand();

        public NumberPanel()
        {
            InitializeComponent();

            SetCommandBinding();
        }

        public void SetControl(Control control)
        {
            _CurrentControl = control;

            _OldMessage = ((TextBox)_CurrentControl).Text;
            _IsUserEnter = false;
        }

        private void SetCommandBinding()
        {
            CommandBinding Cb1 = new CommandBinding(Cmd1, RunCommand, CanExecute);
            CommandBinding Cb2 = new CommandBinding(Cmd2, RunCommand, CanExecute);
            CommandBinding Cb3 = new CommandBinding(Cmd3, RunCommand, CanExecute);
            CommandBinding Cb4 = new CommandBinding(Cmd4, RunCommand, CanExecute);
            CommandBinding Cb5 = new CommandBinding(Cmd5, RunCommand, CanExecute);
            CommandBinding Cb6 = new CommandBinding(Cmd6, RunCommand, CanExecute);
            CommandBinding Cb7 = new CommandBinding(Cmd7, RunCommand, CanExecute);
            CommandBinding Cb8 = new CommandBinding(Cmd8, RunCommand, CanExecute);
            CommandBinding Cb9 = new CommandBinding(Cmd9, RunCommand, CanExecute);
            CommandBinding Cb0 = new CommandBinding(Cmd0, RunCommand, CanExecute);
            CommandBinding CbBackspace = new CommandBinding(CmdBackspace, RunCommand, CanExecute);
            CommandBinding CbLeftMove = new CommandBinding(CmdLeftMove, RunCommand, CanExecute);
            CommandBinding CbRightMove = new CommandBinding(CmdRightMove, RunCommand, CanExecute);

            CommandManager.RegisterClassCommandBinding(typeof(NumberPanel), Cb1);
            CommandManager.RegisterClassCommandBinding(typeof(NumberPanel), Cb2);
            CommandManager.RegisterClassCommandBinding(typeof(NumberPanel), Cb3);
            CommandManager.RegisterClassCommandBinding(typeof(NumberPanel), Cb4);
            CommandManager.RegisterClassCommandBinding(typeof(NumberPanel), Cb5);
            CommandManager.RegisterClassCommandBinding(typeof(NumberPanel), Cb6);
            CommandManager.RegisterClassCommandBinding(typeof(NumberPanel), Cb7);
            CommandManager.RegisterClassCommandBinding(typeof(NumberPanel), Cb8);
            CommandManager.RegisterClassCommandBinding(typeof(NumberPanel), Cb9);
            CommandManager.RegisterClassCommandBinding(typeof(NumberPanel), Cb0);
            CommandManager.RegisterClassCommandBinding(typeof(NumberPanel), CbBackspace);
            CommandManager.RegisterClassCommandBinding(typeof(NumberPanel), CbLeftMove);
            CommandManager.RegisterClassCommandBinding(typeof(NumberPanel), CbRightMove);
        }

        private void CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void RunCommand(object sender, ExecutedRoutedEventArgs e)
        {
            startChar = 0;
            if (_CurrentControl is TextBox)
            {
                TextBox tb = _CurrentControl as TextBox;
                startChar = tb.SelectionStart;
            }

            if (e.Command == Cmd1)
            {
                NumberText += "1";
            }
            else if (e.Command == Cmd2)
            {
                NumberText += "2";
            }
            else if (e.Command == Cmd3)
            {
                NumberText += "3";
            }
            else if (e.Command == Cmd4)
            {
                NumberText += "4";
            }
            else if (e.Command == Cmd5)
            {
                NumberText += "5";
            }
            else if (e.Command == Cmd6)
            {
                NumberText += "6";
            }
            else if (e.Command == Cmd7)
            {
                NumberText += "7";
            }
            else if (e.Command == Cmd8)
            {
                NumberText += "8";
            }
            else if (e.Command == Cmd9)
            {
                NumberText += "9";
            }
            else if (e.Command == Cmd0)
            {
                NumberText += "0";
            }
            else if (e.Command == CmdBackspace)
            {
                if (!string.IsNullOrEmpty(this.NumberText))
                {
                    if (_CurrentControl is TextBox)
                    {
                        TextBox tb = _CurrentControl as TextBox;

                        int len = tb.SelectionLength;
                        startChar = tb.SelectionStart; //character index.

                        if (len == 0)
                        {
                            if (startChar == 0)
                                return;
                            tb.Text = tb.Text.Remove(startChar - 1, 1);
                            tb.SelectionStart = startChar - 1;
                        }
                        else if (len == 1)
                        {
                            tb.Text = tb.Text.Remove(startChar, 1);
                            tb.SelectionStart = startChar;
                        }
                        else
                        {
                            tb.Text = tb.Text.Remove(startChar, len);
                            tb.SelectionStart = startChar;
                        }

                        if (string.IsNullOrEmpty(tb.Text) == true)
                        {
                            tb.Text = _OldMessage;
                            _IsUserEnter = false;
                        }

                        tb.Focus();
                    }
                }               
            }
            else if (e.Command == CmdLeftMove)
            {
                if (!string.IsNullOrEmpty(this.NumberText))
                {
                    if (_CurrentControl is TextBox)
                    {
                        TextBox tb = _CurrentControl as TextBox;
                        if (startChar > 0)
                        {
                            tb.SelectionStart = startChar - 1;
                            startChar = tb.SelectionStart;
                            
                        }
                        tb.Focus();
                    }
                }                
            }
            else if (e.Command == CmdRightMove)
            {
                if (!string.IsNullOrEmpty(this.NumberText))
                {
                    if (_CurrentControl is TextBox)
                    {
                        TextBox tb = _CurrentControl as TextBox;
                        if (startChar <= tb.Text.Length)
                        {
                            tb.SelectionStart = startChar + 1;
                            startChar = tb.SelectionStart;
                        }
                        tb.Focus();
                    }
                }
            }
        }

        
    }
}
