﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigitalSignage.TouchScreenAgent
{
    public delegate void TouchflowTimeOutDelegate(TimeOutType timeOutType);

    /// <summary>
    /// cjy_20090624: Timeout 
    /// </summary>
    public class TSTimeOutTimer 
    {
        public TimeOutType TimeOutType { set; get; }
        public TouchflowTimeOutDelegate TouchflowTimeOutExecuteHander;

        private static readonly TSTimeOutTimer _timerInstance ;
        System.Windows.Threading.DispatcherTimer timer;

        static TSTimeOutTimer()
        {            
            _timerInstance = new TSTimeOutTimer();   
        }

        private TSTimeOutTimer()
        {            
        }

        public static TSTimeOutTimer TimerInstance
        {
            get { return _timerInstance; }
        }

        public void CreateDispatcherTimer()
        {
            if (timer == null)
            {
                timer = new System.Windows.Threading.DispatcherTimer();
                timer.Tick += new EventHandler(timer_Tick);
            }
        }

        public double IntervalSecond
        {
            set
            {
                if (timer == null)
                    return;
                timer.Interval = TimeSpan.FromSeconds(value);
            }
        }
        

        //public TSTimeOutTimer(TimeOutType type)
        //{
        //    SetTimeOutType(type);
        //    //timer = new System.Windows.Threading.DispatcherTimer();
        //    //timer.Tick += new EventHandler(timer_Tick);
        //}

        private void SetTimeOutType(TimeOutType type)
        {
            TimeOutType = type;
        }

        public void TimeoutStart()
        {
            if (timer != null)
                timer.Start();
        }

        public void TimeoutStop()
        {
            if (timer != null)
                timer.Stop();
        }

        public void Restart()
        {
            if (timer != null && timer.IsEnabled==true)
            {
                TimeoutStop();               
            }
            TimeoutStart();
            
        }

        public void TimeoutKill()
        {
            if (timer != null)
            {
                TimeoutStop();
                timer.Tick -= timer_Tick;
                timer = null;
                TouchflowTimeOutExecuteHander = null;
            }
        }

        void timer_Tick(object sender, EventArgs e)
        {
            if (timer != null)
            {
                //Console.WriteLine(this.timer.Dispatcher.Thread);
                timer.Stop();
                Execute();
            }
        }

        private void Execute()
        {
            if (TouchflowTimeOutExecuteHander.Target != null)
                TouchflowTimeOutExecuteHander(TimeOutType);
        }
    }

    public enum TimeOutType
    {
        None,
        ScreenHide,
        ProcessEnd,
        ProcessRestart
    }
}
