﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Xml.Linq;
using DigitalSignage.TouchScreenAgent.Model;
using NLog;

namespace DigitalSignage.TouchScreenAgent
{
    public delegate void MoveScreePageDelegate(Guid targetID, object ViewObj);
    public delegate void GotFocusTextBoxDelegate();

    /// <summary>
    /// Interaction logic for MainTSWindow.xaml
    /// </summary>
    public partial class MainTSWindow : Window
    {
        #region private 멤버 변수
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private string TSWorkflowFilePath;
        #endregion

        #region Delegate 선언
        public delegate void SendTargetTSScreenIDDelegate(Guid guid, object ViewObj, bool IsNEScreenRepeat);
        public SendTargetTSScreenIDDelegate SendTargetTSScreenID;
        #endregion

        public int ScreenWidth { get; set; }
        public int ScreenHeight { get; set; }
        public bool IsBgTransparent { get; set; }
        public bool IsBorderBurshTransparent { get; set; }
        public string BorderThicknessSize { get; set; }

        Dictionary<Guid, TSScreen> ScreenDictionary = new Dictionary<Guid, TSScreen>();
        Dictionary<Guid, TSScreen> PlayingScreenDictionary = new Dictionary<Guid, TSScreen>();
        Dictionary<Guid, Decision> DecisionDictionary = new Dictionary<Guid, Decision>();
        TSScreen StartProcess = new TSScreen();
        TSScreen EndProcess = new TSScreen();

        public MainTSWindow() : this(new Size(0, 0))
        {  
        }

        public MainTSWindow(Size size)
        {
            InitializeComponent();

            InitTSWidnow(size);
        }

        public void InitTSWidnow(Size size)
        {
            if (size.IsEmpty == false)
            {
                this.Width = size.Width;
                this.Height = size.Height;
            }

            //윈도우 배경을 투명하게 설정 - 뒤에 있는 SmartSign Screen이 보이도록하기 위해...
            SolidColorBrush solid = new SolidColorBrush();
            solid.Opacity = 0.002;
            solid.Color = Color.FromRgb(16, 0, 16);

            this.Background = solid;

            //this.Style = (Style)FindResource("TSTransparentBackground");
        }

        public void Show(UIElement parent)
        {
            //if (parent is Window)
            //    this.Owner = parent as Window; //이 설정을 해제시 윈도우 Taskbar가 나오는 경우 발생.

            this.Show();
        }

        public void CloseTSView()
        {
            this.Close();
        }

        /// <summary>
        /// tsmap 파일을 xml형식으로 load하여 터치스크린 정보를 셋팅한다.
        /// </summary>
        /// <param name="tsWorkflowFilePath"></param>
        /// <returns></returns>
        public bool CreateTSWorkflowProcess(string tsWorkflowFilePath)
        {
            try
            {
                if (string.IsNullOrEmpty(tsWorkflowFilePath) == true)
                    return false;

                TSWorkflowFilePath = tsWorkflowFilePath;
                XElement tsWorkflowXml = XElement.Load(TSWorkflowFilePath);

                return CreateTSWorkflowProcess(tsWorkflowXml.Element("Touchflow"));
            }
            catch(Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// 페이지 정보 셋팅.
        /// </summary>
        /// <param name="touchflowElement"></param>
        /// <returns></returns>
        public bool CreateTSWorkflowProcess(XElement touchflowElement)
        {
            try
            {               
                XElement tsWorkflowXml = touchflowElement;

                XElement xmlStart = tsWorkflowXml.Element("Workflows").Element("Workflow").Element("Shapes").Element("Start");
                this.StartProcess.ID = new Guid(xmlStart.Element("ItemID").Value);

                XElement xmlEnd = tsWorkflowXml.Element("Workflows").Element("Workflow").Element("Shapes").Element("End");
                this.EndProcess.ID = new Guid(xmlEnd.Element("ItemID").Value);

                ScreenWidth = Convert.ToInt32(tsWorkflowXml.Element("Workflows").Element("Workflow").Element("ScreenWidth").Value);
                ScreenHeight = Convert.ToInt32(tsWorkflowXml.Element("Workflows").Element("Workflow").Element("ScreenHeight").Value);
                IsBgTransparent = Convert.ToBoolean(tsWorkflowXml.Element("Workflows").Element("Workflow").Element("BackgroundTransparent").Value);
                IsBorderBurshTransparent = Convert.ToBoolean(tsWorkflowXml.Element("Workflows").Element("Workflow").Element("BorderBrushTransparent").Value);
                BorderThicknessSize = tsWorkflowXml.Element("Workflows").Element("Workflow").Element("BorderSize").Value;

                AddTouchScreenItems(tsWorkflowXml.Element("Workflows").Element("Workflow").Element("Shapes").Elements("Screen"));
              
                IEnumerable<XElement> xmlDecisionList = tsWorkflowXml.Element("Workflows").Element("Workflow").Element("Shapes").Elements("Decision");

                foreach (XElement xmlDecision in xmlDecisionList)
                {
                    Guid guid = new Guid(xmlDecision.Element("ItemID").Value);
                    string name = xmlDecision.Element("ItemName").Value;

                    XElement xmlDecisionInfo = xmlDecision.Element("DecisionInfo");

                    Decision decision = new Decision();
                    decision.SourceScreenID = new Guid(xmlDecisionInfo.Element("SourceScreenID").Value);
                    decision.SourceScreenControlID = new Guid(xmlDecisionInfo.Element("SourceScreenControlID").Value);
                    decision.SourceScreenControlValueType = xmlDecisionInfo.Element("SourceScreenControlValueType").Value;

                    foreach (XElement xml in xmlDecision.Element("DecisionInfo").Elements("TargetScreenInfo"))
                    {
                        TargetScreenInfo tsinfo = new TargetScreenInfo();
                        tsinfo.TargetScreenID = new Guid(xml.Element("TargetID").Value);
                        tsinfo.Value1 = xml.Element("Value1").Value;
                        tsinfo.Value1Condition = xml.Element("Value1Condition").Value;
                        tsinfo.Value2 = xml.Element("Value2").Value;
                        tsinfo.Value2Condition = xml.Element("Value2Condition").Value;
                        tsinfo.ControlValueType = xml.Element("ControlValueType").Value;
                        tsinfo.ConditionType = xml.Element("ConditionType").Value;

                        decision.DecisionTargetScreenInfos.Add(tsinfo);
                    }

                    DecisionDictionary.Add(guid, decision);
                }
                
                return true;
            }
            catch 
            {
                logger.Error("don't Create Touch Screen Workflow Process");
                return false;
            }          
        }

        private void AddTouchScreenItems(IEnumerable<XElement> ScreenElements)
        {
            foreach (XElement element in ScreenElements)
            {
                TSScreen ScreenView = CreateTSScreen(element, true);
                if (ScreenView != null)
                {
                    if (ScreenDictionary.ContainsKey(ScreenView.ID) == false)
                        ScreenDictionary.Add(ScreenView.ID, ScreenView);
                }
            }            
        }

        private TSScreen CreateTSScreen(XElement ShapeElement, bool IsTemplate)
        {
            try
            {
                Guid TSViewGuid = Guid.Empty;
                if (IsTemplate)
                    TSViewGuid = new Guid(ShapeElement.Element("ItemID").Value);
                else
                    TSViewGuid = Guid.NewGuid();

                string TSViewName = ShapeElement.Element("ItemName").Value;
                IEnumerable<XElement> xmlScreenDesignUIContrls = ShapeElement.Element("ScreenUIDesign").Element("UIPages").Element("UIPage").Elements("UIObjects");

                TSScreen screenPage = new TSScreen(TSViewGuid, Guid.Empty, TSViewName);
                screenPage.Tag = ShapeElement;
                screenPage.StartID = this.StartProcess.ID;
                screenPage.EndID = this.EndProcess.ID;

                screenPage.Width = ScreenWidth;
                screenPage.Height = ScreenHeight;
                screenPage.IsBgTransparent = this.IsBgTransparent;
                screenPage.IsBorderBurshTransparent = this.IsBorderBurshTransparent;
                screenPage.BorderThicknessSize = this.BorderThicknessSize;

                screenPage.ScreenDisplayType = ShapeElement.Element("TouchControlInfo").Element("TouchExcuteType").Value;//cjy_20090623: Screen 디스플레이 타입 설정
                screenPage.TimeoutType = ShapeElement.Element("TouchControlInfo").Element("TimeOutType").Value;//cjy_20090624: Timeout 타입 설정
                screenPage.TimeoutValue = ShapeElement.Element("TouchControlInfo").Element("Time").Value;//cjy_20090624 : Timeout 값 설정

                screenPage.GetScreenDesignUIControl(xmlScreenDesignUIContrls);

                screenPage.DisplayControl();

                screenPage.MoveScreePage = new MoveScreePageDelegate(MoveScreePageFunc);
                screenPage.GotFocusTextBox = new GotFocusTextBoxDelegate(GotFocusTextBoxFunc);

                return screenPage;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// 페이지 ID를 이용하여 페이지 Canvas를 생성.
        /// </summary>
        /// <param name="touchScreenID"></param>
        /// <returns></returns>
        public Canvas DrawTSView(string touchScreenID)
        {
            
            if (string.IsNullOrEmpty(touchScreenID) == true)
                return null;

            //Console.WriteLine(touchScreenID);
            TSScreen tsScreen = null;
            //Pane Screen 컨텐츠와 맵핑하는 터치 스크린이 템플릿 저장소에 존재한다면...
            if (ScreenDictionary.TryGetValue(new Guid(touchScreenID), out tsScreen) == true)
            {                
                if (tsScreen != null)
                {
                    TSScreen screenTemp = null;
                    
                    //if (PlayingScreenDictionary.Count > 10)
                    {
                        //Console.WriteLine("2");
                        CheckPlayingTouchScreen();//부모가 없는 Touch Screen은 PlayingScreenDictionary 에서 제거.
                        //Console.WriteLine("3");
                    }
                    
                    screenTemp = this.CreateTSScreen((XElement)tsScreen.Tag, false);                    

                    PlayingScreenDictionary.Add(screenTemp.ID, screenTemp);
                    //cjy_20090623: 스크린의 Display 설정
                    screenTemp.SetTSScreenDisplay();
                    screenTemp.StartTouchScreenTimeout();
                    
                    return screenTemp;
                }
            }           

            return null;
        }
        
        /// <summary>
        /// 부모가 없는 Touch Screen 을 PlayingScreenDictionary 에서 제거한다.
        /// </summary>
        private void CheckPlayingTouchScreen()
        {            
            try
            {
                List<Guid> removeList = new List<Guid>();
                foreach (KeyValuePair<Guid, TSScreen> keyValue in PlayingScreenDictionary)
                {
                    TSScreen ts = keyValue.Value;
                    if (ts.Parent != null)
                        continue;

                    removeList.Add(keyValue.Key);
                    ts = null;
                }

                foreach (Guid id in removeList)
                {
                    PlayingScreenDictionary.Remove(id);
                }
            }
            catch (Exception ex)
            {
                //Console.WriteLine(ex.Message);
            }
            
            //IList<TSScreen> list = PlayingScreenDictionary.Values.ToList();
            //foreach (TSScreen ts in list)
            //{
            //    if (ts.Parent == null)
            //        PlayingScreenDictionary.Remove(ts.ID);
            //}
        }

        private void MoveScreePageFunc(Guid id, object ViewObj)
        {
            //Console.WriteLine("MoveScreePageFunc 시작");
            //HideWindow();//Test

            Guid targetScreenID = id;
                    
            if (EndProcess.ID.Equals(id) == true)//끝일 경우 종료
            {
                targetScreenID = this.EndProcess.ID;
                //처음 터치스크린으로 이동한다.
                KeyValuePair<Guid, TSScreen> keyValue = ScreenDictionary.FirstOrDefault();
                if (keyValue.Key != null)
                {
                    targetScreenID = keyValue.Key;
                }
            }
            else if (StartProcess.ID.Equals(id) == true)//시작일 경우 처음으로 이동 
            {
                targetScreenID = this.StartProcess.ID;
                //처음 터치스크린으로 이동한다.
                KeyValuePair<Guid, TSScreen> keyValue = ScreenDictionary.FirstOrDefault();
                if (keyValue.Key != null)
                {
                    targetScreenID = keyValue.Key;
                }
            }
            //다음 이동 Target이 Decision이다.
            else if (DecisionDictionary.ContainsKey(id) == true)
            {
                Decision decision = null;
                DecisionDictionary.TryGetValue(id, out decision);

                //Decision의 "소스 Screen"의 ID가 있다.
                if (ScreenDictionary.ContainsKey(decision.SourceScreenID) == true)
                {
                    TSScreen sourceScreenPage = null;
                    ScreenDictionary.TryGetValue(decision.SourceScreenID, out sourceScreenPage);

                    //Screen Page에 분기 컨트롤을 찾는다.
                    foreach (Control child in sourceScreenPage.Children)
                    {
                        Guid guid = (Guid)child.Tag;
                        if (guid.Equals(decision.SourceScreenControlID) == true)
                        {
                            TextBox textBox = child as TextBox;
                            if (textBox != null)
                            {
                                string strValue = textBox.Text;

                                if (decision.SourceScreenControlValueType.Equals("NumberType") == true)
                                {

                                    int numValue = Convert.ToInt32(strValue);
                                    foreach (TargetScreenInfo tsinfo in decision.DecisionTargetScreenInfos)
                                    {
                                        if (tsinfo.ConditionType.Equals("SingleConditionMode") == true)
                                        {
                                            //해당하는 값이 있다면 foreach 문을 나간다.
                                            if (CompareValue(numValue, tsinfo.Value1Condition, Convert.ToInt32(tsinfo.Value1)) == true)
                                            {
                                                targetScreenID = tsinfo.TargetScreenID;
                                                break;
                                            }
                                        }
                                        else if (tsinfo.ConditionType.Equals("IncludeConditionMode") == true)
                                        {
                                            if (CompareValue(numValue, tsinfo.Value1Condition, Convert.ToInt32(tsinfo.Value1)) == true)
                                            {
                                                if (CompareValue(numValue, tsinfo.Value2Condition, Convert.ToInt32(tsinfo.Value2)) == true)
                                                {
                                                    targetScreenID = tsinfo.TargetScreenID;
                                                    break;
                                                }
                                            }
                                        }
                                        else if (tsinfo.ConditionType.Equals("ExclusionConditionMode") == true)
                                        {
                                            if (CompareValue(numValue, tsinfo.Value1Condition, Convert.ToInt32(tsinfo.Value1)) == true)
                                            {
                                                targetScreenID = tsinfo.TargetScreenID;
                                                break;
                                            }

                                            if (CompareValue(numValue, tsinfo.Value2Condition, Convert.ToInt32(tsinfo.Value2)) == true)
                                            {
                                                targetScreenID = tsinfo.TargetScreenID;
                                                break;
                                            }
                                        }
                                    }
                                }
                                else if (decision.SourceScreenControlValueType.Equals("StringType") == true)
                                {
                                    foreach (TargetScreenInfo tsinfo in decision.DecisionTargetScreenInfos)
                                    {
                                        if (tsinfo.Value1Condition.Equals("NotSame") == true)
                                        {
                                            if (tsinfo.Value1.Equals(strValue) == false)
                                            {
                                                targetScreenID = tsinfo.TargetScreenID;
                                                break;
                                            }
                                        }
                                        else if (tsinfo.Value1Condition.Equals("Same") == true)
                                        {
                                            if (tsinfo.Value1.Equals(strValue) == true)
                                            {
                                                targetScreenID = tsinfo.TargetScreenID;
                                                break;
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("Decision의 값 타입이 설정되지 않음???.");
                                    return;
                                }
                            }
                            break;
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Decision의 \"소스 Screen\"의 ID가 존재하지 않는다.");
                    return;
                }
            }

            TSScreen screenPage = null;

            ScreenDictionary.TryGetValue(targetScreenID, out screenPage);

            if (screenPage == null)
                return;
            
            //NEPaler으로 전달
            if (SendTargetTSScreenID != null)
            {
                bool isScreenRepeat = false;
                if (screenPage.TimeoutType.Equals("ProcessEnd") == true || screenPage.TimeoutType.Equals("ProcessRestart") == true)
                    isScreenRepeat = true;

                SendTargetTSScreenID(targetScreenID, ViewObj, isScreenRepeat);               
            }
            //Console.WriteLine("MoveScreePageFunc 끝");
            //this.ShowWindow();
        }

        private bool CompareValue(int targetValue, string condition, int compareValue)
        {
            bool bResult = false;

            switch (condition)
            {
                case "AndOver":
                    if (targetValue >= compareValue)
                        bResult = true;
                    break;
                case "AndUnder":
                    if (targetValue <= compareValue)
                        bResult = true;
                    break;
                case "LessThan":
                    if (targetValue < compareValue)
                        bResult = true;
                    break;
                case "MoreThan":
                    if (targetValue > compareValue)
                        bResult = true;
                    break;
                case "Same":
                    if (targetValue == compareValue)
                        bResult = true;
                    break;
            }

            return bResult;
        }

        private void GotFocusTextBoxFunc()
        {
        }


        public void HideWindow()
        {
            this.Topmost = false;
            this.Visibility = Visibility.Hidden;
        }

        public void ShowWindow()
        {
            this.Topmost = true;
            this.Visibility = Visibility.Visible;
        }

        private void Window_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            
        }
    }
}
