﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml.Linq;
using DigitalSignage.TouchScreenAgent.TSControls;
using System.Windows.Markup;
using System.IO;
using System.Xml;
using System.Windows.Controls.Primitives;

namespace DigitalSignage.TouchScreenAgent
{
    public class TouchUIControl
    {
        public Type type { get; set; }
        public Guid ID { get; set; }
        public string Name { get; set; }
        public double PosX { get; set; }
        public double PosY { get; set; }
        public double Width { get; set; }
        public double Height { get; set; }
        public int ZIndex { get; set; }
        public bool IsBgTransparent { get; set; }
        public bool IsBorderBurshTransparent { get; set; }
        public string BorderThicknessSize { get; set; }
        public CloseScreen CloseScreen { get; set; }
        public MoveScreen MoveScreen { get; set; }
        public InputNumber InputNumber { get; set; }
        public double FontSize { get; set; }
        public string FontFamily { get; set; }
        public string FontStyle { get; set; }
        public string Foreground { get; set; }
        public string FontWeight { get; set; }
        public string TextWrapping { get; set; }
        public string TextAlignment { get; set; }
        public bool IsSupprotKeyboard { get; set; }
        public double Opacity { get; set; }
        private Style controlStyle;
        public Style ControlStyle 
        {
            get { return controlStyle; }
            set { controlStyle = value; }
        }

        public TouchUIControl()
        {
            this.CloseScreen = new CloseScreen();
            this.MoveScreen = new MoveScreen();
            this.InputNumber = new InputNumber();
        }
    }

    public class MoveScreen
    {
        public string MoveScreenType;
        public Guid MoveScreenTargetID;
    }

    public class CloseScreen
    {
        public string CloseScreenType;
        public Guid CloseScreenTargetID;
    }

    public class InputNumber
    {
        public string InputNumberType;
        public Guid InputNumberTargetID;
    }

    /// <summary>
    /// Interaction logic for TSScreen.xaml
    /// </summary>
    public partial class TSScreen : Canvas, IDisposable
    {
        public Guid TargetID;
        public Guid ID;
        public string Name;
        public bool IsBgTransparent { get; set; }
        public bool IsBorderBurshTransparent { get; set; }
        public string BorderThicknessSize { get; set; }
        public Guid EndID { get; set; }//cjy_20090624
        public Guid StartID { get; set; }//cjy_20090624

        public List<TouchUIControl> PageControls;
        public MoveScreePageDelegate MoveScreePage;
        public GotFocusTextBoxDelegate GotFocusTextBox;

        private IList<Popup> controlWrapPopupList = new List<Popup>();

        TSTimeOutTimer touchScreenTimeout;//cjy_20090624: Timeout 

        #region 페이지 디스플레이 타입
        //<< cjy_20090623
        private TouchExcuteType _screenDisplayType;
        public string ScreenDisplayType
        {
            get { return _screenDisplayType.ToString(); }
            set
            {
                if (string.IsNullOrEmpty(value) != true)
                {
                    switch (value)
                    {
                        case "None":
                            this._screenDisplayType = TouchExcuteType.None;
                            break;
                        case "DisplayAfterTouch":
                            this._screenDisplayType = TouchExcuteType.DisplayAfterTouch;
                            break;
                        default:
                            this._screenDisplayType = TouchExcuteType.None;
                            break;
                    }
                }
            }
        }
        //>>
        #endregion

        #region 터치 페이지 타임아웃 관련 값 설정
        public double _timeoutValue;
        public string TimeoutValue
        {
            get { return int.Parse(_timeoutValue.ToString()).ToString(); }
            set
            {
                double tempValue;
                if (double.TryParse(value, out tempValue) != true)
                    return;
                _timeoutValue = tempValue;
            }
        }

        private TimeOutType _timeoutType;
        public string TimeoutType
        {
            get { return _timeoutType.ToString(); }
            set
            {
                if (string.IsNullOrEmpty(value) != true)
                {
                    switch (value)
                    {
                        case "None":
                            this._timeoutType = TimeOutType.None;
                            break;
                        case "ProcessEnd":
                            this._timeoutType = TimeOutType.ProcessEnd;
                            break;
                        case "ProcessRestart":
                            this._timeoutType = TimeOutType.ProcessRestart;
                            break;
                        case "ScreenHide":
                            this._timeoutType = TimeOutType.ScreenHide;
                            break;
                        default:
                            this._timeoutType = TimeOutType.None;
                            break;
                    }
                }
            }
        }
        //<< cjy_20090624
        //>>
        #endregion

        public TSScreen()
        {
            InitializeComponent();
           
            PageControls = new List<TouchUIControl>();
            //this.Background = Brushes.Transparent;
            //this.Opacity = 0.002;
        }

        public void Dispose()
        {
            if (touchScreenTimeout != null)
            {
                touchScreenTimeout = null;
            }
        }

        ~TSScreen()
        {
            Dispose();
        }

        public TSScreen(Guid id, Guid targetID, string name)
            : this()
        {
            TargetID = targetID;
            ID = id;
            Name = name;
        }

        private Popup InsertInPopup(TouchUIControl control)
        {
            Popup popup = new Popup();
            popup.PlacementTarget = this;
            popup.Placement = PlacementMode.RelativePoint;
            popup.VerticalOffset = control.PosY;
            popup.HorizontalOffset = control.PosX;
            popup.Width = control.Width;
            popup.Height = control.Height;
            //popup.Child = GetButton(control);
            popup.IsOpen = false;

            return popup;
        }


        public void DisplayControl()
        {
            foreach (TouchUIControl control in PageControls)
            {
                Popup popup = InsertInPopup(control);
                popup.AllowsTransparency = true;
                popup.Opacity = 0.02;
                switch (control.type.Name)
                    {
                    case "TSButton":
                        popup.Child = GetButton(control);
                        this.Children.Add(popup);
                        controlWrapPopupList.Add(popup);
                        break;
                    case "KeyboardTextBox":
                        popup.Child = GetTextBox(control);
                        this.Children.Add(popup);
                        controlWrapPopupList.Add(popup);
                        //this.Children.Add(GetTextBox(control));
                        break;
                    case "NumberTextBox":
                        popup.Child = GetTextBox(control);
                        this.Children.Add(popup);
                        controlWrapPopupList.Add(popup);
                        //this.Children.Add(GetTextBox(control));
                        break;
                    case "Rectangle":
                        this.Children.Add(GetRectangle(control));
                        break;
                    case "NumberPanel":
                        popup.Child = GetNumberPanel(control);
                        //popup.AllowsTransparency = true;
                        //popup.Opacity = 0.002;
                        this.Children.Add(popup);
                        controlWrapPopupList.Add(popup);
                        //this.Children.Add(GetNumberPanel(control));
                        break;
                }
            }

            var NumberPanelControlList = PageControls.Where(p => p.type == typeof(NumberPanel));

            foreach (TouchUIControl control in NumberPanelControlList)
            {
                NumberPanel numberPanel = (NumberPanel)GetScreenControl(control.ID, control.type.Name);
                if (numberPanel != null)
                {
                    Control controlTemp = GetScreenControl(control.InputNumber.InputNumberTargetID, typeof(NumberTextBox).Name);
                    numberPanel.SetControl(controlTemp);
                }               
            }
        }

        private TouchUIControl GetTouchUIControl(Guid controlID)
        {
            var control = PageControls.Where(p => p.ID.Equals(controlID)==true).SingleOrDefault();
            if (control != null)
            {
                return control;
            }
            return null;
        }

        private Control GetScreenControl(Guid controlID, string typeName)
        {
            foreach (Popup popup in this.controlWrapPopupList)
            {
                string typeNameTemp = popup.Child.GetType().Name;
                if (typeNameTemp.Equals(typeName) == true)
                {
                    Control control = popup.Child as Control;
                    if (control != null)
                    {
                        Guid id = (Guid)control.Tag;
                        if (id.Equals(controlID) == true)
                            return control;
                    }
                }
            }
            return null;
        }

        public void GetScreenDesignUIControl(IEnumerable<XElement> xmlScreenDesignUIContrls)
        {
            IEnumerable<XElement> xmlButtonList = xmlScreenDesignUIContrls.Elements("Button");
            foreach (XElement xml in xmlButtonList)
            {
                PageControls.Add(GetControl(xml, typeof(TSButton)));
            }

            IEnumerable<XElement> xmlTextBoxList = xmlScreenDesignUIContrls.Elements("Text");
            foreach (XElement xml in xmlTextBoxList)
            {
                bool IsSupportKeyboard = true;
                if (xml.Element("SupportKeyboard") != null)
                {
                    IsSupportKeyboard = Convert.ToBoolean(xml.Element("SupportKeyboard").Value);
                }

                if (IsSupportKeyboard == true)
                    PageControls.Add(GetControl(xml, typeof(KeyboardTextBox)));
                else
                    PageControls.Add(GetControl(xml, typeof(NumberTextBox)));
            }


            IEnumerable<XElement> xmlRectAreaList = xmlScreenDesignUIContrls.Elements("RectArea");
            foreach (XElement xml in xmlRectAreaList)
            {
                PageControls.Add(GetControl(xml, typeof(Rectangle)));
            }

            IEnumerable<XElement> xmlNumberPanelList = xmlScreenDesignUIContrls.Elements("NumberPanel");
            foreach (XElement xml in xmlNumberPanelList)
            {
                PageControls.Add(GetControl(xml, typeof(NumberPanel)));
            }

            return;
        }

        private bool CheckElement(XElement xElement, string elementName, ref string value)
        {
            XElement tempElement = xElement.Element(elementName);

            if (tempElement != null)
            {
                value = tempElement.Value;
                return true;
            }           
            else
            {
                value = "";
                return false;
            }            
        }

        public TouchUIControl GetControl(XElement xmlButton, Type type)
        {
            TouchUIControl control = new TouchUIControl();
            control.type = type;
            control.ID = new Guid(xmlButton.Element("ID").Value);
            control.Name = xmlButton.Element("Name").Value;
            control.PosX = Convert.ToDouble(xmlButton.Element("PosX").Value);
            control.PosY = Convert.ToDouble(xmlButton.Element("PosY").Value);
            control.Width = Convert.ToDouble(xmlButton.Element("Width").Value);
            control.Height = Convert.ToDouble(xmlButton.Element("Height").Value);
            control.ZIndex = Convert.ToInt32(xmlButton.Element("ZIndex").Value);

            string value = null;
            if (CheckElement(xmlButton, "FontSize", ref value) == true)
                control.FontSize = Convert.ToDouble(value);
            if (CheckElement(xmlButton, "FontFamily", ref value) == true)
                control.FontFamily = value;
            if (CheckElement(xmlButton, "FontStyle", ref value) == true)
                control.FontStyle = value;
            if (CheckElement(xmlButton, "Foreground", ref value) == true)
                control.Foreground = value;
            if (CheckElement(xmlButton, "FontWeight", ref value) == true)
                control.FontWeight = value;
            if (CheckElement(xmlButton, "TextWrapping", ref value) == true)
                control.TextWrapping = value;
            if (CheckElement(xmlButton, "TextAlignment", ref value) == true)
                control.TextAlignment = value;
            if (CheckElement(xmlButton, "Opacity", ref value) == true)
                control.Opacity = Convert.ToDouble(value);

            control.IsBgTransparent = this.IsBgTransparent;// Convert.ToBoolean(xmlButton.Element("BackgroundTransparent").Value);
            control.IsBorderBurshTransparent = this.IsBorderBurshTransparent;// Convert.ToBoolean(xmlButton.Element("BorderBrushTransparent").Value);
            control.BorderThicknessSize = this.BorderThicknessSize;// xmlButton.Element("BorderSize").Value;

            //버튼 스타일 얻어오기
            var styleXml = xmlButton.Element("ButtonStyle");
            if (styleXml != null)
            {
                string xaml = styleXml.Value;

                if (string.IsNullOrEmpty(xaml) == false)
                {
                    ResourceDictionary resDict = (ResourceDictionary)XamlReader.Parse(xaml);//ResourceDictionary로 포장되어 있어야 한다.
                    foreach (object objResource in resDict.Values)
                    {
                        Style style = objResource as Style;
                        if (style != null)
                        {
                            control.ControlStyle = style;
                            break;
                        }
                    }
                }
            }

            IEnumerable<XElement> xmlEvnets = xmlButton.Elements("Events");

            foreach (XElement xmlEvent in xmlEvnets.Elements())
            {
                string eventName = xmlEvent.Name.ToString();
                IEnumerable<XElement> xmlEvnetActions = xmlEvent.Elements("EventAction");
                switch (eventName)
                {
                    case "Click":                        
                        foreach (XElement xmlEventAction in xmlEvnetActions)
                        {
                            XAttribute xAttrActionType = xmlEventAction.Attribute("EventActionType");
                            if (xAttrActionType.Value == "Move")
                            {
                                control.MoveScreen.MoveScreenType = xAttrActionType.Value;
                                XAttribute xAttrTargetScreenID = xmlEventAction.Attribute("TargetID");
                                control.MoveScreen.MoveScreenTargetID = new Guid(xAttrTargetScreenID.Value);
                            }
                            else if (xAttrActionType.Value == "Close")
                            {
                                control.CloseScreen.CloseScreenType = xAttrActionType.Value;
                                XAttribute xAttrTargetScreenID = xmlEventAction.Attribute("TargetID");
                                control.CloseScreen.CloseScreenTargetID = new Guid(xAttrTargetScreenID.Value);
                            }                           
                            else if (xAttrActionType.Value == "Display")
                            {
                            }
                        }
                        break;
                    case "Input":
                        foreach (XElement xmlEventAction in xmlEvnetActions)
                        {
                            XAttribute xAttrActionType = xmlEventAction.Attribute("EventActionType");
                            if (xAttrActionType.Value == "NumberInput")
                            {
                                control.InputNumber.InputNumberType = xAttrActionType.Value;
                                XAttribute xAttrTargetScreenID = xmlEventAction.Attribute("TargetID");
                                control.InputNumber.InputNumberTargetID = new Guid(xAttrTargetScreenID.Value);
                            }                            
                        }
                        break;
                }
            }

            return control;
        }
      
        private Button GetButton(TouchUIControl control)
        {
            TSButton btn = new TSButton();
            btn.Content = control.Name;//구분을 위해 임시로
            btn.Tag = control.ID;
            btn.Width = control.Width;
            btn.Height = control.Height;

            Canvas.SetLeft(btn, control.PosX);
            Canvas.SetTop(btn, control.PosY);
            Canvas.SetZIndex(btn, control.ZIndex);            

            if (control.IsBgTransparent == true)
                btn.Background = Brushes.Transparent;
            if (control.IsBorderBurshTransparent == true)
                btn.BorderBrush = Brushes.Transparent;
            if (string.IsNullOrEmpty(control.BorderThicknessSize) == false)
                btn.BorderThickness = new Thickness(Convert.ToDouble(control.BorderThicknessSize));

            btn.Style = SetControlStyle(control.ControlStyle, GetResourceStyle("ButtonStyle"));
            
            if (control.FontSize > 0.0)
                btn.FontSize = control.FontSize;

            if (string.IsNullOrEmpty(control.FontFamily) != true)
                btn.FontFamily = (FontFamily)(new FontFamilyConverter().ConvertFromString(control.FontFamily));
            if (string.IsNullOrEmpty(control.FontStyle) != true)
                btn.FontStyle = (FontStyle)(new FontStyleConverter().ConvertFromString(control.FontStyle));
            if (string.IsNullOrEmpty(control.FontWeight) != true)
                btn.FontWeight = (FontWeight)(new FontWeightConverter().ConvertFromString(control.FontWeight));
            if (string.IsNullOrEmpty(control.Foreground) != true)
                btn.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFromString(control.Foreground));

            btn.Click += new RoutedEventHandler(btn_Click);

            return btn;

        }

        /// <summary>
        /// 스타일 적용.
        /// </summary>
        /// <param name="controlStyle"></param>
        /// <param name="defaultStyle"></param>
        /// <returns></returns>
        private Style SetControlStyle(Style controlStyle, Style defaultStyle)
        {
            if (controlStyle != null)
                return controlStyle;
            else
                return defaultStyle;
        }

        private TextBox GetTextBox(TouchUIControl control)
        {
            TextBox text = new TextBox();

            if (control.IsSupprotKeyboard == true)
                text = new KeyboardTextBox();
            else
                text = new NumberTextBox();

            text.Text = control.Name;//구분을 위해 임시로
            text.Tag = control.ID;
            text.Opacity = control.Opacity;
            text.Width = control.Width;
            text.Height = control.Height;   
            Canvas.SetLeft(text, control.PosX);
            Canvas.SetTop(text, control.PosY);
            Canvas.SetZIndex(text, control.ZIndex);
            text.Style = this.GetResourceStyle("WaterMarkTextBoxStyle");

            if (control.FontSize > 0.0)
                text.FontSize = control.FontSize;

            if (string.IsNullOrEmpty(control.FontFamily) != true)
                text.FontFamily = (FontFamily)(new FontFamilyConverter().ConvertFromString(control.FontFamily));
            if (string.IsNullOrEmpty(control.FontStyle) != true)
                text.FontStyle = (FontStyle)(new FontStyleConverter().ConvertFromString(control.FontStyle));
            if (string.IsNullOrEmpty(control.FontWeight) != true)
                text.FontWeight = (FontWeight)(new FontWeightConverter().ConvertFromString(control.FontWeight));
            if (string.IsNullOrEmpty(control.Foreground) != true)
                text.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFromString(control.Foreground));

            TextWrapping textWrapping = TextWrapping.NoWrap;
            if (control.TextWrapping == TextWrapping.Wrap.ToString())
                textWrapping = TextWrapping.Wrap;
            else if (control.TextWrapping == TextWrapping.NoWrap.ToString())
                textWrapping = TextWrapping.NoWrap;
            else
                textWrapping = TextWrapping.WrapWithOverflow;

            text.TextWrapping = textWrapping;

            TextAlignment textAlignment = TextAlignment.Center;
            if (control.TextAlignment == TextAlignment.Center.ToString())
                textAlignment = TextAlignment.Center;
            else if (control.TextAlignment == TextAlignment.Left.ToString())
                textAlignment = TextAlignment.Left;
            else if (control.TextAlignment == TextAlignment.Right.ToString())
                textAlignment = TextAlignment.Right;
            else
                textAlignment = TextAlignment.Justify;

            text.TextAlignment = textAlignment;
            
            text.GotFocus += new RoutedEventHandler(text_GotFocus);
            text.LostFocus += new RoutedEventHandler(text_LostFocus);
            text.TextChanged += new TextChangedEventHandler(text_TextChanged);

            return text;
        }

        void text_LostFocus(object sender, RoutedEventArgs e)
        {
            //KeyboardTextBox text = sender as KeyboardTextBox;
            base.OnLostFocus(e);
        }

        void text_TextChanged(object sender, TextChangedEventArgs e)
        {
            TSTimeOutTimer.TimerInstance.Restart();
        }

        private Rectangle GetRectangle(TouchUIControl control)
        {
            Rectangle rect = new Rectangle();
            
            rect.Fill = Brushes.Transparent;
            rect.Tag = control.ID;
            rect.Width = control.Width;
            rect.Height = control.Height;
            Canvas.SetLeft(rect, control.PosX);
            Canvas.SetTop(rect, control.PosY);
            Canvas.SetZIndex(rect, control.ZIndex);

            if (control.IsBgTransparent == true)
                rect.Fill = Brushes.Transparent;
            if (control.IsBorderBurshTransparent == true)
                rect.Stroke = Brushes.Transparent;
            if (string.IsNullOrEmpty(control.BorderThicknessSize) == false)
                rect.StrokeThickness = Convert.ToDouble(control.BorderThicknessSize);

            rect.MouseLeftButtonDown += new MouseButtonEventHandler(rect_MouseLeftButtonDown);
            return rect;

        }

        private NumberPanel GetNumberPanel(TouchUIControl control)
        {
            NumberPanel numberPanel = new NumberPanel();

            //numberPanel.Content = control.Name;//구분을 위해 임시로
            numberPanel.Tag = control.ID;
            numberPanel.Width = control.Width;
            numberPanel.Height = control.Height;
            Canvas.SetLeft(numberPanel, control.PosX);
            Canvas.SetTop(numberPanel, control.PosY);
            Canvas.SetZIndex(numberPanel, control.ZIndex);

            return numberPanel;
        }      

        void text_GotFocus(object sender, RoutedEventArgs e)
        {
            TSTimeOutTimer.TimerInstance.Restart();

            TextBox txt = sender as TextBox;

            Guid id = (Guid)txt.Tag;
            TouchUIControl control = new TouchUIControl();

            foreach (TouchUIControl tuicontrol in PageControls)
            {
                if (id.Equals(tuicontrol.ID) == true)
                {
                    control = tuicontrol;
                    break;
                }
            }

            if (control != null && control.MoveScreen.MoveScreenTargetID != null)
            {
                //GotFocusTextBox();
            }
        }

        void btn_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                StopTouchScreenTimeout();

                Button btn = sender as Button;

                Guid id = (Guid)btn.Tag;
                TouchUIControl control = new TouchUIControl();

                foreach (TouchUIControl tuicontrol in PageControls)
                {
                    if (id.Equals(tuicontrol.ID) == true)
                    {
                        control = tuicontrol;
                        break;
                    }
                }

                if (control != null && control.MoveScreen.MoveScreenTargetID != null)
                {
                    if (MoveScreePage != null)
                    {
                        MoveScreePage(control.MoveScreen.MoveScreenTargetID, this.Tag);
                    }
                   
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        void rect_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            StopTouchScreenTimeout();

            Rectangle btn = sender as Rectangle;

            Guid id = (Guid)btn.Tag;
            TouchUIControl control = new TouchUIControl();

            foreach (TouchUIControl tuicontrol in PageControls)
            {
                if (id.Equals(tuicontrol.ID) == true)
                {
                    control = tuicontrol;
                    break;
                }
            }

            if (control != null && control.MoveScreen.MoveScreenTargetID != null)
            {
                MoveScreePage(control.MoveScreen.MoveScreenTargetID, this.Tag);
                this.HideTSScreen(this._timeoutType);
            }
        }

        /// <summary>
        /// 스타일 가져오기
        /// </summary>
        /// <param name="styleName"></param>
        /// <returns></returns>
        private Style GetResourceStyle(string styleName)
        {
            Style style = (Style)FindResource(styleName);

            return style;
        }

        /// <summary>
        /// Screen Display 설정
        /// </summary>
        public void SetTSScreenDisplay()
        {
            bool isOpen = false;
            switch (_screenDisplayType)
            {
                case TouchExcuteType.None:
                    SetScreenVisibility(Visibility.Visible);
                    isOpen = true;
                    break;
                case TouchExcuteType.DisplayAfterTouch:
                    SetScreenVisibility(Visibility.Hidden);
                    isOpen = false;
                    break;
            }

            SetIsOpenControlWrapPopupList(isOpen);
        }

        /// <summary>
        ///  cjy_20090623 : 
        /// </summary>
        public void ShowTSScreen()
        {
            //Console.WriteLine(this.Visibility.ToString());
            if (this.Visibility != Visibility.Visible)
            {                
                SetScreenVisibility(Visibility.Visible);
                
            }

            SetIsOpenControlWrapPopupList(true);
        }

        /// <summary>
        /// cjy_20090624 : 현재 페이지 숨김
        /// </summary>
        /// <param name="type"></param>
        public void HideTSScreen(TimeOutType type)
        {
            if (this.Visibility == Visibility.Visible)
            {
                SetScreenVisibility(Visibility.Visible);
                SetIsOpenControlWrapPopupList(false);
            }            
        }

        private void SetScreenVisibility(Visibility visibility)
        {
            this.Visibility = visibility;
        }

        private void SetIsOpenControlWrapPopupList(bool isOpen)
        {
            //Console.WriteLine(isOpen.ToString());
            foreach (Popup p in controlWrapPopupList)
            {
                p.IsOpen = isOpen;
            }
        }

        /// <summary>
        /// cjy_20090624 : Timeout 설정
        /// </summary>
        public void StartTouchScreenTimeout()
        {
            TSTimeOutTimer.TimerInstance.TimeoutStop();

            switch (_timeoutType)
            {
                case TimeOutType.None:
                    break;
                case TimeOutType.ProcessEnd:
                    CreateTouchScreenTimeout(this._timeoutType, new TouchflowTimeOutDelegate(GoEndScreenFunc));                  
                    break;
                case TimeOutType.ProcessRestart:
                    CreateTouchScreenTimeout(this._timeoutType, new TouchflowTimeOutDelegate(GoStartScreenFunc));                  
                    break;
                case TimeOutType.ScreenHide:
                    CreateTouchScreenTimeout(this._timeoutType, new TouchflowTimeOutDelegate(HideTSScreen));                   
                    break;
                default:
                    break;
            }
        }

        public void RestartTouchScreenTimeout()
        {
            TSTimeOutTimer.TimerInstance.Restart();
        }
       

        /// <summary>
        /// cjy_20090624 : Timeout 종료
        /// </summary>
        public void StopTouchScreenTimeout()
        {
            TSTimeOutTimer.TimerInstance.TimeoutStop();
        }
        

        /// <summary>
        /// cjy_20090624 : End 로 이동하는 콜백함수
        /// </summary>
        /// <param name="type"></param>
        public void GoEndScreenFunc(TimeOutType type)
        {
            MoveScreePage(EndID, this.Tag);
            StopTouchScreenTimeout();
            this.HideTSScreen(this._timeoutType);

        }

        /// <summary>
        /// cjy_20090624 : Start 로 이동하는 콜백함수
        /// </summary>
        /// <param name="type"></param>
        public void GoStartScreenFunc(TimeOutType type)
        {
            MoveScreePage(StartID, this.Tag);
            StopTouchScreenTimeout();
            this.HideTSScreen(this._timeoutType);
        }

       
        public void CreateTouchScreenTimeout(TimeOutType tot, TouchflowTimeOutDelegate func)
        {
            TSTimeOutTimer.TimerInstance.CreateDispatcherTimer();
            TSTimeOutTimer.TimerInstance.IntervalSecond = this._timeoutValue;
            TSTimeOutTimer.TimerInstance.TouchflowTimeOutExecuteHander = func;
            TSTimeOutTimer.TimerInstance.TimeoutStart();
        }
    }

    public enum TouchExcuteType
    {
        None,
        DisplayAfterTouch
    }
}
