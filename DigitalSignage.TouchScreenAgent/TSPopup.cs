﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls.Primitives;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace DigitalSignage.TouchScreenAgent
{
    public class TSPopup : Popup
    {
        public bool IsPopupOpen
        {
            get
            {
                return this.IsOpen;
            }
            set
            {
                this.IsOpen = value;
                //if (this.IsOpen == true)
                //    Application.Current.MainWindow.IsEnabled = false;
                //else
                //    Application.Current.MainWindow.IsEnabled = true;
            }
        }

        public void InitSetting(UIElement child, PlacementMode mode, UIElement placementTarget)
        {
            this.SetChild(child);
            this.SetPlacement(mode);
            this.SetPlacementTarget(placementTarget);
        }

        public void SetChild(UIElement child)
        {
       //     this.Child = new Border { Background = Brushes.White, BorderBrush = Brushes.Red, BorderThickness = new Thickness(2), Child = child };
            this.Child = child;
        }

        public void SetPlacement(PlacementMode mode)
        {
            this.Placement = PlacementMode.MousePoint;
        }

        public void SetPlacementTarget(UIElement element)
        {
            this.PlacementTarget = element;
        }
    }
}
