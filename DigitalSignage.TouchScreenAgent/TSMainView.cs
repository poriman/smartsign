﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace DigitalSignage.TouchScreenAgent
{
    public class TSMainView : Canvas
    {
        private TSPopup tsPopupView = new TSPopup();
        private MainTSWindow tsPopupWiddow;

        public TSMainView() : this(null, new Size(0, 0))
        {
        }

        public TSMainView(UIElement parent, Size size)
        {
            if (parent != null)
            {
                tsPopupWiddow = new MainTSWindow(size);              
            }

            //테스트 코드
            this.Background = Brushes.Blue;
            this.Opacity = 0.2;
            Button btn = new Button();
            btn.Background = Brushes.Red;
            btn.Opacity = 0.2;
            
            btn.Width = 500;
            btn.Height = 500;
            btn.Content = "1111111111111111111";
            Canvas.SetLeft(btn, 0);
            Canvas.SetTop(btn, 0);
            this.Children.Add(btn);
            btn.Click += new RoutedEventHandler(btn_Click);
        }

        void btn_Click(object sender, RoutedEventArgs e)
        {
            string sss = string.Format("Left: {0}, Top: {1}\nRight: {2}, Bottom: {3}\nWidth: {4}, Height: {5}",
                Canvas.GetLeft(sender as Button).ToString(),
                Canvas.GetTop(sender as Button).ToString(),
                Canvas.GetRight(sender as Button).ToString(),
                Canvas.GetBottom(sender as Button).ToString(),
                tsPopupView.Width,
                tsPopupView.Height);
            MessageBox.Show(sss);
        }

        public void CloseTSView()
        {
            tsPopupWiddow.Close();
        }
    }
}
