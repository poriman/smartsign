﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using NLog;
using System.Threading;
using DigitalSignage.Common;
using System.Collections.ObjectModel;
using System.Xml;
using System.ComponentModel;

namespace DigitalSignage.ServerTaskHandler
{
	/// <summary>
	/// 제어 스케줄 처리기
	/// </summary>
	public class TimerLoop
	{
		/// <summary>
		/// 로그 기록기
		/// </summary>
		private static Logger logger = LogManager.GetCurrentClassLogger();

		/// <summary>
		/// Lock 오브젝트
		/// </summary>
		private static readonly object semaphore = new object();

		/// <summary>
		/// 서버와 동기화한 마지막 타임 스템프
		/// </summary>
		long timestamp = 0;

		private bool processing_flag = false;
		/// <summary>
		/// 제어 스케줄 처리기 인스턴스
		/// </summary>
		private static TimerLoop instance = null;

		/// <summary>
		/// 제어 스케줄 처리기 인스턴스 프로퍼티
		/// </summary>
		public static TimerLoop GetInstance
		{
			get
			{
				lock (semaphore)
				{
					if (instance == null)
					{
						instance = new TimerLoop();
					}
					return instance;
				}
			}
		}

		/// <summary>
		/// 제어 스케줄 보관
		/// </summary>
		private TaskPlaylist _controlPlaylist = new TaskPlaylist();

		private TaskPlaylist _actived = new TaskPlaylist();

		private BackgroundWorker bwProcessingControl = new BackgroundWorker();


		/// <summary>
		/// 제어 스케줄 스레드 처리기
		/// </summary>
		private Thread control = null;

		/// <summary>
		/// 서버 .NetRemoting Task 부분
		/// </summary>
		ITaskList serverTasksList = null;
		IDataCacheList serverDataCacheList = null;

		/// <summary>
		/// 서버 접속
		/// </summary>
		/// <returns></returns>
		bool ConnectToServer(out long server_TS)
		{
			try
			{
				try
				{
					server_TS = serverTasksList.GetTasksTimestamp();
					return true;
				}
				catch
				{
					logger.Info("Connecting to .Net Remoting Server...");
					serverTasksList = (ITaskList)Activator.GetObject(
							typeof(ITaskList), "tcp://127.0.0.1:888/TaskListHost/rt");
					serverDataCacheList = (IDataCacheList)Activator.GetObject(
							typeof(IDataCacheList), "tcp://127.0.0.1:888/DataCacheHost/rt");

					server_TS = serverTasksList.GetTasksTimestamp();

					logger.Info(".Net Remoting server access is succeed!");
				}

			}
			catch (Exception e)
			{
				// conection failed
				logger.Error(String.Format("Connecting Failed to .Net Remoting Server: {0}", e.Message));
				logger.Error("It has to be with SmartSign Service.");

				server_TS = timestamp;

				return false;
			}
			return true;
		}

		
		/// <summary>
		/// TimerLoop를 시작한다.
		/// </summary>
		/// <returns></returns>
		public int Start()
		{
			processing_flag = true;

			//starting control thread
			control = new Thread(this.controlThread);
			control.Priority = ThreadPriority.BelowNormal;
			control.Start();

			try
			{
				if (null != bwProcessingControl)
				{
					bwProcessingControl.Dispose();
					bwProcessingControl = null;
				}

				bwProcessingControl = new BackgroundWorker();
				bwProcessingControl.DoWork += new DoWorkEventHandler(bwProcessingControl_DoWork);

				bwProcessingControl.RunWorkerAsync();
			}
			catch { }

			return 0;
		}

		/// <summary>
		/// TimerLoop를 정지시킨다
		/// </summary>
		/// <returns>reserved</returns>
		public int Stop()
		{
			processing_flag = false;
			try
			{
				bwProcessingControl.Dispose();
				bwProcessingControl = null;
			}
			catch { }
			return 0;
		}

		/// <summary>
		/// 1초마다 Task루틴을 수행할 스레드를 생성한다.
		/// </summary>
		private void controlThread()
		{
			logger.Info("Starting Timeloop thread...");
			
			while (processing_flag)
			{
				long server_TS = -1;
				if (ConnectToServer(out server_TS))
				{

					runTasks(server_TS);

				} 
				Thread.Sleep(1000);

			}

			logger.Info("Ending Timeloop thread...");
		}

		/// <summary>
		/// Task 관련 업무를 수행할 스레드 함수. 로컬 DB를 쿼리해 현재 이용가능한 데이터를 처리한다. 
		/// </summary>
		private void runTasks(long server_TS)
		{
			try
			{
				if (server_TS > timestamp)
				{
					//	timestamp 가 0 인 경우 현재 시간에 걸려있는 스케줄을 조회하고, 0이 아닌경우 timestamp 이후에 생성된 스케줄만 가져온다.
					Task[] tasks = serverTasksList.GetTasksByNameAndType(timestamp, (int)TaskCommands.CmdControlSchedule, TaskControlSchedules.CtrlPowerOn.ToString());

					if (tasks == null)
					{
						logger.Info("No active tasks");
						return; // nothing new
					}
					logger.Info(string.Format("{0} Tasks has been detected.", tasks.Length));

					for (int i = 0; i < tasks.Length; i++)
					{
						//running task
						switch ((TaskCommands)tasks[i].type)
						{
							case TaskCommands.CmdControlSchedule:

								ControlTask(tasks[i]);
								break;
							default:
								logger.Error(String.Format("Unmanaged Task is detected: Task Type: {0}", ((TaskCommands)tasks[i].type).ToString()));
								break;
						}
					}

					timestamp = server_TS;
				}

				RunControlTask();
			}
			catch (Exception ex)
			{
				logger.Error(ex);
			}
		}

		#region 제어 스케줄 처리

		/// <summary>
		/// 프로그램 스크린의 재생/종료을 담당한다.
		/// </summary>
		private void RunControlTask()
		{
			Collection<Task> _arrWillRemove = new Collection<Task>();

			Task next = _controlPlaylist.First;

			do
			{
				if (next == null) continue;

				long current = TimeConverter.ConvertToUTP(DateTime.UtcNow);

				if(next.TaskStart <= current && next.TaskEnd >= current)
				{
					//	1. 일일 내 시간 검사
					//	2. 요일 검사
					if (CheckContainTimeofDay(next.starttimeofday, next.endtimeofday) && CheckDayOfWeek(next.daysofweek))
					{
						//	현재 시간과 비교하여 하루 시간 대와 요일에 맞는다면..
						lock (lockProcess)
						{
							_actived.AddTask(next);
						}
						//if (PlayControlSchedule(next))
						{
							logger.Info(String.Format("[CmdControlSchedule][{0}] Started schedule.", next.description));
							if (next.starttimeofday == next.endtimeofday)
							{
								// 반복 패턴이 없는 경우
								_arrWillRemove.Add(next);
							}
						}
					
					}
					else if (next.TaskEnd < current)
					{
						logger.Info(String.Format("[CmdControlSchedule][{0}] {1} will be ended.",next.description, next.uuid));
						_arrWillRemove.Add(next);
					}
				}

			}
			while ((next = _controlPlaylist.NextToEnd) != null);


			try
			{
				//	처리된 제어 스케줄을 제거한다. 
				foreach (Task t in _arrWillRemove)
				{
					_controlPlaylist.RemoveTask(t);
				}
			}
			catch { }

		}

		static object lockProcess = new object();

		void bwProcessingControl_DoWork(object sender, DoWorkEventArgs e)
		{
			logger.Info("Background Process Thread Starting...");
			while (this.processing_flag)
			{
				try
				{
					Task t = null;

					lock (lockProcess)
					{
						t = _actived.First;
					}

					while(t != null)
					{
						PlayControlSchedule(t);

						lock (lockProcess)
						{
							t = _actived.Pop();
						}
					}

				}
				catch { }
				Thread.Sleep(1000);
			}

			logger.Info("Background Process Thread Stopping...");

		}


		/// <summary>
		/// 제어 스케줄 재생
		/// </summary>
		/// <param name="item"></param>
		/// <returns></returns>
		private bool PlayControlSchedule(Task item)
		{
			try
			{
				logger.Info(String.Format("Schedule Name: {0}", item.description));
				string xmlReturns = String.Empty;
				if ((String.IsNullOrEmpty(item.pid) || item.pid.Equals("-1")))
				{
					//	그룹 스케줄
					logger.Info("Unit: Group");
					xmlReturns = serverDataCacheList.GetGroupNode(item.gid);
				}
				else
				{
					//	플레이어 스케줄
					logger.Info("Unit: Player");
					xmlReturns = serverDataCacheList.GetPlayerNode(item.pid);
				}

				XmlDocument networkTree = new XmlDocument();

				networkTree.LoadXml(xmlReturns);
				XmlNodeList nodePlayers = networkTree.SelectNodes("//child::player[attribute::del_yn=\"N\"]");

				if (item.description.Equals(TaskControlSchedules.CtrlPowerOn.ToString()))
				{
					logger.Info(String.Format("Control Type: {0}", item.description));

					foreach (XmlNode player in nodePlayers)
					{
						/// 제어 스레드 생성
						Thread th = new Thread(new ParameterizedThreadStart(thControlPowerOn));
						th.Start(player);
					}

					return true;
				}
			}
			catch (Exception ex)
			{
				logger.Error(ex);
			}			
			return false;
		}

		private void thControlPowerOn(object pnode)
		{
			XmlNode player = pnode as XmlNode;
			try
			{
				logger.Info(String.Format("Player ID: {0}", player.Attributes["pid"].Value));

				int nMntType = Convert.ToInt32(player.Attributes["_mnttype"].Value.ToString());

				if (nMntType != 0)
				{
					string sIPv4 = player.Attributes["_ipv4addr"].Value.ToString();
					string sMAC = player.Attributes["_macaddr"].Value.ToString();

					if ((nMntType & (int)PowerMntType.AMT) == (int)PowerMntType.AMT)
					{

						string sAuthId = player.Attributes["_auth_id"].Value.ToString();
						string sAuthPass = player.Attributes["_auth_pass"].Value.ToString();

						logger.Info(String.Format("AMT Call: IP({0}), AuthID({1}), AuthPass({2})", sIPv4, sAuthId, sAuthPass));

						try
						{
							Intel.vPro.AMT.CVProCtrl vProControl = new Intel.vPro.AMT.CVProCtrl();
							vProControl.SetPowerOn(sIPv4, sAuthId, sAuthPass);
							logger.Info("AMT Call Complete!");
						}
						catch (Exception exr)
						{
							logger.Error(String.Format("AMT Call Exception: ", exr.ToString()));
						}
					}

					if ((nMntType & (int)PowerMntType.WoL) == (int)PowerMntType.WoL)
					{
						try
						{
							logger.Info(String.Format("WoL Call: IP({0}) MAC({1})", sIPv4, sMAC));

							Intel.vPro.AMT.CVProCtrl vProControl = new Intel.vPro.AMT.CVProCtrl();
							vProControl.CallWOL(sMAC.Replace(":", ""), 40000);
							logger.Info("WoL Call Complete!");
						}
						catch (Exception exr)
						{
							logger.Error(String.Format("WoL Call Exception: ", exr.ToString()));
						}
					}
				}
			}
			catch { }
		}

		/// <summary>
		/// 제어 스케줄 처리
		/// </summary>
		/// <param name="task"></param>
		private void ControlTask(Task task)
		{
			if (task.state == TaskState.StateWait)
			{
				if (task.TaskEnd > TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()))
				{
					//	현재 시간보다 스케줄 종료 시간이 더 크다면
					_controlPlaylist.Push(task);

					logger.Info("Task " + task.ToString());

					logger.Info(String.Format("[CmdControlSchedule][{0}] StateWait.", task.description));
				}
				else
				{

					logger.Info("[" + task.description + " (" + task.uuid + ")] End time is wrong. It will be canceled.");

					_controlPlaylist.RemoveTask(task);
				}
			}
			else if (task.state == TaskState.StateEdit)
			{
				logger.Info("Task " + task.ToString());
				logger.Info(String.Format("[CmdControlSchedule][{0}] StateEdit.", task.description));

				_controlPlaylist.RemoveTask(task);

				_controlPlaylist.AddTask(task);
			}
			else if (task.state == TaskState.StateCanceled)
			{
				logger.Info("Changing status to [StateCanceled]. (UUID=" + task.uuid + ")");

				_controlPlaylist.RemoveTask(task);

			}
			else
			{
				logger.Error("[CmdControlSchedule] Unknown TaskState : " + task.state.ToString());
			}
		}
		#endregion

		/// <summary>
		/// 현재 시간이 TimeOfDay에 포함되는지
		/// </summary>
		/// <param name="nStartTime"></param>
		/// <param name="nEndTime"></param>
		/// <returns></returns>
		private bool CheckContainTimeofDay(int nStartTime, int nEndTime)
		{
			//	둘다 같은 값이라면 Whole Day로 간주
			if (nStartTime == nEndTime) return true;

			DateTime dtLocal = DateTime.Now;
			int nCurr = dtLocal.Second + (dtLocal.Minute * 60) + (dtLocal.Hour * 3600);

			if (nStartTime < nEndTime)
			{
				//	시간대가 하루에 국한되는 경우

				return nCurr >= nStartTime && nCurr < nEndTime;
			}
			else
			{
				//	시간대가 오늘부터 다음 날로 넘어 간 경우
				return nCurr < nStartTime || nCurr >= nEndTime;
			}
		}

		/// <summary>
		/// 요일 체크
		/// </summary>
		/// <param name="dayofweek"></param>
		/// <returns></returns>
		private bool CheckDayOfWeek(int dayofweek)
		{
			DayOfWeek dow = DateTime.Today.DayOfWeek;
			if (dow == DayOfWeek.Monday)
				return 0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_MON);
			else if (dow == DayOfWeek.Tuesday)
				return 0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_TUE);
			else if (dow == DayOfWeek.Wednesday)
				return 0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_WED);
			else if (dow == DayOfWeek.Thursday)
				return 0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_THU);
			else if (dow == DayOfWeek.Friday)
				return 0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_FRI);
			else if (dow == DayOfWeek.Saturday)
				return 0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_SAT);
			else if (dow == DayOfWeek.Sunday)
				return 0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_SUN);
			else
			{
				logger.Error("Exception : invalid DayOfWeek. : " + dayofweek.ToString());
			}
			return false;
		}

		/// <summary>
		/// 데이터베이스 상에 State 가 Processed 또는 Processing이면 변경한다. 왜냐하면 
		/// PlayAgent에서 State를 변경했을 경우 공교롭게 엎어쓸수도 있기때문이다. 
		/// </summary>
		/// <param name="willChange"></param>
		/// <param name="uuid"></param>
		void ChangeTaskState(TaskState willChange, Guid uuid)
		{
			try
			{
				serverTasksList.ChangeTaskState(willChange, uuid);
			}
			catch (System.Exception e)
			{
				logger.Error(e.Message);
			}
		}
	}
}
