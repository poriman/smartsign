﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DigitalSignage.Common;

namespace TrialKeyMaker
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
			dt_installed.Value = DateTime.Now;
		}

		private void button1_Click(object sender, EventArgs e)
		{
			try
			{
				DateTime dt = dt_installed.Value;
				int days = String.IsNullOrEmpty(tb_days.Text) ? 0 : Convert.ToInt32(tb_days.Text);

				TrialChecker tc = new TrialChecker();
				
				tc.CreateDefaultKey();

				tc.InstalledTime = new DateTime(dt.Year, dt.Month, dt.Day, 0, 0, 0);
				tc.LastExecutedTime = new DateTime(dt.Year, dt.Month, dt.Day, 0, 0, 0);
				tc.IsTrial = cb_isTrial.Checked;
				tc.TrialDuration = new TimeSpan(days, 0, 0, 0);

				MessageBox.Show("Success!");
			}
			catch (Exception ee)
			{
				MessageBox.Show(ee.ToString());
			}

		}

		private void btn_check_Click(object sender, EventArgs e)
		{
			try
			{
				TrialChecker tc = new TrialChecker();

				tb_result.Text = String.Format(
					"설치 날짜 : {0}\r\nTrial 여부 : {1}\r\nTrial 일 수 : {2}\r\n마지막 실행 시간 : {3}",
					tc.InstalledTime.ToString(), tc.IsTrial.ToString(), tc.TrialDuration.Days.ToString(), tc.LastExecutedTime.ToString());
			}
			catch (Exception ee)
			{
				tb_result.Text = "정보가 없습니다.";
			}			
		}

		private void cb_isTrial_CheckedChanged(object sender, EventArgs e)
		{
			tb_days.Enabled = cb_isTrial.Checked;
		}
	}
}
