﻿namespace TrialKeyMaker
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
			this.dt_installed = new System.Windows.Forms.DateTimePicker();
			this.button1 = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.tb_days = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.tb_result = new System.Windows.Forms.TextBox();
			this.btn_check = new System.Windows.Forms.Button();
			this.cb_isTrial = new System.Windows.Forms.CheckBox();
			this.SuspendLayout();
			// 
			// dt_installed
			// 
			this.dt_installed.Location = new System.Drawing.Point(76, 12);
			this.dt_installed.Name = "dt_installed";
			this.dt_installed.Size = new System.Drawing.Size(200, 21);
			this.dt_installed.TabIndex = 0;
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(201, 73);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(75, 23);
			this.button1.TabIndex = 1;
			this.button1.Text = "만들기";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(13, 16);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(57, 12);
			this.label1.TabIndex = 2;
			this.label1.Text = "설치 날짜";
			// 
			// tb_days
			// 
			this.tb_days.Enabled = false;
			this.tb_days.Location = new System.Drawing.Point(76, 39);
			this.tb_days.Name = "tb_days";
			this.tb_days.Size = new System.Drawing.Size(41, 21);
			this.tb_days.TabIndex = 3;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(13, 42);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(62, 12);
			this.label2.TabIndex = 4;
			this.label2.Text = "Trial 일 수";
			// 
			// tb_result
			// 
			this.tb_result.Location = new System.Drawing.Point(20, 102);
			this.tb_result.Multiline = true;
			this.tb_result.Name = "tb_result";
			this.tb_result.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.tb_result.Size = new System.Drawing.Size(256, 116);
			this.tb_result.TabIndex = 5;
			// 
			// btn_check
			// 
			this.btn_check.Location = new System.Drawing.Point(20, 73);
			this.btn_check.Name = "btn_check";
			this.btn_check.Size = new System.Drawing.Size(75, 23);
			this.btn_check.TabIndex = 6;
			this.btn_check.Text = "체크";
			this.btn_check.UseVisualStyleBackColor = true;
			this.btn_check.Click += new System.EventHandler(this.btn_check_Click);
			// 
			// cb_isTrial
			// 
			this.cb_isTrial.AutoSize = true;
			this.cb_isTrial.Location = new System.Drawing.Point(123, 41);
			this.cb_isTrial.Name = "cb_isTrial";
			this.cb_isTrial.Size = new System.Drawing.Size(77, 16);
			this.cb_isTrial.TabIndex = 7;
			this.cb_isTrial.Text = "Trial 버전";
			this.cb_isTrial.UseVisualStyleBackColor = true;
			this.cb_isTrial.CheckedChanged += new System.EventHandler(this.cb_isTrial_CheckedChanged);
			// 
			// Form1
			// 
			this.AcceptButton = this.btn_check;
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(293, 230);
			this.Controls.Add(this.cb_isTrial);
			this.Controls.Add(this.btn_check);
			this.Controls.Add(this.tb_result);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.tb_days);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.dt_installed);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "Form1";
			this.Text = "Trial Maker";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.DateTimePicker dt_installed;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox tb_days;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox tb_result;
		private System.Windows.Forms.Button btn_check;
		private System.Windows.Forms.CheckBox cb_isTrial;
	}
}

