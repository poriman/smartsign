﻿//------------------------------------------------------------------------------
// <auto-generated>
//     이 코드는 도구를 사용하여 생성되었습니다.
//     런타임 버전:2.0.50727.1433
//
//     파일 내용을 변경하면 잘못된 동작이 발생할 수 있으며, 코드를 다시 생성하면
//     이러한 변경 내용이 손실됩니다.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IVisionService
{
    using System.Runtime.Serialization;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "3.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="Topic", Namespace="http://schemas.datacontract.org/2004/07/IVisionService")]
    public partial class Topic : object, System.Runtime.Serialization.IExtensibleDataObject
    {
        
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        private string GroupidField;
        
        private IVisionService.UserLevels LevelField;
        
        private string MediaidField;
        
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData
        {
            get
            {
                return this.extensionDataField;
            }
            set
            {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Groupid
        {
            get
            {
                return this.GroupidField;
            }
            set
            {
                this.GroupidField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public IVisionService.UserLevels Level
        {
            get
            {
                return this.LevelField;
            }
            set
            {
                this.LevelField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Mediaid
        {
            get
            {
                return this.MediaidField;
            }
            set
            {
                this.MediaidField = value;
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "3.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="UserLevels", Namespace="http://schemas.datacontract.org/2004/07/IVisionService")]
    public enum UserLevels : int
    {
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        Admin = 0,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        Group = 1,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        Media = 2,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        Scheduler = 3,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        None = 4,
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "3.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="Message", Namespace="http://schemas.datacontract.org/2004/07/IVisionService")]
    public partial class Message : object, System.Runtime.Serialization.IExtensibleDataObject
    {
        
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        private string ContentField;
        
        private IVisionService.UserLevels LevelField;
        
        private IVisionService.MessageType MsgTypeField;
        
        private string ResultField;
        
        private IVisionService.Topic SenderField;
        
        private System.DateTime TimeField;
        
        private string VersionField;
        
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData
        {
            get
            {
                return this.extensionDataField;
            }
            set
            {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Content
        {
            get
            {
                return this.ContentField;
            }
            set
            {
                this.ContentField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public IVisionService.UserLevels Level
        {
            get
            {
                return this.LevelField;
            }
            set
            {
                this.LevelField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public IVisionService.MessageType MsgType
        {
            get
            {
                return this.MsgTypeField;
            }
            set
            {
                this.MsgTypeField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Result
        {
            get
            {
                return this.ResultField;
            }
            set
            {
                this.ResultField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public IVisionService.Topic Sender
        {
            get
            {
                return this.SenderField;
            }
            set
            {
                this.SenderField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.DateTime Time
        {
            get
            {
                return this.TimeField;
            }
            set
            {
                this.TimeField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Version
        {
            get
            {
                return this.VersionField;
            }
            set
            {
                this.VersionField = value;
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "3.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="MessageType", Namespace="http://schemas.datacontract.org/2004/07/IVisionService")]
    public enum MessageType : int
    {
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        msg_Login = 0,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        msg_CtrlSche = 1,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        msg_MediaSche = 2,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        msg_UrgSche = 3,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        msg_SubtitleSche = 4,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        msg_TimeSche = 5,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        msg_AppUpdate = 6,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        msg_ChangeID = 7,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        msg_Leave = 8,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        msg_Error = 9,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        msg_Status = 10,

		[System.Runtime.Serialization.EnumMemberAttribute()]
		msg_PlayerInfo = 11,

        [System.Runtime.Serialization.EnumMemberAttribute()]
        msg_RequestGSRelay = 12,

        [System.Runtime.Serialization.EnumMemberAttribute()]
        msg_ResponseGSRelay = 13,
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "3.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="FileMessage", Namespace="http://schemas.datacontract.org/2004/07/IVisionService")]
    public partial class FileMessage : object, System.Runtime.Serialization.IExtensibleDataObject
    {
        
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        private byte[] DataField;
        
        private bool ExcuteField;
        
        private string FileNameField;
        
        private string FilePathField;
        
        private IVisionService.Topic SenderField;
        
        private System.DateTime TimeField;
        
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData
        {
            get
            {
                return this.extensionDataField;
            }
            set
            {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public byte[] Data
        {
            get
            {
                return this.DataField;
            }
            set
            {
                this.DataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public bool Excute
        {
            get
            {
                return this.ExcuteField;
            }
            set
            {
                this.ExcuteField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string FileName
        {
            get
            {
                return this.FileNameField;
            }
            set
            {
                this.FileNameField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string FilePath
        {
            get
            {
                return this.FilePathField;
            }
            set
            {
                this.FilePathField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public IVisionService.Topic Sender
        {
            get
            {
                return this.SenderField;
            }
            set
            {
                this.SenderField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.DateTime Time
        {
            get
            {
                return this.TimeField;
            }
            set
            {
                this.TimeField = value;
            }
        }
    }
}


[System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "3.0.0.0")]
[System.ServiceModel.ServiceContractAttribute(ConfigurationName="INetworkService", CallbackContract=typeof(INetworkServiceCallback), SessionMode=System.ServiceModel.SessionMode.Required)]
public interface INetworkService
{
    
    [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/INetworkService/Connect", ReplyAction="http://tempuri.org/INetworkService/ConnectResponse")]
    bool Connect(IVisionService.Topic client);
    
    [System.ServiceModel.OperationContractAttribute(AsyncPattern=true, Action="http://tempuri.org/INetworkService/Connect", ReplyAction="http://tempuri.org/INetworkService/ConnectResponse")]
    System.IAsyncResult BeginConnect(IVisionService.Topic client, System.AsyncCallback callback, object asyncState);
    
    bool EndConnect(System.IAsyncResult result);
    
    [System.ServiceModel.OperationContractAttribute(IsOneWay=true, Action="http://tempuri.org/INetworkService/SendMessage")]
    void SendMessage(IVisionService.Message msg);
    
    [System.ServiceModel.OperationContractAttribute(IsOneWay=true, AsyncPattern=true, Action="http://tempuri.org/INetworkService/SendMessage")]
    System.IAsyncResult BeginSendMessage(IVisionService.Message msg, System.AsyncCallback callback, object asyncState);
    
    void EndSendMessage(System.IAsyncResult result);
    
    [System.ServiceModel.OperationContractAttribute(IsOneWay=true, Action="http://tempuri.org/INetworkService/Whisper")]
    void Whisper(IVisionService.Message msg, IVisionService.Topic receiver);
    
    [System.ServiceModel.OperationContractAttribute(IsOneWay=true, AsyncPattern=true, Action="http://tempuri.org/INetworkService/Whisper")]
    System.IAsyncResult BeginWhisper(IVisionService.Message msg, IVisionService.Topic receiver, System.AsyncCallback callback, object asyncState);
    
    void EndWhisper(System.IAsyncResult result);
    
    [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/INetworkService/SendFile", ReplyAction="http://tempuri.org/INetworkService/SendFileResponse")]
    bool SendFile(IVisionService.FileMessage fileMsg, IVisionService.Topic receiver);
    
    [System.ServiceModel.OperationContractAttribute(AsyncPattern=true, Action="http://tempuri.org/INetworkService/SendFile", ReplyAction="http://tempuri.org/INetworkService/SendFileResponse")]
    System.IAsyncResult BeginSendFile(IVisionService.FileMessage fileMsg, IVisionService.Topic receiver, System.AsyncCallback callback, object asyncState);
    
    bool EndSendFile(System.IAsyncResult result);
    
    [System.ServiceModel.OperationContractAttribute(IsOneWay=true, IsTerminating=true, Action="http://tempuri.org/INetworkService/Disconnect")]
    void Disconnect(IVisionService.Topic client);
    
    [System.ServiceModel.OperationContractAttribute(IsOneWay=true, IsTerminating=true, AsyncPattern=true, Action="http://tempuri.org/INetworkService/Disconnect")]
    System.IAsyncResult BeginDisconnect(IVisionService.Topic client, System.AsyncCallback callback, object asyncState);
    
    void EndDisconnect(System.IAsyncResult result);
}

[System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "3.0.0.0")]
public interface INetworkServiceCallback
{
    
    [System.ServiceModel.OperationContractAttribute(IsOneWay=true, Action="http://tempuri.org/INetworkService/RefreshClients")]
    void RefreshClients(IVisionService.Topic[] clients);
    
    [System.ServiceModel.OperationContractAttribute(IsOneWay=true, AsyncPattern=true, Action="http://tempuri.org/INetworkService/RefreshClients")]
    System.IAsyncResult BeginRefreshClients(IVisionService.Topic[] clients, System.AsyncCallback callback, object asyncState);
    
    void EndRefreshClients(System.IAsyncResult result);
    
    [System.ServiceModel.OperationContractAttribute(IsOneWay=true, Action="http://tempuri.org/INetworkService/Receive")]
    void Receive(IVisionService.Message msg);
    
    [System.ServiceModel.OperationContractAttribute(IsOneWay=true, AsyncPattern=true, Action="http://tempuri.org/INetworkService/Receive")]
    System.IAsyncResult BeginReceive(IVisionService.Message msg, System.AsyncCallback callback, object asyncState);
    
    void EndReceive(System.IAsyncResult result);
    
    [System.ServiceModel.OperationContractAttribute(IsOneWay=true, Action="http://tempuri.org/INetworkService/ReceiveWhisper")]
    void ReceiveWhisper(IVisionService.Message msg, IVisionService.Topic receiver);
    
    [System.ServiceModel.OperationContractAttribute(IsOneWay=true, AsyncPattern=true, Action="http://tempuri.org/INetworkService/ReceiveWhisper")]
    System.IAsyncResult BeginReceiveWhisper(IVisionService.Message msg, IVisionService.Topic receiver, System.AsyncCallback callback, object asyncState);
    
    void EndReceiveWhisper(System.IAsyncResult result);
    
    [System.ServiceModel.OperationContractAttribute(IsOneWay=true, Action="http://tempuri.org/INetworkService/ReceiverFile")]
    void ReceiverFile(IVisionService.FileMessage fileMsg, IVisionService.Topic receiver);
    
    [System.ServiceModel.OperationContractAttribute(IsOneWay=true, AsyncPattern=true, Action="http://tempuri.org/INetworkService/ReceiverFile")]
    System.IAsyncResult BeginReceiverFile(IVisionService.FileMessage fileMsg, IVisionService.Topic receiver, System.AsyncCallback callback, object asyncState);
    
    void EndReceiverFile(System.IAsyncResult result);
    
    [System.ServiceModel.OperationContractAttribute(IsOneWay=true, Action="http://tempuri.org/INetworkService/UserJoin")]
    void UserJoin(IVisionService.Topic client);
    
    [System.ServiceModel.OperationContractAttribute(IsOneWay=true, AsyncPattern=true, Action="http://tempuri.org/INetworkService/UserJoin")]
    System.IAsyncResult BeginUserJoin(IVisionService.Topic client, System.AsyncCallback callback, object asyncState);
    
    void EndUserJoin(System.IAsyncResult result);
    
    [System.ServiceModel.OperationContractAttribute(IsOneWay=true, Action="http://tempuri.org/INetworkService/UserLeave")]
    void UserLeave(IVisionService.Topic client);
    
    [System.ServiceModel.OperationContractAttribute(IsOneWay=true, AsyncPattern=true, Action="http://tempuri.org/INetworkService/UserLeave")]
    System.IAsyncResult BeginUserLeave(IVisionService.Topic client, System.AsyncCallback callback, object asyncState);
    
    void EndUserLeave(System.IAsyncResult result);
}

[System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "3.0.0.0")]
public interface INetworkServiceChannel : INetworkService, System.ServiceModel.IClientChannel
{
}

[System.Diagnostics.DebuggerStepThroughAttribute()]
[System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "3.0.0.0")]
public partial class NetworkServiceClient : System.ServiceModel.DuplexClientBase<INetworkService>, INetworkService
{
    
    public NetworkServiceClient(System.ServiceModel.InstanceContext callbackInstance) : 
            base(callbackInstance)
    {
    }
    
    public NetworkServiceClient(System.ServiceModel.InstanceContext callbackInstance, string endpointConfigurationName) : 
            base(callbackInstance, endpointConfigurationName)
    {
    }
    
    public NetworkServiceClient(System.ServiceModel.InstanceContext callbackInstance, string endpointConfigurationName, string remoteAddress) : 
            base(callbackInstance, endpointConfigurationName, remoteAddress)
    {
    }
    
    public NetworkServiceClient(System.ServiceModel.InstanceContext callbackInstance, string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
            base(callbackInstance, endpointConfigurationName, remoteAddress)
    {
    }
    
    public NetworkServiceClient(System.ServiceModel.InstanceContext callbackInstance, System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
            base(callbackInstance, binding, remoteAddress)
    {
    }
    
    public bool Connect(IVisionService.Topic client)
    {
        return base.Channel.Connect(client);
    }
    
    public System.IAsyncResult BeginConnect(IVisionService.Topic client, System.AsyncCallback callback, object asyncState)
    {
        return base.Channel.BeginConnect(client, callback, asyncState);
    }
    
    public bool EndConnect(System.IAsyncResult result)
    {
        return base.Channel.EndConnect(result);
    }
    
    public void SendMessage(IVisionService.Message msg)
    {
        base.Channel.SendMessage(msg);
    }
    
    public System.IAsyncResult BeginSendMessage(IVisionService.Message msg, System.AsyncCallback callback, object asyncState)
    {
        return base.Channel.BeginSendMessage(msg, callback, asyncState);
    }
    
    public void EndSendMessage(System.IAsyncResult result)
    {
        base.Channel.EndSendMessage(result);
    }
    
    public void Whisper(IVisionService.Message msg, IVisionService.Topic receiver)
    {
        base.Channel.Whisper(msg, receiver);
    }
    
    public System.IAsyncResult BeginWhisper(IVisionService.Message msg, IVisionService.Topic receiver, System.AsyncCallback callback, object asyncState)
    {
        return base.Channel.BeginWhisper(msg, receiver, callback, asyncState);
    }
    
    public void EndWhisper(System.IAsyncResult result)
    {
        base.Channel.EndWhisper(result);
    }
    
    public bool SendFile(IVisionService.FileMessage fileMsg, IVisionService.Topic receiver)
    {
        return base.Channel.SendFile(fileMsg, receiver);
    }
    
    public System.IAsyncResult BeginSendFile(IVisionService.FileMessage fileMsg, IVisionService.Topic receiver, System.AsyncCallback callback, object asyncState)
    {
        return base.Channel.BeginSendFile(fileMsg, receiver, callback, asyncState);
    }
    
    public bool EndSendFile(System.IAsyncResult result)
    {
        return base.Channel.EndSendFile(result);
    }
    
    public void Disconnect(IVisionService.Topic client)
    {
        base.Channel.Disconnect(client);
    }
    
    public System.IAsyncResult BeginDisconnect(IVisionService.Topic client, System.AsyncCallback callback, object asyncState)
    {
        return base.Channel.BeginDisconnect(client, callback, asyncState);
    }
    
    public void EndDisconnect(System.IAsyncResult result)
    {
        base.Channel.EndDisconnect(result);
    }
}
