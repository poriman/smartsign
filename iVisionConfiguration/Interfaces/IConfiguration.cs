﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace iVisionConfiguration
{
	public interface IConfiguration
	{
		bool LoadConfig(string path);
		bool SaveConfig(string path);
	}
}
