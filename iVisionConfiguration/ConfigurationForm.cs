﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using iVisionConfiguration.Pages;
using System.IO;

namespace iVisionConfiguration
{
	public partial class ConfigurationForm : GLib.Options.OptionsForm
	{
		public ConfigurationForm() : base(Properties.Settings.Default)
		{
			InitializeComponent();

			bool hasPlayer = false, hasServer = false, hasContServer = false, hasManager = false, hasDesigner = false;
			string[] arrFiles = Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory);

			var returns = from file in arrFiles
                          where file.ToLower().EndsWith("ds.screencast.exe")
						  select file;

			hasPlayer = (returns.Count() > 0);

			returns = from file in arrFiles
					  where file.ToLower().EndsWith("schedulerservice.exe")
						  select file;

			hasServer = (returns.Count() > 0);

			returns = from file in arrFiles
                      where file.ToLower().EndsWith("ds.screenmanager.exe")
					  select file;

			hasManager = (returns.Count() > 0);

			returns = from file in arrFiles
                      where file.ToLower().EndsWith("ds.screendesigner.exe")
					  select file;

			hasDesigner = (returns.Count() > 0);

			returns = from file in arrFiles
					  where file.ToLower().EndsWith("slimftpd.exe")
					  select file;

			hasContServer = (returns.Count() > 0);

						
			Panels.Add(new EnvironmentPage());
			Panels.Add(new ChangeEditionPage());
			if(hasPlayer)
			{
				Panels.Add(new PlayerUIPage());
				Panels.Add(new SubtitleUIPage());
				Panels.Add(new PUpdatePage());
			}
			if(hasManager)
				Panels.Add(new MUpdatePage());
			if(hasServer)
				Panels.Add(new ServerPage());
			if(hasContServer)
				Panels.Add(new ContServerPage());

		}

		private void ConfigurationForm_OptionsSaved(object sender, EventArgs e)
		{
			foreach (IConfiguration config in Panels)
			{
				config.SaveConfig(AppDomain.CurrentDomain.BaseDirectory);
			}
		}
	}
}
