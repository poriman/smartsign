﻿namespace iVisionConfiguration
{
	partial class EnvironmentPage
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			this.gbRegion = new System.Windows.Forms.GroupBox();
			this.btnConfirm = new System.Windows.Forms.Button();
			this.label6 = new System.Windows.Forms.Label();
			this.tbAddress = new System.Windows.Forms.TextBox();
			this.cboxCity = new System.Windows.Forms.ComboBox();
			this.cboxCountry = new System.Windows.Forms.ComboBox();
			this.label5 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.dtpickerInstallation = new System.Windows.Forms.DateTimePicker();
			this.gbRegion.SuspendLayout();
			this.SuspendLayout();
			// 
			// gbRegion
			// 
			this.gbRegion.Controls.Add(this.btnConfirm);
			this.gbRegion.Controls.Add(this.label6);
			this.gbRegion.Controls.Add(this.tbAddress);
			this.gbRegion.Controls.Add(this.cboxCity);
			this.gbRegion.Controls.Add(this.cboxCountry);
			this.gbRegion.Controls.Add(this.label5);
			this.gbRegion.Controls.Add(this.label4);
			this.gbRegion.Controls.Add(this.label3);
			this.gbRegion.Controls.Add(this.label2);
			this.gbRegion.Controls.Add(this.dtpickerInstallation);
			this.gbRegion.Location = new System.Drawing.Point(3, 3);
			this.gbRegion.Name = "gbRegion";
			this.gbRegion.Size = new System.Drawing.Size(316, 197);
			this.gbRegion.TabIndex = 3;
			this.gbRegion.TabStop = false;
			this.gbRegion.Text = "Environment Information";
			// 
			// btnConfirm
			// 
			this.btnConfirm.Location = new System.Drawing.Point(236, 146);
			this.btnConfirm.Name = "btnConfirm";
			this.btnConfirm.Size = new System.Drawing.Size(52, 25);
			this.btnConfirm.TabIndex = 10;
			this.btnConfirm.Text = "Confirm";
			this.btnConfirm.UseVisualStyleBackColor = true;
			this.btnConfirm.Click += new System.EventHandler(this.btnConfirm_Click);
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(5, 152);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(99, 13);
			this.label6.TabIndex = 9;
			this.label6.Text = "System Information:";
			// 
			// tbAddress
			// 
			this.tbAddress.Location = new System.Drawing.Point(95, 97);
			this.tbAddress.Multiline = true;
			this.tbAddress.Name = "tbAddress";
			this.tbAddress.Size = new System.Drawing.Size(193, 43);
			this.tbAddress.TabIndex = 7;
			// 
			// cboxCity
			// 
			this.cboxCity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cboxCity.FormattingEnabled = true;
			this.cboxCity.Location = new System.Drawing.Point(95, 73);
			this.cboxCity.Name = "cboxCity";
			this.cboxCity.Size = new System.Drawing.Size(104, 21);
			this.cboxCity.TabIndex = 6;
			// 
			// cboxCountry
			// 
			this.cboxCountry.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cboxCountry.FormattingEnabled = true;
			this.cboxCountry.Location = new System.Drawing.Point(95, 48);
			this.cboxCountry.Name = "cboxCountry";
			this.cboxCountry.Size = new System.Drawing.Size(104, 21);
			this.cboxCountry.TabIndex = 5;
			this.cboxCountry.SelectionChangeCommitted += new System.EventHandler(this.cboxCountry_SelectionChangeCommitted);
			this.cboxCountry.SelectedIndexChanged += new System.EventHandler(this.cboxCountry_SelectionChangeCommitted);
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(5, 101);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(48, 13);
			this.label5.TabIndex = 4;
			this.label5.Text = "Address:";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(5, 76);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(27, 13);
			this.label4.TabIndex = 3;
			this.label4.Text = "City:";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(5, 51);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(46, 13);
			this.label3.TabIndex = 2;
			this.label3.Text = "Country:";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(5, 26);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(84, 13);
			this.label2.TabIndex = 1;
			this.label2.Text = "Installation date:";
			// 
			// dtpickerInstallation
			// 
			this.dtpickerInstallation.Location = new System.Drawing.Point(95, 22);
			this.dtpickerInstallation.Name = "dtpickerInstallation";
			this.dtpickerInstallation.Size = new System.Drawing.Size(193, 20);
			this.dtpickerInstallation.TabIndex = 0;
			// 
			// EnvironmentPage
			// 
			this.AccessibleDescription = "Information for the Player.";
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.CategoryPath = "General\\Environment";
			this.Controls.Add(this.gbRegion);
			this.DisplayName = "Environment";
			this.Location = new System.Drawing.Point(0, 0);
			this.MinimumSize = new System.Drawing.Size(291, 186);
			this.Name = "EnvironmentPage";
			this.Size = new System.Drawing.Size(360, 357);
			this.gbRegion.ResumeLayout(false);
			this.gbRegion.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.GroupBox gbRegion;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.DateTimePicker dtpickerInstallation;
		private System.Windows.Forms.TextBox tbAddress;
		private System.Windows.Forms.ComboBox cboxCity;
		private System.Windows.Forms.ComboBox cboxCountry;
		private System.Windows.Forms.Button btnConfirm;
		private System.Windows.Forms.Label label6;
	}
}
