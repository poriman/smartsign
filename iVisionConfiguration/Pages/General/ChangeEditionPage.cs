﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using DigitalSignage.Common;
using DigitalSignage.SerialKey;

namespace iVisionConfiguration
{
	public partial class ChangeEditionPage : GLib.Options.OptionsPanel, IConfiguration
	{
		KeyChecker checker = KeyChecker.GetInstance;

		public ChangeEditionPage()
		{
			InitializeComponent();
			LoadConfig(AppDomain.CurrentDomain.BaseDirectory);
		}

		public bool LoadConfig(string path)
		{
			bool bRet = false;
			try
			{
				NESerialKey._ProductCode code = checker.CheckProductType();

				tbEditionInfo.Text = String.Format("Product Code:\r\n {0}", SerialKey.ProductName(code));
				bRet = true;
			}
			catch
			{
				
			}
			return bRet;
		}

		public bool SaveConfig(string path)
		{
			return true;
			// 			bool bRet = false;
			// 			return bRet;
		}

		private void btnConfirmKey_Click(object sender, EventArgs e)
		{
			NESerialKey._ProductCode code = KeyChecker.GetInstance.VerifyProductTypeFromKey(tbSerialKey.Text);
			if (code != SerialKey._ProductCode.Undefined)
			{
				DialogResult result =  MessageBox.Show(String.Format("Edition will be changed to '{0}', continue?", SerialKey.ProductName(code)), "", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
				if(result == DialogResult.Yes)
				{
					KeyChecker.GetInstance.SetProductType(code);
					LoadConfig(AppDomain.CurrentDomain.BaseDirectory);
				}
			}
			else MessageBox.Show("This key is invalid.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
		}
	}
}
