﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iVisionConfiguration.Library;
using DigitalSignage.Common;
using System.Xml;

namespace iVisionConfiguration
{
	public partial class EnvironmentPage : GLib.Options.OptionsPanel, IConfiguration
	{
		GatheringSystemInformation systeminfomation = null;
		const string XMLFILENAME = "systeminfo.xml";
		const string XMLFILENAME1 = "config.xml";

		const string XMLFILENAME2 = "Cities.xml";

        const string XMLFILENAME3 = "DS.ScreenCast.exe.config";

		ConfigSettings _conf = null;

		XmlDocument cities = null;
		XmlDocument player_config = null;

		public EnvironmentPage()
		{
			InitializeComponent();

			try
			{
				string settings_path = String.Format(@"{0}Settings\", AppDomain.CurrentDomain.BaseDirectory);

				cities = new XmlDocument();
				cities.Load(settings_path + XMLFILENAME2);

				XmlNodeList list = cities.SelectNodes("//child::Nation");
				foreach (XmlNode node in list)
				{
					cboxCountry.Items.Add(node.Attributes["Value"].Value);
				}
				cboxCountry.SelectedIndex = 0;
			}
			catch {}

			LoadConfig(AppDomain.CurrentDomain.BaseDirectory);

		}


		private void btnConfirm_Click(object sender, EventArgs e)
		{
			SystemInformationViewer viewer = new SystemInformationViewer(systeminfomation);
			viewer.ShowDialog();
		}

		public bool LoadConfig(string path)
		{
			bool bRet = true;

			string app_path = String.Format(@"{0}PlayerData\", AppDomain.CurrentDomain.BaseDirectory);

			try
			{
				_conf = new ConfigSettings(app_path + XMLFILENAME1);
				String datetime = ConfigSettings.ReadSetting("installation_date");

				this.dtpickerInstallation.Value = String.IsNullOrEmpty(datetime) ? DateTime.Now : new DateTime(Convert.ToInt64(datetime));
				String country = ConfigSettings.ReadSetting("country");
				String city = ConfigSettings.ReadSetting("city");
				tbAddress.Text = ConfigSettings.ReadSetting("address");

				try
				{
					cboxCountry.SelectedItem = country;
					cboxCity.SelectedItem = city;
				}
				catch { bRet = bRet || false; }
			}
			catch { bRet = bRet || false; }

			try
			{
				player_config = new XmlDocument();
				player_config.Load(AppDomain.CurrentDomain.BaseDirectory + XMLFILENAME3);

				systeminfomation = new GatheringSystemInformation(AppDomain.CurrentDomain.BaseDirectory + XMLFILENAME);
			}
			catch { bRet = bRet || false; }

			return bRet;
		}

		public bool SaveConfig(string path)
		{
			bool bRet = true;

			string app_path = String.Format(@"{0}PlayerData\", AppDomain.CurrentDomain.BaseDirectory);
			string settings_path = String.Format(@"{0}Settings\", AppDomain.CurrentDomain.BaseDirectory);

			try
			{
				ConfigSettings.WriteSetting("installation_date", this.dtpickerInstallation.Value.Ticks.ToString());
				ConfigSettings.WriteSetting("country", this.cboxCountry.SelectedItem.ToString());
				ConfigSettings.WriteSetting("city", this.cboxCity.SelectedItem.ToString());
				ConfigSettings.WriteSetting("address", this.tbAddress.Text);

				player_config.Load(AppDomain.CurrentDomain.BaseDirectory + XMLFILENAME3);
				XmlNode country_node = player_config.SelectSingleNode("//child::add[attribute::key=\"country\"]");
				if(country_node == null)
				{
					country_node = player_config.CreateNode(XmlNodeType.Element, "add", "");

					XmlAttribute attr1 = player_config.CreateAttribute("key");
					attr1.Value = "country";
					XmlAttribute attr2 = player_config.CreateAttribute("value");
					attr2.Value = this.cboxCountry.SelectedItem.ToString();

					country_node.Attributes.Append(attr1);
					country_node.Attributes.Append(attr2);

					player_config.SelectSingleNode("//child::appSettings").AppendChild(country_node);
				}
				else
					country_node.Attributes["value"].Value = this.cboxCountry.SelectedItem.ToString();

				XmlNode city_node = player_config.SelectSingleNode("//child::add[attribute::key=\"city\"]");
				if (city_node == null)
				{
					city_node = player_config.CreateNode(XmlNodeType.Element, "add", "");

					XmlAttribute attr1 = player_config.CreateAttribute("key");
					attr1.Value = "city";
					XmlAttribute attr2 = player_config.CreateAttribute("value");
					attr2.Value = this.cboxCity.SelectedItem.ToString();

					city_node.Attributes.Append(attr1);
					city_node.Attributes.Append(attr2);

					player_config.SelectSingleNode("//child::appSettings").AppendChild(city_node);
				}
				else
					city_node.Attributes["value"].Value = this.cboxCity.SelectedItem.ToString();

				player_config.Save(AppDomain.CurrentDomain.BaseDirectory + XMLFILENAME3);

			}
			catch { bRet = bRet || false; }

			return true;
		}

		private void cboxCountry_SelectionChangeCommitted(object sender, EventArgs e)
		{
			try
			{
				cboxCity.Items.Clear();
				XmlNode nationNode = cities.SelectSingleNode(String.Format("//child::Nation[attribute::Value=\"{0}\"]", cboxCountry.SelectedItem));
				foreach (XmlNode node in nationNode.ChildNodes)
				{
					cboxCity.Items.Add(node.Attributes["Value"].Value);
				}
				cboxCity.SelectedIndex = 0;
			}
			catch { }
		}
	}
}
