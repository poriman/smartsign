﻿namespace iVisionConfiguration
{
	partial class ChangeEditionPage
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.tbEditionInfo = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.btnConfirmKey = new System.Windows.Forms.Button();
			this.tbSerialKey = new System.Windows.Forms.TextBox();
			this.groupBox2.SuspendLayout();
			this.SuspendLayout();
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.tbEditionInfo);
			this.groupBox2.Controls.Add(this.label2);
			this.groupBox2.Controls.Add(this.btnConfirmKey);
			this.groupBox2.Controls.Add(this.tbSerialKey);
			this.groupBox2.Location = new System.Drawing.Point(3, 3);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(316, 179);
			this.groupBox2.TabIndex = 6;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Edition information";
			// 
			// tbEditionInfo
			// 
			this.tbEditionInfo.Location = new System.Drawing.Point(9, 19);
			this.tbEditionInfo.Multiline = true;
			this.tbEditionInfo.Name = "tbEditionInfo";
			this.tbEditionInfo.ReadOnly = true;
			this.tbEditionInfo.Size = new System.Drawing.Size(301, 111);
			this.tbEditionInfo.TabIndex = 16;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(6, 133);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(82, 13);
			this.label2.TabIndex = 15;
			this.label2.Text = "Change Edition:";
			// 
			// btnConfirmKey
			// 
			this.btnConfirmKey.Location = new System.Drawing.Point(234, 149);
			this.btnConfirmKey.Name = "btnConfirmKey";
			this.btnConfirmKey.Size = new System.Drawing.Size(62, 21);
			this.btnConfirmKey.TabIndex = 14;
			this.btnConfirmKey.Text = "Apply";
			this.btnConfirmKey.UseVisualStyleBackColor = true;
			this.btnConfirmKey.Click += new System.EventHandler(this.btnConfirmKey_Click);
			// 
			// tbSerialKey
			// 
			this.tbSerialKey.Location = new System.Drawing.Point(6, 149);
			this.tbSerialKey.MaxLength = 50;
			this.tbSerialKey.Name = "tbSerialKey";
			this.tbSerialKey.Size = new System.Drawing.Size(222, 20);
			this.tbSerialKey.TabIndex = 13;
			// 
			// ChangeEditionPage
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.CategoryPath = "General\\Edition";
			this.Controls.Add(this.groupBox2);
			this.DisplayName = "Edition";
			this.Location = new System.Drawing.Point(0, 0);
			this.MinimumSize = new System.Drawing.Size(291, 186);
			this.Name = "ChangeEditionPage";
			this.Size = new System.Drawing.Size(360, 357);
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.Button btnConfirmKey;
		private System.Windows.Forms.TextBox tbSerialKey;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox tbEditionInfo;

	}
}
