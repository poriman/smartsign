﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace iVisionConfiguration
{
	public partial class MUpdatePage : GLib.Options.OptionsPanel, IConfiguration
	{
		const string XMLFILENAME3 = "ManagerUpdater.exe.config";

		XmlDocument updater_config = null;

		public MUpdatePage()
		{
			InitializeComponent();
			LoadConfig(AppDomain.CurrentDomain.BaseDirectory);
		}


		public bool LoadConfig(string path)
		{
			bool bRet = true;

			try
			{
				updater_config = new XmlDocument();
				updater_config.Load(AppDomain.CurrentDomain.BaseDirectory + XMLFILENAME3);

				XmlNode node = updater_config.SelectSingleNode("//child::setting[attribute::name=\"UpdateURL\"]");
				XmlNode nodevalue = node.SelectSingleNode("child::value");
				this.tbUpdateURL.Text = nodevalue.ChildNodes[0].Value;
			}
			catch { bRet = bRet || false; }

			return bRet;
		}

		public bool SaveConfig(string path)
		{
			bool bRet = true;

			try
			{
				updater_config = new XmlDocument();
				updater_config.Load(AppDomain.CurrentDomain.BaseDirectory + XMLFILENAME3);

				XmlNode node = updater_config.SelectSingleNode("//child::setting[attribute::name=\"UpdateURL\"]");
				XmlNode nodevalue = node.SelectSingleNode("child::value");

				nodevalue.ChildNodes[0].Value = this.tbUpdateURL.Text;

				updater_config.Save(AppDomain.CurrentDomain.BaseDirectory + XMLFILENAME3);
			}
			catch { bRet = bRet || false; }

			return bRet;
		}

	}
}
