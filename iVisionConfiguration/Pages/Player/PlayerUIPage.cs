﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using DigitalSignage.Common;
using Microsoft.Win32;
using SerialPortController;

namespace iVisionConfiguration.Pages
{
	public partial class PlayerUIPage : GLib.Options.OptionsPanel, IConfiguration
	{
        const string XMLFILENAME1 = "DS.ScreenCast.exe.config";
		XmlDocument player_config = null;
		PlayerSettings player_ui = null;
		const String UPDATER = "DS.Updater";
		
		#region Player UI Settings
		public bool FadeStart
		{
			get
			{
				bool fade = false;
				string val = null;
				val = PlayerSettings.ReadSetting("fade_start");
				try
				{
					bool.TryParse(val, out fade);
					if (String.IsNullOrEmpty(val))
					{
						fade = false;
						PlayerSettings.WriteSetting("fade_start", fade + "");
					}
				}
				catch
				{
				}
				return fade;
			}
			set
			{
				PlayerSettings.WriteSetting("fade_start", value + "");
			}
		}

		public bool FadeEnd
		{
			get
			{
				bool fade = false;
				string val = null;

				val = PlayerSettings.ReadSetting("fade_end");

				try
				{
					bool.TryParse(val, out fade);
					if (String.IsNullOrEmpty(val))
					{
						fade = false;
						PlayerSettings.WriteSetting("fade_end", fade + "");
					}
				}
				catch
				{
				}
				return fade;
			}
			set
			{
				PlayerSettings.WriteSetting("fade_end", value + "");
			}
		}

		public bool IsManualToPlayer
		{
			get
			{
				bool manual = false;
				string val = null;

				val = PlayerSettings.ReadSetting("opt_manual");
				try
				{
					bool.TryParse(val, out manual);
					if (String.IsNullOrEmpty(val))
					{
						manual = false;
						PlayerSettings.WriteSetting("opt_manual", manual + "");
					}
				}
				catch
				{
				}
				return manual;
			}
			set
			{
				PlayerSettings.WriteSetting("opt_manual", value + "");
			}
		}

		public bool ShowWindowEdge
		{
			get
			{
				bool edge = false;
				string val = null;

				val = PlayerSettings.ReadSetting("show_window_edge");

				try
				{
					bool.TryParse(val, out edge);
					if (String.IsNullOrEmpty(val))
					{
						edge = false;
						PlayerSettings.WriteSetting("show_window_edge", edge + "");
					}
				}
				catch
				{
				}
				return edge;
			}
			set
			{
				PlayerSettings.WriteSetting("show_window_edge", value + "");
			}
		}

		public long WindowXPos
		{
			get
			{
				long posX = 0;
				string val = null;

				val = PlayerSettings.ReadSetting("X");
				try
				{
					long.TryParse(val, out posX);
					if (String.IsNullOrEmpty(val))
						PlayerSettings.WriteSetting("X", posX + "");
				}
				catch
				{
				}
				return posX;
			}
			set
			{
				PlayerSettings.WriteSetting("X", value + "");
			}
		}
		public long WindowYPos
		{
			get
			{
				long posY = 0;
				string val = null;

				val = PlayerSettings.ReadSetting("Y");
				try
				{
					long.TryParse(val, out posY);
					if (String.IsNullOrEmpty(val))
						PlayerSettings.WriteSetting("Y", posY + "");
				}
				catch
				{
				}
				return posY;
			}
			set
			{
				PlayerSettings.WriteSetting("Y", value + "");
			}
		}
		public long WindowWidth
		{
			get
			{
				long width = 800;
				string val = null;

				val = PlayerSettings.ReadSetting("width");
				try
				{
					long.TryParse(val, out width);
					if (String.IsNullOrEmpty(val))
					{
						width = 800;
						PlayerSettings.WriteSetting("width", width + "");
					}
				}
				catch
				{
				}
				return width;
			}
			set
			{
				PlayerSettings.WriteSetting("width", value + "");
			}
		}
		public long WindowHeight
		{
			get
			{
				long height = 600;
				string val = null;

				val = PlayerSettings.ReadSetting("height");

				try
				{
					long.TryParse(val, out height);
					if (String.IsNullOrEmpty(val))
					{
						height = 600;
						PlayerSettings.WriteSetting("height", height + "");
					}
				}
				catch
				{
				}
				return height;
			}
			set
			{
				PlayerSettings.WriteSetting("height", value + "");
			}
		}

		public bool WindowTopMost
		{
			get
			{
				bool topmost = false;
				string val = null;

				val = PlayerSettings.ReadSetting("topmost");

				try
				{
					bool.TryParse(val, out topmost);
					if (String.IsNullOrEmpty(val))
					{
						topmost = false;
						PlayerSettings.WriteSetting("topmost", topmost + "");
					}
				}
				catch
				{
				}
				return topmost;
			}
			set
			{
				PlayerSettings.WriteSetting("topmost", value + "");
			}
		}

		public SupportedSerialPortDevice SerialPortDevice
		{
			get
			{
				SupportedSerialPortDevice device = SupportedSerialPortDevice.deviceNone;
				string val = null;
				val = PlayerSettings.ReadSetting("serial_port_device");
				try
				{
					int nDevice = 0;
					if(int.TryParse(val, out nDevice))
					{
						device = (SupportedSerialPortDevice)nDevice;
					}
					
					if (String.IsNullOrEmpty(val))
					{
						device = SupportedSerialPortDevice.deviceNone;
						PlayerSettings.WriteSetting("serial_port_device", ((int)device).ToString());
					}
				}
				catch
				{
				}
				return device;
			}
			set
			{
				PlayerSettings.WriteSetting("serial_port_device", (int)value + "");
			}
		}

		public bool UsingWoL
		{
			get
			{
				bool bValue = false;
				string val = null;
				val = PlayerSettings.ReadSetting("UsingWoL");
				
				try
				{
					if (String.IsNullOrEmpty(val))
					{
						PlayerSettings.WriteSetting("UsingWoL", bValue.ToString());
					}
					else
					{
						bool.TryParse(val, out bValue);
					}
				}
				catch
				{
				}
				return bValue;
			}
			set
			{
				PlayerSettings.WriteSetting("UsingWoL", (bool)value + "");
			}
		}

		public bool UsingAMT
		{
			get
			{
				bool bValue = false;
				string val = null;
				val = PlayerSettings.ReadSetting("UsingAMT");

				try
				{
					if (String.IsNullOrEmpty(val))
					{
						PlayerSettings.WriteSetting("UsingAMT", bValue.ToString());
					}
					else
					{
						bool.TryParse(val, out bValue);
					}
				}
				catch
				{
				}
				return bValue;
			}
			set
			{
				PlayerSettings.WriteSetting("UsingAMT", (bool)value + "");
			}
		}

		public String AuthID
		{
			get
			{
				string val = null;
				val = PlayerSettings.ReadSetting("AMT_AuthID");

				try
				{
					if (String.IsNullOrEmpty(val))
					{
						PlayerSettings.WriteSetting("AMT_AuthID", "admin");
					}
				}
				catch
				{
				}
				return val;
			}
			set
			{
				PlayerSettings.WriteSetting("AMT_AuthID", value);
			}
		}

		public String AuthPass
		{
			get
			{
				string val = null;
				val = PlayerSettings.ReadSetting("AMT_AuthPass");

				try
				{
					if (String.IsNullOrEmpty(val))
					{
						PlayerSettings.WriteSetting("AMT_AuthPass", "Admin12#$");
					}
				}
				catch
				{
				}
				return val;
			}
			set
			{
				PlayerSettings.WriteSetting("AMT_AuthPass", value);
			}
		}
		#endregion

		public PlayerUIPage()
		{
			InitializeComponent();

			player_ui = new PlayerSettings(AppDomain.CurrentDomain.BaseDirectory + "\\PlayerData\\player_ui.xml");

			cboxProvider.Items.Add(SupportedSerialPortDevice.deviceNone);
			cboxProvider.Items.Add(SupportedSerialPortDevice.deviceSindoProjector);
			cboxProvider.Items.Add(SupportedSerialPortDevice.deviceLGDisplay);
			cboxProvider.Items.Add(SupportedSerialPortDevice.deviceSamsungPDP_SPD_50c92hd);
            cboxProvider.Items.Add(SupportedSerialPortDevice.deviceSamsung_570DX);
            cboxProvider.Items.Add(SupportedSerialPortDevice.deviceHyundaiIT);

			cboxProvider.SelectedIndex = 0;

			LoadConfig(AppDomain.CurrentDomain.BaseDirectory);

		}

		public bool LoadConfig(string path)
		{
			bool bRet = false;
			try
			{
				player_config = new XmlDocument();
				player_config.Load(AppDomain.CurrentDomain.BaseDirectory + XMLFILENAME1);

				XmlNode WPFNode = player_config.SelectSingleNode("//child::add[attribute::key='UseWPF']");

				if (WPFNode != null)
				{
					cboxWPF.Checked = Convert.ToBoolean(WPFNode.Attributes["value"].Value);
				}
				else
					cboxWPF.Checked = false;
			}
			catch
			{
				bRet = bRet || false;
			}

			try
			{
				RegistryKey user = Registry.LocalMachine;
				RegistryKey run = user.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run");

				String executed = run.GetValue(UPDATER).ToString();

				cboxAlwaysRun.Checked = System.IO.File.Exists(executed);

				run.Close();
				user.Close();
			}
			catch
			{
				bRet = bRet || false;
			}

			cboxManualSetting.Checked = !this.IsManualToPlayer;
			cboxFadeIn.Checked = this.FadeStart;
			cboxFadeOut.Checked = this.FadeEnd;
			cboxShowEdge.Checked = this.ShowWindowEdge;
			cboxTopMost.Checked = this.WindowTopMost;
			tbLeft.Value = this.WindowXPos;
            tbTop.Value = this.WindowYPos;
            tbWidth.Value = this.WindowWidth;
            tbHeight.Value = this.WindowHeight;

			cboxProvider.SelectedItem = SerialPortDevice;

			cbAMT.Checked = this.UsingAMT;
			cbWoL.Checked = this.UsingWoL;
			tbAuthID.Text = this.AuthID;
			tbAuthPass.Text = this.AuthPass;

			return bRet;
		}

		public bool SaveConfig(string path)
		{
			bool bRet = false;

			try
			{
				player_config = new XmlDocument();
				player_config.Load(AppDomain.CurrentDomain.BaseDirectory + XMLFILENAME1);

				XmlNode WPFNode = player_config.SelectSingleNode("//child::add[attribute::key='UseWPF']");

				if (WPFNode != null)
				{
					WPFNode.Attributes["value"].Value = cboxWPF.Checked.ToString();
					player_config.Save(AppDomain.CurrentDomain.BaseDirectory + XMLFILENAME1);
				}
			}
			catch
			{
				bRet = bRet || false;
			}

			try
			{
				RegistryKey user = Registry.LocalMachine;
				RegistryKey run = user.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);

				if(cboxAlwaysRun.Checked)
				{
					try
					{
                        run.SetValue(UPDATER, AppDomain.CurrentDomain.BaseDirectory + "DS.Updater.exe");
					}
					catch {}
				}
				else
				{
					try
					{
						run.DeleteValue(UPDATER);
					}
					catch { }
					
				}
				run.Close();
				user.Close();
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
				bRet = bRet || false;
			}

			this.IsManualToPlayer = !cboxManualSetting.Checked;
			this.FadeStart = cboxFadeIn.Checked;
			this.FadeEnd = cboxFadeOut.Checked;
			this.ShowWindowEdge = cboxShowEdge.Checked;
			this.WindowTopMost = cboxTopMost.Checked;

			this.WindowXPos = Convert.ToInt64(tbLeft.Value);
            this.WindowYPos = Convert.ToInt64(tbTop.Value);
            this.WindowWidth = Convert.ToInt64(tbWidth.Value);
            this.WindowHeight = Convert.ToInt64(tbHeight.Value);

			this.SerialPortDevice = (SupportedSerialPortDevice)cboxProvider.SelectedItem;

			this.UsingAMT = cbAMT.Checked;
			this.UsingWoL = cbWoL.Checked;
			this.AuthID = tbAuthID.Text;
			this.AuthPass = tbAuthPass.Text;

			return bRet;
		}

		private void cboxManualSetting_CheckStateChanged(object sender, EventArgs e)
		{
			cboxShowEdge.Enabled = tbLeft.Enabled = tbTop.Enabled = tbWidth.Enabled = tbHeight.Enabled = !cboxManualSetting.Checked;
		}

		private void cboxWPF_CheckStateChanged(object sender, EventArgs e)
		{
			cboxFadeIn.Enabled = cboxFadeOut.Enabled = cboxWPF.Checked;

			if(!cboxWPF.Checked)
			{
				cboxFadeIn.Checked = cboxFadeOut.Checked = false;
			}
		}

		private void cbAMT_CheckedChanged(object sender, EventArgs e)
		{
			panelAMT.Visible = cbAMT.Checked;
		}
	}
}
