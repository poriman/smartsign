﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace iVisionConfiguration.Pages
{
	public partial class SubtitleUIPage : GLib.Options.OptionsPanel, IConfiguration
	{
		SubtitleSettings subtitle_ui = null;

		public SubtitleUIPage()
		{
			InitializeComponent();
			subtitle_ui = new SubtitleSettings(AppDomain.CurrentDomain.BaseDirectory + "\\PlayerData\\subtitle_ui.xml");
			
			LoadConfig(AppDomain.CurrentDomain.BaseDirectory);
		}

		#region Subtitle UI Settings
		public long SubSpeed
		{
			get
			{
				long subspeed = 50;
				string val = null;

				val = SubtitleSettings.ReadSetting("speed");
				try
				{
					long.TryParse(val, out subspeed);
					if (String.IsNullOrEmpty(val))
					{
						subspeed = 50;
						SubtitleSettings.WriteSetting("speed", subspeed + "");
					}
				}
				catch
				{
				}
				return subspeed;
			}
			set
			{
				SubtitleSettings.WriteSetting("speed", value + "");
			}
		}
        public bool IsTransparentToSubtitle
        {
            get
            {
                bool bTransparent = false;

                string val = null;

                val = SubtitleSettings.ReadSetting("opt_transparent");
                try
                {
                    bool.TryParse(val, out bTransparent);
                    if (String.IsNullOrEmpty(val))
                    {
                        bTransparent = true;
                        SubtitleSettings.WriteSetting("opt_transparent", bTransparent + "");
                    }
                }
                catch 
                {
                }
                return bTransparent;
            }
            set
            {
                SubtitleSettings.WriteSetting("opt_transparent", value + "");
 
            }
        }
		public bool IsManualToSubtitle
		{
			get
			{
				bool bManual = false;

				string val = null;

				val = SubtitleSettings.ReadSetting("opt_manual");
				try
				{
					bool.TryParse(val, out bManual);
					if (String.IsNullOrEmpty(val))
					{
						bManual = false;
						SubtitleSettings.WriteSetting("opt_manual", bManual + "");
					}
				}
				catch
				{
				}
				return bManual;
			}
			set
			{
				SubtitleSettings.WriteSetting("opt_manual", value + "");

			}
		}
		public long SubPositionY
		{
			get
			{
				long subposY = 550;
				string val = null;

				val = SubtitleSettings.ReadSetting("positionY");
				try
				{
					long.TryParse(val, out subposY);
					if (String.IsNullOrEmpty(val))
					{
						subposY = 550;
						SubtitleSettings.WriteSetting("positionY", subposY + "");
					}
				}
				catch
				{
				}

				return subposY;
			}
			set
			{
				SubtitleSettings.WriteSetting("positionY", value + "");
			}
		}
        public string SubBackcolor
        {
            get
            {
                string subcolor = "#000000";
                string val = null;

               
                val = SubtitleSettings.ReadSetting("background");
                try
                {
                    subcolor = val;
                    if (String.IsNullOrEmpty(val))
                    {
                        subcolor = "#000000";
                        SubtitleSettings.WriteSetting("background", subcolor + "");
                    }
                }
                catch { }

                return subcolor;
            }
            set
            {
                SubtitleSettings.WriteSetting("background", value + "");
            }
        }
		#endregion

		public bool LoadConfig(string path)
		{
			bool bRet = true;
			cboxManualSetting.Checked = this.IsManualToSubtitle;
			tbSpeed.Text = this.SubSpeed.ToString();
			tbTop.Text = this.SubPositionY.ToString();
            btnBackColor.BackColor = ConvertToColor(this.SubBackcolor);
			return bRet;
		}

		public bool SaveConfig(string path)
		{
			bool bRet = true;
			this.IsManualToSubtitle = cboxManualSetting.Checked;
			this.SubSpeed = Convert.ToInt64(tbSpeed.Text);
			this.SubPositionY = Convert.ToInt64(tbTop.Text);
            this.IsTransparentToSubtitle = cbIsTransparent.Checked;
            this.SubBackcolor = ConvertFromColor(btnBackColor.BackColor);
			return bRet;
		}

		private void cboxManualSetting_CheckedChanged(object sender, EventArgs e)
		{
			tbTop.Enabled = cboxManualSetting.Checked;
		}

        private void cbIsTransparent_CheckedChanged(object sender, EventArgs e)
        {
            btnBackColor.Enabled = !cbIsTransparent.Checked;
        }

        private void btnBackColor_Click(object sender, EventArgs e)
        {
            colorPicker.Color = btnBackColor.BackColor;

            if (colorPicker.ShowDialog() == DialogResult.OK)
            {
                btnBackColor.BackColor = colorPicker.Color;
            }
        }

        private Color ConvertToColor(String sColor)
        {
            try
            {
                int red = Convert.ToInt32(sColor.Substring(1, 2), 16);
                int green = Convert.ToInt32(sColor.Substring(3, 2), 16);
                int blue = Convert.ToInt32(sColor.Substring(5, 2), 16);

                return Color.FromArgb(red, green, blue);
            }
            catch { return Color.Black; }
        }

        private String ConvertFromColor(Color col)
        {
            return String.Format("#{0:X2}{1:X2}{2:X2}", col.R, col.G, col.B);
        }

	}
}
