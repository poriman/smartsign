﻿namespace iVisionConfiguration
{
	partial class PUpdatePage
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.tbUpdateURL = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.groupBox2.SuspendLayout();
			this.SuspendLayout();
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.tbUpdateURL);
			this.groupBox2.Controls.Add(this.label2);
			this.groupBox2.Location = new System.Drawing.Point(3, 3);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(316, 64);
			this.groupBox2.TabIndex = 4;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Update Setting";
			// 
			// tbUpdateURL
			// 
			this.tbUpdateURL.Location = new System.Drawing.Point(75, 16);
			this.tbUpdateURL.Multiline = true;
			this.tbUpdateURL.Name = "tbUpdateURL";
			this.tbUpdateURL.Size = new System.Drawing.Size(236, 40);
			this.tbUpdateURL.TabIndex = 1;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(5, 19);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(73, 13);
			this.label2.TabIndex = 0;
			this.label2.Text = "Updater URL:";
			// 
			// PUpdatePage
			// 
			this.AccessibleDescription = "Setting";
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.CategoryPath = "Player\\Update";
			this.Controls.Add(this.groupBox2);
			this.DisplayName = "Update";
			this.Location = new System.Drawing.Point(0, 0);
			this.MinimumSize = new System.Drawing.Size(291, 186);
			this.Name = "PUpdatePage";
			this.Size = new System.Drawing.Size(360, 357);
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.TextBox tbUpdateURL;
		private System.Windows.Forms.Label label2;
	}
}
