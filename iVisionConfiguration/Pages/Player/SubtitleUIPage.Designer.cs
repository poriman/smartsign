﻿namespace iVisionConfiguration.Pages
{
	partial class SubtitleUIPage
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.gbRegion = new System.Windows.Forms.GroupBox();
            this.btnBackColor = new System.Windows.Forms.Button();
            this.cbIsTransparent = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbSpeed = new System.Windows.Forms.NumericUpDown();
            this.tbTop = new System.Windows.Forms.NumericUpDown();
            this.cboxManualSetting = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.colorPicker = new System.Windows.Forms.ColorDialog();
            this.gbRegion.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbSpeed)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTop)).BeginInit();
            this.SuspendLayout();
            // 
            // gbRegion
            // 
            this.gbRegion.Controls.Add(this.btnBackColor);
            this.gbRegion.Controls.Add(this.cbIsTransparent);
            this.gbRegion.Controls.Add(this.label1);
            this.gbRegion.Controls.Add(this.tbSpeed);
            this.gbRegion.Controls.Add(this.tbTop);
            this.gbRegion.Controls.Add(this.cboxManualSetting);
            this.gbRegion.Controls.Add(this.label5);
            this.gbRegion.Controls.Add(this.label4);
            this.gbRegion.Controls.Add(this.label2);
            this.gbRegion.Location = new System.Drawing.Point(3, 3);
            this.gbRegion.Name = "gbRegion";
            this.gbRegion.Size = new System.Drawing.Size(369, 167);
            this.gbRegion.TabIndex = 4;
            this.gbRegion.TabStop = false;
            this.gbRegion.Text = "Subtitle UI";
            // 
            // btnBackColor
            // 
            this.btnBackColor.Location = new System.Drawing.Point(96, 94);
            this.btnBackColor.Name = "btnBackColor";
            this.btnBackColor.Size = new System.Drawing.Size(33, 23);
            this.btnBackColor.TabIndex = 20;
            this.btnBackColor.UseVisualStyleBackColor = true;
            this.btnBackColor.Click += new System.EventHandler(this.btnBackColor_Click);
            // 
            // cbIsTransparent
            // 
            this.cbIsTransparent.AutoSize = true;
            this.cbIsTransparent.Location = new System.Drawing.Point(9, 77);
            this.cbIsTransparent.Name = "cbIsTransparent";
            this.cbIsTransparent.Size = new System.Drawing.Size(231, 16);
            this.cbIsTransparent.TabIndex = 19;
            this.cbIsTransparent.Text = "Make the background to transparent.";
            this.cbIsTransparent.UseVisualStyleBackColor = true;
            this.cbIsTransparent.CheckedChanged += new System.EventHandler(this.cbIsTransparent_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 99);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 12);
            this.label1.TabIndex = 18;
            this.label1.Text = "BackColor:";
            // 
            // tbSpeed
            // 
            this.tbSpeed.Location = new System.Drawing.Point(58, 128);
            this.tbSpeed.Maximum = new decimal(new int[] {
            2000,
            0,
            0,
            0});
            this.tbSpeed.Minimum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.tbSpeed.Name = "tbSpeed";
            this.tbSpeed.Size = new System.Drawing.Size(51, 21);
            this.tbSpeed.TabIndex = 17;
            this.tbSpeed.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            // 
            // tbTop
            // 
            this.tbTop.Enabled = false;
            this.tbTop.Location = new System.Drawing.Point(60, 40);
            this.tbTop.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.tbTop.Name = "tbTop";
            this.tbTop.Size = new System.Drawing.Size(51, 21);
            this.tbTop.TabIndex = 16;
            this.tbTop.Value = new decimal(new int[] {
            550,
            0,
            0,
            0});
            // 
            // cboxManualSetting
            // 
            this.cboxManualSetting.AutoSize = true;
            this.cboxManualSetting.Location = new System.Drawing.Point(9, 19);
            this.cboxManualSetting.Name = "cboxManualSetting";
            this.cboxManualSetting.Size = new System.Drawing.Size(108, 16);
            this.cboxManualSetting.TabIndex = 15;
            this.cboxManualSetting.Text = "Manual Setting";
            this.cboxManualSetting.UseVisualStyleBackColor = true;
            this.cboxManualSetting.CheckedChanged += new System.EventHandler(this.cboxManualSetting_CheckedChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(112, 130);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(113, 12);
            this.label5.TabIndex = 4;
            this.label5.Text = "(Pixel Per Second)";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 130);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 12);
            this.label4.TabIndex = 3;
            this.label4.Text = "Speed:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(23, 42);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 12);
            this.label2.TabIndex = 1;
            this.label2.Text = "Top:";
            // 
            // colorPicker
            // 
            this.colorPicker.SolidColorOnly = true;
            // 
            // SubtitleUIPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.CategoryPath = "Player\\Subtitle";
            this.Controls.Add(this.gbRegion);
            this.DisplayName = "Subtitle";
            this.Location = new System.Drawing.Point(0, 0);
            this.MinimumSize = new System.Drawing.Size(339, 172);
            this.Name = "SubtitleUIPage";
            this.Size = new System.Drawing.Size(420, 330);
            this.gbRegion.ResumeLayout(false);
            this.gbRegion.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbSpeed)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tbTop)).EndInit();
            this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.GroupBox gbRegion;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
		private System.Windows.Forms.CheckBox cboxManualSetting;
        private System.Windows.Forms.NumericUpDown tbTop;
        private System.Windows.Forms.NumericUpDown tbSpeed;
        private System.Windows.Forms.CheckBox cbIsTransparent;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnBackColor;
        private System.Windows.Forms.ColorDialog colorPicker;

	}
}
