﻿namespace iVisionConfiguration.Pages
{
	partial class PlayerUIPage
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.gbRegion = new System.Windows.Forms.GroupBox();
			this.cboxProvider = new System.Windows.Forms.ComboBox();
			this.label1 = new System.Windows.Forms.Label();
			this.tbHeight = new System.Windows.Forms.NumericUpDown();
			this.tbWidth = new System.Windows.Forms.NumericUpDown();
			this.tbTop = new System.Windows.Forms.NumericUpDown();
			this.tbLeft = new System.Windows.Forms.NumericUpDown();
			this.cboxTopMost = new System.Windows.Forms.CheckBox();
			this.cboxWPF = new System.Windows.Forms.CheckBox();
			this.cboxFadeOut = new System.Windows.Forms.CheckBox();
			this.cboxFadeIn = new System.Windows.Forms.CheckBox();
			this.cboxShowEdge = new System.Windows.Forms.CheckBox();
			this.cboxAlwaysRun = new System.Windows.Forms.CheckBox();
			this.cboxManualSetting = new System.Windows.Forms.CheckBox();
			this.label5 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.cbWoL = new System.Windows.Forms.CheckBox();
			this.cbAMT = new System.Windows.Forms.CheckBox();
			this.label6 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.tbAuthID = new System.Windows.Forms.TextBox();
			this.tbAuthPass = new System.Windows.Forms.TextBox();
			this.panelAMT = new System.Windows.Forms.Panel();
			this.gbRegion.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.tbHeight)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.tbWidth)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.tbTop)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.tbLeft)).BeginInit();
			this.groupBox1.SuspendLayout();
			this.panelAMT.SuspendLayout();
			this.SuspendLayout();
			// 
			// gbRegion
			// 
			this.gbRegion.Controls.Add(this.cboxProvider);
			this.gbRegion.Controls.Add(this.label1);
			this.gbRegion.Controls.Add(this.tbHeight);
			this.gbRegion.Controls.Add(this.tbWidth);
			this.gbRegion.Controls.Add(this.tbTop);
			this.gbRegion.Controls.Add(this.tbLeft);
			this.gbRegion.Controls.Add(this.cboxTopMost);
			this.gbRegion.Controls.Add(this.cboxWPF);
			this.gbRegion.Controls.Add(this.cboxFadeOut);
			this.gbRegion.Controls.Add(this.cboxFadeIn);
			this.gbRegion.Controls.Add(this.cboxShowEdge);
			this.gbRegion.Controls.Add(this.cboxAlwaysRun);
			this.gbRegion.Controls.Add(this.cboxManualSetting);
			this.gbRegion.Controls.Add(this.label5);
			this.gbRegion.Controls.Add(this.label4);
			this.gbRegion.Controls.Add(this.label3);
			this.gbRegion.Controls.Add(this.label2);
			this.gbRegion.Location = new System.Drawing.Point(3, 3);
			this.gbRegion.Name = "gbRegion";
			this.gbRegion.Size = new System.Drawing.Size(369, 255);
			this.gbRegion.TabIndex = 4;
			this.gbRegion.TabStop = false;
			this.gbRegion.Text = "Player UI";
			// 
			// cboxProvider
			// 
			this.cboxProvider.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cboxProvider.FormattingEnabled = true;
			this.cboxProvider.Location = new System.Drawing.Point(131, 228);
			this.cboxProvider.Name = "cboxProvider";
			this.cboxProvider.Size = new System.Drawing.Size(221, 20);
			this.cboxProvider.TabIndex = 27;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(5, 231);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(109, 12);
			this.label1.TabIndex = 26;
			this.label1.Text = "Serial Port Device:";
			// 
			// tbHeight
			// 
			this.tbHeight.Enabled = false;
			this.tbHeight.Location = new System.Drawing.Point(180, 166);
			this.tbHeight.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
			this.tbHeight.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.tbHeight.Name = "tbHeight";
			this.tbHeight.Size = new System.Drawing.Size(51, 21);
			this.tbHeight.TabIndex = 25;
			this.tbHeight.Value = new decimal(new int[] {
            600,
            0,
            0,
            0});
			// 
			// tbWidth
			// 
			this.tbWidth.Enabled = false;
			this.tbWidth.Location = new System.Drawing.Point(71, 166);
			this.tbWidth.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
			this.tbWidth.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.tbWidth.Name = "tbWidth";
			this.tbWidth.Size = new System.Drawing.Size(51, 21);
			this.tbWidth.TabIndex = 24;
			this.tbWidth.Value = new decimal(new int[] {
            800,
            0,
            0,
            0});
			// 
			// tbTop
			// 
			this.tbTop.Enabled = false;
			this.tbTop.Location = new System.Drawing.Point(180, 142);
			this.tbTop.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
			this.tbTop.Minimum = new decimal(new int[] {
            99999,
            0,
            0,
            -2147483648});
			this.tbTop.Name = "tbTop";
			this.tbTop.Size = new System.Drawing.Size(51, 21);
			this.tbTop.TabIndex = 23;
			// 
			// tbLeft
			// 
			this.tbLeft.Enabled = false;
			this.tbLeft.Location = new System.Drawing.Point(71, 142);
			this.tbLeft.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
			this.tbLeft.Minimum = new decimal(new int[] {
            99999,
            0,
            0,
            -2147483648});
			this.tbLeft.Name = "tbLeft";
			this.tbLeft.Size = new System.Drawing.Size(51, 21);
			this.tbLeft.TabIndex = 22;
			// 
			// cboxTopMost
			// 
			this.cboxTopMost.AutoSize = true;
			this.cboxTopMost.Location = new System.Drawing.Point(7, 39);
			this.cboxTopMost.Name = "cboxTopMost";
			this.cboxTopMost.Size = new System.Drawing.Size(110, 16);
			this.cboxTopMost.TabIndex = 21;
			this.cboxTopMost.Text = "Always on Top";
			this.cboxTopMost.UseVisualStyleBackColor = true;
			// 
			// cboxWPF
			// 
			this.cboxWPF.AutoSize = true;
			this.cboxWPF.Location = new System.Drawing.Point(7, 60);
			this.cboxWPF.Name = "cboxWPF";
			this.cboxWPF.Size = new System.Drawing.Size(111, 16);
			this.cboxWPF.TabIndex = 20;
			this.cboxWPF.Text = "Use WPF Mode";
			this.cboxWPF.UseVisualStyleBackColor = true;
			this.cboxWPF.CheckStateChanged += new System.EventHandler(this.cboxWPF_CheckStateChanged);
			// 
			// cboxFadeOut
			// 
			this.cboxFadeOut.AutoSize = true;
			this.cboxFadeOut.Enabled = false;
			this.cboxFadeOut.Location = new System.Drawing.Point(24, 102);
			this.cboxFadeOut.Name = "cboxFadeOut";
			this.cboxFadeOut.Size = new System.Drawing.Size(75, 16);
			this.cboxFadeOut.TabIndex = 19;
			this.cboxFadeOut.Text = "Fade Out";
			this.cboxFadeOut.UseVisualStyleBackColor = true;
			// 
			// cboxFadeIn
			// 
			this.cboxFadeIn.AutoSize = true;
			this.cboxFadeIn.Enabled = false;
			this.cboxFadeIn.Location = new System.Drawing.Point(24, 81);
			this.cboxFadeIn.Name = "cboxFadeIn";
			this.cboxFadeIn.Size = new System.Drawing.Size(66, 16);
			this.cboxFadeIn.TabIndex = 18;
			this.cboxFadeIn.Text = "Fade In";
			this.cboxFadeIn.UseVisualStyleBackColor = true;
			// 
			// cboxShowEdge
			// 
			this.cboxShowEdge.AutoSize = true;
			this.cboxShowEdge.Enabled = false;
			this.cboxShowEdge.Location = new System.Drawing.Point(24, 189);
			this.cboxShowEdge.Name = "cboxShowEdge";
			this.cboxShowEdge.Size = new System.Drawing.Size(137, 16);
			this.cboxShowEdge.TabIndex = 17;
			this.cboxShowEdge.Text = "Show Window Edge";
			this.cboxShowEdge.UseVisualStyleBackColor = true;
			// 
			// cboxAlwaysRun
			// 
			this.cboxAlwaysRun.AutoSize = true;
			this.cboxAlwaysRun.Location = new System.Drawing.Point(7, 18);
			this.cboxAlwaysRun.Name = "cboxAlwaysRun";
			this.cboxAlwaysRun.Size = new System.Drawing.Size(227, 16);
			this.cboxAlwaysRun.TabIndex = 17;
			this.cboxAlwaysRun.Text = "Always run when starting windows.";
			this.cboxAlwaysRun.UseVisualStyleBackColor = true;
			// 
			// cboxManualSetting
			// 
			this.cboxManualSetting.AutoSize = true;
			this.cboxManualSetting.Checked = true;
			this.cboxManualSetting.CheckState = System.Windows.Forms.CheckState.Checked;
			this.cboxManualSetting.Location = new System.Drawing.Point(7, 123);
			this.cboxManualSetting.Name = "cboxManualSetting";
			this.cboxManualSetting.Size = new System.Drawing.Size(128, 16);
			this.cboxManualSetting.TabIndex = 15;
			this.cboxManualSetting.Text = "Maximize Window";
			this.cboxManualSetting.UseVisualStyleBackColor = true;
			this.cboxManualSetting.CheckStateChanged += new System.EventHandler(this.cboxManualSetting_CheckStateChanged);
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(129, 168);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(44, 12);
			this.label5.TabIndex = 4;
			this.label5.Text = "Height:";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(21, 168);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(39, 12);
			this.label4.TabIndex = 3;
			this.label4.Text = "Width:";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(129, 145);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(31, 12);
			this.label3.TabIndex = 2;
			this.label3.Text = "Top:";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(21, 145);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(29, 12);
			this.label2.TabIndex = 1;
			this.label2.Text = "Left:";
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.panelAMT);
			this.groupBox1.Controls.Add(this.cbAMT);
			this.groupBox1.Controls.Add(this.cbWoL);
			this.groupBox1.Location = new System.Drawing.Point(3, 264);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(369, 127);
			this.groupBox1.TabIndex = 5;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Power Management";
			// 
			// cbWoL
			// 
			this.cbWoL.AutoSize = true;
			this.cbWoL.Location = new System.Drawing.Point(7, 20);
			this.cbWoL.Name = "cbWoL";
			this.cbWoL.Size = new System.Drawing.Size(140, 16);
			this.cbWoL.TabIndex = 0;
			this.cbWoL.Text = "Using Wake on LAN.";
			this.cbWoL.UseVisualStyleBackColor = true;
			// 
			// cbAMT
			// 
			this.cbAMT.AutoSize = true;
			this.cbAMT.Location = new System.Drawing.Point(7, 42);
			this.cbAMT.Name = "cbAMT";
			this.cbAMT.Size = new System.Drawing.Size(189, 16);
			this.cbAMT.TabIndex = 1;
			this.cbAMT.Text = "Using Intel AMT Technology.";
			this.cbAMT.UseVisualStyleBackColor = true;
			this.cbAMT.CheckedChanged += new System.EventHandler(this.cbAMT_CheckedChanged);
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(0, 4);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(80, 12);
			this.label6.TabIndex = 2;
			this.label6.Text = "AMT Auth ID:";
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(0, 28);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(126, 12);
			this.label7.TabIndex = 3;
			this.label7.Text = "AMT Auth Password:";
			// 
			// tbAuthID
			// 
			this.tbAuthID.Location = new System.Drawing.Point(129, 1);
			this.tbAuthID.Name = "tbAuthID";
			this.tbAuthID.Size = new System.Drawing.Size(100, 21);
			this.tbAuthID.TabIndex = 4;
			this.tbAuthID.Text = "admin";
			// 
			// tbAuthPass
			// 
			this.tbAuthPass.Location = new System.Drawing.Point(129, 25);
			this.tbAuthPass.Name = "tbAuthPass";
			this.tbAuthPass.PasswordChar = '*';
			this.tbAuthPass.Size = new System.Drawing.Size(100, 21);
			this.tbAuthPass.TabIndex = 5;
			this.tbAuthPass.Text = "Admin12#$";
			// 
			// panelAMT
			// 
			this.panelAMT.Controls.Add(this.label6);
			this.panelAMT.Controls.Add(this.tbAuthPass);
			this.panelAMT.Controls.Add(this.label7);
			this.panelAMT.Controls.Add(this.tbAuthID);
			this.panelAMT.Location = new System.Drawing.Point(21, 63);
			this.panelAMT.Name = "panelAMT";
			this.panelAMT.Size = new System.Drawing.Size(247, 57);
			this.panelAMT.TabIndex = 6;
			this.panelAMT.Visible = false;
			// 
			// PlayerUIPage
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.CategoryPath = "Player\\Player UI";
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.gbRegion);
			this.DisplayName = "Player UI";
			this.Location = new System.Drawing.Point(0, 0);
			this.MinimumSize = new System.Drawing.Size(339, 172);
			this.Name = "PlayerUIPage";
			this.Size = new System.Drawing.Size(420, 412);
			this.gbRegion.ResumeLayout(false);
			this.gbRegion.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.tbHeight)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.tbWidth)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.tbTop)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.tbLeft)).EndInit();
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.panelAMT.ResumeLayout(false);
			this.panelAMT.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.GroupBox gbRegion;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
		private System.Windows.Forms.CheckBox cboxManualSetting;
		private System.Windows.Forms.CheckBox cboxShowEdge;
		private System.Windows.Forms.CheckBox cboxFadeOut;
		private System.Windows.Forms.CheckBox cboxFadeIn;
		private System.Windows.Forms.CheckBox cboxWPF;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.CheckBox cboxAlwaysRun;
		private System.Windows.Forms.CheckBox cboxTopMost;
        private System.Windows.Forms.NumericUpDown tbHeight;
        private System.Windows.Forms.NumericUpDown tbWidth;
        private System.Windows.Forms.NumericUpDown tbTop;
        private System.Windows.Forms.NumericUpDown tbLeft;
		private System.Windows.Forms.ComboBox cboxProvider;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.CheckBox cbWoL;
		private System.Windows.Forms.TextBox tbAuthPass;
		private System.Windows.Forms.TextBox tbAuthID;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.CheckBox cbAMT;
		private System.Windows.Forms.Panel panelAMT;

	}
}
