﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace iVisionConfiguration
{
	public partial class ServerPage : GLib.Options.OptionsPanel, IConfiguration
	{
        const string XMLFILENAME1 = "DS.ScreenManagerService.exe.config";
		XmlDocument server_config = null;

		public ServerPage()
		{
			InitializeComponent();
			LoadConfig(AppDomain.CurrentDomain.BaseDirectory);
		}

		public bool LoadConfig(string path)
		{
			bool bRet = false;
			try
			{
				server_config = new XmlDocument();
				server_config.Load(AppDomain.CurrentDomain.BaseDirectory + XMLFILENAME1);

				XmlNodeList settingNodes = server_config.SelectNodes("//child::add");

				foreach(XmlNode node in settingNodes)
				{
					try
					{
						switch (node.Attributes["key"].Value)
						{
							case "webservice_refreshtime":
								tbRefreshRate.Value = Convert.ToInt32(node.Attributes["value"].Value);
								break;
							case "webservice_refdata_list":
								tbWSList.Text = node.Attributes["value"].Value;
								break;
                            case "connection_string":
                                tbConnString.Text = node.Attributes["value"].Value;
                                break;
                            case "log_connection_string":
                                tbLogConnString.Text = node.Attributes["value"].Value;
                                break;
                            case "db_connections":
								tbConnCount.Value = Convert.ToInt32(node.Attributes["value"].Value);
								break;
							case "serial_key":
								tbSerialKey.Text = node.Attributes["value"].Value;
								break;
						}
					}
					catch { }
				}
			}
			catch
			{
				bRet = bRet || false;
			}

			return bRet;
		}

		public bool SaveConfig(string path)
		{
			bool bRet = false;
			try
			{
				server_config = new XmlDocument();
				server_config.Load(AppDomain.CurrentDomain.BaseDirectory + XMLFILENAME1);

				XmlNodeList settingNodes = server_config.SelectNodes("//child::add");

				foreach (XmlNode node in settingNodes)
				{
					try
					{
						switch (node.Attributes["key"].Value)
						{
							case "webservice_refreshtime":
                                node.Attributes["value"].Value = tbRefreshRate.Value.ToString();
								break;
							case "webservice_refdata_list":
								node.Attributes["value"].Value = tbWSList.Text;
								break;
                            case "connection_string":
                                node.Attributes["value"].Value = tbConnString.Text;
                                break;
                            case "log_connection_string":
                                node.Attributes["value"].Value = tbLogConnString.Text;
                                break;
                            case "db_connections":
								node.Attributes["value"].Value = tbConnCount.Value.ToString();
								break;
							case "serial_key":
								node.Attributes["value"].Value = tbSerialKey.Text;
								break;
						}
					}
					catch {}

				}

				server_config.Save(AppDomain.CurrentDomain.BaseDirectory + XMLFILENAME1);

			}
			catch
			{
				bRet = bRet || false;
			}

			return bRet;
		}

		private void cboxManualSetting_CheckedChanged(object sender, EventArgs e)
		{
            tbLogConnString.Enabled = tbConnCount.Enabled = tbConnString.Enabled = !cboxManualSetting.Checked;

			if(cboxManualSetting.Checked)
			{
				tbConnCount.Value = 20;
				tbConnString.Text = "Driver=SQLite3 ODBC Driver;";
                tbLogConnString.Text = "Driver=SQLite3 ODBC Driver;";
			}

		}

		private void btnConfirmKey_Click(object sender, EventArgs e)
		{
			DigitalSignage.SerialKey.NESerialKey key = new DigitalSignage.SerialKey.NESerialKey();
			if (key.ParseSerialKey(tbSerialKey.Text)) MessageBox.Show("This serialkey is valid.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
			else
			{
				MessageBox.Show("This serialkey is invalid.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
				tbSerialKey.Focus();
				tbSerialKey.SelectAll();
			}
		}
	}
}
