﻿namespace iVisionConfiguration
{
	partial class ServerPage
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.gbRegion = new System.Windows.Forms.GroupBox();
            this.tbConnCount = new System.Windows.Forms.NumericUpDown();
            this.cboxManualSetting = new System.Windows.Forms.CheckBox();
            this.tbConnString = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tbRefreshRate = new System.Windows.Forms.NumericUpDown();
            this.tbWSList = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnConfirmKey = new System.Windows.Forms.Button();
            this.tbSerialKey = new System.Windows.Forms.TextBox();
            this.tbLogConnString = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.gbRegion.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbConnCount)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbRefreshRate)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbRegion
            // 
            this.gbRegion.Controls.Add(this.tbLogConnString);
            this.gbRegion.Controls.Add(this.label5);
            this.gbRegion.Controls.Add(this.tbConnCount);
            this.gbRegion.Controls.Add(this.cboxManualSetting);
            this.gbRegion.Controls.Add(this.tbConnString);
            this.gbRegion.Controls.Add(this.label4);
            this.gbRegion.Controls.Add(this.label2);
            this.gbRegion.Location = new System.Drawing.Point(3, 55);
            this.gbRegion.Name = "gbRegion";
            this.gbRegion.Size = new System.Drawing.Size(369, 113);
            this.gbRegion.TabIndex = 4;
            this.gbRegion.TabStop = false;
            this.gbRegion.Text = "Database";
            // 
            // tbConnCount
            // 
            this.tbConnCount.Location = new System.Drawing.Point(142, 38);
            this.tbConnCount.Maximum = new decimal(new int[] {
            2000,
            0,
            0,
            0});
            this.tbConnCount.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.tbConnCount.Name = "tbConnCount";
            this.tbConnCount.Size = new System.Drawing.Size(51, 21);
            this.tbConnCount.TabIndex = 16;
            this.tbConnCount.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
            // 
            // cboxManualSetting
            // 
            this.cboxManualSetting.AutoSize = true;
            this.cboxManualSetting.Location = new System.Drawing.Point(7, 18);
            this.cboxManualSetting.Name = "cboxManualSetting";
            this.cboxManualSetting.Size = new System.Drawing.Size(144, 16);
            this.cboxManualSetting.TabIndex = 15;
            this.cboxManualSetting.Text = "Use default (SQLITE)";
            this.cboxManualSetting.UseVisualStyleBackColor = true;
            this.cboxManualSetting.CheckedChanged += new System.EventHandler(this.cboxManualSetting_CheckedChanged);
            // 
            // tbConnString
            // 
            this.tbConnString.Location = new System.Drawing.Point(142, 60);
            this.tbConnString.MaxLength = 150;
            this.tbConnString.Name = "tbConnString";
            this.tbConnString.Size = new System.Drawing.Size(203, 21);
            this.tbConnString.TabIndex = 13;
            this.tbConnString.Text = "Driver=SQLite3 ODBC Driver;";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(21, 63);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(79, 12);
            this.label4.TabIndex = 3;
            this.label4.Text = "Conn. String:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(21, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(114, 12);
            this.label2.TabIndex = 1;
            this.label2.Text = "Connection Count: ";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tbRefreshRate);
            this.groupBox1.Controls.Add(this.tbWSList);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(3, 174);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(369, 100);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Reference Data";
            // 
            // tbRefreshRate
            // 
            this.tbRefreshRate.Location = new System.Drawing.Point(103, 22);
            this.tbRefreshRate.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.tbRefreshRate.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.tbRefreshRate.Name = "tbRefreshRate";
            this.tbRefreshRate.Size = new System.Drawing.Size(51, 21);
            this.tbRefreshRate.TabIndex = 17;
            this.tbRefreshRate.Value = new decimal(new int[] {
            3600,
            0,
            0,
            0});
            // 
            // tbWSList
            // 
            this.tbWSList.Location = new System.Drawing.Point(103, 44);
            this.tbWSList.MaxLength = 200;
            this.tbWSList.Multiline = true;
            this.tbWSList.Name = "tbWSList";
            this.tbWSList.Size = new System.Drawing.Size(228, 46);
            this.tbWSList.TabIndex = 13;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 12);
            this.label1.TabIndex = 3;
            this.label1.Text = "List:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(85, 12);
            this.label3.TabIndex = 1;
            this.label3.Text = "Refresh Rate: ";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnConfirmKey);
            this.groupBox2.Controls.Add(this.tbSerialKey);
            this.groupBox2.Location = new System.Drawing.Point(3, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(369, 47);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Serial Key";
            // 
            // btnConfirmKey
            // 
            this.btnConfirmKey.Location = new System.Drawing.Point(276, 18);
            this.btnConfirmKey.Name = "btnConfirmKey";
            this.btnConfirmKey.Size = new System.Drawing.Size(72, 19);
            this.btnConfirmKey.TabIndex = 14;
            this.btnConfirmKey.Text = "Confirm";
            this.btnConfirmKey.UseVisualStyleBackColor = true;
            this.btnConfirmKey.Click += new System.EventHandler(this.btnConfirmKey_Click);
            // 
            // tbSerialKey
            // 
            this.tbSerialKey.Location = new System.Drawing.Point(10, 18);
            this.tbSerialKey.MaxLength = 30;
            this.tbSerialKey.Name = "tbSerialKey";
            this.tbSerialKey.Size = new System.Drawing.Size(258, 21);
            this.tbSerialKey.TabIndex = 13;
            // 
            // tbLogConnString
            // 
            this.tbLogConnString.Location = new System.Drawing.Point(142, 82);
            this.tbLogConnString.MaxLength = 150;
            this.tbLogConnString.Name = "tbLogConnString";
            this.tbLogConnString.Size = new System.Drawing.Size(203, 21);
            this.tbLogConnString.TabIndex = 18;
            this.tbLogConnString.Text = "Driver=SQLite3 ODBC Driver;";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(21, 85);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(104, 12);
            this.label5.TabIndex = 17;
            this.label5.Text = "Log Conn. String:";
            // 
            // ServerPage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.CategoryPath = "Server\\General";
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.gbRegion);
            this.DisplayName = "General";
            this.Location = new System.Drawing.Point(0, 0);
            this.MinimumSize = new System.Drawing.Size(339, 172);
            this.Name = "ServerPage";
            this.Size = new System.Drawing.Size(420, 330);
            this.gbRegion.ResumeLayout(false);
            this.gbRegion.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbConnCount)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.tbRefreshRate)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.GroupBox gbRegion;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbConnString;
		private System.Windows.Forms.CheckBox cboxManualSetting;
		private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox tbWSList;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.Button btnConfirmKey;
		private System.Windows.Forms.TextBox tbSerialKey;
        private System.Windows.Forms.NumericUpDown tbConnCount;
        private System.Windows.Forms.NumericUpDown tbRefreshRate;
        private System.Windows.Forms.TextBox tbLogConnString;
        private System.Windows.Forms.Label label5;

	}
}
