﻿namespace iVisionConfiguration.Pages
{
	partial class ContServerPage
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.gbRegion = new System.Windows.Forms.GroupBox();
			this.cboxAlwaysRun = new System.Windows.Forms.CheckBox();
			this.gbRegion.SuspendLayout();
			this.SuspendLayout();
			// 
			// gbRegion
			// 
			this.gbRegion.Controls.Add(this.cboxAlwaysRun);
			this.gbRegion.Location = new System.Drawing.Point(3, 3);
			this.gbRegion.Name = "gbRegion";
			this.gbRegion.Size = new System.Drawing.Size(316, 51);
			this.gbRegion.TabIndex = 4;
			this.gbRegion.TabStop = false;
			this.gbRegion.Text = "Contents Server";
			// 
			// cboxAlwaysRun
			// 
			this.cboxAlwaysRun.AutoSize = true;
			this.cboxAlwaysRun.Location = new System.Drawing.Point(8, 19);
			this.cboxAlwaysRun.Name = "cboxAlwaysRun";
			this.cboxAlwaysRun.Size = new System.Drawing.Size(190, 17);
			this.cboxAlwaysRun.TabIndex = 16;
			this.cboxAlwaysRun.Text = "Always run when starting windows.";
			this.cboxAlwaysRun.UseVisualStyleBackColor = true;
			// 
			// ContServerPage
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.CategoryPath = "Contents Server\\General";
			this.Controls.Add(this.gbRegion);
			this.DisplayName = "General";
			this.Location = new System.Drawing.Point(0, 0);
			this.MinimumSize = new System.Drawing.Size(291, 186);
			this.Name = "ContServerPage";
			this.Size = new System.Drawing.Size(360, 357);
			this.gbRegion.ResumeLayout(false);
			this.gbRegion.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.GroupBox gbRegion;
		private System.Windows.Forms.CheckBox cboxAlwaysRun;

	}
}
