﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Microsoft.Win32;

namespace iVisionConfiguration.Pages
{
	public partial class ContServerPage : GLib.Options.OptionsPanel, IConfiguration
	{
		const String UPDATER = "iVisionServiceFTP";

		public ContServerPage()
		{
			InitializeComponent();
			LoadConfig(AppDomain.CurrentDomain.BaseDirectory);
		}

		public bool LoadConfig(string path)
		{
			bool bRet = true;
			try
			{
				RegistryKey user = Registry.CurrentUser;
				RegistryKey run = user.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run");

				String executed = run.GetValue(UPDATER).ToString();

				cboxAlwaysRun.Checked = System.IO.File.Exists(executed);

				run.Close();
				user.Close();
			}
			catch
			{
				bRet = bRet || false;
			} 
			
			return bRet;
		}

		public bool SaveConfig(string path)
		{
			bool bRet = true;

			try
			{
				RegistryKey user = Registry.CurrentUser;
				RegistryKey run = user.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);

				if (cboxAlwaysRun.Checked)
				{
					try
					{
						run.SetValue(UPDATER, AppDomain.CurrentDomain.BaseDirectory + "SlimFTPd.exe");
					}
					catch { }
				}
				else
				{
					try
					{
						run.DeleteValue(UPDATER);
					}
					catch { }

				}
				run.Close();
				user.Close();
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
				bRet = bRet || false;
			}

			return bRet;
		}
	}
}
