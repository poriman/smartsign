﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace iVisionConfiguration
{
	public partial class ConfigurationLoader : Form
	{
		ConfigurationForm optForm = null;

		public ConfigurationLoader()
		{
			InitializeComponent();
		}


		private void Form1_Load(object sender, EventArgs e)
		{
			if (optForm == null)
			{
				optForm = new ConfigurationForm();
			}

			optForm.ShowDialog(this);
			this.Close();
		}
	}
}
