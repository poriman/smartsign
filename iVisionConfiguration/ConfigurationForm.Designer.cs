﻿namespace iVisionConfiguration
{
	partial class ConfigurationForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ConfigurationForm));
			this.SuspendLayout();
			// 
			// ConfigurationForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(653, 504);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MinimumSize = new System.Drawing.Size(659, 351);
			this.Name = "ConfigurationForm";
			this.OptionDescrDescription = "";
			this.OptionsNoDescription = "";
			this.ShowCategoryDescription = true;
			this.ShowCategoryHeader = true;
			this.ShowIcon = true;
			this.ShowInTaskbar = true;
			this.ShowOptionsDescription = false;
			this.ShowOptionsPanelPath = true;
			this.Text = "DS Screen Config";
			this.OptionsSaved += new System.EventHandler(this.ConfigurationForm_OptionsSaved);
			this.ResumeLayout(false);

		}

		#endregion
	}
}

