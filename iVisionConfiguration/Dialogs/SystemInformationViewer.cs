﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using iVisionConfiguration.Library;
using System.Xml;

namespace iVisionConfiguration
{
	public partial class SystemInformationViewer : Form
	{
		private GatheringSystemInformation systeminfomation = null;

		public SystemInformationViewer(object info)
		{
			InitializeComponent();
			systeminfomation = info as GatheringSystemInformation;
			BindingTreeView(systeminfomation.Document, this.treeSystemInfo.Nodes);
			this.treeSystemInfo.ExpandAll();
		}

		private void btnRefresh_Click(object sender, EventArgs e)
		{
			systeminfomation.Refresh();
			this.treeSystemInfo.Nodes.Clear();
			BindingTreeView(systeminfomation.Document, this.treeSystemInfo.Nodes);
			this.treeSystemInfo.ExpandAll();
		}

		private void BindingTreeView(XmlNode parent_node, TreeNodeCollection collections)
		{
			try
			{
				foreach (XmlNode node in parent_node.ChildNodes)
				{
					TreeNode currNode = new TreeNode((node as XmlText != null) ? node.Value : node.LocalName);
					collections.Add(currNode);
					if (node.HasChildNodes)
					{
						currNode.ForeColor = System.Drawing.Color.Blue;
						BindingTreeView(node, currNode.Nodes);
					}

				}
			}
			catch { }
		}
	}
}
