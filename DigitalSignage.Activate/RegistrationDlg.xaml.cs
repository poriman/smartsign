﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;
using System.Net.NetworkInformation;
using System.Net;
using System.IO;

namespace DigitalSignage.Activate
{
    /// <summary>
    /// RegistrationDlg.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class RegistrationDlg : Window
    {
        private static string regstry = "{4B382C69-2C2D-419e-9718-766FE0FB6B51}";
        public RegistrationDlg()
        {
            InitializeComponent();
        }
        public static bool Registry_YN(out string sSerialKey)
        {
            try
            {
                sSerialKey = String.Empty;

                RegistryKey root = Registry.ClassesRoot;
                RegistryKey clsid = root.OpenSubKey("CLSID", true);
                RegistryKey dest = clsid.OpenSubKey(regstry, true);
                
                if (dest == null)
                {
                    clsid.Close();
                    root.Close();
                    dest.Close();
                    return false;
                }
                else
                {
                    string SK = dest.GetValue("SK").ToString();
                    if (SK == "")
                    {
                        clsid.Close();
                        root.Close();
                        dest.Close();
                        return false;
                    }
                    else
                    {
                        sSerialKey = SK;
                        clsid.Close();
                        root.Close();
                        dest.Close();
                        return true;
                    }
                }
            }catch(Exception ex){
                sSerialKey = String.Empty;
                return false;
            }
        }


        public static string Serialkey_Respone(string sSerialKey)
        {
            string sResponseCD = "999";
            try
            {

                HttpWebRequest request = HttpWebRequest.Create("http://saas.myivision.com:7777/Home/UpdateSerialKey?serialKey=" + sSerialKey) as HttpWebRequest;
                HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                Encoding encoder = Encoding.GetEncoding(response.CharacterSet);

                using (StreamReader reader = new StreamReader(response.GetResponseStream(), encoder))
                {
                    sResponseCD = reader.ReadToEnd();
                }

            }
            catch (Exception e)
            {
            }

            return sResponseCD;
        }
        public static string Serialkey_Respone(string sSerialKey, string name, string macaddr, string ipaddr, string host)
        {
            string sResponseCD = "999";
            try
            {
                
                HttpWebRequest request = HttpWebRequest.Create("http://saas.myivision.com:7777/Home/ActivateKey?serialKey=" + sSerialKey + "&name=" + name + "&macaddr=" + macaddr + "&ipaddr=" + ipaddr + "&host=" + host) as HttpWebRequest;

                HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                Encoding encoder = Encoding.GetEncoding(response.CharacterSet);

                using (StreamReader reader = new StreamReader(response.GetResponseStream(), encoder))
                {
                    sResponseCD = reader.ReadToEnd();

                    if (sResponseCD == "000")
                    {
                        //  레지스트리 등록
                        RegistryKey root = Registry.ClassesRoot;
                        RegistryKey clsid = root.OpenSubKey("CLSID", true);
                        RegistryKey dest = clsid.OpenSubKey(regstry, true);
                        dest.SetValue("SK", sSerialKey);
                        clsid.Close();
                        root.Close();
                        dest.Close();

                    }
                }

                return sResponseCD;
            }catch(Exception e){
                return sResponseCD;
            }
        } 

        private void btn_insert_Click(object sender, RoutedEventArgs e)
        {
            //ip주소
            System.Net.IPHostEntry host = System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName());
            string clientIP = string.Empty;
            for (int i = 0; i < host.AddressList.Length; i++)
            {
                if (host.AddressList[i].AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                {
                    clientIP = host.AddressList[i].ToString();
                }
            }

            //mac 주소
            string MacAddress = "";

            NetworkInterface[] adapters = NetworkInterface.GetAllNetworkInterfaces();
            foreach (NetworkInterface adapter in adapters)
            {
                System.Net.NetworkInformation.PhysicalAddress pa = adapter.GetPhysicalAddress();
                if (pa != null && !pa.ToString().Equals(""))
                {
                    MacAddress = pa.ToString();
                    break;
                }
            }

            //사용자 이름

            string name = txt_name.Text;

            //시리얼키
            string sSerialKey = txt_serialkey.Text;

            // 호스트이름
            string mashinename = Environment.MachineName;

            //갱신
            string sResult = "999";
            if ("000" == (sResult = Serialkey_Respone(sSerialKey, name, MacAddress, clientIP, mashinename)))
            {
                MessageBox.Show("인증이 성공하였습니다.", this.Title, MessageBoxButton.OK, MessageBoxImage.Information);

                this.DialogResult = true;
            }
            else
            {
                string sMessage = String.Empty;

                switch(sResult)
                {
                    case "100":
                        sMessage = "이미 등록된 시리얼 키입니다.";
                        break;
                    case "101":
                        sMessage = "시리얼 키가 유효하지 않습니다.";
                        break;
                    case "999":
                        sMessage = "네트워크에 문제가 있거나 인증 서버에 내부 오류가 발생하여 인증을 할 수 없습니다. 관리자에게 문의하십시오.";
                        break;
                }

                MessageBox.Show(String.Format("인증이 실패하였습니다. \r\n - 실패 이유: {0}", sMessage), this.Title, MessageBoxButton.OK, MessageBoxImage.Error);

            }
        }

        private void btn_cansel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            txt_name.Focus();
        }
    }
}
