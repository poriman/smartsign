﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;

using DigitalSignage.ServerTaskHandler;
using System.IO;
using NLog.Targets;
using NLog;


namespace DigitalSignage.TaskHandlerService
{
	public partial class HandlerService : ServiceBase
	{
		DigitalSignage.ServerTaskHandler.TimerLoop tm = new TimerLoop();

		public HandlerService()
		{
			InitializeComponent();
			InitializeLogger();
		}

		private void InitializeLogger()
		{
			String sExecutingPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
			string appData = sExecutingPath + "\\ServiceData\\";
			if (!Directory.Exists(appData))
				Directory.CreateDirectory(appData);

			try
			{
				// configuring log output
				FileTarget target = new FileTarget();
				target.Layout = "${longdate}\t[${level}]\t[${callsite}]\t${message}";
				//             target.Layout = "${longdate} ${logger} ${message}";
				target.FileName = appData + "TaskHandler.Log.txt";
				target.ArchiveFileName = appData + "archives/Manager.Log.{#####}.txt";
				// 			target.ArchiveAboveSize = 256 * 1024 * 10; // archive files greater than 2560 KB
				target.MaxArchiveFiles = 31;
				target.ArchiveEvery = FileArchivePeriod.Day;// FileTarget.ArchiveEveryMode.Day;
				target.ArchiveNumbering = ArchiveNumberingMode.Sequence;// FileTarget.ArchiveNumberingMode.Sequence;
				// this speeds up things when no other processes are writing to the file
				target.ConcurrentWrites = true;
				NLog.Config.SimpleConfigurator.ConfigureForTargetLogging(target, LogLevel.Debug);

			}
			catch { }
		}

		protected override void OnStart(string[] args)
		{
			if (-1 == tm.Start())
				this.Stop();
		}

		protected override void OnStop()
		{
			tm.Stop();
		}
	}
}
