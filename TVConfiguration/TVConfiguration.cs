﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DirectShowLib;
using TVControl.Library;
using DirectShowLib.BDA;
using System.Xml;
using System.Runtime.InteropServices;
using System.Threading;

namespace TVConfiguration
{
	public partial class TVConfiguration : Form
	{
        const UInt32 INFINITE       = 0xFFFFFFFF;
        const UInt32 WAIT_ABANDONED = 0x00000080;
        const UInt32 WAIT_OBJECT_0  = 0x00000000;
        const UInt32 WAIT_TIMEOUT   = 0x00000102;

        public enum MPEG_STREAM_TYPE
        {
            MPEG2_VIDEO_STREAM  = 0x02,
            H264_VIDEO_STREAM   = 0x1B,
            MPEG1_AUDIO_STREAM  = 0x03,
            MPEG2_AUDIO_STREAM  = 0x04,
            AAC_AUDIO_STREAM    = 0x0F, 
            AC3_AUDIO_STREAM    = 0x81
        };

        public struct channelinfo
        {
            public int channel_number;
            public int MajorCh;
            public int MinorCh;
            public int Source_id;
            public int PcrPID;
            public int VideoPid;
            public int AudioPid;
            public int OtherPid;
            public string channelName;
        };

		DeviceInfo device_info = new DeviceInfo();
		ChannelList _channellist = null;

        IntPtr hPsipEvent = IntPtr.Zero;
        IPsiFilter pPsiFilter;

		public TVConfiguration()
		{
			InitializeComponent();
			this.tvControl1.StatisticsUpdated += new EventHandler(tvControl1_StatisticsUpdated);
		
			string pathToXMLFile = AppDomain.CurrentDomain.BaseDirectory + @"settings\TVSetting.xml";

			if (!device_info.LoadXmlFile(pathToXMLFile))
				throw new Exception("XML 파일을 로드할 수 없습니다: " + pathToXMLFile);

			string pathToChannelXMLFile = AppDomain.CurrentDomain.BaseDirectory + @"settings\ChannelList.xml";

			LoadChannels(pathToChannelXMLFile);

            hPsipEvent = Utils.CreateEvent(IntPtr.Zero, false, false, "PSI Event");
		}

		#region 수동 채널 관련

		public enum TV_type { TV_ANALOGTV, TV_COMPONENT, TV_SVIDEO, TV_ATSC, TV_DVB_T, TV_DVB_S };
		public enum TV_detail_type { _8VSB, _QAM16, _QAM32 };

		private TunerInputType _inputtype = TunerInputType.Antenna;
		private TV_detail_type _detail_type = TV_detail_type._8VSB;
		
		private TV_detail_type TVDetailType
		{
			get
			{
				switch (cbox_TVType.SelectedIndex)
				{
					case 0:
						return TV_detail_type._8VSB;
					case 1:
						return TV_detail_type._QAM16;
					case 2:
						return TV_detail_type._QAM32;
				}
				return TV_detail_type._8VSB;
			}
		}
		private TV_type TVType
		{
			get
			{
				switch (cbox_TVType.SelectedIndex)
				{
					case 0:
						return TV_type.TV_ANALOGTV;
					case 1:
						return TV_type.TV_SVIDEO;
					case 2:
						return TV_type.TV_COMPONENT;
					case 3:
						return TV_type.TV_ATSC;
					case 4:
						return TV_type.TV_DVB_S;
					case 5:
						return TV_type.TV_DVB_T;
				}
				return TV_type.TV_ANALOGTV;
			}
		}

		public ModulationType Modulation
		{
			get
			{
				switch (_detail_type)
				{
					case TV_detail_type._8VSB:
						return ModulationType.Mod8Vsb;
					case TV_detail_type._QAM16:
						return ModulationType.Mod16Qam;
					case TV_detail_type._QAM32:
						return ModulationType.Mod32Qam;
				}
				return ModulationType.ModNotSet;
			}
		}
		private string VideoSource
		{
			get {
				String sName = "Unknown";
				switch (TVType)
				{
					case TV_type.TV_ANALOGTV:
						sName = "Video Tuner";
						break;
					case TV_type.TV_COMPONENT:
						sName = "Video Composite";
						break;
					case TV_type.TV_SVIDEO:
						sName = "Video SVideo";
						break;
				}

				return sName;
			}
		}
		
		private void SaveFilterGraph()
		{
			SaveFileDialog dlg = new SaveFileDialog();

			dlg.AddExtension = true;
			dlg.DefaultExt = "grf";
			dlg.Filter = "Filter Graph(*.grf)|*.grf;";
			if(DialogResult.OK == dlg.ShowDialog())
			{
				this.tvControl1.SaveFilterGraph(dlg.FileName);
			}
		}

		private Channel MakeChannel()
		{
			Channel channelinfo = null;

			switch (TVType)
			{
				case TV_type.TV_ANALOGTV:
					channelinfo = new ChannelAnalogic();
					channelinfo.CountryCode = Convert.ToInt32(this.ud_contury.Value);
					channelinfo.ChannelNumber = Convert.ToInt32(this.ud_channel.Value);
					((ChannelAnalogic)channelinfo).Mode = AMTunerModeType.TV;
					((ChannelAnalogic)channelinfo).InputType = rb_Antenna.Checked ? TunerInputType.Antenna : TunerInputType.Cable;
					((ChannelAnalogic)channelinfo).VideoStandard = AnalogVideoStandard.NTSC_M;
// 					((ChannelAnalogic)channelinfo).VideoCaptureDeviceName = this.WDMdeviceName;
					((ChannelAnalogic)channelinfo).VideoSource = this.VideoSource;
// 					((ChannelAnalogic)channelinfo).AudioCaptureDeviceName = this.AudioDeviceName;
					((ChannelAnalogic)channelinfo).AudioMode = TVAudioMode.Stereo;
					break;
				case TV_type.TV_COMPONENT:
				case TV_type.TV_SVIDEO:
					channelinfo = new ChannelAnalogic();
					channelinfo.CountryCode = Convert.ToInt32(this.ud_contury.Value);
					channelinfo.ChannelNumber = 0;
					((ChannelAnalogic)channelinfo).Mode = AMTunerModeType.Default;
					((ChannelAnalogic)channelinfo).VideoStandard = AnalogVideoStandard.NTSC_M;
// 					((ChannelAnalogic)channelinfo).VideoCaptureDeviceName = this.WDMdeviceName;
// 					((ChannelAnalogic)channelinfo).AudioCaptureDeviceName = this.AudioDeviceName;
					((ChannelAnalogic)channelinfo).VideoSource = this.VideoSource;
					((ChannelAnalogic)channelinfo).InputType = rb_Antenna.Checked ? TunerInputType.Antenna : TunerInputType.Cable;
					((ChannelAnalogic)channelinfo).AudioMode = TVAudioMode.Stereo;
					break;
				case TV_type.TV_ATSC:
					channelinfo = new ChannelATSC();
					channelinfo.CountryCode = Convert.ToInt32(this.ud_contury.Value);
					channelinfo.ChannelNumber = Convert.ToInt32(this.ud_channel.Value);
					((ChannelATSC)channelinfo).Modulation = this.Modulation;

                    ((ChannelATSC)channelinfo).InputType = rb_Antenna.Checked ? TunerInputType.Antenna : TunerInputType.Cable;
                    ((ChannelATSC)channelinfo).AudioPidNum = int.Parse(this.text_Audio.Text);
                    ((ChannelATSC)channelinfo).VideoPidNum = int.Parse(this.text_Video.Text);

// 					((ChannelATSC)channelinfo).TunerDevice = this.BDADeviceName;
					break;
				case TV_type.TV_DVB_S:
					channelinfo = new ChannelDVBS();
					break;
				case TV_type.TV_DVB_T:
					channelinfo = new ChannelDVBT();
					break;
			}
			return channelinfo;
		}
		#endregion
		
		private void btnStop_Click(object sender, EventArgs e)
		{

			this.tvControl1.Stop();
		}

		private void btn_Play_Click(object sender, EventArgs e)
		{
			this.tvControl1.Device = device_info;
			this.tvControl1.ChannelInfo = MakeChannel();

			this.tvControl1.Play();

		}


		private void TVConfiguration_Load(object sender, EventArgs e)
		{
			try
			{
				if (cbox_TVType.Items.Count > 0)
					cbox_TVType.SelectedIndex = 3;

				if (cbox_detail_type.Items.Count > 0)
					cbox_detail_type.SelectedIndex = 0;
			}
			catch { }
		}

		private void btn_tune_Click(object sender, EventArgs e)
		{
			this.tvControl1.ChannelInfo.ChannelNumber = Convert.ToInt32(ud_channel.Value);
			this.tvControl1.SubmitRequest();
		}

		private void btnSetting_Click(object sender, EventArgs e)
		{
			DeviceConfiguration dlg = new DeviceConfiguration();
			dlg.Load(device_info.WDMVideoDevice, device_info.WDMAudioDevice, device_info.BDADevice, device_info.IsVMR9);

			if(dlg.DialogResult == DialogResult.OK)
			{
				device_info.WDMVideoDevice = dlg.WDMVideoDevice;
				device_info.WDMAudioDevice = dlg.WDMAudioDevice;
				device_info.BDADevice = dlg.BDADevice;
				device_info.IsVMR9 = dlg.IsVMR9;

				string pathToXMLFile = AppDomain.CurrentDomain.BaseDirectory + @"settings\TVSetting.xml";
				device_info.SaveXmlFile(pathToXMLFile);
			}


		}

		void tvControl1_StatisticsUpdated(object sender, EventArgs e)
		{
			this.tb_statistics.Text = String.Format("Locked: {0}\r\nPresent: {1}\r\nQuality: {2}\r\nStrength: {3}", this.tvControl1.Looked.ToString(), this.tvControl1.Present.ToString(),
				this.tvControl1.Quality, this.tvControl1.Strength);
		}

		private void btnSearch_Click(object sender, EventArgs e)
		{
			SearchForm dlg = new SearchForm();

			dlg.ChannelChanged += new EventHandler(dlg_ChannelChanged);
			//dlg.ConfirmSignal += new EventHandler(dlg_ConfirmSignal);

			dataGridChannel.Rows.Clear();
			_channellist.Clear();

			dlg.DoModal(true, true, rb_Antenna2.Checked);


		}

		void dlg_ConfirmSignal(object sender, EventArgs e)
		{
			if (this.tvControl1.HasSignal())
			{
				DataGridViewRow row = new DataGridViewRow();
				DataGridViewCell cell1 = new DataGridViewTextBoxCell();
				DataGridViewCell cell2 = new DataGridViewTextBoxCell();
				DataGridViewCell cell3 = new DataGridViewTextBoxCell();
				cell1.Value = this.tvControl1.ChannelInfo.ChannelNumber;
				cell2.Value = this.tvControl1.ChannelInfo.Name;
				cell3.Value = this.tvControl1.ChannelInfo.GetType().Name;
				row.Cells.Add(cell1);
				row.Cells.Add(cell2);
				row.Cells.Add(cell3);
				this.dataGridChannel.Rows.Add(row);

				_channellist.AddChannel((Channel)this.tvControl1.ChannelInfo.Clone());

			}
		}

		void dlg_ChannelChanged(object sender, EventArgs e)
		{
            int WaitRet = 0;
			Channel ch = sender as Channel;
            //if ((this.tvControl1.ChannelInfo != null) && (this.tvControl1.ChannelInfo.GetType() == ch.GetType()))
            //{
            //    this.tvControl1.ChannelInfo.Name = ch.Name;
            //    this.tvControl1.ChannelInfo.ChannelNumber = ch.ChannelNumber;
            //    this.tvControl1.SubmitRequest();
            //}
            //else
            //{
            //    this.tvControl1.Device = device_info;
            //    this.tvControl1.ChannelInfo = (Channel)ch.Clone();
            //    this.tvControl1.Play();

            //}
            this.tvControl1.Device = device_info;            
            this.tvControl1.ChannelInfo = ch;
            this.tvControl1.Play();
            this.tvControl1.PSIMODE = true;

            pPsiFilter = this.tvControl1.GetPsiFilter();
            if (pPsiFilter == null)
                return;

            pPsiFilter.SetProgNameEventHandle(hPsipEvent);

            IAMTVTuner tuner = this.tvControl1.GetAMTVTuner();
            if (tuner == null)
                return;
            AMTunerSignalStrength signal = AMTunerSignalStrength.NoSignal;
            Thread.Sleep(3000);
            int ret = tuner.SignalPresent(out signal);
            if (signal != AMTunerSignalStrength.NoSignal)
            {
                WaitRet = Utils.WaitForSingleObject(hPsipEvent, 3000);
                //if (WaitRet == WAIT_OBJECT_0)
                {
                    BuildChannelItem(ch.ChannelNumber);
                }
            }

            this.tvControl1.Stop();
		}

        private void SetChannelInfo(channelinfo info)
        {
            DataGridViewRow row = new DataGridViewRow();
            DataGridViewCell cell1 = new DataGridViewTextBoxCell();
            DataGridViewCell cell2 = new DataGridViewTextBoxCell();
            DataGridViewCell cell3 = new DataGridViewTextBoxCell();
            DataGridViewCell cell4 = new DataGridViewTextBoxCell();
            cell1.Value = info.channel_number;
            cell2.Value = info.channelName;
            cell3.Value = info.VideoPid;
            cell4.Value = info.AudioPid;
            row.Cells.Add(cell1);
            row.Cells.Add(cell2);
            row.Cells.Add(cell3);
            row.Cells.Add(cell4);
            this.dataGridChannel.Rows.Add(row);
            Channel channel = (Channel)this.tvControl1.ChannelInfo.Clone();
            channel.ChannelNumber = info.channel_number;
            channel.Name = info.channelName;
            channel.VideoPidNum = info.VideoPid;
            channel.AudioPidNum = info.AudioPid;

            _channellist.AddChannel(channel);
        }

        private int BuildChannelItem(int ichannel)
        {
            try
            {
                int proCnt;
                pPsiFilter.GetProgramCount(out proCnt);

                int Program_number = 0;
                System.Text.StringBuilder short_name = new StringBuilder(32);

                int MajorCh = 0;
                int MinorCh = 0;
                int elementary_stream_count = 0;
                int source_id = 0;
                int PcrPID = 0;

                for (int ProgIndex = 0; ProgIndex < proCnt; ProgIndex++)
                {
                    pPsiFilter.GetProgramNumber(ProgIndex, out Program_number);
                    pPsiFilter.GetProgramInfo(Program_number, short_name, out MajorCh, out MinorCh, out elementary_stream_count, out source_id, out PcrPID);

                    if (MajorCh >= 1000)
                    {
                        MajorCh = ((MajorCh & 0x0f) << 10) + MinorCh;
                        MinorCh = 0;
                    }

                    if (MajorCh == 0)
                    {
                        MajorCh = ichannel;
                        MinorCh = ProgIndex + 1;
                    }
                    channelinfo info = new channelinfo();
                    info.MajorCh = MajorCh;
                    info.MinorCh = MinorCh;
                    info.Source_id = source_id;
                    info.PcrPID = PcrPID;

                    Byte[] test = System.Text.Encoding.Unicode.GetBytes(short_name.ToString());
                    string m_receive = System.Text.Encoding.UTF8.GetString(test);
                    int index = m_receive.IndexOf('\\');
                    string value = "";
                    for (int i = 0; i< m_receive.Length; i++)
                    {
                        if(m_receive[i] != 0)
                        value += m_receive[i];
                    }

                    info.channelName = value;
                    info.channel_number = ichannel;

                    int videocount = 0;
                    int audiocount = 0;
                    int othercount = 0;

                    System.Text.StringBuilder codestring = new StringBuilder(4);
                    bool isVidio = false;
                    bool isAudeo = false;

                    for (int StreamIndex = 0; StreamIndex < elementary_stream_count; StreamIndex++)
                    {
                        int type = 0;
                        int PID = 0;
                        int scrambled = 0;

                        pPsiFilter.GetElementaryPID(Program_number, StreamIndex, out type, out PID, codestring, out scrambled);

                        if (type == (int)MPEG_STREAM_TYPE.MPEG2_VIDEO_STREAM || type == (int)MPEG_STREAM_TYPE.H264_VIDEO_STREAM)
                        {
                            if (videocount == 0)
                            {
                                info.VideoPid = PID;
                                isVidio = true;
                                videocount++;
                            }
                        }
                        else if (type == (int)MPEG_STREAM_TYPE.MPEG1_AUDIO_STREAM ||
                                 type == (int)MPEG_STREAM_TYPE.MPEG2_AUDIO_STREAM ||
                                 type == (int)MPEG_STREAM_TYPE.AAC_AUDIO_STREAM ||
                                 type == (int)MPEG_STREAM_TYPE.AC3_AUDIO_STREAM)
                        {
                            info.AudioPid = PID;
                            isAudeo = true;
                            audiocount++;
                        }
                        else
                        {
                            info.OtherPid = PID;
                            othercount++;
                        }

                    }

                    if (isVidio && isAudeo)
                    {
                        SetChannelInfo(info);
                    }
                }

            }
            catch (Exception ex)
            {
               // MessageBox.Show(ex.Message.ToString());
            }
            return 0;
        }

        private void ResetChannelStatus()
        {
            if (hPsipEvent != null)
            {
                Utils.ResetEvent(hPsipEvent);
            }
            if (pPsiFilter != null)
            {
                pPsiFilter.ResetParsing();
            }
        }
		private void btnSave_Click(object sender, EventArgs e)
		{
			string pathToXMLFile = AppDomain.CurrentDomain.BaseDirectory + @"settings\ChannelList.xml";

			SaveChannels(pathToXMLFile);
		}

		private void SaveChannels(string filepath)
		{
			this._channellist.Save();
		}
		private void LoadChannels(string filepath)
		{
			_channellist = new ChannelList(filepath);
			foreach (Channel ch in _channellist.GetChannels())
			{
				DataGridViewRow row = new DataGridViewRow();
				DataGridViewCell cell1 = new DataGridViewTextBoxCell();
				DataGridViewCell cell2 = new DataGridViewTextBoxCell();
				DataGridViewCell cell3 = new DataGridViewTextBoxCell();
                DataGridViewCell cell4 = new DataGridViewTextBoxCell();
				cell1.Value = ch.ChannelNumber;
				cell2.Value = ch.Name;
                cell3.Value = ch.VideoPidNum;
                cell4.Value = ch.AudioPidNum;
				row.Cells.Add(cell1);
				row.Cells.Add(cell2);
				row.Cells.Add(cell3);
                row.Cells.Add(cell4);
				this.dataGridChannel.Rows.Add(row);
			}
		}

		private void dataGridChannel_CellEndEdit(object sender, DataGridViewCellEventArgs e)
		{
			_channellist.UpdateChannel(e.RowIndex, dataGridChannel.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());
		}

		private void dataGridChannel_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
		{
			//dlg_ChannelChanged((object)_channellist.GetChannel(e.RowIndex), null);
            PlayTV(_channellist.GetChannel(e.RowIndex));
		}

        private void PlayTV(Channel channel)
        {
            this.tvControl1.Device = device_info;
            this.tvControl1.ChannelInfo = (Channel)channel.Clone();
            this.tvControl1.Play();
        }

		private void btnSaveFilter_Click(object sender, EventArgs e)
		{
			this.SaveFilterGraph();
		}

        private void ud_channel_ValueChanged(object sender, EventArgs e)
        {

        }
	}

    public class Utils
    {
        [DllImport("user32.dll")]
        public static extern IntPtr GetCapture();

        [DllImport("kernel32.dll")]
        public static extern IntPtr CreateEvent(IntPtr lpEventAttributes, bool bManualReset, bool bInitialState, string lpName);

        [DllImport("kernel32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool CloseHandle(IntPtr hObject);

        [DllImport("kernel32.dll")]
        public static extern bool SetEvent(IntPtr hEvent);


        [DllImport("kernel32.dll")]
        public static extern bool ResetEvent(IntPtr hEvent);

        [DllImport("kernel32.dll")]
        public static extern bool PulseEvent(IntPtr hEvent);

        [DllImport("kernel32", SetLastError = true, ExactSpelling = true)]
        internal static extern Int32 WaitForSingleObject(IntPtr handle, Int32 milliseconds);
    }
}
