﻿namespace TVConfiguration
{
	partial class DeviceConfiguration
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DeviceConfiguration));
			this.groupBox4 = new System.Windows.Forms.GroupBox();
			this.label8 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.cbox_soundDevice = new System.Windows.Forms.ComboBox();
			this.cbox_WDMDevice = new System.Windows.Forms.ComboBox();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.label1 = new System.Windows.Forms.Label();
			this.cbox_device = new System.Windows.Forms.ComboBox();
			this.btnOk = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.cb_isVMR9 = new System.Windows.Forms.CheckBox();
			this.label5 = new System.Windows.Forms.Label();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.groupBox4.SuspendLayout();
			this.groupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.SuspendLayout();
			// 
			// groupBox4
			// 
			this.groupBox4.Controls.Add(this.label8);
			this.groupBox4.Controls.Add(this.label9);
			this.groupBox4.Controls.Add(this.cbox_soundDevice);
			this.groupBox4.Controls.Add(this.cbox_WDMDevice);
			this.groupBox4.Location = new System.Drawing.Point(13, 80);
			this.groupBox4.Name = "groupBox4";
			this.groupBox4.Size = new System.Drawing.Size(304, 119);
			this.groupBox4.TabIndex = 11;
			this.groupBox4.TabStop = false;
			this.groupBox4.Text = "WDM 관련";
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(6, 65);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(69, 12);
			this.label8.TabIndex = 8;
			this.label8.Text = "사운드 장치";
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Location = new System.Drawing.Point(6, 21);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(57, 12);
			this.label9.TabIndex = 7;
			this.label9.Text = "튜너 장치";
			// 
			// cbox_soundDevice
			// 
			this.cbox_soundDevice.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbox_soundDevice.FormattingEnabled = true;
			this.cbox_soundDevice.Location = new System.Drawing.Point(8, 80);
			this.cbox_soundDevice.Name = "cbox_soundDevice";
			this.cbox_soundDevice.Size = new System.Drawing.Size(290, 20);
			this.cbox_soundDevice.TabIndex = 6;
			// 
			// cbox_WDMDevice
			// 
			this.cbox_WDMDevice.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbox_WDMDevice.FormattingEnabled = true;
			this.cbox_WDMDevice.Location = new System.Drawing.Point(8, 36);
			this.cbox_WDMDevice.Name = "cbox_WDMDevice";
			this.cbox_WDMDevice.Size = new System.Drawing.Size(290, 20);
			this.cbox_WDMDevice.TabIndex = 3;
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Controls.Add(this.cbox_device);
			this.groupBox1.Location = new System.Drawing.Point(13, 205);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(304, 73);
			this.groupBox1.TabIndex = 12;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "BDA 관련";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(6, 21);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(29, 12);
			this.label1.TabIndex = 7;
			this.label1.Text = "장치";
			// 
			// cbox_device
			// 
			this.cbox_device.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cbox_device.FormattingEnabled = true;
			this.cbox_device.Location = new System.Drawing.Point(8, 36);
			this.cbox_device.Name = "cbox_device";
			this.cbox_device.Size = new System.Drawing.Size(290, 20);
			this.cbox_device.TabIndex = 3;
			// 
			// btnOk
			// 
			this.btnOk.Location = new System.Drawing.Point(118, 284);
			this.btnOk.Name = "btnOk";
			this.btnOk.Size = new System.Drawing.Size(97, 23);
			this.btnOk.TabIndex = 13;
			this.btnOk.Text = "Ok";
			this.btnOk.UseVisualStyleBackColor = true;
			this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.Location = new System.Drawing.Point(222, 284);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(97, 23);
			this.btnCancel.TabIndex = 14;
			this.btnCancel.Text = "Cancel";
			this.btnCancel.UseVisualStyleBackColor = true;
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// cb_isVMR9
			// 
			this.cb_isVMR9.AutoSize = true;
			this.cb_isVMR9.Checked = true;
			this.cb_isVMR9.CheckState = System.Windows.Forms.CheckState.Checked;
			this.cb_isVMR9.Location = new System.Drawing.Point(8, 37);
			this.cb_isVMR9.Name = "cb_isVMR9";
			this.cb_isVMR9.Size = new System.Drawing.Size(91, 16);
			this.cb_isVMR9.TabIndex = 16;
			this.cb_isVMR9.Text = "VMR-9 사용";
			this.cb_isVMR9.UseVisualStyleBackColor = true;
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(6, 17);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(77, 12);
			this.label5.TabIndex = 15;
			this.label5.Text = "비디오렌더러";
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.label5);
			this.groupBox2.Controls.Add(this.cb_isVMR9);
			this.groupBox2.Location = new System.Drawing.Point(12, 12);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(305, 62);
			this.groupBox2.TabIndex = 17;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "일반";
			// 
			// DeviceConfiguration
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(329, 315);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.btnOk);
			this.Controls.Add(this.btnCancel);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.groupBox4);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "DeviceConfiguration";
			this.Text = "Device Configuration";
			this.groupBox4.ResumeLayout(false);
			this.groupBox4.PerformLayout();
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.GroupBox groupBox4;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.ComboBox cbox_soundDevice;
		private System.Windows.Forms.ComboBox cbox_WDMDevice;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.ComboBox cbox_device;
		private System.Windows.Forms.Button btnOk;
		private System.Windows.Forms.Button btnCancel;
		private System.Windows.Forms.CheckBox cb_isVMR9;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.GroupBox groupBox2;
	}
}