﻿namespace TVConfiguration
{
	partial class TVConfiguration
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TVConfiguration));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.tvControl1 = new TVControl.TVControl();
            this.btnSaveFilter = new System.Windows.Forms.Button();
            this.btnSetting = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.text_Audio = new System.Windows.Forms.TextBox();
            this.text_Video = new System.Windows.Forms.TextBox();
            this.rb_cable = new System.Windows.Forms.RadioButton();
            this.rb_Antenna = new System.Windows.Forms.RadioButton();
            this.label2 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.cbox_detail_type = new System.Windows.Forms.ComboBox();
            this.ud_contury = new System.Windows.Forms.NumericUpDown();
            this.btn_tune = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.ud_channel = new System.Windows.Forms.NumericUpDown();
            this.cbox_TVType = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btn_Play = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.rb_Cable2 = new System.Windows.Forms.RadioButton();
            this.rb_Antenna2 = new System.Windows.Forms.RadioButton();
            this.btnSave = new System.Windows.Forms.Button();
            this.dataGridChannel = new System.Windows.Forms.DataGridView();
            this.colChannel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.aupid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnSearch = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tb_statistics = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ud_contury)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ud_channel)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridChannel)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.tvControl1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.btnSaveFilter);
            this.splitContainer1.Panel2.Controls.Add(this.btnSetting);
            this.splitContainer1.Panel2.Controls.Add(this.tabControl1);
            this.splitContainer1.Panel2.Controls.Add(this.groupBox3);
            this.splitContainer1.Size = new System.Drawing.Size(853, 517);
            this.splitContainer1.SplitterDistance = 603;
            this.splitContainer1.TabIndex = 0;
            // 
            // tvControl1
            // 
            this.tvControl1.BackColor = System.Drawing.Color.Black;
            this.tvControl1.ChannelInfo = null;
            this.tvControl1.Device = null;
            this.tvControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tvControl1.Location = new System.Drawing.Point(0, 0);
            this.tvControl1.Name = "tvControl1";
            this.tvControl1.Size = new System.Drawing.Size(603, 517);
            this.tvControl1.TabIndex = 0;
            // 
            // btnSaveFilter
            // 
            this.btnSaveFilter.Location = new System.Drawing.Point(13, 479);
            this.btnSaveFilter.Name = "btnSaveFilter";
            this.btnSaveFilter.Size = new System.Drawing.Size(75, 23);
            this.btnSaveFilter.TabIndex = 15;
            this.btnSaveFilter.Text = "GRF 저장";
            this.btnSaveFilter.UseVisualStyleBackColor = true;
            this.btnSaveFilter.Click += new System.EventHandler(this.btnSaveFilter_Click);
            // 
            // btnSetting
            // 
            this.btnSetting.Location = new System.Drawing.Point(133, 479);
            this.btnSetting.Name = "btnSetting";
            this.btnSetting.Size = new System.Drawing.Size(73, 23);
            this.btnSetting.TabIndex = 14;
            this.btnSetting.Text = "설정...";
            this.btnSetting.UseVisualStyleBackColor = true;
            this.btnSetting.Click += new System.EventHandler(this.btnSetting_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(246, 345);
            this.tabControl1.TabIndex = 13;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.flowLayoutPanel1);
            this.tabPage1.Location = new System.Drawing.Point(4, 21);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(238, 320);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "수동 채널";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.groupBox2);
            this.flowLayoutPanel1.Controls.Add(this.btn_Play);
            this.flowLayoutPanel1.Controls.Add(this.btnStop);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(6, 6);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(207, 307);
            this.flowLayoutPanel1.TabIndex = 7;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.text_Audio);
            this.groupBox2.Controls.Add(this.text_Video);
            this.groupBox2.Controls.Add(this.rb_cable);
            this.groupBox2.Controls.Add(this.rb_Antenna);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.cbox_detail_type);
            this.groupBox2.Controls.Add(this.ud_contury);
            this.groupBox2.Controls.Add(this.btn_tune);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.ud_channel);
            this.groupBox2.Controls.Add(this.cbox_TVType);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox2.Location = new System.Drawing.Point(3, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(200, 264);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "채널 정보";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(4, 213);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 12);
            this.label5.TabIndex = 18;
            this.label5.Text = "오디오 PID";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(4, 188);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 12);
            this.label1.TabIndex = 17;
            this.label1.Text = "비디오 PID";
            // 
            // text_Audio
            // 
            this.text_Audio.Location = new System.Drawing.Point(72, 209);
            this.text_Audio.Name = "text_Audio";
            this.text_Audio.Size = new System.Drawing.Size(74, 21);
            this.text_Audio.TabIndex = 16;
            this.text_Audio.Text = "20";
            // 
            // text_Video
            // 
            this.text_Video.Location = new System.Drawing.Point(72, 182);
            this.text_Video.Name = "text_Video";
            this.text_Video.Size = new System.Drawing.Size(74, 21);
            this.text_Video.TabIndex = 15;
            this.text_Video.Text = "17";
            // 
            // rb_cable
            // 
            this.rb_cable.AutoSize = true;
            this.rb_cable.Location = new System.Drawing.Point(130, 112);
            this.rb_cable.Name = "rb_cable";
            this.rb_cable.Size = new System.Drawing.Size(59, 16);
            this.rb_cable.TabIndex = 14;
            this.rb_cable.Text = "케이블";
            this.rb_cable.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.rb_cable.UseVisualStyleBackColor = true;
            // 
            // rb_Antenna
            // 
            this.rb_Antenna.AutoSize = true;
            this.rb_Antenna.Checked = true;
            this.rb_Antenna.Location = new System.Drawing.Point(130, 90);
            this.rb_Antenna.Name = "rb_Antenna";
            this.rb_Antenna.Size = new System.Drawing.Size(59, 16);
            this.rb_Antenna.TabIndex = 13;
            this.rb_Antenna.TabStop = true;
            this.rb_Antenna.Text = "안테나";
            this.rb_Antenna.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.rb_Antenna.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 12);
            this.label2.TabIndex = 8;
            this.label2.Text = "Modulation";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 83);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(57, 12);
            this.label7.TabIndex = 12;
            this.label7.Text = "국가 코드";
            // 
            // cbox_detail_type
            // 
            this.cbox_detail_type.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbox_detail_type.FormattingEnabled = true;
            this.cbox_detail_type.Items.AddRange(new object[] {
            "8VSB",
            "16QAM",
            "32QAM"});
            this.cbox_detail_type.Location = new System.Drawing.Point(79, 57);
            this.cbox_detail_type.Name = "cbox_detail_type";
            this.cbox_detail_type.Size = new System.Drawing.Size(110, 20);
            this.cbox_detail_type.TabIndex = 6;
            // 
            // ud_contury
            // 
            this.ud_contury.Location = new System.Drawing.Point(8, 98);
            this.ud_contury.Name = "ud_contury";
            this.ud_contury.Size = new System.Drawing.Size(55, 21);
            this.ud_contury.TabIndex = 11;
            this.ud_contury.Value = new decimal(new int[] {
            82,
            0,
            0,
            0});
            // 
            // btn_tune
            // 
            this.btn_tune.Location = new System.Drawing.Point(114, 134);
            this.btn_tune.Name = "btn_tune";
            this.btn_tune.Size = new System.Drawing.Size(75, 23);
            this.btn_tune.TabIndex = 5;
            this.btn_tune.Text = "채널 변경";
            this.btn_tune.UseVisualStyleBackColor = true;
            this.btn_tune.Click += new System.EventHandler(this.btn_tune_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 122);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 12);
            this.label4.TabIndex = 10;
            this.label4.Text = "채널";
            // 
            // ud_channel
            // 
            this.ud_channel.Location = new System.Drawing.Point(8, 137);
            this.ud_channel.Name = "ud_channel";
            this.ud_channel.Size = new System.Drawing.Size(55, 21);
            this.ud_channel.TabIndex = 4;
            this.ud_channel.Value = new decimal(new int[] {
            61,
            0,
            0,
            0});
            this.ud_channel.ValueChanged += new System.EventHandler(this.ud_channel_ValueChanged);
            // 
            // cbox_TVType
            // 
            this.cbox_TVType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbox_TVType.FormattingEnabled = true;
            this.cbox_TVType.Items.AddRange(new object[] {
            "Analog TV",
            "SVideo",
            "Component",
            "ATSC",
            "DVB_T",
            "DVB_S"});
            this.cbox_TVType.Location = new System.Drawing.Point(8, 32);
            this.cbox_TVType.Name = "cbox_TVType";
            this.cbox_TVType.Size = new System.Drawing.Size(181, 20);
            this.cbox_TVType.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 17);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 12);
            this.label3.TabIndex = 9;
            this.label3.Text = "튜너 타입";
            // 
            // btn_Play
            // 
            this.btn_Play.Location = new System.Drawing.Point(3, 273);
            this.btn_Play.Name = "btn_Play";
            this.btn_Play.Size = new System.Drawing.Size(73, 23);
            this.btn_Play.TabIndex = 1;
            this.btn_Play.Text = "시작";
            this.btn_Play.UseVisualStyleBackColor = true;
            this.btn_Play.Click += new System.EventHandler(this.btn_Play_Click);
            // 
            // btnStop
            // 
            this.btnStop.Location = new System.Drawing.Point(82, 273);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(73, 23);
            this.btnStop.TabIndex = 2;
            this.btnStop.Text = "종료";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.rb_Cable2);
            this.tabPage2.Controls.Add(this.rb_Antenna2);
            this.tabPage2.Controls.Add(this.btnSave);
            this.tabPage2.Controls.Add(this.dataGridChannel);
            this.tabPage2.Controls.Add(this.btnSearch);
            this.tabPage2.Location = new System.Drawing.Point(4, 21);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(238, 320);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "채널 검색";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // rb_Cable2
            // 
            this.rb_Cable2.AutoSize = true;
            this.rb_Cable2.Location = new System.Drawing.Point(71, 261);
            this.rb_Cable2.Name = "rb_Cable2";
            this.rb_Cable2.Size = new System.Drawing.Size(59, 16);
            this.rb_Cable2.TabIndex = 16;
            this.rb_Cable2.Text = "케이블";
            this.rb_Cable2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.rb_Cable2.UseVisualStyleBackColor = true;
            // 
            // rb_Antenna2
            // 
            this.rb_Antenna2.AutoSize = true;
            this.rb_Antenna2.Checked = true;
            this.rb_Antenna2.Location = new System.Drawing.Point(6, 261);
            this.rb_Antenna2.Name = "rb_Antenna2";
            this.rb_Antenna2.Size = new System.Drawing.Size(59, 16);
            this.rb_Antenna2.TabIndex = 15;
            this.rb_Antenna2.TabStop = true;
            this.rb_Antenna2.Text = "안테나";
            this.rb_Antenna2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.rb_Antenna2.UseVisualStyleBackColor = true;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(142, 291);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 4;
            this.btnSave.Text = "채널 저장";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // dataGridChannel
            // 
            this.dataGridChannel.AllowUserToAddRows = false;
            this.dataGridChannel.AllowUserToDeleteRows = false;
            this.dataGridChannel.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridChannel.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colChannel,
            this.colName,
            this.colType,
            this.aupid});
            this.dataGridChannel.Dock = System.Windows.Forms.DockStyle.Top;
            this.dataGridChannel.Location = new System.Drawing.Point(3, 3);
            this.dataGridChannel.MultiSelect = false;
            this.dataGridChannel.Name = "dataGridChannel";
            this.dataGridChannel.RowHeadersWidth = 20;
            this.dataGridChannel.RowTemplate.Height = 23;
            this.dataGridChannel.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridChannel.Size = new System.Drawing.Size(232, 252);
            this.dataGridChannel.TabIndex = 3;
            this.dataGridChannel.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridChannel_CellEndEdit);
            this.dataGridChannel.CellMouseDoubleClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridChannel_CellMouseDoubleClick);
            // 
            // colChannel
            // 
            this.colChannel.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.colChannel.HeaderText = "CH.";
            this.colChannel.MaxInputLength = 4;
            this.colChannel.MinimumWidth = 10;
            this.colChannel.Name = "colChannel";
            this.colChannel.ReadOnly = true;
            this.colChannel.Width = 50;
            // 
            // colName
            // 
            this.colName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colName.HeaderText = "이름";
            this.colName.MaxInputLength = 30;
            this.colName.Name = "colName";
            // 
            // colType
            // 
            this.colType.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.colType.HeaderText = "V pid";
            this.colType.Name = "colType";
            this.colType.ReadOnly = true;
            this.colType.Width = 60;
            // 
            // aupid
            // 
            this.aupid.HeaderText = "A Pid";
            this.aupid.Name = "aupid";
            this.aupid.ReadOnly = true;
            this.aupid.Width = 60;
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(3, 291);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(73, 23);
            this.btnSearch.TabIndex = 2;
            this.btnSearch.Text = "시작";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.tb_statistics);
            this.groupBox3.Location = new System.Drawing.Point(7, 354);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(232, 119);
            this.groupBox3.TabIndex = 9;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "통계";
            // 
            // tb_statistics
            // 
            this.tb_statistics.Location = new System.Drawing.Point(8, 20);
            this.tb_statistics.Multiline = true;
            this.tb_statistics.Name = "tb_statistics";
            this.tb_statistics.Size = new System.Drawing.Size(218, 93);
            this.tb_statistics.TabIndex = 0;
            // 
            // TVConfiguration
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(853, 517);
            this.Controls.Add(this.splitContainer1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(800, 450);
            this.Name = "TVConfiguration";
            this.Text = "TV Configuration";
            this.Load += new System.EventHandler(this.TVConfiguration_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ud_contury)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ud_channel)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridChannel)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.SplitContainer splitContainer1;
		private System.Windows.Forms.ComboBox cbox_TVType;
		private TVControl.TVControl tvControl1;
		private System.Windows.Forms.Button btn_Play;
		private System.Windows.Forms.Button btnStop;
		private System.Windows.Forms.NumericUpDown ud_channel;
		private System.Windows.Forms.Button btn_tune;
		private System.Windows.Forms.ComboBox cbox_detail_type;
		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.NumericUpDown ud_contury;
		private System.Windows.Forms.TextBox tb_statistics;
		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.TabPage tabPage1;
		private System.Windows.Forms.TabPage tabPage2;
		private System.Windows.Forms.Button btnSetting;
		private System.Windows.Forms.Button btnSearch;
		private System.Windows.Forms.DataGridView dataGridChannel;
        private System.Windows.Forms.Button btnSave;
		private System.Windows.Forms.Button btnSaveFilter;
		private System.Windows.Forms.RadioButton rb_cable;
		private System.Windows.Forms.RadioButton rb_Antenna;
		private System.Windows.Forms.RadioButton rb_Cable2;
		private System.Windows.Forms.RadioButton rb_Antenna2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox text_Audio;
        private System.Windows.Forms.TextBox text_Video;
        private System.Windows.Forms.DataGridViewTextBoxColumn colChannel;
        private System.Windows.Forms.DataGridViewTextBoxColumn colName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colType;
        private System.Windows.Forms.DataGridViewTextBoxColumn aupid;

	}
}

