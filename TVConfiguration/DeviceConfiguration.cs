﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DirectShowLib;
using TVControl.Library;
using System.Xml;

namespace TVConfiguration
{
	public partial class DeviceConfiguration : Form
	{
		public DeviceConfiguration()
		{
			InitializeComponent();
		}

		public String WDMVideoDevice
		{
			get {
				return this.cbox_WDMDevice.SelectedItem.ToString();
			}
		}
		public String WDMAudioDevice
		{
			get {
				return this.cbox_soundDevice.SelectedItem.ToString();
			}
		}
		public String BDADevice
		{
			get {
				return this.cbox_device.SelectedItem.ToString();
			}
		}
		public bool IsVMR9
		{
			get {
				return this.cb_isVMR9.Checked;
			}
		}
		private void Initialize()
		{
			int hr = 0;
			DsDevice[] devices = null;

			try
			{
				cbox_device.Items.Clear();
				cbox_WDMDevice.Items.Clear();
				cbox_soundDevice.Items.Clear();

				// Enumerate BDA Source filters category and found one that can connect to the network provider
				devices = DsDevice.GetDevicesOfCat(FilterCategory.BDASourceFiltersCategory);

				foreach (DsDevice dv in devices)
				{
					if (dv.Name != null)
						cbox_device.Items.Add(dv.Name);
				}
				if (cbox_device.Items.Count > 0)
					cbox_device.SelectedIndex = 0;


				foreach (DsDevice dv in GraphBuilderTV.GetVideoDsDevices())
				{
					if (dv.Name != null)
						cbox_WDMDevice.Items.Add(dv.Name);
				}
				if (cbox_WDMDevice.Items.Count > 0)
					cbox_WDMDevice.SelectedIndex = 0;

				cbox_soundDevice.Items.Add("<없음>");
				foreach (DsDevice dv in GraphBuilderTV.GetAudioDsDevices())
				{
					if (dv.Name != null)
						cbox_soundDevice.Items.Add(dv.Name);
				}
				if (cbox_soundDevice.Items.Count > 0)
					cbox_soundDevice.SelectedIndex = 0;

			}
			catch { }
		}

		public void Load(string WDMVideoDevice, string WDMAudioDevice, string BDADevice, bool isVMR9)
		{
			Initialize();
			cbox_WDMDevice.SelectedItem = WDMVideoDevice;
			cbox_soundDevice.SelectedItem = WDMAudioDevice;
			cbox_device.SelectedItem = BDADevice;
			cb_isVMR9.Checked = isVMR9;

			this.ShowDialog();
		}

		private void btnOk_Click(object sender, EventArgs e)
		{
			this.DialogResult = DialogResult.OK;
			Close();
		}

		private void btnCancel_Click(object sender, EventArgs e)
		{
			this.DialogResult = DialogResult.Cancel;
			Close();
		}
		
	}
}
