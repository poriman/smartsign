﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using TVControl.Library;
using System.Collections.ObjectModel;

namespace TVConfiguration
{
	public partial class SearchForm : Form
	{
		public event EventHandler ChannelChanged = null;
		public event EventHandler ConfirmSignal = null;
		Timer channeling = null;

		Collection<Channel> arrSearchList = new Collection<Channel>();
		int nIndex = 0;

		public SearchForm()
		{
			InitializeComponent();
		}

		public void DoModal(bool doAnalog, bool doDigital, bool isAntenna)
		{
			arrSearchList.Clear();
			MakeSearchList(doAnalog, doDigital, isAntenna);

			ShowDialog();
		}

		void MakeSearchList(bool doAnalog, bool doDigital, bool isAntenna)
		{
            //if(doAnalog)
            //{
            //    for(int i = 2; i < 60; ++i)
            //    {
            //        ChannelAnalogic ch = new ChannelAnalogic();
            //        ch.Name = i.ToString();
            //        ch.CountryCode = 82;
            //        ch.ChannelNumber = i;
            //        ch.AudioMode = DirectShowLib.TVAudioMode.Stereo;
            //        ch.InputType = isAntenna ? DirectShowLib.TunerInputType.Antenna : DirectShowLib.TunerInputType.Cable;
            //        ch.VideoStandard = DirectShowLib.AnalogVideoStandard.NTSC_M;
            //        ch.VideoSource = "Video Tuner";
            //        ch.Mode = DirectShowLib.AMTunerModeType.Default;

            //        arrSearchList.Add(ch);

            //    }
            //}

			if(doDigital)
			{
				//for (int i = 2; i < 135; ++i)
                for (int i = 60; i < 70; ++i)
				{
                    ChannelATSC ch = new ChannelATSC();
                    ch.PSIMODE = true;
					ch.CountryCode = 82;
					ch.Name = String.Format("D{0}", i);
					ch.ChannelNumber = i;
					ch.Modulation = DirectShowLib.BDA.ModulationType.Mod8Vsb;
                    ch.AudioPid = 17;
                    ch.VideoPid =20;
                    ch.InputType = isAntenna ? DirectShowLib.TunerInputType.Antenna : DirectShowLib.TunerInputType.Cable;

					arrSearchList.Add(ch);
				}
			}
		}

		private void SearchForm_Load(object sender, EventArgs e)
		{
			if(arrSearchList.Count > 0)
			{
				nIndex = 0;
				channeling = new Timer();
				channeling.Tick += new EventHandler(channeling_Tick);
				channeling.Interval += 100;

				channeling.Start();
			}
			else
			{
				this.DialogResult = DialogResult.Cancel;
				this.Close();
			}
		}

		void channeling_Tick(object sender, EventArgs e)
		{
			channeling.Stop();
			if (ConfirmSignal != null) ConfirmSignal(this, null);

			if (ChannelChanged != null)
			{
				try
				{
					if (ChannelChanged != null)
					{
						Channel ch = arrSearchList[nIndex++];
						label1.Text = String.Format("채널: {0}, 종류: {1}", ch.ChannelNumber, ch.GetType().Name);
						ChannelChanged((object)ch, null);
					}
				}
				catch (ArgumentOutOfRangeException indexerr)
				{
					channeling.Stop();
					this.DialogResult = DialogResult.OK;
					this.Close();

					return;
				}
			catch (Exception ex)
				{
					MessageBox.Show(ex.Message);
					channeling.Stop();
					this.DialogResult = DialogResult.Abort;
					this.Close();

					return;
				}
			}
			channeling.Start();
		}

		private void btnCancel_Click(object sender, EventArgs e)
		{
			channeling.Stop();
			channeling = null;
			this.DialogResult = DialogResult.Abort;
			this.Close();
		}

		
	}
}
