﻿namespace SerialKeyGenerator
{
	partial class Form1
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.labelIP = new System.Windows.Forms.Label();
            this.textA = new System.Windows.Forms.TextBox();
            this.numericMaxPlayers = new System.Windows.Forms.NumericUpDown();
            this.textB = new System.Windows.Forms.TextBox();
            this.textC = new System.Windows.Forms.TextBox();
            this.textD = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textResult = new System.Windows.Forms.TextBox();
            this.btnCreate = new System.Windows.Forms.Button();
            this.btnConfirm = new System.Windows.Forms.Button();
            this.textKey = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.comboProduct = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.numericMaxPlayers)).BeginInit();
            this.SuspendLayout();
            // 
            // labelIP
            // 
            this.labelIP.AutoSize = true;
            this.labelIP.Location = new System.Drawing.Point(12, 9);
            this.labelIP.Name = "labelIP";
            this.labelIP.Size = new System.Drawing.Size(67, 12);
            this.labelIP.TabIndex = 0;
            this.labelIP.Text = "IP Address";
            // 
            // textA
            // 
            this.textA.Location = new System.Drawing.Point(14, 24);
            this.textA.MaxLength = 3;
            this.textA.Name = "textA";
            this.textA.Size = new System.Drawing.Size(34, 21);
            this.textA.TabIndex = 1;
            this.textA.Text = "192";
            // 
            // numericMaxPlayers
            // 
            this.numericMaxPlayers.Increment = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.numericMaxPlayers.Location = new System.Drawing.Point(249, 25);
            this.numericMaxPlayers.Maximum = new decimal(new int[] {
            2000,
            0,
            0,
            0});
            this.numericMaxPlayers.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericMaxPlayers.Name = "numericMaxPlayers";
            this.numericMaxPlayers.Size = new System.Drawing.Size(75, 21);
            this.numericMaxPlayers.TabIndex = 5;
            this.numericMaxPlayers.ThousandsSeparator = true;
            this.numericMaxPlayers.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // textB
            // 
            this.textB.Location = new System.Drawing.Point(54, 24);
            this.textB.MaxLength = 3;
            this.textB.Name = "textB";
            this.textB.Size = new System.Drawing.Size(34, 21);
            this.textB.TabIndex = 2;
            this.textB.Text = "178";
            // 
            // textC
            // 
            this.textC.Location = new System.Drawing.Point(94, 24);
            this.textC.MaxLength = 3;
            this.textC.Name = "textC";
            this.textC.Size = new System.Drawing.Size(34, 21);
            this.textC.TabIndex = 3;
            this.textC.Text = "0";
            // 
            // textD
            // 
            this.textD.Location = new System.Drawing.Point(134, 24);
            this.textD.MaxLength = 3;
            this.textD.Name = "textD";
            this.textD.Size = new System.Drawing.Size(34, 21);
            this.textD.TabIndex = 4;
            this.textD.Text = "1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(247, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 12);
            this.label1.TabIndex = 7;
            this.label1.Text = "Max Players";
            // 
            // textResult
            // 
            this.textResult.Location = new System.Drawing.Point(12, 80);
            this.textResult.Multiline = true;
            this.textResult.Name = "textResult";
            this.textResult.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textResult.Size = new System.Drawing.Size(312, 160);
            this.textResult.TabIndex = 8;
            // 
            // btnCreate
            // 
            this.btnCreate.Location = new System.Drawing.Point(255, 50);
            this.btnCreate.Name = "btnCreate";
            this.btnCreate.Size = new System.Drawing.Size(69, 22);
            this.btnCreate.TabIndex = 7;
            this.btnCreate.Text = "생성";
            this.btnCreate.UseVisualStyleBackColor = true;
            this.btnCreate.Click += new System.EventHandler(this.btnCreate_Click);
            // 
            // btnConfirm
            // 
            this.btnConfirm.Location = new System.Drawing.Point(255, 246);
            this.btnConfirm.Name = "btnConfirm";
            this.btnConfirm.Size = new System.Drawing.Size(69, 22);
            this.btnConfirm.TabIndex = 10;
            this.btnConfirm.Text = "키 확인";
            this.btnConfirm.UseVisualStyleBackColor = true;
            this.btnConfirm.Click += new System.EventHandler(this.btnConfirm_Click);
            // 
            // textKey
            // 
            this.textKey.Location = new System.Drawing.Point(12, 248);
            this.textKey.Name = "textKey";
            this.textKey.Size = new System.Drawing.Size(237, 21);
            this.textKey.TabIndex = 9;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 55);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 12);
            this.label3.TabIndex = 12;
            this.label3.Text = "Product";
            // 
            // comboProduct
            // 
            this.comboProduct.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboProduct.FormattingEnabled = true;
            this.comboProduct.Location = new System.Drawing.Point(66, 51);
            this.comboProduct.Name = "comboProduct";
            this.comboProduct.Size = new System.Drawing.Size(169, 20);
            this.comboProduct.TabIndex = 13;
            // 
            // Form1
            // 
            this.AcceptButton = this.btnCreate;
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(336, 278);
            this.Controls.Add(this.comboProduct);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.textKey);
            this.Controls.Add(this.btnConfirm);
            this.Controls.Add(this.btnCreate);
            this.Controls.Add(this.textResult);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textD);
            this.Controls.Add(this.textC);
            this.Controls.Add(this.textB);
            this.Controls.Add(this.numericMaxPlayers);
            this.Controls.Add(this.textA);
            this.Controls.Add(this.labelIP);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "NE SerialKey Generator";
            ((System.ComponentModel.ISupportInitialize)(this.numericMaxPlayers)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label labelIP;
		private System.Windows.Forms.TextBox textA;
		private System.Windows.Forms.NumericUpDown numericMaxPlayers;
		private System.Windows.Forms.TextBox textB;
		private System.Windows.Forms.TextBox textC;
		private System.Windows.Forms.TextBox textD;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox textResult;
		private System.Windows.Forms.Button btnCreate;
		private System.Windows.Forms.Button btnConfirm;
		private System.Windows.Forms.TextBox textKey;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.ComboBox comboProduct;
	}
}

