﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DigitalSignage.SerialKey;

namespace SerialKeyGenerator
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();

			comboProduct.Items.Add(NESerialKey._ProductCode.EmbededEdition);
			comboProduct.Items.Add(NESerialKey._ProductCode.StandaloneEdition);
			comboProduct.Items.Add(NESerialKey._ProductCode.Menuboard);
			comboProduct.Items.Add(NESerialKey._ProductCode.NetworkEdition);
			comboProduct.Items.Add(NESerialKey._ProductCode.SoftwareAsaService);

			comboProduct.SelectedItem = NESerialKey._ProductCode.NetworkEdition;
		}

		private void btnCreate_Click(object sender, EventArgs e)
		{
			try
			{
				int nCount = 1;
				for(int i = 0 ; i < nCount ; ++i)
				{
					NESerialKey key = new NESerialKey();
					textResult.Text += key.MakeSerialKey((SerialKey._ProductCode)comboProduct.SelectedItem, Convert.ToInt32(textA.Text), Convert.ToInt32(textB.Text), Convert.ToInt32(textC.Text), Convert.ToInt32(textD.Text), Convert.ToInt32(numericMaxPlayers.Value));
					textResult.Text += "\r\n";
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}
		}

		private void btnConfirm_Click(object sender, EventArgs e)
		{
			int A = 0, B = 0, C = 0, D = 0, MaxPlayers = 0;
			NESerialKey._ProductCode pcode = SerialKey._ProductCode.NetworkEdition;
			try
			{
				NESerialKey key = new NESerialKey();
				if (key.ParseSerialKey(textKey.Text, ref pcode, ref A, ref B, ref C, ref D, ref MaxPlayers))
				{
					MessageBox.Show(String.Format("사용할 수 있는 Serial Key입니다. \r\n Product Code: {0} \r\n IP: {1}-{2}-{3}-{4}\r\n Max Players: {5}",
						pcode.ToString(), A, B, C, D, MaxPlayers));
				}
				else
				{
					MessageBox.Show("유효하지 않는 키입니다.");
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}
			
			
		}
	}
}
