﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using NLog.Targets;
using System.IO;
using NLog;

namespace TestTaskControlHandler
{
	public partial class Form1 : Form
	{
		DigitalSignage.ServerTaskHandler.TimerLoop timer = new DigitalSignage.ServerTaskHandler.TimerLoop();

		public Form1()
		{
			InitializeComponent();
			InitializeLogger();
		}

		private void InitializeLogger()
		{
			String sExecutingPath = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
			string appData = sExecutingPath + "\\ServiceData\\";
			if (!Directory.Exists(appData))
				Directory.CreateDirectory(appData);

			try
			{
				// configuring log output
				FileTarget target = new FileTarget();
				target.Layout = "${longdate}\t[${level}]\t[${callsite}]\t${message}";
				//             target.Layout = "${longdate} ${logger} ${message}";
				target.FileName = appData + "TaskHandler.Log.txt";
				target.ArchiveFileName = appData + "archives/Manager.Log.{#####}.txt";
				// 			target.ArchiveAboveSize = 256 * 1024 * 10; // archive files greater than 2560 KB
				target.MaxArchiveFiles = 31;
				target.ArchiveEvery = FileArchivePeriod.Day;// FileTarget.ArchiveEveryMode.Day;
				target.ArchiveNumbering = ArchiveNumberingMode.Sequence;// FileTarget.ArchiveNumberingMode.Sequence;
				// this speeds up things when no other processes are writing to the file
				target.ConcurrentWrites = true;
				NLog.Config.SimpleConfigurator.ConfigureForTargetLogging(target, LogLevel.Debug);

			}
			catch { }
		}

		private void button2_Click(object sender, EventArgs e)
		{
			timer.Stop();
		}

		private void button1_Click(object sender, EventArgs e)
		{
			timer.Start();
		}
	}
}
