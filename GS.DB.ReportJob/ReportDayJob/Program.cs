﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GSReportLibrary;

namespace ReportDayJob
{
    class Program
    {
        static void Main(string[] args)
        {
            //매일 저녁에 해당 작업을 수행 하도록 Windows 예약 작업에 등록 하도록 한다.
            DateTime _now = DateTime.Now.AddDays(-1);

            int yyyy = _now.Year;
            int month = _now.Month;
            int day = _now.Day;

            PlayReport _playReport = new PlayReport();
            _playReport.startPlayReport(yyyy, month, day);

            RelayReport _replayReport = new RelayReport();
            _replayReport.startRelayReport(yyyy, month, day);

            RunningReport _runningReport = new RunningReport();
            _runningReport.startRunningReport(yyyy, month, day);        
        }
    }
}
