﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GSReportLibrary
{
    public class CommonUnit
    {
        public static string GetString(object o)
        {
            try
            {
                if (o != System.DBNull.Value)
                {
                    return (string)o;
                }
                else
                {
                    return "";
                }
            }
            catch
            {
                return "";
            }
        }

        public static int GetInt(object o)
        {
            try
            {
                if (o != System.DBNull.Value)
                {
                    return (int)o;
                }
                else
                {
                    return 0;
                }
            }
            catch
            {
                return 0;
            }
        }

        public static Int64 GetInt64(object o)
        {
            try
            {
                if (o != System.DBNull.Value)
                {
                    return (Int64)o;
                }
                else
                {
                    return 0;
                }
            }
            catch
            {
                return 0;
            }
        }
    }
}
