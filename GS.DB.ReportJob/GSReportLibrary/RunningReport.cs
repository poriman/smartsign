﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NLog.Targets;
using NLog;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using DigitalSignage.Common;

namespace GSReportLibrary
{
    public class RunningReport
    {
        public RunningReport()
        {
            // configuring log output
            FileTarget target = new FileTarget();
            target.Layout = "${longdate}\t[${level}]\t[${callsite}]\t${message}";
            target.FileName = AppDomain.CurrentDomain.BaseDirectory + "/Service.Log.txt";
            target.ArchiveFileName = AppDomain.CurrentDomain.BaseDirectory + "/archives/Service.Log.{#####}.txt";
            target.MaxArchiveFiles = 31;
            target.ArchiveEvery = FileArchivePeriod.Day;// FileTarget.ArchiveEveryMode.Day;
            target.ArchiveNumbering = ArchiveNumberingMode.Sequence;// FileTarget.ArchiveNumberingMode.Sequence;
            target.ConcurrentWrites = true;
            NLog.Config.SimpleConfigurator.ConfigureForTargetLogging(target, LogLevel.Debug);
        }

        private static Logger logger = LogManager.GetCurrentClassLogger();

        public void startRunningReport(int _year, int _month, int _day)
        {
            logger.Info("운영현황 리포트 작업 시작");

            Database db = DatabaseFactory.CreateDatabase();

            string player_query = "select pid , sum(end_dt - start_dt)/60 as r_time from playlogs where end_dt > @ST_TIME and end_dt <= @END_TIME and screen_id is not null group by pid";

            for (int i = 0; i < 24; i++)
            {
                try
                {
                    DbCommand command = db.GetSqlStringCommand(player_query);

                    long _st_tm = TimeConverter.ConvertToUTP(new DateTime(_year, _month, _day, i, 0, 0).ToUniversalTime());
                    long _ed_tm = TimeConverter.ConvertToUTP(new DateTime(_year, _month, _day, i, 59, 59).ToUniversalTime());

                    logger.Debug("select pid , sum(end_dt - start_dt)/60 as r_time from playlogs where start_dt >" + _st_tm.ToString() + " and end_dt < " + _ed_tm.ToString() + " and screen_id is not null group by pid");

                    db.AddInParameter(command, "@ST_TIME", DbType.Int64, _st_tm);
                    db.AddInParameter(command, "@END_TIME", DbType.Int64, _ed_tm);

                    using (IDataReader dataReader = db.ExecuteReader(command))
                    {
                        while (dataReader.Read())
                        {
                            string p_id = CommonUnit.GetString(dataReader["pid"]);
                            int running_time = Convert.ToInt32(CommonUnit.GetInt64(dataReader["r_time"]));

                            string insertSql = "insert into report_running_time(YYYY,MM,DD,H24,pid,tm) values (@YYYY,@MM,@DD,@H24,@PID,@TM)";

                            DbCommand insertCommand = db.GetSqlStringCommand(insertSql);

                            db.AddInParameter(insertCommand, "@YYYY", DbType.String, _year.ToString());
                            if (_month < 10)
                                db.AddInParameter(insertCommand, "@MM", DbType.String, "0" + _month.ToString());
                            else
                                db.AddInParameter(insertCommand, "@MM", DbType.String, _month.ToString());
                            
                            if (_day < 10)
                                db.AddInParameter(insertCommand, "@DD", DbType.String, "0" + _day.ToString());
                            else
                                db.AddInParameter(insertCommand, "@DD", DbType.String, _day.ToString());

                            if (i < 10)
                                db.AddInParameter(insertCommand, "@H24", DbType.String, "0" + i.ToString());
                            else
                                db.AddInParameter(insertCommand, "@H24", DbType.String, i.ToString());

                            db.AddInParameter(insertCommand, "@PID", DbType.String, p_id);
                            db.AddInParameter(insertCommand, "@TM", DbType.Int32, running_time);

                            db.ExecuteNonQuery(insertCommand);
                        }
                    }
                }
                catch (Exception e)
                {
                    logger.Error("가동현황 리포트 작업 오류 발생 (" + e.Message + ") year:" + _year.ToString() + " month:" + _month.ToString() + " day:" + _day.ToString());
                }
            }

            logger.Info("운영현황 리포트 작업 완료");
        }
    }
}
