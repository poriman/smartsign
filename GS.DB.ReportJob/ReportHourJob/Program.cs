﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GSReportLibrary;

namespace ReportHourJob
{
    class Program
    {
        static void Main(string[] args)
        {

            PlayReport p_report = new PlayReport();
            for (DateTime dt = new DateTime(2011, 11, 1, 0, 0, 0); dt < DateTime.Now; dt += TimeSpan.FromHours(1))
            {
                p_report.startPlayReport(dt.Year, dt.Month, dt.Day);
            }

            ScheduleReport s_report = new ScheduleReport();
            for (DateTime dt = new DateTime(2011, 11, 1, 0, 0, 0); dt < DateTime.Now; dt += TimeSpan.FromHours(1))
            {
                s_report.startScheduleReport(dt.Year, dt.Month, dt.Day, dt.Hour);
            }

            RunningReport r_report = new RunningReport();
            for (DateTime dt = new DateTime(2011, 11, 1, 0, 0, 0); dt < DateTime.Now; dt += TimeSpan.FromHours(1))
            {
                r_report.startRunningReport(dt.Year, dt.Month, dt.Day);
            }
            RelayReport rl_report = new RelayReport();
            for (DateTime dt = new DateTime(2011, 11, 1, 0, 0, 0); dt < DateTime.Now; dt += TimeSpan.FromHours(1))
            {
                rl_report.startRelayReport(dt.Year, dt.Month, dt.Day);
            }
        }
    }
}
