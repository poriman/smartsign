﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NLog.Targets;
using NLog;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;

namespace GSReportLibrary
{
    class RelayReport
    {
        public RelayReport()
        {
            // configuring log output
            FileTarget target = new FileTarget();
            target.Layout = "${longdate}\t[${level}]\t[${callsite}]\t${message}";
            target.FileName = AppDomain.CurrentDomain.BaseDirectory + "/Service.Log.txt";
            target.ArchiveFileName = AppDomain.CurrentDomain.BaseDirectory + "/archives/Service.Log.{#####}.txt";
            target.MaxArchiveFiles = 31;
            target.ArchiveEvery = FileArchivePeriod.Day;// FileTarget.ArchiveEveryMode.Day;
            target.ArchiveNumbering = ArchiveNumberingMode.Sequence;// FileTarget.ArchiveNumberingMode.Sequence;
            target.ConcurrentWrites = true;
            NLog.Config.SimpleConfigurator.ConfigureForTargetLogging(target, LogLevel.Error);
        }

        private static Logger logger = LogManager.GetCurrentClassLogger();

        public void startRelayReport(int _year, int _month, int _day)
        {
            logger.Info("릴레이 리포트 작업 시작");

            try
            {
                Database db = DatabaseFactory.CreateDatabase();

                //전체 Meta Tag에 대한 그룹을 조회
                string meta_groupSql = "select g_id,g_name from meta_tag_groups";

                DbCommand meta_command = db.GetSqlStringCommand(meta_groupSql);

                string insertSql = "INSERT INTO report_relay(YYYY,MM,DD,H24,r_value,g_id,CNT) SELECT @YYYY,@MM,@DD,@H24,relaylogs_detail.r_value,@G_ID,COUNT(relaylogs_detail.r_value) from relaylogs_detail,relaylogs where relaylogs_detail.relay_id=relaylogs.relay_id and r_key=@META_KEY and relaylogs.client_down_ts > @START_DT and relaylogs.client_down_ts < @END_DT group by relaylogs_detail.r_value";

                using (IDataReader dbreader = db.ExecuteReader(meta_command))
                {
                    while (dbreader.Read())
                    {
                        for (int i = 0; i < 24; i++)
                        {
                            long _startDt = (new DateTime(_year, _month, _day, i, 0, 0)).ToUniversalTime().Ticks;
                            long _endDt = (new DateTime(_year, _month, _day, i, 59, 59)).ToUniversalTime().Ticks;

                            //Meta group의 조회 결과를 loop 돌면서 해당 SUM 정보를 DB에 기록을 수행 한다.                    
                            DbCommand insertCommand = db.GetSqlStringCommand(insertSql);

                            db.AddInParameter(insertCommand, "@YYYY", DbType.String, _year.ToString());
                            if (_month < 10)
                                db.AddInParameter(insertCommand, "@MM", DbType.String, "0" + _month.ToString());
                            else
                                db.AddInParameter(insertCommand, "@MM", DbType.String, _month.ToString());

                            if (_day < 10)
                                db.AddInParameter(insertCommand, "@DD", DbType.String, "0" + _day.ToString());
                            else
                                db.AddInParameter(insertCommand, "@DD", DbType.String, _day.ToString());
                            
                            if (i < 10)
                                db.AddInParameter(insertCommand, "@H24", DbType.String, "0" + i.ToString());
                            else
                                db.AddInParameter(insertCommand, "@H24", DbType.String, i.ToString());
                            db.AddInParameter(insertCommand, "@G_ID", DbType.Int64, CommonUnit.GetInt64(dbreader["g_id"]));
                            db.AddInParameter(insertCommand, "@META_KEY", DbType.String, CommonUnit.GetString(dbreader["g_name"]));

                            db.AddInParameter(insertCommand, "@START_DT", DbType.Int64, _startDt);
                            db.AddInParameter(insertCommand, "@END_DT", DbType.Int64, _endDt);

                            //logger.Debug("INSERT INTO report_relay(YYYY,MM,DD,H24,r_value,g_id,CNT) SELECT " + _year.ToString() + "," + _month.ToString() + "," + _day.ToString() + "," + i.ToString() + ",relaylogs_detail.r_value," + CommonUnit.GetInt64(dbreader["g_id"]) + ",COUNT(relaylogs_detail.r_value) from relaylogs_detail,relaylogs where relaylogs_detail.relay_id=relaylogs.relay_id and r_key=" + CommonUnit.GetString(dbreader["g_name"]) + " and relaylogs.client_down_ts > " + _startDt.ToString() + " and relaylogs.client_down_ts < " + _endDt + " group by relaylogs_detail.r_value");

                            db.ExecuteNonQuery(insertCommand);
                        }
                    }
                }

                
            }
            catch (Exception e)
            {
                logger.Error("릴레이 리포트 작업 오류 발생 (" + e.Message + ") year:" + _year.ToString() + " month:" + _month.ToString() + " day:" + _day.ToString());
            }

            logger.Info("릴레이 리포트 작업 종료");
        }
    }
}
