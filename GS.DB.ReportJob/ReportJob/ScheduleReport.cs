﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NLog.Targets;
using NLog;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using DigitalSignage.Common;
using System.Collections.ObjectModel;

namespace GSReportLibrary
{
    class ScheduleReport
    {
        public ScheduleReport()
        {
            // configuring log output
            FileTarget target = new FileTarget();
            target.Layout = "${longdate}\t[${level}]\t[${callsite}]\t${message}";
            target.FileName = AppDomain.CurrentDomain.BaseDirectory + "/Service.Log.txt";
            target.ArchiveFileName = AppDomain.CurrentDomain.BaseDirectory + "/archives/Service.Log.{#####}.txt";
            target.MaxArchiveFiles = 31;
            target.ArchiveEvery = FileArchivePeriod.Day;// FileTarget.ArchiveEveryMode.Day;
            target.ArchiveNumbering = ArchiveNumberingMode.Sequence;// FileTarget.ArchiveNumberingMode.Sequence;
            target.ConcurrentWrites = true;
            NLog.Config.SimpleConfigurator.ConfigureForTargetLogging(target, LogLevel.Error);
        }

        private static Logger logger = LogManager.GetCurrentClassLogger();
 
        public void startScheduleReport(int _year, int _month, int _day, int _hour)
        {
            logger.Info("스케줄현황 리포트 작업 시작");

            Database db = DatabaseFactory.CreateDatabase();

            try
            {
                /// 1. 스케줄 전체 조회
                /// 2. 플레이어 쿼리 
                /// 3. 플레이어 스케줄 조회
                /// 4. 그룹 스케줄 조회
                DateTime dtlocalStartTime = new DateTime(_year, _month, _day, _hour, 0, 0);
                long start_dt = TimeConverter.ConvertToUTP(dtlocalStartTime.ToUniversalTime());
                long end_dt = start_dt + 3599;

                /// 스케줄 전체 조회
                string stbQuery = String.Format("SELECT  daysofweek, duration, duuid, endtime, endtimeofday, etime, gid, guuid, name, pid, priority, starttime, starttimeofday, state, type, uuid, is_ad, meta_tag, show_flag FROM tasks WHERE (starttime BETWEEN {0} AND {1}) AND (state < 3 OR state = 5) OR (state < 3 OR state = 5) AND (endtime BETWEEN {0} AND {1}) OR (state < 3 OR state = 5) AND ({0} BETWEEN starttime AND endtime) OR (state < 3 OR state = 5) AND ({1} BETWEEN starttime AND endtime)", start_dt, end_dt);

                DbCommand stbCommand = db.GetSqlStringCommand(stbQuery);

                using (DataTable dtTasks = new DataTable())
                {
                    using (IDataReader dataReader = db.ExecuteReader(stbCommand))
                    {
                        dtTasks.Load(dataReader);
                        dataReader.Dispose();
                    }

                    //  플레이어 쿼리

                    string playerQuery = "select pid , gid from players where del_yn='N'";

                    DbCommand playerCommand = db.GetSqlStringCommand(playerQuery);

                    using (IDataReader dataReader = db.ExecuteReader(playerCommand))
                    {
                        while (dataReader.Read())
                        {
                            int nCntTimeSched = 0;
                            int nCntRollingSched = 0;

                            //  플레이어 ID
                            string pid = CommonUnit.GetString(dataReader["pid"]);
                            Collection<string> arrGids = new Collection<string>();

                            string gid = CommonUnit.GetString(dataReader["gid"]);

                            #region 그룹 ID 리스트 작성
                            string groupQuery = String.Format("WITH GROUP_CTE(LEVEL,GID,PARENT_GID) AS ( SELECT 1 AS LEVEL,  A.GID , A.PARENT_GID FROM GROUPS A WHERE a.GID = '{0}'  UNION ALL SELECT LEVEL+1,B.GID,B.PARENT_GID FROM GROUPS B JOIN GROUP_CTE C ON B.GID = C.PARENT_GID ) SELECT GID FROM GROUP_CTE;", gid);

                            DbCommand groupCommand = db.GetSqlStringCommand(groupQuery);

                            using (IDataReader groupDR = db.ExecuteReader(groupCommand))
                            {
                                while (groupDR.Read())
                                {
                                    arrGids.Add(CommonUnit.GetString(groupDR["gid"]));
                                }
                                groupDR.Dispose();
                            }
                            groupCommand.Dispose();
                            #endregion

                            logger.Debug(String.Format("Player: {0}, Parent gid({1}), parent groups count ({2})", pid, gid, arrGids.Count));

                            /// 스케줄 검증
                            foreach (DataRow task in dtTasks.Rows)
                            {
                                int type = Convert.ToInt32(task["type"]);
                                int state = Convert.ToInt32(task["state"]);

                                String desc_pid = task["pid"] as String;
                                String desc_gid = task["gid"] as String;

                                /// 반복 스케줄 또는 시간 스케줄이 아닌 경우
                                if (type != 3 && type != 10) continue;
                                /// 스케줄이 삭제된 경우
                                if (state == 3) continue;

                                /// 해당 스케줄이 그룹 ID에 포함되거나 플레이어 ID에 포함되는 경우
                                if ((arrGids.Contains(desc_gid) && String.IsNullOrEmpty(desc_pid)) || desc_pid == pid)
                                {
                                    int starttimeofday = Convert.ToInt32(task["starttimeofday"]);
                                    int endtimeofday = Convert.ToInt32(task["endtimeofday"]);
                                    int daysofweek = Convert.ToInt32(task["daysofweek"]);

                                    if (CheckContainTimeofDay(dtlocalStartTime, starttimeofday, endtimeofday)
                                        && CheckDayOfWeek(dtlocalStartTime, daysofweek))
                                    {
                                        if (type == 3) nCntTimeSched++;
                                        else nCntRollingSched++;
                                    }
                                }
                            }

                            #region Insert Query
                            string insertSql1 = "insert into report_time_schedule(YYYY,MM,DD,H24,pid,type,cnt) values(@YYYY,@MM,@DD,@H24,@pid,@type,@CNT)";

                            DbCommand insertCommand1 = db.GetSqlStringCommand(insertSql1);

                            db.AddInParameter(insertCommand1, "@YYYY", DbType.String, _year.ToString());

                            if (_month < 10)
                                db.AddInParameter(insertCommand1, "@MM", DbType.String, "0" + _month.ToString());
                            else
                                db.AddInParameter(insertCommand1, "@MM", DbType.String, _month.ToString());

                            if (_day < 10)
                                db.AddInParameter(insertCommand1, "@DD", DbType.String, "0" + _day.ToString());
                            else
                                db.AddInParameter(insertCommand1, "@DD", DbType.String, _day.ToString());

                            if (_hour < 10)
                                db.AddInParameter(insertCommand1, "@H24", DbType.String, "0" + _hour.ToString());
                            else
                                db.AddInParameter(insertCommand1, "@H24", DbType.String, _hour.ToString());

                            db.AddInParameter(insertCommand1, "@pid", DbType.String, pid);
                            db.AddInParameter(insertCommand1, "@type", DbType.Int32, 3);
                            db.AddInParameter(insertCommand1, "@CNT", DbType.Int32, nCntTimeSched);

                            db.ExecuteNonQuery(insertCommand1);


                            string insertSql2 = "insert into report_time_schedule(YYYY,MM,DD,H24,pid,type,cnt) values(@YYYY,@MM,@DD,@H24,@pid,@type,@CNT)";

                            DbCommand insertCommand2 = db.GetSqlStringCommand(insertSql2);

                            db.AddInParameter(insertCommand2, "@YYYY", DbType.String, _year.ToString());

                            if (_month < 10)
                                db.AddInParameter(insertCommand2, "@MM", DbType.String, "0" + _month.ToString());
                            else
                                db.AddInParameter(insertCommand2, "@MM", DbType.String, _month.ToString());

                            if (_day < 10)
                                db.AddInParameter(insertCommand2, "@DD", DbType.String, "0" + _day.ToString());
                            else
                                db.AddInParameter(insertCommand2, "@DD", DbType.String, _day.ToString());

                            if (_hour < 10)
                                db.AddInParameter(insertCommand2, "@H24", DbType.String, "0" + _hour.ToString());
                            else
                                db.AddInParameter(insertCommand2, "@H24", DbType.String, _hour.ToString());

                            db.AddInParameter(insertCommand2, "@pid", DbType.String, pid);
                            db.AddInParameter(insertCommand2, "@type", DbType.Int32, 10);
                            db.AddInParameter(insertCommand2, "@CNT", DbType.Int32, nCntRollingSched);

                            db.ExecuteNonQuery(insertCommand2);
                            #endregion
                        }
                    }
                }

            }
            catch (Exception e)
            {
                logger.Error("스케줄 리포트 작업 오류 발생 (" + e.Message + ") year:" + _year.ToString() + " month:" + _month.ToString() + " day:" + _day.ToString() + " hour:" + _hour.ToString());
            }
            logger.Info("스케줄현황 리포트 작업 종료");
        }

        /// <summary>
        /// 요일 체크
        /// </summary>
        /// <param name="dtLocal"></param>
        /// <param name="dayofweek"></param>
        /// <returns></returns>
        private bool CheckDayOfWeek(DateTime dtLocal, int dayofweek)
        {
            DayOfWeek dow = dtLocal.DayOfWeek;
            if (dow == DayOfWeek.Monday)
                return 0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_MON);
            else if (dow == DayOfWeek.Tuesday)
                return 0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_TUE);
            else if (dow == DayOfWeek.Wednesday)
                return 0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_WED);
            else if (dow == DayOfWeek.Thursday)
                return 0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_THU);
            else if (dow == DayOfWeek.Friday)
                return 0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_FRI);
            else if (dow == DayOfWeek.Saturday)
                return 0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_SAT);
            else if (dow == DayOfWeek.Sunday)
                return 0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_SUN);
            else
            {
                logger.Error("Exception : invalid DayOfWeek. : " + dayofweek.ToString());
            }
            return false;
        }

        /// <summary>
        /// dtLocal 시간이 TimeOfDay에 포함되는지
        /// </summary>
        /// <param name="dtLocal"></param>
        /// <param name="nStartTime"></param>
        /// <param name="nEndTime"></param>
        /// <returns></returns>
        private bool CheckContainTimeofDay(DateTime dtLocal, int nStartTime, int nEndTime)
        {
            //	둘다 같은 값이라면 Whole Day로 간주
            if (nStartTime == nEndTime) return true;

            int nCurr = dtLocal.Second + (dtLocal.Minute * 60) + (dtLocal.Hour * 3600);

            if (nStartTime < nEndTime)
            {
                //	시간대가 하루에 국한되는 경우

                return nCurr >= nStartTime && nCurr < nEndTime;
            }
            else
            {
                //	시간대가 오늘부터 다음 날로 넘어 간 경우
                return nCurr < nStartTime || nCurr >= nEndTime;
            }
        }

        /// <summary>
        ///	스케줄을 적용할 요일 정보
        /// </summary>
        public enum TaskApplyDayOfWeek
        {
            Day_MON = 1,
            Day_TUE = 2,
            Day_WED = 4,
            Day_THU = 8,
            Day_FRI = 16,
            Day_SAT = 32,
            Day_SUN = 64
        }
    }
}
