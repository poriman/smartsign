﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GSReportLibrary
{
    class Program
    {
        static void Main(string[] args)
        {
            ScheduleReport p_report = new ScheduleReport();
//            p_report.startScheduleReport(2011, 11, 1, 2);
            for(DateTime dt = new DateTime(2011, 11, 1, 0,0,0) ; dt < DateTime.Now ; dt += TimeSpan.FromHours(1))
            {
                p_report.startScheduleReport(dt.Year, dt.Month, dt.Day, dt.Hour);
            }
            //Console.ReadLine();
        }
    }
}
