﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NLog.Targets;
using NLog;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Data;
using DigitalSignage.Common;

namespace GSReportLibrary
{
    class PlayReport
    {
        public PlayReport()
        {
            // configuring log output
            FileTarget target = new FileTarget();
            target.Layout = "${longdate}\t[${level}]\t[${callsite}]\t${message}";
            target.FileName = AppDomain.CurrentDomain.BaseDirectory + "/Service.Log.txt";
            target.ArchiveFileName = AppDomain.CurrentDomain.BaseDirectory + "/archives/Service.Log.{#####}.txt";
            target.MaxArchiveFiles = 31;
            target.ArchiveEvery = FileArchivePeriod.Day;// FileTarget.ArchiveEveryMode.Day;
            target.ArchiveNumbering = ArchiveNumberingMode.Sequence;// FileTarget.ArchiveNumberingMode.Sequence;
            target.ConcurrentWrites = true;
            NLog.Config.SimpleConfigurator.ConfigureForTargetLogging(target, LogLevel.Error);
        }

        private static Logger logger = LogManager.GetCurrentClassLogger();

        public void startPlayReport(int _year, int _month, int _day)
        {
            logger.Info("플레이 리포트 작업 시작");

            Database db = DatabaseFactory.CreateDatabase();

            string player_query = "select screen_id,uuid,count(screen_id) as CNT from playlogs where end_dt >@ST_TIME and end_dt < @END_TIME and screen_id is not null group by screen_id ,uuid ";

            for (int i = 0; i < 24; i++)
            {
                try
                {
                    DbCommand command = db.GetSqlStringCommand(player_query);

                    long _st_tm = TimeConverter.ConvertToUTP(new DateTime(_year, _month, _day, i, 0, 0));
                    long _ed_tm = TimeConverter.ConvertToUTP(new DateTime(_year, _month, _day, i, 59, 59));

                    //logger.Debug("select screen_id,uuid,count(screen_id) as CNT from playlogs where end_dt >" + _st_tm.ToString() + " and end_dt < " + _ed_tm .ToString()+ " and screen_id is not null group by screen_id ,uuid");

                    db.AddInParameter(command, "@ST_TIME", DbType.Int64, _st_tm);
                    db.AddInParameter(command, "@END_TIME", DbType.Int64, _ed_tm);

                    using (IDataReader dataReader = db.ExecuteReader(command))
                    {
                        while (dataReader.Read())
                        {
                            long s_id = CommonUnit.GetInt64(dataReader["screen_id"]);
                            string uuid = CommonUnit.GetString(dataReader["uuid"]);
                            int cnt = CommonUnit.GetInt(dataReader["CNT"]);

                            string insertSql = "insert into report_play(YYYY,MM,DD,H24,CNT,s_id,uuid) values (@YYYY,@MM,@DD,@H24,@CNT,@S_ID,@UUID)";

                            DbCommand insertCommand = db.GetSqlStringCommand(insertSql);

                            db.AddInParameter(insertCommand, "@YYYY", DbType.String, _year.ToString());
                            if (_month < 10)
                                db.AddInParameter(insertCommand, "@MM", DbType.String, "0" + _month.ToString());
                            else
                                db.AddInParameter(insertCommand, "@MM", DbType.String, _month.ToString());
                            
                            if (_day < 10)
                                db.AddInParameter(insertCommand, "@DD", DbType.String, "0"+_day.ToString());
                            else
                                db.AddInParameter(insertCommand, "@DD", DbType.String, _day.ToString());

                            if (i < 10)
                                db.AddInParameter(insertCommand, "@H24", DbType.String, "0" + i.ToString());
                            else
                                db.AddInParameter(insertCommand, "@H24", DbType.String, i.ToString());
                            db.AddInParameter(insertCommand, "@CNT", DbType.Int32, cnt);
                            db.AddInParameter(insertCommand, "@S_ID", DbType.Int64, s_id);
                            db.AddInParameter(insertCommand, "@UUID", DbType.String, uuid);

                            db.ExecuteNonQuery(insertCommand);

                            //logger.Debug("s_id = " + s_id.ToString() + ", cnt=" + cnt.ToString());
                        }
                    }
                }
                catch (Exception e)
                {
                    logger.Error("재생 리포트 작업 오류 발생 (" + e.Message + ") year:" + _year.ToString() +" month:"+_month.ToString()+" day:"+_day.ToString());
                }
            }

            logger.Info("플레이 리포트 작업 종료");
        }
    }
}
