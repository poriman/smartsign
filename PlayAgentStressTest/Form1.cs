﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace PlayAgentStressTest
{
	public partial class Form1 : Form
	{
		int start_pid;
		int end_pid;
		int refresh;
		string hostname;
		AgentLoader loader = null;

		public Form1()
		{
			InitializeComponent();
		}

		private void btn_create_Click(object sender, EventArgs e)
		{
			if(!validationCheck())
				return;
			if(loader == null)
			{
				loader = new AgentLoader(hostname, start_pid, end_pid, refresh);
				loader.Start();
				btn_create.Text = "종료하기";
			}
			else
			{
				loader.Stop();
				loader = null;
				btn_create.Text = "생성하기";
			}

		}

		private bool validationCheck()
		{
			try
			{
				start_pid = Convert.ToInt32(tb_start_pid.Text);
				end_pid = Convert.ToInt32(tb_end_pid.Text);
				refresh = Convert.ToInt32(textRefresh.Text);
				hostname = textHostName.Text;

			}
			catch (Exception e)
			{
				MessageBox.Show(e.ToString());
				return false;
			}
			return true;
		}
	}
}
