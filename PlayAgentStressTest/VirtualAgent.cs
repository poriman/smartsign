﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading;
using System.IO;
using System.Reflection;

// logging routines
using NLog;
using NLog.Targets;
using System.Runtime.Remoting;
using System.Xml;
using System.Collections.ObjectModel;
using System.Diagnostics;

using DigitalSignage.Common;
using DigitalSignage.ServerDatabase;

namespace PlayAgentStressTest
{
	class VirtualAgent : INetworkServiceCallback
	{
		private Logger logger = null;

		bool bProcess = false;
		int nWaitSecond = 15;
		Nullable<DateTime> _connectedTime = null;
		
		readonly object semaphore = new object();


		Thread _thProcess = null;
		Thread _thLoggingProcess = null;
		String _hostname;
		String _pid;
		String _gid;

		long lastSync = 0;

		bool bAuth = false;

		NetworkServiceClient proxy = null;
		IVisionService.Topic clientInfo = null;

		void DetachProxy()
		{
			lock (semaphore) { proxy = null; }
		}
		private CommunicationState ProxyState
		{
			get {
				lock (semaphore){
					if (proxy != null) return proxy.State;
					else return CommunicationState.Closed;
				}
			}
		}

		public VirtualAgent(string hostname, string pid, int refresh)
		{
			_hostname = hostname;
			nWaitSecond = refresh;
			_pid = pid;
            _gid = "0000000000000";

			
			String sExecutingPath = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
			String appData = sExecutingPath + "\\Logs\\";
			try
			{
				if (!Directory.Exists(appData))
					Directory.CreateDirectory(appData);

			}
			catch{}

			// configuring log output
			FileTarget target = new FileTarget();
			target.Layout = "${longdate}\t[${level}]\t[${callsite}]\t${message}";
			target.FileName = appData + "logfile.txt";
			target.Name = _pid;
// 			target.ArchiveFileName = appData + "archives/log.{#####}.txt";
// 
// 			target.ArchiveEvery = FileTarget.ArchiveEveryMode.Day;
// 			target.MaxArchiveFiles = 31;
// 			target.ArchiveNumbering = FileTarget.ArchiveNumberingMode.Sequence;
			// this speeds up things when no other processes are writing to the file
			target.ConcurrentWrites = true;
			NLog.Config.SimpleConfigurator.ConfigureForTargetLogging(target, LogLevel.Info);
// 			logger = LogManager.GetCurrentClassLogger() ;
			logger = LogManager.GetLogger(_pid);
		}

		#region Functions

		#region Start/Stop
		public void Start()
		{
			logger.Info(_pid + " : Start Func..");
			if ((bProcess && _thProcess.IsAlive) || 
				(_thProcess != null && _thProcess.IsAlive))
			{
				_thProcess.Abort();
				bProcess = false;
			}
			bProcess = true;
			_thProcess = new Thread(procThread);
			_thProcess.Start();

			if ((_thLoggingProcess != null && _thLoggingProcess.IsAlive))
			{
				_thLoggingProcess.Abort();
			}

			_thLoggingProcess = new Thread(procLogging);
			_thLoggingProcess.Start();

			Thread.Sleep(10);
		}
		public void Stop()
		{
			logger.Info(_pid + " : Stop Func..");
			bProcess = false;
		}
		#endregion

		#region Thread loop
		void procThread()
		{
			logger.Info(_pid + " : Thread Start..");

			while(bProcess)
			{
				if(bAuth)
				{
					DateTime dt = DateTime.Now;
					startSynchronize();
					TimeSpan ts = DateTime.Now - dt;
					if (ts.TotalSeconds > 5.0)
						logger.Warn(_pid + " : " + "Synchronize Exec Time : " + ts.TotalSeconds.ToString());
					
					dt = DateTime.Now;
					if(!SendStatusToServer())
					{
						RetryTask();
					}
					ts = DateTime.Now - dt;
					if (ts.TotalSeconds > 5.0)
						logger.Warn(_pid + " : " + "Send Status Exec Time : " + ts.TotalSeconds.ToString());
				}
				else
				{
					//	1. 접속 루틴
					Connect();
				}

				int nWait = 0;
				while (nWait++ < nWaitSecond)
				{
					if (!bProcess)
					{
						Disconnect();
						logger.Info(_pid + " : Thread End..");

						return;
					}
					Thread.Sleep(1000);
				}
			}
			Disconnect();
			logger.Info(_pid+ " : Thread End..");

		}

		void procLogging()
		{
			logger.Info(_pid + " : Logging Thread Start..");

			while (true)
			{
				if(serverLogList != null)
				{
					try
					{
						DateTime dt = DateTime.Now;
//						serverLogList.InsertLogs(_pid, 10, 0, Guid.NewGuid().ToString(), "testLog", "testLog: " + _pid, 0, TimeConverter.ConvertToUTP(DateTime.Now));
						string xmlLogs = "<Logs>";
						for(int i = 0 ; i < 10 ; ++i)
						{
							xmlLogs += String.Format("<Log pid=\"{0}\" logcd=\"{1}\" uuid=\"{2}\" name=\"{3}\" description=\"{4}\" error_code=\"{5}\" start_dt=\"{6}\" end_dt=\"{6}\" />", _pid, 10, Guid.NewGuid().ToString(), "testLog", "testLog: " + _pid, 0, TimeConverter.ConvertToUTP(DateTime.Now));
						}
						xmlLogs += "</Logs>";

						serverLogList.InsertDetailLogs(xmlLogs);

						TimeSpan ts = DateTime.Now - dt;
						if (ts.TotalSeconds > 5.0)
							logger.Warn(_pid + " : " + "Logging Exec Time : " + ts.TotalSeconds.ToString());

						if (!bProcess)
						{
							return;
						}
					}
					catch (System.Exception ex)
					{
						logger.Error(ex.Message);
					}


				}
				Thread.Sleep(60000);
			}

			logger.Info("Logging Thread End!");

		}
		#endregion

		#region WCF Authoring
		public void Connect()
		{
			if (proxy == null || (ProxyState != CommunicationState.Opened && ProxyState != CommunicationState.Opening))
			{
				try
				{
					if (proxy != null)
						Disconnect();


					logger.Info(_pid + " : Connecting WCF Endpoint : " + _hostname);

					EndpointAddress epAddress = new EndpointAddress("net.tcp://" + _hostname + ":40526/INetworkHost/tcp");

					InstanceContext context = new InstanceContext(this);
					proxy = new NetworkServiceClient(context, "", epAddress);

					clientInfo = new IVisionService.Topic();
					clientInfo.Groupid = _gid;
					clientInfo.Mediaid = _pid;
					clientInfo.Level = IVisionService.UserLevels.Media;

					IAsyncResult iar = proxy.BeginConnect(this.clientInfo, new AsyncCallback(OnEndJoin), null);

				}
				catch (Exception ex)
				{
					logger.Error(_pid + " : " +ex.Message);
					RetryTask();
				}

			}

		}

		public void Disconnect()
		{
			try
			{
				if (proxy != null)
				{
					clientInfo = new IVisionService.Topic();
					clientInfo.Groupid = _gid;
					clientInfo.Mediaid = _pid;
					clientInfo.Level = IVisionService.UserLevels.Media;

					logger.Info("Disconnect start!");
					try
					{
						proxy.Disconnect(this.clientInfo);
						proxy = null;
					}
					catch
					{
						proxy = null;
					}
					finally
					{
						DetachProxy();
						bAuth = false;
						_connectedTime = null;
					}

					logger.Info(_pid + " : Disconnected");

				}
			}
			catch (Exception e)
			{
				logger.Error(_pid + " : " +e.Message);
				DetachProxy();
				bAuth = false;
			}
		}

		private void OnEndJoin(IAsyncResult iar)
		{
			try
			{
				if (proxy != null)
				{
					if (ProxyState != CommunicationState.Opened)
					{
						RetryTask();
					}
					else
					{

						//	성공
						if (bAuth)
						{
							if (connectToServer() == 0)
							{
								_connectedTime = null;
								_connectedTime = new Nullable<DateTime>(DateTime.Now);
							}
							else
							{
								logger.Warn("The [ConnectToServer] is failed.");

								RetryTask();
							}
						}
						else
						{
							logger.Warn("The [Authoring] is failed.");

							RetryTask();
						}
					}
				}
				else
				{
					logger.Warn(_pid + " : The [proxy] is NULL.");

					// 다시 접속
					RetryTask();
				}
			}
			catch (Exception e)
			{
				logger.Error(_pid + " : " + e.Message);
				RetryTask();
			}
		}

		public void RetryTask()
		{
			try
			{
				Disconnect();
				bAuth = false;
			}
			catch (Exception e)
			{
				logger.Error(_pid + " : " + e.Message);
			}

		}
		#endregion

		#region .Net Remoting
		ITaskList serverTaskList = null;
		IGroupList serverGroupList = null;
		IPlayersList serverPlayerList = null;
		IServerCmdReceiver serverCmdReceiver = null;
		ILogList serverLogList = null;


		public int connectToServer()
		{
			try
			{
				if (bAuth == false)
				{
					throw new Exception("WCF has not been connected.");
				}
				String ServerPath = _hostname;

				if (!ServerPath.StartsWith("tcp://"))
					ServerPath = "tcp://" + ServerPath + ":888";
				if (!ServerPath.EndsWith("/"))
					ServerPath += "/";

				// initializing remote interfaces
				serverTaskList = (ITaskList)Activator.GetObject(
				  typeof(ITaskList), ServerPath + "TaskListHost/rt");
				serverGroupList = (IGroupList)Activator.GetObject(
					typeof(IGroupList), ServerPath + "GroupListHost/rt");
				serverPlayerList = (IPlayersList)Activator.GetObject(
					typeof(IPlayersList), ServerPath + "PlayerListHost/rt");
				serverCmdReceiver = (IServerCmdReceiver)Activator.GetObject(
                    typeof(IServerCmdReceiver), ServerPath + "CmdReceiverHost/rt");
				serverLogList = (ILogList)Activator.GetObject(
					typeof(ILogList), ServerPath + "LogListHost/rt");

				return receiveNetworkConfiguration();
			}
			catch (Exception e)
			{
				logger.Error(e + "");
			}
			return -1;
		}

		public int receiveNetworkConfiguration()
		{
			try
			{
				if (bAuth == false)
				{
					throw new Exception("WCF has not been connected.");
				}
				string grouplist = serverGroupList.GetGroupsXmlByPID(_pid);
				if (!String.IsNullOrEmpty(grouplist))
				{
					string gids = "";
					XmlDocument doc = new XmlDocument();
					doc.LoadXml(grouplist);
					XmlNodeList list = doc.SelectNodes("//child::group");
					foreach(XmlNode node in list)
					{
						gids += String.Format("{0}|", node.Attributes["gid"].Value);
					}
					_gid = gids.TrimEnd('|');
				}
				else
				{
					//	Server에 Player가 없음 
					throw new Exception("There is no [Player ID] in serverDB. (PID=" + _pid + ")");
				}

				logger.Info("Player (GID=" + _gid + ", PID=" + _pid + ")");

				return 0;
			}
			catch (System.Net.Sockets.SocketException sockex)
			{
				return 0;
			}
			catch (Exception e)
			{
				logger.Error(e.ToString());
			}
			return -1;
		}

		private void startSynchronize()
		{
			try
			{
				// updating uptime data

				//	서버의 Task 오브젝트의 TimeStamp를 가져온다.
				long server_ts = serverTaskList.GetTasksTimestamp();

				if(server_ts > lastSync)
				{

					ServerDataSet.tasksDataTable newTasks = serverTaskList.GetNewTasks(lastSync, _pid, _gid);

					if (newTasks.Rows.Count > 0)
						logger.Info("Found " + newTasks.Rows.Count + " new tasks");

					lastSync = server_ts;

					newTasks.Dispose();
				}


				return;
			}
			catch (System.Net.Sockets.SocketException sockex)
			{
				logger.Error(sockex.Message);
				connectToServer();
			}
			catch (Exception e)
			{
				logger.Error(e.Message);
				RetryTask();
			}
		}
		#endregion

		public bool SendStatusToServer()
		{
			bool bRet = false;
			if (proxy != null && bAuth)
			{
				try
				{
					if (ProxyState == CommunicationState.Opened)
					{
						TimeSpan ts = (DateTime.Now - _connectedTime.Value);
						IVisionService.Message msg = new IVisionService.Message();
						msg.Sender = new IVisionService.Topic();
						msg.Sender.Groupid = _gid;
						msg.Sender.Mediaid = _pid;
						msg.Sender.Level = IVisionService.UserLevels.Media;
						msg.MsgType = IVisionService.MessageType.msg_Status;
						msg.Content = String.Format("65537|983408|150224624|{0}|0|ATECPC|10|40|0/0|0/|LG_BS_CM| ", Convert.ToInt32(ts.TotalMinutes));
						proxy.SendMessage(msg);
						bRet = true;
					}
					else
					{
						logger.Error(ProxyState.ToString());
						bRet = false;

					}
				}
				catch (Exception ex)
				{
					logger.Error(_pid + " : " + ex.Message);
					bRet = false;
				}
			}

			return bRet;
		}
		#endregion

		#region Callback Members

		public void RefreshClients(IVisionService.Topic[] clients)
		{
		}

		public void Receive(IVisionService.Message msg)
		{
		}

		public void ReceiveWhisper(IVisionService.Message msg, IVisionService.Topic receiver)
		{
			if (msg != null)
			{
				logger.Info(_pid + " : " + "Received message from server: " + msg.Result + ", " + msg.Content);
				switch (msg.Result)
				{
					case "0000": //	인증 성공
						bAuth = true;
						logger.Info("The authoring is succeed. (GID=" + receiver.Groupid + "PID=" + receiver.Mediaid+ ")");
						break;
					case "0001": //	DB에 없었으나 추가 됨
						Disconnect();
						bAuth = false;
						logger.Info("No media or group in the server DB. (GID=" + receiver.Groupid + "PID=" + receiver.Mediaid + ")");

						break;
					case "0002": //	중복되었지만 PID변경됨
						Disconnect();
						bAuth = false;
						logger.Info("Already added, authored media. (GID=" + receiver.Groupid + "PID=" + receiver.Mediaid + ")");

						break;
					case "0003": //	서버에서 접근 제한 (플레이어 개수 한도 초과)
						Disconnect();
						bAuth = false;
						logger.Info("It was disconnected because of an excess of the allowed maximum count. (GID=" + receiver.Groupid + "PID=" + receiver.Mediaid + ")");
						break;
					case "0004": //	Download Time Zone XML.
						logger.Info("Server has been disconnected. (PID=" + receiver.Mediaid + ")");
						bAuth = false;
						RetryTask();
						break;
					case "0005": // Modified Player ID.
						logger.Info("Current PID : " + receiver.Mediaid);
						break;
					case "0006": // Modified Group ID.
						logger.Info(_pid + " : " + "Current GID : " + receiver.Groupid);
						break;
					case "0011": // Modified Group ID.
						logger.Info("Download Timer Schedule has been arrived. :" + _pid);
						break;

				}
			}
		}

		public void EndReceiveWhisper(System.IAsyncResult result)
		{
		}

		public void IsWritingCallback(IVisionService.Topic client)
		{
		}

		public void EndIsWritingCallback(System.IAsyncResult result)
		{
		}

		public void ReceiverFile(IVisionService.FileMessage fileMsg, IVisionService.Topic receiver)
		{
		}

		public void UserJoin(IVisionService.Topic client)
		{
		}

		public void UserLeave(IVisionService.Topic client)
		{
		}

		public void EndRefreshClients(System.IAsyncResult result)
		{
		}

		public void EndReceive(System.IAsyncResult result)
		{
		}

		public void EndReceiverFile(System.IAsyncResult result)
		{
		}

		public void EndUserJoin(System.IAsyncResult result)
		{
		}

		public void EndUserLeave(System.IAsyncResult result)
		{
		}

		public System.IAsyncResult BeginReceiveWhisper(IVisionService.Message msg, IVisionService.Topic receiver, System.AsyncCallback callback, object asyncState)
		{
			throw new NotImplementedException();
		}

		public System.IAsyncResult BeginIsWritingCallback(IVisionService.Topic client, System.AsyncCallback callback, object asyncState)
		{
			throw new NotImplementedException();
		}

		public System.IAsyncResult BeginReceiverFile(IVisionService.FileMessage fileMsg, IVisionService.Topic receiver, System.AsyncCallback callback, object asyncState)
		{
			throw new NotImplementedException();
		}

		public System.IAsyncResult BeginUserJoin(IVisionService.Topic client, System.AsyncCallback callback, object asyncState)
		{
			throw new NotImplementedException();
		}

		public System.IAsyncResult BeginReceive(IVisionService.Message msg, System.AsyncCallback callback, object asyncState)
		{
			throw new NotImplementedException();
		}

		public System.IAsyncResult BeginUserLeave(IVisionService.Topic client, System.AsyncCallback callback, object asyncState)
		{
			throw new NotImplementedException();
		}

		public System.IAsyncResult BeginRefreshClients(IVisionService.Topic[] clients, System.AsyncCallback callback, object asyncState)
		{
			throw new NotImplementedException();
		}

		#endregion Callback Members

	}
}
