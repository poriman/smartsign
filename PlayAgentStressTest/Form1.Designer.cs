﻿namespace PlayAgentStressTest
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label2 = new System.Windows.Forms.Label();
			this.tb_start_pid = new System.Windows.Forms.TextBox();
			this.tb_end_pid = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.btn_create = new System.Windows.Forms.Button();
			this.panel1 = new System.Windows.Forms.Panel();
			this.textRefresh = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.textHostName = new System.Windows.Forms.TextBox();
			this.tabResult = new System.Windows.Forms.TabControl();
			this.tabStatus = new System.Windows.Forms.TabPage();
			this.textStatus = new System.Windows.Forms.TextBox();
			this.tabLog = new System.Windows.Forms.TabPage();
			this.textLog = new System.Windows.Forms.TextBox();
			this.panel1.SuspendLayout();
			this.tabResult.SuspendLayout();
			this.tabStatus.SuspendLayout();
			this.tabLog.SuspendLayout();
			this.SuspendLayout();
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(12, 36);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(68, 12);
			this.label2.TabIndex = 1;
			this.label2.Text = "플레이어 ID";
			// 
			// tb_start_pid
			// 
			this.tb_start_pid.Location = new System.Drawing.Point(90, 33);
			this.tb_start_pid.MaxLength = 7;
			this.tb_start_pid.Name = "tb_start_pid";
			this.tb_start_pid.Size = new System.Drawing.Size(100, 21);
			this.tb_start_pid.TabIndex = 2;
			this.tb_start_pid.Text = "1000000";
			// 
			// tb_end_pid
			// 
			this.tb_end_pid.Location = new System.Drawing.Point(210, 33);
			this.tb_end_pid.MaxLength = 7;
			this.tb_end_pid.Name = "tb_end_pid";
			this.tb_end_pid.Size = new System.Drawing.Size(100, 21);
			this.tb_end_pid.TabIndex = 3;
			this.tb_end_pid.Text = "1000699";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(194, 44);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(14, 12);
			this.label3.TabIndex = 7;
			this.label3.Text = "~";
			// 
			// btn_create
			// 
			this.btn_create.Location = new System.Drawing.Point(316, 31);
			this.btn_create.Name = "btn_create";
			this.btn_create.Size = new System.Drawing.Size(75, 23);
			this.btn_create.TabIndex = 4;
			this.btn_create.Text = "생성하기";
			this.btn_create.UseVisualStyleBackColor = true;
			this.btn_create.Click += new System.EventHandler(this.btn_create_Click);
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.textRefresh);
			this.panel1.Controls.Add(this.label4);
			this.panel1.Controls.Add(this.label1);
			this.panel1.Controls.Add(this.textHostName);
			this.panel1.Controls.Add(this.tabResult);
			this.panel1.Controls.Add(this.label2);
			this.panel1.Controls.Add(this.btn_create);
			this.panel1.Controls.Add(this.tb_start_pid);
			this.panel1.Controls.Add(this.tb_end_pid);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(397, 423);
			this.panel1.TabIndex = 10;
			// 
			// textRefresh
			// 
			this.textRefresh.Location = new System.Drawing.Point(279, 6);
			this.textRefresh.MaxLength = 2;
			this.textRefresh.Name = "textRefresh";
			this.textRefresh.Size = new System.Drawing.Size(31, 21);
			this.textRefresh.TabIndex = 1;
			this.textRefresh.Text = "15";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(214, 9);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(59, 12);
			this.label4.TabIndex = 13;
			this.label4.Text = "갱신 Sec.";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(12, 9);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(44, 12);
			this.label1.TabIndex = 11;
			this.label1.Text = "서버 IP";
			// 
			// textHostName
			// 
			this.textHostName.Location = new System.Drawing.Point(90, 6);
			this.textHostName.MaxLength = 16;
			this.textHostName.Name = "textHostName";
			this.textHostName.Size = new System.Drawing.Size(118, 21);
			this.textHostName.TabIndex = 0;
			this.textHostName.Text = "211.192.178.198";
			// 
			// tabResult
			// 
			this.tabResult.Controls.Add(this.tabStatus);
			this.tabResult.Controls.Add(this.tabLog);
			this.tabResult.Location = new System.Drawing.Point(3, 60);
			this.tabResult.Name = "tabResult";
			this.tabResult.SelectedIndex = 0;
			this.tabResult.Size = new System.Drawing.Size(396, 367);
			this.tabResult.TabIndex = 5;
			// 
			// tabStatus
			// 
			this.tabStatus.Controls.Add(this.textStatus);
			this.tabStatus.Location = new System.Drawing.Point(4, 21);
			this.tabStatus.Name = "tabStatus";
			this.tabStatus.Padding = new System.Windows.Forms.Padding(3);
			this.tabStatus.Size = new System.Drawing.Size(388, 342);
			this.tabStatus.TabIndex = 0;
			this.tabStatus.Text = "상태";
			this.tabStatus.UseVisualStyleBackColor = true;
			// 
			// textStatus
			// 
			this.textStatus.Dock = System.Windows.Forms.DockStyle.Fill;
			this.textStatus.Location = new System.Drawing.Point(3, 3);
			this.textStatus.Multiline = true;
			this.textStatus.Name = "textStatus";
			this.textStatus.ScrollBars = System.Windows.Forms.ScrollBars.Both;
			this.textStatus.Size = new System.Drawing.Size(382, 336);
			this.textStatus.TabIndex = 6;
			// 
			// tabLog
			// 
			this.tabLog.Controls.Add(this.textLog);
			this.tabLog.Location = new System.Drawing.Point(4, 21);
			this.tabLog.Name = "tabLog";
			this.tabLog.Padding = new System.Windows.Forms.Padding(3);
			this.tabLog.Size = new System.Drawing.Size(388, 342);
			this.tabLog.TabIndex = 1;
			this.tabLog.Text = "로그";
			this.tabLog.UseVisualStyleBackColor = true;
			// 
			// textLog
			// 
			this.textLog.Dock = System.Windows.Forms.DockStyle.Fill;
			this.textLog.Location = new System.Drawing.Point(3, 3);
			this.textLog.Multiline = true;
			this.textLog.Name = "textLog";
			this.textLog.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.textLog.Size = new System.Drawing.Size(382, 336);
			this.textLog.TabIndex = 7;
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(397, 423);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.panel1);
			this.Name = "Form1";
			this.Text = "PlayAgent 생성기";
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.tabResult.ResumeLayout(false);
			this.tabStatus.ResumeLayout(false);
			this.tabStatus.PerformLayout();
			this.tabLog.ResumeLayout(false);
			this.tabLog.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox tb_start_pid;
		private System.Windows.Forms.TextBox tb_end_pid;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Button btn_create;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.TabControl tabResult;
		private System.Windows.Forms.TabPage tabStatus;
		private System.Windows.Forms.TextBox textStatus;
		private System.Windows.Forms.TabPage tabLog;
		private System.Windows.Forms.TextBox textLog;
		private System.Windows.Forms.TextBox textRefresh;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox textHostName;
	}
}

