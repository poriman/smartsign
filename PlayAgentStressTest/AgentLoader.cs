﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace PlayAgentStressTest
{
	class AgentLoader
	{
		public AgentLoader(string hostname, int start_pid, int end_pid, int refresh)
		{
			_start_pid = start_pid;
			_end_pid = end_pid;
			_hostname = hostname;
			_refresh = refresh;
		}


		#region Private Properties
		/// <summary>
		/// 가상 에이전트를 관리할 Collection 객체
		/// </summary>
		private Collection<VirtualAgent> _arrAgents = new Collection<VirtualAgent>();
		private bool _isStarted = false;
		private int _loaded = 0;
		private int _start_pid = 0;
		private int _end_pid = 0;
		private int _refresh = 15;
		private string _hostname;
		
		#endregion

		#region Public Properties

		public int LoadedAgentsCount { get { return _loaded; } }
		public bool IsStarted { get { return _isStarted; } }

		#endregion

		#region Functions
		
		public void Start()
		{
			if (_isStarted)
				return;

			for(int i = _start_pid ; i <= _end_pid ; ++i)
			{
				VirtualAgent agent = new VirtualAgent(_hostname, i.ToString("D7"), _refresh);
				agent.Start();

				_arrAgents.Add(agent);
				_loaded++;

			}

			_isStarted = true;
		}
		public void Stop()
		{
			if (!_isStarted)
				return;

			foreach (VirtualAgent agent in _arrAgents)
			{
				agent.Stop();
			}

			_arrAgents.Clear();
			_loaded = 0;

			_isStarted = false;
		}
		
		#endregion
	}
}
