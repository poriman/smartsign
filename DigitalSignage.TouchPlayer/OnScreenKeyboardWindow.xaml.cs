﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Diagnostics;

namespace DigitalSignage.TouchPlayer
{
    public delegate void ShowOnScreenKeyboardDelegate();
    /// <summary>
    /// Interaction logic for UserTouchWindow.xaml
    /// </summary>
    public partial class OnScreenKeyboardWindow : Window
    {
        public bool isShowKeyboardState { get; set; }
        public bool IsTimeScheduleProgram { get; set; }
        public ShowOnScreenKeyboardDelegate ShowOnScreenKeyboardHandler;
        private DispatcherTimer _TouchProjectTimer = new DispatcherTimer();
        private Stopwatch _Stopwatch = new Stopwatch();
        public static ScheduleType ScheduleType { get; set; }

        public OnScreenKeyboardWindow()
        {
            InitializeComponent();

            this.WindowStartupLocation = WindowStartupLocation.Manual;
            this.Left = SystemParameters.FullPrimaryScreenWidth - keyboardImg.Width;
            this.Top = SystemParameters.FullPrimaryScreenHeight - keyboardImg.Height;
            this.Width = 180;
            this.Height = 50;
            this.Opacity = 0.3;

            this._TouchProjectTimer.Interval = TimeSpan.FromMilliseconds(1000);
            this._TouchProjectTimer.Tick += new EventHandler(TouchProjectTimer_Tick);
            this.Loaded += (s, e) =>
            {
                this._TouchProjectTimer.Start();
                this._Stopwatch.Start();
            };
            this.Show();
            this.isShowKeyboardState = false;
        }
        
        /// <summary>
        /// 키보드 실행 팝업윈도우 숨기기
        /// 2012.02.17: Overlay Screen에서 Off Screen으로 전환시 Off Screen의 동영상이 버벅거림 현상이 발생하는 문제로 수정
        /// 원인: Off Screent상태에서 키보드 팝업윈도우가 Opacity 값이 0으로 띄워져서 보이지 않은 상태에서 발생.
        /// </summary>
        public void HideKeyboardIconWindow()
        {
            if (this.Visibility == System.Windows.Visibility.Visible)
            {
                this._TouchProjectTimer.Stop();
                this._Stopwatch.Stop();
                this._Stopwatch.Reset();
                this.Opacity = 0.0;               
                this.Visibility = System.Windows.Visibility.Collapsed;
            }            
            this.isShowKeyboardState = false;
        }

        public void ShowKeyboardIconWindow()
        {
            if (this.Visibility != System.Windows.Visibility.Visible)
            {
                this.WindowStartupLocation = WindowStartupLocation.Manual;
                this.Left = SystemParameters.FullPrimaryScreenWidth - keyboardImg.Width;
                this.Top = SystemParameters.FullPrimaryScreenHeight - keyboardImg.Height;
                this.Width = 180;
                this.Height = 50;
                this.Opacity = 0.3;
                ResetUserTouchCheckTimer();
                this.Visibility = System.Windows.Visibility.Visible;
            }
            this.isShowKeyboardState = true;            
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (ShowOnScreenKeyboardHandler != null)
                ShowOnScreenKeyboardHandler();
        }       

        public void ResetUserTouchCheckTimer()
        {
            this._Stopwatch.Reset();
            this._Stopwatch.Start();
            if (ScheduleType == ScheduleType.DefaultTouchSchedule || ScheduleType == ScheduleType.TimeTouchSchedule)
            {
                this._TouchProjectTimer.Start();
            }
        }

        public void CloseKeyboardIconWindow()
        {
            this._TouchProjectTimer.Stop();
            this._TouchProjectTimer = null;
            this._Stopwatch.Stop();
            this.Close();
        }

        void TouchProjectTimer_Tick(object sender, EventArgs e)
        {
            DispatcherTimer dispathcherTimer = sender as DispatcherTimer;
            if (dispathcherTimer != null)
            {
                if (this._Stopwatch.Elapsed.Seconds > 10)//Touch Project Time 을 넘겼을 경우에 대해 처리한다.
                {
                    if (this.isShowKeyboardState == true)
                        HideKeyboardIconWindow();
                }
                else
                {
                    if (this.isShowKeyboardState == false)
                    {                        
                        ShowKeyboardIconWindow();
                    }
                }
            }
        }      
    }
}
