﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DigitalSignage.TouchPlayer
{
    /// <summary>
    /// Interaction logic for UserTouchMsg.xaml
    /// </summary>
    public partial class UserTouchMsg : Window
    {
        public delegate void CloseWindowDelegate();
        public delegate void OkWindowDelegate();
        public CloseWindowDelegate CloseWindowHandler;
        public OkWindowDelegate OkWindowHandler;
        public bool IsShowWindow { get; private set; }

        public UserTouchMsg()
        {
            InitializeComponent();

            //CloseWindowHandler = new CloseWindowDelegate();
        }

        private void MsgYesButton_Click(object sender, RoutedEventArgs e)
        {
            //this.DialogResult = true;
            if (OkWindowHandler != null)
            {
                OkWindowHandler();
            }

            this.CloseWindow();
        }

        private void MsgNoButton_Click(object sender, RoutedEventArgs e)
        {
            this.CloseWindow();
        }

        public void ShowWindow()
        {
            this.IsShowWindow = true;
            this.Show();
        }

        public void CloseWindow()
        {
            this.IsShowWindow = false;
            this.Close();
        }
    }
}
