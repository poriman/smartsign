﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace DigitalSignage.TouchPlayer
{


    public interface ITouchPlayer
    {
        //string TouchPlayerProgramPath { get; set; }
        ChangePaneScreenDelegate ChangePaneScreen { get; set; }
        ChangedTouchPageDelegate ChangedTouchPageHandler { get; set; }
        SetPlayStateTouchProjectDelegate SetPlayStateTouchProjectHandler { get; set; }
        RaisedUserTouchDelegate RaisedUserTouchHandler { get; set; }
        UIElement LoadTouchProject(string touchProjectFile, Grid playerGrid, Grid primaryBuffer, double touchScheduleTime);
        void PlayTouchProject();
        void ClosedTouchScheduleProgram(Grid playlistPlayer);
        void ClosedPreviousTouchScheduleProgram();
        void SetTouchWindowSize(Size size);
        void CreateTouchScreen(Grid grid, Dictionary<string, object> paneScreenItems, UIElement windowOwner);
        void DisplayTSView(string paneScreenID, object playlistPlayer);
        bool LoadProgram();
        void SetUserTouchWindow(Window owner);
        void ShowHideUserTouchWindow(bool isShow);

        //void SetOnScreenKeyboardWindow(Window owner);
        void ShowHideOnScreenKeyboardWindow(bool isShow);

        bool IsTouchWindow { get; set; }
        bool IsTimeScheduleProgram { get; set; }
        ScheduleType ScheduleType { get; set; }
        void CloseMsgWindow();
        void SizeChangedScreen(Size size);
        void ClosediVisionPlayer();
        void InitTochPlayer(Window player);//SmartSign의 TouchManager Agent를 초기화한다.
        void ChangePlayState(bool isDefaultProgram, Grid playlistPlayer);
    }
}
