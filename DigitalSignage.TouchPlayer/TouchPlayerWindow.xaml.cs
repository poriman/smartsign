﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Xml.Linq;
using InSysTouchflowData.Models.ProjectModels;
using InSysTouchflowData;
using InSysDSDisplayer.Views;
using System.Windows.Interop;
using System.Windows.Forms;
using InSysTouchPlayer;
using NLog;

namespace DigitalSignage.TouchPlayer
{
    public delegate void ChangePaneScreenDelegate(string paneScreenID, object paneScreenView, bool isLinked, bool isScreenRepeat);
    public delegate void ChangedTouchPageDelegate(UIElement changedPage, UIElement playerScreen);
    public delegate UIElement LoadTouchProjectDelegate(string touchProjectFile, Grid playerGrid);
    public delegate void PlayTouchProjectDelegate();
    public delegate void SetPlayStateTouchProjectDelegate(bool isPlayState, Grid playlistPlayer);
    public delegate void RaisedUserTouchDelegate();

    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class TouchPlayerWindow : Window, ITouchPlayer
    {
        #region Private Members

        private string ProgramFullPath { get; set; }
        private Program program;
        private SendTargetTSScreenIDDelegate SendTargetTSScreenID;
        private TouchProjectInfo touchProjectInfo;
        private OnScreenKeyboardWindow onScreenKeyboardWindow;
        private List<TFPlayerManager> touchManagerList = new List<TFPlayerManager>();       
        public bool IsTouchSchedulePlaying = false;       
        private Size playerWindowSize;
        private Window iVisionPlayerWindow;

        /// <summary>
        /// 로컬 로깅 Instance
        /// </summary>
        private static Logger logger = LogManager.GetCurrentClassLogger();

        #endregion

        #region Singleton 생성자

        private static readonly TouchPlayerWindow theInstance;

        public static TouchPlayerWindow TheOnlyTSAgent
        {
            get { return theInstance; }
        }

        static TouchPlayerWindow()
        {
            theInstance = new TouchPlayerWindow();
        }

        private TouchPlayerWindow()
        {
            InitializeComponent();

            this.StartWndProcHandler();

            this.Closed += (s, e) =>
            {
                if (onScreenKeyboardWindow != null)
                {
                    onScreenKeyboardWindow.CloseKeyboardIconWindow();
                }
            };
        }

        ~TouchPlayerWindow()
        {
            this.StopWndProcHandler();
        }

        #endregion

        #region ITouchPlayer Members

        //string ITouchPlayer.TouchPlayerProgramPath { get; set; }

        ChangePaneScreenDelegate ITouchPlayer.ChangePaneScreen
        {
            get;
            set;
        }

        ChangedTouchPageDelegate ITouchPlayer.ChangedTouchPageHandler
        {
            get;
            set;
        }

        SetPlayStateTouchProjectDelegate ITouchPlayer.SetPlayStateTouchProjectHandler
        {
            get;
            set;
        }

        RaisedUserTouchDelegate ITouchPlayer.RaisedUserTouchHandler
        {
            get;
            set;
        }

        UserTouchMsg utm = null;
        bool ITouchPlayer.IsTimeScheduleProgram { get; set; }

        bool ITouchPlayer.IsTouchWindow { get; set; }
        private object lockObject = new object();

        /// <summary>
        /// 플래쉬 등 WindowFormHost를 사용하는 컨트롤 때문에 별도 투명윈도우를 띄워서 사용자 클릭이벤트를 캡쳐한다.
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="isShow"></param>
        void ITouchPlayer.SetUserTouchWindow(Window owner)
        {           
        }


        void ITouchPlayer.ShowHideUserTouchWindow(bool isShow)
        {
            lock (lockObject)
            {
                ITouchPlayer touchPlayer = this as ITouchPlayer;
                if (touchPlayer != null)
                {                    
                    OnScreenKeyboardWindow.ScheduleType = touchPlayer.ScheduleType;
                }
            }
        }

        /// <summary>
        /// SmartSign의 TouchManager Agent를 초기화한다.
        /// </summary>
        /// <param name="player">SmartSign Player 윈도우</param>
        void ITouchPlayer.InitTochPlayer(Window player)
        {
            this.iVisionPlayerWindow = player;
            playerWindowSize = new Size(player.ActualWidth, player.ActualHeight);//Player의 사이즈 할당.

            if (onScreenKeyboardWindow == null)//Screen Keyboard 팝업 윈도우 초기화.
            {
                this.onScreenKeyboardWindow = new OnScreenKeyboardWindow();
                this.onScreenKeyboardWindow.Owner = this.iVisionPlayerWindow;  
               
                InSysTouchPlayer.TFPlayerManager.TouchDown(new TouchDownDelegate(() =>
                {
                    if (this.onScreenKeyboardWindow != null)
                        this.onScreenKeyboardWindow.ResetUserTouchCheckTimer();
                }
                ));
            }
        }

        void SetOnScreenKeyboardWindow(Window owner)
        {
            if (onScreenKeyboardWindow == null)
            {
                onScreenKeyboardWindow = new OnScreenKeyboardWindow();
                this.onScreenKeyboardWindow.Owner = owner;
               
                InSysTouchPlayer.TFPlayerManager.TouchDown(new TouchDownDelegate(() =>
                {
                    if (onScreenKeyboardWindow != null)
                        onScreenKeyboardWindow.ResetUserTouchCheckTimer();
                }
                ));
            }
        }

        void ITouchPlayer.ShowHideOnScreenKeyboardWindow(bool isShow)
        {
            if (onScreenKeyboardWindow != null)
            {
                ITouchPlayer touchPlayer = this as ITouchPlayer;
                onScreenKeyboardWindow.IsTimeScheduleProgram = touchPlayer.IsTimeScheduleProgram;
                OnScreenKeyboardWindow.ScheduleType = touchPlayer.ScheduleType;
                if (isShow == true)
                {
                    onScreenKeyboardWindow.ShowKeyboardIconWindow();
                }
                else
                {
                    onScreenKeyboardWindow.HideKeyboardIconWindow();
                }
            }
        }

        void ITouchPlayer.CloseMsgWindow()
        {         
        }

        void ITouchPlayer.SetTouchWindowSize(Size size)
        {
            this.Width = size.Width;
            this.Height = size.Height;
        }

        /// <summary>
        /// NEPlayer 화면에 Display되는 Program의 Pane 구성 정보를 분석하여 Main Touch Screen Widnow에 적용한다.
        /// </summary>
        /// <param name="grid">NEPlayer 화면을 구성하는 Root Grid</param>
        void ITouchPlayer.CreateTouchScreen(Grid grid, Dictionary<string, object> paneScreenItems, UIElement windowOwner)
        {           
        }

        /// <summary>
        /// 현재 NEPlayer에 로드되어 있는 하나의 Pane Screen을 Display한다.
        /// </summary>
        /// <param name="screenID">Playlist의 ID</param>
        /// <param name="playlistPlayer">Pnae Screen을 구성하는 PlaylistPlayer 인스턴스</param>
        void ITouchPlayer.DisplayTSView(string paneScreenID, object playlistPlayer)
        {

        }

        /// <summary>
        /// Program 파일 로드시 NEPlayer에서 호출된다.
        /// Main Touch Screen Widnow를 생성한다.
        /// </summary>
        /// <param name="loadProgramFilePath"></param>
        bool ITouchPlayer.LoadProgram()
        {
            if (string.IsNullOrEmpty(ProgramFullPath) == true)
                return false;

            if (CheckFileValidation(ProgramFullPath) == false)
            {
                return false;
            }

            try
            {
                if (program != null)
                    program = null;

                program = new Program(ProgramFullPath, new Size(this.Width, this.Height));


                this.SendTargetTSScreenID = new SendTargetTSScreenIDDelegate(SendTargetTSScreenIDFunc);
                this.CreateTSWorkflowProcess(GetTSMappingFilePath());

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }


        UIElement ITouchPlayer.LoadTouchProject(string touchProjectFile, Grid playerGrid, Grid primaryBuffer, double touchScheduleTime)
        {
            TFPlayerManager touchPlayerManager = new TFPlayerManager();
            touchPlayerManager.InitTouch();
            touchPlayerManager.ChangedTouchPageHandler = new InSysDSDisplayer.ChangedTouchPageDelegate(ChangedTouchPage);
            touchPlayerManager.PassedProjectLifeTimeHandler = new InSysDSDisplayer.PassedProjectLifeTimeDelegate(PassedProjectLifeTime);
            touchPlayerManager.PlayerScreen = playerGrid;//현재 Play 중인 Grid 컨트롤을 저장한다..  
            touchPlayerManager.PrimaryBuffer = primaryBuffer;//실제 PageView를 담는 Grid
            touchPlayerManager.SetPlayerContainerSize(this.playerWindowSize);
            touchPlayerManager.SetPlayerWindowState(this.iVisionPlayerWindow, this.iVisionPlayerWindow.WindowState);

            if (this.onScreenKeyboardWindow != null)
            {
                this.onScreenKeyboardWindow.ShowOnScreenKeyboardHandler = () =>
                {
                    if (touchPlayerManager != null) 
                        touchPlayerManager.ShowOnScreenKeyboard(null, onScreenKeyboardWindow);
                };
            }
            
            Dictionary<string, object> touchProjectData = touchPlayerManager.LoadTouchProjectData(touchProjectFile, touchScheduleTime);
            if (touchProjectData != null)
            {
                touchProjectInfo = touchProjectData["TouchflowData"] as TouchProjectInfo;
                if (touchProjectInfo != null)
                {
                    UIElement touchScreenView = touchPlayerManager.GetRootTouchPage();
                    if (touchScreenView != null)
                    {
                        ITouchPlayer touchPlayer = this as ITouchPlayer;
                        //touchPlayer.PlayTouchProject();

                        if (touchPlayer.SetPlayStateTouchProjectHandler != null)
                            touchPlayer.SetPlayStateTouchProjectHandler(true, null);

                        touchPlayerManager.PlayTouch();
                        this.touchManagerList.Add(touchPlayerManager);
                        IsTouchSchedulePlaying = true;// 현재 Playlist가 터치임을 설정한다.

                        return touchScreenView;
                    }
                }
            }            

            touchPlayerManager.SetOnScreenKeyboardParent(this.onScreenKeyboardWindow);
            return null;
        }


        void ITouchPlayer.PlayTouchProject()
        {           
            //touchPlayerManager.PlayTouch();

            ITouchPlayer touchPlayer = this as ITouchPlayer;          

            touchPlayer.IsTouchWindow = false;
            if (touchPlayer.ScheduleType == ScheduleType.TimeSchedule)
            {
                touchPlayer.ScheduleType = ScheduleType.TimeTouchSchedule;
                touchPlayer.ShowHideUserTouchWindow(false);
                touchPlayer.ShowHideOnScreenKeyboardWindow(true);
            }


            //touchPlayer.ShowTransparentTouchWindow(true);
        }

        void ITouchPlayer.ClosedTouchScheduleProgram(Grid playlistPlayer)
        {
            TFPlayerManager tfPlayerManager = this.touchManagerList.Where(o => o.PlayerScreen.Equals(playlistPlayer) == true).FirstOrDefault();
            if (tfPlayerManager != null)
            {
                
                tfPlayerManager.CloseOnScreenKeyboardParent();
                tfPlayerManager.ClosedCurrentTouchSchedule();
                this.touchManagerList.Remove(tfPlayerManager);
                tfPlayerManager = null;
                IsTouchSchedulePlaying = false;// 이전 Playlist가 터치인 경우를 포함한다.              
            }

            //ITouchPlayer touchPlayer = this as ITouchPlayer;
            //if (touchPlayer.SetPlayStateTouchProjectHandler != null)
            //    touchPlayer.SetPlayStateTouchProjectHandler(false, playlistPlayer);
        }

        void ITouchPlayer.ChangePlayState(bool isDefaultProgram, Grid playlistPlayer)
        {
            ITouchPlayer touchPlayer = this as ITouchPlayer;
            if (touchPlayer.SetPlayStateTouchProjectHandler != null)
            {
                touchPlayer.SetPlayStateTouchProjectHandler(isDefaultProgram, playlistPlayer);
            }
        }

        void ITouchPlayer.ClosedPreviousTouchScheduleProgram()
        {
            //if (touchPlayerManager != null)
            //{
            //    touchPlayerManager.ClosedPreviousTouchScheduleProgram();
            //}
        }

        void ITouchPlayer.ClosediVisionPlayer()
        {
            this.Close();
        }

        ScheduleType ITouchPlayer.ScheduleType { get; set; }

        void ITouchPlayer.SizeChangedScreen(Size size)
        {
            foreach(var touchmanager in this.touchManagerList)
                touchmanager.SizeChangedScreen(size);

            playerWindowSize = size;
        }

        #endregion


        #region Private Methods

        
        private void ChangedTouchPage(UIElement playlistPlayer, UIElement changedPage)
        {
            TFPlayerManager tfPlayerManager = this.touchManagerList.Where(o => o.PlayerScreen.Equals(playlistPlayer) == true).FirstOrDefault();
            if (tfPlayerManager != null)
            {
                if (tfPlayerManager.PlayerScreen != null)
                {
                    Grid grid = tfPlayerManager.PrimaryBuffer as Grid;// tfPlayerManager.PlayerScreen as Grid;
                    if (grid != null)
                    {
                        //foreach (var child in grid.Children)
                        //{
                        //    if (child is Grid)
                        //    {
                        //        (child as Grid).Children.Clear();
                        //    }
                        //}
                        grid.Children.Clear();
                        grid.Children.Add(changedPage);
                        grid.UpdateLayout();
                    }
                }

                ITouchPlayer touchPalyer = this as ITouchPlayer;
                if (touchPalyer.ChangedTouchPageHandler != null)
                    touchPalyer.ChangedTouchPageHandler(changedPage, tfPlayerManager.PlayerScreen);
            }           
        }
        

        /// <summary>
        /// Project Time이 종료되면 자동 호출된다..
        /// </summary>
        private void PassedProjectLifeTime(UIElement playlistPlayer)
        {
            try
            {
                ITouchPlayer touchPalyer = this as ITouchPlayer;
                touchPalyer.ClosedTouchScheduleProgram(playlistPlayer as Grid);
                touchPalyer.ChangePlayState(false, playlistPlayer as Grid);
            }
            catch (Exception e)
            {
                logger.Error(e.Message);
            }            
        }

        /// <summary>
        /// Program 파일, 맵핑 파일, Workflow Design 파일 체크한다.
        /// </summary>
        /// <param name="programFilePath"></param>
        /// <returns></returns>
        private bool CheckFileValidation(string programFilePath)
        {
            try
            {
                string mappingFilePath = this.GetTSMappingFilePath(programFilePath);
                if (File.Exists(mappingFilePath) == false)
                    throw new Exception(mappingFilePath + " 파일이 존재하지 않습니다.");

                return true;
            }
            catch (Exception ex)
            {
                //Console.WriteLine(ex.Message);
            }

            return false;
        }

        /// <summary>
        /// program.xml 파일 경로를 얻어서 동일 경로의 tamap 파일을 가져온다.
        /// </summary>
        /// <param name="loadProgramFilePath"></param>
        /// <returns></returns>
        private string GetTSMappingFilePath(string loadProgramFilePath)
        {
            if (File.Exists(loadProgramFilePath) == false)
                return "";

            XElement program = XElement.Load(loadProgramFilePath);
            XElement touchMapping = program.Element("touchmappingfile");

            FileInfo fileInfo = new FileInfo(loadProgramFilePath);
            string touchMappingFile = touchMapping.Attribute("source").Value;
            return loadProgramFilePath.Replace(fileInfo.Name, touchMappingFile);
        }

        /// <summary>
        /// tsmap 파일을 xml형식으로 load하여 터치스크린 정보를 셋팅한다.
        /// </summary>
        /// <param name="tsWorkflowFilePath"></param>
        /// <returns></returns>
        private bool CreateTSWorkflowProcess(string tsWorkflowFilePath)
        {
            try
            {
                if (string.IsNullOrEmpty(tsWorkflowFilePath) == true)
                    return false;

                XElement tsWorkflowXml = XElement.Load(tsWorkflowFilePath);

                return CreateTSWorkflowProcess(tsWorkflowXml.Element("Touchflow"));
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// program.xml 파일 경로를 얻어서 동일 경로의 tamap 파일을 가져온다.
        /// </summary>
        /// <param name="loadProgramFilePath"></param>
        /// <returns></returns>
        private string GetTSMappingFilePath()
        {
            if (File.Exists(ProgramFullPath) == false)
                return "";

            XElement program = XElement.Load(ProgramFullPath);
            XElement touchMapping = program.Element("touchmappingfile");

            FileInfo fileInfo = new FileInfo(ProgramFullPath);
            string touchMappingFile = touchMapping.Attribute("source").Value;
            return ProgramFullPath.Replace(fileInfo.Name, touchMappingFile);
        }

        /// <summary>
        /// 페이지 정보 셋팅.
        /// </summary>
        /// <param name="touchflowElement"></param>
        /// <returns></returns>
        public bool CreateTSWorkflowProcess(XElement touchflowElement)
        {
            return true;
        }

        private void SendTargetTSScreenIDFunc(Guid targetGuid, object ViewObj, bool IsNEScreenRepeat)
        {
            string mappingFilePath = this.GetTSMappingFilePath(this.ProgramFullPath);

            bool isLinked = true;
            string paneScreenID = this.GetPaneScreenIDFromMappingFile(mappingFilePath, targetGuid.ToString(), out isLinked);

            ITouchPlayer touchPlayer = this;
            touchPlayer.ChangePaneScreen(paneScreenID, ViewObj, isLinked, IsNEScreenRepeat);
        }

        private string GetPaneScreenIDFromMappingFile(string mappingFilePath, string tsID, out bool isLinked)
        {
            XElement tsMappingXml = XElement.Load(mappingFilePath);

            IEnumerable<XElement> ScreenMappingList = tsMappingXml.Element("Mapping").Elements("ScreenMapping");

            foreach (XElement item in ScreenMappingList)
            {
                string PaneScreenID = item.Attribute("PaneScreenID").Value;
                string TouchScreenID = item.Attribute("TouchScreenID").Value;
                isLinked = true;
                if (bool.TryParse(item.Attribute("IsLinked").Value, out isLinked) == false)
                    isLinked = true;

                if (TouchScreenID.Equals(tsID) == true)
                {
                    return PaneScreenID;
                }
            }
            isLinked = true;
            return "";
        }

        private string GetTouchScreenIDFromMappingFile(string mappingFilePath, string psID)
        {
            XElement tsMappingXml = XElement.Load(mappingFilePath);

            IEnumerable<XElement> ScreenMappingList = tsMappingXml.Element("Mapping").Elements("ScreenMapping");

            foreach (XElement item in ScreenMappingList)
            {
                string PaneScreenID = item.Attribute("PaneScreenID").Value;
                string TouchScreenID = item.Attribute("TouchScreenID").Value;

                if (PaneScreenID.Equals(psID) == true)
                {
                    return TouchScreenID;
                }
            }

            return "";
        }

        /// <summary>
        /// NEPlayer 화면에 Display되는 Program의 Pane 구성 정보를 분석하여 Main Touch Screen Widnow에 적용한다.
        /// </summary>
        /// <param name="grid">NEPlayer 화면을 구성하는 Root Grid</param>
        private void CreateTouchScreenForm(Grid grid, Dictionary<string, object> paneScreenItems, UIElement windowOwner)
        {
            if (grid != null)
            {
                CreateTouchScreenFormAsyncMethod(grid, paneScreenItems);
            }
        }

        private bool CreateTouchScreenFormAsyncMethod(Grid grid, Dictionary<string, object> paneScreenItems)
        {
            Grid tsGrid = CopyNEPlayerUI(grid, Grid.GetRow(grid), Grid.GetColumn(grid)) as Grid;

            if (tsGrid != null)
            {
                this.Content = tsGrid;
            }

            if (program == null)
                throw new Exception("NE Player Program 인스턴스 오류!!!");

            this.ShowDialog();

            return true;
        }


        /// <summary>
        /// NEPlayer의 Screen 구조로 TouchScreen 화면을 생성한다.
        /// </summary>
        /// <param name="orig"></param>
        /// <param name="row"></param>
        /// <param name="col"></param>
        /// <returns></returns>
        public static UIElement CopyNEPlayerUI(Grid orig, int row, int col)
        {
            if (orig != null && orig is Grid)
            {
                Grid grid = new Grid();
                grid.Tag = orig;//NEPlayer의 Screen과 서로 연관시킨다.

                //grid.ShowGridLines = true;
                grid.Margin = new Thickness(0);
                Grid.SetColumn(grid, col);
                Grid.SetRow(grid, row);
                grid.UpdateLayout();

                RowDefinitionCollection rowCollection = orig.RowDefinitions;
                foreach (RowDefinition rowdef in rowCollection)
                {
                    RowDefinition rowDefTemp = new RowDefinition();
                    rowDefTemp.Height = rowdef.Height;
                    grid.RowDefinitions.Add(rowDefTemp);
                }

                ColumnDefinitionCollection colCollection = orig.ColumnDefinitions;
                foreach (ColumnDefinition coldef in colCollection)
                {
                    ColumnDefinition colDefTemp = new ColumnDefinition();
                    colDefTemp.Width = coldef.Width;
                    grid.ColumnDefinitions.Add(colDefTemp);
                }

                if (orig.Children.Count > 0)
                {
                    foreach (UIElement child in orig.Children)
                    {
                        if (child is Grid)
                        {
                            Grid gridTemp =
                                CopyNEPlayerUI(child as Grid, Grid.GetRow(child), Grid.GetColumn(child)) as Grid;
                            grid.Children.Add(gridTemp);
                        }
                        else
                        {
                            Border item = new Border();

                            item.Tag = (child as Border).Child;//NEPlayer의 Screen과 서로 연관시킨다. (PlaylistPlayer 객체)
                            item.BorderThickness = new Thickness(0);
                            item.Margin = new Thickness(0);
                            item.ClipToBounds = true;
                            grid.Children.Add(item);
                            Grid.SetColumn(item, Grid.GetColumn(child));
                            Grid.SetRow(item, Grid.GetRow(child));
                            item.Background = Brushes.Transparent;//이값을 주어야 클릭 이벤트 처리됨.
                            //item.MouseDown += new System.Windows.Input.MouseButtonEventHandler(TouchScreenBorder_MouseDown);

                            item.UpdateLayout();
                        }
                    }
                }

                return grid;
            }

            return null;
        }

        #endregion

        #region InputDevice

        #region InputDevice

        InSysDSDisplayer.Hooking.InputDevice inputDevice;
        int NumberOfKeyboards;
        Message message = new Message();

        #endregion

        public IntPtr WndProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            if (inputDevice != null)
            {
                // I could have done one of two things here.
                // 1. Use a Message as it was used before.
                // 2. Changes the ProcessMessage method to handle all of these parameters(more work).
                //    I opted for the easy way.

                //Note: Depending on your application you may or may not want to set the handled param.

                message.HWnd = hwnd;
                message.Msg = msg;
                message.LParam = lParam;
                message.WParam = wParam;

                inputDevice.ProcessMessage(message);
            }
            //Console.WriteLine(id.ToString());
            return IntPtr.Zero;
        }

        public void StartWndProcHandler()
        {
            IntPtr hwnd = IntPtr.Zero;
            Window myWin = App.Current.MainWindow;

            try
            {
                hwnd = new WindowInteropHelper(myWin).Handle;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            //Get the Hwnd source   
            HwndSource source = HwndSource.FromHwnd(hwnd);
            //Win32 queue sink
            source.AddHook(new HwndSourceHook(WndProc));

            inputDevice = new InSysDSDisplayer.Hooking.InputDevice(source.Handle);
            NumberOfKeyboards = inputDevice.EnumerateDevices();
            inputDevice.MousePressed += new InSysDSDisplayer.Hooking.InputDevice.MouseDeviceEventHandler(_MousePressed);
            inputDevice.KeyPressed -= new InSysDSDisplayer.Hooking.InputDevice.DeviceEventHandler(_KeyPressed);

        }

        public void StopWndProcHandler()
        {
            if (inputDevice != null)
            {
                inputDevice.MousePressed -= new InSysDSDisplayer.Hooking.InputDevice.MouseDeviceEventHandler(_MousePressed);
                inputDevice.KeyPressed -= new InSysDSDisplayer.Hooking.InputDevice.DeviceEventHandler(_KeyPressed);
                inputDevice = null;
            }
        }

        private void _MousePressed(object sender, InSysDSDisplayer.Hooking.InputDevice.MouseControlEventArgs e)
        {
            ITouchPlayer touchPalyer = this as ITouchPlayer;
            if (touchPalyer != null)
            {
                if (IsTouchSchedulePlaying == true)
                {
                    foreach(var touchManager in this.touchManagerList)
                        touchManager._MousePressed(sender, e);
                }
                else
                {
                    if (touchPalyer.RaisedUserTouchHandler != null)
                        touchPalyer.RaisedUserTouchHandler();
                }
            }
        }

        public void _KeyPressed(object sender, InSysDSDisplayer.Hooking.InputDevice.KeyControlEventArgs e)
        {
            if (IsTouchSchedulePlaying == true)
            {
                foreach (var touchManager in this.touchManagerList)
                    touchManager._KeyPressed(sender, e);
            }
        }

        #endregion
    }
}
