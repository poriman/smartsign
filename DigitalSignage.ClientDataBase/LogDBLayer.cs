﻿using System;
using System.Collections.Generic;
//using System.Linq;
//using System.Text;
using System.IO;


//SQLite & DB routines
using System.Data.SQLite;

using NLog;

namespace DigitalSignage.ClientDataBase
{
    /// <summary>
    /// 로그 DB 레이어
    /// </summary>
    public class LogDBLayer
    {
		private static Logger logger = LogManager.GetCurrentClassLogger();

		//	log semaphore
		private readonly object semaphore_for_log = new object();

        private SQLiteConnection connection = null;
        private string dataPath = "";

		private string srcfilename = "";

        /// <summary>
        /// 생성자
        /// </summary>
		public LogDBLayer()
        {
        }

        /// <summary>
        /// DB 열기
        /// </summary>
        /// <param name="dataPath"></param>
        /// <returns></returns>
        public int openDB(string dataPath)
        {
			try
			{
				//// configuring log output
				//FileTarget target = new FileTarget();
				//target.Layout = "${longdate}\t[${level}]\t[${callsite}]\t${message}";
				//target.FileName = dataPath + "logfiledb.txt";
				//target.ArchiveFileName = dataPath + "archives/logdb.{#####}.txt";
				//target.MaxArchiveFiles = 31;
				//target.ArchiveEvery = FileArchivePeriod.Day;// FileTarget.ArchiveEveryMode.Day;

				//target.ArchiveNumbering = ArchiveNumberingMode.Sequence;// FileTarget.ArchiveNumberingMode.Sequence;
				//// this speeds up things when no other processes are writing to the file
				//target.ConcurrentWrites = true;

				//NLog.Config.SimpleConfigurator.ConfigureForTargetLogging(target, LogLevel.Debug);

				logger.Info("Log Database start");
// 

				this.dataPath = dataPath;
//	20081117 hsshin SQLite Class를 사용하여 DBFactory를 얻어오도록 변경
// 				DbProviderFactory fact = DbProviderFactories.GetFactory("System.Data.SQLite");
				SQLiteFactory fact = new System.Data.SQLite.SQLiteFactory();
				connection = (SQLiteConnection)fact.CreateConnection();
				connection.ConnectionString = "Data Source=" + dataPath + "log.db3";
				
				srcfilename = dataPath + "log.db3";
				
				logger.Info(connection.ConnectionString);

				connection.Open();

				logger.Info("Log Data base opened");

			}
			catch (System.Data.SQLite.SQLiteException exSQLite)
			{
				logger.Error(exSQLite + "");
			}
			catch (Exception e)
			{
				logger.Error(e + "");
			}
            return 1;
        }

        /// <summary>
        /// DB 열기
        /// </summary>
        /// <param name="dataPath"></param>
        /// <param name="filename"></param>
        /// <returns></returns>
		public int openDB(string dataPath, string filename)
		{

			this.dataPath = dataPath;
			//	20081117 hsshin SQLite Class를 사용하여 DBFactory를 얻어오도록 변경
			// 				DbProviderFactory fact = DbProviderFactories.GetFactory("System.Data.SQLite");
			SQLiteFactory fact = new System.Data.SQLite.SQLiteFactory();
			connection = (SQLiteConnection)fact.CreateConnection();
			connection.ConnectionString = "Data Source=" + dataPath + filename;
			
			srcfilename = dataPath + filename;
			logger.Info(connection.ConnectionString);

			connection.Open();
			logger.Info("Log Data base opened");

			return 0;
		}
        /// <summary>
        /// Copy DB
        /// </summary>
        /// <param name="dest"></param>
        /// <returns></returns>
		public bool CopyDB(string dest)
		{
			lock (semaphore_for_log)
			{
				CompactDB();
				File.Copy(srcfilename, dest, false);

				return true;
			}
		}

        /// <summary>
        /// DB 압축
        /// </summary>
		public void CompactDB()
		{
			using (SQLiteCommand cmd = connection.CreateCommand())
			{
				cmd.CommandText = "VACUUM";
				cmd.ExecuteNonQuery();
			}
		}

        public int ExecuteNonQuery(String sQuery)
        {
            try
            {
                using (SQLiteCommand cmd = connection.CreateCommand())
                {
                    cmd.CommandText = sQuery;
                    return cmd.ExecuteNonQuery();
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }

            return -1;
        }

        /// <summary>
        /// DB 종료
        /// </summary>
        /// <returns></returns>
        public int closeDB()
        {
            connection.Close();
            connection = null;
            return 0;
        }

        /// <summary>
        /// 로그 마스터 DB Adapter 생성
        /// </summary>
        /// <returns></returns>
		public LogsDataSetTableAdapters.logmasterTableAdapter CreateLogMasterDA()
        {
			lock (semaphore_for_log)
			{
				LogsDataSetTableAdapters.logmasterTableAdapter da = new LogsDataSetTableAdapters.logmasterTableAdapter();

				da.Connection = connection;
				return da;
			}
        }

        /// <summary>
        /// 로그 DB Adapter 생성
        /// </summary>
        /// <returns></returns>
		public LogsDataSetTableAdapters.logsTableAdapter CreateLogDA()
        {
			lock (semaphore_for_log)
			{
				LogsDataSetTableAdapters.logsTableAdapter da = new LogsDataSetTableAdapters.logsTableAdapter();

				da.Connection = connection;
				return da;
			}
        }

        /// <summary>
        /// 클라이언트 로그 DB Adapter 생성
        /// </summary>
        /// <returns></returns>
		public LogsDataSetTableAdapters.client_logsTableAdapter CreateClientLogDA()
		{
			lock (semaphore_for_log)
			{
				LogsDataSetTableAdapters.client_logsTableAdapter da = new LogsDataSetTableAdapters.client_logsTableAdapter();

				da.Connection = connection;
				return da;
			}
		}

        /// <summary>
        /// 프로모션 연동 로그 성공 DB Adapter 생성
        /// </summary>
        /// <returns></returns>
        public LogsDataSetTableAdapters.promotionsTableAdapter CreatePromotionInfoDA()
        {
            lock (semaphore_for_log)
            {
                LogsDataSetTableAdapters.promotionsTableAdapter da = new LogsDataSetTableAdapters.promotionsTableAdapter();

                da.Connection = connection;
                return da;
            }
        }


        /// <summary>
        /// 타겟 광고 정보 DB Adapter 생성
        /// </summary>
        /// <returns></returns>
        public LogsDataSetTableAdapters.target_ad_infoTableAdapter CreateTargetAdInfoDA()
        {
            lock (semaphore_for_log)
            {
                LogsDataSetTableAdapters.target_ad_infoTableAdapter da = new LogsDataSetTableAdapters.target_ad_infoTableAdapter();

                da.Connection = connection;
                return da;
            }
        }
        /// <summary>
        /// 타겟 광고 성공 DB Adapter 생성
        /// </summary>
        /// <returns></returns>
        public LogsDataSetTableAdapters.target_ad_successTableAdapter CreateTargetAdSuccessDA()
        {
            lock (semaphore_for_log)
            {
                LogsDataSetTableAdapters.target_ad_successTableAdapter da = new LogsDataSetTableAdapters.target_ad_successTableAdapter();

                da.Connection = connection;
                return da;
            }
        }
        /// <summary>
        /// 타겟 광고 에러 DB Adapter 생성
        /// </summary>
        /// <returns></returns>
        public LogsDataSetTableAdapters.target_ad_errorTableAdapter CreateTargetAdErrorDA()
        {
            lock (semaphore_for_log)
            {
                LogsDataSetTableAdapters.target_ad_errorTableAdapter da = new LogsDataSetTableAdapters.target_ad_errorTableAdapter();

                da.Connection = connection;
                return da;
            }
        }
 
        /// <summary>
        /// SQLite Connection 반환
        /// </summary>
        SQLiteConnection Connection
        {
            get { return connection; }
        }
    }
}