﻿using System;
using System.Collections.Generic;
//using System.Linq;
//using System.Text;
using System.IO;


//SQLite & DB routines
using System.Data.SQLite;

using NLog;

namespace DigitalSignage.ClientDataBase
{
    /// <summary>
    /// DB 레이어
    /// </summary>
    public class DBLayer
    {
		private static Logger logger = LogManager.GetCurrentClassLogger();

        private SQLiteConnection connection = null;
        private string dataPath = "";

        /// <summary>
        /// 생성자
        /// </summary>
        public DBLayer()
        {
        }

        /// <summary>
        /// DB 열기
        /// </summary>
        /// <param name="dataPath"></param>
        /// <returns></returns>
        public int openDB(string dataPath)
        {
			try
			{
				//// configuring log output
				//FileTarget target = new FileTarget();
				//target.Layout = "${longdate}\t[${level}]\t[${callsite}]\t${message}";
				//target.FileName = dataPath + "logfiledb.txt";
				//target.ArchiveFileName = dataPath + "archives/logdb.{#####}.txt";
				//target.MaxArchiveFiles = 31;
				//target.ArchiveEvery = FileArchivePeriod.Day;// FileTarget.ArchiveEveryMode.Day;// FileArchivePeriod.Day; 닷넷 2.0

				//target.ArchiveNumbering = ArchiveNumberingMode.Sequence;// FileTarget.ArchiveNumberingMode.Sequence;// ArchiveNumberingMode.Sequence; 닷넷 2.0
				//// this speeds up things when no other processes are writing to the file
				//target.ConcurrentWrites = true;

				//NLog.Config.SimpleConfigurator.ConfigureForTargetLogging(target, LogLevel.Debug);

				logger.Info("Database start");
// 

				this.dataPath = dataPath;
//	20081117 hsshin SQLite Class를 사용하여 DBFactory를 얻어오도록 변경
// 				DbProviderFactory fact = DbProviderFactories.GetFactory("System.Data.SQLite");
				SQLiteFactory fact = new System.Data.SQLite.SQLiteFactory();
				connection = (SQLiteConnection)fact.CreateConnection();
				connection.ConnectionString = "Data Source=" + dataPath + "playlist.db3";
				logger.Info(connection.ConnectionString);

				connection.Open();
				logger.Info("Data base opened");
				try
				{
					using (SQLiteCommand sql = new SQLiteCommand(connection))
					{
						//initializing database structure
						sql.CommandText = "SELECT * FROM tasks";
						sql.ExecuteNonQuery();
					}
				}
				catch (Exception e)
				{
					logger.Warn(e + "");
				}
			}
			catch (System.Data.SQLite.SQLiteException exSQLite)
			{
				logger.Error(exSQLite + "");
			}
			catch (Exception e)
			{
				logger.Error(e + "");
			}
            return 1;
        }

        /// <summary>
        /// DB 닫기
        /// </summary>
        /// <returns></returns>
        public int closeDB()
        {
            connection.Close();
            connection = null;
            return 0;
        }

        /// <summary>
        /// Group DB Adapter 생성
        /// </summary>
        /// <returns></returns>
		public DataSetTableAdapters.groupsTableAdapter CreateGroupDA()
        {
			DataSetTableAdapters.groupsTableAdapter da =
				new DataSetTableAdapters.groupsTableAdapter();
			da.Connection = connection;
            return da;
        }

        /// <summary>
        /// Task DB Adapter 생성
        /// </summary>
        /// <returns></returns>
        public DataSetTableAdapters.tasksTableAdapter CreateTaskDA()
        {
			DataSetTableAdapters.tasksTableAdapter da = new DataSetTableAdapters.tasksTableAdapter();
			da.Connection = connection;
            return da;
        }

        /// <summary>
        /// Player DB Adapter 생성
        /// </summary>
        /// <returns></returns>
        public DataSetTableAdapters.playersTableAdapter CreatePlayersDA()
        {
			DataSetTableAdapters.playersTableAdapter da = new DataSetTableAdapters.playersTableAdapter();
			da.Connection = connection;
            return da;
        }

        /// <summary>
        /// User DB Adapter 생성
        /// </summary>
        /// <returns></returns>
        public DataSetTableAdapters.usersTableAdapter CreateUsersDA()
        {
			DataSetTableAdapters.usersTableAdapter da = new DataSetTableAdapters.usersTableAdapter();
			da.Connection = connection;
            return da;
        }

        /// <summary>
        /// RS232C DB Adapter 생성
        /// </summary>
        /// <returns></returns>
        public DataSetTableAdapters.RSCommandsTableAdapter CreateCmdDA()
        {
			DataSetTableAdapters.RSCommandsTableAdapter da = new DataSetTableAdapters.RSCommandsTableAdapter();
			da.Connection = connection;

			return da;
        }

        /// <summary>
        /// SQLite Connection 반환
        /// </summary>
        public SQLiteConnection Connection
        {
            get { return connection; }
        }
    }
}