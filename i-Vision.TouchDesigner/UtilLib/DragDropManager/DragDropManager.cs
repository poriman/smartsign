
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Controls;
using System.Xml;
using System.IO;
using System.Windows.Documents;
using System.Diagnostics;
using System.Windows.Controls.Primitives;
using System.Windows.Media;

namespace UtilLib.DragDropManager
{
	public static class DragDropManager
	{
		private static UIElement _draggedElt;
		private static bool _isMouseDown = false;
		private static Point _startPoint;
		private static DropPreviewAdorner _overlayElt;

		private static IDragSourceAdvisor _currentSourceAdvisor;
		private static IDropTargetAdvisor _currentTargetAdvisor;

		#region Dependency Properties
		public static readonly DependencyProperty DragSourceAdvisorProperty =
			DependencyProperty.RegisterAttached("DragSourceAdvisor", typeof(IDragSourceAdvisor), typeof(DragDropManager),
			new FrameworkPropertyMetadata(new PropertyChangedCallback(OnDragSourceAdvisorChanged)));

		public static readonly DependencyProperty DropTargetAdvisorProperty =
			DependencyProperty.RegisterAttached("DropTargetAdvisor", typeof(IDropTargetAdvisor), typeof(DragDropManager),
			new FrameworkPropertyMetadata(new PropertyChangedCallback(OnDropTargetAdvisorChanged)));

		public static void SetDragSourceAdvisor(DependencyObject depObj, bool isSet)
		{
			depObj.SetValue(DragSourceAdvisorProperty, isSet);
		}

		public static void SetDropTargetAdvisor(DependencyObject depObj, bool isSet)
		{
			depObj.SetValue(DropTargetAdvisorProperty, isSet);
		}

		public static IDragSourceAdvisor GetDragSourceAdvisor(DependencyObject depObj)
		{                   
			return depObj.GetValue(DragSourceAdvisorProperty) as IDragSourceAdvisor;
		}

		public static IDropTargetAdvisor GetDropTargetAdvisor(DependencyObject depObj)
		{
            return _currentTargetAdvisor as IDropTargetAdvisor;
			//return depObj.GetValue(DropTargetAdvisorProperty) as IDropTargetAdvisor;
		}

        public static void SetDropTargetAdvisor(IDropTargetAdvisor dragSourceAdvisor)
        {
            _currentTargetAdvisor = dragSourceAdvisor;
        }

		#endregion

		#region Property Change handlers
		private static void OnDragSourceAdvisorChanged(DependencyObject depObj, DependencyPropertyChangedEventArgs args)
		{
			UIElement sourceElt = depObj as UIElement;
			if (args.NewValue != null && args.OldValue == null)
			{
				sourceElt.PreviewMouseLeftButtonDown += new MouseButtonEventHandler(DragSource_PreviewMouseLeftButtonDown);
				sourceElt.PreviewMouseMove += new MouseEventHandler(DragSource_PreviewMouseMove);
				sourceElt.PreviewMouseLeftButtonUp += new MouseButtonEventHandler(DragSource_PreviewMouseLeftButtonUp);
				sourceElt.PreviewGiveFeedback += new GiveFeedbackEventHandler(DragSource_PreviewGiveFeedback);

				// Set the Drag source UI
				IDragSourceAdvisor advisor = args.NewValue as IDragSourceAdvisor;
				advisor.SourceUI = sourceElt;
			}
			else if (args.NewValue == null && args.OldValue != null)
			{
				sourceElt.PreviewMouseLeftButtonDown -= DragSource_PreviewMouseLeftButtonDown;
				sourceElt.PreviewMouseMove -= DragSource_PreviewMouseMove;
				sourceElt.PreviewMouseLeftButtonUp -= DragSource_PreviewMouseLeftButtonUp;
				sourceElt.PreviewGiveFeedback -= DragSource_PreviewGiveFeedback;
			}
		}

		static void DragSource_PreviewGiveFeedback(object sender, GiveFeedbackEventArgs e)
		{
			// Can be used to set custom cursors
		}

		private static void OnDropTargetAdvisorChanged(DependencyObject depObj, DependencyPropertyChangedEventArgs args)
		{
            var panel = DragDropManager.FindLogicalChild<Canvas>(depObj);//자식 컨트롤중 Canvas(ZoomCanvas)를 찾는다.
            UIElement targetElt;
            UIElement allowTargetOutElt = null;
            if (panel != null)
            {
                targetElt = panel as UIElement;
                allowTargetOutElt = depObj as UIElement;

                //실제 작업뷰 밖의 Drop 허용 영역에 대한 처리
                if (allowTargetOutElt != null && args.NewValue != null && args.OldValue == null)
                {
                    allowTargetOutElt.PreviewDragEnter += new DragEventHandler(AllowDropTarget_PreviewDragEnter);
                    allowTargetOutElt.PreviewDragOver += new DragEventHandler(AllowDropTarget_PreviewDragOver);
                    allowTargetOutElt.PreviewDragLeave += new DragEventHandler(AllowDropTarget_PreviewDragLeave);
                    allowTargetOutElt.Drop += new DragEventHandler(AllowDropTarget_Drop);
                    allowTargetOutElt.AllowDrop = true;
                }
                else
                {
                    allowTargetOutElt.PreviewDragEnter -= AllowDropTarget_PreviewDragEnter;
                    allowTargetOutElt.PreviewDragOver -= AllowDropTarget_PreviewDragOver;
                    allowTargetOutElt.PreviewDragLeave -= AllowDropTarget_PreviewDragLeave;
                    allowTargetOutElt.Drop -= AllowDropTarget_Drop;
                    allowTargetOutElt.AllowDrop = false;
                }
            }
            else
            {
                targetElt = depObj as UIElement;
            }           

            //실제 작업뷰 Drop 허용 영역에 대한 처리
			if (args.NewValue != null && args.OldValue == null)
			{
				targetElt.PreviewDragEnter += new DragEventHandler(DropTarget_PreviewDragEnter);
				targetElt.PreviewDragOver += new DragEventHandler(DropTarget_PreviewDragOver);
				targetElt.PreviewDragLeave += new DragEventHandler(DropTarget_PreviewDragLeave);
				targetElt.PreviewDrop += new DragEventHandler(DropTarget_PreviewDrop);
				targetElt.AllowDrop = true;

				// Set the Drag source UI
                _currentTargetAdvisor = args.NewValue as IDropTargetAdvisor;
                _currentTargetAdvisor.TargetCanvas = targetElt;
                _currentTargetAdvisor.TargetContainer = allowTargetOutElt;
                _currentTargetAdvisor.AllowTargetOutElt = allowTargetOutElt;
			}
			else if (args.NewValue == null && args.OldValue != null)
			{
				targetElt.PreviewDragEnter -= DropTarget_PreviewDragEnter;
				targetElt.PreviewDragOver -= DropTarget_PreviewDragOver;
				targetElt.PreviewDragLeave -= DropTarget_PreviewDragLeave;
				targetElt.PreviewDrop -= DropTarget_PreviewDrop;
				targetElt.AllowDrop = false;
			}
		}
		#endregion

        /* ____________________________________________________________________
		 *		Drop Target(실제 작업뷰) 밖의 허용 영역 events 
		 * ____________________________________________________________________
		 */
        static void AllowDropTarget_Drop(object sender, DragEventArgs e)
        {
            if (UpdateEffects(sender, e) == false) return;

            IDropTargetAdvisor advisor = GetDropTargetAdvisor(sender as DependencyObject);
            //Point dropPoint = e.GetPosition(sender as UIElement);
            Point dropPoint = e.GetPosition(advisor.TargetCanvas);

            // Calculate displacement for (Left, Top)
            Point offset = e.GetPosition(_overlayElt);
            dropPoint.X = dropPoint.X - offset.X;
            dropPoint.Y = dropPoint.Y - offset.Y;

            advisor.OnDropCompleted(e.Data, dropPoint);
        }

        static void AllowDropTarget_PreviewDragLeave(object sender, DragEventArgs e)
        {
            if (UpdateEffects(sender, e) == false) return;

            RemovePreviewAdorner();

            e.Handled = true;
        }

        static void AllowDropTarget_PreviewDragOver(object sender, DragEventArgs e)
        {
            if (UpdateEffects(sender, e) == false)
            {
                _currentTargetAdvisor.TargetCanvas.AllowDrop = false;
                _currentTargetAdvisor.TargetContainer.AllowDrop = false;
                return;
            }
            _currentTargetAdvisor.TargetCanvas.AllowDrop = true;
            _currentTargetAdvisor.TargetContainer.AllowDrop = true;

            Point position = e.GetPosition(GetTopContainer());
            _overlayElt.Left = position.X - _startPoint.X;
            _overlayElt.Top = position.Y - _startPoint.Y;

            e.Handled = true;
        }

        static void AllowDropTarget_PreviewDragEnter(object sender, DragEventArgs e)
        {
            if (UpdateEffects(sender, e) == false)
                return;

            CreatePreviewAdorner();

            e.Handled = true;
        }

		/* ____________________________________________________________________
		 *		Drop Target events 
		 * ____________________________________________________________________
		 */
		static void DropTarget_PreviewDrop(object sender, DragEventArgs e)
		{
			if (UpdateEffects(sender, e) == false) return;

			IDropTargetAdvisor advisor = GetDropTargetAdvisor(sender as DependencyObject);
			Point dropPoint = e.GetPosition(sender as UIElement);

			// Calculate displacement for (Left, Top)
			Point offset = e.GetPosition(_overlayElt);
			dropPoint.X = dropPoint.X - offset.X;
			dropPoint.Y = dropPoint.Y - offset.Y;

            advisor.OnDropCompleted(e.Data, dropPoint);

            e.Handled = true;
		}

		static void DropTarget_PreviewDragLeave(object sender, DragEventArgs e)
		{
			if (UpdateEffects(sender, e) == false) return;

			RemovePreviewAdorner();

			e.Handled = true;
		}

		static void DropTarget_PreviewDragOver(object sender, DragEventArgs e)
		{
            if (UpdateEffects(sender, e) == false)
            {
                _currentTargetAdvisor.TargetCanvas.AllowDrop = false;
                _currentTargetAdvisor.TargetContainer.AllowDrop = false;
                return;
            }
            _currentTargetAdvisor.TargetCanvas.AllowDrop = true;
            _currentTargetAdvisor.TargetContainer.AllowDrop = true;

			Point position = e.GetPosition(GetTopContainer());
			_overlayElt.Left = position.X - _startPoint.X;
			_overlayElt.Top = position.Y - _startPoint.Y;

			e.Handled = true;
		}

		static void DropTarget_PreviewDragEnter(object sender, DragEventArgs e)
		{
            if (UpdateEffects(sender, e) == false)                  
                return;

			CreatePreviewAdorner();

			e.Handled = true;
		}

		static bool UpdateEffects(object uiObject, DragEventArgs e)
        {           
			IDropTargetAdvisor advisor = GetDropTargetAdvisor(uiObject as DependencyObject);
            if (advisor.IsValidDataObject(e.Data) == false) return false;

			if ((e.AllowedEffects & DragDropEffects.Move) == 0 &&
				(e.AllowedEffects & DragDropEffects.Copy) == 0)
			{
				e.Effects = DragDropEffects.None;
				return false;
			}

			if ((e.AllowedEffects & DragDropEffects.Move) != 0 &&
				(e.AllowedEffects & DragDropEffects.Copy) != 0)
			{
				e.Effects = ((e.KeyStates & DragDropKeyStates.ControlKey) != 0) ?
					DragDropEffects.Copy : DragDropEffects.Move;
			}

			return true;
		}

		/* ____________________________________________________________________
		 *		Drag Source events 
		 * ____________________________________________________________________
		 */
		static void DragSource_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{
			// Make this the new drag source
			_currentSourceAdvisor = GetDragSourceAdvisor(sender as DependencyObject);
            if (_currentTargetAdvisor != null)
                _currentTargetAdvisor.AllowTargetOutElt.AllowDrop = true;

			if (_currentSourceAdvisor.IsDraggable(e.Source as UIElement) == false) 
                return;
            if (_currentSourceAdvisor.IsDraggable(e.Source as Panel) == true) //Panel은 Drag & Drop 막음
                return;

            if (_currentSourceAdvisor.IsDraggable(e.Source as ToolBar) == true) //ToolBar는 Drag & Drop 막음
                return;

            _draggedElt = sender as UIElement;
			_startPoint = e.GetPosition(GetTopContainer());
			_isMouseDown = true;

		}

		static void DragSource_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
		{
			_isMouseDown = false;
		}

		static void DragSource_PreviewMouseMove(object sender, MouseEventArgs e)
		{
			if (_isMouseDown && IsDragGesture(e.GetPosition(GetTopContainer())))
			{
				DragStarted(sender);
			}
		}

		static void DragStarted(object uiObject)
		{
			_isMouseDown = false;
			Mouse.Capture(_draggedElt);

			DataObject data = _currentSourceAdvisor.GetDataObject(_draggedElt);
			DragDropEffects supportedEffects = _currentSourceAdvisor.SupportedEffects;

			// Perform DragDrop
			DragDropEffects effects = System.Windows.DragDrop.DoDragDrop(_draggedElt, data, supportedEffects);           
			_currentSourceAdvisor.FinishDrag(_draggedElt, effects);

			// Clean up
			EndDragDrop();
		}

		static bool IsDragGesture(Point point)
		{
			bool hGesture = Math.Abs(point.X - _startPoint.X) > SystemParameters.MinimumHorizontalDragDistance;
			bool vGesture = Math.Abs(point.Y - _startPoint.Y) > SystemParameters.MinimumVerticalDragDistance;

			return (hGesture | vGesture);
		}

		/* ____________________________________________________________________
		 *		Utility functions
		 * ____________________________________________________________________
		 */
		static UIElement GetTopContainer()
		{
			UIElement container = Application.Current.MainWindow.Content as UIElement;
			return container;
		}

		private static void CreatePreviewAdorner()
		{
			// Clear if there is an existing adorner
			RemovePreviewAdorner();

			AdornerLayer layer = AdornerLayer.GetAdornerLayer(GetTopContainer());
			UIElement feedbackUI = _currentSourceAdvisor.GetVisualFeedback(_draggedElt);
			_overlayElt = new DropPreviewAdorner(feedbackUI, _draggedElt, layer);
			layer.Add(_overlayElt);
		}

		private static void RemovePreviewAdorner()
		{
			if (_overlayElt != null)
			{
				AdornerLayer.GetAdornerLayer(GetTopContainer()).Remove(_overlayElt);
				_overlayElt = null;
			}
		}

		private static void EndDragDrop()
		{
			Mouse.Capture(null);
			RemovePreviewAdorner();
		}

        public static T FindLogicalChild<T>(DependencyObject parent) where T : DependencyObject
        {
            if (parent == null)
                return null;
            System.Collections.IEnumerable list = LogicalTreeHelper.GetChildren(parent);
            foreach (var child in list)
            {
                if (child.GetType().IsSubclassOf(typeof(Canvas)) == true)
                {
                    return child as T;
                }
                else
                {
                    T result = FindLogicalChild<T>(child as DependencyObject);
                    if (result != null)
                        return result;
                }
            }
            
            return null;
        }
        
        public static T FindVisualChildByName<T>(DependencyObject parent, string name) where T : DependencyObject
        {
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(parent); i++)
            {
                var child = VisualTreeHelper.GetChild(parent, i);
                string controlName = child.GetValue(Control.NameProperty) as string;
                if (controlName == name)
                {
                    return child as T;
                }
                else
                {
                    T result = FindVisualChildByName<T>(child, name);
                    if (result != null)
                        return result;
                }
            }
            return null;
        }
	}
}
