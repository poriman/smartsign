using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;

namespace UtilLib.DragDropManager
{
    public interface IDropTargetAdvisor
    {
        UIElement TargetContainer
        {
            get;
            set;
        }
		UIElement TargetCanvas
		{
			get;
			set;
		}
        UIElement AllowTargetOutElt
        {
            get;
            set;
        }

		bool IsValidDataObject(IDataObject obj);
		void OnDropCompleted(IDataObject obj, Point dropPoint);
   }
}
