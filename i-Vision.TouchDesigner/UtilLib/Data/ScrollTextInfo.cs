﻿using System.Text;
using System.Windows.Media;
using System.Runtime.Serialization;
using System.Windows;
using System.Windows.Controls;
using System;
using UtilLib.Etc;

namespace UtilLib.Data
{
    [Serializable]
    public class ScrollTextInfo : DependencyObject
    {
        private int _speed;
        /// <summary>
        /// Speed property
        /// </summary>
        public int speed
        {
            get
            {
                return _speed;
            }

            set
            {
                _speed = value;
            }
        }


        private object _content;
        /// <summary>
        /// Content property
        /// </summary>
        public object content
        {
            get
            {
                return _content;
            }

            set
            {
                _content = value;
            }
        }

        private ScrollTextDirection _direction;
        /// <summary>
        /// Direction property
        /// </summary>
        public ScrollTextDirection direction
        {
            get
            {
                return _direction;
            }

            set
            {
                _direction = value;
            }
        }

        private Brush _background;
        private Brush _descbackground;
        /// <summary>
        /// Background property
        /// </summary>
        public Brush background
        {
            get
            {
                return _background;
            }

            set
            {
                _background = value;
            }
        }

        public Brush descbackground
        {
            get
            {
                return _descbackground;
            }

            set
            {
                _descbackground = value;
            }
        }
        private Brush _foreground;
        private Brush _descforeground;
        /// <summary>
        /// Foreground property
        /// </summary>
        public Brush foreground
        {
            get
            {
                return _foreground;
            }

            set
            {
                _foreground = value;
            }
        }
        public Brush descforeground
        {
            get
            {
                return _descforeground;
            }

            set
            {
                _descforeground = value;
            }
        }
        private FontFamily _fontFamily;
        private FontFamily _descfontFamily;
        /// <summary>
        /// FontFamily property
        /// </summary>
        public FontFamily FontFamily
        {
            get
            {
                return _fontFamily;
            }
            set
            {
                _fontFamily = value;
            }
        }
        public FontFamily DescFontFamily
        {
            get
            {
                return _descfontFamily;
            }
            set
            {
                _descfontFamily = value;
            }
        }

        private double _fontSize;
        private double _descfontSize;
        /// <summary>
        /// FontSize property
        /// </summary>
        public double FontSize
        {
            get
            {
                return _fontSize;
            }
            set
            {
                _fontSize = value;
            }
        }

        public double DescFontSize
        {
            get
            {
                return _descfontSize;
            }
            set
            {
                _descfontSize = value;
            }
        }
        private FontStyle _fontStyle;
        private FontStyle _descfontStyle;
        /// <summary>
        /// FontWeight property
        /// </summary>
        public FontStyle FontStyle
        {
            get
            {
                return _fontStyle;
            }
            set
            {
                _fontStyle = value;
            }
        }
        public FontStyle DescFontStyle
        {
            get
            {
                return _descfontStyle;
            }
            set
            {
                _descfontStyle = value;
            }
        }

        private FontWeight _fontWeight;
        private FontWeight _descfontWeight;
        /// <summary>
        /// FontWeight property
        /// </summary>
        public FontWeight FontWeight
        {
            get
            {
                return _fontWeight;
            }
            set
            {
                _fontWeight = value;
            }
        }
        public FontWeight DescFontWeight
        {
            get
            {
                return _descfontWeight;
            }
            set
            {
                _descfontWeight = value;
            }
        }

        public ScrollTextInfo()
        {
            direction = ScrollTextDirection.RightToLeft;
            content = "";
            speed = 100;
            background = new SolidColorBrush(Color.FromArgb(255, 255, 255, 255));
            foreground = new SolidColorBrush(Color.FromArgb(255, 0, 0, 0));
            _fontWeight = FontWeights.Normal;
            _fontStyle = FontStyles.Normal;
            _fontSize = 14.0;
            _fontFamily = new FontFamily("Arial");

            descbackground = new SolidColorBrush(Color.FromArgb(255, 255, 255, 255));
            descforeground = new SolidColorBrush(Color.FromArgb(255, 0, 0, 0));
            _descfontWeight = FontWeights.Normal;
            _descfontStyle = FontStyles.Normal;
            _descfontSize = 12.0;
            _descfontFamily = new FontFamily("Arial");
        }

        public ScrollTextInfo(object content, int speed, ScrollTextDirection direction, Brush background)
        {
            this.direction = direction;
            this.speed = speed;
            this.content = content;
            this.background = background;
            foreground = new SolidColorBrush(Color.FromArgb(255, 0, 0, 0));
            _fontWeight = FontWeights.Normal;
            _fontStyle = FontStyles.Normal;
            _fontSize = 14.0;
            _fontFamily = new FontFamily("Arial");

            descbackground = background;
            descforeground = new SolidColorBrush(Color.FromArgb(255, 0, 0, 0));
            _descfontWeight = FontWeights.Normal;
            _descfontStyle = FontStyles.Normal;
            _descfontSize = 12.0;
            _descfontFamily = new FontFamily("Arial");
        }

        public override string ToString()
        {
            if (content.ToString().Length < 15)
            {
                return content.ToString() + "/Speed: " + speed.ToString() + "/Direction: " + direction.ToString();
            }
            else
            {
                return content.ToString().Substring(0, 15) + ".../Speed " + speed.ToString() + "/Direction: " + direction.ToString();
            }
        }
    }
}
