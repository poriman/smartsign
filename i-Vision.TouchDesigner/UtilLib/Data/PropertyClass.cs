﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace UtilLib.Data
{
    public class PropertyClass
    {
        /// <summary>
        /// Sets the specified property by the specified object, interface and property value
        /// </summary>
        public static void SetProperty(object obj, string interfaceName, string propertyName, object value)
        {
            PropertyInfo[] props = obj.GetType().GetInterface(interfaceName).GetProperties();

            foreach (PropertyInfo p in props)
            {
                if (p.Name == propertyName && p.CanWrite)
                {
                    try
                    {
                        p.SetValue(obj, value, null);
                    }
                    catch
                    {
                        return;
                    }
                }
            }
        }
    }
}
