﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UtilLib.Etc
{
    /// <summary>
    /// Manages the clock`s Gmt
    /// </summary>
    public class GmtManager
    {
        /// <summary>
        /// Returns TimeZoneInfo from system time zones by given index in string
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public static TimeZoneInfo GetTimeZoneFromIndexInString(string index)
        {
            int ind = 0;
            if (Int32.TryParse(index, out ind))
            {
                return TimeZoneInfo.GetSystemTimeZones()[ind];
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Returns the string index of the specified TimeZoneInfo
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        public static string GetStringIndexOfTimeZone(TimeZoneInfo info)
        {
            try
            {
                return TimeZoneInfo.GetSystemTimeZones().IndexOf(info).ToString();
            }
            catch { return null; }
        }
    }
}
