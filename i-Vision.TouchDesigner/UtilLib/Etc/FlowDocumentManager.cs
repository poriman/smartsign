﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Documents;
using System.Xml;
using System.IO;
using System.Windows.Markup;

namespace UtilLib.Etc
{
    /// <summary>
    /// Manages the FlowDocument serializing
    /// </summary>
    public class FlowDocumentManager
    {
        /// <summary>
        /// Deserializes the FlowDocument from its string representation using XAMLReader
        /// </summary>
        /// <param name="serializedDocument">FlowDocument serialized w/ XAMLWriter</param>
        /// <returns>FlowDocument</returns>
        public static FlowDocument DeserializeFlowDocument(string serializedDocument)
        {
            if (serializedDocument != "")
            {
                StringReader reader = new StringReader(serializedDocument);
                XmlReader xmlReader = XmlReader.Create(reader);
                return (FlowDocument)XamlReader.Load(xmlReader);
            }
            else
            {
                return new FlowDocument();
            }
        }

        /// <summary>
        /// Serializes the FlowDocument w/ XAMLWriter
        /// </summary>
        /// <param name="doc">FlowDocument to serialize</param>
        /// <returns>The string representation of serialized FlowDocument instance</returns>
        public static string SerializeFlowDocument(FlowDocument doc)
        {
            if (doc != null)
            {
                return XamlWriter.Save(doc);
            }
            else
            {
                return String.Empty;
            }
        }
    }
}
