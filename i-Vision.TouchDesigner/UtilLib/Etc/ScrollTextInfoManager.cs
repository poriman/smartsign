﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UtilLib.Data;
using System.IO;
using System.Xml;
using System.Windows.Markup;

namespace UtilLib.Etc
{
    /// <summary>
    /// Manages the serializing/deserializing of ScrolTextInfo class
    /// </summary>
    public class ScrollTextInfoManager
    {
        /// <summary>
        /// Deserializes the ScrollTextInfo classs instance from string
        /// </summary>
        /// <param name="infoDefinition">The string representation of serialized ScrollTextInfo class instance</param>
        /// <returns>The ScrollTextInfo instance</returns>
        public static ScrollTextInfo DeserializeScrollTextInfo(string infoDefinition)
        {
            if (infoDefinition == "")
            {
                return new ScrollTextInfo();
            }
            try
            {
                StringReader sr = new StringReader(infoDefinition);
                XmlReader xr = XmlReader.Create(sr);
                return (ScrollTextInfo)XamlReader.Load(xr);
            }
            catch
            {
                return new ScrollTextInfo();
            }
        }

        /// <summary>
        /// Serializes the specified ScrollTextInfo class instance into string
        /// </summary>
        /// <param name="info">ScrollTextInfo to serialized</param>
        /// <returns>String representation of the given ScrolTextInfo instance</returns>
        public static string SerializeScrollTextInfo(ScrollTextInfo info)
        {
            return XamlWriter.Save(info);
        }

        /// <summary>
        /// Updates the 
        /// </summary>
        /// <param name="infoDefinition"></param>
        /// <param name="mediaPath"></param>
        public static void UpdateImageBrushesOfScrollTextInfo(string infoDefinition, string mediaPath)
        {

        }
    }
}
