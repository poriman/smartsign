﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UtilLib.Etc
{
    /// <summary>
    /// 스크롤링 텍스트 컴포넌트에서 사용하는 스크롤링 방향
    /// </summary>
    public enum ScrollTextDirection
    {
        /// <summary>
        /// 왼쪽에서 오른쪽
        /// </summary>
        LeftToRight,
        /// <summary>
        /// 오른쪽에서 왼쪽
        /// </summary>
        RightToLeft,
        /// <summary>
        /// 위에서 아래
        /// </summary>
        TopToBottom,
        /// <summary>
        /// 아래에서 위
        /// </summary>
        BottomToTop
    }

}
