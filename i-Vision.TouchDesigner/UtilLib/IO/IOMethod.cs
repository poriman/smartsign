﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace UtilLib.IO
{
    public static class IOMethod
    {
        public static bool ExistsFile(string fullpath)
        {
            if (string.IsNullOrEmpty(fullpath) == true)
                return false;
            return File.Exists(fullpath);           
        }
    }
}
