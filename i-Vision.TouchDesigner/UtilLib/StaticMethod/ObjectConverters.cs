﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Xml.Linq;
using System.IO;
using System.Xml;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace UtilLib.StaticMethod
{
    public static class ObjectConverters
    {
        public static Brush ConvertBrushFromBrushXamlString(string burshXamlString)
        {
            XElement element = XElement.Parse(burshXamlString);
            StringReader stringReader = new StringReader(burshXamlString);
            XmlReader xmlReader = XmlReader.Create(stringReader);

            return (Brush)System.Windows.Markup.XamlReader.Load(xmlReader);
        }

        public static string ConvertXamlStringFromObject(object value)
        {
            string xaml = System.Windows.Markup.XamlWriter.Save(value);
            return xaml;
        }

        public static object ConvertPropertyObjectFromPropertyXamlString(string burshXamlString)
        {
            XElement element = XElement.Parse(burshXamlString);
            StringReader stringReader = new StringReader(burshXamlString);
            XmlReader xmlReader = XmlReader.Create(stringReader);

            return System.Windows.Markup.XamlReader.Load(xmlReader);
        }

        public static Image GetImage(string filepath, UriKind uriKind)
        {
            Image image = new Image();
            image.Source = new BitmapImage(new Uri(filepath, UriKind.Absolute));

            return image;
        }

        public static ImageSource GetImageSource(string filepath, UriKind uriKind)
        { 
            
            #region Test Code : 만들어진 이미지를 확인하기 위해 파일로 저장
            /* 
            FileStream filestream = new FileStream("Test.jpg", FileMode.Create);
            JpegBitmapEncoder encoder = new JpegBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create((BitmapImage)imageSource));
            encoder.Save(filestream);
            */
            #endregion

            //ImageSource imageSource = new BitmapImage(new Uri(filepath, uriKind));

            try
            {
                // 파일
                FileStream fs = File.OpenRead(filepath);
                byte[] data = new byte[fs.Length];
                fs.Read(data, 0, data.Length);
               
                MemoryStream ms1 = new System.IO.MemoryStream(data);
                System.Drawing.Image img = System.Drawing.Image.FromStream(ms1);

                //////////////////////////////////////////////
                //convert System.Drawing.Image to WPF image
                System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(img);
                IntPtr hBitmap = bmp.GetHbitmap();
                System.Windows.Media.ImageSource imageSource1 = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(hBitmap, IntPtr.Zero, System.Windows.Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());

                Image wpfImage = new Image();
                wpfImage.Source = imageSource1;
                wpfImage.Width = wpfImage.Height = 16;
                //////////////////////////////////////////////

                return wpfImage.Source;
            }
            catch
            {
                Image img1 = new Image();
                img1.Source = new BitmapImage(new Uri(filepath, UriKind.Relative));
                img1.Width = img1.Height = 16;
                return img1.Source;
            }
        }
    }
}
