﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace UtilLib.StaticMethod
{
    public static class IOHelper
    {
        public static string StyleLoad(string file)
        {
            string line;
            try
            {
                if (File.Exists(file))
                {
                    using (StreamReader reader = new StreamReader(file))
                    {
                        line = reader.ReadToEnd();
                        return line;
                    }

                }               
                return "";
            }
            catch
            {
                return "";
            }
        }
    }
}
