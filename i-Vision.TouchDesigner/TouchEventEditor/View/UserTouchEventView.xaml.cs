﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Caliburn.Core;
using Caliburn.PresentationFramework;
using TouchEventEditor.Model;
using System.Xml.Linq;

namespace TouchEventEditor.View
{
    /// <summary>
    /// Interaction logic for UserEventView.xaml
    /// </summary>
    public partial class UserTouchEventView : UserControl
    {
        public TouchEventType TouchEventType { get; set; }        
        public string TouchEventFilePath { get; set; }        
        public string UserTouchEventName { get; set; } //로딩시 또는 적용시 사용자 터치이벤트 명

        public UserTouchEventView()
        {
            CaliburnFramework.ConfigureCore().WithPresentationFramework().Start();
            TouchEventType = Model.TouchEventType.TC_EVT_OBJECT_START;            
            InitializeComponent();
        }

        #region Routed Event

        public static readonly RoutedEvent CloseUserTouchEventViewEvent = EventManager.RegisterRoutedEvent("CloseUserTouchEventView", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(UserTouchEventView));
        public event RoutedEventHandler CloseUserTouchEventView
        {
            add { AddHandler(CloseUserTouchEventViewEvent, value); }
            remove { RemoveHandler(CloseUserTouchEventViewEvent, value); }
        }

        public void RaiseCloseUserTouchEventViewEvent(bool dialogResult, Dictionary<string, string> resultObject, string selectedValue, List<UserTouchEvent> userTouchEvents)
        {
            ResultValueEventArgs eventArgs = new ResultValueEventArgs(UserTouchEventView.CloseUserTouchEventViewEvent, dialogResult, resultObject, selectedValue, userTouchEvents);
            RaiseEvent(eventArgs);
        }

        #endregion
    }
}
