﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Caliburn.Core;
using Caliburn.PresentationFramework;
using TouchEventEditor.Controller;
using TouchEventEditor.ViewModel;

namespace TouchEventEditor.View
{
    /// <summary>
    /// Interaction logic for iVisionNETouchEventView.xaml
    /// </summary>
    public partial class TouchEventView : UserControl
    {
        public static readonly RoutedEvent InsertEventFunctionRoutedEvent = EventManager.RegisterRoutedEvent("InsertEventFunction", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(TouchEventView));

        public event RoutedEventHandler InsertEventFunction
        {
            add { AddHandler(InsertEventFunctionRoutedEvent, value); }
            remove { RemoveHandler(InsertEventFunctionRoutedEvent, value); }
        }

        public TouchEventView()
        {

            CaliburnFramework.ConfigureCore().WithPresentationFramework().Start();

            InitializeComponent();

            this.Loaded += (s, e) =>
            {
                TouchEventViewController touchEventViewController = TouchEventViewController.Instance;
                TouchEventViewModel touchEventViewModel = this.DataContext as TouchEventViewModel;
                touchEventViewModel.RootContents.Clear();
                foreach (var item in touchEventViewController.RootContents)
                {
                    touchEventViewModel.RootContents.Add(item);
                }
                touchEventViewModel.SetSelfTouchContent(touchEventViewController.SelfTouchContent);
                touchEventViewModel.MakeEventFunction(touchEventViewController.SelfEventObjectType);
            };
        }

       
    }
}
