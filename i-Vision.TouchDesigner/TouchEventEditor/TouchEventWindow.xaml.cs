﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using TouchEventEditor.Model;
using System.Collections.ObjectModel;
using System.Xml.Linq;
using System.IO;
using TouchEventEditor.Controller;

namespace TouchEventEditor
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class TouchEventWindow : Window, INotifyPropertyChanged
    {
        public string TouchEventFileFullPath { get; set; }
        public Dictionary<string, string> TouchEventDic { get; set; }
        public List<UserTouchEvent> UserTouchEvents { get; set; }
        private Dictionary<string, string> tempUserTouchEventDic { get; set; }

        public Dictionary<string, string> comboBoxItemTouchEventDic;
        public Dictionary<string, string> ComboBoxItemTouchEventDic
        {
            get { return comboBoxItemTouchEventDic; }
            set { comboBoxItemTouchEventDic = value; OnPropertyChanged("ComboBoxItemTouchEventDic"); }
        }

        public TouchEventType touchEventType;
        public TouchEventType TouchEVentType
        {
            get { return touchEventType; }
            set { touchEventType = value; OnPropertyChanged("TouchEVentType"); }
        }

        private ObservableCollection<TouchEventItem> touchEventItems;
        public ObservableCollection<TouchEventItem> TouchEventItems
        {
            get { return this.touchEventItems; }
            set { this.touchEventItems = value; OnPropertyChanged("TouchEventItems"); }
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        public virtual void OnPropertyChanged(params string[] propertyNames)
        {
            if (PropertyChanged == null)
                return;

            foreach (string propertyName in propertyNames)
            {
                try
                {
                    PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
                }
                catch { }
            }
        }

        #endregion

        public TouchEventWindow()
        {
            InitializeComponent();
            TouchEventDic = new Dictionary<string, string>();
            comboBoxItemTouchEventDic = new Dictionary<string, string>();
            tempUserTouchEventDic = new Dictionary<string, string>();
            touchEventItems = new ObservableCollection<TouchEventItem>();            
            this.DataContext = this;           
           
            string[] enumList = Enum.GetNames(typeof(TouchEventType));
            foreach (var enumitem in enumList)
            {
                TouchEventItem item = new TouchEventItem();
                item.TouchEventType = enumitem;
                TouchEventItems.Add(item);
            }
            this.Loaded += (s, e) =>
            {
                //this.SetTouchEventDic(this.TouchEventDic);
                LoadTouchEventName();
            };
        }

        private void LoadTouchEventName()
        {
            if (File.Exists(TouchEventFileFullPath) == true)
            {
                XElement projectTouchEventXElement = XElement.Load(TouchEventFileFullPath);
                if (projectTouchEventXElement != null)
                {
                    IEnumerable<string> list = projectTouchEventXElement.Elements().Select(o => o.Attribute("name").Value);
                    Dictionary<string, string> dic = new Dictionary<string, string>();
                    int i = 0;
                    list.ToList().ForEach(o => { dic.Add(i.ToString(), o); i++; });

                    SetTouchEventDic(dic);

                    foreach (var keyvalue in this.TouchEventDic)
                        this.tempUserTouchEventDic.Add(keyvalue.Key, keyvalue.Value);
                }
            }
            else
            {
                Dictionary<string, string> dic = new Dictionary<string, string>();
                SetTouchEventDic(dic);
            }
        }

        private void TouchEventEditorComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox combobox = sender as ComboBox;
            if (combobox != null)
            {
                TouchEventItem touchEventItem = combobox.Tag as TouchEventItem;

                string touchEventTypeStr = touchEventItem.TouchEventType;
                KeyValuePair<string, string> keyvalue = (KeyValuePair<string, string>)combobox.SelectedItem;
                if (keyvalue.Key == "EDIT_TOUCHEVENT")
                {
                    TouchEventEditorWindow touchEventEditorWindow = new TouchEventEditorWindow();                    
                    touchEventEditorWindow.Owner = this;
                    touchEventEditorWindow.TouchEventFileFullPath = TouchEventFileFullPath;
                    touchEventEditorWindow.SelectedTouchEventType = (TouchEventType)Enum.Parse(typeof(TouchEventType), touchEventItem.TouchEventType, true);

                    object previousSelectedItem = e.RemovedItems.Count == 0 ? null : e.RemovedItems[0];
                    if (previousSelectedItem != null)
                    {
                        KeyValuePair<string, string> tempKeyValue = (KeyValuePair<string, string>)previousSelectedItem;
                         if(string.IsNullOrEmpty(tempKeyValue.Value) == false)
                             touchEventEditorWindow.SelectedUserTouchEventName = tempKeyValue.Value;
                         else
                             touchEventEditorWindow.SelectedUserTouchEventName = "";
                    }
                    
                    if (touchEventEditorWindow.ShowDialog() == true)
                    {
                        string userSelectedTouchEventName = touchEventEditorWindow.SelectedUserTouchEventName;
                        userSelectedTouchEventName = userSelectedTouchEventName.Trim();
                        this.UserTouchEvents = touchEventEditorWindow.UserTouchEvents;
                        this.SetTouchEventDic(touchEventEditorWindow.UserTouchEventList);
                        if (string.IsNullOrEmpty(userSelectedTouchEventName) != true)//해당 TouchEvent 업데이트
                        {
                            if (this.tempUserTouchEventDic.ContainsKey(touchEventTypeStr) == true)
                            {
                                this.tempUserTouchEventDic[touchEventTypeStr] = userSelectedTouchEventName;
                            }
                            else
                            {
                                this.tempUserTouchEventDic.Add(touchEventTypeStr, userSelectedTouchEventName);
                            }

                            combobox.SelectedItem = touchEventItem.TouchEventNames.Where(o => o.Value.Equals(userSelectedTouchEventName) == true).FirstOrDefault();
                        }
                        else//해당 TouchEvent 가 비어 있을때는 제거한다.
                        {
                            if (this.tempUserTouchEventDic.ContainsKey(touchEventTypeStr) == true)
                                this.tempUserTouchEventDic.Remove(touchEventTypeStr);

                            combobox.SelectedIndex = 0;// = touchEventItem.TouchEventNames.Where(o => o.Value.Equals(touchEventEditorWindow.SelectedUserTouchEvent) == true).FirstOrDefault();
                            
                        }
                    }
                    else
                    {
                        combobox.SelectedItem = previousSelectedItem;
                    }
                }
                else
                {
                    if (this.tempUserTouchEventDic.ContainsKey(touchEventTypeStr) == true)
                    {
                        this.tempUserTouchEventDic[touchEventTypeStr] = keyvalue.Value;
                    }
                    else
                    {
                        this.tempUserTouchEventDic.Add(touchEventTypeStr, keyvalue.Value);
                    }

                    touchEventItem.TouchEventName = keyvalue.Value;
                }
            }
        }

        public void SetTouchEventDic(Dictionary<string, string> dic)
        {
            this.ComboBoxItemTouchEventDic.Clear();
            this.ComboBoxItemTouchEventDic.Add("EMPTY_TOUCHEVENT", "");
            if (dic != null)
            {
                foreach (var keyvalue in dic)
                    this.ComboBoxItemTouchEventDic.Add(keyvalue.Key, keyvalue.Value);
            }
            this.ComboBoxItemTouchEventDic.Add("EDIT_TOUCHEVENT", "[편집] ...");

            UpdateTouchEventNames();
        }      

        public void UpdateTouchEventNames()
        {
            foreach (var item in this.touchEventItems)
            {
                Dictionary<string, string> tempDic = new Dictionary<string,string>();
                item.TouchEventNames.Clear();
                foreach (var keyvalue in this.ComboBoxItemTouchEventDic)
                {
                    tempDic.Add(keyvalue.Key, keyvalue.Value);
                }

                item.TouchEventNames = tempDic;
            }
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void ApplyButton_Click(object sender, RoutedEventArgs e)
        {
            this.TouchEventDic.Clear();
            foreach (var keyvalue in this.tempUserTouchEventDic)
            {
                if (string.IsNullOrEmpty(keyvalue.Value.Trim()) == true)
                    continue;
                this.TouchEventDic.Add(keyvalue.Key, keyvalue.Value);
            }
            
            this.DialogResult = true;
        }

        private void TouchEventEditorComboBox_Loaded(object sender, RoutedEventArgs e)
        {
            ComboBox combobox = sender as ComboBox;
            if (combobox != null)
            {
                TouchEventItem te_item = combobox.Tag as TouchEventItem;
                if (te_item != null)
                {
                    if (this.tempUserTouchEventDic.ContainsKey(te_item.TouchEventType) == true)
                    {
                        combobox.SelectedItem = te_item.TouchEventNames.Where(o => o.Value.Equals(this.tempUserTouchEventDic[te_item.TouchEventType]) == true).FirstOrDefault();
                        if (combobox.SelectedItem == null)
                        {
                            combobox.SelectedIndex = 0;
                            te_item.TouchEventName = "";
                        }
                        else
                        {
                            te_item.TouchEventName = this.tempUserTouchEventDic[te_item.TouchEventType];
                        }
                    }
                    else
                    {
                        combobox.SelectedIndex = 0;
                        te_item.TouchEventName = "";
                    }
                }
            }
        }

        public RootContent CreateEventRootContent()
        {
            TouchEventViewController touchEventViewController = TouchEventViewController.Instance;
            return touchEventViewController.CreateEventRootContent();
        }

        public ScreenTouchContent CreateScreenTouchContent(RootContent rootContent, string screenName, string screenID, bool isSelf)
        {
            TouchEventViewController touchEventViewController = TouchEventViewController.Instance;
            return touchEventViewController.CreateScreenTouchContent(rootContent, screenName, screenID, isSelf);
        }

        public ElementTouchContent CreateElementTouchContent(ScreenTouchContent screenContent, string elementName, EventObjectType objectType, bool isSelf)
        {
            TouchEventViewController touchEventViewController = TouchEventViewController.Instance;
            return touchEventViewController.CreateElementTouchContent(screenContent, elementName, objectType, isSelf);
        }
        
    }

    //public static class CollectionTouchEvent
    //{
    //    private static Dictionary<string, string> touchEventDic = new Dictionary<string, string>();
    //    public static Dictionary<string, string> GetTouchEventDic()
    //    {
    //        return touchEventDic;
    //    }

    //    public static void SetTouchEventDic(Dictionary<string, string> dic)
    //    {
    //        touchEventDic.Clear();
    //        touchEventDic.Add("EMPTY_TOUCHEVENT", "");
            
    //        foreach (var keyvalue in dic)
    //            touchEventDic.Add(keyvalue.Key, keyvalue.Value);
    //        touchEventDic.Add("EDIT_TOUCHEVENT", "[편집] ...");
    //    }
    //}

    public class TouchEventItem : INotifyPropertyChanged
    {
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        public virtual void OnPropertyChanged(params string[] propertyNames)
        {
            if (PropertyChanged == null)
                return;

            foreach (string propertyName in propertyNames)
            {
                try
                {
                    PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
                }
                catch { }
            }
        }

        #endregion

        string touchEventType ;
        string touchEventName ;
        Dictionary<string, string> touchEventNames;

        public TouchEventItem()
        {
            touchEventNames = new Dictionary<string, string>();
        }

        public string TouchEventType
        {
            get { return this.touchEventType; }
            set { this.touchEventType = value; OnPropertyChanged("TouchEventType"); } 
        }

        public string TouchEventName
        {
            get { return this.touchEventName; }
            set { this.touchEventName = value; OnPropertyChanged("TouchEventName"); }
        }

        public Dictionary<string, string> TouchEventNames
        {
            get { return touchEventNames; }
            set { touchEventNames = value; OnPropertyChanged("TouchEventNames"); }
        }
    }
}
