﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TouchEventEditor.editors
{
    /// <summary>
    /// Interaction logic for NumberInputWindow.xaml
    /// </summary>
    public partial class PositionInputWindow : Window
    {
        private Point position;
        public Point Position
        {
            get { return this.position; }
            set { this.position = value; }
        }

        public PositionInputWindow()
        {
            InitializeComponent();

            this.Loaded += new RoutedEventHandler(PositionInputWindow_Loaded);
        }

        void PositionInputWindow_Loaded(object sender, RoutedEventArgs e)
        {
            XPositionInputValue.Text = this.position.X.ToString();
            YPositionInputValue.Text = this.position.Y.ToString();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            double xvalue = 0, yvalue=0;
            if (double.TryParse(XPositionInputValue.Text, out xvalue) == false)
            {
                return;
            }
            if (double.TryParse(YPositionInputValue.Text, out yvalue) == false)
            {
                return;
            }

            position.X = xvalue;
            position.Y = yvalue;
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}
