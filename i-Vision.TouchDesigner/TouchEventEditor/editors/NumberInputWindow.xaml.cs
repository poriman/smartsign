﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TouchEventEditor.editors
{
    /// <summary>
    /// Interaction logic for NumberInputWindow.xaml
    /// </summary>
    public partial class NumberInputWindow : Window
    {
        private double prevValue;

        private double maxNumberValue = 0;
        public double MaxNumberValue
        {
            get { return maxNumberValue; }
            set { maxNumberValue = value; }
        }

        private double numberValue;
        public double NumberValue
        {
            get { return this.numberValue; }
            set { this.numberValue = value; }
        }

        public NumberInputWindow()
        {
            InitializeComponent();

            this.Loaded += new RoutedEventHandler(NumberInputWindow_Loaded);
        }

        void NumberInputWindow_Loaded(object sender, RoutedEventArgs e)
        {
            numberInputValue.Text = this.numberValue.ToString();

            prevValue = this.numberValue;
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            double value = 0;
            if (double.TryParse(numberInputValue.Text, out value) == false)
            {
                return;
            }

            numberValue = value;
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void numberInputValue_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox textbox = sender as TextBox;
            if (textbox != null)
            {
                double value = 0;
                if (double.TryParse(textbox.Text, out value) == true)
                {
                    if (maxNumberValue != -1)
                    {
                        if (value > maxNumberValue)
                        {
                            textbox.Text = prevValue.ToString();
                            return;
                        }
                    }
                }

                prevValue = value;
            }
        }
    }
}
