﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TouchEventEditor.editors
{
    /// <summary>
    /// Interaction logic for NumberInputWindow.xaml
    /// </summary>
    public partial class SizeInputWindow : Window
    {
        private Size sizeValue;
        public Size SizeValue
        {
            get { return this.sizeValue; }
            set { this.sizeValue = value; }
        }

        public SizeInputWindow()
        {
            InitializeComponent();

            this.Loaded += new RoutedEventHandler(SizeInputWindow_Loaded);
        }

        void SizeInputWindow_Loaded(object sender, RoutedEventArgs e)
        {
            XPositionInputValue.Text = this.sizeValue.Width.ToString();
            YPositionInputValue.Text = this.sizeValue.Height.ToString();
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            double xvalue = 0, yvalue=0;
            if (double.TryParse(XPositionInputValue.Text, out xvalue) == false)
            {
                return;
            }
            if (double.TryParse(YPositionInputValue.Text, out yvalue) == false)
            {
                return;
            }

            sizeValue.Width = xvalue;
            sizeValue.Height = yvalue;
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}
