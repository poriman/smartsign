﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using BrushPicker;
using System.Windows;
using TouchEventEditor.editors;

namespace TouchEventEditor
{
    public class TouchEventEditorManager
    {
        private static readonly TouchEventEditorManager _instance = new TouchEventEditorManager();

        public static TouchEventEditorManager Instance
        {
            get { return _instance; }
        }

        private TouchEventEditorManager()
        {
        }
       
        public Brush ShowBrushEditor(Brush brush)
        {
            BrushEditorWindow brushEditor = new BrushEditorWindow(false);
            brushEditor.Owner = Application.Current.MainWindow;
            brushEditor.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            brushEditor.SetBrush(brush);
            if (brushEditor.ShowDialog() == true)
            {
                return brushEditor.BrushValue;
            }

            return brush;
        }

        public double ShowNumberInputEditor(double numValue, double maxInputValue)
        {
            NumberInputWindow numInputWin = new NumberInputWindow();
            numInputWin.Owner = Application.Current.MainWindow;
            numInputWin.NumberValue = numValue;
            numInputWin.MaxNumberValue = maxInputValue;
            if (numInputWin.ShowDialog() == true)
            {
                return numInputWin.NumberValue;
            }

            return numValue;
        }

        public Size ShowSizeInputEditor(Size numValue)
        {
            SizeInputWindow numInputWin = new SizeInputWindow();
            numInputWin.Owner = Application.Current.MainWindow;
            numInputWin.SizeValue = numValue;
            if (numInputWin.ShowDialog() == true)
            {
                return numInputWin.SizeValue;
            }

            return numValue;
        }

        public Point ShowPositionInputEditor(double valueX, double valueY)
        {
            Point point = new Point(valueX, valueY);
            PositionInputWindow positionInputWin = new PositionInputWindow();
            positionInputWin.Owner = Application.Current.MainWindow;
            positionInputWin.Position = point;
            if (positionInputWin.ShowDialog() == true)
            {
                return positionInputWin.Position;
            }

            return point;
        }
    }
}
