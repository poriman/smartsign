﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using TouchEventEditor.Model;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using TouchEventEditor.View;
using System.Xml.Linq;
using System.Windows.Markup;
using System.IO;

namespace TouchEventEditor.ViewModel
{
    public class UserTouchEventViewModel : ViewModelBase
    {
        private UserTouchEventView userTouchEventView;
        private ComboBox userTouchEventComboBox;
        private TouchEventType currentTouchEventType;
        private UserTouchEvent currentUserTouchEvent;
        public ObservableCollection<UserTouchEvent> UserTouchEvents { get; set; }
        public ObservableCollection<UserContentFunction> UserContentFunctionList { get; set; }
        private XElement projectTouchEventXElement { get; set; }
        private string userTouchEventName;
        public string UserTouchEventName 
        {
            get { return userTouchEventName; }
            set { userTouchEventName = value; OnPropertyChanged("UserTouchEventName"); }
        }

        #region IsDesignTime

        public bool IsDesignTime
        {
            get
            {
                return (Application.Current == null) || (Application.Current.GetType() == typeof(Application));
            }
        }

        #endregion

        public UserTouchEventViewModel()
        {
            UserTouchEvents = new ObservableCollection<UserTouchEvent>();
            UserContentFunctionList = new ObservableCollection<UserContentFunction>();
            currentUserTouchEvent = new UserTouchEvent();

            if (!this.IsDesignTime)
            {
                UserTouchEvent blankUserTouchEvent = new UserTouchEvent();
                blankUserTouchEvent.TouchEventName = "                                   ";
                UserTouchEvents.Add(blankUserTouchEvent);
            }
        }

        public void InsertTouchEventFunction(System.Windows.RoutedEventArgs e)
        {
            EventFunctionEventArgs eventargs = e.OriginalSource as EventFunctionEventArgs;
            if (eventargs != null)
            {
                ContentFunction contentFunction = eventargs.ContentFunction;
                UserContentFunction userContentFunction = new UserContentFunction(contentFunction.FunctionType, contentFunction.Function);
                switch (eventargs.EventType)
                {
                    case EventObjectType.None:
                        break;
                    case EventObjectType.Project:
                        userContentFunction.TargetFullPath = string.Format("{0}.{1}", EventObjectType.Project.ToString(), contentFunction.Function.ToString());
                        userContentFunction.TargetFullName = string.Format("{0}", EventObjectType.Project.ToString());
                        userContentFunction.ElementName = string.IsNullOrEmpty(eventargs.ContentName) == true ? "" : eventargs.ContentName;
                        userContentFunction.ScreenName = string.IsNullOrEmpty(eventargs.ContentName) == true ? "" : eventargs.PageName;
                        userContentFunction.ScreenID = string.IsNullOrEmpty(eventargs.ContentName) == true ? "" : eventargs.PageID;
                        break;
                    case EventObjectType.Screen:
                        userContentFunction.TargetFullPath = string.Format("{0}.{1}", eventargs.PageName, contentFunction.Function.ToString());
                        userContentFunction.TargetFullName = string.Format("{0}", eventargs.PageName);
                        userContentFunction.ElementName = string.IsNullOrEmpty(eventargs.ContentName) == true ? "" : eventargs.ContentName;
                        userContentFunction.ScreenName = string.IsNullOrEmpty(eventargs.ContentName) == true ? "" : eventargs.PageName;
                        userContentFunction.ScreenID = string.IsNullOrEmpty(eventargs.ContentName) == true ? "" : eventargs.PageID;
                        break;
                    case EventObjectType.Utility:
                        userContentFunction.TargetFullPath = string.Format("{0}.{1}", EventObjectType.Utility.ToString(), contentFunction.Function.ToString());
                        userContentFunction.TargetFullName = string.Format("{0}", EventObjectType.Utility.ToString());
                        userContentFunction.ElementName = string.IsNullOrEmpty(eventargs.ContentName) == true ? "" : eventargs.ContentName;
                        userContentFunction.ScreenName = string.IsNullOrEmpty(eventargs.ContentName) == true ? "" : eventargs.PageName;
                        userContentFunction.ScreenID = string.IsNullOrEmpty(eventargs.ContentName) == true ? "" : eventargs.PageID;
                        break;
                    default:
                        userContentFunction.TargetFullPath = string.Format("{0}.{1}.{2}", eventargs.PageName, eventargs.ContentName, contentFunction.Function.ToString());
                        userContentFunction.TargetFullName = string.Format("{0}.{1}", eventargs.PageName, eventargs.ContentName);
                        userContentFunction.ElementName = string.IsNullOrEmpty(eventargs.ContentName) == true ? "" : eventargs.ContentName;
                        userContentFunction.ScreenName = string.IsNullOrEmpty(eventargs.ContentName) == true ? "" : eventargs.PageName;
                        userContentFunction.ScreenID = string.IsNullOrEmpty(eventargs.ContentName) == true ? "" : eventargs.PageID;
                        break;
                }
                if (UserContentFunctionList.Count == 0)
                    userContentFunction.OrderNo = 1;
                else
                    userContentFunction.OrderNo = UserContentFunctionList.Max(o => o.OrderNo) + 1;

                SaveUserContentFunction(userContentFunction);
            }
        }

        public void SaveUserContentFunction(UserContentFunction userContentFunction)
        {
            userContentFunction.OrderNo = UserContentFunctionList.Count + 1;
            UserContentFunctionList.Add(userContentFunction);
        }

        public void SaveUserTouchEvent()
        {
            if (UserTouchEvents.Contains(currentUserTouchEvent) == true)
            {
            }
            else
            {
                int maxIndex = currentUserTouchEvent.UserContentFunctionList.Max(o => o.OrderNo);
                UserTouchEvents.Add(currentUserTouchEvent);
            }
        }

        public void QuickSetupButton_Click(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            if (button != null)
            {
                UserContentFunction usercontentfunction = button.DataContext as UserContentFunction;
                if (usercontentfunction != null)
                {
                    TouchEventEditorManager controller = TouchEventEditorManager.Instance;
                    switch (usercontentfunction.Function)
                    {
                        case Function.Background:
                            {
                                Brush bursh = new SolidColorBrush(Colors.White);
                                if (usercontentfunction.Value != null)
                                {
                                    bursh = (Brush)usercontentfunction.Value;
                                }
                                usercontentfunction.Value = controller.ShowBrushEditor(bursh);// new SolidColorBrush(Colors.Blue);
                                usercontentfunction.VariableName = "";
                                usercontentfunction.ValueType = Model.ValueType.object_value;
                            }
                            break;
                        case Function.Foreground:
                            {
                                Brush bursh = new SolidColorBrush(Colors.White);
                                if (usercontentfunction.Value != null)
                                {
                                    bursh = (Brush)usercontentfunction.Value;
                                }
                                usercontentfunction.Value = controller.ShowBrushEditor(bursh);// new SolidColorBrush(Colors.Blue);
                                usercontentfunction.VariableName = "";
                                usercontentfunction.ValueType = Model.ValueType.object_value;
                            }
                            break;
                        case Function.Width:
                            {
                                double width = 0;
                                if (usercontentfunction.Value != null)
                                {
                                    width = (double)usercontentfunction.Value;
                                }
                                usercontentfunction.Value = controller.ShowNumberInputEditor(width, -1);// new SolidColorBrush(Colors.Blue);
                                usercontentfunction.VariableName = "";
                                usercontentfunction.ValueType = Model.ValueType.number_value;
                            }
                            break;
                        case Function.Height:
                            {
                                double height = 0;
                                if (usercontentfunction.Value != null)
                                {
                                    height = (double)usercontentfunction.Value;
                                }
                                usercontentfunction.Value = controller.ShowNumberInputEditor(height, -1);// new SolidColorBrush(Colors.Blue);
                                usercontentfunction.VariableName = "";
                                usercontentfunction.ValueType = Model.ValueType.number_value;
                            }
                            break;
                        case Function.Move:
                            {
                                Point point = new Point();
                                if (usercontentfunction.Value != null)
                                {
                                    point = (Point)usercontentfunction.Value;
                                   
                                }
                                usercontentfunction.Value = controller.ShowPositionInputEditor(point.X, point.Y);// new SolidColorBrush(Colors.Blue);
                                usercontentfunction.VariableName = "";
                                usercontentfunction.ValueType = Model.ValueType.object_value;
                            }
                            break;
                        case Function.Resize:
                            {
                                Size size = new Size();
                                if (usercontentfunction.Value != null)
                                {
                                    size = (Size)usercontentfunction.Value;
                                }
                                usercontentfunction.Value = controller.ShowSizeInputEditor(size);// new SolidColorBrush(Colors.Blue);
                                usercontentfunction.VariableName = "";
                                usercontentfunction.ValueType = Model.ValueType.object_value;
                            }
                            break;
                        case Function.Rotation:
                            {
                                double rotation = 0;
                                if (usercontentfunction.Value != null)
                                {
                                    rotation = (double)usercontentfunction.Value;
                                }
                                usercontentfunction.Value = controller.ShowNumberInputEditor(rotation, 360);// new SolidColorBrush(Colors.Blue);
                                usercontentfunction.VariableName = "";
                                usercontentfunction.ValueType = Model.ValueType.number_value;
                            }
                            break;
                        case Function.Opacity:
                            {
                                double opacity = 0;
                                if (usercontentfunction.Value != null)
                                {
                                    opacity = (double)usercontentfunction.Value;
                                }
                                usercontentfunction.Value = controller.ShowNumberInputEditor(opacity, 1);// new SolidColorBrush(Colors.Blue);
                                usercontentfunction.VariableName = "";
                                usercontentfunction.ValueType = Model.ValueType.number_value;
                            }
                            break;
                        case Function.Text:
                            {
                               
                            }
                            break;                       
                    }
                }
            }
        }

        public void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            if (button != null)
            {
                UserContentFunction usercontentfunction = button.DataContext as UserContentFunction;
                if (usercontentfunction != null)
                {                    
                    this.UserContentFunctionList.Remove(usercontentfunction);

                    int noIndex = 1;
                    UserContentFunctionList.ToList().ForEach(o => o.OrderNo = noIndex++);
                }
            }
        }

        private void UpdateUserTouchEvent(UserTouchEvent updateUserTouchEvent)
        {
            updateUserTouchEvent.UserContentFunctionList.Clear();
            foreach (var item in this.UserContentFunctionList)
            {
                updateUserTouchEvent.UserContentFunctionList.Add(this.CopyUserContentFunction(item));
            }
        }

        private void RenameUserTouchEventFile(string sourceEventName, string changeEventName)
        {
            if (this.projectTouchEventXElement != null)
            {
                XElement toucheventXElement = this.projectTouchEventXElement.Elements().Where(o => o.Attribute("name").Value.Equals(sourceEventName) == true || o.Attribute("name").Value.ToLower().Equals(sourceEventName.ToLower()) == true).FirstOrDefault();
                if (toucheventXElement != null)
                {
                    toucheventXElement.Attribute("name").Value = changeEventName;
                }
            }
        }

        private void DeleteUserTouchEventFile(string sourceEventName)
        {
            if (this.projectTouchEventXElement != null)
            {
                XElement toucheventXElement = this.projectTouchEventXElement.Elements().Where(o => o.Attribute("name").Value.Equals(sourceEventName) == true || o.Attribute("name").Value.ToLower().Equals(sourceEventName.ToLower()) == true).FirstOrDefault();
                if (toucheventXElement != null)
                {
                    toucheventXElement.Remove();                   
                }
            }
        }

        public void UserTouchEventSaveButton_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(UserTouchEventName) == true)
                return;

            if (this.UserContentFunctionList.Count <= 0)
                return;

            if (userTouchEventComboBox != null)
            {
                UserTouchEvent userTouchEvent = userTouchEventComboBox.SelectedItem as UserTouchEvent;
                if (userTouchEvent != null)
                {
                    if (string.IsNullOrEmpty(userTouchEvent.TouchEventName.Trim()) != true)
                    {
                        if (userTouchEvent.TouchEventName.Equals(UserTouchEventName) == false || userTouchEvent.TouchEventName.ToLower().Equals(UserTouchEventName.ToLower()) == false)
                        {
                            this.RenameUserTouchEventFile(userTouchEvent.TouchEventName, UserTouchEventName);
                            userTouchEvent.TouchEventName = UserTouchEventName;
                        }
                        
                        this.UpdateUserTouchEvent(userTouchEvent);
                        this.UpdateTouchEvent(userTouchEvent);
                        return;
                    }                    
                }
            }

            if (this.UserTouchEvents.Where(o => o.TouchEventName.Equals(UserTouchEventName) == true || o.TouchEventName.ToLower().Equals(UserTouchEventName.ToLower()) == true).FirstOrDefault() != null) 
            {
                MessageBox.Show(string.Format("동일한 이름 \"{0}\"의 Touch Event 항목이 존재합니다.", this.UserTouchEventName), "Touch Event");
                return ;
            }

            this.currentUserTouchEvent = CreateUserTouchEvent(this.UserTouchEventName, this.UserContentFunctionList);

            this.UserTouchEvents.Add(this.currentUserTouchEvent);

            userTouchEventComboBox.SelectedItem = this.currentUserTouchEvent;

            XElement regEventXElement = CreateTouchEvent(this.currentUserTouchEvent, this.currentUserTouchEvent.TouchEventName);//테스트 - 구현중...
            if (regEventXElement != null)
            {
                if (this.projectTouchEventXElement != null)
                {
                    List<XElement> eventListXElement = this.projectTouchEventXElement.Elements().ToList();
                    XElement existEvnetXElement = eventListXElement.Where(o => o.Attribute("name").Value.Equals(regEventXElement.Attribute("name").Value) == true || o.Attribute("name").Value.ToLower().Equals(regEventXElement.Attribute("name").Value.ToLower()) == true).FirstOrDefault();
                    if (existEvnetXElement != null)
                    {
                        existEvnetXElement.Remove();
                    }

                    this.projectTouchEventXElement.Add(regEventXElement);

                    this.projectTouchEventXElement.Save(userTouchEventView.TouchEventFilePath);
                }
            }
        }   

        private UserTouchEvent CreateUserTouchEvent(string userTouchEventName, ObservableCollection<UserContentFunction> userContentFunctionCollection)
        {
            UserTouchEvent userTouchEvent = new UserTouchEvent();

            userTouchEvent.TouchEventName = this.UserTouchEventName;            

            foreach (var item in userContentFunctionCollection)
            {
                userTouchEvent.UserContentFunctionList.Add(this.CopyUserContentFunction(item));
            }

            return userTouchEvent;
        }

        private UserContentFunction CopyUserContentFunction(UserContentFunction sourceUserContentFunction)
        {
            UserContentFunction copyUserContentFunction = new UserContentFunction(sourceUserContentFunction.FunctionType, sourceUserContentFunction.Function);
            copyUserContentFunction.ContentType = sourceUserContentFunction.ContentType;
            copyUserContentFunction.ElementName = sourceUserContentFunction.ElementName;
            copyUserContentFunction.FunctionName = sourceUserContentFunction.FunctionName;
            copyUserContentFunction.TargetFullPath = sourceUserContentFunction.TargetFullPath;
            copyUserContentFunction.TargetFullName = sourceUserContentFunction.TargetFullName;
            copyUserContentFunction.OrderNo = sourceUserContentFunction.OrderNo;
            copyUserContentFunction.ValueType = sourceUserContentFunction.ValueType;
            copyUserContentFunction.Value = sourceUserContentFunction.Value;
            copyUserContentFunction.VariableName = sourceUserContentFunction.VariableName;
            copyUserContentFunction.ScreenID = sourceUserContentFunction.ScreenID;
            copyUserContentFunction.ScreenName = sourceUserContentFunction.ScreenName;
            copyUserContentFunction.ElementName = sourceUserContentFunction.ElementName;
            return copyUserContentFunction;
        }

        public void UserTouchEventDeleteButton_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(UserTouchEventName) == true)
                return;

            if (this.UserContentFunctionList.Count <= 0)
                return;

            UserTouchEvent userTouchEvent = this.UserTouchEvents.Where(o => o.TouchEventName.Equals(this.UserTouchEventName) == true || o.TouchEventName.ToLower().Equals(this.UserTouchEventName.ToLower()) == true).FirstOrDefault();
            if (userTouchEvent != null)
            {
                if (MessageBox.Show(string.Format("\"{0}\"을 삭제하시겠습니까?", this.UserTouchEventName), "Touch Event 항목 삭제", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {
                    this.DeleteUserTouchEventFile(this.UserTouchEventName);
                    this.UserTouchEvents.Remove(userTouchEvent);
                    userTouchEventComboBox.SelectedIndex = 0;
                }
            }
        }

        public void UserTouchEventTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            UserTouchEventName = (sender as TextBox).Text;
        }

        public void UserTouchEventView_Loaded(object sender, RoutedEventArgs e)
        {
            userTouchEventView = sender as UserTouchEventView;
            if (userTouchEventView != null)
            {
                this.UserTouchEventName = userTouchEventView.UserTouchEventName;//이전 UserTouchEventName 전달.
                this.currentTouchEventType = userTouchEventView.TouchEventType;

                try
                {
                    if (string.IsNullOrEmpty(userTouchEventView.TouchEventFilePath) == true)
                        return;
                    if (File.Exists(userTouchEventView.TouchEventFilePath) == false)
                    {
                        this.projectTouchEventXElement = this.CreateNewProjectTouchEvent();
                        this.projectTouchEventXElement.Save(userTouchEventView.TouchEventFilePath);
                    }
                    else
                    {
                        this.projectTouchEventXElement = XElement.Load(userTouchEventView.TouchEventFilePath);
                    }

                    SetUserTouchEventFromXElement(this.projectTouchEventXElement);                    
                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.Message + " >> " + ex.StackTrace);
                }
            }
        }

        public void SetUserTouchEventFromXml(string touchEvnetXmlFilePath)
        {
            XElement projectTouchEventXElement = XElement.Load(touchEvnetXmlFilePath);

            SetUserTouchEventFromXElement(projectTouchEventXElement);
            
        }

        public void SetUserTouchEventFromXElement(XElement touchEventXElement)
        {
            foreach (var userEventXElement in touchEventXElement.Elements())
            {
                UserTouchEvent userTouchEvent = new UserTouchEvent();
                userTouchEvent.TouchEventName = userEventXElement.Attribute("name").Value;

                XElement actions = userEventXElement.Elements().FirstOrDefault();
                if (actions == null)
                    return ;
                foreach (var actionXElement in actions.Elements())
                {
                    UserContentFunction userContentFunction = new UserContentFunction(actionXElement.Attribute("type").Value, actionXElement.Attribute("name").Value);
                    userContentFunction.OrderNo = int.Parse(actionXElement.Attribute("order").Value);
                    userContentFunction.TargetFullName = actionXElement.Attribute("targetname").Value;
                    userContentFunction.TargetFullPath = actionXElement.Attribute("targetpath").Value;
                    userContentFunction.ScreenName = actionXElement.Attribute("screenname").Value;
                    userContentFunction.ScreenID = actionXElement.Attribute("screenid").Value;
                    userContentFunction.ElementName = actionXElement.Attribute("elementname").Value;

                    foreach (var paramXElement in actionXElement.Elements())//지금은 하나의 parameter만 지원한다. 
                    {
                        TouchEventEditor.Model.ValueType paramValueType = (TouchEventEditor.Model.ValueType)Enum.Parse(typeof(TouchEventEditor.Model.ValueType), paramXElement.Attribute("type").Value, true);
                        userContentFunction.ValueType = paramValueType;
                        switch (paramValueType)
                        {
                            case Model.ValueType.number_value:
                                userContentFunction.Value = double.Parse(paramXElement.Value);
                                break;                            
                            case Model.ValueType.string_value:
                                userContentFunction.Value = paramXElement.Value;
                                break;
                            case Model.ValueType.object_value:
                                XElement element = paramXElement.Elements().FirstOrDefault();
                                if (element != null)
                                {
                                    StringReader stringReader = new StringReader(element.ToString());
                                    System.Xml.XmlReader xmlReader = System.Xml.XmlReader.Create(stringReader);
                                    userContentFunction.Value = XamlReader.Load(xmlReader);
                                }
                                break;
                        }
                        break;
                    }

                    userTouchEvent.UserContentFunctionList.Add(userContentFunction);
                }

                this.UserTouchEvents.Add(userTouchEvent);
            }
            
        }      
  
        /// <summary>
        /// 터치 이벤트명에 해당하는 컨텐츠의 속성 값을 가져온다.
        /// </summary>
        /// <param name="touchEventName">터치 이벤트 이름</param>
        /// <param name="contentName">컨텐츠 이름</param>
        /// <returns></returns>
        public List<object> GetContentPropertyValue(string touchEventName, string contentName, string propertyName)
        {
            if (this.UserTouchEvents != null && this.UserTouchEvents.Count > 0)
            {
                UserTouchEvent userTouchEvent = this.UserTouchEvents.Where(o => o.TouchEventName.Equals(touchEventName) == true || o.TouchEventName.ToLower().Equals(touchEventName.ToLower()) == true).FirstOrDefault();
                if (userTouchEvent != null)
                {
                    UserContentFunction userContentFunction = userTouchEvent.UserContentFunctionList.
                        Where(o => (o.ElementName.Equals(contentName) == true || o.ElementName.ToLower().Equals(contentName.ToLower()) == true) && (o.FunctionName.Equals(propertyName) == true || o.FunctionName.ToLower().Equals(propertyName.ToLower()) == true)).
                        FirstOrDefault();
                    if (userContentFunction != null)
                    {
                        return new List<object>() {userContentFunction.Value };// 값 배열로 전달
                    }
                }
            }
            return null; 
        }

        public void UserTouchEventComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //if (!this.IsDesignTime)
            if (userTouchEventComboBox != null)
            {
                UserTouchEvent userTouchEvent = userTouchEventComboBox.SelectedItem as UserTouchEvent;
                if (userTouchEvent != null)
                {
                    this.UserContentFunctionList.Clear();                    
                    foreach (var item in userTouchEvent.UserContentFunctionList)
                    {
                        this.UserContentFunctionList.Add(this.CopyUserContentFunction(item));
                    }
                    UserTouchEventName = userTouchEvent.TouchEventName.Trim();
                    this.currentUserTouchEvent = userTouchEvent;
                }
            }
        }


        public void UserTouchEventComboBox_Loaded(object sender, RoutedEventArgs e)
        {
            userTouchEventComboBox = sender as ComboBox;
            UserTouchEvent userTouchEvent = UserTouchEvents.Where(o => o.TouchEventName.Equals(this.userTouchEventView.UserTouchEventName) == true || o.TouchEventName.ToLower().Equals(this.userTouchEventView.UserTouchEventName.ToLower()) == true).FirstOrDefault();
            if (userTouchEvent != null)
                userTouchEventComboBox.SelectedItem = userTouchEvent;
            else
                userTouchEventComboBox.SelectedIndex = 0;
        }

        public void UserTouchEventApplyButton_Click(object sender, RoutedEventArgs e)
        {
            string selectedUserTouchEvent = "";
            Dictionary<string, string> UserTouchEventDic = new Dictionary<string, string>();
            if (userTouchEventComboBox != null)
            {
                UserTouchEvent userTouchEvent = userTouchEventComboBox.SelectedItem as UserTouchEvent;
                if (userTouchEvent != null)
                {
                    userTouchEventView.UserTouchEventName = userTouchEvent.TouchEventName;
                    selectedUserTouchEvent = userTouchEvent.TouchEventName;
                }

                int i = 0;
                foreach (var item in userTouchEventComboBox.Items)
                {
                     UserTouchEvent tempItem = item as UserTouchEvent;
                     if (tempItem != null && tempItem.TouchEventName.Trim() != "")
                     {
                         UserTouchEventDic.Add(i.ToString(), tempItem.TouchEventName);
                         i++;
                     }
                }
            }

            this.UserTouchEvents.RemoveAt(0);//전달하기 전에 첫번째 공백은 삭제한다.
            userTouchEventView.RaiseCloseUserTouchEventViewEvent(true, UserTouchEventDic, selectedUserTouchEvent, this.UserTouchEvents.ToList());
        }

        public void UserTouchEventCloseButton_Click(object sender, RoutedEventArgs e)
        {
            if (userTouchEventComboBox != null)
            {
                UserTouchEvent userTouchEvent = userTouchEventComboBox.SelectedItem as UserTouchEvent;
                if (userTouchEvent != null)
                {
                    userTouchEventView.UserTouchEventName = userTouchEvent.TouchEventName;
                }
            }

            userTouchEventView.RaiseCloseUserTouchEventViewEvent(false, null, "", this.UserTouchEvents.ToList());
        }

        private XElement CreateNewProjectTouchEvent()
        {
            XElement toucheventXElement = new XElement("touchevent");

            return toucheventXElement;
        }

        private void UpdateTouchEvent(UserTouchEvent userTouchEvent)
        {
            if (this.projectTouchEventXElement != null)
            {
                List<XElement> eventListXElement = this.projectTouchEventXElement.Elements().ToList();
                XElement existEvnetXElement = eventListXElement.Where(o => o.Attribute("name").Value.Equals(userTouchEvent.TouchEventName) == true || o.Attribute("name").Value.ToLower().Equals(userTouchEvent.TouchEventName.ToLower()) == true).FirstOrDefault();
                XElement actionsElement = existEvnetXElement.Elements().FirstOrDefault();
                if (actionsElement != null)
                {
                    actionsElement.RemoveAll();
                    foreach (var userContentFunction in userTouchEvent.UserContentFunctionList)
                    {
                        XElement tempActionElement = this.CreateActionXElement(userContentFunction);
                        actionsElement.Add(tempActionElement);
                    }
                }               

                this.projectTouchEventXElement.Save(userTouchEventView.TouchEventFilePath);
            }
        }

        private XElement CreateActionXElement(UserContentFunction userContentFunction)
        {
            XElement actionElement = new XElement("action",
                    new XAttribute("order", userContentFunction.OrderNo),
                    new XAttribute("type", userContentFunction.FunctionType),
                    new XAttribute("name", userContentFunction.FunctionName),
                    new XAttribute("targetname", userContentFunction.TargetFullName),
                    new XAttribute("targetpath", userContentFunction.TargetFullPath),
                    new XAttribute("screenname", userContentFunction.ScreenName),
                    new XAttribute("screenid", userContentFunction.ScreenID),
                    new XAttribute("elementname", userContentFunction.ElementName));

            XElement paramElement = new XElement("param", new XAttribute("order", 1), new XAttribute("type", userContentFunction.ValueType));
            this.SaveValue(paramElement, userContentFunction.FunctionName, userContentFunction.ValueType, userContentFunction.Value);
            actionElement.Add(paramElement);

            return actionElement;
        }

        private XElement CreateTouchEvent(UserTouchEvent userTouchEvent, string userEventName)
        {
            XElement eventElement = new XElement("userevent", new XAttribute("name", userEventName));
            XElement actionsElement = new XElement("actions");
            eventElement.Add(actionsElement);

            foreach (var userContentFunction in userTouchEvent.UserContentFunctionList)
            {
                XElement actionElement = this.CreateActionXElement(userContentFunction);
                actionsElement.Add(actionElement);
            }
            return eventElement;
        }

        private XElement SaveValue(XElement paramElement, string functionName, TouchEventEditor.Model.ValueType valueType, object objValue)
        {
            switch (functionName)
            {
                case "Foreground":
                case "Background":
                    {
                        if (valueType == Model.ValueType.object_value)
                        {
                            string xaml = XamlWriter.Save(objValue);
                            XElement xamlXElement = XElement.Parse(xaml);
                            paramElement.Add(xamlXElement);
                        }
                        else
                        {
                            paramElement.Value = objValue.ToString();
                        }                        
                    }
                    break;
                case "Text":
                    {
                        if (valueType == Model.ValueType.object_value)
                        {
                            string xaml = XamlWriter.Save(objValue);
                            XElement xamlXElement = XElement.Parse(xaml);
                            paramElement.Add(xamlXElement);
                        }
                        else
                        {
                            paramElement.Value = objValue.ToString();
                        }                        
                    }
                    break;
                case "Opacity":
                case "Rotation":
                case "PositionX":
                case "PositionY":
                case "Width":
                case "Height":
                    {
                        paramElement.Value = objValue.ToString();                        
                    }
                    break;
                case "Move":
                    {
                        if (valueType == Model.ValueType.object_value)
                        {
                            string xaml = XamlWriter.Save(objValue);
                            XElement xamlXElement = XElement.Parse(xaml);
                            paramElement.Add(xamlXElement);                            
                        }
                        else
                        {
                            paramElement.Value = objValue.ToString();
                        }      
                    }
                    break;
                case "Content":
                    break;
                case "EnableEvent":
                    break;
                case "Visible":
                    break;
                case "Mute":
                    break;
                case "Volumn":
                    break;
                case "Sleep":
                    break;
            }

            return paramElement;
        }

        private void ConvertTouchEventFileFormat(UserTouchEvent userTouchEvent)
        {
            XElement projectTouchEvent = new XElement("project", 
                new XAttribute("name", userTouchEvent.TouchEventName));

            XElement events = new XElement("events");
            projectTouchEvent.Add(events);
        }
    }
}
