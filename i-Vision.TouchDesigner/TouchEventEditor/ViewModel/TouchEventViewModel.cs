﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using TouchEventEditor.Model;
using System.Windows.Controls;
using System.Windows;
using System.Windows.Input;
using TouchEventEditor.View;

namespace TouchEventEditor.ViewModel
{
    public class TouchEventViewModel : ViewModelBase
    {
        private TouchContent selfTouchContent;
        private object currentSelectedItem;
        private TouchEventView touchEventView;
        private string selectedScreenID;
              
        public TouchEventViewModel()
        {
            RootContents = new ObservableCollection<RootContent>();
            ContentFunctions = new ObservableCollection<ContentFunction>();

            //MockUp_EventContentItemData();

            //MockUp_EventFunction();
        }

        public RootContent CreateEventRootContent()
        {
            RootContent selfRootContent = new RootContent();
            selfRootContent.RootName = "Self";
            selfRootContent.RootType = RootType.Self;

            RootContent utilityRootContent = new RootContent();
            utilityRootContent.RootName = "Utility";
            utilityRootContent.RootType = RootType.Utility;

            RootContent contentsRootContent = new RootContent();
            contentsRootContent.RootName = "Project";
            contentsRootContent.RootType = RootType.Project;
            contentsRootContent.ScreenContents = new ObservableCollection<ScreenTouchContent>();

            RootContents.Add(selfRootContent);
            RootContents.Add(utilityRootContent);
            RootContents.Add(contentsRootContent);

            return contentsRootContent;
        }

        public ScreenTouchContent CreateScreenTouchContent(RootContent rootContent, string screenName, string screenID)
        {
            ScreenTouchContent screenTouchContent = new ScreenTouchContent();
            screenTouchContent.ScreenName = screenName;
            screenTouchContent.ScreenID = screenID;
            screenTouchContent.TouchContentType = EventObjectType.Screen;
            screenTouchContent.ElementContents = new ObservableCollection<ElementTouchContent>();

            rootContent.ScreenContents.Add(screenTouchContent);

            return screenTouchContent;
        }

        public ElementTouchContent CreateElementTouchContent(ScreenTouchContent screenContent, string elementName, EventObjectType objectType)
        {
            ElementTouchContent elementTouchContent = new ElementTouchContent();
            elementTouchContent.ScreenName = screenContent.ScreenName;
            elementTouchContent.ElementName = elementName;
            elementTouchContent.TouchContentType = objectType;

            screenContent.ElementContents.Add(elementTouchContent);

            return elementTouchContent;
        }

        public void SetSelfTouchContent(TouchContent touchContent)
        {
            selfTouchContent = touchContent;
        }

        private void MockUp_EventContentItemData()
        {
            RootContent contentsRootContent = CreateEventRootContent();

            ScreenTouchContent screenContent1 = CreateScreenTouchContent(contentsRootContent, "Screen1", "1229-d93a-2342-c839-329d-830b");

            ElementTouchContent buttonContent1 = CreateElementTouchContent(screenContent1, "Button1", EventObjectType.Button);

            ElementTouchContent flashContent1 = CreateElementTouchContent(screenContent1, "Flash1", EventObjectType.Flash);

            ScreenTouchContent screenContent2 = CreateScreenTouchContent(contentsRootContent, "Screen2", "3a29-d93a-2342-c839-329d-810c");

            ElementTouchContent buttonContent2 = CreateElementTouchContent(screenContent2, "Button2", EventObjectType.Button);

            ElementTouchContent imageContent1 = CreateElementTouchContent(screenContent2, "Image1", EventObjectType.Image);

            SetSelfTouchContent(imageContent1);
        }

        public void MockUp_EventFunction()
        {
            MakeEventFunction(EventObjectType.Screen);
        }

        public void MakeEventFunction(EventObjectType contentType)
        {
            ContentFunctions.Clear();

            switch (contentType)
            {
                case EventObjectType.Utility:
                    AddContentFunction(Function.Sleep);
                    AddContentFunction(Function.RunProgram);
                    AddContentFunction(Function.SaveFile);
                    AddContentFunction(Function.ReadFile);
                    break;
                case EventObjectType.Project:                      
                    AddContentFunction(Function.MoveScreen);
                    AddContentFunction(Function.MoveNextScreen);
                    AddContentFunction(Function.MoveBackScreen);
                    AddContentFunction(Function.MovePreviousScreen);
                    break;
                case EventObjectType.Screen:
                    AddContentFunction(Function.Background);
                    AddContentFunction(Function.EnableEvent);
                    AddContentFunction(Function.Opacity);                  
                    break;
                case EventObjectType.Button:
                    AddContentFunction(Function.Move);
                    AddContentFunction(Function.Resize);
                    AddContentFunction(Function.Rotation);
                    AddContentFunction(Function.Background);
                    AddContentFunction(Function.EnableEvent);
                    AddContentFunction(Function.Width);
                    AddContentFunction(Function.Height);
                    AddContentFunction(Function.Opacity);
                    AddContentFunction(Function.Content);
                    break;
                case EventObjectType.Image:
                    AddContentFunction(Function.Move);
                    AddContentFunction(Function.Resize);
                    AddContentFunction(Function.Rotation);
                    AddContentFunction(Function.Background);                   
                    AddContentFunction(Function.EnableEvent); 
                    AddContentFunction(Function.Width);
                    AddContentFunction(Function.Height);
                    AddContentFunction(Function.Opacity);
                    break;
                case EventObjectType.Flash:
                    AddContentFunction(Function.Move);
                    AddContentFunction(Function.Resize);
                    AddContentFunction(Function.Rotation);
                    AddContentFunction(Function.Background);                   
                    AddContentFunction(Function.EnableEvent); 
                    AddContentFunction(Function.Width);
                    AddContentFunction(Function.Height);
                    AddContentFunction(Function.Opacity);
                    break;
                case EventObjectType.RectArea:                    
                    AddContentFunction(Function.Move);
                    AddContentFunction(Function.Resize);
                    AddContentFunction(Function.Rotation);
                    AddContentFunction(Function.Background);                   
                    AddContentFunction(Function.EnableEvent); 
                    AddContentFunction(Function.Width);
                    AddContentFunction(Function.Height);
                    AddContentFunction(Function.Opacity);
                    break;
                case EventObjectType.Video:
                    AddContentFunction(Function.Move);
                    AddContentFunction(Function.Resize);
                    AddContentFunction(Function.Rotation);
                    AddContentFunction(Function.Background);
                    AddContentFunction(Function.EnableEvent);
                    AddContentFunction(Function.Width);
                    AddContentFunction(Function.Height);
                    AddContentFunction(Function.Opacity);
                    break;
                case EventObjectType.TextBox:                                                      
                    AddContentFunction(Function.Move);
                    AddContentFunction(Function.Resize);
                    AddContentFunction(Function.Text);     
                    AddContentFunction(Function.Rotation);
                    AddContentFunction(Function.Background);                   
                    AddContentFunction(Function.EnableEvent); 
                    AddContentFunction(Function.Width);
                    AddContentFunction(Function.Height);
                    AddContentFunction(Function.Opacity);
                    break;
            }
        }

        public ContentFunction CreateContentFunction(Function function)
        {
            ContentFunction contentFunction = new ContentFunction(function);
            return contentFunction;
        }

        public void AddContentFunction(Function function)
        {
            if (ContentFunctions.Where(o => o.Function.Equals(function) == true).FirstOrDefault() == null)
                ContentFunctions.Add(CreateContentFunction(function));
        }

        public void EventContentItemTreeView_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            EventObjectType eventtype = EventObjectType.None;
            TreeView treeview = sender as TreeView;
            if (treeview != null)
            {
                currentSelectedItem = treeview.SelectedItem;

                if (treeview.SelectedItem.GetType() == typeof(RootContent))
                {
                    RootContent rootContent = treeview.SelectedItem as RootContent;
                    if (rootContent != null)
                    {
                        switch (rootContent.RootType)
                        {
                            case RootType.Self:
                                if (this.selfTouchContent.GetType() == typeof(ScreenTouchContent))
                                {
                                    eventtype = EventObjectType.Screen;
                                }
                                else if (this.selfTouchContent.GetType() == typeof(ElementTouchContent))
                                {
                                    ElementTouchContent elementContent = this.selfTouchContent as ElementTouchContent;
                                    if (elementContent != null)
                                    {
                                        eventtype = elementContent.TouchContentType;
                                    }
                                }
                                break;
                            case RootType.Utility:
                                eventtype = EventObjectType.Utility;
                                break;
                            case RootType.Project:
                                eventtype = EventObjectType.Project;
                                break;
                        }
                    }
                }
                else if (treeview.SelectedItem.GetType() == typeof(ScreenTouchContent))
                {
                    ScreenTouchContent screenContent = treeview.SelectedItem as ScreenTouchContent;
                    eventtype = EventObjectType.Screen;
                    //this.selectedScreenID = screenContent.ScreenID;
                }
                else if (treeview.SelectedItem.GetType() == typeof(ElementTouchContent))
                {
                    ElementTouchContent elementContent = treeview.SelectedItem as ElementTouchContent;
                    if (elementContent != null)
                    {
                        eventtype = elementContent.TouchContentType;                      
                    }
                }
                MakeEventFunction(eventtype);
            }
        }

        public void TouchEventViewUserControl_Loaded(object sender, RoutedEventArgs e)
        {
            this.touchEventView = sender as TouchEventView;            
        }

        private void RaiseInsertEventFunctionRoutedEvent(EventFunctionEventArgs e)
        {
            if (this.touchEventView != null)
            {
                RoutedEventArgs newEventArgs = new RoutedEventArgs(TouchEventView.InsertEventFunctionRoutedEvent, e);
                this.touchEventView.RaiseEvent(newEventArgs);
            }
        }

        public void ListView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ListView listview = sender as ListView;
            if (listview != null)
            {
                object obj = listview.SelectedItem;
                if (obj != null)
                {
                    EventObjectType eventtype = EventObjectType.None;
                    string pagename = "";
                    string pageid = "";
                    string contentname = "";
                    GetEventFunctonInfo(ref eventtype, ref pagename, ref pageid, ref contentname);
                    EventFunctionEventArgs eventargs = new EventFunctionEventArgs(obj as ContentFunction, eventtype, pagename, pageid, contentname);
                    this.RaiseInsertEventFunctionRoutedEvent(eventargs);
                }
            }
        }

        private void GetEventFunctonInfo(ref EventObjectType eventtype, ref string pagename, ref string pageid, ref string contentname)
        {
            if (currentSelectedItem != null)
            {
                if (currentSelectedItem.GetType() == typeof(RootContent))
                {
                    RootContent rootContent = currentSelectedItem as RootContent;
                    if (rootContent != null)
                    {
                        switch (rootContent.RootType)
                        {
                            case RootType.Self://TouchEvent 편집창을 띄울때 선택된 Content에 대한 정보 설정.
                                if (this.selfTouchContent.GetType() == typeof(ScreenTouchContent))
                                {
                                    ScreenTouchContent screencontent = this.selfTouchContent as ScreenTouchContent;
                                    if (screencontent != null)
                                    {
                                        eventtype = EventObjectType.Screen;
                                        pagename = screencontent.ScreenName;
                                        pageid = screencontent.ScreenID;
                                        contentname = "";
                                    }
                                }
                                else if (this.selfTouchContent.GetType() == typeof(ElementTouchContent))
                                {
                                    ElementTouchContent elementContent = this.selfTouchContent as ElementTouchContent;
                                    if (elementContent != null)
                                    {
                                        eventtype = elementContent.TouchContentType;
                                        pagename = elementContent.ScreenName;
                                        pageid = elementContent.ScreenID;
                                        contentname = elementContent.ElementName;
                                    }
                                }
                                break;
                            case RootType.Utility:
                                eventtype = EventObjectType.Utility;
                                pagename = "";
                                pageid = "";
                                contentname = "";
                                break;
                            case RootType.Project:
                                eventtype = EventObjectType.Project;
                                pagename = "";
                                pageid = "";
                                contentname = "";
                                break;
                        }
                    }
                }
                else if (currentSelectedItem.GetType() == typeof(ScreenTouchContent))
                {
                    ScreenTouchContent screencontent = currentSelectedItem as ScreenTouchContent;
                    if (screencontent != null)
                    {
                        eventtype = EventObjectType.Screen;
                        pagename = screencontent.ScreenName;
                        pageid = screencontent.ScreenID;
                        contentname = "";
                    }
                }
                else if (currentSelectedItem.GetType() == typeof(ElementTouchContent))
                {
                    ElementTouchContent elementContent = currentSelectedItem as ElementTouchContent;
                    if (elementContent != null)
                    {
                        eventtype = elementContent.TouchContentType;
                        pagename = elementContent.ScreenName;
                        pageid = elementContent.ScreenID;
                        contentname = elementContent.ElementName;
                    }
                }
            }           
        }
       
       
        public ObservableCollection<RootContent> RootContents { get; set; }

        public ObservableCollection<ContentFunction> ContentFunctions { get; set; }

        public TouchEventType CurrentTouchEventType { get; set; }
    }
}
