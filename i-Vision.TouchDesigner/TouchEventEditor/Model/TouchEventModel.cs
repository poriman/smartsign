﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace TouchEventEditor.Model
{
    #region enum 정의

    public enum RootType
    {
        Self,
        Utility,
        Project
    }

    public enum ValueType
    {
        string_value,
        number_value,
        object_value
    }

    public enum TouchEventType
    {
        TC_EVT_OBJECT_START,
        TC_EVT_OBJECT_END,
        TC_EVT_OBJECT_KEY_DOWN,
        TC_EVT_OBJECT_KEY_UP,
        TC_EVT_OBJECT_CLICK,
        TC_EVT_OBJECT_TIMEOUT,
        TC_EVT_OBJECT_DCLICK
    }

    public enum EventObjectType
    {
        None,
        Utility,
        Project,
        Screen,
        Image,
        Video,
        Audio,
        Flash,
        Streaming,
        Button,
        TextBox,
        Label,
        ScrollText,
        RectArea,
        Web,
        RSS,
        Shape,
        Control,
        SlideViewer
    }

    public enum FunctionType
    {       
        Method,
        Property
    }

    public enum Function
    {        
        #region Method   
        SaveFile,
        ReadFile,
        SetTextColor,
        Move,
        Resize,        
        MoveScreen, 
        MoveNextScreen,
        MoveBackScreen, //플로우 구조상의 이전 스크린
        MovePreviousScreen, //히스토리상의 이전 스크린
        Play,
        Stop,
        Pause,
        Reload, // 미디어 요소에 적용할 외부 파일을 재 설정하는 함수
        Unload, // 미디어 요소에 적용된 외부 파일을 해재한는 함수. 해제시 디폴트 적용
        Sleep,
        RunProgram,
        Mute,
        Volumn,
        #endregion
        
        #region Property
        Content,
        Text,
        EnableEvent,    // bool
        Opacity,        // double
        Background,     // Brush
        Foreground,     // Brush
        Visible,        // bool
        Rotation,       // double
        PositionX,      // double
        PositionY,      // double
        Width,          // double
        Height          // double
        #endregion
    }

    #endregion

    public class RootContent
    {
        public RootType RootType { get; set; }
        public string RootName { get; set; }

        public ObservableCollection<ScreenTouchContent> ScreenContents { get; set; }    
    }

    public class TouchContent
    {
        public string ScreenName { get; set; }
        public string ScreenID { get; set; }
        public EventObjectType TouchContentType { get; set; }
    }


    public class ScreenTouchContent : TouchContent
    {
        public ObservableCollection<ElementTouchContent> ElementContents { get; set; }     
    }

    public class ElementTouchContent : TouchContent
    {
        public string ElementName { get; set; }
    }

    public class ContentFunction
    {
        public FunctionType FunctionType { get; set; }
        public Function Function { get; set; }
        public string FunctionName { get; set; }
        public string FunctionDescription { get; set; }
        public string FunctionSample { get; set; }

        public ContentFunction(Function function)
        {
            this.Function = function;
            this.FunctionName = function.ToString();

            setActionType(function);            
        }

        private void setActionType(Function function)
        {
            switch (function)
            {                   
                case Model.Function.SaveFile:
                    this.FunctionType = FunctionType.Method;
                    this.FunctionDescription = "Text 파일로 저장합니다.";
                    break;
                case Model.Function.ReadFile:
                    this.FunctionType = FunctionType.Method;
                    this.FunctionDescription = "Text 파일로 부터 Text를 읽어옵니다.";
                    break;
                case Model.Function.SetTextColor:
                    this.FunctionType = FunctionType.Method;
                    this.FunctionDescription = "Text의 색상을 설정합니다.";
                    break;
                case Model.Function.Move:
                    this.FunctionType = FunctionType.Method;
                    this.FunctionDescription = "화면상에서 요소의 위치를 이동합니다.";
                    break;
                case Model.Function.Resize:
                    this.FunctionType = FunctionType.Method;
                    this.FunctionDescription = "화면상에서 요소의 크기를 변경합니다.";
                    break;
                case Model.Function.MoveScreen:
                    this.FunctionType = FunctionType.Method;
                    this.FunctionDescription = "다른 스크린으로 이동합니다.";
                    break;
                case Model.Function.MoveNextScreen:
                    this.FunctionType = FunctionType.Method;
                    this.FunctionDescription = "다음 하위 스크린으로 이동합니다.";
                    break;
                case Model.Function.MoveBackScreen:
                    this.FunctionType = FunctionType.Method;
                    this.FunctionDescription = "상위 스크린으로 이동합니다.";
                    break;
                case Model.Function.MovePreviousScreen:
                    this.FunctionType = FunctionType.Method;
                    this.FunctionDescription = "이전 스크린으로 이동합니다.";
                    break;
                case Model.Function.Play:
                    this.FunctionType = FunctionType.Method;
                    this.FunctionDescription = "미디어를 플레이합니다.";
                    break;
                case Model.Function.Stop:
                    this.FunctionType = FunctionType.Method;
                    this.FunctionDescription = "미디어를 종료합니다.";
                    break;
                case Model.Function.Pause:
                    this.FunctionType = FunctionType.Method;
                    this.FunctionDescription = "미디어를 일시 정지합니다.";
                    break;
                case Model.Function.Reload:
                    this.FunctionType = FunctionType.Method;
                    this.FunctionDescription = "지정된 파일의 미디어를 불러옵니다.";
                    break;
                case Model.Function.Unload:
                    this.FunctionType = FunctionType.Method;
                    this.FunctionDescription = "지정된 파일의 미디어를 해제합니다.";
                    break;
                case Model.Function.Sleep:
                    this.FunctionType = FunctionType.Method;
                    this.FunctionDescription = "이 후의 실행을 특정 시간만큼 잠시 중시합니다.";
                    break;
                case Model.Function.RunProgram:
                    this.FunctionType = FunctionType.Method;
                    this.FunctionDescription = "특정 경로의 외부 프로그램을 실행합니다.";
                    break;
                case Model.Function.Mute:
                    this.FunctionType = FunctionType.Property;
                    this.FunctionDescription = "미디어의 음소거를 설정합니다.";
                    break;
                case Model.Function.Volumn:
                    this.FunctionType = FunctionType.Property;
                    this.FunctionDescription = "미디어의 소리를 설정합니다.";
                    break;
                case Model.Function.Text:
                    this.FunctionType = FunctionType.Property;
                    this.FunctionDescription = "문자열을 설정합니다.";
                    break;
                case Model.Function.EnableEvent:
                    this.FunctionType = FunctionType.Property;
                    this.FunctionDescription = "재생 중 이벤트 반응여부를 결정합니다.";
                    break;
                case Model.Function.Opacity:
                    this.FunctionType = FunctionType.Property;
                    this.FunctionDescription = "투명도를 조절합니다.";
                    break;              
                case Model.Function.Background:
                    this.FunctionType = FunctionType.Property;
                    this.FunctionDescription = "배경 색상을 나타내는 속성입니다.";
                    break;
                case Model.Function.Foreground:
                    this.FunctionType = FunctionType.Property;
                    this.FunctionDescription = "전경 색상을 나타내는 속성입니다.";
                    break;
                case Model.Function.Visible:
                    this.FunctionType = FunctionType.Property;
                    this.FunctionDescription = "요소를 화면에 보여줄지 숨길지를 나타내는 속성입니다.";
                    break;
                case Model.Function.Rotation:
                    this.FunctionType = FunctionType.Property;
                    this.FunctionDescription = "회전 정도를 나타내는 속성입니다.";
                    break;
                case Model.Function.PositionX:
                    this.FunctionType = FunctionType.Property;
                    this.FunctionDescription = "수평 위치인 X좌표를 나타내는 속성입니다.";
                    break;
                case Model.Function.PositionY:
                    this.FunctionType = FunctionType.Property;
                    this.FunctionDescription = "수직 위치인 Y좌표를 나타내는 속성입니다.";
                    break;
                case Model.Function.Width:
                    this.FunctionType = FunctionType.Method;
                    this.FunctionDescription = "수평 크기를 나타내는 속성입니다.";
                    break;
                case Model.Function.Height:
                    this.FunctionType = FunctionType.Property;
                    this.FunctionDescription = "수직 크기를 나타내는 속성입니다.";
                    break;              
            }
        }
    }

    public class EventFunctionEventArgs : System.Windows.RoutedEventArgs
    {
        private EventObjectType evnetType;

        public EventObjectType EventType
        {
            get { return this.evnetType; }
            set { this.evnetType = value; }
        }

        private string pageName;

        public string PageName
        {
            get { return this.pageName; }
            set { this.pageName = value; }
        }

        private string pageID;

        public string PageID
        {
            get { return this.pageID; }
            set { this.pageID = value; }
        }

        private string contentName;

        public string ContentName
        {
            get { return this.contentName; }
            set { this.contentName = value; }
        }

        private ContentFunction contentFunction;
        public ContentFunction ContentFunction
        {
            get { return this.contentFunction; }
            set { this.contentFunction = value; }
        }
        public EventFunctionEventArgs(ContentFunction contentFunction, EventObjectType eventtype, string pagename, string pageid, string contentname)
        {
            this.contentFunction = contentFunction;
            this.evnetType = eventtype;
            this.pageName = pagename;
            this.pageID = pageid;
            this.contentName = contentname;
        }
    }
 
}
