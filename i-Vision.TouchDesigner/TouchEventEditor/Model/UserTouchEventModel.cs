﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Xml.Linq;
using System.Windows;

namespace TouchEventEditor.Model
{
    public class UserContentFunction : ViewModelBase
    {
        private int orderNo;
        public int OrderNo
        {
            get { return this.orderNo; }
            set { this.orderNo = value; OnPropertyChanged("OrderNo"); }
        }

        private string targetFullPath;
        public string TargetFullPath
        {
            get { return this.targetFullPath; }
            set { this.targetFullPath = value; OnPropertyChanged("TargetFullPath"); }
        }

        private string targetFullName;
        public string TargetFullName
        {
            get { return this.targetFullName; }
            set { this.targetFullName = value; OnPropertyChanged("TargetFullName"); }
        }

        private string screenName;
        public string ScreenName
        {
            get { return this.screenName; }
            set { this.screenName = value; OnPropertyChanged("ScreenName"); }
        }

        private string screenID;
        public string ScreenID
        {
            get { return this.screenID; }
            set { this.screenID = value; OnPropertyChanged("ScreenID"); }
        }

        private string elementName;
        public string ElementName
        {
            get { return this.elementName; }
            set { this.elementName = value; OnPropertyChanged("ElementName"); }
        }

        private string functionName;
        public string FunctionName
        {
            get { return this.functionName; }
            set { this.functionName = value; OnPropertyChanged("FunctionName"); }
        }
      
        private string variableName;
        public string VariableName
        {
            get { return this.variableName; }
            set { this.variableName = value; OnPropertyChanged("VariableName"); }
        }
        public FunctionType FunctionType { get; set; }
        public Function Function { get; set; }
        public EventObjectType ContentType { get; set; }
        
        private object value;
        public object Value
        {
            get { return this.value; }
            set { this.value = value; OnPropertyChanged("Value"); }
        }

        private ValueType valueType;
        public ValueType ValueType
        {
            get { return this.valueType; }
            set { this.valueType = value; OnPropertyChanged("ValueType"); }
        }

        public UserContentFunction(FunctionType functionType, Function function)
        {
            this.FunctionType = functionType;
            this.Function = function;
            this.FunctionName = function.ToString();
        }

        public UserContentFunction(string strFunctionType, string strFunction)
        {           
            this.FunctionType = (FunctionType)Enum.Parse(typeof(FunctionType), strFunctionType, true);
            this.Function = (Function)Enum.Parse(typeof(Function), strFunction, true);           
                        
            this.FunctionName = strFunction;
        }
    }

    public class UserTouchEvent : ViewModelBase
    {
        public UserTouchEvent()
        {
            UserContentFunctionList = new ObservableCollection<UserContentFunction>();
        }

        private string touchEventName;
        public string TouchEventName 
        {
            get { return this.touchEventName; }
            set { this.touchEventName = value; OnPropertyChanged("TouchEventName"); }
        }
        public ObservableCollection<UserContentFunction> UserContentFunctionList { get; set; }        
    }

    public class ResultValueEventArgs : RoutedEventArgs
    {
        private bool dialogResult;
        private Dictionary<string, string> resultValue;
        private string selectedValue;
        private List<UserTouchEvent> userTouchEvents;

        public bool DialogResult
        {
            get { return dialogResult; }
            set { value = dialogResult; }
        }

        public Dictionary<string, string> ResultValue
        {
            get { return resultValue; }
            set { value = resultValue; }
        }

        public string SelectedValue
        {
            get { return this.selectedValue; }
            set { this.selectedValue = value; }
        }

        public List<UserTouchEvent> UserTouchEvents
        {
            get { return this.userTouchEvents; }
            set { this.userTouchEvents = value; }
        }

        public ResultValueEventArgs(RoutedEvent routedEvent, bool dialogResult, Dictionary<string, string> resultValue, string selectedValue, List<UserTouchEvent> userTouchEvents)
            : base(routedEvent)
        {
            this.dialogResult = dialogResult;
            this.resultValue = resultValue;
            this.selectedValue = selectedValue;
            this.userTouchEvents = userTouchEvents;
        }
    }
}
