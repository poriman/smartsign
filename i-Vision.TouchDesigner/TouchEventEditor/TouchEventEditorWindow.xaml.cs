﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TouchEventEditor.Model;

namespace TouchEventEditor
{
    /// <summary>
    /// Interaction logic for TouchEventEditorWindow.xaml
    /// </summary>
    public partial class TouchEventEditorWindow : Window
    {
        //public string UserTouchEventName { get; set; }
        public Dictionary<string, string> UserTouchEventList { get; set; }
        public List<UserTouchEvent> UserTouchEvents { get; set; }
        public string SelectedUserTouchEventName { get; set; }
        public string TouchEventFileFullPath { get; set; }
        public TouchEventType SelectedTouchEventType { get; set; }

        public TouchEventEditorWindow()
        {
            InitializeComponent();            

            this.Loaded += (s, e) =>
            {
                this.userTouchEventView.UserTouchEventName = this.SelectedUserTouchEventName;
                this.userTouchEventView.TouchEventFilePath = this.TouchEventFileFullPath;
                this.userTouchEventView.TouchEventType = this.SelectedTouchEventType;
            };
        }

        private void TouchEventView_InsertEventFunction(object sender, RoutedEventArgs e)
        {
            TouchEventEditor.ViewModel.UserTouchEventViewModel viewmodel = userTouchEventView.DataContext as TouchEventEditor.ViewModel.UserTouchEventViewModel;// TouchEventEditor.ViewModel.UserTouchEventViewModel.Instance;
            if (viewmodel != null)
            {
                viewmodel.InsertTouchEventFunction(e);
            }
        }

        private void userTouchEventView_CloseUserTouchEventView(object sender, RoutedEventArgs e)
        {
            ResultValueEventArgs eventArgs = e as ResultValueEventArgs;
            if (eventArgs.DialogResult == true)
            {
                this.SelectedUserTouchEventName = eventArgs.SelectedValue;
                this.UserTouchEventList = eventArgs.ResultValue;
                this.UserTouchEvents = eventArgs.UserTouchEvents;
            }
            this.DialogResult = eventArgs.DialogResult;

        }
    }
}
