﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TouchEventEditor.ViewModel;
using TouchEventEditor.Model;
using System.Collections.ObjectModel;

namespace TouchEventEditor.Controller
{
    public class TouchEventViewController
    {
        private TouchEventViewModel touchEventViewModel;
        public ObservableCollection<RootContent> RootContents { get; set; }
        public TouchContent SelfTouchContent { get; set; }
        public EventObjectType SelfEventObjectType { get; set; }

        #region 싱글톤

        private static readonly TouchEventViewController _instance;

        public static TouchEventViewController Instance
        {
            get { return _instance; }
        }

        static TouchEventViewController()
        {
            _instance = new TouchEventViewController();            
        }

        private TouchEventViewController()
        {
            RootContents = new ObservableCollection<RootContent>();
        }

        #endregion

        public RootContent CreateEventRootContent()
        {
            this.RootContents.Clear();

            RootContent selfRootContent = new RootContent();
            selfRootContent.RootName = "Self";
            selfRootContent.RootType = RootType.Self;

            RootContent utilityRootContent = new RootContent();
            utilityRootContent.RootName = "Utility";
            utilityRootContent.RootType = RootType.Utility;

            RootContent contentsRootContent = new RootContent();
            contentsRootContent.RootName = "Project";
            contentsRootContent.RootType = RootType.Project;
            contentsRootContent.ScreenContents = new ObservableCollection<ScreenTouchContent>();

            RootContents.Add(selfRootContent);
            RootContents.Add(utilityRootContent);
            RootContents.Add(contentsRootContent);

            return contentsRootContent;
        }

        public ScreenTouchContent CreateScreenTouchContent(RootContent rootContent, string screenName, string screenID, bool isSelf)
        {
            ScreenTouchContent screenTouchContent = new ScreenTouchContent();
            screenTouchContent.ScreenName = screenName;
            screenTouchContent.ScreenID = screenID;
            screenTouchContent.TouchContentType = EventObjectType.Screen;
            screenTouchContent.ElementContents = new ObservableCollection<ElementTouchContent>();

            rootContent.ScreenContents.Add(screenTouchContent);

            if (isSelf == true)
                SelfTouchContent = screenTouchContent;

            return screenTouchContent;
        }

        public ElementTouchContent CreateElementTouchContent(ScreenTouchContent screenContent, string elementName, EventObjectType objectType, bool isSelf)
        {
            ElementTouchContent elementTouchContent = new ElementTouchContent();
            elementTouchContent.ScreenName = screenContent.ScreenName;
            elementTouchContent.ScreenID = screenContent.ScreenID;
            elementTouchContent.ElementName = elementName;
            elementTouchContent.TouchContentType = objectType;

            screenContent.ElementContents.Add(elementTouchContent);
            if (isSelf == true)
                SelfTouchContent = elementTouchContent;
            return elementTouchContent;
        }
    }
}
