﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;
using Caliburn.Core;
using Caliburn.PresentationFramework;

namespace TouchEventEditor
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
            CaliburnFramework.ConfigureCore().WithPresentationFramework().Start();
        }
    }
}
