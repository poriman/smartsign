﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.ComponentModel;
using DenisVuyka.Controls.PropertyGrid.Attributes;
using DenisVuyka.Controls.PropertyGrid;
using DenisVuyka.Controls.PropertyGrid.Editors;
using System.Windows.Media;
using InSysTouchflowData;
using InSysBasicControls.Commons;
using InSysBasicControls.PropertyControls;

namespace InSysBasicControls.InSysProperties
{
    public enum Highlight { Diffuse, Elliptical }

    public class ElementPropertyObject : DependencyObject
    {
        public static PropertyChangedDelegate ChangedWidthPropertyCallback;
        public static PropertyChangedDelegate ChangedHeightPropertyCallback;
        public static PropertyChangedDelegate ChangedPlaylistPropertyCallback;
        public static PropertyChangedDelegate ContentElementPropertyChanged;

        public ElementPropertyObject()
        {

        }

        #region Layout Category

        #region Width

        public static readonly DependencyProperty WidthProperty = DependencyProperty.Register("Width", typeof(double), typeof(ElementPropertyObject), 
            new PropertyMetadata(200.0));

        [Category("Layout")]
        [LocalizedDisplayName("Width")]
        [PropertyOrder(1)]
        public double Width
        {
            get { return (double)GetValue(WidthProperty); }
            set
            {
                string strValue = value.ToString("####");
                if (string.IsNullOrEmpty(strValue) != true)
                {
                    double changedValue = Convert.ToDouble(strValue);
                    SetValue(WidthProperty, changedValue);
                }
            }
        }

        private static void Changed_WidthProperty(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            //if (ChangedWidthPropertyCallback != null)
            //    ChangedWidthPropertyCallback(d, e);
        }

        #endregion

        #region Height

        public static readonly DependencyProperty HeightProperty = DependencyProperty.Register("Height", typeof(double), typeof(ElementPropertyObject),
            new PropertyMetadata(200.0));

        [Category("Layout")]
        [LocalizedDisplayName("Height")]
        [PropertyOrder(2)]
        public double Height
        {
            get { return (double)GetValue(HeightProperty); }
            set
            {
                string strValue = value.ToString("####");
                if (string.IsNullOrEmpty(strValue) != true)
                {
                    double changedValue = Convert.ToDouble(strValue);
                    SetValue(HeightProperty, changedValue);
                }
            }
        }

        private static void Changed_HeightProperty(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            //if (ChangedHeightPropertyCallback != null)
            //{
            //    double value = (double)e.NewValue;

            //    string strValue = value.ToString("####");
            //    if (string.IsNullOrEmpty(strValue) != true)
            //    {
            //        double changedValue = Convert.ToDouble(strValue);
            //        ChangedHeightPropertyCallback(d, e);
            //    }
            //}
        }

        #endregion   

    
        #region ZIndex
        public static readonly DependencyProperty ZIndexProperty = DependencyProperty.Register("ZIndex", typeof(int), typeof(ElementPropertyObject), 
            new PropertyMetadata(0));

        [Category("Layout")]
        [LocalizedDisplayName("ZIndex")]
        [PropertyOrder(3)]
        public int ZIndex
        {
            get { return (int)GetValue(ZIndexProperty); }
            set { SetValue(ZIndexProperty, value); }
        }
        #endregion                        

        #region Alignment

        public static readonly DependencyProperty AlignmentProperty = DependencyProperty.Register("Alignment", typeof(Alignment), typeof(ElementPropertyObject), new PropertyMetadata());
       
        [Category("Layout")]
        [LocalizedDisplayName("Alignment")]
        [PropertyOrder(4)]
        [PropertyEditor(typeof(AlignmentPropertyControl))]
        public Alignment Alignment
        {
            get { return (Alignment)GetValue(AlignmentProperty); }
            set { SetValue(AlignmentProperty, value); }
        }
        #endregion

        #region HorizontalAlignment

        public static readonly DependencyProperty HorizontalAlignmentProperty = DependencyProperty.Register("HorizontalAlignment", typeof(HorizontalAlignment), typeof(ElementPropertyObject), new PropertyMetadata());

        [Category("Layout")]
        [LocalizedDisplayName("HorizontalAlignment")]
        [PropertyOrder(4)]       
        public HorizontalAlignment HorizontalAlignment
        {
            get { return (HorizontalAlignment)GetValue(HorizontalAlignmentProperty); }
            set { SetValue(HorizontalAlignmentProperty, value); }
        }
        #endregion

        #region VerticalAlignment

        public static readonly DependencyProperty VerticalAlignmentProperty = DependencyProperty.Register("VerticalAlignment", typeof(VerticalAlignment), typeof(ElementPropertyObject), new PropertyMetadata());

        [Category("Layout")]
        [LocalizedDisplayName("VerticalAlignment")]
        [PropertyOrder(5)]
        public VerticalAlignment VerticalAlignment
        {
            get { return (VerticalAlignment)GetValue(VerticalAlignmentProperty); }
            set { SetValue(VerticalAlignmentProperty, value); }
        }
        #endregion

        #region FitPage (페이지 고정) 설정 속성 - 필요 여부 파악 검토.

        public static readonly DependencyProperty FitPageProperty = DependencyProperty.Register("FitToPage", typeof(FitToPage), typeof(ElementPropertyObject), new PropertyMetadata(FitToPage.None));

        [Category("Layout")]
        [LocalizedDisplayName("Fit To Page")]
        [PropertyOrder(6)]
        [PropertyEditor(typeof(FitToPagePropertyControl))]
        public FitToPage FitToPage
        {
            get { return (FitToPage)GetValue(FitPageProperty); }
            set { SetValue(FitPageProperty, value); }
        }

        #endregion     

        #region AspectRatio (화면비율) 설정 속성

        public static readonly DependencyProperty AspectRatioProperty = DependencyProperty.Register("AspectRatio", typeof(bool), typeof(ElementPropertyObject), new PropertyMetadata(false));

        [Category("Layout")]
        [LocalizedDisplayName("AspectRatio")]//화면비율
        [PropertyOrder(7)]
        [PropertyEditor(typeof(AspectRatioFixCheckBox))]
        public bool AspectRatio
        {
            get { return (bool)GetValue(AspectRatioProperty); }
            set { SetValue(AspectRatioProperty, value); }
        }

        #endregion

        #region SlideViewWidth

        public static readonly DependencyProperty SlideViewWidthProperty = DependencyProperty.Register("SlideViewWidth", typeof(double), typeof(ElementPropertyObject),
            new PropertyMetadata(200.0));

        [Category("Layout")]
        [LocalizedDisplayName("SlideViewWidth")]
        [PropertyOrder(8)]
        public double SlideViewWidth
        {
            get { return (double)GetValue(SlideViewWidthProperty); }
            set
            {
                string strValue = value.ToString("####");
                if (string.IsNullOrEmpty(strValue) != true)
                {
                    double changedValue = Convert.ToDouble(strValue);
                    SetValue(SlideViewWidthProperty, changedValue);
                }
            }
        }

        #endregion

        #region SlideViewHeight

        public static readonly DependencyProperty SlideViewHeightProperty = DependencyProperty.Register("SlideViewHeight", typeof(double), typeof(ElementPropertyObject),
            new PropertyMetadata(200.0));

        [Category("Layout")]
        [LocalizedDisplayName("SlideViewHeight")]
        [PropertyOrder(9)]
        public double SlideViewHeight
        {
            get { return (double)GetValue(SlideViewHeightProperty); }
            set
            {
                string strValue = value.ToString("####");
                if (string.IsNullOrEmpty(strValue) != true)
                {
                    double changedValue = Convert.ToDouble(strValue);
                    SetValue(SlideViewHeightProperty, changedValue);
                }
            }
        }

        #endregion

        #endregion

        #region AspectRatio Category

        #region AspectRatioWidth

        public static readonly DependencyProperty AspectRatioWidthProperty = DependencyProperty.Register("AspectRatioWidth", typeof(int), typeof(ElementPropertyObject),
            new PropertyMetadata());

        [Category("AspectRatio")]
        [LocalizedDisplayName("Width")]
        [PropertyOrder(1)]
        [PropertyEditor(typeof(AspectRatioPropertyControl))]
        public int AspectRatioWidth
        {
            get { return (int)GetValue(AspectRatioWidthProperty); }
            set { SetValue(AspectRatioWidthProperty, value); }
        }

        #endregion

        #region AspectRatioHeight

        public static readonly DependencyProperty AspectRatioHeightProperty = DependencyProperty.Register("AspectRatioHeight", typeof(int), typeof(ElementPropertyObject),
            new PropertyMetadata());

        [Category("AspectRatio")]
        [LocalizedDisplayName("Height")]
        [PropertyOrder(2)]
        [PropertyEditor(typeof(AspectRatioPropertyControl))]
        public int AspectRatioHeight
        {
            get { return (int)GetValue(AspectRatioHeightProperty); }
            set { SetValue(AspectRatioHeightProperty, value); }
        }

        #endregion       
 
        #endregion  

        #region Appearance Category       

        #region BorderThickness
        public static readonly DependencyProperty BorderThicknessProperty = DependencyProperty.Register("BorderThickness", typeof(double), typeof(ElementPropertyObject), new PropertyMetadata(0.0));

        [Category("Appearance")]
        [LocalizedDisplayName("BorderThickness")]
        [PropertyOrder(1)]
        public double BorderThickness
        {
            get { return (double)GetValue(BorderThicknessProperty); }
            set { SetValue(BorderThicknessProperty, value); }
        }
        #endregion

        #region BorderCorner
        public static readonly DependencyProperty BorderCornerProperty = DependencyProperty.Register("BorderCorner", typeof(double), typeof(ElementPropertyObject), new PropertyMetadata());

        [Category("Appearance")]
        [LocalizedDisplayName("BorderCorner")]
        [PropertyOrder(2)]
        public double BorderCorner
        {
            get { return (double)GetValue(BorderCornerProperty); }
            set { SetValue(BorderCornerProperty, value); }
        }
        #endregion      
 
        #region Opacity Category
        public static readonly DependencyProperty OpacityProperty = DependencyProperty.Register("Opacity", typeof(double), typeof(ElementPropertyObject), new PropertyMetadata(0.5));

        [Category("Appearance")]
        [LocalizedDisplayName("Opacity")]
        [PropertyOrder(3)]
        public double Opacity
        {
            get { return (double)GetValue(OpacityProperty); }
            set { SetValue(OpacityProperty, value); }
        }

        #endregion

        #region Visibility Category
        public static readonly DependencyProperty VisibilityProperty = DependencyProperty.Register("Visibility", typeof(Visibility), typeof(ElementPropertyObject), new PropertyMetadata(Visibility.Visible));

        [Category("Appearance")]
        [LocalizedDisplayName("Visibility")]
        [PropertyOrder(4)]
        public Visibility Visibility
        {
            get { return (Visibility)GetValue(VisibilityProperty); }
            set { SetValue(VisibilityProperty, value); }
        }
        #endregion

        #region HighlightBrightness       

        public static readonly DependencyProperty HighlightBrightnessProperty = DependencyProperty.Register("HighlightBrightness", typeof(int), typeof(ElementPropertyObject), new PropertyMetadata());

        [Category("Appearance")]
        [LocalizedDisplayName("HighlightBrightness")]
        [PropertyOrder(5)]
        public int HighlightBrightness
        {
            get { return (int)GetValue(HighlightBrightnessProperty); }
            set { SetValue(HighlightBrightnessProperty, value); }
        }
        #endregion       

        #region ButtonStyle
        public static readonly DependencyProperty ButtonStyleProperty = DependencyProperty.Register("ButtonStyle", typeof(Style), typeof(ElementPropertyObject), new PropertyMetadata());

        [Category("Appearance")]
        [LocalizedDisplayName("ButtonStyle")]
        [PropertyOrder(6)]
        [PropertyEditor(typeof(ButtonStyleEditor))]
        public Style ButtonStyle
        {
            get { return (Style)GetValue(ButtonStyleProperty); }
            set { SetValue(ButtonStyleProperty, value); }
        }
        #endregion

        #endregion

        #region Common Category

        #region Type

        public static readonly DependencyProperty TypeProperty = DependencyProperty.Register("Type", typeof(Type), typeof(ElementPropertyObject), new PropertyMetadata());

        [Category("Common")]
        [LocalizedDisplayName("Type")]
        [PropertyOrder(1)]
        public Type Type
        {
            get { return (Type)GetValue(TypeProperty); }
            set { SetValue(TypeProperty, value); }
        }
        #endregion
       
        #region X
        public static readonly DependencyProperty XProperty = DependencyProperty.Register("X", typeof(double), typeof(ElementPropertyObject), new PropertyMetadata());

        [Category("Common")]
        [LocalizedDisplayName("X")]
        [PropertyOrder(1)]
        public double X
        {
            get { return (double)GetValue(XProperty); }
            set 
            {
                string strValue = value.ToString("####");
                if (string.IsNullOrEmpty(strValue) != true)
                {
                    double changedValue = Convert.ToDouble(strValue);
                    SetValue(XProperty, changedValue);
                }
                //SetValue(XProperty, value);
            }
        }
         #endregion

        #region Y
        public static readonly DependencyProperty YProperty = DependencyProperty.Register("Y", typeof(double), typeof(ElementPropertyObject), new PropertyMetadata());

        [Category("Common")]
        [LocalizedDisplayName("Y")]
        [PropertyOrder(2)]
        public double Y
        {
            get { return (double)GetValue(YProperty); }
            set 
            {
                string strValue = value.ToString("####");
                if (string.IsNullOrEmpty(strValue) != true)
                {
                    double changedValue = Convert.ToDouble(strValue);
                    SetValue(YProperty, changedValue);
                }
                //SetValue(YProperty, value); 
            }
        }
         #endregion

        #region Name
        public static readonly DependencyProperty NameProperty = DependencyProperty.Register("Name", typeof(string), typeof(ElementPropertyObject), new PropertyMetadata());

        [Category("Common")]
        [LocalizedDisplayName("Name")]
        [PropertyOrder(3)]
        public string Name
        {
            get { return (string)GetValue(NameProperty); }
            set { SetValue(NameProperty, value); }
        }
        #endregion

        #region ElementContent

        public static readonly DependencyProperty ContentProperty = DependencyProperty.Register("Content", typeof(object), typeof(ElementPropertyObject), new PropertyMetadata());

        [Category("Common")]
        [LocalizedDisplayName("Content")]
        [PropertyOrder(4)]
        public object Content
        {
            get { return (object)GetValue(ContentProperty); }
            set { SetValue(ContentProperty, value); }
        }
        #endregion

        #region ElementContent

        public static readonly DependencyProperty TextProperty = DependencyProperty.Register("Text", typeof(string), typeof(ElementPropertyObject), new PropertyMetadata());

        [Category("Common")]
        [LocalizedDisplayName("Text")]
        [PropertyEditor(typeof(TextEditorButton))]
        [PropertyOrder(5)]
        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }
        #endregion

        #region PageElements
        public static readonly DependencyProperty PageElementsProperty = DependencyProperty.Register("PageElements", typeof(List<FrameworkElement>), typeof(ElementPropertyObject), new PropertyMetadata());

        [Category("Common")]
        [LocalizedDisplayName("Page Elements")]
        [PropertyOrder(6)]
        public List<FrameworkElement> PageElements
        {
            get { return (List<FrameworkElement>)GetValue(PageElementsProperty); }
            set { SetValue(PageElementsProperty, value); }
        }
        #endregion
        
        #region Playlist
        public static readonly DependencyProperty PlaylistProperty = DependencyProperty.Register("Playlist", typeof(List<PlaylistItem>), typeof(ElementPropertyObject), new PropertyMetadata(Changed_PlaylistProperty));

        private static void Changed_PlaylistProperty(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (ChangedPlaylistPropertyCallback != null)
                ChangedPlaylistPropertyCallback(sender, e);
        }

        [Category("Common")]
        [LocalizedDisplayName("Playlist")]
        [PropertyOrder(1)]
        public List<PlaylistItem> Playlist
        {
            get { return (List<PlaylistItem>)GetValue(PlaylistProperty); }
            set { SetValue(PlaylistProperty, value); }
        }
        #endregion

        #endregion

        #region Brushes Category

        #region Background

        public static readonly DependencyProperty BackgroundProperty = DependencyProperty.Register("Background", typeof(Brush), typeof(ElementPropertyObject), new PropertyMetadata());

        [Category("Brushes")]
        [LocalizedDisplayName("Background")]
        [PropertyOrder(1)]
        [PropertyEditor(typeof(BrushEditorCallButton))]
        public Brush Background
        {
            get { return (Brush)GetValue(BackgroundProperty); }
            set { SetValue(BackgroundProperty, value); }
        }
        #endregion        

        #region BorderBrush
        public static readonly DependencyProperty BorderBrushProperty = DependencyProperty.Register("BorderBrush", typeof(Brush), typeof(ElementPropertyObject), new PropertyMetadata());

        [Category("Brushes")]
        [LocalizedDisplayName("BorderBrush")]
        [PropertyOrder(2)]
        [PropertyEditor(typeof(BrushEditorCallButton))]
        public Brush BorderBrush
        {
            get { return (Brush)GetValue(BorderBrushProperty); }
            set { SetValue(BorderBrushProperty, value); }
        }
        #endregion

        #region Foreground
        public static readonly DependencyProperty ForegroundProperty = DependencyProperty.Register("Foreground", typeof(Brush), typeof(ElementPropertyObject), new PropertyMetadata());

        [Category("Brushes")]
        [LocalizedDisplayName("Foreground")]
        [PropertyOrder(3)]
        [PropertyEditor(typeof(BrushEditorCallButton))]
        public Brush Foreground
        {
            get { return (Brush)GetValue(ForegroundProperty); }
            set { SetValue(ForegroundProperty, value); }
        }
        #endregion

        #endregion

        #region Text Category

        #region FontSize
        public static readonly DependencyProperty FontSizeProperty = DependencyProperty.Register("FontSize", typeof(double), typeof(ElementPropertyObject), new PropertyMetadata(12.0));

        [Category("Text")]
        [LocalizedDisplayName("FontSize")]
        [PropertyOrder(1)]
        public double FontSize
        {
            get { return (double)GetValue(FontSizeProperty); }
            set { SetValue(FontSizeProperty, value); }
        }

        #endregion

        #region FontWeight

        public static readonly DependencyProperty FontWeightProperty = DependencyProperty.Register("FontWeight", typeof(FontWeight), typeof(ElementPropertyObject), new PropertyMetadata());

        [Category("Text")]
        [LocalizedDisplayName("FontWeight")]
        [PropertyOrder(2)]
        public FontWeight FontWeight
        {
            get { return (FontWeight)GetValue(FontWeightProperty); }
            set { SetValue(FontWeightProperty, value); }
        }

        #endregion

        #region FontFamily

        public static readonly DependencyProperty FontFamilyProperty = DependencyProperty.Register("FontFamily", typeof(FontFamily), typeof(ElementPropertyObject), new PropertyMetadata());

        [Category("Text")]
        [LocalizedDisplayName("FontFamily")]
        [PropertyOrder(3)]
        public FontFamily FontFamily
        {
            get { return (FontFamily)GetValue(FontFamilyProperty); }
            set { SetValue(FontFamilyProperty, value); }
        }
        #endregion

        #region TextWrapping

        public static readonly DependencyProperty TextWrappingProperty = DependencyProperty.Register("TextWrapping", typeof(TextWrapping), typeof(ElementPropertyObject), new PropertyMetadata());

        [Category("Text")]
        [LocalizedDisplayName("TextWrapping")]
        [PropertyOrder(4)]
        public TextWrapping TextWrapping
        {
            get { return (TextWrapping)GetValue(TextWrappingProperty); }
            set { SetValue(TextWrappingProperty, value); }
        }
        #endregion

        #region TextMultiline

        public static readonly DependencyProperty MultilineProperty = DependencyProperty.Register("Multiline", typeof(bool), typeof(ElementPropertyObject), new PropertyMetadata());

        [Category("Text")]
        [LocalizedDisplayName("Multiline")]
        [PropertyOrder(4)]
        public bool Multiline
        {
            get { return (bool)GetValue(MultilineProperty); }
            set { SetValue(MultilineProperty, value); }
        }
        #endregion

        #endregion

        #region Time Category

        #region ApplyLifeTime

        public static readonly DependencyProperty IsApplyLifeTimeProperty = DependencyProperty.Register("IsApplyLifeTime", typeof(bool), typeof(ElementPropertyObject), new PropertyMetadata(false, new PropertyChangedCallback(Changed_ApplyLifeTimeProperty)));

        private static void Changed_ApplyLifeTimeProperty(object sender, DependencyPropertyChangedEventArgs e)
        {
        }

        [Category("Time")]
        [LocalizedDisplayName("Apply LifeTime")]
        [PropertyOrder(1)]
        public bool IsApplyLifeTime
        {
            get { return (bool)GetValue(IsApplyLifeTimeProperty); }
            set { SetValue(IsApplyLifeTimeProperty, value); }
        }

        #endregion

        #region LifeTime
        public static readonly DependencyProperty LifeTimeProperty = DependencyProperty.Register("LifeTime", typeof(double), typeof(ElementPropertyObject), new PropertyMetadata());

        [Category("Time")]
        [LocalizedDisplayName("Life Time")]
        [PropertyOrder(1)]
        public double LifeTime
        {
            get { return (double)GetValue(LifeTimeProperty); }
            set { SetValue(LifeTimeProperty, value); }
        }
        #endregion

        #region StartTime

        public static readonly DependencyProperty StartTimeProperty = DependencyProperty.Register("StartTime", typeof(TimeSpan), typeof(ElementPropertyObject), new PropertyMetadata(new PropertyChangedCallback(Changed_StartTimeProperty)));
        private static void Changed_StartTimeProperty(object sender, DependencyPropertyChangedEventArgs e)
        {
        }

        [Category("Time")]
        [LocalizedDisplayName("Start Time")]
        [PropertyOrder(2)]
        public TimeSpan StartTime
        {
            get { return (TimeSpan)GetValue(StartTimeProperty); }
            set { SetValue(StartTimeProperty, value); }
        }

        #endregion

        #region EndTime

        public static readonly DependencyProperty EndTimeProperty = DependencyProperty.Register("EndTime", typeof(TimeSpan), typeof(ElementPropertyObject), new PropertyMetadata(new PropertyChangedCallback(Changed_EndTimeProperty)));
        private static void Changed_EndTimeProperty(object sender, DependencyPropertyChangedEventArgs e)
        {
        }

        [Category("Time")]
        [LocalizedDisplayName("End Time")]
        [PropertyOrder(3)]
        public TimeSpan EndTime
        {
            get { return (TimeSpan)GetValue(EndTimeProperty); }
            set { SetValue(EndTimeProperty, value); }
        }

        #endregion

        #endregion

        #region Media

        #region Stretch
        public static readonly DependencyProperty ImageStretchProperty = DependencyProperty.Register("Stretch", typeof(Stretch), typeof(ElementPropertyObject), new PropertyMetadata());

        [Category("Media")]
        [LocalizedDisplayName("Stretch")]
        [PropertyOrder(1)]
        public Stretch Stretch
        {
            get { return (Stretch)GetValue(ImageStretchProperty); }
            set { SetValue(ImageStretchProperty, value); }
        }
        #endregion

        #region IsMuted
        public static readonly DependencyProperty IsMutedProperty = DependencyProperty.Register("IsMuted", typeof(bool), typeof(ElementPropertyObject), new PropertyMetadata());

        [Category("Media")]
        [LocalizedDisplayName("Mute")]
        [PropertyOrder(1)]
        public bool IsMuted
        {
            get { return (bool)GetValue(IsMutedProperty); }
            set { SetValue(IsMutedProperty, value); }
        }
        #endregion

        #region Volume
        public static readonly DependencyProperty VolumeProperty = DependencyProperty.Register("Volume", typeof(double), typeof(ElementPropertyObject), new PropertyMetadata(0.5));

        [Category("Media")]
        [LocalizedDisplayName("Volume")]
        [PropertyOrder(2)]
        public double Volume
        {
            get { return (double)GetValue(VolumeProperty); }
            set { SetValue(VolumeProperty, value); }
        }
        #endregion

        #endregion

        #region Event Category

        #region ActionEvent
        public static readonly DependencyProperty ActionsProperty = DependencyProperty.Register("ActionEvent", typeof(Array), typeof(ElementPropertyObject), new PropertyMetadata());

        [Category("Event")]
        [LocalizedDisplayName("Action Event")]
        [PropertyOrder(1)]
        public Array ActionEvent
        {
            get { return (Array)GetValue(ActionsProperty); }
            set { SetValue(ActionsProperty, value); }
        }
        #endregion

        #region TargetPages
        public static readonly DependencyProperty TargetPagesProperty = DependencyProperty.Register("TargetPages", typeof(Array), typeof(ElementPropertyObject), new PropertyMetadata());

        [Category("Event")]
        [LocalizedDisplayName("Page")]
        [PropertyOrder(2)]
        public Array TargetPages
        {
            get { return (Array)GetValue(TargetPagesProperty); }
            set { SetValue(TargetPagesProperty, value); }
        }
        #endregion

        #region ShowHideType
        public static readonly DependencyProperty ShowHideTypeProperty = DependencyProperty.Register("ShowHideType", typeof(ShowHideType), typeof(ElementPropertyObject), new PropertyMetadata());

        [Category("Event")]
        [LocalizedDisplayName("ShowHideType")]
        [PropertyOrder(3)]
        public ShowHideType ShowHideType
        {
            get { return (ShowHideType)GetValue(ShowHideTypeProperty); }
            set { SetValue(ShowHideTypeProperty, value); }
        }
        #endregion

        #region TargetObjects
        public static readonly DependencyProperty TargetObjectProperty = DependencyProperty.Register("TargetObjects", typeof(Array), typeof(ElementPropertyObject), new PropertyMetadata());

        [Category("Event")]
        [LocalizedDisplayName("Target")]
        [PropertyOrder(3)]
        public Array TargetObjects
        {
            get { return (Array)GetValue(TargetObjectProperty); }
            set { SetValue(TargetObjectProperty, value); }
        }
        #endregion

        #region URL
        public static readonly DependencyProperty URLProperty = DependencyProperty.Register("URL", typeof(string), typeof(ElementPropertyObject), new PropertyMetadata());

        [Category("Event")]
        [LocalizedDisplayName("URL")]
        [PropertyOrder(4)]
        public string URL
        {
            get { return (string)GetValue(URLProperty); }
            set { SetValue(URLProperty, value); }
        }
        #endregion

        #region File
        public static readonly DependencyProperty FileProperty = DependencyProperty.Register("File", typeof(string), typeof(ElementPropertyObject), new PropertyMetadata());

        [Category("Event")]
        [LocalizedDisplayName("File")]
        [PropertyOrder(5)]
        public string File
        {
            get { return (string)GetValue(FileProperty); }
            set { SetValue(FileProperty, value); }
        }
        #endregion

        #region PageEffect
        public static readonly DependencyProperty PageEffectProperty = DependencyProperty.Register("PageEffect", typeof(PageEffect), typeof(ElementPropertyObject), new PropertyMetadata(PageEffect.None));

        [Category("Event")]
        [LocalizedDisplayName("Page Effect")]
        [PropertyOrder(5)]
        public PageEffect PageEffect
        {
            get { return (PageEffect)GetValue(PageEffectProperty); }
            set { SetValue(PageEffectProperty, value); }
        }
        #endregion

        #region TouchEvent

        public static readonly DependencyProperty TouchEventProperty = DependencyProperty.Register("TouchEvent", typeof(Dictionary<string, string>), typeof(ElementPropertyObject));

        [Category("Event")]
        [LocalizedDisplayName("Touch Event")]
        [PropertyOrder(7)]
        [PropertyEditor(typeof(TouchEventPropertyControl))]
        public Dictionary<string, string> TouchEvent
        {
            get { return (Dictionary<string, string>)GetValue(TouchEventProperty); }
            set { SetValue(TouchEventProperty, value); }
        }

        #endregion

        #endregion

        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);
            if (ContentElementPropertyChanged != null)
                ContentElementPropertyChanged(this, e);
        }
    }
}
