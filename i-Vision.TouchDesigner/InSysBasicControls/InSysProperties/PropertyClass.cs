﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Xml.Linq;
using System.IO;
using System.Windows.Markup;
using System.Xml;

namespace InSysBasicControls.InSysProperties
{
    public class PropertyClass
    {
        /// <summary>
        /// Sets the specified property by the specified object, interface and property value
        /// </summary>
        public static void SetProperty(object obj, string interfaceName, string propertyName, object value)
        {
            PropertyInfo[] props = obj.GetType().GetInterface(interfaceName).GetProperties();

            foreach (PropertyInfo p in props)
            {
                if (p.Name == propertyName && p.CanWrite)
                {
                    try
                    {
                        p.SetValue(obj, value, null);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                        return;
                    }
                }
            }
        }

        public static void SetSerializationProperty(object obj, string interfaceName, string propertyName, object value)
        {
            PropertyInfo[] props = obj.GetType().GetInterface(interfaceName).GetProperties();

            foreach (PropertyInfo p in props)
            {
                if (p.Name == propertyName && p.CanWrite)
                {
                    try
                    {
                        XElement element = XElement.Parse(value.ToString());
                        StringReader stringReader = new StringReader(element.ToString());
                        XmlReader xmlReader = XmlReader.Create(stringReader);
                        object objValue = XamlReader.Load(xmlReader);
                        p.SetValue(obj, objValue, null);
                        break;
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                        return;
                    }
                }
            }
        }
    }
}
