﻿
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace InSysBasicControls.CommonControls
{
    public partial class FlashControl : UserControl
    {
		private string sRefPath = null;
		
		AxShockwaveFlashObjects.AxShockwaveFlash viewer = null;

		public String RefDataPath
		{
			get { return sRefPath; }
			set { sRefPath = value; }
		}

        public FlashControl()
        {
            InitializeComponent();
			this.Disposed += new EventHandler(UserControl_Disposed);
        }

		public System.Drawing.Bitmap Thumbnail(int cx, int cy)
		{
			try
			{
				Bitmap bmp = new Bitmap(cx, cy, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
				using (Graphics g = Graphics.FromImage(bmp))
				{
					Point pos = this.PointToScreen(new Point(0, 0));
					g.CopyFromScreen(pos, new Point(0, 0), this.Size, CopyPixelOperation.SourceCopy);
				}

				return bmp;
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}

			return null;

		}
        /// <summary>
        /// Plays the loaded file
        /// </summary>
        public void Play(string source)
        {
			AxShockwaveFlashObjects.AxShockwaveFlash flash = InitFlash();

// 			viewer.LoadMovie(0, source);
// 			viewer.LoadMovie(0, source);
			flash.Movie = source;
			flash.SetVariable("xml_path", RefDataPath);
// 			this.Visible = true;

// 			ResizeFlash();

			flash.Play();

			if (viewer != null)
			{
				viewer.Stop();
				viewer.Dispose();
				viewer = null;
			}

			viewer = flash;
        }

        public void SetFlash()
        {
            this.viewer = InitFlash();
        }

		private AxShockwaveFlashObjects.AxShockwaveFlash InitFlash()
		{
			AxShockwaveFlashObjects.AxShockwaveFlash flash = new AxShockwaveFlashObjects.AxShockwaveFlash();

			((System.ComponentModel.ISupportInitialize)(flash)).BeginInit();
			this.SuspendLayout();

			flash.Enabled = false;
			flash.Name = "viewer";
			flash.TabIndex = 0;

			this.Controls.Add(flash);


			((System.ComponentModel.ISupportInitialize)(flash)).EndInit();
			this.ResumeLayout(false);
			flash.Size = new Size(this.Size.Width, this.Size.Height);

			flash.ScaleMode = 2;
			flash.Loop = true;

			return flash;
		}

        /// <summary>
        /// Stops playing
        /// </summary>
        public void Stop()
        {
			if (viewer != null)
			{
// 				this.Visible = false;
// 				viewer.Stop();
				viewer.StopPlay();
// 				Clear();
			}

		}

		public void Clear()
		{

			this.Controls.Clear();

			if(viewer != null)
			{
				viewer.Stop();
				viewer.Dispose();
				viewer = null;
			}

// 			GC.Collect();
		}

		public void UserControl_Disposed(Object sender, EventArgs args)
		{
			try
			{
				Clear();               				
			}
			catch
			{
				
			}
		}
		private void ResizeFlash()
		{
			if (viewer != null)
				viewer.Size = new Size(this.Size.Width, this.Size.Height);
		}

		private void FlashControl_SizeChanged(object sender, EventArgs e)
		{
			ResizeFlash();
		}
    }
}
