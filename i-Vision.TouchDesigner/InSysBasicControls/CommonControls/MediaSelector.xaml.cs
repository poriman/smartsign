﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Markup;
using QuartzTypeLib;
using System.IO;
using InSysTouchflowData;

namespace InSysBasicControls.CommonControls
{
    /// <summary>
    /// Interaction logic for MediaSelector.xaml
    /// </summary>
    public partial class MediaSelector : Window
    {
        private bool IsApply = false;
        System.Windows.Forms.OpenFileDialog OpenFileDialog;
        TimeSpan duration;
        string loadedPath;

        /// <summary>
        /// Sets the filter of dialog that is used for choosing the media locally
        /// </summary>
        public string DialogFilter
        {
            set
            {
                OpenFileDialog.Filter = value;
            }
        }

        public bool ISAPPLY
        {
            set { IsApply = value; }
            get { return IsApply; }
        }

        public MediaSelector()
        {
            InitializeComponent();
            OpenFileDialog = new System.Windows.Forms.OpenFileDialog();
            OpenFileDialog.Filter = "Video (WMV, AVI, MPG/MPEG)| *.wmv; *.avi; *.mpg;*.mpeg";//|Images (BMP, JPG, TIFF, GIF)|*.bmp; *.jpg; *.jpeg; *.tiff; *.gif|Audio (WMA)|*.wma";
            OpenFileDialog.Title = "Open video";
            OpenFileDialog.FileOk += new System.ComponentModel.CancelEventHandler(OpenFileDialog_FileOk);
        }

        void OpenFileDialog_FileOk(object sender, System.ComponentModel.CancelEventArgs e)
        {
            FileInfo fi = new FileInfo(OpenFileDialog.FileName);
            string s = fi.Name;

            if (System.IO.Path.GetExtension(s).ToLower() == ".wmv")
            {
                try
                {
                    MediaElement me = new MediaElement();
                    me.Source = new Uri(OpenFileDialog.FileName);
                    duration = me.NaturalDuration.TimeSpan;                   
                }
                catch
                {
                }
            }
            else if (System.IO.Path.GetExtension(s).ToLower() == ".mp3")
            {
                try
                {
                    MP3Header header = new MP3Header();
                    header.ReadMP3Information(s);
                    duration = new TimeSpan(0, 0, header.intLength);
                }
                catch { }
            }
            else
            {
                try
                {
                    FilgraphManager filterGraph = new FilgraphManager();
                    filterGraph.RenderFile(OpenFileDialog.FileName);
                    IMediaPosition pos = filterGraph as IMediaPosition;
                    duration = new TimeSpan(0, 0, (int)pos.Duration);
                }
                catch { }
                try
                {
                    //DigitalSignage.Controls.QuickTimeControl qc = new DigitalSignage.Controls.QuickTimeControl();
                    //duration = qc.GetQuickTimeMovieDuration(OpenFileDialog.FileName);
                    //qc.Dispose();
                    //qc = null;
                }
                catch { }
            }
            pathBox.Text = s;
        }

        public void Load(string mediaPath)
        {
            ISAPPLY = false;
            pathBox.Text = mediaPath;
            loadedPath = mediaPath;

			if (String.IsNullOrEmpty(pathBox.Text))
			{
				pathBox.Text = "mms://";
			}
			pathBox.Focus();
			pathBox.SelectAll();

            ShowDialog();
        }

        public string GetContent()
        {
			if (pathBox.Text.Equals("mms://"))
			{
				return loadedPath = "";
			}

			return loadedPath = pathBox.Text;
        }

        public string GetDuration()
        {
            if (duration != null)
                return XamlWriter.Save(duration);
            else
                return XamlWriter.Save(new TimeSpan());
        }

        private void okButton_Click(object sender, RoutedEventArgs e)
        {
            ISAPPLY = true;
            loadedPath = pathBox.Text;
            Close();
        }

        private void applyButton_Click(object sender, RoutedEventArgs e)
        {
            loadedPath = pathBox.Text;
            ISAPPLY = true;
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void openDialogButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog.ShowDialog();
        }
    }
}