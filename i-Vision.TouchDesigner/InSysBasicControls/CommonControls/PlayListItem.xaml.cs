﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Media.Effects;

using QuartzTypeLib;

namespace InSysBasicControls.CommonControls
{
    /// <summary>
    /// Interaction logic for cm_ContentItem.xaml
    /// </summary>
    public partial class PlayListItem : Grid
    {
        private object dataContext = null;

        /// <summary>
        /// Creates a new playlist item
        /// </summary>
        /// <param name="contents">Item title name</param>
        /// <param name="duration">A duration of item</param>
		/// <param name="comment"></param>
		/// <param name="icon"></param>
		public PlayListItem(string contents, TimeSpan duration, string comment, BitmapSource icon)
        {
            InitializeComponent();
            itemName.Content    = contents;
            commentUI.Content   = comment;
            SetDuration(duration.Hours, duration.Minutes, duration.Seconds);
            SetIcon(icon);
        }

#region Events
        public delegate void MouseDownEvent(object sender, MouseButtonEventArgs e);
        public event MouseDownEvent MouseDownPI = null;

        public delegate void DropEvent(object sender, DragEventArgs e);
        public event DropEvent DropPI = null;
#endregion

#region properties
        public object DataContext
        {
            get { return dataContext; }
            set { dataContext = value; }
        }

        public object Comment
        {
            get { return commentUI.Content; }
            set { commentUI.Content = value; }
        }
#endregion

        public void SetDuration( int hours, int minutes, int seconds )
        {
            if (( hours == 0 ) && ( minutes == 0 ) && ( seconds == 0))
            {
                durationUI.Content = "";
                return;
            }
            string durationStr = "";
            if (hours < 10)
                durationStr += "0" + hours;
            else
                durationStr += hours;
            durationStr += ":";
            if (minutes < 10)
                durationStr += "0" + minutes;
            else
                durationStr += minutes;
            durationStr += ":";
            if (seconds < 10)
                durationStr += "0" + seconds;
            else
                durationStr += seconds;
            durationUI.Content = durationStr;
        }

        public void SetItem(string name)
        {
            itemName.Content = name;
        }

        public void SetIcon(BitmapSource icon)
        {
            iconUI.Source = icon;
            iconUI.Width = 64;
            iconUI.Height = 48; 
        }

        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                if (MouseDownPI != null)
                    MouseDownPI(this, e );
            }
            catch (Exception err)
            {
                throw err;
            }
        }

        private void Grid_Drop(object sender, DragEventArgs e)
        {
            try
            {
                if ( DropPI != null)
                    DropPI(this, e);
                e.Handled = true;
            }
            catch (Exception err)
            {
                throw err;
            }
        }
   }
}