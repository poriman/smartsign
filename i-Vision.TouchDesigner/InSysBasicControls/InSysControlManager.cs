﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using InSysBasicControls.Views;
using System.Windows;
using InSysBasicControls.Interfaces;
using InSysBasicControls.Events;
using InSysBasicControls.Commons;
using System.Windows.Markup;
using System.Xml.Linq;
using System.IO;
using System.Xml;
using DenisVuyka.Controls.PropertyGrid;
using System.Windows.Controls;
using DenisVuyka.Controls.PropertyGrid.Data;
using System.Windows.Media;
using InSysTouchflowData.Models.ProjectModels;
using DenisVuyka.Controls.PropertyGrid.Editors;
using InSysBasicControls.InSysProperties;
using InSysTouchflowData.Models.ActionEvents;
using InSysTouchflowData;
using BrushPicker;
using ButtonStyleLoader;
using InSysBasicControls.Editors;
using System.Windows.Documents;
using TouchEventEditor;
using TouchEventEditor.Model;
using NLog;

namespace InSysBasicControls
{    
    public delegate TouchProjectInfo GetTouchProjectInfoDelegate();
    public class InSysControlManager
    {
		private static Logger logger = NLog.LogManager.GetCurrentClassLogger();// LogManager.GetLogger("TouchDesignerLog");
        private static PropertyGrid _PropertyGrid;        
        public static UIElement _CurrentElement;        
        public static GetTouchProjectInfoDelegate GetTouchProjectInfoHandler;
        public static List<UserTouchEvent> g_UserTouchEvents { get; set; }        

        static InSysControlManager()
        {
            g_UserTouchEvents = new List<UserTouchEvent>();
        }

        /// <summary>
        /// InSysControl 타입 얻기
        /// </summary>
        /// <param name="controlType">Control Type String</param>
        /// <returns></returns>
        public static InSysControlType GetControlType(string controlType)
        {
            switch (controlType)
            {
                case "InSysSlideViewer":
                    return InSysControlType.InSysSlideViewer;
                case "InSysImageBox":
                    return InSysControlType.InSysImageBox;
                case "InSysFlashBox":
                    return InSysControlType.InSysFlashBox;
                case "InSysBasicButton":
                    return InSysControlType.InSysBasicButton;
                case "InSysRectangle":
                   return InSysControlType.InSysRectangle;
                case "InSysRectArea":
                   return InSysControlType.InSysRectArea;
                case "InSysBasicTextBox":
                    return InSysControlType.InSysBasicTextBox;
                case "InSysLabel":
                    return InSysControlType.InSysLabel;
                case "InSysVideoBox":
                    return InSysControlType.InSysVideoBox;
                case "InSysStreamingBox":
                    return InSysControlType.InSysStreamingBox;
                case "InSysAudioBox":
                    return InSysControlType.InSysAudioBox;
                case "InSysAnalogueClock":
                    return InSysControlType.InSysAnalogueClock;
                case "InSysDate":
                    return InSysControlType.InSysDate;
                case "InSysDigitalClock":
                    return InSysControlType.InSysDigitalClock;
                case "InSysEllipse":
                    return InSysControlType.InSysEllipse;
                case "InSysRSS":
                    return InSysControlType.InSysRSS;
                case "InSysScrollText":
                    return InSysControlType.InSysScrollText;
                case "InSysWebControl":
                    return InSysControlType.InSysWebControl;                   
                default:
                    return InSysControlType.None;
            }
        }

        public static InSysControlType GetControlTypeFromToolBoxType(string toolBoxTypeName)
        {
            switch (toolBoxTypeName)
            {
                case "SliderViewer":
                    return InSysControlType.InSysSlideViewer;
                case "Image":
                    return InSysControlType.InSysImageBox;
                case "Flash":
                    return InSysControlType.InSysFlashBox;
                case "Button":
                    return InSysControlType.InSysBasicButton;
                case "Rectangle":
                    return InSysControlType.InSysRectangle;
                case "RectArea":
                    return InSysControlType.InSysRectArea;
                case "Text":
                    return InSysControlType.InSysBasicTextBox;
                case "Label":
                    return InSysControlType.InSysLabel;
                case "Video":
                    return InSysControlType.InSysVideoBox;
                case "Streaming":
                    return InSysControlType.InSysStreamingBox;
                case "Audio":
                    return InSysControlType.InSysAudioBox;
                case "AnalogueClock":
                    return InSysControlType.InSysAnalogueClock;
                case "Date":
                    return InSysControlType.InSysDate;
                case "DigitalClock":
                    return InSysControlType.InSysDigitalClock;
                case "Ellipse":
                    return InSysControlType.InSysEllipse;
                case "RSS":
                    return InSysControlType.InSysRSS;
                case "ScrollText":
                    return InSysControlType.InSysScrollText;
                case "Web":
                    return InSysControlType.InSysWebControl;               
                default:
                    return InSysControlType.None;
            }
        }

        public static UIElement CreateInSysControlObjectFromToolBoxType(string controlTypeName, ViewState viewMode)
        {
            InSysControlType controlType;
            switch (controlTypeName)
            {
                case "SlideViewer":
                    controlType = InSysControlType.InSysSlideViewer;
                    break;
                case "Image":
                    controlType = InSysControlType.InSysImageBox;
                    break;
                case "Flash":
                    controlType = InSysControlType.InSysFlashBox;
                    break;
                case "Button":
                    controlType = InSysControlType.InSysBasicButton;
                    break;
                case "Rectangle":
                    controlType = InSysControlType.InSysRectangle;
                    break;
                case "RectArea":
                    controlType = InSysControlType.InSysRectArea;
                    break;
                case "Text":
                    controlType = InSysControlType.InSysBasicTextBox;
                    break;
                case "Label":
                    controlType = InSysControlType.InSysLabel;
                    break;
                case "Video":
                    controlType = InSysControlType.InSysVideoBox;
                    break;
                case "Streaming":
                    controlType = InSysControlType.InSysStreamingBox;
                    break;
                case "Audio":
                    controlType = InSysControlType.InSysAudioBox;
                    break;
                case "AnalogueClock":
                    controlType = InSysControlType.InSysAnalogueClock;
                    break;
                case "Date":
                    controlType = InSysControlType.InSysDate;
                    break;
                case "DigitalClock":
                    controlType = InSysControlType.InSysDigitalClock;
                    break;
                case "Ellipse":
                    controlType = InSysControlType.InSysEllipse;
                    break;
                case "RSS":
                    controlType = InSysControlType.InSysRSS;
                    break;
                case "ScrollText":
                    controlType = InSysControlType.InSysScrollText;
                    break;
                case "Web":
                    controlType = InSysControlType.InSysWebControl;
                    break;
                
                default:
                    controlType = InSysControlType.None;
                    break;
            }

            return CreateInSysControlObject(controlType, viewMode);
        }

        /// <summary>
        /// InSysControl Object 얻기
        /// </summary>
        /// <param name="controlType">Control Type String</param>
        /// <returns></returns>
        public static UIElement CreateInSysControlObject(InSysControlType controlType, ViewState viewState)
        {
            UIElement element = null;
            switch (controlType)
            {
                case InSysControlType.InSysSlideViewer:
                    element = new InSysSlideViewer();
                    Init_SetInSysControlEvent(element, controlType, viewState);     
                    break;
                case InSysControlType.InSysBasicButton:
                    element = new InSysBasicButton();
                    Init_SetInSysControlEvent(element, controlType, viewState);
                    IDesignElement designElement = element as IDesignElement;
                    if (designElement != null)
                        designElement.ButtonStyle = ButtonStyleLoader.ButtonStyleLoaderWindow.GetDefaultButtonStyle();
                    break;
                case InSysControlType.InSysImageBox:
                    element = new InSysImageBox();
                    Init_SetInSysControlEvent(element, controlType, viewState);                    
                    break;
                case InSysControlType.InSysRectangle:
                    element = new InSysRectangle();
                    Init_SetInSysControlEvent(element, controlType, viewState);
                    break;
                case InSysControlType.InSysRectArea:
                    element = new InSysRectArea();
                    Init_SetInSysControlEvent(element, controlType, viewState);
                    break;
                case InSysControlType.InSysBasicTextBox:
                    element = new InSysBasicTextBox();
                    Init_SetInSysControlEvent(element, controlType, viewState);
                    break;
                case InSysControlType.InSysLabel:
                    element = new InSysLabel();
                    Init_SetInSysControlEvent(element, controlType, viewState);
                    break;
                case InSysControlType.InSysVideoBox:
                    element = new InSysVideoBox();
                    Init_SetInSysControlEvent(element, controlType, viewState);
                    break;
                case InSysControlType.InSysStreamingBox:
                    element = new InSysStreamingBox();
                    Init_SetInSysControlEvent(element, controlType, viewState);
                    break;
                case InSysControlType.InSysAudioBox:
                    element = new InSysAudioBox();
                    Init_SetInSysControlEvent(element, controlType, viewState);
                    break;
                case InSysControlType.InSysAnalogueClock:
                    element = new InSysAnalogueClock();
                    Init_SetInSysControlEvent(element, controlType, viewState);
                    break;
                case InSysControlType.InSysDate:
                    element = new InSysDate();
                    Init_SetInSysControlEvent(element, controlType, viewState);
                    break;
                case InSysControlType.InSysDigitalClock:
                    element = new InSysDigitalClock();
                    Init_SetInSysControlEvent(element, controlType, viewState);
                    break;
                case InSysControlType.InSysEllipse:
                    element = new InSysEllipse();
                    Init_SetInSysControlEvent(element, controlType, viewState);
                    break;
                case InSysControlType.InSysRSS:
                    element = new InSysRSS();
                    Init_SetInSysControlEvent(element, controlType, viewState);
                    break;
                case InSysControlType.InSysScrollText:
                    element = new InSysScrollText();
                    Init_SetInSysControlEvent(element, controlType, viewState);
                    break;
                case InSysControlType.InSysWebControl:
                    element = new InSysWebControl();
                    Init_SetInSysControlEvent(element, controlType, viewState);
                    break;
                case InSysControlType.InSysFlashBox:
                    element = new InSysFlashBox();
                    Init_SetInSysControlEvent(element, controlType, viewState);
                    break;
                default:
                    return null;
            }
            
            return element;
        }

        public static void Init_SetInSysControlEvent(UIElement element, InSysControlType controlType, ViewState viewState)
        {
            if (viewState == ViewState.Design)
            {
                IDesignElement designElement = element as IDesignElement;
                if (designElement != null)
                {
                    switch (controlType)
                    {
                        case InSysControlType.InSysSlideViewer:
                            designElement.SetDelegate(new UserDelegates.ShowInSysControlPlayerEditorDelegate(ShowInSysControlPlayerEditor));
                            break;
                        case InSysControlType.InSysBasicButton:
                            designElement.SetDelegate(new UserDelegates.ShowInSysControlPlayerEditorDelegate(ShowInSysControlPlayerEditor));
                            break;
                        case InSysControlType.InSysImageBox:
                            designElement.SetDelegate(new UserDelegates.ShowInSysControlPlayerEditorDelegate(ShowInSysControlPlayerEditor));
                            break;
                        case InSysControlType.InSysRectangle:
                            designElement.SetDelegate(new UserDelegates.ShowInSysControlPlayerEditorDelegate(ShowInSysControlPlayerEditor));
                            break;
                        case InSysControlType.InSysRectArea:
                            designElement.SetDelegate(new UserDelegates.ShowInSysControlPlayerEditorDelegate(ShowInSysControlPlayerEditor));
                            break;
                        case InSysControlType.InSysBasicTextBox:
                            designElement.SetDelegate(new UserDelegates.ShowInSysControlPlayerEditorDelegate(ShowInSysControlPlayerEditor));
                            break;
                        case InSysControlType.InSysLabel:
                            designElement.SetDelegate(new UserDelegates.ShowInSysControlPlayerEditorDelegate(ShowInSysControlPlayerEditor));
                            break;
                        case InSysControlType.InSysVideoBox:
                            designElement.SetDelegate(new UserDelegates.ShowInSysControlPlayerEditorDelegate(ShowInSysControlPlayerEditor));
                            break;
                        case InSysControlType.InSysStreamingBox:
                            designElement.SetDelegate(new UserDelegates.ShowInSysControlPlayerEditorDelegate(ShowInSysControlPlayerEditor));
                            break;
                        case InSysControlType.InSysAudioBox:
                            designElement.SetDelegate(new UserDelegates.ShowInSysControlPlayerEditorDelegate(ShowInSysControlPlayerEditor));
                            break;
                        case InSysControlType.InSysAnalogueClock:
                            designElement.SetDelegate(new UserDelegates.ShowInSysControlPlayerEditorDelegate(ShowInSysControlPlayerEditor));
                            break;
                        case InSysControlType.InSysDate:
                            designElement.SetDelegate(new UserDelegates.ShowInSysControlPlayerEditorDelegate(ShowInSysControlPlayerEditor));
                            break;
                        case InSysControlType.InSysDigitalClock:
                            designElement.SetDelegate(new UserDelegates.ShowInSysControlPlayerEditorDelegate(ShowInSysControlPlayerEditor));
                            break;
                        case InSysControlType.InSysEllipse:
                            designElement.SetDelegate(new UserDelegates.ShowInSysControlPlayerEditorDelegate(ShowInSysControlPlayerEditor));
                            break;
                        case InSysControlType.InSysRSS:
                            designElement.SetDelegate(new UserDelegates.ShowInSysControlPlayerEditorDelegate(ShowInSysControlPlayerEditor));
                            break;
                        case InSysControlType.InSysScrollText:
                            designElement.SetDelegate(new UserDelegates.ShowInSysControlPlayerEditorDelegate(ShowInSysControlPlayerEditor));
                            break;
                        case InSysControlType.InSysWebControl:
                            designElement.SetDelegate(new UserDelegates.ShowInSysControlPlayerEditorDelegate(ShowInSysControlPlayerEditor));
                            break;
                        case InSysControlType.InSysFlashBox:
                            designElement.SetDelegate(new UserDelegates.ShowInSysControlPlayerEditorDelegate(ShowInSysControlPlayerEditor));
                            break;
                    }
                }
            }
        }

        public static string CheckInSysControlName(InSysControlType controlType, UIElementCollection uielementCollection)
        {
            List<FrameworkElement> controls = new List<FrameworkElement>();

            foreach (FrameworkElement fe in uielementCollection)
            {
                controls.Add(fe);
            }
            string name = CheckInSysControlName(controlType, controls);           

            return name;
        }

        /// <summary>
        /// InSysControl Name Check
        /// </summary>
        /// <param name="controlType"></param>
        /// <param name="uielementCollection"></param>
        /// <returns></returns>
        public static string CheckInSysControlName(InSysControlType controlType, List<FrameworkElement> controls)
        {
            string name = "Unknown";
            int index = 1;
            while (true)
            {
                switch (controlType)
                {
                    case InSysControlType.InSysSlideViewer:
                        name = string.Format("SliderViewer-{0}", index);
                        break;
                    case InSysControlType.InSysBasicButton:
                        name = string.Format("BasicButton-{0}", index);
                        break;
                    case InSysControlType.InSysImageBox:
                        name = string.Format("ImageBox-{0}", index);
                        break;
                    case InSysControlType.InSysRectangle:
                        name = string.Format("Rectangle-{0}", index);
                        break;
                    case InSysControlType.InSysRectArea:
                        name = string.Format("RectArea-{0}", index);
                        break;
                    case InSysControlType.InSysBasicTextBox:
                        name = string.Format("BasicTextBox-{0}", index);
                        break;
                    case InSysControlType.InSysLabel:
                        name = string.Format("Label-{0}", index);
                        break;
                    case InSysControlType.InSysVideoBox:
                        name = string.Format("VideoBox-{0}", index);
                        break;
                    case InSysControlType.InSysStreamingBox:
                        name = string.Format("StreamingBox-{0}", index);
                        break;
                    case InSysControlType.InSysAudioBox:
                        name = string.Format("AudioBox-{0}", index);
                        break;
                    case InSysControlType.InSysAnalogueClock:
                        name = string.Format("AnalogueClock-{0}", index);
                        break;
                    case InSysControlType.InSysDate:
                        name = string.Format("Date-{0}", index);
                        break;
                    case InSysControlType.InSysDigitalClock:
                        name = string.Format("DigitalClock-{0}", index);
                        break;
                    case InSysControlType.InSysEllipse:
                        name = string.Format("Ellipse-{0}", index);
                        break;
                    case InSysControlType.InSysRSS:
                        name = string.Format("RSS-{0}", index);
                        break;
                    case InSysControlType.InSysScrollText:
                        name = string.Format("ScrollText-{0}", index);
                        break;
                    case InSysControlType.InSysWebControl:
                        name = string.Format("WebControl-{0}", index);
                        break;
                    case InSysControlType.InSysFlashBox:
                        name = string.Format("FlashControl-{0}", index);
                        break;
                    default:
                        name = string.Format("Unknown-{0}", index);
                        return null;
                }
                index++;
                IEnumerable<FrameworkElement> list = controls.Where(o => (o as IDesignElement) != null && string.IsNullOrEmpty((o as IDesignElement).Name) == false && (o as IDesignElement).Name.Equals(name) == true);
                if (list == null || list.Count() == 0)
                {
                    break;
                }                
            }

            return name;
        }

        /// <summary>
        /// IPlayer 인터페이스 객체 얻기
        /// </summary>
        /// <param name="insysControl">IPlayer 인터페이스 객체를 반환할 InSysControl 객체</param>
        /// <returns></returns>
        public static IDesignElement GetInSysControlPlayer(UIElement insysControl)
        {
            Type controlType = insysControl.GetType();
            switch (controlType.Name)
            {               
                case "InSysSlideViewer":
                case "InSysImageBox":
                case "InSysFlashBox":
                case "InSysVideoBox":
                case "InSysStreamingBox":
                case "InSysAudioBox":
                case "InSysAnalogueClock":
                case "InSysDate":
                case "InSysDigitalClock":
                case "InSysEllipse":
                case "InSysRSS":
                case "InSysScrollText":
                case "InSysWebControl":
                case "InSysBasicButton":
                case "InSysRectArea":
                case "InSysRectangle":
                case "InSysLabel":
                case "InSysBasicTextBox":
                    return insysControl as IDesignElement;     
                default:
                    return null;
            }
        }

        public static void SetInSysControlPropertyVisibilty(UIElement insysControl)
        {
            if (_PropertyGrid == null)
                return;
            Type controlType = insysControl.GetType();

            if (_PropertyGrid != null)
                _PropertyGrid.UpdateLayout();

            switch (controlType.Name)
            {
                case "InSysSlideViewer":
                    break;
                case "InSysImageBox":
                case "InSysVideoBox":                    
                    break;
                case "InSysStreamingBox":
                    break;
                case "InSysFlashBox":
                    break;
                case "InSysAudioBox":
                    InSysBasicControls.InSysControlManager.SetVisibilityState_ControlContentOfProperty("Visibility", false);
                    break;
                case "InSysAnalogueClock":
                case "InSysDate":
                case "InSysDigitalClock":
                case "InSysEllipse":
                case "InSysRSS":
                case "InSysScrollText":
                case "InSysWebControl":
                    break;
                case "InSysBasicButton":
                    InSysBasicControls.InSysControlManager.HidePropertyDisplayName("BackgroundTest");                    
                    break;
                case "InSysRectArea":
                    InSysBasicControls.InSysControlManager.SetVisibilityState_ControlContentOfProperty("Opacity", false);
                    InSysBasicControls.InSysControlManager.ShowPropertyCategoryItem("Appearance", Visibility.Collapsed);
                    break;
                case "InSysRectangle":
                    
                default:
                    break;
            }
        }
      
        public static void SettingContentElementEditor(IDesignElement designElement)
        {
            ContentEditor contentEditor = Get_TargetPagesPropertyItemContent("ContentEdit", true) as ContentEditor;
            if (contentEditor != null)
            {
                contentEditor.ChangedEditValueHandler = (editValue) =>
                {
                    ShowInSysControlPlayerEditor(designElement);
                };
            }           
        }

        public static void Set_ButtonStylePropertyControl(IDesignElement designElement)
        {
            ButtonStyleEditor buttonStyleEditor = Get_TargetPagesPropertyItemContent("ButtonStyle", true) as ButtonStyleEditor;
            if (buttonStyleEditor != null)
            {
                buttonStyleEditor.CallButtonSTyleEditorHandler = () =>
                {
                    ButtonStyle buttonStyle = ShowButtonStyle(designElement.ButtonStyle, designElement.ButtonStyleKey);
                    if (buttonStyle != null)
                    {
                        designElement.ButtonStyle = buttonStyle.StyleContent;
                        designElement.ButtonStyleKey = buttonStyle.StyleKeyName;
                        return buttonStyle.StyleContent;
                    }

                    return designElement.ButtonStyle;
                };
                buttonStyleEditor.SetButtonStyle(designElement.ButtonStyle);
            }
        }

        public static void changeButtonStylePropertyEditor(IDesignElement designElement)
        {
            ButtonStyleEditor buttonStyleEditor = Get_TargetPagesPropertyItemContent("ButtonStyle", true) as ButtonStyleEditor;
            if (buttonStyleEditor != null)
            {
                buttonStyleEditor.setScaleTransform(designElement.Width, designElement.Height);
            }
        }

        #region TouchEvent

        public static void Set_TouchEventPropertyControl(IDesignElement designElement)
        {
            TouchEventPropertyControl touchEventPropertyControl = Get_TargetPagesPropertyItemContent("TouchEvent", true) as TouchEventPropertyControl;
            if (touchEventPropertyControl != null)
            {
                touchEventPropertyControl.CallTouchEventPropertyControlHandler = () =>
                {
                    Dictionary<string, string> touchEventDic = ShowTouchEventEditor(designElement.TouchEvent, false, designElement.Name);
                    designElement.TouchEvent = touchEventDic;
                    return touchEventDic;
                };
                touchEventPropertyControl.SetTouchEventDic(designElement.TouchEvent);
            }
        }

        /// <summary>
        /// Touch Event
        /// </summary>
        /// <param name="touchEventDic"></param>
        /// <returns></returns>
        public static Dictionary<string, string> ShowTouchEventEditor(Dictionary<string, string> touchEventDic, bool isScreen, string touchContentName)
        {
            TouchEventWindow touchEventWindow = new TouchEventWindow();
            touchEventWindow.Owner = Application.Current.MainWindow;
            touchEventWindow.TouchEventDic = touchEventDic;
            if (string.IsNullOrEmpty(InSysDataManager.TouchflowProjectPath) == false)
            {
                if (File.Exists(InSysDataManager.TouchflowProjectPath) == true)
                {
                    FileInfo fi = new FileInfo(InSysDataManager.TouchflowProjectPath);
                    touchEventWindow.TouchEventFileFullPath = string.Format("{0}\\{1}", fi.DirectoryName, "project_touchevent.xml");//LoadUserTouchEventFromXml 함수에도 정의
                }
            }

            if (InSysControlManager.GetTouchProjectInfoHandler != null)
            {
                TouchProjectInfo touchProjectInfo = InSysControlManager.GetTouchProjectInfoHandler();

                RootContent rootContent = touchEventWindow.CreateEventRootContent();
                bool isSelfTouchContent = false;
                foreach (var pageinfo in touchProjectInfo.PageInfoList)
                {
                    if (isScreen == true && (pageinfo.PageNameTemp.Equals(touchContentName) == true || pageinfo.PageNameTemp.ToLower().Equals(touchContentName.ToLower()) == true))//스크린
                        isSelfTouchContent = true;
                    ScreenTouchContent screenTouchContent = touchEventWindow.CreateScreenTouchContent(rootContent, pageinfo.PageNameTemp, pageinfo.PageGuid, isSelfTouchContent);
                    foreach (var contentInfo in pageinfo.Contents)
                    {
                        if (isScreen == false && (contentInfo.ContentName.Equals(touchContentName) == true || contentInfo.ContentName.ToLower().Equals(touchContentName.ToLower()) == true))//컨텐츠.
                            isSelfTouchContent = true;
                        ElementTouchContent elementTouchContent = touchEventWindow.CreateElementTouchContent(screenTouchContent, contentInfo.ContentName, GetEventObjectType(contentInfo.ContentType), isSelfTouchContent);
                    }
                }
            }

            if (touchEventWindow.ShowDialog() == true)
            {
                g_UserTouchEvents = touchEventWindow.UserTouchEvents;
                return touchEventWindow.TouchEventDic;
            }

            return touchEventDic;
        }

        private static EventObjectType GetEventObjectType(Type contentType)
        {
            switch(contentType.Name)
            {
                case "InSysSlideViewer":
                    return EventObjectType.SlideViewer;
                case "InSysImageBox":
                    return EventObjectType.Image;
                case "InSysFlashBox":
                    return EventObjectType.Flash;
                case "InSysVideoBox":
                    return EventObjectType.Video;
                case "InSysStreamingBox":
                    return EventObjectType.Streaming;
                case "InSysAudioBox":
                    return EventObjectType.Audio;
                case "InSysAnalogueClock":
                    return EventObjectType.Control;
                case "InSysDate":
                    return EventObjectType.Control;
                case "InSysDigitalClock":
                    return EventObjectType.Control;
                case "InSysEllipse":
                    return EventObjectType.Shape;
                case "InSysRSS":
                    return EventObjectType.RSS;
                case "InSysScrollText":
                    return EventObjectType.ScrollText;
                case "InSysWebControl":
                    return EventObjectType.Web;
                case "InSysBasicButton":
                    return EventObjectType.Button;
                case "InSysRectArea":
                    return EventObjectType.RectArea;
                case "InSysRectangle":
                    return EventObjectType.Shape;
                case "InSysLabel":
                    return EventObjectType.Label;
                case "InSysBasicTextBox":
                    return EventObjectType.TextBox;
                default:
                    return EventObjectType.None;
            }
        }

        #endregion

        public static void Set_TextProperty(IDesignElement designElement)
        {
            TextEditorButton contentEditor = Get_TargetPagesPropertyItemContent("Text", true) as TextEditorButton;
            if (contentEditor != null)
            {
                contentEditor.CallButtonSimpleTextEditorHandler = () =>
                {
                    ShowSimpleTextEditor(designElement);
                    contentEditor.SetTextBlock((string)designElement.Content);
                    return ;
                };

                contentEditor.TextChangedSimpleTextEditorHandler = (string changedText) =>
                {
                    designElement.Content = changedText;
                };

                contentEditor.SetTextBlock((string)designElement.Content);
            }
        }

        /// <summary>
        /// Content Element 색 설정
        /// </summary>
        /// <param name="designElement"></param>
        public static void Set_ContentElementBrushProperty(IDesignElement designElement)
        {
            BrushEditorCallButton backgroundBrushEditor = Get_TargetPagesPropertyItemContent("Background", true) as BrushEditorCallButton;
            if (backgroundBrushEditor != null)
            {
                backgroundBrushEditor.CallBrushEditorEventHandler = () =>
                {
                    Brush brush = ShowBrushEditor(designElement.Background, false);
                    designElement.Background = brush;
                    return brush;
                };
                backgroundBrushEditor.SetBrush(designElement.Background);       
            }
            

            BrushEditorCallButton borderBrushBrushEditor = Get_TargetPagesPropertyItemContent("BorderBrush", true) as BrushEditorCallButton;
            if (borderBrushBrushEditor != null)
            {
                borderBrushBrushEditor.CallBrushEditorEventHandler = () =>
                {
                    Brush brush = ShowBrushEditor(designElement.BorderBrush, false); 
                    designElement.BorderBrush = brush;
                    return brush;
                };
                borderBrushBrushEditor.SetBrush(designElement.BorderBrush);
            }
            

            BrushEditorCallButton foregroundBrushEditor = Get_TargetPagesPropertyItemContent("Foreground", true) as BrushEditorCallButton;
            if (foregroundBrushEditor != null)
            {
                foregroundBrushEditor.CallBrushEditorEventHandler = () =>
                {
                    Brush brush = ShowBrushEditor(designElement.Foreground, false); 
                    designElement.Foreground = brush;
                    return brush;
                };
                foregroundBrushEditor.SetBrush(designElement.Foreground);
            }
            
        }

        public static Brush ShowBrushEditor(Brush brush, bool isSupportImageBackground)
        {
            BrushEditorWindow brushEditor = new BrushEditorWindow(isSupportImageBackground);
            brushEditor.Owner = Application.Current.MainWindow;
            brushEditor.SetBrush(brush);
            
            if (brushEditor.ShowDialog() == true)
            {
                return brushEditor.BrushValue;
            }

            return brush;
        }

        public static ButtonStyle ShowButtonStyle(Style style, string key)
        {           
            ButtonStyleLoaderWindow buttonStyleLoaderWindow = new ButtonStyleLoaderWindow();
            buttonStyleLoaderWindow.Owner = Application.Current.MainWindow;

            buttonStyleLoaderWindow.SelectedButtonStyle = new ButtonStyle(key, style);
            if (buttonStyleLoaderWindow.ShowDialog() == true)
            {
                if (buttonStyleLoaderWindow.SelectedButtonStyle == null)
                    return null;
                return buttonStyleLoaderWindow.SelectedButtonStyle;
            }

            return null;            
        }

        /// <summary>
        /// 텍스트 에디터를 이용하여 텍스트 입력.
        /// </summary>
        /// <param name="textblock"></param>
        public static void ShowSimpleTextEditor(IDesignElement designElement)
        {
            string text = (string)designElement.Content;            
            
            SimpleTextEditor simpleTextEditor = new SimpleTextEditor();
            simpleTextEditor.Owner = Application.Current.MainWindow;            
            string[] result = text.Split(new string[] { @"\n", @"\r\n" }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string s in result)
            {
                simpleTextEditor.Inlines.Add(new Run(s));
            }
            simpleTextEditor.SetText(simpleTextEditor.Inlines);
            if (simpleTextEditor.ShowDialog() == true)
            {
                designElement.Content = "";
                List<Inline> inlines = simpleTextEditor.Inlines;
                if (inlines != null && inlines.Count != 0)
                {
                for (int i = 0; i < inlines.Count; i++)
                {
                    if (i != 0)
                    {
                        Run run = (Run)inlines[i];                        
                        designElement.Content += run.Text;
                    }
                    else
                    {
                        Run run = (Run)inlines[i];
                        designElement.Content += run.Text;
                    }
                }
                    }
            }
            simpleTextEditor.Dispose();
            simpleTextEditor.Close();

            return ;
        }

        public static void Set_StartTime(TimeSpan startTime)
        {
            TimePickerEditor timePickerEditor = Get_TargetPagesPropertyItemContent("StartTime", true) as TimePickerEditor;
            if (timePickerEditor != null)
            {
                timePickerEditor.SelectedTime = startTime;
                timePickerEditor.UpdateLayout();
            }
        }

        public static void Set_InitTimeControl(string timeName)
        {
            TimePickerEditor timePickerEditor = Get_TargetPagesPropertyItemContent(timeName, true) as TimePickerEditor;
            if (timePickerEditor != null)
            {
                timePickerEditor.InitControl();
            }
        }

        public static void Set_EndTime(TimeSpan endTime)
        {
            TimePickerEditor timePickerEditor = Get_TargetPagesPropertyItemContent("EndTime", true) as TimePickerEditor;
            if (timePickerEditor != null)
            {
                timePickerEditor.SelectedTime = endTime;                
                timePickerEditor.UpdateLayout();
            }
        }      

        public static void Set_ControlLifeTimeEditProperty(IDesignElement designElement, bool isApplyPageLifeTime)
        {
            TimePickerEditor starttimePickerEditor = Get_TargetPagesPropertyItemContent("StartTime", true) as TimePickerEditor;
            TimePickerEditor endtimePickerEditor = Get_TargetPagesPropertyItemContent("EndTime", true) as TimePickerEditor;

            if (starttimePickerEditor != null)
            {
                starttimePickerEditor.ChangedEditValueHandler = (editValue) =>
                {
                    if (designElement.IsApplyLifeTime == true)
                    {
                        if (designElement.StartTime.Equals(editValue) == false)
                            designElement.StartTime = editValue;
                    }
                };

                if (designElement.IsApplyLifeTime == true)
                {
                    if (designElement.StartTime == TimeSpan.Zero)
                    {
                        if (designElement.EndTime == TimeSpan.Zero)
                        {
                            starttimePickerEditor.EnableButton(false, false);
                            endtimePickerEditor.EnableButton(true, false);
                        }
                        else
                        {
                            starttimePickerEditor.EnableButton(true, false);
                            endtimePickerEditor.EnableButton(true, true);
                        }
                    }
                    else
                    {
                        if (designElement.StartTime == designElement.EndTime)
                        {
                            starttimePickerEditor.EnableButton(false, true);
                            endtimePickerEditor.EnableButton(true, false);
                        }
                        else if (designElement.StartTime < designElement.EndTime)
                        {
                            starttimePickerEditor.EnableButton(true, true);
                            endtimePickerEditor.EnableButton(true, true);
                        }
                    }                    
                }
                else
                {
                    starttimePickerEditor.EnableButton(false, false);
                }

                if (starttimePickerEditor.SelectedTime.Equals(designElement.StartTime) == false)
                {
                    starttimePickerEditor.SelectedTime = designElement.StartTime;
                }
            }

           
            if (endtimePickerEditor != null)
            {
                endtimePickerEditor.ChangedEditValueHandler = (editValue) =>
                {
                    if (designElement.IsApplyLifeTime == true)
                    {
                        if (designElement.EndTime.Equals(editValue) == false)
                            designElement.EndTime = editValue;
                    }
                };               

                if (endtimePickerEditor.SelectedTime.Equals(designElement.EndTime) == false)
                {
                    endtimePickerEditor.SelectedTime = designElement.EndTime;
                }
            }

            _PropertyGrid.UpdateLayout();
        }    

        public static void ShowInSysControlPlayerEditor(IDesignElement designElement)
        {
            
            if (designElement != null)
            {
                switch (designElement.GetType().Name)
                {
                    case "InSysLabel":
                    case "InSysBasicTextBox":
                        {
                            TextEditorButton contentEditor = Get_TargetPagesPropertyItemContent("Text", true) as TextEditorButton;
                            if (contentEditor != null)
                            {
                                ShowSimpleTextEditor(designElement);
                                contentEditor.SetTextBlock((string)designElement.Content);
                            }                          
                        }
                        break;
                    default:
                        {
                            Editors.PlaylistEditor playEditor = new Editors.PlaylistEditor();
                            playEditor.ShowEditor(designElement, designElement.Playlist);
                            if (playEditor.IsEditOK == true)
                            {
                                designElement.Playlist = playEditor.GetPlaylist();
                                //designElementInterface.UpdatePlaylistItems();//VideoScreenShot.CaptureScreen 함수 두번 호출문제로 주석처리함.
                            }

                            playEditor.Close();
                        }
                        break;                   
                }                
            }
        }

        public static void Init_ContentElementProperty(UIElement element, double pageLifeTime, bool isApplyPageTime)
        {
            Type controlType = element.GetType();
            IDesignElement designElement = element as IDesignElement;
            if (designElement != null)
            {
                switch (controlType.Name)
                {
                    case "InSysSlideViewer":
                        break;
                    case "InSysImageBox":
                    case "InSysVideoBox":
                    case "InSysStreamingBox":
                    case "InSysAudioBox":
                    case "InSysRSS":
                    case "InSysScrollText":
                    case "InSysWebControl":
                    case "InSysFlashBox":
                        SettingContentElementEditor(designElement);
                        break;
                    case "InSysBasicButton":
                        Set_ButtonStylePropertyControl(designElement);
                        Set_TouchEventPropertyControl(designElement);
                        break;
                    case "InSysBasicTextBox":
                    case "InSysLabel":
                        Set_TextProperty(designElement);
                        break;
                    case "InSysRectArea":
                        Set_TouchEventPropertyControl(designElement);
                        break;
                    case "InSysRectangle":
                    case "InSysAnalogueClock":
                    case "InSysDate":
                    case "InSysDigitalClock":
                    case "InSysEllipse":                        
                        break;
                    default:
                        break;
                }

                Set_ContentElementBrushProperty(designElement);
                if (isApplyPageTime == true)
                    TimePickerEditor.LimitLifeTime = TimeSpan.FromSeconds(pageLifeTime);
                else
                    TimePickerEditor.LimitLifeTime = TimeSpan.FromSeconds(0);
                Set_ControlLifeTimeEditProperty(designElement, isApplyPageTime);

                #region 디자인뷰에서 초기 및 선택시 LifeTime 컨트롤 설정

                IElementProperties elementProperty = element as IElementProperties;              

                Set_ContentElementIsEnableProperty(elementProperty.ElementProperties.IsApplyLifeTime, true);
                #endregion
            }
        }

        public static void Set_ContentElementIsEnableProperty(bool isApplyLifeTime, bool isModifyControlName)
        {
            Set_ContentElementControlIsEnableProperty("StartTime", isApplyLifeTime);
            Set_ContentElementControlIsEnableProperty("EndTime", isApplyLifeTime);
            Set_ContentElementControlIsEnableProperty("Name", isModifyControlName);
        }

        public static void Init_SetControlActionEvent(UIElement element, List<TouchPageInfo> pageInfos, TouchPageInfo currentPageInfo)
        {
            Type controlType = element.GetType();
            _CurrentElement = element;
            switch (controlType.Name)
            {
                case "InSysSlideViewer":
                    break;
                case "InSysBasicButton":
                    Set_InSysControlActionEvent(element, pageInfos, currentPageInfo);
                    #region HighlightStyle 속성 숨김
                    //Set_ButtonShapeProperty();
                    SettingPropertyItemContainer("HighlightStyle", false);
                    #endregion
                    break;
                case "InSysRectArea":
                    Set_InSysControlActionEvent(element, pageInfos, currentPageInfo);     
                    break;
                case "InSysFlashBox":
                case "InSysRectangle":
                case "InSysImageBox":                
                case "InSysVideoBox":
                case "InSysStreamingBox":
                case "InSysAudioBox":
                case "InSysAnalogueClock":
                case "InSysDate":
                case "InSysDigitalClock":
                case "InSysEllipse":
                case "InSysRSS":
                case "InSysScrollText":
                case "InSysWebControl":                    
                default:
                    break;
            }
        }

        private static void Set_InSysControlActionEvent(UIElement element, List<TouchPageInfo> pageInfoList, TouchPageInfo currentPageInfo)
        {
            IDesignElement designElement = element as IDesignElement;
            if (designElement != null)
            {
                if (_PropertyGrid != null)
                    _PropertyGrid.UpdateLayout();

                Set_ActionEvent(pageInfoList, currentPageInfo);

                if (designElement.ActionEvent != null)
                {
                    switch (designElement.ActionEvent.ActionEventType)
                    {
                        case ActionEventType.GoToPage:
                            GoToPageEvent gotopage_event = designElement.ActionEvent as GoToPageEvent;
                            Set_GoToPageActionEvent(gotopage_event, pageInfoList, currentPageInfo);
                            break;
                        case ActionEventType.Hyperlink:
                            HyperlinkEvent hyperlink_event = designElement.ActionEvent as HyperlinkEvent;
                            Set_HyperlinkActionEvent(hyperlink_event);
                            break;
                        case ActionEventType.OpenFile:
                            OpenFileEvent openfile_event = designElement.ActionEvent as OpenFileEvent;
                            Set_OpenFileActionEvent(openfile_event);
                            break;
                        case ActionEventType.ShowHide:
                            ShowHideEvent showhide_event = designElement.ActionEvent as ShowHideEvent;
                            Set_ShowHideActionEvent(showhide_event, currentPageInfo);                                                
                            break;
                        default:
                            break;
                    }
                }
                else
                {
                    Set_GoToPageActionEvent(null, pageInfoList, currentPageInfo);
                }
            }
        }

        private static ComboBox Set_ActionEventComboBox(ActionEvent actionEvent, ActionEventType actionEventType)
        {
            ComboBox combobox = GetControlEditorOfProperty("ActionEvent") as ComboBox;
            if (combobox != null && combobox.Items.Count <= 0)
            {
                combobox.Items.Clear();

                GoToPageEvent  gotopageAction = new GoToPageEvent("Go To Page", ActionEventType.GoToPage);
                if (actionEvent != null && (actionEvent as GoToPageEvent) != null)
                {
                    gotopageAction.SelectedPageItem = (actionEvent as GoToPageEvent).SelectedPageItem;
                }
                AddComboBoxItem(gotopageAction, combobox);
                ShowHideEvent showhideAction = new ShowHideEvent("Show/Hide", ActionEventType.ShowHide);
                if (actionEvent != null && (actionEvent as ShowHideEvent) != null)
                {
                    showhideAction.Type = (actionEvent as ShowHideEvent).Type;
                    showhideAction.SelectedItemName = (actionEvent as ShowHideEvent).SelectedItemName;
                }
                AddComboBoxItem(showhideAction, combobox);
                HyperlinkEvent hyperlinkAction = new HyperlinkEvent("Hyperlink", ActionEventType.Hyperlink);
                if (actionEvent != null && (actionEvent as HyperlinkEvent) != null)
                {
                    hyperlinkAction.URL = (actionEvent as HyperlinkEvent).URL;
                }
                AddComboBoxItem(hyperlinkAction, combobox);
                OpenFileEvent openfileAction = new OpenFileEvent("Open File", ActionEventType.OpenFile);
                if (actionEvent != null && (actionEvent as OpenFileEvent) != null)
                {
                    openfileAction.OpenFile = (actionEvent as OpenFileEvent).OpenFile;
                }
                AddComboBoxItem(openfileAction, combobox);
 
                combobox.SelectionChanged += (s, e) =>
                {
                    if (e.RemovedItems.Count == 1)
                    {
                        if (e.AddedItems[0].Equals(e.RemovedItems[0]) == true)
                            return;
                    }

                    ComboBox combobox_actionEvent = s as ComboBox;
                    if (combobox_actionEvent != null)
                    {
                        ComboBoxItem comboboxItem = combobox_actionEvent.SelectedItem as ComboBoxItem;
                        if (comboboxItem != null)
                        {
                            IDesignElement designElement = _CurrentElement as IDesignElement;
                            ActionEvent aeItem = comboboxItem.Tag as ActionEvent;

                            if (designElement.ActionEvent == null)
                            {
                                designElement.ActionEvent = aeItem;
                            }
                            else
                            {
                                designElement.ActionEvent = aeItem;
                                //designElement.ActionEvent.ActionEventType = aeItem.ActionEventType;
                                //designElement.ActionEvent.EventName = aeItem.EventName;
                            }                           

                            switch (aeItem.ActionEventType)
                            {
                                case ActionEventType.GoToPage:
                                    SettingPropertyItemContainer("TargetPages", true);
                                    SettingPropertyItemContainer("ShowHideType", false);
                                    SettingPropertyItemContainer("TargetObjects", false);
                                    SettingPropertyItemContainer("URL", false);
                                    SettingPropertyItemContainer("File", false);
                                    break;
                                case ActionEventType.Hyperlink:
                                    SettingPropertyItemContainer("TargetPages", false);
                                    SettingPropertyItemContainer("ShowHideType", false);
                                    SettingPropertyItemContainer("TargetObjects", false);
                                    SettingPropertyItemContainer("URL", true);
                                    SettingPropertyItemContainer("File", false);
                                    break;
                                case ActionEventType.OpenFile:  
                                    SettingPropertyItemContainer("TargetPages", false);
                                    SettingPropertyItemContainer("ShowHideType", false);
                                    SettingPropertyItemContainer("TargetObjects", false);
                                    SettingPropertyItemContainer("URL", false);
                                    SettingPropertyItemContainer("File", true);
                                    break;
                                case ActionEventType.ShowHide:
                                    SettingPropertyItemContainer("TargetPages", false);
                                    SettingPropertyItemContainer("ShowHideType", true);
                                    SettingPropertyItemContainer("TargetObjects", true);
                                    SettingPropertyItemContainer("URL", false);
                                    SettingPropertyItemContainer("File", false);
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                };

                foreach (ComboBoxItem cbItem in combobox.Items)
                {
                    if ((cbItem.Tag as ActionEvent).ActionEventType.Equals(actionEventType) == true)
                    {
                        combobox.SelectedItem = cbItem;
                        break;
                    }
                }

                if (combobox.SelectedItem == null)
                    combobox.SelectedIndex = 0;

            }

           
            return combobox;
        }       
        
        private static void Set_GoToPageActionEvent(GoToPageEvent actionEvent, List<TouchPageInfo> pageInfos, TouchPageInfo currentPageInfo)
        {
            ActionEventType actionEventType = ActionEventType.GoToPage;
            if (actionEvent != null)
                actionEventType = actionEvent.ActionEventType;

            Set_ActionEventComboBox(actionEvent, actionEventType);
            ComboBox pagesComboBox = Get_TargetPagesPropertyItemEditor("TargetPages", true) as ComboBox;

            if (pagesComboBox != null)
            {
                if (actionEvent != null)
                {
                    foreach (ComboBoxItem cbItem in pagesComboBox.Items)
                    {
                        MovePageItem item = cbItem.Tag as MovePageItem;
                        if (item != null)
                        {
                            if (item.PageID.Equals(actionEvent.SelectedPageItem.PageID) == true && item.PageName.Equals(actionEvent.SelectedPageItem.PageName))
                            {
                                pagesComboBox.SelectedItem = cbItem;
                                break;
                            }
                        }
                    }
                }

                if (pagesComboBox.SelectedItem == null)
                {
                    pagesComboBox.SelectedIndex = 0;
                }
            }

            SettingPropertyItemContainer("TargetPages", true);
            SettingPropertyItemContainer("ShowHideType", false);
            SettingPropertyItemContainer("TargetObjects", false);
            SettingPropertyItemContainer("URL", false);
            SettingPropertyItemContainer("File", false);
        }

        private static void Set_ShowHideActionEvent(ShowHideEvent actionEvent, TouchPageInfo currentPageInfo)
        {
            ActionEventType actionEventType = ActionEventType.ShowHide;
            if (actionEvent != null)
                actionEventType = actionEvent.ActionEventType;

            Set_ActionEventComboBox(actionEvent, actionEventType);
            ComboBox showhideTypeComboBox = Get_TargetPagesPropertyItemEditor("ShowHideType", true) as ComboBox;           
            ComboBox targetComboBox = Get_TargetPagesPropertyItemEditor("TargetObjects", true) as ComboBox;
            if (actionEvent != null)
            {
                if (showhideTypeComboBox != null)
                {
                    foreach (ShowHideType type in showhideTypeComboBox.Items)
                    {
                        if (actionEvent.Type.Equals(type) == true)
                        {
                            showhideTypeComboBox.SelectedItem = type;
                            break;
                        }
                    }
                }
                if (targetComboBox != null)
                {
                    foreach (ComboBoxItem cbItem in targetComboBox.Items)
                    {
                        //if (cbItem.Equals(targetComboBox.SelectedItem) == true)
                        //{
                        //    targetComboBox.SelectedItem = cbItem;
                        //    break;
                        //}
                        TouchContentInfo item = cbItem.Tag as TouchContentInfo;
                        string selectItemName = actionEvent.SelectedItemName;
                        if (item != null && string.IsNullOrEmpty(selectItemName) == false && item.ContentName.Equals(selectItemName) == true)
                        {
                            targetComboBox.SelectedItem = cbItem;
                            break;
                        }
                    }

                    if (targetComboBox.SelectedItem == null)
                    {
                        targetComboBox.SelectedIndex = 0;
                    }         
                }
            }

         

            SettingPropertyItemContainer("TargetPages", false);
            SettingPropertyItemContainer("ShowHideType", true);
            SettingPropertyItemContainer("TargetObjects", true);
            SettingPropertyItemContainer("URL", false);
            SettingPropertyItemContainer("File", false);
        }

        private static void Set_HyperlinkActionEvent(HyperlinkEvent actionEvent)
        {
            ActionEventType actionEventType = ActionEventType.Hyperlink;
            if (actionEvent != null)
                actionEventType = actionEvent.ActionEventType;

            Set_ActionEventComboBox(actionEvent, actionEventType);
            TextBox textbox_Hyperlink = Get_TargetPagesPropertyItemEditor("URL", true) as TextBox;
            textbox_Hyperlink.Text = actionEvent.URL;

            SettingPropertyItemContainer("TargetPages", false);
            SettingPropertyItemContainer("ShowHideType", false);
            SettingPropertyItemContainer("TargetObjects", false);
            SettingPropertyItemContainer("URL", true);
            SettingPropertyItemContainer("File", false);
        }

        private static void Set_OpenFileActionEvent(OpenFileEvent actionEvent)
        {
            ActionEventType actionEventType = ActionEventType.OpenFile;
            if (actionEvent != null)
                actionEventType = actionEvent.ActionEventType;

            Set_ActionEventComboBox(actionEvent, actionEventType);
            UIEditor fe = Get_TargetPagesPropertyItemEditor("File", true) as UIEditor;
            fe.EditValue = actionEvent.OpenFile;

            SettingPropertyItemContainer("TargetPages", false);
            SettingPropertyItemContainer("ShowHideType", false);
            SettingPropertyItemContainer("TargetObjects", false);
            SettingPropertyItemContainer("URL", false);
            SettingPropertyItemContainer("File", true);
        }

        private static void Set_ActionEvent(List<TouchPageInfo> pageInfoList, TouchPageInfo currentPageInfo)
        {
            try
            {
                #region GoToPage Target Page 등록

                ComboBox pagesComboBox = Get_TargetPagesPropertyItemEditor("TargetPages", true) as ComboBox;
                if (pagesComboBox != null)
                {
                    pagesComboBox.Items.Clear();
                    pagesComboBox.SelectionChanged += (s, e) =>
                    {
                        ComboBox combobox = s as ComboBox;
                        if (combobox != null)
                        {
                            ComboBoxItem item = combobox.SelectedItem as ComboBoxItem;
                            if (item != null)
                            {
                                MovePageItem mpItem = item.Tag as MovePageItem;
                                if (mpItem != null)
                                {
                                    IDesignElement designElement = _CurrentElement as IDesignElement;
                                    GoToPageEvent aeItem = designElement.ActionEvent as GoToPageEvent;
                                    if (aeItem != null)
                                        aeItem.SelectedPageItem = mpItem;
                                }
                            }
                        }
                    };

                    #region None Page 등록
                    MovePageItem noneItem = new MovePageItem("", "[No Move]");
                    ComboBoxItem cbnoneItem = new ComboBoxItem();
                    cbnoneItem.Content = noneItem.PageName;
                    cbnoneItem.Tag = noneItem;
                    pagesComboBox.Items.Add(cbnoneItem);
                    #endregion

                    #region Next Page 등록
                    int nextLayer = currentPageInfo.PageLayerIndex + 1;
                    TouchPageInfo nextPageInfo = pageInfoList.Where(o => o.PageLayerIndex.Equals(nextLayer) == true && o.ParentPageGuid.Equals(currentPageInfo.PageGuid) == true).FirstOrDefault();//순서는 일단 무시 차후 고려: && o.PageDisplayOrder == 1
                    if (nextPageInfo != null)
                    {
                        MovePageItem item = new MovePageItem(nextPageInfo.PageGuid, "[Next]");
                        ComboBoxItem cbitem = new ComboBoxItem();
                        cbitem.Content = item.PageName;
                        cbitem.Tag = item;
                        pagesComboBox.Items.Add(cbitem);
                    }
                    #endregion

                    #region Back Page 등록
                    int backLayer = currentPageInfo.PageLayerIndex - 1;
                    if (backLayer >= 0)
                    {
                        TouchPageInfo backPageInfo = pageInfoList.Where(o => o.PageLayerIndex.Equals(backLayer) == true && o.PageGuid.Equals(currentPageInfo.ParentPageGuid) == true).FirstOrDefault();
                        if (backPageInfo != null)
                        {
                            MovePageItem item = new MovePageItem(backPageInfo.PageGuid, "[Back]");
                            ComboBoxItem cbitem = new ComboBoxItem();
                            cbitem.Content = item.PageName;
                            cbitem.Tag = item;
                            pagesComboBox.Items.Add(cbitem);
                        }
                    }
                    #endregion

                    #region Root Page 등록
                    int rootLayer = 0;
                    TouchPageInfo rootPageInfo = pageInfoList.Where(o => o.PageLayerIndex.Equals(rootLayer) == true).FirstOrDefault();
                    if (rootPageInfo != null)
                    {
                        MovePageItem item = new MovePageItem(rootPageInfo.PageGuid, "[Main]");
                        ComboBoxItem cbitem = new ComboBoxItem();
                        cbitem.Content = item.PageName;
                        cbitem.Tag = item;
                        pagesComboBox.Items.Add(cbitem);
                    }
                    #endregion

                    #region Move Page 등록

                    foreach (TouchPageInfo pageinfo in pageInfoList)
                    {
                        MovePageItem item = new MovePageItem(pageinfo.PageGuid, pageinfo.PageNameTemp);
                        ComboBoxItem cbitem = new ComboBoxItem();
                        cbitem.Content = pageinfo.PageNameTemp;
                        cbitem.Tag = item;
                        pagesComboBox.Items.Add(cbitem);
                    }

                    #endregion
                }


                #endregion

                #region ShowHide Target Object 등록

                #region ShowHideType ComboBox 설정

                ComboBox showhideTypeComboBox = Get_TargetPagesPropertyItemEditor("ShowHideType", true) as ComboBox;
                if (showhideTypeComboBox != null)
                {
                    showhideTypeComboBox.SelectionChanged += (s, e) =>
                    {
                        ComboBox combobox = s as ComboBox;
                        if (combobox != null && combobox.SelectedItem != null)
                        {
                            ShowHideType type = (ShowHideType)combobox.SelectedItem;

                            IDesignElement designElement = _CurrentElement as IDesignElement;
                            if (designElement != null)
                            {
                                ShowHideEvent aeItem = designElement.ActionEvent as ShowHideEvent;
                                if (aeItem != null)
                                    aeItem.Type = type;
                            }
                        }
                    };
                }
                #endregion

                #region Target Control 설정

                ComboBox targetComboBox = Get_TargetPagesPropertyItemEditor("TargetObjects", true) as ComboBox;
                if (targetComboBox != null)
                {
                    targetComboBox.Items.Clear();
                    //pagesComboBox.SelectionChanged += new SelectionChangedEventHandler(ShowHideActionEventProperty_SelectionChanged);
                    targetComboBox.SelectionChanged += (s, e) =>
                    {
                        ComboBox combobox = s as ComboBox;
                        if (combobox != null)
                        {
                            ComboBoxItem item = combobox.SelectedItem as ComboBoxItem;
                            if (item != null)
                            {
                                TouchContentInfo contentItem = item.Tag as TouchContentInfo;
                                if (contentItem != null)
                                {
                                    IDesignElement designElement = _CurrentElement as IDesignElement;
                                    ShowHideEvent aeItem = designElement.ActionEvent as ShowHideEvent;
                                    if (aeItem != null)
                                        aeItem.SelectedItemName = contentItem.ContentName;
                                }
                            }
                        }
                    };
                #endregion

                    #region Page위의 Control 등록

                    List<TouchContentInfo> contents = new List<TouchContentInfo>();
                    foreach (TouchContentInfo contentInfo in currentPageInfo.Contents)
                    {
                        ComboBoxItem cbitem = new ComboBoxItem();
                        cbitem.Content = contentInfo.ContentName;
                        cbitem.Tag = contentInfo;
                        targetComboBox.Items.Add(cbitem);
                    }
                    #endregion //Page위의 Control 등록
                }
                #endregion //ShowHide Target Object 등록

                #region Hyperlink

                TextBox textbox_Hyperlink = Get_TargetPagesPropertyItemEditor("URL", true) as TextBox;
                if (textbox_Hyperlink != null)
                {
                    textbox_Hyperlink.TextChanged += (s, e) =>
                    {
                        IDesignElement designElement = _CurrentElement as IDesignElement;
                        HyperlinkEvent aeItem = designElement.ActionEvent as HyperlinkEvent;
                        if (aeItem != null)
                            aeItem.URL = textbox_Hyperlink.Text;
                    };
                }


                #endregion

                #region OpenFile

                UIEditor textbox_OpenFile = Get_TargetPagesPropertyItemEditor("File", true) as UIEditor;
                if (textbox_OpenFile != null)
                {
                    textbox_OpenFile.ChangedEditValueHandler = (editValue) =>
                    {
                        IDesignElement designElement = _CurrentElement as IDesignElement;
                        OpenFileEvent aeItem = designElement.ActionEvent as OpenFileEvent;
                        if (aeItem != null)
                            aeItem.OpenFile = textbox_OpenFile.EditValue as string;
                    };
                }

                #endregion
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private static void Set_ButtonShapeProperty()
        {
            ComboBox pagesComboBox = Get_TargetPagesPropertyItemEditor("HighlightStyle", true) as ComboBox;

            if (pagesComboBox != null)
            {
                pagesComboBox.ItemsSource = Enum.GetValues(typeof(Highlight));

                pagesComboBox.SelectionChanged += (s, e) =>
                {
                    ComboBox combobox = s as ComboBox;
                    if (combobox != null)
                    {
                        Highlight item = (Highlight)combobox.SelectedItem;
                        IDesignElement designElement = _CurrentElement as IDesignElement;
                        if (designElement != null)
                        {
                            designElement.HighlightStyle = (int)item;
                        }
                    }
                };               

                //#region Elliptical 등록                
                //ComboBoxItem cbnoneItem = new ComboBoxItem();
                //cbnoneItem.Content = "Diffuse";                
                //pagesComboBox.Items.Add(cbnoneItem);

                //cbnoneItem = new ComboBoxItem();
                //cbnoneItem.Content = "Elliptical";
                //pagesComboBox.Items.Add(cbnoneItem);
                //#endregion

                if (pagesComboBox.SelectedItem == null)
                    pagesComboBox.SelectedIndex = 0;
            }
        }

        private static void AddComboBoxItem(ActionEvent action, ComboBox combobox)
        {
            if (combobox != null)
            {
                ComboBoxItem comItem = new ComboBoxItem();
                comItem.Content = action.EventName;
                comItem.Tag = action;
                combobox.Items.Add(comItem);
            }
        }
        
        private static FrameworkElement Get_TargetPagesPropertyItemEditor(string propertyName, bool isShow)
        {
            FrameworkElement fe = GetControlEditorOfProperty(propertyName);

            if (fe != null)
            {
                if (isShow == true)
                    fe.Visibility = Visibility.Visible;
                else
                    fe.Visibility = Visibility.Collapsed;
            }

            return fe;
        }

        private static FrameworkElement Get_TargetPagesPropertyItemContent(string propertyName, bool isShow)
        {
            FrameworkElement fe = GetControlContentOfProperty(propertyName);

            if (fe != null)
            {
                if (isShow == true)
                    fe.Visibility = Visibility.Visible;
                else
                    fe.Visibility = Visibility.Collapsed;
            }

            return fe;
        }

        private static FrameworkElement SettingPropertyItemContainer(string propertyName, bool isShow)
        {
            if (_PropertyGrid == null)
                return null;
            FrameworkElement fe = GetPropertyItemContainer(propertyName);

            if (fe != null)
            {
                if (isShow == true)
                    fe.Visibility = Visibility.Visible;
                else
                    fe.Visibility = Visibility.Collapsed;
            }

            return fe;
        }

        private static void SetPropertyItemEventHandler(PropertyGrid propertyGrid, string categoryName, string propertyItem)
        {
            CategoryItem category = propertyGrid.Categories.OfType<CategoryItem>().Where(item => (item.Name == categoryName)).FirstOrDefault();

            if (category != null)
            {
                foreach (PropertyItem propItem in category.Items)
                {
                    if (propItem.PropertyLabel.Equals(propertyItem) == true || propItem.PropertyLabel.ToLower().Equals(propertyItem.ToLower()) == true)
                    {
                        propItem.PropertyChanged -= propItem_PropertyChanged;
                        propItem.PropertyChanged += new System.ComponentModel.PropertyChangedEventHandler(propItem_PropertyChanged);

                        break;
                    }
                }
            }
        }

        static void propItem_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            PropertyItem propertyItem = sender as PropertyItem;
        }

        public static void ShowPropertyCategoryItem(string categoryName, Visibility visibiliyType)
        {
            CategoryItem category = _PropertyGrid.Categories.OfType<CategoryItem>().Where(item => (item.Name == categoryName)).FirstOrDefault();

            ItemsControl categoriesList = _PropertyGrid.Template.FindName("PART_items", _PropertyGrid) as ItemsControl;
            ContentPresenter categoryPresenter = categoriesList.ItemContainerGenerator.ContainerFromItem(category) as ContentPresenter;
            if (categoryPresenter != null)
            {
                Expander categoryExpander = VisualTreeHelper.GetChild(categoryPresenter, 0) as Expander;
                categoryExpander.Visibility = visibiliyType;
            }
        }
          
        private static FrameworkElement GetPropertyItemContainer(string propertyName)
        {
            //_PropertyGrid.UpdateLayout();

            PropertyItem name = _PropertyGrid.Properties.OfType<PropertyItem>().Where(property => property.PropertyName.Equals(propertyName) == true).FirstOrDefault();
            if (name == null)
            {               
                return null;
            }

            FrameworkElement frameworkElement = _PropertyGrid.TryFindPropertyItemContainer(name);
            if (frameworkElement == null)
            {                
                return null;
            }

            return frameworkElement;
        }

        private static FrameworkElement GetControlEditorOfProperty(string propertyName)
        {
            if (_PropertyGrid == null)
                return null;
            PropertyItem name = _PropertyGrid.Properties.OfType<PropertyItem>().Where(property => property.PropertyName.Equals(propertyName) == true).FirstOrDefault();
            if (name == null)
            {               
                return null;
            }

            FrameworkElement frameworkElement = _PropertyGrid.TryFindEditor(name);
            if (frameworkElement == null)
            {               
                return null;
            }

            return frameworkElement;
        }

        public static FrameworkElement GetControlContentOfProperty(string propertyName)
        {
            if (_PropertyGrid != null)
            {
                _PropertyGrid.UpdateLayout();

                PropertyItem name = _PropertyGrid.Properties.OfType<PropertyItem>().Where(property => property.PropertyName.Equals(propertyName) == true).FirstOrDefault();
                if (name == null)
                {
                    return null;
                }

                FrameworkElement frameworkElement = _PropertyGrid.TryFindContent(name);
                if (frameworkElement == null)
                {
                    return null;
                }

                return frameworkElement;
            }
            return null;
        }

        private static void HidePropertyDisplayName(string propertyName)
        {
            if (_PropertyGrid != null)
            {
                //_PropertyGrid.UpdateLayout();

                PropertyItem propertyItem = _PropertyGrid.Properties.OfType<PropertyItem>().Where(property => property.PropertyName.Equals(propertyName) == true).FirstOrDefault();
                if (propertyItem == null)
                {
                    return;
                }
                _PropertyGrid.HidePropertyDisplayName(propertyItem);

               
            }
        }

        public static void Set_ContentElementControlIsEnableProperty(string propertyName, bool isEnable)
        {
            FrameworkElement frameworkElement = GetControlContentOfProperty(propertyName);
            
            if (frameworkElement != null)
                frameworkElement.IsEnabled = isEnable;
        }

        public static void SetVisibilityState_ControlContentOfProperty(string propertyName, bool isVisible)
        {
            FrameworkElement frameworkElement = GetPropertyItemContainer(propertyName);

            if (frameworkElement != null)
            {
                if(isVisible == true)
                    frameworkElement.Visibility = Visibility.Visible;
                else
                    frameworkElement.Visibility = Visibility.Collapsed;
            }
        }

        public static void SetPropertyGrid(object propertyGrid)
        {
            _PropertyGrid = propertyGrid as PropertyGrid;
        }

        public static PropertyGrid GetPropertyGrid()
        {
            return _PropertyGrid;
        }

        public static Dictionary<string, object> SerializePropertiesInIDesignElement(Dictionary<string, object> properties)
        {
            Dictionary<string, object> serializedProperties = new Dictionary<string, object>();
            foreach (string key in properties.Keys)
            {
                if (properties[key] == null)
                {
                    serializedProperties.Add(key, properties[key]);
                    continue;
                }

                switch (properties[key].GetType().Name)
                {
                    //string 형식으로 저장되지 않는 Type은 Xaml의 String 형식으로 저장한다.
                    case "SolidColorBrush":
                    case "BorderBrush":
                    case "LinearGradientBrush":
                    case "ImageBrush":
                    case "Brush":
                    case "FontFamily":
                    case "FontWeight":
                    case "Point":
                    case "ButtonStyle":
                        serializedProperties.Add(key, XamlWriter.Save(properties[key]));
                        break;
                    default:
                        serializedProperties.Add(key, properties[key]);
                        break;
                }
            }
            return serializedProperties;
        }

        public static Dictionary<string, object> DeserializePropertiesInIDesignElement(Dictionary<string, object> properties, string mediaPath)
        {
            Dictionary<string, object> deserializedProperties = new Dictionary<string, object>();

            foreach (string key in properties.Keys)
            {
                if (properties[key] == null)
                {
                    deserializedProperties.Add(key, properties[key]);
                    continue;
                }
                switch (properties[key].GetType().Name)
                {
                    case "String":
                        {
                            string tempKey = properties[key].ToString();
                            if (tempKey[0] == '<')
                            {
                                XElement element = XElement.Parse(tempKey);

                                switch (element.Name.LocalName)
                                {
                                    case "ImageBrush":
                                        {
                                            if (element.Attributes("ImageSource").Count() == 1)
                                            {
                                                element.Attribute("ImageSource").Value = mediaPath + System.IO.Path.GetFileName(element.Attribute("ImageSource").Value);
                                            }
                                            break;
                                        }
                                    default: break;
                                }

                                tempKey = element.ToString();
                                StringReader stringReader = new StringReader(tempKey);
                                XmlReader xmlReader = XmlReader.Create(stringReader);
                                deserializedProperties.Add(key, XamlReader.Load(xmlReader));
                            }
                            else
                            {
                                deserializedProperties.Add(key, tempKey);
                            }
                            break;
                        }
                    default:
                        {
                            deserializedProperties.Add(key, properties[key]);
                            break;
                        }
                }
            }

            return deserializedProperties;
        }

        public static UIElement GetNewInSysControl(Type inSysControlType, Dictionary<string, object> properties, ViewState viewState)
        {
            InSysControlType type = GetControlType(inSysControlType.Name);
            UIElement inSysControl = CreateInSysControlObject(type, viewState);
            IDesignElement designElement = inSysControl as IDesignElement;            
            if (designElement != null)
            {
                designElement.Properties = properties;
            }            

            return inSysControl;
        }

        public static UIElement GetNewSerializationInSysControl(Type inSysControlType, Dictionary<string, object> properties, ViewState viewState)
        {
            InSysControlType type = GetControlType(inSysControlType.Name);
            UIElement inSysControl = CreateInSysControlObject(type, viewState);
            IDesignElement designElement = inSysControl as IDesignElement;
            if (designElement != null)
            {
                foreach (string key in properties.Keys)
                {
                    try
                    {
                        switch (key)
                        {
                            case "BorderBrush":
                            case "Background":
                            case "FontFamily":
                            case "FontWeight":
                            case "Foreground":
                                PropertyClass.SetSerializationProperty(inSysControl, "IDesignElement", key, properties[key]);
                                break;
                            case "ButtonStyle":
                                break;
                            case "ButtonStyleKey":
                                {
                                    string buttonStyleKey = (string)properties[key];
                                    if (string.IsNullOrEmpty(buttonStyleKey) == false)
                                    {
                                        designElement.ButtonStyle = ButtonStyleLoader.ButtonStyleLoaderWindow.GetButtonStyle(buttonStyleKey);
                                        designElement.ButtonStyleKey = buttonStyleKey;
                                    }
                                    else
                                    {
                                        PropertyClass.SetSerializationProperty(inSysControl, "IDesignElement", key, properties[key]);
                                    }
                                }
                                break;
                            case "Text":
                                designElement.Content = properties[key];
                                break;
                            //case "Playlist":
                            //    PropertyClass.SetProperty(inSysControl, "IDesignElement", key, properties[key]);
                            //    break;
                            default:
                                PropertyClass.SetProperty(inSysControl, "IDesignElement", key, properties[key]);
                                break;
                        }
                    }
                    catch (Exception)
                    {
                        continue;
                    }
                }
            }

            return inSysControl;
        }

        public static UIElement GetNewInSysControl(Type inSysControlType, Dictionary<string, object> properties, string controlName, ViewState viewState)
        {
            InSysControlType type = GetControlType(inSysControlType.Name);
            UIElement inSysControl = CreateInSysControlObject(type, viewState);
            IDesignElement designElement = inSysControl as IDesignElement;
            if (designElement != null)
            {
                foreach (string key in properties.Keys)
                {
                    switch (key)
                    {
                        case "BorderBrush":
                        case "Background":
                        case "FontFamily":
                        case "FontWeight":
                        case "Foreground":
                        case "ButtonStyle":
                            PropertyClass.SetSerializationProperty(inSysControl, "IDesignElement", key, properties[key]);
                            break;
                        case "Text"://Content 가 Text인 Label, Text 류의 컨트롤 일 경우
                            designElement.Content = properties[key];
                            break;
                        case "Playlist":
                            List<PlaylistItem> playlist = properties[key] as List<PlaylistItem>;
                            if (playlist != null && playlist.Count > 0)
                            {
                                foreach(var playlistitem in playlist)
                                {
                                    PlaylistItem item = new PlaylistItem();
                                    item.Content = playlistitem.Content;
                                    item.CONTENTSVALUE = playlistitem.CONTENTSVALUE;
                                    item.Duration = playlistitem.Duration;
                                    //item.DurationAsTimeSpan = playlistitem.DurationAsTimeSpan;
                                    item.IsInfinity = playlistitem.IsInfinity;
                                    item.TIME = playlistitem.TIME;
                                    designElement.Playlist.Add(item);
                                }
                            }
                            
                            break;
                        case "Name"://
                            designElement.Name = controlName;
                            break;                      
                        default:
                            PropertyClass.SetProperty(inSysControl, "IDesignElement", key, properties[key]);
                            break;
                    }

                }
            }

            return inSysControl;
        }

        public static void CopyProperties(FrameworkElement inSysControl, FrameworkElement sourceControl)
        {
            IDesignElement designElement = inSysControl as IDesignElement;
            IDesignElement sourceDesignElement = sourceControl as IDesignElement;
            designElement.Properties.Clear();
            Dictionary<string, object> tempDic = new Dictionary<string, object>();
            if (designElement != null && sourceDesignElement != null)
            {
                foreach (var key in sourceDesignElement.Properties.Keys)
                {
                    designElement.Properties[key] = sourceDesignElement.Properties[key];
                    //designElement.Properties.Add(key, sourceDesignElement.Properties[key]);
                }

                
                //designElement.Properties = tempDic;
            }
        }

        public static void SetProperty(FrameworkElement inSysControl, FrameworkElement sourceControl, string propertyName)
        {
            Dictionary<string, object> desDic = new Dictionary<string, object>();
            IDesignElement designElement = inSysControl as IDesignElement;
            IDesignElement sourceDesignElement = sourceControl as IDesignElement;

            if (designElement != null && sourceDesignElement != null)
            {
                //designElement.Properties[propertyName] = sourceDesignElement.Properties[propertyName];
                if (propertyName.Equals("IsApplyLifeTime") == true)
                {
                    designElement.IsApplyLifeTime = sourceDesignElement.IsApplyLifeTime;                    
                }
                designElement = sourceDesignElement;
            }

        }

        public static void Init_ActionEvent()
        {
            #region GoToPage Target Page Property 초기화

            ComboBox pagesComboBox = Get_TargetPagesPropertyItemEditor("TargetPages", true) as ComboBox;
            if (pagesComboBox != null)
            {
                pagesComboBox.Items.Clear();
            }
          
            #endregion

            #region ShowHide Target Object 등록

            pagesComboBox = Get_TargetPagesPropertyItemEditor("TargetObjects", true) as ComboBox;
            if (pagesComboBox != null)
            {
                pagesComboBox.Items.Clear();
            }
            #endregion //ShowHide Target Object 등록

            #region Hyperlink

            TextBox textbox_Hyperlink = Get_TargetPagesPropertyItemEditor("URL", true) as TextBox;            
            if (textbox_Hyperlink != null)
            {
                textbox_Hyperlink.Text = "";
            }

            #endregion

            #region OpenFile

            UIEditor textbox_OpenFile = Get_TargetPagesPropertyItemEditor("File", true) as UIEditor;
            if (textbox_OpenFile != null)
            {
                textbox_OpenFile.EditValue = "";
            }

            #endregion
        }

        public static Dictionary<string, object> GetPropertyValue(FrameworkElement element)
        {
            IDesignElement designElement = element as IDesignElement;

            return designElement.Properties;
        }

        public static void SetPropertyValue(FrameworkElement element, string propertyName, out object propertyValue, object defaultValue)
        {
            IDesignElement designElement = element as IDesignElement;

            var valueTemp = designElement.Properties.Where(o => o.Key.Equals(propertyName) == true).Select(o => o.Value).SingleOrDefault();
            if (valueTemp != null)
                propertyValue = valueTemp;
            else
                propertyValue = defaultValue;
        }

        public static void LoadUserTouchEventFromXml(string touchProjectDirectory)
        {
            try
            {
                string userTouchXmlFilePath = string.Format("{0}\\{1}", touchProjectDirectory, "project_touchevent.xml");//ShowTouchEventEditor 함수에도 정의
                if (File.Exists(userTouchXmlFilePath) == true)
                {
                    XElement projectTouchEventXElement = XElement.Load(userTouchXmlFilePath);

                    LoadUserTouchEventFromXElement(projectTouchEventXElement);
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message + ">> \r\n" + ex.StackTrace);
            }
        }

        public static void LoadUserTouchEventFromXElement(XElement touchEventXElement)
        {
            g_UserTouchEvents = new List<UserTouchEvent>();
            foreach (var userEventXElement in touchEventXElement.Elements())
            {
                UserTouchEvent userTouchEvent = new UserTouchEvent();
                userTouchEvent.TouchEventName = userEventXElement.Attribute("name").Value;

                XElement actions = userEventXElement.Elements().FirstOrDefault();
                if (actions == null)
                    return;
                foreach (var actionXElement in actions.Elements())
                {
                    UserContentFunction userContentFunction = new UserContentFunction(actionXElement.Attribute("type").Value, actionXElement.Attribute("name").Value);
                    userContentFunction.OrderNo = int.Parse(actionXElement.Attribute("order").Value);
                    userContentFunction.TargetFullName = actionXElement.Attribute("targetname").Value;
                    userContentFunction.TargetFullPath = actionXElement.Attribute("targetpath").Value;
                    userContentFunction.ScreenName = actionXElement.Attribute("screenname").Value;
                    userContentFunction.ScreenID = actionXElement.Attribute("screenid").Value;
                    userContentFunction.ElementName = actionXElement.Attribute("elementname").Value;

                    foreach (var paramXElement in actionXElement.Elements())//지금은 하나의 parameter만 지원한다. 
                    {
                        TouchEventEditor.Model.ValueType paramValueType = (TouchEventEditor.Model.ValueType)Enum.Parse(typeof(TouchEventEditor.Model.ValueType), paramXElement.Attribute("type").Value, true);
                        userContentFunction.ValueType = paramValueType;
                        switch (paramValueType)
                        {
                            case TouchEventEditor.Model.ValueType.number_value:
                                userContentFunction.Value = double.Parse(paramXElement.Value);
                                break;
                            case TouchEventEditor.Model.ValueType.string_value:
                                userContentFunction.Value = paramXElement.Value;
                                break;
                            case TouchEventEditor.Model.ValueType.object_value:
                                XElement element = paramXElement.Elements().FirstOrDefault();
                                if (element != null)
                                {
                                    StringReader stringReader = new StringReader(element.ToString());
                                    System.Xml.XmlReader xmlReader = System.Xml.XmlReader.Create(stringReader);
                                    userContentFunction.Value = XamlReader.Load(xmlReader);
                                }
                                break;
                        }
                        break;
                    }

                    userTouchEvent.UserContentFunctionList.Add(userContentFunction);
                }

                g_UserTouchEvents.Add(userTouchEvent);
            }
        }

        public static UserTouchEvent GetUserTouchEvent(string touchEventName)
        {
            UserTouchEvent userTouchEvent = g_UserTouchEvents.Where(o => o.TouchEventName.Equals(touchEventName) == true || o.TouchEventName.ToLower().Equals(touchEventName.ToLower()) == true).FirstOrDefault();

            return userTouchEvent;
        }
    }
}
