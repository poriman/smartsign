﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
//using NLog;

namespace DigitalSignage.Controls
{
    public partial class WebControl : UserControl
    {
        public WebControl()
        {
            InitializeComponent();
			webBrowser1.ScriptErrorsSuppressed = false;
			this.Disposed += new EventHandler(WebControl_Disposed);
        }

        void WebControl_Disposed(object sender, EventArgs e)
        {
			Stop();
			webBrowser1.Dispose();
            this.Controls.Clear();
        }

        public void Navigate(Uri url)
        {
			
			webBrowser1.Navigate(url);
        }

		public void Zoom(int zoomfactor)
		{
			webBrowser1.Zoom(zoomfactor);
		}

		public void NavigateScript(string script)
		{
			try
			{
				String sTempFile = System.IO.Path.GetTempFileName();
				using (System.IO.FileStream stream = System.IO.File.OpenWrite(sTempFile))
				{
					if(!script.Contains("<body"))
					{
						script = String.Format("<body style=\"margin=0px\">{0}</body>", script);
					}

					byte[] arrstrings = Encoding.ASCII.GetBytes(script);

					stream.Write(arrstrings, 0, arrstrings.Length);
					stream.Close();

					stream.Dispose();
				}

				webBrowser1.Navigate(sTempFile);
			}
			catch (System.Exception e)
			{
				webBrowser1.Navigate("about:blank");
                throw e;
			}
		}

		public void Stop()
		{
			if (!IsDisposed && webBrowser1 != null && !webBrowser1.IsDisposed)
			{
				webBrowser1.Navigate(new Uri("about:blank"));
				webBrowser1.Stop();
			}
		}
    }
}
