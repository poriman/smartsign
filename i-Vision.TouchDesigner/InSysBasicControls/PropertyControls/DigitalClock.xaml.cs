﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace InSysBasicControls.PropertyControls
{
    /// <summary>
    /// Interaction logic for DigitalClock.xaml
    /// </summary>
    public partial class DigitalClock : UserControl
    {
        DispatcherTimer clockTimer;
        TimeSpan ts;
        public TimeZoneInfo gmt;

        public DigitalClock()
        {
            InitializeComponent();

            gmt = TimeZoneInfo.Local;
            clockTimer = new DispatcherTimer();
            clockTimer.Interval = new TimeSpan(0, 0, 1);
            clockTimer.Tick += new EventHandler(clockTimer_Tick);
        }

        public void Start()
        {
            UpdateClock();
            clockTimer.Start();
        }
        private void UpdateClock()
        {
            ts = new TimeSpan(DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second) - TimeZoneInfo.Local.BaseUtcOffset + gmt.BaseUtcOffset;
            TimeSpan ts2 = new TimeSpan(ts.Hours, ts.Minutes, ts.Seconds);
            timeLabel.Content = ts2;
        }

        void clockTimer_Tick(object sender, EventArgs e)
        {
            UpdateClock();
        }

        public void Stop()
        {
            clockTimer.Stop();
            timeLabel.Content = "00:00:00";
        }

        public FontFamily DigitalClockFontFamily
        {
            get { return this.timeLabel.FontFamily; }
            set { this.timeLabel.FontFamily = value; }
        }

        public double DigitalClockFontSize
        {
            get { return this.timeLabel.FontSize; }
            set { this.timeLabel.FontSize = value; }
        }

        public FontWeight DigitalClockFontWeight
        {
            get { return this.timeLabel.FontWeight; }
            set { this.timeLabel.FontWeight = value; }
        }

        public Brush DigitalClockForeground
        {
            get { return this.timeLabel.Foreground; }
            set { this.timeLabel.Foreground = value; }
        }

    }
}
