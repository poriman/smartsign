﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using InSysBasicControls.Interfaces;

namespace InSysBasicControls.PropertyControls
{
    /// <summary>
    /// Interaction logic for Clock.xaml
    /// </summary>
    public partial class Clock : UserControl, IDecoratable, IShape
    {
        private DispatcherTimer dt;

        #region GMT property defining
        public static readonly DependencyProperty GMTProperty = DependencyProperty.Register("GMT", typeof(int), typeof(Clock), new PropertyMetadata(GMTPropertyValueChanged));
        public int GMT
        {
            get
            {
                return (int)GetValue(GMTProperty);
            }

            set
            {
                SetValue(GMTProperty, value);
            }
        }
        private static void GMTPropertyValueChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            Clock dc = (Clock)d;
            dc.GMT = (int)e.NewValue;
        }
        #endregion

        public TimeZoneInfo gmt;

        public Path hHand, mHand, sHand;

        public Ellipse centerCircle;

        private bool _StrokesVisible;
       
        public Rectangle[] FaceStrokes
        {
            get
            {
                return (Rectangle[])strokeCanvas.Children.OfType<Rectangle>().ToArray();
            }
        }

        bool _Rectangular;

        public bool Rectangular
        {
            get
            {
                return _Rectangular;
            }
            set
            {
                _Rectangular = value;
                if (_Rectangular)
                {
                    clockBg.RadiusX = 0;
                    clockBg.RadiusY = 0;
                }

                else
                {
                    clockBg.RadiusX = 150;
                    clockBg.RadiusY = 150;
                }
            }
        }

        public bool StrokesVisible
        {
            get
            {
                return _StrokesVisible;
            }

            set
            {
                _StrokesVisible = value;
                if (_StrokesVisible)
                {
                    foreach (Rectangle stroke in strokeCanvas.Children)
                    {
                        strokeCanvas.Visibility = Visibility.Visible;
                    }
                }
                else
                {
                    foreach (Rectangle stroke in strokeCanvas.Children)
                    {
                        strokeCanvas.Visibility = Visibility.Hidden;
                    }
                }
            }
        }

        public Rectangle GetClockBg()
        {
            return clockBg;
        }

        public Clock()
        {
            InitializeComponent();

            _StrokesVisible = true;
            _Rectangular = false;
            // Insert code required on object creation below this point.
            dt = new DispatcherTimer();
            dt.Interval = new TimeSpan(0, 0, 1);
            dt.Tick += new EventHandler(dt_Tick);
            gmt = TimeZoneInfo.Local;
            // set the initial angle values so hands aren't all pointing up when we start
            SetTimeAngles();

            hHand = hourArrow;
            mHand = minuteArrow;
            sHand = secondArrow;
            centerCircle = circle;
        }

        public void Start()
        {
            dt.Start();
        }

        public void Stop()
        {
            dt.Stop();
        }

        void dt_Tick(object sender, EventArgs e)
        {
            SetTimeAngles();
        }

        public void SetTimeAngles()
        {
            int MinuteAngle = (int)DateTime.Now.Minute * 6;
            int DivMinute = MinuteAngle / 30;
            double FactorMinute = DivMinute * 2.5;
            this.Resources["dHourAsAngle"] = ((double)(DateTime.Now.Hour - TimeZoneInfo.Local.BaseUtcOffset.Hours + gmt.BaseUtcOffset.Hours) * 30) + FactorMinute;
            this.Resources["dMinuteAsAngle"] = (double)(DateTime.Now.Minute - TimeZoneInfo.Local.BaseUtcOffset.Minutes + gmt.BaseUtcOffset.Minutes) * 6;
            this.Resources["dSecondAsAngle"] = (double)DateTime.Now.Second * 6;
        }

        public void SetBorderBrush(Brush borderbrush)
        {
        }

        public void SetBorderThickness(Thickness borderThickness)
        {
        }

        public void SetBackround(Brush background)
        {
            clockBg.Fill = background;
        }

        public void SetCornerRadius(double cornerRadius)
        {
        }

        public Brush GetBorderBrush()
        {
            return null;
        }

        public Brush GetBackgound()
        {
            return clockBg.Fill;
        }

        public Thickness GetBorderThickness()
        {
            return new Thickness(0);
        }

        public double GetCornerRadius()
        {
            return 0;
        }

        public void SetStroke(Brush brush)
        {
            clockBg.Stroke = brush;
        }

        public void SetFill(Brush brush)
        {
            clockBg.Fill = brush;
        }

        public void SetStrokeDashArray(int[] sdarray)
        {
            clockBg.StrokeDashArray.Clear();
            for (int i = 0; i < sdarray.Count(); i++)
                clockBg.StrokeDashArray.Add(sdarray[i]);
        }

        public void SetStrokeDashCap(string strokedashcap)
        {
            switch (strokedashcap)
            {
                case "Flat":
                    {
                        clockBg.StrokeDashCap = PenLineCap.Flat;
                        break;
                    }
                case "Round":
                    {

                        clockBg.StrokeDashCap = PenLineCap.Round;
                        break;
                    }
                case "Triangle":
                    {
                        clockBg.StrokeDashCap = PenLineCap.Triangle;
                        break;
                    }
                case "Square":
                    {
                        clockBg.StrokeDashCap = PenLineCap.Square;
                        break;
                    }
                default: break;
            }
        }

        public void SetStrokeThickness(double strokethickness)
        {
            clockBg.StrokeThickness = strokethickness;
        }

        public Brush GetStroke()
        {
            return clockBg.Stroke;
        }

        public Brush GetFill()
        {
            return clockBg.Fill;
        }

        public int[] GetStrokeDashArray()
        {
            int[] sdarray = new int[clockBg.StrokeDashArray.Count];
            int i = 0;
            foreach (double d in clockBg.StrokeDashArray)
            {
                sdarray[i] = Convert.ToInt32(d);
                i++;
            }
            return sdarray;
        }

        public string GetStrokeDashCap()
        {
            return clockBg.StrokeDashCap.ToString();
        }

        public double GetStrokeThickness()
        {
            return clockBg.StrokeThickness;
        }

       
    }
}
