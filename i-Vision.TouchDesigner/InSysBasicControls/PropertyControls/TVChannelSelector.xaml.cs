﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Xml;
using System.IO;

namespace InSysBasicControls.PropertyControls
{
	/// <summary>
	/// TVChannelSelector.xaml에 대한 상호 작용 논리
	/// </summary>
	public partial class TVChannelSelector : Window
	{
		public TVChannelSelector()
		{
			InitializeComponent();
		}

		private string _returnvalue = "0;";

		public bool ISAPPLY = false;
		public string GetContent()
		{
			return _returnvalue;
		}

		/// <summary>
		/// XML에서 사용 가능한 Channel을 로드합니다.
		/// </summary>
		private void LoadChannelData()
		{
			ISAPPLY = false;

			string path = AppDomain.CurrentDomain.BaseDirectory + "settings\\ChannelList.xml";
			if (!File.Exists(path))
			{
                MessageBox.Show(String.Format(Cultures.DesignerResources.Resources.mbErrorTvDefinition, path), "Designer", MessageBoxButton.OK, MessageBoxImage.Error);
				return;
			}

			try
			{
				XmlDocument doc = new XmlDocument();
				doc.Load(path);

				XmlNodeList channel_nodelist = doc.SelectNodes("//child::Channel");

				if(channel_nodelist != null)
				{
					cbox_Channel.Items.Clear();
					foreach(XmlNode node in channel_nodelist)
					{
						cbox_Channel.Items.Add(node.Attributes["Name"].Value);
					}
					if(cbox_Channel.Items.Count > 0)
					{
						cbox_Channel.SelectedIndex = 0;
					}
				}
			}
			catch (Exception e)
			{
                MessageBox.Show(e.Message, Cultures.DesignerResources.Resources.titleTouchDesignerWindow);
			}
		}

		public void Load(string sContents)
		{
			LoadChannelData();

			try
			{
				string[] arrInfos = sContents.Split(';');

				int nIndex = 0;
				if (int.TryParse(arrInfos[0], out nIndex))
				{
					switch (nIndex)
					{
						case 0:
							rb_Television.IsChecked = true;
							break;
						case 1:
							rb_Composite.IsChecked = true;
							break;
						case 2:
							rb_SVideo.IsChecked = true;
							break;
					}
				}
				else { rb_Television.IsChecked = true; }

				cbox_Channel.SelectedItem = arrInfos[1];
				if (cbox_Channel.SelectedIndex < 0)
					cbox_Channel.SelectedIndex = 0;
			}
			catch 
			{
				rb_Television.IsChecked = true;
				if (cbox_Channel.SelectedIndex < 0)
					cbox_Channel.SelectedIndex = 0;
			}

			this.ShowDialog();
		}

		private void Fill()
		{
			string sReturn = "";
			if (rb_Television.IsChecked == true)
			{
				sReturn = "0;";
				sReturn += cbox_Channel.SelectedValue;
			}
			else if (rb_Composite.IsChecked == true)
			{
				sReturn = "1;NONE";
			}
			else
			{
				sReturn = "2;NONE";
			}


			this._returnvalue = sReturn;
			ISAPPLY = true;
		}

		private void okButton_Click(object sender, RoutedEventArgs e)
		{
			Fill();
			Close();
		}


		private void cancelButton_Click(object sender, RoutedEventArgs e)
		{
			Close();
		}

		private void Type_Checked(object sender, RoutedEventArgs e)
		{
			try
			{
				cbox_Channel.IsEnabled = rb_Television.IsChecked.Value;
			}
			catch
			{}
		}
	}
}
