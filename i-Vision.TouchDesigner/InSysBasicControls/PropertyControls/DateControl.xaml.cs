﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace InSysBasicControls.PropertyControls
{
    /// <summary>
    /// Interaction logic for DateControl.xaml
    /// </summary>
    public partial class DateControl : UserControl
    {
        DispatcherTimer dateTimer;

        public DateControl()
        {
            InitializeComponent();

            dateTimer = new DispatcherTimer();
            dateTimer.Interval = new TimeSpan(0, 1, 0);
            dateTimer.Tick += new EventHandler(dateTimer_Tick);
            dateLabel.Content = DateTime.Today.ToLongDateString();
        }

        void dateTimer_Tick(object sender, EventArgs e)
        {
            dateLabel.Content = DateTime.Today.ToLongDateString();
        }

        public void Start()
        {
            dateTimer.Start();
        }

        public void Stop()
        {
            dateTimer.Stop();
        }

        public FontFamily DateControlFontFamily
        {
            get { return this.dateLabel.FontFamily; }
            set { this.dateLabel.FontFamily = value; }
        }

        public double DateControlFontSize
        {
            get { return this.dateLabel.FontSize; }
            set { this.dateLabel.FontSize = value; }
        }

        public FontWeight DateControlFontWeight
        {
            get { return this.dateLabel.FontWeight; }
            set { this.dateLabel.FontWeight = value; }
        }

        public Brush DateControlForeground
        {
            get { return this.dateLabel.Foreground; }
            set { this.dateLabel.Foreground = value; }
        }
    }
}
