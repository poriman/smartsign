﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using InSysTouchflowData;

namespace InSysBasicControls.PropertyControls
{
    /// <summary>
    /// Interaction logic for AlignmentPropertyControl.xaml
    /// </summary>
    public partial class AlignmentPropertyControl : UserControl
    {
        public delegate void ChangedValueDelegate(Alignment editValue);
        public ChangedValueDelegate ChangedValueHandler;
       
        public AlignmentPropertyControl()
        {
            InitializeComponent();
          
        }

        public static readonly DependencyProperty AlignmentValueProperty = DependencyProperty.Register("AlignmentValue", typeof(Alignment), typeof(AlignmentPropertyControl), new FrameworkPropertyMetadata(Changed_AlignmentValue));
        public Alignment AlignmentValue
        {
            get { return (Alignment)GetValue(AlignmentValueProperty); }
            set { SetValue(AlignmentValueProperty, value); }
        }

        private static void Changed_AlignmentValue(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            
        }

        private void align_top_MouseDown(object sender, MouseButtonEventArgs e)
        {
            AlignmentValue = Alignment.Top;
            if (ChangedValueHandler != null)
                ChangedValueHandler(Alignment.Top);
        }

        private void align_vert_center_MouseDown(object sender, MouseButtonEventArgs e)
        {
            AlignmentValue = Alignment.VerticalCenter;
            if (ChangedValueHandler != null)
                ChangedValueHandler(Alignment.VerticalCenter);
        }

        private void align_bottom_MouseDown(object sender, MouseButtonEventArgs e)
        {
            AlignmentValue = Alignment.Bottom;
            if (ChangedValueHandler != null)
                ChangedValueHandler(Alignment.Bottom);
        }

        private void align_left_MouseDown(object sender, MouseButtonEventArgs e)
        {
            AlignmentValue = Alignment.Left;
            if (ChangedValueHandler != null)
                ChangedValueHandler(Alignment.Left);
        }

        private void align_horz_center_MouseDown(object sender, MouseButtonEventArgs e)
        {
            AlignmentValue = Alignment.HorizontalCenter;
            if (ChangedValueHandler != null)
                ChangedValueHandler(Alignment.HorizontalCenter);
        }

        private void align_right_MouseDown(object sender, MouseButtonEventArgs e)
        {
            AlignmentValue = Alignment.Right;
            if (ChangedValueHandler != null)
                ChangedValueHandler(Alignment.Right);
        }     
    }
}
