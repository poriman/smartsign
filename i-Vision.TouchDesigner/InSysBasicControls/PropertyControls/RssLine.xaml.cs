﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.Threading;
using System.Windows.Threading;
using System.Windows.Media.Animation;
using UtilLib.Etc;
using InSysTouchflowData;
using InSysBasicControls.Interfaces;

namespace InSysBasicControls.PropertyControls
{
    /// <summary>
    /// Interaction logic for RssLine.xaml
    /// </summary>
    public partial class RssLine : UserControl, ITextContainer
    {
        //private static Logger logger = LogManager.GetCurrentClassLogger();
        //        Brush background = Brushes.Transparent;
        private Thread load = null;
        private string feedUrl = "http://newsrss.bbc.co.uk/rss/newsonline_uk_edition/world/rss.xml";
        private RssFeed feed = null;
        private int nCurrIndex = 0;

        // scroll pseed (px per second)
        private int speed = 10;
        DispatcherTimer dt;
        ScrollTextDirection direction = ScrollTextDirection.RightToLeft;

        DoubleAnimation animation;
        TimeSpan duration;
        TranslateTransform transformation;

        // separators
        BitmapImage separatorImg;
        char separatorChar = '\t';

        // flags
        private Boolean state = false;

        private bool _isPlay = false;

        //	RSS Error Message Return Event
        public event EventHandler RssLoadErrorEvent;

        public RssLine()
        {
            InitializeComponent();

            animation = new DoubleAnimation();
            duration = new TimeSpan();
            Background = new SolidColorBrush(Color.FromArgb(0, 255, 255, 255));
        }

        public void Start(TimeSpan intervalOfRSSDataPulling)
        {
            //             if (state == false) // if not running yet
            //             {
            Stop();
            // 			}
            // loading feed

            if ((load == null) || (!load.IsAlive))
            {
                dt = new DispatcherTimer();
                dt.Interval = intervalOfRSSDataPulling;
                dt.Tick += new EventHandler(dt_Tick);
                dt.Start();
                load = new Thread(new ThreadStart(this.UpdateChannelContent));
                load.Name = "RSS load thread";
                load.Start();

                this.SizeChanged += new SizeChangedEventHandler(RssLine_SizeChanged);

                _isPlay = true;
            }
        }

        void RssLine_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            try
            {
                switch (direction)
                {
                    case ScrollTextDirection.LeftToRight:
                    case ScrollTextDirection.RightToLeft:
                        {
                            Canvas.SetTop(scrollingPanel, (rootcanvas.ActualHeight - scrollingPanel.ActualHeight) / 2.0);
                            break;
                        }
                }
            }
            catch { }
        }

        void dt_Tick(object sender, EventArgs e)
        {
            try
            {
                if (state == false) return;
                load = new Thread(new ThreadStart(this.UpdateChannelContent));
                load.Name = "RSS load thread";
                load.Start();
            }
            catch (Exception err)
            {
                throw err;
                //logger.Error(err + "");
            }
        }

        public void Stop()
        {
            state = false;
            try
            {

                scrollingLabel.Text = "";
                scrollingDescription.Text = "";

                if (animation != null)
                {
                    animation.Completed -= new EventHandler(animation_Completed);
                    animation = null;
                }
                animation = new DoubleAnimation();

                if (transformation != null)
                {
                    transformation.BeginAnimation(TranslateTransform.XProperty, null);
                    transformation.BeginAnimation(TranslateTransform.YProperty, null);
                }
            }
            catch (Exception ex)
            {
                throw ex;
                //logger.Error(ex + "");
            }

            if (load != null)
            {
                load.Abort();
                load = null;
            }

            if (dt != null)
            {
                dt.Stop();
                dt = null;
            }

            feed = null;

            this.SizeChanged -= new SizeChangedEventHandler(RssLine_SizeChanged);

            _isPlay = false;
        }

        TimeSpan CalculateDurationFromSpeed(int speed)
        {
            TimeSpan ts = new TimeSpan();

            switch (direction)
            {
                case ScrollTextDirection.BottomToTop:
                    {
                        scrollingLabel.TextWrapping = TextWrapping.Wrap;
                        scrollingLine.Visibility = Visibility.Hidden;
                        scrollingDescription.Visibility = Visibility.Visible;
                        scrollingLabel.UpdateLayout();

                        scrollingPanel.Width = rootcanvas.ActualWidth;
                        ts = new TimeSpan(0, 0, 0, 0, (int)(((scrollingPanel.ActualHeight + ActualHeight) / speed) * 1000));
                        // 						ts = new TimeSpan(0, 0, (int)Math.Ceiling((scrollingLabel.ActualHeight + ActualHeight) / speed));
                        break;
                    }
                case ScrollTextDirection.RightToLeft:
                    {
                        scrollingLabel.TextWrapping = TextWrapping.NoWrap;
                        scrollingLine.Visibility = Visibility.Collapsed;
                        scrollingDescription.Visibility = Visibility.Collapsed;
                        scrollingLabel.UpdateLayout();

                        scrollingPanel.Width = double.NaN;
                        ts = new TimeSpan(0, 0, 0, 0, (int)(((scrollingPanel.ActualWidth + ActualWidth) / speed) * 1000));
                        // 						ts = new TimeSpan(0, 0, (int)Math.Ceiling((scrollingLabel.ActualWidth + ActualWidth) / speed));
                        break;
                    }
                case ScrollTextDirection.LeftToRight:
                    {
                        scrollingLabel.TextWrapping = TextWrapping.NoWrap;
                        scrollingLine.Visibility = Visibility.Collapsed;
                        scrollingDescription.Visibility = Visibility.Collapsed;
                        scrollingLabel.UpdateLayout();

                        scrollingPanel.Width = double.NaN;
                        ts = new TimeSpan(0, 0, 0, 0, (int)(((scrollingPanel.ActualWidth + ActualWidth) / speed) * 1000));
                        // 						ts = new TimeSpan(0, 0, (int)Math.Ceiling((scrollingLabel.ActualWidth + ActualWidth) / speed));
                        break;
                    }
                case ScrollTextDirection.TopToBottom:
                    {
                        scrollingLabel.TextWrapping = TextWrapping.Wrap;
                        scrollingLine.Visibility = Visibility.Hidden;
                        scrollingDescription.Visibility = Visibility.Visible;
                        scrollingLabel.UpdateLayout();

                        scrollingPanel.Width = rootcanvas.ActualWidth;
                        ts = new TimeSpan(0, 0, 0, 0, (int)(((scrollingPanel.ActualHeight + ActualHeight) / speed) * 1000));
                        // 						ts = new TimeSpan(0, 0, (int)Math.Ceiling((scrollingLabel.ActualHeight + ActualHeight) / speed));
                        break;
                    }
                default: break;
            }
            return ts;
        }

        private void prepareLine()
        {
            if (feed == null || feed.Items.Count == 0)
                return;

            string sLine = "";
            string sDescription = "";
            try
            {
                sLine = feed.Items[nCurrIndex].Title;

                if (direction == ScrollTextDirection.BottomToTop ||
                    direction == ScrollTextDirection.TopToBottom)
                {
                    sDescription = feed.Items[nCurrIndex].Description;
                }
                nCurrIndex++;
            }
            catch
            {
                nCurrIndex = 0;
                sLine = feed.Items[nCurrIndex].Title;

                if (direction == ScrollTextDirection.BottomToTop ||
                    direction == ScrollTextDirection.TopToBottom)
                {
                    sDescription = feed.Items[nCurrIndex].Description;
                }
                nCurrIndex++;
            }

            string sDescritionWithoutTAG = "";
            if (!String.IsNullOrEmpty(sDescription))
            {
                bool bTagging = false;
                foreach (char ch in sDescription.ToCharArray())
                {
                    if (ch.Equals('<'))
                    {
                        bTagging = true;
                    }
                    else if (ch.Equals('>'))
                    {
                        bTagging = false;
                    }
                    else
                    {
                        if (!bTagging)
                        {
                            sDescritionWithoutTAG += ch;
                        }
                    }
                }
            }

            scrollingLabel.Text = sLine;
            scrollingDescription.Text = sDescritionWithoutTAG;
            scrollingLabel.UpdateLayout();
            scrollingDescription.UpdateLayout();
        }

        private void runAnimation()
        {
            if (!_isPlay) return;

            prepareLine();

            if (String.IsNullOrEmpty(scrollingLabel.Text))
                return;

            duration = CalculateDurationFromSpeed(speed);

            scrollingPanel.UpdateLayout();

            animation = new DoubleAnimation();
            transformation = new TranslateTransform();
            animation.Duration = new Duration(duration);
            animation.Completed += new EventHandler(animation_Completed);

            //	hsshin 이렇게 해제 안하면 메모리 릭이 난다!!!
            if (scrollingPanel.RenderTransform as TranslateTransform != null)
            {
                scrollingPanel.RenderTransform.BeginAnimation(TranslateTransform.XProperty, null);
                scrollingPanel.RenderTransform.BeginAnimation(TranslateTransform.YProperty, null);
                scrollingPanel.RenderTransform = null;
            }

            scrollingPanel.RenderTransform = transformation;

            if (!_isPlay) return;

            switch (direction)
            {
                case ScrollTextDirection.LeftToRight:
                    {
                        Canvas.SetTop(scrollingPanel, (rootcanvas.ActualHeight - scrollingPanel.ActualHeight) / 2.0);

                        animation.From = -scrollingPanel.ActualWidth;
                        animation.To = ActualWidth;
                        transformation.BeginAnimation(TranslateTransform.XProperty, animation);
                        break;
                    }
                case ScrollTextDirection.RightToLeft:
                    {
                        Canvas.SetTop(scrollingPanel, (rootcanvas.ActualHeight - scrollingPanel.ActualHeight) / 2.0);

                        animation.From = ActualWidth;
                        animation.To = -scrollingPanel.ActualWidth;
                        transformation.BeginAnimation(TranslateTransform.XProperty, animation);
                        break;
                    }
                case ScrollTextDirection.TopToBottom:
                    {
                        Canvas.SetTop(scrollingPanel, 0);

                        animation.From = -scrollingPanel.ActualHeight;
                        animation.To = ActualHeight;
                        transformation.BeginAnimation(TranslateTransform.YProperty, animation);
                        break;
                    }
                case ScrollTextDirection.BottomToTop:
                    {
                        Canvas.SetTop(scrollingPanel, 0);

                        animation.From = ActualHeight;
                        animation.To = -scrollingPanel.ActualHeight;
                        transformation.BeginAnimation(TranslateTransform.YProperty, animation);
                        break;
                    }
                default: break;
            }
        }

        #region network & async routines
        delegate void realStartCB(Object nul);
        private void realStart(Object nul)
        {
            try
            {
                if (!this.Dispatcher.CheckAccess())
                {
                    this.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                       new realStartCB(realStart), null);
                }
                else
                {
                    runAnimation();
                }
            }
            catch (Exception e)
            {
                throw e;
                //logger.Error(e + "");
            }
        }

        void animation_Completed(object sender, EventArgs e)
        {
            if (state == false) return;

            animation.Completed -= new EventHandler(animation_Completed);

            runAnimation();
        }

        void UpdateChannelContent()
        {
            int nRetry = 0;
            String sErrorMessage = "";

            do
            {
                try
                {
                    RssReader rssReader = new RssReader();
                    rssReader.RdfMode = false;

                    RssFeed newFeed = rssReader.Retrieve(feedUrl);
                    if (newFeed.ErrorMessage == null || newFeed.ErrorMessage == "")
                    {
                        feed = newFeed;
                        realStart(null);
                        state = true;
                    }
                    else
                    {
                        sErrorMessage = newFeed.ErrorMessage;
                        state = false;
                        nRetry++;
                    }
                }
                catch (Exception e)
                {
                    sErrorMessage = e.Message;
                    //logger.Error(e + "");
                    //	0.5초를 쉬어 준다.
                    Thread.Sleep(500);
                    nRetry++;
                }
            }
            while (state == false && nRetry < 3);	//	3번 재시도를 한다.

            if (!state && RssLoadErrorEvent != null) RssLoadErrorEvent(sErrorMessage, null);

            return;
        }

        #endregion

        #region ITextContainer Members

        void ITextContainer.SetFontSize(double fontsize)
        {
            scrollingLabel.SetValue(TextBlock.FontSizeProperty, fontsize);
        }

        void ITextContainer.SetFontBrush(Brush fontcolor)
        {
            scrollingLabel.Foreground = fontcolor;
        }

        double ITextContainer.GetFontSize()
        {
            return (double)scrollingLabel.GetValue(TextBlock.FontSizeProperty);
        }

        Brush ITextContainer.GetFontBrush()
        {
            return scrollingLabel.Foreground;
        }

        FontFamily ITextContainer.GetFontFamily()
        {
            return (FontFamily)scrollingLabel.GetValue(TextBlock.FontFamilyProperty);
        }

        void ITextContainer.SetFontFamily(FontFamily family)
        {
            scrollingLabel.SetValue(TextBlock.FontFamilyProperty, family);
        }

        FontWeight ITextContainer.GetFontWeight()
        {
            return scrollingLabel.FontWeight;
        }

        void ITextContainer.SetFontWeight(FontWeight fontWeight)
        {
            scrollingLabel.FontWeight = fontWeight;
        }

        void ITextContainer.SetDescFontSize(double fontsize)
        {
            scrollingDescription.SetValue(TextBlock.FontSizeProperty, fontsize);
        }

        void ITextContainer.SetDescFontBrush(Brush fontcolor)
        {
            scrollingDescription.Foreground = fontcolor;
        }

        double ITextContainer.GetDescFontSize()
        {
            return (double)scrollingDescription.GetValue(TextBlock.FontSizeProperty);
        }

        Brush ITextContainer.GetDescFontBrush()
        {
            return scrollingDescription.Foreground;
        }

        FontFamily ITextContainer.GetDescFontFamily()
        {
            return (FontFamily)scrollingDescription.GetValue(TextBlock.FontFamilyProperty);
        }

        void ITextContainer.SetDescFontFamily(FontFamily family)
        {
            scrollingDescription.SetValue(TextBlock.FontFamilyProperty, family);
        }

        FontWeight ITextContainer.GetDescFontWeight()
        {
            return scrollingDescription.FontWeight;
        }

        void ITextContainer.SetDescFontWeight(FontWeight fontWeight)
        {
            scrollingDescription.FontWeight = fontWeight;
        }
        #endregion

        #region properties
        public ScrollTextDirection Direction
        {
            get { return direction; }
            set
            {
                direction = value;
            }
        }

        public String FeedUrl
        {
            get
            {
                return feedUrl;
            }
            set
            {
                feedUrl = value;
            }
        }

        public BitmapImage SeparatorImg
        {
            get { return separatorImg; }
            set
            {
                separatorChar = '\0';
                separatorImg = value;
            }
        }

        public char SeparatorChar
        {
            get { return separatorChar; }
            set
            {
                separatorImg = null;
                separatorChar = value;
            }
        }
        public int Speed
        {
            get { return speed; }
            set
            {
                if (value > 0)
                    speed = value;
                else value = 40;
            }
        }

        public FontStyle TBFontStyle
        {
            set
            {
                scrollingLabel.FontStyle = value;
                scrollingLabel.UpdateLayout();

            }
            get { return scrollingLabel.FontStyle; }
        }

        public FontWeight TBFontWeight
        {
            set
            {
                ((ITextContainer)this).SetFontWeight(value);
                scrollingLabel.UpdateLayout();
            }
            get { return scrollingLabel.FontWeight; }
        }

        public FontStyle TBDescFontStyle
        {
            set
            {
                scrollingDescription.FontStyle = value;
                scrollingDescription.UpdateLayout();

            }
            get { return scrollingDescription.FontStyle; }
        }

        public FontWeight TBDescFontWeight
        {
            set
            {
                ((ITextContainer)this).SetDescFontWeight(value);
                scrollingDescription.UpdateLayout();
            }
            get { return scrollingDescription.FontWeight; }
        }
        #endregion

        #region Event handlers

        private void runningLine_Loaded(object sender, RoutedEventArgs e)
        {
            /*
            frontLine = string1;
            backLine = string2;
            frontTransform = new TranslateTransform();
            frontAnimation = new DoubleAnimation();
            frontAnimation.Completed += new EventHandler(string1Animation_Completed);
            frontLine.RenderTransform = frontTransform;

            backTransform= new TranslateTransform();
            backAnimation = new DoubleAnimation();
            backAnimation.Completed += new EventHandler(string1Animation_Completed);
            backLine.RenderTransform = backTransform;*/
        }
        #endregion
    }
}
