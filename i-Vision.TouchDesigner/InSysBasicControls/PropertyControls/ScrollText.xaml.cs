﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;
using InSysTouchflowData;
using InSysBasicControls.Interfaces;

namespace InSysBasicControls.PropertyControls
{
    /// <summary>
    /// Interaction logic for ScrollText.xaml
    /// </summary>
    public partial class ScrollText : UserControl, ITextContainer
    {
        DoubleAnimation animation;
        TimeSpan duration;
        TranslateTransform transformation;
        ScrollTextDirection direction;
        int speed;
        ScrollTextInfo _info;
        object contents;
        public ScrollTextInfo Info
        {
            get
            {
                return _info;
            }
        }

        public bool IsStartWhenLoading { get; set; }//이 화면에 부모에 Loaded 될 때 바로 실행하기 위한 플래그. (Loaded 되기전에 Start 가 호출되는 것을 방지하기 플래그.)

        /// <summary>
        /// ScrollText 생성잔
        /// </summary>
        public ScrollText()
        {
            InitializeComponent();
            animation = new DoubleAnimation();
            duration = new TimeSpan();
            Background = new SolidColorBrush(Color.FromArgb(0, 255, 255, 255));
            IsStartWhenLoading = false;
        }

        /// <summary>
        /// Loads the scrolling text component parameters
        /// </summary>
        /// <param name="stInfo">Information that will be scrolling</param>
        public void Load(ScrollTextInfo stInfo)
        {
            contents = stInfo.content;
            scrollingLabel.Text = contents.ToString();
            this.direction = stInfo.direction;
            UpdateLayout();
            duration = CalculateDurationFromSpeed(stInfo.speed);
            speed = stInfo.speed;

            scrollingLabel.Background = stInfo.background;
            scrollingLabel.Foreground = stInfo.foreground;
            scrollingLabel.FontWeight = stInfo.FontWeight;
            scrollingLabel.FontStyle = stInfo.FontStyle;
            scrollingLabel.FontSize = stInfo.FontSize;
            scrollingLabel.FontFamily = stInfo.FontFamily;
            _info = stInfo;

            this.SizeChanged += new SizeChangedEventHandler(ScrollText_SizeChanged);

            scrollingLabel.UpdateLayout();

            this.Loaded += (s, e) =>
            {
                if (IsStartWhenLoading == true)
                {
                    this.Start();
                    IsStartWhenLoading = false;
                }
            };

            
        }

        void ScrollText_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            try
            {
                switch (direction)
                {
                    case ScrollTextDirection.LeftToRight:
                    case ScrollTextDirection.RightToLeft:
                        {
                            Canvas.SetTop(scrollingLabel, (rootCanvas.ActualHeight - scrollingLabel.ActualHeight) / 2.0);
                            break;
                        }
                }
            }
            catch { }
        }

        /// <summary>
        /// Scrolling Speed를 결정하기위한 Duration을 계산하여 반환한다.
        /// </summary>
        /// <param name="speed">sec</param>
        /// <returns>Duration</returns>
        TimeSpan CalculateDurationFromSpeed(int speed)
        {
            TimeSpan ts = new TimeSpan();

            switch (direction)
            {
                case ScrollTextDirection.BottomToTop:
                case ScrollTextDirection.TopToBottom:
                    {
                        scrollingLabel.TextWrapping = TextWrapping.Wrap;
                        scrollingLabel.Width = rootCanvas.ActualWidth;
                        scrollingLabel.UpdateLayout();
                        ts = new TimeSpan(0, 0, 0, 0, (int)(((scrollingLabel.ActualHeight + ActualHeight) / speed) * 1000));
                        break;
                    }
                case ScrollTextDirection.LeftToRight:
                case ScrollTextDirection.RightToLeft:
                    {
                        scrollingLabel.TextWrapping = TextWrapping.NoWrap;
                        scrollingLabel.Width = double.NaN;
                        scrollingLabel.UpdateLayout();
                        ts = new TimeSpan(0, 0, 0, 0, (int)(((scrollingLabel.ActualWidth + ActualWidth) / speed) * 1000));
                        break;
                    }
                default: break;
            }
            return ts;
        }

        /// <summary>
        /// Starts the text scrolling
        /// </summary>
        public void Start()
        {
            scrollingLabel.UpdateLayout();
            try
            {
                duration = CalculateDurationFromSpeed(speed);

                animation = new DoubleAnimation();
                transformation = new TranslateTransform();
                animation.Duration = new Duration(duration);
                animation.SpeedRatio = 1.0;
                animation.RepeatBehavior = RepeatBehavior.Forever;
                animation.By = 200.0;

                //	hsshin 이렇게 해제 안하면 메모리 릭이 난다!!!
                if (scrollingLabel.RenderTransform as TranslateTransform != null)
                {
                    scrollingLabel.RenderTransform.BeginAnimation(TranslateTransform.XProperty, null);
                    scrollingLabel.RenderTransform.BeginAnimation(TranslateTransform.YProperty, null);
                    scrollingLabel.RenderTransform = null;
                }
                scrollingLabel.RenderTransform = transformation;

                switch (direction)
                {
                    case ScrollTextDirection.LeftToRight:
                        {
                            Canvas.SetTop(scrollingLabel, (rootCanvas.ActualHeight - scrollingLabel.ActualHeight) / 2.0);

                            animation.From = -scrollingLabel.ActualWidth;
                            animation.To = ActualWidth;
                            transformation.BeginAnimation(TranslateTransform.XProperty, animation);
                            break;
                        }
                    case ScrollTextDirection.RightToLeft:
                        {
                            Canvas.SetTop(scrollingLabel, (rootCanvas.ActualHeight - scrollingLabel.ActualHeight) / 2.0);

                            animation.From = ActualWidth;
                            animation.To = -scrollingLabel.ActualWidth;
                            transformation.BeginAnimation(TranslateTransform.XProperty, animation);
                            break;
                        }
                    case ScrollTextDirection.TopToBottom:
                        {
                            Canvas.SetTop(scrollingLabel, 0);

                            animation.From = -scrollingLabel.ActualHeight;
                            animation.To = ActualHeight;
                            transformation.BeginAnimation(TranslateTransform.YProperty, animation);
                            break;
                        }
                    case ScrollTextDirection.BottomToTop:
                        {
                            Canvas.SetTop(scrollingLabel, 0);

                            animation.From = ActualHeight;
                            animation.To = -scrollingLabel.ActualHeight;
                            transformation.BeginAnimation(TranslateTransform.YProperty, animation);
                            break;
                        }
                    default: break;
                }
            }
            catch
            {

            }
        }
        /// <summary>
        /// Stops the text scrolling
        /// </summary>
        public void Stop()
        {
            try
            {
                scrollingLabel.Text = null;
                animation = new DoubleAnimation();
                animation.Duration = new Duration(new TimeSpan(0, 0, 0));
                transformation.BeginAnimation(TranslateTransform.XProperty, animation);
                transformation.BeginAnimation(TranslateTransform.YProperty, animation);

                IsStartWhenLoading = false;
            }
            catch { }
        }

        public FontStyle TBFontStyle
        {
            set
            {
                scrollingLabel.FontStyle = value;
                _info.FontStyle = value;
                scrollingLabel.UpdateLayout();
            }
            get { return scrollingLabel.FontStyle; }
        }

        public FontWeight TBFontWeight
        {
            set
            {
                scrollingLabel.FontWeight = value;
                _info.FontWeight = value;
                scrollingLabel.UpdateLayout();
            }
            get { return scrollingLabel.FontWeight; }
        }

        #region ITextContainer Members

        void ITextContainer.SetFontSize(double fontsize)
        {
            scrollingLabel.SetValue(TextBlock.FontSizeProperty, fontsize);
        }

        void ITextContainer.SetFontBrush(Brush fontcolor)
        {
            scrollingLabel.Foreground = fontcolor;
        }

        double ITextContainer.GetFontSize()
        {
            return (double)scrollingLabel.GetValue(TextBlock.FontSizeProperty);
        }

        Brush ITextContainer.GetFontBrush()
        {
            return scrollingLabel.Foreground;
        }

        FontFamily ITextContainer.GetFontFamily()
        {
            return (FontFamily)scrollingLabel.GetValue(TextBlock.FontFamilyProperty);
        }

        void ITextContainer.SetFontFamily(FontFamily family)
        {
            scrollingLabel.SetValue(TextBlock.FontFamilyProperty, family);
        }

        FontWeight ITextContainer.GetFontWeight()
        {
            return scrollingLabel.FontWeight;
        }

        void ITextContainer.SetFontWeight(FontWeight fontWeight)
        {
            scrollingLabel.FontWeight = fontWeight;
        }


        void ITextContainer.SetDescFontSize(double fontsize)
        {
            throw new NotImplementedException();
        }

        void ITextContainer.SetDescFontBrush(Brush fontcolor)
        {
            throw new NotImplementedException();
        }

        double ITextContainer.GetDescFontSize()
        {
            throw new NotImplementedException();
        }

        Brush ITextContainer.GetDescFontBrush()
        {
            throw new NotImplementedException();
        }

        FontFamily ITextContainer.GetDescFontFamily()
        {
            throw new NotImplementedException();
        }

        void ITextContainer.SetDescFontFamily(FontFamily family)
        {
            throw new NotImplementedException();
        }

        FontWeight ITextContainer.GetDescFontWeight()
        {
            throw new NotImplementedException();
        }

        void ITextContainer.SetDescFontWeight(FontWeight fontWeight)
        {
            throw new NotImplementedException();
        }
        #endregion

    }
}
