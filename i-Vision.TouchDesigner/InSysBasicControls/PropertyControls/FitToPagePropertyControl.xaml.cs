﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using InSysTouchflowData;

namespace InSysBasicControls.PropertyControls
{
    /// <summary>
    /// Interaction logic for FitToPagePropertyControl.xaml
    /// </summary>
    public partial class FitToPagePropertyControl : UserControl
    {
        public delegate void ChangedValueDelegate(FitToPage editValue);
        public ChangedValueDelegate ChangedValueHandler;

        public FitToPagePropertyControl()
        {
            InitializeComponent();
        }

        public static readonly DependencyProperty FitToPageValueProperty = DependencyProperty.Register("FitToPageValue", typeof(FitToPage), typeof(FitToPagePropertyControl), new FrameworkPropertyMetadata(Changed_FitToPageValue));
        public FitToPage FitToPageValue
        {
            get { return (FitToPage)GetValue(FitToPageValueProperty); }
            set { SetValue(FitToPageValueProperty, value); }
        }

        private static void Changed_FitToPageValue(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
           
        }

        private void fittopage_full_MouseDown(object sender, MouseButtonEventArgs e)
        {
             FitToPageValue = FitToPage.Full;
            if (ChangedValueHandler != null)
                ChangedValueHandler(FitToPage.Full);
        }

        private void fittopage_horizontal_MouseDown(object sender, MouseButtonEventArgs e)
        {
            FitToPageValue = FitToPage.Horizontal;
            if (ChangedValueHandler != null)
                ChangedValueHandler(FitToPage.Horizontal);
        }

        private void fittopage_vertical_MouseDown(object sender, MouseButtonEventArgs e)
        {
            FitToPageValue = FitToPage.Vertical;
            if (ChangedValueHandler != null)
                ChangedValueHandler(FitToPage.Vertical);
        }
    }
}
