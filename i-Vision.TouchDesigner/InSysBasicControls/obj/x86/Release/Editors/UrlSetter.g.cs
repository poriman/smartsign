﻿#pragma checksum "..\..\..\..\Editors\UrlSetter.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "E510A0EEADA719DF1EF95B27EDA0B9F7"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.269
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace InSysBasicControls.Editors {
    
    
    /// <summary>
    /// UrlSetter
    /// </summary>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
    public partial class UrlSetter : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 26 "..\..\..\..\Editors\UrlSetter.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton rbAddress;
        
        #line default
        #line hidden
        
        
        #line 27 "..\..\..\..\Editors\UrlSetter.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton rbFile;
        
        #line default
        #line hidden
        
        
        #line 28 "..\..\..\..\Editors\UrlSetter.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton rbScript;
        
        #line default
        #line hidden
        
        
        #line 30 "..\..\..\..\Editors\UrlSetter.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel stackPanel1;
        
        #line default
        #line hidden
        
        
        #line 32 "..\..\..\..\Editors\UrlSetter.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox adressTextBox;
        
        #line default
        #line hidden
        
        
        #line 33 "..\..\..\..\Editors\UrlSetter.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button navigateButton;
        
        #line default
        #line hidden
        
        
        #line 35 "..\..\..\..\Editors\UrlSetter.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Forms.Integration.WindowsFormsHost host;
        
        #line default
        #line hidden
        
        
        #line 36 "..\..\..\..\Editors\UrlSetter.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button okButton;
        
        #line default
        #line hidden
        
        
        #line 37 "..\..\..\..\Editors\UrlSetter.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button cancelButton;
        
        #line default
        #line hidden
        
        
        #line 38 "..\..\..\..\Editors\UrlSetter.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button applyButton;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/TouchDesigner.InSysBasicControls;component/editors/urlsetter.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\Editors\UrlSetter.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.rbAddress = ((System.Windows.Controls.RadioButton)(target));
            return;
            case 2:
            this.rbFile = ((System.Windows.Controls.RadioButton)(target));
            
            #line 27 "..\..\..\..\Editors\UrlSetter.xaml"
            this.rbFile.Checked += new System.Windows.RoutedEventHandler(this.rbFile_Checked);
            
            #line default
            #line hidden
            return;
            case 3:
            this.rbScript = ((System.Windows.Controls.RadioButton)(target));
            return;
            case 4:
            this.stackPanel1 = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 5:
            this.adressTextBox = ((System.Windows.Controls.TextBox)(target));
            
            #line 32 "..\..\..\..\Editors\UrlSetter.xaml"
            this.adressTextBox.GotKeyboardFocus += new System.Windows.Input.KeyboardFocusChangedEventHandler(this.tb_GotKeyboardFocus);
            
            #line default
            #line hidden
            return;
            case 6:
            this.navigateButton = ((System.Windows.Controls.Button)(target));
            
            #line 33 "..\..\..\..\Editors\UrlSetter.xaml"
            this.navigateButton.Click += new System.Windows.RoutedEventHandler(this.navigateButton_Click);
            
            #line default
            #line hidden
            return;
            case 7:
            this.host = ((System.Windows.Forms.Integration.WindowsFormsHost)(target));
            return;
            case 8:
            this.okButton = ((System.Windows.Controls.Button)(target));
            
            #line 36 "..\..\..\..\Editors\UrlSetter.xaml"
            this.okButton.Click += new System.Windows.RoutedEventHandler(this.okButton_Click);
            
            #line default
            #line hidden
            return;
            case 9:
            this.cancelButton = ((System.Windows.Controls.Button)(target));
            
            #line 37 "..\..\..\..\Editors\UrlSetter.xaml"
            this.cancelButton.Click += new System.Windows.RoutedEventHandler(this.cancelButton_Click);
            
            #line default
            #line hidden
            return;
            case 10:
            this.applyButton = ((System.Windows.Controls.Button)(target));
            
            #line 38 "..\..\..\..\Editors\UrlSetter.xaml"
            this.applyButton.Click += new System.Windows.RoutedEventHandler(this.applyButton_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

