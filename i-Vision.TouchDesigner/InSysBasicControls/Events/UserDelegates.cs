﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using InSysBasicControls.Interfaces;
using System.Windows;

namespace InSysBasicControls.Events
{
    public class UserDelegates
    {
        public delegate void ShowInSysControlPlayerEditorDelegate(IDesignElement designElementInterface);
        public delegate void EventSettingInSysControlDelegate();
        public delegate void UpdatedItemPropertyDelegate(FrameworkElement element, string propertyName, object propertyValue);
    }
}
