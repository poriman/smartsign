﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace InSysBasicControls.Events
{
    public class ElementEventArgs : EventArgs
    {
        private UIElement _element;
        public UIElement Element { get {return _element;} }

        private object _property;
        public object Property { get { return _property; } }

        public ElementEventArgs(UIElement element, object property)
        {
            _element = element;
            _property = property;
        }
    }
}
