﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.ComponentModel;
using InSysTouchflowData;
using InSysBasicControls.InSysProperties;
using InSysBasicControls.Interfaces;
using InSysBasicControls.Commons;
using InSysBasicControls.Events;
using UtilLib.IO;
using InSysTouchflowData.Models.ActionEvents;
using UtilLib.StaticMethod;
using System.Windows.Media.Animation;
using System.Diagnostics;
using System.Windows.Threading;

namespace InSysBasicControls.Views
{
     
    /// <summary>
    /// Interaction logic for InSysSlideViewer.xaml
    /// </summary>
    public partial class InSysSlideViewer : UserControl, IDesignElement, ISelectable, IElementProperties, INotifyPropertyChanged
    {
        /// <summary>
        /// Represents the playlist of this element
        /// </summary>
        //private List<PlaylistItem> _playlist;
        private bool _IsPlay;
        ContentPlayer player;
        private double navigationButtonWidth = 80;
        
        /// <summary>
        /// Represents the set of changable properties
        /// </summary>
        Dictionary<string, object> _properties;

        private ElementPropertyObject _elementProperties;

        private bool isPlaylistItemsTemp;
        private bool isPlaylistItems;
        public bool IsPlaylistItems
        {
            get { return isPlaylistItems; }
            set 
            { 
                isPlaylistItems = value;
                OnPropertyChanged("IsPlaylistItems");
            }
        }

        public IDesignElement GetElement
        {
            get
            {
                return this as IDesignElement;
            }
        }

        #region Controls 정의

        //private Rectangle imgBrushRect;
        //private ImageBrush imgBrush;
        //private TextBlock tbName;

        #endregion

        #region Events & Deleagets

        //Show InSysSlideViewer Control Editor 델리게이트
        private UserDelegates.ShowInSysControlPlayerEditorDelegate ShowInSysControlPlayerEditorHandler = null;
        private UserDelegates.EventSettingInSysControlDelegate EventSettingInSysControlHandler;
        private UserDelegates.UpdatedItemPropertyDelegate UpdatedItemPropertyHandler;

        private ActionEvent _ActionEvent;

        void IDesignElement.SetActionEvent(object actionEvent)
        {
            this._ActionEvent = actionEvent as ActionEvent;
        }

        #endregion

        /// <summary>
        /// Initializes the new ImageComponent class instance
        /// </summary>
        public InSysSlideViewer()
        {
            InitializeComponent();
            InitInSysElementProperties();
            CreateContentControl();
            this.DataContext = this;

            _IsPlay = false;
            ((IDesignElement)this).IsSupportPlaylist = true;
            ((IDesignElement)this).InitProperties();
            this.SizeChanged += new SizeChangedEventHandler(ImageComponent_SizeChanged);

            this._TouchProjectTimer.Interval = TimeSpan.FromMilliseconds(1);
            this._TouchProjectTimer.Tick += new EventHandler(TouchProjectTimer_Tick);
        }

        private void CreateContentControl()
        {            
        }        
        
        #region Events & Deleagets 구현

        void ImageComponent_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            IDesignElement designElement = this as IDesignElement;
            if (designElement != null)
            {
                //if (e.NewSize.Width - (this.navigationButtonWidth * 2) <= 0)
                //    return;
                designElement.ResizePlaylistPreviewListBox(e.NewSize.Width, e.NewSize.Height);

                designElement.SlideViewWidth = e.NewSize.Width;// -(this.navigationButtonWidth * 2);
                designElement.SlideViewHeight = e.NewSize.Height;

                this.SetNaviButtonSize(this.navigationButtonWidth);
                
                this.rectangleGeometry.Rect = new Rect(0, 0, designElement.SlideViewWidth, designElement.SlideViewHeight);
                foreach (var image in PanelHost.Children)
                {
                    Image img = image as Image;
                    if (img != null)
                    {
                        img.Width = designElement.SlideViewWidth;
                        img.Height = designElement.SlideViewHeight;
                    }
                }
            }
        }

        private void SetNaviButtonSize(double width)
        {
            this.BackNaviButton.Width = width;
            this.NextNaviButton.Width = width;
        }

        private void ResizeSlideViewContents(double width, double heigth)
        {
        }

        private DispatcherTimer _TouchProjectTimer = new DispatcherTimer();
        private Stopwatch _Stopwatch = new Stopwatch();
       
        protected override void OnPreviewMouseDown(MouseButtonEventArgs e)
        {
            base.OnPreviewMouseDown(e);

            if (_IsPlay == true)
            {

                _TouchProjectTimer.Start();
                _Stopwatch.Restart();

                BackNaviButton.Opacity = 0.5;
                NextNaviButton.Opacity = 0.5;
            }
        }

        void TouchProjectTimer_Tick(object sender, EventArgs e)
        {
            DispatcherTimer dispatcherTimer = sender as DispatcherTimer;
            if (_IsPlay == true && dispatcherTimer != null)
            {
                if (this._Stopwatch.Elapsed >= TimeSpan.FromSeconds(10))
                {
                    BackNaviButton.Opacity = 0.1;
                    NextNaviButton.Opacity = 0.1;
                    dispatcherTimer.Stop();
                }
            }
        }

        #region OnMouseDoubleClick 함수
        protected override void OnMouseDoubleClick(MouseButtonEventArgs e)
        {
            base.OnMouseDoubleClick(e);

            if (ShowInSysControlPlayerEditorHandler != null)
            {
                ShowInSysControlPlayerEditorHandler(this as IDesignElement);

                //if (UpdatedItemPropertyHandler != null)
                //    UpdatedItemPropertyHandler(this, "Playlist", (this as IDesignElement).Playlist);
            }            
        }

        #endregion

        #region Set Delegate 콜백 함수

        void IDesignElement.SetDelegate(Delegate handler)
        {
            if (handler.GetType().Equals(typeof(UserDelegates.EventSettingInSysControlDelegate)) == true)
            {
                this.EventSettingInSysControlHandler = handler as UserDelegates.EventSettingInSysControlDelegate;
            }
            else if (handler.GetType().Equals(typeof(UserDelegates.ShowInSysControlPlayerEditorDelegate)) == true)
            {
                this.ShowInSysControlPlayerEditorHandler = handler as UserDelegates.ShowInSysControlPlayerEditorDelegate;
            }
            else if (handler.GetType().Equals(typeof(UserDelegates.UpdatedItemPropertyDelegate)) == true)
            {
                this.UpdatedItemPropertyHandler = handler as UserDelegates.UpdatedItemPropertyDelegate;
            } 
        }

        #endregion

        #endregion

        #region Interface 정의

        #region IElementProperties

        public ElementPropertyObject ElementProperties
        {
            get { return _elementProperties; }
            set { _elementProperties = value; }
        }

        public void InitInSysElementProperties()
        {
            _elementProperties = new ElementPropertyObject();
            this._elementProperties.Playlist = new List<PlaylistItem>();
           
            this._elementProperties.SlideViewWidth = 400;//초기는 15% 70% 15%
            this._elementProperties.SlideViewHeight = 300;
            this._elementProperties.Width = this._elementProperties.SlideViewWidth + (navigationButtonWidth * 2);
            this._elementProperties.Height = this._elementProperties.SlideViewHeight;
            this._elementProperties.X = 0;
            this._elementProperties.Y = 0;
            //this._elementProperties.ZIndex = 0;
            this._elementProperties.Opacity = 1;
            this._elementProperties.StartTime = TimeSpan.Zero;
            this._elementProperties.EndTime = TimeSpan.Zero;
            this._elementProperties.Name = "";
            this._elementProperties.BorderBrush = new SolidColorBrush(Colors.Transparent);
            this._elementProperties.Alignment = Alignment.None;
            this._elementProperties.FitToPage = FitToPage.None;
            this._elementProperties.Stretch = Stretch.Fill;            
        }

        #endregion

        #region IsSelected Property

        public bool IsSelected
        {
            get { return (bool)GetValue(IsSelectedProperty); }
            set { SetValue(IsSelectedProperty, value); }
        }
       
        public static readonly DependencyProperty IsSelectedProperty =
          DependencyProperty.Register("IsSelected",
                                       typeof(bool),
                                       typeof(InSysSlideViewer),
                                       new FrameworkPropertyMetadata(false));

        #endregion


        #region IDesignElement Members     

        RaiseActionEventDelegate IDesignElement.RaiseActionEventHandler
        {
            get;
            set;
        }

        /// <summary>
        /// Touch Event
        /// </summary>
        RaiseDoContentMouseClickTouchEventDelegate IDesignElement.RaiseDoContentMouseClickTouchEventHandler
        {
            get;
            set;
        }

        Dictionary<string, string> IDesignElement.TouchEvent { get; set; } //Touch Event
        string IDesignElement.EventName { get; set; } //Touch Event

        ActionEvent IDesignElement.ActionEvent
        {
            get { return this._ActionEvent; }
            set { this._ActionEvent = value; }
        }

        MediaType IDesignElement.MediaType
        {
            get { return MediaType.Image; }
        }

        InSysControlType IDesignElement.InSysControlType
        {
            get { return InSysControlType.InSysSlideViewer; }
        }

        List<PlaylistItem> IDesignElement.Playlist
        {
            get
            {
                return this._elementProperties.Playlist;
            }
            set
            {
                this._elementProperties.Playlist = value;

                PanelHost.Children.Clear();
                if (this._elementProperties.Playlist.Count > 0)
                {
                    foreach (var playlistitem in this._elementProperties.Playlist)
                    {
                        Image image = new Image();
                        image.Source = new BitmapImage(new Uri(playlistitem.Content, UriKind.RelativeOrAbsolute));
                        image.Stretch = Stretch.Fill;
                        image.Width = this.ElementProperties.SlideViewWidth;
                        image.Height = this.ElementProperties.Height;

                        PanelHost.Children.Add(image);
                    }
                }
            }
        }

        ImageSource IDesignElement.ThumnailImageSource { get; set; }
        
        void IDesignElement.UpdatePlaylistItems()
        {
            IDesignElement designElement = this as IDesignElement;
            if (this._elementProperties.Playlist != null && designElement != null)
            {
                //this.playlistPreviewListBox.ItemsSource = null;
                List<PlaylistListBoxItemData> listboxItems = new List<PlaylistListBoxItemData>();
                foreach (var temp in this.playlistPreviewListBox.Items)
                    listboxItems.Add(temp as PlaylistListBoxItemData);

                int i = 0;
                List<PlaylistListBoxItemData> palylistItemDataList = new List<PlaylistListBoxItemData>();

                if (this._elementProperties.Playlist.Count >= 1)
                {
                    foreach (var item in this._elementProperties.Playlist)
                    {
                        PlaylistItem playlistitem = item as PlaylistItem;
                        //Image image = new Image();
                        PlaylistListBoxItemData data;
                        var sameitem = listboxItems.Where(o => o.ImagePath.Equals(playlistitem.Content) == true || o.ImagePath.ToLower().Equals(playlistitem.Content.ToLower()) == true).FirstOrDefault();

                        if (sameitem == null)
                        {
                            designElement.ThumnailImageSource = ObjectConverters.GetImageSource(playlistitem.Content, UriKind.Absolute);
                            //image.Source = new BitmapImage(new Uri(playlistitem.Content, UriKind.RelativeOrAbsolute));
                            if (i == 0)
                            {
                                data = new PlaylistListBoxItemData(designElement.ThumnailImageSource, 0, 0, this._elementProperties.Width, this._elementProperties.Height, playlistitem.Content);
                            }
                            else
                            {
                                data = new PlaylistListBoxItemData(designElement.ThumnailImageSource, 0, 0, this._elementProperties.Width, this._elementProperties.Height, playlistitem.Content);
                            }
                            palylistItemDataList.Add(data);
                        }
                        else
                        {
                            palylistItemDataList.Add(sameitem);
                        }
                        i++;
                        break;// 하나의 썸네일 이미지만 추출한다.
                    }
                    this.IsPlaylistItems = true;
                }
                else
                {
                    this.IsPlaylistItems = false;
                }
                this.playlistPreviewListBox.ItemsSource = palylistItemDataList;
            }
        }
                
        void IDesignElement.ResizePlaylistPreviewListBox(double width, double height)
        {
            foreach (var item in this.playlistPreviewListBox.Items)
            {
                PlaylistListBoxItemData data = item as PlaylistListBoxItemData;
                data.Width = width;
                data.Height = height;
            }
        }

        bool IDesignElement.IsSupportPlaylist { get; set; }

        void IDesignElement.Play()
        {
            this.isPlaylistItemsTemp = this.IsPlaylistItems;//Play 상태에서는 무조건 디폴트 사진은 안보이도록 한다.
            this.IsPlaylistItems = true;
            
            IDesignElement designElement = this as IDesignElement;
            if (designElement != null)
            {
                if (!_IsPlay && designElement.IsInLifeTime == true)
                {
                    _IsPlay = true;
                    this.rect_line.Visibility = System.Windows.Visibility.Collapsed;
                    this.sp_DisplayText.Visibility = Visibility.Collapsed;
                    this.playlistPreviewListBox.Visibility = System.Windows.Visibility.Collapsed;
                    this.playlistPreviewBorder.Visibility = System.Windows.Visibility.Collapsed;
                    BackNaviButton.Opacity = 0.1;
                    NextNaviButton.Opacity = 0.1;

                    player = new ContentPlayer(this as IDesignElement);
                    player.Start();
                }
            }
        }

        bool IDesignElement.ISPlay
        {
            get { return _IsPlay; }
        }

        void IDesignElement.Stop()
        {
            if (player != null)
                player.Stop();

            this.rect_line.Visibility = System.Windows.Visibility.Visible;
            this.sp_DisplayText.Visibility = Visibility.Visible;
            this.PanelHost.Children.Clear();//            imgBrush.ImageSource = null;
            this.playlistPreviewListBox.Visibility = System.Windows.Visibility.Visible;
            this.playlistPreviewBorder.Visibility = System.Windows.Visibility.Visible;
            
            _IsPlay = false;
            this.IsPlaylistItems = this.isPlaylistItemsTemp;//Stop 상태에서는 Play전의 값으로 재설정한다.
            //GC.Collect();
        }

        void IDesignElement.RePlay()
        {
            if (this._IsPlay == true)
                (this as IDesignElement).Stop();

            (this as IDesignElement).Play();
        }
      

        Dictionary<string, object> IDesignElement.Properties
        {
            get
            {
                if (_properties == null)
                    ((IDesignElement)this).InitProperties();
                return _properties;
            }
            set
            {                
                _properties = value;

                //foreach (string name in properties.Keys)
                //{
                //    PropertyClass.SetProperty(this, "IDesignElement", name, properties[name]);                    
                //}
            }
        }

        double IDesignElement.ControlTimeDuration
        {
            get;
            set;
        }

        void IDesignElement.InitProperties()
        {
            _properties = new Dictionary<string, object>();
            IDesignElement child = this as IDesignElement;

            _properties.Add("Name", child.Name);
            _properties.Add("Width", child.Width);
            _properties.Add("Height", child.Height);
            _properties.Add("X", child.X);
            _properties.Add("Y", child.Y);
            _properties.Add("HorizontalAlignment", child.HorizontalAlignment);
            _properties.Add("VerticalAlignment", child.VerticalAlignment);
            _properties.Add("Opacity", child.Opacity);
            _properties.Add("ZIndex", child.ZIndex);
            //_properties.Add("BorderBrush", child.BorderBrush);
            _properties.Add("BorderBrush", System.Windows.Markup.XamlWriter.Save(child.BorderBrush));
            _properties.Add("BorderThickness", child.BorderThickness);
            _properties.Add("BorderCorner", child.BorderCorner);
            _properties.Add("Stretch", child.Stretch);
            _properties.Add("Type", child.Type);
            _properties.Add("Playlist", child.Playlist);
            _properties.Add("ControlTimeDuration", child.ControlTimeDuration);
            _properties.Add("IsApplyLifeTime", child.IsApplyLifeTime);           
            _properties.Add("StartTime", child.StartTime);
            _properties.Add("EndTime", child.EndTime);
            _properties.Add("Alignment", child.Alignment);
            _properties.Add("FitToPage", child.FitToPage);
            _properties.Add("SlideViewHeight", child.SlideViewHeight);
            _properties.Add("SlideViewWidth", child.SlideViewWidth);
        }

        string IDesignElement.Name
        {
            get
            {
                return this._elementProperties.Name;
            }
            set
            {
                this._elementProperties.Name = value;
            }
        }

        double IDesignElement.Width
        {
            get
            {
                return this._elementProperties.Width;
            }
            set
            {
                this._elementProperties.Width = value;
                this.ElementProperties.SlideViewWidth = this._elementProperties.Width;// -(this.navigationButtonWidth * 2);               

                this.SetNaviButtonSize(this.navigationButtonWidth);
            }
        }

        double IDesignElement.Height
        {
            get
            {
                //return ActualHeight;
                return this._elementProperties.Height;
            }
            set
            {
                //Height = value;
                this._elementProperties.Height = value;
            }
        }

        double IDesignElement.SlideViewWidth
        {
            get
            {
                return this._elementProperties.SlideViewWidth;
            }
            set
            {
                this._elementProperties.SlideViewWidth = value;
            }
        }

        double IDesignElement.SlideViewHeight
        {
            get
            {
                return this._elementProperties.SlideViewHeight;
            }
            set
            {
                this._elementProperties.SlideViewHeight = value;
            }
        }

        double IDesignElement.X
        {
            get
            {
                //return (double)this.GetValue(Canvas.LeftProperty);
                return this._elementProperties.X;
            }
            set
            {
                //this.SetValue(Canvas.LeftProperty, value);
                this._elementProperties.X = value;
            }
        }

        double IDesignElement.Y
        {
            get
            {
                //return (double)this.GetValue(Canvas.TopProperty);
                return this._elementProperties.Y;
            }
            set
            {
                //SetValue(Canvas.TopProperty, value);
                this._elementProperties.Y = value;
            }
        }

        double IDesignElement.Bottom
        {
            get
            {
                return (double)this.GetValue(Canvas.BottomProperty);
            }
            set
            {
                this.SetValue(Canvas.BottomProperty, value);
            }
        }

        double IDesignElement.Right
        {
            get
            {
                return (double)this.GetValue(Canvas.RightProperty);
            }
            set
            {
                SetValue(Canvas.RightProperty, value);
            }
        }

        int IDesignElement.ZIndex
        {
            get
            {
                //return (int)this.GetValue(Canvas.ZIndexProperty);
                return this._elementProperties.ZIndex;
            }
            set
            {
                //this.SetValue(Canvas.ZIndexProperty, value);               
                this.ElementProperties.ZIndex = value;
            }
        }

        Brush IDesignElement.BorderBrush
        {
            get
            {
                return (this._elementProperties as ElementPropertyObject).BorderBrush;
            }
            set
            {
                (this._elementProperties as ElementPropertyObject).BorderBrush = value;
            }
        }

        double IDesignElement.BorderThickness
        {
            get
            {
                return (this._elementProperties as ElementPropertyObject).BorderThickness;
            }
            set
            {
                (this._elementProperties as ElementPropertyObject).BorderThickness = value;
            }
        }            

        double IDesignElement.BorderCorner
        {
            get
            {
                return (this._elementProperties as ElementPropertyObject).BorderCorner;
            }
            set
            {
                (this._elementProperties as ElementPropertyObject).BorderCorner = value;
            }
        }

        int IDesignElement.HighlightStyle
        {
            get;
            set;
        }

        int IDesignElement.HighlightBrightness
        {
            get;
            set;
        }

        Style IDesignElement.ButtonStyle { get; set; }

        string IDesignElement.ButtonStyleKey { get; set; }

        Visibility IDesignElement.Visibility
        {
            get
            {
                return this._elementProperties.Visibility;
            }
            set
            {
                this._elementProperties.Visibility = value;
            }
        }

        private Visibility lifeTimeVisibility;
        public Visibility LifeTimeVisibility
        {
            get
            {
                return lifeTimeVisibility;
            }
            set
            {
                this.lifeTimeVisibility = value;
                OnPropertyChanged("LifeTimeVisibility");
            }
        }    

        double IDesignElement.Opacity
        {
            get
            {
                //return this.Opacity;
                return this._elementProperties.Opacity;
            }
            set
            {
                //this.Opacity = value;
                this._elementProperties.Opacity = value;
            }
        }

        Stretch IDesignElement.Stretch
        {
            get
            {
                //return imgBrush.Stretch;
                return (this._elementProperties as ElementPropertyObject).Stretch;
            }
            set
            {
                //imgBrush.Stretch = value;
                (this._elementProperties as ElementPropertyObject).Stretch = value;
			}
        }

        FitToPage IDesignElement.FitToPage
        {
            get { return this._elementProperties.FitToPage; }
            set { this._elementProperties.FitToPage = value; }

        }

        Alignment IDesignElement.Alignment
        {
            get
            {
                //return this.HorizontalAlignment;
                return this._elementProperties.Alignment;
            }
            set
            {
                //this.HorizontalAlignment = value;
                this._elementProperties.Alignment = value;
            }
        }

        HorizontalAlignment IDesignElement.HorizontalAlignment
        {
            get
            {
                //return this.HorizontalAlignment;
                return this._elementProperties.HorizontalAlignment;
            }
            set
            {
                //this.HorizontalAlignment = value;
                this._elementProperties.HorizontalAlignment = value;
            }
        }

        VerticalAlignment IDesignElement.VerticalAlignment
        {
            get
            {
                //return this.VerticalAlignment;
                return this._elementProperties.VerticalAlignment;
            }
            set
            {
                //this.VerticalAlignment = value;
                this._elementProperties.VerticalAlignment = value;
            }
        }

        bool IDesignElement.IsApplyLifeTime
        {
            get { return this._elementProperties.IsApplyLifeTime; }
            set { this._elementProperties.IsApplyLifeTime = value; }
        }

        bool IDesignElement.IsInLifeTime
        {
            get;
            set;
        }

        TimeSpan IDesignElement.StartTime
        {
            get { return this._elementProperties.StartTime; }
            set { this._elementProperties.StartTime = value; }
        }

        TimeSpan IDesignElement.EndTime
        {
            get { return this._elementProperties.EndTime; }
            set { this._elementProperties.EndTime = value; }
        }

        private bool userVisibilitySet = false;
        bool IDesignElement.UserVisibilitySet
        {
            get { return userVisibilitySet; }
            set { userVisibilitySet = value; }
        }
      
        void IDesignElement.CheckLifeTime(TimeSpan playTimeSpan)
        {
            IDesignElement designeElement = this as IDesignElement;
            if (designeElement != null)
            {
                if (designeElement.IsApplyLifeTime == true)
                {
                    if (playTimeSpan >= designeElement.StartTime && playTimeSpan <= designeElement.EndTime)
                    {
                        if (designeElement.Visibility == System.Windows.Visibility.Visible)
                        {
                            if (this.Visibility != System.Windows.Visibility.Visible)
                                this.Visibility = System.Windows.Visibility.Visible;
                            if (designeElement.LifeTimeVisibility != System.Windows.Visibility.Visible)
                                designeElement.LifeTimeVisibility = System.Windows.Visibility.Visible;
                        }                       
                        if (designeElement.IsInLifeTime == false)
                            designeElement.IsInLifeTime = true;

                        if (_IsPlay == false)
                            designeElement.Play();
                    }
                    else
                    {
                        if (this.Visibility != System.Windows.Visibility.Hidden)
                            this.Visibility = System.Windows.Visibility.Hidden;
                        if (designeElement.LifeTimeVisibility != System.Windows.Visibility.Hidden)
                            designeElement.LifeTimeVisibility = System.Windows.Visibility.Hidden;
                        designeElement.IsInLifeTime = false;
                        if (designeElement.IsInLifeTime == true)
                            designeElement.IsInLifeTime = false;
                        if (_IsPlay == true)
                            designeElement.Stop();
                    }
                }
                else
                {
                    if (designeElement.Visibility == System.Windows.Visibility.Visible)
                    {
                        if (this.Visibility != System.Windows.Visibility.Visible)
                            this.Visibility = System.Windows.Visibility.Visible;
                        if (designeElement.LifeTimeVisibility != System.Windows.Visibility.Visible)
                            designeElement.LifeTimeVisibility = System.Windows.Visibility.Visible;                       
                    }
                    if (designeElement.IsInLifeTime == false)
                        designeElement.IsInLifeTime = true;

                    if (_IsPlay == false)
                        designeElement.Play();
                }
            }
        }

        #region No used Interface Members

        double IDesignElement.Volume
        {
            get;
            set;
        }

        bool IDesignElement.Mute
        {
            get;
            set;
        }

        TimeSpan IDesignElement.RefreshInterval
        {
            get;
            set;
        }

        Brush IDesignElement.Background
        {
            get;
            set;
        }

        FontFamily IDesignElement.FontFamily
        {
            get;
            set;
        }

        double IDesignElement.FontSize
        {
            get;
            set;
        }

        FontWeight IDesignElement.FontWeight
        {
            get;
            set;
        }

        TextWrapping IDesignElement.TextWrapping { get; set; }

        bool IDesignElement.Multiline { get; set; }

        Brush IDesignElement.Foreground
        {
            get;
            set;
        }

        int[] IDesignElement.StrokesLength
        {
            get;
            set;
        }

        PenLineCap IDesignElement.StrokeDashCap
        {
            get;
            set;
        }

        char IDesignElement.SeparatorChar
        {
            get;
            set;
        }

        Point IDesignElement.AspectRatio
        {
            get;
            set;
        }

        System.Drawing.Bitmap IDesignElement.GetThumbnail(int cx, int cy)
        {
            return null;
        }

        #endregion

        Type IDesignElement.Type
        {
            get
            {
                return GetType();
            }
        }
       
        object IDesignElement.Content
        {
			get
			{
                if (imgBrush != null && imgBrush.ImageSource != null)
                    return ((BitmapImage)imgBrush.ImageSource).UriSource.AbsolutePath as string;
                else
                    return null;
			}
            set
            {
                BitmapImage bmpImage = new BitmapImage();
				
				if ((string)value == "")
				{
                    this.sp_DisplayText.Visibility = Visibility.Visible;
                    imgBrush.ImageSource = null;
					return;
				}

                if (IOMethod.ExistsFile(value as string) == true)
                {
                    bmpImage.DecodePixelHeight = Convert.ToInt32(imgBrushRect.ActualHeight);
                    bmpImage.DecodePixelWidth = Convert.ToInt32(imgBrushRect.ActualWidth);

                    bmpImage.BeginInit();
                    bmpImage.UriSource = new Uri(value as string, UriKind.RelativeOrAbsolute);
                    bmpImage.EndInit();

                    //imgBrush.ImageSource = bmpImage;
                    imgBrushRect.Visibility = Visibility.Hidden;
                }
            }
        }

        IMediaFilesManager IDesignElement.FileManager
        {
            get { return Parent as IMediaFilesManager; }
		}
        		
		#endregion

        #endregion       
               
        #region TestPlayImage Test

        private void TestPlayImage()
        {            
            PlaylistItem temp = new PlaylistItem();
            temp.Content = "testSamples/car001.jpg";
            temp.CONTENTSVALUE = "testSamples/car001.jpg";
            temp.Duration = "<TimeSpan xmlns=\"clr-namespace:System;assembly=mscorlib\">00:00:3</TimeSpan>";
            temp.TIME = new TimeSpan(0, 0, 3);
            this._elementProperties.Playlist.Add(temp);

            temp = new PlaylistItem();
            temp.Content = "testSamples/car002.jpg";
            temp.CONTENTSVALUE = "testSamples/car002.jpg";
            temp.Duration = "<TimeSpan xmlns=\"clr-namespace:System;assembly=mscorlib\">00:00:3</TimeSpan>";
            temp.TIME = new TimeSpan(0, 0, 3);
            this._elementProperties.Playlist.Add(temp);

            temp = new PlaylistItem();
            temp.Content = "testSamples/car003.jpg";
            temp.CONTENTSVALUE = "testSamples/car003.jpg";
            temp.Duration = "<TimeSpan xmlns=\"clr-namespace:System;assembly=mscorlib\">00:00:3</TimeSpan>";
            temp.TIME = new TimeSpan(0, 0, 3);

            this._elementProperties.Playlist.Add(temp);
        }
        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        public virtual void OnPropertyChanged(params string[] propertyNames)
        {
            if (PropertyChanged == null)
                return;

            foreach (string propertyName in propertyNames)
            {
                try
                {
                    PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
                }
                catch { }
            }
        }

        #endregion

        double imgSlide = 0;
        double noimgMoved = 1;        
        private void Back_Click(object sender, RoutedEventArgs e)
        {
            if (noimgMoved != 1)
            {
                noimgMoved--;
                imgSlide += this.ElementProperties.SlideViewWidth;
                Animation.SlideImageEffect(this.PanelHost, imgSlide).Begin();
            }
        }

        private void Next_Click(object sender, RoutedEventArgs e)
        {
            if (PanelHost.Children.Count != noimgMoved)
            {
                noimgMoved++;
                imgSlide -= this.ElementProperties.SlideViewWidth;
                Animation.SlideImageEffect(this.PanelHost, imgSlide).Begin();
            }
        }
    }

    public class Animation
    {
        public static Storyboard SlideImageEffect(UIElement controlToAnimate, double positionToMove)
        {
            DoubleAnimation da = new DoubleAnimation();
            da.Duration = new Duration(TimeSpan.FromSeconds(1));

            Storyboard sb = new Storyboard();
            sb.Duration = new Duration(TimeSpan.FromSeconds(1));
            sb.Children.Add(da);

            Storyboard.SetTarget(da, controlToAnimate);
            Storyboard.SetTargetProperty(da, new PropertyPath("(UIElement.RenderTransform).(TranslateTransform.X)"));
            da.To = positionToMove;
            return sb;
        }
    }
}
