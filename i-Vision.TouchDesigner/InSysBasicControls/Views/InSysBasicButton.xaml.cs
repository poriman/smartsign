﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using InSysBasicControls.InSysProperties;
using InSysBasicControls.Interfaces;

using InSysBasicControls.Events;
using InSysTouchflowData;
using InSysBasicControls.Commons;
using System.ComponentModel;
using InSysTouchflowData.Models.ActionEvents;

namespace InSysBasicControls.Views
{
    /// <summary>
    /// Interaction logic for InSysBasicButton.xaml
    /// </summary>
    public partial class InSysBasicButton : UserControl, IDesignElement, ISelectable, IElementProperties, INotifyPropertyChanged
    {       
        private bool _IsPlay;      

        #region Controls

        #endregion

        /// <summary>
        /// Represents the set of changable properties
        /// </summary>
        Dictionary<string, object> _properties;       

        #region IsSelected Property

        public bool IsSelected
        {
            get { return (bool)GetValue(IsSelectedProperty); }
            set { SetValue(IsSelectedProperty, value); }
        }
        public static readonly DependencyProperty IsSelectedProperty =
          DependencyProperty.Register("IsSelected",
                                       typeof(bool),
                                       typeof(InSysBasicButton),
                                       new FrameworkPropertyMetadata(false));

        #endregion

        private ElementPropertyObject _elementProperties;

        private bool isPlaylistItemsTemp;
        private bool isPlaylistItems;
        public bool IsPlaylistItems
        {
            get { return isPlaylistItems; }
            set { isPlaylistItems = value; OnPropertyChanged("IsPlaylistItems"); }
        }

        //Show InSysImageBox Control Editor 델리게이트
        private UserDelegates.ShowInSysControlPlayerEditorDelegate ShowInSysControlPlayerEditorHandler = null;
        private UserDelegates.EventSettingInSysControlDelegate EventSettingInSysControlHandler;
        private UserDelegates.UpdatedItemPropertyDelegate UpdatedItemPropertyHandler;

        private ActionEvent _ActionEvent;       

        void IDesignElement.SetActionEvent(object actionEvent)
        {
            this._ActionEvent = actionEvent as ActionEvent;
        }

        #region IElementProperties

        public ElementPropertyObject ElementProperties
        {
            get
            {
                return _elementProperties;
            }
            set { _elementProperties = value; }
        }

        public void InitInSysElementProperties()
        {
            _elementProperties = new ElementPropertyObject();
            
            this._elementProperties.Width = 140;
            this._elementProperties.Height = 32;
            this._elementProperties.X = 0;
            this._elementProperties.Y = 0;
            //this._elementProperties.ZIndex = 0;
            this._elementProperties.Opacity = 1;
            this._elementProperties.StartTime = TimeSpan.Zero;
            this._elementProperties.EndTime = TimeSpan.Zero;
            this._elementProperties.Name = "";
            this._elementProperties.Content = "Button";
            this._elementProperties.FontSize = button.FontSize;
            this._elementProperties.Background = SystemColors.ControlBrush;
            this._elementProperties.Foreground = button.Foreground;// new SolidColorBrush(Colors.Black);
            this._elementProperties.BorderBrush = button.BorderBrush;// new SolidColorBrush(Colors.Transparent);
            this._elementProperties.FontFamily = button.FontFamily;
            this._elementProperties.FontWeight = button.FontWeight;
            this._elementProperties.Alignment = Alignment.None;
            this._elementProperties.FitToPage = FitToPage.None;
            this._elementProperties.TouchEvent = new Dictionary<string,string>();
            //this._elementProperties.EventName = "";
            
            //button.Style = new Style(typeof(Button));
            this._elementProperties.ButtonStyle = button.Style;
            
            if (this.EventSettingInSysControlHandler != null)
                this.EventSettingInSysControlHandler();
        }

        #endregion

        public double OpacityValue
        {
            get;
            set;
        }

        public InSysBasicButton()
        {
            InitializeComponent();
            InitInSysElementProperties();

            this.DataContext = this;
            _IsPlay = false;
            ((IDesignElement)this).IsSupportPlaylist = false;
            ((IDesignElement)this).TouchEvent = new Dictionary<string,string>();
            OpacityValue = 0.0;

            this.button.Click += new RoutedEventHandler(button_Click);

            isPlaylistItems = true;
        }

        void button_Click(object sender, RoutedEventArgs e)
        {
            IDesignElement designElement = this as IDesignElement;
            if (designElement != null)
            {
                if (designElement.RaiseActionEventHandler != null)
                {
                    designElement.RaiseActionEventHandler((this as IDesignElement).ActionEvent);
                }

                if (designElement.RaiseDoContentMouseClickTouchEventHandler != null)
                {
                    designElement.RaiseDoContentMouseClickTouchEventHandler(this);
                }
            }
        }

        private void CreateContentControl()
        {            
        }

        public IDesignElement GetElement
        {
            get
            {
                return this as IDesignElement;
            }
        }

        #region IDesignElement Members

        RaiseActionEventDelegate IDesignElement.RaiseActionEventHandler
        {
            get;
            set;
        }

        /// <summary>
        /// Touch Event
        /// </summary>
        RaiseDoContentMouseClickTouchEventDelegate IDesignElement.RaiseDoContentMouseClickTouchEventHandler
        {
            get;
            set;
        }

        Dictionary<string, string> IDesignElement.TouchEvent { get; set; } //Touch Event
        string IDesignElement.EventName { get; set; } //Touch Event

        ActionEvent IDesignElement.ActionEvent
        {
            get { return this._ActionEvent; }
            set { this._ActionEvent = value; }
        }

        MediaType IDesignElement.MediaType
        {
            get { return MediaType.Control; }
        }

        InSysControlType IDesignElement.InSysControlType
        {
            get { return InSysControlType.InSysBasicButton; }
        }

        List<PlaylistItem> IDesignElement.Playlist
        {
            get;
            set;
        }

        ImageSource IDesignElement.ThumnailImageSource { get; set; }

         void IDesignElement.UpdatePlaylistItems()
        {
        }

        void IDesignElement.ResizePlaylistPreviewListBox(double width, double height)
        {
        }

        bool IDesignElement.IsSupportPlaylist { get; set; }

        void IDesignElement.Play()
        {
            this.isPlaylistItemsTemp = this.IsPlaylistItems;//Play 상태에서는 무조건 디폴트 사진은 안보이도록 한다.
            this.IsPlaylistItems = true;
            sp_DisplayText.Visibility = Visibility.Collapsed;

            
        }

        bool IDesignElement.ISPlay
        {
            get
            {
                return this._IsPlay;
            }
        }

        void IDesignElement.Stop()
        {
            sp_DisplayText.Visibility = Visibility.Visible;
            this.IsPlaylistItems = this.isPlaylistItemsTemp;//Stop 상태에서는 Play전의 값으로 재설정한다.
        }

        void IDesignElement.RePlay()
        {
        }

        Dictionary<string, object> IDesignElement.Properties
        {
            get
            {
                if (_properties == null)
                    ((IDesignElement)this).InitProperties();
                return _properties;
            }
            set
            {
                _properties = value;
                //foreach (string name in _properties.Keys)
                //{
                //    PropertyClass.SetProperty(this, "IDesignElement", name, _properties[name]);
                //}
            }
        }

        double IDesignElement.ControlTimeDuration
        {
            get;
            set;
        }

        void IDesignElement.InitProperties()
        {
            _properties = new Dictionary<string, object>();
            IDesignElement child = this as IDesignElement;

            _properties.Add("Name", child.Name);
            _properties.Add("Width", child.Width);
            _properties.Add("Height", child.Height);
            _properties.Add("X", child.X);
            _properties.Add("Y", child.Y);
            _properties.Add("HorizontalAlignment", child.HorizontalAlignment);
            _properties.Add("VerticalAlignment", child.VerticalAlignment);
            _properties.Add("VerticalContentAlignment", child.VerticalContentAlignment);
            _properties.Add("Opacity", child.Opacity);
            _properties.Add("ZIndex", child.ZIndex);            
            _properties.Add("BorderBrush", System.Windows.Markup.XamlWriter.Save(child.BorderBrush));
            _properties.Add("BorderThickness", child.BorderThickness);            
            _properties.Add("Background", System.Windows.Markup.XamlWriter.Save(child.Background));
            _properties.Add("BorderCorner", child.BorderCorner);
            _properties.Add("Type", child.Type);
            _properties.Add("ActionEvent", child.ActionEvent);
            _properties.Add("Content", child.Content);
            _properties.Add("FontSize", child.FontSize);
            _properties.Add("FontFamily", child.FontFamily);
            _properties.Add("FontWeight", child.FontWeight);
            _properties.Add("ControlTimeDuration", child.ControlTimeDuration);
            _properties.Add("IsApplyLifeTime", child.IsApplyLifeTime);            
            _properties.Add("StartTime", child.StartTime);
            _properties.Add("EndTime", child.EndTime);
            _properties.Add("HighlightStyle", child.HighlightStyle);
            _properties.Add("HighlightBrightness", child.HighlightBrightness);
            _properties.Add("Foreground", System.Windows.Markup.XamlWriter.Save(child.Foreground));
            _properties.Add("Alignment", child.Alignment);
            _properties.Add("FitToPage", child.FitToPage);
            if (button.Style != null)
                _properties.Add("ButtonStyle", System.Windows.Markup.XamlWriter.Save(button.Style));
            else
                _properties.Add("ButtonStyle", System.Windows.Markup.XamlWriter.Save(SystemColors.ControlBrush));
            _properties.Add("ButtonStyleKey", child.ButtonStyleKey);
            _properties.Add("TouchEvent", child.TouchEvent);
            _properties.Add("EventName", child.EventName);
        }

        string IDesignElement.Name
        {
            get
            {
                return this._elementProperties.Name;
            }
            set
            {
                this._elementProperties.Name = value;
            }
        }

        double IDesignElement.Width
        {
            get
            {
                return this._elementProperties.Width;
            }
            set
            {
                this._elementProperties.Width = value;
            }
        }

        double IDesignElement.Height
        {
            get
            {
                return this._elementProperties.Height;
            }
            set
            {
                this._elementProperties.Height = value;
            }
        }

        double IDesignElement.SlideViewWidth
        {
            get
            {
                return this._elementProperties.SlideViewWidth;
            }
            set
            {
                this._elementProperties.SlideViewWidth = value;
            }
        }

        double IDesignElement.SlideViewHeight
        {
            get
            {
                return this._elementProperties.SlideViewHeight;
            }
            set
            {
                this._elementProperties.SlideViewHeight = value;
            }
        }

        double IDesignElement.X
        {
            get
            {
                //return (double)this.GetValue(Canvas.LeftProperty);
                return this._elementProperties.X;
            }
            set
            {
                //this.SetValue(Canvas.LeftProperty, value);
                this._elementProperties.X = value;
            }
        }

        double IDesignElement.Y
        {
            get
            {
                //return (double)this.GetValue(Canvas.TopProperty);
                return this._elementProperties.Y;
            }
            set
            {
                //SetValue(Canvas.TopProperty, value);
                this._elementProperties.Y = value;
            }
        }

        double IDesignElement.Bottom
        {
            get
            {
                return (double)this.GetValue(Canvas.BottomProperty);
            }
            set
            {
                this.SetValue(Canvas.BottomProperty, value);
            }
        }

        double IDesignElement.Right
        {
            get
            {
                return (double)this.GetValue(Canvas.RightProperty);
            }
            set
            {
                SetValue(Canvas.RightProperty, value);
            }
        }

        int IDesignElement.ZIndex
        {
            get
            {
                //return (int)this.GetValue(Canvas.ZIndexProperty);
                return this._elementProperties.ZIndex;
            }
            set
            {
                //this.SetValue(Canvas.ZIndexProperty, value);
                this.ElementProperties.ZIndex = value;
            }
        }

        Brush IDesignElement.BorderBrush
        {
            get
            {
                return this._elementProperties.BorderBrush;
            }
            set
            {
                this._elementProperties.BorderBrush = value;
            }
        }

      

        Brush IDesignElement.Background
        {
            get
            {
                return (this._elementProperties as ElementPropertyObject).Background;
            }
            set
            {
                (this._elementProperties as ElementPropertyObject).Background = value;
            }
        }
        
        FitToPage IDesignElement.FitToPage
        {
            get { return this._elementProperties.FitToPage; }
            set { this._elementProperties.FitToPage = value; }

        }

        Alignment IDesignElement.Alignment
        {
            get
            {
                return this._elementProperties.Alignment;
            }
            set
            {
                this._elementProperties.Alignment = value;
            }
        }

        HorizontalAlignment IDesignElement.HorizontalAlignment
        {
            get
            {
                //return this.HorizontalAlignment;
                return this._elementProperties.HorizontalAlignment;
            }
            set
            {
                //this.HorizontalAlignment = value;
                this._elementProperties.HorizontalAlignment = value;
            }
        }

        VerticalAlignment IDesignElement.VerticalAlignment
        {
            get
            {
                //return this.VerticalAlignment;
                return this._elementProperties.VerticalAlignment;
            }
            set
            {
                //this.VerticalAlignment = value;
                this._elementProperties.VerticalAlignment = value;
            }
        }

        Visibility IDesignElement.Visibility
        {
            get
            {
                return this._elementProperties.Visibility;
            }
            set
            {
                if (this._elementProperties.Visibility == value)
                    return;
                this._elementProperties.Visibility = value;
            }
        }


        private Visibility lifeTimeVisibility;
        public Visibility LifeTimeVisibility
        {
            get
            {
                return lifeTimeVisibility;
            }
            set
            {
                if (this.lifeTimeVisibility == value)
                    return;
                this.lifeTimeVisibility = value;
                OnPropertyChanged("LifeTimeVisibility");
            }
        }    

        double IDesignElement.Opacity
        {
            get
            {
                //return this.Opacity;
                return this._elementProperties.Opacity;
            }
            set
            {
                if (value == 0.0)
                    OpacityValue = 0.5;
                else
                    OpacityValue = 0.0;
                this._elementProperties.Opacity = value;
            }
        }     

        FontFamily IDesignElement.FontFamily
        {
            get
            {
                return (this._elementProperties as ElementPropertyObject).FontFamily;
            }
            set
            {
                (this._elementProperties as ElementPropertyObject).FontFamily = value;
            }
        }

        double IDesignElement.FontSize
        {
            get
            {
                return (this._elementProperties as ElementPropertyObject).FontSize;
            }
            set
            {
                (this._elementProperties as ElementPropertyObject).FontSize = value;
            }
        }

        FontWeight IDesignElement.FontWeight
        {
            get
            {
                return (this._elementProperties as ElementPropertyObject).FontWeight;
            }
            set
            {
                (this._elementProperties as ElementPropertyObject).FontWeight = value;
            }
        }

        TextWrapping IDesignElement.TextWrapping { get; set; }

        bool IDesignElement.Multiline { get; set; }

        Brush IDesignElement.Foreground
        {
            get
            {
                return (this._elementProperties as ElementPropertyObject).Foreground;
            }
            set
            {
                (this._elementProperties as ElementPropertyObject).Foreground = value;
            }
        }        

        object IDesignElement.Content
        {
            get
            {
                //return this.button.Content;
                return (this._elementProperties as ElementPropertyObject).Content;
            }
            set
            {
                (this._elementProperties as ElementPropertyObject).Content = value;
            }
        }

        IMediaFilesManager IDesignElement.FileManager
        {
            get { return Parent as IMediaFilesManager; }
        }

        VerticalAlignment IDesignElement.VerticalContentAlignment
        {
            get
            {
                return this.VerticalAlignment;
            }
            set
            {
                this.VerticalAlignment = value;
            }
        }

        HorizontalAlignment IDesignElement.HorizontalContentAlignment
        {
            get
            {
                return this.HorizontalAlignment;
            }
            set
            {
                this.HorizontalAlignment = value;
            }
        }

        System.Drawing.Bitmap IDesignElement.GetThumbnail(int cx, int cy)
        {
            return null;
        }

        bool IDesignElement.IsApplyLifeTime
        {
            get { return this._elementProperties.IsApplyLifeTime; }
            set { this._elementProperties.IsApplyLifeTime = value; }
        }

        bool IDesignElement.IsInLifeTime
        {
            get;
            set;
        }

        TimeSpan IDesignElement.StartTime
        {
            get { return this._elementProperties.StartTime; }
            set { this._elementProperties.StartTime = value; }
        }

        TimeSpan IDesignElement.EndTime
        {
            get { return this._elementProperties.EndTime; }
            set { this._elementProperties.EndTime = value; }
        }

      
        private bool userVisibilitySet = false;
        bool IDesignElement.UserVisibilitySet
        {
            get { return userVisibilitySet; }
            set { userVisibilitySet = value; }
        }

        void IDesignElement.CheckLifeTime(TimeSpan playTimeSpan)
        {
            IDesignElement designeElement = this as IDesignElement;
            if (designeElement != null)
            {
                if (designeElement.IsApplyLifeTime == true)
                {
                    if (playTimeSpan >= designeElement.StartTime && playTimeSpan <= designeElement.EndTime)
                    {
                        if (designeElement.Visibility == System.Windows.Visibility.Visible)
                        {
                            if (this.Visibility != System.Windows.Visibility.Visible)
                                this.Visibility = System.Windows.Visibility.Visible;
                            if (designeElement.LifeTimeVisibility != System.Windows.Visibility.Visible)
                                designeElement.LifeTimeVisibility = System.Windows.Visibility.Visible;
                        }
                        if (designeElement.IsInLifeTime == false)
                            designeElement.IsInLifeTime = true;

                        if (_IsPlay == false)
                            designeElement.Play();
                    }
                    else
                    {
                        if (this.Visibility != System.Windows.Visibility.Hidden)
                            this.Visibility = System.Windows.Visibility.Hidden;
                        if (designeElement.LifeTimeVisibility != System.Windows.Visibility.Hidden)
                            designeElement.LifeTimeVisibility = System.Windows.Visibility.Hidden;
                        designeElement.IsInLifeTime = false;
                        if (designeElement.IsInLifeTime == true)
                            designeElement.IsInLifeTime = false;
                        if (_IsPlay == true)
                            designeElement.Stop();
                    }
                }
                else
                {
                    if (designeElement.Visibility == System.Windows.Visibility.Visible)
                    {
                        if (this.Visibility != System.Windows.Visibility.Visible)
                            this.Visibility = System.Windows.Visibility.Visible;
                        if (designeElement.LifeTimeVisibility != System.Windows.Visibility.Visible)
                            designeElement.LifeTimeVisibility = System.Windows.Visibility.Visible;
                    }
                    if (designeElement.IsInLifeTime == false)
                        designeElement.IsInLifeTime = true;

                    if (_IsPlay == false)
                        designeElement.Play();
                }
            }
        }

        #region No Used Interface Members

        double IDesignElement.BorderThickness
        {
            get
            {
                return this.BorderThickness.Left;
            }
            set
            {
                this.BorderThickness = new Thickness(value);
            }
        }

        double IDesignElement.BorderCorner
        {
            get
            {
                return (this._elementProperties as ElementPropertyObject).BorderCorner;
            }
            set
            {
                (this._elementProperties as ElementPropertyObject).BorderCorner = value;
            }
        }

        int IDesignElement.HighlightStyle
        {
            get;
            set;
        }

        int IDesignElement.HighlightBrightness
        {
            get
            {
                return (this._elementProperties as ElementPropertyObject).HighlightBrightness;
            }
            set
            {
                (this._elementProperties as ElementPropertyObject).HighlightBrightness = value;
            }
        }

        Style IDesignElement.ButtonStyle
        {
            get
            {                
                return this._elementProperties.ButtonStyle;
            }
            set
            {
                this.button.Style = value;
                this._elementProperties.ButtonStyle = value;
            }
        }

        string IDesignElement.ButtonStyleKey { get; set; }

        Stretch IDesignElement.Stretch
        {
            get;
            set;
        }

        double IDesignElement.Volume
        {
            get;
            set;
        }

        bool IDesignElement.Mute
        {
            get;
            set;
        }

        TimeSpan IDesignElement.RefreshInterval
        {
            get;
            set;
        }

        int[] IDesignElement.StrokesLength
        {
            get;
            set;
        }

        PenLineCap IDesignElement.StrokeDashCap
        {
            get;
            set;
        }

        char IDesignElement.SeparatorChar
        {
            get;
            set;
        }

        Point IDesignElement.AspectRatio
        {
            get;
            set;
        }

        Type IDesignElement.Type
        {
            get
            {
                return GetType();
            }
        }

        #endregion

        #endregion

        #region OnMouseDoubleClick
        protected override void OnMouseDoubleClick(MouseButtonEventArgs e)
        {
            base.OnMouseDoubleClick(e);

            //if (ShowInSysControlPlayerEditorHandler != null)
            //    ShowInSysControlPlayerEditorHandler(this as IDesignElement);
        }
        #endregion

        #region Set Delegate 콜백 함수
      

        void IDesignElement.SetDelegate(Delegate handler)
        {
            if (handler.GetType().Equals(typeof(UserDelegates.EventSettingInSysControlDelegate)) == true)
            {
                this.EventSettingInSysControlHandler = handler as UserDelegates.EventSettingInSysControlDelegate;
            }
            else if (handler.GetType().Equals(typeof(UserDelegates.ShowInSysControlPlayerEditorDelegate)) == true)
            {
                this.ShowInSysControlPlayerEditorHandler = handler as UserDelegates.ShowInSysControlPlayerEditorDelegate;
            }
            else if (handler.GetType().Equals(typeof(UserDelegates.UpdatedItemPropertyDelegate)) == true)
            {
                this.UpdatedItemPropertyHandler = handler as UserDelegates.UpdatedItemPropertyDelegate;
            } 
        }


        #endregion

        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            base.OnMouseDown(e);

            e.Handled = false;
        }

        protected override void OnMouseUp(MouseButtonEventArgs e)
        {
            base.OnMouseUp(e);

            e.Handled = false;
        }
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        public virtual void OnPropertyChanged(params string[] propertyNames)
        {
            if (PropertyChanged == null)
                return;

            foreach (string propertyName in propertyNames)
            {
                try
                {
                    PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
                }
                catch { }
            }
        }

        #endregion
    }
}
