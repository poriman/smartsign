﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using InSysTouchflowData;
using InSysBasicControls.InSysProperties;
using InSysBasicControls.Interfaces;
using InSysBasicControls.Commons;

using InSysBasicControls.Events;
using System.ComponentModel;
using InSysTouchflowData.Models.ActionEvents;

namespace InSysBasicControls.Views
{
    /// <summary>
    /// Interaction logic for InSysAnalogueClock.xaml
    /// </summary>
    public partial class InSysAnalogueClock : UserControl, IDesignElement, ISelectable, IElementProperties, INotifyPropertyChanged
    {
        private List<PlaylistItem> _playlist;
        private bool _IsPlay;
        ContentPlayer player;
        /// <summary>
        /// Represents the set of changable properties
        /// </summary>
        Dictionary<string, object> _properties;

        private ElementPropertyObject _elementProperties;

        private bool isPlaylistItemsTemp;
        private bool isPlaylistItems;
        public bool IsPlaylistItems
        {
            get { return isPlaylistItems; }
            set
            {
                isPlaylistItems = value;
                OnPropertyChanged("IsPlaylistItems");
            }
        }

        //Show InSysAnalogueClock Control Editor 델리게이트
        private UserDelegates.ShowInSysControlPlayerEditorDelegate ShowInSysControlPlayerEditorHandler = null;
        private UserDelegates.EventSettingInSysControlDelegate EventSettingInSysControlHandler;
        private UserDelegates.UpdatedItemPropertyDelegate UpdatedItemPropertyHandler;

        private ActionEvent _ActionEvent;

        void IDesignElement.SetActionEvent(object actionEvent)
        {
            this._ActionEvent = actionEvent as ActionEvent;
        }

        #region Controls

        #endregion

        #region IElementProperties 

        public ElementPropertyObject ElementProperties
        {
            get { return _elementProperties; }
            set { _elementProperties = value; }
        }

        public void InitInSysElementProperties()
        {
            _elementProperties = new ElementPropertyObject();
            this._elementProperties.Playlist = new List<PlaylistItem>();          

            this._elementProperties.Width = 200;
            this._elementProperties.Height = 200;
            this._elementProperties.X = 0;
            this._elementProperties.Y = 0;
            //this._elementProperties.ZIndex = 0;
            this._elementProperties.Opacity = 1;
            this._elementProperties.StartTime = TimeSpan.Zero;
            this._elementProperties.EndTime = TimeSpan.Zero;
            this._elementProperties.Name = "";
            (this._elementProperties as ElementPropertyObject).BorderBrush = new SolidColorBrush(Colors.Transparent);
            this._elementProperties.Alignment = Alignment.None;
            this._elementProperties.FitToPage = FitToPage.None;
        }

        #endregion

        public InSysAnalogueClock()
        {
            InitializeComponent();
            InitInSysElementProperties();
            CreateContentControl();
            this.DataContext = this;
            _IsPlay = false;
            ((IDesignElement)this).IsSupportPlaylist = true;
            this.SizeChanged += new SizeChangedEventHandler(AnalogueClockComponent_SizeChanged);

            isPlaylistItems = true;
        }

        private void CreateContentControl()
        {            
        }

        void AnalogueClockComponent_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            //Binding b = new Binding();
            //b.Source = GetElement;
            //b.Mode = BindingMode.OneWay;
            //PositionConvert converter = new PositionConvert();
            //b.Converter = converter;
            //_tbName.SetBinding(TextBlock.TextProperty, b);
        }

        public IDesignElement GetElement
        {
            get
            {
                return this as IDesignElement;
            }
        }       

        #region IsSelected Property

        public bool IsSelected
        {
            get { return (bool)GetValue(IsSelectedProperty); }
            set { SetValue(IsSelectedProperty, value); }
        }
        public static readonly DependencyProperty IsSelectedProperty =
          DependencyProperty.Register("IsSelected",
                                       typeof(bool),
                                       typeof(InSysAnalogueClock),
                                       new FrameworkPropertyMetadata(false));

        #endregion

        #region IDesignElement Members

        RaiseActionEventDelegate IDesignElement.RaiseActionEventHandler
        {
            get;
            set;
        }

        /// <summary>
        /// Touch Event 
        /// </summary>
        RaiseDoContentMouseClickTouchEventDelegate IDesignElement.RaiseDoContentMouseClickTouchEventHandler
        {
            get;
            set;
        }

        Dictionary<string, string> IDesignElement.TouchEvent { get; set; } //Touch Event
        string IDesignElement.EventName { get; set; } //Touch Event

        ActionEvent IDesignElement.ActionEvent
        {
            get { return this._ActionEvent; }
            set { this._ActionEvent = value; }
        }

        void IDesignElement.InitProperties()
        {
            _properties = new Dictionary<string, object>();
            IDesignElement child = this as IDesignElement;

            _properties.Add("Name", child.Name);
            _properties.Add("Width", child.Width);
            _properties.Add("Height", child.Height);
            _properties.Add("X", child.X);
            _properties.Add("Y", child.Y);
            _properties.Add("HorizontalAlignment", child.HorizontalAlignment);
            _properties.Add("VerticalAlignment", child.VerticalAlignment);
            _properties.Add("ZIndex", child.ZIndex);
            _properties.Add("Opacity", child.Opacity);
            //_properties.Add("BorderBrush", child.BorderBrush);
            _properties.Add("BorderBrush", System.Windows.Markup.XamlWriter.Save(child.BorderBrush));
            _properties.Add("BorderThickness", child.BorderThickness);
            _properties.Add("StrokeDashCap", child.StrokeDashCap);
            _properties.Add("StrokesLength", child.StrokesLength);
            //_properties.Add("Background", child.Background);
            _properties.Add("Background", System.Windows.Markup.XamlWriter.Save(child.Background));
            _properties.Add("Foreground", child.Foreground);
            _properties.Add("Type", child.Type);
            _properties.Add("Playlist", child.Playlist);
            _properties.Add("ControlTimeDuration", child.ControlTimeDuration);
            _properties.Add("IsApplyLifeTime", child.IsApplyLifeTime);           
            _properties.Add("StartTime", child.StartTime);
            _properties.Add("EndTime", child.EndTime);
            _properties.Add("Alignment", child.Alignment);
            _properties.Add("FitToPage", child.FitToPage);
        }

        MediaType IDesignElement.MediaType
        {
            get { return MediaType.Clock; }
        }

        InSysControlType IDesignElement.InSysControlType
        {
            get { return InSysControlType.InSysAnalogueClock; }
        }

        Dictionary<string, object> IDesignElement.Properties
        {
            get
            {
                if (_properties == null)
                    ((IDesignElement)this).InitProperties();
                return _properties;
            }
            set
            {
                _properties = value;
                //foreach (string name in _properties.Keys)
                //{
                //    PropertyClass.SetProperty(this, "IDesignElement", name, _properties[name]);
                //}
            }
        }

        double IDesignElement.ControlTimeDuration
        {
            get;
            set;
        }

        string IDesignElement.Name
        {
            get
            {
                return this._elementProperties.Name;
            }
            set
            {
                this._elementProperties.Name = value;
            }
        }

        double IDesignElement.Width
        {
            get
            {
                return this._elementProperties.Width;
            }
            set
            {
                this._elementProperties.Width = value;
            }
        }

        double IDesignElement.Height
        {
            get
            {
                return this._elementProperties.Height;
            }
            set
            {
                this._elementProperties.Height = value;
            }
        }

        double IDesignElement.SlideViewWidth
        {
            get
            {
                return this._elementProperties.SlideViewWidth;
            }
            set
            {
                this._elementProperties.SlideViewWidth = value;
            }
        }

        double IDesignElement.SlideViewHeight
        {
            get
            {
                return this._elementProperties.SlideViewHeight;
            }
            set
            {
                this._elementProperties.SlideViewHeight = value;
            }
        }

        double IDesignElement.X
        {
            get
            {
                //return (double)this.GetValue(Canvas.LeftProperty);
                return this._elementProperties.X;
            }
            set
            {
                //this.SetValue(Canvas.LeftProperty, value);
                this._elementProperties.X = value;
            }
        }

        double IDesignElement.Y
        {
            get
            {
                //return (double)this.GetValue(Canvas.TopProperty);
                return this._elementProperties.Y;
            }
            set
            {
                //SetValue(Canvas.TopProperty, value);
                this._elementProperties.Y = value;
            }
        }

        double IDesignElement.Bottom
        {
            get
            {
                return (double)this.GetValue(Canvas.BottomProperty);
            }
            set
            {
                this.SetValue(Canvas.BottomProperty, value);
            }
        }

        double IDesignElement.Right
        {
            get
            {
                return (double)this.GetValue(Canvas.RightProperty);
            }
            set
            {
                SetValue(Canvas.RightProperty, value);
            }
        }

        int IDesignElement.ZIndex
        {
            get
            {
                //return (int)this.GetValue(Canvas.ZIndexProperty);
                return this._elementProperties.ZIndex;
            }
            set
            {
                //this.SetValue(Canvas.ZIndexProperty, value);
                this.ElementProperties.ZIndex = value;
            }
        }

        Brush IDesignElement.BorderBrush
        {
            get
            {
                return ((IShape)Clock).GetStroke();
            }
            set
            {
                ((IShape)Clock).SetStroke(value);
            }
        }

        double IDesignElement.BorderThickness
        {
            get
            {
                return ((IShape)Clock).GetStrokeThickness();
            }
            set
            {
                ((IShape)Clock).SetStrokeThickness(value);
            }
        }

        FitToPage IDesignElement.FitToPage
        {
            get { return this._elementProperties.FitToPage; }
            set { this._elementProperties.FitToPage = value; }

        }

        Alignment IDesignElement.Alignment
        {
            get
            {
                return this._elementProperties.Alignment;
            }
            set
            {
                this._elementProperties.Alignment = value;
            }
        }

        HorizontalAlignment IDesignElement.HorizontalAlignment
        {
            get
            {
                //return this.HorizontalAlignment;
                return this._elementProperties.HorizontalAlignment;
            }
            set
            {
                //this.HorizontalAlignment = value;
                this._elementProperties.HorizontalAlignment = value;
            }
        }

        VerticalAlignment IDesignElement.VerticalAlignment
        {
            get
            {
                //return this.VerticalAlignment;
                return this._elementProperties.VerticalAlignment;
            }
            set
            {
                //this.VerticalAlignment = value;
                this._elementProperties.VerticalAlignment = value;
            }
        }

        Visibility IDesignElement.Visibility
        {
            get
            {
                return this._elementProperties.Visibility;
            }
            set
            {
                this._elementProperties.Visibility = value;
            }
        }

        private Visibility lifeTimeVisibility;
        public Visibility LifeTimeVisibility
        {
            get
            {
                return lifeTimeVisibility;
            }
            set
            {
                this.lifeTimeVisibility = value;
                OnPropertyChanged("LifeTimeVisibility");
            }
        }     

        double IDesignElement.Opacity
        {
            get
            {
                //return this.Opacity;
                return this._elementProperties.Opacity;
            }
            set
            {
                //this.Opacity = value;
                this._elementProperties.Opacity = value;
            }
        }

        Brush IDesignElement.Background
        {
            get
            {
                return ((IDecoratable)Clock).GetBackgound();
            }
            set
            {
                ((IDecoratable)Clock).SetBackround(value);
            }
        }

     

        Brush IDesignElement.Foreground
        {
            get
            {
                return Clock.FaceStrokes[0].Fill;
            }
            set
            {
                foreach (Rectangle rect in Clock.FaceStrokes)
                {
                    rect.Fill = value;
                }
                Clock.mHand.Fill = value;
                Clock.hHand.Fill = value;
                Clock.sHand.Fill = value;
                Clock.centerCircle.Fill = value;
            }
        }

        int[] IDesignElement.StrokesLength
        {
            get
            {
                return ((IShape)Clock).GetStrokeDashArray();
            }
            set
            {
                ((IShape)Clock).SetStrokeDashArray(value);
            }
        }

        PenLineCap IDesignElement.StrokeDashCap
        {
            get
            {
                return Clock.GetClockBg().StrokeDashCap;
            }
            set
            {
                Clock.GetClockBg().StrokeDashCap = value;
            }
        }

        bool IDesignElement.IsApplyLifeTime
        {
            get { return this._elementProperties.IsApplyLifeTime; }
            set { this._elementProperties.IsApplyLifeTime = value; }
        }

        bool IDesignElement.IsInLifeTime
        {
            get;
            set;
        }

        TimeSpan IDesignElement.StartTime
        {
            get { return this._elementProperties.StartTime; }
            set { this._elementProperties.StartTime = value; }
        }

        TimeSpan IDesignElement.EndTime
        {
            get { return this._elementProperties.EndTime; }
            set { this._elementProperties.EndTime = value; }
        }

        private bool userVisibilitySet = false;
        bool IDesignElement.UserVisibilitySet
        {
            get { return userVisibilitySet; }
            set { userVisibilitySet = value; }
        }

        void IDesignElement.CheckLifeTime(TimeSpan playTimeSpan)
        {
            IDesignElement designeElement = this as IDesignElement;
            if (designeElement != null)
            {
                if (designeElement.IsApplyLifeTime == true)
                {
                    if (playTimeSpan >= designeElement.StartTime && playTimeSpan <= designeElement.EndTime)
                    {
                        if (designeElement.Visibility == System.Windows.Visibility.Visible)
                        {
                            if (this.Visibility != System.Windows.Visibility.Visible)
                                this.Visibility = System.Windows.Visibility.Visible;
                            if (designeElement.LifeTimeVisibility != System.Windows.Visibility.Visible)
                                designeElement.LifeTimeVisibility = System.Windows.Visibility.Visible;
                        }
                        if (designeElement.IsInLifeTime == false)
                            designeElement.IsInLifeTime = true;

                        if (_IsPlay == false)
                            designeElement.Play();
                    }
                    else
                    {
                        if (this.Visibility != System.Windows.Visibility.Hidden)
                            this.Visibility = System.Windows.Visibility.Hidden;
                        if (designeElement.LifeTimeVisibility != System.Windows.Visibility.Hidden)
                            designeElement.LifeTimeVisibility = System.Windows.Visibility.Hidden;
                        designeElement.IsInLifeTime = false;
                        if (designeElement.IsInLifeTime == true)
                            designeElement.IsInLifeTime = false;
                        if (_IsPlay == true)
                            designeElement.Stop();
                    }
                }
                else
                {
                    if (designeElement.Visibility == System.Windows.Visibility.Visible)
                    {
                        if (this.Visibility != System.Windows.Visibility.Visible)
                            this.Visibility = System.Windows.Visibility.Visible;
                        if (designeElement.LifeTimeVisibility != System.Windows.Visibility.Visible)
                            designeElement.LifeTimeVisibility = System.Windows.Visibility.Visible;
                    }
                    if (designeElement.IsInLifeTime == false)
                        designeElement.IsInLifeTime = true;

                    if (_IsPlay == false)
                        designeElement.Play();
                }
            }
        }

        #region No used Members

        double IDesignElement.BorderCorner
        {
            get;
            set;
        }

        int IDesignElement.HighlightStyle
        {
            get;
            set;
        }

        int IDesignElement.HighlightBrightness
        {
            get;
            set;
        }

        Style IDesignElement.ButtonStyle { get; set; }

        string IDesignElement.ButtonStyleKey { get; set; }

        Stretch IDesignElement.Stretch
        {
            get;
            set;
        }

        double IDesignElement.Volume
        {
            get;
            set;
        }

        bool IDesignElement.Mute
        {
            get;
            set;
        }

        TimeSpan IDesignElement.RefreshInterval
        {
            get;
            set;
        }


        FontFamily IDesignElement.FontFamily
        {
            get;
            set;
        }

        double IDesignElement.FontSize
        {
            get;
            set;
        }

        FontWeight IDesignElement.FontWeight
        {
            get;
            set;
        }

        TextWrapping IDesignElement.TextWrapping { get; set; }

        bool IDesignElement.Multiline { get; set; }

        char IDesignElement.SeparatorChar
        {
            get;
            set;
        }             

        Point IDesignElement.AspectRatio
        {
            get;
            set;
        }

        Type IDesignElement.Type
        {
            get
            {
                return GetType();
            }
        }

        #endregion

        object IDesignElement.Content
        {
            get
            {
                if (Clock != null && Clock.gmt != null)
                    return UtilLib.Etc.GmtManager.GetStringIndexOfTimeZone(Clock.gmt) as object;

                return null;
            }
            set
            {
                Clock.gmt = UtilLib.Etc.GmtManager.GetTimeZoneFromIndexInString(value as string);
            }
        }

        IMediaFilesManager IDesignElement.FileManager
        {
            get { return Parent as IMediaFilesManager; }
        }

        System.Drawing.Bitmap IDesignElement.GetThumbnail(int cx, int cy)
        {
            return null;
        }

        #endregion

        #region OnMouseDoubleClick
        protected override void OnMouseDoubleClick(MouseButtonEventArgs e)
        {
            base.OnMouseDoubleClick(e);
            if (ShowInSysControlPlayerEditorHandler != null)
            {
                ShowInSysControlPlayerEditorHandler(this as IDesignElement);

                if (UpdatedItemPropertyHandler != null)
                    UpdatedItemPropertyHandler(this, "Playlist", (this as IDesignElement).Playlist);
            }    
        }
        #endregion

        #region Set Delegate 콜백 함수


        void IDesignElement.SetDelegate(Delegate handler)
        {
            if (handler.GetType().Equals(typeof(UserDelegates.EventSettingInSysControlDelegate)) == true)
            {
                this.EventSettingInSysControlHandler = handler as UserDelegates.EventSettingInSysControlDelegate;
            }
            else if (handler.GetType().Equals(typeof(UserDelegates.ShowInSysControlPlayerEditorDelegate)) == true)
            {
                this.ShowInSysControlPlayerEditorHandler = handler as UserDelegates.ShowInSysControlPlayerEditorDelegate;
            }
            else if (handler.GetType().Equals(typeof(UserDelegates.UpdatedItemPropertyDelegate)) == true)
            {
                this.UpdatedItemPropertyHandler = handler as UserDelegates.UpdatedItemPropertyDelegate;
            } 
        }


        #endregion

      
        List<PlaylistItem> IDesignElement.Playlist
        {
            get
            {
                return this._elementProperties.Playlist;
            }
            set
            {
                this._elementProperties.Playlist = value;
            }
        }

        ImageSource IDesignElement.ThumnailImageSource { get; set; }

         void IDesignElement.UpdatePlaylistItems()
        {
        }

        void IDesignElement.ResizePlaylistPreviewListBox(double width, double height)
        {
        }


        bool IDesignElement.IsSupportPlaylist { get; set; }

        void IDesignElement.Play()
        {
            this.isPlaylistItemsTemp = this.IsPlaylistItems;//Play 상태에서는 무조건 디폴트 사진은 안보이도록 한다.
            this.IsPlaylistItems = true;
            IDesignElement designElement = this as IDesignElement;
            if (designElement != null)
            {
                if (!_IsPlay && designElement.IsInLifeTime == true)
                {
                    sp_DisplayText.Visibility = Visibility.Collapsed;
                    player = new ContentPlayer(this as IDesignElement);
                    player.Start();
                    Clock.Start();
                    _IsPlay = true;
                }
            }
        }

        bool IDesignElement.ISPlay
        {
            get { return _IsPlay; }
        }

        void IDesignElement.Stop()
        {
            sp_DisplayText.Visibility = Visibility.Visible;
            if (player != null)
                player.Stop();
            Clock.Stop();
            _IsPlay = false;

            this.IsPlaylistItems = this.isPlaylistItemsTemp;//Stop 상태에서는 Play전의 값으로 재설정한다.
            //GC.Collect();
        }

        void IDesignElement.RePlay()
        {
            if (this._IsPlay == true)
                (this as IDesignElement).Stop();

            (this as IDesignElement).Play();
        }
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        public virtual void OnPropertyChanged(params string[] propertyNames)
        {
            if (PropertyChanged == null)
                return;

            foreach (string propertyName in propertyNames)
            {
                try
                {
                    PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
                }
                catch { }
            }
        }

        #endregion
    }
}
