﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Input;
using System.ComponentModel;
using InSysTouchflowData;
using InSysBasicControls.InSysProperties;
using InSysBasicControls.Interfaces;
using InSysBasicControls.Commons;
using InSysBasicControls.Events;
using InSysTouchflowData.Models.ActionEvents;
using System.IO;
using UtilLib.StaticMethod;
using System.Runtime.InteropServices;


namespace InSysBasicControls.Views
{
    /// <summary>
    /// Interaction logic for InSysWeb.xaml
    /// </summary>
    public partial class InSysWebControl : UserControl, IDesignElement, ISelectable, IElementProperties, INotifyPropertyChanged
    {

        [DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
        public static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int x, int y, int cx, int cy, int flags);

        IntPtr HWND_TOP = IntPtr.Zero;

		NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();// NLog.LogManager.GetLogger("TouchDesignerLog");
        double zoomfactor = 1.0;

        private List<PlaylistItem> _playlist;
        private bool _IsPlay;
        private bool isPlaylistPreview = false;
        ContentPlayer player;
        //         private WebControl web;

        /// <summary>
        /// Represents the set of changable properties
        /// </summary>
        Dictionary<string, object> _properties;

        private ElementPropertyObject _elementProperties;

        private bool isPlaylistItemsTemp;
        private bool isPlaylistItems;
        public bool IsPlaylistItems
        {
            get { return isPlaylistItems; }
            set { isPlaylistItems = value; OnPropertyChanged("IsPlaylistItems"); }
        }

        //Show InSysImageBox Control Editor 델리게이트
        private UserDelegates.ShowInSysControlPlayerEditorDelegate ShowInSysControlPlayerEditorHandler = null;
        private UserDelegates.EventSettingInSysControlDelegate EventSettingInSysControlHandler;
        private UserDelegates.UpdatedItemPropertyDelegate UpdatedItemPropertyHandler;

        private ActionEvent _ActionEvent;

        void IDesignElement.SetActionEvent(object actionEvent)
        {
            this._ActionEvent = actionEvent as ActionEvent;
        }

        #region IElementProperties

        public ElementPropertyObject ElementProperties
        {
            get { return _elementProperties; }
            set { _elementProperties = value; }
        }

        public void InitInSysElementProperties()
        {
            _elementProperties = new ElementPropertyObject();
            this._elementProperties.Playlist = new List<PlaylistItem>();            

            this._elementProperties.Width = 300;
            this._elementProperties.Height = 300;
            this._elementProperties.X = 0;
            this._elementProperties.Y = 0;
            //this._elementProperties.ZIndex = 0;
            this._elementProperties.Opacity = 1;
            this._elementProperties.StartTime = TimeSpan.Zero;
            this._elementProperties.EndTime = TimeSpan.Zero;
            this._elementProperties.Name = "";
            (this._elementProperties as ElementPropertyObject).BorderBrush = new SolidColorBrush(Colors.Transparent);
            this._elementProperties.Alignment = Alignment.None;
            this._elementProperties.FitToPage = FitToPage.None;
        }

        #endregion

        public InSysWebControl()
        {
            InitializeComponent();
            InitInSysElementProperties();
            this.DataContext = this;
            _IsPlay = false;
            ((IDesignElement)this).IsSupportPlaylist = true;     
            this.SizeChanged += new SizeChangedEventHandler(WebComponent_SizeChanged);
            this.LayoutUpdated += new EventHandler(WebComponent_LayoutUpdated);

            this.web.Width = 0;
            this.web.Height = 0;
        }

        void Zooming()
        {
            try
            {
                // 확대/축소 적용
            }
            catch { }
        }
        void WebComponent_LayoutUpdated(object sender, EventArgs e)
        {
            Zooming();
        }

        void WebComponent_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            (this as IDesignElement).ResizePlaylistPreviewListBox(e.NewSize.Width, e.NewSize.Height);
            //Binding b = new Binding();
            //b.Source = GetElement;
            //b.Mode = BindingMode.OneWay;
            //PositionConvert converter = new PositionConvert();
            //b.Converter = converter;
            //ctrlName.SetBinding(TextBlock.TextProperty, b);
        }

        public IDesignElement GetElement
        {
            get
            {
                return this as IDesignElement;
            }
        }

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            //if (web != null)
            //    web.Dispose();
            //web = null;

            try
            {
                web.Dispose();
                web = null;

                if (host != null)
                {
                    host.Child = null;
                    host.Dispose();
                    host = null;
                }
            }
            catch
            {

            }
        }

        #region IsSelected Property

        public bool IsSelected
        {
            get { return (bool)GetValue(IsSelectedProperty); }
            set { SetValue(IsSelectedProperty, value); }
        }
        public static readonly DependencyProperty IsSelectedProperty =
          DependencyProperty.Register("IsSelected",
                                       typeof(bool),
                                       typeof(InSysWebControl),
                                       new FrameworkPropertyMetadata(false));

        #endregion

        #region IDesignElement Members

        RaiseActionEventDelegate IDesignElement.RaiseActionEventHandler
        {
            get;
            set;
        }

        /// <summary>
        /// Touch Event
        /// </summary>
        RaiseDoContentMouseClickTouchEventDelegate IDesignElement.RaiseDoContentMouseClickTouchEventHandler
        {
            get;
            set;
        }

        Dictionary<string, string> IDesignElement.TouchEvent { get; set; } //Touch Event
        string IDesignElement.EventName { get; set; } //Touch Event

        ActionEvent IDesignElement.ActionEvent
        {
            get { return this._ActionEvent; }
            set { this._ActionEvent = value; }
        }

        MediaType IDesignElement.MediaType
        {
            get { return MediaType.Web; }
        }

        InSysControlType IDesignElement.InSysControlType
        {
            get { return InSysControlType.InSysWebControl; }
        }

        Dictionary<string, object> IDesignElement.Properties
        {
            get
            {
                if (_properties == null)
                    ((IDesignElement)this).InitProperties();
                return _properties;
            }
            set
            {
                _properties = value;
                //foreach (string name in _properties.Keys)
                //{
                //    PropertyClass.SetProperty(this, "IDesignElement", name, _properties[name]);
                //}
            }
        }

        double IDesignElement.ControlTimeDuration
        {
            get;
            set;
        }

        List<PlaylistItem> IDesignElement.Playlist
        {
            get
            {
                return this._elementProperties.Playlist;
            }
            set
            {
                this._elementProperties.Playlist = value;
            }
        }

        ImageSource IDesignElement.ThumnailImageSource { get; set; }

        void IDesignElement.UpdatePlaylistItems()
        {
            IDesignElement designElement = this as IDesignElement;
            if (this._elementProperties.Playlist != null && designElement != null)
            {
                this.playlistPreviewListBox.ItemsSource = null;

                List<PlaylistListBoxItemData> playlistItemDataList = new List<PlaylistListBoxItemData>();
                if (this._elementProperties.Playlist.Count >= 1)
                {
                    foreach (var item in this._elementProperties.Playlist)
                    {
                        PlaylistItem playlistitem = item as PlaylistItem;
                        //Image image = new Image();
                        //image.Source = new BitmapImage(new Uri(@"..\Images\flash.jpg", UriKind.RelativeOrAbsolute));

                        designElement.ThumnailImageSource = ObjectConverters.GetImageSource(@"..\Images\internetexplore.png", UriKind.Relative);

                        PlaylistListBoxItemData data = new PlaylistListBoxItemData(designElement.ThumnailImageSource, 0, 0, this._elementProperties.Width, this._elementProperties.Height, playlistitem.Content);
                        playlistItemDataList.Add(data);
                        break;// 하나의 썸네일 이미지만 추출한다. - Flash는 고정 썸네일 사용.
                    }
                    this.IsPlaylistItems = true;
                }
                else
                {
                    this.IsPlaylistItems = false;
                }
                this.playlistPreviewListBox.ItemsSource = playlistItemDataList;
            }
            //SetPlaylistPreview();//WebBroswer 컨트롤이 다른 엘리먼트보다 Position이 위로 오는 문제로 일단 주석처리한다.
            /*
            (this as IDesignElement).Play();
            rect_line.Visibility = System.Windows.Visibility.Collapsed;
            System.Threading.Thread.Sleep(2000);
            if (this._elementProperties.Playlist != null)
            {
                List<PlaylistListBoxItemData> listboxItems = new List<PlaylistListBoxItemData>();
                foreach (var temp in this.playlistPreviewListBox.Items)
                    listboxItems.Add(temp as PlaylistListBoxItemData);

                int i = 0;
                List<PlaylistListBoxItemData> test = new List<PlaylistListBoxItemData>();
                foreach (var item in this._elementProperties.Playlist)
                {
                    PlaylistItem playlistitem = item as PlaylistItem;
                    BitmapSource bitsource = null;
                    System.Drawing.Bitmap source = (this as IDesignElement).GetThumbnail((int)this._elementProperties.Width, (int)this._elementProperties.Height);
                    var hBitmap = source.GetHbitmap();

                    bitsource = System.Windows.Interop.Imaging.CreateBitmapSourceFromHBitmap(hBitmap,
                        IntPtr.Zero,
                        Int32Rect.Empty,
                        BitmapSizeOptions.FromEmptyOptions());


                    Image image = new Image();
                    image.Source = bitsource;
                    PlaylistListBoxItemData data;
                    var sameitem = listboxItems.Where(o => o.ImagePath.ToLower().Equals(playlistitem.Content.ToLower()) == true).FirstOrDefault();

                    if (sameitem == null)
                    {
                        if (i == 0)
                        {
                            data = new PlaylistListBoxItemData(image.Source, 0, 0, this._elementProperties.Width, this._elementProperties.Height, playlistitem.Content);
                            
                        }
                        else
                        {
                            data = new PlaylistListBoxItemData(image.Source, 0, 0, this._elementProperties.Width, this._elementProperties.Height, playlistitem.Content);
                        }
                        test.Add(data);
                        break;
                    }
                    else
                    {
                        test.Add(sameitem);
                    }
                    i++;                    
                }
                this.playlistPreviewListBox.ItemsSource = test;
            }

            (this as IDesignElement).Stop();
             */ 
        }
        
        void IDesignElement.ResizePlaylistPreviewListBox(double width, double height)
        {
            foreach (var item in this.playlistPreviewListBox.Items)
            {
                PlaylistListBoxItemData data = item as PlaylistListBoxItemData;
                data.Width = width;
                data.Height = height;
            }
            /*
            if (this._elementProperties.Playlist != null && this.isPlaylistPreview == true)
            {
                this.web.Width = width - 15;
                this.web.Height = height - 25;
                this.web.UpdateLayout();
            }
             */ 
        }

        /// <summary>
        /// Playlist 가 존재할 경우 Canvas에 실시간으로 Web page를 보여주기 위한 설정 함수
        /// </summary>
        private void SetPlaylistPreview()
        {
            //if (this._elementProperties.Playlist != null)
            //{
            //    (this as IDesignElement).Play();

            //    this.web.Margin = new Thickness(10, 20, 10, 10);
            //    this.web.IsEnabled = false;
            //    this.rect_line.Visibility = Visibility.Visible;
            //    this.sp_DisplayText.Visibility = Visibility.Visible;
            //    this.web.Width = this._elementProperties.Width - 15;
            //    this.web.Height = this._elementProperties.Height - 25;
            //    this.isPlaylistPreview = true;
            //    this.web.UpdateLayout();
            //}
            //else
            //{
            //    this.web.Margin = new Thickness(10);
            //    this.web.Width = this._elementProperties.Width;
            //    this.web.Height = this._elementProperties.Height;
            //    this.rect_line.Visibility = Visibility.Collapsed;
            //    this.sp_DisplayText.Visibility = Visibility.Collapsed;
            //    this.web.IsEnabled = true;
            //    (this as IDesignElement).Stop();
            //    this.isPlaylistPreview = false;
            //}
        }

        bool IDesignElement.IsSupportPlaylist { get; set; }

        void IDesignElement.Play()
        {
            this.isPlaylistItemsTemp = this.IsPlaylistItems;//Play 상태에서는 무조건 디폴트 사진은 안보이도록 한다.
            this.IsPlaylistItems = true;
           IDesignElement designElement = this as IDesignElement;
            if (designElement != null)
            {
                if (!_IsPlay && designElement.IsInLifeTime == true)
                {
                    _IsPlay = true;
                    //	경계라인
                    rect_line.Visibility = Visibility.Collapsed;
                    this.sp_DisplayText.Visibility = Visibility.Collapsed;

                    //this.web.Visibility = designElement.Visibility;

                    if (host != null)
                    {
                        SetWindowPos(host.Handle, HWND_TOP, 0, 0, 0, 0, 3);
                    }

                    host.Visibility = Visibility.Visible;
                    
                    //this.web.Width = designElement.Width;
                    //this.web.Height = designElement.Height;

                    //this.web.UpdateLayout();

                    player = new ContentPlayer(designElement);
                    player.Start();
                }
            }
        }

        bool IDesignElement.ISPlay
        {
            get { return _IsPlay; }
        }

        void IDesignElement.Stop()
        {
            _IsPlay = false;           
            //	경계라인
            rect_line.Visibility = Visibility.Visible;
            this.sp_DisplayText.Visibility = Visibility.Visible;

            //this.web.Width = 0;
            //this.web.Height = 0;

            web.Stop();
            host.Visibility = Visibility.Collapsed;

            if (player != null)
                player.Stop();

            this.IsPlaylistItems = this.isPlaylistItemsTemp;//Stop 상태에서는 Play전의 값으로 재설정한다.
        }

        void IDesignElement.RePlay()
        {
            if (this._IsPlay == true)
                (this as IDesignElement).Stop();

            (this as IDesignElement).Play();
        }

        void IDesignElement.InitProperties()
        {
            _properties = new Dictionary<string, object>();
            IDesignElement child = this as IDesignElement;

            _properties.Add("Name", child.Name);
            _properties.Add("Width", child.Width);
            _properties.Add("Height", child.Height);
            _properties.Add("X", child.X);
            _properties.Add("Y", child.Y);
            _properties.Add("ZIndex", child.ZIndex);
            _properties.Add("HorizontalAlignment", child.HorizontalAlignment);
            _properties.Add("VerticalAlignment", child.VerticalAlignment);
            _properties.Add("Opacity", child.Opacity);           
            _properties.Add("Type", child.Type);
            _properties.Add("Playlist", child.Playlist);
            _properties.Add("ControlTimeDuration", child.ControlTimeDuration);
            _properties.Add("IsApplyLifeTime", child.IsApplyLifeTime);
            
            _properties.Add("StartTime", child.StartTime);
            _properties.Add("EndTime", child.EndTime);
            _properties.Add("Alignment", child.Alignment);
            _properties.Add("FitToPage", child.FitToPage);
        }

        string IDesignElement.Name
        {
            get
            {
                return this._elementProperties.Name;
            }
            set
            {
                this._elementProperties.Name = value;
            }
        }

        double IDesignElement.Width
        {
            get
            {
                return this._elementProperties.Width;
            }
            set
            {
                this._elementProperties.Width = Math.Ceiling(value);
            }
        }

        double IDesignElement.Height
        {
            get
            {
                return this._elementProperties.Height;
            }
            set
            {
                this._elementProperties.Height = Math.Ceiling(value);
            }
        }

        double IDesignElement.SlideViewWidth
        {
            get
            {
                return this._elementProperties.SlideViewWidth;
            }
            set
            {
                this._elementProperties.SlideViewWidth = value;
            }
        }

        double IDesignElement.SlideViewHeight
        {
            get
            {
                return this._elementProperties.SlideViewHeight;
            }
            set
            {
                this._elementProperties.SlideViewHeight = value;
            }
        }

        double IDesignElement.X
        {
            get
            {
                return this._elementProperties.X;
            }
            set
            {
                this._elementProperties.X = Math.Floor(value);
            }
        }

        double IDesignElement.Y
        {
            get
            {
                return this._elementProperties.Y;
            }
            set
            {
                this._elementProperties.Y = Math.Floor(value);
            }
        }

        double IDesignElement.Bottom
        {
            get
            {
                return (double)this.GetValue(Canvas.BottomProperty);
            }
            set
            {
                this.SetValue(Canvas.BottomProperty, Math.Ceiling(value));
            }
        }

        double IDesignElement.Right
        {
            get
            {
                return (double)this.GetValue(Canvas.RightProperty);
            }
            set
            {
                SetValue(Canvas.RightProperty, Math.Ceiling(value));
            }
        }

        int IDesignElement.ZIndex
        {
            get
            {
                //return (int)this.GetValue(Canvas.ZIndexProperty);
                return this._elementProperties.ZIndex;
            }
            set
            {
                //this.SetValue(Canvas.ZIndexProperty, value);
                this.ElementProperties.ZIndex = value;
            }
        }

        Brush IDesignElement.BorderBrush
        {
            get
            {
                return new SolidColorBrush();// this.rect.Stroke;
            }
            set
            {
                //this.rect.Stroke = value;
            }
        }

        double IDesignElement.BorderThickness
        {
            get
            {
                return 0.0; //rect.StrokeThickness;
            }
            set
            {
                //rect.StrokeThickness = value;
            }
        }

        double IDesignElement.BorderCorner
        {
            get
            {
                return 0.0;// rect.RadiusX;
            }
            set
            {
                //rect.RadiusX = rect.RadiusY = value;
            }
        }

        int IDesignElement.HighlightStyle
        {
            get;
            set;
        }

        int IDesignElement.HighlightBrightness
        {
            get;
            set;
        }

        Style IDesignElement.ButtonStyle { get; set; }

        string IDesignElement.ButtonStyleKey { get; set; }

        Visibility IDesignElement.Visibility
        {
            get
            {
                return this._elementProperties.Visibility;
            }
            set
            {

                if (value != System.Windows.Visibility.Visible)
                {
                    this.web.Width = 0;
                    this.web.Height = 0;
                    this.host.Width = 0;
                    this.host.Height = 0;
                }
                else
                {
                    this.web.Width = (int)this.ElementProperties.Width;
                    this.web.Height = (int)this.ElementProperties.Height;
                }

                //this.web.Visibility = value;
                this._elementProperties.Visibility = value;                
            }
        }

        private Visibility lifeTimeVisibility;
        public Visibility LifeTimeVisibility
        {
            get
            {
                return lifeTimeVisibility;
            }
            set
            {
                this.lifeTimeVisibility = value;
                OnPropertyChanged("LifeTimeVisibility");
            }
        }    

        double IDesignElement.Opacity
        {
            get
            {
                return this.Opacity;
            }
            set
            {
                this.Opacity = value;
            }
        }

        FitToPage IDesignElement.FitToPage
        {
            get { return this._elementProperties.FitToPage; }
            set { this._elementProperties.FitToPage = value; }

        }
        Alignment IDesignElement.Alignment
        {
            get
            {
                return this._elementProperties.Alignment;
            }
            set
            {
                this._elementProperties.Alignment = value;
            }
        }

        HorizontalAlignment IDesignElement.HorizontalAlignment
        {
            get
            {
                return this._elementProperties.HorizontalAlignment;
            }
            set
            {
                this._elementProperties.HorizontalAlignment = value;
            }
        }

        VerticalAlignment IDesignElement.VerticalAlignment
        {
            get
            {
                return this.VerticalAlignment;
            }
            set
            {
                this.VerticalAlignment = value;
            }
        }

        bool IDesignElement.IsApplyLifeTime
        {
            get { return this._elementProperties.IsApplyLifeTime; }
            set { this._elementProperties.IsApplyLifeTime = value; }
        }

        bool IDesignElement.IsInLifeTime
        {
            get;
            set;
        }

        TimeSpan IDesignElement.StartTime
        {
            get { return this._elementProperties.StartTime; }
            set { this._elementProperties.StartTime = value; }
        }

        TimeSpan IDesignElement.EndTime
        {
            get { return this._elementProperties.EndTime; }
            set { this._elementProperties.EndTime = value; }
        }


        private bool userVisibilitySet = false;
        bool IDesignElement.UserVisibilitySet
        {
            get { return userVisibilitySet; }
            set { userVisibilitySet = value; }
        }

        void IDesignElement.CheckLifeTime(TimeSpan playTimeSpan)
        {
            IDesignElement designeElement = this as IDesignElement;
            if (designeElement != null)
            {
                if (designeElement.IsApplyLifeTime == true)
                {
                    if (playTimeSpan >= designeElement.StartTime && playTimeSpan <= designeElement.EndTime)
                    {
                        if (designeElement.Visibility == System.Windows.Visibility.Visible)
                        {
                            if (this.Visibility != System.Windows.Visibility.Visible)
                                this.Visibility = System.Windows.Visibility.Visible;
                            if (designeElement.LifeTimeVisibility != System.Windows.Visibility.Visible)
                                designeElement.LifeTimeVisibility = System.Windows.Visibility.Visible;
                        }
                        if (designeElement.IsInLifeTime == false)
                            designeElement.IsInLifeTime = true;

                        if (_IsPlay == false)
                            designeElement.Play();
                    }
                    else
                    {
                        if (this.Visibility != System.Windows.Visibility.Hidden)
                            this.Visibility = System.Windows.Visibility.Hidden;
                        if (designeElement.LifeTimeVisibility != System.Windows.Visibility.Hidden)
                            designeElement.LifeTimeVisibility = System.Windows.Visibility.Hidden;
                        designeElement.IsInLifeTime = false;
                        if (designeElement.IsInLifeTime == true)
                            designeElement.IsInLifeTime = false;
                        if (_IsPlay == true)
                            designeElement.Stop();
                    }
                }
                else
                {
                    if (designeElement.Visibility == System.Windows.Visibility.Visible)
                    {
                        if (this.Visibility != System.Windows.Visibility.Visible)
                            this.Visibility = System.Windows.Visibility.Visible;
                        if (designeElement.LifeTimeVisibility != System.Windows.Visibility.Visible)
                            designeElement.LifeTimeVisibility = System.Windows.Visibility.Visible;
                    }
                    if (designeElement.IsInLifeTime == false)
                        designeElement.IsInLifeTime = true;

                    if (_IsPlay == false)
                        designeElement.Play();
                }
            }
        }

        #region No used interface members

        Stretch IDesignElement.Stretch
        {
            get;
            set;
        }

        double IDesignElement.Volume
        {
            get;
            set;
        }

        TimeSpan IDesignElement.RefreshInterval
        {
            get;
            set;
        }

        bool IDesignElement.Mute
        {
            get;
            set;
        }

        Brush IDesignElement.Background
        {
            get;
            set;
        }

        FontFamily IDesignElement.FontFamily
        {
            get;
            set;
        }

        double IDesignElement.FontSize
        {
            get;
            set;
        }

        FontWeight IDesignElement.FontWeight
        {
            get;
            set;
        }

        TextWrapping IDesignElement.TextWrapping { get; set; }

        bool IDesignElement.Multiline { get; set; }

        Brush IDesignElement.Foreground
        {
            get;
            set;
        }

        int[] IDesignElement.StrokesLength
        {
            get;
            set;
        }

        PenLineCap IDesignElement.StrokeDashCap
        {
            get;
            set;
        }

        char IDesignElement.SeparatorChar
        {
            get;
            set;
        }

        Point IDesignElement.AspectRatio
        {
            get;
            set;
        }

        #endregion

        Type IDesignElement.Type
        {
            get
            {
                return GetType();
            }
        }
        
        object IDesignElement.Content
        {
            get
            {
                return "";
            }
            set
            {
                
                Zooming();
                string strvalue = value as string;
                if (strvalue == "")
                {
                    this.sp_DisplayText.Visibility = Visibility.Visible;
                    web.Navigate(new Uri("about:blank"));                    
                    return;
                }
               
                if (strvalue.StartsWith("Address://"))
                {
                    UriBuilder builder = new UriBuilder(strvalue.Substring(10));
                    web.Navigate(builder.Uri);
                }
                else if (strvalue.StartsWith("File://"))
                {
                    String content = strvalue.Substring(7);
                    UriBuilder builder = new UriBuilder(content);
                    web.Navigate(builder.Uri);

                }
                else if (strvalue.StartsWith("Script://"))
                {
                    //web.Navigate(strvalue.Substring(9));
                    web.NavigateScript(strvalue.Substring(9));

                }
                else
                {
                    web.Navigate(new UriBuilder(strvalue).Uri);
                }
                /*
                string temp = strvalue.Substring(10);                

                string http = "http://";
                int find_index = strvalue.IndexOf(http);
                string find_str = "";
                if (find_index == -1)
                {
                    if (strvalue.StartsWith("Address://"))
                    {
                        find_str = strvalue.Substring(10);
                    }
                    else if (strvalue.StartsWith("Script://"))
                    {
                        find_str = strvalue.Substring(9);
                    }
                    else
                    {
                        find_str = strvalue;
                    }
                }
                else
                {
                    find_str = strvalue.Substring(find_index + http.Length);
                }

                if (find_str.LastIndexOf('/', find_str.Length - 1) != -1)
                    find_str = find_str.Substring(0, find_str.Length - 1);

                UriHostNameType hostType = Uri.CheckHostName(find_str);
                if (hostType == UriHostNameType.Unknown)
                    return;

                UriBuilder builder = new UriBuilder(find_str);
                web.Navigate(builder.Uri);  
                 */ 
            }
        }

        IMediaFilesManager IDesignElement.FileManager
        {
            get { return Parent as IMediaFilesManager; }
        }

        System.Drawing.Bitmap IDesignElement.GetThumbnail(int cx, int cy)
        {
            try
            {
                System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(cx, cy, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
                using (System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(bmp))
                {
                    Point pos = this.PointToScreen(new Point(0, 0));
                    g.CopyFromScreen(new System.Drawing.Point((int)pos.X, (int)pos.Y),
                        new System.Drawing.Point(0, 0),
                        new System.Drawing.Size((int)this.ActualWidth, (int)this.ActualHeight), System.Drawing.CopyPixelOperation.SourceCopy);
                }

                return bmp;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            return null;
        }

        #endregion

        

        #region OnMouseDoubleClick
        protected override void OnMouseDoubleClick(MouseButtonEventArgs e)
        {
            base.OnMouseDoubleClick(e);

            if (ShowInSysControlPlayerEditorHandler != null)
            {
                ShowInSysControlPlayerEditorHandler(this as IDesignElement);

                //if (UpdatedItemPropertyHandler != null)
                //    UpdatedItemPropertyHandler(this, "Playlist", (this as IDesignElement).Playlist);
            }    
        }
        #endregion

        #region Set Delegate 콜백 함수

        void IDesignElement.SetDelegate(Delegate handler)
        {
            if (handler.GetType().Equals(typeof(UserDelegates.EventSettingInSysControlDelegate)) == true)
            {
                this.EventSettingInSysControlHandler = handler as UserDelegates.EventSettingInSysControlDelegate;
            }
            else if (handler.GetType().Equals(typeof(UserDelegates.ShowInSysControlPlayerEditorDelegate)) == true)
            {
                this.ShowInSysControlPlayerEditorHandler = handler as UserDelegates.ShowInSysControlPlayerEditorDelegate;
            }
            else if (handler.GetType().Equals(typeof(UserDelegates.UpdatedItemPropertyDelegate)) == true)
            {
                this.UpdatedItemPropertyHandler = handler as UserDelegates.UpdatedItemPropertyDelegate;
            } 
        }

        #endregion

        #region TestPlayImage Test

        private void TestPlayWeb()
        {
            PlaylistItem temp = new PlaylistItem();
            temp.Content = "http://www.naver.com";
            temp.CONTENTSVALUE = "http://www.naver.com";
            temp.Duration = "<TimeSpan xmlns=\"clr-namespace:System;assembly=mscorlib\">00:00:10</TimeSpan>";
            temp.TIME = new TimeSpan(0, 0, 10);
            this._playlist.Add(temp);

            temp.Content = "http://www.naver.com";
            temp.CONTENTSVALUE = "http://www.naver.com";
            temp.Duration = "<TimeSpan xmlns=\"clr-namespace:System;assembly=mscorlib\">00:00:10</TimeSpan>";
            temp.TIME = new TimeSpan(0, 0, 10);
            this._playlist.Add(temp);

            temp.Content = "http://www.naver.com";
            temp.CONTENTSVALUE = "http://www.naver.com";
            temp.Duration = "<TimeSpan xmlns=\"clr-namespace:System;assembly=mscorlib\">00:00:10</TimeSpan>";
            temp.TIME = new TimeSpan(0, 0, 10);
            this._playlist.Add(temp);

            this._playlist.Add(temp);
        }
        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        public virtual void OnPropertyChanged(params string[] propertyNames)
        {
            if (PropertyChanged == null)
                return;

            foreach (string propertyName in propertyNames)
            {
                try
                {
                    PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
                }
                catch { }
            }
        }

        #endregion  
    }
}
