﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Xml.Linq;
using InSysTouchflowData;
using InSysBasicControls.Interfaces;
using InSysBasicControls.Commons;
using InSysBasicControls.InSysProperties;
using InSysBasicControls.Events;
using System.Windows.Input;
using UtilLib.IO;
using InSysBasicControls.PropertyControls;
using System.ComponentModel;
using InSysTouchflowData.Models.ActionEvents;

namespace InSysBasicControls.Views
{
    /// <summary>
    /// Interaction logic for InSysScrollText.xaml
    /// </summary>
    public partial class InSysScrollText : UserControl, IDesignElement, ISelectable, IElementProperties, INotifyPropertyChanged
    {
        private List<PlaylistItem> _playlist;
        private bool _IsPlay;
        ContentPlayer player;

        /// <summary>
        /// Represents the set of changable properties
        /// </summary>
        Dictionary<string, object> _properties;

        private ElementPropertyObject _elementProperties;

        private bool isPlaylistItemsTemp;
        private bool isPlaylistItems;
        public bool IsPlaylistItems
        {
            get { return isPlaylistItems; }
            set { isPlaylistItems = value; OnPropertyChanged("IsPlaylistItems"); }
        }

        #region Controls

        #endregion

        //Show InSysImageBox Control Editor 델리게이트
        private UserDelegates.ShowInSysControlPlayerEditorDelegate ShowInSysControlPlayerEditorHandler = null;
        private UserDelegates.EventSettingInSysControlDelegate EventSettingInSysControlHandler;
        private UserDelegates.UpdatedItemPropertyDelegate UpdatedItemPropertyHandler;

        private ActionEvent _ActionEvent;

        void IDesignElement.SetActionEvent(object actionEvent)
        {
            this._ActionEvent = actionEvent as ActionEvent;
        }

        #region IElementProperties

        public ElementPropertyObject ElementProperties
        {
            get { return _elementProperties; }
            set { _elementProperties = value; }
        }

        public void InitInSysElementProperties()
        {
            _elementProperties = new ElementPropertyObject();
            this._elementProperties.Playlist = new List<PlaylistItem>();            

            this._elementProperties.Width = 300;
            this._elementProperties.Height = 32;
            this._elementProperties.X = 0;
            this._elementProperties.Y = 0;
            //this._elementProperties.ZIndex = 0;
            this._elementProperties.Opacity = 1;
            this._elementProperties.StartTime = TimeSpan.Zero;
            this._elementProperties.EndTime = TimeSpan.Zero;
            this._elementProperties.Name = "";
            
            this._elementProperties.Alignment = Alignment.None;
            this._elementProperties.FitToPage = FitToPage.None;
            this._elementProperties.BorderBrush = new SolidColorBrush(Colors.Transparent);
            this._elementProperties.Background = new SolidColorBrush(Colors.White);
            //this._elementProperties.Foreground = new SolidColorBrush(Colors.Black);
        }

        #endregion

        public InSysScrollText()
        {
            InitializeComponent();
            InitInSysElementProperties();
            CreateContentControl();
            this.DataContext = this;

            _IsPlay = false;
            ((IDesignElement)this).IsSupportPlaylist = true;
            BorderThickness = new Thickness(0);
            BorderBrush = new SolidColorBrush(Color.FromArgb(255, 0, 0, 0));
            this.SizeChanged += new SizeChangedEventHandler(ScrollTextComponent_SizeChanged);

            isPlaylistItems = true;
        }       

        private void CreateContentControl()
        {
           
        }

        void ScrollTextComponent_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            //Binding b = new Binding();
            //b.Source = GetElement;
            //b.Mode = BindingMode.OneWay;
            //PositionConvert converter = new PositionConvert();
            //b.Converter = converter;
            //_textBlock.SetBinding(TextBlock.TextProperty, b);
        }

        public IDesignElement GetElement
        {
            get
            {
                return this as IDesignElement;
            }
        }

        public void Load(ScrollTextInfo info)
        {
            _scrollText.Load(info);
        }              

        #region IsSelected Property

        public bool IsSelected
        {
            get { return (bool)GetValue(IsSelectedProperty); }
            set { SetValue(IsSelectedProperty, value); }
        }
        public static readonly DependencyProperty IsSelectedProperty =
          DependencyProperty.Register("IsSelected",
                                       typeof(bool),
                                       typeof(InSysScrollText),
                                       new FrameworkPropertyMetadata(false));

        #endregion
        

        #region IDesignElement Members

        RaiseActionEventDelegate IDesignElement.RaiseActionEventHandler
        {
            get;
            set;
        }

        /// <summary>
        /// Touch Event
        /// </summary>
        RaiseDoContentMouseClickTouchEventDelegate IDesignElement.RaiseDoContentMouseClickTouchEventHandler
        {
            get;
            set;
        }

        Dictionary<string, string> IDesignElement.TouchEvent { get; set; } //Touch Event
        string IDesignElement.EventName { get; set; } //Touch Event

        ActionEvent IDesignElement.ActionEvent
        {
            get { return this._ActionEvent; }
            set { this._ActionEvent = value; }
        }

        MediaType IDesignElement.MediaType
        {
            get { return MediaType.Text; }
        }

        InSysControlType IDesignElement.InSysControlType
        {
            get { return InSysControlType.InSysScrollText; }
        }

        Dictionary<string, object> IDesignElement.Properties
        {
            get
            {
                if (_properties == null)
                    ((IDesignElement)this).InitProperties();
                return _properties;
            }
            set
            {
                _properties = value;
                //foreach (string name in _properties.Keys)
                //{
                //    PropertyClass.SetProperty(this, "IDesignElement", name, _properties[name]);
                //}
            }
        }

        double IDesignElement.ControlTimeDuration
        {
            get;
            set;
        }

        List<PlaylistItem> IDesignElement.Playlist
        {
            get
            {
                return this._elementProperties.Playlist;
            }
            set
            {
                this._elementProperties.Playlist = value;
            }
        }

        ImageSource IDesignElement.ThumnailImageSource { get; set; }

         void IDesignElement.UpdatePlaylistItems()
        {
        }

        void IDesignElement.ResizePlaylistPreviewListBox(double width, double height)
        {
        }

        bool IDesignElement.IsSupportPlaylist { get; set; }

        void IDesignElement.Play()
        {
            this.isPlaylistItemsTemp = this.IsPlaylistItems;//Play 상태에서는 무조건 디폴트 사진은 안보이도록 한다.
            this.IsPlaylistItems = true;
            IDesignElement designElement = this as IDesignElement;
            if (designElement != null)
            {
                if (!_IsPlay && designElement.IsInLifeTime == true)
                {                    
                    //	경계라인
                    rect_line.Visibility = Visibility.Collapsed;
                    _scrollText.Margin = new Thickness(0);//ScrollText 컨트롤이 rect_line 컨트롤을 덮어 외곽선이 보이지 않아 처리한 부분. 
                    this.sp_DisplayText.Visibility = Visibility.Collapsed;
                    player = new ContentPlayer(this);
                    player.Start();
                    _IsPlay = true;
                    isPlaylistItems = true;
                }
            }
        }

        bool IDesignElement.ISPlay
        {
            get { return _IsPlay; }
        }

        void IDesignElement.Stop()
        {
            //	경계라인
            rect_line.Visibility = Visibility.Visible;
            _scrollText.Margin = new Thickness(1);//ScrollText 컨트롤이 rect_line 컨트롤을 덮어 외곽선이 보이지 않아 처리한 부분. 
            this.sp_DisplayText.Visibility = Visibility.Visible;
            player.Stop();
            _scrollText.Stop();
            _IsPlay = false;
            isPlaylistItems = false;
            //GC.Collect();
            this.IsPlaylistItems = this.isPlaylistItemsTemp;//Stop 상태에서는 Play전의 값으로 재설정한다.
        }

        void IDesignElement.RePlay()
        {
            if (this._IsPlay == true)
                (this as IDesignElement).Stop();

            (this as IDesignElement).Play();
        }


        void IDesignElement.InitProperties()
        {
            _properties = new Dictionary<string, object>();
            IDesignElement child = this as IDesignElement;

            _properties.Add("Name", child.Name);
            _properties.Add("Width", child.Width);
            _properties.Add("Height", child.Height);
            _properties.Add("X", child.X);
            _properties.Add("Y", child.Y);
            _properties.Add("HorizontalAlignment", child.HorizontalAlignment);
            _properties.Add("VerticalAlignment", child.VerticalAlignment);
            _properties.Add("ZIndex", child.ZIndex);
            _properties.Add("Opacity", child.Opacity);
            _properties.Add("BorderThickness", child.BorderThickness);            
            _properties.Add("Type", child.Type);
            _properties.Add("Playlist", child.Playlist);
            _properties.Add("ControlTimeDuration", child.ControlTimeDuration);
            _properties.Add("IsApplyLifeTime", child.IsApplyLifeTime);
            _properties.Add("StartTime", child.StartTime);
            _properties.Add("EndTime", child.EndTime);
            _properties.Add("FontSize", child.FontSize);
            _properties.Add("FontFamily", child.FontFamily);
            _properties.Add("FontWeight", child.FontWeight);
            _properties.Add("Alignment", child.Alignment);
            _properties.Add("FitToPage", child.FitToPage);

            if (child.BorderBrush == null)
                _properties.Add("BorderBrush", System.Windows.Markup.XamlWriter.Save(new SolidColorBrush(Colors.Transparent)));
            else
                _properties.Add("BorderBrush", System.Windows.Markup.XamlWriter.Save(child.BorderBrush));
            if (child.Background == null)
                _properties.Add("Background", System.Windows.Markup.XamlWriter.Save(new SolidColorBrush(Colors.White)));
            else
                _properties.Add("Background", System.Windows.Markup.XamlWriter.Save(child.Background));
            //if (child.Foreground == null)
            //    _properties.Add("Foreground", System.Windows.Markup.XamlWriter.Save(new SolidColorBrush(Colors.Black)));
            //else
            //    _properties.Add("Foreground", System.Windows.Markup.XamlWriter.Save(child.Foreground));
        }

        string IDesignElement.Name
        {
            get
            {
                return this._elementProperties.Name;
            }
            set
            {
                this._elementProperties.Name = value;
            }
        }

        double IDesignElement.Width
        {
            get
            {
                return this._elementProperties.Width;
            }
            set
            {
                this._elementProperties.Width = value;
            }
        }

        double IDesignElement.Height
        {
            get
            {
                return this._elementProperties.Height;
            }
            set
            {
                this._elementProperties.Height = value;
            }
        }

        double IDesignElement.SlideViewWidth
        {
            get
            {
                return this._elementProperties.SlideViewWidth;
            }
            set
            {
                this._elementProperties.SlideViewWidth = value;
            }
        }

        double IDesignElement.SlideViewHeight
        {
            get
            {
                return this._elementProperties.SlideViewHeight;
            }
            set
            {
                this._elementProperties.SlideViewHeight = value;
            }
        }

        double IDesignElement.X
        {
            get
            {
                return this._elementProperties.X;
            }
            set
            {
                this._elementProperties.X = value;
            }
        }

        double IDesignElement.Y
        {
            get
            {
                return this._elementProperties.Y;
            }
            set
            {
                this._elementProperties.Y = value;
            }
        }

        double IDesignElement.Bottom
        {
            get
            {
                return (double)this.GetValue(Canvas.BottomProperty);
            }
            set
            {
                this.SetValue(Canvas.BottomProperty, value);
            }
        }

        double IDesignElement.Right
        {
            get
            {
                return (double)this.GetValue(Canvas.RightProperty);
            }
            set
            {
                SetValue(Canvas.RightProperty, value);
            }
        }

        int IDesignElement.ZIndex
        {
            get
            {
                //return (int)this.GetValue(Canvas.ZIndexProperty);
                return this._elementProperties.ZIndex;
            }
            set
            {
                //this.SetValue(Canvas.ZIndexProperty, value);
                this.ElementProperties.ZIndex = value;
            }
        }

        Brush IDesignElement.BorderBrush
        {
            get
            {
                //return this._scrollText.BorderBrush;
                return this._elementProperties.BorderBrush;
            }
            set
            {
                //this._scrollText.BorderBrush = value;
                this._elementProperties.BorderBrush = value;
            }
        }

        double IDesignElement.BorderThickness
        {
            get
            {
                //return this._scrollText.BorderThickness.Left;
                return this._elementProperties.BorderThickness;
            }
            set
            {
                //this._scrollText.BorderThickness = new Thickness(value);
                this._elementProperties.BorderThickness = value;
            }
        }

        double IDesignElement.BorderCorner
        {
            get;
            set;
        }

        int IDesignElement.HighlightStyle
        {
            get;
            set;
        }

        int IDesignElement.HighlightBrightness
        {
            get;
            set;
        }

        Style IDesignElement.ButtonStyle { get; set; }

        string IDesignElement.ButtonStyleKey { get; set; }

        TimeSpan IDesignElement.RefreshInterval
        {
            get;
            set;
        }

        FitToPage IDesignElement.FitToPage
        {
            get { return this._elementProperties.FitToPage; }
            set { this._elementProperties.FitToPage = value; }

        }
        Alignment IDesignElement.Alignment
        {
            get
            {
                return this._elementProperties.Alignment;
            }
            set
            {
                this._elementProperties.Alignment = value;
            }
        }

        HorizontalAlignment IDesignElement.HorizontalAlignment
        {
            get
            {
                return this._elementProperties.HorizontalAlignment;
            }
            set
            {
                this._elementProperties.HorizontalAlignment = value;
            }
        }

        VerticalAlignment IDesignElement.VerticalAlignment
        {
            get
            {
                return VerticalAlignment;
            }
            set
            {
                VerticalAlignment = value;
            }
        }

        Visibility IDesignElement.Visibility
        {
            get
            {
                return this._elementProperties.Visibility;
            }
            set
            {
                this._elementProperties.Visibility = value;
            }
        }

        private Visibility lifeTimeVisibility;
        public Visibility LifeTimeVisibility
        {
            get
            {
                return lifeTimeVisibility;
            }
            set
            {
                this.lifeTimeVisibility = value;
                OnPropertyChanged("LifeTimeVisibility");
            }
        }    

        double IDesignElement.Opacity
        {
            get
            {
                return Opacity;
            }
            set
            {
                Opacity = value;
            }
        }


        Brush IDesignElement.Background
        {
            get { return this._elementProperties.Background; }
            set { this._elementProperties.Background = value; }
        }

        FontFamily IDesignElement.FontFamily
        {
            get
            {
                return ((ITextContainer)_scrollText).GetFontFamily();
            }
            set
            {
                ((ITextContainer)_scrollText).SetFontFamily(value);
            }
        }

        double IDesignElement.FontSize
        {
            get
            {
                //double fontsize = ((ITextContainer)_scrollText).GetFontSize();               
                return (this._elementProperties as ElementPropertyObject).FontSize;
            }
            set
            {
                ((ITextContainer)_scrollText).SetFontSize(value);
                (this._elementProperties as ElementPropertyObject).FontSize = value;
            }
        }

        FontWeight IDesignElement.FontWeight
        {
            get
            {
                return ((ITextContainer)_scrollText).GetFontWeight();
            }
            set
            {
                ((ITextContainer)_scrollText).SetFontWeight(value);
            }
        }

        TextWrapping IDesignElement.TextWrapping { get; set; }

        bool IDesignElement.Multiline { get; set; }

        Brush IDesignElement.Foreground
        {
            get
            {
                //return _scrollText.Foreground;
                return this._elementProperties.Foreground;
            }
            set
            {
                this._elementProperties.Foreground = value;
                //_scrollText.Foreground = value;
            }
        }

        bool IDesignElement.IsApplyLifeTime
        {
            get { return this._elementProperties.IsApplyLifeTime; }
            set { this._elementProperties.IsApplyLifeTime = value; }
        }

        bool IDesignElement.IsInLifeTime
        {
            get;
            set;
        }

        TimeSpan IDesignElement.StartTime
        {
            get { return this._elementProperties.StartTime; }
            set { this._elementProperties.StartTime = value; }
        }

        TimeSpan IDesignElement.EndTime
        {
            get { return this._elementProperties.EndTime; }
            set { this._elementProperties.EndTime = value; }
        }

       

        private bool userVisibilitySet = false;
        bool IDesignElement.UserVisibilitySet
        {
            get { return userVisibilitySet; }
            set { userVisibilitySet = value; }
        }

        void IDesignElement.CheckLifeTime(TimeSpan playTimeSpan)
        {
            IDesignElement designeElement = this as IDesignElement;
            if (designeElement != null)
            {
                if (designeElement.IsApplyLifeTime == true)
                {
                    if (playTimeSpan >= designeElement.StartTime && playTimeSpan <= designeElement.EndTime)
                    {
                        if (designeElement.Visibility == System.Windows.Visibility.Visible)
                        {
                            if (this.Visibility != System.Windows.Visibility.Visible)
                                this.Visibility = System.Windows.Visibility.Visible;
                            if (designeElement.LifeTimeVisibility != System.Windows.Visibility.Visible)
                                designeElement.LifeTimeVisibility = System.Windows.Visibility.Visible;
                        }
                        if (designeElement.IsInLifeTime == false)
                            designeElement.IsInLifeTime = true;

                        if (_IsPlay == false)
                            designeElement.Play();
                    }
                    else
                    {
                        if (this.Visibility != System.Windows.Visibility.Hidden)
                            this.Visibility = System.Windows.Visibility.Hidden;
                        if (designeElement.LifeTimeVisibility != System.Windows.Visibility.Hidden)
                            designeElement.LifeTimeVisibility = System.Windows.Visibility.Hidden;
                        designeElement.IsInLifeTime = false;
                        if (designeElement.IsInLifeTime == true)
                            designeElement.IsInLifeTime = false;
                        if (_IsPlay == true)
                            designeElement.Stop();
                    }
                }
                else
                {
                    if (designeElement.Visibility == System.Windows.Visibility.Visible)
                    {
                        if (this.Visibility != System.Windows.Visibility.Visible)
                            this.Visibility = System.Windows.Visibility.Visible;
                        if (designeElement.LifeTimeVisibility != System.Windows.Visibility.Visible)
                            designeElement.LifeTimeVisibility = System.Windows.Visibility.Visible;
                    }
                    if (designeElement.IsInLifeTime == false)
                        designeElement.IsInLifeTime = true;

                    if (_IsPlay == false)
                        designeElement.Play();
                }
            }
        }

        #region No used interface members

        Stretch IDesignElement.Stretch
        {
            get;
            set;
        }

        double IDesignElement.Volume
        {
            get;
            set;
        }

        bool IDesignElement.Mute
        {
            get;
            set;
        }

        int[] IDesignElement.StrokesLength
        {
            get;
            set;
        }

        PenLineCap IDesignElement.StrokeDashCap
        {
            get;
            set;
        }

        char IDesignElement.SeparatorChar
        {
            get;
            set;
        }

        Point IDesignElement.AspectRatio
        {
            get;
            set;
        }

        #endregion

        Type IDesignElement.Type
        {
            get
            {
                return GetType();
            }
        }
       
        object IDesignElement.Content
        {
            get
            {
                if (_scrollText.Info != null)
                {
                    return ScrollTextInfo.SerializeScrollTextInfo(_scrollText.Info);
                }
                else
                {
                }

                return null;
            }
            set
            {
                if (string.IsNullOrEmpty(value as string) == true)
                    return;
                StringBuilder sb = new StringBuilder(value as string);
                XElement def = XElement.Parse(sb.ToString());
                foreach (XElement el in def.Descendants())
                {
                    if (el.Name.LocalName == "ImageBrush")
                    {
                        string path = el.Attribute("ImageSource").Value;
                        int findindex = path.IndexOf("file:///", 0);
                        if (findindex >= 0)
                            path = path.Replace("file:///", "");
                        if (IOMethod.ExistsFile(path) == true)
                            el.Attribute("ImageSource").Value = path;                        
                    }
                }
                
                _scrollText.Load(ScrollTextInfo.DeserializeScrollTextInfo(def.ToString()));
                _scrollText.IsStartWhenLoading = true;
                _scrollText.Start();
            }
        }

        IMediaFilesManager IDesignElement.FileManager
        {
            get { return Parent as IMediaFilesManager; }
        }

        System.Drawing.Bitmap IDesignElement.GetThumbnail(int cx, int cy)
        {
            return null;
        }

        #endregion

        #region OnMouseDoubleClick
        protected override void OnMouseDoubleClick(MouseButtonEventArgs e)
        {
            base.OnMouseDoubleClick(e);

            if (ShowInSysControlPlayerEditorHandler != null)
                ShowInSysControlPlayerEditorHandler(this as IDesignElement);
        }
        #endregion

        #region Set Delegate 콜백 함수

        void IDesignElement.SetDelegate(Delegate handler)
        {
            if (handler.GetType().Equals(typeof(UserDelegates.EventSettingInSysControlDelegate)) == true)
            {
                this.EventSettingInSysControlHandler = handler as UserDelegates.EventSettingInSysControlDelegate;
            }
            else if (handler.GetType().Equals(typeof(UserDelegates.ShowInSysControlPlayerEditorDelegate)) == true)
            {
                this.ShowInSysControlPlayerEditorHandler = handler as UserDelegates.ShowInSysControlPlayerEditorDelegate;
            }
            else if (handler.GetType().Equals(typeof(UserDelegates.UpdatedItemPropertyDelegate)) == true)
            {
                this.UpdatedItemPropertyHandler = handler as UserDelegates.UpdatedItemPropertyDelegate;
            } 
        }

        #endregion

        #region TestPlayImage Test

        private void TestPlayScrollText()
        {
            PlaylistItem temp = new PlaylistItem();
            temp.Content = "<ScrollTextInfo speed=\"100\" content=\"우리나라 대한민국\" direction=\"RightToLeft\" background=\"#00000000\" descbackground=\"#FFFFFFFF\" foreground=\"#FF000000\" descforeground=\"#FF000000\" FontFamily=\"Arial\" DescFontFamily=\"Arial\" FontSize=\"14\" DescFontSize=\"12\" FontStyle=\"Normal\" DescFontStyle=\"Normal\" FontWeight=\"Normal\" DescFontWeight=\"Normal\" xmlns=\"clr-namespace:InSysTouchflowData;assembly=InSysTouchflowData\" />";
            temp.CONTENTSVALUE = "<ScrollTextInfo speed=\"100\" content=\"우리나라 대한민국\" direction=\"RightToLeft\" background=\"#00000000\" descbackground=\"#FFFFFFFF\" foreground=\"#FF000000\" descforeground=\"#FF000000\" FontFamily=\"Arial\" DescFontFamily=\"Arial\" FontSize=\"14\" DescFontSize=\"12\" FontStyle=\"Normal\" DescFontStyle=\"Normal\" FontWeight=\"Normal\" DescFontWeight=\"Normal\" xmlns=\"clr-namespace:InSysTouchflowData;assembly=InSysTouchflowData\" />";
            temp.Duration = "<TimeSpan xmlns=\"clr-namespace:System;assembly=mscorlib\">00:00:10</TimeSpan>";
            temp.TIME = new TimeSpan(0, 0, 10);
            this._playlist.Add(temp);

            temp = new PlaylistItem();
            temp.Content = "<ScrollTextInfo speed=\"100\" content=\"Silverlight 4\" direction=\"RightToLeft\" background=\"#00000000\" descbackground=\"#FFFFFFFF\" foreground=\"#FF000000\" descforeground=\"#FF000000\" FontFamily=\"Arial\" DescFontFamily=\"Arial\" FontSize=\"14\" DescFontSize=\"12\" FontStyle=\"Normal\" DescFontStyle=\"Normal\" FontWeight=\"Normal\" DescFontWeight=\"Normal\" xmlns=\"clr-namespace:InSysTouchflowData;assembly=InSysTouchflowData\" />";
            temp.CONTENTSVALUE = "<ScrollTextInfo speed=\"100\" content=\"우리나라 대한민국\" direction=\"RightToLeft\" background=\"#00000000\" descbackground=\"#FFFFFFFF\" foreground=\"#FF000000\" descforeground=\"#FF000000\" FontFamily=\"Arial\" DescFontFamily=\"Arial\" FontSize=\"14\" DescFontSize=\"12\" FontStyle=\"Normal\" DescFontStyle=\"Normal\" FontWeight=\"Normal\" DescFontWeight=\"Normal\" xmlns=\"clr-namespace:InSysTouchflowData;assembly=InSysTouchflowData\" />";
            temp.Duration = "<TimeSpan xmlns=\"clr-namespace:System;assembly=mscorlib\">00:00:10</TimeSpan>";
            temp.TIME = new TimeSpan(0, 0, 10);
            this._playlist.Add(temp);

            temp = new PlaylistItem();
            temp.Content = "<ScrollTextInfo speed=\"100\" content=\"Windows Phone 7 Series\" direction=\"RightToLeft\" background=\"#00000000\" descbackground=\"#FFFFFFFF\" foreground=\"#FF000000\" descforeground=\"#FF000000\" FontFamily=\"Arial\" DescFontFamily=\"Arial\" FontSize=\"14\" DescFontSize=\"12\" FontStyle=\"Normal\" DescFontStyle=\"Normal\" FontWeight=\"Normal\" DescFontWeight=\"Normal\" xmlns=\"clr-namespace:InSysTouchflowData;assembly=InSysTouchflowData\" />";
            temp.CONTENTSVALUE = "<ScrollTextInfo speed=\"100\" content=\"우리나라 대한민국\" direction=\"RightToLeft\" background=\"#00000000\" descbackground=\"#FFFFFFFF\" foreground=\"#FF000000\" descforeground=\"#FF000000\" FontFamily=\"Arial\" DescFontFamily=\"Arial\" FontSize=\"14\" DescFontSize=\"12\" FontStyle=\"Normal\" DescFontStyle=\"Normal\" FontWeight=\"Normal\" DescFontWeight=\"Normal\" xmlns=\"clr-namespace:InSysTouchflowData;assembly=InSysTouchflowData\" />";
            temp.Duration = "<TimeSpan xmlns=\"clr-namespace:System;assembly=mscorlib\">00:00:10</TimeSpan>";
            temp.TIME = new TimeSpan(0, 0, 10);
            this._playlist.Add(temp);

            this._playlist.Add(temp);
        }
        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        public virtual void OnPropertyChanged(params string[] propertyNames)
        {
            if (PropertyChanged == null)
                return;

            foreach (string propertyName in propertyNames)
            {
                try
                {
                    PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
                }
                catch { }
            }
        }

        #endregion
    }
}
