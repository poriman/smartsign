﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Media.Media3D;
using System.Windows.Markup;
using System.Xml;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows.Threading;
using InSysTouchflowData;
using InSysBasicControls.InSysProperties;
using InSysBasicControls.Interfaces;
using InSysBasicControls.Commons;
using InSysBasicControls.Events;
using UtilLib.IO;
using System.ComponentModel;
using InSysTouchflowData.Models.ActionEvents;

namespace InSysBasicControls.Views
{
    /// <summary>
    /// Interaction logic for InSysVideoBox.xaml
    /// </summary>
    public partial class InSysVideoBox : UserControl, IDesignElement, ISelectable, IElementProperties, INotifyPropertyChanged
    {
        private List<PlaylistItem> _playlist;
        private bool _IsPlay;
        ContentPlayer player;

        /// <summary>
        /// Represents the set of changable properties
        /// </summary>
        Dictionary<string, object> _properties;

        //Show InSysImageBox Control Editor 델리게이트
        private UserDelegates.ShowInSysControlPlayerEditorDelegate ShowInSysControlPlayerEditorHandler = null;
        private UserDelegates.EventSettingInSysControlDelegate EventSettingInSysControlHandler;
        private UserDelegates.UpdatedItemPropertyDelegate UpdatedItemPropertyHandler;

        private ActionEvent _ActionEvent;

        void IDesignElement.SetActionEvent(object actionEvent)
        {
            this._ActionEvent = actionEvent as ActionEvent;
        }

        //	미디어 컴포넌트들
        //MediaElement _element_media = null;      

        // 		//	버퍼링 타이머
        // 		DispatcherTimer tm_buffering = null;

        //	hsshin 스트리밍 컨텐츠인지?
        private bool _isStreaming = false;
        public bool IsStreaming
        {
            get { return _isStreaming; }
            set { _isStreaming = value; }
        }
        private ElementPropertyObject _elementProperties;

        private bool isPlaylistItemsTemp;
        private bool isPlaylistItems;
        public bool IsPlaylistItems
        {
            get { return isPlaylistItems; }
            set { isPlaylistItems = value; OnPropertyChanged("IsPlaylistItems"); }
        }

        #region IElementProperties

        public ElementPropertyObject ElementProperties
        {
            get { return _elementProperties; }
            set { _elementProperties = value; }
        }

        public void InitInSysElementProperties()
        {
            _elementProperties = new ElementPropertyObject();
            this._elementProperties.Playlist = new List<PlaylistItem>();            

            this._elementProperties.Width = 200;
            this._elementProperties.Height = 200;
            this._elementProperties.X = 0;
            this._elementProperties.Y = 0;
            //this._elementProperties.ZIndex = 0;
            this._elementProperties.Opacity = 1;
            this._elementProperties.StartTime = TimeSpan.Zero;
            this._elementProperties.EndTime = TimeSpan.Zero;
            this._elementProperties.Name = "";
            (this._elementProperties as ElementPropertyObject).BorderBrush = new SolidColorBrush(Colors.Transparent);
            this._elementProperties.Alignment = Alignment.None;
            this._elementProperties.FitToPage = FitToPage.None;
            this._elementProperties.Stretch = Stretch.Fill;
        }

        #endregion

        public InSysVideoBox()
        {
            InitializeComponent();
            InitInSysElementProperties();
            this.DataContext = this;

            _IsPlay = false;
            ((IDesignElement)this).IsSupportPlaylist = true;
            rect.Stroke = new SolidColorBrush(Color.FromArgb(255, 0, 0, 0));
            rect.StrokeThickness = 0.0;            

            _element_media.LoadedBehavior = MediaState.Manual;
            _element_media.MediaEnded += new RoutedEventHandler(media_MediaEnded);   
        }

        public InSysVideoBox(bool bIsStreaming) : this()
        {
            _isStreaming = bIsStreaming;
        }  
     

        private void InSysVideoBox_Loaded(object sender, RoutedEventArgs e)
        {
            
                    
        }

        private void InSysVideoBox_Unloaded(object sender, RoutedEventArgs e)
        {
            if (_element_media != null)
            {
                _element_media.Stop();
                _element_media.Close();
                //_element_media.Source = null;
                //_element_media = null;
            }
            if (rect.Fill != null)
            {
                rect.Fill = null;
            }
        }

        private void InSysVideoBox_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            rect.Width = Width;
            rect.Height = Height;

            (this as IDesignElement).ResizePlaylistPreviewListBox(e.NewSize.Width, e.NewSize.Height);
            //Binding b = new Binding();
            //b.Source = GetElement;
            //b.Mode = BindingMode.OneWay;
            //PositionConvert converter = new PositionConvert();
            //b.Converter = converter;
            //ctrlName.SetBinding(TextBlock.TextProperty, b);
        }

        /*
		void _element_media_BufferingStarted(object sender, RoutedEventArgs e)
		{
			tbBuffering.Visibility = Visibility.Visible;
			if (tm_buffering != null)
			{
				tm_buffering.Stop();
				tm_buffering = null;
			}

			tm_buffering = new DispatcherTimer();
			tm_buffering.Interval = new TimeSpan(0, 0, 1);
			tm_buffering.Tick += new EventHandler(tm_buffering_Tick);
		}
		
		void _element_media_BufferingEnded(object sender, RoutedEventArgs e)
		{
			tbBuffering.Visibility = Visibility.Collapsed;

			if (tm_buffering != null)
			{
				tm_buffering.Stop();
				tm_buffering = null;
			}
		}

		void tm_buffering_Tick(object sender, EventArgs e)
		{
			try
			{
				if (_element_media != null && _element_media.IsBuffering)
				{
					tbBuffering.Text = String.Format("Buffering.. {0}", _element_media.BufferingProgress);
				}
			}
			catch {}
		}
		*/

        #region IsSelected Property

        public bool IsSelected
        {
            get { return (bool)GetValue(IsSelectedProperty); }
            set { SetValue(IsSelectedProperty, value); }
        }
        public static readonly DependencyProperty IsSelectedProperty =
          DependencyProperty.Register("IsSelected",
                                       typeof(bool),
                                       typeof(InSysVideoBox),
                                       new FrameworkPropertyMetadata(false));

        #endregion
		
        public IDesignElement GetElement
        {
            get
            {
                return this as IDesignElement;
            }
        }

        void media_MediaEnded(object sender, RoutedEventArgs e)
        {
            _element_media.Position = new TimeSpan(0, 0, 0);
            _element_media.Play();
		}


        #region IDesignElement Members

        RaiseActionEventDelegate IDesignElement.RaiseActionEventHandler
        {
            get;
            set;
        }

        /// <summary>
        /// Touch Event
        /// </summary>
        RaiseDoContentMouseClickTouchEventDelegate IDesignElement.RaiseDoContentMouseClickTouchEventHandler
        {
            get;
            set;
        }

        Dictionary<string, string> IDesignElement.TouchEvent { get; set; } //Touch Event
        string IDesignElement.EventName { get; set; } //Touch Event

        ActionEvent IDesignElement.ActionEvent
        {
            get { return this._ActionEvent; }
            set { this._ActionEvent = value; }
        }

        MediaType IDesignElement.MediaType
        {
            get
            {
                if (this.IsStreaming == true)
                    return MediaType.Streaming;
                else
                    return MediaType.Video; 
            }
        }

        InSysControlType IDesignElement.InSysControlType
        {
            get
            {
                if (this.IsStreaming == true)
                    return InSysControlType.InSysStreamingBox;
                else
                    return InSysControlType.InSysVideoBox;
                
            }
        }

        void IDesignElement.Play()
        {
            this.isPlaylistItemsTemp = this.IsPlaylistItems;//Play 상태에서는 무조건 디폴트 사진은 안보이도록 한다.
            this.IsPlaylistItems = true;
           IDesignElement designElement = this as IDesignElement;
            if (designElement != null)
            {
                if (!_IsPlay && designElement.IsInLifeTime == true)
                {
                    _IsPlay = true;

                    rect_line.Visibility = System.Windows.Visibility.Collapsed;
                    rect.Visibility = Visibility.Collapsed;
                    this.sp_DisplayText.Visibility = Visibility.Collapsed;
                    this.playlistPreviewListBox.Visibility = System.Windows.Visibility.Collapsed;
                    this.playlistPreviewBorder.Visibility = System.Windows.Visibility.Collapsed;

                    player = new ContentPlayer(this as IDesignElement);
                    player.Start();
                }
            }
        }

        bool IDesignElement.ISPlay
        {
            get { return _IsPlay; }
        }

        void IDesignElement.Stop()
        {
            _IsPlay = false;

            rect_line.Visibility = System.Windows.Visibility.Visible;
            rect.Visibility = Visibility.Visible;
            this.sp_DisplayText.Visibility = Visibility.Visible;
            this.playlistPreviewListBox.Visibility = System.Windows.Visibility.Visible;
            this.playlistPreviewBorder.Visibility = System.Windows.Visibility.Visible;

            if (player != null)
                player.Stop();

            if (_element_media != null)
            {
                _element_media.Stop();
                _element_media.Close();
                _element_media.Source = null;
            }
            this.IsPlaylistItems = this.isPlaylistItemsTemp;//Stop 상태에서는 Play전의 값으로 재설정한다.
        }

        void IDesignElement.RePlay()
        {
            if (this._IsPlay == true)
                (this as IDesignElement).Stop();

            (this as IDesignElement).Play();
        }

        List<PlaylistItem> IDesignElement.Playlist
        {
            get
            {
                return this._elementProperties.Playlist;
            }
            set
            {
                this._elementProperties.Playlist = value;
                //foreach (var item in value)
                //{
                //    this._elementProperties.Playlist.Add(item);
                //}   
            }
        }

        ImageSource IDesignElement.ThumnailImageSource { get; set; }

        void IDesignElement.UpdatePlaylistItems()
        {
            IDesignElement designElement = this as IDesignElement;
            if (this._elementProperties.Playlist != null && designElement != null)
            {
                //this.playlistPreviewListBox.ItemsSource = null;
                List<PlaylistListBoxItemData> listboxItems = new List<PlaylistListBoxItemData>();
                foreach (var temp in this.playlistPreviewListBox.Items)
                    listboxItems.Add(temp as PlaylistListBoxItemData);

                int i = 0;
                List<PlaylistListBoxItemData> palylistItemDataList = new List<PlaylistListBoxItemData>();

                if (this._elementProperties.Playlist.Count >= 1)
                {
                    foreach (var item in this._elementProperties.Playlist)
                    {
                        PlaylistItem playlistitem = item as PlaylistItem;
                        PlaylistListBoxItemData data;

                        var sameitem = listboxItems.Where(o => o.ImagePath.Equals(playlistitem.Content) == true || o.ImagePath.ToLower().Equals(playlistitem.Content.ToLower()) == true).FirstOrDefault();
                        if (sameitem == null)
                        {
                            Uri uri = new Uri(playlistitem.Content, UriKind.Absolute);
                            VideoScreenShot.CaptureScreen(uri, TimeSpan.FromSeconds(5), playlistitem.Content, 1, null,
                               (frame, state) =>
                               {
                                   //Image image = new Image();
                                   //image.Width = this._elementProperties.Width;
                                   //image.Height = this._elementProperties.Height;
                                   //image.Source = frame;// new BitmapImage(new Uri(playlistItem.Content, UriKind.RelativeOrAbsolute));   

                                   designElement.ThumnailImageSource = frame;

                                   if (i == 0)
                                   {
                                       data = new PlaylistListBoxItemData(designElement.ThumnailImageSource, 0, 0, this._elementProperties.Width, this._elementProperties.Height, playlistitem.Content);
                                   }
                                   else
                                   {
                                       data = new PlaylistListBoxItemData(designElement.ThumnailImageSource, 0, 0, this._elementProperties.Width, this._elementProperties.Height, playlistitem.Content);
                                   }
                                   palylistItemDataList.Add(data);
                               }
                               );
                        }
                        else
                        {
                            palylistItemDataList.Add(sameitem);
                        }
                        i++;

                    }
                    this.IsPlaylistItems = true;
                }
                else
                {
                    this.IsPlaylistItems = false;
                }

                this.playlistPreviewListBox.ItemsSource = palylistItemDataList;
            }
        }

        void IDesignElement.ResizePlaylistPreviewListBox(double width, double height)
        {
            foreach (var item in this.playlistPreviewListBox.Items)
            {
                PlaylistListBoxItemData data = item as PlaylistListBoxItemData;
                data.Width = width;
                data.Height = height;
            }
        }

        bool IDesignElement.IsSupportPlaylist { get; set; }
  
        Dictionary<string, object> IDesignElement.Properties
        {
            get
            {
                if (_properties == null)
                    ((IDesignElement)this).InitProperties();
                return _properties;
            }
            set
            {
                _properties = value;
                //foreach (string name in _properties.Keys)
                //{
                //    PropertyClass.SetProperty(this, "IDesignElement", name, _properties[name]);
                //}
            }
        }

        double IDesignElement.ControlTimeDuration
        {
            get;
            set;
        }

        void IDesignElement.InitProperties()
        {
            _properties = new Dictionary<string, object>();
            IDesignElement child = this as IDesignElement;

            _properties.Add("Name", child.Name);
            _properties.Add("Width", child.Width);
            _properties.Add("Height", child.Height);
            _properties.Add("X", child.X);
            _properties.Add("Y", child.Y);
            _properties.Add("HorizontalAlignment", child.HorizontalAlignment);
            _properties.Add("VerticalAlignment", child.VerticalAlignment);
			_properties.Add("ZIndex", child.ZIndex);
            _properties.Add("Opacity", child.Opacity);
            //_properties.Add("BorderBrush", child.BorderBrush);
            _properties.Add("BorderBrush", System.Windows.Markup.XamlWriter.Save(child.BorderBrush));
            _properties.Add("BorderThickness", child.BorderThickness);
            _properties.Add("BorderCorner", child.BorderCorner);
            _properties.Add("Stretch", child.Stretch);
			_properties.Add("Type", child.Type);           
            _properties.Add("Mute", child.Mute);
            _properties.Add("Volume", child.Volume);
            _properties.Add("Playlist", child.Playlist);
            _properties.Add("ControlTimeDuration", child.ControlTimeDuration);
            _properties.Add("IsApplyLifeTime", child.IsApplyLifeTime);
            
            _properties.Add("StartTime", child.StartTime);
            _properties.Add("EndTime", child.EndTime);
            _properties.Add("Alignment", child.Alignment);
            _properties.Add("FitToPage", child.FitToPage);         
        }

        string IDesignElement.Name
        {
            get
            {
                return this._elementProperties.Name;
            }
            set
            {
                this._elementProperties.Name = value;
            }
        }

       

        double IDesignElement.Width
        {
            get
            {
                return this._elementProperties.Width;
            }
            set
            {
                this._elementProperties.Width = Math.Ceiling(value);
            }
        }

        double IDesignElement.Height
        {
            get
            {
                return this._elementProperties.Height;
            }
            set
            {
                this._elementProperties.Height = Math.Ceiling(value);
            }
        }

        double IDesignElement.SlideViewWidth
        {
            get
            {
                return this._elementProperties.SlideViewWidth;
            }
            set
            {
                this._elementProperties.SlideViewWidth = value;
            }
        }

        double IDesignElement.SlideViewHeight
        {
            get
            {
                return this._elementProperties.SlideViewHeight;
            }
            set
            {
                this._elementProperties.SlideViewHeight = value;
            }
        }

        double IDesignElement.X
        {
            get
            {
                //return (double)this.GetValue(Canvas.LeftProperty);
                return this._elementProperties.X;
            }
            set
            {
                //this.SetValue(Canvas.LeftProperty, value);
                this._elementProperties.X = Math.Floor(value);
            }
        }

        double IDesignElement.Y
        {
            get
            {
                //return (double)this.GetValue(Canvas.TopProperty);
                return this._elementProperties.Y;
            }
            set
            {
                //SetValue(Canvas.TopProperty, value);
                this._elementProperties.Y = Math.Floor(value);
            }
        }

        double IDesignElement.Bottom
        {
            get
            {
                return (double)this.GetValue(Canvas.BottomProperty);
            }
            set
            {
				this.SetValue(Canvas.BottomProperty, Math.Ceiling(value));
            }
        }

        double IDesignElement.Right
        {
            get
            {
                return (double)this.GetValue(Canvas.RightProperty);
            }
            set
            {
				SetValue(Canvas.RightProperty, Math.Ceiling(value));
            }
        }

        int IDesignElement.ZIndex
        {
            get
            {
                //return (int)this.GetValue(Canvas.ZIndexProperty);
                return this._elementProperties.ZIndex;
            }
            set
            {
                //this.SetValue(Canvas.ZIndexProperty, value);
                this.ElementProperties.ZIndex = value;
            }
        }

        Brush IDesignElement.BorderBrush
        {
            get
            {
                return (this.ElementProperties as ElementPropertyObject).BorderBrush;
            }
            set
            {
                (this.ElementProperties as ElementPropertyObject).BorderBrush = value;
            }
        }

        double IDesignElement.BorderThickness
        {
            get
            {
                return (this.ElementProperties as ElementPropertyObject).BorderThickness;
            }
            set
            {
                (this.ElementProperties as ElementPropertyObject).BorderThickness = value;
            }
        }

        double IDesignElement.BorderCorner
        {
            get
            {
                return (this.ElementProperties as ElementPropertyObject).BorderCorner;
            }
            set
            {
                (this.ElementProperties as ElementPropertyObject).BorderCorner = value;
				//sepLine.RadiusX = sepLine.RadiusY = value;
			}
        }

        int IDesignElement.HighlightStyle
        {
            get;
            set;
        }

        int IDesignElement.HighlightBrightness
        {
            get;
            set;
        }

        Style IDesignElement.ButtonStyle { get; set; }

        string IDesignElement.ButtonStyleKey { get; set; }
        
        Stretch IDesignElement.Stretch
        {
            get
			{
                return (this._elementProperties as ElementPropertyObject).Stretch;
			}
            set
            {
                (this._elementProperties as ElementPropertyObject).Stretch = value;
            }
        }

        FitToPage IDesignElement.FitToPage
        {
            get { return this._elementProperties.FitToPage; }
            set { this._elementProperties.FitToPage = value; }

        }
        Alignment IDesignElement.Alignment
        {
            get
            {
                return this._elementProperties.Alignment;
            }
            set
            {
                this._elementProperties.Alignment = value;
            }
        }

        HorizontalAlignment IDesignElement.HorizontalAlignment
        {
            get
            {
                //return this.HorizontalAlignment;
                return this._elementProperties.HorizontalAlignment;
            }
            set
            {
                //this.HorizontalAlignment = value;
                this._elementProperties.HorizontalAlignment = value;
            }
        }

        VerticalAlignment IDesignElement.VerticalAlignment
        {
            get
            {
                return this._elementProperties.VerticalAlignment;
            }
            set
            {
                this._elementProperties.VerticalAlignment = value;
            }
        }

        Visibility IDesignElement.Visibility
        {
            get
            {
                return this._elementProperties.Visibility;
            }
            set
            {
                this._elementProperties.Visibility = value;
            }
        }

        private Visibility lifeTimeVisibility;
        public Visibility LifeTimeVisibility
        {
            get
            {
                return lifeTimeVisibility;
            }
            set
            {
                this.lifeTimeVisibility = value;
                OnPropertyChanged("LifeTimeVisibility");
            }
        }     

        double IDesignElement.Opacity
        {
            get
            {
                return this._elementProperties.Opacity;
            }
            set
            {
                this._elementProperties.Opacity = value;
            }
        }  

        double IDesignElement.Volume
        {
            get
            {
                return (this._elementProperties as ElementPropertyObject).Volume;
            }
            set
            {
                (this._elementProperties as ElementPropertyObject).Volume = value;
            }
        }

        bool IDesignElement.Mute
        {
            get
            {
                return (this._elementProperties as ElementPropertyObject).IsMuted;
            }
            set
            {
                (this._elementProperties as ElementPropertyObject).IsMuted = value;
            }
        }

        bool IDesignElement.IsApplyLifeTime
        {
            get { return this._elementProperties.IsApplyLifeTime; }
            set { this._elementProperties.IsApplyLifeTime = value; }
        }

        bool IDesignElement.IsInLifeTime
        {
            get;
            set;
        }

        TimeSpan IDesignElement.StartTime
        {
            get { return this._elementProperties.StartTime; }
            set { this._elementProperties.StartTime = value; }
        }

        TimeSpan IDesignElement.EndTime
        {
            get { return this._elementProperties.EndTime; }
            set { this._elementProperties.EndTime = value; }
        }

      

        private bool userVisibilitySet = false;
        bool IDesignElement.UserVisibilitySet
        {
            get { return userVisibilitySet; }
            set { userVisibilitySet = value; }
        }

        void IDesignElement.CheckLifeTime(TimeSpan playTimeSpan)
        {
            IDesignElement designeElement = this as IDesignElement;
            if (designeElement != null)
            {
                if (designeElement.IsApplyLifeTime == true)
                {
                    if (playTimeSpan >= designeElement.StartTime && playTimeSpan <= designeElement.EndTime)
                    {
                        if (designeElement.Visibility == System.Windows.Visibility.Visible)
                        {
                            if (this.Visibility != System.Windows.Visibility.Visible)
                                this.Visibility = System.Windows.Visibility.Visible;
                            if (designeElement.LifeTimeVisibility != System.Windows.Visibility.Visible)
                                designeElement.LifeTimeVisibility = System.Windows.Visibility.Visible;
                        }
                        if (designeElement.IsInLifeTime == false)
                            designeElement.IsInLifeTime = true;

                        if (_IsPlay == false)
                            designeElement.Play();
                    }
                    else
                    {
                        if (this.Visibility != System.Windows.Visibility.Hidden)
                            this.Visibility = System.Windows.Visibility.Hidden;
                        if (designeElement.LifeTimeVisibility != System.Windows.Visibility.Hidden)
                            designeElement.LifeTimeVisibility = System.Windows.Visibility.Hidden;
                        designeElement.IsInLifeTime = false;
                        if (designeElement.IsInLifeTime == true)
                            designeElement.IsInLifeTime = false;
                        if (_IsPlay == true)
                            designeElement.Stop();
                    }
                }
                else
                {
                    if (designeElement.Visibility == System.Windows.Visibility.Visible)
                    {
                        if (this.Visibility != System.Windows.Visibility.Visible)
                            this.Visibility = System.Windows.Visibility.Visible;
                        if (designeElement.LifeTimeVisibility != System.Windows.Visibility.Visible)
                            designeElement.LifeTimeVisibility = System.Windows.Visibility.Visible;
                    }
                    if (designeElement.IsInLifeTime == false)
                        designeElement.IsInLifeTime = true;

                    if (_IsPlay == false)
                        designeElement.Play();
                }
            }
        }

        #region No used interface Members

        Brush IDesignElement.Background
        {
            get;
            set;
        }

        FontFamily IDesignElement.FontFamily
        {
            get;
            set;
        }

        double IDesignElement.FontSize
        {
            get;
            set;
        }

        FontWeight IDesignElement.FontWeight
        {
            get;
            set;
        }

        TextWrapping IDesignElement.TextWrapping { get; set; }

        bool IDesignElement.Multiline { get; set; }

        Brush IDesignElement.Foreground
        {
            get;
            set;
        }

        int[] IDesignElement.StrokesLength
        {
            get;
            set;
        }

		TimeSpan IDesignElement.RefreshInterval
		{
            get;
            set;
		}

        PenLineCap IDesignElement.StrokeDashCap
        {
            get;
            set;
        }

        char IDesignElement.SeparatorChar
        {
            get;
            set;
        }

        Point IDesignElement.AspectRatio
        {
            get;
            set;
        }

        #endregion

        Type IDesignElement.Type
        {
            get
            {
                return GetType();
            }
        }

       

		object IDesignElement.Content
		{
			get
			{
                if (_element_media != null && _element_media.Source != null)
                    return _element_media.Source.AbsolutePath;

                return null;
			}
			set
			{
                if ((string)value == "")
                {
                    this.sp_DisplayText.Visibility = Visibility.Visible;
                    _element_media.Stop();
                    _element_media.Source = null;
                    return;
                }

                string contentfile = value as string;
                if (IsStreaming != true)
                {
                    if (IOMethod.ExistsFile(contentfile) == true)
                    {
                        if (Uri.IsWellFormedUriString(contentfile, UriKind.Absolute))
                        {
                            _element_media.Stop();
                            _element_media.Source = new Uri(contentfile, UriKind.RelativeOrAbsolute);
                        }
                        else
                        {
                            //Uri uri = new Uri(((IDesignElement)this).FileManager.GetFullFilePathByName(value as string));
                            Uri uri = new Uri(contentfile, UriKind.RelativeOrAbsolute);
                            if (_element_media.Source != null && uri.Equals(_element_media.Source))
                            {
                                _element_media.Position = new TimeSpan(0, 0, 0);
                            }
                            else
                            {
                                _element_media.Stop();
                                _element_media.Source = uri;
                            }
                        }
                        _element_media.Play();
                    }
                }
                else
                {
                    if (Uri.IsWellFormedUriString((string)value, UriKind.Absolute))
                    {
                        _element_media.Stop();
                        _element_media.Source = new Uri((string)value);
                        _element_media.Play();
                    }
                }
			}
		}

		IMediaFilesManager IDesignElement.FileManager
        {
            get { return Parent as IMediaFilesManager; }
		}

		System.Drawing.Bitmap IDesignElement.GetThumbnail(int cx, int cy)
		{
            return null;
		}

		#endregion     

        #region OnMouseDoubleClick
        protected override void OnMouseDoubleClick(MouseButtonEventArgs e)
        {
            base.OnMouseDoubleClick(e);

            if (ShowInSysControlPlayerEditorHandler != null)
            {
                ShowInSysControlPlayerEditorHandler(this as IDesignElement);

                //if (UpdatedItemPropertyHandler != null)
                //    UpdatedItemPropertyHandler(this, "Playlist", (this as IDesignElement).Playlist);
            }    
        }       
        #endregion

        #region Set Delegate 콜백 함수

        void IDesignElement.SetDelegate(Delegate handler)
        {
            if (handler.GetType().Equals(typeof(UserDelegates.EventSettingInSysControlDelegate)) == true)
            {
                this.EventSettingInSysControlHandler = handler as UserDelegates.EventSettingInSysControlDelegate;
            }
            else if (handler.GetType().Equals(typeof(UserDelegates.ShowInSysControlPlayerEditorDelegate)) == true)
            {
                this.ShowInSysControlPlayerEditorHandler = handler as UserDelegates.ShowInSysControlPlayerEditorDelegate;
            }
            else if (handler.GetType().Equals(typeof(UserDelegates.UpdatedItemPropertyDelegate)) == true)
            {
                this.UpdatedItemPropertyHandler = handler as UserDelegates.UpdatedItemPropertyDelegate;
            } 
        }

        #endregion

        #region TestPlayVideo Test

        private void TestPlayVideo()
        {
            PlaylistItem temp = new PlaylistItem();
            temp.Content = "testSamples/WhatAGirlWants.avi";
            temp.CONTENTSVALUE = "testSamples/WhatAGirlWants.avi";
            temp.Duration = "<TimeSpan xmlns=\"clr-namespace:System;assembly=mscorlib\">00:00:10</TimeSpan>";
            temp.TIME = new TimeSpan(0, 0, 10);
            this._playlist.Add(temp);

            //temp = new PlaylistItem();
            //temp.Content = "InSysControlsView/testSamples/CL10.wmv";
            //temp.CONTENTSVALUE = "InSysControlsView/testSamples/CL10.wmv";
            //temp.Duration = "<TimeSpan xmlns=\"clr-namespace:System;assembly=mscorlib\">00:00:10</TimeSpan>";
            //temp.TIME = new TimeSpan(0, 0, 10);
            //this._playlist.Add(temp);

            temp = new PlaylistItem();
            temp.Content = "testSamples/CL02.wmv";
            temp.CONTENTSVALUE = "testSamples/CL02.wmv";
            temp.Duration = "<TimeSpan xmlns=\"clr-namespace:System;assembly=mscorlib\">00:00:10</TimeSpan>";
            temp.TIME = new TimeSpan(0, 0, 10);

            this._playlist.Add(temp);
        }
        #endregion      

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        public virtual void OnPropertyChanged(params string[] propertyNames)
        {
            if (PropertyChanged == null)
                return;

            foreach (string propertyName in propertyNames)
            {
                try
                {
                    PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
                }
                catch { }
            }
        }

        #endregion

	}
}
