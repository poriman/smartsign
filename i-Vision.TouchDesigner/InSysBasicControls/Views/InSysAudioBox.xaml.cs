﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using InSysTouchflowData;
using InSysBasicControls.InSysProperties;
using InSysBasicControls.Interfaces;
using InSysBasicControls.Commons;

using InSysBasicControls.Events;
using UtilLib.IO;
using System.ComponentModel;
using InSysTouchflowData.Models.ActionEvents;

namespace InSysBasicControls.Views
{
    /// <summary>
    /// Interaction logic for InSysAudioBox.xaml
    /// </summary>
    public partial class InSysAudioBox : UserControl, IDesignElement, ISelectable, IElementProperties, INotifyPropertyChanged
    {
        private List<PlaylistItem> _playlist;
        private bool _IsPlay;
        private ContentPlayer player;
        private MediaPlayer _audioPlayer;

        /// <summary>
        /// Represents the set of changable properties
        /// </summary>
        Dictionary<string, object> _properties;

        private ElementPropertyObject _elementProperties;

        private bool isPlaylistItemsTemp;
        private bool isPlaylistItems;
        public bool IsPlaylistItems
        {
            get { return isPlaylistItems; }
            set { isPlaylistItems = value; OnPropertyChanged("IsPlaylistItems"); }
        }

        //Show InSysImageBox Control Editor 델리게이트
        private UserDelegates.ShowInSysControlPlayerEditorDelegate ShowInSysControlPlayerEditorHandler = null;
        private UserDelegates.EventSettingInSysControlDelegate EventSettingInSysControlHandler;
        private UserDelegates.UpdatedItemPropertyDelegate UpdatedItemPropertyHandler;

        private ActionEvent _ActionEvent;

        void IDesignElement.SetActionEvent(object actionEvent)
        {
            this._ActionEvent = actionEvent as ActionEvent;
        }

        #region IElementProperties

        public ElementPropertyObject ElementProperties
        {
            get { return _elementProperties; }
            set { _elementProperties = value; }
        }

        public void InitInSysElementProperties()
        {
            _elementProperties = new ElementPropertyObject();
            this._elementProperties.Playlist = new List<PlaylistItem>();            

            this._elementProperties.Width = 50;
            this._elementProperties.Height = 24;
            this._elementProperties.X = 0;
            this._elementProperties.Y = 0;
            //this._elementProperties.ZIndex = 0;
            this._elementProperties.Opacity = 1;
            this._elementProperties.StartTime = TimeSpan.Zero;
            this._elementProperties.EndTime = TimeSpan.Zero;
            this._elementProperties.Name = "";
            (this._elementProperties as ElementPropertyObject).BorderBrush = new SolidColorBrush(Colors.Transparent);
            this._elementProperties.Alignment = Alignment.None;
            this._elementProperties.FitToPage = FitToPage.None;
        }

        #endregion

        #region IsSelected Property

        public bool IsSelected
        {
            get { return (bool)GetValue(IsSelectedProperty); }
            set { SetValue(IsSelectedProperty, value); }
        }
        public static readonly DependencyProperty IsSelectedProperty =
          DependencyProperty.Register("IsSelected",
                                       typeof(bool),
                                       typeof(InSysAudioBox),
                                       new FrameworkPropertyMetadata(false));

        #endregion

        public InSysAudioBox()
        {
            InitializeComponent();
            this.InitInSysElementProperties();
            this.DataContext = this;
            _IsPlay = false;
            ((IDesignElement)this).IsSupportPlaylist = true;
            _audioPlayer = new MediaPlayer();
            _audioPlayer.MediaEnded += new EventHandler(_audioPlayer_MediaEnded);

            isPlaylistItems = true;            
        }               

        void _audioPlayer_MediaEnded(object sender, EventArgs e)
        {
            _audioPlayer.Position = new TimeSpan(0, 0, 0);
            _audioPlayer.Play();
        }


        #region IDesignElement Members

        RaiseActionEventDelegate IDesignElement.RaiseActionEventHandler
        {
            get;
            set;
        }

        /// <summary>
        /// Touch Event
        /// </summary>
        RaiseDoContentMouseClickTouchEventDelegate IDesignElement.RaiseDoContentMouseClickTouchEventHandler
        {
            get;
            set;
        }

        string IDesignElement.Name
        {
            get
            {
                return this._elementProperties.Name;
            }
            set
            {
                this._elementProperties.Name = value;
            }
        }

        Dictionary<string, string> IDesignElement.TouchEvent { get; set; } //Touch Event
        string IDesignElement.EventName { get; set; } //Touch Event

        ActionEvent IDesignElement.ActionEvent
        {
            get { return this._ActionEvent; }
            set { this._ActionEvent = value; }
        }

        MediaType IDesignElement.MediaType
        {
            get { return MediaType.Audio; }
        }

        InSysControlType IDesignElement.InSysControlType
        {
            get { return InSysControlType.InSysAudioBox; }
        }

        List<PlaylistItem> IDesignElement.Playlist
        {
            get
            {
                return this._elementProperties.Playlist;
            }
            set
            {
                this._elementProperties.Playlist = value;
                //foreach (var item in value)
                //{
                //    this._elementProperties.Playlist.Add(item);
                //}   
            }
        }

        ImageSource IDesignElement.ThumnailImageSource { get; set; }

         void IDesignElement.UpdatePlaylistItems()
        {
        }

        void IDesignElement.ResizePlaylistPreviewListBox(double width, double height)
        {
        }

        bool IDesignElement.IsSupportPlaylist { get; set; }

        bool IDesignElement.ISPlay
        {
            get { return _IsPlay; }
        }

        void IDesignElement.Play()
        {
            this.isPlaylistItemsTemp = this.IsPlaylistItems;//Play 상태에서는 무조건 디폴트 사진은 안보이도록 한다.
            this.IsPlaylistItems = true;
            //Visibility = Visibility.Collapsed;
            IDesignElement designElement = this as IDesignElement;
            
            if (designElement != null)
            {
                if (!_IsPlay)
                {
                    if (designElement.IsInLifeTime == true)
                    {
                        player = new ContentPlayer(this as IDesignElement);
                        player.Start();
                        _IsPlay = true;
                        isPlaylistItems = true;
                    }
                }
            }
        }

        void IDesignElement.Stop()
        {
            _audioPlayer.Stop();
            //Visibility = Visibility.Visible;

            if (player != null)
                player.Stop();

            _IsPlay = false;

            this.IsPlaylistItems = this.isPlaylistItemsTemp;//Stop 상태에서는 Play전의 값으로 재설정한다.
        }


        void IDesignElement.RePlay()
        {
            if (this._IsPlay == true)
                (this as IDesignElement).Stop();

            (this as IDesignElement).Play();
        }

        public Dictionary<string, object> Properties
        {
            get
            {
                ((IDesignElement)this).InitProperties();
                return _properties;
            }
            set
            {
                _properties = value;
                _properties = value;
                foreach (string name in _properties.Keys)
                {
                    PropertyClass.SetProperty(this, "IDesignElement", name, _properties[name]);
                }
            }
        }

        double IDesignElement.ControlTimeDuration
        {
            get;
            set;
        }
       
        public void InitProperties()
        {
            _properties = new Dictionary<string, object>();
            IDesignElement child = this as IDesignElement;
            _properties.Add("Name", child.Name);
            _properties.Add("Volume", child.Volume);
            _properties.Add("Mute", child.Mute);
            _properties.Add("ZIndex", child.ZIndex);
            
            _properties.Add("Type", child.Type);
            _properties.Add("Playlist", child.Playlist);
            
            _properties.Add("ControlTimeDuration", child.ControlTimeDuration);
            _properties.Add("IsApplyLifeTime", child.IsApplyLifeTime);
           
            _properties.Add("StartTime", child.StartTime);
            _properties.Add("EndTime", child.EndTime);
        }

        double IDesignElement.SlideViewWidth
        {
            get
            {
                return this._elementProperties.SlideViewWidth;
            }
            set
            {
                this._elementProperties.SlideViewWidth = value;
            }
        }

        double IDesignElement.SlideViewHeight
        {
            get
            {
                return this._elementProperties.SlideViewHeight;
            }
            set
            {
                this._elementProperties.SlideViewHeight = value;
            }
        }

        double IDesignElement.X
        {
            get
            {
                //return (double)this.GetValue(Canvas.LeftProperty);
                return this._elementProperties.X;
            }
            set
            {
                //this.SetValue(Canvas.LeftProperty, value);
                this._elementProperties.X = value;
            }
        }

        double IDesignElement.Y
        {
            get
            {
                //return (double)this.GetValue(Canvas.TopProperty);
                return this._elementProperties.Y;
            }
            set
            {
                //SetValue(Canvas.TopProperty, value);
                this._elementProperties.Y = value;
            }
        }

        double IDesignElement.Bottom
        {
            get
            {
                return (double)this.GetValue(Canvas.BottomProperty);
            }
            set
            {
                this.SetValue(Canvas.BottomProperty, value);
            }
        }

        double IDesignElement.Right
        {
            get
            {
                return (double)this.GetValue(Canvas.RightProperty);
            }
            set
            {
                SetValue(Canvas.RightProperty, value);
            }
        }

        private Visibility lifeTimeVisibility;
        public Visibility LifeTimeVisibility
        {
            get
            {
                return lifeTimeVisibility;
            }
            set
            {
                this.lifeTimeVisibility = value;
                OnPropertyChanged("LifeTimeVisibility");
            }
        }    

        int IDesignElement.ZIndex
        {
            get
            {
                //return (int)this.GetValue(Canvas.ZIndexProperty);
                return this._elementProperties.ZIndex;
            }
            set
            {
                //this.SetValue(Canvas.ZIndexProperty, value);
                this.ElementProperties.ZIndex = value;
            }
        }

        bool IDesignElement.IsApplyLifeTime
        {
            get { return this._elementProperties.IsApplyLifeTime; }
            set { this._elementProperties.IsApplyLifeTime = value; }
        }

        bool IDesignElement.IsInLifeTime
        {
            get;
            set;
        }

        TimeSpan IDesignElement.StartTime
        {
            get { return this._elementProperties.StartTime; }
            set { this._elementProperties.StartTime = value; }
        }

        TimeSpan IDesignElement.EndTime
        {
            get { return this._elementProperties.EndTime; }
            set { this._elementProperties.EndTime = value; }
        }

        private bool userVisibilitySet = false;
        bool IDesignElement.UserVisibilitySet
        {
            get { return userVisibilitySet; }
            set { userVisibilitySet = value; }
        }

        void IDesignElement.CheckLifeTime(TimeSpan playTimeSpan)
        {
            IDesignElement designeElement = this as IDesignElement;
            if (designeElement != null)
            {
                if (designeElement.IsApplyLifeTime == true)
                {
                    if (playTimeSpan >= designeElement.StartTime && playTimeSpan <= designeElement.EndTime)
                    {
                        if (designeElement.LifeTimeVisibility == System.Windows.Visibility.Visible)
                        {
                            designeElement.LifeTimeVisibility = System.Windows.Visibility.Visible;
                        }

                        if (designeElement.ISPlay == false)
                            designeElement.Play();
                        designeElement.IsInLifeTime = true;
                    }
                    else
                    {
                        if (designeElement.ISPlay == true)
                            designeElement.Stop();
                        designeElement.LifeTimeVisibility = System.Windows.Visibility.Hidden;
                        designeElement.IsInLifeTime = false;
                    }
                }
                else
                {
                    if (designeElement.LifeTimeVisibility == System.Windows.Visibility.Visible)
                    {
                        designeElement.LifeTimeVisibility = System.Windows.Visibility.Visible;
                    }
                    if (designeElement.ISPlay == false)
                        designeElement.Play();
                    designeElement.IsInLifeTime = true;
                }
            }
        }

        #region No used interface Members
        /// <summary>
        /// Gets or sets the horizontal ratio.
        /// </summary>
        public double HorizontalRatio
        {
            get;
            set;
        }

        TimeSpan IDesignElement.RefreshInterval
        {
            get;
            set;
        }
        /// <summary>
        /// Gets or sets the vertical ratio.
        /// </summary>
        public double VerticalRatio
        {
            get;
            set;
        }

        public new double BorderThickness
        {
            get;
            set;
        }

        double IDesignElement.BorderCorner
        {
            get;
            set;
        }

        int IDesignElement.HighlightStyle
        {
            get;
            set;
        }

        int IDesignElement.HighlightBrightness
        {
            get;
            set;
        }

        Style IDesignElement.ButtonStyle { get; set; }

        string IDesignElement.ButtonStyleKey { get; set; }

        public Stretch Stretch
        {
            get;
            set;
        }

        public double Volume
        {
            get
            {
                return _audioPlayer.Volume;
            }
            set
            {
                _audioPlayer.Volume = value;
            }
        }

        public bool Mute
        {
            get
            {
                return _audioPlayer.IsMuted;
            }
            set
            {
                _audioPlayer.IsMuted = value;
            }
        }

        public int[] StrokesLength
        {
            get;
            set;
        }

        public PenLineCap StrokeDashCap
        {
            get;
            set;
        }

        public char SeparatorChar
        {
            get;
            set;
        }

        public Point AspectRatio
        {
            get;
            set;
        }

        #endregion

        public Type Type
        {
            get { return GetType(); }
        }
        
        object IDesignElement.Content
        {
            get
            {
                if (_audioPlayer != null && _audioPlayer.Source != null)
                    return _audioPlayer.Source.AbsolutePath as string;

                return null;
            }
            set
            {
                if (IOMethod.ExistsFile(value as string) == true)
                {
                    _audioPlayer.Open(new Uri(value as string, UriKind.RelativeOrAbsolute));
                    _audioPlayer.Play();            
                }
            }
        }

        public IMediaFilesManager FileManager
        {
            get { return ((IMediaFilesManager)this.Parent); }
        }

        System.Drawing.Bitmap IDesignElement.GetThumbnail(int cx, int cy)
        {
            return null;
        }


        FitToPage IDesignElement.FitToPage
        {
            get { return this._elementProperties.FitToPage; }
            set { this._elementProperties.FitToPage = value; }

        }

        Alignment IDesignElement.Alignment
        {
            get;
            set;
        }

        TextWrapping IDesignElement.TextWrapping { get; set; }

        bool IDesignElement.Multiline { get; set; }

        #endregion

        private void UserControl_Unloaded(object sender, RoutedEventArgs e)
        {
            if (_audioPlayer != null)
            {
                _audioPlayer.Stop();
                _audioPlayer.Close();

                _audioPlayer = null;
            }
        }

        #region OnMouseDoubleClick
        protected override void OnMouseDoubleClick(MouseButtonEventArgs e)
        {
            base.OnMouseDoubleClick(e);

            if (ShowInSysControlPlayerEditorHandler != null)
            {
                ShowInSysControlPlayerEditorHandler(this as IDesignElement);

                if (UpdatedItemPropertyHandler != null)
                    UpdatedItemPropertyHandler(this, "Playlist", (this as IDesignElement).Playlist);
            }    
        }
        #endregion

        #region Set Delegate 콜백 함수


        void IDesignElement.SetDelegate(Delegate handler)
        {
            if (handler.GetType().Equals(typeof(UserDelegates.EventSettingInSysControlDelegate)) == true)
            {
                this.EventSettingInSysControlHandler = handler as UserDelegates.EventSettingInSysControlDelegate;
            }
            else if (handler.GetType().Equals(typeof(UserDelegates.ShowInSysControlPlayerEditorDelegate)) == true)
            {
                this.ShowInSysControlPlayerEditorHandler = handler as UserDelegates.ShowInSysControlPlayerEditorDelegate;
            }
            else if (handler.GetType().Equals(typeof(UserDelegates.UpdatedItemPropertyDelegate)) == true)
            {
                this.UpdatedItemPropertyHandler = handler as UserDelegates.UpdatedItemPropertyDelegate;
            } 
        }


        #endregion

        #region TestPlayAudio Test

        private void TestPlayAudio()
        {
            PlaylistItem temp = new PlaylistItem();
            temp.Content = "testSamples/009.mp3";
            temp.CONTENTSVALUE = "testSamples/009.mp3";
            temp.Duration = "<TimeSpan xmlns=\"clr-namespace:System;assembly=mscorlib\">00:00:10</TimeSpan>";
            temp.TIME = new TimeSpan(0, 0, 10);
            this._playlist.Add(temp);

            temp = new PlaylistItem();
            temp.Content = "testSamples/029.mp3";
            temp.CONTENTSVALUE = "testSamples/029.mp3";
            temp.Duration = "<TimeSpan xmlns=\"clr-namespace:System;assembly=mscorlib\">00:00:10</TimeSpan>";
            temp.TIME = new TimeSpan(0, 0, 10);
            this._playlist.Add(temp);

            temp = new PlaylistItem();
            temp.Content = "testSamples/03.mp3";
            temp.CONTENTSVALUE = "testSamples/03.mp3";
            temp.Duration = "<TimeSpan xmlns=\"clr-namespace:System;assembly=mscorlib\">00:00:10</TimeSpan>";
            temp.TIME = new TimeSpan(0, 0, 10);

            this._playlist.Add(temp);
        }
        #endregion

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        public virtual void OnPropertyChanged(params string[] propertyNames)
        {
            if (PropertyChanged == null)
                return;

            foreach (string propertyName in propertyNames)
            {
                try
                {
                    PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
                }
                catch { }
            }
        }

        #endregion
    }
}
