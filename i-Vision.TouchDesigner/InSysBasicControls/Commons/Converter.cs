﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Globalization;
using InSysBasicControls.Interfaces;
using System.Windows;
using System.Windows.Media;
using InSysBasicControls.InSysProperties;

namespace InSysBasicControls.Commons
{
    public class DesignElementConvert : IValueConverter
    {
        #region IValueConverter 멤버

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                double dValue = (double)value;
                return double.Parse(dValue.ToString("F", CultureInfo.InvariantCulture));
            }
            else
            {
                return 0;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }

        #endregion
    }

    public class PositionConvert : IValueConverter
    {
        #region IValueConverter 멤버

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                IDesignElement elemnt = value as IDesignElement;
                string ret = string.Format("{0}({1}*{2})", elemnt.Name, elemnt.Width, elemnt.Height);
                return ret;
            }
            else
            {
                return 0;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }

        #endregion
    }

    public class DoubleConverter : IValueConverter
    {
        #region IValueConverter 멤버

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                double elemnt = (double)value;
                string ret = string.Format("{0:F0}", elemnt);
                return ret;
            }
            else
            {
                return 0;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }

        #endregion
    }

    public class IntConverter : IValueConverter
    {
        #region IValueConverter 멤버

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                //int elemnt = (int)value;
                string ret = string.Format("{0:F0}", value);
                return ret;
            }
            else
            {
                return 0;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }

        #endregion
    }

    public class PlayImageConvert : IValueConverter
    {
        #region IValueConverter 멤버

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                bool isRun = (bool)value;
                string path = "";
                if (true == isRun)
                {
                    path = @"images/stop.png";
                }
                else
                {
                    path = @"images/play.png";
                }

                return path;
            }
            else
            {
                return @"images/play.png";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }

        #endregion
    }

    public class HighlightCornerRadiusConverter : IValueConverter
    {
        public object Convert(object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            CornerRadius corners = (CornerRadius)value;

            if (corners != null)
            {
                corners.BottomLeft = 0;
                corners.BottomRight = 0;
                return corners;
            }

            return null;
        }


        public object ConvertBack(object value, System.Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new System.NotImplementedException();
        }
    }

    public class ColorToAlphaColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            SolidColorBrush brush = (SolidColorBrush)value;

            if (brush != null)
            {
                Color color = brush.Color;
                color.A = byte.Parse(parameter.ToString());

                return color;
            }

            return Colors.Black; // make error obvious
        }


        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class BrightnessToColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            byte brightness = (byte)value;

            return Color.FromArgb(brightness, 255, 255, 255);
        }


        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class ImageClipRectConvert : IValueConverter
    {
        #region IValueConverter 멤버

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            ElementPropertyObject elementPropertyObject = value as ElementPropertyObject;
            if (elementPropertyObject != null)
            {
                RectangleGeometry output = new RectangleGeometry();
                output.Rect = new Rect(0, 0, elementPropertyObject.SlideViewWidth, elementPropertyObject.Height);
                return output.Rect;
            }


            return new Rect(0, 0, 100, 100); ;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }

        #endregion
    }
}
