﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Media;
using System.ComponentModel;

namespace InSysBasicControls.Commons
{
    public class PlaylistListBoxItemData : INotifyPropertyChanged
    {
        public ImageSource ImageSource
        {
            get;
            set;
        }

        public string ImagePath { get; set; }

        private double _width;
        public double Width
        {
            get { return this._width; }
            set { this._width = value; OnPropertyChanged("Width"); }
        }

        private double _height;
        public double Height
        {
            get { return this._height; }
            set { this._height = value; OnPropertyChanged("Height"); }
        }

        public double X
        {
            get;
            set;
        }

        public double Y
        {
            get;
            set;
        }

        public PlaylistListBoxItemData(ImageSource image, double x, double y, double width, double height, string imagepath)
        {
            this.ImageSource = image;           
            this.X = x;
            this.Y = y;
            this.Width = width;
            this.Height = height;
            this.ImagePath = imagepath;
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        public virtual void OnPropertyChanged(params string[] propertyNames)
        {
            if (PropertyChanged == null)
                return;

            foreach (string propertyName in propertyNames)
            {
                try
                {
                    PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
                }
                catch { }
            }
        }

        #endregion
    }
}
