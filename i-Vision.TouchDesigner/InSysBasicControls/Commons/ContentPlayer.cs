﻿using System;
using System.Collections.Generic;
using InSysTouchflowData;
using System.Windows.Threading;
using InSysBasicControls.Interfaces;

namespace InSysBasicControls.Commons
{
    public class ContentPlayer
    {
        #region Logging routines
		private NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();// NLog.LogManager.GetLogger("TouchDesignerLog");
        #endregion

        private object semaphore = null;
        bool bIsStop = false;

        /// <summary>
        /// Represents the playlist of element
        /// </summary>
        private List<PlaylistItem> _playlist;
        /// <summary>
        /// Represents the element
        /// </summary>
        private IDesignElement _designElement;

        /// <summary>
        /// Represents the timer uset for playing content
        /// </summary>
        private DispatcherTimer _timer;

        /// <summary>
        /// Indicates the index of current content
        /// </summary>
        private int _currentContentIndex;

        /// <summary>
        /// Represents the maximum content index (the items count in playlist - 1)
        /// </summary>
        private int _maxContentIndex;

        /// <summary>
        /// Initializes the new PLaylistPlayer class instance
        /// </summary>
        /// <param name="element">The element to play</param>
        public ContentPlayer(IDesignElement element)
        {
            _designElement = element;
            _playlist = element.Playlist;
            _currentContentIndex = 0;
            _maxContentIndex = _playlist.Count - 1;
        }

        /// <summary>
        /// Starts the loaded playlist playback
        /// </summary>
        public void Start()
        {
            if (_playlist.Count == 0) return;
            _timer = new DispatcherTimer();

            bIsStop = false;
            semaphore = new object();

            if (_playlist[_currentContentIndex].DurationAsTimeSpan.TotalSeconds > 0)
            {
                _timer.Interval = _playlist[_currentContentIndex].DurationAsTimeSpan;
                _timer.Tick += new EventHandler(_timer_Tick);
                _timer.Start();

                try
                {
                    _designElement.Content = _playlist[_currentContentIndex].Content;
                }                
                catch (Exception ex)
                {
                    logger.Error(_playlist[_currentContentIndex].Content, ex.Message);
                }
            }
            else
            {
                try
                {
                    _designElement.Content = _playlist[_currentContentIndex].Content;
                }
                catch (Exception ex)
                {
                    logger.Error(_playlist[_currentContentIndex].Content, ex.Message);
                }
            }
        }

        /// <summary>
        /// Is used to load next content and occures when timer ticks
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void _timer_Tick(object sender, EventArgs e)
        {
            lock (semaphore)
            {
                if (bIsStop) return;
                _timer.Stop();
            }
            _currentContentIndex++;
            if (_currentContentIndex > _maxContentIndex)
            {
                _currentContentIndex = 0;
            }

            if (_playlist[_currentContentIndex].DurationAsTimeSpan.TotalSeconds > 0)
            {
                lock (semaphore)
                {
                    if (bIsStop) return;

                    _timer.Interval = _playlist[_currentContentIndex].DurationAsTimeSpan;
                    _timer.Start();
                }
            }

            try
            {
                _designElement.Content = _playlist[_currentContentIndex].Content;
            }
            catch (Exception ex)
            {
                logger.Error(_playlist[_currentContentIndex].Content, ex.Message);
            }
        }

        /// <summary>
        /// Stops the loaded playlist playback
        /// </summary>
        public void Stop()
        {
            if (_playlist.Count == 0) return;

            lock (semaphore)
            {
                _timer.Stop();
                _currentContentIndex = 0;
                bIsStop = true;
            }
        }

        /// <summary>
        /// Static method that calculates and returns the total playback time of specified playlist
        /// </summary>
        /// <param name="playlist">The playlist to calculate total length of</param>
        /// <returns>Total playback time of the playlist</returns>
        public static TimeSpan GetTotalPlayingTime(List<PlaylistItem> playlist)
        {
            TimeSpan timeSpan = new TimeSpan();
            foreach (PlaylistItem item in playlist)
            {
                timeSpan.Add(item.DurationAsTimeSpan);
            }
            return timeSpan;
        }
    }
}
