﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace InSysBasicControls.Commons
{  
    /*
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public sealed class PropertyEditorAttribute : Attribute
    {
        private readonly Type editorType;
        /// <summary>
        /// Get the type of the editor to be used.
        /// </summary>
        public Type EditorType
        {
            get { return editorType; }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PropertyEditorAttribute"/> class.
        /// </summary>
        /// <param name="editorType">Type of the editor.</param>
        public PropertyEditorAttribute(Type editorType)
        {
            if (editorType == null) throw new ArgumentNullException("editorType");
            if (!(typeof(FrameworkElement).IsAssignableFrom(editorType)))
                throw new NotSupportedException("Only FrameworkElement based types are supported!");

            this.editorType = editorType;
        }
    }
     */ 
}
