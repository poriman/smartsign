﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media.Imaging;
using System.Threading;
using System.Windows.Media;
using System.IO;

namespace InSysBasicControls.Commons
{
    public class VideoThumbnailsCapture : DependencyObject
    {
        private delegate void setImageDelegate(BitmapFrame frame);

        public BitmapFrame Thumbnail
        {
            get { return (BitmapFrame)GetValue(ThumbnailProperty); }
            set { SetValue(ThumbnailProperty, value); }
        }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public static readonly DependencyProperty ThumbnailProperty =
        DependencyProperty.Register("Thumbnail", typeof(BitmapFrame), typeof(VideoThumbnailsCapture), new UIPropertyMetadata(null));

        public VideoThumbnailsCapture(string fileName, string filePath)
        {
            FileName = fileName;
            FilePath = filePath;
            ScreenCapture(TimeSpan.FromSeconds(2), new Uri(FilePath, UriKind.Relative));
        }

        private void ScreenCapture(TimeSpan timeSpan, Uri source)
        {
            ThreadPool.QueueUserWorkItem(delegate
            {
                ScreenCaptureWorker(timeSpan, source);
            });
        }

        private MediaPlayer OpenMediaPlayer(ref TimeSpan timeSpan, Uri source)
        {
            var player = new MediaPlayer { Volume = 0, ScrubbingEnabled = true };
            player.Open(source);
            player.Pause();
            player.Position = timeSpan;
            return player;
        }

        private void ScreenCaptureWorker(TimeSpan timeSpan, Uri source)
        {
            var player = OpenMediaPlayer(ref timeSpan, source);
            // letting media player initialize for 2 seconds
            //Thread.Sleep(2000); // consider adding logic for how long to sleep depending on file size.
            var frame = GetVideoThumbnail(player);

            //SaveVideoThumbnail(frame);

            // updating the DP of Thumbnail on a UI thread so that the UI will own the frame and be update correctly with no exceptions
            Dispatcher.Invoke(new setImageDelegate(setImage), frame);
        }

        private void setImage(BitmapFrame frame)
        {
            Thumbnail = frame;
        }

        private static Freezable GetVideoThumbnail(MediaPlayer player)
        {
            var rtb = new RenderTargetBitmap(player.NaturalVideoWidth,
            player.NaturalVideoHeight,
            96,
            96,
            PixelFormats.Pbgra32);
            DrawingVisual dv = new DrawingVisual();
            using (DrawingContext dc = dv.RenderOpen())
                dc.DrawVideo(player, new Rect(0,
                0,
                player.NaturalVideoWidth,
                player.NaturalVideoHeight));
            rtb.Render(dv);
            var frame = BitmapFrame.Create(rtb).GetCurrentValueAsFrozen();           
            return frame;
        }

        private void SaveVideoThumbnail(Freezable frame)
        {
            var encoder = new JpegBitmapEncoder();
            encoder.Frames.Add(frame as BitmapFrame);
            string filename = FilePath + ".jpg";
            using (var fs = new FileStream(filename, FileMode.Create))
                encoder.Save(fs);
        }
    }
}
