﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using InSysTouchflowData;
using InSysBasicControls.Views;

namespace InSysBasicControls.Commons
{
    public class ComponentTypeManager
    {
        public static Type GetComponentTypeFromCompType(CompType type)
        {
            try
            {
                switch (type)
                {
                    case CompType.Image:
                        return typeof(InSysImageBox);
                    case CompType.Media:
                        return typeof(InSysVideoBox);
                    case CompType.Streaming:
                        return typeof(InSysStreamingBox);   
                    case CompType.Text:
                        return typeof(InSysBasicTextBox);
                    case CompType.Label:
                        return typeof(InSysLabel);
                    case CompType.Scrolling_text:
                        return typeof(InSysScrollText);                 
                    case CompType.Rss:
                        return typeof(InSysRSS);
                    case CompType.Web:
                        return typeof(InSysWebControl);
                    case CompType.Rectagle:
                        return typeof(InSysRectangle);
                    case CompType.Digitalclock:
                        return typeof(InSysDigitalClock);
                    case CompType.Ellipse:
                        return typeof(InSysEllipse);
                    case CompType.Audio:
                        return typeof(InSysAudioBox);
                    case CompType.Analogue:
                        return typeof(InSysAnalogueClock);
                    case CompType.RectArea:
                        return typeof(InSysRectArea);
                    case CompType.Button:
                        return typeof(InSysBasicButton);
                    case CompType.SlideViewer:
                        return typeof(InSysSlideViewer);
                    case CompType.Unknown:
                        return null;
                }
            }
            catch
            {
            }

            return null;
        }

        public static CompType GetComponentTypeFromExtension(string sExtension)
        {
            try
            {
                string cleared_ext = sExtension.StartsWith(".") ? sExtension.ToLower().TrimStart('.') : sExtension.ToLower();

                if (cleared_ext.Equals("avi") ||
                    cleared_ext.Equals("wmv") ||
                    cleared_ext.Equals("mpeg") ||
                    cleared_ext.Equals("mpg") ||
                    cleared_ext.Equals("mkv") ||
                    cleared_ext.Equals("flv") ||
                    cleared_ext.Equals("asf") ||
                    cleared_ext.Equals("png"))
                    return CompType.Media;
                else if (cleared_ext.Equals("swf"))
                    return CompType.Flash;
                else if (cleared_ext.Equals("png") ||
                    cleared_ext.Equals("bmp") ||
                    cleared_ext.Equals("jpeg") ||
                    cleared_ext.Equals("jpg") ||
                    cleared_ext.Equals("jpe") ||
                    cleared_ext.Equals("jfif") ||
                    cleared_ext.Equals("gif") ||
                    cleared_ext.Equals("png") ||
                    cleared_ext.Equals("jpg") ||
                    cleared_ext.Equals("tiff") ||
                    cleared_ext.Equals("tif"))
                    return CompType.Image;
                else if (cleared_ext.Equals("mov") ||
                    cleared_ext.Equals("hdmov"))
                    return CompType.Quicktime;
                else if (cleared_ext.Equals("mp3") ||
                    cleared_ext.Equals("wma") ||
                    cleared_ext.Equals("ogg"))
                    return CompType.Audio;
                else if (cleared_ext.Equals("ppt"))
                    return CompType.Ppt;
            }
            catch
            {
            }
            return CompType.Unknown;
        }
    }
}
