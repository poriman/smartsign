﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Media3D;
using System.Windows.Media;
using System.Windows;

namespace InSysBasicControls.Interfaces
{
    public interface ITextContainer
    {
        void SetFontSize(double fontsize);
        void SetFontBrush(Brush fontcolor);
        void SetFontFamily(FontFamily family);
        void SetFontWeight(FontWeight fontWeight);
        double GetFontSize();
        Brush GetFontBrush();
        FontFamily GetFontFamily();
        FontWeight GetFontWeight();

        void SetDescFontSize(double fontsize);
        void SetDescFontBrush(Brush fontcolor);
        void SetDescFontFamily(FontFamily family);
        void SetDescFontWeight(FontWeight fontWeight);
        double GetDescFontSize();
        Brush GetDescFontBrush();
        FontFamily GetDescFontFamily();
        FontWeight GetDescFontWeight();
    }
}
