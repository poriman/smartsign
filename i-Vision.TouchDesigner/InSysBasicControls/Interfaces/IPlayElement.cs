﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InSysBasicControls.Interfaces
{
    public interface IPlayElement
    {
        void Play();
        void Stop();
        bool IsPlay { get; }//플레이 상태
    }
}
