﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;

namespace InSysBasicControls.Interfaces
{
    
    public interface IShape
    {
        void SetStroke(Brush brush);
        void SetFill(Brush brush);
        void SetStrokeDashArray(int[] sdarray);
        void SetStrokeDashCap(string strokedashcap);
        void SetStrokeThickness(double strokethickness);
        Brush GetStroke();
        Brush GetFill();
        int[] GetStrokeDashArray();
        string GetStrokeDashCap();
        double GetStrokeThickness();
    }
}
