﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Windows;

namespace InSysBasicControls.Interfaces
{
    public interface IDecoratable
    {
        void SetBorderBrush(Brush borderbrush);
        void SetBorderThickness(Thickness borderThickness);
        void SetBackround(Brush background);
        void SetCornerRadius(double cornerRadius);
        Brush GetBorderBrush();
        Brush GetBackgound();
        Thickness GetBorderThickness();
        double GetCornerRadius();
    }
}
