﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using InSysBasicControls.InSysProperties;

namespace InSysBasicControls.Interfaces
{
    public interface IElementProperties
    {
        ElementPropertyObject ElementProperties { get; set; }

        void InitInSysElementProperties();
    }
}
