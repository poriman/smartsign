﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Xml;
using System.IO;

namespace InSysBasicControls.Editors
{
	/// <summary>
	/// WeatherSetter.xaml에 대한 상호 작용 논리
	/// </summary>
	public partial class WeatherSetter : Window
	{
		const string MANUAL_SETTING = "[Manual]";

		XmlDocument doc = null;

		public WeatherSetter()
		{
			InitializeComponent();
			
			string path = AppDomain.CurrentDomain.BaseDirectory + "settings\\Cities.xml";
			if (!File.Exists(path))
			{
                MessageBox.Show(String.Format(Cultures.DesignerResources.Resources.mbErrorCityDefinition, path), "Designer", MessageBoxButton.OK, MessageBoxImage.Error);
				return;
			}

			try
			{
				doc = new XmlDocument();
				doc.Load(path);

				XmlNodeList nation_nodelist = doc.SelectNodes("//child::Nation");

				if (nation_nodelist != null)
				{
					cbNation.Items.Clear();
					foreach (XmlNode node in nation_nodelist)
					{
						ComboBoxItem item = new ComboBoxItem();
						item.Content = node.Attributes["Name"].Value;
						item.DataContext = node.Attributes["Value"].Value;
						cbNation.Items.Add(item);
					}
					if (cbNation.Items.Count > 0)
					{
						cbNation.SelectedIndex = 0;
					}
				}
			}
			catch (Exception e)
			{
                MessageBox.Show(e.Message, Cultures.DesignerResources.Resources.titleTouchDesignerWindow);
			}
		}
		string loadCityname = "";

		public void Load(string data)
		{
			try
			{
				if(!string.IsNullOrEmpty(data))
				{
					if (data.Equals(MANUAL_SETTING))
					{
						cbManualSetting.IsChecked = true;
						cbManualSetting_Click(cbManualSetting, null);
					}
					else
					{
						cbManualSetting.IsChecked = false;
						cbManualSetting_Click(cbManualSetting, null);
						
						string[] arrData = data.Split(',');
						loadCityname = data;

						cbNation.SelectedItem = FindComboboxValue(cbNation, arrData[1].Trim());
						cbCity.SelectedItem = FindComboboxValue(cbCity, arrData[0]);
					}
				}

			}
			catch {}

			this.ShowDialog();
		}

		private ComboBoxItem FindComboboxValue(ComboBox cb, string value)
		{
			try
			{
				foreach (ComboBoxItem item in cb.Items)
				{
					if (item.DataContext.Equals(value))
					{
						return item;
					}
				}
			}
			catch { return null; }

			return null;
		}

		private void bOk_Click(object sender, RoutedEventArgs e)
		{
			bool bChecked = cbManualSetting.IsChecked == null ? false : cbManualSetting.IsChecked.Value;
			if (bChecked)
			{
				loadCityname = MANUAL_SETTING;
			}
			else
			{
				loadCityname = String.Format("{0}, {1}", ((ComboBoxItem)cbCity.SelectedItem).DataContext.ToString(),
					((ComboBoxItem)cbNation.SelectedItem).DataContext.ToString());
			}
			this.Close();
		}

		public string GetContent()
		{
			return loadCityname;
		}

		private void bCancel_Click(object sender, RoutedEventArgs e)
		{
			this.Close();
		}

		private void bApply_Click(object sender, RoutedEventArgs e)
		{
			bool bChecked = cbManualSetting.IsChecked == null ? false : cbManualSetting.IsChecked.Value;
			if (bChecked)
			{
				loadCityname = MANUAL_SETTING;
			}
			else
			{
				loadCityname = String.Format("{0}, {1}", ((ComboBoxItem)cbCity.SelectedItem).DataContext.ToString(),
					((ComboBoxItem)cbNation.SelectedItem).DataContext.ToString());
			}
		}

		private void cbNation_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			string sNation = ((ComboBoxItem)cbNation.SelectedItem).DataContext.ToString();
			
			try {
				XmlNode nation_node = doc.SelectSingleNode("//child::Nation[attribute::Value=\""+ sNation + "\"]");

				if (nation_node != null)
				{
					cbCity.Items.Clear();
					foreach (XmlNode node in nation_node.ChildNodes)
					{
						ComboBoxItem item = new ComboBoxItem();
						item.Content = node.Attributes["Name"].Value;
						item.DataContext = node.Attributes["Value"].Value;
						cbCity.Items.Add(item);
					}
					if (cbCity.Items.Count > 0)
					{
						cbCity.SelectedIndex = 0;
					}
				}
			}
			catch {}
		}

		private void cbManualSetting_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				bool bChecked = cbManualSetting.IsChecked.Value;

				cbCity.IsEnabled = cbNation.IsEnabled = !bChecked;
			}
			catch {}

		}
	}
}
