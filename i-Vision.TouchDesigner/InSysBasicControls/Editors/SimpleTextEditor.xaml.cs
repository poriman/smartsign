﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace InSysBasicControls.Editors
{
    /// <summary>
    /// Interaction logic for SimpleTextEditor.xaml
    /// </summary>
    public partial class SimpleTextEditor : Window, IDisposable
    {
        public List<Inline> Inlines = new List<Inline>();
        public SimpleTextEditor()
        {
            InitializeComponent();

            InputTextBox.Text = "";
        }

        public void SetText(List<Inline> inlines)
        {
            for (int i= 0; i< inlines.Count; i++)
            {
                if (i != 0)
                {
                    Run run = (Run)inlines[i];
                    InputTextBox.Text += Environment.NewLine;
                    InputTextBox.Text += run.Text;
                }
                else
                {
                    Run run = (Run)inlines[i];
                    InputTextBox.Text += run.Text;
                }
                
            }
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            Inlines.Clear();
            for(int i = 0 ;i <InputTextBox.LineCount ; i++)
            {
                Inlines.Add(new Run(InputTextBox.GetLineText(i)));
            }
            
            this.DialogResult = true;            
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;            
        }

        #region IDisposable Members

        public void Dispose()
        {
            Inlines.Clear();
            Inlines = null;
            this.Close();
        }

        #endregion
    }
}
