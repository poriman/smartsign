﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.IO;
using QuartzTypeLib;
using System.Windows.Markup;
using InSysTouchflowData;
using InSysBasicControls.Interfaces;
using UtilLib.Etc;
using InSysBasicControls.Commons;
using InSysBasicControls.PropertyControls;
using InSysBasicControls.PropertyControls;

namespace InSysBasicControls.Editors
{   
    /// <summary>
    /// Interaction logic for PlaylistEditor.xaml
    /// </summary>
    public partial class PlaylistEditor : Window
    {
        #region Editors
        System.Windows.Forms.OpenFileDialog _openDialog;

        public bool IsEditOK
        {
            get;
            private set;
        }
        TimeSpan duration;
        TextEditor _textEditor;
        UrlSetter _urlEditor;
        GmtSetter _gmtSetter;
		WeatherSetter _weatherSetter;
        ScrollTextSetter _stSetter;
		RssTextSetter _rtSetter;
        #endregion

        private List<PlaylistItem> _orgplaylist;
        private List<PlaylistItem> _playlist;
        private Type _elementType;

        /// <summary>
        /// Initializes the new PlaylistEditor class instance
        /// </summary>
        public PlaylistEditor()
        {
            InitializeComponent();
            IsEditOK = false;
//             this.dragMgr = new ListViewDragDropManager<PlaylistItem>(ContentsListBox);
//             this.dragMgr.ListView = ContentsListBox;
//             this.dragMgr.ShowDragAdorner = true;

            _openDialog = new System.Windows.Forms.OpenFileDialog();            
        }      

        /// <summary>
        /// Shows the playlist editor
        /// </summary>
        /// <param name="element">The element to load the playlist of</param>
        public bool? ShowEditor(IDesignElement element, List<PlaylistItem> playlist)
        {            
            IsEditOK = false;
            _orgplaylist = playlist;
            _playlist = new List<PlaylistItem>();
            if (_orgplaylist != null)
            {
                foreach (PlaylistItem item in _orgplaylist)
                {
                    PlaylistItem temp = new PlaylistItem();
                    temp.Content = item.Content;
                    temp.CONTENTSVALUE = item.CONTENTSVALUE;
                    temp.Duration = item.Duration;
                    temp.TIME = item.TIME;

                    _playlist.Add(temp);
                }
            }
            _elementType        = element.Type;
			elementType.Content = GetComponentStr(element.Type);
            elementName.Content = element.Name;
            ContentsListBox.ItemsSource = null;
            Fill();
            return this.ShowDialog();
        }

        /// <summary>
        /// Fills the playlist items container with the loaded playlist data
        /// </summary>
        private void Fill()
        {
            try
            {
                PlaylistItemArry task = new PlaylistItemArry();                
                foreach (PlaylistItem item in _playlist)
                {
                    item.Content = item.Content;
                    item.CONTENTSVALUE = item.CONTENTSVALUE;
                    item.TIME = item.DurationAsTimeSpan;                    
                    task.Add(item);
                }

                ContentsListBox.ItemsSource = task;
            }
            catch
            {
            }
        }

        /// <summary>
        /// The event handler that is used for handling events of OK/Apply/Cancel buttons block
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OKApplyCancelhandler(object sender, RoutedEventArgs e)
        {
            Button b = sender as Button;
            switch (b.Name)
            {
                case "OK":
                    {
						if (ValidationCheck())
							Hide();
                        IsEditOK = true;
                        break;
                    }
                case "Apply":
                    {
						if (ValidationCheck())
						{
							_orgplaylist.Clear();
							List<PlaylistItem> tempArry = GetPlaylist();
							foreach (PlaylistItem item in tempArry)
							{
								PlaylistItem temp = new PlaylistItem();
								temp.Content = item.Content;
								temp.CONTENTSVALUE = item.CONTENTSVALUE;
								temp.Duration = item.Duration;
								temp.TIME = item.TIME;

								_orgplaylist.Add(temp);
							}
                            IsEditOK = true;
                        }       
                        break;
                    }
                case "Cancel":
                    {                        
                        Hide();
                        break;
                    }
                default: break;
            }
        }

		private bool ValidationCheck()
		{
			PlaylistItemArry task = ContentsListBox.ItemsSource as PlaylistItemArry;
			int nItemCount = task.Count;

			foreach (PlaylistItem item in task)
			{
				if (item.Content != "No media" && item.Content != "")
				{
					if (nItemCount != 1 && item.TIME.TotalSeconds == 0)
					{
                        return MessageBoxResult.Yes == MessageBox.Show(Cultures.DesignerResources.Resources.mbDurationIsUnlimit, Cultures.DesignerResources.Resources.titleTouchDesignerWindow, MessageBoxButton.YesNo, MessageBoxImage.Warning);
					}
				}
			}

			return true;
		}

        public List<PlaylistItem> GetPlaylist()
        {
            if (true == IsEditOK)
            {
                _playlist.Clear();
                PlaylistItemArry task = ContentsListBox.ItemsSource as PlaylistItemArry;
                foreach (PlaylistItem item in task)
                {
                    if (item.Content != "No media" && item.Content != "")
                    {
                        _playlist.Add(item);
                    }
                }
                return _playlist;
            }

            return _orgplaylist;
        }

        private void AddItem_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                PlaylistItemArry task = ContentsListBox.ItemsSource as PlaylistItemArry;
                task.Add(new PlaylistItem());
                ContentsListBox.ItemsSource = task;
            }
            catch
            {
            }
        }

        private void removeItem_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                PlaylistItem selectitem = ContentsListBox.SelectedItem as PlaylistItem;

                if (selectitem != null)
                {
                    PlaylistItemArry task = ContentsListBox.ItemsSource as PlaylistItemArry;
                    int index = 0; bool isSelect = false;
                    foreach (PlaylistItem screen in task)
                    {
                        if (screen == selectitem)
                        {
                            isSelect = true;
                            break;
                        }
                        index++;
                    }
                    if (isSelect)
                    {
                        task.RemoveAt(index);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
               // logger.Error(ex + " " + ex.StackTrace);
            }
        }

        private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            
        }

        private void upItem_Click(object sender, RoutedEventArgs e)
        {
            if (ContentsListBox.SelectedItem != null)
            {
                PlaylistItem screen = (PlaylistItem)ContentsListBox.SelectedItem;
                ChangeItem(screen, true);
            }
        }

        private void downItem_Click(object sender, RoutedEventArgs e)
        {
            if (ContentsListBox.SelectedItem != null)
            {
                PlaylistItem screen = (PlaylistItem)ContentsListBox.SelectedItem;
                ChangeItem(screen, false);
            }
        }

        private void ChangeItem(PlaylistItem data, bool isUp)
        {
            try
            {
                PlaylistItemArry task = ContentsListBox.ItemsSource as PlaylistItemArry;
                int index = 0;
                bool isSelect = false;
                foreach (PlaylistItem screen in task)
                {
                    if (screen == data)
                    {
                        isSelect = true;
                        break;
                    }
                    index++;
                }
                if (isSelect)
                {
                    if (isUp)
                    {
                        if (index == 0) return;
                        task.RemoveAt(index);
                        index = index - 1;

                    }
                    else
                    {
                        if (index == task.Count - 1) return;
                        task.RemoveAt(index);
                        index = index + 1;
                    }

                    task.Insert(index, data);
                    ContentsListBox.SelectedIndex = index;
                }
            }
            catch (Exception ex)
            {
                throw ex;
               // logger.Error(ex + " " + ex.StackTrace);
            }
        }

        private void durationUI_SelectedTimeChanged(object sender, TimeSelectedChangedRoutedEventArgs e)
        {
            try
            {
                TimePicker ctrl = (TimePicker)sender;
                PlaylistItem item = (PlaylistItem)ctrl.DataContext;
                item.TIME = ctrl.SelectedTime;
                item.Duration = XamlWriter.Save(ctrl.SelectedTime);
            }
            catch
            { }
        }

        private void btn_Infinity_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ContentControl targetContentCtrl = (ContentControl)e.OriginalSource;
                PlaylistItem oldItem = (PlaylistItem)targetContentCtrl.DataContext;
                PlaylistItem targetitem = oldItem;
                targetitem.Duration = XamlWriter.Save(new TimeSpan(0, 0, 0));
                targetitem.TIME = new TimeSpan(0, 0, 0);
                targetitem.IsInfinity = true;
                ChangeItems(oldItem, targetitem);
            }
            catch { }
        }

		private String GetComponentStr(Type type)
		{
			try
			{
				switch (type.Name)
                {
                    case "InSysSlideViewer":
                        {
                            return Cultures.DesignerResources.Resources.toolTipImage;
                        }
                    case "InSysAnalogueClock":
						{
                            return Cultures.DesignerResources.Resources.toolTipAnalogueClock;
						}
                    case "InSysDigitalClock":
						{
                            return Cultures.DesignerResources.Resources.toolTipDigitalClock;
						}
                    case "InSysDate":
						{
                            return Cultures.DesignerResources.Resources.toolTipDate;
						}
                    case "InSysEllipse":
						{
                            return Cultures.DesignerResources.Resources.toolTipEllipse;
						}
                    case "InSysImageBox":
						{
                            return Cultures.DesignerResources.Resources.toolTipImage;
						}
                    case "InSysBasicButton":
                        {
                            return Cultures.DesignerResources.Resources.toolTipBasicButton;
                        }
                    case "InSysAudioBox":
						{
                            return Cultures.DesignerResources.Resources.toolTipAudio;
						}
                    case "InSysVideoBox":
						{
                            return Cultures.DesignerResources.Resources.toolTipMedia;
						}
                    case "InSysStreamingBox":
                        {
                            return Cultures.DesignerResources.Resources.toolTipStreaming;
                        }
					case "RectangleComponent":
						{
                            return Cultures.DesignerResources.Resources.toolTipRectangle;
						}
                    case "InSysRSS":
						{
                            return Cultures.DesignerResources.Resources.toolTipRss;
						}
                    case "InSysScrollText":
						{
                            return Cultures.DesignerResources.Resources.toolTipScrollText;
						}
					case "TextComponent":
						{
                            return Cultures.DesignerResources.Resources.toolTipText;
						}
                    case "InSysWebControl":
						{
                            return Cultures.DesignerResources.Resources.toolTipWeb;
						}
					case "WeatherComponent":
						{
                            return Cultures.DesignerResources.Resources.toolTipWeather;
						}
					case "PptComponent":
						{
                            return Cultures.DesignerResources.Resources.toolTipPpt;
						}
					case "StreamingComponent":
						{
                            return Cultures.DesignerResources.Resources.toolTipStreaming;
						}
					case "TVComponent":
						{
                            return Cultures.DesignerResources.Resources.toolTipTV;
						}
                    case "InSysFlashBox":
                        {
                            return Cultures.DesignerResources.Resources.toolTipFlash;
                        }
				}
			}
			catch 
			{
			}
			return "Unknown";
		}

        private void filePath_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ContentControl targetContentCtrl = (ContentControl)e.OriginalSource;
                PlaylistItem oldItem = (PlaylistItem)targetContentCtrl.DataContext;
                PlaylistItem targetitem = oldItem;

                switch (_elementType.Name)
                {                   
                    case "InSysSlideViewer":
                        {
                            _openDialog.Filter = "All supported|*.jpg;*.bmp;*.gif;*.png|JPEG files|*.jpg|BMP files| *.bmp|GIF files|*.gif|PNG files|*.png";
                            _openDialog.Title = "Open an image";

                            _openDialog.ShowDialog();
                            if (_openDialog.FileName != "" && File.Exists(_openDialog.FileName))
                            {
                                FileInfo fi = new FileInfo(_openDialog.FileName);
                                targetitem.Content = fi.FullName;
                                targetitem.CONTENTSVALUE = fi.Name;
                            }
                            break;
                        }                        
                    case "InSysAnalogueClock":
                    case "InSysDigitalClock":
                        {
                            _gmtSetter = new GmtSetter();
                            _gmtSetter.Load(targetitem.Content);
                            targetitem.Content = _gmtSetter.GetContent();
                            targetitem.CONTENTSVALUE = GmtManager.GetTimeZoneFromIndexInString(targetitem.Content).ToString();
                            break;
                        }
                    case "InSysDate":
                        {
                            break;
                        }
                    case "InSysEllipse":
                        {
                            break;
                        }
                    case "InSysImageBox":
                        {
							_openDialog.Filter = "All supported|*.jpg;*.bmp;*.gif;*.png|JPEG files|*.jpg|BMP files| *.bmp|GIF files|*.gif|PNG files|*.png";
                            _openDialog.Title = "Open an image";
                            
                            _openDialog.ShowDialog();
                            if (_openDialog.FileName != "" && File.Exists(_openDialog.FileName))
                            {
                                FileInfo fi = new FileInfo(_openDialog.FileName);
                                targetitem.Content = fi.FullName;
                                targetitem.CONTENTSVALUE = fi.Name;
                            }
                            break;
                        }
                    case "InSysAudioBox":
                        {
                            string filter = "MP3|*.mp3|WMA|*.wma|ogg|*.ogg";
                            OpenFileDialog(targetitem, "Open Audio", filter);

                            Fill(targetitem);
                            break;
                        }
                    case "InSysVideoBox":
                        {
                            string filter = "Video (WMV, AVI, MPG/MPEG, QuickTime)| *.wmv; *.avi; *.mpg;*.mpeg; *.mov;*.hdmov";
                            OpenFileDialog(targetitem, "Open Video", filter);
                            Fill(targetitem);
                            break;
                        }
                    case "InSysBasicButton":
                        break;
                    case "InSysRectangle":
                        {
                            break;
                        }
                    case "InSysRectArea":
                        break;
                    case "InSysRSS":
						{
							_rtSetter = new RssTextSetter();
                            _rtSetter.Load(ScrollTextInfo.DeserializeScrollTextInfo(targetitem.Content));
							targetitem.CONTENTSVALUE = _rtSetter.GetContent();
							if (targetitem.CONTENTSVALUE == new ScrollTextInfo())
							{
								targetitem.CONTENTSVALUE = "No media";
								targetitem.Content = "";
							}

                            targetitem.Content = ScrollTextInfo.SerializeScrollTextInfo((ScrollTextInfo)targetitem.CONTENTSVALUE);
							targetitem.CONTENTSVALUE = targetitem.Content;
							break;
						}
                    case "InSysScrollText":
                        {
                            _stSetter = new ScrollTextSetter();
                            _stSetter.Load(ScrollTextInfo.DeserializeScrollTextInfo(targetitem.Content));
                            targetitem.CONTENTSVALUE = _stSetter.GetContent();
                            if (targetitem.CONTENTSVALUE == new ScrollTextInfo())
                            {
                                targetitem.CONTENTSVALUE = "No media";
                                targetitem.Content = "";
                            }

                            targetitem.Content = ScrollTextInfo.SerializeScrollTextInfo((ScrollTextInfo)targetitem.CONTENTSVALUE);
                            targetitem.CONTENTSVALUE = targetitem.Content;
                            break;
                        }
                    case "InSysBasicTextBox":
                        {
                            _textEditor = new TextEditor();
                            _textEditor.Load(targetitem.Content);
                            targetitem.Content = _textEditor.GetDocument();
                            targetitem.CONTENTSVALUE = "Text";
                            break;
                        }
                    case "InSysLabel":
                        {
                            _textEditor = new TextEditor();
                            _textEditor.Load(targetitem.Content);
                            targetitem.Content = _textEditor.GetDocument();
                            targetitem.CONTENTSVALUE = "Text";
                            break;
                        }
                    case "InSysWebControl":
                        {
                            _urlEditor = new UrlSetter();
                            _urlEditor.Load(targetitem.Content);
                            targetitem.Content = _urlEditor.GetContent().ToString();
                            targetitem.CONTENTSVALUE = targetitem.Content;
                            break;
                        }
                    case "WeatherComponent":
						{
							_weatherSetter = new WeatherSetter();
							_weatherSetter.Load(targetitem.Content);
							targetitem.Content = _weatherSetter.GetContent();
							targetitem.CONTENTSVALUE = targetitem.Content;
							break;
						}              
					case "TVComponent":
						{
							TVChannelSelector dlg = new TVChannelSelector();
							dlg.Load(targetitem.Content);
							if (dlg.ISAPPLY)
							{
								targetitem.Content = dlg.GetContent();
								targetitem.CONTENTSVALUE = targetitem.Content;

								Fill(targetitem);
							} 
							break;
						}
					case "InSysStreamingBox":
						{
							MediaSelector dlg = new MediaSelector();
							dlg.Load(targetitem.Content);
							if(dlg.ISAPPLY)
							{
								targetitem.Content = dlg.GetContent();
								targetitem.CONTENTSVALUE = targetitem.Content;

								Fill(targetitem);
							}
							break;
						}
                    case "PptComponent":
                        {
                            _openDialog.Filter = "PowerPoint|*.ppt";
                            _openDialog.ShowDialog();
                            if (_openDialog.FileName != "" && File.Exists(_openDialog.FileName))
                            {
                                targetitem.Content = _openDialog.FileName;
                                targetitem.CONTENTSVALUE = targetitem.Content;
                            }
                            break;
                        }
                    case "InSysFlashBox":
                        {
                            string filter = "SWF|*.swf";
                            OpenFileDialog(targetitem, "Open Flash", filter);

                            Fill(targetitem);
                        }
                        break;
                    default: break;
                }

                ChangeItems(oldItem, targetitem);
            }
            catch (Exception ex)
            {
                string errmsg = ex.Message.ToString();
            }
          //*/
        }

        private bool ChangeItems(PlaylistItem oldData, PlaylistItem newData)
        {
            try
            {
                PlaylistItemArry task = ContentsListBox.ItemsSource as PlaylistItemArry;
                int index = 0;
                bool isSelect = false;
                foreach (PlaylistItem screen in task)
                {
                    if (screen == oldData)
                    {
                        isSelect = true;
                        break;
                    }
                    index++;
                }
                if (isSelect)
                {
                    task.RemoveAt(index);
                    task.Insert(index, newData);
                    return true;
                }
            }
            catch (Exception ex)
            {
                throw ex;
               // logger.Error(ex + " " + ex.StackTrace);
            }
            return false;
        }

		public static PlaylistItem MakePlaylistItem(string filename)
		{
			try
			{
                FileInfo fi = new FileInfo(filename);
                string s = fi.Name;
                CompType comp_type = ComponentTypeManager.GetComponentTypeFromExtension(System.IO.Path.GetExtension(s).ToLower());

				PlaylistItem item = new PlaylistItem();

				item.CONTENTSVALUE = item.Content = s;

				TimeSpan duration = new TimeSpan();

				switch (comp_type)
				{
					case CompType.Media:
					case CompType.Quicktime:
						{
							try
							{
								FilgraphManager filterGraph = new FilgraphManager();
								filterGraph.RenderFile(filename);
								IMediaPosition pos = filterGraph as IMediaPosition;
								duration = new TimeSpan(0, 0, (int)pos.Duration);
							}
							catch
							{
								try
								{
                                    MediaElement me = new MediaElement();
                                    me.Source = new Uri(filename);
                                    duration = me.NaturalDuration.TimeSpan;         
								}
								catch { }
							}

						}
						break;
					case CompType.Audio:
						{
							try
							{
								MP3Header header = new MP3Header();
								header.ReadMP3Information(filename);
								duration = new TimeSpan(0, 0, header.intLength);
							}
							catch { }
						}
						break;
				}
				
				item.Duration = XamlWriter.Save(duration);

				return item;
			}
			catch
			{
			}

			return null;
		}

        private void OpenFileDialog(PlaylistItem targetitem, string title, string filter)
        {
            System.Windows.Forms.OpenFileDialog OpenFileDialog = new System.Windows.Forms.OpenFileDialog();
            OpenFileDialog.Filter = filter;// 
            OpenFileDialog.Title = title;
            if (OpenFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                FileInfo fi = new FileInfo(OpenFileDialog.FileName);
                string filename = fi.Name;
                string filepath = fi.FullName;

                CompType comp_type = ComponentTypeManager.GetComponentTypeFromExtension(System.IO.Path.GetExtension(filename).ToLower());
                switch(comp_type)
				{
					case CompType.Media:
					{
						try
						{
							FilgraphManager filterGraph = new FilgraphManager();
							filterGraph.RenderFile(OpenFileDialog.FileName);
							IMediaPosition pos = filterGraph as IMediaPosition;
							duration = new TimeSpan(0, 0, (int)pos.Duration);
						}
						catch 
						{
							try
							{								
                                MediaElement me = new MediaElement();
                                me.Source = new Uri(OpenFileDialog.FileName);
                                duration = me.NaturalDuration.TimeSpan;         
							}
							catch { }
						} 

					}
					break;
                    //case CompType.Quicktime:
                    //{
                    //    try
                    //    {
                    //        DigitalSignage.Controls.QuickTimeControl qc = new DigitalSignage.Controls.QuickTimeControl();
                    //        tempcontrol.Child = qc;
                    //        duration = qc.GetQuickTimeMovieDuration(OpenFileDialog.FileName);
                    //        qc.Dispose();
                    //        qc = null;
                    //        tempcontrol.Child = null;
                    //    }
                    //    catch { }
                    //}
                    //    break;
					case CompType.Audio:
						{
							try
							{
								MP3Header header = new MP3Header();
								header.ReadMP3Information(OpenFileDialog.FileName);
								duration = new TimeSpan(0, 0, header.intLength);
							}
							catch { }
						}
						break;
					case CompType.Ppt:
						break;
					case CompType.Flash:
						break;
					case CompType.Image:
						break;

				}
                targetitem.Content = filepath;
                targetitem.CONTENTSVALUE = filename;
                targetitem.Duration = GetDuration(duration);
                targetitem.TIME = targetitem.DurationAsTimeSpan;
                if (targetitem.TIME != new TimeSpan(0, 0, 0))
                    targetitem.IsInfinity = false;
            }

        }

        public string GetDuration(TimeSpan duration)
        {
            if (duration != null)
                return XamlWriter.Save(duration);
            else
                return XamlWriter.Save(new TimeSpan());
        }

        /// <summary>
        /// Fills the ItemView fields corresponding to the loaded PlaylistItem instance
        /// </summary>
        private void Fill(PlaylistItem targetitem)
        {
            switch (_elementType.Name)
            {
                case "InSysSlideViewer":
                    {
                        if (targetitem.Content != "")
                        {
                            targetitem.CONTENTSVALUE = targetitem.Content;
                        }
                        else
                        {
                            targetitem.CONTENTSVALUE = "No media";
                        }
                        // string images = "*.PNG;*.JPG;*.GIF;*.BMP;*.JPEG;";
                        // _openDialog.Filter = string.Format("Image files({0})|{0}", images);

                        _openDialog.Filter = "All supported|*.jpg;*.bmp;*.gif;*.png|JPEG files|*.jpg|BMP files| *.bmp|GIF files|*.gif|PNG files|*.png";
                        _openDialog.Title = "Open an image";
                        break;
                    }
                    break;
                case "InSysAnalogueClock":
                case "InSysDigitalClock":
                    {
                        if (targetitem.Content != "")
                        {
                            targetitem.CONTENTSVALUE = GmtManager.GetTimeZoneFromIndexInString(targetitem.Content);
                        }
                        else
                        {
                            targetitem.CONTENTSVALUE = "No media";
                        }
                        break;
                    }
                case "InSysDate":
                    {
                        break;
                    }
                case "InSysEllipse":
                    {
                        break;
                    }
                case "InSysImageBox":
                    {
                        if (targetitem.Content != "")
                        {
                            targetitem.CONTENTSVALUE = targetitem.Content;
                        }
                        else
                        {
                            targetitem.CONTENTSVALUE = "No media";
                        }
                        // string images = "*.PNG;*.JPG;*.GIF;*.BMP;*.JPEG;";
                        // _openDialog.Filter = string.Format("Image files({0})|{0}", images);

						_openDialog.Filter = "All supported|*.jpg;*.bmp;*.gif;*.png|JPEG files|*.jpg|BMP files| *.bmp|GIF files|*.gif|PNG files|*.png";
                        _openDialog.Title = "Open an image";
                        break;
                    }
                case "InSysAudioBox":
                case "InSysVideoBox":
				case "TVComponent":
                case "InSysFlashBox":
                case "InSysStreamingBox":
                    {
                        if (targetitem.Content != "")
                        {
                            targetitem.CONTENTSVALUE = targetitem.Content;
                        }
                        else
                        {
                            targetitem.CONTENTSVALUE = "No media";
                        }
                        break;
                    }
                case "InSysRectangle":
                    {
                        if (targetitem.Content != "")
                        {
                            targetitem.CONTENTSVALUE = targetitem.Content;
                        }
                        else
                        {
                            targetitem.CONTENTSVALUE = "No media";
                        }
                        break;
                    }
                case "InSysRectArea":                   
                    break;
                case "InSysBasicButton":
                    break;
                case "InSysRSS":
                case "InSysScrollText":
                    {
                        if (targetitem.Content != "")
                        {
                            targetitem.CONTENTSVALUE = ScrollTextInfo.DeserializeScrollTextInfo(targetitem.Content);
                        }
                        else
                        {
                            targetitem.CONTENTSVALUE = "No media";
                        }
                        break;
                    }
                case "InSysBasicTextBox":
                    {
                        if (targetitem.Content != "")
                        {
                            targetitem.CONTENTSVALUE = "Text";
                        }
                        else
                        {
                            targetitem.CONTENTSVALUE = "No media";
                        }
                        break;
                    }
                case "InSysLabel":
                    {
                        if (targetitem.Content != "")
                        {
                            targetitem.CONTENTSVALUE = "Label";
                        }
                        else
                        {
                            targetitem.CONTENTSVALUE = "No media";
                        }
                        break;
                    }
                case "InSysWebControl":
                    {
                        if (targetitem.Content != "")
                        {
                            targetitem.CONTENTSVALUE = targetitem.Content;
                        }
                        else
                        {
                            targetitem.CONTENTSVALUE = "about:blank";
                            targetitem.Content = "about:blank";
                        }
                        break;
                    }
				case "WeatherComponent":
					{
						if (targetitem.Content != "")
						{
							targetitem.CONTENTSVALUE = targetitem.Content;
						}
						else
						{
							targetitem.CONTENTSVALUE = "Manual Setting";
							targetitem.Content = "Manual Setting";
						}
						break;
					}
                case "PptComponent":
                    {
                        if (targetitem.Content != "")
                        {
                            targetitem.CONTENTSVALUE = targetitem.Content;
                        }
                        else
                        {
                            targetitem.CONTENTSVALUE = "No media";
                            targetitem.Content = "No media";
                        }
                        break;
                    }
                default: break;
            }
        }
    }
}