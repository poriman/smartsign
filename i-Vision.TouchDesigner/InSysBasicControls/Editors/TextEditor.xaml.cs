﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Markup;
using System.IO;
using System.Globalization;
using UtilLib.Etc;

namespace InSysBasicControls.Editors
{
    /// <summary>
    /// Provides an editing of flow documents
    /// </summary>
    public partial class TextEditor : Window
    {
        System.Windows.Forms.ColorDialog cd;
        string loadedDoc;

        public TextEditor()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Init();
        }

        //Setups text editor
        void Init()
        {
			CultureInfo info = System.Threading.Thread.CurrentThread.CurrentUICulture;
			XmlLanguage resourceKey = XmlLanguage.GetLanguage(info != null ? info.IetfLanguageTag : "en-US");
			XmlLanguage enKey = XmlLanguage.GetLanguage("en-US");
			
			foreach (FontFamily ff in Fonts.SystemFontFamilies)
            {
				ComboBoxItem item = new ComboBoxItem();
				item.Content = ff.FamilyNames[resourceKey] != null ? ff.FamilyNames[resourceKey] : ff.FamilyNames[enKey];
				item.DataContext = ff;
// 				item.Content = ff;
				item.FontFamily = ff;
				item.FontSize = 16;
				te_FontFamily.Items.Add(item);

				if (te_RichTextBox.FontFamily != null && te_RichTextBox.FontFamily.FamilyNames.Values.Contains(ff.Source))
					te_FontFamily.SelectedItem = item;

				te_RichTextBox.Focus();

            }

            te_RichTextBox.SpellCheck.IsEnabled = true;
            te_RichTextBox.SpellCheck.SpellingReform = SpellingReform.PreAndPostreform;

            //Initiating color dialog that is used for setting color
            //of currently selected text
            cd = new System.Windows.Forms.ColorDialog();
            cd.AnyColor = true;
            cd.FullOpen = true;
            te_FontColor.MouseDown += new MouseButtonEventHandler(te_FontColor_MouseDown);
            te_backgroundLabel.MouseDown += new MouseButtonEventHandler(te_backgroundLabel_MouseDown);
        }

        void te_backgroundLabel_MouseDown(object sender, MouseButtonEventArgs e)
        {
            BackgroundSetter bs = new BackgroundSetter();
            bs.Load(te_backgroundLabel.Background.Clone());
            if (bs.ISApply)
            {
                te_backgroundLabel.Background = bs.GetContent();
                te_RichTextBox.Document.Background = te_backgroundLabel.Background;
            }
        }

        void te_FontColor_MouseDown(object sender, MouseButtonEventArgs e)
        {
            BackgroundSetter bs = new BackgroundSetter();
            bs.Load(te_FontColor.Background.Clone());
            if (bs.ISApply)
            {
                te_FontColor.Background = bs.GetContent();
                te_RichTextBox.Selection.ApplyPropertyValue(TextElement.ForegroundProperty, te_FontColor.Background);
            }
        }

        //Handling the context menu clicks
        private void MenuItem_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            switch (((MenuItem)sender).Name)
            {
                case "te_Copy":
                    {
                        te_RichTextBox.Copy();
                        break;
                    }
                case "te_Cut":
                    {
                        te_RichTextBox.Cut();
                        break;
                    }
                case "te_Paste":
                    {
                        te_RichTextBox.Paste();
                        break;
                    }
                default: break;
            }
        }

        //Loads a document to a text editor
        public void Load(string doc)
        {
            loadedDoc = doc;
            te_RichTextBox.Document = FlowDocumentManager.DeserializeFlowDocument(loadedDoc);
            te_backgroundLabel.Background = te_RichTextBox.Document.Background == null ? new SolidColorBrush(Color.FromArgb(0, 255, 255, 255)) : te_RichTextBox.Document.Background ;
            if (te_RichTextBox.Document.Blocks.Count > 0)
            {
                te_FontColor.Background = te_RichTextBox.Document.Blocks.FirstBlock.Foreground;
            }
            ShowDialog();
        }

        //Returns an edited document
        public string GetDocument()
        {
            return loadedDoc;
        }

        //Unloads text editor
        public void Unload()
        {
            this.Close();
        }

        //Change font size of selected text
        private void te_FontSize_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string s = ((ComboBoxItem)te_FontSize.SelectedValue).Content.ToString();
            te_RichTextBox.Selection.ApplyPropertyValue(TextElement.FontSizeProperty, Double.Parse(s));
        }

        //Change font family of selected text
        private void te_FontFamily_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {
			te_RichTextBox.Selection.ApplyPropertyValue(TextElement.FontFamilyProperty, ((ComboBoxItem)te_FontFamily.SelectedValue).DataContext);
        }

        private void closeButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void okButton_Click(object sender, RoutedEventArgs e)
        {
            loadedDoc = FlowDocumentManager.SerializeFlowDocument(te_RichTextBox.Document);
            Close();
        }

        private void applyButton_Click(object sender, RoutedEventArgs e)
        {
            loadedDoc = FlowDocumentManager.SerializeFlowDocument(te_RichTextBox.Document);
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btn_AddImage_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                System.Windows.Forms.OpenFileDialog OpenFileDialog = new System.Windows.Forms.OpenFileDialog();
				OpenFileDialog.Filter = "All supported|*.jpg;*.bmp;*.gif;*.png|JPEG files|*.jpg|BMP files| *.bmp|GIF files|*.gif|TIFF files|*.tiff";
                OpenFileDialog.Title = "Add an image";
                if (OpenFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    BitmapImage imageSource = new BitmapImage();
                    imageSource.BeginInit();
                    imageSource.UriSource = new Uri(OpenFileDialog.FileName);
                    imageSource.EndInit();

                    Clipboard.SetImage(imageSource);
                    te_RichTextBox.Paste();
                }
            }
            catch (ArgumentNullException aEx)
            {
                System.Windows.MessageBox.Show(aEx.Message.ToString());
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message.ToString());
            }
        }
    }
}