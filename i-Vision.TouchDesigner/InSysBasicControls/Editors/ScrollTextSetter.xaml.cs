﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Markup;
using System.Globalization;
using InSysTouchflowData;

namespace InSysBasicControls.Editors
{
    /// <summary>
    /// Interaction logic for ScrollTextSetter.xaml
    /// </summary>
    public partial class ScrollTextSetter : Window
    {
        System.Windows.Forms.ColorDialog cd;
        ScrollTextInfo _sti, _loaded;

        /// <summary>
        /// Initializes new ScrollTextSetter instance
        /// </summary>
        public ScrollTextSetter()
        {
            InitializeComponent();
            _sti = new ScrollTextInfo();

			directionComboBox.Items.Add(ScrollTextDirection.LeftToRight);
			directionComboBox.Items.Add(ScrollTextDirection.RightToLeft);
			directionComboBox.Items.Add(ScrollTextDirection.TopToBottom);
			directionComboBox.Items.Add(ScrollTextDirection.BottomToTop);
            
			cd = new System.Windows.Forms.ColorDialog();
            cd.AnyColor = true;
            cd.FullOpen = true;
            
            backgroundLabel.MouseDown += new MouseButtonEventHandler(backgroundLabel_MouseDown);
            fontColorLabel.MouseDown += new MouseButtonEventHandler(fontColorLabel_MouseDown);

			//	FontFamily Setting
			comboBoxFontFamily.ItemsSource = Fonts.SystemFontFamilies;

// 			CultureInfo info = System.Threading.Thread.CurrentThread.CurrentUICulture;
// 			XmlLanguage resourceKey = XmlLanguage.GetLanguage(info != null ? info.IetfLanguageTag : "en-US");
// 			XmlLanguage enKey = XmlLanguage.GetLanguage("en-US");
// 
// 			foreach (FontFamily ff in System.Windows.Media.Fonts.SystemFontFamilies)
// 			{
// 				ComboBoxItem item = new ComboBoxItem();
// 				item.Content = ff.FamilyNames[resourceKey] != null ? ff.FamilyNames[resourceKey] : ff.FamilyNames[enKey];
// 				item.DataContext = ff;
// // 				item.FontFamily = ff;
// // 				item.FontSize = 16;
// 				comboBoxFontFamily.Items.Add(item);
// 			}
			
			st.Load(_sti);

        }

        void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox cb = sender as ComboBox;

            if (_sti == null) return;

            if (cb.SelectedItem == null)
            {
                return;
            }

            switch (cb.Name)
            {
                case "comboBoxFontFamily":
                    {
//                         _sti.FontFamily = ((ComboBoxItem)cb.SelectedItem).FontFamily;
						_sti.FontFamily = (FontFamily)cb.SelectedItem;
                        break;
                    }
                case "comboBoxFontSize":
                    {
						String sSize = ((ComboBoxItem)cb.SelectedItem).Content.ToString();
						_sti.FontSize = double.Parse(sSize);
                        break;
                    }
                default: break;
            }

            st.Load(_sti);
        }

        void fontColorLabel_MouseDown(object sender, MouseButtonEventArgs e)
        {
            BackgroundSetter bs = new BackgroundSetter();
            bs.Load(fontColorLabel.Background.Clone());
            if (bs.ISApply)
            {
                fontColorLabel.Background = bs.GetContent();
                _sti.foreground = fontColorLabel.Background;
                st.Load(_sti);
            }
        }

        /// <summary>
        /// Provides a background setup
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void backgroundLabel_MouseDown(object sender, MouseButtonEventArgs e)
        {
            BackgroundSetter bs = new BackgroundSetter();
            bs.Load(backgroundLabel.Background.Clone());
            if (bs.ISApply)
            {
                backgroundLabel.Background = bs.GetContent();
                _sti.background = backgroundLabel.Background;
                st.Load(_sti);
            }
        }
        
        /// <summary>
        /// Loads the ScrollTextInfo object into ScrollTextSetter
        /// </summary>
        /// <param name="stinfo"></param>
        public void Load(ScrollTextInfo stinfo)
        {/*
            _loaded = new ScrollTextInfo();
            _loaded.background  = stinfo.background;
            _loaded.content     = stinfo.content;
            _loaded.direction   = stinfo.direction;
            _loaded.FontFamily  = stinfo.FontFamily;
            _loaded.FontSize    = stinfo.FontSize;
            _loaded.FontStyle   = stinfo.FontStyle;
            _loaded.FontWeight  = stinfo.FontWeight;
            _loaded.foreground  = stinfo.foreground;
            _loaded.speed       = stinfo.speed;
            */

            _loaded = stinfo;
            _sti = stinfo;
            st.Load(this._sti);
            directionComboBox.SelectedItem  = _sti.direction;
            speedTextBox.Text               = _sti.speed.ToString();
            scrollTextBox.Text              = _sti.content.ToString();
            backgroundLabel.Background      = _sti.background;
            fontColorLabel.Background       = _sti.foreground;

			comboBoxFontFamily.SelectedItem = _sti.FontFamily;

//             foreach (ComboBoxItem item in comboBoxFontFamily.Items)
//             {
//                 if (item.FontFamily.Equals(_sti.FontFamily))
//                 {
//                     comboBoxFontFamily.SelectedItem = item;
//                     break;
//                 }
//             }

            this.te_Bold.IsChecked = (FontWeights.Normal == _sti.FontWeight) ? false : true;
            this.te_Italic.IsChecked = (FontStyles.Normal == _sti.FontStyle) ? false : true;

			foreach (ComboBoxItem item in comboBoxFontSize.Items)
			{
				if (Convert.ToDouble(item.Content).Equals(_sti.FontSize))
				{
					comboBoxFontSize.SelectedItem = item;
					break;
				}
			}

            this.ShowDialog();
        }

        /// <summary>
        /// Returns an edited ScrollTextInfo object
        /// </summary>
        /// <returns></returns>
        public ScrollTextInfo GetContent()
        {
            return _loaded;
        }

        /// <summary>
        /// Loads the ScrollTextInfo settings into ScrollText component
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void previewButton_Click(object sender, RoutedEventArgs e)
        {
            _sti.content = scrollTextBox.Text;

            int test = 0;
            if (!Int32.TryParse(speedTextBox.Text, out test))
            {
                speedTextBox.Text = "0";
            }
            _sti.speed = Int32.Parse(speedTextBox.Text);
            _sti.direction = (ScrollTextDirection)directionComboBox.SelectedItem;
            _sti.background = backgroundLabel.Background;
            _sti.foreground = fontColorLabel.Background;

            st.Load(_sti);
        }

        /// <summary>
        /// Provides the changing of scrolling direction
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void directionComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (((ScrollTextDirection)directionComboBox.SelectedItem) == ScrollTextDirection.BottomToTop || 
                ((ScrollTextDirection)directionComboBox.SelectedItem) == ScrollTextDirection.TopToBottom)
            {
                scrollTextBox.AcceptsReturn = true;
            }
            else scrollTextBox.AcceptsReturn = false;

            _sti.direction = (ScrollTextDirection)directionComboBox.SelectedItem;
            st.Load(_sti);
        }

        private void scrollTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            _sti.content = scrollTextBox.Text;
        }

        private void startScrollingButton_Click(object sender, RoutedEventArgs e)
        {
            _sti.content = scrollTextBox.Text;

            int test = 0;
            if (!Int32.TryParse(speedTextBox.Text, out test))
            {
                speedTextBox.Text = "0";
            }
            _sti.speed = Int32.Parse(speedTextBox.Text);
            _sti.direction = (ScrollTextDirection)directionComboBox.SelectedItem;
            _sti.background = backgroundLabel.Background;
            _sti.foreground = fontColorLabel.Background;

            st.Load(_sti);
            st.Start();
        }

        private void stopScrollingButton_Click(object sender, RoutedEventArgs e)
        {
            st.Stop();
        }

        private void okButton_Click(object sender, RoutedEventArgs e)
        {
            _sti.content = scrollTextBox.Text;
            int test = 0;
            if (!Int32.TryParse(speedTextBox.Text, out test))
            {
                speedTextBox.Text = "0";
            }
            _sti.speed = Int32.Parse(speedTextBox.Text);
            _sti.direction = (ScrollTextDirection)directionComboBox.SelectedItem;
			//	hsshin 무조건 투명색으로 저장하자
// 			_sti.background = backgroundLabel.Background;
			_sti.background = new SolidColorBrush(System.Windows.Media.Color.FromArgb(0, 0, 0, 0));
			_sti.foreground = fontColorLabel.Background;
            _loaded = _sti;

            this.DialogResult = true;

            Close();
        }

        private void applyButton_Click(object sender, RoutedEventArgs e)
        {
            _sti.content = scrollTextBox.Text;
            int test = 0;
            if (!Int32.TryParse(speedTextBox.Text, out test))
            {
                speedTextBox.Text = "0";
            }
            _sti.speed = Int32.Parse(speedTextBox.Text);
            _sti.direction = (ScrollTextDirection)directionComboBox.SelectedItem;
            _sti.background = backgroundLabel.Background;
            _sti.foreground = fontColorLabel.Background;
            st.Load(_sti);
            _loaded = _sti;

            this.DialogResult = true;

        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;

            Close();
        }

        private void speedTextBox_PreviewTextInput_1(object sender, TextCompositionEventArgs e)
        {
            int test = 0;
            if (!Int32.TryParse(e.Text, out test))
            {
                e.Handled = true;
            }
        }

        private void te_Bold_Checked(object sender, RoutedEventArgs e)
        {
            _sti.FontWeight = FontWeights.Bold;
            st.TBFontWeight = FontWeights.Bold;
        }

        private void te_Bold_Unchecked(object sender, RoutedEventArgs e)
        {
           _sti.FontWeight = FontWeights.Normal;
           st.TBFontWeight = FontWeights.Normal;
        }

        private void te_Italic_Checked(object sender, RoutedEventArgs e)
        {
            _sti.FontStyle = FontStyles.Italic;
            st.TBFontStyle = FontStyles.Italic;
        }

        private void te_Italic_Unchecked(object sender, RoutedEventArgs e)
        {
            _sti.FontStyle = FontStyles.Normal;
            st.TBFontStyle = FontStyles.Normal;
        }

    }
}