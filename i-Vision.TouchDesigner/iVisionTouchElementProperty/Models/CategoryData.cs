﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Windows;
using InSysTouchflowData.Models.ActionEvents;
using InSysTouchflowData;
using InSysBasicControls.InSysProperties;
using System.Xml.Linq;
using System.IO;
using System.Xml;
using UtilLib.StaticMethod;

namespace iVisionTouchElementProperty.Models
{
    public interface ICategoryData
    {
        List<CategoryType> SupportCategroyTypeList { get; set; }
        Dictionary<string, PropertySetting> SupportProperties { get; set; }//Value는 Label 컨트롤에 대한 Show/Hide 여부

        #region Layout Category 변수
        double Height { get; set; }
        double Width { get; set; }
        bool AspectRatio { get; set; }
        FitToPage FitPage { get; set; }
        int ZIndex { get; set; }
        HorizontalAlignment HorizontalAlignment { get; set; }
        VerticalAlignment VerticalAlignment { get; set; }
        Alignment Alignment { get; set; }
        #endregion

        #region AspectRatio Category 변수
        int AspectRatioHeight { get; set; }
        int AspectRatioWidth { get; set; }
        #endregion

        #region Appearance Category 변수
        double Opacity { get; set; }
        Visibility Visibility { get; set; }
        double BorderCorner { get; set; }
        double BorderThickness { get; set; }
        int HighlightBrightness { get; set; }
        Style ButtonStyle { get; set; }
        #endregion

        #region Common Category 변수
        string ContentName { get; set; }
        string PageName { get; set; }
        double X { get; set; }
        double Y { get; set; }
        object Content { get; set; }
        List<FrameworkElement> PageElements { get; set; }        
        int DisplayOrder { get; set; }
        List<PlaylistItem> Playlist { get; set; }
        #endregion

        #region Brushes Category 변수
        Brush BorderBrush { get; set; }
        Brush Background { get; set; }
        Brush Foreground { get; set; }
        #endregion   

        #region Time Category 변수
        bool ApplyLifeTime { get; set; }
        TimeSpan StartTime { get; set; }
        TimeSpan EndTime { get; set; }
        double LifeTime { get; set; }        
        #endregion  

        #region Text Category 변수
        double FontSize { get; set; }
        FontWeight FontWeight { get; set; }
        FontFamily FontFamily { get; set; }
        TextWrapping TextWrapping { get; set; }
        bool Multiline { get; set; }
        #endregion  

        #region Media

        Stretch Stretch { get; set; }
        bool IsMuted { get; set; }
        double Volume { get; set; }

        #endregion

        #region Event

        ActionEvent TouchActionEvent { get; set; }

        PageEffect PageEffect { get; set; }

        #endregion

        Type Type { get; set; }

        void SetPropertyValues(ElementPropertyObject propertyObject);
        //void SetData(Dictionary<string, object> properties, params string[] support_prop_list);
        //void SetData(KeyValuePair<string, object> propertyData);       
        void SetCategories(Dictionary<string, object> properties);
    }

    public class PropertySetting
    {
        public bool IsShowControl { get; set; }
        public bool IsShowLabel { get; set; }
        public bool IsReadOnly { get; set; }

        public PropertySetting(bool isShowControl, bool isShowLabel, bool isReadOnly)
        {
            IsShowControl = isShowControl;
            IsShowLabel = isShowLabel;
            IsReadOnly = isReadOnly;
        }
    }

    public class CategoryData : ICategoryData
    {
        public CategoryData()
        {
            SupportCategroyTypeList = new List<CategoryType>();
            SupportProperties = new Dictionary<string, PropertySetting>();
            Playlist = new List<PlaylistItem>();
        }

        #region ICategoryData Members

        public List<CategoryType> SupportCategroyTypeList { get; set; }
        public Dictionary<string, PropertySetting> SupportProperties { get; set; }

        public double Height { get; set; }
        public double Width { get; set; }
        public double SlideViewHeight { get; set; }
        public double SlideViewWidth { get; set; }
        public bool AspectRatio { get; set; }
        public FitToPage FitPage { get; set; }
        public int ZIndex { get; set; }
        public HorizontalAlignment HorizontalAlignment { get; set; }
        public VerticalAlignment VerticalAlignment { get; set; }
        public int AspectRatioHeight { get; set; }
        public int AspectRatioWidth { get; set; }
        public double Opacity { get; set; }
        public Visibility Visibility { get; set; }
        public double BorderCorner { get; set; }
        public double BorderThickness { get; set; }
        public int HighlightBrightness { get; set; }
        public Style ButtonStyle { get; set; }
        public Alignment Alignment { get; set; }
        public string ContentName { get; set; }
        public string PageName { get; set; }
        public double X { get; set; }
        public double Y { get; set; }
        public object Content { get; set; }
        public string Text { get; set; }
        public Brush BorderBrush { get; set; }
        public Brush Background { get; set; }
        public Brush Foreground { get; set; }
        public double FontSize { get; set; }
        public FontWeight FontWeight { get; set; }
        public FontFamily FontFamily { get; set; }
        public TextWrapping TextWrapping { get; set; }
        public bool Multiline { get; set; }
        public bool ApplyLifeTime { get; set; }
        public bool IsApplyPageLifeTime { get; set; }
        public double LifeTime { get; set; }       
        public TimeSpan StartTime { get; set; }
        public TimeSpan EndTime { get; set; }
        public Stretch Stretch { get; set; }
        public bool IsMuted { get; set; }
        public double Volume { get; set; }
        public List<FrameworkElement> PageElements { get; set; }
        public PageEffect PageEffect { get; set; }        
        public int DisplayOrder { get; set; }
        public List<PlaylistItem> Playlist { get; set; }

        public ActionEvent TouchActionEvent { get; set; }
        public Dictionary<string, string> TouchEvent { get; set; } //Touch Event
        public string EventName { get; set; }//Touch Event

        public Type Type { get; set; }
        
        public void SetPropertyValues(ElementPropertyObject propertyObject)
        {
            propertyObject.Height = this.Height;
            propertyObject.Width = this.Width;
            propertyObject.AspectRatio = this.AspectRatio;
            propertyObject.FitToPage = this.FitPage;
            propertyObject.HorizontalAlignment = this.HorizontalAlignment;
            propertyObject.VerticalAlignment = this.VerticalAlignment;
            propertyObject.Opacity = this.Opacity;
            propertyObject.Name = this.ContentName;
            propertyObject.BorderBrush = this.BorderBrush;
            propertyObject.Background = this.Background;
            propertyObject.Foreground = this.Foreground;
            propertyObject.X = this.X;
            propertyObject.X = this.Y;
            propertyObject.FontSize = this.FontSize;
            propertyObject.FontFamily = this.FontFamily;
            propertyObject.FontWeight = this.FontWeight;
            propertyObject.TextWrapping = this.TextWrapping;
            propertyObject.Multiline = this.Multiline;
            propertyObject.IsApplyLifeTime = this.ApplyLifeTime;
            propertyObject.LifeTime = this.LifeTime;
            propertyObject.StartTime = this.StartTime;
            propertyObject.EndTime = this.EndTime;
            propertyObject.Content = this.Content;
            propertyObject.Text = this.Text;
            propertyObject.HighlightBrightness = this.HighlightBrightness;
            propertyObject.BorderCorner = this.BorderCorner;
            propertyObject.IsMuted = this.IsMuted;
            propertyObject.Volume = this.Volume;
            propertyObject.Stretch = this.Stretch;
            propertyObject.AspectRatioWidth = this.AspectRatioWidth;
            propertyObject.AspectRatioHeight = this.AspectRatioHeight;
            propertyObject.Visibility = this.Visibility;
            propertyObject.ZIndex = this.ZIndex;
            propertyObject.Alignment = this.Alignment;
            propertyObject.FitToPage = this.FitPage;
            propertyObject.Type = this.Type;
            propertyObject.ButtonStyle = this.ButtonStyle;
            propertyObject.TouchEvent = this.TouchEvent;
            propertyObject.SlideViewHeight = this.SlideViewHeight;
            propertyObject.SlideViewWidth = this.SlideViewWidth;
        }
       
        public void SetCategories(Dictionary<string, object> properties)
        {
            Type contentType = properties["Type"] as Type;
            switch (contentType.Name)
            {
                #region InSysSlideViewer
                case "InSysSlideViewer":
                    AddCategroy(properties, CategoryType.Layout, "Height", "Width", "SlideViewHeight", "SlideViewWidth", "FitPage", "ZIndex", "Alignment", "FitToPage");
                    AddCategroy(properties, CategoryType.AspectRatio, "AspectRatioWidth", "AspectRatioHeight");
                    AddCategroy(properties, CategoryType.Appearance, "Opacity", "Visibility", "BorderCorner", "BorderThickness", "HighlightBrightness");
                    AddCategroy(properties, CategoryType.Common, "Name", "X", "Y", "Content", "PageElements");
                    AddCategroy(properties, CategoryType.Brushes, "Background", "BorderBrush", "Foreground");
                    AddCategroy(properties, CategoryType.Text, "FontSize", "FontWeight", "FontFamily");
                    AddCategroy(properties, CategoryType.Time, "IsApplyLifeTime", "LifeTime", "StartTime", "EndTime");
                    AddCategroy(properties, CategoryType.Media, "Stretch", "IsMuted", "Volume");
                    break;
                #endregion
                #region InSysImageBox
                case "InSysImageBox":
                    AddCategroy(properties, CategoryType.Layout, "Height", "Width", "FitPage", "ZIndex", "Alignment", "FitToPage");
                    AddCategroy(properties, CategoryType.AspectRatio, "AspectRatioWidth", "AspectRatioHeight");
                    AddCategroy(properties, CategoryType.Appearance, "Opacity", "Visibility", "BorderCorner", "BorderThickness", "HighlightBrightness");
                    AddCategroy(properties, CategoryType.Common, "Name", "X", "Y", "Content", "PageElements");
                    AddCategroy(properties, CategoryType.Brushes, "Background", "BorderBrush", "Foreground");
                    AddCategroy(properties, CategoryType.Text, "FontSize", "FontWeight", "FontFamily");
                    AddCategroy(properties, CategoryType.Time, "IsApplyLifeTime", "LifeTime", "StartTime", "EndTime");
                    AddCategroy(properties, CategoryType.Media, "Stretch", "IsMuted", "Volume");                             
                    break;
                #endregion
                #region InSysVideoBox
                case "InSysVideoBox":
                    AddCategroy(properties, CategoryType.Layout, "Height", "Width", "FitPage", "ZIndex", "Alignment", "FitToPage");
                    AddCategroy(properties, CategoryType.AspectRatio, "AspectRatioWidth", "AspectRatioHeight");
                    AddCategroy(properties, CategoryType.Appearance, "Opacity", "Visibility", "BorderCorner", "BorderThickness", "HighlightBrightness");
                    AddCategroy(properties, CategoryType.Common, "Name", "X", "Y", "Content", "PageElements");
                    AddCategroy(properties, CategoryType.Brushes, "Background", "BorderBrush", "Foreground");
                    AddCategroy(properties, CategoryType.Text, "FontSize", "FontWeight", "FontFamily");
                    AddCategroy(properties, CategoryType.Time, "IsApplyLifeTime", "LifeTime", "StartTime", "EndTime");
                    AddCategroy(properties, CategoryType.Media, "Stretch", "IsMuted", "Volume");
                    break;
                #endregion
                #region InSysStreamingBox
                case "InSysStreamingBox":
                    AddCategroy(properties, CategoryType.Layout, "Height", "Width", "FitPage", "ZIndex", "Alignment", "FitToPage");
                    AddCategroy(properties, CategoryType.AspectRatio, "AspectRatioWidth", "AspectRatioHeight");
                    AddCategroy(properties, CategoryType.Appearance, "Opacity", "Visibility", "BorderCorner", "BorderThickness");
                    AddCategroy(properties, CategoryType.Common, "Name", "X", "Y", "Content", "PageElements");
                    AddCategroy(properties, CategoryType.Brushes, "Background", "BorderBrush", "Foreground");
                    AddCategroy(properties, CategoryType.Text, "FontSize", "FontWeight", "FontFamily");
                    AddCategroy(properties, CategoryType.Time, "IsApplyLifeTime", "LifeTime", "StartTime", "EndTime");
                    AddCategroy(properties, CategoryType.Media, "Stretch", "IsMuted", "Volume");
                    break;
                #endregion
                #region InSysFlashBox
                case "InSysFlashBox":
                    AddCategroy(properties, CategoryType.Layout, "Height", "Width", "FitPage", "ZIndex", "Alignment", "FitToPage");
                    AddCategroy(properties, CategoryType.AspectRatio, "AspectRatioWidth", "AspectRatioHeight");
                    AddCategroy(properties, CategoryType.Appearance, "Opacity", "Visibility", "BorderCorner", "BorderThickness", "HighlightBrightness");
                    AddCategroy(properties, CategoryType.Common, "Name", "X", "Y", "Content", "PageElements");
                    AddCategroy(properties, CategoryType.Brushes, "Background", "BorderBrush", "Foreground");
                    AddCategroy(properties, CategoryType.Text, "FontSize", "FontWeight", "FontFamily");
                    AddCategroy(properties, CategoryType.Time, "IsApplyLifeTime", "LifeTime", "StartTime", "EndTime");                   
                    break;
                #endregion
                #region InSysBasicButton
                case "InSysBasicButton":
                    AddCategroy(properties, CategoryType.Layout, "Height", "Width", "FitPage", "ZIndex", "Alignment", "FitToPage");
                    AddCategroy(properties, CategoryType.AspectRatio, "AspectRatioWidth", "AspectRatioHeight");
                    AddCategroy(properties, CategoryType.Appearance, "Opacity", "Visibility", "BorderCorner", "HighlightBrightness", "ButtonStyle");
                    AddCategroy(properties, CategoryType.Common,  "Name", "X", "Y", "Content", "PageElements");
                    AddCategroy(properties, CategoryType.Brushes, "Background", "Foreground");
                    AddCategroy(properties, CategoryType.Text, "FontSize", "FontWeight", "FontFamily");
                    AddCategroy(properties, CategoryType.Time, "IsApplyLifeTime", "LifeTime", "StartTime", "EndTime");
                    AddCategroy(properties, CategoryType.Media, "Stretch", "IsMuted", "Volume");
                    AddCategroy(properties, CategoryType.Event, "ActionEvent", "TargetPages", "ShowHideType", "TargetObjects", "URL", "File", "PageEffect", /* "TouchEvent",*/ "EventName");
                    break;
                #endregion
                #region InSysRectangle
                case "InSysRectangle":
                    AddCategroy(properties, CategoryType.Layout, "Height", "Width", "FitPage", "ZIndex", "Alignment", "FitToPage");
                    AddCategroy(properties, CategoryType.AspectRatio, "AspectRatioWidth", "AspectRatioHeight");
                    AddCategroy(properties, CategoryType.Appearance, "Opacity", "Visibility", "BorderCorner", "BorderThickness", "HighlightBrightness");
                    AddCategroy(properties, CategoryType.Common,  "Name", "X", "Y", "Content", "PageElements");
                    AddCategroy(properties, CategoryType.Brushes, "Background", "BorderBrush", "Foreground");
                    AddCategroy(properties, CategoryType.Text, "FontSize", "FontWeight", "FontFamily");
                    AddCategroy(properties, CategoryType.Time, "IsApplyLifeTime", "LifeTime", "StartTime", "EndTime");
                    AddCategroy(properties, CategoryType.Media, "Stretch", "IsMuted", "Volume");                    
                    break;
                #endregion
                #region InSysRectArea
                case "InSysRectArea":
                    AddCategroy(properties, CategoryType.Layout, "Height", "Width", "FitPage", "ZIndex", "Alignment", "FitToPage");
                    AddCategroy(properties, CategoryType.AspectRatio, "AspectRatioWidth", "AspectRatioHeight");
                    //AddCategroy(properties, CategoryType.Appearance, "Opacity", "Visibility", "BorderCorner", "BorderThickness", "HighlightBrightness");
                    AddCategroy(properties, CategoryType.Common, "Name", "X", "Y", "Content", "PageElements");
                    //AddCategroy(properties, CategoryType.Brushes, "Background", "BorderBrush", "Foreground");
                    AddCategroy(properties, CategoryType.Text, "FontSize", "FontWeight", "FontFamily");
                    AddCategroy(properties, CategoryType.Time, "IsApplyLifeTime", "LifeTime", "StartTime", "EndTime");
                    AddCategroy(properties, CategoryType.Media, "Stretch", "IsMuted", "Volume");
                    AddCategroy(properties, CategoryType.Event, "ActionEvent", "TargetPages", "ShowHideType", "TargetObjects", "URL", "File", "PageEffect", /*"TouchEvent",*/ "EventName");
                    break;
                #endregion
                #region InSysBasicTextBox
                case "InSysBasicTextBox":
                    AddCategroy(properties, CategoryType.Layout, "Height", "Width", "FitPage", "Alignment", "FitToPage");
                    AddCategroy(properties, CategoryType.AspectRatio, "AspectRatioWidth", "AspectRatioHeight");
                    AddCategroy(properties, CategoryType.Appearance, "Opacity", "Visibility", "BorderCorner", "BorderThickness", "HighlightBrightness");
                    AddCategroy(properties, CategoryType.Common, "Name", "X", "Y", "Text");
                    AddCategroy(properties, CategoryType.Brushes, "Background", "BorderBrush", "Foreground");
                    AddCategroy(properties, CategoryType.Text, "FontSize", "FontWeight", "FontFamily", "TextWrapping", "Multiline");
                    AddCategroy(properties, CategoryType.Time, "IsApplyLifeTime", "LifeTime", "StartTime", "EndTime");
                    //AddCategroy(properties, CategoryType.Media, "Stretch", "IsMuted", "Volume");
                    break;
                #endregion    
                #region InSysLabel
                case "InSysLabel":
                    AddCategroy(properties, CategoryType.Layout, "Height", "Width", "FitPage", "Alignment", "FitToPage", "VerticalAlignment", "HorizontalAlignment");
                    AddCategroy(properties, CategoryType.AspectRatio, "AspectRatioWidth", "AspectRatioHeight");
                    AddCategroy(properties, CategoryType.Appearance, "Opacity", "Visibility", "BorderThickness");
                    AddCategroy(properties, CategoryType.Common, "Name", "X", "Y", "Text");
                    AddCategroy(properties, CategoryType.Brushes, "Background", "BorderBrush", "Foreground");
                    AddCategroy(properties, CategoryType.Text, "FontSize", "FontWeight", "FontFamily", "TextWrapping");
                    AddCategroy(properties, CategoryType.Time, "IsApplyLifeTime", "LifeTime", "StartTime", "EndTime");
                    //AddCategroy(properties, CategoryType.Media, "Stretch");
                    break;
                #endregion  
                #region InSysAudioBox
                case "InSysAudioBox":
                    //AddCategroy(properties, CategoryType.Layout, "Height", "Width", "FitPage", "ZIndex");
                    //AddCategroy(properties, CategoryType.AspectRatio, "AspectRatioWidth", "AspectRatioHeight");
                    //AddCategroy(properties, CategoryType.Appearance, "Opacity", "Visibility", "BorderCorner", "BorderThickness", "HighlightBrightness");
                    AddCategroy(properties, CategoryType.Common, "Name");
                    //AddCategroy(properties, CategoryType.Brushes, "Background", "BorderBrush", "Foreground");
                    //AddCategroy(properties, CategoryType.Text, "FontSize", "FontWeight", "FontFamily");
                    AddCategroy(properties, CategoryType.Time, "IsApplyLifeTime", "LifeTime", "StartTime", "EndTime");
                    AddCategroy(properties, CategoryType.Media, "IsMuted", "Volume");
                    //AddCategroy(properties, CategoryType.Event, "ActionEvent", "TargetPages", "ShowHideType", "TargetObjects", "URL", "File", "PageEffect");
                    break;
                #endregion
                #region InSysAnalogueClock
                case "InSysAnalogueClock":
                    AddCategroy(properties, CategoryType.Layout, "Height", "Width", "FitPage", "Alignment", "FitToPage");
                    AddCategroy(properties, CategoryType.AspectRatio, "AspectRatioWidth", "AspectRatioHeight");
                    AddCategroy(properties, CategoryType.Appearance, "Opacity", "Visibility", "BorderCorner", "BorderThickness", "HighlightBrightness");
                    AddCategroy(properties, CategoryType.Common, "Name", "X", "Y", "Content", "PageElements");
                    AddCategroy(properties, CategoryType.Brushes, "Background", "BorderBrush", "Foreground");
                    AddCategroy(properties, CategoryType.Text, "FontSize", "FontWeight", "FontFamily");
                    AddCategroy(properties, CategoryType.Time, "IsApplyLifeTime", "LifeTime", "StartTime", "EndTime");
                    AddCategroy(properties, CategoryType.Media, "Stretch", "IsMuted", "Volume");
                    break;
                #endregion
                #region InSysDate
                case "InSysDate":
                    AddCategroy(properties, CategoryType.Layout, "Height", "Width", "FitPage", "Alignment", "FitToPage");
                    AddCategroy(properties, CategoryType.AspectRatio, "AspectRatioWidth", "AspectRatioHeight");
                    AddCategroy(properties, CategoryType.Appearance, "Opacity", "Visibility", "BorderCorner", "BorderThickness", "HighlightBrightness");
                    AddCategroy(properties, CategoryType.Common, "Name", "X", "Y", "Content", "PageElements");
                    AddCategroy(properties, CategoryType.Brushes, "Background", "BorderBrush", "Foreground");
                    AddCategroy(properties, CategoryType.Text, "FontSize", "FontWeight", "FontFamily");
                    AddCategroy(properties, CategoryType.Time, "IsApplyLifeTime", "LifeTime", "StartTime", "EndTime");
                    AddCategroy(properties, CategoryType.Media, "Stretch", "IsMuted", "Volume");
                    break;
                #endregion
                #region InSysDigitalClock
                case "InSysDigitalClock":
                    AddCategroy(properties, CategoryType.Layout, "Height", "Width", "FitPage", "Alignment", "FitToPage");
                    AddCategroy(properties, CategoryType.AspectRatio, "AspectRatioWidth", "AspectRatioHeight");
                    AddCategroy(properties, CategoryType.Appearance, "Opacity", "Visibility", "BorderCorner", "BorderThickness", "HighlightBrightness");
                    AddCategroy(properties, CategoryType.Common, "Name", "X", "Y", "Content", "PageElements");
                    AddCategroy(properties, CategoryType.Brushes, "Background", "BorderBrush", "Foreground");
                    AddCategroy(properties, CategoryType.Text, "FontSize", "FontWeight", "FontFamily");
                    AddCategroy(properties, CategoryType.Time, "IsApplyLifeTime", "LifeTime", "StartTime", "EndTime");
                    AddCategroy(properties, CategoryType.Media, "Stretch", "IsMuted", "Volume");
                    break;
                #endregion
                #region InSysEllipse
                case "InSysEllipse":
                    AddCategroy(properties, CategoryType.Layout, "Height", "Width", "FitPage", "Alignment", "FitToPage");
                    AddCategroy(properties, CategoryType.AspectRatio, "AspectRatioWidth", "AspectRatioHeight");
                    AddCategroy(properties, CategoryType.Appearance, "Opacity", "Visibility", "BorderCorner", "BorderThickness", "HighlightBrightness");
                    AddCategroy(properties, CategoryType.Common, "Name", "X", "Y", "Content", "PageElements");
                    AddCategroy(properties, CategoryType.Brushes, "Background", "BorderBrush", "Foreground");
                    AddCategroy(properties, CategoryType.Text, "FontSize", "FontWeight", "FontFamily");
                    AddCategroy(properties, CategoryType.Time, "IsApplyLifeTime", "LifeTime", "StartTime", "EndTime");
                    AddCategroy(properties, CategoryType.Media, "Stretch", "IsMuted", "Volume");
                    break;
                #endregion
                #region InSysRSS
                case "InSysRSS":
                    AddCategroy(properties, CategoryType.Layout, "Height", "Width", "FitPage", "Alignment", "FitToPage");
                    AddCategroy(properties, CategoryType.AspectRatio, "AspectRatioWidth", "AspectRatioHeight");
                    AddCategroy(properties, CategoryType.Appearance, "Opacity", "Visibility", "BorderCorner", "BorderThickness", "HighlightBrightness");
                    AddCategroy(properties, CategoryType.Common, "Name", "X", "Y", "Content", "PageElements");
                    AddCategroy(properties, CategoryType.Brushes, "Background", "BorderBrush", "Foreground");
                    AddCategroy(properties, CategoryType.Text, "FontSize", "FontWeight", "FontFamily");
                    AddCategroy(properties, CategoryType.Time, "IsApplyLifeTime", "LifeTime", "StartTime", "EndTime");
                    AddCategroy(properties, CategoryType.Media, "Stretch", "IsMuted", "Volume");
                    break;
                #endregion
                #region InSysScrollText
                case "InSysScrollText":
                    AddCategroy(properties, CategoryType.Layout, "Height", "Width", "FitPage", "Alignment", "FitToPage");
                    AddCategroy(properties, CategoryType.AspectRatio, "AspectRatioWidth", "AspectRatioHeight");
                    AddCategroy(properties, CategoryType.Appearance, "Opacity", "Visibility", "BorderCorner", "BorderThickness");
                    AddCategroy(properties, CategoryType.Common, "Name", "X", "Y", "Content", "PageElements");
                    AddCategroy(properties, CategoryType.Brushes, "Background", "BorderBrush");
                    AddCategroy(properties, CategoryType.Text, "FontSize", "FontWeight", "FontFamily");
                    AddCategroy(properties, CategoryType.Time, "IsApplyLifeTime", "LifeTime", "StartTime", "EndTime");
                    break;
                #endregion
                #region InSysWebControl
                case "InSysWebControl":
                    AddCategroy(properties, CategoryType.Layout, "Height", "Width", "FitPage", "Alignment", "FitToPage");
                    AddCategroy(properties, CategoryType.AspectRatio, "AspectRatioWidth", "AspectRatioHeight");
                    AddCategroy(properties, CategoryType.Appearance, "Opacity", "Visibility", "BorderCorner", "BorderThickness", "HighlightBrightness");
                    AddCategroy(properties, CategoryType.Common, "Name", "X", "Y", "Content", "PageElements");
                    AddCategroy(properties, CategoryType.Brushes, "Background", "BorderBrush", "Foreground");
                    AddCategroy(properties, CategoryType.Text, "FontSize", "FontWeight", "FontFamily");
                    AddCategroy(properties, CategoryType.Time, "IsApplyLifeTime", "LifeTime", "StartTime", "EndTime");
                    AddCategroy(properties, CategoryType.Media, "Stretch", "IsMuted", "Volume");                    
                    break;
                #endregion
                #region PageCanvas
                case "PageCanvas":
                    AddCategroy(properties, CategoryType.Layout, "PageHeight", "PageWidth");
                    AddCategroy(properties, CategoryType.Common, "PageName", "DisplayOrder");
                    AddCategroy(properties, CategoryType.Brushes, "Background");
                    AddCategroy(properties, CategoryType.Time, "IsApplyPageLifeTime", "PageLifeTime");
                    AddCategroy(properties, CategoryType.Event /*"TouchEvent"*/);
                    break;
                #endregion
                default:
                    break;
            }
        }

        public void AddCategroy(Dictionary<string, object> properties, CategoryType support_categorytype, params string[] support_propertis)
        {           
            if (SetData(properties, support_propertis) == true)//속성값 설정
            {
                if (SupportCategroyTypeList.Contains(support_categorytype) == false)
                    SupportCategroyTypeList.Add(support_categorytype);
            }
        }


        private bool SetData(Dictionary<string, object> properties, params string[] support_prop_list)
        {
            bool isHasProperty = false;

            foreach (var prop in support_prop_list)
            {
                if (properties.ContainsKey(prop) == true)
                {
                    SetData(new KeyValuePair<string, object>(prop, properties[prop]));
                    isHasProperty = true; 
                }
            }

            return isHasProperty;
        }

        private void SetData(KeyValuePair<string, object> propertyData)
        {
            switch (propertyData.Key)
            {
                case "Height":
                    this.Height = (double)propertyData.Value;
                    UpdateProperty(propertyData.Key, true, true, false);
                    break;
                case "Width":
                    this.Width = (double)propertyData.Value;
                    UpdateProperty(propertyData.Key, true, true, false);
                    break;
                case "PageHeight":
                    this.Height = (double)propertyData.Value;
                    UpdateProperty(propertyData.Key, true, true, true);
                    break;
                case "PageWidth":
                    this.Width = (double)propertyData.Value;
                    UpdateProperty(propertyData.Key, true, true, true);
                    break;
                case "FitPage":
                    UpdateProperty(propertyData.Key, true, true, false);
                    break;
                case "Opacity":
                    this.Opacity = (double)propertyData.Value;
                    UpdateProperty(propertyData.Key, true, true, false);
                    break;
                case "ZIndex":
                    this.ZIndex = (int)propertyData.Value;
                    UpdateProperty(propertyData.Key, true, true, false);
                    break;
                case "AspectRatio":
                    UpdateProperty(propertyData.Key, true, true, false);
                    break;
                case "HorizontalAlignment":
                    this.HorizontalAlignment = (HorizontalAlignment)propertyData.Value;
                    UpdateProperty(propertyData.Key, true, false, false);
                    break;
                case "VerticalAlignment":
                    this.VerticalAlignment = (VerticalAlignment)propertyData.Value;
                    UpdateProperty(propertyData.Key, true, false, false);
                    break;               
                case "BorderBrush":
                    if (propertyData.Value.GetType().Name.Equals("String") == true)
                    {
                        this.BorderBrush = ObjectConverters.ConvertBrushFromBrushXamlString(propertyData.Value.ToString());
                    }
                    else
                    {
                        this.BorderBrush = (Brush)propertyData.Value;
                    }
                    UpdateProperty(propertyData.Key, true, true, false);
                    break;
                case "Background":
                    if (propertyData.Value.GetType().Name.Equals("String") == true)
                    {
                        this.Background = ObjectConverters.ConvertBrushFromBrushXamlString(propertyData.Value.ToString());
                    }
                    else
                    {
                        this.Background = (Brush)propertyData.Value;
                    }
                    UpdateProperty(propertyData.Key, true, false, false);
                    break;               
                case "Foreground":
                    if (propertyData.Value.GetType().Name.Equals("String") == true)
                    {
                        this.Foreground = ObjectConverters.ConvertBrushFromBrushXamlString(propertyData.Value.ToString());
                    }
                    else
                    {
                        this.Foreground = (Brush)propertyData.Value;
                    }
                    UpdateProperty(propertyData.Key, true, true, false);
                    break;
                case "Stroke":
                case "BorderThickness":
                    this.BorderThickness = (double)propertyData.Value;
                    UpdateProperty(propertyData.Key, true, true, false);
                    break;
                case "BorderCorner":
                    this.BorderCorner = (double)propertyData.Value;
                    UpdateProperty(propertyData.Key, true, true, false);
                    break;
                case "Name":
                    this.ContentName = (string)propertyData.Value;
                    UpdateProperty(propertyData.Key, true, true, false);
                    break;
                case "PageName":
                    this.ContentName = (string)propertyData.Value;
                    UpdateProperty(propertyData.Key, true, true, false);
                    break;
                case "X":
                    this.X = (double)propertyData.Value;
                    UpdateProperty(propertyData.Key, true, true, false);
                    break;
                case "Y":
                    this.Y = (double)propertyData.Value;
                    UpdateProperty(propertyData.Key, true, true, false);
                    break;
                case "ActionEvent":
                    this.TouchActionEvent = (ActionEvent)propertyData.Value;
                    UpdateProperty(propertyData.Key, true, true, false);
                    UpdateProperty("TargetPages", true, true, false);
                    UpdateProperty("ShowHideType", true, true, false);
                    UpdateProperty("URL", true, true, false);
                    UpdateProperty("File", true, true, false);
                    break;               
                case "PageEffect":
                    UpdateProperty(propertyData.Key, true, true, false);
                    break;
                case "FontSize":
                    this.FontSize = (double)propertyData.Value;
                    UpdateProperty(propertyData.Key, true, true, false);
                    break;
                case "FontWeight":
                    this.FontWeight = (FontWeight)propertyData.Value;
                    UpdateProperty(propertyData.Key, true, true, false);
                    break;
                case "FontFamily":
                    this.FontFamily = (FontFamily)propertyData.Value;
                    UpdateProperty(propertyData.Key, true, true, false);
                    break;
                case "TextWrapping":
                    this.TextWrapping = (TextWrapping)propertyData.Value;
                    UpdateProperty(propertyData.Key, true, true, false);
                    break;
                case "Multiline":
                    this.Multiline = (bool)propertyData.Value;
                    UpdateProperty(propertyData.Key, true, true, false);
                    break;
                case "IsApplyLifeTime":
                    this.ApplyLifeTime = (bool)propertyData.Value;
                    UpdateProperty(propertyData.Key, true, true, false);
                    break;
                case "IsApplyPageLifeTime":
                    this.IsApplyPageLifeTime = (bool)propertyData.Value;
                    UpdateProperty(propertyData.Key, true, true, false);
                    break;
                case "StartTime":
                    this.StartTime = (TimeSpan)propertyData.Value;
                    UpdateProperty(propertyData.Key, true, true, false);
                    break;
                case "EndTime":
                    this.EndTime = (TimeSpan)propertyData.Value;
                    UpdateProperty(propertyData.Key, true, true, false);
                    break;
                case "Content":
                    this.Content = (object)propertyData.Value;
                    UpdateProperty(propertyData.Key, true, true, false);
                    break;
                case "Text":
                    this.Text = (string)propertyData.Value;
                    UpdateProperty(propertyData.Key, true, true, false);
                    break;
                case "HighlightBrightness":
                    this.HighlightBrightness = (int)propertyData.Value;
                    UpdateProperty(propertyData.Key, true, true, false);
                    break;
                case "Volume":
                    this.Volume = (double)propertyData.Value;
                    UpdateProperty(propertyData.Key, true, true, false);
                    break;
                case "Mute":
                    this.IsMuted = (bool)propertyData.Value;
                    UpdateProperty(propertyData.Key, true, true, false);
                    break;
                case "Stretch":
                    this.Stretch = (Stretch)propertyData.Value;
                    UpdateProperty(propertyData.Key, true, true, false);
                    break;
                case "LifeTime":
                    this.LifeTime = (double)propertyData.Value;
                    UpdateProperty(propertyData.Key, true, true, false);
                    break;
                case "PageLifeTime":
                    this.LifeTime = (double)propertyData.Value;
                    UpdateProperty(propertyData.Key, true, true, false);
                    break;               
                case "PageElements":
                    UpdateProperty(propertyData.Key, true, true, false);
                    break;
                case "DisplayOrder":
                    this.DisplayOrder = (int)propertyData.Value;
                    UpdateProperty(propertyData.Key, true, true, false);
                    break;
                case "Playlist":
                    this.Playlist = propertyData.Value as List<PlaylistItem>;
                    UpdateProperty(propertyData.Key, true, true, false);
                    break;
                case "Alignment":
                    this.Alignment = (Alignment)propertyData.Value;
                    UpdateProperty(propertyData.Key, true, true, false);
                    break;
                case "FitToPage":
                    this.FitPage = (FitToPage)propertyData.Value;
                    UpdateProperty(propertyData.Key, true, true, false);
                    break;
                case "ButtonStyle":
                    //this.ButtonStyle = (Style)propertyData.Value;
                    if (propertyData.Value.GetType().Name.Equals("String") == true)
                    {
                        this.ButtonStyle = (Style)ObjectConverters.ConvertPropertyObjectFromPropertyXamlString(propertyData.Value.ToString());
                    }
                    else
                    {
                        this.ButtonStyle = (Style)propertyData.Value;
                    }
                    UpdateProperty(propertyData.Key, true, true, false);
                    break;
                case "Type":
                    Type type = (Type)propertyData.Value;
                    this.Type = type;
                    UpdateProperty(propertyData.Key, true, true, false);
                    break;
                case "TouchEvent":
                    this.TouchEvent = (Dictionary<string, string>)propertyData.Value;
                    UpdateProperty(propertyData.Key, true, true, false);
                    break;
                //case "EventName":
                //    this.EventName = (string)propertyData.Value;
                //    UpdateProperty(propertyData.Key, true, true, false);
                //    break;
                case "SlideViewWidth":
                    this.SlideViewWidth = (double)propertyData.Value;
                    UpdateProperty(propertyData.Key, true, true, false);
                    break;
                case "SlideViewHeight":
                    this.SlideViewHeight = (double)propertyData.Value;
                    UpdateProperty(propertyData.Key, true, true, false);
                    break;
                default:
                    break;
            }

        }

        private void UpdateProperty(string propertyName, bool isShowControl, bool isShowLabel, bool isReadOnly)
        {
            PropertySetting propertySetting = new PropertySetting(isShowControl, isShowLabel, isReadOnly);
            if (SupportProperties.ContainsKey(propertyName) == false)
                SupportProperties.Add(propertyName, propertySetting);
            else
                SupportProperties[propertyName] = propertySetting;
        }
       
        #endregion
    }
}
