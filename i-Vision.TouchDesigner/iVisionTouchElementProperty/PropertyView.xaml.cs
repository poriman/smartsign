﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Caliburn.Core;
using Caliburn.PresentationFramework;
using iVisionTouchElementProperty.Models;
using System.Windows.Media.Animation;
using InSysBasicControls.InSysProperties;
using DenisVuyka.Controls.PropertyGrid.Data;
using System.Globalization;

namespace iVisionTouchElementProperty
{
    /// <summary>
    /// Interaction logic for PropertyView.xaml
    /// </summary>
    public partial class PropertyView : UserControl
    {        
        public bool IsProperyViewShow { get; set; }

        public static readonly DependencyProperty PropertyWidthProperty= DependencyProperty.Register("PropertyWidth", typeof(double), typeof(PropertyView), new PropertyMetadata(300.0)); 
        public double PropertyWidth
        {
            get { return (double)base.GetValue(PropertyWidthProperty); }
            set { base.SetValue(PropertyWidthProperty, value); }
        }

        private readonly static DependencyProperty PropertyInstanceProperty = DependencyProperty.Register("PropertyInstance", typeof(object), typeof(PropertyView), new PropertyMetadata(null, new PropertyChangedCallback(PropertyChanged_PropertyInstance)));

        public object PropertyInstance
        {
            get { return (object)GetValue(PropertyInstanceProperty); }
            set { SetValue(PropertyInstanceProperty, value); }
        }

        private static void PropertyChanged_PropertyInstance(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            PropertyView z = d as PropertyView;
            if (z != null)
                z.PropertyChanged_PropertyInstance(e);
        }

        private void PropertyChanged_PropertyInstance(DependencyPropertyChangedEventArgs e)
        {
            if (e.NewValue != null)
            {
                Type type = e.NewValue.GetType();
                this.PropertyGrid.SelectedObject = e.NewValue;
            }
            else
            {
                this.PropertyGrid.SelectedObject = null;
            }
        }

        public PropertyView()
        {
            
            InitializeComponent();

            if (System.ComponentModel.DesignerProperties.GetIsInDesignMode(this) != true)
            {
                CaliburnFramework.ConfigureCore().WithPresentationFramework().Start();
                IsProperyViewShow = true;
            }
           
            this.DataContext = this;

            this.Loaded += (s, e) =>
            {
                this.Width = PropertyWidth; 
            };
        }

        public void SetCategories(ICategoryData categoryData)
        {
            PropertyGrid.TryHideAllCategoryControl();//카테고리 전체 삭제
           
            ElementPropertyObject elementPropertyObject = this.FindResource("elementPropertyObject") as ElementPropertyObject;
            if (elementPropertyObject == null)
            {
                return;
            }

            //categoryData.SetPropertyValues(elementPropertyObject);            
           
            string typeName = "";
            foreach (var type in categoryData.SupportCategroyTypeList)
            {            
                typeName = type.ToString();
                PropertyGrid.TryShowCategoryControl(typeName);
            }

            PropertyGrid.UpdateLayout();//Property View 업데이트 - 없어면 정상동작하지 않은.
            PropertyGrid.TryHideAllPropertyItemContainer();//모든 속성 숨김
            
            foreach (var property in categoryData.SupportProperties)
            {
                #region TextBox를 가진 Property 수정후 Enter 키 입력으로 적용 - 구현하지 못함
                /*
                FrameworkElement frameworkElement = PropertyGrid.TryGetPropertyItemControl(property.Key);
                if (frameworkElement != null)
                {
                    if (frameworkElement.GetType() == typeof(TextBox))
                    {
                        TextBox tb = frameworkElement as TextBox;
                        tb.KeyDown += (s, e) =>
                        {
                            if (e.Key == Key.Enter)
                            {
                                this.PropertyGrid.UpdateLayout();
                            }
                        };
                    }
                }
                 */ 
                #endregion

                PropertySetting proeprtySetting = property.Value;
                PropertyGrid.TryShowHidePropertyItemContainer(property.Key, proeprtySetting.IsShowControl, proeprtySetting.IsReadOnly);//Property 컨트롤 설정.
                PropertyGrid.TryShowHidePropertyLabelControl(property.Key, proeprtySetting.IsShowLabel);//Label 컨트롤 제어
            }           

            PropertyGrid.UpdateLayout();//Property View 업데이트 - 없어면 정상동작하지 않은.            
        }

        public void SetPageCategories(ICategoryData categoryData)
        {
            PropertyGrid.TryHideAllCategoryControl();//카테고리 전체 삭제

            ElementPropertyObject elementPropertyObject = this.FindResource("elementPropertyObject") as ElementPropertyObject;
            if (elementPropertyObject == null)
            {
                return;
            }
            //categoryData.SetPropertyValues(elementPropertyObject);            

            string typeName = "";
            foreach (var type in categoryData.SupportCategroyTypeList)
            {
                typeName = type.ToString();
                PropertyGrid.TryShowCategoryControl(typeName);
            }

            PropertyGrid.UpdateLayout();//Property View 업데이트 - 없어면 정상동작하지 않은.
            PropertyGrid.TryHideAllPropertyItemContainer();//모든 속성 숨김

            foreach (var property in categoryData.SupportProperties)
            {
                PropertySetting proeprtySetting = property.Value;
                PropertyGrid.TryShowHidePropertyItemContainer(property.Key, proeprtySetting.IsShowControl, proeprtySetting.IsReadOnly);//Property 컨트롤 설정.
                PropertyGrid.TryShowHidePropertyLabelControl(property.Key, proeprtySetting.IsShowLabel);//Label 컨트롤 제어
            }
            PropertyGrid.UpdateLayout();//Property View 업데이트 - 없어면 정상동작하지 않은.            

            //this.PropertyInstance = elementPropertyObject;
        }

        private void ShowButton_Click(object sender, RoutedEventArgs e)
        {            
            if (this.IsProperyViewShow == true)
            {                
                this.IsProperyViewShow = false;
                Storyboard sbdLabelRotation = (Storyboard)FindResource("HideStoryboard");

                DoubleAnimationUsingKeyFrames animation = sbdLabelRotation.Children[0] as DoubleAnimationUsingKeyFrames;
                EasingDoubleKeyFrame keyframe = animation.KeyFrames[0] as EasingDoubleKeyFrame;
                keyframe.Value = PropertyWidth;
 

                sbdLabelRotation.Begin();

                TransformGroup group = (TransformGroup)ArrowPath.RenderTransform;
                RotateTransform scale = (RotateTransform)group.Children[2];
                scale.Angle = 0;
               
            }
            else
            {                
                this.IsProperyViewShow = true;
                Storyboard sbdLabelRotation = (Storyboard)FindResource("ShowStoryboard");
                
                DoubleAnimationUsingKeyFrames animation = sbdLabelRotation.Children[0] as DoubleAnimationUsingKeyFrames;
                EasingDoubleKeyFrame keyframe = animation.KeyFrames[1] as EasingDoubleKeyFrame;
                keyframe.Value = PropertyWidth;

                sbdLabelRotation.Begin();

                TransformGroup group = (TransformGroup)ArrowPath.RenderTransform;
                RotateTransform scale = (RotateTransform)group.Children[2];
                scale.Angle = 180;
            }
        }
        
        bool isPressed = false;        
        Cursor beforeCursor;      

        Point point = new Point();
        Point beforepoint = new Point();
        private void rectangle_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            isPressed = true;
            beforeCursor = this.Cursor;

            point = e.GetPosition(Application.Current.MainWindow);
            this.Cursor = Cursors.ScrollWE;
            beforepoint = point;
            Mouse.Capture(rectangle);
        }

        private void rectangle_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            isPressed = false;
            this.Cursor = Cursors.Arrow;
            rectangle.ReleaseMouseCapture();
        }
      
        private void rectangle_MouseEnter(object sender, MouseEventArgs e)
        {
            this.Cursor = Cursors.ScrollWE;
        }

        private void rectangle_MouseLeave(object sender, MouseEventArgs e)
        {
            this.Cursor = Cursors.Arrow;
        }
                
        private void rectangle_MouseMove(object sender, MouseEventArgs e)
        {
            point = e.GetPosition(Application.Current.MainWindow);
            if (e.LeftButton == MouseButtonState.Pressed && isPressed == true)
            {
                double diff = point.X - beforepoint.X;
                if (diff < 0)
                {
                    PropertyWidth = PropertyWidth + diff;                   
                }
                else if (diff > 0)
                {
                    PropertyWidth = PropertyWidth + diff;                    
                }               
            }
            beforepoint = point;
        }

        public FrameworkElement GetControlContentOfProperty(string propertyName)
        {
            this.PropertyGrid.UpdateLayout();

            PropertyItem name = this.PropertyGrid.Properties.OfType<PropertyItem>().Where(property => property.PropertyName.Equals(propertyName) == true).FirstOrDefault();
            if (name == null)
            {
                return null;
            }

            FrameworkElement frameworkElement = this.PropertyGrid.TryFindContent(name);
            if (frameworkElement == null)
            {
                return null;
            }

            return frameworkElement;
        }

        public object GetPropertyGridObject()
        {
            return this.PropertyGrid;
        }

        #region Language 설정

        public static void setCultureInfo(CultureInfo info, ObjectDataProvider resourceProvider)
        {
            DenisVuyka.Controls.PropertyGrid.PropertyGrid.setCultureInfo(info, resourceProvider);
        }
        #endregion
    }
}
