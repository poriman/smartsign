﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using iVisionTouchToolbox.Models;
using iVisionTouchElementProperty.Models;
using iVisionTouchTimelineBody.Model;
using iVisionPageFlowView.Core;
using iVisionPageFlowView.Util;
using DesignWorkCanvas;
using DesignWorkCanvas.Views;
using InSysTouchflowData.Models.ProjectModels;
using iVisionTouchDesigner.Controls;
using InSysTouchflowData.Events;
using InSysBasicControls.Interfaces;
using InSysTouchflowData.InSysProperties;
using InSysTouchflowData;
using InSysBasicControls.InSysProperties;
using System.Xml.Linq;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using InSysBasicControls.PropertyControls;
using DenisVuyka.Controls.PropertyGrid.Editors;
using System.Windows.Media;
using UtilLib.StaticMethod;
using System.Globalization;
using iVisionTouchDesigner.Cultures;
using System.Windows.Data;

namespace iVisionTouchDesigner
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
		private NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();// NLog.LogManager.GetLogger("TouchDesignerLog");
        private bool IsInsertPageState = true;
        private MainManager MainManager = MainManager.Instance;
        Slider pageflowZoomSlider = null;
        double pageflowZoomSliderValue = 1.0;
        ScaleTransform pageflowScaleTransform = new ScaleTransform(1.0, 1.0);

        public MainWindow()
        {
            #region Language 설정

            CultureInfo info = Properties.Settings.Default.DefaultCulture;
            if (info != null)
            {
                //info = new CultureInfo("en-US");//en-US, ko-KR
                Cultures.Resources.Culture = info;
                CultureResources.ChangeCulture(info);
                Thread.CurrentThread.CurrentCulture = info;
            }
            
            iVisionTouchElementProperty.PropertyView.setCultureInfo(Cultures.Resources.Culture, Cultures.CultureResources.ResourceProvider);//Language 적용 - InitializeComponent 함수 이전에 셋팅해야함.
            #endregion

            InitializeComponent();

            this.Loaded += new RoutedEventHandler(MainWindow_Loaded);
           
            SetWindowResize(this);            

            InSysBasicControls.InSysControlManager.SetPropertyGrid(this.PropertyView.GetPropertyGridObject());
            MainManager.SetMainManager(this);   
        }

        /// <summary>
        /// MainWindow Loaded Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            #region Language 설정
            this.pageflowview.setCultureInfo(Cultures.Resources.Culture);
            this.timelinebody.setCultureInfo(Cultures.Resources.Culture);
            DesignContainer.setCultureInfo(Cultures.Resources.Culture);
            #endregion

            this.MainManager.HideTouchDesignerWindow();
            //Test_InsertSamplePages();
            timelinebody.DataContext = this.MainManager.TimelineData;

            this.timelinebody.UpdatedTimelineContentItemBoxEventHandler = new iVisionTouchTimelineBody.View.TimelineBody.ChangedTimelineSettingDelegate(Update_TimlineContentItems);
            this.timelinebody.ChangedContentItemEventHandler = new ChangedContentItemDelegate(ChangedContentItem);
            this.pageflowview.PageFlowViewCommandHandler = new iVisionPageFlowView.PageFlowView.PageFlowViewCommandDelegate(PageFlowViewCommand);


            #region 초기 Timeline Tab이 Load 되도록 적용
            this.flowviewTabItem.IsSelected = true;
            this.flowviewTabItem.UpdateLayout();
            this.timelineTabItem.IsSelected = true;
            ShowPageFlowZoomSliderControl(false);
            this.timelineTabItem.UpdateLayout();

            #endregion

            #region TimelineContent DoubleClick Event 콜백 함수
            timelinebody.TimelineContentDoubleClick += (s1, eventargs) =>
            {
                ContentItemBoxData itemboxData = eventargs.OriginalSource as ContentItemBoxData;
                if (itemboxData != null)
                {
                    IDesignElement designElement = itemboxData.Item as IDesignElement;
                    if (designElement != null && designElement.IsSupportPlaylist == true)
                    {
                        InSysBasicControls.InSysControlManager.ShowInSysControlPlayerEditor(designElement);
                    }
                }
            };
            #endregion

            #region TimelineContent SelectChanged Event 콜백 함수
            timelinebody.TimelineContentSelectChanged += (s2, eventargs) =>
            {
                ContentItemBoxData itemboxData = eventargs.OriginalSource as ContentItemBoxData;
                if (itemboxData != null)
                {
                    FrameworkElement frameworkElement = itemboxData.Item as FrameworkElement;
                    if (frameworkElement != null)
                    {
                        this.MainManager.CurrentDesignContainer.releaseAllAdorner();
                        this.MainManager.CurrentDesignContainer.selectContentElement(frameworkElement);
                        this.MainManager.CurrentDesignContainer.selectItemAdorner(frameworkElement);
                    }
                }
            };
            #endregion

            InitStringResource();

            MainManager.StartProject();
        }
        
        private void InitStringResource()
        {
            titleTouchDesigner.Text = Cultures.Resources.titleTouchDesignerWindow;
            iVisionTouchDesignerMinimizeBtn.ToolTip = Cultures.Resources.toolTipMinimize;
            iVisionTouchDesignerMaximizeBtn.ToolTip = Cultures.Resources.toolTipMaximize;
            iVisionTouchDesignerCloseBtn.ToolTip = Cultures.Resources.toolTipClose;
            touchProjectSave.ToolTip = Cultures.Resources.menuFileSave;
            touchProjectNewPage.ToolTip = Cultures.Resources.menuFileNew;
            iVissionFileMenuItem.Header = Cultures.Resources.labelFile;
            iVissionSettingMenuItem.Header = Cultures.Resources.menuSetting;
            //iVissionWindowMenuItem.Header = Cultures.Resources.menuWindow;
            iVissionHelpMenuItem.Header = Cultures.Resources.menuHelp;

            NewProject.Header = Cultures.Resources.menuNewProject;
            OpenProject.Header = Cultures.Resources.menuOpneProject;
            NewScreen.Header = Cultures.Resources.menuFileNew;
            CloseScreen.Header = Cultures.Resources.menuFileClose;
            CloseProject.Header = Cultures.Resources.menuCloseProject;
            SaveProject.Header = Cultures.Resources.menuSaveProject;
            SaveAsProject.Header = Cultures.Resources.menuSaveAsProject;
            ExitWindow.Header = Cultures.Resources.menuExit;

            menuLanguage.Header = Cultures.Resources.menuitemLanguage;
            menuAbout.Header = Cultures.Resources.menuTouchDesignerAbout;

            ProjectPreviewButton.Content = Cultures.Resources.strProjectPreview;
            PagePreviewButton.Content = Cultures.Resources.strScreenPreview;

            flowviewTabItem.Header = Cultures.Resources.strFlowTitle;
            timelineTabItem.Header = Cultures.Resources.strTimelineTitle;

        }

        public void PageFlowViewCommand(ICommand command)
        {
            try
            {
                if (command == ApplicationCommands.Copy)
                {
                    #region PageFlow 화면에서 Page 복사시 Serialize 지원되지 않는 객체에 대해 처리
                    TouchPageInfo touchpageinfo = this.pageflowview.SelectedFlowPageItem.PageInfoData as TouchPageInfo;
                    foreach (var contentInfo in touchpageinfo.Contents)
                    {
                        if (contentInfo != null)
                        {
                            IDesignElement designElement = contentInfo.ContentObject as IDesignElement;
                            contentInfo.DesignElementProperties = InSysBasicControls.InSysControlManager.SerializePropertiesInIDesignElement(designElement.Properties);
                        }
                    }
                    #endregion

                    var rootXml = new XElement("DesignerItem", new XElement("TouchflowPageInfo", Serialize(this.pageflowview.SelectedFlowPageItem.PageInfoData)));

                    Clipboard.Clear();
                    Clipboard.SetData(DataFormats.Xaml, rootXml);
                }
                else if (command == ApplicationCommands.Cut)
                {
                }
                else if (command == ApplicationCommands.Delete)
                {
                    this.DeleteFlowPageItem(this.pageflowview.SelectedFlowPageItem);
                }
                else if (command == ApplicationCommands.Paste)
                {
                    this.IsInsertPageState = false;
                    try
                    {
                        XElement rootXml = LoadSerializedDataFromClipBoard();
                        XElement itemXML = rootXml.Elements("TouchflowPageInfo").SingleOrDefault();

                        Object content = DeSerialize(itemXML.Value);
                        TouchPageInfo copyPageInfo = content as TouchPageInfo;

                        TouchPageInfo selectedPageInfo = this.pageflowview.SelectedFlowPageItem.PageInfoData as TouchPageInfo;

                        if (selectedPageInfo != null)
                        {
                            int pageLayerIndex = selectedPageInfo.PageLayerIndex + 1;//부모의 자식 layer로 등록한다.                        

                            #region DesignContainer 생성
                            DesignContainer designContainer = this.CreateNewTouchPage(Guid.NewGuid(),
                                new Guid(selectedPageInfo.PageGuid),
                                this.MainManager.PageWidth,
                                this.MainManager.PageHeight,
                                this.MainManager.DefaultPageLifeTime,
                                this.MainManager.DefaultPageName,
                                pageLayerIndex);
                            #endregion

                            this.copyTouchPageInfo(copyPageInfo, designContainer.TouchPageInfo);

                            var sameIndexList = this.MainManager.GetSameLayerPageInfo(designContainer.TouchPageInfo);
                            designContainer.TouchPageInfo.PageDisplayOrder = sameIndexList.Count() + 1;

                            //double pagelifetime = this.MainManager.DefaultPageLifeTime;
                            //string pagename = this.MainManager.DefaultPageName;
                            //double width = this.MainManager.PageWidth;
                            //double height = this.MainManager.PageHeight;

                            this.MainManager.InsertTouchPage(designContainer);
                            this.pageflowview.ArrangePageThumbItems();
                        }
                    }
                    catch (Exception ex)
                    {
                    }
                    finally
                    {
                        this.IsInsertPageState = true;
                    }
                }
                else if (command == ApplicationCommands.New)
                {
                    this.Create_NewPage(this.pageflowview.SelectedFlowPageItem);
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message + " >> " + ex.StackTrace);
            }
        }

        private void DeleteFlowPageItem(FlowPageItem delete_flowpageitem)
        {
            if (delete_flowpageitem != null)
            {
                string deletePageID = delete_flowpageitem.ID.ToString();
                PageTabItem pagetabitem = this.SearchPageTabItem(deletePageID);
                if (pagetabitem != null)
                {
                    TouchPageInfo deleteTouchPageInfo = this.getTouchPageInfoFromPageTabItem(pagetabitem);//삭제될 TouchPageInfo 구하기
                    if (deleteTouchPageInfo != null)
                    {
                        string parentPageID = this.pageflowview.getParentPageIDFromFlow(deleteTouchPageInfo.PageGuid);//삭제될 TouchPageInfo의 부모 TouchPageInfo 구하기
                        if (this.pageflowview.DeleteFlowPageItem(delete_flowpageitem) == true)
                        {
                            this.TouchDesignTabControl.RemoveTabItem(deletePageID);
                            this.MainManager.RemoveTouchPageInfo(deleteTouchPageInfo);
                            this.pageflowview.selectFlowPageItemAfterDelete(deleteTouchPageInfo);

                            if (string.IsNullOrEmpty(parentPageID) == false)
                            {
                                PageTabItem selectPageTabItem = this.SearchPageTabItem(parentPageID);
                                selectPageTabItem.IsSelected = true;
                            }
                        }
                    }
                }
            }
        }

        private void ChangedContentItem(ContentItemBoxData contentItemBoxData, ContentItemData contentItemData)
        {
            if (this.MainManager.CurrentDesignContainer.TouchPageInfo != null)
            {
                foreach (var item in this.MainManager.CurrentDesignContainer.TouchPageInfo.Contents)
                {
                    try
                    {
                        if (contentItemBoxData.Item.Equals(item.ContentObject) == true)
                        {
                            List<PlaylistItem> playlistItems = (List<PlaylistItem>)item.DesignElementProperties["Playlist"];
                            foreach (var playlistitem in playlistItems)
                            {
                                if (contentItemData.Playlistitem.Equals(playlistitem) == true)
                                {
                                    #region Playlist Item 의 Time을 업데이트 한다.
                                    playlistitem.TIME = TimeSpan.FromSeconds(contentItemData.TotalSeconds);
                                    playlistitem.Duration = System.Windows.Markup.XamlWriter.Save(playlistitem.TIME);
                                    #endregion
                                    break;
                                }
                            }
                            //IDesignElement designElement = item.ContentObject as IDesignElement;
                            //if (designElement != null)
                            //    this.MainManager.UpdateContentItemData(designElement, designElement.Playlist);
                            break;
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message + "\n" + ex.StackTrace);
                    }
                }
            }            
        }

        public void Update_TimlineContentItems(string tmelineHandUnitType, int timelineHandDistance, int timelineHandIntervalTime, double timelineTotalTime)
        {
            this.MainManager.TimelineData.SetTimelineSetting(tmelineHandUnitType, timelineHandDistance, timelineHandIntervalTime, timelineTotalTime);

            //this.updateTimlineContentItemBox(this.MainManager.CurrentDesignContainer.TouchPageInfo);
        }

        public void updateTimlineContentItemBox(TouchPageInfo touchpageinfo)
        {
            if (touchpageinfo != null)
            {
                this.MainManager.ResetTimelineLength();

                foreach (var contentInfo in touchpageinfo.Contents)
                {
                    IDesignElement designElement = contentInfo.ContentObject as IDesignElement;
                    if (designElement != null )
                    {
                        if (designElement.IsApplyLifeTime == false)
                        {
                            //컨텐츠 요소의 LifeTime이 체크되어 있지 않을 경우 EndTime을 Screen의 ScreenTime으로 설정.
                            designElement.EndTime = TimeSpan.FromSeconds(touchpageinfo.PageLifeTimeTemp);
                        }
                        this.MainManager.ResizeTimeline(designElement);
                    }
                }                
            }
        }

        public ContentItemBoxData CreateNewContentItemBoxData(FrameworkElement frameworkElement, DesignContainer containerCanvas)
        {
            ContentItemBoxData contentItemBoxData = new ContentItemBoxData();
            IDesignElement designElement = frameworkElement as IDesignElement;
            if (designElement != null)
            {
                #region Element Type 명 얻기
                string elementTypeName;
                string elementHeaderImagePath="";//Playlist를 지원하지않는 Element의 Header 이미지 경로.
                switch (designElement.InSysControlType)
                {
                    case InSysControlType.InSysSlideViewer:
                        elementTypeName = ToolboxType.SlideShow.ToString();
                        //elementHeaderImagePath = string.Format("{0}", @"\images\test_image1.jpg");
                        break;
                    case InSysControlType.InSysBasicButton:
                        elementTypeName = ToolboxType.Button.ToString();
                        elementHeaderImagePath = string.Format("{0}", @"\images\timeline_button.png");
                        break;
                    case InSysControlType.InSysImageBox:
                        elementTypeName = ToolboxType.Image.ToString();
                        //elementHeaderImagePath = string.Format("{0}", @"\images\test_image1.jpg");
                        break;
                    case InSysControlType.InSysRectangle:
                        elementTypeName = ToolboxType.Rectangle.ToString();
                        elementHeaderImagePath = string.Format("{0}", @"\images\test_image1.jpg");
                        break;
                    case InSysControlType.InSysRectArea:
                        elementTypeName = ToolboxType.RectArea.ToString();
                        elementHeaderImagePath = string.Format("{0}", @"\images\timeline_rectangleArea.png");
                        break;
                    case InSysControlType.InSysBasicTextBox:
                        elementTypeName = ToolboxType.Text.ToString();
                        elementHeaderImagePath = string.Format("{0}", @"\images\timeline_text.png");
                        break;
                    case InSysControlType.InSysLabel:
                        elementTypeName = ToolboxType.Text.ToString();
                        elementHeaderImagePath = string.Format("{0}", @"\images\timeline_text.png");
                        break;
                    case InSysControlType.InSysStreamingBox:
                        elementTypeName = ToolboxType.Streaming.ToString();
                        //elementHeaderImagePath = string.Format("{0}", @"\images\toolbox_streaming.png");
                        break;
                    case InSysControlType.InSysVideoBox:
                        elementTypeName = ToolboxType.Video.ToString();
                        //elementHeaderImagePath = string.Format("{0}", @"\images\test_image1.jpg");
                        break;
                    case InSysControlType.InSysAudioBox:
                        elementTypeName = ToolboxType.Audio.ToString();
                        //elementHeaderImagePath = string.Format("{0}", @"\images\test_image1.jpg");
                        break;
                    case InSysControlType.InSysAnalogueClock:
                        elementTypeName = ToolboxType.AnalogueClock.ToString();
                        elementHeaderImagePath = string.Format("{0}", @"\images\test_image1.jpg");
                        break;
                    case InSysControlType.InSysDate:
                        elementTypeName = ToolboxType.Date.ToString();
                        elementHeaderImagePath = string.Format("{0}", @"\images\test_image1.jpg");
                        break;
                    case InSysControlType.InSysDigitalClock:
                        elementTypeName = ToolboxType.DigitalClock.ToString();
                        elementHeaderImagePath = string.Format("{0}", @"\images\test_image1.jpg");
                        break;
                    case InSysControlType.InSysEllipse:
                        elementTypeName = ToolboxType.Ellipse.ToString();
                        elementHeaderImagePath = string.Format("{0}", @"\images\test_image1.jpg");
                        break;
                    case InSysControlType.InSysRSS:
                        elementTypeName = ToolboxType.RSS.ToString();
                        elementHeaderImagePath = string.Format("{0}", @"\images\test_image1.jpg");
                        break;
                    case InSysControlType.InSysScrollText:
                        elementTypeName = ToolboxType.ScrollText.ToString();
                        elementHeaderImagePath = string.Format("{0}", @"\images\timeline_text.png");
                        break;
                    case InSysControlType.InSysWebControl:
                        elementTypeName = ToolboxType.Web.ToString();
                        //elementHeaderImagePath = string.Format("{0}", @"\images\test_image1.jpg");
                        break;
                    case InSysControlType.InSysFlashBox:
                        elementTypeName = ToolboxType.Flash.ToString();
                        //elementHeaderImagePath = string.Format("{0}", @"\images\test_image1.jpg");
                        break;
                    default:
                        elementTypeName = ToolboxType.None.ToString();
                        elementHeaderImagePath = string.Format("{0}", @"\images\test_image1.jpg");
                        break;
                }
                #endregion

                contentItemBoxData.ContentTypeName = elementTypeName;
                contentItemBoxData.ContentName = designElement.Name;
                contentItemBoxData.Item = designElement;

                if (designElement.IsSupportPlaylist == false)
                {                    
                    ContentItemData contentItemData = new ContentItemData(); 
                    contentItemData.ContentName = designElement.Name;
                    contentItemData.Thumbnail = ObjectConverters.GetImageSource(elementHeaderImagePath, UriKind.Relative);// image.Source;                   
                    this.Set_ContentItemData(contentItemData, designElement, containerCanvas);
                    
                    contentItemData.OrderIndex = 0;

                    contentItemBoxData.ContentBoxLength = contentItemData.ContentLength;
                    contentItemBoxData.ContentItemDatas.Add(contentItemData);
                }

                return contentItemBoxData;
            }

            return null;
        }

        public void Set_ContentItemData(ContentItemData contentItemData, IDesignElement designElement, DesignContainer containerCanvas)
        {
            if (designElement.IsApplyLifeTime == true)//Element 플레이 시간 설정 - 설정이 되어 있을 경우는 Element 플레이 시간 그대로 한다.
            {
                contentItemData.ContentLength = this.timelinebody.GetContentItemLength(designElement.ControlTimeDuration);// designElement.ControlTimeDuration * this.MainManager.TimelineData.TimelineHandDistance / this.MainManager.TimelineData.TimelineHandIntervalTime;
                contentItemData.ContentPlayTime = this.timelinebody.ContentItemTimeDisplay(contentItemData.ContentLength);
                contentItemData.TotalSeconds = designElement.ControlTimeDuration;                
            }
            else
            {
                if (containerCanvas.TouchPageInfo.IsApplyPageLifeTime == true)//페이지 플레이 시간 설정
                {
                    contentItemData.ContentLength = this.timelinebody.GetContentItemLength(containerCanvas.TouchPageInfo.PageLifeTimeTemp);//containerCanvas.TouchPageInfo.PageLifeTimeTemp * this.MainManager.TimelineData.TimelineHandDistance / this.MainManager.TimelineData.TimelineHandIntervalTime;
                    contentItemData.ContentPlayTime = this.timelinebody.ContentItemTimeDisplay(contentItemData.ContentLength);
                    contentItemData.TotalSeconds = containerCanvas.TouchPageInfo.PageLifeTimeTemp;
                }
                else
                {
                    contentItemData.ContentLength = this.timelinebody.GetContentItemLength(this.MainManager.TimelineData.TimelineTotalTime); //this.MainManager.TimelineData.TimelineTotalTime * this.MainManager.TimelineData.TimelineHandDistance / this.MainManager.TimelineData.TimelineHandIntervalTime;
                    contentItemData.ContentPlayTime = "∞";
                    contentItemData.TotalSeconds = this.timelinebody.GetTimelineHeaderTimeSeconds();//0;// this.MainManager.TimelineData.TimelineTotalTime; ;
                }
            }
        }

        public void AddPageTabItem(string pagename, DesignContainer containerCanvas)
        {
            #region Page TabItem 추가

            PageTabItem tabitem = new PageTabItem();
            tabitem.HeaderText = pagename;
            tabitem.Content = containerCanvas;
            this.TouchDesignTabControl.AddTabItem(tabitem);
            tabitem.Focus();
            MainManager.AddPageTabItem(tabitem);

            #endregion
        }

        public DesignContainer CreateNewDesignContainer(double width, double height, double pagelifetime, string pagename, int pageDepthIndex)
        {
            #region DesignContainer 생성
            DesignContainer designContainerCanvas = this.MainManager.CreateTouchDesignContainerCanvas();
            #endregion

            #region ContainerCanvas(designContainerCanvas) 이벤트 등록.

            designContainerCanvas.Loaded += new RoutedEventHandler(designContainerCanvas_Loaded);//ContainerCanvas Load 함수
            designContainerCanvas.SelectedContent += new RoutedEventHandler(designContainerCanvas_SelectedContent);//SelectedContent 이벤트 콜백 함수- Content Element 선택시 호출
            designContainerCanvas.InsertContent += new RoutedEventHandler(designContainerCanvas_InsertContent);//InsertContent 이벤트 콜백 함수- Content Element 추가시 호출
            designContainerCanvas.DeleteContent += new RoutedEventHandler(designContainerCanvas_DeleteContent);//DeleteContent 이벤트 콜백 함수- Content Element 삭제시 호출

            designContainerCanvas.SelectedPage += new RoutedEventHandler(designContainerCanvas_SelectedPage);//Page 선택시 호출 함수
            designContainerCanvas.PageUpdate += new RoutedEventHandler(designContainerCanvas_PageUpdate);//Page Property Thumbnail Update시 호출 함수
            designContainerCanvas.ItemUpdatedProperty += new RoutedEventHandler(designContainerCanvas_ItemUpdatedProperty);//Content Element Property Update시 호출 함수
           
            #endregion   
           
            Set_ChangedContentElementPropertyCallback(); 

            #region Page Property Callback

            changedPagePropertyCallback();            

            #endregion

            #region Page TabItem 추가

            this.AddPageTabItem(pagename, designContainerCanvas);           

            #endregion
            
            return designContainerCanvas;
        }

        void designContainerCanvas_ItemUpdatedProperty(object sender, RoutedEventArgs e)
        {
            DesignContainer designContainerCanvas = sender as DesignContainer;
            if (designContainerCanvas != null)
            {
                ItemUpdatedPropertyEventArgs eventArgs = e as ItemUpdatedPropertyEventArgs;
                this.MainManager.ChangedContentElementPropertyValue(eventArgs.Sender as FrameworkElement, eventArgs.PropertyName);

                designContainerCanvas.RaisePageUpdateEvent(null);//Playlist Editor 창에서 Playlist 업데이트시 flow view 썸네일 업데이트 시키는 부분.
            }
        }

        void designContainerCanvas_PageUpdate(object sender, RoutedEventArgs e)
        {
             DesignContainer designContainerCanvas = sender as DesignContainer;
             if (designContainerCanvas != null)
             {
                 designContainerCanvas.UpdateLayout();
                 FlowPageItem flowpageitem = this.pageflowview.FindFlowPageItem(designContainerCanvas.TouchPageInfo.PageGuid, designContainerCanvas.TouchPageInfo.PageLayerIndex);
                 this.UpdateFlowPageItemCaptureImage(flowpageitem, designContainerCanvas.PageDesignCanvasBox);
             }
        }

        void designContainerCanvas_SelectedPage(object sender, RoutedEventArgs e)
        {
            DesignContainer designContainerCanvas = sender as DesignContainer;
            if (designContainerCanvas != null)
            {
                RoutedEventArgs args = e as RoutedEventArgs;
                this.bindingContentItemBoxData(designContainerCanvas);
                this.ChangedPageProperty(designContainerCanvas);
                this.changeTimelineContentItemBoxSize(this.MainManager.CurrentDesignContainer);
            }
        }

        void designContainerCanvas_DeleteContent(object sender, RoutedEventArgs e)
        {
            DesignContainer designContainerCanvas = sender as DesignContainer;
            if (designContainerCanvas != null)
            {
                FrameworkElement deleteElement = e.OriginalSource as FrameworkElement;
                if (deleteElement != null)
                {
                    IDesignElement designElement = deleteElement as IDesignElement;
                    if (designElement != null)
                    {
                        var deleteContentItemBoxData = this.MainManager.TimelineData.ContentItemBoxDataList.Where(o => o.Item.Equals(designElement) == true).FirstOrDefault();
                        if (deleteContentItemBoxData != null)
                        {
                            this.MainManager.TimelineData.ContentItemBoxDataList.Remove(deleteContentItemBoxData);
                            this.MainManager.TimelineData.DeleteTimelineContentsDictionary(designContainerCanvas.TouchPageInfo.PageGuid, deleteContentItemBoxData);
                        }
                    }
                }
            }
        }

        void designContainerCanvas_InsertContent(object sender, RoutedEventArgs e)
        {
             DesignContainer designContainerCanvas = sender as DesignContainer;
             if (designContainerCanvas != null)
             {

                 ContentItemBoxData contentItemBoxData = new ContentItemBoxData();
                 FrameworkElement insertElement = e.OriginalSource as FrameworkElement;
                 if (insertElement != null)
                 {
                     IDesignElement designElement = insertElement as IDesignElement;
                     if (designElement != null)
                     {
                         designElement.SetDelegate(new InSysBasicControls.Events.UserDelegates.UpdatedItemPropertyDelegate(UpdatedItemProperty));
                     }
                     contentItemBoxData = this.CreateNewContentItemBoxData(e.OriginalSource as FrameworkElement, designContainerCanvas);
                     if (contentItemBoxData != null)
                     {
                         this.MainManager.TimelineData.ContentItemBoxDataList.Add(contentItemBoxData);
                         this.MainManager.TimelineData.AddTimelineContentItemBoxData(designContainerCanvas.TouchPageInfo.PageGuid, contentItemBoxData);
                         //this.timelinebody.RefreshTimeline();
                     }
                 }
             }
        }

        void designContainerCanvas_SelectedContent(object sender, RoutedEventArgs e)
        {
            try
            {
                DesignContainer designContainerCanvas = sender as DesignContainer;
                if (designContainerCanvas != null)
                {
                    RoutedEventArgs args = e as RoutedEventArgs;
                    FrameworkElement selectedElement = args.OriginalSource as FrameworkElement;
                    this.ChangedContentElementProperty(designContainerCanvas, selectedElement);
                    this.SetCurrentContentElementItem(selectedElement);

                    #region 선택된 ContentElement Property 설정
                    this.MainManager.CurrentDesignContainer.Init_SetInSysControl(selectedElement, this.MainManager.TouchProjectInfo.PageInfoList);
                    //double page_lifetime = this.MainManager.CurrentDesignContainer.TouchPageInfo.PageLifeTimeTemp;
                    //bool isapply_lifetime = this.MainManager.CurrentDesignContainer.TouchPageInfo.ApplyLifeTime;
                    //InSysBasicControls.InSysControlManager.Init_ContentElementProperty(selectedElement, page_lifetime, isapply_lifetime);
                    #endregion
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message + ">> \r\n" + ex.StackTrace);
            }
        }

        void designContainerCanvas_Loaded(object sender, RoutedEventArgs e)
        {
            DesignContainer designContainerCanvas = sender as DesignContainer;
            if (designContainerCanvas != null)
            {
                SetDragSource();
                SetDropTarget(designContainerCanvas);
            }
        }

        private static bool changingProperty = false;
        private void Set_ChangedContentElementPropertyCallback()
        {            
            #region ChangedPlaylistProperty 이벤트 콜백 함수- Element의 Playlist 속성 변경시 호출

            if (ElementPropertyObject.ChangedPlaylistPropertyCallback == null)
            {
                ElementPropertyObject.ChangedPlaylistPropertyCallback = new InSysTouchflowData.PropertyChangedDelegate(ChangedPlaylistProperty);               
            }

            #endregion

            //컨텐츠 요소의 Property 값 변경 이벤트
            if (ElementPropertyObject.ContentElementPropertyChanged == null)
            {
                #region Content 요소의 Property 값 변경시 호출되는 콜백 함수

                ElementPropertyObject.ContentElementPropertyChanged = (s, e) =>
                {
                    if (this.MainManager.IsPreviewPlaying == true)
                        return null;

                    DependencyPropertyChangedEventArgs eventArgs = (DependencyPropertyChangedEventArgs)e;
                    UIElement contentElementItem = this.GetCurrentContentElementItem();
                    if (contentElementItem != null)
                    {
                        switch (eventArgs.Property.Name)
                        {
                            case "ZIndex":
                                #region ZIndex 변경

                                if (changingProperty == true) return null;//내부적으로 Property Vlaue 변경중t
                                changingProperty = true;
                                try
                                {
                                    if (this.MainManager.CurrentDesignContainer != null && contentElementItem != null && this.MainManager.CurrentDesignContainer.IsElementInsertingState == false)
                                    {
                                        this.MainManager.CurrentDesignContainer.UpdateZOrder(contentElementItem, (int)eventArgs.NewValue);
                                    }
                                }
                                catch (Exception ex) { Console.WriteLine(ex.Message); }
                                finally
                                {
                                    changingProperty = false;
                                }
                                #endregion
                                break;
                            case "Stretch":
                                break;
                            case "Alignment":
                                break;
                            case "StartTime":
                                if (this.MainManager.CurrentDesignContainer != null && contentElementItem != null && this.MainManager.CurrentDesignContainer.IsElementInsertingState == false)
                                {
                                    IDesignElement designElement = contentElementItem as IDesignElement;
                                    if (designElement != null)
                                    {
                                        TimeSpan starttime = (TimeSpan)eventArgs.NewValue;
                                        designElement.ControlTimeDuration = designElement.EndTime.TotalSeconds - starttime.TotalSeconds;
                                    }
                                    #region 선택된 ContentElement Property 설정
                                    this.MainManager.CurrentDesignContainer.Init_SetInSysControl(contentElementItem, this.MainManager.TouchProjectInfo.PageInfoList);
                                    //double page_lifetime = this.MainManager.CurrentDesignContainer.TouchPageInfo.PageLifeTimeTemp;
                                    //bool isapply_lifetime = this.MainManager.CurrentDesignContainer.TouchPageInfo.ApplyLifeTime;
                                    //InSysBasicControls.InSysControlManager.Init_ContentElementProperty(contentElementItem, page_lifetime, isapply_lifetime);
                                    #endregion

                                    updateTimlineContentItemBox(this.MainManager.CurrentDesignContainer.TouchPageInfo);
                                }
                                break;
                            case "EndTime":
                                if (this.MainManager.CurrentDesignContainer != null && contentElementItem != null && this.MainManager.CurrentDesignContainer.IsElementInsertingState == false)
                                {
                                    IDesignElement designElement = contentElementItem as IDesignElement;
                                    if (designElement != null)
                                    {
                                        TimeSpan endtime = (TimeSpan)eventArgs.NewValue;
                                        designElement.ControlTimeDuration = endtime.TotalSeconds - designElement.StartTime.TotalSeconds;
                                    }

                                    //this.MainManager.ChangedElementPropertyValue(contentElementItem as FrameworkElement, eventArgs.Property.Name);
                                    #region 선택된 ContentElement Property 설정
                                    this.MainManager.CurrentDesignContainer.Init_SetInSysControl(contentElementItem, this.MainManager.TouchProjectInfo.PageInfoList);
                                    //double page_lifetime = this.MainManager.CurrentDesignContainer.TouchPageInfo.PageLifeTimeTemp;
                                    //bool isapply_lifetime = this.MainManager.CurrentDesignContainer.TouchPageInfo.ApplyLifeTime;
                                    //InSysBasicControls.InSysControlManager.Init_ContentElementProperty(contentElementItem, page_lifetime, isapply_lifetime);
                                    #endregion

                                    updateTimlineContentItemBox(this.MainManager.CurrentDesignContainer.TouchPageInfo);
                                }
                                break;
                            case "IsApplyLifeTime":
                                if (this.MainManager.CurrentDesignContainer != null && contentElementItem != null && this.MainManager.CurrentDesignContainer.IsElementInsertingState == false)
                                {
                                    IDesignElement designElement = contentElementItem as IDesignElement;
                                    if (designElement != null)
                                    {
                                        designElement.IsApplyLifeTime = (bool)eventArgs.NewValue;
                                        //this.MainManager.CurrentDesignContainer.TouchPageInfo.IsApplyPageLifeTime = designElement.IsApplyLifeTime;
                                        if (designElement.IsApplyLifeTime != true)
                                        {
                                            designElement.LifeTimeVisibility = designElement.Visibility;
                                            InSysBasicControls.InSysControlManager.Set_InitTimeControl("EndTime");
                                            designElement.StartTime = TimeSpan.FromSeconds(0.0);
                                            designElement.EndTime = TimeSpan.FromSeconds(this.MainManager.CurrentDesignContainer.TouchPageInfo.PageLifeTimeTemp);
                                        }
                                        else
                                        {
                                            designElement.ControlTimeDuration = designElement.EndTime.TotalSeconds - designElement.StartTime.TotalSeconds;
                                        }

                                        #region 선택된 ContentElement Property 설정
                                        this.MainManager.CurrentDesignContainer.Init_SetInSysControl(contentElementItem, this.MainManager.TouchProjectInfo.PageInfoList);
                                        //double page_lifetime = this.MainManager.CurrentDesignContainer.TouchPageInfo.PageLifeTimeTemp;
                                        //bool isapply_lifetime = this.MainManager.CurrentDesignContainer.TouchPageInfo.ApplyLifeTime;
                                        //InSysBasicControls.InSysControlManager.Init_ContentElementProperty(contentElementItem, page_lifetime, isapply_lifetime);
                                        #endregion

                                        InSysBasicControls.InSysControlManager.Set_ControlLifeTimeEditProperty(designElement, designElement.IsApplyLifeTime);

                                        updateTimlineContentItemBox(this.MainManager.CurrentDesignContainer.TouchPageInfo);
                                    }
                                }

                                break;
                            case "X":
                                if (this.MainManager.CurrentDesignContainer != null && contentElementItem != null && this.MainManager.CurrentDesignContainer.IsElementInsertingState == false)
                                {
                                    //this.MainManager.CurrentDesignContainer.MoveContentElementDistanceLine(contentElementItem);//내부함수에서 속성 변경구문//내부함수에서 속성 변경구문으로 오브플로우 발생으로 오브플로우 발생

                                }
                                break;
                            case "Y":
                                if (this.MainManager.CurrentDesignContainer != null && contentElementItem != null && this.MainManager.CurrentDesignContainer.IsElementInsertingState == false)
                                {
                                    //this.MainManager.CurrentDesignContainer.MoveContentElementDistanceLine(contentElementItem);

                                }
                                break;
                            case "Name":
                                {
                                    #region Name 명 비교 - Name Property 변경시 현 Screen내의 요소 Name을 비교하여 동일한 Name일 경우에 대해 적용되지 않도록 한다.
                                    IDesignElement designElement = contentElementItem as IDesignElement;
                                    if (designElement != null)
                                    {
                                        List<FrameworkElement> elementColection = new List<FrameworkElement>();
                                        this.MainManager.CurrentDesignContainer.TouchPageInfo.Contents.Where(o => o.ContentObject.Equals(contentElementItem) == false).Select(o => o.ContentObject).ToList().ForEach(o => elementColection.Add(o));

                                        var sameNameElement = elementColection.Where(o => (o as IDesignElement).Name.Equals((string)eventArgs.NewValue) == true || (o as IDesignElement).Name.ToLower().Equals(((string)eventArgs.NewValue).ToLower()) == true).FirstOrDefault();
                                        if (sameNameElement != null)
                                        {
                                            MessageBox.Show("동일한 이름의 요소가 존재합니다.");
                                            designElement.Name = eventArgs.OldValue as string;
                                            this.MainManager.CurrentDesignContainer.selectItemAdorner(contentElementItem);
                                            break;
                                        }
                                    }
                                    #endregion
                                }
                                break;
                        }
                    }
                    return null;
                };

                #endregion
            }
        }

        public object ChangedPlaylistProperty(object sender, DependencyPropertyChangedEventArgs obj)
        {
            if (this.MainManager.IsPreviewPlaying == true)
                return null;

            DependencyPropertyChangedEventArgs eventArgs = (DependencyPropertyChangedEventArgs)obj;
            List<PlaylistItem> newPlaylistItems = eventArgs.NewValue as List<PlaylistItem>;
            List<PlaylistItem> oldPlaylistItems = eventArgs.OldValue as List<PlaylistItem>;

            if (newPlaylistItems == null)
                return null;

            if (oldPlaylistItems == null && newPlaylistItems.Count == 0)//이 경우는 새로 생성시 호출되는 부분임으로 이후 동작이 불필요하다.
                return null;

            if (oldPlaylistItems != null && oldPlaylistItems.Count == 0 && newPlaylistItems.Count == 0)//이 경우는 새로 생성시 호출되는 부분임으로 이후 동작이 불필요하다.
                return null;

            FrameworkElement contentElementItem = this.GetCurrentContentElementItem() as FrameworkElement;
            if (this.MainManager.CurrentDesignContainer != null && contentElementItem != null && this.MainManager.CurrentDesignContainer.IsElementInsertingState == false)
            {
                IDesignElement designElement = contentElementItem as IDesignElement;
                if (newPlaylistItems.Count == 0)
                {
                    designElement.UpdatePlaylistItems();
                    designElement.ThumnailImageSource = null;
                    return null;
                }
                this.MainManager.CreatePlaylistItem(this.MainManager.CurrentDesignContainer.TouchPageInfo.PageGuid, designElement, eventArgs.NewValue as List<PlaylistItem>);

                foreach (var contentItem in this.MainManager.CurrentDesignContainer.PageDesignCanvasBox.Children)
                {
                    if (contentElementItem.Equals(contentItem) == true)
                        continue;
                    designElement = contentItem as IDesignElement;
                    if (designElement != null)
                    {
                        if (designElement.IsSupportPlaylist == false)
                        {
                            ContentItemBoxData currentItemBoxData = this.MainManager.TimelineData.ContentItemBoxDataList.Where(o => o.Item.Equals(designElement) == true).SingleOrDefault();
                            if (currentItemBoxData != null)
                            {
                                double infinityContentItemLength = this.MainManager.GetTimelineContentBoxItemMaxTime(this.MainManager.TimelineData.TimelineTotalTime);
                                ContentItemData contentItemData = currentItemBoxData.ContentItemDatas[0];
                                contentItemData.ContentLength = this.timelinebody.GetContentItemLength(infinityContentItemLength);
                                contentItemData.TotalSeconds = infinityContentItemLength;
                            }
                        }
                        else
                        {
                            //this.MainManager.UpdateContentItemData(designElement, designElement.Playlist);
                        }
                    }
                }
            }

           

            return null;
        }

        private void changedPagePropertyCallback()
        {
            #region Page Name
            if (PagePropertyObject.ChangedPageNameCallback == null)
            {
                PagePropertyObject.ChangedPageNameCallback = (s, e) =>
                {
                    if (IsInsertPageState == false)//Page 추가중에는 이 콜백함수 호출 안되도록 한다.
                        return null;
                    if (this.MainManager.CurrentDesignContainer.TouchPageInfo == null)
                        return null;
                    string pagenameTemp = e.NewValue as string;
                    //if (string.IsNullOrEmpty(pagenameTemp) == false)
                    //    this.MainManager.CurrentDesignContainer.TouchPageInfo.PageNameTemp = e.NewValue as string;
                    //else
                    //    this.MainManager.CurrentDesignContainer.TouchPageInfo.PageNameTemp = "Blank Page";

                    if (MainManager.ChangedPagePropertyValue(this.MainManager.CurrentDesignContainer.TouchPageInfo.PageGuid.ToString(), "PageName", pagenameTemp) == true)
                    {
                        this.pageflowview.SelectedFlowPageItem.SetPageName(this.MainManager.CurrentDesignContainer.TouchPageInfo.PageNameTemp);
                    }                   
                    
                    return null;
                };
            }
            #endregion

            if (PagePropertyObject.PagePropertyChanged == null)
            {
                #region Screen의 Priperty 값 변경시 호출되는 콜백함수

                PagePropertyObject.PagePropertyChanged = (s, e) =>
                {
                    DependencyPropertyChangedEventArgs eventArgs = (DependencyPropertyChangedEventArgs)e;
                    switch (eventArgs.Property.Name)
                    {
                        case "PageName":
                            this.TouchDesignTabControl.ChangeTabItemName(this.MainManager.CurrentPageTabItem);
                            this.MainManager.RenamePageOfLocalDirectory((string)eventArgs.OldValue, (string)eventArgs.NewValue);
                            break;
                        case "IsApplyPageLifeTime":
                            {
                                updateTimlineContentItemBox(this.MainManager.CurrentDesignContainer.TouchPageInfo);
                            }
                            break;
                        case "PageLifeTime":
                            {
                                updateTimlineContentItemBox(this.MainManager.CurrentDesignContainer.TouchPageInfo);
                            }
                            break;
                    }
                    return null;
                };

                #endregion
            }
        }

        public void UpdatedItemProperty(FrameworkElement element, string propertyName, object propertyValue)
        {
            this.MainManager.CurrentDesignContainer.RaiseItemUpdatedPropertyEvent(element, "Playlist");
        }

        public void InsertNewTouchPage(Guid pageID, Guid parentID, double width, double height, double pagelifetime, string pagename, int pageDepthIndex)
        {
            IsInsertPageState = false;
            DesignContainer containerCanvas = this.CreateNewTouchPage(pageID, parentID, width, height, pagelifetime, pagename, pageDepthIndex);
            
            #region 추가된 Touch Page의 속성 Display
            this.bindingContentItemBoxData(containerCanvas);
            this.ChangedPageProperty(containerCanvas);
            this.changeTimelineContentItemBoxSize(containerCanvas);

            #endregion

            IsInsertPageState = true;
        }

        public DesignContainer CreateNewTouchPage(Guid pageID, Guid parentID, double width, double height, double pagelifetime, string pagename, int pageDepthIndex)
        {
            #region DesignContainer 생성

            DesignContainer containerCanvas = this.CreateNewDesignContainer(width, height, pagelifetime, pagename, pageDepthIndex);
            
            #endregion

            #region ContainerCanvas에 Page 생성

            TouchPageInfo touchpageinfo = containerCanvas.CreateNewPage(pageID, parentID, width, height, pagelifetime, pagename, pageDepthIndex);
            MainManager.AddTouchPageInfo(touchpageinfo);
            this.MainManager.CurrentDesignContainer.setTouchPageInfo(touchpageinfo);
            #endregion

            #region FlowView에 FlowPageItem 생성 및 추가
          
            FlowPageItem flowpageitem = this.CreateNewFlowPageItem(containerCanvas.PageDesignCanvasBox, pageID, parentID, pagename, MainManager.DefaultPageLifeTime, pageDepthIndex);
            touchpageinfo.PageDisplayOrder = flowpageitem.DisplayOrder;
            flowpageitem.PageInfoData = touchpageinfo;
            #endregion

            //#region 현재 TouchPageInfo 객체 설정

            //this.MainManager.CurrentDesignContainer.setTouchPageInfo(containerCanvas.TouchPageInfo);

            //#endregion
                      
            #region 추가된 FlowPageItem 선택

            flowpageitem.SelectionItem();

            #endregion

            #region FlowPageItem 선택시 호출되는 이벤트 함수

            flowpageitem.SelectFlowPageItem += (s, e) =>
            {
                PageTabItem pagetabitem = this.SearchPageTabItem(flowpageitem.ID.ToString());
                if (pagetabitem != null)
                {
                    #region PageTabItem 변경
                    pagetabitem.IsSelected = true;                    
                    pagetabitem.UpdateLayout();
                    #endregion
                    DesignContainer container = pagetabitem.Content as DesignContainer;       
                    this.UpdateFlowPageItemCaptureImage(flowpageitem, container.PageDesignCanvasBox);                    
                }
                else
                {
                    pagetabitem = MainManager.GetPageTabItem(flowpageitem.ID.ToString());
                    if (pagetabitem != null)
                    {
                        this.TouchDesignTabControl.AddTabItem(pagetabitem);
                        pagetabitem.IsSelected = true;
                    }
                }
                
                this.pageflowview.SelectedFlowPageItem = flowpageitem;
                this.MainManager.SetCurrentPageTabItem(pagetabitem);//현재 Page 설정
            };
            #endregion  
             
            flowpageitem.Loaded += (s, e) =>
            {    
                flowpageitem.RaiseSelectFlowPageItemEvent(null);
                //PageTabItem pagetabitem = this.SearchPageTabItem(flowpageitem.ID.ToString());
                //if (pagetabitem != null)
                //{                   
                //    DesignContainer container = pagetabitem.Content as DesignContainer;
                //    this.UpdateFlowPageItemCaptureImage(flowpageitem, container.PageDesignCanvasBox);
                //    this.pageflowview.SelectedFlowPageItem = flowpageitem;
                //    this.MainManager.SetCurrentPageTabItem(pagetabitem);//현재 Page 설정
                //}
            };

            return containerCanvas;
        }

        public void ArrangeFlowPage()
        {
            #region FlowView 재정렬

            if (this.flowviewTabItem.IsSelected == true)
            {
                this.pageflowview.ArrangePageThumbItems();
            }

            #endregion
        }

        public void UpdateFlowPageItemCaptureImage(FlowPageItem flowpageitem, PageCanvas pageCanvas)
        {
            this.MainManager.UpdateFlowPageItemCaptureImage(flowpageitem, pageCanvas);
        }

        private delegate void setImageDelegate(string controlName, BitmapFrame frame);

        private void setImage(string controlName, BitmapFrame frame)
        {
            var control = FindName(controlName);
            if (control != null)
                ((Image)control).Source = frame;
        }
        
        private PageTabItem SearchPageTabItem(string searchID)
        {
            foreach (var t_item in this.TouchDesignTabControl.Items)
            {
                PageTabItem pagetabitem = t_item as PageTabItem;
                DesignContainer container = pagetabitem.Content as DesignContainer;
                if (container.TouchPageInfo.PageGuid.Equals(searchID) == true)
                {
                    return pagetabitem;
                }
            }

            return null;
        }

        public TouchPageInfo getTouchPageInfoFromPageTabItem(PageTabItem pagetabitem)
        {
            foreach (var item in this.TouchDesignTabControl.Items)
            {
                PageTabItem tabitem = item as PageTabItem;
                if (tabitem != null)
                {
                    if (tabitem.Equals(pagetabitem) == true)
                    {
                        DesignContainer container = tabitem.Content as DesignContainer;
                        if (container != null)
                        {
                            return container.TouchPageInfo;
                        }
                    }
                }
            }

            return null;
        }

        private void ChangedPageProperty(DesignContainer containerCanvas)
        {
            if (containerCanvas.TouchPageInfo == null)
                return;
            Dictionary<string, object> properties = containerCanvas.GetPageCanvasPropertyValue();
            ICategoryData categoryData = new CategoryData();

            #region 이부분이 있어야 속성이 나옴 - 추후 분석후 수정 필요
            PropertyView.PropertyInstance = containerCanvas.TouchPageInfo.PageProperty;
            this.PropertyView.UpdateLayout();
            #endregion

            categoryData.SetCategories(properties);//컨트롤 타입에 따라 속성 카테고리 설정           
            categoryData.SetCategories(properties);//컨트롤 타입에 따라 속성 카테고리 설정                      
            PropertyView.SetPageCategories(categoryData);//Property View에 속성값과 카테고리 생성 

            #region Page Background 설정 및 Brush Editor 콜백함수

            BrushEditorCallButton backgroundBrushEditor = PropertyView.GetControlContentOfProperty("Background") as BrushEditorCallButton;
            if (backgroundBrushEditor != null)
            {
                backgroundBrushEditor.CallBrushEditorEventHandler = () =>
                {
                    Brush brush = InSysBasicControls.InSysControlManager.ShowBrushEditor(containerCanvas.PageDesignCanvasBox.Background, true);
                    
                    containerCanvas.PageDesignCanvasBox.Background = brush;
                    categoryData.Background = brush;

                    containerCanvas.TouchPageInfo.Background = brush;
                    return brush;
                };
                backgroundBrushEditor.SetBrush(containerCanvas.PageDesignCanvasBox.Background);
            }
            #endregion

            #region 

            TimePickerEditor timePickerEditor = PropertyView.GetControlContentOfProperty("PageLifeTime") as TimePickerEditor;
            if (timePickerEditor != null)
            {
                timePickerEditor.ChangedEditValueHandler = (editValue) =>
                {
                    containerCanvas.TouchPageInfo.PageLifeTimeTemp = editValue.TotalSeconds;
                };
                TimePickerEditor.LimitLifeTime = TimeSpan.Zero;
                timePickerEditor.SelectedTime = TimeSpan.FromSeconds(containerCanvas.TouchPageInfo.PageLifeTimeTemp);
                timePickerEditor.UpdateLayout();            
            }
            #endregion

            PropertyView.PropertyInstance = containerCanvas.TouchPageInfo.PageProperty;

            this.MainManager.CurrentDesignContainer.setTouchPageInfo(containerCanvas.TouchPageInfo);
            
            /*
            //this.bindingContentItemBoxData(containerCanvas);
            
            //this.MainManager.TimelineData.ContentItemBoxDataList.Clear();           
            foreach (var videoitem in containerCanvas.PageDesignCanvasBox.Children)
            {
                IDesignElement designElement = videoitem as IDesignElement;
                if (designElement != null)
                {
                    ContentItemBoxData contentItemBoxData = this.MainManager.TimelineData.GetTimelineContentBox(this.MainManager.CurrentDesignContainer.TouchPageInfo.PageGuid, videoitem as FrameworkElement);
                    if (this.MainManager.TimelineData.ContentItemBoxDataList.Contains(contentItemBoxData) == false)
                        this.MainManager.TimelineData.ContentItemBoxDataList.Add(contentItemBoxData);
                    this.MainManager.changeControlSizeOfContentItemBox(contentItemBoxData, designElement, contentItemBoxData.ContentBoxLength);
                }
            }

            foreach (var audioitem in containerCanvas.PageAudioBox.Children)
            {
                IDesignElement designElement = audioitem as IDesignElement;
                if (designElement != null)
                {
                    ContentItemBoxData contentItemBoxData = this.MainManager.TimelineData.GetTimelineContentBox(this.MainManager.CurrentDesignContainer.TouchPageInfo.PageGuid, audioitem as FrameworkElement);
                    if (this.MainManager.TimelineData.ContentItemBoxDataList.Contains(contentItemBoxData) == false)
                        this.MainManager.TimelineData.ContentItemBoxDataList.Add(contentItemBoxData);
                }
            }
           */
        }

        private void bindingContentItemBoxData(DesignContainer containerCanvas)
        {
            List<FrameworkElement> controlItemList = getContainerCanvasChildren(containerCanvas);
            TouchPageInfo touchpageinfo = containerCanvas.TouchPageInfo;
            foreach (var controlItem in controlItemList)
            {
                IDesignElement designElement = controlItem as IDesignElement;
                if (designElement != null)
                {
                    ContentItemBoxData contentItemBoxData = this.MainManager.TimelineData.GetTimelineContentBox(touchpageinfo.PageGuid, controlItem);
                    if (this.MainManager.TimelineData.ContentItemBoxDataList.Contains(contentItemBoxData) == false)
                        this.MainManager.TimelineData.ContentItemBoxDataList.Add(contentItemBoxData);                    
                }
            }    
        }

        private void changeTimelineContentItemBoxSize(DesignContainer designContainer)
        {
            List<FrameworkElement> controlItemList = this.getContainerCanvasChildren(designContainer);
            TouchPageInfo touchpageinfo = designContainer.TouchPageInfo;
            foreach (var controlItem in controlItemList)
            {
                IDesignElement designElement = controlItem as IDesignElement;
                if (designElement != null)
                {
                    ContentItemBoxData contentItemBoxData = this.MainManager.TimelineData.GetTimelineContentBox(touchpageinfo.PageGuid, controlItem);
                    if (contentItemBoxData == null)
                        continue;
                    this.MainManager.changeControlSizeOfContentItemBox(contentItemBoxData, designElement, contentItemBoxData.ContentBoxLength);
                }
            }   
        }

        private List<FrameworkElement> getContainerCanvasChildren(DesignContainer designContainer)
        {
            List<FrameworkElement> list = new List<FrameworkElement>();

            foreach (var item in designContainer.PageDesignCanvasBox.Children)
            {
                list.Add(item as FrameworkElement);
            }

            foreach (var item in designContainer.PageAudioBox.Children)
            {
                list.Add(item as FrameworkElement);
            }

            return list;
        }

        /// <summary>
        /// ContentElement 가 변경되었을 때 호출 되는 함수.
        /// </summary>
        /// <param name="containerCanvas"></param>
        /// <param name="frameworkElement"></param>
        private void ChangedContentElementProperty(DesignContainer containerCanvas, FrameworkElement frameworkElement)
        {
            try
            {
                Dictionary<string, object> properties = containerCanvas.GetPropertyValue(frameworkElement);

                IDesignElement designElement = frameworkElement as IDesignElement;
                IElementProperties elementProperty = frameworkElement as IElementProperties;
                ICategoryData categoryData = new CategoryData();

                #region 이부분이 있어야 속성이 나옴 - 추후 분석후 수정 필요
                PropertyView.PropertyInstance = elementProperty.ElementProperties;
                this.PropertyView.UpdateLayout();
                #endregion

                categoryData.SetCategories(properties);//컨트롤 타입에 따라 속성 카테고리 설정
                PropertyView.SetCategories(categoryData);//Property View에 속성값과 카테고리 생성                
                PropertyView.PropertyInstance = elementProperty.ElementProperties;

                if (designElement.Type.Name.Equals("InSysAudioBox") == true)//레이아웃이 없는 Audio를 체크한다.
                    return;
                if (designElement.Type.Name.Equals("InSysBasicButton") == true)
                    InSysBasicControls.InSysControlManager.changeButtonStylePropertyEditor(designElement);

                if (this.MainManager != null)
                {
                    if (this.MainManager.CurrentDesignContainer != null && this.MainManager.CurrentDesignContainer.SelectedCurrentContentItem != null)
                    {
                        AlignmentPropertyControl alignmentPropertyControl = this.PropertyView.GetControlContentOfProperty("Alignment") as AlignmentPropertyControl;
                        if (alignmentPropertyControl != null)
                        {
                            if (alignmentPropertyControl.ChangedValueHandler == null)
                            {
                                alignmentPropertyControl.ChangedValueHandler = (value) =>
                                {
                                    this.MainManager.Alignment_ContentElement(value);
                                };
                            }
                        }
                        else
                        {
                            throw new Exception("alignmentPropertyControl is null.");
                        }

                        FitToPagePropertyControl fitToPagePropertyControl = this.PropertyView.GetControlContentOfProperty("FitToPage") as FitToPagePropertyControl;
                        if (fitToPagePropertyControl != null)
                        {
                            if (fitToPagePropertyControl.ChangedValueHandler == null)
                            {
                                fitToPagePropertyControl.ChangedValueHandler = (value) =>
                                {
                                    this.MainManager.FitToPage_ContentElement(value);
                                    //this.MainManager.Align_ContentElement(value);
                                };
                            }
                        }
                        else
                        {
                            throw new Exception("fitToPagePropertyControl is null.");
                        }
                    }
                }
                else
                {
                    throw new Exception("this.MainManager is null.");
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message + ">> \r\n" + ex.StackTrace);
            }
        }

        private void TouchDesignTabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            PageTabControl pagetabcontrol = sender as PageTabControl;
            if (pagetabcontrol == null)
                return;

            PageTabItem selectedPagetabitem = pagetabcontrol.SelectedItem as PageTabItem;
            if (selectedPagetabitem !=null)
            {
                if (selectedPagetabitem.Equals(this.MainManager.CurrentPageTabItem) == true)
                {
                    return;
                }

                DesignContainer containerCanvas = selectedPagetabitem.Content as DesignContainer;

                if (containerCanvas != null && containerCanvas.TouchPageInfo != null)
                {                    
                    

                    SetDragSource();
                    SetDropTarget(containerCanvas);

                    FlowPageItem flowpageitem = this.pageflowview.FindFlowPageItem(containerCanvas.TouchPageInfo.PageGuid, containerCanvas.TouchPageInfo.PageLayerIndex);
                    if (flowpageitem != null)
                    {
                        flowpageitem.SelectionItem();
                        this.MainManager.CurrentPageTabItem = selectedPagetabitem;
                        this.pageflowview.SelectedFlowPageItem = flowpageitem;
                    }
                    this.MainManager.TimelineData.ContentItemBoxDataList.Clear();//타임라인에 등록된 이전 페이지의 컨트롤 삭제.
                    this.bindingContentItemBoxData(containerCanvas);
                    this.ChangedPageProperty(containerCanvas);
                    this.changeTimelineContentItemBoxSize(containerCanvas);
                }
            }
        }

        private void SetDragSource()
        {
            CanvasDragSourceAdvisor dragSourceAdvisor = new DesignWorkCanvas.CanvasDragSourceAdvisor();
            dragSourceAdvisor.IsRemoveTargetUI = false;
            toolbox.SetDragSourceAdvisor(UtilLib.DragDropManager.DragDropManager.DragSourceAdvisorProperty, dragSourceAdvisor);
        }

        private void SetDropTarget(DesignContainer designContainer)
        {
            designContainer.SetValue(UtilLib.DragDropManager.DragDropManager.DropTargetAdvisorProperty, null);

            CanvasDropTargetAdvisor dropTargetAdvisor = new CanvasDropTargetAdvisor();
            designContainer.SetValue(UtilLib.DragDropManager.DragDropManager.DropTargetAdvisorProperty, dropTargetAdvisor);
        }

        private void NewPageCreateButton_Click(object sender, RoutedEventArgs e)
        {
            Create_NewPage(this.pageflowview.SelectedFlowPageItem);
        }

        /// <summary>
        /// FlowPageView에 FlowPageItem 생성.
        /// </summary>
        /// <param name="captureCanvas"></param>
        /// <param name="pageID"></param>
        /// <param name="parentID"></param>
        /// <param name="pageName"></param>
        /// <param name="pageLifeTime"></param>
        /// <param name="pageDepth"></param>
        /// <returns></returns>
        public FlowPageItem CreateNewFlowPageItem(PageCanvas captureCanvas, Guid pageID, Guid parentID, string pageName, double pageLifeTime, int pageDepth)
        {
            FlowPageItem flowPageItem = new FlowPageItem();
            //this.UpdateFlowPageItemCaptureImage(flowPageItem, captureCanvas);//flowPageItem.Content = CommonUtils.ExportToPng(captureCanvas);
            flowPageItem.SetValues(pageName, pageLifeTime, pageDepth, pageID, parentID);
            this.pageflowview.InsertFlowPage(flowPageItem);
            
            this.pageflowview.SelectedFlowPageItem = flowPageItem;

            return flowPageItem;
        }

        private void Create_NewPage(FlowPageItem selectedFlowPageItem)
        {
            int layerIndex = 0;
            double pagelifetime = this.MainManager.DefaultPageLifeTime;
            string pagename = this.MainManager.DefaultPageName;
            double width = this.MainManager.PageWidth;
            double height = this.MainManager.PageHeight;
            Guid flowpageID = Guid.Empty;
            if (selectedFlowPageItem == null)
            {
                layerIndex = 0;
                flowpageID = Guid.Empty;
            }
            else
            {
                layerIndex = selectedFlowPageItem.PageDepthIndex + 1;
                flowpageID = selectedFlowPageItem.ID;
            }

            InsertNewTouchPage(Guid.NewGuid(), flowpageID, width, height, pagelifetime, pagename, layerIndex);

            this.flowviewTabItem.IsSelected = true;
            this.ArrangeFlowPage();
        }

        private void Test_InsertSamplePages()
        {
            //this.InsertTouchDesignPage(1200, 1000, 100, this.MainManager.DefaultPageName);
            //this.InsertTouchDesignPage(1200, 1000, 100, this.MainManager.DefaultPageName);

            //this.Test_FlowViewDataSetting();
            this.Test_TimelineDataSetting();
        }

        private void Test_TimelineDataSetting()
        {
            TimelineData timelineData = new TimelineData();

            ContentItemBoxData contentItemBoxData = new ContentItemBoxData();
            contentItemBoxData.ContentTypeName = "Image";

            ContentItemData contentItemData = new ContentItemData();
            contentItemData.ContentName = "TestName";
            Image image = new Image();
            image.Source = new BitmapImage(new Uri(@"\images\test_image1.jpg", UriKind.RelativeOrAbsolute));
            image.Width = 32;
            image.Height = 32;
            contentItemData.Thumbnail = image.Source;
            contentItemData.ContentLength = 60 * 5;
            contentItemData.OrderIndex = 0;
            contentItemBoxData.ContentItemDatas.Add(contentItemData);

            contentItemData = new ContentItemData();
            contentItemData.ContentName = "TestName2";
            Image image2 = new Image();
            image2.Source = new BitmapImage(new Uri(@"\images\test_image2.jpg", UriKind.RelativeOrAbsolute));
            image2.Width = 32;
            image2.Height = 32;
            contentItemData.Thumbnail = image2.Source;
            contentItemData.ContentLength = 250;
            contentItemData.OrderIndex = 1;
            contentItemBoxData.ContentItemDatas.Add(contentItemData);

            contentItemData = new ContentItemData();
            contentItemData.ContentName = "TestName3";
            Image image3 = new Image();
            image3.Source = new BitmapImage(new Uri(@"\images\test_image3.jpg", UriKind.RelativeOrAbsolute));
            image3.Width = 32;
            image3.Height = 32;
            contentItemData.Thumbnail = image3.Source;
            contentItemData.ContentLength = 150;
            contentItemData.OrderIndex = 2;
            contentItemBoxData.ContentItemDatas.Add(contentItemData);

            timelineData.ContentItemBoxDataList.Add(contentItemBoxData);

            //--------------------------------------------------------------------

            contentItemBoxData = new ContentItemBoxData();
            contentItemBoxData.ContentTypeName = "Button";

            contentItemData = new ContentItemData();
            contentItemData.ContentName = "Button 1";
            Image image4 = new Image();
            image4.Source = new BitmapImage(new Uri(@"\images\test_image3.jpg", UriKind.RelativeOrAbsolute));
            image4.Width = 32;
            image4.Height = 32;
            contentItemData.Thumbnail = image4.Source;
            contentItemData.ContentLength = 350;
            contentItemData.OrderIndex = 0;
            contentItemBoxData.ContentItemDatas.Add(contentItemData);

            timelineData.ContentItemBoxDataList.Add(contentItemBoxData);

            //--------------------------------------------------------------------

            contentItemBoxData = new ContentItemBoxData();
            contentItemBoxData.ContentTypeName = "Text";

            contentItemData = new ContentItemData();
            contentItemData.ContentName = "Text 1";
            Image image5 = new Image();
            image5.Source = new BitmapImage(new Uri(@"\images\test_image1.jpg", UriKind.RelativeOrAbsolute));
            image5.Width = 32;
            image5.Height = 32;
            contentItemData.Thumbnail = image5.Source;
            contentItemData.ContentLength = 250;
            contentItemData.OrderIndex = 0;
            contentItemBoxData.ContentItemDatas.Add(contentItemData);

            timelineData.ContentItemBoxDataList.Add(contentItemBoxData);

            //--------------------------------------------------------------------

            contentItemBoxData = new ContentItemBoxData();
            contentItemBoxData.ContentTypeName = "Video";
            contentItemData = new ContentItemData();
            contentItemData.ContentName = "Video 1";
            Image image6 = new Image();
            image6.Source = new BitmapImage(new Uri(@"\images\test_image2.jpg", UriKind.RelativeOrAbsolute));
            image6.Width = 32;
            image6.Height = 32;
            contentItemData.Thumbnail = image6.Source;
            contentItemData.ContentLength = 550;
            contentItemData.OrderIndex = 0;
            contentItemBoxData.ContentItemDatas.Add(contentItemData);

            timelineData.ContentItemBoxDataList.Add(contentItemBoxData);

            timelinebody.DataContext = timelineData;
        }

        private void Test_FlowViewDataSetting()
        {
            this.pageflowview.ClearFlowItems();

            TestCanvas canvas = new TestCanvas();

            FlowPageItem flowPageItem = new FlowPageItem();
            canvas.ChangeImage("기타1.JPG");
            flowPageItem.Content = CommonUtils.ExportToPng(canvas);
            flowPageItem.SetValues("flowpagedata", 100, 0, Guid.NewGuid(), Guid.Empty);
            this.pageflowview.InsertFlowPage(flowPageItem);

            FlowPageItem flowpagedata11 = new FlowPageItem();
            canvas.ChangeImage("기타2.JPG");
            flowpagedata11.Content = CommonUtils.ExportToPng(canvas);
            flowpagedata11.SetValues("flowpagedata11", 100, 1, Guid.NewGuid(), flowPageItem.ID);
            this.pageflowview.InsertFlowPage(flowpagedata11);

            FlowPageItem flowpagedata12 = new FlowPageItem();
            canvas.ChangeImage("기타3.JPG");
            flowpagedata12.Content = CommonUtils.ExportToPng(canvas);
            flowpagedata12.SetValues("flowpagedata12", 100, 1, Guid.NewGuid(), flowPageItem.ID);
            this.pageflowview.InsertFlowPage(flowpagedata12);

            FlowPageItem flowpagedata111 = new FlowPageItem();
            canvas.ChangeImage("기타4.JPG");
            flowpagedata111.Content = CommonUtils.ExportToPng(canvas);
            flowpagedata111.SetValues("flowpagedata111", 100, 2, Guid.NewGuid(), flowpagedata11.ID);
            this.pageflowview.InsertFlowPage(flowpagedata111);

            FlowPageItem flowpagedata112 = new FlowPageItem();
            canvas.ChangeImage("기타5.JPG");
            flowpagedata112.Content = CommonUtils.ExportToPng(canvas);
            flowpagedata112.SetValues("flowpagedata112", 100, 2, Guid.NewGuid(), flowpagedata11.ID);
            this.pageflowview.InsertFlowPage(flowpagedata112);

            FlowPageItem flowpagedata113 = new FlowPageItem();
            canvas.ChangeImage("기타6.JPG");
            flowpagedata113.Content = CommonUtils.ExportToPng(canvas);
            flowpagedata113.SetValues("flowpagedata113", 100, 2, Guid.NewGuid(), flowpagedata11.ID);
            this.pageflowview.InsertFlowPage(flowpagedata113);

            FlowPageItem flowpagedata121 = new FlowPageItem();
            canvas.ChangeImage("기타7.JPG");
            flowpagedata121.Content = CommonUtils.ExportToPng(canvas);
            flowpagedata121.SetValues("flowpagedata121", 100, 2, Guid.NewGuid(), flowpagedata12.ID);
            this.pageflowview.InsertFlowPage(flowpagedata121);

            FlowPageItem flowpagedata122 = new FlowPageItem();
            canvas.ChangeImage("기타13.JPG");
            flowpagedata122.Content = CommonUtils.ExportToPng(canvas);
            flowpagedata122.SetValues("flowpagedata122", 100, 2, Guid.NewGuid(), flowpagedata12.ID);
            this.pageflowview.InsertFlowPage(flowpagedata122);

            FlowPageItem flowpagedata1121 = new FlowPageItem();
            canvas.ChangeImage("기타8.JPG");
            flowpagedata1121.Content = CommonUtils.ExportToPng(canvas);
            flowpagedata1121.SetValues("flowpagedata1121", 100, 3, Guid.NewGuid(), flowpagedata112.ID);
            this.pageflowview.InsertFlowPage(flowpagedata1121);

            FlowPageItem flowpagedata1131 = new FlowPageItem();
            canvas.ChangeImage("기타9.JPG");
            flowpagedata1131.Content = CommonUtils.ExportToPng(canvas);
            flowpagedata1131.SetValues("flowpagedata1131", 100, 3, Guid.NewGuid(), flowpagedata113.ID);
            this.pageflowview.InsertFlowPage(flowpagedata1131);

            FlowPageItem flowpagedata1132 = new FlowPageItem();
            canvas.ChangeImage("기타10.JPG");
            flowpagedata1132.Content = CommonUtils.ExportToPng(canvas);
            flowpagedata1132.SetValues("flowpagedata1132", 100, 3, Guid.NewGuid(), flowpagedata113.ID);
            this.pageflowview.InsertFlowPage(flowpagedata1132);

            FlowPageItem flowpagedata1211 = new FlowPageItem();
            canvas.ChangeImage("기타11.JPG");
            flowpagedata1211.Content = CommonUtils.ExportToPng(canvas);
            flowpagedata1211.SetValues("flowpagedata1211", 100, 3, Guid.NewGuid(), flowpagedata121.ID);
            this.pageflowview.InsertFlowPage(flowpagedata1211);

            FlowPageItem flowpagedata1212 = new FlowPageItem();
            canvas.ChangeImage("기타12.JPG");
            flowpagedata1212.Content = CommonUtils.ExportToPng(canvas);
            flowpagedata1212.SetValues("flowpagedata1212", 100, 3, Guid.NewGuid(), flowpagedata121.ID);
            this.pageflowview.InsertFlowPage(flowpagedata1212);

            FlowPageItem flowpagedata12121 = new FlowPageItem();
            canvas.ChangeImage("기타12.JPG");
            flowpagedata12121.Content = CommonUtils.ExportToPng(canvas);
            flowpagedata12121.SetValues("flowpagedata12121", 100, 4, Guid.NewGuid(), flowpagedata1212.ID);
            this.pageflowview.InsertFlowPage(flowpagedata12121);

            FlowPageItem flowpagedata121211 = new FlowPageItem();
            canvas.ChangeImage("기타12.JPG");
            flowpagedata121211.Content = CommonUtils.ExportToPng(canvas);
            flowpagedata121211.SetValues("flowpagedata121211", 100, 5, Guid.NewGuid(), flowpagedata12121.ID);
            this.pageflowview.InsertFlowPage(flowpagedata121211);

            FlowPageItem flowpagedata1212111 = new FlowPageItem();
            canvas.ChangeImage("기타12.JPG");
            flowpagedata1212111.Content = CommonUtils.ExportToPng(canvas);
            flowpagedata1212111.SetValues("flowpagedata1212111", 100, 6, Guid.NewGuid(), flowpagedata121211.ID);
            this.pageflowview.InsertFlowPage(flowpagedata1212111);

            FlowPageItem flowpagedata12121111 = new FlowPageItem();
            canvas.ChangeImage("기타12.JPG");
            flowpagedata12121111.Content = CommonUtils.ExportToPng(canvas);
            flowpagedata12121111.SetValues("flowpagedata12121111", 100, 7, Guid.NewGuid(), flowpagedata1212111.ID);
            this.pageflowview.InsertFlowPage(flowpagedata12121111);

            FlowPageItem flowpagedata121211111 = new FlowPageItem();
            canvas.ChangeImage("기타12.JPG");
            flowpagedata121211111.Content = CommonUtils.ExportToPng(canvas);
            flowpagedata121211111.SetValues("flowpagedata121211111", 100, 8, Guid.NewGuid(), flowpagedata12121111.ID);
            this.pageflowview.InsertFlowPage(flowpagedata121211111);

            FlowPageItem flowpagedata1212111111 = new FlowPageItem();
            canvas.ChangeImage("기타12.JPG");
            flowpagedata1212111111.Content = CommonUtils.ExportToPng(canvas);
            flowpagedata1212111111.SetValues("flowpagedata1212111111", 100, 9, Guid.NewGuid(), flowpagedata121211111.ID);
            this.pageflowview.InsertFlowPage(flowpagedata1212111111);

            this.pageflowview.ArrangePageThumbItems();
        }       

        private void Image_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left && e.ClickCount == 2)
            {
                this.Close();
            }
        }

        private void PagePreviewButton_Click(object sender, RoutedEventArgs e)
        {
            this.MainManager.ReleaseAllAdornerOnDesignerCanvas();
            this.MainManager.PreviewPage();
        }

        private void ProjectPreviewButton_Click(object sender, RoutedEventArgs e)
        {
            this.MainManager.ReleaseAllAdornerOnDesignerCanvas();
            this.MainManager.PreviewProject();
        }

        protected override void OnPreviewMouseLeftButtonUp(MouseButtonEventArgs e)
        {
            base.OnPreviewMouseLeftButtonUp(e);
        }

        private void iVisionTouchDesignerTitlebar_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();

            if (e.ChangedButton == MouseButton.Left && e.ClickCount == 2)
            {
                SetWindowState();
            }
        }

        private void iVisionTouchDesignerMinimizeBtn_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.SetWindowState(System.Windows.WindowState.Minimized);
        }

        private void iVisionTouchDesignerMaximizeBtn_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            SetWindowState();            
        }

        private void SetWindowResize(Window window)
        {
            WindowResizer wr = new WindowResizer(window);
            wr.addResizerRight(rightSizeGrip);
            wr.addResizerLeft(leftSizeGrip);
            wr.addResizerUp(topSizeGrip);
            wr.addResizerDown(bottomSizeGrip);
            wr.addResizerLeftUp(topLeftSizeGrip);
            wr.addResizerRightUp(topRightSizeGrip);
            wr.addResizerLeftDown(bottomLeftSizeGrip);
            wr.addResizerRightDown(bottomRightSizeGrip);
        }

        private void SetWindowState()
        {
            if (this.WindowState == System.Windows.WindowState.Maximized)
            {
                this.SetWindowState(System.Windows.WindowState.Normal);
                this.iVisionTouchDesignerMaximizeBtn.ToolTip = Cultures.Resources.toolTipMaximize;
            }
            else
            {
                //Width = SystemParameters.WorkArea.Width;
                //Height = SystemParameters.WorkArea.Height;
                //Left = 0;
                //Top = 0;
                this.SetWindowState(System.Windows.WindowState.Maximized);
                this.iVisionTouchDesignerMaximizeBtn.ToolTip = Cultures.Resources.toolTipRestoreDown;
            }
        }

        private void SetWindowState(System.Windows.WindowState windowState)
        {
            this.WindowState = windowState;
        }
        
        private void iVisionTouchDesignerCloseBtn_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            this.Close();
        }

        
        private string Serialize(object objectToSerialize)
        {
            string serialString = null;
            using (System.IO.MemoryStream ms1 = new System.IO.MemoryStream())
            {
                BinaryFormatter b = new BinaryFormatter();
                b.Serialize(ms1, objectToSerialize);
                byte[] arrayByte = ms1.ToArray();
                serialString = Convert.ToBase64String(arrayByte);
            }
            return serialString;
        }

        private object DeSerialize(string serializationString)
        {
            object deserialObject = null;
            byte[] arrayByte = Convert.FromBase64String(serializationString);
            using (System.IO.MemoryStream ms1 = new System.IO.MemoryStream(arrayByte))
            {
                BinaryFormatter b = new BinaryFormatter();
                deserialObject = b.Deserialize(ms1);
            }
            return deserialObject;
        }

        public XElement LoadSerializedDataFromClipBoard()
        {
            if (Clipboard.ContainsData(DataFormats.Xaml))
            {
                String clipboardData = Clipboard.GetData(DataFormats.Xaml) as String;

                if (String.IsNullOrEmpty(clipboardData))
                    return null;
                try
                {
                    return XElement.Load(new StringReader(clipboardData));
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.StackTrace, e.Message, MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }

            return null;
        }

        private void copyTouchPageInfo(TouchPageInfo sourceCopyPageInfo, TouchPageInfo targetTouchPageInfo)
        {
            targetTouchPageInfo.PageLifeTimeTemp = sourceCopyPageInfo.PageLifeTimeTemp;
            targetTouchPageInfo.ViewHeightTemp = sourceCopyPageInfo.ViewHeightTemp;
            targetTouchPageInfo.ViewWidthTemp = sourceCopyPageInfo.ViewWidthTemp;

            foreach (TouchContentInfo sourceContentInfo in sourceCopyPageInfo.Contents)
            {
                FrameworkElement inSysContentControl = InSysBasicControls.InSysControlManager.GetNewInSysControl(sourceContentInfo.ContentType, sourceContentInfo.DesignElementProperties, ViewState.Design) as FrameworkElement;
                TouchContentInfo copyContentInfo = TouchContentInfo.Copy_ContentInfo(inSysContentControl, sourceContentInfo, sourceCopyPageInfo.PageLifeTimeTemp);

                targetTouchPageInfo.Contents.Add(copyContentInfo);
            }


            return;
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);

            if (e.Key == Key.F5)
            {
                this.MainManager.PreviewProject();
            }
        }

        private void flowviewTabItem_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.N && Keyboard.Modifiers == ModifierKeys.Control)
            {
                if (this.pageflowview.SelectedFlowPageItem != null && this.pageflowview.PageFlowViewCommandHandler != null)
                {
                    this.pageflowview.PageFlowViewCommandHandler(ApplicationCommands.New);
                }
            }
            else if (e.Key == Key.C && Keyboard.Modifiers == ModifierKeys.Control)
            {
                if (this.pageflowview.SelectedFlowPageItem != null && this.pageflowview.PageFlowViewCommandHandler != null)
                {
                    this.pageflowview.PageFlowViewCommandHandler(ApplicationCommands.Copy);
                }
            }
            else if (e.Key == Key.V && Keyboard.Modifiers == ModifierKeys.Control)
            {
                if (this.pageflowview.SelectedFlowPageItem != null && this.pageflowview.PageFlowViewCommandHandler != null)
                {
                    this.pageflowview.PageFlowViewCommandHandler(ApplicationCommands.Paste);
                }
            }
            else if (e.Key == Key.Delete )
            {
                if (this.pageflowview.SelectedFlowPageItem != null && this.pageflowview.PageFlowViewCommandHandler != null)
                {
                    this.pageflowview.PageFlowViewCommandHandler(ApplicationCommands.Delete);
                }
            }
           
        }

        private void SetCurrentContentElementItem(UIElement item)
        {
            this.MainManager.CurrentDesignContainer.SelectedCurrentContentItem = item;
        }

        private UIElement GetCurrentContentElementItem()
        {
            if (this.MainManager.CurrentDesignContainer == null)
                return null;
            return this.MainManager.CurrentDesignContainer.SelectedCurrentContentItem;
        }

        private void NewProject_Click(object sender, RoutedEventArgs e)
        {
            this.MainManager.NewProject();
        }

        private void NewPage_Click(object sender, RoutedEventArgs e)
        {
            this.Create_NewPage(this.pageflowview.SelectedFlowPageItem);
        }

        private void ClosePage_Click(object sender, RoutedEventArgs e)
        {
            this.TouchDesignTabControl.RemoveTabItem(this.MainManager.CurrentPageTabItem);

            if (this.TouchDesignTabControl.Items.Count == 0)
            {
                this.PropertyView.PropertyInstance = null;
            }            
        }

        private void CloseProject_Click(object sender, RoutedEventArgs e)
        {
            this.TouchDesignTabControl.RemoveAll();
            this.MainManager.InitProject();
            this.PropertyView.PropertyInstance = null;
            this.MainManager.IsProjectOpenState = false;
        }

        private void SaveProject_Click(object sender, RoutedEventArgs e)
        {
            if (this.MainManager.IsProjectOpenState == true)
                this.MainManager.SaveProject();
        }

        private void OpenProject_Click(object sender, RoutedEventArgs e)
        {
            this.MainManager.OpenProject();
        }

        private void SaveAsProject_Click(object sender, RoutedEventArgs e)
        {
            if (this.MainManager.IsProjectOpenState == true)
                this.MainManager.SaveAsProject();
        }

        private void ExitWindow_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        public void ChangedTimelineSetting(double timeLength)
        {
            //this.timelinebody.RefershTimelineHeader(timeLength);
            this.timelinebody.UpdatedTimelineContentItemBox(timeLength);
            
        }

        public void Update_TimelineModelData()
        {
            this.timelinebody.Update_TimelineModelData(this.MainManager.TimelineData);
        }

        private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {

        }

        private void PageFlowTabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {    
            TabItem sel_pagetabitem = (sender as TabControl).SelectedItem as TabItem;
            if (sel_pagetabitem != null)
            {
                if (sel_pagetabitem.Equals(timelineTabItem) == true)
                {
                    if (this.pageflowZoomSlider != null)
                    {
                        ShowPageFlowZoomSliderControl(false);
                    }
                }
                else if (sel_pagetabitem.Equals(this.flowviewTabItem) == true)
                {
                    if (this.pageflowZoomSlider != null)
                    {
                        ShowPageFlowZoomSliderControl(true);
                    }
                }
            }
        }
        
        private void PageFlowZoomSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (this.pageflowZoomSlider != null)
            {
                pageflowZoomSliderValue = pageflowZoomSlider.Value / 100;
                this.pageflowview.RenderTransform = new ScaleTransform(pageflowZoomSliderValue, pageflowZoomSliderValue);
            }
        }

        private void PageFlowZoomSlider_Loaded(object sender, RoutedEventArgs e)
        {
            TabItem sel_pagetabitem = this.PageFlowTabControl.SelectedItem as TabItem;
            
            this.pageflowZoomSlider = sender as Slider;
        }

        private void ShowPageFlowZoomSliderControl(bool isShow)
        {
            if (this.pageflowZoomSlider != null)
            {
                if(isShow == true)
                    this.pageflowZoomSlider.Visibility = System.Windows.Visibility.Visible;
                else
                    this.pageflowZoomSlider.Visibility = System.Windows.Visibility.Collapsed;
            }
        }
    }
}
