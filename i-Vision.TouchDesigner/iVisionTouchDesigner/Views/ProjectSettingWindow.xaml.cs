﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using InSysTouchflowData.Events;

namespace iVisionTouchDesigner.Views
{
    /// <summary>
    /// Interaction logic for ProjectSettingWindow.xaml
    /// </summary>
    public partial class ProjectSettingWindow : Window, IDisposable
    {
        private double dresValue = (double)(3.0 / 4.0);
        private bool isInt = true;
        public event EventHandler<TouchProjectInfoEventArgs> SetOKTouchProjectInfoEvent = delegate { };
        public event EventHandler<EventArgs> CancelScreenSizeEvent = delegate { };
        private string timeType = "분";

        public ProjectSettingWindow()
        {
            InitializeComponent();
        }

        public void Dispose()
        {
        }

        public void ShowWindow(Window owner)
        {
            this.Owner = owner;
        }

        private void WindowOK_Click(object sender, RoutedEventArgs e)
        {
            double timeValue = 0.0;
            double projectTimeValue = 0.0;
            if (double.TryParse(this.PageTime.Text, out timeValue) == false)
            {
                MessageBox.Show("Page Time을 정확하게 입력해주세요.");
                return;
            }

            switch (timeType)
            {
                case "초":
                    projectTimeValue = timeValue;
                    break;
                case "분":
                    projectTimeValue = timeValue * 60;
                    break;
                case "시":
                    projectTimeValue = timeValue * 60 * 60;
                    break;
                default:
                    projectTimeValue = timeValue * 60;
                    break;
            }

            TouchProjectInfoEventArgs touchProjectInfo = new TouchProjectInfoEventArgs(this.ScreenWidth.Text, this.ScreenHeight.Text, projectTimeValue);            

            SetOKTouchProjectInfoEvent(sender, touchProjectInfo);
            this.DialogResult = true;
        }

        private void Border_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
        }

        private void WindowCancel_Click(object sender, RoutedEventArgs e)
        {
            CloseWindow();
        }

        private void CloseWindow()
        {
            CancelScreenSizeEvent(null, null);
            this.Close();
        }

        private void UserControl_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                WindowCancel_Click(null, null);
            }
            else if (e.Key == Key.Enter)
            {
                WindowOK_Click(null, null);
            }
        }

        private void resList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SetWidth();
        }

        private void SetWidth()
        {
            if (resList == null)
                return;

            switch (resList.SelectedIndex)
            {
                case 0:
                    dresValue = (double)(4.0 / 3.0);
                    break;
                case 1:
                    dresValue = (double)(3.0 / 4.0);
                    break;
                case 2:
                    dresValue = (double)(16.0 / 9.0);
                    break;
                case 3:
                    dresValue = (double)(9.0 / 16.0);
                    break;
                default:
                    dresValue = (double)(3.0 / 4.0);
                    break;
            }

            if (this.ScreenHeight != null && this.IsConnect.IsChecked == true)
            {
                try
                {
                    this.ScreenHeight.Text = ((int)(double.Parse(ScreenWidth.Text) * dresValue)).ToString();
                }
                catch { }
            }
        }

        private void widthBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (this.ScreenHeight != null && this.IsConnect.IsChecked == true && isInt)
            {
                try
                {
                    this.ScreenHeight.Text = ((int)(double.Parse(ScreenWidth.Text) * dresValue)).ToString();
                }
                catch
                { }
            }
        }

        void widthBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            try
            {
                int digit = 0;
                if (!Int32.TryParse(e.Text, out digit))
                {
                    e.Handled = true;
                    isInt = false;
                }
                else
                    isInt = true;
            }
            catch { }
        }

        private void IsConnect_Checked(object sender, RoutedEventArgs e)
        {
            if (this.IsConnect != null)
            {
                if (this.IsConnect.IsChecked == true)
                {
                    if (ScreenHeight != null)
                        ScreenHeight.IsEnabled = false;
                    SetWidth();
                }
                else if (ScreenHeight != null)
                    ScreenHeight.IsEnabled = true;
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            CloseWindow();
        }

        private void timeCombobox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox combobox = sender as ComboBox;
            if (combobox != null)
            {
                ComboBoxItem comboboxitem = (ComboBoxItem)combobox.SelectedItem;
                if (comboboxitem != null)
                {
                    timeType = comboboxitem.Content as string;
                }
            }
        }
    }
}
