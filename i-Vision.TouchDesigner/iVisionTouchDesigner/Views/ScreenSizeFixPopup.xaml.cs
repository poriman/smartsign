﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Controls.Primitives;
using InSysTouchflowData.Events;
using iVisionTouchDesigner.Controls;

namespace iVisionTouchDesigner.Views
{
    /// <summary>
    /// ScreenSizeFixPopup.xaml에 대한 상호 작용 논리
    /// </summary>    

    public partial class ScreenSizeFixPopup : UserControl, IDisposable
    {
        PopupControl popup;
        private double dresValue = (double)(3.0 / 4.0);
        private bool isInt = true;
        public event EventHandler<TouchProjectInfoEventArgs> SetOKTouchProjectInfoEvent = delegate { };
        public event EventHandler<EventArgs> CancelScreenSizeEvent = delegate { };
        
        public ScreenSizeFixPopup()
        {
            InitializeComponent();
        }

        public void Dispose()
        {
        }

        public void ShowPopup(object owner)
        {
            if (popup == null)
            {
                popup = new PopupControl();
                StackPanel sp = new StackPanel { Margin = new Thickness(5) };
                sp.Background = Brushes.Transparent;
                sp.Children.Add(this);

                ScrollViewer scrollViewer = new ScrollViewer();
                scrollViewer.HorizontalScrollBarVisibility = ScrollBarVisibility.Auto;
                scrollViewer.VerticalScrollBarVisibility = ScrollBarVisibility.Auto;
                scrollViewer.Content = sp;

                popup.Child = new Border { Background = Brushes.Transparent, BorderBrush = Brushes.Transparent, BorderThickness = new Thickness(2), Child = scrollViewer };

                this.popup.PlacementTarget = owner as UIElement;
                this.popup.Placement = PlacementMode.Center;
                this.popup.IsPopupOpen = true;
            }
            else
            {
                if (popup.IsPopupOpen == false)
                {
                    popup.IsPopupOpen = true;
                }
            }
        }

        private void PopupOK_Click(object sender, RoutedEventArgs e)
        {            
            double projectTime = 0.0;
            if (double.TryParse(this.PageTime.Text, out projectTime) == false)
            {
                MessageBox.Show("Page Time을 정확하게 입력해주세요.");
                return ;
            }
            TouchProjectInfoEventArgs touchProjectInfo = new TouchProjectInfoEventArgs(this.ScreenWidth.Text, this.ScreenHeight.Text, projectTime);

            if (popup != null)
            {
                this.popup.IsPopupOpen = false;
            }

            SetOKTouchProjectInfoEvent(sender, touchProjectInfo);            
        }

        private void Border_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
        }

        private void PopupCancel_Click(object sender, RoutedEventArgs e)
        {
            if (popup != null)
            {
                this.popup.IsPopupOpen = false;
                CancelScreenSizeEvent(null, null);
            }
        }

        private void UserControl_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                PopupCancel_Click(null, null);
            }
            else if (e.Key == Key.Enter)
            {
                PopupOK_Click(null, null);
            }
        }

        private void resList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SetWidth();
        }

        private void SetWidth()
        {
            if (resList == null)
                return;

            switch (resList.SelectedIndex)
            {
                case 0:
                    dresValue = (double)(4.0 / 3.0);
                    break;
                case 1:
                    dresValue = (double)(3.0 / 4.0);
                    break;
                case 2:
                    dresValue = (double)(16.0 / 9.0);
                    break;
                case 3:
                    dresValue = (double)(9.0 / 16.0);
                    break;
                default:
                    dresValue = (double)(3.0 / 4.0);
                    break;
            }

            if (this.ScreenHeight != null && this.IsConnect.IsChecked == true)
            {
                try
                {
                    this.ScreenHeight.Text = ((int)(double.Parse(ScreenWidth.Text) * dresValue)).ToString();
                }
                catch { }
            }
        }

        private void widthBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (this.ScreenHeight != null && this.IsConnect.IsChecked == true && isInt)
            {
                try
                {
                    this.ScreenHeight.Text = ((int)(double.Parse(ScreenWidth.Text) * dresValue)).ToString();
                }
                catch
                { }
            }
        }

        void widthBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            try
            {
                int digit = 0;
                if (!Int32.TryParse(e.Text, out digit))
                {
                    e.Handled = true;
                    isInt = false;
                }
                else
                    isInt = true;
            }
            catch { }
        }

        private void IsConnect_Checked(object sender, RoutedEventArgs e)
        {
            if (this.IsConnect != null)
            {
                if (this.IsConnect.IsChecked == true)
                {
                    if (ScreenHeight != null)
                        ScreenHeight.IsEnabled = false;
                    SetWidth();
                }
                else if (ScreenHeight != null)
                    ScreenHeight.IsEnabled = true;
            }
        }
    }
}
