﻿using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using InSysTouchflowData.Models.ProjectModels;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using iVisionTouchDesigner.Views;

namespace iVisionTouchDesigner.ViewModels
{
    public class OpenWindowViewModel : INotifyPropertyChanged
    {
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        public virtual void OnPropertyChanged(params string[] propertyNames)
        {
            if (PropertyChanged == null)
                return;

            foreach (string propertyName in propertyNames)
            {
                try
                {
                    PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
                }
                catch { }
            }
        }

        #endregion

        //private ProjectManager _projectManager = ProjectManager.Instance;
        private ProjectOption _projectOption;
        private OpenWindow _openWindow;
        private MainManager MainManger = MainManager.Instance;

        #region IsDesignTime

        public bool IsDesignTime
        {
            get
            {
                return (Application.Current == null) || (Application.Current.GetType() == typeof(Application));
            }
        }

        #endregion

        public ObservableCollection<ProjectFileInfo> ProjectFileInfoList
        {
            get;
            set;
        }

        public OpenWindowViewModel()
        {
            if (this.IsDesignTime)
            {

            }
            else
            {
                ProjectFileInfoList = new ObservableCollection<ProjectFileInfo>();
                _projectOption = ProjectOption.Instance;
                this.MainManger.CancelActionScreenSizeByUserHandler = new InSysTouchflowData.ActionByUserDelegate(onCancelByUser);
                this.MainManger.OkActionTouchProjectInfoByUserHandler = new InSysTouchflowData.ActionByUserDelegate(onOkByUser);

                List<ProjectFileInfo> infoList = _projectOption.GetProjectFileInfoList();
                infoList = infoList.OrderBy(o => o.Index).ToList();
                foreach (var item in infoList)
                {
                    this.ProjectFileInfoList.Add(item);
                }
            }            
        }

        public void OpenWindow_Loaded(object sender, RoutedEventArgs e)
        {
            _openWindow = sender as OpenWindow;
            if (_openWindow == null)
            {                
                return;
            }

            MainWindow mainwindow = this._openWindow.Owner as MainWindow;
            if (mainwindow != null)
            {
                mainwindow.IsHitTestVisible = false;

                _openWindow.Closed += (s, e2) =>
                {
                    mainwindow.IsHitTestVisible = true;
                };
            }
            
        }

        public void ProjectItemListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this._openWindow.Hide();
            ListBox listbox = sender as ListBox;
            if (listbox != null)
            {
                ProjectFileInfo selProInfo = listbox.SelectedItem as ProjectFileInfo;
                if (selProInfo != null)
                {
                    MainWindow mainwindow = this._openWindow.Owner as MainWindow;

                    if (this.MainManger.OpenProject(selProInfo.ProjectPath) == true)
                    {
                        this.CloseOpenWindow();
                        return;
                    }
                    else
                    {
                        MessageBox.Show("선택한 파일에 오류가 존재합니다.");
                    }
                    /*
                    this._projectManager.InitProjectView(mainwindow.insysCanvas, mainwindow.insysFlowView);
                    if (this._projectManager.OpenProject(selProInfo.ProjectPath) == true)
                    {
                        this.CloseOpenWindow();
                        return;
                    }
                    else
                    {
                        MessageBox.Show("선택한 파일에 오류가 존재합니다.");
                    }
                     */
                }
                else
                {
                    MessageBox.Show("선택한 파일이 존재하지 않습니다.");
                    
                }

                this._openWindow.Show();
            }
        }

        public void NewProjectGrid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this._openWindow.Hide();
            MainWindow mainwindow = this._openWindow.Owner as MainWindow;
            MainManger.NewProject();
            /*
            this._projectManager.InitProjectView(mainwindow.insysCanvas, mainwindow.insysFlowView);
            this._projectManager.NewProject();
             */ 
            
        }

        public void OpenProjectGrid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this._openWindow.Hide();
            MainWindow mainwindow = this._openWindow.Owner as MainWindow;
          
            MainManger.OpenProject();
            /*
            this._projectManager.InitProjectView(mainwindow.insysCanvas, mainwindow.insysFlowView);
            this._projectManager.OpenProject();
             */
        }

        public void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.CloseOpenWindow();
        }

        private void onCancelByUser()
        {
            this._openWindow.Show();
        }

        private void onOkByUser()
        {
            this.CloseOpenWindow();
        }

        private void CloseOpenWindow()
        {
            MainManager.Instance.CancelActionScreenSizeByUserHandler = null;
            MainManager.Instance.OkActionTouchProjectInfoByUserHandler = null;
            this._openWindow.Close();
            MainManger.ShowTouchDesignerWindow();
        }

        public void OenpWindow_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                this.CloseOpenWindow();
            }
        }     
    }
}
