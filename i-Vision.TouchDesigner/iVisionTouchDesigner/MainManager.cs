﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Win32;
using InSysTouchflowData;
using System.IO;
using System.Windows;
using InSysTouchflowData.Models.ProjectModels;
using iVisionTouchDesigner.Views;
using InSysTouchflowData.Events;
using DesignWorkCanvas.Views;
using iVisionTouchDesigner.Controls;
using iVisionTouchTimelineBody.Model;
using InSysBasicControls.Interfaces;
using System.Windows.Input;
using iVisionPageFlowView.Core;
using iVisionPageFlowView.Util;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using InSysBasicControls.Commons;
using InSysDSDisplayer.Views;
using InSysDSDisplayer;
using System.Xml.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using InSysBasicControls;
using UtilLib.StaticMethod;
using InSysTouchflowData.Models.ActionEvents;

namespace iVisionTouchDesigner
{
    public class MainManager
    {
        #region singleton 생성자

        private static MainManager _instance;
        public static MainManager Instance
        {
            get { return _instance; }
        }

        static MainManager()
        {
            _instance = new MainManager();
        }

        private MainManager()
        {
            touchProjectInfo = TouchProjectInfo.Instance;
            pageTabItems = new List<PageTabItem>();
            timelineData = new TimelineData();
            previewDisplayManager = DSDisplayManager.Instance;
        }

        #endregion       

        public ActionByUserDelegate CancelActionScreenSizeByUserHandler;
        public ActionByUserDelegate OkActionTouchProjectInfoByUserHandler;

        private MainWindow mainWindow;
        private TouchProjectInfo touchProjectInfo;
        private List<PageTabItem> pageTabItems;
        private PageTabItem currentPageTabItem;       
        private TimelineData timelineData;
        private string currentProjectPath;
        private double projectTime;
        private DSDisplayManager previewDisplayManager;
		private NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();// NLog.LogManager.GetLogger("TouchDesignerLog");

        public bool IsProjectOpenState = false;

        public bool IsPreviewPlaying
        {
            get { return this.previewDisplayManager.IsPreviewPlaying; }
            set { this.previewDisplayManager.IsPreviewPlaying = value; }
        }
        

        #region public 프로퍼티

        public TouchProjectInfo TouchProjectInfo
        {
            get { return this.touchProjectInfo; }
        }

        public double DefaultPageLifeTime
        {
            get { return this.touchProjectInfo.DefaultPageLifeTime; }
        }

        public double PageWidth
        {
            get { return this.touchProjectInfo.PageSize.Width; }
        }

        public double PageHeight
        {
            get { return this.touchProjectInfo.PageSize.Height; }
        }

        public string DefaultPageName
        {
            get { return this.touchProjectInfo.CheckDefaultPageName(); }
        }       

        public TimelineData TimelineData
        {
            get { return this.timelineData; }
            set { this.timelineData = value; }
        }

        public PageTabItem CurrentPageTabItem
        {
            get 
            {
                return currentPageTabItem;
            }

            set { currentPageTabItem = value; }
        }

        public DesignContainer CurrentDesignContainer
        {
            get { return this.CurrentPageTabItem.Content as DesignContainer; }
        }
        
        #endregion

        public DesignContainer CreateTouchDesignContainerCanvas()
        {
            DesignContainer containerCanvas = new DesignContainer();
            containerCanvas.Background = new SolidColorBrush(Colors.Beige);

            return containerCanvas;
        }

        public void SetMainManager(MainWindow window)
        {
            mainWindow = window;
        }

        public void StartProject()
        {
            OpenWindow openWindow = new OpenWindow();
            openWindow.Owner = mainWindow;
            openWindow.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            openWindow.Show();
        }

        public void NewProject()
        {
            ProjectSettingWindow psWindow = new ProjectSettingWindow();
            psWindow.Owner = this.mainWindow;
            psWindow.SetOKTouchProjectInfoEvent += new EventHandler<TouchProjectInfoEventArgs>(popup_SetOKTouchProjectInfoEvent);
            psWindow.CancelScreenSizeEvent += (s, e) =>
            {
                if (CancelActionScreenSizeByUserHandler != null)
                    CancelActionScreenSizeByUserHandler();                    
            };
            psWindow.ShowDialog();

        }

        public void ShowTouchDesignerWindow()
        {
            this.mainWindow.Show();
        }

        public void HideTouchDesignerWindow()
        {
            this.mainWindow.Hide();
        }

        void popup_SetOKTouchProjectInfoEvent(object sender, TouchProjectInfoEventArgs e)
        {
            if (OkActionTouchProjectInfoByUserHandler != null)
                OkActionTouchProjectInfoByUserHandler();

            this.currentProjectPath = "";

            double width = double.Parse(e.Width);
            double height = double.Parse(e.Height);
            double pagetime = e.PageTime;

            this.InitProject(); 

            touchProjectInfo.PageSize = new Size(width, height);
            touchProjectInfo.DefaultPageLifeTime = pagetime;// 디폴트 Page 시간 설정
            projectTime = e.PageTime;
            this.mainWindow.InsertNewTouchPage(Guid.NewGuid(), Guid.Empty, width, height, touchProjectInfo.DefaultPageLifeTime, touchProjectInfo.CheckDefaultPageName(), 0);

            this.mainWindow.ArrangeFlowPage();

            this.IsProjectOpenState = true;

            SetProjectTitle("", "");
        }

        public void AddPageTabItem(PageTabItem pagetabitem)
        {
            if (this.pageTabItems.Contains(pagetabitem) == false)
                this.pageTabItems.Add(pagetabitem);

            //추가시 자동 선택되도록 한다.
            this.SetCurrentPageTabItem(pagetabitem);
        }

        public PageTabItem GetPageTabItem(string id)
        {
            var pageTabItem = this.pageTabItems.Where(o => (o.Content as DesignContainer).TouchPageInfo != null && (o.Content as DesignContainer).TouchPageInfo.PageGuid.Equals(id) == true).SingleOrDefault();

            return pageTabItem;
        }

        public DesignContainer GetDesignContainer(string id)
        {
            var pageTabItem = this.pageTabItems.Where(o => (o.Content as DesignContainer).TouchPageInfo != null && (o.Content as DesignContainer).TouchPageInfo.PageGuid.Equals(id) == true).SingleOrDefault();

            return pageTabItem.Content as DesignContainer;
        }

        public void AddTouchPageInfo(TouchPageInfo pageInfo)
        {
            this.touchProjectInfo.AddTouchPageInfo(pageInfo);
        }

        public void RemoveTouchPageInfo(TouchPageInfo pageInfo)
        {
            this.touchProjectInfo.RemovePageInfo(pageInfo);
        }

        public bool ChangedPagePropertyValue(string oldPageID, string key, object value)
        {
            PageTabItem pagetabitem = this.GetPageTabItem(oldPageID);
            if (pagetabitem == null)
                return false;
            switch (key)
            {
                case "PageName":
                    {
                        string oldPageName = pagetabitem.HeaderText;
                        string newPageName = value as string;
                        if (oldPageName.Equals(newPageName) == true || oldPageName.ToLower().Equals(newPageName.ToLower()) == true)//페이지명이 디렉토리 명으로 사용되기 때문에 대소문자 구분하지 않음. 중문 간체일 경우 ToLower() 동작 안됨.
                            return false;

                        if (CheckPageName(oldPageID, newPageName) == false)
                        {
                            MessageBox.Show("동일한 페이지명이 존재합니다.");
                            this.CurrentDesignContainer.TouchPageInfo.PageNameTemp = oldPageName;
                            return false;
                        }

                        if (this.ChangePageTabItemHeaderName(pagetabitem, newPageName, oldPageName) == true)
                        {
                            if (string.IsNullOrEmpty(InSysDataManager.TouchflowProjectPath) == false && File.Exists(InSysDataManager.TouchflowProjectPath) == true)
                            {
                                this.RenamePageOfLocalDirectory(oldPageName, newPageName);
                                List<Dictionary<string, object>> touchflowData = SetTouchflowProjectInfoFile(touchProjectInfo);
                                //InSysDataManager.SaveAuto(touchflowData, touchProjectInfo, SerializedFormat.Binary);
                            }

                            this.ChangeEventTargetPageName(oldPageID, oldPageName, newPageName);//Event가 적용된 ControlElement의 속성에서 페이지명 변경
                        }
                    }
                    break;
            };

            return true;
        }

        /// <summary>
        /// 페이지명 변경시에 GoToPage Event가 설정된 컨트톨을 찾아서 변경된 페이지명으로 변경한다.
        /// </summary>
        /// <param name="pageID">변경된 Page ID</param>
        /// <param name="oldPageName">변경전 PageName</param>
        /// <param name="newPageName">변경된 PageName</param>
        private void ChangeEventTargetPageName(string pageID, string oldPageName, string newPageName)
        {
            foreach (var pageinfo in this.touchProjectInfo.PageInfoList)
            {
                foreach (var contentinfo in pageinfo.Contents)
                {
                    IDesignElement designElement = contentinfo.ContentObject as IDesignElement;
                    if (designElement != null)
                    {
                        if (designElement.ActionEvent != null)
                        {
                            switch (designElement.ActionEvent.ActionEventType)
                            {
                                case ActionEventType.GoToPage:
                                    {
                                        MovePageItem mpItem = (designElement.ActionEvent as GoToPageEvent).SelectedPageItem;
                                        if (mpItem != null)
                                        {
                                            if (mpItem.PageID.ToLower().Equals(pageID.ToLower()) == true)
                                            {
                                                mpItem.PageName = newPageName;
                                            }
                                        }
                                    }
                                    break;
                            }
                        }
                    }                    
                }
            }
        }

        /// <summary>
        /// 동일 페이지 명 체크.
        /// 대소문자 구분함.
        /// 페이지명이 디렉토리 명으로 사용되기 때문에 대소문자로 파일명 변경가능하도록 하였음.
        /// </summary>
        /// <param name="newPageName"></param>
        /// <returns></returns>
        private bool CheckPageName(string pageid, string newPageName)
        {
            var pageInfo = touchProjectInfo.PageInfoList.Where(o => o.PageGuid.ToString() != pageid && (o.PageNameTemp.Equals(newPageName) == true || o.PageNameTemp.ToLower().Equals(newPageName.ToLower()) == true)).FirstOrDefault();
            if (pageInfo != null)
                return false;

            return true;
        }

        public bool ChangePageTabItemHeaderName(PageTabItem pageTabItem, string newPageName, string oldPageName)
        {
            try
            {
                pageTabItem.HeaderText = newPageName;
                this.mainWindow.TouchDesignTabControl.ChangeTabItemName(pageTabItem);
                return true;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message + ">> " + ex.StackTrace);
                return false;
            }
        }

        public void ChangedContentElementPropertyValue(FrameworkElement elementControl, string propertyName)
        {
            IDesignElement designElement = elementControl as IDesignElement;
            if (designElement != null)
            {
                switch (propertyName)
                {
                    case "Playlist":
                        if (designElement.IsSupportPlaylist == true && designElement.Playlist != null)
                        {
                            this.UpdatePlaylistItem(this.CurrentDesignContainer.TouchPageInfo.PageGuid, designElement, designElement.Playlist);
                            this.UpdateContentItemData(designElement, designElement.Playlist);
                            this.CurrentDesignContainer.TouchPageInfo.UpdateControlContent(elementControl, propertyName, designElement.Properties[propertyName]);
                            designElement.InitProperties();
                        }
                        break;
                    default:
                        designElement.InitProperties();
                        this.CurrentDesignContainer.TouchPageInfo.UpdateControlContent(elementControl, designElement.Properties);
                        break;
                }                
            }
        }

        public void RenamePageOfLocalDirectory(string oldPageName, string newPageName)
        {
            InSysDataManager.RenamePageOfLocalDirectory(this.currentProjectPath, this.CurrentDesignContainer.TouchPageInfo, oldPageName, newPageName);
        }

        public void ResizeTimeline(IDesignElement designElement)
        {
            if (designElement != null)
            {
                if (designElement.IsSupportPlaylist == true)
                {
                    if (designElement.Playlist == null)
                        throw new Exception(designElement.Name + " 이름을 가진 " + designElement.Type.Name + " 타입의 컨텐츠의 Playlist의 값이 Null이빈다.");
                    this.ResizeTimelineContentItem(designElement, designElement.Playlist);
                }
                else
                {
                    this.ResizeTimelineContentItem(designElement);
                }
            }
        }

        /// <summary>
        /// PlaylistItem을 가지고 있는 ContentItem의 크기를 재조정한다.
        /// </summary>
        /// <param name="designElement"></param>
        /// <param name="playlistItems"></param>
        private void ResizeTimelineContentItem(IDesignElement designElement, List<PlaylistItem> playlistItems)
        {
            ContentItemBoxData currentItemBoxData = this.TimelineData.ContentItemBoxDataList.Where(o => o.Item.Equals(designElement) == true).SingleOrDefault();
            if (currentItemBoxData != null)
            {           
                bool isFullTime = false;
                double totalLength = 0;
                foreach (var update_playlistitem in playlistItems)
                {
                    ContentItemData contentItemData = currentItemBoxData.ContentItemDatas.Where(o => o.Playlistitem.Equals(update_playlistitem) == true).FirstOrDefault();
                    if (contentItemData != null)
                    {                        
                        //contentItemData.ContentLength = this.mainWindow.timelinebody.GetContentItemLength(update_playlistitem.DurationAsTimeSpan.TotalSeconds);// *this.TimelineData.TimelineHandDistance;
                        //contentItemData.TotalSeconds = update_playlistitem.DurationAsTimeSpan.TotalSeconds;
                        //contentItemData.ContentPlayTime = this.mainWindow.timelinebody.ContentItemPlalyTime(update_playlistitem.DurationAsTimeSpan.TotalSeconds);// this.mainWindow.timelinebody.ContentItemTimeDisplay(contentItemData.ContentLength);             
                        changeContentItemData(contentItemData, update_playlistitem.TIME, ref isFullTime);
                        totalLength += contentItemData.ContentLength;
                    }
                }

                changeControlSizeOfContentItemBox(currentItemBoxData, designElement, totalLength);
            }
        }
        
        /// <summary>
        /// PlaylistItem을 가지고 있지 않는 ContentItem의 크기를 재조정한다.
        /// </summary>
        /// <param name="designElement"></param>
        private void ResizeTimelineContentItem(IDesignElement designElement)
        {
            ContentItemBoxData currentItemBoxData = this.TimelineData.ContentItemBoxDataList.Where(o => o.Item.Equals(designElement) == true).SingleOrDefault();
            if (currentItemBoxData != null && currentItemBoxData.ContentItemDatas.Count > 0)
            {
                ContentItemData contentItemData = currentItemBoxData.ContentItemDatas[0];
                if (contentItemData != null)
                {
                    this.mainWindow.Set_ContentItemData(contentItemData, designElement, this.CurrentDesignContainer);
                    changeControlSizeOfContentItemBox(currentItemBoxData, designElement, contentItemData.ContentLength);
                }
            }
        }

        public void changeControlSizeOfContentItemBox(ContentItemBoxData currentItemBoxData, IDesignElement designElement, double totalTime)
        {
            double width = 0.0;
            double starttimeValue = designElement.StartTime.TotalSeconds;
            double endtimeValue = designElement.EndTime.TotalSeconds;

            if ((endtimeValue - starttimeValue) <= 0)
                return;

            if (this.CurrentDesignContainer.TouchPageInfo.IsApplyPageLifeTime == true)
            {
                if (designElement.IsApplyLifeTime == true)
                {
                    if (this.CurrentDesignContainer.TouchPageInfo.PageLifeTimeTemp > (endtimeValue - starttimeValue))
                    {
                        double left = this.mainWindow.timelinebody.GetContentItemLength(starttimeValue);
                        width = this.mainWindow.timelinebody.GetContentItemLength(endtimeValue - starttimeValue);
                        this.mainWindow.timelinebody.changeContentItemBoxSize(currentItemBoxData, left, width);
                    }
                    else
                    {
                        double left = 0;
                        width = this.mainWindow.timelinebody.GetContentItemLength(this.CurrentDesignContainer.TouchPageInfo.PageLifeTimeTemp);
                        this.mainWindow.timelinebody.changeContentItemBoxSize(currentItemBoxData, left, width);
                    }
                }
                else
                {
                    double left = 0;
                    width = this.mainWindow.timelinebody.GetContentItemLength(this.CurrentDesignContainer.TouchPageInfo.PageLifeTimeTemp);
                    this.mainWindow.timelinebody.changeContentItemBoxSize(currentItemBoxData, left, width);
                }
            }
            else
            {
                if (designElement.IsApplyLifeTime == true)
                {
                    double left = this.mainWindow.timelinebody.GetContentItemLength(starttimeValue);
                    width = this.mainWindow.timelinebody.GetContentItemLength(endtimeValue - starttimeValue);
                    this.mainWindow.timelinebody.changeContentItemBoxSize(currentItemBoxData, left, width);
                }
                else
                {
                    double left = 0;
                    width = this.mainWindow.timelinebody.GetContentItemLength(totalTime);
                    this.mainWindow.timelinebody.changeContentItemBoxSize(currentItemBoxData, left, width);
                }
                
            }

            currentItemBoxData.ContentBoxLength = width;// this.mainWindow.timelinebody.GetContentItemLength(totalTime);
        }

        public void CreatePlaylistItem(string pageID, IDesignElement designElement, List<PlaylistItem> playlistItems)
        {
            try
            {
                if (this.IsPreviewPlaying == true) return;

                if (this.TimelineData != null && this.TimelineData.ContentItemBoxDataList.Count > 0)
                {
                    ContentItemBoxData currentItemBoxData = this.TimelineData.ContentItemBoxDataList.Where(o => o != null & o.Item != null && o.Item.Equals(designElement) == true).SingleOrDefault();
                    if (currentItemBoxData != null)
                    {
                        List<ContentItemData> tempContentItemDatas = currentItemBoxData.ContentItemDatas.ToList();

                        currentItemBoxData.ContentItemDatas.Clear();

                        int orderIndex = 1;
                        foreach (var newPlaylistItem in playlistItems)
                        {
                            ContentItemData contentItemData = tempContentItemDatas.Where(o => o.Playlistitem.Equals(newPlaylistItem) == true).FirstOrDefault();
                            if (contentItemData != null)
                            {
                                PlaylistItem playlistitem = contentItemData.Playlistitem as PlaylistItem;
                                if (playlistitem != null)
                                {
                                    playlistitem = newPlaylistItem;
                                }
                                else
                                {
                                    contentItemData = this.CreateNewContentItemData(designElement, newPlaylistItem, designElement.InSysControlType, orderIndex++, designElement.Name);
                                }
                                currentItemBoxData.ContentItemDatas.Add(contentItemData);
                            }
                            else
                            {
                                contentItemData = this.CreateNewContentItemData(designElement, newPlaylistItem, designElement.InSysControlType, orderIndex++, designElement.Name);
                                currentItemBoxData.ContentItemDatas.Add(contentItemData);
                            }
                        }                     
                    }
                }
                
                this.UpdateContentItemData(designElement, playlistItems);
                designElement.InitProperties();
                this.CurrentDesignContainer.TouchPageInfo.UpdateControlContent(designElement as FrameworkElement, "Playlist", designElement.Properties["Playlist"]);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        public void UpdatePlaylistItem(string pageID, IDesignElement designElement, List<PlaylistItem> playlistItems)
        {
            ContentItemBoxData currentItemBoxData = this.TimelineData.ContentItemBoxDataList.Where(o => o.Item.Equals(designElement) == true).SingleOrDefault();           
            if (currentItemBoxData != null)
            {
                int orderIndex = 1;                
                foreach (var update_playlistitem in playlistItems)
                {
                    ContentItemData contentItemData = currentItemBoxData.ContentItemDatas.Where(o => o.Playlistitem.Equals(update_playlistitem) == true).FirstOrDefault();
                    if (contentItemData != null)
                    {
                        PlaylistItem playlistitem = contentItemData.Playlistitem as PlaylistItem;
                        if (playlistitem != null)
                        {
                            playlistitem = update_playlistitem;
                        }
                        else
                        {
                            contentItemData = this.CreateNewContentItemData(designElement, update_playlistitem, designElement.InSysControlType, orderIndex++, designElement.Name);
                        }
                        currentItemBoxData.ContentItemDatas.Add(contentItemData);
                    }
                    else
                    {
                        contentItemData = this.CreateNewContentItemData(designElement, update_playlistitem, designElement.InSysControlType, orderIndex++, designElement.Name);
                        currentItemBoxData.ContentItemDatas.Add(contentItemData);
                    }
                }
            }
        }

        public double GetTimelineContentBoxItemMaxTime(double defaultMaxTime)
        {
            double maxContentBoxItemTotalTime = defaultMaxTime;
            foreach (var box in this.TimelineData.ContentItemBoxDataList)
            {
                IDesignElement designElement = box.Item as IDesignElement;
                if (designElement == null || designElement.IsSupportPlaylist == false)
                    continue;
                double tempTotal = 0;
                box.ContentItemDatas.ToList().ForEach(o => tempTotal += o.TotalSeconds);

                if (maxContentBoxItemTotalTime < tempTotal)
                {
                    maxContentBoxItemTotalTime = tempTotal;
                }
            }

            return maxContentBoxItemTotalTime;
        }

        /// <summary>
        /// Timeline Header의 전체 길이를 재설정
        /// </summary>
        public void ResetTimelineLength()
        {
            double currentTimelineLength = this.TimelineData.TimelineTotalTime;//현재 Timeline 길이.

            double maxContentBoxItemTotalTime = currentTimelineLength;
            foreach (var box in this.TimelineData.ContentItemBoxDataList)
            {
                IDesignElement designElement = box.Item as IDesignElement;
                if (designElement == null || designElement.IsSupportPlaylist == false)
                    continue;
                double tempTotal = 0;
                box.ContentItemDatas.ToList().ForEach(o => tempTotal += o.TotalSeconds);

                if (maxContentBoxItemTotalTime < tempTotal)
                {
                    maxContentBoxItemTotalTime = tempTotal;
                }
            }

            if (currentTimelineLength < maxContentBoxItemTotalTime)
                this.SetTimelineHeader(maxContentBoxItemTotalTime);
            else
                this.SetTimelineHeader(currentTimelineLength);
        }

        public void UpdateContentItemData(IDesignElement designElement, List<PlaylistItem> playlistItems)
        {
            double maxContentBoxItemTotalTime = GetTimelineContentBoxItemMaxTime(this.TimelineData.TimelineTotalTime);         

            ContentItemBoxData currentItemBoxData = this.TimelineData.ContentItemBoxDataList.Where(o => o.Item.Equals(designElement) == true).SingleOrDefault();
            if (currentItemBoxData != null)
            {
                int playlistitemOrder = 1;
                double totalTime = 0.0;                
                bool isHaveInfinityPlayTime = false;
                double infinityContentItemLength = 50.0;
                bool isInfinityEnd = false;
                if (designElement.IsSupportPlaylist == true)
                {
                    foreach (var update_playlistitem in playlistItems)
                    {
                        ContentItemData contentItemData = currentItemBoxData.ContentItemDatas.Where(o => o.Playlistitem.Equals(update_playlistitem) == true).FirstOrDefault();
                        if (contentItemData == null)
                            continue;

                        changeContentItemData(contentItemData, update_playlistitem.TIME, ref isHaveInfinityPlayTime);

                        if (isHaveInfinityPlayTime == false)//PlaylistItem 이 플레이 시간을 가지고 있다면
                        {
                            totalTime += contentItemData.TotalSeconds;
                        }
                        else//그렇지 않고 무한대 시간을 가진 PlaylistItem 일 경우
                        {
                            if (playlistitemOrder == 1)//그 것이 첫번째 PlaylistItem 이라면 Timeline의 크기를 신경쓰지 않는다.
                            {
                                totalTime = maxContentBoxItemTotalTime;// this.TimelineData.TimelineTotalTime;
                            }
                            else//그 것이 두번째 PlaylistItem 이라면 첫번째 PlaylistItem 플레이 시간을 고려해서 계산한다.
                            {
                                if (isInfinityEnd == false)
                                {
                                    if (totalTime < maxContentBoxItemTotalTime)
                                    {
                                        contentItemData.ContentLength = this.mainWindow.timelinebody.GetContentItemLength(maxContentBoxItemTotalTime - totalTime);
                                        contentItemData.TotalSeconds = maxContentBoxItemTotalTime - totalTime;

                                        totalTime = maxContentBoxItemTotalTime;
                                    }
                                    else if (totalTime > maxContentBoxItemTotalTime)
                                    {
                                        contentItemData.ContentLength = this.mainWindow.timelinebody.GetContentItemLength(infinityContentItemLength);
                                        contentItemData.TotalSeconds = infinityContentItemLength;
                                        totalTime = totalTime + infinityContentItemLength;
                                    }
                                }
                            }
                            isInfinityEnd = true;
                        }

                        playlistitemOrder++;
                    }

                    currentItemBoxData.ContentBoxLength = this.mainWindow.timelinebody.GetContentItemLength(totalTime);//ContentItem을 포함하는 ListBox의 ContentItem Container 컨트롤의 크기를 설정한다.

                    //changeControlSizeOfContentItemBox(currentItemBoxData, designElement, totalTime);                 
                }                

                #region Timeline Header 눈금 재조정
                if (this.CurrentDesignContainer.TouchPageInfo.IsApplyPageLifeTime == true)//Page 타임이 설정되어 있을 경우에 Timeline Header의 눈금을 Page 타임에 조정한다.
                {
                    this.SetTimelineHeader((int)this.CurrentDesignContainer.TouchPageInfo.PageLifeTimeTemp);
                    this.mainWindow.timelinebody.SetContentBoxItemMaxTime(this.CurrentDesignContainer.TouchPageInfo.PageLifeTimeTemp);
                }
                else
                {
                    if (isHaveInfinityPlayTime == true)
                    {
                        if (totalTime != 0.0)
                        {
                            if (totalTime < maxContentBoxItemTotalTime)
                                totalTime = maxContentBoxItemTotalTime;//this.mainWindow.timelinebody.GetTimelineHeaderLength();

                            this.mainWindow.timelinebody.SetTimelineHeader((int)totalTime);
                        }
                        else
                        {
                            this.mainWindow.timelinebody.SetTimelineHeader((int)maxContentBoxItemTotalTime);
                        }
                    }
                    else
                    {
                        if (totalTime < maxContentBoxItemTotalTime)
                            totalTime = maxContentBoxItemTotalTime;
                        this.mainWindow.timelinebody.SetTimelineHeader((int)totalTime);
                    }

                    this.mainWindow.timelinebody.SetContentBoxItemMaxTime(totalTime);
                }
                #endregion
            }
        }

        public void SetTimelineHeader(int time)
        {
            this.mainWindow.timelinebody.SetTimelineHeader(time);
        }

        /// <summary>
        /// Timeline Header Length 설정.
        /// </summary>
        /// <param name="length"></param>
        public void SetTimelineHeader(double length)
        {
            this.mainWindow.timelinebody.SetTimelineHeader(length);
        }

        public void changeContentItemData(ContentItemData contentItemData, TimeSpan time, ref bool isHaveInfinityPlayTime)
        {
            if (isHaveInfinityPlayTime == false)
            {
                if (time == TimeSpan.Zero)
                {
                    if (this.CurrentDesignContainer.TouchPageInfo.IsApplyPageLifeTime == true)
                    {
                        contentItemData.ContentLength = this.mainWindow.timelinebody.GetContentItemLength(this.CurrentDesignContainer.TouchPageInfo.PageLifeTimeTemp);
                        contentItemData.ContentPlayTime = this.mainWindow.timelinebody.ContentItemTimeDisplay(contentItemData.ContentLength);
                        contentItemData.TotalSeconds = this.CurrentDesignContainer.TouchPageInfo.PageLifeTimeTemp;
                    }
                    else
                    {
                        contentItemData.ContentLength = this.mainWindow.timelinebody.GetContentItemLength(this.TimelineData.TimelineTotalTime);
                        contentItemData.ContentPlayTime = "∞";
                        contentItemData.TotalSeconds = this.TimelineData.TimelineTotalTime;
                    }
                    isHaveInfinityPlayTime = true;
                }
            }
            else
            {
                contentItemData.ContentLength = 0;
                contentItemData.TotalSeconds = 0;
            }
        }

        private ContentItemData CreateNewContentItemData(IDesignElement designElement, PlaylistItem playlistItem, InSysControlType elementType, int orderIndex, string name)
        {
            ContentItemData contentItemData = new ContentItemData();
            switch (elementType)
            {
                case InSysControlType.InSysSlideViewer:
                    {
                        string contentFilepath = playlistItem.Content as string;
                        contentItemData.ContentName = contentFilepath;
                        designElement.UpdatePlaylistItems();
                        contentItemData.Thumbnail = designElement.ThumnailImageSource;// ObjectConverters.GetImageSource(contentFilepath, UriKind.Absolute);
                        contentItemData.ContentLength = playlistItem.DurationAsTimeSpan.TotalSeconds * this.TimelineData.TimelineHandDistance / this.TimelineData.TimelineHandIntervalTime;
                        contentItemData.TotalSeconds = playlistItem.DurationAsTimeSpan.TotalSeconds;
                        contentItemData.ContentPlayTime = this.mainWindow.timelinebody.ContentItemTimeDisplay(contentItemData.ContentLength);
                        contentItemData.OrderIndex = orderIndex;
                        contentItemData.Playlistitem = playlistItem;
                    }
                    break;
                case InSysControlType.InSysBasicButton:
                    break;
                case InSysControlType.InSysImageBox:
                    {      
                        string contentFilepath = playlistItem.Content as string;
                        contentItemData.ContentName = contentFilepath;
                        designElement.UpdatePlaylistItems();
                        contentItemData.Thumbnail = designElement.ThumnailImageSource;// ObjectConverters.GetImageSource(contentFilepath, UriKind.Absolute);
                        contentItemData.ContentLength = playlistItem.DurationAsTimeSpan.TotalSeconds * this.TimelineData.TimelineHandDistance / this.TimelineData.TimelineHandIntervalTime;
                        contentItemData.TotalSeconds = playlistItem.DurationAsTimeSpan.TotalSeconds;
                        contentItemData.ContentPlayTime = this.mainWindow.timelinebody.ContentItemTimeDisplay(contentItemData.ContentLength);
                        contentItemData.OrderIndex = orderIndex;
                        contentItemData.Playlistitem = playlistItem;
                    }
                    break;
                case InSysControlType.InSysRectangle:
                    break;
                case InSysControlType.InSysRectArea:
                    break;
                case InSysControlType.InSysBasicTextBox:
                    break;
                case InSysControlType.InSysLabel:
                    break;     
                case InSysControlType.InSysStreamingBox:
                case InSysControlType.InSysVideoBox:
                    {
                        //타임라인에 들어가는 썸네일 이미지 캡쳐하는 부분 - 디자인페이지의 썸네일과 중복 수행(차후 부하 줄일 필요 있음.)
                        /*
                        Uri uri = new Uri(playlistItem.Content, UriKind.Absolute);                        
                        VideoScreenShot.CaptureScreen(uri, TimeSpan.FromSeconds(5), playlistItem.Content, 1, null,
                            (frame, state) =>
                            {
                                contentItemData.Thumbnail = frame;
                            }
                            );
                         */
                        designElement.UpdatePlaylistItems();
                        contentItemData.Thumbnail = designElement.ThumnailImageSource;
                        contentItemData.ContentName = this.GetFileName(playlistItem.CONTENTSVALUE as string);

                        contentItemData.ContentLength = playlistItem.DurationAsTimeSpan.TotalSeconds * this.TimelineData.TimelineHandDistance / this.TimelineData.TimelineHandIntervalTime;
                        contentItemData.TotalSeconds = playlistItem.DurationAsTimeSpan.TotalSeconds;
                        contentItemData.ContentPlayTime = this.mainWindow.timelinebody.ContentItemTimeDisplay(contentItemData.ContentLength); 
                        contentItemData.OrderIndex = orderIndex;
                        contentItemData.Playlistitem = playlistItem;
                    }

                    break;
                case InSysControlType.InSysAudioBox:
                    {                        
                        contentItemData.ContentName = this.GetFileName(playlistItem.Content as string);
                        contentItemData.Thumbnail = ObjectConverters.GetImageSource(@"\images\timeline_audio.png", UriKind.Relative);// image.Source;
                        contentItemData.ContentLength = playlistItem.DurationAsTimeSpan.TotalSeconds * this.TimelineData.TimelineHandDistance / this.TimelineData.TimelineHandIntervalTime;
                        contentItemData.TotalSeconds = playlistItem.DurationAsTimeSpan.TotalSeconds;
                        contentItemData.ContentPlayTime = this.mainWindow.timelinebody.ContentItemTimeDisplay(contentItemData.ContentLength);
                        contentItemData.OrderIndex = orderIndex;
                        contentItemData.Playlistitem = playlistItem;
                    }
                    break;
                case InSysControlType.InSysAnalogueClock:
                    break;
                case InSysControlType.InSysDate:
                    break;
                case InSysControlType.InSysDigitalClock:
                    break;
                case InSysControlType.InSysEllipse:
                    break;
                case InSysControlType.InSysRSS:
                    break;
                case InSysControlType.InSysScrollText:
                    {
                        contentItemData.ContentName = playlistItem.Content;
                        designElement.UpdatePlaylistItems();
                        //contentItemData.Thumbnail = designElement.ThumnailImageSource; //image.Source;
                        contentItemData.ContentLength = playlistItem.DurationAsTimeSpan.TotalSeconds * this.TimelineData.TimelineHandDistance / this.TimelineData.TimelineHandIntervalTime;
                        contentItemData.TotalSeconds = playlistItem.DurationAsTimeSpan.TotalSeconds;
                        contentItemData.ContentPlayTime = this.mainWindow.timelinebody.ContentItemTimeDisplay(contentItemData.ContentLength);
                        contentItemData.OrderIndex = orderIndex;
                        contentItemData.Playlistitem = playlistItem;
                    }
                    break;
                case InSysControlType.InSysWebControl:
                    {
                        contentItemData.ContentName = playlistItem.Content;
                        designElement.UpdatePlaylistItems();
                        contentItemData.Thumbnail = designElement.ThumnailImageSource; //image.Source;
                        contentItemData.ContentLength = playlistItem.DurationAsTimeSpan.TotalSeconds * this.TimelineData.TimelineHandDistance / this.TimelineData.TimelineHandIntervalTime;
                        contentItemData.TotalSeconds = playlistItem.DurationAsTimeSpan.TotalSeconds;
                        contentItemData.ContentPlayTime = this.mainWindow.timelinebody.ContentItemTimeDisplay(contentItemData.ContentLength);
                        contentItemData.OrderIndex = orderIndex;
                        contentItemData.Playlistitem = playlistItem;
                    }
                    break;
                case InSysControlType.InSysFlashBox:
                    {
                        contentItemData.ContentName = playlistItem.Content;
                        designElement.UpdatePlaylistItems();
                        contentItemData.Thumbnail = designElement.ThumnailImageSource; //image.Source;
                        contentItemData.ContentLength = playlistItem.DurationAsTimeSpan.TotalSeconds * this.TimelineData.TimelineHandDistance / this.TimelineData.TimelineHandIntervalTime;
                        contentItemData.TotalSeconds = playlistItem.DurationAsTimeSpan.TotalSeconds;
                        contentItemData.ContentPlayTime = this.mainWindow.timelinebody.ContentItemTimeDisplay(contentItemData.ContentLength);
                        contentItemData.OrderIndex = orderIndex;
                        contentItemData.Playlistitem = playlistItem;
                    }
                    break;
                default:
                    break;
            }

            return contentItemData;
        }

        public void MakeContentElementThumnailImage()
        {
        }

        public void UpdateFlowPageItemCaptureImage(FlowPageItem flowPageItem, PageCanvas captureCanvas)
        {
            flowPageItem.Content = CommonUtils.ExportToPng(captureCanvas);
        }

        public void SetCurrentPageTabItem(PageTabItem selectedPageTabItem)
        {
            this.currentPageTabItem = selectedPageTabItem;
        }

        public void SaveProject()
        {
            this.SaveProject(this.currentProjectPath);
        }

        public void SaveAsProject()
        {
            this.SaveProject("");
        }

        public void SaveProject(string projectPath)
        {
            try
            {
                Mouse.OverrideCursor = Cursors.Wait;
                if (string.IsNullOrEmpty(projectPath) == true)
                {
                    SaveFileDialog saveFileDialog = new SaveFileDialog();
                    saveFileDialog.DefaultExt = ".tpf";
                    saveFileDialog.Filter = "Touch Project(.tpf)|*.tpf";

                    if (saveFileDialog.ShowDialog() == true)
                    {                       
                        string filefullpath = saveFileDialog.FileName;
                        this.SaveProject(this.touchProjectInfo, filefullpath, true);

                        InSysTouchflowData.Models.ProjectModels.ProjectOption projectOption = InSysTouchflowData.Models.ProjectModels.ProjectOption.Instance;
                        projectOption.AddResentProject(InSysDataManager.TouchflowProjectPath);

                        this.currentProjectPath = InSysDataManager.TouchflowProjectPath;

                        FileInfo fi = new FileInfo(filefullpath);
                        SetProjectTitle(fi.Name, fi.FullName);
                    }
                }
                else
                {
                    FileInfo fileinfo = new FileInfo(this.currentProjectPath);

                    string savePath = string.Format("{0}{1}", fileinfo.Directory.FullName, fileinfo.Extension);
                    this.SaveProject(this.touchProjectInfo, this.currentProjectPath, false);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                Mouse.OverrideCursor = null;
            }
        }       

        public bool SaveProject(TouchProjectInfo touchProjectInfo, string savePath, bool isNewSave)
        {
            TouchProjectInfo tempTouchProjectInfo = touchProjectInfo.CloneProperties();
            List<TouchPageInfo> touchPageInfoList = tempTouchProjectInfo.PageInfoList;

            foreach (TouchPageInfo tpInfo in touchPageInfoList)
            {
                List<FrameworkElement> elementColection = new List<FrameworkElement>();
                tpInfo.Contents.Select(o => o.ContentObject).ToList().ForEach(o => elementColection.Add(o));

                foreach (TouchContentInfo touchContentInfo in tpInfo.Contents)
                {
                    IDesignElement designElement = touchContentInfo.ContentObject as IDesignElement;
                    if (designElement != null)
                    {
                        if (string.IsNullOrEmpty(designElement.Name) == true)
                        {
                            designElement.Name = InSysControlManager.CheckInSysControlName(InSysControlManager.GetControlType(touchContentInfo.ContentType.Name), elementColection);
                            touchContentInfo.ContentName = designElement.Name;
                        }
                        designElement.InitProperties();
                        touchContentInfo.DesignElementProperties = InSysBasicControls.InSysControlManager.SerializePropertiesInIDesignElement(designElement.Properties);
                    }
                }
            }
            bool isReplace = false;
            if (File.Exists(savePath) == true)
                isReplace = true;
            List<Dictionary<string, object>> touchflowData = SetTouchflowProjectInfoFile(tempTouchProjectInfo);
            if (isNewSave == true)
                InSysDataManager.SaveNew(touchflowData, tempTouchProjectInfo, savePath, SerializedFormat.Binary, isReplace);
            else
                InSysDataManager.SaveAgain(touchflowData, tempTouchProjectInfo, savePath, SerializedFormat.Binary, isReplace);

            return true;
        }

        private List<Dictionary<string, object>> SetTouchflowProjectInfoFile(TouchProjectInfo tfProjectInfo)
        {            
            List<Dictionary<string, object>> list = new List<Dictionary<string, object>>();
            Dictionary<string, object> projectInfoDic = new Dictionary<string, object>();
            projectInfoDic.Add("TouchflowData", tfProjectInfo);
            projectInfoDic.Add("PlayTime", TimeSpan.FromSeconds(projectTime));
            projectInfoDic.Add("ScheduleTime", TimeSpan.FromSeconds(projectTime));
            projectInfoDic.Add("DefaultPageLifeTime", TimeSpan.FromSeconds(tfProjectInfo.DefaultPageLifeTime));
            projectInfoDic.Add("Width", tfProjectInfo.PageSize.Width);
            projectInfoDic.Add("Height", tfProjectInfo.PageSize.Height);
            list.Add(projectInfoDic);

            return list;
        }

        private void SetProjectTitle(string title, string path)
        {
            if (string.IsNullOrEmpty(title) == true)
            {
                this.mainWindow.titleTouchDesigner.Text = string.Format("{0} (Empty)", Cultures.Resources.titleTouchDesignerWindow);
            }
            else
            {
                this.mainWindow.titleTouchDesigner.Text = string.Format("{0} ({1} - {2})", Cultures.Resources.titleTouchDesignerWindow, title, path);
            }
        }

        public void OpenProject()
        {
            try
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();

                openFileDialog.Filter = "Touch Project(.tpf)| *.tpf";
                openFileDialog.Title = "Touch Project 파일 Open";
                openFileDialog.FileOk += (of_sender, of_e) =>
                {
                    if (OkActionTouchProjectInfoByUserHandler != null)
                        OkActionTouchProjectInfoByUserHandler();

                    string filefullpath = openFileDialog.FileName;
                    FileInfo fi = new FileInfo(filefullpath);
                    string filename = fi.Name;
                    if (System.IO.Path.GetExtension(filename).ToLower() == ".tpf")
                    {
                        this.OpenProject(filefullpath);
                    }
                };
                
                if (openFileDialog.ShowDialog(this.mainWindow) != true)
                {
                    if (CancelActionScreenSizeByUserHandler != null)
                        CancelActionScreenSizeByUserHandler();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public bool OpenProject(string projectPath)
        {
            try
            {
                //this.mainWindow.Cursor = Cursors.Wait;
                Mouse.OverrideCursor = Cursors.Wait;
                this.mainWindow.UpdateLayout();
                this.InitProject();

                TouchProjectInfo projectInfo = this.GetTouchProjectInfoFromFile(projectPath) as TouchProjectInfo;

                foreach (TouchPageInfo pageInfo in projectInfo.PageInfoList)
                {
                    this.InsertPage(pageInfo, true);
                }

                this.touchProjectInfo = projectInfo;
                this.currentProjectPath = projectPath;//프로젝트 파일 Load시 현재 프로젝트 경로 설정.

                //Project Option에 최근 Project 파일 목록 추가.
                InSysTouchflowData.Models.ProjectModels.ProjectOption projectOption = InSysTouchflowData.Models.ProjectModels.ProjectOption.Instance;
                projectOption.AddResentProject(projectPath);

                FileInfo fi = new FileInfo(projectPath);
                this.SetProjectTitle(fi.Name, fi.FullName);

                this.mainWindow.ArrangeFlowPage();              

                this.IsProjectOpenState = true;

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                Mouse.OverrideCursor = null;
            }
        }
              
        public object GetTouchProjectInfoFromFile(string path)
        {
            try
            {
                Dictionary<string, object> touchProjectData = InSysDataManager.LoadTouchProjectData(path, SerializedFormat.Binary);
                //TouchProjectInfo tempProjectInfo = InSysDataManager.Load(path, SerializedFormat.Binary);
                TouchProjectInfo projectInfo = touchProjectData["TouchflowData"] as TouchProjectInfo;
                if (projectInfo != null)
                {
                    #region ProjectTime 
                    TimeSpan ts = (TimeSpan)touchProjectData["PlayTime"];
                    if (ts != null)
                    {
                        if (ts.Seconds != 0)
                            projectTime = ts.TotalSeconds;
                        else
                            projectTime = projectInfo.DefaultPageLifeTime;
                    }
                    else
                    {
                        projectTime = projectInfo.DefaultPageLifeTime;
                    }
                    #endregion

                    foreach (TouchPageInfo pageInfo in projectInfo.PageInfoList)
                    {
                        double pagetime = pageInfo.PageLifeTimeTemp;
                        string pagename = pageInfo.PageNameTemp;
                        if (pageInfo.IsApplyPageLifeTime == true && pagetime == 0.0)
                        {
                            pagetime = projectInfo.DefaultPageLifeTime;
                        }
                        if (string.IsNullOrEmpty(pagename) == true)
                            pagename = projectInfo.CheckDefaultPageName();

                        #region DesignContainer 생성
                        DesignContainer designContainer = this.mainWindow.CreateNewTouchPage(new Guid(pageInfo.PageGuid), new Guid(pageInfo.ParentPageGuid), projectInfo.PageSize.Width, projectInfo.PageSize.Height, pagetime, pagename, pageInfo.PageLayerIndex);
                        designContainer.PageDesignCanvasBox.Background = pageInfo.Background;
                        designContainer.setTouchPageInfo(pageInfo);
                        
                        #endregion

                        foreach (TouchContentInfo item in pageInfo.Contents)
                        {
                            FrameworkElement inSysContentControl = InSysBasicControls.InSysControlManager.GetNewSerializationInSysControl(item.ContentType, item.DesignElementProperties, ViewState.Design) as FrameworkElement;

                            if (inSysContentControl != null)
                            {
                                IDesignElement designElement = inSysContentControl as IDesignElement;
                                if (designElement != null)
                                {
                                    item.ContentObject = inSysContentControl;

                                    #region ContentControl 타입에 따라 Insert 하는 위치 구분.
                                    switch (designElement.InSysControlType)
                                    {
                                        case InSysControlType.InSysAudioBox:
                                            designContainer.PageAudioBox.Children.Add(inSysContentControl);
                                            designContainer.RaiseInsertContentEvent(designElement);
                                            break;
                                        default:
                                            designContainer.PageDesignCanvasBox.Children.Add(inSysContentControl);
                                            designContainer.RaiseInsertContentEvent(designElement);
                                            break;
                                    }
                                    #endregion //ContentControl 타입에 따라 Insert 하는 위치 구분.

                                    #region Playlist의 경로 정보를 가상경로(CONTENTSVALUE)를 이용하여 Opne Project시 절대경로(Content)로 변경한다.
                                    if (item.DesignElementProperties.ContainsKey("Playlist") == true)
                                    {
                                        List<PlaylistItem> playlistItems = (List<PlaylistItem>)item.DesignElementProperties["Playlist"];
                                        switch (designElement.InSysControlType)
                                        {
                                            case InSysControlType.InSysStreamingBox:
                                            case InSysControlType.InSysScrollText:                                               
                                            case InSysControlType.InSysWebControl:
                                                break;
                                            default:
                                                {
                                                    FileInfo projectFileinfo = new FileInfo(path);
                                                    if (projectFileinfo.Exists == true)
                                                    {
                                                        foreach (var playlistitem in playlistItems)
                                                        {
                                                            string fullpath = string.Format("{0}\\{1}", projectFileinfo.Directory.FullName, playlistitem.CONTENTSVALUE);
                                                            playlistitem.Content = string.Format(@"{0}", fullpath);

                                                            logger.Info(fullpath);
                                                        }
                                                    }
                                                }
                                                break;
                                        }                                                                               

                                        designElement.UpdatePlaylistItems();
                                        designElement.InitProperties();//Open한 프로젝트 파일에 대해 다시 File Save할 경우에 대해 오류를 없애준다. ???
                                        this.CreatePlaylistItem(pageInfo.PageGuid, designElement, playlistItems);
                                    }
                                    #endregion

                                    this.ResizeTimeline(designElement);

                                    if (item.DesignElementProperties.Keys.Contains("ActionEvent") == true)
                                        designElement.SetActionEvent(item.DesignElementProperties["ActionEvent"]);//이벤트 설정

                                    if (designElement.IsApplyLifeTime == false)
                                        designElement.EndTime = TimeSpan.FromSeconds(pageInfo.PageLifeTimeTemp);

                                    #region  Timeline 정보 설정.

                                    double startTiem = designElement.StartTime.TotalSeconds;
                                    double endTiem = designElement.EndTime.TotalSeconds;
                                    if (designElement == null)
                                    {
                                        startTiem = 0;
                                        endTiem = pageInfo.PageLifeTimeTemp;
                                    }                               

                                    #endregion //Timeline 정보 설정.
                                }
                            }
                        }
                    }
                }
                this.ResetTimelineLength();

                //#region 프로젝트파일 로드후 루트 페이지를 선택페이지로 설정한다.
                //FlowPageItem rootFllowPageItem = this.mainWindow.pageflowview.FindRootFlowPageItem();                
                //rootFllowPageItem.SelectionItem();
                //#endregion

                return projectInfo;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }
        }

        public bool InsertPage(TouchPageInfo insertPageInfo, bool isOpenProject)
        {
            try
            {
                if (isOpenProject != true)
                {
                    if (this.CurrentDesignContainer.TouchPageInfo != null)
                    {
                        insertPageInfo.PageLayerIndex = this.CurrentDesignContainer.TouchPageInfo.PageLayerIndex + 1;
                        insertPageInfo.ParentPageGuid = this.CurrentDesignContainer.TouchPageInfo.PageGuid;
                        var sameIndexList = this.GetSameLayerPageInfo(insertPageInfo);
                        insertPageInfo.PageDisplayOrder = sameIndexList.Count() + 1;
                    }
                    else
                    {
                        insertPageInfo.PageLayerIndex = 0;
                        insertPageInfo.ParentPageGuid = string.Empty;
                    }
                }

                //this.touchProjectInfo.AddTouchPageInfo(insertPageInfo);       


                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }

        }

        public void ExportPage(TouchPageInfo exportPageInfo)
        {
            /*
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.DefaultExt = ".tsf";
            saveFileDialog.Filter = "Touch Screen(.tsf)|*.tsf";
            if (saveFileDialog.ShowDialog() == true)
            {
                string filefullpath = saveFileDialog.FileName;                

                FileStream flStream = new FileStream(filefullpath, FileMode.OpenOrCreate, FileAccess.Write);
                try
                {
                    //PageInfo pageInfo = this.View.artBoard.DataContext as PageInfo;
                    exportPageInfo.Contents.Clear();//이전에 저장된 Page 정보 삭제.
                    foreach (FrameworkElement item in this.CurrentDesignContainer.PageDesignCanvasBox.Children)
                    {
                        if (InSysBasicControls.InSysControlManager.GetControlType(item.GetType().Name) == InSysControlType.None)
                            continue;

                        TouchContentInfo contentInfo = new TouchContentInfo();

                        contentInfo.ContentType = item.GetType();
                        contentInfo.ContentPosition = new Point(Canvas.GetLeft(item), Canvas.GetTop(item));
                        contentInfo.ContentSize = new Size(item.Width, item.Height);
                        contentInfo.ContentObject = item;
                        contentInfo.DesignElementProperties = InSysBasicControls.InSysControlManager.SerializePropertiesInIDesignElement((item as IDesignElement).Properties);
                        contentInfo.ContentName = (item as IDesignElement).Name;

                        exportPageInfo.Contents.Add(contentInfo);
                    }
                    BinaryFormatter binFormatter = new BinaryFormatter();
                    binFormatter.Serialize(flStream, exportPageInfo);
                }
                finally
                {
                    flStream.Close();
                }
            }
             */ 
        }

        public void ImportPage()
        {
           
        }

        public IEnumerable<TouchPageInfo> GetSameLayerPageInfo(TouchPageInfo pageInfo)
        {
            var sameLayerIndexList = this.touchProjectInfo.PageInfoList.Where(o => o.PageLayerIndex.Equals(pageInfo.PageLayerIndex) == true && o.PageGuid.Equals(pageInfo.PageGuid) == false);

            return sameLayerIndexList;
        }

        public void ReleaseAllAdornerOnDesignerCanvas()
        {
            this.CurrentDesignContainer.releaseAllAdorner();
        }

        public void PreviewProject()
        {
            this.IsPreviewPlaying = true;
            List<TouchPageInfo> pages = this.touchProjectInfo.PageInfoList;
            foreach (var pageInfo in pages)
            {
                DisplayCanvas displayCanvas = new DisplayCanvas();
                DesignContainer container = this.GetDesignContainer(pageInfo.PageGuid);
                container.PageDesignCanvasBox.ClonePreviewDisplayCanvasProperties(displayCanvas);
                
                this.previewDisplayManager.AddPageViewVM(displayCanvas, pageInfo);
                InsertAudioBoxControlInDisplayView(pageInfo, displayCanvas);
            }
            this.previewDisplayManager.TouchProjectFilePath = this.currentProjectPath;
            this.previewDisplayManager.PreviewProject(App.Current.MainWindow, null, false);
        }

        public void PreviewPage()
        {
            this.IsPreviewPlaying = true;

            PageCanvas cloneZoomCanvas = new PageCanvas();
            DisplayCanvas displayCanvas = new DisplayCanvas();

            this.CurrentDesignContainer.PageDesignCanvasBox.ClonePreviewDisplayCanvasProperties(displayCanvas);
            //cloneZoomCanvas = this.CurrentDesignContainer.PageDesignCanvasBox.CloneNewPage();
            InsertAudioBoxControlInDisplayView(this.CurrentDesignContainer.TouchPageInfo, displayCanvas);
            this.previewDisplayManager.TouchProjectFilePath = this.currentProjectPath;
            this.previewDisplayManager.PreviewPage(App.Current.MainWindow, displayCanvas, this.CurrentDesignContainer.TouchPageInfo);

        }
               
        private void InsertAudioBoxControlInDisplayView(TouchPageInfo pageInfo, DisplayCanvas displayCanvas)
        {
            try
            {
                var audioList = pageInfo.Contents.Where(o => o.ContentType.Name.Equals("InSysAudioBox") == true).Select(o => o.ContentObject);
                if (audioList != null && audioList.Count() > 0)
                {
                    foreach (var audio in audioList)
                    {
                        FrameworkElement destAudio = InSysDSDisplayer.DSDisplayManager.CreateNewInSysControl(audio.GetType().Name) as FrameworkElement;
                        if (destAudio != null)
                        {
                            audio.CloneInterfaceProperties(destAudio, "IDesignElement");
                            destAudio.Visibility = Visibility.Collapsed;
                            destAudio.Width = 0;
                            destAudio.Height = 0;
                            displayCanvas.Children.Add(destAudio);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
                throw ex;
            }
        }

        public TouchPageInfo InsertTouchPage(DesignContainer designContainer)
        {
            try
            {
                //#region DesignContainer 생성
                //DesignContainer designContainer = this.mainWindow.CreateNewTouchPage(new Guid(pageInfo.PageGuid), new Guid(pageInfo.ParentPageGuid), width, height, pagelifetime, pagename, pageInfo.PageLayerIndex);
                //#endregion                
               
                foreach (TouchContentInfo item in designContainer.TouchPageInfo.Contents)
                {
                    FrameworkElement inSysContentControl = InSysBasicControls.InSysControlManager.GetNewSerializationInSysControl(item.ContentType, item.DesignElementProperties, ViewState.Design) as FrameworkElement;

                    if (inSysContentControl != null)
                    {
                        IDesignElement designElement = inSysContentControl as IDesignElement;
                        if (designElement != null)
                        {
                            item.ContentObject = inSysContentControl;

                            #region ContentControl 타입에 따라 Insert 하는 위치 구분.
                            switch (designElement.InSysControlType)
                            {
                                case InSysControlType.InSysAudioBox:
                                    designContainer.PageAudioBox.Children.Add(inSysContentControl);
                                    break;
                                default:
                                    designContainer.PageDesignCanvasBox.Children.Add(inSysContentControl);
                                    designContainer.RaiseInsertContentEvent(designElement);
                                    break;
                            }
                            #endregion //ContentControl 타입에 따라 Insert 하는 위치 구분.

                            #region Playlist의 경로 정보를 가상경로(CONTENTSVALUE)를 이용하여 Opne Project시 절대경로(Content)로 변경한다.
                            if (item.DesignElementProperties.ContainsKey("Playlist") == true)
                            {
                                List<PlaylistItem> playlistItems = (List<PlaylistItem>)item.DesignElementProperties["Playlist"];
                                //foreach (var playlistitem in playlistItems)
                                //{
                                //    FileInfo projectFileinfo = new FileInfo(playlistitem.Content);
                                //    if (projectFileinfo.Exists == true)
                                //    {
                                //        string fullpath = string.Format("{0}\\{1}", projectFileinfo.Directory.FullName, playlistitem.CONTENTSVALUE);
                                //        playlistitem.Content = string.Format(@"{0}", fullpath);
                                //    }
                                //}

                                this.CreatePlaylistItem(designContainer.TouchPageInfo.PageGuid, designElement, playlistItems);
                                designElement.UpdatePlaylistItems();
                            }
                            #endregion

                            if (item.DesignElementProperties.Keys.Contains("ActionEvent") == true)
                                designElement.SetActionEvent(item.DesignElementProperties["ActionEvent"]);//이벤트 설정

                            #region  Timeline 정보 설정.

                            double startTiem = designElement.StartTime.TotalSeconds;
                            double endTiem = designElement.EndTime.TotalSeconds;
                            if (designElement == null)
                            {
                                startTiem = 0;
                                endTiem = designContainer.TouchPageInfo.PageLifeTimeTemp;
                            }

                            #endregion //Timeline 정보 설정.

                        }
                    }
                }
                //designContainer.setTouchPageInfo(pageInfo);
                //this.touchProjectInfo.AddTouchPageInfo(pageInfo);

                #region Copy, Cut 할 때 사용하는 TouchPageInfo 객체 저장.
                FlowPageItem flowpageitem = this.mainWindow.pageflowview.FindFlowPageItem(designContainer.TouchPageInfo.PageGuid);
                flowpageitem.PageInfoData = designContainer.TouchPageInfo;
                flowpageitem.RaiseSelectFlowPageItemEvent(null);
                #endregion

                return designContainer.TouchPageInfo;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }
        }

        private string GetFileName(string path)
        {
            int index = path.LastIndexOf("\\");
            if (index == -1)
                return path;

            return path.Substring(index + 1, path.Length - (index + 1));
        }

        public void Alignment_ContentElement(Alignment alignment)
        {
            if (alignment == Alignment.Top)            
                this.CurrentDesignContainer.PageDesignCanvasBox.AlignTop(this.CurrentDesignContainer.SelectedContentItems);
            else if (alignment == Alignment.Bottom)
                this.CurrentDesignContainer.PageDesignCanvasBox.AlignBottom(this.CurrentDesignContainer.SelectedContentItems);
            else if (alignment == Alignment.VerticalCenter)
                this.CurrentDesignContainer.PageDesignCanvasBox.AlignVerticalCenter(this.CurrentDesignContainer.SelectedContentItems);
            else if (alignment == Alignment.Left)
                this.CurrentDesignContainer.PageDesignCanvasBox.AlignLeft(this.CurrentDesignContainer.SelectedContentItems);
            else if (alignment == Alignment.Right)
                this.CurrentDesignContainer.PageDesignCanvasBox.AlignRight(this.CurrentDesignContainer.SelectedContentItems);
            else if (alignment == Alignment.HorizontalCenter)
                this.CurrentDesignContainer.PageDesignCanvasBox.AlignHorizontalCenter(this.CurrentDesignContainer.SelectedContentItems);
        }

        public void FitToPage_ContentElement(FitToPage fittiopage)
        {
            if (fittiopage == FitToPage.Full)
            {
                this.CurrentDesignContainer.PageDesignCanvasBox.DistributeHorizontal(this.CurrentDesignContainer.SelectedContentItems);
                this.CurrentDesignContainer.PageDesignCanvasBox.DistributeVertical(this.CurrentDesignContainer.SelectedContentItems);
            }
            else if (fittiopage == FitToPage.Horizontal)
                this.CurrentDesignContainer.PageDesignCanvasBox.DistributeHorizontal(this.CurrentDesignContainer.SelectedContentItems);
            else if (fittiopage == FitToPage.Vertical)
                this.CurrentDesignContainer.PageDesignCanvasBox.DistributeVertical(this.CurrentDesignContainer.SelectedContentItems);
        }

        public void InitProject()
        {
            this.pageTabItems.Clear();
            this.mainWindow.pageflowview.CloseFlowView();
            this.timelineData.ContentItemBoxDataList.Clear();
            this.timelineData.TimelineContentsDictionary.Clear();
            this.mainWindow.TouchDesignTabControl.Items.Clear();
            this.touchProjectInfo.PageInfoList.Clear();
            this.currentProjectPath = "";
            this.mainWindow.timelinebody.Init_Timeline();

            InSysControlManager.GetTouchProjectInfoHandler = () =>
            {
                return this.TouchProjectInfo;
            };
        }
    }  
}
