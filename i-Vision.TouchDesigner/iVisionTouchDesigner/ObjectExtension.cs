﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using InSysDSDisplayer;
using System.Reflection;
using DesignWorkCanvas.Views;
using InSysDSDisplayer.Views;

namespace iVisionTouchDesigner
{
    public static class ObjectExtension
    {
        public static PageCanvas CloneNewPage(this PageCanvas origin)
        {
            PageCanvas destination = new PageCanvas();
            PropertyInfo[] propertyInfos = origin.GetType().GetProperties();
            EventInfo eventInfo = origin.GetType().GetEvent("InsertControlContent");
            EventInfo eventInfo2 = destination.GetType().GetEvent("InsertControlContent");

            eventInfo2 = eventInfo;

            try
            {

                foreach (var destinationProperty in propertyInfos)
                {
                    if (origin != null && destinationProperty.CanWrite)
                    {
                        origin.GetType().GetProperties().Where(x => x.CanRead && (x.Name == destinationProperty.Name && x.PropertyType == destinationProperty.PropertyType)).ToList()
                            .ForEach(x => destinationProperty.SetValue(destination, x.GetValue(origin, null), null));
                    }
                }

                List<FrameworkElement> feList = new List<FrameworkElement>();
                foreach (var child in origin.Children)
                {
                    if (child is FrameworkElement)
                        feList.Add(child as FrameworkElement);
                }

                for (int i = 0; i < feList.Count; i++)
                {
                    FrameworkElement item = feList[i];
                    FrameworkElement destItem = DSDisplayManager.CreateNewInSysControl(item.GetType().Name) as FrameworkElement;
                    if (destItem == null) continue;
                    item.CloneInterfaceProperties(destItem, "IDesignElement");

                    double left = Canvas.GetLeft(item);
                    double top = Canvas.GetTop(item);
                    destination.Children.Add(destItem);
                    Canvas.SetLeft(destItem, left);
                    Canvas.SetTop(destItem, top);
                }

                //foreach (FrameworkElement item in origin.Children)
                //{
                //    FrameworkElement destItem = DSDisplayManager.CreateNewInSysControl(item.GetType().Name) as FrameworkElement;
                //    if (destItem == null) continue;
                //    item.CloneInterfaceProperties(destItem, "IDesignElement");

                //    double left = Canvas.GetLeft(item);
                //    double top = Canvas.GetTop(item);
                //    destination.Children.Add(destItem);
                //    Canvas.SetLeft(destItem, left);
                //    Canvas.SetTop(destItem, top);
                //}
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            return destination;
        }

        public static List<FrameworkElement> CopyChildren(this PageCanvas origin)
        {
            List<FrameworkElement> items = new List<FrameworkElement>();

            foreach (FrameworkElement item in origin.Children)
            {
                FrameworkElement destItem = DSDisplayManager.CreateNewInSysControl(item.GetType().Name) as FrameworkElement;
                if (destItem == null) continue;
                item.CloneInterfaceProperties(destItem, "IDesignElement");

                double left = Canvas.GetLeft(item);
                double top = Canvas.GetTop(item);
                Canvas.SetLeft(destItem, left);
                Canvas.SetTop(destItem, top);

                items.Add(destItem);
            }

            return items;
        }

        static void destination_SendContentSelected(object sender, RoutedEventArgs e)
        {
            throw new NotImplementedException();
        }

        public static void ClonePreviewDisplayCanvasProperties(this PageCanvas origin, DisplayCanvas destination)
        {
            if (destination == null)
                throw new ArgumentNullException("destination", "Destination object must first be instantiated.");

            PropertyInfo[] propertyInfos = destination.GetType().GetProperties();

            foreach (var destinationProperty in propertyInfos)
            {
                if (destinationProperty.PropertyType.Name.Equals("Transform") == true)
                    continue;
                if (origin != null && destinationProperty.CanWrite)
                {
                    origin.GetType().GetProperties().Where(x => x.CanRead && (x.Name == destinationProperty.Name && x.PropertyType == destinationProperty.PropertyType)).ToList()
                        .ForEach(x => destinationProperty.SetValue(destination, x.GetValue(origin, null), null));
                }
            }

            destination.Children.Clear();// Add하기전에 제거한다.
            
            List<FrameworkElement> children = new List<FrameworkElement>();
            foreach(var child in origin.Children)
            {
                 if( child is FrameworkElement)
                     children.Add(child as FrameworkElement);
            }
            for (int i = 0; i < children.Count; i++)
            {
                FrameworkElement oldFrameworkElement = children[i];
                FrameworkElement newFrameworkElement = DSDisplayManager.CreateNewInSysControl(oldFrameworkElement.GetType().Name) as FrameworkElement;
                if (newFrameworkElement == null)
                    continue;

                oldFrameworkElement.CloneInterfaceProperties(newFrameworkElement, "IDesignElement");

                double left = Canvas.GetLeft(oldFrameworkElement);
                double top = Canvas.GetTop(oldFrameworkElement);
                destination.Children.Add(newFrameworkElement);
                Canvas.SetLeft(newFrameworkElement, left);
                Canvas.SetTop(newFrameworkElement, top);
            }
            //foreach (FrameworkElement item in origin.Children)
            //{
            //    FrameworkElement destItem = DSDisplayManager.CreateNewInSysControl(item.GetType().Name) as FrameworkElement;
            //    if (destItem == null) continue;
            //    item.CloneInterfaceProperties(destItem, "IDesignElement");

            //    double left = Canvas.GetLeft(item);
            //    double top = Canvas.GetTop(item);
            //    destination.Children.Add(destItem);
            //    Canvas.SetLeft(destItem, left);
            //    Canvas.SetTop(destItem, top);

            //    //destination.Width = item.Width;
            //    //destination.Height = item.Height;
            //}
        }

        public static void UpdateCanvasProperties(this PageCanvas origin, DisplayCanvas destination)
        {
            try
            {
                if (destination == null)
                    throw new ArgumentNullException("destination", "Destination object must first be instantiated.");

                PropertyInfo[] propertyInfos = destination.GetType().GetProperties();

                foreach (var destinationProperty in propertyInfos)
                {
                    if (destinationProperty.PropertyType.Name.Equals("Transform") == true)
                        continue;
                    if (origin != null && destinationProperty.CanWrite)
                    {
                        origin.GetType().GetProperties().Where(x => x.CanRead && (x.Name == destinationProperty.Name && x.PropertyType == destinationProperty.PropertyType)).ToList()
                            .ForEach(x => destinationProperty.SetValue(destination, x.GetValue(origin, null), null));
                    }
                }

                destination.Children.Clear();// Add하기전에 제거한다.           

                foreach (FrameworkElement item in origin.Children)
                {
                    FrameworkElement destItem = DSDisplayManager.CreateNewInSysControl(item.GetType().Name) as FrameworkElement;
                    if (destItem == null) continue;
                    item.CloneInterfaceProperties(destItem, "IDesignElement");

                    double left = Canvas.GetLeft(item);
                    double top = Canvas.GetTop(item);
                    destination.Children.Add(destItem);
                    Canvas.SetLeft(destItem, left);
                    Canvas.SetTop(destItem, top);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        public static void CloneZoomCanvasProperties(this PageCanvas origin, PageCanvas destination)
        {
            if (destination == null)
                throw new ArgumentNullException("destination", "Destination object must first be instantiated.");

            PropertyInfo[] propertyInfos = destination.GetType().GetProperties();

            foreach (var destinationProperty in propertyInfos)
            {
                if (destinationProperty.PropertyType.Name.Equals("Transform") == true)
                    continue;
                if (origin != null && destinationProperty.CanWrite)
                {
                    origin.GetType().GetProperties().Where(x => x.CanRead && (x.Name == destinationProperty.Name && x.PropertyType == destinationProperty.PropertyType)).ToList()
                        .ForEach(x => destinationProperty.SetValue(destination, x.GetValue(origin, null), null));
                }
            }

            destination.Children.Clear();// Add하기전에 제거한다.

            List<FrameworkElement> children = new List<FrameworkElement>();
            foreach (var child in origin.Children)
            {
                if (child is FrameworkElement)
                    children.Add(child as FrameworkElement);
            }
            for (int i = 0; i < children.Count; i++)
            {
                FrameworkElement fe = children[i];
                FrameworkElement destItem = DSDisplayManager.CreateNewInSysControl(fe.GetType().Name) as FrameworkElement;
                if (destItem == null) continue;
                fe.CloneInterfaceProperties(destItem, "IDesignElement");

                double left = Canvas.GetLeft(fe);
                double top = Canvas.GetTop(fe);
                destination.Children.Add(destItem);
                Canvas.SetLeft(destItem, left);
                Canvas.SetTop(destItem, top);
            }
        }

        public static void CloneProperties(this FrameworkElement origin, FrameworkElement destination)
        {
            if (destination == null)
                throw new ArgumentNullException("destination", "Destination object must first be instantiated.");

            foreach (var destinationProperty in destination.GetType().GetProperties())
            {
                if (origin != null && destinationProperty.CanWrite)
                {

                    origin.GetType().GetProperties().Where(x => x.CanRead && (x.Name == destinationProperty.Name && x.PropertyType == destinationProperty.PropertyType)).ToList()
                        .ForEach(x => destinationProperty.SetValue(destination, x.GetValue(origin, null), null));
                }
            }
        }

        public static void CloneInterfaceProperties(this FrameworkElement origin, FrameworkElement destination, string interfaceName)
        {
            if (destination == null)
            {
                throw new ArgumentNullException("destination", "Destination object must first be instantiated.");
            }

            Type type = destination.GetType().GetInterface(interfaceName);
            if (type != null)
            {
                foreach (var destinationProperty in type.GetProperties())
                {
                    if (origin != null && destinationProperty.CanWrite)
                    {
                        try
                        {
                            var originProperty = origin.GetType().GetInterface(interfaceName).GetProperties().Where(x => x.CanRead && (x.Name == destinationProperty.Name && x.PropertyType == destinationProperty.PropertyType)).FirstOrDefault();


                            switch (destinationProperty.Name)
                            {
                                case "Opacity":
                                    destination.Opacity = (double)originProperty.GetValue(origin, null);
                                    break;
                                case "BorderBrush":
                                    if (destination is UserControl)
                                    {
                                        (destination as UserControl).BorderBrush = null;
                                    }
                                    break;
                                case "BorderThickness":
                                    if (destination is UserControl)
                                    {
                                        (destination as UserControl).BorderThickness = new Thickness();
                                    }
                                    break;
                                case "Visibility":
                                    destination.Visibility = (Visibility)originProperty.GetValue(origin, null);
                                    break;
                                case "IsApplyLifeTime":
                                    DSDisplayManager.SetProperty(destination, origin, destinationProperty.Name);
                                    break;
                                case "Properties":
                                    DSDisplayManager.CopyProperties(destination, origin);
                                    break;
                                default:
                                    destinationProperty.SetValue(destination, originProperty.GetValue(origin, null), null);
                                    break;
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message + ex.StackTrace);
                            continue;
                        }
                    }
                }
            }
        }
    }
}
