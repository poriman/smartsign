﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace iVisionTouchDesigner
{
    /// <summary>
    /// Interaction logic for TestCanvas.xaml
    /// </summary>
    public partial class TestCanvas : Canvas
    {
        public TestCanvas()
        {
            InitializeComponent();
        }

        public void ChangeImage(string imagename)
        {
            string strUri2 = String.Format(@"\images\{0}", imagename);
            image1.Source = new BitmapImage(new Uri(strUri2, UriKind.Relative));           
        }
    }
}
