﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;
using System.Globalization;
using System.Threading;
using System.Text.RegularExpressions;
using NLog.Targets;
using NLog;


namespace iVisionTouchDesigner
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
            //string strlanguage = ConfigurationManager.AppSettings.Get("LANGUAGE");
            //if (strlanguage.Equals("0"))
                //Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            //else if (strlanguage.Equals("1"))
                Thread.CurrentThread.CurrentCulture = new CultureInfo("ko-KR");

                InitLog();
        }

        private void InitLog()
        {
            Logger logger = LogManager.GetLogger("TouchDesignerLog");
            string logFolder = AppDomain.CurrentDomain.BaseDirectory + "TouchDesignerLog";
            FileTarget target = new FileTarget();
            target.Layout = "${longdate}\t[${level}]\t[${callsite}]\t${message}";
            target.FileName = logFolder + @"\" + "TouchDesignerLog.txt";
            target.ArchiveFileName = logFolder + @"\" + "archives/logdb.{#####}.txt";
            target.MaxArchiveFiles = 31;
            target.ArchiveEvery = FileArchivePeriod.Day;

            target.ArchiveNumbering = ArchiveNumberingMode.Sequence;
            target.ConcurrentWrites = true;

            NLog.Config.SimpleConfigurator.ConfigureForTargetLogging(target, LogLevel.Debug);

            logger.Info("TouchDesigner start");           
        } 
    }
}
