﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace iVisionTouchDesigner.Controls
{
    using System.Windows.Controls.Primitives;
    using System.Windows;

    public class PopupControl : Popup
    {
        public bool IsPopupOpen
        {
            get
            {
                return this.IsOpen;
            }
            set
            {
                this.IsOpen = value;
                //if (this.IsOpen == true)
                //    Application.Current.MainWindow.IsHitTestVisible = false;
                //else
                //    Application.Current.MainWindow.IsHitTestVisible = true;

            }
        }
    }
}
