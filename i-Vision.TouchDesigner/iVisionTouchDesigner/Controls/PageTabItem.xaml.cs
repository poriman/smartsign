﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;

namespace iVisionTouchDesigner.Controls
{
    /// <summary>
    /// Interaction logic for PageTabItem.xaml
    /// </summary>
    public partial class PageTabItem : TabItem
    {
        Rectangle leftRectangle;
        Rectangle rightRectangle;
        public bool IsPageClosed = false;
       
        private string headerText
        {
            get;
            set;
        }


        public string HeaderText
        {
            get
            {
                return this.headerText;
            }

            set
            {
                this.headerText = value;
                SetPageNameToolTip(value);
                if (this.headerText.Length > 18)
                    this.Header = this.headerText.Substring(0, 16) + "...";
                else
                    this.Header = this.headerText;
                
            }
        }

        public PageTabItem()
        {
            InitializeComponent();

            this.Loaded += (s, e) =>
            {
                IsPageClosed = false;
            };
        }

        //public void SetPageTabHeader(string pageName)
        //{
        //    if (string.IsNullOrEmpty(this.PageName) != true)
        //    {
        //        if (this.PageName.Equals(pageName) == true)
        //            return;
        //    }

        //    this.PageName = pageName;
        //    if (TextBlockDesignerName != null)
        //    {
        //        TextBlockDesignerName.Text = this.DisplayText;
        //        SetPageNameToolTip(pageName);
        //    }
        //}

        public void SetPageNameToolTip(string displayName)
        {
            if (this.ToolTip == null)
            {
                ToolTip tooltip = new ToolTip();
                tooltip.Content = displayName;
                this.ToolTip = tooltip;
            }
            else
            {
                ToolTip tooltip = this.ToolTip as ToolTip;
                if (tooltip != null)
                {
                    tooltip.Content = displayName;
                }
            }

            //if (this.ToolTip == null)
            //{
            //    ToolTip tooltip = new ToolTip();
            //    tooltip.Content = displayName;
            //    this.ToolTip = tooltip;
            //}
            //else
            //{
            //    ToolTip tooltip = this.ToolTip as ToolTip;
            //    if (tooltip != null)
            //    {
            //        tooltip.Content = displayName;
            //    }
            //}
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            PageTabControl parentTabControl = this.Parent as PageTabControl;
            if (parentTabControl != null)
            {
                parentTabControl.RemoveTabItem(this);
            }
        }
     
        public void SetLeftRectangle()
        {
            this.leftRectangle.Visibility = System.Windows.Visibility.Visible;
        }

        public void ReleaseLeftRectangle()
        {
            this.leftRectangle.Visibility = System.Windows.Visibility.Collapsed;
        }

        public void SetRightRectangle()
        {
            this.rightRectangle.Visibility = System.Windows.Visibility.Visible;
        }

        public void ReleaseRightRectangle()
        {
            this.rightRectangle.Visibility = System.Windows.Visibility.Collapsed;
        }

        private void rightRectangle_Loaded(object sender, RoutedEventArgs e)
        {
            rightRectangle = sender as Rectangle;
            this.rightRectangle.Visibility = System.Windows.Visibility.Collapsed;
        }

        private void leftRectangle_Loaded(object sender, RoutedEventArgs e)
        {
            leftRectangle = sender as Rectangle;
            this.leftRectangle.Visibility = System.Windows.Visibility.Collapsed;
        }

        private void CloseButton_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            IsPageClosed = true;
        }

        private void ArrangeMenu_Click(object sender, RoutedEventArgs e)
        {
            TabControl parentTabControl = this.Parent as TabControl;
            if (parentTabControl != null)
            {
                List<PageTabItem> tabitems = new List<PageTabItem>();
                foreach (var item in parentTabControl.Items)
                    tabitems.Add(item as PageTabItem);

                parentTabControl.Items.Clear();
                var list = tabitems.OrderBy(o => o.Header);

                list.ToList().ForEach(o => parentTabControl.Items.Add(o));

            }
        }

        private void AllCloseMenu_Click(object sender, RoutedEventArgs e)
        {
            TabControl parentTabControl = this.Parent as TabControl;
            if (parentTabControl != null)
            {
                parentTabControl.Items.Clear();
            }
        }

        private void CloseAllButThis_Click(object sender, RoutedEventArgs e)
        {
            TabControl parentTabControl = this.Parent as TabControl;
            if (parentTabControl != null)
            {
                List<PageTabItem> tabitems = new List<PageTabItem>();
                foreach (var item in parentTabControl.Items)
                {
                    if(this.Equals(item) == false)
                        tabitems.Add(item as PageTabItem);
                }

                tabitems.ForEach(o => parentTabControl.Items.Remove(o));
            }
        }    
    }
}
