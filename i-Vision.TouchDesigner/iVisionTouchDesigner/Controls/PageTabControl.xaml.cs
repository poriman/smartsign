﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml.Linq;
using DesignWorkCanvas.Views;
using System.Collections.ObjectModel;
using System.ComponentModel;
using UtilLib.StaticMethod;

namespace iVisionTouchDesigner.Controls
{   
    /// <summary>
    /// Interaction logic for PageTabControl.xaml
    /// </summary>
    public partial class PageTabControl : TabControl
    {
        private double space = 100;
        private bool isPressedState = false;
        public ObservableCollection<PageData> PageDataList 
        {
            get;
            set;
        }
       
        public PageTabControl()
        {
            InitializeComponent();
            PageDataList = new ObservableCollection<PageData>();          
            this.DataContext = this;
        }

        protected override void OnPreviewMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            base.OnPreviewMouseLeftButtonDown(e);

            this.AllowDrop = true;
        }

        protected override void OnPreviewMouseRightButtonUp(MouseButtonEventArgs e)
        {
            base.OnPreviewMouseRightButtonUp(e);

            this.AllowDrop = false;
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnPreviewMouseMove(e);

            if (e.LeftButton == MouseButtonState.Pressed && e.Source.GetType() == typeof(PageTabItem))
            {
                PageTabItem tabitem = this.SelectedItem as PageTabItem;
                if (tabitem.IsPageClosed == false)
                {
                    DragDrop.DoDragDrop(tabitem, tabitem, DragDropEffects.Move);
                    this.AllowDrop = true;
                }
            }
        }

        protected override void OnPreviewDragOver(DragEventArgs e)
        {
            base.OnPreviewDragOver(e);

            PageTabItem sourceTabItem = e.Source as PageTabItem;
            if (sourceTabItem == null || sourceTabItem.GetType() != typeof(PageTabItem))
            {                
                return;
            }

            PageTabItem moveTabItem = e.Data.GetData(typeof(PageTabItem)) as PageTabItem;
            if (moveTabItem == null || moveTabItem.GetType() != typeof(PageTabItem))
            {
                return;
            }

            if (sourceTabItem.Equals(moveTabItem) == true)
            {
                return;
            }
            else
            {
                int sourceIndex = this.Items.IndexOf(sourceTabItem);
                int moveIndex = this.Items.IndexOf(moveTabItem);

                foreach (var obj in this.Items)
                {
                    PageTabItem tabitem = obj as PageTabItem;
                    if (sourceTabItem.Equals(tabitem) == false)
                    {
                        tabitem.ReleaseLeftRectangle();
                        tabitem.ReleaseRightRectangle();
                    }
                }

                if (moveIndex < sourceIndex)
                {
                    sourceTabItem.SetRightRectangle();
                }
                else
                {
                    sourceTabItem.SetLeftRectangle();
                }
            }
        }

        protected override void OnPreviewDragEnter(DragEventArgs e)
        {
            this.AllowDrop = true;
            base.OnPreviewDragEnter(e);
        }

        protected override void OnPreviewDragLeave(DragEventArgs e)
        {
            base.OnPreviewDragLeave(e);
        }

        protected override void OnPreviewDrop(DragEventArgs e)
        {
            base.OnPreviewDrop(e);

            PageTabItem sourceTabItem = e.Source as PageTabItem;

            if (sourceTabItem== null || sourceTabItem.GetType() != typeof(PageTabItem))
                return;

            PageTabItem moveTabItem = e.Data.GetData(typeof(PageTabItem)) as PageTabItem;

            if (sourceTabItem.Equals(moveTabItem) == true)
                return;

            int sourceIndex = this.Items.IndexOf(sourceTabItem);
            int moveIndex = this.Items.IndexOf(moveTabItem);
            if (moveIndex < sourceIndex)
            {
                this.Items.Remove(moveTabItem);
                this.Items.Insert(sourceIndex, moveTabItem);
            }
            else
            {
                this.Items.Remove(moveTabItem);
                this.Items.Insert(sourceIndex, moveTabItem);
            }

            foreach (var obj in this.Items)
            {
                PageTabItem tabitem = obj as PageTabItem;
                tabitem.ReleaseLeftRectangle();
                tabitem.ReleaseRightRectangle();
            }
            
        }
        
        public void RemoveTabItem(string pageid)
        {
            PageTabItem deleteTabItem = null;
            foreach (var t_item in this.Items)
            {
                PageTabItem pagetabitem = t_item as PageTabItem;
                DesignContainer container = pagetabitem.Content as DesignContainer;
                if (container.TouchPageInfo.PageGuid.Equals(pageid) == true)
                {
                    deleteTabItem = pagetabitem;
                    break ;
                }
            }

            if (deleteTabItem != null)
            {
                this.RemoveTabItem(deleteTabItem);
            }
        }

        public void RemoveAll()
        {
            this.Items.Clear();
        }

        public void RemoveTabItem(PageTabItem pagetabitem)
        {
            if (this.Items.Contains(pagetabitem) == true)
            {
                var delItem = PageDataList.Where(o=>o.PageItem.Equals(pagetabitem) == true).FirstOrDefault();
                if (delItem != null)
                    PageDataList.Remove(delItem);

                this.Items.Remove(pagetabitem);

                ComboBox combobox = UIHelper.FindChild<ComboBox>(this, "PageListComboBox");
                if (combobox != null)
                    combobox.SelectedItem = null;
            }
        }

        public void AddTabItem(PageTabItem item)
        {
            if (item != null)
            {
                //ComboBox combobox = UIHelper.FindChild<ComboBox>(this, "PageListComboBox");
                if (PageDataList.Where(o => o.PageItem.Equals(item)).FirstOrDefault() == null)
                {
                    PageData pagedata = new PageData();
                    pagedata.Text = item.HeaderText;
                    pagedata.PageItem = item;
                    PageDataList.Add(pagedata);
                }

                if (this.Items.Contains(item) == false)
                    this.Items.Add(item);

                //if (IsInsertPageTabItem() == true)
                //{
                //}
            }
        }

        public void ChangeTabItemName(PageTabItem pagetabitem)
        {
            var tabitem = PageDataList.Where(o => o.PageItem.Equals(pagetabitem) == true).FirstOrDefault();
            if (tabitem != null)
            {
                tabitem.Text = pagetabitem.HeaderText;
            }
        }

        public void IsSelectedPageTabItem(PageTabItem pagetabitem, bool isSelected)
        {
            pagetabitem.IsSelected = isSelected;

             //ComboBox combobox = UIHelper.FindChild<ComboBox>(this, "PageListComboBox");
             //if (combobox != null)
             //{
             //    if (isSelected == true)
             //        combobox.SelectedItem = null;
             //}
        }

        private bool IsInsertPageTabItem()
        {
            double tatalTabControlWidth = this.ActualWidth;
            double width = 0;           
            foreach (var tabitem in this.Items)
            {
                PageTabItem ptab = tabitem as PageTabItem;
                width += ptab.ActualWidth;
            }

            if (tatalTabControlWidth - space <= width)
            {
                return false;
            }
            return true;
        }

        private void PageListComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox combobox = sender as ComboBox;
            PageTabItem sel_pagetabitem = null;
            PageData pagedata = combobox.SelectedItem as PageData;
            List<PageTabItem> pagetabitemlist = new List<PageTabItem>();
            if (pagedata != null)
            {
                if (pagedata.PageItem != null)
                {
                    foreach (var tabitem in this.Items)
                    {
                        PageTabItem ptab = tabitem as PageTabItem;
                        if (ptab != null)
                            pagetabitemlist.Add(ptab);                        
                    }
                    sel_pagetabitem = pagetabitemlist.Where(o => o.Equals(pagedata.PageItem) == true).FirstOrDefault();

                    if (sel_pagetabitem != null)
                    {
                        sel_pagetabitem.IsSelected = true;
                    }
                    else
                    {
                        if (pagetabitemlist.Count != 0)//디자인 뷰에 PageTabItem이 있을 경우 처리
                        {
                            PageTabItem del_pagetabitem = pagetabitemlist[pagetabitemlist.Count - 1];
                            if (del_pagetabitem != null)
                            {
                                if (IsInsertPageTabItem() == false)
                                    this.RemoveTabItem(del_pagetabitem);
                            }

                            this.Items.Insert(0, pagedata.PageItem);
                            pagedata.PageItem.IsSelected = true;
                        }
                        else//디자인 뷰에 PageTabItem이 모두 Close되었을 경우 처리
                        {
                        }
                    }
                }
            }
        }

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            PageTabItem sel_pagetabitem = (sender as PageTabControl).SelectedItem as PageTabItem;
            if (sel_pagetabitem != null)
            {
                ComboBox combobox = UIHelper.FindChild<ComboBox>(this, "PageListComboBox");
                if (combobox != null)
                {
                    var pagetabitem = PageDataList.Where(o => o.PageItem.Equals(sel_pagetabitem) == true).FirstOrDefault();
                    if (pagetabitem != null)
                    {
                        combobox.SelectedItem = pagetabitem;
                    }
                }
            }
        }       
    }

    public class PageData : INotifyPropertyChanged
    {
        private string text;
        public string Text 
        {
            get { return text; }
            set { this.text = value; OnPropertyChanged("Text"); }
        }
        public PageTabItem PageItem { get; set; }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        public virtual void OnPropertyChanged(params string[] propertyNames)
        {
            if (PropertyChanged == null)
                return;

            foreach (string propertyName in propertyNames)
            {
                try
                {
                    PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
                }
                catch { }
            }
        }

        #endregion
    }
}
