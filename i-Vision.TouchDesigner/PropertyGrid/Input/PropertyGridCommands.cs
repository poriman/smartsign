﻿using System.Windows.Threading;
using System.Windows.Input;

namespace DenisVuyka.Controls.PropertyGrid
{
  /// <summary>
  /// Provides a standard set of property grid related commands.
  /// </summary>
  public static class PropertyGridCommands
  {
    #region Fields
    private static Dispatcher dispatcher; 
    #endregion

    #region ctor
    /// <summary>
    /// Initializes the <see cref="PropertyGridCommands"/> class.
    /// </summary>
    static PropertyGridCommands()
    {
      dispatcher = Dispatcher.CurrentDispatcher;
    } 
    #endregion

    #region Commands

    private static readonly RoutedUICommand _ShowFilter = new RoutedUICommand("Show Filter", "ShowFilter", typeof(PropertyGridCommands));
    private static readonly RoutedUICommand _HideFilter = new RoutedUICommand("Hide Filter", "HideFilter", typeof(PropertyGridCommands));
    private static readonly RoutedUICommand _ToggleFilter = new RoutedUICommand("Toggle Filter", "ToggleFilter", typeof(PropertyGridCommands));
    private static readonly RoutedUICommand _ResetFilter = new RoutedUICommand("Reset Filter", "ResetFilter", typeof(PropertyGridCommands));
    private static readonly RoutedUICommand _Reload = new RoutedUICommand("Reload", "Reload", typeof(PropertyGridCommands));
    private static readonly RoutedUICommand _CollapseCategories = new RoutedUICommand("Collapse Categories", "CollapseCategories", typeof(PropertyGridCommands));
    private static readonly RoutedUICommand _ExpandCategories = new RoutedUICommand("Expand Categories", "ExpandCategories", typeof(PropertyGridCommands));
    private static readonly RoutedUICommand _ToggleCategories = new RoutedUICommand("Toggle Categories", "ToggleCategories", typeof(PropertyGridCommands));
    private static readonly RoutedUICommand _ShowReadOnlyProperties = new RoutedUICommand("Show Read-Only Properties", "ShowReadOnlyProperties", typeof(PropertyGridCommands));
    private static readonly RoutedUICommand _HideReadOnlyProperties = new RoutedUICommand("Hide Read-Only Properties", "HideReadOnlyProperties", typeof(PropertyGridCommands));
    private static readonly RoutedUICommand _ToggleReadOnlyProperties = new RoutedUICommand("Toggle Read-Only Properties", "ToggleReadOnlyProperties", typeof(PropertyGridCommands));
    private static readonly RoutedUICommand _ShowAttachedProperties = new RoutedUICommand("Show Attached Properties", "ShowAttachedProperties", typeof(PropertyGridCommands));
    private static readonly RoutedUICommand _HideAttachedProperties = new RoutedUICommand("Hide Attached Properties", "HideAttachedProperties", typeof(PropertyGridCommands));
    private static readonly RoutedUICommand _ToggleAttachedProperties = new RoutedUICommand("Toggle Attached Properties", "ToggleAttachedProperties", typeof(PropertyGridCommands));

    /// <summary>
    /// Represents a command for the control to show property filter box.
    /// </summary>
    public static RoutedUICommand ShowFilter { get { return _ShowFilter; } }

    /// <summary>
    /// Represents a command for the control to hide property filter box.
    /// </summary>
    public static RoutedUICommand HideFilter { get { return _HideFilter; } }

    /// <summary>
    /// Represents a command for the control to toggle visibility of property filter box.
    /// </summary>
    public static RoutedUICommand ToggleFilter { get { return _ToggleFilter; } }

    /// <summary>
    /// Represents a command that resets current grid filter.
    /// </summary>
    public static RoutedUICommand ResetFilter { get { return _ResetFilter; } }    

    /// <summary>
    /// Represents a command that reloads current grid properties.
    /// </summary>
    public static RoutedUICommand Reload { get { return _Reload; } }

    /// <summary>
    /// Represents a command that collapses all property categories.
    /// </summary>
    public static RoutedUICommand CollapseCategories { get { return _CollapseCategories; } }

    /// <summary>
    /// Represents a command that expands all property categories.
    /// </summary>
    public static RoutedUICommand ExpandCategories { get { return _ExpandCategories; } }

    /// <summary>
    /// Represents a command that toggles all property categories collapsed/expanded state.
    /// </summary>
    public static RoutedUICommand ToggleCategories { get { return _ToggleCategories; } }

    /// <summary>
    /// Represents a command for the control to show all read-only properties.
    /// </summary>
    public static RoutedUICommand ShowReadOnlyProperties { get { return _ShowReadOnlyProperties; } }

    /// <summary>
    /// Represetns a command for the control to hide all read-only properties.
    /// </summary>
    public static RoutedUICommand HideReadOnlyProperties { get { return _HideReadOnlyProperties; } }

    /// <summary>
    /// Represents a command for the control to toggle visibility of read-only properties.
    /// </summary>
    public static RoutedUICommand ToggleReadOnlyProperties { get { return _ToggleReadOnlyProperties; } }

    /// <summary>
    /// Represents a command for the control to show all attached properties.
    /// </summary>
    public static RoutedUICommand ShowAttachedProperties { get { return _ShowAttachedProperties; } }

    /// <summary>
    /// Represents a command for the control to hide all attached properties.
    /// </summary>
    public static RoutedUICommand HideAttachedProperties { get { return _HideAttachedProperties; } }

    /// <summary>
    /// Represents a command for the control to toggle visibility of attached properties.
    /// </summary>
    public static RoutedUICommand ToggleAttachedProperties { get { return _ToggleAttachedProperties; } }

    #endregion
  }
}
