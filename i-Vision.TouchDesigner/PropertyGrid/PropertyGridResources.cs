﻿namespace DenisVuyka.Controls.PropertyGrid
{
  /// <summary>
  /// Provides component resource keys for styling and templating.
  /// </summary>
  public static class PropertyGridResources
  {
    /// <summary>Identifies the Foreground brush component resource key.</summary>
    public static readonly string Foreground = "Foreground";
    /// <summary>Identifies the DisabledForeground brush component resource key.</summary>
    public static readonly string DisabledForeground = "DisabledForeground";
    /// <summary>Identifies the Background brush component resource key.</summary>
    public static readonly string Background = "Background";
    /// <summary>Identifies the SunkenBackground brush component resource key.</summary>
    public static readonly string SunkenBackground = "SunkenBackground";
    /// <summary>Identifies the DisabledSunkenBackground brush component resource key.</summary>
    public static readonly string DisabledSunkenBackground = "DisabledSunkenBackground";
    /// <summary>Identifies the Highlight brush component resource key.</summary>
    public static readonly string Highlight = "Highlight";
    /// <summary>Identifies the DisabledHighlight brush component resource key.</summary>
    public static readonly string DisabledHighlight = "DisabledHighlight";
    /// <summary>Identifies the RaisedHighlight brush component resource key.</summary>
    public static readonly string RaisedHighlight = "RaisedHighlight";
    /// <summary>Identifies the Shadow brush component resource key.</summary>
    public static readonly string Shadow = "Shadow";
    /// <summary>Identifies the RaisedShadow brush component resource key.</summary>
    public static readonly string RaisedShadow = "RaisedShadow";
    /// <summary>Identifies the DisabledShadow brush component resource key.</summary>
    public static readonly string DisabledShadow = "DisabledShadow";

    public static readonly string Checked = "Checked";

    public static readonly string HeaderBorderBrush = "HeaderBorderBrush";
    public static readonly string HeaderBackgroundBrush = "HeaderBackgroundBrush";
    public static readonly string PropertiesBackgroundBrush = "PropertiesBackgroundBrush";
    public static readonly string PropertyGridTitle = "PropertyGridTitle";
    public static readonly string PropertyGridBackground = "PropertyGridBackground";

    public static readonly string BorderDisabled = "BorderDisabled";

    public static readonly string CommonBackground = "CommonBackground";
    public static readonly string CommonDisabled = "CommonDisabled";

    #region Classes

    public static readonly string PropertyGrid = "PropertyGrid";
    public static readonly string CheckBox = "CheckBox";
    public static readonly string TextBox = "TextBox";
    public static readonly string TextBlock = "TextBlock";
    public static readonly string Button = "Button";

    public static readonly string TabControl = "TabControl";
    public static readonly string TabItem = "TabItem";
   
    public static readonly string ScrollBar = "ScrollBar";
    public static readonly string VerticalScrollBarTemplate = "VerticalScrollBarTemplate";
    public static readonly string VerticalScrollBarThumb = "VerticalScrollBarThumb";
    public static readonly string HorizontalScrollBarTemplate = "HorizontalScrollBarTemplate";
    public static readonly string HorizontalScrollBarThumb = "HorizontalScrollBarThumb";
    public static readonly string ScrollBarPageButton = "ScrollBarPageButton";

    public static readonly string ScrollViewer = "ScrollViewer";

    public static readonly string Slider = "Slider";
    public static readonly string HorizontalSliderTemplate = "HorizontalSliderTemplate";
    public static readonly string SliderButtonStyle = "SliderButtonStyle";
    public static readonly string SliderThumbStyle = "SliderThumbStyle";

    public static readonly string ToggleButton = "ToggleButton";

    public static readonly string Expander = "Expander";
    public static readonly string ExpanderBackgroundBrush = "ExpanderBackgroundBrush";
    public static readonly string ExpanderForegroundBrush = "ExpanderForegroundBrush";

    public static readonly string ComboBox = "ComboBox";
    public static readonly string ComboBoxToggleButton = "ComboBoxToggleButton";
    public static readonly string ComboBoxTextBox = "ComboBoxTextBox";
    public static readonly string ComboBoxItem = "ComboBoxItem";

    public static readonly string Alignment = "Alignment";

    public static readonly string UserControl = "UserControl";

    #endregion

    public static readonly string MouseOverStoryboard = "MouseOverStoryboard";
  }
}
