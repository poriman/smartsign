﻿using System;
using System.Diagnostics;
using System.Windows.Media;

namespace DenisVuyka.Controls.PropertyGrid
{
  /// <summary>
  /// Provides basic information for named colors.
  /// </summary>
  public class NamedColor
  {
    #region Properties
    /// <summary>
    /// Gets or sets the name.
    /// </summary>
    /// <value>The name.</value>
    public String Name { get; private set; }

    /// <summary>
    /// Gets or sets the color.
    /// </summary>
    /// <value>The color.</value>
    public Color Color { get; private set; }

    /// <summary>
    /// Gets or sets the brush.
    /// </summary>
    /// <value>The brush.</value>
    public Brush Brush { get; private set; } 
    #endregion

    #region ctor
    /// <summary>
    /// Initializes a new instance of the <see cref="NamedColor"/> class.
    /// </summary>
    /// <param name="name">The name.</param>
    /// <param name="color">The color.</param>
    public NamedColor(String name, Color color)
    {
      Debug.Assert(color != null);
      this.Name = name;
      this.Color = color;
      this.Brush = new SolidColorBrush(color);
    } 
    #endregion

    #region Overrides
    /// <summary>
    /// Returns a <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
    /// </summary>
    /// <returns>
    /// A <see cref="T:System.String"/> that represents the current <see cref="T:System.Object"/>.
    /// </returns>
    public override String ToString()
    {
      return Name;
    } 
    #endregion
  }
}
