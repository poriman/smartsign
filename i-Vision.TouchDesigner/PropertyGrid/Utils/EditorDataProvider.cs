﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;
using System.Windows;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;

namespace DenisVuyka.Controls.PropertyGrid
{
  /// <summary>
  /// Provides data required by different UI editors.
  /// </summary>
  public static class EditorDataProvider
  {
    /// <summary>
    /// Represents an {x:Null} value.
    /// </summary>
    public const string NullValueString = "{x:Null}";

    #region Fields
    private static ICollection<FontStyle> _defaultFontStyles;
    private static ICollection<FontWeight> _defaultFontWeights;
    private static ICollection<FontStretch> _defaultFontStretches;
    private static ICollection<string> _defaultCursorNames;
    private static ICollection<NamedColor> _defaultColorNames;
    private static ICollection<XmlLanguage> _defaultLanguages;
    #endregion

    #region Properties
    /// <summary>
    /// Gets the default font styles.
    /// </summary>
    /// <value>The default font styles.</value>
    public static ICollection<FontStyle> DefaultFontStyles
    {
      get 
      {
        if (_defaultFontStyles == null)
          _defaultFontStyles = GetDefaultFontStyles();

        return _defaultFontStyles; 
      }
    }

    /// <summary>
    /// Gets the default font weights.
    /// </summary>
    /// <value>The default font weights.</value>
    public static ICollection<FontWeight> DefaultFontWeights
    {
      get 
      { 
        if (_defaultFontWeights == null)
          _defaultFontWeights = GetDefaultFontWeights();

        return _defaultFontWeights; 
      }
    }

    /// <summary>
    /// Gets the default font stretches.
    /// </summary>
    /// <value>The default font stretches.</value>
    public static ICollection<FontStretch> DefaultFontStretches
    {
      get 
      {
        if (_defaultFontStretches == null)
          _defaultFontStretches = GetDefaultFontStretches();

        return _defaultFontStretches; 
      }
    }

    /// <summary>
    /// Gets the default cursor names.
    /// </summary>
    /// <value>The default cursor names.</value>
    public static ICollection<string> DefaultCursorNames
    {
      get 
      { 
        if (_defaultCursorNames == null)
          _defaultCursorNames = GetDefaultCursorNames();

        return _defaultCursorNames; 
      }
    }

    /// <summary>
    /// Gets the default color names.
    /// </summary>
    /// <value>The default color names.</value>
    public static ICollection<NamedColor> DefaultColorNames
    {
      get 
      {
        if (_defaultColorNames == null)
          _defaultColorNames = GetDefaultColorNames();

        return _defaultColorNames; 
      }
    }

    /// <summary>
    /// Gets the default languages.
    /// </summary>
    /// <value>The default languages.</value>
    public static ICollection<XmlLanguage> DefaultLanguages
    {
      get 
      {
        if (_defaultLanguages == null)
          _defaultLanguages = GetDefaultLanguages();

        return _defaultLanguages; 
      }
    }
    #endregion

    #region Implementation
    private static ICollection<FontStyle> GetDefaultFontStyles()
    {
      return new FontStyle[]
      {
        FontStyles.Italic,
        FontStyles.Normal,
        FontStyles.Oblique        
      };
    }

    private static ICollection<FontWeight> GetDefaultFontWeights()
    {
      return new FontWeight[]
      {
        FontWeights.Light,
        FontWeights.Normal,
        FontWeights.Medium,
        FontWeights.Thin,
        FontWeights.ExtraLight,
        FontWeights.SemiBold,
        FontWeights.Bold,
        FontWeights.ExtraBold,
        FontWeights.Black,
        FontWeights.ExtraBlack
      };
    }

    private static ICollection<FontStretch> GetDefaultFontStretches()
    {
      return new FontStretch[]
      {
        FontStretches.UltraCondensed,
        FontStretches.ExtraCondensed,
        FontStretches.Condensed,
        FontStretches.SemiCondensed,
        FontStretches.Normal,
        FontStretches.SemiExpanded,
        FontStretches.Expanded,
        FontStretches.ExtraExpanded,        
        FontStretches.UltraExpanded
      };
    }

    private static ICollection<string> GetDefaultCursorNames()
    {
      List<string> names = new List<string>(Enum.GetNames(typeof(CursorType)));
      names.Add(NullValueString);
      return names;
    }

    // http://shevaspace.spaces.live.com/blog/cns!FD9A0F1F8DD06954!435.entry
    private static ICollection<NamedColor> GetDefaultColorNames()
    {
      List<NamedColor> names = new List<NamedColor>();

      NamedColor nc;
      MethodAttributes inclusiveAttributes = MethodAttributes.Static | MethodAttributes.Public;

      foreach (PropertyInfo pi in typeof(Colors).GetProperties())
      {
        if (pi.PropertyType == typeof(Color))
        {
          MethodInfo mi = pi.GetGetMethod();
          if ((mi != null) && ((mi.Attributes & inclusiveAttributes) == inclusiveAttributes))
          {
            nc = new NamedColor(pi.Name, (Color)pi.GetValue(null, null));
            names.Add(nc);
          }
        }
      }

      return names;
    }
    
    private static ICollection<XmlLanguage> GetDefaultLanguages()
    {
      List<XmlLanguage> results = new List<XmlLanguage>();
      foreach (CultureInfo ci in CultureInfo.GetCultures(CultureTypes.AllCultures))
      {
        if (string.IsNullOrEmpty(ci.IetfLanguageTag)) continue;
        results.Add(XmlLanguage.GetLanguage(ci.IetfLanguageTag));
      }
      return results;
    }
    #endregion
  }
}
