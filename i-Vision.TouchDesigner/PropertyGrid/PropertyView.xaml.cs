﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DenisVuyka.Controls.PropertyGrid.Data;

namespace DenisVuyka.Controls.PropertyGrid
{
    /// <summary>
    /// Interaction logic for PropertyView.xaml
    /// </summary>
    public partial class PropertyView : UserControl
    {
        private readonly static DependencyProperty PropertyInstanceProperty = DependencyProperty.Register("PropertyInstance", typeof(object), typeof(PropertyView), new PropertyMetadata(null, new PropertyChangedCallback(PropertyChanged_PropertyInstance)));

        public object PropertyInstance
        {
            get { return (object)GetValue(PropertyInstanceProperty); }
            set { SetValue(PropertyInstanceProperty, value); }
        }

        public PropertyView()
        {
            InitializeComponent();
        }

        private static void PropertyChanged_PropertyInstance(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            PropertyView z = d as PropertyView;
            if (z != null)
                z.PropertyChanged_PropertyInstance(e);
        }

        private void PropertyChanged_PropertyInstance(DependencyPropertyChangedEventArgs e)
        {
            Type type = e.NewValue.GetType();
            this.propertyGrid.SelectedObject = e.NewValue;//임시

            //InSysBasicControl.Interfaces.IElementProperties property = e.NewValue as InSysBasicControl.Interfaces.IElementProperties;
            //if (property != null)
            //{
            //    this.propertyGrid.Instance = property.ElementProperties;
            //}
        }

        public ComboBox GetComboBoxOfArrayProperty(string propertyName)
        {
            this.UpdateLayout();

            PropertyItem name = this.propertyGrid.Properties
              .OfType<PropertyItem>()
              .Where(property => property.PropertyName.Equals(propertyName) == true)
              .FirstOrDefault();

            if (name == null)
            {
                string msg = string.Format("Property '{0}Property' not found!", propertyName);
                MessageBox.Show(msg);
                return null;
            }

            ComboBox comboBox = this.propertyGrid.TryFindEditor(name) as ComboBox;

            if (comboBox == null)
            {
                MessageBox.Show("ComboBox not found!");
                return null;
            }
            return comboBox;
        }

        private void ShowPropertyCategoryItem(string categoryName)
        {
            CategoryItem category = propertyGrid.Categories.OfType<CategoryItem>().Where(item => (item.Name == categoryName)).FirstOrDefault();

            ItemsControl categoriesList = this.propertyGrid.Template.FindName("PART_items", propertyGrid) as ItemsControl;
            ContentPresenter categoryPresenter = categoriesList.ItemContainerGenerator.ContainerFromItem(category) as ContentPresenter;
            Expander categoryExpander = VisualTreeHelper.GetChild(categoryPresenter, 0) as Expander;
            categoryExpander.Visibility = System.Windows.Visibility.Visible;
        }

        public object GetPropertyGridObject()
        {
            return this.propertyGrid;
        }
    }

    public class ArrayPropertyUI : Control
    {
        static ArrayPropertyUI()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ArrayPropertyUI), new FrameworkPropertyMetadata(typeof(ArrayPropertyUI)));
        }
    }
}
