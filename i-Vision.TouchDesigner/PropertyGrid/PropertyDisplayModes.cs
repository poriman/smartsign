﻿namespace DenisVuyka.Controls.PropertyGrid
{
  /// <summary>
  /// Defines modes for property visualization.
  /// </summary>
  public enum PropertyDisplayModes
  {
    /// <summary>
    /// Show all properties.
    /// </summary>
    All,
    /// <summary>
    /// Show dependency properties only.
    /// </summary>
    Dependency,
    /// <summary>
    /// Show native CLR properties only.
    /// </summary>
    Native
  }
}
