﻿using System;
using System.Windows;
using System.Windows.Controls;
using DenisVuyka.Controls.PropertyGrid.Data;

namespace DenisVuyka.Controls.PropertyGrid
{
  /// <summary>
  /// Provides a custom way to choose a System.Windows.DataTemplate based on the data object and the data-bound element.
  /// </summary>
  public class PropertyGridTemplateSelector : DataTemplateSelector
  {
    /// <summary>
    /// When overridden in a derived class, returns a <see cref="T:System.Windows.DataTemplate"/> based on custom logic.
    /// </summary>
    /// <param name="item">The data object for which to select the template.</param>
    /// <param name="container">The data-bound object.</param>
    /// <returns>
    /// Returns a <see cref="T:System.Windows.DataTemplate"/> or null. The default value is null.
    /// </returns>
    public override DataTemplate SelectTemplate(object item, DependencyObject container)
    {
      PropertyItem property = item as PropertyItem;
      if (property == null)
        throw new ArgumentException("item must be of type Property");

      // Examine the PropertyEditor attribute definition
      PropertyEditorAttribute editor = (PropertyEditorAttribute)property.PropertyDescriptor.Attributes[typeof(PropertyEditorAttribute)];
      if (editor != null)
      {        
        FrameworkElementFactory factory = new FrameworkElementFactory(editor.EditorType);

        DataTemplate dataTemplate = new DataTemplate();
        dataTemplate.VisualTree = factory;

        return dataTemplate;
      }

      FrameworkElement element = container as FrameworkElement;
      if (element == null)
        return base.SelectTemplate(property.PropertyValue, container);

      DataTemplate template = FindDataTemplate(property, element);
      return template;      
    }

    private static DataTemplate FindDataTemplate(PropertyItem property, FrameworkElement element)
    {
      Type propertyType = property.PropertyType;

      DataTemplate template = TryFindDataTemplate(element, propertyType);

      while (template == null && propertyType.BaseType != null)
      {
        propertyType = propertyType.BaseType;
        template = TryFindDataTemplate(element, propertyType);
      }

      if (template == null)
        template = TryFindDataTemplate(element, property.PropertyName);

      if (template == null)
        template = TryFindDataTemplate(element, "default");

      return template;
    }

    private static DataTemplate TryFindDataTemplate(FrameworkElement element, object dataTemplateKey)
    {
      object dataTemplate = element.TryFindResource(dataTemplateKey);
      if (dataTemplate == null)
      {
        dataTemplateKey = new ComponentResourceKey(typeof(PropertyGrid), dataTemplateKey);
        dataTemplate = element.TryFindResource(dataTemplateKey);
      }
      return dataTemplate as DataTemplate;
    }
  }
}
