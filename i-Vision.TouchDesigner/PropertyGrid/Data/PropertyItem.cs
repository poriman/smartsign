﻿using System;
using System.ComponentModel;
using System.Windows;
using DenisVuyka.Controls.PropertyGrid.Attributes;

namespace DenisVuyka.Controls.PropertyGrid.Data
{
    /// <summary>
    /// Represents a wrapper around dependency property
    /// </summary>
    public class PropertyItem : GridItemBase
    {
        #region Fields

        private readonly object component;
        private readonly PropertyDescriptor descriptor;

        #endregion

        #region ctor

        /// <summary>
        /// Initializes a new instance of the <see cref="PropertyItem"/> class.
        /// </summary>
        /// <param name="component">The component.</param>
        /// <param name="descriptor">The descriptor.</param>
        public PropertyItem(object component, PropertyDescriptor descriptor)
        {
            if (component == null) throw new ArgumentNullException("component");
            if (descriptor == null) throw new ArgumentNullException("descriptor");

            this.component = component;
            this.descriptor = descriptor;

            //<<--thinkblue_20100628: PropertyOrder Setting
            PropertyOrderAttribute editor = (PropertyOrderAttribute)descriptor.Attributes[typeof(PropertyOrderAttribute)];
           
            if (editor == null)
                this.propertyOrder = 0;
            else
                this.propertyOrder = editor.Order;
            //-->>

            this.descriptor.AddValueChanged(component, Component_PropertyChanged);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PropertyItem"/> class.
        /// </summary>
        /// <param name="component">The component.</param>
        /// <param name="property">The property.</param>
        public PropertyItem(object component, DependencyProperty property)
            : this(component, DependencyPropertyDescriptor.FromProperty(property, component.GetType()))
        {            
        }
        #endregion

        #region Events

        public event Action<PropertyItem, object> PropertyValueChanged;

        private void OnPropertyValueChanged(object oldValue)
        {
            Action<PropertyItem, object> handler = PropertyValueChanged;
            if (handler != null)
                handler(this, oldValue);
        }

        #endregion

        #region Properties

        #region Value property

        /// <summary>
        /// Gets or sets the value of the property.
        /// </summary>
        /// <value>The value of the property.</value>
        public object PropertyValue
        {
            get
            {
                return GetPropertyValueCore(component);
            }
            set
            {
                object oldValue = GetPropertyValueCore(component);
                if (value != null && value.Equals(oldValue))
                {
                    return;
                }
                Type propertyType = descriptor.PropertyType;
                if (propertyType == typeof(object) ||
                  value == null && propertyType.IsClass ||
                  value != null && propertyType.IsAssignableFrom(value.GetType()))
                {
                    SetPropertyValueCore(component, value);
                }
                else
                {
                    try
                    {
                        TypeConverter converter = TypeDescriptor.GetConverter(descriptor.PropertyType);
                        object convertedValue = converter.ConvertFrom(value);
                        SetPropertyValueCore(component, convertedValue);
                    }
                    catch { }
                }

                OnPropertyValueChanged(oldValue);
                NotifyPropertyChanged("PropertyValue");
            }
        }
        #endregion

        #region PropertyLabel

        /// <summary>
        /// Gets the property label.
        /// </summary>
        /// <value>The property label.</value>
        public string PropertyLabel
        {
            get { return descriptor.DisplayName; }
        }

        #endregion

        #region PropertyOrder

        private int propertyOrder;
        /// <summary>
        /// Gets the property label.
        /// </summary>
        /// <value>The property label.</value>
        public int PropertyOrder
        {
            get { return propertyOrder; }
            set { propertyOrder = value; }
        }

        #endregion

        #region PropertyName
        /// <summary>
        /// Gets the property name.
        /// </summary>
        /// <value>The name of the property.</value>
        public string PropertyName
        {
            get
            {
                if (this.descriptor != null)
                    return descriptor.Name;
                return null;
            }
        }
        #endregion

        #region PropertyDescriptor

        /// <summary>
        /// Gets the property descriptor.
        /// </summary>
        /// <value>The property descriptor.</value>
        public PropertyDescriptor PropertyDescriptor
        {
            get { return descriptor; }
        }

        #endregion

        #region Description
        /// <summary>
        /// Gets the description.
        /// </summary>
        /// <value>The description.</value>
        public string Description
        {
            get { return descriptor.Description; }
        }
        #endregion

        #region HasDescription

        /// <summary>
        /// Gets a value indicating whether this instance has description.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance has description; otherwise, <c>false</c>.
        /// </value>
        public bool HasDescription
        {
            get { return !string.IsNullOrEmpty(Description); }
        }

        #endregion

        #region IsWriteable
        /// <summary>
        /// Gets a value indicating whether property is writeable.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if property is writeable; otherwise, <c>false</c>.
        /// </value>
        public bool IsWriteable
        {
            get { return !descriptor.IsReadOnly; }
        }
        #endregion

        #region IsReadOnly
        /// <summary>
        /// Gets a value indicating whether property is read only.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if property is read only; otherwise, <c>false</c>.
        /// </value>
        public bool IsReadOnly
        {
            get { return descriptor.IsReadOnly; }
        }
        #endregion

        #region IsLabelVisible

        private bool isLabelVisible = true;
        public bool IsLabelVisible
        {
            get { return isLabelVisible; }
            set 
            { 
                this.isLabelVisible = value; 
                if (value == true)
                    propertyLabelWidth = "0.35*";
                else
                    propertyLabelWidth = "Auto";

                OnPropertyValueChanged("IsLabelVisible");
            }
        }
        #endregion

        #region IsLabelVisible

        private string propertyLabelWidth = "0.35*";
        public string PropertyLabelWidth
        {
            get { return propertyLabelWidth; }
            set { this.propertyLabelWidth = value; OnPropertyValueChanged("PropertyLabelWidth"); }
        }
        #endregion

        #region PropertyType
        /// <summary>
        /// Gets the type of the property.
        /// </summary>
        /// <value>The type of the property.</value>
        public Type PropertyType
        {
            get { return descriptor.PropertyType; }
        }
        #endregion

        #region Category

        /// <summary>
        /// Gets the category.
        /// </summary>
        /// <value>The category.</value>
        public string Category
        {
            get
            {
                string result = this.descriptor.Category;
                if (!string.IsNullOrEmpty(result))
                    return result;
                else
                    return CategoryAttribute.Default.Category;
            }
        }
        #endregion

        #region CanResetValue
        /// <summary>
        /// Gets a value indicating whether this property value can be reset to default value.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if the value can be reset to default; otherwise, <c>false</c>.
        /// </value>
        public bool CanResetValue
        {
            get { return descriptor.CanResetValue(component); }
        }
        #endregion

        #endregion

        #region Event Handlers
        /// <summary>
        /// Handles the PropertyChanged event of the Component control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        private void Component_PropertyChanged(object sender, EventArgs e)
        {
            NotifyPropertyChanged("PropertyValue");
        }

        #endregion

        #region Public API

        /// <summary>
        /// Resets the property value to default value.
        /// </summary>
        public void ResetValue()
        {
            if (descriptor.CanResetValue(component))
            {
                object oldValue = GetPropertyValueCore(component);
                descriptor.ResetValue(component);
                OnPropertyValueChanged(oldValue);
                NotifyPropertyChanged("PropertyValue");
            }
        }

        #endregion

        #region IDisposable Members
        /// <summary>
        /// Releases unmanaged and - optionally - managed resources
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected override void Dispose(bool disposing)
        {
            if (Disposed)
                return;

            if (disposing)
                descriptor.RemoveValueChanged(component, Component_PropertyChanged);

            base.Dispose(disposing);
        }
        #endregion

        internal object GetPropertyValueCore(object target)
        {
            object value;
            if (this.descriptor == null) return null;

            if (target is ICustomTypeDescriptor)
                target = ((ICustomTypeDescriptor)target).GetPropertyOwner(descriptor);

            try
            {
                value = this.descriptor.GetValue(target);
            }
            catch
            {
                throw;
            }
            return value;
        }

        internal void SetPropertyValueCore(object target, object value)
        {
            if (this.descriptor == null) return;

            object component = target;
            if (component is ICustomTypeDescriptor)
                component = ((ICustomTypeDescriptor)component).GetPropertyOwner(this.descriptor);

            if (component != null)
                this.descriptor.SetValue(component, value);
        }
    }
}
