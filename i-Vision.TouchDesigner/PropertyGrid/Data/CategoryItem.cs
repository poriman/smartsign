﻿using System;
using System.ComponentModel;

namespace DenisVuyka.Controls.PropertyGrid.Data
{
  /// <summary>
  /// Represents a property category.
  /// </summary>
  public class CategoryItem : GridItemContainer
  {
    #region Properties

    #region Name property

    private string _Name = CategoryAttribute.Default.Category;
    /// <summary>
    /// Gets or sets the name of the category.
    /// </summary>
    /// <value>The name of the category.</value>
    public string Name
    {
      get { return _Name; }
      set
      {
        _Name = value;
        NotifyPropertyChanged("Name");
      }
    }
    #endregion

    #region IsExpanded
    private bool _IsExpanded = true;
    /// <summary>
    /// Gets or sets a value indicating whether this category is expanded.
    /// </summary>
    /// <value>
    /// 	<c>true</c> if this category is expanded; otherwise, <c>false</c>.
    /// </value>
    public bool IsExpanded
    {
      get { return _IsExpanded; }
      set
      {
        _IsExpanded = value;
        NotifyPropertyChanged("IsExpanded");
      }
    }
    #endregion

    #region Filter property
    /// <summary>
    /// Gets or sets the filter.
    /// </summary>
    /// <value>The filter.</value>
    public Predicate<object> Filter
    {
      get { return ItemsView.Filter; }
      set { ItemsView.Filter = value; }
    }
    #endregion

    #endregion

    #region ctors
    /// <summary>
    /// Initializes a new instance of the <see cref="CategoryItem"/> class.
    /// </summary>
    public CategoryItem()
    {

    }

    /// <summary>
    /// Initializes a new instance of the <see cref="CategoryItem"/> class.
    /// </summary>
    /// <param name="name">The name.</param>
    public CategoryItem(string name)
      : this()
    {
      if (!string.IsNullOrEmpty(name))
        Name = name;
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="CategoryItem"/> class.
    /// </summary>
    /// <param name="name">The name.</param>
    /// <param name="expanded">if set to <c>true</c> the category will be initially expanded.</param>
    public CategoryItem(string name, bool expanded)
      : this(name)
    {
      IsExpanded = expanded;
    }
    #endregion    
  }
}
