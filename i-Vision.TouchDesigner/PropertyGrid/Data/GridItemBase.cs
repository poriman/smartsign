﻿using System;
using System.ComponentModel;
using System.Windows;

namespace DenisVuyka.Controls.PropertyGrid.Data
{
  /// <summary>
  /// Base grid item
  /// </summary>
  public abstract class GridItemBase : IDisposable, INotifyPropertyChanged
  {
    #region IDisposable Members

    private bool _disposed;

    /// <summary>
    /// Gets a value indicating whether this <see cref="GridItemBase"/> is disposed.
    /// </summary>
    /// <value><c>true</c> if disposed; otherwise, <c>false</c>.</value>
    protected bool Disposed
    {
      get { return _disposed; }
    }

    /// <summary>
    /// Releases unmanaged and - optionally - managed resources
    /// </summary>
    /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
    protected virtual void Dispose(bool disposing)
    {
      if (!Disposed)
        _disposed = true;
    }

    /// <summary>
    /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
    /// </summary>
    public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    }

    /// <summary>
    /// Releases unmanaged resources and performs other cleanup operations before the
    /// <see cref="GridItemBase"/> is reclaimed by garbage collection.
    /// </summary>
    ~GridItemBase()
    {
      Dispose(false);
    }

    #endregion

    #region INotifyPropertyChanged Members
    /// <summary>
    /// Occurs when a property value changes.
    /// </summary>
    [field: NonSerialized]
    public event PropertyChangedEventHandler PropertyChanged;

    /// <summary>
    /// Notifies that the property was changed.
    /// </summary>
    /// <param name="propertyName">Name of the property.</param>
    protected void NotifyPropertyChanged(string propertyName)
    {
      PropertyChangedEventHandler handler = PropertyChanged;
      if (handler != null)
        handler(this, new PropertyChangedEventArgs(propertyName));
    }

    #endregion
  }
}
