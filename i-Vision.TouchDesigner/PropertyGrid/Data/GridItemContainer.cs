﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Data;

namespace DenisVuyka.Controls.PropertyGrid.Data
{
  /// <summary>
  /// Base class for composite items
  /// </summary>
  public abstract class GridItemContainer : GridItemBase
  {
    #region Properties

    #region Items property
    private readonly ObservableCollection<GridItemBase> _items = new ObservableCollection<GridItemBase>();
    /// <summary>
    /// Gets the items.
    /// </summary>
    /// <value>The items.</value>
    public ObservableCollection<GridItemBase> Items
    {
      get { return _items; }
    }
    #endregion

    #region ItemsView property
    private ListCollectionView _ItemsView;
    /// <summary>
    /// Gets the items view.
    /// </summary>
    /// <value>The items view.</value>
    public ListCollectionView ItemsView
    {
      get
      {
        if (_ItemsView == null)
          InitializeItemsView();

        return _ItemsView;
      }
    }
    #endregion

    #region HasItems property

    private bool _HasItems = true;

    /// <summary>
    /// Gets a value indicating whether this instance has items.
    /// </summary>
    /// <value><c>true</c> if this instance has items; otherwise, <c>false</c>.</value>    
    public bool HasItems
    {
      get { return _HasItems; }
    }
    #endregion

    #region IsVisible property

    private bool _IsVisible = true;

    /// <summary>
    /// Gets a value indicating whether this instance has items.
    /// </summary>
    /// <value><c>true</c> if this instance has items; otherwise, <c>false</c>.</value>    
    public bool IsVisible
    {
        get { return _IsVisible; }
    }
    #endregion

    #endregion

    #region Private methods

    private void InitializeItemsView()
    {
      _ItemsView = new ListCollectionView(_items);
      //_ItemsView.SortDescriptions.Add(new SortDescription("Name", ListSortDirection.Ascending));//thinkblue_20100628 : 이 부분에 의해 속성 소팅이 안되어 주석처림함.
      _ItemsView.CurrentChanged += new EventHandler(ItemsView_CurrentChanged);

      _IsVisible = false;
      NotifyPropertyChanged("IsVisible");
    }

    private void ItemsView_CurrentChanged(object sender, System.EventArgs e)
    {
      _HasItems = ItemsView != null && !ItemsView.IsEmpty;
      NotifyPropertyChanged("HasItems");
    }

    #endregion

    #region IDisposable Members

    /// <summary>
    /// Releases unmanaged and - optionally - managed resources
    /// </summary>
    /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
    protected override void Dispose(bool disposing)
    {
      if (Disposed)
        return;

      if (disposing)
      {
        foreach (GridItemBase item in Items)
          item.Dispose();
      }
      base.Dispose(disposing);
    }

    #endregion
  }
}
 