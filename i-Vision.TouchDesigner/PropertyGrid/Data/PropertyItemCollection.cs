﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using DenisVuyka.Controls.PropertyGrid.Attributes;

namespace DenisVuyka.Controls.PropertyGrid.Data
{
  /// <summary>
  /// Represents a collection of property items
  /// </summary>
  public class PropertyItemCollection : GridItemContainer
  {
    #region Properties

    public PropertyGrid Owner { get; protected set; }
    public object Component { get; protected set; }

    #endregion

    #region ctor

    /// <summary>
    /// Initializes a new instance of the <see cref="PropertyItemCollection"/> class.
    /// </summary>
    public PropertyItemCollection() : base() { }

    /// <summary>
    /// Initializes a new instance of the <see cref="GridItemCollection"/> class.
    /// </summary>
    /// <param name="owner">The owner.</param>
    /// <param name="component">The component.</param>
    public PropertyItemCollection(PropertyGrid owner, object component)
    {
      if (owner == null) throw new ArgumentNullException("owner");
      if (component == null) throw new ArgumentNullException("component");

      this.Owner = owner;
      this.Component = component;

      InitializeCollection(Owner, Component);
    }

    #endregion

    #region Initialization

    HashSet<string> cache = new HashSet<string>();

    /// <summary>
    /// Initializes the collection.
    /// </summary>
    /// <param name="owner">The owner.</param>
    /// <param name="component">The component.</param>
    protected virtual void InitializeCollection(PropertyGrid owner, object component)
    {
      cache = new HashSet<string>();

      // get property descriptors
      PropertyDescriptorCollection descriptors = TypeDescriptor.GetProperties(component,
        new Attribute[] { new PropertyFilterAttribute(PropertyFilterOptions.SetValues | 
                                                      PropertyFilterOptions.UnsetValues | 
                                                      PropertyFilterOptions.Valid) });


      List<PropertyItem> propertyCollection = new List<PropertyItem>();

      foreach (PropertyDescriptor propertyDescriptor in descriptors)
        CollectProperties(component, propertyDescriptor, propertyCollection);

        propertyCollection = propertyCollection.OrderBy(o=>o.PropertyOrder).ToList();//thinkblue_20100628      

        foreach (PropertyItem property in propertyCollection)
        {
            InitializeProperty(property);
        }

      
      cache.Clear();
    }

    /// <summary>
    /// Initializes the property.
    /// </summary>
    /// <param name="property">The property.</param>
    protected virtual void InitializeProperty(PropertyItem property)
    {      
      if (property == null) throw new ArgumentNullException("property");
      if (cache.Contains(property.PropertyName)) return;

      cache.Add(property.PropertyName);
      Items.Add(property);
    }

    /// <summary>
    /// Collects the properties.
    /// </summary>
    /// <param name="component">The component.</param>
    /// <param name="descriptor">The descriptor.</param>
    /// <param name="propertyList">The property list.</param>
    protected virtual void CollectProperties(object component, PropertyDescriptor descriptor, IList<PropertyItem> propertyList)
    {
      if (descriptor.Attributes[typeof(FlatternHierarchyAttribute)] == null)
      {
        DependencyPropertyDescriptor dpDescriptor = DependencyPropertyDescriptor.FromProperty(descriptor);
        // Provide additional checks for dependency properties
        if (dpDescriptor != null)
        {
          // Check whether dependency properties are not prohibited
          if (Owner.PropertyDisplayMode == PropertyDisplayModes.Native) return;

          // Check whether attached properties are to be displayed
          if (dpDescriptor.IsAttached && !Owner.ShowAttachedProperties) return;
        }
        else
        {
          if (Owner.PropertyDisplayMode == PropertyDisplayModes.Dependency) return;
        }


        // Check whether readonly properties are to be displayed
        if (descriptor.IsReadOnly && !Owner.ShowReadOnlyProperties) return;

        // Check whether property is browsable and add it to the collection
        if (!descriptor.IsBrowsable) return;

        PropertyItem item = new PropertyItem(component, descriptor);

        // Evaluate the schema conditions
        if (Owner.PropertyDisplaySchema != null && !Owner.PropertyDisplaySchema.Evaluate(item)) return;

        propertyList.Add(item);
      }
      else
      {
        component = descriptor.GetValue(component);
        PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(component);
        foreach (PropertyDescriptor propertyDescriptor in properties)
          CollectProperties(component, propertyDescriptor, propertyList);
      }
    }

    #endregion
  }
}
