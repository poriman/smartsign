﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Data;

namespace DenisVuyka.Controls.PropertyGrid.Data
{
  /// <summary>
  /// Represents a collection of property item categories.
  /// </summary>
  public class CategoryItemCollection : PropertyItemCollection
  {
    #region Properties

    #region Categories property
    private readonly ObservableCollection<GridItemBase> _Categories = new ObservableCollection<GridItemBase>();
    
    /// <summary>
    /// Gets the categories.
    /// </summary>
    /// <value>The categories.</value>
    public ObservableCollection<GridItemBase> Categories
    {
      get { return _Categories; }
    }
    #endregion

    #region CategoriesView property

    private ListCollectionView _CategoriesView;

    /// <summary>
    /// Gets the categories view.
    /// </summary>
    /// <value>The categories view.</value>
    public ListCollectionView CategoriesView
    {
      get
      {
        if (_CategoriesView == null)
          InitializeCategoriesView();

        return _CategoriesView;
      }
    }
    #endregion

    #region HasCategories property

    private bool _HasCategories = true;

    /// <summary>
    /// Gets a value indicating whether this instance has categories.
    /// </summary>
    /// <value>
    /// 	<c>true</c> if this instance has categories; otherwise, <c>false</c>.
    /// </value>
    public bool HasCategories
    {
      get { return _HasCategories; }
    }
    #endregion

    #endregion

    #region ctors

    /// <summary>
    /// Initializes a new instance of the <see cref="CategoryItemCollection"/> class.
    /// </summary>
    public CategoryItemCollection() : base() { }

    /// <summary>
    /// Initializes a new instance of the <see cref="CategoryItemCollection"/> class.
    /// </summary>
    /// <param name="owner">The owner.</param>
    /// <param name="component">The component.</param>
    public CategoryItemCollection(PropertyGrid owner, object component) : base(owner, component) { }

    #endregion

    #region Initialization

    Dictionary<string, CategoryItem> cache = new Dictionary<string, CategoryItem>();

    /// <summary>
    /// Initializes the collection.
    /// </summary>
    /// <param name="owner">The owner.</param>
    /// <param name="component">The component.</param>
    protected override void InitializeCollection(PropertyGrid owner, object component)
    {
      cache = new Dictionary<string, CategoryItem>();
      base.InitializeCollection(owner, component);
      cache.Clear();
    }

    /// <summary>
    /// Initializes the property.
    /// </summary>
    /// <param name="property">The property.</param>
    protected override void InitializeProperty(PropertyItem property)
    {
      base.InitializeProperty(property);

      CategoryItem propertyCategory;
      
      // Get existing or create a new category
      if (cache.ContainsKey(property.Category))
        propertyCategory = cache[property.Category];
      else
      {
        propertyCategory = new CategoryItem(property.Category, Owner.ExpandCategories);
        cache[property.Category] = propertyCategory;
        _Categories.Add(propertyCategory);
      }
      // Add property to the appropriate category
      propertyCategory.Items.Add(property);
    }
    
    #endregion

    #region Private methods

    private void InitializeCategoriesView()
    {
      _CategoriesView = new ListCollectionView(_Categories);
      _CategoriesView.SortDescriptions.Add(new SortDescription("Name", ListSortDirection.Ascending));
      _CategoriesView.CurrentChanged += new EventHandler(ItemsView_CurrentChanged);
    }

    private void ItemsView_CurrentChanged(object sender, System.EventArgs e)
    {
      _HasCategories = ItemsView != null && !ItemsView.IsEmpty;
      NotifyPropertyChanged("HasItems");
    }

    #endregion
  }
}
