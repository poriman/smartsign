﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows;

namespace DenisVuyka.Controls.PropertyGrid
{
    public class ComboBoxEditor : ComboBox
    {
        static ComboBoxEditor()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ComboBoxEditor), new FrameworkPropertyMetadata(typeof(ComboBoxEditor)));
        }
    }
}
