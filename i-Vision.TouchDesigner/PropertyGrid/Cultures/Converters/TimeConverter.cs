﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace DenisVuyka.Controls.PropertyGrid.Converters
{
    public class TimeConverter : IValueConverter
    {
        #region IValueConverter Members

        /// <summary>
        /// Converter for the calender control to measure the widths to calculate
        /// </summary>
        /// <param name="value">Pass the Actual width of the parent control</param>
        /// <param name="targetType">Target type</param>
        /// <param name="parameter">Pass widthCell to calculate the width a particular cell.
        /// Pass widthCellContainer to calculate the witdth of the parent control</param>
        /// <param name="culture">The current culture in use</param>
        /// <returns>Returns the new width to use</returns>
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            int valuePassed = (int)value;
            return valuePassed.ToString("D2");
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return Binding.DoNothing;
        }

        #endregion
    }
}
