﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Win32;
using DenisVuyka.Controls.PropertyGrid.Editors;

namespace DenisVuyka.Controls.PropertyGrid.Converters
{
    public class URLPropertyValueProvider : IValueProvider
    {
        #region IValueProvider Members

        public object ProvideValue(object component, object value)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();

            if (openFileDialog.ShowDialog(null) == true)
                return openFileDialog.FileName;

            return null;
        }

        #endregion
    }
}
