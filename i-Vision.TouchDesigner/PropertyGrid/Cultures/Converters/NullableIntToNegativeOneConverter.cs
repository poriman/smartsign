﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace DenisVuyka.Controls.PropertyGrid.Converters
{
  /// <summary>
  /// Nullable System.Int32 converter.
  /// </summary>
  [ValueConversion(typeof(int?), typeof(int))]
  public class NullableIntToNegativeOneConverter : IValueConverter
  {
    /// <summary>
    /// Converts a value.
    /// </summary>
    /// <param name="value">The value produced by the binding source.</param>
    /// <param name="targetType">The type of the binding target property.</param>
    /// <param name="parameter">The converter parameter to use.</param>
    /// <param name="culture">The culture to use in the converter.</param>
    /// <returns>
    /// A converted value. If the method returns null, the valid null value is used.
    /// </returns>
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      return (value == null) ? -1 : value;
    }

    /// <summary>
    /// Converts a value.
    /// </summary>
    /// <param name="value">The value that is produced by the binding target.</param>
    /// <param name="targetType">The type to convert to.</param>
    /// <param name="parameter">The converter parameter to use.</param>
    /// <param name="culture">The culture to use in the converter.</param>
    /// <returns>
    /// A converted value. If the method returns null, the valid null value is used.
    /// </returns>
    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      int i;

      if (value == null)
        return null;
      else
      {
        if (int.TryParse(value.ToString(), out i))
          return (i == -1) ? null : value;
        else
          return value;
      }
    }
  }
}
