﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace DenisVuyka.Controls.PropertyGrid.Attributes
{
    public class LocalizedDisplayNameAttribute: DisplayNameAttribute
    {
        public LocalizedDisplayNameAttribute(string resourceId) : base(GetMessageFromResource(resourceId))
        {
        }

        private static string GetMessageFromResource(string resourceId)
        {
            string retValue = resourceId;
            // TODO: Return the string from the resource file
            switch (resourceId)
            {
                case "Width":
                    retValue = Cultures.Resources.Width;
                    break;
                case "Height":
                    retValue = Cultures.Resources.Height;
                    break;
                case "Opacity":
                    retValue = Cultures.Resources.Opacity;
                    break;
                case "Type":
                    retValue = Cultures.Resources.Type;
                    break;
                case "Page Elements":
                    retValue = Cultures.Resources.Elements;
                    break;
                case "Page Name":
                    retValue = Cultures.Resources.PageName;
                    break;
                case "DisplayOrder":
                    retValue = Cultures.Resources.DisplayOrder;
                    break;
                case "Background":
                    retValue = Cultures.Resources.Background;
                    break;
                case "Apply Page Time":
                    retValue = Cultures.Resources.ApplyTime;
                    break;
                case "Page Time":
                    retValue = Cultures.Resources.PageTime;
                    break;
                case "ZIndex":
                    retValue = Cultures.Resources.ZIndex;
                    break;
                case "Alignment":
                    retValue = Cultures.Resources.Alignment;
                    break;
                case "HorizontalAlignment":
                    retValue = Cultures.Resources.HorizontalAlignment;
                    break;
                case "VerticalAlignment":
                    retValue = Cultures.Resources.VerticalAlignment;
                    break;
                case "Fit To Page":
                    retValue = Cultures.Resources.FitToPage;
                    break;
                case "AspectRatio":
                    retValue = Cultures.Resources.AspectRatio;
                    break;
                case "BorderThickness":
                    retValue = Cultures.Resources.BorderThickness;
                    break;
                case "BorderCorner":
                    retValue = Cultures.Resources.BorderCorner;
                    break;
                case "Visibility":
                    retValue = Cultures.Resources.Visibility;
                    break;
                case "HighlightBrightness":
                    retValue = Cultures.Resources.HighlightBrightness;
                    break;
                case "ButtonStyle":
                    retValue = Cultures.Resources.ButtonStyle;
                    break;
                case "X":
                    retValue = Cultures.Resources.X;
                    break;
                case "Y":
                    retValue = Cultures.Resources.Y;
                    break;
                case "Name":
                    retValue = Cultures.Resources.Name;
                    break;
                case "Content":
                    retValue = Cultures.Resources.Content;
                    break;
                case "Playlist":
                    retValue = Cultures.Resources.Playlist;
                    break;
                case "BorderBrush":
                    retValue = Cultures.Resources.BorderBrush;
                    break;
                case "Foreground":
                    retValue = Cultures.Resources.Foreground;
                    break;
                case "FontSize":
                    retValue = Cultures.Resources.FontSize;
                    break;
                case "FontWeight":
                    retValue = Cultures.Resources.FontWeight;
                    break;
                case "FontFamily":
                    retValue = Cultures.Resources.FontFamily;
                    break;
                case "TextWrapping":
                    retValue = Cultures.Resources.TextWrapping;
                    break;
                case "Multiline":
                    retValue = Cultures.Resources.Multiline;
                    break;
                case "Life Time":
                    retValue = Cultures.Resources.LifeTime;
                    break;
                case "Start Time":
                    retValue = Cultures.Resources.StartTime;
                    break;
                case "End Time":
                    retValue = Cultures.Resources.EndTime;
                    break;
                case "Stretch":
                    retValue = Cultures.Resources.Stretch;
                    break;
                case "Mute":
                    retValue = Cultures.Resources.Mute;
                    break;
                case "Volume":
                    retValue = Cultures.Resources.Volume;
                    break;
                case "Action Event":
                    retValue = Cultures.Resources.ActionEvent;
                    break;
                case "Page":
                    retValue = Cultures.Resources.Page;
                    break;
                case "ShowHideType":
                    retValue = Cultures.Resources.ShowHideType;
                    break;
                case "Target":
                    retValue = Cultures.Resources.Target;
                    break;
                case "URL":
                    retValue = Cultures.Resources.URL;
                    break;
                case "File":
                    retValue = Cultures.Resources.File;
                    break;
                case "Page Effect":
                    retValue = Cultures.Resources.PageEffect;
                    break; 
                case "Apply LifeTime":
                    retValue = Cultures.Resources.ApplyLifeTime;
                    break;
                case "Touch Event":
                    retValue = Cultures.Resources.TouchEvent;                    
                    break;
                default:
                    retValue = "";
                    break;
            }

            return retValue;
        }
    }
}
