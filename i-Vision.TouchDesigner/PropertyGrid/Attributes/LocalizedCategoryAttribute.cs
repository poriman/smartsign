﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace DenisVuyka.Controls.PropertyGrid.Attributes
{   
    public class LocalizedCategoryAttribute : CategoryAttribute
    {
        public LocalizedCategoryAttribute(string resourceId)
            : base(GetMessageFromResource(resourceId))
        {
        }

        private static string GetMessageFromResource(string resourceId)
        {
            string retValue = resourceId;
            // TODO: Return the string from the resource file
            switch (resourceId)
            {
                case "Layout":
                    retValue = Cultures.Resources.Layout;
                    break;
                case "Appearance":
                    retValue = Cultures.Resources.Appearance;
                    break;
                case "Common":
                    retValue = Cultures.Resources.Common;
                    break;
                case "Brushes":
                    retValue = Cultures.Resources.Brushes;
                    break;
                case "Time":
                    retValue = Cultures.Resources.Time;
                    break;
                case "AspectRatio":
                    retValue = Cultures.Resources.AspectRatio;
                    break;
                case "Text":
                    retValue = Cultures.Resources.Text;
                    break;
                case "Media":
                    retValue = Cultures.Resources.Media;
                    break;
                case "Event":
                    retValue = Cultures.Resources.Event;
                    break;              
            }

            return retValue;
        }
    }
}
