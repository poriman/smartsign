﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Collections;

namespace DenisVuyka.Controls.PropertyGrid.Attributes
{
    public class PropertySorter : ExpandableObjectConverter
    {
        #region Methods
        public override bool GetPropertiesSupported(ITypeDescriptorContext context)
        {
            return true;
        }

        public override PropertyDescriptorCollection GetProperties(ITypeDescriptorContext context, object value, Attribute[] attributes)
        {
            //
            // This override returns a list of properties in order
            //
            PropertyDescriptorCollection pdc = TypeDescriptor.GetProperties(value, attributes);
            PropertyDescriptor[] properties = new PropertyDescriptor[] { };
            ArrayList orderedProperties = new ArrayList();

            /* property at below used to copy ONLY properties in an specific category */
            PropertyDescriptorCollection props = new PropertyDescriptorCollection(properties);
            foreach (PropertyDescriptor pd in pdc)
            {
                // used to filter for category
                //if (pd.Category == "Smartechnology")
                {
                    Attribute attribute = pd.Attributes[typeof(PropertyOrderAttribute)];
                    if (attribute != null)
                    {
                        //
                        // If the attribute is found, then create an pair object to hold it
                        //


                        PropertyOrderAttribute poa = (PropertyOrderAttribute)attribute;

                        /* You can pass ONLY the PropertyDescriptor.. but..you know.. i'm testing..*/
                        /* This line was adapted from original code */
                        orderedProperties.Add(new PropertyOrderPair(pd.Name, poa.Order, pd.Category, pd));


                    }
                    else
                    {
                        //
                        // If no order attribute is specifed then given it an order of 0
                        //

                        /* You can pass ONLY the PropertyDescriptor.. but..you know.. i'm testing..*/
                        /* This line was adapted from original code */
                        orderedProperties.Add(new PropertyOrderPair(pd.Name, 0, pd.Category, pd));
                    }
                }
            }

            //
            // Perform the actual order using the value PropertyOrderPair classes
            // implementation of IComparable to sort
            //
            orderedProperties.Sort();
            //
            // Build a string list of the ordered names
            //
            ArrayList propertyNames = new ArrayList();
            foreach (PropertyOrderPair pop in orderedProperties)
            {
                /* !!!! The FILTER is implemented here !!!! */
                /* TYPE your OWN category */
                if (pop.Category.ToLower() == "smartechnology")
                {
                    propertyNames.Add(pop.Name);
                    props.Add(pop.PropDescriptor);
                }


            }
            //
            // Pass in the ordered list for the PropertyDescriptorCollection to sort by
            //
            /* We are returning the personalized properties only */
            return props.Sort((string[])propertyNames.ToArray(typeof(string)));
        }
        #endregion
    }

    #region Helper Class - PropertyOrderAttribute
    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
    public class PropertyOrderAttribute : Attribute
    {
        //
        // Simple attribute to allow the order of a property to be specified
        //
        private int _order;
        public PropertyOrderAttribute(int order)
        {
            _order = order;
        }

        public int Order
        {
            get
            {
                return _order;
            }
        }
    }
    #endregion

    #region Helper Class - PropertyOrderPair
    public class PropertyOrderPair : IComparable
    {
        private int _order;
        private string _name;
        /* Following two members added by CFQüeb */
        private string _category;
        private PropertyDescriptor _propDescriptor;
        public string Name
        {
            get
            {
                return _name;
            }
        }
        /* Following property added since original code */
        public string Category
        {
            get
            {
                return _category;
            }
            set
            {
                if (_category == value)
                    return;
                _category = value;
            }
        }
        /* Following property added since original code */
        public PropertyDescriptor PropDescriptor
        {
            get
            {
                return _propDescriptor;
            }
            set
            {
                if (_propDescriptor == value)
                    return;
                _propDescriptor = value;
            }
        }

        /* Modified constructor */
        public PropertyOrderPair(string name, int order, string category, PropertyDescriptor propdesc)
        {
            _order = order;
            _name = name;

            /* next two members were added by CFQüeb */
            _category = category;
            _propDescriptor = propdesc;
        }

        public int CompareTo(object obj)
        {
            //
            // Sort the pair objects by ordering by order value
            // Equal values get the same rank
            //
            int otherOrder = ((PropertyOrderPair)obj)._order;
            if (otherOrder == _order)
            {
                //
                // If order not specified, sort by name
                //
                string otherName = ((PropertyOrderPair)obj)._name;
                return string.Compare(_name, otherName);
            }
            else if (otherOrder > _order)
            {
                return -1;
            }
            return 1;
        }
    }
    #endregion

}
