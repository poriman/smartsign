﻿using System;
using System.Windows;

namespace DenisVuyka.Controls.PropertyGrid
{
  /// <summary>
  /// Specifies the editor to use to change a property. This class cannot be inherited.
  /// </summary>
  [AttributeUsage(AttributeTargets.Property, AllowMultiple = false, Inherited = true)]
  public sealed class PropertyEditorAttribute : Attribute
  {
    private readonly Type editorType;
    /// <summary>
    /// Get the type of the editor to be used.
    /// </summary>
    public Type EditorType
    {
      get { return editorType; }
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="PropertyEditorAttribute"/> class.
    /// </summary>
    /// <param name="editorType">Type of the editor.</param>
    public PropertyEditorAttribute(Type editorType)
    {
      if (editorType == null) throw new ArgumentNullException("editorType");
      if (!(typeof(FrameworkElement).IsAssignableFrom(editorType)))
        throw new NotSupportedException("Only FrameworkElement based types are supported!");

      this.editorType = editorType;      
    }
  }
}
