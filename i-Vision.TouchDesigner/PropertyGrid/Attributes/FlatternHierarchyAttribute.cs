﻿using System;

namespace DenisVuyka.Controls.PropertyGrid
{
  [AttributeUsage(AttributeTargets.Property, Inherited = false, AllowMultiple = false)]
  public sealed class FlatternHierarchyAttribute : Attribute { }
}
