﻿using System.ComponentModel;
using System.Windows;
using DenisVuyka.Controls.PropertyGrid;
using System.Windows.Media;

namespace DenisVuyka.Controls.PropertyGrid.MockData
{

    public class MockDataObject : DependencyObject
    {
        public static readonly DependencyProperty IsEnabledProperty =
          DependencyProperty.Register("IsEnabled", typeof(bool), typeof(MockDataObject), new PropertyMetadata(false));

        public static readonly DependencyProperty HorizontalAlignmentProperty =
          DependencyProperty.Register("HorizontalAlignment", typeof(HorizontalAlignment), typeof(MockDataObject), new PropertyMetadata(HorizontalAlignment.Left));

        public static readonly DependencyProperty VerticalAlignmentProperty =
          DependencyProperty.Register("VerticalAlignment", typeof(VerticalAlignment), typeof(MockDataObject), new PropertyMetadata(VerticalAlignment.Top));

        public static readonly DependencyProperty NameProperty =
          DependencyProperty.Register("Name", typeof(string), typeof(MockDataObject), new PropertyMetadata("Unknown"));

        public static readonly DependencyProperty BrushProperty =
          DependencyProperty.Register("Brush", typeof(GradientBrush), typeof(MockDataObject), new PropertyMetadata());

        [Category("Category One")]
        [DisplayName("Is enabled")]
        public bool IsEnabled
        {
            get { return (bool)GetValue(IsEnabledProperty); }
            set { SetValue(IsEnabledProperty, value); }
        }

        [Category("Layout")]
        public HorizontalAlignment HorizontalAlignment
        {
            get { return (HorizontalAlignment)GetValue(HorizontalAlignmentProperty); }
            set { SetValue(HorizontalAlignmentProperty, value); }
        }

        [Category("Layout")]
        public VerticalAlignment VerticalAlignment
        {
            get { return (VerticalAlignment)GetValue(VerticalAlignmentProperty); }
            set { SetValue(VerticalAlignmentProperty, value); }
        }

        [Category("Layout")]
        public GradientBrush Brush
        {
            get { return (GradientBrush)GetValue(BrushProperty); }
            set { SetValue(BrushProperty, value); }
        }

    }
}
