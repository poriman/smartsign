﻿namespace DenisVuyka.Controls.PropertyGrid
{
  /// <summary>
  /// Defines the display schema condition evaluation mode
  /// </summary>
  public enum DisplaySchemaModes
  {
    /// <summary>
    /// Check for positive conditions evaluation result.
    /// </summary>
    Inclusive,
    /// <summary>
    /// Check for negative conditions evaluation result.
    /// </summary>
    Exclusive
  }
}
