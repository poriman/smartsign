﻿namespace DenisVuyka.Controls.PropertyGrid.Editors
{
  public interface IValueProvider
  {
    object ProvideValue(object component, object value);
  }
}
