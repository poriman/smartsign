﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DenisVuyka.Controls.PropertyGrid.Editors
{
    /// <summary>
    /// Interaction logic for TextEditorButton.xaml
    /// </summary>
    public partial class TextEditorButton : UserControl
    {
        public delegate void CallButtonSimpleTextEditorDelegate();
        public delegate void TextChangedSimpleTextEditorDelegate(string text);
        public CallButtonSimpleTextEditorDelegate CallButtonSimpleTextEditorHandler;
        public TextChangedSimpleTextEditorDelegate TextChangedSimpleTextEditorHandler;
        
        public TextEditorButton()
        {
            InitializeComponent();
        }

        private void EditorButton_Click(object sender, RoutedEventArgs e)
        {           
            if (this.CallButtonSimpleTextEditorHandler != null)
            {
                this.CallButtonSimpleTextEditorHandler();
            }
        }

        public void SetTextBlock(string text)
        {
            this.ContentTextBox.Text = text;
        }

        private void ContentTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (this.TextChangedSimpleTextEditorHandler != null)
            {
                this.TextChangedSimpleTextEditorHandler(this.ContentTextBox.Text);
            }
        }
    }
}
