﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DenisVuyka.Controls.PropertyGrid.Editors
{
    /// <summary>
    /// Interaction logic for ButtonStyleEditor.xaml
    /// </summary>
    public partial class ButtonStyleEditor : UserControl
    {
        public delegate Style CallButtonSTyleEditorDelegate();

        public CallButtonSTyleEditorDelegate CallButtonSTyleEditorHandler;
        private Size previewButtonSize = new Size(80, 28);

        public ButtonStyleEditor()
        {
            InitializeComponent();
        }
      
        private void BrushEditorButton_Click(object sender, RoutedEventArgs e)
        {
            Style style = this.CurrentButtonStyleButton.Style;
            if (this.CallButtonSTyleEditorHandler != null)
            {
                Style changedStyle = this.CallButtonSTyleEditorHandler();
                if (changedStyle != null)
                {
                    List<Setter> setters = changedStyle.Setters.Where(o => (o is Setter) == true).Select(o => o as Setter).ToList();
                    var template_setter = setters.Where(o => o.Property.ToString().Equals("Template") == true).FirstOrDefault();

                    if (template_setter != null)
                    {
                        ControlTemplate controlTemplate = template_setter.Value as ControlTemplate;
                        DependencyObject depObj = controlTemplate.LoadContent();

                        var width_setter = setters.Where(o => o.Property.ToString().Equals("Width") == true).FirstOrDefault();
                        var height_setter = setters.Where(o => o.Property.ToString().Equals("Height") == true).FirstOrDefault();

                        if (width_setter != null && height_setter != null)
                        {
                            double width = (double)width_setter.Value;
                            double height = (double)height_setter.Value;

                            FrameworkElement uielement = depObj as FrameworkElement;
                            if (uielement != null && width != 0 && height != 0)
                            {
                                this.setScaleTransform(width, height);      
                            }
                        }
                        else
                        {

                            this.setScaleTransform(previewButtonSize.Width, previewButtonSize.Height);                          
                        }
                    }

                    //CurrentButtonStyleButton.Width = previewButtonSize.Width;
                    //CurrentButtonStyleButton.Height = previewButtonSize.Height;
                    CurrentButtonStyleButton.Style = changedStyle;
                }
            }
        }

        public void setScaleTransform(double width, double height)
        {
            CurrentButtonStyleButton.Width = width;
            CurrentButtonStyleButton.Height = height;

            ScaleTransform scaletransform = new ScaleTransform();
            scaletransform.ScaleX = previewButtonSize.Width / width;
            scaletransform.ScaleY = previewButtonSize.Height / height;
            CurrentButtonStyleButton.LayoutTransform = scaletransform;
        }

        public void SetButtonStyle(Style style)
        {
            CurrentButtonStyleButton.Style = style;
        }
    }
}
