﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DenisVuyka.Controls.PropertyGrid.Editors
{
    /// <summary>
    /// Interaction logic for TouchEventEditor.xaml
    /// </summary>
    public partial class TouchEventPropertyControl : UserControl
    {
        public delegate Dictionary<string, string> CallTouchEventPropertyControlDelegate();

        public CallTouchEventPropertyControlDelegate CallTouchEventPropertyControlHandler;
        Dictionary<string, string> TouchEventDic { get; set; }

        public TouchEventPropertyControl()
        {
            InitializeComponent();
        }

        private void TouchEventEditorButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.CallTouchEventPropertyControlHandler != null)
            {
                TouchEventDic = this.CallTouchEventPropertyControlHandler();
                if (TouchEventDic != null && TouchEventDic.Count > 0)
                {
                }
            }
        }

        public void SetTouchEventDic(Dictionary<string, string> touchEventDic)
        {
            TouchEventDic = touchEventDic;
        }
    }
}
