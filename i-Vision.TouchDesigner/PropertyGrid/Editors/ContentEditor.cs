﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows;

namespace DenisVuyka.Controls.PropertyGrid.Editors
{
    public class ContentEditor : Control
    {
       public delegate void ChangedEditValueDelegate(string editValue);

        public ChangedEditValueDelegate ChangedEditValueHandler;
        static ContentEditor()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ContentEditor), new FrameworkPropertyMetadata(typeof(ContentEditor)));
        }

        public static readonly DependencyProperty EditComponentProperty =
          DependencyProperty.Register("EditComponent", typeof(object), typeof(ContentEditor),
          new FrameworkPropertyMetadata(null));

        public object EditComponent
        {
            get { return GetValue(EditComponentProperty); }
            set { SetValue(EditComponentProperty, value); }
        }

        public static readonly DependencyProperty EditedValueProperty =
          DependencyProperty.Register("EditValue", typeof(object), typeof(ContentEditor),
          new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public object EditValue
        {
            get { return GetValue(EditedValueProperty); }
            set { SetValue(EditedValueProperty, value); }
        }

        public static readonly DependencyProperty ValueProviderProperty =
          DependencyProperty.Register("ValueProvider", typeof(Type), typeof(ContentEditor), new PropertyMetadata(null));

        public Type ValueProvider
        {
            get { return (Type)GetValue(ValueProviderProperty); }
            set { SetValue(ValueProviderProperty, value); }
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            Button selectButton = base.GetTemplateChild("PART_button") as Button;
            selectButton.Click += new RoutedEventHandler(selectButton_Click);
        }

        void selectButton_Click(object sender, RoutedEventArgs e)
        {
            if (EditComponent == null) return;
            if (ValueProvider == null) return;

            IValueProvider provider = Activator.CreateInstance(ValueProvider) as IValueProvider;
            if (provider == null) return;

            EditValue = provider.ProvideValue(EditComponent, EditValue);
            if (ChangedEditValueHandler != null)
                ChangedEditValueHandler(EditValue as string);
        }
    }  
}
