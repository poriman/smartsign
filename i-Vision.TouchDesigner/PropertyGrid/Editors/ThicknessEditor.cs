﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;

namespace DenisVuyka.Controls.PropertyGrid.Editors
{
  /// <summary>
  /// Identifies the <see cref="Thickness"/> member.
  /// </summary>
  public enum ThicknessDisplayMember
  {
    /// <summary>
    /// Display all
    /// </summary>
    All,
    /// <summary>
    /// Left
    /// </summary>
    Left,
    /// <summary>
    /// Top
    /// </summary>
    Top,
    /// <summary>
    /// Right
    /// </summary>
    Right,
    /// <summary>
    /// Bottom
    /// </summary>
    Bottom
  }

  /// <summary>
  /// The editor that provides possibilities editing defind <see cref="Thickness"/> members.
  /// </summary>
  public class ThicknessEditor : TextBox
  {
    /// <summary>
    /// Represents an empty <see cref="Thickness"/> value.
    /// </summary>
    public static readonly Thickness EmptyValue = new Thickness();

    #region Fields

    private bool updating;

    #endregion

    #region Properties

    #region Value property
    /// <summary>
    /// Identifies the <see cref="EditValue"/> dependency property.
    /// </summary>
    public static readonly DependencyProperty EditValueProperty =
      DependencyProperty.Register("EditValue", typeof(Thickness), typeof(ThicknessEditor),
        new FrameworkPropertyMetadata(
          new Thickness(),
          FrameworkPropertyMetadataOptions.AffectsMeasure | FrameworkPropertyMetadataOptions.BindsTwoWayByDefault,
          new PropertyChangedCallback(OnEditValuePropertyChanged), new CoerceValueCallback(OnEditValuePropertyCoerceValue), false, UpdateSourceTrigger.LostFocus));

    private static void OnEditValuePropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
    {
      ThicknessEditor textbox = (ThicknessEditor)sender;
      textbox.UpdateText();
    }

    private static object OnEditValuePropertyCoerceValue(DependencyObject sender, object baseValue)
    {
      return baseValue;
    }

    /// <summary>
    /// Gets or sets the edited value. This is a dependency property.
    /// </summary>
    /// <value>The edited value.</value>
    public Thickness EditValue
    {
      get { return (Thickness)GetValue(EditValueProperty); }
      set { SetValue(EditValueProperty, value); }
    }
    #endregion

    #region DisplayMember property
    /// <summary>
    /// Identifies the <see cref="DisplayMember"/> dependency property.
    /// </summary>
    public static readonly DependencyProperty DisplayMemberProperty =
      DependencyProperty.Register("DisplayMember", typeof(ThicknessDisplayMember), typeof(ThicknessEditor),
        new FrameworkPropertyMetadata(ThicknessDisplayMember.All));

    /// <summary>
    /// Gets or sets the display member. This is a dependency property.
    /// </summary>
    /// <value>The display member.</value>
    public ThicknessDisplayMember DisplayMember
    {
      get { return (ThicknessDisplayMember)GetValue(DisplayMemberProperty); }
      set { SetValue(DisplayMemberProperty, value); }
    }
    #endregion

    #endregion

    #region Overrides
    /// <summary>
    /// Is called when content in this editing control changes.
    /// </summary>
    /// <param name="e">The arguments that are associated with the <see cref="E:System.Windows.Controls.Primitives.TextBoxBase.TextChanged"/> event.</param>
    protected override void OnTextChanged(TextChangedEventArgs e)
    {
      if (!updating)
      {
        base.OnTextChanged(e);

        double value = 0;
        if (double.TryParse(this.Text, out value))
        {
          switch (DisplayMember)
          {
            case ThicknessDisplayMember.Left:
              this.EditValue = new Thickness(value, EditValue.Top, EditValue.Right, EditValue.Bottom);
              break;
            case ThicknessDisplayMember.Top:
              this.EditValue = new Thickness(EditValue.Left, value, EditValue.Right, EditValue.Bottom);
              break;
            case ThicknessDisplayMember.Right:
              this.EditValue = new Thickness(EditValue.Left, EditValue.Top, value, EditValue.Bottom);
              break;
            case ThicknessDisplayMember.Bottom:
              this.EditValue = new Thickness(EditValue.Left, EditValue.Top, EditValue.Right, value);
              break;
            default:
              break;
          }
        }
      }
    }

    /// <summary>
    /// Invoked when an unhandled <see cref="E:System.Windows.Input.TextCompositionManager.PreviewTextInput"/> attached event reaches an element in its route that is derived from this class. Implement this method to add class handling for this event.
    /// </summary>
    /// <param name="e">The <see cref="T:System.Windows.Input.TextCompositionEventArgs"/> that contains the event data.</param>
    protected override void OnPreviewTextInput(TextCompositionEventArgs e)
    {
      e.Handled = !AreAllValidNumericChars(e.Text);
      base.OnPreviewTextInput(e);
    }

    /// <summary>
    /// Raises the <see cref="E:System.Windows.FrameworkElement.Initialized"/> event. This method is invoked whenever <see cref="P:System.Windows.FrameworkElement.IsInitialized"/> is set to true internally.
    /// </summary>
    /// <param name="e">The <see cref="T:System.Windows.RoutedEventArgs"/> that contains the event data.</param>
    protected override void OnInitialized(EventArgs e)
    {
      base.OnInitialized(e);

      if (EditValue == EmptyValue)
        UpdateText();
    }
    #endregion

    #region Private members

    private void UpdateText()
    {
      updating = true;

      switch (DisplayMember)
      {
        case ThicknessDisplayMember.Left:
          Text = EditValue.Left.ToString(CultureInfo.CurrentCulture);
          break;
        case ThicknessDisplayMember.Top:
          Text = EditValue.Top.ToString(CultureInfo.CurrentCulture);
          break;
        case ThicknessDisplayMember.Right:
          Text = EditValue.Right.ToString(CultureInfo.CurrentCulture);
          break;
        case ThicknessDisplayMember.Bottom:
          Text = EditValue.Bottom.ToString(CultureInfo.CurrentCulture);
          break;
        default:
          Text = EditValue.ToString();
          break;
      }

      updating = false;
    }

    // http://blogs.microsoft.co.il/blogs/tamir/archive/2007/11/15/number-only-textbox.aspx
    private static bool AreAllValidNumericChars(string str)
    {
      bool ret = true;
      if (str == NumberFormatInfo.CurrentInfo.CurrencyDecimalSeparator |
          str == NumberFormatInfo.CurrentInfo.CurrencyGroupSeparator |
          str == NumberFormatInfo.CurrentInfo.CurrencySymbol |
          str == NumberFormatInfo.CurrentInfo.NegativeSign |
          str == NumberFormatInfo.CurrentInfo.NegativeInfinitySymbol |
          str == NumberFormatInfo.CurrentInfo.NumberDecimalSeparator |
          str == NumberFormatInfo.CurrentInfo.NumberGroupSeparator |
          str == NumberFormatInfo.CurrentInfo.PercentDecimalSeparator |
          str == NumberFormatInfo.CurrentInfo.PercentGroupSeparator |
          str == NumberFormatInfo.CurrentInfo.PercentSymbol |
          str == NumberFormatInfo.CurrentInfo.PerMilleSymbol |
          str == NumberFormatInfo.CurrentInfo.PositiveInfinitySymbol |
          str == NumberFormatInfo.CurrentInfo.PositiveSign)
        return ret;

      int l = str.Length;
      for (int i = 0; i < l; i++)
      {
        char ch = str[i];
        ret &= char.IsDigit(ch);
      }

      return ret;
    }
    #endregion

    #region ctor
    /// <summary>
    /// Initializes the <see cref="ThicknessEditor"/> class.
    /// </summary>
    static ThicknessEditor()
    {
      DefaultStyleKeyProperty.OverrideMetadata(typeof(ThicknessEditor), new FrameworkPropertyMetadata(typeof(ThicknessEditor)));
    }
    #endregion
  }
}
