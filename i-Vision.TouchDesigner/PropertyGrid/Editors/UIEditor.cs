﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace DenisVuyka.Controls.PropertyGrid.Editors
{
    public class UIEditor : Control
    {
        public delegate void ChangedEditValueDelegate(string editValue);

        public ChangedEditValueDelegate ChangedEditValueHandler;
        static UIEditor()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(UIEditor), new FrameworkPropertyMetadata(typeof(UIEditor)));
        }

        public static readonly DependencyProperty EditComponentProperty =
          DependencyProperty.Register("EditComponent", typeof(object), typeof(UIEditor),
          new FrameworkPropertyMetadata(null));

        public object EditComponent
        {
            get { return GetValue(EditComponentProperty); }
            set { SetValue(EditComponentProperty, value); }
        }

        public static readonly DependencyProperty EditedValueProperty =
          DependencyProperty.Register("EditValue", typeof(object), typeof(UIEditor),
          new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

        public object EditValue
        {
            get { return GetValue(EditedValueProperty); }
            set { SetValue(EditedValueProperty, value); }
        }

        public static readonly DependencyProperty ValueProviderProperty =
          DependencyProperty.Register("ValueProvider", typeof(Type), typeof(UIEditor), new PropertyMetadata(null));

        public Type ValueProvider
        {
            get { return (Type)GetValue(ValueProviderProperty); }
            set { SetValue(ValueProviderProperty, value); }
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            Button selectButton = base.GetTemplateChild("PART_button") as Button;
            selectButton.Click += new RoutedEventHandler(selectButton_Click);
        }

        void selectButton_Click(object sender, RoutedEventArgs e)
        {
            if (EditComponent == null) return;
            if (ValueProvider == null) return;

            IValueProvider provider = Activator.CreateInstance(ValueProvider) as IValueProvider;
            if (provider == null) return;

            EditValue = provider.ProvideValue(EditComponent, EditValue);
            if (ChangedEditValueHandler != null)
                ChangedEditValueHandler(EditValue as string);
        }
    }
}
