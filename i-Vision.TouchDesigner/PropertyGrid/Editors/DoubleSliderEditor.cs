﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;

namespace DenisVuyka.Controls.PropertyGrid.Editors
{
    public class DoubleSliderEditor : Slider
    {
        #region Fields
        private bool isUpdating;
        #endregion

        #region EditValue
        /// <summary>
        /// Identifies the <see cref="EditValue"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty EditValueProperty = DependencyProperty.Register("EditValue", typeof(double), typeof(DoubleSliderEditor), 
            new FrameworkPropertyMetadata(1.0, OnEditValuePropertyChanged));

        private static void OnEditValuePropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            DoubleSliderEditor editor = (DoubleSliderEditor)sender;
            editor.UpdateValue();
        }

        private static object OnEditValuePropertyCoerceValue(DependencyObject sender, object baseValue)
        {
            return baseValue;
        }
       
        /// <summary>
        /// Gets or sets the edited value. This is a dependency property.
        /// </summary>
        /// <value>The edited value.</value>
        public double EditValue
        {
            get { return (double)GetValue(EditValueProperty); }
            set { SetValue(EditValueProperty, value); }
        }
        #endregion

        #region ctor
        /// <summary>
        /// Initializes a new instance of the <see cref="PointEditor"/> class.
        /// </summary>
        public DoubleSliderEditor()
            : base()
        {
            this.ValueChanged += new RoutedPropertyChangedEventHandler<double>(DoubleSliderEditor_ValueChanged);

            this.AutoToolTipPlacement = AutoToolTipPlacement.BottomRight;
            this.IsDirectionReversed = false;
            this.IsMoveToPointEnabled = true;
            this.IsSnapToTickEnabled = false;

            this.Value = this.EditValue;
        }

        void DoubleSliderEditor_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            
        }
        #endregion

        protected override void OnValueChanged(double oldValue, double newValue)
        {
            if (isUpdating)
                return;
            base.OnValueChanged(oldValue, newValue);
            this.EditValue = (float)Value;  
        }

        private void UpdateValue()
        {
            if (isUpdating) 
                return;

            isUpdating = true;
            this.Value = EditValue;
            isUpdating = false;
        }
    }
}
