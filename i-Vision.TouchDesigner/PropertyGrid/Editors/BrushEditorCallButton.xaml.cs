﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DenisVuyka.Controls.PropertyGrid.Editors
{
    /// <summary>
    /// Interaction logic for BrushEditorCallButton.xaml
    /// </summary>
    public partial class BrushEditorCallButton : UserControl
    {
        public delegate Brush CallBrushEditorDelegate();

        public CallBrushEditorDelegate CallBrushEditorEventHandler;

        public BrushEditorCallButton()
        {
            InitializeComponent();
        }

        private void BrushEditorButton_Click(object sender, RoutedEventArgs e)
        {
            //Brush currentBrush = CurrentBrushValueRectangle.Fill;
            if (this.CallBrushEditorEventHandler != null)
            {
                Brush changedBrush = this.CallBrushEditorEventHandler();
                if (changedBrush != null)
                {
                    CurrentBrushValueRectangle.Fill = changedBrush;
                }
            }
        }

        public void SetBrush(Brush brush)
        {
            CurrentBrushValueRectangle.Fill = brush;
        }
    }
}
