﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;

namespace DenisVuyka.Controls.PropertyGrid
{
    using Data;
    using System.Collections.Generic;
    using System.Globalization;
    using DenisVuyka.Controls.PropertyGrid.Cultures;
    using System.Threading;

    /// <summary>
    /// PropertyGrid control.
    /// </summary>
    public class PropertyGrid : Control
    {
        #region Static fields
        /// <summary>
        /// Identifies the empty property filter.
        /// </summary>
        public static readonly string EmptyPropertyFilter = string.Empty;
        #endregion

        #region ctor
        static PropertyGrid()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(PropertyGrid), new FrameworkPropertyMetadata(typeof(PropertyGrid)));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="PropertyGrid"/> class.
        /// </summary>
        public PropertyGrid()
        {
            this.CommandBindings.Add(new CommandBinding(PropertyGridCommands.ResetFilter, OnResetFilterCommand));
            this.CommandBindings.Add(new CommandBinding(PropertyGridCommands.Reload, OnReloadCommand));
            this.CommandBindings.Add(new CommandBinding(PropertyGridCommands.ExpandCategories, OnExpandCategoriesCommand));
            this.CommandBindings.Add(new CommandBinding(PropertyGridCommands.CollapseCategories, OnCollapseCategoriesCommand));
            this.CommandBindings.Add(new CommandBinding(PropertyGridCommands.ToggleCategories, OnToggleCategoriesCommand));
            this.CommandBindings.Add(new CommandBinding(PropertyGridCommands.ShowReadOnlyProperties, OnShowReadOnlyPropertiesCommand));
            this.CommandBindings.Add(new CommandBinding(PropertyGridCommands.HideReadOnlyProperties, OnHideReadOnlyPropertiesCommand));
            this.CommandBindings.Add(new CommandBinding(PropertyGridCommands.ToggleReadOnlyProperties, OnToggleReadOnlyPropertiesCommand));
            this.CommandBindings.Add(new CommandBinding(PropertyGridCommands.ShowAttachedProperties, OnShowAttachedPropertiesCommand));
            this.CommandBindings.Add(new CommandBinding(PropertyGridCommands.HideAttachedProperties, OnHideAttachedPropertiesCommand));
            this.CommandBindings.Add(new CommandBinding(PropertyGridCommands.ToggleAttachedProperties, OnToggleAttachedPropertiesCommand));
            this.CommandBindings.Add(new CommandBinding(PropertyGridCommands.ShowFilter, OnShowFilterCommand));
            this.CommandBindings.Add(new CommandBinding(PropertyGridCommands.HideFilter, OnHideFilterCommand));
            this.CommandBindings.Add(new CommandBinding(PropertyGridCommands.ToggleFilter, OnToggleFilterCommand));
        }
        #endregion

        #region Events

        /// <summary>
        /// Identifies the <see cref="PropertyValueChanged"/> routed event.
        /// </summary>
        public static readonly RoutedEvent PropertyValueChangedEvent =
          EventManager.RegisterRoutedEvent(
            "PropertyValueChanged",
            RoutingStrategy.Bubble,
            typeof(PropertyValueChangedEventHandler),
            typeof(PropertyGrid));

        /// <summary>
        /// Occurs when property item value is changed.
        /// </summary>
        public event PropertyValueChangedEventHandler PropertyValueChanged
        {
            add { AddHandler(PropertyValueChangedEvent, value); }
            remove { RemoveHandler(PropertyValueChangedEvent, value); }
        }

        private void RaisePropertyValueChangedEvent(PropertyItem property, object oldValue)
        {
            PropertyValueChangedEventArgs args = new PropertyValueChangedEventArgs(PropertyValueChangedEvent, property, oldValue);
            RaiseEvent(args);
        }

        #endregion

        #region Properties

        #region SelectedObject Property

        /// <summary>
        /// Identifies the <see cref="SelectedObject"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty SelectedObjectProperty =
          DependencyProperty.Register("SelectedObject", typeof(object), typeof(PropertyGrid),
          new FrameworkPropertyMetadata(null, OnSelectedObjectChanged, CoerceSelectedObject));

        /// <summary>
        /// Object for editing properties. This is a dependency property.
        /// </summary>
        /// <value>The selected object.</value>
        public object SelectedObject
        {
            get { return (object)GetValue(SelectedObjectProperty); }
            set { SetValue(SelectedObjectProperty, value); }
        }

        /// <summary>
        /// Invoked on Instance change.
        /// </summary>
        /// <param name="d">The object that was changed</param>
        /// <param name="e">Dependency property changed event arguments</param>
        static void OnSelectedObjectChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            PropertyGrid propertyGrid = d as PropertyGrid;
            propertyGrid.DoReload();
        }

        static object CoerceSelectedObject(DependencyObject d, object value)
        {
            PropertyGrid propertyGrid = d as PropertyGrid;
            if (value == null)
                return propertyGrid.FallbackValue;

            return value;
        }

        #endregion

        #region FallbackValue

        /// <summary>
        /// Identifies the <see cref="FallbackValue"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty FallbackValueProperty =
          DependencyProperty.Register("FallbackValue", typeof(object), typeof(PropertyGrid),
          new FrameworkPropertyMetadata(null, OnFallbackValueChanged));

        /// <summary>
        /// Gets or sets the fallback value.
        /// </summary>
        /// <value>The fallback value.</value>
        public object FallbackValue
        {
            get { return GetValue(FallbackValueProperty); }
            set { SetValue(FallbackValueProperty, value); }
        }

        /// <summary>
        /// Called when FallbackValue property changed.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="System.Windows.DependencyPropertyChangedEventArgs"/> instance containing the event data.</param>
        static void OnFallbackValueChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
        }
        #endregion

        #region Properties property
        private static readonly DependencyPropertyKey PropertiesPropertyKey =
          DependencyProperty.RegisterReadOnly("Properties", typeof(ObservableCollection<GridItemBase>), typeof(PropertyGrid),
          new FrameworkPropertyMetadata(new ObservableCollection<GridItemBase>(), OnPropertiesPropertyChanged));

        /// <summary>
        /// Identifies the <see cref="Properties"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty PropertiesProperty = PropertiesPropertyKey.DependencyProperty;

        /// <summary>
        /// Gets the properties. This is a dependency property.
        /// </summary>
        /// <value>The properties.</value>
        public ObservableCollection<GridItemBase> Properties
        {
            get { return (ObservableCollection<GridItemBase>)GetValue(PropertiesProperty); }
            private set { SetValue(PropertiesPropertyKey, value); }
        }

        private static void OnPropertiesPropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            PropertyGrid propertyGrid = sender as PropertyGrid;
            ObservableCollection<GridItemBase> oldItems = e.OldValue as ObservableCollection<GridItemBase>;

            foreach (GridItemBase item in oldItems)
            {
                propertyGrid.UnhookPropertyChanged(item);
                item.Dispose();
            }

            ObservableCollection<GridItemBase> newItems = e.NewValue as ObservableCollection<GridItemBase>;
            if (newItems != null)
                foreach (GridItemBase item in newItems)
                    propertyGrid.HookPropertyChanged(item);

            propertyGrid.PropertiesView = new ListCollectionView(propertyGrid.Properties);
            propertyGrid.PropertiesView.SortDescriptions.Add(new SortDescription("PropertyOrder", ListSortDirection.Descending));//thinkblue_20100628
            propertyGrid.Properties.CollectionChanged += new NotifyCollectionChangedEventHandler(propertyGrid.Properties_CollectionChanged);

            //foreach (PropertyItem propItem in propertyGrid.Properties)
            //{
            //    FrameworkElement frameworkElement = propertyGrid.TryFindEditor(propItem);
            //}
        }

        private void Properties_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            HasProperties = (this.PropertiesView != null && !this.PropertiesView.IsEmpty);
        }
        #endregion

        #region HasProperties property

        private static readonly DependencyPropertyKey HasPropertiesPropertyKey =
          DependencyProperty.RegisterReadOnly("HasProperties", typeof(bool), typeof(PropertyGrid), new FrameworkPropertyMetadata(false));

        /// <summary>
        /// Identifies the <see cref="HasProperties"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty HasPropertiesProperty = HasPropertiesPropertyKey.DependencyProperty;

        /// <summary>
        /// Gets a value indicating whether this instance has properties. This is a dependency property.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if this instance has properties; otherwise, <c>false</c>.
        /// </value>
        public bool HasProperties
        {
            get { return (bool)GetValue(HasPropertiesProperty); }
            private set { SetValue(HasPropertiesPropertyKey, value); }
        }

        #endregion

        #region PropertiesView property

        private static readonly DependencyPropertyKey PropertiesViewPropertyKey =
          DependencyProperty.RegisterReadOnly("PropertiesView", typeof(ListCollectionView), typeof(PropertyGrid),
          new FrameworkPropertyMetadata(null));

        /// <summary>
        /// Identifies the <see cref="PropertiesView"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty PropertiesViewProperty = PropertiesViewPropertyKey.DependencyProperty;

        /// <summary>
        /// Gets the properties view. This is a dependency property.
        /// </summary>
        /// <value>The properties view.</value>
        public ListCollectionView PropertiesView
        {
            get { return (ListCollectionView)GetValue(PropertiesViewProperty); }
            private set { SetValue(PropertiesViewPropertyKey, value); }
        }

        #endregion

        #region Categories Property

        private static readonly DependencyPropertyKey CategoriesPropertyKey =
          DependencyProperty.RegisterReadOnly("Categories", typeof(ObservableCollection<GridItemBase>), typeof(PropertyGrid),
          new FrameworkPropertyMetadata(new ObservableCollection<GridItemBase>(), OnCategoriesPropertyChanged));

        /// <summary>
        /// Identifies the <see cref="Categories"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty CategoriesProperty = CategoriesPropertyKey.DependencyProperty;

        /// <summary>
        /// Gets the categories. This is a dependency property.
        /// </summary>
        /// <value>The categories.</value>
        public ObservableCollection<GridItemBase> Categories
        {
            get { return (ObservableCollection<GridItemBase>)GetValue(CategoriesProperty); }
            private set { SetValue(CategoriesPropertyKey, value); }
        }

        private static void OnCategoriesPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            PropertyGrid propertyGrid = d as PropertyGrid;
            ObservableCollection<GridItemBase> items = e.OldValue as ObservableCollection<GridItemBase>;
            foreach (GridItemBase item in items)
                item.Dispose();

            propertyGrid.CategoriesView = new ListCollectionView(propertyGrid.Categories);
            propertyGrid.CategoriesView.SortDescriptions.Add(new SortDescription("Name", ListSortDirection.Ascending));
            //propertyGrid.CategoriesView.SortDescriptions.Add(new SortDescription());
            propertyGrid.Categories.CollectionChanged += new NotifyCollectionChangedEventHandler(propertyGrid.Categories_CollectionChanged);
            propertyGrid.UpdateHasCategoriesProperty();
        }

        private void Categories_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            UpdateHasCategoriesProperty();
        }

        private void UpdateHasCategoriesProperty()
        {
            SetValue(HasCategoriesPropertyKey, (this.CategoriesView != null) && !this.CategoriesView.IsEmpty);
        }

        #endregion

        #region HasCategories property

        private static readonly DependencyPropertyKey HasCategoriesPropertyKey =
          DependencyProperty.RegisterReadOnly("HasCategories", typeof(bool), typeof(PropertyGrid), new FrameworkPropertyMetadata(false));

        /// <summary>
        /// Identifies the <see cref="HasCategories"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty HasCategoriesProperty = HasCategoriesPropertyKey.DependencyProperty;

        /// <summary>
        /// Gets a value indicating whether this instance has categories. This is a dependency property.
        /// </summary>
        /// <value><c>true</c> if this instance has categories; otherwise, <c>false</c>.</value>
        public bool HasCategories
        {
            get { return (bool)GetValue(HasCategoriesProperty); }
            private set { SetValue(HasCategoriesPropertyKey, value); }
        }

        #endregion

        #region CategoriesView property

        private static readonly DependencyPropertyKey CategoriesViewPropertyKey =
          DependencyProperty.RegisterReadOnly("CategoriesView", typeof(ListCollectionView), typeof(PropertyGrid),
          new FrameworkPropertyMetadata(null));

        /// <summary>
        /// Identifies the <see cref="CategoriesView"/> dependency property
        /// </summary>
        public static readonly DependencyProperty CategoriesViewProperty = CategoriesViewPropertyKey.DependencyProperty;

        /// <summary>
        /// Gets the categories view. This is a dependency property.
        /// </summary>
        /// <value>The items view.</value>
        public ListCollectionView CategoriesView
        {
            get { return (ListCollectionView)GetValue(CategoriesViewProperty); }
            private set { SetValue(CategoriesViewPropertyKey, value); }
        }

        #endregion

        #region ExpandCategories property

        /// <summary>
        /// Identifies the <see cref="ExpandCategories"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty ExpandCategoriesProperty =
          DependencyProperty.Register("ExpandCategories", typeof(bool), typeof(PropertyGrid),
          new PropertyMetadata(true, OnExpandCategoriesPropertyChanged));

        private static void OnExpandCategoriesPropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            PropertyGrid pg = (PropertyGrid)sender;
            if (pg.HasCategories)
            {
                foreach (CategoryItem category in pg.Categories)
                    category.IsExpanded = (bool)e.NewValue;
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether the categories should be expanded by default. This is a dependency property.
        /// </summary>
        /// <value>
        ///   <c>true</c> if categories should be expanded; otherwise, <c>false</c>. Default is <c>true</c>.
        /// </value>
        public bool ExpandCategories
        {
            get { return (bool)GetValue(ExpandCategoriesProperty); }
            set { SetValue(ExpandCategoriesProperty, value); }
        }

        #endregion

        #region ShowReadOnlyProperties property

        /// <summary>
        /// Identifies the <see cref="ShowReadOnlyProperties"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty ShowReadOnlyPropertiesProperty =
         DependencyProperty.Register("ShowReadOnlyProperties", typeof(bool), typeof(PropertyGrid),
         new PropertyMetadata(false, OnShowReadOnlyPropertiesChanged));

        private static void OnShowReadOnlyPropertiesChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            PropertyGrid pg = (PropertyGrid)sender;
            if (pg.Categories.Count > 0 | pg.Properties.Count > 0)
                pg.DoReload();
        }

        /// <summary>
        /// Gets or sets a value indicating whether read-only properties should be displayed. This is a dependency property.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if read-only properties should be displayed; otherwise, <c>false</c>. Default is <c>false</c>.
        /// </value>
        public bool ShowReadOnlyProperties
        {
            get { return (bool)GetValue(ShowReadOnlyPropertiesProperty); }
            set { SetValue(ShowReadOnlyPropertiesProperty, value); }
        }

        #endregion

        #region ShowAttachedProperties property

        /// <summary>
        /// Identifies the <see cref="ShowAttachedProperties"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty ShowAttachedPropertiesProperty =
          DependencyProperty.Register("ShowAttachedProperties", typeof(bool), typeof(PropertyGrid),
          new PropertyMetadata(false, OnShowAttachedPropertiesChanged));

        private static void OnShowAttachedPropertiesChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            PropertyGrid pg = (PropertyGrid)sender;
            if (pg.Categories.Count > 0 | pg.Properties.Count > 0)
                pg.DoReload();
        }

        /// <summary>
        /// Gets or sets a value indicating whether attached properties should be displayed.
        /// </summary>
        /// <value>
        /// 	<c>true</c> if attached properties should be displayed; otherwise, <c>false</c>. Default is <c>false</c>.
        /// </value>
        public bool ShowAttachedProperties
        {
            get { return (bool)GetValue(ShowAttachedPropertiesProperty); }
            set { SetValue(ShowAttachedPropertiesProperty, value); }
        }

        #endregion

        #region PropertyFilter
        /// <summary>
        /// Identifies the <see cref="PropertyFilter"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty PropertyFilterProperty =
          DependencyProperty.Register("PropertyFilter", typeof(string), typeof(PropertyGrid),
          new FrameworkPropertyMetadata(EmptyPropertyFilter, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault, new PropertyChangedCallback(OnPropertyFilterChanged)));

        private static void OnPropertyFilterChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            PropertyGrid propertyGrid = (PropertyGrid)sender;
            foreach (CategoryItem category in propertyGrid.Categories.OfType<CategoryItem>())
            {
                category.Filter = propertyGrid.DefaultPropertyFilter;
            }
        }

        /// <summary>
        /// Gets or sets the property filter. This is a dependency property.
        /// </summary>
        /// <value>The property filter.</value>
        public string PropertyFilter
        {
            get { return (string)GetValue(PropertyFilterProperty); }
            set { SetValue(PropertyFilterProperty, value); }
        }
        #endregion

        #region ShowPropertyFilter
        /// <summary>
        /// Identifies the <see cref="ShowPropertyFilter"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty ShowPropertyFilterProperty =
          DependencyProperty.Register("ShowPropertyFilter", typeof(bool), typeof(PropertyGrid),
          new FrameworkPropertyMetadata(true, new PropertyChangedCallback(OnShowPropertyFilterChanged)));

        private static void OnShowPropertyFilterChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            if (!(bool)e.NewValue)
            {
                PropertyGrid propertyGrid = (PropertyGrid)sender;
                propertyGrid.DoResetFilter();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to show property filter. This is a dependency property.
        /// </summary>
        /// <value>
        ///   <c>true</c> if property filter should be displayed; otherwise, <c>false</c>. Default is <c>true</c>.
        /// </value>
        public bool ShowPropertyFilter
        {
            get { return (bool)GetValue(ShowPropertyFilterProperty); }
            set { SetValue(ShowPropertyFilterProperty, value); }
        }
        #endregion

        #region ShowPropertyTitle
        /// <summary>
        /// Identifies the <see cref="ShowPropertyTitle"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty ShowPropertyTitleProperty =
          DependencyProperty.Register("ShowPropertyTitle", typeof(bool), typeof(PropertyGrid),
          new FrameworkPropertyMetadata(true, new PropertyChangedCallback(OnShowPropertyTitlePropertyChanged)));

        private static void OnShowPropertyTitlePropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            if (!(bool)e.NewValue)
            {
                PropertyGrid propertyGrid = (PropertyGrid)sender;
                propertyGrid.DoResetFilter();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to show property filter. This is a dependency property.
        /// </summary>
        /// <value>
        ///   <c>true</c> if property filter should be displayed; otherwise, <c>false</c>. Default is <c>true</c>.
        /// </value>
        public bool ShowPropertyTitle
        {
            get { return (bool)GetValue(ShowPropertyTitleProperty); }
            set { SetValue(ShowPropertyTitleProperty, value); }
        }
        #endregion

        #region PropertyDisplayMode

        /// <summary>
        /// Identifies the <see cref="PropertyDisplayMode"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty PropertyDisplayModeProperty =
          DependencyProperty.Register("PropertyDisplayMode", typeof(PropertyDisplayModes), typeof(PropertyGrid),
          new FrameworkPropertyMetadata(PropertyDisplayModes.All, new PropertyChangedCallback(OnPropertyDisplayModePropertyChanged)));

        /// <summary>
        /// Gets or sets the property display mode. This is a dependency property.
        /// </summary>
        /// <value>The property display mode.</value>
        public PropertyDisplayModes PropertyDisplayMode
        {
            get { return (PropertyDisplayModes)GetValue(PropertyDisplayModeProperty); }
            set { SetValue(PropertyDisplayModeProperty, value); }
        }

        private static void OnPropertyDisplayModePropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            PropertyGrid propertyGrid = (PropertyGrid)sender;
            propertyGrid.DoReload();
        }

        #endregion

        #region PropertyDisplaySchema

        /// <summary>
        /// Identifies the <see cref="PropertyDisplaySchema"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty PropertyDisplaySchemaProperty =
          DependencyProperty.Register("PropertyDisplaySchema", typeof(PropertyDisplaySchema), typeof(PropertyGrid),
          new FrameworkPropertyMetadata(null, new PropertyChangedCallback(OnPropertyDisplaySchemaPropertyChanged)));

        /// <summary>
        /// Gets or sets the property display schema. This is a dependency property.
        /// </summary>
        /// <value>The property display schema.</value>
        public PropertyDisplaySchema PropertyDisplaySchema
        {
            get { return (PropertyDisplaySchema)GetValue(PropertyDisplaySchemaProperty); }
            set { SetValue(PropertyDisplaySchemaProperty, value); }
        }

        private static void OnPropertyDisplaySchemaPropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            PropertyGrid propertyGrid = (PropertyGrid)sender;
            propertyGrid.DoReload();
        }

        #endregion

        #region IsVisibleCategory
        /// <summary>
        /// Identifies the <see cref="IsVisibleCategory"/> dependency property.
        /// </summary>
        public static readonly DependencyProperty IsVisibleCategoryProperty =
          DependencyProperty.Register("IsVisibleCategory", typeof(bool), typeof(PropertyGrid), 
          new FrameworkPropertyMetadata(false, new PropertyChangedCallback(OnIsVisibleCategoryPropertyChanged)));

        private static void OnIsVisibleCategoryPropertyChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            if (!(bool)e.NewValue)
            {
                PropertyGrid propertyGrid = (PropertyGrid)sender;
                propertyGrid.DoResetFilter();
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to show property filter. This is a dependency property.
        /// </summary>
        /// <value>
        ///   <c>true</c> if property filter should be displayed; otherwise, <c>false</c>. Default is <c>true</c>.
        /// </value>
        public bool IsVisibleCategory
        {
            get { return (bool)GetValue(IsVisibleCategoryProperty); }
            set { SetValue(IsVisibleCategoryProperty, value); }
        }
        #endregion

        #endregion

        #region Commands

        private void OnResetFilterCommand(object sender, ExecutedRoutedEventArgs e)
        {
            DoResetFilter();
        }

        private void OnReloadCommand(object sender, ExecutedRoutedEventArgs e)
        {
            DoReload();
        }

        private void OnExpandCategoriesCommand(object sender, ExecutedRoutedEventArgs e)
        {
            ExpandCategories = true;
        }

        private void OnCollapseCategoriesCommand(object sender, ExecutedRoutedEventArgs e)
        {
            ExpandCategories = false;
        }

        private void OnToggleCategoriesCommand(object sender, ExecutedRoutedEventArgs e)
        {
            ExpandCategories = !ExpandCategories;
        }

        private void OnShowReadOnlyPropertiesCommand(object sender, ExecutedRoutedEventArgs e)
        {
            ShowReadOnlyProperties = true;
        }

        private void OnHideReadOnlyPropertiesCommand(object sender, ExecutedRoutedEventArgs e)
        {
            ShowReadOnlyProperties = false;
        }

        private void OnToggleReadOnlyPropertiesCommand(object sender, ExecutedRoutedEventArgs e)
        {
            ShowReadOnlyProperties = !ShowReadOnlyProperties;
        }

        private void OnShowAttachedPropertiesCommand(object sender, ExecutedRoutedEventArgs e)
        {
            ShowAttachedProperties = true;
        }

        private void OnHideAttachedPropertiesCommand(object sender, ExecutedRoutedEventArgs e)
        {
            ShowAttachedProperties = false;
        }

        private void OnToggleAttachedPropertiesCommand(object sender, ExecutedRoutedEventArgs e)
        {
            ShowAttachedProperties = !ShowAttachedProperties;
        }

        private void OnShowFilterCommand(object sender, ExecutedRoutedEventArgs e)
        {
            ShowPropertyFilter = true;
        }

        private void OnHideFilterCommand(object sender, ExecutedRoutedEventArgs e)
        {
            ShowPropertyFilter = false;
        }

        private void OnToggleFilterCommand(object sender, ExecutedRoutedEventArgs e)
        {
            ShowPropertyFilter = !ShowPropertyFilter;
        }
        #endregion

        #region Private members

        private void DoResetFilter()
        {
            this.PropertyFilter = EmptyPropertyFilter;
        }

        public bool DefaultPropertyFilter(object target)
        {
            if (string.IsNullOrEmpty(PropertyFilter))
                return true;

            string PropertyLabel = ((PropertyItem)target).PropertyLabel;
            bool result = (target is PropertyItem) && PropertyLabel.StartsWith(PropertyFilter, StringComparison.CurrentCultureIgnoreCase);
            return result;
        }

        private void DoReload()
        {
            if (SelectedObject == null)
            {
                Categories = new ObservableCollection<GridItemBase>();
                Properties = new ObservableCollection<GridItemBase>();
            }
            else
            {
                CategoryItemCollection categories = new CategoryItemCollection(this, SelectedObject);
                this.Categories = categories.Categories;

                this.Properties = categories.Items;
            }
        }

        private void OnPropertyItemValueChanged(PropertyItem property, object oldValue)
        {
            RaisePropertyValueChangedEvent(property, oldValue);
        }

        private void HookPropertyChanged(GridItemBase item)
        {
            PropertyItem property = item as PropertyItem;
            if (property == null) return;
            property.PropertyValueChanged += OnPropertyItemValueChanged;
        }

        private void UnhookPropertyChanged(GridItemBase item)
        {
            PropertyItem property = item as PropertyItem;
            if (property == null) return;
            property.PropertyValueChanged -= OnPropertyItemValueChanged;
        }

        #endregion

        #region Public API

        // TODO: Note that this function is experimental and has no error checks!!! Function body is a subject to change!
        public FrameworkElement TryFindEditor(PropertyItem property)
        {
            CategoryItem category = this.Categories.OfType<CategoryItem>().Where(item => (item.Name == property.Category)).FirstOrDefault();

            ItemsControl categoriesList = this.Template.FindName("PART_items", this) as ItemsControl;
            ContentPresenter categoryPresenter = categoriesList.ItemContainerGenerator.ContainerFromItem(category) as ContentPresenter;
            Expander categoryExpander = VisualTreeHelper.GetChild(categoryPresenter, 0) as Expander;

            // Expander should be expanded only if the required editor is found indeed,
            // maybe an argument should be expected determining whether to expand the category or not...
            if (!categoryExpander.IsExpanded) categoryExpander.IsExpanded = true;

            ItemsControl categoryItemsList = categoryExpander.FindName("PART_items") as ItemsControl;
            ContentPresenter propertyPresenter = categoryItemsList.ItemContainerGenerator.ContainerFromItem(property) as ContentPresenter;

            FrameworkElement grid = VisualTreeHelper.GetChild(propertyPresenter, 0) as FrameworkElement;
            ContentControl propertyContent = grid.FindName("PART_content") as ContentControl;

            ContentPresenter editorPresenter = VisualTreeHelper.GetChild(propertyContent, 0) as ContentPresenter;
            FrameworkElement candidate = VisualTreeHelper.GetChild(editorPresenter, 0) as FrameworkElement;
            FrameworkElement editor = candidate.FindName("PART_editor") as FrameworkElement;

            return editor;
        }

        public FrameworkElement TryFindPropertyItemContainer(PropertyItem property)
        {
            CategoryItem category = this.Categories.OfType<CategoryItem>().Where(item => (item.Name == property.Category)).FirstOrDefault();

            ItemsControl categoriesList = this.Template.FindName("PART_items", this) as ItemsControl;
            ContentPresenter categoryPresenter = categoriesList.ItemContainerGenerator.ContainerFromItem(category) as ContentPresenter;
            Expander categoryExpander = VisualTreeHelper.GetChild(categoryPresenter, 0) as Expander;

            // Expander should be expanded only if the required editor is found indeed,
            // maybe an argument should be expected determining whether to expand the category or not...
            if (!categoryExpander.IsExpanded) categoryExpander.IsExpanded = true;

            ItemsControl categoryItemsList = categoryExpander.FindName("PART_items") as ItemsControl;
            ContentPresenter propertyPresenter = categoryItemsList.ItemContainerGenerator.ContainerFromItem(property) as ContentPresenter;

            if (propertyPresenter == null)
                return null;

            FrameworkElement grid = VisualTreeHelper.GetChild(propertyPresenter, 0) as FrameworkElement;


            return grid;
        }

        public FrameworkElement TryFindContent(PropertyItem property)
        {
            CategoryItem category = this.Categories.OfType<CategoryItem>().Where(item => (item.Name == property.Category)).FirstOrDefault();

            ItemsControl categoriesList = this.Template.FindName("PART_items", this) as ItemsControl;
            ContentPresenter categoryPresenter = categoriesList.ItemContainerGenerator.ContainerFromItem(category) as ContentPresenter;
            Expander categoryExpander = VisualTreeHelper.GetChild(categoryPresenter, 0) as Expander;

            // Expander should be expanded only if the required editor is found indeed,
            // maybe an argument should be expected determining whether to expand the category or not...
            if (!categoryExpander.IsExpanded) categoryExpander.IsExpanded = true;

            ItemsControl categoryItemsList = categoryExpander.FindName("PART_items") as ItemsControl;
            ContentPresenter propertyPresenter = categoryItemsList.ItemContainerGenerator.ContainerFromItem(property) as ContentPresenter;
            if (propertyPresenter == null)
                return null;

            FrameworkElement grid = VisualTreeHelper.GetChild(propertyPresenter, 0) as FrameworkElement;
            ContentControl propertyContent = grid.FindName("PART_content") as ContentControl;

            //return propertyContent;

            ContentPresenter editorPresenter = VisualTreeHelper.GetChild(propertyContent, 0) as ContentPresenter;
            FrameworkElement candidate = VisualTreeHelper.GetChild(editorPresenter, 0) as FrameworkElement;

            return candidate;
        }

        public void HidePropertyDisplayName(PropertyItem property)
        {
            CategoryItem category = this.Categories.OfType<CategoryItem>().Where(item => (item.Name == property.Category)).FirstOrDefault();

            ItemsControl categoriesList = this.Template.FindName("PART_items", this) as ItemsControl;
            ContentPresenter categoryPresenter = categoriesList.ItemContainerGenerator.ContainerFromItem(category) as ContentPresenter;
            Expander categoryExpander = VisualTreeHelper.GetChild(categoryPresenter, 0) as Expander;

            // Expander should be expanded only if the required editor is found indeed,
            // maybe an argument should be expected determining whether to expand the category or not...
            if (!categoryExpander.IsExpanded) categoryExpander.IsExpanded = true;

            ItemsControl categoryItemsList = categoryExpander.FindName("PART_items") as ItemsControl;
            ContentPresenter propertyPresenter = categoryItemsList.ItemContainerGenerator.ContainerFromItem(property) as ContentPresenter;

            FrameworkElement grid = VisualTreeHelper.GetChild(propertyPresenter, 0) as FrameworkElement;
            ContentControl propertyContent = grid.FindName("PART_content") as ContentControl;

            TextBlock textblock = VisualTreeHelper.GetChild(grid, 0) as TextBlock;
            textblock.Visibility = Visibility.Collapsed;

            int col = Grid.GetColumn(propertyContent);
            Grid.SetColumn(propertyContent, 0);
            Grid.SetColumnSpan(propertyContent, 2);
        }

        /// <summary>
        /// 카테고리 컨트롤 찾기
        /// </summary>
        /// <param name="categoryItem"></param>
        /// <returns></returns>
        public FrameworkElement TryFindCategoryControl(CategoryItem categoryItem)
        {
            CategoryItem category = this.Categories.OfType<CategoryItem>().Where(item => (item.Name == categoryItem.Name)).FirstOrDefault();

            ItemsControl categoriesList = this.Template.FindName("PART_items", this) as ItemsControl;
            ContentPresenter categoryPresenter = categoriesList.ItemContainerGenerator.ContainerFromItem(category) as ContentPresenter;
            Expander categoryExpander = VisualTreeHelper.GetChild(categoryPresenter, 0) as Expander;

            if (categoryPresenter != null)
            {
                // Expander should be expanded only if the required editor is found indeed,
                // maybe an argument should be expected determining whether to expand the category or not...
                if (!categoryExpander.IsExpanded)
                    categoryExpander.IsExpanded = true;

                return categoryExpander;
            }

            return null;
        }

        /// <summary>
        /// 카테고리 컨트롤 찾기
        /// </summary>
        /// <param name="categoryName"></param>
        /// <returns></returns>
        public FrameworkElement TryFindCategoryControl(string categoryName)
        {
            CategoryItem categoryItemme = this.Categories.OfType<CategoryItem>().Where(category => category.Name.Equals(categoryName) == true).FirstOrDefault();
            if (categoryItemme == null)
            {
                return null;
            }

            FrameworkElement frameworkElement = this.TryFindCategoryControl(categoryItemme);
            if (frameworkElement == null)
            {
                return null;
            }

            return frameworkElement;
        }

        /// <summary>
        /// 카테고리 영역 숨김 설정
        /// </summary>
        /// <param name="categoryName"></param>
        /// <returns></returns>
        public bool TryHideCategoryControl(string categoryName)
        {
            CategoryItem categoryItemme = this.Categories.OfType<CategoryItem>().Where(category => category.Name.Equals(categoryName) == true).FirstOrDefault();
            if (categoryItemme == null)
            {
                return false;
            }

            ItemsControl categoriesList = this.Template.FindName("PART_items", this) as ItemsControl;
            ContentPresenter categoryPresenter = categoriesList.ItemContainerGenerator.ContainerFromItem(categoryItemme) as ContentPresenter;
            Expander categoryExpander = VisualTreeHelper.GetChild(categoryPresenter, 0) as Expander;

            if (categoryPresenter != null)
            {
                if (!categoryExpander.IsExpanded)
                    categoryExpander.IsExpanded = true;

                categoryExpander.Visibility = System.Windows.Visibility.Collapsed;
                return true;
            }

            return false;
        }

        public void TryHideAllCategoryControl()
        {
            var categoryItems = this.Categories.OfType<CategoryItem>();

            foreach (var item in categoryItems)
            {
                ItemsControl categoriesList = this.Template.FindName("PART_items", this) as ItemsControl;
                ContentPresenter categoryPresenter = categoriesList.ItemContainerGenerator.ContainerFromItem(item) as ContentPresenter;
                Expander categoryExpander = VisualTreeHelper.GetChild(categoryPresenter, 0) as Expander;

                if (categoryPresenter != null)
                {
                    categoryExpander.IsExpanded = false;
                    categoryExpander.Visibility = System.Windows.Visibility.Collapsed;                    
                }
            }           
        }

        public void TryShowAllCategoryControl()
        {
            var categoryItems = this.Categories.OfType<CategoryItem>();

            foreach (var item in categoryItems)
            {
                ItemsControl categoriesList = this.Template.FindName("PART_items", this) as ItemsControl;
                ContentPresenter categoryPresenter = categoriesList.ItemContainerGenerator.ContainerFromItem(item) as ContentPresenter;
                Expander categoryExpander = VisualTreeHelper.GetChild(categoryPresenter, 0) as Expander;

                if (categoryPresenter != null)
                {
                    categoryExpander.IsExpanded = true;
                    categoryExpander.Visibility = System.Windows.Visibility.Visible;
                }
            }
        }

        /// <summary>
        /// 카테고리 영역 표시 설정
        /// </summary>
        /// <param name="categoryName"></param>
        /// <returns></returns>
        public bool TryShowCategoryControl(string categoryName)
        {
            CategoryItem categoryItemme = this.Categories.OfType<CategoryItem>().Where(category => category.Name.Equals(categoryName) == true).FirstOrDefault();
            if (categoryItemme == null)
            {
                return false;
            }

            ItemsControl categoriesList = this.Template.FindName("PART_items", this) as ItemsControl;
            ContentPresenter categoryPresenter = categoriesList.ItemContainerGenerator.ContainerFromItem(categoryItemme) as ContentPresenter;
            Expander categoryExpander = VisualTreeHelper.GetChild(categoryPresenter, 0) as Expander;

            if (categoryPresenter != null)
            {
                if (!categoryExpander.IsExpanded)
                    categoryExpander.IsExpanded = true;

                categoryExpander.Visibility = System.Windows.Visibility.Visible;
                return true;
            }

            return false;
        }

        /// <summary>
        /// Property(속서) Label 영역의 컨트롤 표시 여부 설정
        /// </summary>
        /// <param name="propertyName"></param>
        /// <param name="isShow"></param>
        public void TryShowHidePropertyLabelControl(string propertyName, bool isShow)
        {
            PropertyItem propertyIteme = this.Properties.OfType<PropertyItem>().Where(proeprty => proeprty.PropertyName.Equals(propertyName) == true).FirstOrDefault();
            if (propertyIteme == null)
            {
                return ;
            }

            propertyIteme.IsLabelVisible = isShow;
            return;                       
        }

        public FrameworkElement TryGetPropertyItemControl(string propertyName)
        {
            PropertyItem propertyIteme = this.Properties.OfType<PropertyItem>().Where(property => property.PropertyName.Equals(propertyName) == true).FirstOrDefault();
            if (propertyIteme == null)
            {
                return null;
            }

            FrameworkElement frameworkElement = this.TryFindContent(propertyIteme);

            return frameworkElement;
        }

        public void TryShowHidePropertyItemContainer(string propertyName, bool isShowControl, bool isReadOnly)
        {
            PropertyItem propertyIteme = this.Properties.OfType<PropertyItem>().Where(property => property.PropertyName.Equals(propertyName) == true).FirstOrDefault();
            if (propertyIteme == null)
            {
                return ;
            }

            FrameworkElement frameworkElement = this.TryFindPropertyItemContainer(propertyIteme);
            if (frameworkElement == null)
            {
                return ;
            }

            if (isShowControl == true)
            {
                frameworkElement.Visibility = System.Windows.Visibility.Visible;
                if (isReadOnly == true)
                    frameworkElement.IsEnabled = false;
                else
                    frameworkElement.IsEnabled = true;
            }
            else
            {
                frameworkElement.Visibility = System.Windows.Visibility.Collapsed;
            }

            return ;
        }

        public void TryHideAllPropertyItemContainer()
        {
            //_PropertyGrid.UpdateLayout();

            List<PropertyItem> propertyItems = this.Properties.OfType<PropertyItem>().ToList();
            if (propertyItems == null || propertyItems.Count <= 0)
            {
                return;
            }
            foreach (var p_item in propertyItems)
            {
                FrameworkElement fe = this.TryFindPropertyItemContainer(p_item);
                if (fe == null)
                    continue;
                fe.Visibility = System.Windows.Visibility.Collapsed;
            }
            //propertyItems.ForEach(o => this.TryFindPropertyItemContainer(o).Visibility = System.Windows.Visibility.Hidden);           

            return;
        }

        public void TryShowAllPropertyItemContainer()
        {
            //_PropertyGrid.UpdateLayout();

            List<PropertyItem> propertyItems = this.Properties.OfType<PropertyItem>().ToList();
            if (propertyItems == null || propertyItems.Count <= 0)
            {
                return;
            }
            foreach (var p_item in propertyItems)
            {
                FrameworkElement fe = this.TryFindPropertyItemContainer(p_item);
                if (fe == null)
                    continue;
                fe.Visibility = System.Windows.Visibility.Visible;
            }
            //propertyItems.ForEach(o => this.TryFindPropertyItemContainer(o).Visibility = System.Windows.Visibility.Hidden);           

            return;
        }
     
        #endregion

        #region Language 설정
        public static void setCultureInfo(CultureInfo info, ObjectDataProvider resourceProvider)
        {
            if (info != null)
            {
                Cultures.Resources.Culture = info;
                CultureResources.ChangeCulture(info);
                Thread.CurrentThread.CurrentCulture = info;
            }

            Cultures.CultureResources.ResourceProvider = resourceProvider;
        }
        #endregion
    }
}
