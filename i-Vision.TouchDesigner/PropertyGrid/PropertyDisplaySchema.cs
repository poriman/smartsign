﻿using DenisVuyka.Controls.PropertyGrid.Data;

namespace DenisVuyka.Controls.PropertyGrid
{
  public sealed class PropertyDisplaySchema : DisplaySchema<PropertyItem>
  {
    public PropertyDisplaySchema(DisplaySchemaModes mode) : base(mode) { }
  }
}
