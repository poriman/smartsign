﻿using System.Windows;
using DenisVuyka.Controls.PropertyGrid.Data;

namespace DenisVuyka.Controls.PropertyGrid
{
  public delegate void PropertyValueChangedEventHandler(object sender, PropertyValueChangedEventArgs e);

  public sealed class PropertyValueChangedEventArgs : RoutedEventArgs
  {
    private readonly object oldValue;
    private readonly object newValue;
    private readonly PropertyItem property;

    public object OldValue { get { return oldValue; } }
    public object NewValue { get { return newValue; } }
    public PropertyItem Property { get { return property; } }

    public PropertyValueChangedEventArgs(RoutedEvent routedEvent, PropertyItem property, object oldValue)
      : base(routedEvent, property)
    {
      this.property = property;
      this.newValue = property.PropertyValue;
      this.oldValue = oldValue;
    }
  }
}
