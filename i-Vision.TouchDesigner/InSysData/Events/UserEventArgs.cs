﻿using System;
using System.Windows;

namespace InSysTouchflowData.Events
{
    public class ItemSelectedEventArgs : RoutedEventArgs
    {
        private DependencyObject _dependencyObjectProperty;
        private object _sendSource;
        private object _canvasView;

        public object DependencyObjectProperty
        {
            get { return _dependencyObjectProperty; }
            set { value = _dependencyObjectProperty; }
        }

        public object SendSource
        {
            get { return _sendSource; }
            set { value = _sendSource; }
        }

        public object CanvasView
        {
            get { return _canvasView; }
            set { value = _canvasView; }
        }

        public ItemSelectedEventArgs(RoutedEvent routedEvent, DependencyObject dependencyObject, object sendSource, object canvasView)
            : base(routedEvent)
        {
            this._sendSource = sendSource;
            this._dependencyObjectProperty = dependencyObject;
            this._canvasView = canvasView;
        }
    }

    /// <summary>
    /// DesigneView 업데이트 EventArgs
    /// </summary>
    public class DesignView_UpdateEventArgs : EventArgs
    {
        private object _view;
        private object _pageInfo;

        public object View
        {
            get { return _view; }
            set { value = _view; }
        }

        public object PageInfo
        {
            get { return _pageInfo; }
            set { value = _pageInfo; }
        }

        public DesignView_UpdateEventArgs(object view, object pageinfo)
        {
            this._view = view;
            this._pageInfo = pageinfo;
        }
    }

    /// <summary>
    /// DesigneView 업데이트 EventArgs
    /// </summary>
    public class ItemUpdatedPropertyEventArgs : RoutedEventArgs
    {
        private object _sender;
        private string _propertyName;

        public object Sender
        {
            get { return _sender; }
            set { value = _sender; }
        }

        public string PropertyName
        {
            get { return _propertyName; }
            set { value = _propertyName; }
        }

        public ItemUpdatedPropertyEventArgs(RoutedEvent routedEvent, object sender, string propertyName)
            : base(routedEvent)
        {
            this._sender = sender;
            this._propertyName = propertyName;
        }
    }

    /// <summary>
    /// Flow View에서 Page 선택시 EventArgs
    /// </summary>
    public class FlowView_SelectedPageEventArgs : EventArgs
    {        
        private object _pageInfo;

     
        public object PageInfo
        {
            get { return _pageInfo; }
            set { value = _pageInfo; }
        }

        public FlowView_SelectedPageEventArgs(object pageinfo)
        {
            this._pageInfo = pageinfo;
        }
    }

    public class DesignView_InsertedPageEventArgs : EventArgs
    {
        private object _pageInfo;


        public object PageInfo
        {
            get { return _pageInfo; }
            set { value = _pageInfo; }
        }

        public DesignView_InsertedPageEventArgs(object pageinfo)
        {
            this._pageInfo = pageinfo;
        }
    }

    public class FlowView_CopyPageEventArgs : EventArgs
    {
        private object _copyPageInfo;
        public object CopyPageInfo
        {
            get { return _copyPageInfo; }
            set { value = _copyPageInfo; }
        }

        private object _selectedPageInfo;
        public object SelectedPageInfo
        {
            get { return _selectedPageInfo; }
            set { value = _selectedPageInfo; }
        }

        public FlowView_CopyPageEventArgs(object copyPageInfo, object selectedPageInfo)
        {
            this._copyPageInfo = copyPageInfo;
            this._selectedPageInfo = selectedPageInfo;
        }
    }

    public class TouchProjectInfoEventArgs : EventArgs
    {
        private string _width;
        public string Width
        {
            get { return _width; }
            set { _width = value; }
        }
        private string _height;
        public string Height
        {
            get { return _height; }
            set { _height = value; }
        }

        private double _projectTime;
        public double PageTime
        {
            get { return _projectTime; }
            set { _projectTime = value; }
        }

        public TouchProjectInfoEventArgs(string Width, string Height, double projectTime)
        {
            this._width = Width;
            this._height = Height;
            this._projectTime = projectTime;
        }
    }
}
