﻿
namespace InSysTouchflowData
{
    /// <summary>
    /// 스크롤링 텍스트 컴포넌트에서 사용하는 스크롤링 방향
    /// </summary>
    public enum ScrollTextDirection
    {
        /// <summary>
        /// 왼쪽에서 오른쪽
        /// </summary>
        LeftToRight,
        /// <summary>
        /// 오른쪽에서 왼쪽
        /// </summary>
        RightToLeft,
        /// <summary>
        /// 위에서 아래
        /// </summary>
        TopToBottom,
        /// <summary>
        /// 아래에서 위
        /// </summary>
        BottomToTop
    }

    public enum CompType
    {
        Image,
        Media,
        Streaming,
        Quicktime,
        Ppt,
        Text,
        Label,
        Scrolling_text,
        Flash,
        Rss,
        Web,
        Rectagle,
        Digitalclock,
        Ellipse,
        Audio,
        Analogue,
        RectArea,
        Button,
        SlideViewer,
        Unknown
    };

    public enum InSysDelegateType
    {
        DesignView_UpdatedPage = 0, //Design View 업데이트 Event.
        DesignView_InsertedPage,
        FlowView_SelectedPage, //Flow View에서 Page 선택시 Event.
        Request_AllPageInfo,
        FlowView_CopyPage
    }

    public enum AlignmentType
    {
        AlignLeft,
        AlignHorizontalCenter,
        AlignRight,
        AlignTop,
        AlignVerticalCenter,
        AlignBottom,
        DistributeHorizontal,
        DistributeVertical,
        FixedViewSize
    }

    public enum ZoomType
    {
        Custom,
        FixedPage
    }

    public enum TimelineType
    {
        Page,
        Control
    }

    public enum ViewState
    {
        Design,
        Play
    }

    public enum InSysControlType
    {
        None,
        InSysBasicButton,
        InSysImageBox,
        InSysRectangle,
        InSysBasicTextBox,
        InSysLabel,
        InSysVideoBox,
        InSysStreamingBox,
        InSysAudioBox,
        InSysAnalogueClock,
        InSysDate,
        InSysDigitalClock,
        InSysEllipse,
        InSysRSS,
        InSysScrollText,
        InSysWebControl,
        InSysRectArea,
        InSysFlashBox,
        InSysSlideViewer
    }

    public enum MediaType
    {
        Image,
        Video,
        Streaming,
        Ppt,
        Text,
        Flash,
        Rss,
        Web,
        Shape,
        Clock,
        Audio,
        Date,
        Control,
        Unknown
    };

    public enum CategoryType
    {
        Common,
        Layout,
        Brushes,
        Appearance,
        Text,
        AspectRatio,
        Time,
        Media,
        Event
    }

    public enum FitToPage
    {
        None,
        Full,
        Vertical,
        Horizontal
    }

    public enum Alignment
    {
        None,
        Top,
        HorizontalCenter,
        Bottom,
        Left,
        VerticalCenter,
        Right
    }

    //public enum Highlight { Diffuse, Elliptical }
   
    public enum PageEffect
    {
        None,
        Effect1,
        Effect2,
        Effect3,
        Effect4
    }

    public enum ActionEventType
    {
        GoToPage,
        ShowHide,
        Hyperlink,
        OpenFile
    }

    public enum ShowHideType
    {
        Show,
        Hide,
        Toggle
    }

    public enum ResizeDirection
    {
        Left,
        Right,
        Bottom,
        Top,
        TopLeft,
        BottomLeft,
        TopRight,
        BottomRight, 
        All
    }
}
