﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using InSysTouchflowData.InSysProperties;
using System.Windows.Controls;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.IO;
using System.Xml.Serialization;
using UtilLib.StaticMethod;

namespace InSysTouchflowData.Models.ProjectModels
{
    [Serializable()]
    public class TouchPageInfo : ViewModelBase, ISerializable
    {
        #region private field

        private Image _pageImage;
        private List<TouchContentInfo> _contents;        
        private PagePropertyObject _pageProperty;
        private int _pageOrderLayerIndex;//페이지 실행 계층 인덱스
        private int _pageDisplayOrder;       
        private string _pageGuid;
        private string _parentPageGuid;
        private List<object> _actionEventTargets;
        private double _timeDuration;       

        #endregion

        #region Members, Properites

        [XmlAttributeAttribute("width")]       
        [XmlIgnoreAttribute()]
        public double ViewWidthTemp
        {
            get { return PageProperty.PageWidth; }
            set
            {
                PageProperty.PageWidth = value;
                OnPropertyChanged("ViewWidthTemp");
            }
        }

        [XmlAttributeAttribute("height")]        
        [XmlIgnoreAttribute()]
        public double ViewHeightTemp
        {
            get { return PageProperty.PageHeight; }
            set
            {
                PageProperty.PageHeight = value;
                OnPropertyChanged("ViewHeightTemp");
            }
        }

        [XmlAttributeAttribute("opacity")]        
        [XmlIgnoreAttribute()]
        public double OpacityTemp
        {
            get { return PageProperty.Opacity; }
            set
            {
                PageProperty.Opacity = value;
                OnPropertyChanged("OpacityTemp");
            }
        }

        [XmlAttributeAttribute("timeduration")]
        public double TimeDuration
        {
            get { return this._timeDuration; }
            set { this._timeDuration = value; /*OnPropertyChanged("TimeDuration"); */}
        }

        [XmlAttributeAttribute("pagelifetime")]        
        [XmlIgnoreAttribute()]
        public double PageLifeTimeTemp
        {
            get { return PageProperty.PageLifeTime.TotalSeconds; }
            set
            {
                PageProperty.PageLifeTime = TimeSpan.FromSeconds(value);
                OnPropertyChanged("PageLifeTimeTemp");
            }
        }

        [XmlAttribute("isapplylifetime")]        
        [XmlIgnoreAttribute()]
        public bool IsApplyPageLifeTime
        {
            get
            {
                return PageProperty.IsApplyPageLifeTime;
            }
            set
            {
                PageProperty.IsApplyPageLifeTime = value;
                OnPropertyChanged("IsApplyPageLifeTime");
            }
        }

        [XmlArray("contents"), XmlArrayItem("content", typeof(TouchContentInfo))]
        public List<TouchContentInfo> Contents
        {
            get { return _contents; }
            set { _contents = value; /*OnPropertyChanged("Contents");*/ }
        }

        [XmlAttributeAttribute("pagename")]        
        [XmlIgnoreAttribute()]
        public string PageNameTemp
        {
            get { return PageProperty.PageName; }
            set
            {
                PageProperty.PageName = value;
                OnPropertyChanged("PageNameTemp");
            }
        }

        [XmlIgnoreAttribute()]
        public PagePropertyObject PageProperty
        {
            get { return _pageProperty; }
            set { _pageProperty = value; /*OnPropertyChanged("ScreenProperty"); */}
        }

        [XmlIgnoreAttribute()]
        public Image PageImage
        {
            get { return _pageImage; }
            set { _pageImage = value; /*OnPropertyChanged("PageImage"); */}
        }

        [XmlIgnoreAttribute()]
        public Brush Background
        {
            get { return PageProperty.Background; }
            set {
                PageProperty.Background = value; 
                this.BackgroundXaml = ObjectConverters.ConvertXamlStringFromObject(value); 
                OnPropertyChanged("Background"); 
            }
        }

        [XmlIgnoreAttribute()]
        private string BackgroundXaml { get; set; }

        [XmlAttributeAttribute("pagelayerindex")]
        public int PageLayerIndex
        {
            get { return _pageOrderLayerIndex; }
            set { _pageOrderLayerIndex = value; /*OnPropertyChanged("PageLayerIndex");*/ }
        }

        [XmlAttributeAttribute("pagedisplayorder")]
        public int PageDisplayOrder
        {
            get { return PageProperty.DisplayOrder; }
            set 
            {
                PageProperty.DisplayOrder = value;               
                OnPropertyChanged("PageDisplayOrder");
            }
        }

        [XmlAttributeAttribute("guid")]
        public string PageGuid
        {
            get { return _pageGuid; }
            set { _pageGuid = value; /*OnPropertyChanged("PageGuid");*/ }
        }

        [XmlAttributeAttribute("parentguid")]
        public string ParentPageGuid
        {
            get { return _parentPageGuid; }
            set { _parentPageGuid = value; /*OnPropertyChanged("ParentPageGuid");*/ }
        }

        [XmlArray("actionevents"), XmlArrayItem("actionevent", typeof(object))]
        public List<object> ActionEventTargets
        {
            get { return _actionEventTargets; }
            set { _actionEventTargets = value; /*OnPropertyChanged("ActionEventTargets");*/ }
        }

        #endregion

        public TouchPageInfo()
        {
            if (PageProperty == null)//DependencyObject는 Serialize안됨.
            {
                PageProperty = new PagePropertyObject();
                //PagePropertyObject.ChangedPageNameCallback = new PropertyChangedDelegate(ChangedPageName);
            }

            _contents = new List<TouchContentInfo>();           
            _pageProperty = new PagePropertyObject();            
            _pageOrderLayerIndex = 0;
            _pageDisplayOrder = 1;
            _pageGuid = Guid.NewGuid().ToString();
            //this.ViewWidthTemp = 1200;
            //this.ViewHeightTemp = 1000;
            this.OpacityTemp = 1.0;
            //this.PageName = "Blank Page";
        }

        public TouchPageInfo(double width, double height, double pageLifeTime, string pagename)
            : this()
        {
            this.ViewWidthTemp = width;
            this.ViewHeightTemp = height;
            this.PageLifeTimeTemp = pageLifeTime;
            this.PageNameTemp = pagename;
        }

        public TouchPageInfo(string id, string parentID, double width, double height, double pageLifeTime, string pagename, int pageDepthIndex)
            : this(width, height, pageLifeTime, pagename)
        {
            this.PageGuid = id;
            this.ParentPageGuid = parentID;
            this.PageLayerIndex = pageDepthIndex;
        }

        protected TouchPageInfo(SerializationInfo info, StreamingContext context)
        {
            try
            {
                if (PageProperty == null)//DependencyObject는 Serialize안됨.
                {
                    PageProperty = new PagePropertyObject();
                    //PagePropertyObject.ChangedPageNameCallback = new PropertyChangedDelegate(ChangedPageName);
                }

                ViewWidthTemp = info.GetDouble("ViewWidthTemp");
                ViewHeightTemp = info.GetDouble("ViewHeightTemp");
                PageLifeTimeTemp = info.GetDouble("PageLifeTimeTemp");
                IsApplyPageLifeTime = info.GetBoolean("ApplyLifeTime");
                PageNameTemp = info.GetString("PageNameTemp");
                OpacityTemp = info.GetDouble("OpacityTemp");
                Contents = (List<TouchContentInfo>)info.GetValue("Contents", typeof(List<TouchContentInfo>));
                PageLayerIndex = info.GetInt32("PageLayerIndex");
                PageDisplayOrder = info.GetInt32("PageDisplayOrder");
                PageGuid = info.GetString("PageGuid");
                ParentPageGuid = info.GetString("ParentPageGuid");
                ActionEventTargets = (List<object>)info.GetValue("ActionEventTargets", typeof(List<object>));
                BackgroundXaml = info.GetString("BackgroundXaml");
                if (string.IsNullOrEmpty(BackgroundXaml) == false)
                    this.Background = ObjectConverters.ConvertBrushFromBrushXamlString(BackgroundXaml);
            }
            catch
            {
            }
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {          
            info.AddValue("ViewWidthTemp", this.ViewWidthTemp);
            info.AddValue("ViewHeightTemp", this.ViewHeightTemp);
            info.AddValue("PageLifeTimeTemp", this.PageLifeTimeTemp);
            info.AddValue("ApplyLifeTime", this.IsApplyPageLifeTime);
            info.AddValue("PageNameTemp", this.PageNameTemp);
            info.AddValue("OpacityTemp", this.OpacityTemp);
            info.AddValue("Contents", this.Contents);
            info.AddValue("PageLayerIndex", this.PageLayerIndex);
            info.AddValue("PageDisplayOrder", this.PageDisplayOrder);
            info.AddValue("PageGuid", this.PageGuid);
            info.AddValue("ParentPageGuid", this.ParentPageGuid);
            info.AddValue("ActionEventTargets", this.ActionEventTargets);
            info.AddValue("BackgroundXaml", this.BackgroundXaml);
        }

        public void SetPageInfo(TouchPageInfo pageInfo)
        {
            if (PageProperty == null)
            {
                PageProperty = new PagePropertyObject();
                //PagePropertyObject.ChangedPageNameCallback = new PropertyChangedDelegate(ChangedPageName);
            }

            this.ViewHeightTemp = pageInfo.ViewHeightTemp;
            this.ViewWidthTemp = pageInfo.ViewWidthTemp;
            this.PageLifeTimeTemp = pageInfo.PageLifeTimeTemp;
            this.PageNameTemp = pageInfo.PageNameTemp;
            this.Background = pageInfo.Background;
        }

        [OnSerializing]
        private void OnSerializing(StreamingContext sc)
        {
        }

        [OnDeserialized]
        private void OnDeserialized(StreamingContext sc)
        {

        }

        //private object ChangedPageName(object sender, object obj)
        //{
        //    string pagenameTemp = obj as string;
        //    if(string.IsNullOrEmpty(pagenameTemp) == false)
        //        this.PageName = obj as string;
        //    else
        //        this.PageName = "Blank Page";
            
        //    return null;
        //}

        public void UpdatePageInfo(FrameworkElement frameworkElement)
        {
            this._pageImage = ExportToPng(frameworkElement);
        }

        public static Image ExportToPng(FrameworkElement surface)
        {
            Transform transform = surface.LayoutTransform;// Save current canvas transform            
            surface.LayoutTransform = null;// reset current transform (in case it is scaled or rotated)

            Size size = new Size(surface.Width, surface.Height);// Get the size of canvas                      
            surface.Measure(size);// Measure and arrange the surface, VERY IMPORTANT
            surface.Arrange(new Rect(size));

            RenderTargetBitmap renderBitmap = new RenderTargetBitmap((int)size.Width, (int)size.Height, 96d, 96d, PixelFormats.Pbgra32);// Create a render bitmap and push the surface to it
            renderBitmap.Render(surface);

            MemoryStream outStream = new MemoryStream();
            PngBitmapEncoder encoder = new PngBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(renderBitmap));// push the rendered bitmap to it            
            encoder.Save(outStream);// save the data to the stream

            BitmapImage bimg = new BitmapImage();
            bimg.BeginInit();
            bimg.StreamSource = new MemoryStream(outStream.ToArray());
            bimg.EndInit();
            Image img = new Image();
            img.Source = bimg;
            img.Stretch = Stretch.Uniform;
            //img.Width = 200;

            /*
            using (FileStream outStream2 = new FileStream("testImage.png", FileMode.Create))
            {                
                PngBitmapEncoder encoder2 = new PngBitmapEncoder();               
                encoder2.Frames.Add(BitmapFrame.Create(renderBitmap));               
                encoder2.Save(outStream2);
            }
             * */

            surface.LayoutTransform = transform;// Restore previously saved layout

            return img;
        }

        public void AddControlContent(FrameworkElement addElement, Dictionary<string, object> properties)
        {
            var item = this.Contents.Where(o => o.ContentObject.Equals(addElement) == true).FirstOrDefault();
            if (item != null)
                return;
            TouchContentInfo contentInfo = new TouchContentInfo();

            contentInfo.ContentObject = addElement;

            contentInfo.ContentType = addElement.GetType();
            contentInfo.ContentPosition = new Point(Canvas.GetLeft(addElement), Canvas.GetTop(addElement));
            contentInfo.ContentSize = new Size(addElement.Width, addElement.Height);
            contentInfo.DesignElementProperties = properties;
            contentInfo.ContentName = properties["Name"] as string;

            this.Contents.Add(contentInfo);
        }


        public void UpdateControlContent(FrameworkElement updateElement, Dictionary<string, object> properties)
        {
            TouchContentInfo contentInfo = this.Contents.Where(o => o.ContentObject.Equals(updateElement) == true).FirstOrDefault();

            if (contentInfo != null)
            {
                contentInfo.ContentPosition = new Point(Canvas.GetLeft(updateElement), Canvas.GetTop(updateElement));
                contentInfo.ContentSize = new Size(updateElement.Width, updateElement.Height);
                contentInfo.DesignElementProperties = properties;
                contentInfo.ContentName = properties["Name"] as string;
            }
        }

        private void UpdateDesignElementProperties(TouchContentInfo contentInfo, string propertyName, Dictionary<string, object> properties)
        {
            contentInfo.DesignElementProperties[propertyName] = properties;
        }

        public void UpdateControlContent(FrameworkElement updateElement, string propertyName, object propertyValue)
        {
            TouchContentInfo contentInfo = this.Contents.Where(o => o.ContentObject.Equals(updateElement) == true).FirstOrDefault();

            if (contentInfo != null)
            {                
                contentInfo.DesignElementProperties[propertyName] = propertyValue;
            }
        }

        public TouchContentInfo GetTouchContentInfo(FrameworkElement updateElement)
        {
            TouchContentInfo contentInfo = this.Contents.Where(o => o.ContentObject.Equals(updateElement) == true).FirstOrDefault();

            return contentInfo;
        }

        public TouchPageInfo CloneProperties()
        {
            TouchPageInfo clone = new TouchPageInfo();
            this.CloneProperties(clone);

            //clone.ActionEventTargets = this.ActionEventTargets;
            //clone.ApplyLifeTime = this.ApplyLifeTime;
            //clone.Opacity = this.Opacity;
            //clone.PageChildren = this.PageChildren;
            //clone.PageDisplayOrder = this.PageDisplayOrder;
            //clone.PageGuid = this.PageGuid;

            //foreach (ContentInfo contentInfo in clone.Contents)
            //{
            //    ContentInfo tempContentInfo = contentInfo.CloneProperties();
            //    clone.Contents.Add(tempContentInfo);
            //}

            return clone;
        }

        public string NewID()
        {
            return Guid.NewGuid().ToString();
        }

    }

    public static class PageInfoExtension
    {
        public static void CloneProperties(this TouchPageInfo origin, TouchPageInfo destination)
        {
            if (destination == null)
                throw new ArgumentNullException("destination", "Destination object must first be instantiated.");

            foreach (var destinationProperty in destination.GetType().GetProperties())
            {
                if (origin != null && destinationProperty.CanWrite)
                {

                    origin.GetType().GetProperties().Where(x => x.CanRead && (x.Name == destinationProperty.Name && x.PropertyType == destinationProperty.PropertyType)).ToList()
                        .ForEach(x => destinationProperty.SetValue(destination, x.GetValue(origin, null), null));

                }
            }
        }
    }
}
