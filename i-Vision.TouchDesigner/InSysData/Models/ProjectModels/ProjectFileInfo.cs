﻿
namespace InSysTouchflowData.Models.ProjectModels
{
    public class ProjectFileInfo
    {
        private string _projectName;
        private string _projectPath;
        private int _index;
        private string _accessDate;

        public string ProjectName
        {
            get
            {
                return _projectName;
            }
            set
            {
                this._projectName = value;
            }
        }

        public string ProjectPath
        {
            get
            {
                return _projectPath;
            }
            set
            {
                this._projectPath = value;
            }
        }

        public int Index
        {
            get
            {
                return _index;
            }
            set
            {
                this._index = value;
            }
        }

        public string AccessDate
        {
            get
            {
                return _accessDate;
            }
            set
            {
                this._accessDate = value;
            }
        }
    }
}
