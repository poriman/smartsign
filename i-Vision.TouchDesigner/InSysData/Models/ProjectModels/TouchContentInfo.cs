﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Windows;
using System.Xml.Serialization;

namespace InSysTouchflowData.Models.ProjectModels
{
    [Serializable()]
    public class TouchContentInfo : ISerializable
    {
        private Type _contentType;

        [XmlAttributeAttribute("contenttype")]
        public Type ContentType
        {
            get { return _contentType; }
            set { _contentType = value; }
        }

        private Dictionary<string, object> _DesignElementProperties;
        [XmlAttributeAttribute("properties")]       
        public Dictionary<string, object> DesignElementProperties
        {
            get { return _DesignElementProperties; }
            set { _DesignElementProperties = value; }
        }

        private Point _currentPosition;
         [XmlAttributeAttribute("position")]
        public Point ContentPosition
        {
            get { return _currentPosition; }
            set { _currentPosition = value; }
        }

        private Size _contentSize;
         [XmlAttributeAttribute("size")]
        public Size ContentSize
        {
            get { return _contentSize; }
            set { _contentSize = value; }
        }

        private FrameworkElement _contentObject;
        [XmlAttributeAttribute("object")]
        public FrameworkElement ContentObject
        {
            get { return _contentObject; }
            set { _contentObject = value; }
        }

        private string _contentName;
        [XmlAttributeAttribute("name")]
        public string ContentName
        {
            get { return _contentName; }
            set { _contentName = value; }
        }       

        public TouchContentInfo()
        { }

        protected TouchContentInfo(SerializationInfo info, StreamingContext context)
        {
            ContentSize = (Size)info.GetValue("ContentSize", typeof(Size));
            ContentPosition = (Point)info.GetValue("ContentPosition", typeof(Point));
            ContentType = (Type)info.GetValue("ContentType", typeof(Type));
            //LifeContentStartTime = info.GetDouble("LifeContentStartTime");
            //LifeContentEndTime = info.GetDouble("LifeContentEndTime");            
            DesignElementProperties = (Dictionary<string, object>)info.GetValue("DesignElementProperties", typeof(Dictionary<string, object>));
            ContentName = info.GetString("ContentName");
        }

        #region ISerializable Members

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("ContentSize", this.ContentSize);
            info.AddValue("ContentPosition", this.ContentPosition);
            info.AddValue("ContentType", this.ContentType);
            //info.AddValue("LifeContentStartTime", this.LifeContentStartTime);
            //info.AddValue("LifeContentEndTime", this.LifeContentEndTime);           
            info.AddValue("DesignElementProperties", this.DesignElementProperties);
            info.AddValue("ContentName", this.ContentName);                  
        }

        #endregion

        public TouchContentInfo CloneProperties()
        {            
            TouchContentInfo clone = new TouchContentInfo();
            this.CloneProperties(clone);

            //clone.ContentName = this.ContentName;
            //clone.ContentObject = this.ContentObject;
            //clone.ContentPosition = this.ContentPosition;            
            //clone.ContentSize = this.ContentSize;
            //clone.ContentType = this.ContentType;
            //clone.DesignElementProperties = this.DesignElementProperties;
            //clone.LifeContentEndTime = this.LifeContentEndTime;
            //clone.LifeContentStartTime = this.LifeContentStartTime;

            return clone;
        }

        public void update_Position()
        {
            if (this._contentObject != null)
            {
                _currentPosition.X = System.Windows.Controls.Canvas.GetLeft(this._contentObject);
                _currentPosition.Y = System.Windows.Controls.Canvas.GetTop(this._contentObject);
            }            
        }

        public static TouchContentInfo Copy_ContentInfo(FrameworkElement inSysContentControl, TouchContentInfo sourceContentInfo, double pageLifeTime)
        {
            TouchContentInfo newContentInfo = new TouchContentInfo();
            if (inSysContentControl != null)
            {
                inSysContentControl.UpdateLayout();

                #region ContentInfo 정보 Copy

                newContentInfo.ContentType = sourceContentInfo.ContentType;
                newContentInfo.ContentPosition = sourceContentInfo.ContentPosition;
                newContentInfo.ContentSize = sourceContentInfo.ContentSize;
                newContentInfo.ContentObject = inSysContentControl;
                newContentInfo.DesignElementProperties = sourceContentInfo.DesignElementProperties;
                //if (isSerialization == true)
                //    newContentInfo.DesignElementProperties = InSysBasicControls.InSysControlManager.SerializePropertiesInIDesignElement(designElement.Properties);
                //else
                //    newContentInfo.DesignElementProperties = designElement.Properties;

                newContentInfo.ContentName = sourceContentInfo.ContentName;

                #endregion

                //this.workCanvasController.Init_SetInSysControl(this.DesignContainer.TouchPageInfo, inSysContentControl, this.workCanvasController.GetProjectInfo().PageInfos);
            }

            return newContentInfo;
        }
    }

    public static class ContentInfoExtension
    {
        public static void CloneProperties(this TouchContentInfo origin, TouchContentInfo destination)
        {
            if (destination == null)
                throw new ArgumentNullException("destination", "Destination object must first be instantiated.");

            foreach (var destinationProperty in destination.GetType().GetProperties())
            {
                if (origin != null && destinationProperty.CanWrite)
                {

                    origin.GetType().GetProperties().Where(x => x.CanRead && (x.Name == destinationProperty.Name && x.PropertyType == destinationProperty.PropertyType)).ToList()
                        .ForEach(x => destinationProperty.SetValue(destination, x.GetValue(origin, null), null));

                }
            }
        }
    }
}
