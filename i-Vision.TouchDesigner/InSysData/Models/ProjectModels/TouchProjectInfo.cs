﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Xml.Serialization;

namespace InSysTouchflowData.Models.ProjectModels
{
    [Serializable()]
    [XmlRootAttribute("project", Namespace="", IsNullable=false)]
    public class TouchProjectInfo
    {
        private static TouchProjectInfo _instance;
        public static TouchProjectInfo Instance
        {
            get { return _instance; }
        }

        static TouchProjectInfo()
        {
            _instance = new TouchProjectInfo();
        }

        private TouchProjectInfo()
        {
            this._pageInfoList = new List<TouchPageInfo>();
            this._pageSize = new Size(1200, 1000);
            this._defaultPageLifeTime = 60 * 60;
        }

        private List<TouchPageInfo> _pageInfoList;

        [XmlArray("touchflowpages"), XmlArrayItem("touchflowpage", typeof(TouchPageInfo))]
        public List<TouchPageInfo> PageInfoList
        {
            get { return _pageInfoList; }
            set { _pageInfoList = value; }
        }

        private Size _pageSize;
        [XmlAttributeAttribute("pagesize")]
        public Size PageSize
        {
            get { return _pageSize; }
            set { _pageSize = value; }
        }

        private double _defaultPageLifeTime;
        [XmlAttributeAttribute("defaultlifetime")]
        public double DefaultPageLifeTime
        {
            get { return _defaultPageLifeTime; }
            set { _defaultPageLifeTime = value; }
        }

        public void AddTouchPageInfo(TouchPageInfo pageInfo)
        {
            if (this._pageInfoList != null)
            {
                if (this._pageInfoList.Contains(pageInfo) == false)
                    this._pageInfoList.Add(pageInfo);
            }
        }

        public void RemovePageInfo(TouchPageInfo deletePageInfo)
        {
            if (this._pageInfoList.Contains(deletePageInfo) == true)
            {
                this._pageInfoList.Remove(deletePageInfo);
            }
        }

        public void InitProjectInfo(Size size, double pageLifeTime)
        {
            this._pageInfoList.Clear();
            this._pageSize = size;
            this._defaultPageLifeTime = pageLifeTime;
        }

        public TouchProjectInfo CloneProperties()
        {
            TouchProjectInfo clone = new TouchProjectInfo();
            this.CloneProperties(clone);         

            return clone;
        }

        public string CheckDefaultPageName()
        {
            int index = 1;
            string name = "Page-1";
            while (true)
            {
                name = string.Format("Page-{0}", index);

                IEnumerable<TouchPageInfo> list = this._pageInfoList.Where(o => o.PageNameTemp.Equals(name) == true);
                if (list == null || list.Count() == 0)
                {
                    break;
                }           
                index++;
            }

            return name;
        }

        public string CheckPageName(string pagename)
        {
            int index = 1;
            string name = "Page-1";
            while (true)
            {
                name = string.Format("{0}-{1}", pagename, index);

                IEnumerable<TouchPageInfo> list = this._pageInfoList.Where(o => o.PageNameTemp.Equals(name) == true);
                if (list == null || list.Count() == 0)
                {
                    break;
                }
                index++;
            }

            return name;
        }
    }

    public static class ProjectInfoExtension
    {
        public static void CloneProperties(this TouchProjectInfo origin, TouchProjectInfo destination)
        {
            if (destination == null)
                throw new ArgumentNullException("destination", "Destination object must first be instantiated.");

            foreach (var destinationProperty in destination.GetType().GetProperties())
            {
                if (origin != null && destinationProperty.CanWrite)
                {

                    origin.GetType().GetProperties().Where(x => x.CanRead && (x.Name == destinationProperty.Name && x.PropertyType == destinationProperty.PropertyType)).ToList()
                        .ForEach(x => destinationProperty.SetValue(destination, x.GetValue(origin, null), null));

                }
            }
        }
    }
}
