﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.IO;

namespace InSysTouchflowData.Models.ProjectModels
{
    public class ProjectOption
    {
        private string _optionFilepath;

        #region singleton 생성자

        private static ProjectOption _instance;
        public static ProjectOption Instance
        {
            get { return _instance; }
        }

        static ProjectOption()
        {
            _instance = new ProjectOption();
        }

        private ProjectOption()
        {
            this._optionFilename = "ProjectSetting.xml";
            this._optionFilepath = string.Format("{0}{1}", AppDomain.CurrentDomain.BaseDirectory, this._optionFilename);

            CreateProjectOptionFormat(this._optionFilepath);            
        }
        
        #endregion     

        private string _optionFilename;
        private XElement _rootElement;
        
        private ProjectOption _projectOptionManager;
        public ProjectOption ProjectOptionManager
        {
            get { return _projectOptionManager; }
            set { _projectOptionManager = value; }
        }

        public void CreateProjectOptionFormat(string path)
        {
            if (File.Exists(path) == false)
            {
                _rootElement = new XElement("project");                
                XElement resent = new XElement("resent");               
                _rootElement.Add(resent);
                _rootElement.Save(path);
            }
            else
            {
                _rootElement = XElement.Load(path);
            }
        }

        public void AddResentProject(string path)
        {
            FileInfo fi = new FileInfo(path);
            if (_rootElement != null)
            {
                string name = fi.Name.Replace(fi.Extension, "");
                string fullpath = fi.FullName;
                int index = 1;
                XElement resent = this._rootElement.Element("resent");
                if (resent != null)
                {
                    var resentList = resent.Elements("resent_project");

                    var sameItem = resentList.Where(o => o.Attribute("path").Value.Equals(fullpath) == true || o.Attribute("path").Value.ToLower().Equals(fullpath.ToLower()) == true).FirstOrDefault();
                    if (sameItem != null)
                    {
                        int findIndex = int.Parse(sameItem.Attribute("index").Value);
                        sameItem.Remove();
                        
                        resentList.Where(o => int.Parse(o.Attribute("index").Value) > findIndex).ToList().ForEach(o => o.Attribute("index").Value = (int.Parse(o.Attribute("index").Value) - 1).ToString());
                        
                    }
                    if (resentList.Count() >= 5)
                    {
                        List<XElement> delList = new List<XElement>();
                        foreach (var element in resentList)
                        {
                            int i = int.Parse(element.Attribute("index").Value);
                            if (i < 5)
                            {
                                element.Attribute("index").Value = (i + 1).ToString();
                            }
                            else
                            {                               
                                delList.Add(element);
                            }
                        }
                        foreach (var del in delList)
                            del.Remove();
                    }
                    else
                    {
                        resentList.ToList().ForEach(o => o.Attribute("index").Value = (int.Parse(o.Attribute("index").Value) + 1).ToString());                        
                    }
                }
                else
                {
                    resent = new XElement("resent");
                    this._rootElement.Add(resent);
                }               
               
                resent.Add(new XElement("resent_project", new XAttribute("name", name), new XAttribute("path", fullpath), new XAttribute("index", index), new XAttribute("date", DateTime.Now.ToString())));
                
                //resent.Elements("resent_project").ToList().OrderBy(o => o.Attribute("index").Value);

                _rootElement.Save(this._optionFilepath);
            }
        }

        public List<ProjectFileInfo> GetProjectFileInfoList()
        {
            List<ProjectFileInfo> list = new List<ProjectFileInfo>();
            var resent = this._rootElement.Element("resent");
            if (resent == null)
                return null;
            var resentList = resent.Elements("resent_project");

            foreach (var item in resentList)
            {
                ProjectFileInfo info = new ProjectFileInfo();
                XAttribute att = item.Attribute("name");
                if(att != null)
                    info.ProjectName = att.Value;

                att = item.Attribute("path");
                if (att != null)
                    info.ProjectPath = att.Value;

                att = item.Attribute("index");
                if (att != null)
                    info.Index = int.Parse(att.Value);

                list.Add(info);
            }

            return list;
        }
    }
}
