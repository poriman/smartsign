﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InSysTouchflowData.Models.ActionEvents
{
    [Serializable()]
    public class ActionEvent
    {
        public string EventName { get; set; }
        public ActionEventType ActionEventType { get; set; }

        public ActionEvent()
        {
        }

        public ActionEvent(string eventName, ActionEventType actEventType)
            : this()
        {
            EventName = eventName;
            ActionEventType = actEventType;
        }
    }

   
    [Serializable()]
    public class MovePageItem
    {
        public string PageID { get; set; }
        public string PageName { get; set; }

        public MovePageItem()
        {
        }

        public MovePageItem(string pageID, string pageName)
        {
            this.PageID = pageID;
            this.PageName = pageName;
        }
    }

    [Serializable()]
    public class GoToPageEvent : ActionEvent
    {
        public MovePageItem SelectedPageItem { get; set; }

        public GoToPageEvent()
        {
        }

        public GoToPageEvent(string eventName, ActionEventType actEventType)
            : base(eventName, actEventType)
        {
            this.SelectedPageItem = new MovePageItem();
        }
    }

    [Serializable()]
    public class ShowHideEvent : ActionEvent
    {
        public string SelectedItemName { get; set; }
        public ShowHideType Type
        {
            get;
            set;
        }

        public ShowHideEvent()
        {
        }

        public ShowHideEvent(string eventName, ActionEventType actEventType)
            : base(eventName, actEventType)
        {
            this.Type = ShowHideType.Show;
        }
    }

    [Serializable()]
    public class HyperlinkEvent : ActionEvent
    {
        public string URL { get; set; }

        public HyperlinkEvent()
        {
        }

        public HyperlinkEvent(string eventName, ActionEventType actEventType)
            : base(eventName, actEventType)
        {
        }
    }

    [Serializable()]
    public class OpenFileEvent : ActionEvent
    {
        public string OpenFile { get; set; }

        public OpenFileEvent()
        {
        }

        public OpenFileEvent(string eventName, ActionEventType actEventType)
            : base(eventName, actEventType)
        {
        }
    }
}
