﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InSysTouchflowData.Models.ActionEvents
{
    [Serializable()]
    public class ProgramAction
    {
        public string ActionName { get; set; }
        public ProgramActionType ProgramActionType { get; set; }
        
        public ProgramAction()
        {
        }

        public ProgramAction(string actionName, ProgramActionType actEventType)
            : this()
        {
            ActionName = actionName;
            ProgramActionType = actEventType;
        }
    }

    public enum ProgramActionType
    {        
        //Load,
        Close
    }

    [Serializable()]
    public class PageCloseItem
    {
        public string PageID { get; set; }
        public string PageName { get; set; }

        public PageCloseItem()
        {
        }

        public PageCloseItem(string pageID, string pageName)
        {
            this.PageID = pageID;
            this.PageName = pageName;
        }
    }

    [Serializable()]
    public class PageCloseAction : ProgramAction
    {
        public PageCloseItem SelectedPageCloseItem { get; set; }

        public PageCloseAction()
        {
        }

        public PageCloseAction(string actionName, ProgramActionType actEventType)
            : base(actionName, actEventType)
        {
            this.SelectedPageCloseItem = new PageCloseItem();
        }
    }
}
