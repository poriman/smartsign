﻿using System.Collections.Generic;
using System.Windows;
using InSysTouchflowData.Models.ProjectModels;

namespace InSysTouchflowData
{
    public delegate object PropertyChangedDelegate(object sender, DependencyPropertyChangedEventArgs eventArgs);
    public delegate object InsertPageDelegate(FrameworkElement desingerItem);
    public delegate void InsertPageOnFlowViewDelegate(TouchPageInfo pageInfo);
    public delegate void ArrangeItemsFlowViewDelegate();
    public delegate bool InsertInSysControlDelegate(FrameworkElement insysControlItem, Dictionary<string, object> properties);
    public delegate bool ActionEventDelegate(InSysDelegateType actionEventType, object sendData);
    public delegate void CloseViewDelegate(object sendObj);
    public delegate void ClosePreviewDelegate();
    public delegate object ContnrolOfPropertyDelegate(string propertyName);    
    public delegate TouchProjectInfo GetProjectInfoDelegate();
    public delegate void RaiseActionEventDelegate(object parameter);
    public delegate void RaiseDoContentMouseClickTouchEventDelegate(FrameworkElement targetElement);//Touch Event Delegate
    public delegate void GoToPageActionEventDelegate(string movingPageID, object currentPageViewVM);
    public delegate void ShowHideActionEventDelegate(object targetControl, object showhideType, object currentPageViewVM);
    public delegate void HyperlinkActionEventDelegate(string url);
    public delegate void OpenFileActionEventDelegate(string openfile);
    public delegate void SelectedInSysControlItemDelegate(FrameworkElement designerItem);
    public delegate void ChangeControlTimeDurationDelegate(FrameworkElement changeControl, double changedValue);
    public delegate void ControlChangePropertyDelegate(FrameworkElement frameworkElement, string propertyName, object value);
    public delegate void ActionByUserDelegate();
    public delegate bool ChangePageNameDelegate(string PageGuid, string changedName);
    public delegate void WaitingProgressDelegate(bool isWaiting);
    public delegate void TouchDownDelegate();
   
}
