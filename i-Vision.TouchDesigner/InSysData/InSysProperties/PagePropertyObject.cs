﻿using System;
using System.Windows;
using System.ComponentModel;
using DenisVuyka.Controls.PropertyGrid.Attributes;
using System.Collections.Generic;
using System.Windows.Media;
using DenisVuyka.Controls.PropertyGrid;
using DenisVuyka.Controls.PropertyGrid.Editors;

namespace InSysTouchflowData.InSysProperties
{
    [Serializable()]
    public class PagePropertyObject : DependencyObject
    {
        public static PropertyChangedDelegate PageLifeTimeCallback;
        public static PropertyChangedDelegate AutoPageCallback;
        public static PropertyChangedDelegate ChangedPageNameCallback;
        public static PropertyChangedDelegate PagePropertyChanged;

        #region Layout Category

        #region Width

        public static readonly DependencyProperty PageWidthProperty = DependencyProperty.Register("PageWidth", typeof(double), typeof(PagePropertyObject),
            new PropertyMetadata());

        [Category("Layout")]
        //[DisplayName("Width")]
        [LocalizedDisplayName("Width")]
        [PropertyOrder(1)]
        public double PageWidth
        {
            get { return (double)GetValue(PageWidthProperty); }
            set
            {
                string strValue = value.ToString("####");
                if (string.IsNullOrEmpty(strValue) != true)
                {
                    double changedValue = Convert.ToDouble(strValue);
                    SetValue(PageWidthProperty, changedValue);
                }
            }
        }     

        #endregion

        #region Height

        public static readonly DependencyProperty PageHeightProperty = DependencyProperty.Register("PageHeight", typeof(double), typeof(PagePropertyObject),
            new PropertyMetadata());

        [Category("Layout")]
        //[DisplayName("Height")]
        [LocalizedDisplayName("Height")]
        [PropertyOrder(2)]
        public double PageHeight
        {
            get { return (double)GetValue(PageHeightProperty); }
            set
            {
                string strValue = value.ToString("####");
                if (string.IsNullOrEmpty(strValue) != true)
                {
                    double changedValue = Convert.ToDouble(strValue);
                    SetValue(PageHeightProperty, changedValue);
                }
            }
        }
        
        #endregion      

        #endregion

        #region Appearance Category

        #region Opacity Category
        public static readonly DependencyProperty OpacityProperty = DependencyProperty.Register("Opacity", typeof(double), typeof(PagePropertyObject), new PropertyMetadata(0.5));

        [Category("Appearance")]
        //[DisplayName("Opacity")]
        [LocalizedDisplayName("Opacity")]
        [PropertyOrder(1)]
        public double Opacity
        {
            get { return (double)GetValue(OpacityProperty); }
            set { SetValue(OpacityProperty, value); }
        }

        #endregion       

        #endregion

        #region Common Category

        #region Type

        public static readonly DependencyProperty TypeProperty = DependencyProperty.Register("Type", typeof(Type), typeof(PagePropertyObject), new PropertyMetadata());

        [Category("Common")]
        //[DisplayName("Type")]
        [LocalizedDisplayName("Type")]
        [PropertyOrder(1)]
        public Type Type
        {
            get { return (Type)GetValue(TypeProperty); }
            set { SetValue(TypeProperty, value); }
        }
        #endregion

        #region PageElements
        public static readonly DependencyProperty PageElementsProperty = DependencyProperty.Register("PageElements", typeof(List<FrameworkElement>), typeof(PagePropertyObject), new PropertyMetadata());

        [Category("Common")]
        [LocalizedDisplayName("Page Elements")]
        [PropertyOrder(5)]
        public List<FrameworkElement> PageElements
        {
            get { return (List<FrameworkElement>)GetValue(PageElementsProperty); }
            set { SetValue(PageElementsProperty, value); }
        }
        #endregion  
      
        #region PageName

        public static readonly DependencyProperty PageNameProperty = DependencyProperty.Register("PageName", typeof(string), typeof(PagePropertyObject), new PropertyMetadata("", new PropertyChangedCallback(Changed_PageName)));
        private static void Changed_PageName(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (ChangedPageNameCallback != null)
                ChangedPageNameCallback(d, e);
        }

        [Category("Common")]
        [LocalizedDisplayName("Page Name")]
        [PropertyOrder(3)]
        public string PageName
        {
            get { return (string)GetValue(PageNameProperty); }
            set { SetValue(PageNameProperty, value); }
        }

        #endregion

        #region DisplayOrder

        public static readonly DependencyProperty DisplayOrderProperty =
           DependencyProperty.Register("DisplayOrder", typeof(int), typeof(PagePropertyObject), new PropertyMetadata());

        [Category("Common")]
        [LocalizedDisplayName("DisplayOrder")]
        [PropertyOrder(5)]
        public int DisplayOrder
        {
            get { return (int)GetValue(DisplayOrderProperty); }
            set { SetValue(DisplayOrderProperty, value); }
        }
        #endregion

        #endregion

        #region Brushes Category

        #region Background

        public static readonly DependencyProperty BackgroundProperty = DependencyProperty.Register("Background", typeof(Brush), typeof(PagePropertyObject), new PropertyMetadata());

        [Category("Brushes")]
        [LocalizedDisplayName("Background")]
        [PropertyOrder(1)]
        [PropertyEditor(typeof(BrushEditorCallButton))]
        public Brush Background
        {
            get { return (Brush)GetValue(BackgroundProperty); }
            set { SetValue(BackgroundProperty, value); }
        }
        #endregion      

        #endregion              

        #region Time Category

        #region ApplyPageLifeTime

        public static readonly DependencyProperty IsApplyPageLifeTimeProperty = DependencyProperty.Register("IsApplyPageLifeTime", typeof(bool), typeof(PagePropertyObject), new PropertyMetadata(false, new PropertyChangedCallback(Changed_IsApplyPageLifeTimeProperty)));

        private static void Changed_IsApplyPageLifeTimeProperty(object sender, DependencyPropertyChangedEventArgs e)
        {
        }

        [Category("Time")]
        [LocalizedDisplayName("Apply Page Time")]
        [PropertyOrder(1)]
        public bool IsApplyPageLifeTime
        {
            get { return (bool)GetValue(IsApplyPageLifeTimeProperty); }
            set { SetValue(IsApplyPageLifeTimeProperty, value); }
        }

        #endregion

        #region PageLifeTime

        public static readonly DependencyProperty PageLifeTimeProperty = DependencyProperty.Register("PageLifeTime", typeof(TimeSpan), typeof(PagePropertyObject), new PropertyMetadata(new TimeSpan(0, 1, 0), new PropertyChangedCallback(Changed_PageLifeTime)));

        private static void Changed_PageLifeTime(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            //if (PageLifeTimeCallback != null)
            //    PageLifeTimeCallback(d, e);
        }

        [Category("Time")]
        [LocalizedDisplayName("Page Time")]
        [PropertyOrder(2)]
        public TimeSpan PageLifeTime
        {
            get { return (TimeSpan)GetValue(PageLifeTimeProperty); }
            set { SetValue(PageLifeTimeProperty, value); }
        }
        #endregion

        #endregion

        #region Touch

        #region TouchEvent

        public static readonly DependencyProperty TouchEventProperty = DependencyProperty.Register("TouchEvent", typeof(string), typeof(PagePropertyObject));

        [Category("Event")]
        [LocalizedDisplayName("Touch Event")]
        [PropertyOrder(1)]
        [PropertyEditor(typeof(TouchEventPropertyControl))]
        public string TouchEvent
        {
            get { return (string)GetValue(TouchEventProperty); }
            set { SetValue(TouchEventProperty, value); }
        }

        #endregion

        //#region TouchName

        //public static readonly DependencyProperty EventNameProperty = DependencyProperty.Register("EventName", typeof(Array), typeof(PagePropertyObject));

        //[Category("Event")]
        //[LocalizedDisplayName("Event Name")]
        //[PropertyOrder(1)]
        //public Array EventName
        //{
        //    get { return (Array)GetValue(EventNameProperty); }
        //    set { SetValue(EventNameProperty, value); }
        //}
        //#endregion

        #endregion

      
        public PagePropertyObject()
        {
            
        }

        private static void Changed_PageProperty(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (PagePropertyChanged != null)
                PagePropertyChanged(d, e);
        }

        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);
        }      
    }
}
