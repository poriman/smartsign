﻿using System;
using System.Collections.Generic;
using System.Linq;
using InSysTouchflowData.Models.ProjectModels;
using System.IO;
using System.Windows;

namespace InSysTouchflowData
{
    public static class InSysDataManager
    {
        /// <summary>
        /// "media_files" 폴더 명.
        /// </summary>
        public static string MEDIAFILES_NAME = "media_files";
        public static string TouchflowProjectPath;
		private static NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();// NLog.LogManager.GetLogger("TouchDesignerLog");

        public static void SaveNew(List<Dictionary<string, object>> touchflowData, TouchProjectInfo tfProject, string path, SerializedFormat format, bool isReplace)
        {
            FileInfo fileInfo = new FileInfo(path);
            string newCreateFolderName = fileInfo.Name.Replace(fileInfo.Extension, "");
            string rootFolderPath = fileInfo.FullName.Replace(fileInfo.Name, "");
            string relativeProjectPath = string.Format(@"{0}", newCreateFolderName);

            string absoluteFullpath = "";
            if (fileInfo.Directory.Name.Equals(newCreateFolderName) == true || fileInfo.Directory.Name.ToLower().Equals(newCreateFolderName.ToLower()) == true)
            {
                absoluteFullpath = string.Format(@"{0}", rootFolderPath);
            }
            else
            {
                absoluteFullpath = string.Format(@"{0}{1}", rootFolderPath, newCreateFolderName);
            }
            
            DirectoryInfo dirInfo = new DirectoryInfo(absoluteFullpath);
            if (dirInfo.Exists == false)
            {
                dirInfo.Create();        
            }
            //else
            //{
            //    if (MessageBox.Show("프로젝트가 존재합니다. 덮어쓰시겠습니까?", "", MessageBoxButton.YesNo) == MessageBoxResult.No)
            //        return;                    
            //}

            List<string> pagePathList = new List<string>();
            string mediafilesPath = string.Format(@"{0}\{1}", absoluteFullpath, MEDIAFILES_NAME);
            foreach (var pageinfo in tfProject.PageInfoList)
            {
                string relativePageFolderPath = string.Format(@"{0}", pageinfo.PageNameTemp);
                string absolutePageFolderPath = string.Format(@"{0}\{1}", mediafilesPath, pageinfo.PageNameTemp);
                pagePathList.Add(absolutePageFolderPath);
                DirectoryInfo newDirectoryInfo = Directory.CreateDirectory(absolutePageFolderPath);

                List<PlaylistItem> totalPlaylist = new List<PlaylistItem>();
                foreach (var content in pageinfo.Contents)
                {
                    if (content.DesignElementProperties.ContainsKey("Playlist") == false)
                        continue;
                    
                    List<PlaylistItem> playlistItems = (List<PlaylistItem>)content.DesignElementProperties["Playlist"];

                    switch (content.ContentType.Name)
                    {
                        case "InSysWebControl":
                            {
                                foreach (var playlistitem in playlistItems)
                                {
                                    try
                                    {
                                        string strvalue = playlistitem.Content;
                                        if (strvalue.StartsWith("Address://"))
                                        {
                                            String httpAddress = strvalue.Substring(10);
                                            playlistitem.Content = httpAddress;
                                            continue;
                                        }
                                        else if (strvalue.StartsWith("File://"))
                                        {
                                            String itemPath = strvalue.Substring(7);

                                            FileInfo media_fileinfo = new FileInfo(itemPath);
                                            if (media_fileinfo.Exists == true)
                                            {
                                                string mediafileFullName = string.Format(@"{0}\{1}", absolutePageFolderPath, media_fileinfo.Name);
                                                if (playlistitem.Content.Equals(mediafileFullName) == false || playlistitem.Content.ToLower().Equals(mediafileFullName.ToLower()) == false)
                                                {
                                                    File.Copy(itemPath, mediafileFullName, true);
                                                }
                                                playlistitem.Content = string.Format(@"{0}", mediafileFullName);
                                                playlistitem.CONTENTSVALUE = string.Format(@"{0}\{1}\{2}", MEDIAFILES_NAME, relativePageFolderPath, media_fileinfo.Name);
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        continue;
                                    }
                                }
                            }
                            break;
                        default:
                            {
                                foreach (var playlistitem in playlistItems)
                                {
                                    try
                                    {
                                        FileInfo media_fileinfo = new FileInfo(playlistitem.Content);

                                        if (media_fileinfo.Exists == true)
                                        {
                                            string mediafileFullName = string.Format(@"{0}\{1}", absolutePageFolderPath, media_fileinfo.Name);
                                            if (playlistitem.Content.Equals(mediafileFullName) == false || playlistitem.Content.ToLower().Equals(mediafileFullName.ToLower()) == false)
                                            {
                                                File.Copy(playlistitem.Content, mediafileFullName, true);
                                            }
                                            playlistitem.Content = string.Format(@"{0}", mediafileFullName);
                                            playlistitem.CONTENTSVALUE = string.Format(@"{0}\{1}\{2}", MEDIAFILES_NAME, relativePageFolderPath, media_fileinfo.Name);
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        continue;
                                    }
                                }
                            }
                            break;
                    }
                   
                    totalPlaylist.AddRange(playlistItems);
                    
                }

                DeleteUnusedMedias(totalPlaylist.Select(o => o.Content).ToList(), newDirectoryInfo.GetFiles().Select(o => o.FullName).ToList());
            }

            DirectoryInfo mediafilesFolderDir = new DirectoryInfo(mediafilesPath);
            DeleteUnusedPages(pagePathList, mediafilesFolderDir.GetDirectories().Select(o => o.FullName).ToList());
            TouchflowProjectPath = absoluteFullpath + "\\" + fileInfo.Name;

            //ObjectXMLSerializer<TouchProjectInfo>.Save(tfProject, TouchflowProjectPath, null, format);
            ObjectXMLSerializer<List<Dictionary<string, object>>>.Save(touchflowData, TouchflowProjectPath, null, format);
            
        }

        public static void RenamePageOfLocalDirectory(string projectPath, TouchPageInfo touchPageInfo, string oldPageName, string newPageName)
        {
            FileInfo projectFileInfo = new FileInfo(projectPath);
            string projectFolderName = projectFileInfo.Directory.Name;
            string mediaFolderName = string.Format(@"{0}\{1}", projectFileInfo.DirectoryName, MEDIAFILES_NAME);

            DirectoryInfo directoryInfo = new DirectoryInfo(mediaFolderName);
            if (directoryInfo.Exists == true)
            {
                DirectoryInfo [] directoryInfos = directoryInfo.GetDirectories();
                foreach (DirectoryInfo di in directoryInfos)
                {
                    if (di.Name.Equals(oldPageName) == true || di.Name.ToLower().Equals(oldPageName.ToLower()) == true)
                    {
                        try
                        {

                            string newDirectoryName = string.Format(@"{0}\{1}", di.Parent.FullName, newPageName);

                            foreach (var contentinfo in touchPageInfo.Contents)
                            {
                                if (contentinfo.DesignElementProperties.ContainsKey("Playlist") == true)
                                {
                                    List<PlaylistItem> playlistItems = (List<PlaylistItem>)contentinfo.DesignElementProperties["Playlist"];
                                    foreach (var playlistitem in playlistItems)
                                    {
                                        try
                                        {
                                            FileInfo media_fileinfo = new FileInfo(playlistitem.Content);
                                            if (media_fileinfo.Exists == true)
                                            {
                                                playlistitem.Content = string.Format(@"{0}\{1}", newDirectoryName, media_fileinfo.Name);
                                                playlistitem.CONTENTSVALUE = string.Format(@"{0}\{1}\{2}", MEDIAFILES_NAME, newPageName, media_fileinfo.Name);
                                            }
                                        }
                                        catch
                                        {
                                            continue;
                                        }
                                    }
                                }
                            }

                            Directory.Move(di.FullName, di.Parent.FullName + @"\" + newPageName);
                            System.Threading.Thread.Sleep(1);
                            break;
                        }
                        catch
                        {
                            continue;
                        }
                    }
                }
            }
        }

        public static void RenamePage(string projectPath, string oldPageName, string newPageName)
        {
            FileInfo projectFileInfo = new FileInfo(projectPath);
            string projectFolderName = projectFileInfo.Directory.Name;
            string mediaFolderName = string.Format(@"{0}\{1}", projectFileInfo.DirectoryName, MEDIAFILES_NAME);
            DirectoryInfo directoryInfo = new DirectoryInfo(mediaFolderName);

            Directory.Move(directoryInfo.FullName + @"\" + oldPageName, directoryInfo.FullName + @"\" + newPageName);
        }

        public static void SaveAgain(List<Dictionary<string, object>> touchflowData, TouchProjectInfo tfProject, string path, SerializedFormat format, bool isReplace)
        {
            FileInfo fileInfo = new FileInfo(path);
            string folderName = fileInfo.Directory.Name;
            string relativeProjectPath = string.Format(@"{0}", folderName);
            string absoluteFullpath = fileInfo.DirectoryName;
            DirectoryInfo dirInfo = new DirectoryInfo(absoluteFullpath);

            if (dirInfo.Exists == false)
            {
                dirInfo.Create();
            }
            //else
            //{
            //    if (MessageBox.Show("프로젝트가 존재합니다. 덮어쓰시겠습니까?", "", MessageBoxButton.YesNo) == MessageBoxResult.No)
            //        return;                    
            //}

            List<string> pagePathList = new List<string>();
            string mediafilesPath = string.Format(@"{0}\{1}", absoluteFullpath, MEDIAFILES_NAME);
            foreach (var pageinfo in tfProject.PageInfoList)
            {
                string relativePageFolderPath = string.Format(@"{0}", pageinfo.PageNameTemp);
                string absolutePageFolderPath = string.Format(@"{0}\{1}", mediafilesPath, pageinfo.PageNameTemp);
                pagePathList.Add(absolutePageFolderPath);
                DirectoryInfo newDirectoryInfo = Directory.CreateDirectory(absolutePageFolderPath);

                List<PlaylistItem> totalPlaylist = new List<PlaylistItem>();
                foreach (var content in pageinfo.Contents)
                {
                    if (content.DesignElementProperties.ContainsKey("Playlist") == false)
                        continue;

                    List<PlaylistItem> playlistItems = (List<PlaylistItem>)content.DesignElementProperties["Playlist"];
                    switch (content.ContentType.Name)
                    {
                        case "InSysWebControl":
                            {
                                foreach (var playlistitem in playlistItems)
                                {
                                    try
                                    {
                                        string strvalue = playlistitem.Content;
                                        if (strvalue.StartsWith("Address://"))
                                        {
                                            String httpAddress = strvalue.Substring(10);
                                            playlistitem.Content = string.Format("Address://{0}", httpAddress);// httpAddress;
                                            continue;
                                        }
                                        else if (strvalue.StartsWith("http:"))
                                        {
                                            playlistitem.Content = strvalue;
                                            continue;
                                        }
                                        else if (strvalue.StartsWith("File://"))
                                        {
                                            String itemPath = strvalue.Substring(7);

                                            FileInfo media_fileinfo = new FileInfo(itemPath);
                                            if (media_fileinfo.Exists == true)
                                            {
                                                string mediafileFullName = string.Format(@"{0}\{1}", absolutePageFolderPath, media_fileinfo.Name);
                                                if (playlistitem.Content.Equals(mediafileFullName) == false || playlistitem.Content.ToLower().Equals(mediafileFullName.ToLower()) == false)
                                                {
                                                    File.Copy(itemPath, mediafileFullName, true);
                                                }
                                                playlistitem.Content = mediafileFullName;
                                                playlistitem.CONTENTSVALUE = string.Format(@"{0}\{1}\{2}", MEDIAFILES_NAME, relativePageFolderPath, media_fileinfo.Name);
                                            }
                                        }
                                        else
                                        {
                                            FileInfo media_fileinfo = new FileInfo(strvalue);
                                            if (media_fileinfo.Exists == true)
                                            {
                                                string mediafileFullName = string.Format(@"{0}\{1}", absolutePageFolderPath, media_fileinfo.Name);
                                                if (playlistitem.Content.Equals(mediafileFullName) == false || playlistitem.Content.ToLower().Equals(mediafileFullName.ToLower()) == false)
                                                {
                                                    File.Copy(strvalue, mediafileFullName, true);
                                                }
                                                playlistitem.Content = mediafileFullName;
                                                playlistitem.CONTENTSVALUE = string.Format(@"{0}\{1}\{2}", MEDIAFILES_NAME, relativePageFolderPath, media_fileinfo.Name);
                                            }
                                        }

                                    }
                                    catch (Exception ex)
                                    {
                                        continue;
                                    }
                                }
                            }
                            break;
                        default:
                            {
                                foreach (var playlistitem in playlistItems)
                                {
                                    try
                                    {
                                        FileInfo media_fileinfo = new FileInfo(playlistitem.Content);

                                        if (media_fileinfo.Exists == true)
                                        {
                                            string mediafileFullName = string.Format(@"{0}\{1}", absolutePageFolderPath, media_fileinfo.Name);
                                            if (playlistitem.Content.Equals(mediafileFullName) == false || playlistitem.Content.ToLower().Equals(mediafileFullName.ToLower()) == false)
                                            {
                                                File.Copy(playlistitem.Content, mediafileFullName, true);
                                            }
                                            playlistitem.Content = string.Format(@"{0}", mediafileFullName);
                                            playlistitem.CONTENTSVALUE = string.Format(@"{0}\{1}\{2}", MEDIAFILES_NAME, relativePageFolderPath, media_fileinfo.Name);
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        continue;
                                    }
                                }
                            }
                            break;
                    }

                    totalPlaylist.AddRange(playlistItems);

                }

                DeleteUnusedMedias(totalPlaylist.Select(o => o.Content).ToList(), newDirectoryInfo.GetFiles().Select(o => o.FullName).ToList());
            }

            DirectoryInfo mediafileDirectoryInfo = new DirectoryInfo(mediafilesPath);
            DeleteUnusedPages(pagePathList, mediafileDirectoryInfo.GetDirectories().Select(o => o.FullName).ToList());
            TouchflowProjectPath = absoluteFullpath + "\\" + fileInfo.Name;

            //ObjectXMLSerializer<TouchProjectInfo>.Save(tfProject, TouchflowProjectPath, null, format);
            ObjectXMLSerializer<List<Dictionary<string, object>>>.Save(touchflowData, TouchflowProjectPath, null, format);

        }


        public static void SaveAuto(List<Dictionary<string, object>> touchflowData, TouchProjectInfo tfProject, SerializedFormat format)
        {
            FileInfo projectFileInfo = new FileInfo(TouchflowProjectPath);
            string projectFolderName = projectFileInfo.Directory.Name;
            string mediaFolderName = string.Format(@"{0}\{1}", projectFileInfo.DirectoryName, MEDIAFILES_NAME);
            DirectoryInfo mediafileDirectoryInfo = new DirectoryInfo(mediaFolderName);
            List<string> pagePathList = new List<string>();

            foreach (var pageinfo in tfProject.PageInfoList)
            {
                string pageFolderPath = string.Format(@"{0}\{1}", mediaFolderName, pageinfo.PageNameTemp);
                pagePathList.Add(pageFolderPath);
            }

            DeleteUnusedPages(pagePathList, mediafileDirectoryInfo.GetDirectories().Select(o => o.FullName).ToList());
            
            ObjectXMLSerializer<List<Dictionary<string, object>>>.Save(touchflowData, TouchflowProjectPath, null, format);
        }

        public static TouchProjectInfo Load(string path, SerializedFormat format)
        {
            FileInfo fileInfo = new FileInfo(path);
            string projectFolderPath = fileInfo.FullName.Replace(fileInfo.Name, "");

            //TouchProjectInfo tfProject = ObjectXMLSerializer<TouchProjectInfo>.Load(path, SerializedFormat.Binary);
            List<Dictionary<string, object>> touchflowData = ObjectXMLSerializer<List<Dictionary<string, object>>>.Load(path, SerializedFormat.Binary);

            TouchProjectInfo tfProject = touchflowData[0]["TouchflowData"] as TouchProjectInfo;
            if (tfProject != null)
            {
                if (touchflowData[0].Keys.Contains("DefaultPageLifeTime") == false)
                {
                    TimeSpan ts = (TimeSpan)touchflowData[0]["PlayTime"];
                    tfProject.DefaultPageLifeTime = ts.TotalSeconds;
                }
                else
                {
                    TimeSpan ts = (TimeSpan)touchflowData[0]["DefaultPageLifeTime"];
                    tfProject.DefaultPageLifeTime = ts.TotalSeconds;
                }
                
                TouchflowProjectPath = path;
                foreach (var pageinfo in tfProject.PageInfoList)
                {
                    foreach (var content in pageinfo.Contents)
                    {
                        if (content.DesignElementProperties.ContainsKey("Playlist") == false)
                            continue;

                        //logger.Info(string.Format("Content load >> Type: {0}, Name: {1}", content.ContentType.ToString(), content.ContentName));

                        List<PlaylistItem> playlistItems = (List<PlaylistItem>)content.DesignElementProperties["Playlist"];
                        foreach (var playlistitem in playlistItems)
                        {
                            try
                            {
                                //string contentPath = string.Format("{0}{1}\\{2}", projectFolderPath, MEDIAFILES_NAME, playlistitem.Content);
                                string contentPath = string.Format("{0}{1}", projectFolderPath, playlistitem.CONTENTSVALUE);
                                if (File.Exists(contentPath) == true)
                                {
                                    playlistitem.Content = string.Format("{0}{1}\\{2}", projectFolderPath, MEDIAFILES_NAME, playlistitem.Content);
                                }
                            }
                            catch
                            {
                                continue;
                            }
                            finally
                            {
                                //logger.Info(string.Format("PlaylistItem: {0}", playlistitem.Content));
                            }
                        }
                    }
                }
            }

            return tfProject;
        }

        public static Dictionary<string, object> LoadTouchProjectData(string path, SerializedFormat format)
        {
            Dictionary<string, object> touchProjectData = new Dictionary<string, object>();
            FileInfo projectFileInfo = new FileInfo(path);
            string projectFolderPath = projectFileInfo.FullName.Replace(projectFileInfo.Name, "");

            //TouchProjectInfo tfProject = ObjectXMLSerializer<TouchProjectInfo>.Load(path, SerializedFormat.Binary);
            List<Dictionary<string, object>> touchflowData = ObjectXMLSerializer<List<Dictionary<string, object>>>.Load(path, SerializedFormat.Binary);

            TouchProjectInfo tfProject = touchflowData[0]["TouchflowData"] as TouchProjectInfo;
            if (tfProject != null)
            {
                //if (touchflowData[0].Keys.Contains("DefaultPageLifeTime") == false)
                //{
                //    TimeSpan ts = (TimeSpan)touchflowData[0]["PlayTime"];
                //    tfProject.DefaultPageLifeTime = ts.TotalSeconds;
                //}
                //else
                //{
                //    TimeSpan ts = (TimeSpan)touchflowData[0]["DefaultPageLifeTime"];
                //    tfProject.DefaultPageLifeTime = ts.TotalSeconds;
                //}

                TouchflowProjectPath = path;
                foreach (var pageinfo in tfProject.PageInfoList)
                {
                    foreach (var content in pageinfo.Contents)
                    {
                        if (content.DesignElementProperties.ContainsKey("Playlist") == false)
                            continue;

                        //logger.Info(string.Format("Content load >> Type: {0}, Name: {1}", content.ContentType.ToString(), content.ContentName));

                        List<PlaylistItem> playlistItems = (List<PlaylistItem>)content.DesignElementProperties["Playlist"];
                        foreach (var playlistitem in playlistItems)
                        {
                            try
                            {
                                string contentPath = string.Format("{0}{1}", projectFolderPath, playlistitem.CONTENTSVALUE);
                                if (File.Exists(contentPath) == true)
                                {
                                    playlistitem.Content = contentPath;
                                }

                                logger.Info(contentPath);
                            }
                            catch
                            {
                                continue;
                            }
                            finally
                            {
                                //logger.Info(string.Format("PlaylistItem: {0}", playlistitem.Content));
                            }
                        }
                    }
                }
                touchflowData[0]["TouchflowData"] = tfProject;
                touchProjectData = touchflowData[0];
            }

            return touchProjectData;
        }

        private static void DeleteUnusedMedias(List<string> playlistContents, List<string> currentFolderContents)
        {
            foreach (var itempath in currentFolderContents)
            {
                var samePath = playlistContents.Where(o => o.Equals(itempath) == true || o.ToLower().Equals(itempath.ToLower()) == true).FirstOrDefault();
                if (string.IsNullOrEmpty(samePath) == true)
                {
                    File.Delete(itempath);
                }
            }
        }

        private static void DeleteUnusedPages(List<string> pages, List<string> localPages)
        {
            foreach (var itempath in localPages)
            {
                var samePath = pages.Where(o => o.Equals(itempath) == true || o.ToLower().Equals(itempath.ToLower()) == true).FirstOrDefault();
                if (string.IsNullOrEmpty(samePath) == true)
                {
                    Directory.Delete(itempath, true);
                }
            }
        }
    }
}
