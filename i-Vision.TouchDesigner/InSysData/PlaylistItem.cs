﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Markup;
using System.Xml;
using System.Collections.ObjectModel;

namespace InSysTouchflowData
{
    [Serializable]
    public class PlaylistItem
    {
        string _key;
        string _value;
        TimeSpan _time;
        object _object;
        bool _isInfinity;

        public bool IsInfinity
        {
            set { _isInfinity = value; }
            get { return _isInfinity; }
        }

        public string Content
        {
            get
            {
                return _key;
            }

            set
            {
                _key = value;
            }
        }

        public string Duration
        {
            get
            {
                return _value;
            }
            set
            {
                _value = value;
            }
        }

        public TimeSpan TIME
        {
            set { _time = value; }
            get { return _time; }
        }

        public object CONTENTSVALUE
        {
            set { _object = value; }
            get { return _object; }
        }

        /// <summary>
        /// Initializes the new PlaylistItem class instance. The default content value is String.Empty and 
        /// the default Duration value is 00:00:00
        /// </summary>
        public PlaylistItem()
        {
            _key = String.Empty;
            _value = XamlWriter.Save(new TimeSpan(0, 0, 0));
            TIME = new TimeSpan(0, 0, 0);
            CONTENTSVALUE = null;
        }

        /// <summary>
        /// Gets the item duration as TimeSpan, deserializing the Duration
        /// </summary>
        public TimeSpan DurationAsTimeSpan
        {
            get
            {
                StringReader sr = new StringReader(_value);
                XmlReader xr = XmlReader.Create(sr);
                return (TimeSpan)XamlReader.Load(xr);
            }
        }
    }

    public class PlaylistItemArry : ObservableCollection<PlaylistItem>
    {
        public PlaylistItemArry() : base() { }
    }
}
