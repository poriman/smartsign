﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Diagnostics;
using System.Windows.Controls;
using SplitContainer.Controls.Primitives;
using System.Windows.Media;
using SplitContainer.Common;

namespace SplitContainer.Controls
{
    /// <summary>
    /// Panel which arranges its children in a direction and allows users resize them with splitters
    /// </summary>
    public class SplitContainer : LogicalPanel
    {
        public SplitContainer()
        {
            //this.Background = new SolidColorBrush(Color.FromArgb(0xFF, 0x5D, 0x60, 0x67));
        }

        /// <summary>
        /// Compute the desidered size of the panel
        /// </summary>
        /// <param name="availableSize"></param>
        /// <returns></returns>

        protected override Size MeasureOverride(Size availableSize)
        {
            int iChild = 0;
            double offset = 0.0;
            double desideredWidth = 0.0;
            double desideredHeight = 0.0;

            while (iChild < VisualChildrenCount)
            {
                FrameworkElement child = GetVisualChild(iChild) as FrameworkElement;

                double sumStars = 0.0;
                for (int i = iChild; i < VisualChildrenCount; i += 2)
                {
                    Visual temp = GetVisualChild(i);
                    sumStars += GetSplitSize(GetVisualChild(i)).Value;
                }
                
                if (ResizeOrientation == Orientation.Horizontal)
                {
                    #region Horizontal orientation
                    double finalWidth = availableSize.Width - Splitter.WIDTH * (VisualChildrenCount - 1 - iChild) / 2;

                    double childWidth = sumStars > 0.0 ? (finalWidth - offset) * (GetSplitSize(child).Value / sumStars) : 0.0;
                    
                    childWidth = Math.Max(childWidth, child.MinWidth);
                    childWidth = Math.Min(childWidth, child.MaxWidth);
                    child.Measure(new Size( 
                        childWidth,
                        availableSize.Height));

                    offset += childWidth;
                    double childHeight = child.DesiredSize.Height;
                    desideredHeight = Math.Max(childHeight, desideredHeight);
                    iChild++;

                    if (iChild < VisualChildrenCount)
                    {
                        child = GetVisualChild(iChild) as FrameworkElement;
                        child.Measure(new Size(
                            double.PositiveInfinity,
                            childHeight));

                        offset += child.DesiredSize.Width;
                        iChild++;
                    }
                    #endregion
                }
                else //if (ResizeOrientation == Orientation.Vertical)
                {
                    #region Vertical orientation
                    double finalHeight = availableSize.Height - Splitter.HEIGHT * (VisualChildrenCount - 1 - iChild) / 2;

                    double childHeight = sumStars > 0.0 ? (finalHeight - offset) * (GetSplitSize(child).Value / sumStars) : 0.0;
                    childHeight = Math.Max(childHeight, child.MinHeight);
                    childHeight = Math.Min(childHeight, child.MaxHeight);
                    child.Measure(new Size(
                        availableSize.Width,
                        childHeight));

                    offset += childHeight;
                    double childWidth = child.DesiredSize.Width;
                    desideredWidth = Math.Max(childWidth, desideredWidth);
                    iChild++;

                    if (iChild < VisualChildrenCount)
                    {
                        child = GetVisualChild(iChild) as FrameworkElement;
                        child.Measure(new Size(
                            childWidth,
                            double.PositiveInfinity));

                        offset += child.DesiredSize.Height;
                        iChild++;
                    }
                    #endregion
                }
            }

            if (ResizeOrientation == Orientation.Horizontal)
            {
                return new Size(availableSize.Width, desideredHeight);
            }
            else //if (Orientation == Orientation.Vertical)
            {
                return new Size(desideredWidth, availableSize.Height);
            }
       }

        /// <summary>
        /// Arranges children giving them a proportional space according to their <see cref="SplitSize"/> attached property value
        /// </summary>
        /// <param name="finalSize"></param>
        /// <returns></returns>
        protected override Size ArrangeOverride(Size finalSize)
        {
            //with no children fill the space
            if (VisualChildrenCount == 0)
                return finalSize;

            int iChild = 0;
            double offset = 0.0;

            while (iChild < VisualChildrenCount)
            {
                FrameworkElement child = GetVisualChild(iChild) as FrameworkElement;
                
                double sumStars = 0.0;
                for (int i = iChild; i < VisualChildrenCount; i += 2)
                {
                    sumStars += GetSplitSize(GetVisualChild(i)).Value;
                }

                if (ResizeOrientation == Orientation.Horizontal)
                {
                    #region Horizontal orientation
                    double finalWidth = finalSize.Width - Splitter.WIDTH * (VisualChildrenCount - 1 - iChild) / 2;

                    double childWidth = sumStars > 0.0 ? (finalWidth - offset) * (GetSplitSize(child).Value / sumStars) : 0.0;
                    childWidth = Math.Max(childWidth, child.MinWidth);
                    childWidth = Math.Min(childWidth, child.MaxWidth);

                    child.Arrange(new Rect(
                        offset,
                        0,
                        childWidth,
                        finalSize.Height));
                    
                    offset += childWidth;
                    iChild++;

                    if (iChild < VisualChildrenCount)  //splitter  추가 되는 부분
                    {
                        child = GetVisualChild(iChild) as FrameworkElement;
                        child.Arrange(new Rect(
                            offset,
                            0,
                            child.DesiredSize.Width,
                            finalSize.Height));
                        offset += child.ActualWidth;
                        iChild++;
                    }
                    #endregion
                }
                else
                {
                    #region Vertical orientation
                    double finalHeight = finalSize.Height - Splitter.HEIGHT * (VisualChildrenCount - 1 - iChild) / 2;

                    double childHeight = sumStars > 0.0 ? (finalHeight - offset) * (GetSplitSize(child).Value / sumStars) : 0.0;
                    childHeight = Math.Max(childHeight, child.MinHeight);
                    childHeight = Math.Min(childHeight, child.MaxHeight);
                    
                    child.Arrange(new Rect(
                        0,
                        offset,
                        finalSize.Width,
                        childHeight));

                    offset += childHeight;
                    iChild++;

                    if (iChild < VisualChildrenCount)  //splitter  추가 되는 부분
                    {
                        child = GetVisualChild(iChild) as FrameworkElement;

                        child.Arrange(new Rect(
                            0,
                            offset,
                            finalSize.Width,
                            child.DesiredSize.Height));

                        offset += child.ActualHeight;
                        iChild++;
                    }
                    #endregion
                }


            }

            // Debug.Assert(offset == finalSize.Width);
            if (ResizeOrientation == Orientation.Horizontal)
                return new Size(offset, finalSize.Height);
            else
                return new Size(finalSize.Width, offset);
        }

        /// <summary>
        /// Add/remove a splitter between childs
        /// </summary>
        /// <param name="childAdded"></param>
        /// <param name="childRemoved"></param>
        protected override void OnLogicalChildrenChanged(UIElement childAdded, UIElement childRemoved)
        {
            if (childAdded != null)
            {

                if (VisualChildrenCount > 0)
                {
                    Splitter split = new Splitter(this);
                    //Splitter is added on only in the visual tree
                    AddVisualChild(split);
                }
                
                //child is alreay present in the logical tree
                AddVisualChild(childAdded);
            }

            if (childRemoved != null)
            {
                for (int i = 0; i < VisualChildrenCount - 1; i += 2)
                {
                    if (childRemoved == GetVisualChild(i))
                    {
                        //remove the child from visual tree along with corresponding splitter
                        RemoveVisualChild(childRemoved);
                        RemoveVisualChild(GetVisualChild(i));
                        break;
                    }
                }
            }

            base.OnLogicalChildrenChanged(childAdded, childRemoved);
        }

        /// <summary>
        /// Get or sets the direction of the children positioning
        /// </summary>
        public Orientation ResizeOrientation
        {
            get { return (Orientation)GetValue(ResizeOrientationProperty); }
            set { SetValue(ResizeOrientationProperty, value); }
        }

        /// <summary>
        /// ResizeOrientation attached property
        /// </summary>
        public static readonly DependencyProperty ResizeOrientationProperty =
            DependencyProperty.Register("ResizeOrientation", typeof(Orientation), typeof(SplitContainer), new FrameworkPropertyMetadata(Orientation.Horizontal, FrameworkPropertyMetadataOptions.AffectsMeasure));

        /// <summary>
        /// Get the SplitSize attached property value for an object
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static GridLength GetSplitSize(DependencyObject obj)
        {
            return (GridLength)obj.GetValue(SplitSizeProperty);
        }

        /// <summary>
        /// Set the SplitSize attached property value for an object
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static void SetSplitSize(DependencyObject obj, GridLength value)
        {
            obj.SetValue(SplitSizeProperty, value);
        }

        /// <summary>
        /// Define the SplitSize attached property
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static readonly DependencyProperty SplitSizeProperty =
            DependencyProperty.RegisterAttached("SplitSize", typeof(GridLength), typeof(SplitContainer), new UIPropertyMetadata(new GridLength(1.0, GridUnitType.Star)));


        /// <summary>
        /// This method is called by a splitter when it is dragged
        /// </summary>
        /// <param name="splitter">Dragged splitter</param>
        /// <param name="delta"></param>
        internal void Resize(Splitter splitter, double delta)
        {
            
            for (int i = 1; i < VisualChildrenCount - 1; i += 2)
            {
                if (GetVisualChild(i) == splitter)
                {
                    FrameworkElement childPrev = GetVisualChild(i - 1) as FrameworkElement;
                    FrameworkElement childNext = GetVisualChild(i + 1) as FrameworkElement;

                    if (ResizeOrientation == Orientation.Horizontal)
                    {
                        #region Horizontal orientation
                        double totWidth = childPrev.ActualWidth + childNext.ActualWidth;

                        double totFactor = GetSplitSize(childPrev).Value + GetSplitSize(childNext).Value;
                        double scaleFactor = totFactor / totWidth;

                        double prevStarFactor = Math.Max(0.0, GetSplitSize(childPrev).Value + delta * scaleFactor);
                        double nextStarFactor = Math.Max(0.0, GetSplitSize(childNext).Value - delta * scaleFactor);

                        if (prevStarFactor < childPrev.MinWidth * scaleFactor)
                        {
                            prevStarFactor = childPrev.MinWidth * scaleFactor;
                            nextStarFactor = totFactor - prevStarFactor;
                        }
                        if (prevStarFactor > childPrev.MaxWidth * scaleFactor)
                        {
                            prevStarFactor = childPrev.MaxWidth * scaleFactor;
                            nextStarFactor = totFactor - prevStarFactor;

                        }

                        if (prevStarFactor > totFactor)
                        {
                            prevStarFactor = totFactor;
                            nextStarFactor = 0.0;
                        }

                        if (nextStarFactor < childNext.MinWidth * scaleFactor)
                        {
                            nextStarFactor = childNext.MinWidth * scaleFactor;
                            prevStarFactor = totFactor - nextStarFactor;
                        }

                        if (nextStarFactor > childNext.MaxWidth * scaleFactor)
                        {
                            nextStarFactor = childNext.MaxWidth * scaleFactor;
                            prevStarFactor = totFactor - nextStarFactor;
                        }

                        if (nextStarFactor > totFactor)
                        {
                            nextStarFactor = totFactor;
                            prevStarFactor = 0.0;
                        }

                        //Debug.WriteLine(string.Format("PrevFactor={0}, NextFactor={1}", prevStarFactor, nextStarFactor));

                        SetSplitSize(childPrev, new GridLength(prevStarFactor, GridUnitType.Star));
                        SetSplitSize(childNext, new GridLength(nextStarFactor, GridUnitType.Star));
                        InvalidateMeasure();
                        //InvalidateArrange();
                        break;
                        #endregion
                    }
                    else //if ResizeOrientation == Orientation.Vertical
                    {
                        #region Horizontal orientation
                        double totHeight = childPrev.ActualHeight + childNext.ActualHeight;

                        double totFactor = GetSplitSize(childPrev).Value + GetSplitSize(childNext).Value;
                        double scaleFactor = totFactor / totHeight;

                        double prevStarFactor = Math.Max(0.0, GetSplitSize(childPrev).Value + delta * scaleFactor);
                        double nextStarFactor = Math.Max(0.0, GetSplitSize(childNext).Value - delta * scaleFactor);

                        if (prevStarFactor < childPrev.MinHeight * scaleFactor)
                        {
                            prevStarFactor = childPrev.MinHeight * scaleFactor;
                            nextStarFactor = totFactor - prevStarFactor;
                        }
                        if (prevStarFactor > childPrev.MaxHeight * scaleFactor)
                        {
                            prevStarFactor = childPrev.MaxHeight * scaleFactor;
                            nextStarFactor = totFactor - prevStarFactor;

                        }

                        if (prevStarFactor > totFactor)
                        {
                            prevStarFactor = totFactor;
                            nextStarFactor = 0.0;
                        }

                        if (nextStarFactor < childNext.MinHeight * scaleFactor)
                        {
                            nextStarFactor = childNext.MinHeight * scaleFactor;
                            prevStarFactor = totFactor - nextStarFactor;
                        }

                        if (nextStarFactor > childNext.MaxHeight * scaleFactor)
                        {
                            nextStarFactor = childNext.MaxHeight * scaleFactor;
                            prevStarFactor = totFactor - nextStarFactor;
                        }

                        if (nextStarFactor > totFactor)
                        {
                            nextStarFactor = totFactor;
                            prevStarFactor = 0.0;
                        }

                        //Debug.WriteLine(string.Format("PrevFactor={0}, NextFactor={1}", prevStarFactor, nextStarFactor));

                        SetSplitSize(childPrev, new GridLength(prevStarFactor, GridUnitType.Star));
                        SetSplitSize(childNext, new GridLength(nextStarFactor, GridUnitType.Star));
                        InvalidateMeasure();
                        //InvalidateArrange();
                        break;
                        #endregion
                    }
                }
            }
        }
    }
}
