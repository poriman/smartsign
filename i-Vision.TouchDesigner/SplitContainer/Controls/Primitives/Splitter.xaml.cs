﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Controls.Primitives;

namespace SplitContainer.Controls.Primitives
{
    /// <summary>
    /// Splitter.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class Splitter : Thumb
    {
        public event EventHandler<EventArgs> WorkFlowBtnSelectedEvent = delegate { };
        public event EventHandler<EventArgs> ScreenUIBtnSelectedEvent = delegate { };
        public event EventHandler<EventArgs> SwitchBtnSelectedEvent = delegate { };
        public event EventHandler<EventArgs> OrientationCheckEvent = delegate { };

        public Splitter()
        {
            //this.Background = new SolidColorBrush(Colors.DarkBlue);
        }

         SplitContainer _container;

        public const double WIDTH = 30;
        public const double HEIGHT = 7;


        public Splitter(SplitContainer container) : this()
        {
            InitializeComponent();

            _container = container;
            
            this.DragStarted += new DragStartedEventHandler(Split_DragStarted);
            this.DragDelta += new DragDeltaEventHandler(Split_DragDelta);
            this.DragCompleted += new DragCompletedEventHandler(Split_DragCompleted);

            this.Loaded += (s, e) =>
                {
                };
        }

        void Split_DragCompleted(object sender, DragCompletedEventArgs e)
        {

        }

        void Split_DragDelta(object sender, DragDeltaEventArgs e)
        {
            _container.Resize(this, _container.ResizeOrientation == Orientation.Horizontal ?
                e.HorizontalChange : e.VerticalChange);
        }


        void Split_DragStarted(object sender, DragStartedEventArgs e)
        {
        }

        protected override Size MeasureOverride(Size constraint)
        {
            if (_container.ResizeOrientation == Orientation.Horizontal)
            {
                return new Size(WIDTH, constraint.Height);
            }
            else if (_container.ResizeOrientation == Orientation.Vertical)
            {
                return new Size(constraint.Width, HEIGHT);
            }

            return base.MeasureOverride(constraint);
        }

        protected override void OnMouseEnter(System.Windows.Input.MouseEventArgs e)
        {
            Cursor = _container.ResizeOrientation == Orientation.Horizontal ?
                Cursors.SizeWE : Cursors.SizeNS;

            base.OnMouseEnter(e);
        }

        private void WorkFlowBtn_Click(object sender, RoutedEventArgs e)
        {
            WorkFlowBtnSelectedEvent(sender, e);
        }

        private void ScreenUIBtn_Click(object sender, RoutedEventArgs e)
        {
            ScreenUIBtnSelectedEvent(sender, e);
        }

        private void Switch_Click(object sender, RoutedEventArgs e)
        {
            SwitchBtnSelectedEvent(sender, e);

        }

        private void btnOrientation_Click(object sender, RoutedEventArgs e)
        {
            OrientationCheckEvent(sender, e);
        }
    }
}
