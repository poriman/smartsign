﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;

namespace InSysTouchPlayer.ViewModel
{
    public class TFPlayerWindowViewModel
    {
        private TFPlayerWindow _tfPlayerWindow;
        #region singleton 생성자

        private static TFPlayerWindowViewModel _instance;
        public static TFPlayerWindowViewModel Instance
        {
            get { return _instance; }
        }

        static TFPlayerWindowViewModel()
        {
            _instance = new TFPlayerWindowViewModel();
        }

        public TFPlayerWindowViewModel()
        {            
        }

        #endregion    
               
        #region System Event handlers

        public void TFPlayerWindow_Loaded(object sender, RoutedEventArgs e)
        {
            _tfPlayerWindow = sender as TFPlayerWindow;
        }    

        public void TFPlayerWindow_Closed(object sender, EventArgs e)
        {            
        }

        #endregion

        public void SetDisPlayUI(object grid)
        {
            if (_tfPlayerWindow != null)
            {
                this._tfPlayerWindow.Content = grid;
            }
        }

        public Window GetDisplayWindow()
        {
            return this._tfPlayerWindow;
        }
    }
}
