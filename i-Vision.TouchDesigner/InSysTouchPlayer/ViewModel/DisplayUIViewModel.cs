﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Windows;
using InSysTouchPlayer.View;

namespace InSysTouchPlayer.ViewModel
{
    public class DisplayUIViewModel
    {
        private bool show_cursor = false;
        private XElement doc;
        private string docStart;
        private DisplayUI _displayUI;

         #region singleton 생성자

        private static DisplayUIViewModel _instance;
        public static DisplayUIViewModel Instance
        {
            get { return _instance; }
        }

        static DisplayUIViewModel()
        {
            _instance = new DisplayUIViewModel();
        }

        public DisplayUIViewModel()
        {
        }

        #endregion    

        public void Load(string name, bool showCursor)
        {
            show_cursor = showCursor;

            Load(name);
        }

        public void Load(string name)
        {
            doc = XElement.Load(name);
            docStart = System.IO.Path.GetDirectoryName(name);
        }

        public void Start()
        {
            if (doc == null)
                return;

            //this.Children.Clear();
        }

        #region System Event handlers

        public void DisplayUIGrid_Loaded(object sender, RoutedEventArgs e)
        {
            _displayUI = sender as DisplayUI;
        }
        #endregion

        public object GetDisplayUI()
        {
            return _displayUI;
        }
    }
}
