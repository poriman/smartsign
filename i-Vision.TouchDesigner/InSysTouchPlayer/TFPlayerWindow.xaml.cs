﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;
using System.Windows.Interop;

namespace InSysTouchPlayer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class TFPlayerWindow : Window
    {
        InSysDSDisplayer.Hooking.InputDevice inputDevice;
        int NumberOfKeyboards;
        System.Windows.Forms.Message message = new System.Windows.Forms.Message();

        public TFPlayerWindow()
        {
            InitializeComponent();

            if ((new TFPlayerManager(this)).OpenTFFile() == false)
            {
                Application.Current.Shutdown();
            }            
            this.Loaded += (s, e) =>
                {
                    
                };
        }

        //private void _KeyPressed(object sender, InputDevice.KeyControlEventArgs e)
        //{
        //    string[] tokens = e.Keyboard.Name.Split(';');
        //    string token = tokens[1];

        //    lbHandle.Content = e.Keyboard.deviceHandle.ToString();
        //    lbType.Content = e.Keyboard.deviceType;
        //    lbName.Content = e.Keyboard.deviceName;
        //    lbKey.Content = e.Keyboard.key.ToString();
        //    lbVKey.Content = e.Keyboard.vKey;
        //    lbDescription.Content = token;
        //    lbNumKeyboards.Content = NumberOfKeyboards.ToString();
        //}

        public IntPtr WndProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            if (inputDevice != null)
            {
                // I could have done one of two things here.
                // 1. Use a Message as it was used before.
                // 2. Changes the ProcessMessage method to handle all of these parameters(more work).
                //    I opted for the easy way.

                //Note: Depending on your application you may or may not want to set the handled param.

                message.HWnd = hwnd;
                message.Msg = msg;
                message.LParam = lParam;
                message.WParam = wParam;

                inputDevice.ProcessMessage(message);

                if (msg == 513)
                {
                    Console.WriteLine(msg.ToString());
                }
                else if (msg == 514)
                {
                    Console.WriteLine(msg.ToString());
                }


            }
            //Console.WriteLine(id.ToString());
            return IntPtr.Zero;
        }

        protected override void OnSourceInitialized(EventArgs e)
        {
            // I am new to WPF and I don't know where else to call this function.
            // It has to be called after the window is created or the handle won't
            // exist yet and the function will throw an exception.          

            base.OnSourceInitialized(e);
        }

        void StartWndProcHandler()
        {
            IntPtr hwnd = IntPtr.Zero;
            Window myWin = Application.Current.MainWindow;

            try
            {
                hwnd = new WindowInteropHelper(myWin).Handle;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            //Get the Hwnd source   
            HwndSource source = HwndSource.FromHwnd(hwnd);
            //Win32 queue sink
            source.AddHook(new HwndSourceHook(WndProc));

            inputDevice = new InSysDSDisplayer.Hooking.InputDevice(source.Handle);
            NumberOfKeyboards = inputDevice.EnumerateDevices();
            //id.KeyPressed += new InputDevice.DeviceEventHandler(_KeyPressed);
        }
    }
}
