﻿using System;
using System.Windows.Forms;
using System.IO;
using InSysTouchPlayer.ViewModel;
using InSysTouchflowData.Models.ProjectModels;
using System.Windows;
using InSysDSDisplayer;
using System.Collections.Generic;
using InSysDSDisplayer.Hooking;
using InSysTouchflowData;
using NLog.Targets;
using NLog;

namespace InSysTouchPlayer
{
    public class TFPlayerManager
    {
        DSDisplayManager tfDisplayManger = InSysDSDisplayer.DSDisplayManager.Instance;
        TFPlayerWindow _tfPlayerWindow;
        TFPlayerWindowViewModel _tfPlayerWindowViewModel = TFPlayerWindowViewModel.Instance;
        DisplayUIViewModel _displayUIViewModel = DisplayUIViewModel.Instance;
        public ChangedTouchPageDelegate ChangedTouchPageHandler;
        public PassedProjectLifeTimeDelegate PassedProjectLifeTimeHandler;
        public UIElement PlayerScreen { get; set; }
        public UIElement PrimaryBuffer { get; set; }//실제 PageView를 담는 Grid
        public Size PlayerContainerSize { get; set; }       

        #region 싱글톤

        //private static readonly TFPlayerManager _instance;

        //public static TFPlayerManager Instance
        //{
        //    get { return _instance; }
        //}
        
        //static TFPlayerManager()
        //{
        //    _instance = new TFPlayerManager();
        //}
        #endregion

        #region 생성자
        public TFPlayerManager()
        {
            tfDisplayManger.ChangedTouchPageHandler = new InSysDSDisplayer.ChangedTouchPageDelegate(ChangedTouchPage);
            tfDisplayManger.PassedProjectLifeTimeHandler = new PassedProjectLifeTimeDelegate(PassedProjectLifeTime);

            //HookManager.KeyPress += HookManager_KeyDown;
            //HookManager.MouseUp += HookManager_MouseUp;

            //tfDisplayManger.StartWndProcHandler();
            InitLog();
        }

        ~TFPlayerManager()
        {
            //HookManager.KeyPress -= HookManager_KeyDown;
            //HookManager.MouseUp -= HookManager_MouseUp;

            if (tfDisplayManger != null)
                tfDisplayManger.StopWndProcHandler();
        }

       

        public TFPlayerManager(TFPlayerWindow tfPlayerWindow)
            : this()
        {
            _tfPlayerWindow = tfPlayerWindow;

            
        }
        #endregion

        private void InitLog()
        {
			Logger logger = LogManager.GetCurrentClassLogger();// LogManager.GetLogger("TouchDesignerLog");
            //string logFolder = AppDomain.CurrentDomain.BaseDirectory + "TouchDesignerLog";
			//FileTarget target = new FileTarget();
			//target.Layout = "${longdate}\t[${level}]\t[${callsite}]\t${message}";
			//target.FileName = logFolder + @"\" + "TouchDesignerLog.txt";
			//target.ArchiveFileName = logFolder + @"\" + "archives/logdb.{#####}.txt";
			//target.MaxArchiveFiles = 31;
			//target.ArchiveEvery = FileArchivePeriod.Day;

			//target.ArchiveNumbering = ArchiveNumberingMode.Sequence;
			//target.ConcurrentWrites = true;

			//NLog.Config.SimpleConfigurator.ConfigureForTargetLogging(target, LogLevel.Debug);

            logger.Info("TouchDesigner Player Manager start");
        } 
       
        public void SetTFWindow(TFPlayerWindow tfPlayerWindow)
        {
            _tfPlayerWindow = tfPlayerWindow;
        }

        public bool OpenTFFile()
        {
            try
            {
                OpenFileDialog openFileDialog = new OpenFileDialog();

                openFileDialog.Filter = "Touch Project(.tpf)| *.tpf";
                openFileDialog.Title = "Touch Project 파일 Open";
                openFileDialog.FileOk += (of_sender, of_e) =>
                {
                    string filefullpath = openFileDialog.FileName;
                    FileInfo fi = new FileInfo(filefullpath);
                    string filename = fi.Name;
                    if (System.IO.Path.GetExtension(filename).ToLower() == ".tpf")
                    {
                        OpenTouchProject(filefullpath);                        
                    }
                };

                if (openFileDialog.ShowDialog() != DialogResult.OK)
                {
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {                
                return false;
            }         
        }

        public void OpenTouchProject(string touchProject)
        {
            tfDisplayManger.SetDisplayUIByTouchflowPlayer(touchProject);
            tfDisplayManger.PreviewProject(null, this._tfPlayerWindow, true);            
        }

        public void InitTouch()
        {
            this.tfDisplayManger.InitTouch();
        }

        /// <summary>
        /// 사용하지 않음.
        /// </summary>
        /// <param name="projectPath"></param>
        /// <returns></returns>
        public TouchProjectInfo LoadTouchProject(string projectPath)
        {
            tfDisplayManger.StartWndProcHandler();
            return tfDisplayManger.LoadTouchProject(projectPath);
        }

        /// <summary>
        /// Touch Program Load
        /// </summary>
        /// <param name="projectPath"></param>
        /// <param name="scheduleTime"></param>
        /// <returns></returns>
        public Dictionary<string, object> LoadTouchProjectData(string projectPath, double scheduleTime)
        {
            tfDisplayManger.StartWndProcHandler();
            return tfDisplayManger.LoadTouchProjectData(projectPath, scheduleTime);
        }

        private void ChangedTouchPage(UIElement playlistPlayer, UIElement changedPage)
        {
            if (ChangedTouchPageHandler != null)
            {
                ChangedTouchPageHandler(this.PlayerScreen, changedPage);
            }
        }

        /// <summary>
        /// Project Time이 지날 경우 발생.
        /// </summary>
        private void PassedProjectLifeTime(UIElement playlistPlayer)
        {
            if (PassedProjectLifeTimeHandler != null)
            {
                PassedProjectLifeTimeHandler(this.PlayerScreen);
            }
        }

        public void PlayTouch()
        {
            tfDisplayManger.PlayTouch();
        }

        public void PlayTouch(string pageid)
        {
            tfDisplayManger.PlayTouch();
        }

        public void ClosedCurrentTouchSchedule()
        {
            tfDisplayManger.ClosedCurrentTouchSchedule();
        }

        public void BackupCurrentTouchSchedule()
        {
            tfDisplayManger.BackupCurrentTouchSchedule();
        }

        public void ClosedPreviousTouchScheduleProgram()
        {
            tfDisplayManger.ClosedPreviousTouchScheduleProgram();
        }

        public void CloseTouch(string pageid)
        {
            tfDisplayManger.BackupCurrentTouchSchedule();
        }

        public UIElement GetRootTouchPage()
        {
            return tfDisplayManger.GetRootTouchPage();
        }

        public UIElement GetTouchPage(string pageid)
        {
            return tfDisplayManger.GetTouchPage(pageid);
        }

        public void SetOnScreenKeyboardParent(Window displayWindow)
        {
            tfDisplayManger.SetOnScreenKeyboardParent(displayWindow);
        }

        public void CloseOnScreenKeyboardParent()
        {
            tfDisplayManger.CloseOnScreenKeyboard();
        }

        public void ShowOnScreenKeyboard(FrameworkElement element, Window owner)
        {
            tfDisplayManger.ShowOnScreenKeyboard(element, owner);
        }

        public void ResetTouchScreenScheduleTime()
        {
            tfDisplayManger.ResetTouchScreenScheduleTime();
        }

        private void HookManager_KeyDown(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            tfDisplayManger.ResetTouchStopWatch();
        }

        private void HookManager_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            tfDisplayManger.ResetTouchStopWatch();
            tfDisplayManger.MonitoringRectArea(e);
        }

        public static void TouchDown(TouchDownDelegate delegateFunc)
        {
            DSDisplayManager.TouchDown(delegateFunc);
        }

        public void SizeChangedScreen(Size size)
        {
            tfDisplayManger.SizeChangedScreen(size);
        }

        public void SetPlayerContainerSize(Size size)
        {
            tfDisplayManger.SetPlayerContainerSize(size);
        }

        public void StartWndProcHandler()
        {
            if (tfDisplayManger != null)
                tfDisplayManger.StartTouchProjectTimer();
        }

        public void StopWndProcHandler()
        {
            if (tfDisplayManger != null)
                tfDisplayManger.StopWndProcHandler();
        }

        public void _MousePressed(object sender, InputDevice.MouseControlEventArgs e)
        {
            if (tfDisplayManger != null)
                tfDisplayManger._MousePressed(sender, e);
            
            //Point point = e.Point;
            //tfDisplayManger.ResetTouchStopWatch();
            //tfDisplayManger.MonitoringRectArea(point.X, point.Y); 
        }

        public void _KeyPressed(object sender, InputDevice.KeyControlEventArgs e)
        {
            if (tfDisplayManger != null)
                tfDisplayManger._KeyPressed(sender, e);
        }

        public void SetPlayerWindowState(Window palyerWidnow, WindowState playerWindowState)
        {
            if (tfDisplayManger != null)
            {
                tfDisplayManger.PlayerWindowState = playerWindowState;
                //tfDisplayManger.PlayerWindow = palyerWidnow;
            }
        }
    }
}
