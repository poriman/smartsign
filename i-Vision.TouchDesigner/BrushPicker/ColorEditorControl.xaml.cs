﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BrushPicker
{
    /// <summary>
    /// Interaction logic for ColorEditorControl.xaml
    /// </summary>
    public partial class ColorEditorControl : UserControl
    {
        private string previousHexText;
        private string previousText;

        public Brush BrushValue { get; set; }
        public BrushPicker.Delegates.ChangedBrushDelegate ChangedBrushHandler;

        public ColorEditorControl()
        {
            InitializeComponent();

            this.Loaded += (s, e) =>
                {
                    SendCurrentGradientBrush(this.Picker.SelectedColor);
                };
        }

        public void SetChangeBrushColorEvent(BrushPicker.Delegates.ChangedBrushDelegate delegateFunc)
        {
            ColorPicker.ColorChangedHandler = new ColorPicker.SelectedColorChangedDelegate(SelectedColorChanged);            
        }             

        void SelectedColorChanged(Color color)
        {
            SendCurrentGradientBrush(color);
        }

        private void SendCurrentGradientBrush(Color color)
        {
            SolidColorBrush solid = new SolidColorBrush(color);  
            if (ChangedBrushHandler != null)
            {
                ChangedBrushHandler(solid);                
            }

            ChangedBrush(color);          
        }

        public void SetColorPicker(Brush brush)
        {  
            ColorConverter colorconv = new ColorConverter();
            Color color = (Color)ColorConverter.ConvertFromString(brush.ToString());

            Picker.SelectedColor = color;
            BrushValue = brush;
        }

        private void ChangedBrush(Brush brush)
        {
            this.ColorPreview.Fill = brush;
            BrushValue = brush;
        }

        public void ChangedBrush(Color color)
        {
            if (this.ColorPreview != null)
            {
                SolidColorBrush solid = new SolidColorBrush(color);
                this.ColorPreview.Fill = solid;
                BrushValue = solid;
            }
        }
       
        private void txtAlpha_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            TextBox textbox = sender as TextBox;
            
            e.Handled = !ColorUtilities.ValidColorAlpha(e.Text);
        }

        private void txtAlphaHex_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {          
            TextBox textbox = sender as TextBox;
            e.Handled = !ColorUtilities.ValidColorAlphaHex(e.Text, textbox.Text);
        }

        private void txtAlphaHex_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (this.txtAlphaHex == null)
                return;

            if (this.txtAlphaHex.Text.Length > 2)
                this.txtAlphaHex.Text = previousHexText;
            else
                previousHexText = this.txtAlphaHex.Text;

            string hex = String.Format("{0:X2}", this.txtAlphaHex.Text);
            if (hex == "")
                hex = "00";
            if (hex.Length == 1)
                hex = String.Format("0{0}", hex);
            byte[] bytevalue = ColorUtilities.HexStringToByteArray(hex);

            this.Picker.SelectedColor = Color.FromArgb(bytevalue[0], this.Picker.SelectedColor.R, this.Picker.SelectedColor.G, this.Picker.SelectedColor.B);
            ChangedBrush(this.Picker.SelectedColor);
        }

        private void txtAlpha_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (this.txtAlpha == null)
                return;

            int value = Convert.ToInt32(this.txtAlpha.Text);            
            if (value > 255)
            {
                this.txtAlpha.Text = previousText;
            }
            else
            {
                previousText = this.txtAlpha.Text = value.ToString();
                this.Picker.SelectedColor = Color.FromArgb(Convert.ToByte(value), this.Picker.SelectedColor.R, this.Picker.SelectedColor.G, this.Picker.SelectedColor.B);
                ChangedBrush(this.Picker.SelectedColor);
            }
        }
    }
}
