﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Data;
using System.Windows.Media;

namespace BrushPicker
{
    public class HueToSolidBrush : IValueConverter
    {
        public static HueToSolidBrush Instance = new HueToSolidBrush();
        #region IValueConverter 멤버

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            double Hue = (double)value;
            if (Hue >= 360) Hue = 0;
            return ColorUtilities.CreateColorFromHSB(255, Hue, 1, 1);
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        #endregion
    }

    public static class ColorUtilities
    {

        public static double[] ToHSB(this Color Source)
        {
            double R = ((double)Source.R / 255.0);
            double G = ((double)Source.G / 255.0);
            double B = ((double)Source.B / 255.0);

            double Max = Math.Max(R, Math.Max(G, B));
            double Min = Math.Min(R, Math.Min(G, B));

            double Hue = 0.0, Saturation = 0.0, Brightness = 0.0;

            if (Max == R && G >= B && (Max - Min == 0)) Hue = 0;
            else if (Max == R && G >= B) Hue = 60 * (G - B) / (Max - Min);
            else if (Max == R && G < B) Hue = 60 * (G - B) / (Max - Min) + 360;
            else if (Max == G) Hue = 60 * (B - R) / (Max - Min) + 120;
            else if (Max == B) Hue = 60 * (R - G) / (Max - Min) + 240;

            Saturation = (Max == 0) ? 0.0 : (1.0 - (Min / Max));
            Brightness = Max;

            return new double[3] { Hue, Saturation, Brightness };
        }

        public static Color CreateColorFromHSB(byte Alpha, double Hue, double Saturation, double Brightness)
        {

            if (Hue >= 360) Hue = 0;

            if (Saturation == 0)
            {
                byte ColorValue = (byte)(Brightness * 255);
                return Color.FromArgb(Alpha, ColorValue, ColorValue, ColorValue);
            }

            double SectorPosistion = Hue / 60.0;
            int SectorIndex = (int)(Math.Floor(SectorPosistion));
            double FractionalSector = SectorPosistion - SectorIndex;

            double p = Brightness * (1.0 - Saturation);
            double q = Brightness * (1.0 - (Saturation * FractionalSector));
            double t = Brightness * (1.0 - (Saturation * (1 - FractionalSector)));

            if (SectorIndex == 0) return Color.FromArgb(Alpha, (byte)(Brightness * 255), (byte)(t * 255), (byte)(p * 255));
            else if (SectorIndex == 1) return Color.FromArgb(Alpha, (byte)(q * 255), (byte)(Brightness * 255), (byte)(p * 255));
            else if (SectorIndex == 2) return Color.FromArgb(Alpha, (byte)(p * 255), (byte)(Brightness * 255), (byte)(t * 255));
            else if (SectorIndex == 3) return Color.FromArgb(Alpha, (byte)(p * 255), (byte)(q * 255), (byte)(Brightness * 255));
            else if (SectorIndex == 4) return Color.FromArgb(Alpha, (byte)(t * 255), (byte)(p * 255), (byte)(Brightness * 255));
            else if (SectorIndex == 5) return Color.FromArgb(Alpha, (byte)(Brightness * 255), (byte)(p * 255), (byte)(q * 255));

            return Colors.White;

        }

        public static bool ValidColorAlphaHex(string text, string previousText)
        {
            bool ret = true;
            if (text == System.Globalization.NumberFormatInfo.CurrentInfo.CurrencyDecimalSeparator |
                text == System.Globalization.NumberFormatInfo.CurrentInfo.CurrencyGroupSeparator |
                text == System.Globalization.NumberFormatInfo.CurrentInfo.CurrencySymbol |
                text == System.Globalization.NumberFormatInfo.CurrentInfo.NegativeSign |
                text == System.Globalization.NumberFormatInfo.CurrentInfo.NegativeInfinitySymbol |
                text == System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator |
                text == System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator |
                text == System.Globalization.NumberFormatInfo.CurrentInfo.PercentDecimalSeparator |
                text == System.Globalization.NumberFormatInfo.CurrentInfo.PercentGroupSeparator |
                text == System.Globalization.NumberFormatInfo.CurrentInfo.PercentSymbol |
                text == System.Globalization.NumberFormatInfo.CurrentInfo.PerMilleSymbol |
                text == System.Globalization.NumberFormatInfo.CurrentInfo.PositiveInfinitySymbol |
                text == System.Globalization.NumberFormatInfo.CurrentInfo.PositiveSign)
                return ret;

            int l = text.Length;
            for (int i = 0; i < l; i++)
            {
                char ch = text[i];
                //ret &= Char.IsLetter(ch);

                if (Char.IsLetter(ch) == true)
                {
                    if (ret == true)
                    {
                        string s = ch.ToString();
                        ch = Convert.ToChar(s.ToUpper());
                        if (ch > 'F')
                            ret &= false;
                    }
                }
                else if (Char.IsDigit(ch) == true)
                {
                    
                }
                else
                {
                    ret &= false;
                }
            }

            return ret; 
        }

        public static bool ValidColorAlpha(string text)
        {
            bool ret = true;
            if (text == System.Globalization.NumberFormatInfo.CurrentInfo.CurrencyDecimalSeparator |
                text == System.Globalization.NumberFormatInfo.CurrentInfo.CurrencyGroupSeparator |
                text == System.Globalization.NumberFormatInfo.CurrentInfo.CurrencySymbol |
                text == System.Globalization.NumberFormatInfo.CurrentInfo.NegativeSign |
                text == System.Globalization.NumberFormatInfo.CurrentInfo.NegativeInfinitySymbol |
                text == System.Globalization.NumberFormatInfo.CurrentInfo.NumberDecimalSeparator |
                text == System.Globalization.NumberFormatInfo.CurrentInfo.NumberGroupSeparator |
                text == System.Globalization.NumberFormatInfo.CurrentInfo.PercentDecimalSeparator |
                text == System.Globalization.NumberFormatInfo.CurrentInfo.PercentGroupSeparator |
                text == System.Globalization.NumberFormatInfo.CurrentInfo.PercentSymbol |
                text == System.Globalization.NumberFormatInfo.CurrentInfo.PerMilleSymbol |
                text == System.Globalization.NumberFormatInfo.CurrentInfo.PositiveInfinitySymbol |
                text == System.Globalization.NumberFormatInfo.CurrentInfo.PositiveSign)
                return ret;

            int l = text.Length;
            for (int i = 0; i < l; i++)
            {
                char ch = text[i];
                ret &= Char.IsDigit(ch);
            }

            return ret; 
        }

        public static byte[] HexStringToByteArray(string Hex)
        {
            byte[] Bytes = new byte[Hex.Length / 2];
            int[] HexValue = new int[] { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09,
                                 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0A, 0x0B, 0x0C, 0x0D,
                                 0x0E, 0x0F };

            for (int x = 0, i = 0; i < Hex.Length; i += 2, x += 1)
            {
                Bytes[x] = (byte)(HexValue[Char.ToUpper(Hex[i + 0]) - '0'] << 4 |
                                  HexValue[Char.ToUpper(Hex[i + 1]) - '0']);
            }

            return Bytes;
        }
    }
}
