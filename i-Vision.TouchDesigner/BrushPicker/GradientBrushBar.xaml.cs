﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BrushPicker
{
    /// <summary>
    /// Interaction logic for GradientBrushBar.xaml
    /// </summary>
    public partial class GradientBrushBar : UserControl
    {
        public static BrushPicker.Delegates.ChangedBrushDelegate BrushChangedHandler;
                
        private bool isButtonDown = false;
        private GradientStopBar SelectedGradientStopBar = null;
        List<GradientStop> _gradientStops = new List<GradientStop>();
        LinearGradientBrush linearGradientBrush = new LinearGradientBrush(Colors.Black, Colors.White, new Point(0, 0.5), new Point(1, 0.5));

        #region CurrentColorProperty
        private static readonly DependencyProperty CurrentColorProperty =
            DependencyProperty.Register("CurrentColor", typeof(Color), typeof(GradientBrushBar), new PropertyMetadata(new PropertyChangedCallback(Changed_CurrentColor)));

        public Color CurrentColor
        {
            get { return (Color)GetValue(CurrentColorProperty); }
            set { SetValue(CurrentColorProperty, value); }
        }

        private static void Changed_CurrentColor(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {           
        }

        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);

            if (e.Property.Name.Equals("CurrentColor") == true)
            {
                if (this.SelectedGradientStopBar != null)
                {
                    Color color = (Color)ColorConverter.ConvertFromString(e.NewValue.ToString());
                    GetGradientStrop(this.SelectedGradientStopBar.GradientStop, color, this.SelectedGradientStopBar.Position / this.gradientColorArea.Width);

                    SendCurrentGradientBrush();
                }
            }
        }
        #endregion

        private void GetGradientStrop(GradientStop gradientStop, Color color, double offset)
        {
            if (gradientStop == null)
            {
                gradientStop = new GradientStop();
            }           

            gradientStop.Color = color;
            gradientStop.Offset = offset;
        }

        private GradientStop CreateNewGradientStrop(Color color, double offset)
        {
            GradientStop g;
            g = new GradientStop();
            g.Color = color;
            g.Offset = offset;

            return g;
        }

        public GradientBrushBar()
        {
            InitializeComponent();

            this.canvas.Loaded += (s, e) =>
                {
                    //if (linearGradientBrush == null)
                    //    linearGradientBrush = (LinearGradientBrush)this.gradientColorArea.Fill;
                };
           
            this.MouseLeftButtonUp += new MouseButtonEventHandler(GradientBrushBar_MouseLeftButtonUp);
            this.MouseMove += new MouseEventHandler(GradientBrushBar_MouseMove);
            this.SizeChanged += new SizeChangedEventHandler(GradientBrushBar_SizeChanged);
        }

        void GradientBrushBar_SizeChanged(object sender, SizeChangedEventArgs e)
        {            
            ChangedGradientColorAreaSize();
            ChangedGradientStopBar(e);
        }

        private void ChangedGradientColorAreaSize()
        {
            gradientColorArea.Width = this.canvas.ActualWidth - 10;
            gradientColorArea.Height = this.canvas.ActualHeight - 25;
            Canvas.SetLeft(gradientColorArea, 0);
            Canvas.SetTop(gradientColorArea, 0);
        }

        private void ChangedGradientStopBar(SizeChangedEventArgs e)
        {
            var list = this.canvas.Children.OfType<GradientStopBar>().ToList();
            foreach (var item in list)
            {
                double position = (e.NewSize.Width * item.Position) / e.PreviousSize.Width;
                SetGradientStopBarPosition(item, position, this.gradientColorArea.Height);
            }
        }

        private void GradientColorArea_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Point point = e.GetPosition(this.gradientColorArea);

            if (SelectedGradientStopBar != null)
                SelectedGradientStopBar.IsSelected = false;

            this.SelectedGradientStopBar = InsertGradientStropBar(point);
            SelectedGradientStopBar.IsSelected = true;
        }

        private GradientStopBar InsertGradientStropBar(Point point)
        {
            GradientStopBar newGradientStopBar = new GradientStopBar();

            SetGradientStopBarEvent(newGradientStopBar);
            SetGradientStopBarPosition(newGradientStopBar, point.X, this.gradientColorArea.Height);

            newGradientStopBar.GradientStop = CreateNewGradientStrop(this.CurrentColor, point.X / this.gradientColorArea.Width);

            this._gradientStops.Add(newGradientStopBar.GradientStop);

            this.canvas.Children.Add(newGradientStopBar);

            SetGradient();

            return newGradientStopBar;
        }

        void GradientStopBar_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            isButtonDown = true;
            if (this.SelectedGradientStopBar != null)
                this.SelectedGradientStopBar.IsSelected = false;

            this.SelectedGradientStopBar = sender as GradientStopBar;
            if (this.SelectedGradientStopBar != null)
            {
                this.SelectedGradientStopBar.CaptureMouse();
                this.SelectedGradientStopBar.IsSelected = true;

                this.CurrentColor = this.SelectedGradientStopBar.GradientStop.Color;
            }            
        }
        
        void GradientStopBar_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (SelectedGradientStopBar != null)
            {
                isButtonDown = false;
                SelectedGradientStopBar.ReleaseMouseCapture();               
            }
        }       

        private void GradientBrushBar_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            isButtonDown = false;
        }

        private void GradientBrushBar_MouseMove(object sender, MouseEventArgs e)
        {
            if (this.SelectedGradientStopBar != null && isButtonDown == true)
            {
                Point point = e.GetPosition(this.gradientColorArea);

                if (point.X >= 0 && point.X <= this.gradientColorArea.Width)
                {
                    Canvas.SetLeft(this.SelectedGradientStopBar, point.X);

                    this.SelectedGradientStopBar.GradientStop.Offset = point.X / this.gradientColorArea.Width;
                    this.SelectedGradientStopBar.Position = point.X;

                    SendCurrentGradientBrush();
                }
            }            
        }

        private void GradientColorArea_Loaded(object sender, RoutedEventArgs e)
        {
            var gradientStopBarList = this.canvas.Children.OfType<GradientStopBar>().ToList();
            foreach (var gradientStopBar in gradientStopBarList)
                this.canvas.Children.Remove(gradientStopBar);
            _gradientStops.Clear();

            LinearGradientBrush lgbrush = (LinearGradientBrush)gradientColorArea.Fill;
            GradientStopCollection gscollection = lgbrush.GradientStops;

            foreach (var gradientStop in gscollection)
            {
                GradientStopBar newGradientStopBar = new GradientStopBar();
                newGradientStopBar.GradientStop = gradientStop;
                
                _gradientStops.Add(gradientStop);

                newGradientStopBar.Position = gradientStop.Offset * this.gradientColorArea.Width;

                SetGradientStopBarEvent(newGradientStopBar);
                SetGradientStopBarPosition(newGradientStopBar, newGradientStopBar.Position, this.gradientColorArea.Height);
                
                this.canvas.Children.Add(newGradientStopBar);
            }

            SendCurrentGradientBrush();
        }

        public void Init_SelectColor()
        {
            var selectedStopBar = this.canvas.Children.OfType<GradientStopBar>().FirstOrDefault();
            if (selectedStopBar != null)
            {
                this.SelectedGradientStopBar = selectedStopBar;
                this.SelectedGradientStopBar.IsSelected = true;
                this.CurrentColor = this.SelectedGradientStopBar.GradientStop.Color;
            }       
        }

        public void SetBrush(Brush brush)
        {
            if (brush.GetType() == typeof(LinearGradientBrush))
            {
                this.gradientColorArea.Fill = new LinearGradientBrush(linearGradientBrush.GradientStops, new Point(0.0, 0.5), new Point(1.0, 0.5));//LinearGradientBrush 편집 화면에서 GradientStop 방향은 고정이다.
            }
            else
            {
                this.gradientColorArea.Fill = brush;
            }
            linearGradientBrush = (LinearGradientBrush)brush;
        }

        public Brush GetBrush()
        {
            return linearGradientBrush;
        }

        private void InsertGradientStopBar()
        {
        }

        private void SetGradientStopBarPosition(GradientStopBar item, double left, double top)
        {
            Canvas.SetLeft(item, left);
            Canvas.SetTop(item, top);
            item.Position = left;
        }

        private void SetGradientStopBarEvent(GradientStopBar newItem)
        {
            newItem.MouseLeftButtonDown += new MouseButtonEventHandler(GradientStopBar_MouseLeftButtonDown);
            newItem.MouseLeftButtonUp += new MouseButtonEventHandler(GradientStopBar_MouseLeftButtonUp);                            
        }

        private void SetGradient()
        {
            LinearGradientBrush lgbrush = (LinearGradientBrush)this.gradientColorArea.Fill;
            GradientStopCollection gscollection = lgbrush.GradientStops;
          
            gscollection.Clear();
      
            for (int i = 0; i < _gradientStops.Count; i++)
            {
                gscollection.Add(_gradientStops[i]);
            }
        }

        private void SendCurrentGradientBrush()
        {
            if (BrushChangedHandler != null)
            {
                linearGradientBrush.GradientStops = ((LinearGradientBrush)this.gradientColorArea.Fill).GradientStops;

                BrushChangedHandler(linearGradientBrush);
            }
        }

    }
}
