﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;

namespace BrushPicker
{
    /// <summary>
    /// Interaction logic for GradientStopBar.xaml
    /// </summary>
    public partial class GradientStopBar : UserControl, INotifyPropertyChanged
    {
        public GradientStop GradientStop { get; set; }
        public double Position { get; set; }

        private bool isSelected;
        public bool IsSelected 
        {
            get { return isSelected; }
            set 
            {
                isSelected = value;
                if (isSelected == true)
                {
                    this.Background = new SolidColorBrush(Colors.Yellow);
                }
                else
                {
                    this.Background = new SolidColorBrush(Colors.White);
                }
                OnPropertyChanged("IsSelected"); 
            }
        }

        public GradientStopBar()
        {
            InitializeComponent();
            isSelected = false;
            GradientStop = new GradientStop();
            this.DataContext = this;           
        }

        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        public virtual void OnPropertyChanged(params string[] propertyNames)
        {
            if (PropertyChanged == null)
                return;

            foreach (string propertyName in propertyNames)
            {
                try
                {
                    PropertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
                }
                catch { }
            }
        }

        #endregion
    }
}
