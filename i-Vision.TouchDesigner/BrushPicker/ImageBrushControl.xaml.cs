﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;

namespace BrushPicker
{
    /// <summary>
    /// Interaction logic for ImageBrushControl.xaml
    /// </summary>
    public partial class ImageBrushControl : UserControl
    {
        public Brush BrushValue { get; private set; }

        public ImageBrushControl()
        {
            InitializeComponent();
        }

        private void FileBroswerButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();

            openFileDialog.Filter = "Image|*.jpg;*.bmp;*.png|JPEG|*.jpg|BMP| *.gif|PNG|*.png";
            openFileDialog.Title = "Add an image background";
            openFileDialog.FileOk += (of_sender, of_e) =>
            {
                BitmapImage imageSource = new BitmapImage();
                imageSource.BeginInit();
                imageSource.UriSource = new UriBuilder(openFileDialog.FileName).Uri;
                imageSource.EndInit();

                ImageControl.Source = imageSource;
                ImageBrush imageBrush = new ImageBrush();
                imageBrush.Stretch = Stretch.Fill;
                imageBrush.ImageSource = imageSource;
                //PreviewBackground.Background = imageBrush;

                BrushValue = imageBrush;
            };

            if (openFileDialog.ShowDialog() != true)
            {
                
            }
        }
    }
}
