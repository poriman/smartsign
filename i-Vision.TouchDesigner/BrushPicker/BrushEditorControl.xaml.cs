﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BrushPicker
{
    
    /// <summary>
    /// Interaction logic for BrushEditorControl.xaml
    /// </summary>
    public partial class BrushEditorControl : UserControl
    {
        private string previousHexText;
        private string previousText;
        public Brush UserSettingLinearBrush = new LinearGradientBrush();
        public Brush BrushValue { get; private set; }
        public BrushPicker.Delegates.ChangedBrushDelegate ChangedBrushHandler;

        public BrushEditorControl()
        {
            InitializeComponent();

            this.Loaded += (s, e) =>
            {
                Console.Write("");
            };
        }

        public void SetChangeBrushColorEvent(BrushPicker.Delegates.ChangedBrushDelegate delegateFunc)
        {
            GradientBrushBar.BrushChangedHandler = new Delegates.ChangedBrushDelegate(ChangedBrushCallback); 
        }

        private void ChangedBrushCallback(Brush brush)
        {            
            if (brush.GetType() == typeof(LinearGradientBrush))
            {
                LinearGradientBrush linearBrush = (LinearGradientBrush)brush;
                SetDirectionComboBox(linearBrush);
                if (UserSettingLinearBrush == null)
                    return;

                //linearBrush.GradientStops = ((LinearGradientBrush)UserSettingLinearBrush).GradientStops;
                this.ColorPreview.Fill = linearBrush;
                BrushValue = linearBrush;
            }
            else
            {
                this.ColorPreview.Fill = brush;
                BrushValue = brush;
            }
        }

        private void ChangedBrush(Brush brush)
        {
            if (brush.GetType() == typeof(LinearGradientBrush))
            {
                LinearGradientBrush linearBrush = (LinearGradientBrush)brush;
                SetDirectionComboBox(linearBrush);
                if (UserSettingLinearBrush == null)
                    return;

                linearBrush.GradientStops = ((LinearGradientBrush)UserSettingLinearBrush).GradientStops;
                this.ColorPreview.Fill = linearBrush;
                BrushValue = linearBrush;
            }
            else
            {
                this.ColorPreview.Fill = brush;
                BrushValue = brush;
            }
        }

        private void SetDirectionComboBox(LinearGradientBrush linearBrush)
        {
            string direction = "Right";            
            if (linearBrush.EndPoint == new Point(0.0, 0.5) && linearBrush.StartPoint == new Point(1.0, 0.5)) //"Left"
            {
                direction = "Left";
            }
            else if (linearBrush.EndPoint == new Point(1.0, 0.5) && linearBrush.StartPoint == new Point(0.0, 0.5))//Right
            {
                direction = "Right";
            }
            else if (linearBrush.EndPoint == new Point(0.5, 0.0) && linearBrush.StartPoint == new Point(0.5, 1.0))//Top
            {
                direction = "Top";
            }
            else if (linearBrush.EndPoint == new Point(0.5, 1.0) && linearBrush.StartPoint == new Point(0.5, 0.0))//Bottom
            {
                direction = "Bottom";
            }

            foreach (var item in GradientDirectionComboBox.Items)
            {
                ComboBoxItem comboboxitem = (ComboBoxItem)item;
                if (direction.Equals((string)comboboxitem.Tag) == true)
                {
                    GradientDirectionComboBox.SelectedItem = item;
                    break;
                }

            }
        }

        private void ChangedBrush(Color color)
        {
            if (this.ColorPreview != null)
            {
                SolidColorBrush solid = new SolidColorBrush(color);
                this.ColorPreview.Fill = solid;
                BrushValue = solid;
            }
            else
            {
            }
        }

        private void SetColorPicker(Color color)
        {
            this.Picker.SelectedColor = color;
        }

        public void SetBrush(Brush brush)
        {
            UserSettingLinearBrush = brush;
            this.gradientBrushBar.SetBrush(brush);            
        }

        public Brush GetBrush()
        {
            if (BrushValue.GetType() == typeof(LinearGradientBrush))
            {
                BrushValue = this.ColorPreview.Fill;
            }
            else if (BrushValue.GetType() == typeof(RadialGradientBrush))
            {
                BrushValue = this.ColorPreview.Fill;
            }
            else
            {
                BrushValue = this.ColorPreview.Fill;
            }

            return BrushValue;
        }

        private void txtAlpha_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            TextBox textbox = sender as TextBox;

            e.Handled = !ColorUtilities.ValidColorAlpha(e.Text);
        }

        private void txtAlpha_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (this.txtAlphaHex == null)
                return;
            if (this.txtAlphaHex.Text.Length > 2)
                this.txtAlphaHex.Text = previousHexText;
            else
                previousHexText = this.txtAlphaHex.Text;

            string hex = String.Format("{0:X2}", this.txtAlphaHex.Text);
            if (hex == "")
                hex = "00";
            if (hex.Length == 1)
                hex = String.Format("0{0}", hex);
            byte[] bytevalue = ColorUtilities.HexStringToByteArray(hex);

            this.Picker.SelectedColor = Color.FromArgb(bytevalue[0], this.Picker.SelectedColor.R, this.Picker.SelectedColor.G, this.Picker.SelectedColor.B);
            ChangedBrush(this.Picker.SelectedColor);
        }

        private void txtAlphaHex_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            TextBox textbox = sender as TextBox;
            e.Handled = !ColorUtilities.ValidColorAlphaHex(e.Text, textbox.Text);
        }

        private void txtAlphaHex_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (this.txtAlpha == null)
                return;
            int value = Convert.ToInt32(this.txtAlpha.Text);
            if (value > 255)
            {
                this.txtAlpha.Text = previousText;
            }
            else
            {
                previousText = this.txtAlpha.Text = value.ToString();
                this.Picker.SelectedColor = Color.FromArgb(Convert.ToByte(value), this.Picker.SelectedColor.R, this.Picker.SelectedColor.G, this.Picker.SelectedColor.B);
                ChangedBrush(this.Picker.SelectedColor);
            }
        }

        private void GradientDirectionComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox combobox = sender as ComboBox;
            if (combobox != null)
            {
                if (combobox.SelectedItem != null)
                {
                    Point StartPoint = new Point();
                    Point EndPoint = new Point();
                    ComboBoxItem comboboxitem = combobox.SelectedItem as ComboBoxItem;
                    if (comboboxitem != null && BrushValue != null)
                    {  
                        LinearGradientBrush brush = (LinearGradientBrush)this.ColorPreview.Fill;
                        GradientStopCollection gscol = brush.GradientStops;                        
                        
                        string direction = (string)comboboxitem.Tag;
                        if (direction.Equals("Left") == true)
                        {
                            StartPoint = new Point(1.0, 0.5);
                            EndPoint = new Point(0.0, 0.5);  
                        }
                        else if (direction.Equals("Right") == true)
                        {
                            StartPoint = new Point(0.0, 0.5);
                            EndPoint = new Point(1.0, 0.5);
                        }
                        else if (direction.Equals("Top") == true)
                        {
                            StartPoint = new Point(0.5, 1.0);
                            EndPoint = new Point(0.5, 0.0);                            
                        }
                        else if (direction.Equals("Bottom") == true)
                        {
                            StartPoint = new Point(0.5, 0.0);
                            EndPoint = new Point(0.5, 1.0);
                        }

                        LinearGradientBrush linear = new LinearGradientBrush(brush.GradientStops, StartPoint, EndPoint);                        
                        this.ColorPreview.Fill = linear;
                    }
                }
            }
        }
    }
}
