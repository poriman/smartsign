﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BrushPicker
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class BrushEditorWindow : Window
    {
        public Brush BrushValue { get; private set; }       

        private BrushEditorWindow()
        {
            InitializeComponent();            
        }

        public BrushEditorWindow(bool isSupportImageBrush) : this()
        {
            //InitializeComponent();

            ColorPickerTabControl.SetChangeBrushColorEvent(ChangedBrushDelegate);
            ColorPickerTabControl.ShowImageBackgroundTabControl(isSupportImageBrush); 
        }

        private void OKButton_Click(object sender, RoutedEventArgs e)
        {
            BrushValue = ColorPickerTabControl.GetBrush();
            this.DialogResult = true;
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void ChangedBrushDelegate(Brush brushValue)
        {
            BrushValue = brushValue;
        }

        public void SetBrush(Brush brush)
        {
            BrushValue = brush;
         
            ColorPickerTabControl.SetBrush(brush);
        }
    }
}
