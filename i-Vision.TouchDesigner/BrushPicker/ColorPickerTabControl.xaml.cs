﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BrushPicker
{
    /// <summary>
    /// Interaction logic for ColorPickerTabControl.xaml
    /// </summary>
    public partial class ColorPickerTabControl : TabControl
    {
        public Brush SelectedBrush { get; set; }
        public BrushPicker.Delegates.ChangedBrushDelegate ChangedBrushHandler;

        public ColorPickerTabControl()
        {
            InitializeComponent();
            this.Loaded += (s,e) =>
                {

                };
        }

        #region CurrentColorProperty
        private static readonly DependencyProperty TabItemStyleProperty =
            DependencyProperty.Register("TabItemStyle", typeof(Style), typeof(ColorPickerTabControl), new PropertyMetadata());

        public Style TabItemStyle
        {
            get { return (Style)GetValue(TabItemStyleProperty); }
            set { SetValue(TabItemStyleProperty, value); }
        }

        protected override void OnPropertyChanged(DependencyPropertyChangedEventArgs e)
        {
            base.OnPropertyChanged(e);

            if (e.Property.Name.Equals("TabItemStyle") == true)
            {
                foreach (var item in this.Items)
                {
                    (item as TabItem).Style = (Style)e.NewValue;
                }
            }
        }
        #endregion

        public void SetChangeBrushColorEvent(BrushPicker.Delegates.ChangedBrushDelegate delegateFunc)
        {
            this.brushEditorControl.SetChangeBrushColorEvent(delegateFunc);
            this.colorEditorControl.SetChangeBrushColorEvent(delegateFunc);
        }

        private void SendCurrentGradientBrush()
        {
            if (ChangedBrushHandler != null)
            {
                
            }
        }

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        public Brush GetBrush()
        {
            Brush brush = null;
            TabItem tabitem = this.SelectedItem as TabItem;
            if (tabitem != null)
            {
                if (tabitem.Content is BrushEditorControl)
                {
                    BrushEditorControl brusheditor = tabitem.Content as BrushEditorControl;                   
                    brush = brusheditor.GetBrush();
                }
                else if (tabitem.Content is ColorEditorControl)
                {
                    ColorEditorControl coloreditor = tabitem.Content as ColorEditorControl;
                    brush = coloreditor.BrushValue;
                }
                else if (tabitem.Content is ImageBrushControl)
                {
                    ImageBrushControl imageBrushEditor = tabitem.Content as ImageBrushControl;
                    brush = imageBrushEditor.BrushValue;
                }
            }

            return brush;
        }

        public void SetBrush(Brush brush)
        {
            if (brush is SolidColorBrush)
            {                
                TabItem tabitem = this.Items[1] as TabItem;
                this.SelectedItem = tabitem;
                ColorEditorControl brusheditor = tabitem.Content as ColorEditorControl;
                brusheditor.SetColorPicker(brush);
                
            }
            else if (brush is LinearGradientBrush)
            {
                TabItem tabitem = this.Items[0] as TabItem;
                this.SelectedItem = tabitem;
                BrushEditorControl coloreditor = tabitem.Content as BrushEditorControl;
                coloreditor.SetBrush(brush);
            }
            else if (brush is LinearGradientBrush)//Image가 Content일 경우에 대한 처리...현재 수행되지 않음.
            {
                TabItem tabitem = this.Items[2] as TabItem;
                this.SelectedItem = tabitem;
                ImageBrushControl imageBurhsEditor = tabitem.Content as ImageBrushControl;                
            }            
        }

        public void ShowImageBackgroundTabControl(bool isShow)
        {
            TabItem tabitem = this.Items[2] as TabItem;
            if (tabitem == null)
                return;
            if (isShow == true)
            {
                tabitem.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                tabitem.Visibility = System.Windows.Visibility.Hidden;
            }
        }
    }
}
