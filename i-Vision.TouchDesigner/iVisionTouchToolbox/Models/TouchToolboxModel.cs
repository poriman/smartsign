﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.Windows;

namespace iVisionTouchToolbox.Models
{
    public class ToolboxGroupModel : DependencyObject, INotifyPropertyChanged
    {
        public static readonly DependencyProperty ItemsProperty = DependencyProperty.RegisterAttached("Items", typeof(ObservableCollection<ToolboxItemModel>), typeof(ToolboxGroupModel), new PropertyMetadata());

        public ObservableCollection<ToolboxItemModel> Items
        {
            get { return (ObservableCollection<ToolboxItemModel>)GetValue(ToolboxGroupModel.ItemsProperty); }
            set { SetValue(ToolboxGroupModel.ItemsProperty, value); }
        }

        private string name;
        public string Name
        {
            get { return name; }
            set { name = value; OnPropertyChanged("Name"); }
        }

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public virtual void OnPropertyChanged(params string[] propertyNames)
        {
            if (PropertyChanged == null)
                return;

            foreach (string propertyName in propertyNames)
            {
                try
                {
                    PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
                }
                catch { }
            }
        }

        #endregion

        public ToolboxGroupModel()
        {
            Items = new ObservableCollection<ToolboxItemModel>();
        }
    }

    public class ToolboxItemModel : DependencyObject, INotifyPropertyChanged
    {
        private string name;
        public string Name
        {
            get { return name; }
            set { name = value; OnPropertyChanged("Name"); }
        }

        private string image;
        public string Image
        {
            get { return image; }
            set { image = value; OnPropertyChanged("Image"); }
        }

        private string groupName;
        public string GroupName
        {
            get { return groupName; }
            set { groupName = value; OnPropertyChanged("GroupName"); }
        }
                
        public ToolboxType ToolType
        {
            get { return (ToolboxType)GetValue(ToolboxItemModel.ToolTypeProperty); }
            set { SetValue(ToolboxItemModel.ToolTypeProperty, value); }
        }

        public static readonly DependencyProperty ToolTypeProperty = DependencyProperty.RegisterAttached("ToolType", typeof(ToolboxType), typeof(ToolboxItemModel), new PropertyMetadata(ToolboxType.Button));

        

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        public virtual void OnPropertyChanged(params string[] propertyNames)
        {
            if (PropertyChanged == null)
                return;

            foreach (string propertyName in propertyNames)
            {
                try
                {
                    PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
                }
                catch { }
            }
        }

        #endregion
    }

    public enum ToolboxType
    {
        Image,
        Video,
        Streaming,
        Audio,
        Button,
        Text,
        Label,
        ScrollText,
        RectArea,
        Rectangle,
        Ellipse,
        Map,
        Web,
        Mobile,
        SlideShow,
        Timer,
        Flash,
        RSS,
        DigitalClock,
        AnalogueClock,
        Date,
        SlideViewer,
        None

    }
}
