﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using iVisionTouchToolbox.Models;
using System.Collections.ObjectModel;
using UtilLib.StaticMethod;

namespace iVisionTouchToolbox
{
	/// <summary>
	/// Interaction logic for ToolboxControl.xaml
	/// </summary>
	public partial class ToolboxControl : UserControl
	{
        List<ListBox> ToolboxItems = new List<ListBox>();

        private object selectedToolboxGroup = null;//여러개의 Group ListBox가 있을 경우 각 ListBox별로 Focus가 유지되는 것을 막기 위해 필요.

        #region SelectedToolboxItemEvent : Toolbox의 Item이 선택되었을 때 발생하는 이벤트

        public static readonly RoutedEvent SelectedToolboxItemEvent = EventManager.RegisterRoutedEvent("SelectedToolboxItem", RoutingStrategy.Direct, typeof(RoutedEventHandler), typeof(ToolboxControl));

        public event RoutedEventHandler SelectedToolboxItem
        {
            add { AddHandler(SelectedToolboxItemEvent, value); }
            remove { RemoveHandler(SelectedToolboxItemEvent, value); }
        }

        private void RaiseSelectedToolboxItemEvent(object source)
        {
            RoutedEventArgs newEventArg = new RoutedEventArgs(ToolboxControl.SelectedToolboxItemEvent, source);
            RaiseEvent(newEventArg);
        }

        #endregion

        #region ToolboxGroupsProperty

        public static readonly DependencyProperty ToolboxGroupsProperty;
        public ObservableCollection<ToolboxGroupModel> ToolboxGroups
        {
            get { return (ObservableCollection<ToolboxGroupModel>)GetValue(ToolboxControl.ToolboxGroupsProperty); }
            set { SetValue(ToolboxControl.ToolboxGroupsProperty, value); }
        }

        #endregion

        #region ContentSelectionType

        //private static readonly DependencyProperty ContentSelectionTypeProperty;
        //public ToolboxType ContentSelectionType
        //{
        //    get { return (ToolboxType)GetValue(ToolboxControl.ContentSelectionTypeProperty); }
        //    set { SetValue(ToolboxControl.ContentSelectionTypeProperty, value); }
        //}

        #endregion

        static ToolboxControl()
        {
            ToolboxControl.ToolboxGroupsProperty = DependencyProperty.Register("ToolboxGroups", typeof(ObservableCollection<ToolboxGroupModel>), typeof(ToolboxControl), new FrameworkPropertyMetadata());
            //ToolboxControl.ContentSelectionTypeProperty = DependencyProperty.Register("ContentSelectionType", typeof(ToolboxType), typeof(ToolboxControl), new FrameworkPropertyMetadata());
        }
                
		public ToolboxControl()
		{
            this.InitializeComponent();

            ToolboxGroups = new ObservableCollection<ToolboxGroupModel>();
            this.DataContext = ToolboxGroups;            
		}        

        /// <summary>
        /// 스크롤 컨트롤을 찾는다.
        /// </summary>
        /// <returns></returns>
        private ScrollViewer FindScroll()
        {
            ListBox listbox = UIHelper.FindChild<ListBox>(this, "ToolboxContainer");
            if (listbox == null)
                return null;

            Border scroll_border = VisualTreeHelper.GetChild(listbox, 0) as Border;
            if (scroll_border is Border)
            {
                ScrollViewer scroll = scroll_border.Child as ScrollViewer;

                if (scroll is ScrollViewer)
                {
                    return scroll;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 오른쪽으로 스크롤한다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void svRight(object sender, RoutedEventArgs e)
        {
            ScrollViewer sv = FindScroll();

            if (sv != null)
                sv.LineRight();
            else
                throw new Exception("Could not find ScrollViewer.");
        }

        /// <summary>
        /// 왼쪽으로 스크롤한다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void svLeft(object sender, RoutedEventArgs e)
        {
            ScrollViewer sv = FindScroll();

            if (sv != null)
                sv.LineLeft();
            else
                throw new Exception("Could not find ScrollViewer.");
        }

        /// <summary>
        /// 아래오 스크롤한다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void svDown(object sender, RoutedEventArgs e)
        {
            ScrollViewer sv = FindScroll();

            if (sv != null)
                sv.LineDown();
            else
                throw new Exception("Could not find ScrollViewer.");
        }

        /// <summary>
        /// 위로 스크롤한다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void svUp(object sender, RoutedEventArgs e)
        {
            ScrollViewer sv = FindScroll();

            if (sv != null)
                sv.LineUp();
            else
                throw new Exception("Could not find ScrollViewer.");
        }
              
        /// <summary>
        /// ToolboxItem 선택시 호출되는 SelectionChanged 콜백함수.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToolboxItemListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ListBox listbox = sender as ListBox;
            if (listbox != null)
            {                
                if (listbox.SelectedItem != null)
                {
                    ToolboxItemModel itemModel = listbox.SelectedItem as ToolboxItemModel;
                    if (itemModel != null)
                    {
                        RaiseSelectedToolboxItemEvent(itemModel);

                        if (selectedToolboxGroup != null)
                        {
                            if (selectedToolboxGroup.Equals(listbox) != true)//이전 선택 Group일 아닐 경우 이전 ListBox의 SelectedItem을 해제한다.
                            {
                                ListBox provious_listbox = selectedToolboxGroup as ListBox;
                                provious_listbox.SelectedItem = null;
                            }
                        }
                    }
                }

                selectedToolboxGroup = listbox;
            }
        }

        public List<ListBox> GetToolboxContainerListBox()
        {
            return ToolboxItems;
        }
        
        /// <summary>
        /// 등록된 ToolBox Item 리스트를 얻어오기
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToolboxItemListBox_Loaded(object sender, RoutedEventArgs e)
        {
            ListBox listbox = sender as ListBox;
            if (listbox != null)
            {
                if (ToolboxItems.Contains(listbox) == false)
                    ToolboxItems.Add(listbox);
            }
        }

        /// <summary>
        /// 드래그 소스 설정
        /// </summary>
        /// <param name="dependencyProperty"></param>
        /// <param name="dragSourceAdvisor"></param>
        public void SetDragSourceAdvisor(DependencyProperty dependencyProperty , object dragSourceAdvisor)
        {
            foreach (var toolbox in ToolboxItems)
            {
                for (int i = 0; i < toolbox.Items.Count; i++)
                {
                    ListBoxItem listboxitem = (ListBoxItem)(toolbox.ItemContainerGenerator.ContainerFromIndex(i));
                    ToolboxItemModel tbItemModel = toolbox.Items[i] as ToolboxItemModel;
                    listboxitem.Tag = tbItemModel.ToolType.ToString();
                    object dependencyObject = listboxitem.GetValue(dependencyProperty);
                    if (dependencyObject == null)
                    {
                        listboxitem.SetValue(dependencyProperty, dragSourceAdvisor);
                    }
                }
                /* 위와 동일 기능 수행하는 다른 표현
                foreach (var item in toolbox.Items)
                {
                    ListBoxItem listboxitem = (ListBoxItem)(toolbox.ItemContainerGenerator.ContainerFromItem(item));
                    listboxitem.SetValue(dependencyProperty, dragSourceAdvisor);
                } 
                 * */
            }
        }
	}
}