﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Collections.ObjectModel;

namespace ButtonStyleLoader
{
    /// <summary>
    /// Interaction logic for ButtonStyleSelectWindow.xaml
    /// </summary>
    public partial class ButtonStyleSelectWindow : Window
    {
        public ObservableCollection<ButtonStyle> ButtonStyleList
        {
            get;
            set;
        }

        public ButtonStyle SelectedButtonStyle;

        public ButtonStyleSelectWindow()
        {
            InitializeComponent();
            ButtonStyleList = new ObservableCollection<ButtonStyle>();
            this.DataContext = this;
        }

        private void ButtonStyleFileLoadButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void ButtonSTyleRegisterButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }

        private void ButtonStyleListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SelectedButtonStyle = ButtonStyleListBox.SelectedItem as ButtonStyle;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (ButtonStyleList != null && ButtonStyleList.Count >= 1)
                ButtonStyleListBox.SelectedItem = ButtonStyleList[0];
        }        
    }
}
