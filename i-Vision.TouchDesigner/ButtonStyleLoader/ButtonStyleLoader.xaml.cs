﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Windows.Markup;
using System.ComponentModel;
using System.Collections.ObjectModel;
using Microsoft.Win32;
using UtilLib.StaticMethod;
using System.Globalization;

namespace ButtonStyleLoader
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class ButtonStyleLoaderWindow : Window, INotifyPropertyChanged
    {
         #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        public virtual void OnPropertyChanged(params string[] propertyNames)
        {
            if (PropertyChanged == null)
                return;

            foreach (string propertyName in propertyNames)
            {
                try
                {
                    PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
                }
                catch { }
            }
        }

        #endregion

        private string uerButtonStyleFileName = "UserButtonStyle.xaml";
        private static string buttonStyleFolderName = "ButtonStyles";
        private List<FileInfo> ButtoneStyleFileList;
        private string userButtonStyleFilepath;

        public ObservableCollection<ButtonStyle> ButtonStyleList
        {
            get;
            set;
        }

        private ButtonStyle selectedButtonStyle;
        public ButtonStyle SelectedButtonStyle 
        {
            get { return selectedButtonStyle; }
            set { this.selectedButtonStyle = value; OnPropertyChanged("SelectedButtonStyle"); }
        }

        public ButtonStyleLoaderWindow()
        {
            InitializeComponent();           

            ButtonStyleList = new ObservableCollection<ButtonStyle>();
            this.DataContext = this;

            string buttonStyleFolderPath = AppDomain.CurrentDomain.BaseDirectory + buttonStyleFolderName;// System.Environment.CurrentDirectory + "\\" + buttonStyleFolderName; -> 설치후 단축아이콘의 경로를 얻어온다.
            if (!Directory.Exists(buttonStyleFolderPath))
                Directory.CreateDirectory(buttonStyleFolderPath);

            string[] ButtonStyleNames = Directory.GetFiles(buttonStyleFolderPath);

            ButtoneStyleFileList = new List<FileInfo>();

            //ButtonStyle buttonStyle = new ButtonStyle("DefaultButtonStyle", ButtonStylePreview.Style, false);
            //ButtonStyleList.Add(buttonStyle);
            //selectedButtonStyle = buttonStyle;
            //ButtonStyleListBox.SelectedItem = buttonStyle;

            //MessageBox.Show(buttonStyleFolderPath + " - Count: " +ButtonStyleNames.Count().ToString());

            foreach (string item in ButtonStyleNames)
            {                
                FileInfo tempFileInfo = new FileInfo(item);
                string xaml = IOHelper.StyleLoad(tempFileInfo.FullName);
                if (string.IsNullOrEmpty(xaml) == false)
                {
                    ResourceDictionary resDict = (ResourceDictionary)XamlReader.Parse(xaml);//ResourceDictionary로 포장되어 있어야 한다.                    
                    bool isUserStyle = false;
                    if (tempFileInfo.Name.Equals("UserButtonStyle.xaml") == true)
                    {
                        userButtonStyleFilepath = tempFileInfo.FullName;
                        isUserStyle = true;                                        
                    }
                    else
                    {
                        isUserStyle = false;                                       
                    }

                    foreach (var key in resDict.Keys)
                    {
                        Style style = resDict[key] as Style;
                        if (style != null && style.TargetType == typeof(Button))
                        {
                            List<Setter> setters = style.Setters.Where(o => (o is Setter) == true).Select(o => o as Setter).ToList();
                            var template_setter = setters.Where(o => o.Property.ToString().Equals("Template") == true).FirstOrDefault();

                            ButtonStyle buttonStyle = new ButtonStyle(key as string, style, isUserStyle);

                            if (template_setter != null)
                            {
                                ControlTemplate controlTemplate = template_setter.Value as ControlTemplate;
                                DependencyObject depObj = controlTemplate.LoadContent();

                                var width_setter = setters.Where(o => o.Property.ToString().Equals("Width") == true).FirstOrDefault();
                                var height_setter = setters.Where(o => o.Property.ToString().Equals("Height") == true).FirstOrDefault();

                                if (width_setter != null && height_setter != null)
                                {
                                    double width = (double)width_setter.Value;
                                    double height = (double)height_setter.Value;

                                    FrameworkElement uielement = depObj as FrameworkElement;
                                    if (uielement != null && width != 0 && height != 0)
                                    {
                                        buttonStyle.Width = width;
                                        buttonStyle.Height = height;
                                        buttonStyle.ScaleX = 80 / width;
                                        buttonStyle.ScaleY = 24 / height;
                                    }
                                }
                            }
                            ButtonStyleList.Add(buttonStyle);
                        }
                    }       
                }
            }
        }

        private void ButtonStyleListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ListBox listbox = sender as ListBox;
            if (listbox != null)
            {
                var buttonstyle = listbox.SelectedItem as ButtonStyle;
                if (buttonstyle != null)
                {
                    buttonstyle.ScaleX = ButtonStylePreview.Width / buttonstyle.Width;
                    buttonstyle.ScaleY = ButtonStylePreview.Height / buttonstyle.Height;

                    ButtonStylePreview.DataContext = buttonstyle;
                }
            }
        }


        private void ButtonStyleFileLoadButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();

            openFileDialog.Filter = "Touch Project(.xaml)| *.xaml";
            openFileDialog.Title = "Button ResourceDictionary Open";
            openFileDialog.FileOk += (of_sender, of_e) =>
            {
                string filefullpath = openFileDialog.FileName;
                FileInfo fi = new FileInfo(filefullpath);
                string filename = fi.Name;
                if (System.IO.Path.GetExtension(filename).ToLower() == ".xaml")
                {
                    string xaml = IOHelper.StyleLoad(fi.FullName);
                    if (string.IsNullOrEmpty(xaml) == false)
                    {
                        ButtonStyleSelectWindow buttonStyleSelectWindow = new ButtonStyleSelectWindow();
                        ResourceDictionary resDict = (ResourceDictionary)XamlReader.Parse(xaml);
                        foreach (var key in resDict.Keys)
                        {
                            Style style = resDict[key] as Style;
                            ButtonStyle buttonStyle = new ButtonStyle(key as string, style, true);
                            buttonStyleSelectWindow.ButtonStyleList.Add(buttonStyle);
                        }

                        if (buttonStyleSelectWindow.ButtonStyleList.Count > 1)
                        {
                            buttonStyleSelectWindow.Owner = this;
                            buttonStyleSelectWindow.WindowStartupLocation = System.Windows.WindowStartupLocation.CenterOwner;
                            if (buttonStyleSelectWindow.ShowDialog() == true)
                            {
                                SelectedButtonStyle = buttonStyleSelectWindow.SelectedButtonStyle;
                            }
                        }
                        else
                        {
                            SelectedButtonStyle = buttonStyleSelectWindow.ButtonStyleList[0];
                        }

                        if (SelectedButtonStyle != null)
                        {
                            this.RegisterButtonStyleButton.Visibility = System.Windows.Visibility.Visible;                            
                            this.RegisterButtonStyleText.IsEnabled = true;                            
                        }
                    }
                }
            };

            if (openFileDialog.ShowDialog(this) != true)
            {               
            }
        }

        private void ButtonSTyleRegisterButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.SelectedButtonStyle != null)
            {
                var sameKeyStyle = ButtonStyleList.Where(o => o.StyleKeyName.Equals(this.SelectedButtonStyle.StyleKeyName) == true || o.StyleKeyName.ToLower().Equals(this.SelectedButtonStyle.StyleKeyName.ToLower()) == true).FirstOrDefault();
                if (sameKeyStyle != null)
                {
                    MessageBox.Show("동일 키를 가진 버튼 스타일이 존재합니다. 다른 이름의 키를 입력해주세요.");
                    return;
                }
                if (ButtonStyleList.Contains(this.SelectedButtonStyle) == false)
                {
                    ButtonStyleList.Add(this.SelectedButtonStyle);

                    ResourceDictionary resDict = new ResourceDictionary();
                    foreach (var style in ButtonStyleList)
                    {
                        if(style.IsUserStyle == true)
                            resDict.Add(style.StyleKeyName, style.StyleContent);
                    }
                    string resText = XamlWriter.Save(resDict);

                    using (StreamWriter sw = new StreamWriter(userButtonStyleFilepath))
                    {
                        sw.WriteLine(resText);
                    }
                }
            }
        }

        private void CancelButtonStyleButton_Click(object sender, RoutedEventArgs e)
        {            
            this.DialogResult = false;
        }

        private void SettinButtonStyleButton_Click(object sender, RoutedEventArgs e)
        {
            ButtonStyle buttonstyle = ButtonStyleListBox.SelectedItem as ButtonStyle;
            this.selectedButtonStyle = buttonstyle;

            this.DialogResult = true;
        }

        static public string FindNameFromResource(ResourceDictionary dictionary, object resourceItem)
        {
            foreach (object key in dictionary.Keys)
            {
                if (dictionary[key] == resourceItem)
                {
                    return key.ToString();
                }
            }

            return null;
        }

        public static Style GetDefaultButtonStyle()
        {
            string buttonStyleFolderPath = AppDomain.CurrentDomain.BaseDirectory + buttonStyleFolderName;// System.Environment.CurrentDirectory + "\\" + buttonStyleFolderName; -> 설치후 단축아이콘의 경로를 얻어온다.
            if (!Directory.Exists(buttonStyleFolderPath))
                Directory.CreateDirectory(buttonStyleFolderPath);//iVisionButtonStyle.xaml

            FileInfo tempFileInfo = new FileInfo(buttonStyleFolderPath + "\\iVisionButtonStyle.xaml");
            string xaml = IOHelper.StyleLoad(tempFileInfo.FullName);
            if (string.IsNullOrEmpty(xaml) == false)
            {
                ResourceDictionary resDict = (ResourceDictionary)XamlReader.Parse(xaml);//ResourceDictionary로 포장되어 있어야 한다.                    

                if (tempFileInfo.Name.Equals("iVisionButtonStyle.xaml") == true)
                {
                    foreach (var key in resDict.Keys)
                    {
                        Style style = resDict[key] as Style;
                        if (style != null && style.TargetType == typeof(Button) && key.Equals("DefaultButtonStyle") == true)
                        {
                            return style;
                        }
                    }
                }
            }

            return null;
        }

        public static Style GetButtonStyle(string buttonStyleKey)
        {
            string buttonStyleFolderPath = AppDomain.CurrentDomain.BaseDirectory + buttonStyleFolderName;
            if (!Directory.Exists(buttonStyleFolderPath))
                Directory.CreateDirectory(buttonStyleFolderPath);//iVisionButtonStyle.xaml

            FileInfo tempFileInfo = new FileInfo(buttonStyleFolderPath + "\\iVisionButtonStyle.xaml");
            string xaml = IOHelper.StyleLoad(tempFileInfo.FullName);
            if (string.IsNullOrEmpty(xaml) == false)
            {
                ResourceDictionary resDict = (ResourceDictionary)XamlReader.Parse(xaml);//ResourceDictionary로 포장되어 있어야 한다.                    

                if (tempFileInfo.Name.Equals("iVisionButtonStyle.xaml") == true)
                {
                    foreach (var key in resDict.Keys)
                    {
                        string keyName = (string)key;
                        if (string.IsNullOrEmpty(keyName) == true)
                            continue;

                        if (keyName.Equals(buttonStyleKey) == true)
                        {
                            return resDict[key] as Style;
                        }                        
                    }
                }
            }

            tempFileInfo = new FileInfo(buttonStyleFolderPath + "\\UserButtonStyle.xaml");
            xaml = IOHelper.StyleLoad(tempFileInfo.FullName);
            if (string.IsNullOrEmpty(xaml) == false)
            {
                ResourceDictionary resDict = (ResourceDictionary)XamlReader.Parse(xaml);//ResourceDictionary로 포장되어 있어야 한다.
                if (tempFileInfo.Name.Equals("UserButtonStyle.xaml") == true)
                {
                    foreach (var key in resDict.Keys)
                    {
                        string keyName = (string)key;
                        if (string.IsNullOrEmpty(keyName) == true)
                            continue;

                        if (keyName.Equals(buttonStyleKey) == true)
                        {
                            return resDict[key] as Style;
                        }
                    }
                }
            }

            return GetDefaultButtonStyle();
        }
    }

    public class ButtonStyle : INotifyPropertyChanged
    {
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        public virtual void OnPropertyChanged(params string[] propertyNames)
        {
            if (PropertyChanged == null)
                return;

            foreach (string propertyName in propertyNames)
            {
                try
                {
                    PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
                }
                catch { }
            }
        }

        #endregion

        public bool IsUserStyle { get; set; }
        public double ScaleX { get; set; }
        public double ScaleY { get; set; }

        public double Width { get; set; }
        public double Height { get; set; }

        private string styleKeyName;
        public string StyleKeyName
        {
            get { return this.styleKeyName; }
            set { this.styleKeyName = value; OnPropertyChanged("StyleKeyName"); }
        }

        private Style styleContent;
        public Style StyleContent
        {
            get { return this.styleContent; }
            set { this.styleContent = value; OnPropertyChanged("StyleContent"); }
        }

        public ButtonStyle()
        {
            ScaleX = 1.0;
            ScaleY = 1.0;
            this.Width = 80;
            this.Height = 24;
        }

        public ButtonStyle(string styleName, Style styleContent) : this()
        {
            this.styleKeyName = styleName;
            this.StyleContent = styleContent;             
        }

        public ButtonStyle(string styleName, Style styleContent, bool isUserStyle)
            : this(styleName, styleContent)
        {
            this.IsUserStyle = isUserStyle;
        }
    }

    public class StyleConverter : IValueConverter
    {
        #region IValueConverter 멤버

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }

        #endregion
    }
}
