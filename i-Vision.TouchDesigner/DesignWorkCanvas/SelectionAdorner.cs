﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Documents;
using System.Windows.Controls.Primitives;
using System.Windows.Media;
using System.Windows;
using System.Windows.Input;
using System.Windows.Controls;
using DesignWorkCanvas.Views;
using InSysTouchflowData;

namespace DesignWorkCanvas
{
    public class SelectionAdorner : Adorner
    {
        Thumb topLeft, topRight, bottomLeft, bottomRight, left, top, right, bottom;
        System.Windows.Shapes.Rectangle rect;
        VisualCollection visualChildren;
        private Panel _rootPanel;
        private UIElement _adornedElement;
        private bool _isResize = true;

        #region 생성자

        public SelectionAdorner(UIElement adornedElement, bool isResize)
            : base(adornedElement)
        {
            visualChildren = new VisualCollection(this);
            _adornedElement = adornedElement;
            this._isResize = isResize;

            // Call a helper method to initialize the Thumbs
            // with a customized cursors.
            BuildAdornerCorner(ref topLeft, Cursors.SizeNWSE);
            BuildAdornerCorner(ref topRight, Cursors.SizeNESW);
            BuildAdornerCorner(ref bottomLeft, Cursors.SizeNESW);
            BuildAdornerCorner(ref bottomRight, Cursors.SizeNWSE);

            BuildAdornerCorner(ref left, Cursors.SizeWE);
            BuildAdornerCorner(ref right, Cursors.SizeWE);
            BuildAdornerCorner(ref top, Cursors.SizeNS);
            BuildAdornerCorner(ref bottom, Cursors.SizeNS);
            
            if (rect == null)
            {
                rect = new System.Windows.Shapes.Rectangle();//Canvas.GetLeft(adornedElement), Canvas.GetTop(adornedElement), (adornedElement as FrameworkElement).Width, (adornedElement as FrameworkElement).Height);
               
                rect.Stroke = new SolidColorBrush(Color.FromArgb(0xFF, 0x79, 0x79, 0xCA));//#FF7979CA
                rect.StrokeThickness = 2;
                rect.Height = adornedElement.DesiredSize.Height;
                rect.Width = adornedElement.DesiredSize.Width;
                rect.Opacity = 0.0;                

                visualChildren.Add(rect);
            }

            if (this._isResize == false)
                return;

            // Add handlers for resizing.
            bottomLeft.DragDelta += new DragDeltaEventHandler(HandleBottomLeft);
            
            bottomRight.DragDelta += new DragDeltaEventHandler(HandleBottomRight);
            
            topLeft.DragDelta += new DragDeltaEventHandler(HandleTopLeft);
            
            topRight.DragDelta += new DragDeltaEventHandler(HandleTopRight);
            

            left.DragDelta += new DragDeltaEventHandler(HandleLeft);
            
            right.DragDelta += new DragDeltaEventHandler(HandleRight);
            
            top.DragDelta += new DragDeltaEventHandler(HandleTop);
            
            bottom.DragDelta += new DragDeltaEventHandler(HandleBottom);
        }

        public SelectionAdorner(Panel rootPanel, UIElement adornedElement, bool isResize)
            : this(adornedElement, isResize)
        {
            this._rootPanel = rootPanel;

            this.ClearLineOnCanvas(bottomLeft, this._rootPanel as Canvas);
            this.ClearLineOnCanvas(bottomRight, this._rootPanel as Canvas);
            this.ClearLineOnCanvas(topLeft, this._rootPanel as Canvas);
            this.ClearLineOnCanvas(topRight, this._rootPanel as Canvas);
            this.ClearLineOnCanvas(left, this._rootPanel as Canvas);
            this.ClearLineOnCanvas(right, this._rootPanel as Canvas);
            this.ClearLineOnCanvas(top, this._rootPanel as Canvas);
            this.ClearLineOnCanvas(bottom, this._rootPanel as Canvas);
        }
       
        /// <summary>
        /// Thumb 컨트롤에 대한 Resizing이 완료시 Canvas위의 동일 선상의 라인 표시 컨트롤을 제거한다.
        /// </summary>
        /// <param name="thumb"></param>
        /// <param name="canvas"></param>
        private void ClearLineOnCanvas(Thumb thumb, Canvas canvas)
        {
            thumb.DragCompleted += (s, e) => 
            { 
                canvas.ClearLine();

                PageCanvas zoomCanvas = this._rootPanel as PageCanvas;
                zoomCanvas.RaiseChangedContentControlEvent(_adornedElement);               
            };
        }
       
        #endregion

        // Handler for resizing from the bottom-right.
        void HandleBottomRight(object sender, DragDeltaEventArgs args)
        {
            FrameworkElement adornedElement = this.AdornedElement as FrameworkElement;
            Thumb hitThumb = sender as Thumb;

            if (adornedElement == null || hitThumb == null) return;
            FrameworkElement parentElement = adornedElement.Parent as FrameworkElement;

            // Ensure that the Width and Height are properly initialized after the resize.
            EnforceSize(adornedElement);

            double HorizontalChange = args.HorizontalChange;
            double VerticalChange = args.VerticalChange;

            if (Keyboard.Modifiers == ModifierKeys.Shift)
            {
                double value = Math.Max(args.HorizontalChange, args.VerticalChange);
                HorizontalChange = value;
                VerticalChange = value;
            }

            // Change the size by the amount the user drags the mouse, as long as it's larger 
            // than the width or height of an adorner, respectively.
            adornedElement.Width = Math.Max(adornedElement.Width + HorizontalChange, hitThumb.DesiredSize.Width);
            adornedElement.Height = Math.Max(VerticalChange + adornedElement.Height, hitThumb.DesiredSize.Height);

            ResizeRect(adornedElement.Width, adornedElement.Height, ResizeDirection.BottomRight);
        }

        // Handler for resizing from the bottom-left.
        void HandleBottomLeft(object sender, DragDeltaEventArgs args)
        {         
            FrameworkElement adornedElement = AdornedElement as FrameworkElement;
            Thumb hitThumb = sender as Thumb;

            if (adornedElement == null || hitThumb == null) return;

            // Ensure that the Width and Height are properly initialized after the resize.
            EnforceSize(adornedElement);

            double HorizontalChange = args.HorizontalChange;
            double VerticalChange = args.VerticalChange;
            if (Keyboard.Modifiers == ModifierKeys.Shift)
            {
                double value = Math.Max(args.HorizontalChange, args.VerticalChange);
                HorizontalChange = args.HorizontalChange;
                VerticalChange = -args.HorizontalChange;
            }

            // Change the size by the amount the user drags the mouse, as long as it's larger 
            // than the width or height of an adorner, respectively.
            //adornedElement.Width = Math.Max(adornedElement.Width - args.HorizontalChange, hitThumb.DesiredSize.Width);
            adornedElement.Height = Math.Max(VerticalChange + adornedElement.Height, hitThumb.DesiredSize.Height);

            double width_old = adornedElement.Width;
            double width_new = Math.Max(adornedElement.Width - HorizontalChange, hitThumb.DesiredSize.Width);
            double left_old = Canvas.GetLeft(adornedElement);
            adornedElement.Width = width_new;            
            Canvas.SetLeft(adornedElement, left_old - (width_new - width_old));

            ResizeRect(adornedElement.Width, adornedElement.Height, ResizeDirection.BottomLeft);
        }

        // Handler for resizing from the top-right.
        void HandleTopRight(object sender, DragDeltaEventArgs args)
        {
            FrameworkElement adornedElement = this.AdornedElement as FrameworkElement;
            Thumb hitThumb = sender as Thumb;

            if (adornedElement == null || hitThumb == null) return;
            FrameworkElement parentElement = adornedElement.Parent as FrameworkElement;

            // Ensure that the Width and Height are properly initialized after the resize.
            EnforceSize(adornedElement);

            double HorizontalChange = args.HorizontalChange;
            double VerticalChange = args.VerticalChange;
            if (Keyboard.Modifiers == ModifierKeys.Shift)
            {
                double value = Math.Max(args.HorizontalChange, args.VerticalChange);
                HorizontalChange = -args.VerticalChange;
                VerticalChange = args.VerticalChange;
            }

            // Change the size by the amount the user drags the mouse, as long as it's larger 
            // than the width or height of an adorner, respectively.
            adornedElement.Width = Math.Max(adornedElement.Width + HorizontalChange, hitThumb.DesiredSize.Width);
            //adornedElement.Height = Math.Max(adornedElement.Height - args.VerticalChange, hitThumb.DesiredSize.Height);

            double height_old = adornedElement.Height;
            double height_new = Math.Max(adornedElement.Height - VerticalChange, hitThumb.DesiredSize.Height);
            double top_old = Canvas.GetTop(adornedElement);
            adornedElement.Height = height_new;
            Canvas.SetTop(adornedElement, top_old - (height_new - height_old));

            ResizeRect(adornedElement.Width, adornedElement.Height, ResizeDirection.TopRight);
        }

        // Handler for resizing from the top-left.
        void HandleTopLeft(object sender, DragDeltaEventArgs args)
        {
            FrameworkElement adornedElement = AdornedElement as FrameworkElement;
            Thumb hitThumb = sender as Thumb;

            if (adornedElement == null || hitThumb == null) return;

            // Ensure that the Width and Height are properly initialized after the resize.
            EnforceSize(adornedElement);

            double HorizontalChange = args.HorizontalChange;
            double VerticalChange = args.VerticalChange;

            if (Keyboard.Modifiers == ModifierKeys.Shift)
            {
                double value = Math.Max(args.HorizontalChange, args.VerticalChange);
                HorizontalChange = value;
                VerticalChange = value;
            }

            // Change the size by the amount the user drags the mouse, as long as it's larger 
            // than the width or height of an adorner, respectively.
            //adornedElement.Width = Math.Max(adornedElement.Width - args.HorizontalChange, hitThumb.DesiredSize.Width);
            //adornedElement.Height = Math.Max(adornedElement.Height - args.VerticalChange, hitThumb.DesiredSize.Height);

            double width_old = adornedElement.Width;
            double width_new = Math.Max(adornedElement.Width - args.HorizontalChange, hitThumb.DesiredSize.Width);
            double left_old = Canvas.GetLeft(adornedElement);
            adornedElement.Width = width_new;
            Canvas.SetLeft(adornedElement, left_old - (width_new - width_old));

            double height_old = adornedElement.Height;
            double height_new = Math.Max(adornedElement.Height - args.VerticalChange, hitThumb.DesiredSize.Height);
            double top_old = Canvas.GetTop(adornedElement);
            adornedElement.Height = height_new;
            Canvas.SetTop(adornedElement, top_old - (height_new - height_old));

            ResizeRect(adornedElement.Width, adornedElement.Height, ResizeDirection.TopLeft);
        }

        // Handler for resizing from the left.
        void HandleLeft(object sender, DragDeltaEventArgs args)
        {
            FrameworkElement adornedElement = this.AdornedElement as FrameworkElement;
            Thumb hitThumb = sender as Thumb;

            if (adornedElement == null || hitThumb == null) return;
            FrameworkElement parentElement = adornedElement.Parent as FrameworkElement;

            // Ensure that the Width and Height are properly initialized after the resize.
            EnforceSize(adornedElement);

            double HorizontalChange = args.HorizontalChange;
            double VerticalChange = args.VerticalChange;

            if (Keyboard.Modifiers == ModifierKeys.Shift)
            {
                double value = Math.Max(args.HorizontalChange, args.VerticalChange);
                HorizontalChange = value;
                VerticalChange = value;
            }
           
            double width_old = adornedElement.Width;
            double width_new = Math.Max(adornedElement.Width - args.HorizontalChange, hitThumb.DesiredSize.Width);
            double left_old = Canvas.GetLeft(adornedElement);
            adornedElement.Width = width_new;
            Canvas.SetLeft(adornedElement, left_old - (width_new - width_old));

            ResizeRect(adornedElement.Width, adornedElement.Height, ResizeDirection.Left);
        }

        // Handler for resizing from the right.
        void HandleRight(object sender, DragDeltaEventArgs args)
        {
            FrameworkElement adornedElement = this.AdornedElement as FrameworkElement;
            Thumb hitThumb = sender as Thumb;

            if (adornedElement == null || hitThumb == null) return;
            FrameworkElement parentElement = adornedElement.Parent as FrameworkElement;

            // Ensure that the Width and Height are properly initialized after the resize.
            EnforceSize(adornedElement);

            double HorizontalChange = args.HorizontalChange;
            double VerticalChange = args.VerticalChange;
            if (Keyboard.Modifiers == ModifierKeys.Shift)
            {
                double value = Math.Max(args.HorizontalChange, args.VerticalChange);
                HorizontalChange = -args.VerticalChange;
                VerticalChange = args.VerticalChange;
            }
                       
            adornedElement.Width = Math.Max(adornedElement.Width + HorizontalChange, hitThumb.DesiredSize.Width);

            ResizeRect(adornedElement.Width, adornedElement.Height, ResizeDirection.Right);
        }

        // Handler for resizing from the top.
        void HandleTop(object sender, DragDeltaEventArgs args)
        {
            FrameworkElement adornedElement = this.AdornedElement as FrameworkElement;
            Thumb hitThumb = sender as Thumb;

            if (adornedElement == null || hitThumb == null) return;
            FrameworkElement parentElement = adornedElement.Parent as FrameworkElement;

            // Ensure that the Width and Height are properly initialized after the resize.
            EnforceSize(adornedElement);

            double HorizontalChange = args.HorizontalChange;
            double VerticalChange = args.VerticalChange;
            if (Keyboard.Modifiers == ModifierKeys.Shift)
            {
                double value = Math.Max(args.HorizontalChange, args.VerticalChange);
                HorizontalChange = -args.VerticalChange;
                VerticalChange = args.VerticalChange;
            }                      

            double height_old = adornedElement.Height;
            double height_new = Math.Max(adornedElement.Height - VerticalChange, hitThumb.DesiredSize.Height);
            double top_old = Canvas.GetTop(adornedElement);
            adornedElement.Height = height_new;
            Canvas.SetTop(adornedElement, top_old - (height_new - height_old));

            ResizeRect(adornedElement.Width, adornedElement.Height, ResizeDirection.Top);
        }

        // Handler for resizing from the bottom.
        void HandleBottom(object sender, DragDeltaEventArgs args)
        {
            FrameworkElement adornedElement = AdornedElement as FrameworkElement;
            Thumb hitThumb = sender as Thumb;

            if (adornedElement == null || hitThumb == null) return;

            // Ensure that the Width and Height are properly initialized after the resize.
            EnforceSize(adornedElement);

            double HorizontalChange = args.HorizontalChange;
            double VerticalChange = args.VerticalChange;
            if (Keyboard.Modifiers == ModifierKeys.Shift)
            {
                double value = Math.Max(args.HorizontalChange, args.VerticalChange);
                HorizontalChange = args.HorizontalChange;
                VerticalChange = -args.HorizontalChange;
            }

            adornedElement.Height = Math.Max(VerticalChange + adornedElement.Height, hitThumb.DesiredSize.Height);

            ResizeRect(adornedElement.Width, adornedElement.Height, ResizeDirection.Bottom);
        }

        // Arrange the Adorners.
        protected override Size ArrangeOverride(Size finalSize)
        {
            

            // desiredWidth and desiredHeight are the width and height of the element that's being adorned.  
            // These will be used to place the ResizingAdorner at the corners of the adorned element.  
            double desiredWidth = AdornedElement.DesiredSize.Width;
            double desiredHeight = AdornedElement.DesiredSize.Height;
            // adornerWidth & adornerHeight are used for placement as well.
            double adornerWidth = this.DesiredSize.Width;
            double adornerHeight = this.DesiredSize.Height;

            topLeft.Arrange(new Rect(-adornerWidth / 2, -adornerHeight / 2, adornerWidth, adornerHeight));
            topRight.Arrange(new Rect(desiredWidth - adornerWidth / 2, -adornerHeight / 2, adornerWidth, adornerHeight));
            bottomLeft.Arrange(new Rect(-adornerWidth / 2, desiredHeight - adornerHeight / 2, adornerWidth, adornerHeight));
            bottomRight.Arrange(new Rect(desiredWidth - adornerWidth / 2, desiredHeight - adornerHeight / 2, adornerWidth, adornerHeight));

            left.Arrange(new Rect(-adornerWidth / 2, -adornerHeight / adornerHeight, adornerWidth, adornerHeight));
            right.Arrange(new Rect(desiredWidth - adornerWidth / 2, -adornerHeight / adornerHeight, adornerWidth, adornerHeight));
            top.Arrange(new Rect(-adornerWidth / adornerWidth, -adornerHeight / 2, adornerWidth, adornerHeight));
            bottom.Arrange(new Rect(desiredWidth - adornerWidth, desiredHeight - adornerHeight / 2, adornerWidth, adornerHeight));            

            rect.Height = adornerWidth;
            rect.Width = adornerHeight;

            rect.Arrange(new Rect(0, 0, adornerWidth, adornerHeight));

            // Return the final size.
            return finalSize;
        }

        // Helper method to instantiate the corner Thumbs, set the Cursor property, 
        // set some appearance properties, and add the elements to the visual tree.
        void BuildAdornerCorner(ref Thumb cornerThumb, Cursor customizedCursor)
        {
            if (cornerThumb != null) return;

            cornerThumb = new Thumb();

            // Set some arbitrary visual characteristics.
            if (this._isResize == true)
                cornerThumb.Cursor = customizedCursor;
            cornerThumb.Height = cornerThumb.Width = 10;
            cornerThumb.Opacity = 0.40;
            cornerThumb.Background = new SolidColorBrush(Colors.Transparent);
            cornerThumb.BorderBrush = new SolidColorBrush(Colors.Black);
            cornerThumb.BorderThickness = new Thickness(1);
         
            visualChildren.Add(cornerThumb);
        }

        void ResizeRect(double width, double height, ResizeDirection resizeDirection)
        {
            rect.Height = height;
            rect.Width = width;

            //컨트롤 Resize시 거리 표시 라인 업데이트 시킴.
            this.AdornedElement.ResizeContentElementDistanceLine(this._rootPanel as Canvas, this._rootPanel.Tag as Dictionary<UIElement, List<DesignWorkCanvas.Views.LineDistanceView>>, resizeDirection);
        }
        
        // This method ensures that the Widths and Heights are initialized.  Sizing to content produces
        // Width and Height values of Double.NaN.  Because this Adorner explicitly resizes, the Width and Height
        // need to be set first.  It also sets the maximum size of the adorned element.
        void EnforceSize(FrameworkElement adornedElement)
        {
            if (adornedElement.Width.Equals(Double.NaN))
                adornedElement.Width = adornedElement.DesiredSize.Width;
            if (adornedElement.Height.Equals(Double.NaN))
                adornedElement.Height = adornedElement.DesiredSize.Height;

            FrameworkElement parent = adornedElement.Parent as FrameworkElement;
            if (parent != null)
            {
                //adornedElement.MaxHeight = parent.ActualHeight;
                //adornedElement.MaxWidth = parent.ActualWidth;
            }
        }
        // Override the VisualChildrenCount and GetVisualChild properties to interface with 
        // the adorner's visual collection.
        protected override int VisualChildrenCount { get { return visualChildren.Count; } }
        protected override Visual GetVisualChild(int index) { return visualChildren[index]; }
    }
}
