﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Input;
using InSysBasicControls.Interfaces;
using InSysTouchflowData.InSysProperties;
using InSysTouchflowData;
using InSysTouchflowData.Models.ProjectModels;
using InSysTouchflowData.Events;
using DesignWorkCanvas.Controllers;
using InSysBasicControls.CommonClass;
using InSysBasicControls;
using System;
using System.Linq;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Controls;
using System.Xml.Linq;

namespace DesignWorkCanvas.ViewModels
{
    public class WorkCanvasViewModel : ViewModelBase
    {
        public enum State
        {
            Copy,
            Cut,
            None
        }

        private Point CurrentMousePoint;
        private State CopyState = State.None;
        private List<UIElement> CutItems = new List<UIElement>();
        private WorkCanvasController workCanvasController = WorkCanvasController.Instance;
        private WorkCanvas View;
      
     
        public static RoutedCommand BringToFrontCommand = new RoutedCommand();
        public static RoutedCommand BringForwardCommand = new RoutedCommand();
        public static RoutedCommand SendBackwardCommand = new RoutedCommand();
        public static RoutedCommand SendToBackCommand = new RoutedCommand();
              
        #region Properties
        
        private bool _isApplyPageTimeline;
        public bool IsApplyPageTimeline
        {
            get { return _isApplyPageTimeline; }
            set
            {
                _isApplyPageTimeline = value;
                if (_currentWorkingPageInfo != null)
                    _currentWorkingPageInfo.ApplyLifeTime = _isApplyPageTimeline;
                      
            }
        }     

        private double _timelineDefaultLength = 100;
        public double TimelineDefaultLength
        {
            get { return _timelineDefaultLength; }
            set { _timelineDefaultLength = value; OnPropertyChanged("TimelineDefaultLength"); }
        }

        private TouchflowPageInfo _checkPageNamePageInfo;
        private TouchflowPageInfo _priviousPageInfo;

        private TouchflowPageInfo _currentWorkingPageInfo;
        public TouchflowPageInfo CurrentWorkingPageInfo
        {
            get { return _currentWorkingPageInfo; }
            set { _currentWorkingPageInfo = value; OnPropertyChanged("CurrentWorkingPageInfo"); }
        }

        #endregion

        public void WorkDesignView_Loaded(object sender, RoutedEventArgs e)
        {            
            View = sender as WorkCanvas;

            InitCommand();

            InitEvent_AfterViewLoaded();

            View.Focus();
        }

        public void ZoomCanvas_Loaded(object sender, RoutedEventArgs e)
        {
            ZoomCanvas zoomCanvas = sender as ZoomCanvas;
            if (zoomCanvas != null)
            {
                zoomCanvas.ResizePanel();
            }
        }

        public void TimelineControl_Loaded(object sender, RoutedEventArgs e)
        {
        }

        private void InitCommand()
        {
            View.CommandBindings.Add(new CommandBinding(WorkCanvasViewModel.BringToFrontCommand, BringToFrontCommand_Executed, BringToFrontCommand_CanExecute));
            View.CommandBindings.Add(new CommandBinding(WorkCanvasViewModel.BringForwardCommand, BringForwardCommand_Executed, BringForwardCommand_CanExecute));
            View.CommandBindings.Add(new CommandBinding(WorkCanvasViewModel.SendBackwardCommand, SendBackwardCommand_Executed, SendBackwardCommand_CanExecute));
            View.CommandBindings.Add(new CommandBinding(WorkCanvasViewModel.SendToBackCommand, SendToBackCommand_Executed, SendToBackCommand_CanExecute));

            View.CommandBindings.Add(new CommandBinding(ApplicationCommands.Cut, Cut_Executed, Cut_CanExecute));
            View.CommandBindings.Add(new CommandBinding(ApplicationCommands.Copy, Copy_Executed, Copy_CanExecute));
            View.CommandBindings.Add(new CommandBinding(ApplicationCommands.Paste, Paste_Executed, Paste_CanExecute));
            View.CommandBindings.Add(new CommandBinding(ApplicationCommands.Delete, Delete_Executed, Delete_CanExecute));
        }

        #region Order Command

        #region BringToFront Command

        private void BringToFrontCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            IEnumerable<UIElement> unselectedItems = this.View.artBoard.Children.OfType<UIElement>()
                .Where(o => InSysBasicControls.InSysControlManager.GetControlType(o.GetType().Name) != InSysControlType.None)
                .Where(o => this.View.SelectedContentItems.Contains(o) != true).OrderBy(o => (o as IDesignElement).ZIndex);
            List<UIElement> tempList = unselectedItems.ToList();

            int zIndex = 0;
            unselectedItems.ToList().ForEach(o => (o as IDesignElement).ZIndex = zIndex++);

            IEnumerable<UIElement> selectedItems = this.View.SelectedContentItems.OrderBy(o => (o as IDesignElement).ZIndex);
            selectedItems.ToList().ForEach(o => (o as IDesignElement).ZIndex = zIndex++);
        }

        private void BringToFrontCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            if (this.View.SelectedContentItems != null && this.View.SelectedContentItems.Count >= 1)
                e.CanExecute = true;
            else
                e.CanExecute = false;
        }
        #endregion//BringToFront Command

        #region BringForward Command

        private void BringForwardCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            //int maxZindex = this.View.SelectedContentItems.Select(o => Canvas.GetZIndex(o)).Max();
            //IEnumerable<UIElement> items = this.View.SelectedContentItems.Where(o => Canvas.GetZIndex(o) > maxZindex);
            //items.ToList().ForEach(o=>Canvas.SetZIndex(o, Canvas.GetZIndex(o) + 1));
            //this.View.SelectedContentItems.ForEach(o => Canvas.SetZIndex(o, Canvas.GetZIndex(o) + 1));

            IEnumerable<UIElement> frontItems = this.View.artBoard.Children.OfType<UIElement>().Where(o => InSysBasicControls.InSysControlManager.GetControlType(o.GetType().Name) != InSysControlType.None);

            //foreach (UIElement item in frontItems)
            //{
            //    int currentZIndex = Canvas.GetZIndex(item);
            //}

            foreach (UIElement item in this.View.SelectedContentItems)
            {
                int currentZIndex = (item as IDesignElement).ZIndex;

                var frontItem = frontItems.Select(o => o as IDesignElement).Where(o => o.ZIndex == currentZIndex + 1).FirstOrDefault();
                if (frontItem != null)
                {
                    (frontItem as IDesignElement).ZIndex = currentZIndex;
                    (item as IDesignElement).ZIndex = currentZIndex + 1;
                }
            }
        }

        private void BringForwardCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            if (this.View.SelectedContentItems != null && this.View.SelectedContentItems.Count >= 1)
                e.CanExecute = true;
            else
                e.CanExecute = false;
        }
        #endregion//BringForward Command

        #region SendBackward Command

        private void SendBackwardCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            IEnumerable<UIElement> frontItems = this.View.artBoard.Children.OfType<UIElement>().Where(o => InSysBasicControls.InSysControlManager.GetControlType(o.GetType().Name) != InSysControlType.None);
            foreach (UIElement item in this.View.SelectedContentItems)
            {
                int currentZIndex = (item as IDesignElement).ZIndex;

                var frontItem = frontItems.Select(o => o as IDesignElement).Where(o => o.ZIndex == currentZIndex - 1).FirstOrDefault();
                if (frontItem != null)
                {
                    (frontItem as IDesignElement).ZIndex = currentZIndex;
                    (item as IDesignElement).ZIndex = currentZIndex - 1;
                }
            }
        }

        private void SendBackwardCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            if (this.View.SelectedContentItems != null && this.View.SelectedContentItems.Count >= 1)
                e.CanExecute = true;
            else
                e.CanExecute = false;
        }
        #endregion//SendBackward Command

        #region SendToBack Command

        private void SendToBackCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            IEnumerable<UIElement> unselectedItems = this.View.artBoard.Children.OfType<UIElement>()
                .Where(o => InSysBasicControls.InSysControlManager.GetControlType(o.GetType().Name) != InSysControlType.None)
                .Where(o => this.View.SelectedContentItems.Contains(o) != true).OrderBy(o => (o as IDesignElement).ZIndex);
            List<UIElement> tempList = unselectedItems.ToList();

            int zIndex = 0;           

            IEnumerable<UIElement> selectedItems = this.View.SelectedContentItems.OrderBy(o => (o as IDesignElement).ZIndex);
            selectedItems.ToList().ForEach(o => (o as IDesignElement).ZIndex = zIndex++);

            unselectedItems.ToList().ForEach(o => (o as IDesignElement).ZIndex = zIndex++);
        }

        private void SendToBackCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            if (this.View.SelectedContentItems != null && this.View.SelectedContentItems.Count >= 1)
                e.CanExecute = true;
            else
                e.CanExecute = false;
        }
        #endregion//SendToBack Command

        #endregion

        #region Control Cut, Copy, Paste, Delete Command

        #region Cut Command

        private void Cut_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            this.CutItems.Clear();

            XElement rootContentItemsElement = new XElement("InSysControls");
            foreach (FrameworkElement contentControl in this.View.SelectedContentItems)
            {
                TouchflowContentInfo contentInfo = this._currentWorkingPageInfo.Contents.Where(o => o.ContentObject.Equals(contentControl) == true).FirstOrDefault();
                if (contentInfo != null)
                {
                    TouchflowContentInfo copyContentInfo = this.Copy_ContentInfo(contentInfo, this._currentWorkingPageInfo.PageLifeTime, true);
                    XElement contentItemElement = new XElement("InSysControlContentInfo", Serialize(copyContentInfo));
                    rootContentItemsElement.Add(contentItemElement);
                }
                this.CutItems.Add(contentControl as UIElement);

            }
            Clipboard.Clear();
            Clipboard.SetData(DataFormats.Xaml, rootContentItemsElement);

            this.CopyState = State.Cut;
        }

        private void Cut_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            if (this.View.SelectedContentItems != null && this.View.SelectedContentItems.Count >= 1)
                e.CanExecute = true;
            else
                e.CanExecute = false;
        }

        #endregion

        #region Copy Command

        private void Copy_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            XElement rootContentItemsElement = new XElement("InSysControls");
            foreach (FrameworkElement contentControl in this.View.SelectedContentItems)
            {
                TouchflowContentInfo contentInfo = this._currentWorkingPageInfo.Contents.Where(o => o.ContentObject.Equals(contentControl) == true).FirstOrDefault();
                if (contentInfo != null)
                {
                    TouchflowContentInfo copyContentInfo = this.Copy_ContentInfo(contentInfo, this._currentWorkingPageInfo.PageLifeTime, true);
                    XElement contentItemElement = new XElement("InSysControlContentInfo", Serialize(copyContentInfo));
                    rootContentItemsElement.Add(contentItemElement);                    
                }
            }
            Clipboard.Clear();
            Clipboard.SetData(DataFormats.Xaml, rootContentItemsElement);

            this.CopyState = State.Copy;            
        }

        private void Copy_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {           
            if (this.View.SelectedContentItems != null && this.View.SelectedContentItems.Count >= 1)
                e.CanExecute = true;
            else
                e.CanExecute = false;
        }

        #endregion

        #region Paste Command

        private void Paste_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            XElement rootXml = LoadSerializedDataFromClipBoard();
            IEnumerable<XElement> itemsXML = rootXml.Elements("InSysControlContentInfo");
            int pasteCount = itemsXML.Count();
            
            foreach (XElement itemElement in itemsXML)
            {
                Object content = DeSerialize(itemElement.Value);
                TouchflowContentInfo copyContentInfo = content as TouchflowContentInfo;
                FrameworkElement copyInSysContentControl = InSysControlManager.GetNewSerializationInSysControl(copyContentInfo.ContentType, copyContentInfo.DesignElementProperties, InSysBasicControls.CommonClass.ViewState.Design) as FrameworkElement;

                #region 추가 ContentControl 정보 Update

                this.AddNewControlContent(copyInSysContentControl);
                
                if (InSysControlManager.GetControlType(copyContentInfo.ContentType.Name) == InSysControlType.InSysAudioBox)
                {
                    #region Audio Paste

                    this.View.DesignCanvasBar.Children.Add(copyInSysContentControl);
                    //this.workCanvasController.Init_SetInSysControl(this._currentWorkingPageInfo, copyInSysContentControl, this.workCanvasController.GetProjectInfo().PageInfos);

                    copyInSysContentControl.UpdateLayout();

                    IDesignElement designeElement = copyInSysContentControl as IDesignElement;
                    if (designeElement != null)
                    {
                        string name = InSysControlManager.CheckInSysControlName(InSysControlManager.GetControlType(copyContentInfo.ContentType.Name), this.View.DesignCanvasBar.Children);
                        designeElement.Name = name;
                    }
                    #endregion //Audio Paste
                }
                else
                {
                    #region Audio를 제외한 컨트롤 Paste

                    this.View.artBoard.Children.Add(copyInSysContentControl);
                    //this.workCanvasController.Init_SetInSysControl(this._currentWorkingPageInfo, copyInSysContentControl, this.workCanvasController.GetProjectInfo().PageInfos);

                    copyInSysContentControl.UpdateLayout();
                    #region Paste할 Position 지정하기
                    /* 복사 대상이 1개일 경우 마우스 포인트 위치에 붙여놓기
                    double left = this.CurrentMousePoint.X;// copyContentInfo.ContentPosition.X + 10;
                    double top = this.CurrentMousePoint.Y; // copyContentInfo.ContentPosition.Y + 10;

                    if (pasteCount > 1)//Paste 대상이 여러개 일경우에는 동일 위치에 복사
                    {
                        left = Canvas.GetLeft(copyInSysContentControl);
                        top = Canvas.GetTop(copyInSysContentControl);
                    }
                     
                    Canvas.SetLeft(copyInSysContentControl, left);
                    Canvas.SetTop(copyInSysContentControl, top);
                     **/

                    double left = Canvas.GetLeft(copyInSysContentControl);
                    double top = Canvas.GetTop(copyInSysContentControl);                   

                    Canvas.SetLeft(copyInSysContentControl, left);
                    Canvas.SetTop(copyInSysContentControl, top);

                    #endregion

                    IDesignElement designeElement = copyInSysContentControl as IDesignElement;
                    if (designeElement != null)
                    {
                        string name = InSysControlManager.CheckInSysControlName(InSysControlManager.GetControlType(copyContentInfo.ContentType.Name), this.View.artBoard.Children);
                        designeElement.Name = name;
                    }
                    #endregion //Audio를 제외한 컨트롤 Paste
                }

                #endregion//ContentInfo Object를 PageInfo Object 정보에 Update
            }

            if (this.CopyState == State.Cut)
            {
                foreach (UIElement delItem in CutItems)
                {
                    this.DeleteControlItem(delItem);
                }
                this.CutItems.Clear();
            }
            //this.CopyState = State.None;//JunYoung : Paste가 계속 진행을 위해
        }

        private void Paste_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            if (this.CopyState != State.None)
                e.CanExecute = true;
            else
                e.CanExecute = false;
        }

        #endregion

        #region Delete Command

        private void Delete_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            this.Delete_SelectedControlItems();
        }
     
        private void Delete_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            if (this.View.SelectedContentItems != null && this.View.SelectedContentItems.Count >= 1)
                e.CanExecute = true;
            else
                e.CanExecute = false;
        }

        #endregion

        #endregion //Control Cut, Copy, Paste, Delete Command

        private string Serialize(object objectToSerialize)
        {
            
            string serialString = null;
            using (System.IO.MemoryStream ms1 = new System.IO.MemoryStream())
            {
                try
                {
                    BinaryFormatter b = new BinaryFormatter();
                    b.Serialize(ms1, objectToSerialize);
                    byte[] arrayByte = ms1.ToArray();
                    serialString = Convert.ToBase64String(arrayByte);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            return serialString;
        }

        private object DeSerialize(string serializationString)
        {
            object deserialObject = null;
            byte[] arrayByte = Convert.FromBase64String(serializationString);
            using (System.IO.MemoryStream ms1 = new System.IO.MemoryStream(arrayByte))
            {
                BinaryFormatter b = new BinaryFormatter();
                deserialObject = b.Deserialize(ms1);
            }
            return deserialObject;
        }

        private XElement LoadSerializedDataFromClipBoard()
        {
            if (Clipboard.ContainsData(DataFormats.Xaml))
            {
                String clipboardData = Clipboard.GetData(DataFormats.Xaml) as String;

                if (String.IsNullOrEmpty(clipboardData))
                    return null;
                try
                {
                    return XElement.Load(new StringReader(clipboardData));
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.StackTrace, e.Message, MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }

            return null;
        }

        private object Changed_PageLifeTime(object sender, object obj)
        {
            return obj;
        }

        private object Changed_AutoPage(object sender, object obj)
        {
            this.IsApplyPageTimeline = (bool)obj;
            
            
            this.EnablePageLifeTime(this._currentWorkingPageInfo);

            return obj;
        }

        private object Changed_PageName(object sender, object obj)
        {
            DependencyPropertyChangedEventArgs eventArgs = (DependencyPropertyChangedEventArgs)obj;
            
            if (eventArgs != null)
            {
                if (this._currentWorkingPageInfo != null)
                {
                    string oldPageName = (string)eventArgs.OldValue;
                    string newPageName = (string)eventArgs.NewValue;
                    if(string.IsNullOrEmpty(oldPageName) == true)//이전 Page Name이 없을 경우에 대해서는 처리 하지 않는다.(page 삽입일 경우)
                        return obj;
                    if (this._checkPageNamePageInfo != null)
                    {
                        if (this._currentWorkingPageInfo.PageGuid.Equals(this._checkPageNamePageInfo.PageGuid) == true)
                        {
                            if (oldPageName.ToLower().Equals(newPageName.ToLower()) == true)
                                return obj;

                            if (this.workCanvasController.ChangedPageName(this._currentWorkingPageInfo.PageGuid, newPageName) == false)
                            {
                                this._currentWorkingPageInfo.PageName = oldPageName;
                            }
                        }
                        else
                        {
                            if (this.workCanvasController.ChangedPageName(this._currentWorkingPageInfo.PageGuid, newPageName) == false)
                            {
                                this._currentWorkingPageInfo.PageName = oldPageName;
                            }
                        }
                    }
                }

                this._checkPageNamePageInfo = this._currentWorkingPageInfo;//이전 Page 정보 저장
                return obj;
            }
            return null;
        }

        private object Changed_StartTimeProperty(object sender, object obj)
        {
            TimeSpan startTime = (TimeSpan)obj;

            FrameworkElement frameworkElement = this.View.SelectedCurrentContentItem as FrameworkElement;
            if (frameworkElement != null)
            {
                
                IDesignElement designElement = frameworkElement as IDesignElement;
                if (designElement != null)
                {
                  
                    if (designElement.IsInfinityTime == true)//무하대
                    {
                        if (designElement.ControlTimeDuration != 0.0)
                        {
                            designElement.EndTime = TimeSpan.FromSeconds(designElement.ControlTimeDuration);                           
                        }
                    }
                }               
            }

            return obj;
        }

        private object Changed_EndTimeProperty(object sender, object obj)
        {            
            return obj;
        }

        private void ChangedInfinityTimeProperty(bool isInfinity)
        {
            
        }

        #region Control Timeline 설정 플래그 변경시 호출
        private object Changed_IsApplyControlLifeTimeProperty(object sender, object obj)
        {
            if (this.View.SelectedCurrentContentItem == null)
                return obj;

            if (this.View.SelectedCurrentContentItem is IElementProperties)
            {
                IElementProperties property = this.View.SelectedCurrentContentItem as IElementProperties;
                if (property != null && property.ElementProperties != null)
                {
                    if (property.ElementProperties.GetType().Name.Equals(sender.GetType().Name) == false)
                        return obj;

                    #region 컨트롤 업데이트시 Property가 업데이트되면서 값을 변경함으로서 발생하는 오류를 방지
                    InSysBasicControls.InSysProperties.InSysProperty insysProperty = sender as InSysBasicControls.InSysProperties.InSysProperty;
                    if (insysProperty != null)
                    {
                        if (insysProperty.Name.ToLower().Equals(property.ElementProperties.Name.ToLower()) == false)
                            return obj;
                    }
                    #endregion
                }
                else
                {
                    return obj;
                }
            }
           
            IDesignElement designElement = this.View.SelectedCurrentContentItem as IDesignElement;
            this.SetControlTimeline((bool)obj);
            
            designElement.IsApplyLifeTime = (bool)obj;            
            if (designElement != null)
            {
                if (designElement.IsApplyLifeTime == true)
                {
                    if (designElement.EndTime != null )
                    {
                        if (designElement.ControlTimeDuration == 0.0)
                            designElement.ControlTimeDuration = this._currentWorkingPageInfo.PageLifeTime;
                        if (designElement.EndTime == TimeSpan.Zero)
                            designElement.EndTime = TimeSpan.FromSeconds(this._currentWorkingPageInfo.PageLifeTime);
                    }                    

                    
                    InSysBasicControls.InSysControlManager.Set_ControlLifeTimeEditProperty(designElement, this._isApplyPageTimeline);
                    
                }
                else
                {
                    designElement.LifeTimeVisibility = designElement.Visibility;
                    InSysBasicControls.InSysControlManager.Set_InitTimeControl("EndTime");
                    designElement.StartTime = TimeSpan.FromSeconds(0.0);
                    designElement.EndTime = TimeSpan.FromSeconds(0.0);
                    InSysBasicControls.InSysControlManager.Set_ControlLifeTimeEditProperty(designElement, this._isApplyPageTimeline);
                }
            }           

            return obj;
        }
        #endregion

        #region Timeline Control 변경시 호출#
        private void timelineControl_ChangedLifeTimeValue(object sender, RoutedEventArgs e)
        {
            if (this.View.SelectedCurrentContentItem != null)
            {
                IDesignElement designeElement = this.View.SelectedCurrentContentItem as IDesignElement;
                if (designeElement != null)
                {
                   
                }
            }
        }
        #endregion

        private void ChangeControlTimeDuration(FrameworkElement changedControl, double changedValue)
        {
            if (this.View.SelectedCurrentContentItem != null && this.View.SelectedCurrentContentItem.Equals(changedControl) == true)
            {
                IDesignElement designElement2 = this.View.SelectedCurrentContentItem as IDesignElement;
                if (designElement2 != null)
                {
                    designElement2.ControlTimeDuration = changedValue;
                }
            }            
        }

        private void ControlChangeProperty(FrameworkElement frameworkElement, string propertyName, object value)
        {
            switch (propertyName)
            {
                case "Visibility":
                    Visibility visibility = (Visibility)value;
                    if (frameworkElement != null)
                    {                        
                        IDesignElement designElement = frameworkElement as IDesignElement;
                        if (designElement != null)
                        {
                            //designElement.Visibility = visibility;
                            designElement.LifeTimeVisibility = visibility;
                        }
                    }
                    break;
            }
        }

        public WorkCanvasViewModel()
        {
            _checkPageNamePageInfo = null;
            InSysScreenProperty.PageLifeTimeCallback = new PropertyChangedDelegate(Changed_PageLifeTime);
            InSysScreenProperty.AutoPageCallback = new PropertyChangedDelegate(Changed_AutoPage);
            InSysScreenProperty.PageNameCallback = new PropertyChangedDelegate(Changed_PageName);
            
            InitEvent_BeforeViewLoaded();
        }

        private void InitEvent_BeforeViewLoaded()
        {
            this.workCanvasController.SelectedPageHandler = new WorkCanvasController.SelectedPageDelegate(OnSelectedPage);
            this.workCanvasController.ChangedPageHandler = new WorkCanvasController.ChangedPageDelegate(OnChangedPage);
            this.workCanvasController.SelectControlHandler = new WorkCanvasController.SendContentSelectionDelegate(OnSelectControl);
            this.workCanvasController.CurrentPageInfoHandler = new WorkCanvasController.CurrentPageInfoDelegate(OnGetCurrentPageInfo);
            this.workCanvasController.OpenPageHandler = new WorkCanvasController.OpenProgramFileDelegate(OnOpenPage);
            this.workCanvasController.OpenProjectHandler = new WorkCanvasController.OpenProgramFileDelegate(OnOpenProject);
            this.workCanvasController.SavePageHandler = new WorkCanvasController.SavePageDelegate(OnSavePage);
            this.workCanvasController.SaveProjectHandler = new WorkCanvasController.SaveProjectDelegate(OnSaveProject);
            this.workCanvasController.CreateNewPageHandler = new WorkCanvasController.CreateNewPageDelegate(OnCreateNewPage);
            this.workCanvasController.CopyPageHandler = new WorkCanvasController.CopyPageDelegate(OnCopyPage);
            this.workCanvasController.ReleaseAdornerHandler = new WorkCanvasController.ReleaseAdornerDelegate(OnReleaseAllAdorner);
            this.workCanvasController.InitWorkCanvasHandler = new WorkCanvasController.InitWorkCanvasDelegate(InitWorkCanvasForNewProject);
            this.workCanvasController.SendContentSelectionHandler = new WorkCanvasController.SendContentSelectionDelegate(RaiseSendContentSelectionEvent);

            InSysBasicControls.InSysProperties.InSysProperty.ChangedIsApplyLifeTimePropertyCallback = new PropertyChangedDelegate(Changed_IsApplyControlLifeTimeProperty);
            InSysBasicControls.InSysProperties.InSysProperty.ChangedStartTimePropertyCallback = new PropertyChangedDelegate(Changed_StartTimeProperty);
            InSysBasicControls.InSysProperties.InSysProperty.ChangedEndTimePropertyCallback = new PropertyChangedDelegate(Changed_EndTimeProperty);
            InSysBasicControls.InSysControlManager.ChangedInfinityTimePropertyHandler = new ChangedInfinityTimePropertyDelegate(ChangedInfinityTimeProperty);
        }

        private void InitWorkCanvasForNewProject(TouchflowProjectInfo projectInfo)
        {            
            projectInfo.InitProjectInfo(projectInfo.PageSize, projectInfo.DefaultPageLifeTime);

            this.View.ZoomCanvas.Width = projectInfo.PageSize.Width;
            this.View.ZoomCanvas.Height = projectInfo.PageSize.Height;

            this.View.ZoomCanvas.Children.Clear();
            this.View.ZoomCanvas.UpdateLayout();
            this.View.ZoomCanvas.ResizePanel();
            
        }

        private void InitEvent_AfterViewLoaded()
        {
            this.workCanvasController.DoAlignmentHandler = new WorkCanvasController.DoAlignmentDelegate(this.View.OnDoAlignment);
            this.workCanvasController.DoZoomHandler = new WorkCanvasController.DoZoomDelegate(this.View.OnDoZoom);

            this.View.SelectedInSysControlItemHandler = new SelectedInSysControlItemDelegate(SelectedInSysControlItem);
            
        }

        private TouchflowPageInfo CreatePageInfo(double width, double height, double pageLiftTime, string pagename)
        {
            TouchflowPageInfo pageInfo = new TouchflowPageInfo(width, height, pageLiftTime, pagename);
            
            return pageInfo;
        }

        private TouchflowPageInfo CopyPageInfo(TouchflowPageInfo sourceCopyPageInfo)
        {
            TouchflowPageInfo newCopyPageInfo = new TouchflowPageInfo();
            newCopyPageInfo.PageLifeTime = sourceCopyPageInfo.PageLifeTime;
            newCopyPageInfo.ViewHeight = sourceCopyPageInfo.ViewHeight;
            newCopyPageInfo.ViewWidth = sourceCopyPageInfo.ViewWidth;            
          
            foreach (TouchflowContentInfo sourceContentInfo in sourceCopyPageInfo.Contents)
            {
                 TouchflowContentInfo copyContentInfo = this.Copy_ContentInfo(sourceContentInfo, sourceCopyPageInfo.PageLifeTime, false);

                 newCopyPageInfo.Contents.Add(copyContentInfo);
            }
           

            return newCopyPageInfo;
        }

        public TouchflowContentInfo Copy_ContentInfo(TouchflowContentInfo sourceContentInfo, double pageLifeTime, bool isSerialization)
        {
            TouchflowContentInfo newContentInfo = new TouchflowContentInfo();
            FrameworkElement inSysContentControl = InSysControlManager.GetNewInSysControl(sourceContentInfo.ContentType, sourceContentInfo.DesignElementProperties, InSysBasicControls.CommonClass.ViewState.Design) as FrameworkElement;
            if (inSysContentControl != null)
            {
                inSysContentControl.UpdateLayout();

                IDesignElement designElement = inSysContentControl as IDesignElement;

                if (designElement != null)
                {
                    #region  Timeline 정보 설정.

                    double startTiem = designElement.StartTime.TotalSeconds;
                    double endTiem = designElement.EndTime.TotalSeconds;
                    if (designElement == null)
                    {
                        startTiem = 0;
                        endTiem = pageLifeTime;
                    }
                    
                    #endregion //Timeline 정보 설정.

                    #region ContentInfo 정보 Copy

                    newContentInfo.ContentType = sourceContentInfo.ContentType;
                    newContentInfo.ContentPosition = sourceContentInfo.ContentPosition;
                    newContentInfo.ContentSize = sourceContentInfo.ContentSize;
                    newContentInfo.ContentObject = inSysContentControl;
                    if (isSerialization == true)
                        newContentInfo.DesignElementProperties = InSysBasicControls.InSysControlManager.SerializePropertiesInIDesignElement(designElement.Properties);
                    else
                        newContentInfo.DesignElementProperties = designElement.Properties;

                    newContentInfo.ContentName = sourceContentInfo.ContentName;

                    #endregion
                }

                //this.workCanvasController.Init_SetInSysControl(this._currentWorkingPageInfo, inSysContentControl, this.workCanvasController.GetProjectInfo().PageInfos);
            }

            return newContentInfo;
        }

        public TouchflowPageInfo OnCreateNewPage(double width, double height, double pageLiftTime, string pagename)
        {
            this.CurrentWorkingPageInfo = CreatePageInfo(width, height, pageLiftTime, pagename);
            
            this.View.ZoomCanvas.Width = width;
            this.View.ZoomCanvas.Height = height;
            this.View.ZoomCanvas.UpdateLayout();
            this.View.ZoomCanvas.ResizePanel();
          
            return this._currentWorkingPageInfo;
        }

        public TouchflowPageInfo OnCopyPage(TouchflowPageInfo copyPageInfo)
        {  
            this._currentWorkingPageInfo = CopyPageInfo(copyPageInfo);
            return this._currentWorkingPageInfo;
        }

        private TouchflowPageInfo OnGetCurrentPageInfo()
        {
            this.View.UpdateLayout();
            return this._currentWorkingPageInfo;
        }

        private void OnChangedPage(TouchflowPageInfo pageinfo)
        {
            this._currentWorkingPageInfo = pageinfo;
        }

        private void OnSelectedPage(TouchflowPageInfo pageinfo)
        {
            ChangePage(pageinfo);
        }

        private void OnSelectControl(object selectObj)
        {
            SelectControl(selectObj);
        }

        /// <summary>
        /// Screen 바이너리 데이타 저장하기
        /// </summary>
        /// <param name="path"></param>
        public void OnSavePage(TouchflowPageInfo pageInfo, string path)
        {
            FileStream flStream = new FileStream(path, FileMode.OpenOrCreate, FileAccess.Write);
            try
            {
                //PageInfo pageInfo = this.View.artBoard.DataContext as PageInfo;
                pageInfo.Contents.Clear();//이전에 저장된 Page 정보 삭제.
                foreach (FrameworkElement item in this.View.artBoard.Children)
                {
                    if (InSysBasicControls.InSysControlManager.GetControlType(item.GetType().Name) == InSysBasicControls.CommonClass.InSysControlType.None)
                        continue;
                    
                    TouchflowContentInfo contentInfo = new TouchflowContentInfo();
                    
                    contentInfo.ContentType = item.GetType();
                    contentInfo.ContentPosition = new Point(Canvas.GetLeft(item), Canvas.GetTop(item));
                    contentInfo.ContentSize = new Size(item.Width, item.Height);
                    contentInfo.ContentObject = item;                    
                    contentInfo.DesignElementProperties = InSysBasicControls.InSysControlManager.SerializePropertiesInIDesignElement((item as IDesignElement).Properties);
                    contentInfo.ContentName = (item as IDesignElement).Name;

                    pageInfo.Contents.Add(contentInfo);
                }
                BinaryFormatter binFormatter = new BinaryFormatter();
                binFormatter.Serialize(flStream, pageInfo);
            }
            finally
            {
                flStream.Close();
            }
        }

        /// <summary>
        /// Screen 바이너리 데이타 읽어오기
        /// </summary>
        /// <param name="path"></param>
        public object OnOpenPage(string path)
        {
            FileStream flStream = new FileStream(path, FileMode.Open, FileAccess.Read);
            try
            {
                BinaryFormatter binFormatter = new BinaryFormatter();
                TouchflowPageInfo pageInfo = (TouchflowPageInfo)binFormatter.Deserialize(flStream);
              
                pageInfo.PageGuid = pageInfo.NewID();//ID 새로 핟당

                this.View.artBoard.Children.Clear();//이전 작업 영업에 있는 자식 컨트롤 모두 제거한다.
                this.View.artBoard.Width = pageInfo.ViewWidth;
                this.View.artBoard.Height = pageInfo.ViewHeight;

                foreach (TouchflowContentInfo item in pageInfo.Contents)
                { 
                    FrameworkElement inSysContentControl = InSysControlManager.GetNewSerializationInSysControl(item.ContentType, item.DesignElementProperties, InSysBasicControls.CommonClass.ViewState.Design) as FrameworkElement;
                    if (inSysContentControl != null)
                    {
                        IDesignElement designElement = inSysContentControl as IDesignElement;
                        if (designElement != null)
                        {
                            item.ContentObject = inSysContentControl;
                            if (item.DesignElementProperties.Keys.Contains("ActionEvent") == true)
                                designElement.SetActionEvent(item.DesignElementProperties["ActionEvent"]);//이벤트 설정

                            #region  Timeline 정보 설정.

                            double startTiem = designElement.StartTime.TotalSeconds;
                            double endTiem = designElement.EndTime.TotalSeconds;
                            if (designElement == null)
                            {
                                startTiem = 0;
                                endTiem = pageInfo.PageLifeTime;
                            }
                            
                            #endregion //Timeline 정보 설정.

                            #region ContentControl 타입에 따라 Insert 하는 위치 구분.

                            switch (designElement.InSysControlType)
                            {
                                case InSysBasicControls.CommonClass.InSysControlType.InSysAudioBox:
                                    this.View.DesignCanvasBar.Children.Add(inSysContentControl);
                                    break;
                                default:
                                    this.View.artBoard.Children.Add(inSysContentControl);
                                    break;
                            }

                            #endregion //ContentControl 타입에 따라 Insert 하는 위치 구분.
                        }

                        //this.workCanvasController.Init_SetInSysControl(pageInfo, inSysContentControl, this.workCanvasController.GetProjectInfo().PageInfos);
                    }
                }

                this.workCanvasController.AddPageInfo(pageInfo, this.View.artBoard);
                
                return pageInfo;
            }
            finally
            {
                flStream.Close();
            }
        }

        public void OnSaveProject(string path)
        {
            try
            {
                TouchflowProjectInfo projectInfo = this.workCanvasController.GetProjectInfo();
                TouchflowProjectInfo tempProjectInfo = projectInfo.CloneProperties();
                List<TouchflowPageInfo> pageInfoList = tempProjectInfo.PageInfos;
                
                foreach (TouchflowPageInfo pageInfo in pageInfoList)
                {
                    foreach (TouchflowContentInfo contentInfoItem in pageInfo.Contents)
                    {
                        contentInfoItem.DesignElementProperties = InSysBasicControls.InSysControlManager.SerializePropertiesInIDesignElement((contentInfoItem.ContentObject as IDesignElement).Properties);
                    }
                }
                List<Dictionary<string, object>> touchflowData = SetTouchflowProjectInfoFile(tempProjectInfo);
                InSysDataManager.Save(touchflowData, tempProjectInfo, path, SerializedFormat.Binary);
            }
            catch (Exception ex)
            {
                throw ex;
            }           
        }

        private List<Dictionary<string, object>> SetTouchflowProjectInfoFile(TouchflowProjectInfo tfProjectInfo)
        {
            List<Dictionary<string, object>> list = new List<Dictionary<string, object>>();

            Dictionary<string, object> projectInfoDic = new Dictionary<string, object>();
            projectInfoDic.Add("TouchflowData", tfProjectInfo);
            projectInfoDic.Add("PlayTime", new TimeSpan());
            projectInfoDic.Add("Width", tfProjectInfo.PageSize.Width);
            projectInfoDic.Add("Height", tfProjectInfo.PageSize.Height);
           
            list.Add(projectInfoDic);


            return list;
        }

        private Dictionary<string, object> GetTouchflowProjectInfoFile(TouchflowProjectInfo tfProjectInfo)
        {
            Dictionary<string, object> dic = new Dictionary<string, object>();
            dic.Add("TouchflowData", tfProjectInfo);
            dic.Add("PlayTime", new TimeSpan());

            return dic;
        }

        public object OnOpenProject(string path)
        {
            //TouchflowProjectInfo tfProject = ObjectXMLSerializer<TouchflowProjectInfo>.Load(path, SerializedFormat.Binary);
            try
            {
                TouchflowProjectInfo tempProjectInfo = InSysDataManager.Load(path, SerializedFormat.Binary);                

                TouchflowProjectInfo projectInfo = this.workCanvasController.GetProjectInfo();
                projectInfo = tempProjectInfo;

                this.View.artBoard.Children.Clear();//이전 작업 영업에 있는 자식 컨트롤 모두 제거한다.
                this.View.artBoard.Width = projectInfo.PageSize.Width;
                this.View.artBoard.Height = projectInfo.PageSize.Height;                

                foreach (TouchflowPageInfo pageInfo in projectInfo.PageInfos)
                {
                    pageInfo.SetPageInfo(pageInfo);
                    this.View.artBoard.Children.Clear();
                    foreach (TouchflowContentInfo item in pageInfo.Contents)
                    {
                        FrameworkElement inSysContentControl = InSysControlManager.GetNewSerializationInSysControl(item.ContentType, item.DesignElementProperties, InSysBasicControls.CommonClass.ViewState.Design) as FrameworkElement;

                        if (inSysContentControl != null)
                        {
                            IDesignElement designElement = inSysContentControl as IDesignElement;
                            if (designElement != null)
                            {
                                item.ContentObject = inSysContentControl;

                                #region Playlist의 경로 정보를 가상경로(CONTENTSVALUE)를 이용하여 Opne Project시 절대경로(Content)로 변경한다.
                                if (item.DesignElementProperties.ContainsKey("Playlist") == true)
                                {
                                    FileInfo projectFileinfo = new FileInfo(path);
                                    if (projectFileinfo.Exists == true)
                                    {
                                        List<PlaylistItem> playlistItems = (List<PlaylistItem>)item.DesignElementProperties["Playlist"];
                                        foreach (var playlistitem in playlistItems)
                                        {
                                            string fullpath = string.Format("{0}\\{1}", projectFileinfo.Directory.FullName, playlistitem.CONTENTSVALUE);
                                            playlistitem.Content = string.Format(@"{0}", fullpath);
                                        }
                                    }
                                }
                                #endregion

                                if (item.DesignElementProperties.Keys.Contains("ActionEvent") == true)
                                    designElement.SetActionEvent(item.DesignElementProperties["ActionEvent"]);//이벤트 설정

                                #region  Timeline 정보 설정.

                                double startTiem = designElement.StartTime.TotalSeconds;
                                double endTiem = designElement.EndTime.TotalSeconds;
                                if (designElement == null)
                                {
                                    startTiem = 0;
                                    endTiem = pageInfo.PageLifeTime;
                                }
                                
                                #endregion //Timeline 정보 설정.

                                #region ContentControl 타입에 따라 Insert 하는 위치 구분.
                                switch (designElement.InSysControlType)
                                {
                                    case InSysBasicControls.CommonClass.InSysControlType.InSysAudioBox:
                                        this.View.DesignCanvasBar.Children.Add(inSysContentControl);
                                        break;
                                    default:
                                        this.View.artBoard.Children.Add(inSysContentControl);
                                        break;
                                }
                                #endregion //ContentControl 타입에 따라 Insert 하는 위치 구분.
                            }

                            //this.workCanvasController.Init_SetInSysControl(pageInfo, inSysContentControl, projectInfo.PageInfos);
                        }
                    }

                    this.workCanvasController.AddPageInfo(pageInfo, this.View.artBoard);
                }

                return projectInfo;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }
        }

        private void ChangePage(TouchflowPageInfo changed_pageInfo)
        {
            this._priviousPageInfo = this._currentWorkingPageInfo;

            if (this._currentWorkingPageInfo != null)
            {
                this.workCanvasController.UpdateDesignView(this.View.artBoard, this._currentWorkingPageInfo);//변경전 Page 정보 업데이트.
                if (this._currentWorkingPageInfo.Equals(changed_pageInfo) != true)
                    this._currentWorkingPageInfo = changed_pageInfo;
            }
            else
            {
                this._currentWorkingPageInfo = changed_pageInfo;
            }     

            this.View.artBoard.Children.Clear();
            this.View.DesignCanvasBar.Children.Clear();

            this.SetControlTimeline(this._currentWorkingPageInfo.ApplyLifeTime);

            foreach (TouchflowContentInfo item in changed_pageInfo.Contents)
            {
                TouchflowContentInfo content = new TouchflowContentInfo();
                content.ContentType = item.ContentType;
                content.ContentPosition = item.ContentPosition;
               
                content.ContentSize = item.ContentSize;
                content.ContentName = item.ContentName;
                content.DesignElementProperties = item.DesignElementProperties;
                
                FrameworkElement contentControl = item.ContentObject as FrameworkElement;
                IDesignElement designElement = contentControl as IDesignElement;
                if (designElement != null)
                {
                    switch (designElement.InSysControlType)
                    {
                        case InSysBasicControls.CommonClass.InSysControlType.InSysAudioBox:
                            this.View.DesignCanvasBar.Children.Add(contentControl);
                            break;
                        default:
                            this.View.artBoard.Children.Add(contentControl);
                            break;
                    }
                }                
                
                this.View.SelectedCurrentContentItem = null;
                this.View.SelectedContentItems.Clear();
            }
           
            this.RaiseSendContentSelectionEvent(changed_pageInfo.ScreenProperty, this.View.artBoard);
            this.EnablePageLifeTime(changed_pageInfo);
           

            SetPageViewProperty();

            this.View.artBoard.Focusable = true;//JunYoung : 하단 Flow View 클릭시 WorkCanvas 화면에 Focus가 가도록 설정.
            this.View.artBoard.Focus();
        }

        private void SetPageViewProperty()
        {
            InSysBasicControls.InSysControlManager.SetEnableState_ControlContentOfProperty("Width", false);
            InSysBasicControls.InSysControlManager.SetEnableState_ControlContentOfProperty("Height", false);
        }

        private void EnablePageLifeTime(TouchflowPageInfo pageInfo)
        {
            if (pageInfo == null)
            {
                InSysBasicControls.InSysControlManager.SetEnableState_ControlContentOfProperty("PageLifeTime", false);
                return;
            }
            if (pageInfo.ApplyLifeTime == true)
            {
                InSysBasicControls.InSysControlManager.SetEnableState_ControlContentOfProperty("PageLifeTime", true);
            }
            else
            {
                InSysBasicControls.InSysControlManager.SetEnableState_ControlContentOfProperty("PageLifeTime", false);
            }
        }

        private void RaiseSendContentSelectionEvent(DependencyObject property, object obj)
        {
            ItemSelectedEventArgs newEventArgs = new ItemSelectedEventArgs(WorkCanvas.SendContentSelectedEvent, property, obj, View.artBoard);
            View.RaiseEvent(newEventArgs);
        }

        public void RaiseSendContentSelectionEvent(object source)
        {
            UIElement selectedElement = this.View.FindCanvasChild(source as DependencyObject);
            IElementProperties elementProperty = selectedElement as IElementProperties;//선택된 Element의 속성 객체를 관리하는 인터페이스 얻기
            if (elementProperty != null)
                RaiseSendContentSelectionEvent(elementProperty.ElementProperties, selectedElement);//속성 객체 전달
        }

        public void ZoomCanvas_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            SelectControl(e.Source);            
        }

        public void TouchDesignCanvas_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (InSysBasicControls.InSysControlManager.GetInSysControlPlayer(e.Source as UIElement) != null)
            {
                RaiseSendContentSelectionEvent(e.Source);
                UIElement elementControl = e.Source as UIElement;
                if (elementControl != null)
                {                   
                    //this.workCanvasController.Init_SetInSysControl(this._currentWorkingPageInfo, elementControl, this.workCanvasController.GetProjectInfo().PageInfos);                   
                }
            }
        }

        public void TouchDesignCanvas_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            SelectControl(e.Source);
        }

        public void WorkCanvas_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (this.View.SelectedCurrentContentItem != null)//선택된 Control이 없을 때만 처리
            {
                this.SelectedInSysControlItem(this.View.SelectedCurrentContentItem as FrameworkElement);
                //this.workCanvasController.UpdateDesignView(this.View.artBoard, this._currentWorkingPageInfo);
                return;
            }
            if (e.Source.GetType() == typeof(ZoomCanvas) || e.Source.GetType() == typeof(WorkCanvas))
            {               
                if (this._currentWorkingPageInfo != null)
                {                    
                    this.workCanvasController.UpdateDesignView(this.View.artBoard, this._currentWorkingPageInfo);
                    
                    this.IsApplyPageTimeline = this._currentWorkingPageInfo.ApplyLifeTime;
                                          
                    RaiseSendContentSelectionEvent(_currentWorkingPageInfo.ScreenProperty, this.View.ZoomCanvas);//ZoomCanvas Property를 Property View에 전달
                }
                
                SetPageViewProperty();
            }

            if (_currentWorkingPageInfo != null)
            {
                this.EnablePageLifeTime(_currentWorkingPageInfo);
            }
        }

        private void SelectControl(object selectObj)
        {
            if (selectObj.GetType() == typeof(ZoomCanvas))
            {
                
            }
            else
            {                
                UIElement elementControl = selectObj as UIElement;
                if (elementControl != null)
                {
                    RaiseSendContentSelectionEvent(elementControl);
                    IDesignElement designElement = elementControl as IDesignElement;
                    if (designElement == null)
                        return;
                    FrameworkElement frameworkElement = elementControl as FrameworkElement;
                    if (frameworkElement == null)
                        return;

                    //this.workCanvasController.Init_SetInSysControl(this._currentWorkingPageInfo, elementControl, this.workCanvasController.GetProjectInfo().PageInfos);
                    this.workCanvasController.UpdateDesignView(this.View.artBoard, this._currentWorkingPageInfo);
                    
                    this.SetControlTimeline(designElement.IsApplyLifeTime);
                    
                    InSysBasicControls.InSysControlManager.SetInSysControlPropertyVisibilty(elementControl);
                                     
                }
            }            
        }

        private void SetControlTimeline(bool isApplyControlTimeline)
        {
           
        }       

        public void artBoard_InsertControlContent(object sender, RoutedEventArgs e)
        {
            IDesignElement designElement = e.OriginalSource as IDesignElement;
            if (designElement != null)
            {
                FrameworkElement frameworkElement = e.OriginalSource as FrameworkElement;
                if (frameworkElement != null)
                {
                    switch (designElement.InSysControlType)
                    {
                        case InSysBasicControls.CommonClass.InSysControlType.InSysAudioBox:
                            View.DesignCanvasBar.Children.Add(frameworkElement);
                            break;
                        default:
                            View.artBoard.Children.Add(frameworkElement);
                            break;
                    }

                    AddNewControlContent(e.OriginalSource);
                    if (this._currentWorkingPageInfo != null)
                    {
                        InitSetting_InSysContentControl(designElement, this._currentWorkingPageInfo);//추가 컨텐츠컨트롤 정보 셋팅                 
                        this.SelectControl(frameworkElement);
                        this.View.ReleaseAllAdorner();
                        this.View.SelectedInSysControl(frameworkElement);
                               
                        //this.workCanvasController.Init_SetInSysControl(this._currentWorkingPageInfo, e.OriginalSource as UIElement, this.workCanvasController.GetProjectInfo().PageInfos);
                        //this.workCanvasController.UpdateDesignView(this.View.artBoard, this._currentWorkingPageInfo);
                    }
                }
            }
        }

        public void artBoard_ChangedContentControl(object sender, RoutedEventArgs e)
        {
            this.View.SelectedCurrentContentItem = (UIElement)e.OriginalSource;
            this.workCanvasController.UpdateDesignView(this.View.artBoard, this._currentWorkingPageInfo);
        }

        /// <summary>
        /// 초기 기본 정보 셋팅.
        /// </summary>
        /// <param name="contentControl"></param>
        /// <param name="pageinfo"></param>
        private void InitSetting_InSysContentControl(IDesignElement contentControl, TouchflowPageInfo pageinfo)
        {
            contentControl.StartTime = TimeSpan.FromSeconds(0);
            if (this.IsApplyPageTimeline == true)
                contentControl.EndTime = TimeSpan.FromSeconds(pageinfo.PageLifeTime);//Page End Time 설정.            
        }

        private void AddNewControlContent(object sender)
        {
            this.AddNewControlContent(sender as FrameworkElement);
        }

        public void AddNew_SerializedControlContent(FrameworkElement element)
        {
            IDesignElement designElement = element as IDesignElement;
            if (designElement != null)
            {
                Dictionary<string, object> properties = InSysControlManager.SerializePropertiesInIDesignElement(designElement.Properties);
                this._currentWorkingPageInfo.AddControlContent(element, properties);

                
            }
        }

        public void AddNewControlContent(FrameworkElement element)
        {            
            IDesignElement designElement = element as IDesignElement;
            if (designElement != null)
            {
                this.workCanvasController.InsertControl(element, designElement.Properties);
               
            }
        }

       
        public void WorkCanvas_MouseMove(object sender, MouseEventArgs e)
        {
            //this.CurrentMousePoint = e.GetPosition(this.View.artBoard);
        }

        public void SelectedInSysControlItem(FrameworkElement fwElement)
        {
            IDesignElement designElement = fwElement as IDesignElement;
            if (designElement != null)
            {
                //Dictionary<string, object> properties = InSysControlManager.SerializePropertiesInIDesignElement(designElement.Properties);
                Dictionary<string, object> properties = designElement.Properties;
                this._currentWorkingPageInfo.UpdateControlContent(fwElement, properties);
            }
        }
        
        public void OnReleaseAllAdorner()
        {
            this.View.ReleaseAllAdorner();
        }

        public void WorkCanvasUser_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            this.PressedKey(e);
        }
      
        private void PressedKey(KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Delete)
            {
                //this.Delete_SelectedControlItems();//ApplicationCommands.Delete Command에서 호출됨.
            }
            if (this.View.SelectedContentItems != null && this.View.SelectedContentItems.Count >= 1)
            {
                if (e.Key >= Key.Left && e.Key <= Key.Down)
                {
                    MoveControl(e);
                }
            }
        }

        private void MoveControl(KeyEventArgs e)
        {
            int step = 5;
            if (Keyboard.Modifiers == ModifierKeys.Control)
            {
                step = 1;
            }
            if (e.Key == Key.Left)
            {
                foreach (UIElement moveElement in this.View.SelectedContentItems)
                {
                    Canvas.SetLeft(moveElement, Canvas.GetLeft(moveElement) - step);
                }
            }
            else if (e.Key == Key.Right)
            {
                foreach (UIElement moveElement in this.View.SelectedContentItems)
                {
                    Canvas.SetLeft(moveElement, Canvas.GetLeft(moveElement) + step);
                }
            }
            else if (e.Key == Key.Up)
            {
                foreach (UIElement moveElement in this.View.SelectedContentItems)
                {
                    Canvas.SetTop(moveElement, Canvas.GetTop(moveElement) - step);
                }
            }
            else if (e.Key == Key.Down)
            {
                foreach (UIElement moveElement in this.View.SelectedContentItems)
                {
                    Canvas.SetTop(moveElement, Canvas.GetTop(moveElement) + step);
                }
            }
            e.Handled = true;
        }

        private void DeleteControlItem(UIElement uielement)
        {
            this.View.ReleaseElementDistance(uielement);
            //this.View.ReleaseSelectionAdorner(uielement);

            if (InSysBasicControls.InSysControlManager.GetControlType(uielement.GetType().Name) == InSysControlType.InSysAudioBox)
                this.View.DesignCanvasBar.Children.Remove(uielement);
            else
                this.View.artBoard.Children.Remove(uielement);
        }

        private void DeleteControlItems(List<UIElement> items)
        {
            foreach (UIElement uie in items)
            {
                this.DeleteControlItem(uie);
                
                this.DeleteControlInDesignView(this._currentWorkingPageInfo, uie as FrameworkElement);
            }
        }

        private void Delete_SelectedControlItems()
        {
            if (this.View.SelectedContentItems != null && this.View.SelectedContentItems.Count >= 1)
            {
                if (MessageBox.Show("삭제하시겠습니까?", "컨트롤 삭제", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {
                    this.DeleteControlItems(this.View.SelectedContentItems);
                }
            }
        }

        private void DeleteControlInDesignView(TouchflowPageInfo updatedPageInfo, FrameworkElement deleteElement)
        {
            var items = updatedPageInfo.Contents.Where(o => o.ContentObject.Equals(deleteElement) == true);
            List<TouchflowContentInfo> contentInfos = items.ToList();
            foreach (var contentInfo in contentInfos)
                updatedPageInfo.Contents.Remove(contentInfo);           
        }

        public void OnMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (this.View.SelectedContentItems != null && this.View.SelectedContentItems.Count == 1)
            {
                this.workCanvasController.UpdateDesignView(this.View.artBoard, this._currentWorkingPageInfo);
            }
        }

        public void WorkDesignView_MouseEnter(object sender, MouseEventArgs e)
        {

        }
    }
}
