﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using DesignWorkCanvas.Views;
using InSysBasicControls.Interfaces;
using InSysTouchflowData;
using InSysTouchflowData.Events;
using System.Windows.Input;
using InSysTouchflowData.Models.ProjectModels;
using System.Xml.Linq;
using InSysBasicControls;
using System.Windows.Controls;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.Diagnostics;

namespace DesignWorkCanvas.ViewModels
{
    public class DesignContainerViewModel : ViewModelBase
    {
        public enum State
        {
            Copy,
            Cut,
            None
        }

        private static DesignContainer CopyNCutSourceDesignContainer;
        private static State CopyState = State.None;
        public static List<UIElement> CutItems = new List<UIElement>();
        private bool _isFixedSize = false;
        private DesignContainer DesignContainer;
        double pastePositionValue = 15.0;        

        #region  RoutedCommand

        public static RoutedCommand BringToFrontCommand = new RoutedCommand();
        public static RoutedCommand BringForwardCommand = new RoutedCommand();
        public static RoutedCommand SendBackwardCommand = new RoutedCommand();
        public static RoutedCommand SendToBackCommand = new RoutedCommand();

        #endregion

        public DesignContainerViewModel()
        {
        }

        public void WorkDesignView_Loaded(object sender, RoutedEventArgs e)
        {
            if (DesignContainer == null)
            {
                DesignContainer = sender as DesignContainer;
                InitRoutedCommand();
                //DesignContainer.SetDesignCanvasZoomFixed();
            }
          
        }

        private void InitRoutedCommand()
        {
            DesignContainer.CommandBindings.Add(new CommandBinding(DesignContainerViewModel.BringToFrontCommand, BringToFrontCommand_Executed, BringToFrontCommand_CanExecute));
            DesignContainer.CommandBindings.Add(new CommandBinding(DesignContainerViewModel.BringForwardCommand, BringForwardCommand_Executed, BringForwardCommand_CanExecute));
            DesignContainer.CommandBindings.Add(new CommandBinding(DesignContainerViewModel.SendBackwardCommand, SendBackwardCommand_Executed, SendBackwardCommand_CanExecute));
            DesignContainer.CommandBindings.Add(new CommandBinding(DesignContainerViewModel.SendToBackCommand, SendToBackCommand_Executed, SendToBackCommand_CanExecute));

            DesignContainer.CommandBindings.Add(new CommandBinding(ApplicationCommands.Cut, Cut_Executed, Cut_CanExecute));
            DesignContainer.CommandBindings.Add(new CommandBinding(ApplicationCommands.Copy, Copy_Executed, Copy_CanExecute));
            DesignContainer.CommandBindings.Add(new CommandBinding(ApplicationCommands.Paste, Paste_Executed, Paste_CanExecute));
            DesignContainer.CommandBindings.Add(new CommandBinding(ApplicationCommands.Delete, Delete_Executed, Delete_CanExecute));
        }

        #region Order Command

        #region BringToFront Command

        private void BringToFrontCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            this.DesignContainer.BringToFront();            
        }

        private void BringToFrontCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            if (this.DesignContainer.SelectedContentItems != null && this.DesignContainer.SelectedContentItems.Count >= 1)
                e.CanExecute = true;
            else
                e.CanExecute = false;
        }
        #endregion//BringToFront Command

        #region BringForward Command

        private void BringForwardCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            this.DesignContainer.BringForward();           
        }

        private void BringForwardCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            if (this.DesignContainer.SelectedContentItems != null && this.DesignContainer.SelectedContentItems.Count >= 1)
                e.CanExecute = true;
            else
                e.CanExecute = false;
        }
        #endregion//BringForward Command

        #region SendBackward Command

        private void SendBackwardCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            this.DesignContainer.SendBackward();
        }

        private void SendBackwardCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            if (this.DesignContainer.SelectedContentItems != null && this.DesignContainer.SelectedContentItems.Count >= 1)
                e.CanExecute = true;
            else
                e.CanExecute = false;
        }
        #endregion//SendBackward Command

        #region SendToBack Command

        private void SendToBackCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            this.DesignContainer.SendToBack();
        }

        private void SendToBackCommand_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            if (this.DesignContainer.SelectedContentItems != null && this.DesignContainer.SelectedContentItems.Count >= 1)
                e.CanExecute = true;
            else
                e.CanExecute = false;
        }
        #endregion//SendToBack Command

        #endregion

        #region Control Cut, Copy, Paste, Delete Command

        #region Cut Command

        private void Cut_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            InitPastePosition();
            DesignContainerViewModel.CutItems.Clear();

            XElement rootContentItemsElement = new XElement("InSysControls");
            foreach (FrameworkElement contentControl in this.DesignContainer.SelectedContentItems)
            {
                TouchContentInfo contentInfo = this.DesignContainer.TouchPageInfo.Contents.Where(o => o.ContentObject.Equals(contentControl) == true).FirstOrDefault();
                if (contentInfo != null)
                {
                    FrameworkElement inSysContentControl = InSysBasicControls.InSysControlManager.GetNewInSysControl(contentInfo.ContentType, contentInfo.DesignElementProperties, ViewState.Design) as FrameworkElement;
                    TouchContentInfo copyContentInfo = this.Copy_ContentInfo(inSysContentControl, contentInfo, this.DesignContainer.TouchPageInfo.PageLifeTimeTemp, true);
                    XElement contentItemElement = new XElement("InSysControlContentInfo", Serialize(copyContentInfo));
                    rootContentItemsElement.Add(contentItemElement);
                }
                DesignContainerViewModel.CutItems.Add(contentControl as UIElement);

            }
            Clipboard.Clear();
            Clipboard.SetData(DataFormats.Xaml, rootContentItemsElement);

            DesignContainerViewModel.CopyState = State.Cut;
            DesignContainerViewModel.CopyNCutSourceDesignContainer = this.DesignContainer;//Cut한 다음 다른 페이지영역에서 Paste할 경우 삭제하기위한 원본 DesignContainer 저장.
        }

        private void Cut_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            if (this.DesignContainer.SelectedContentItems != null && this.DesignContainer.SelectedContentItems.Count >= 1)
                e.CanExecute = true;
            else
                e.CanExecute = false;
        }

        #endregion

        #region Copy Command

        private void Copy_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            InitPastePosition();
            XElement rootContentItemsElement = new XElement("InSysControls");
            foreach (FrameworkElement contentControl in this.DesignContainer.SelectedContentItems)
            {
                TouchContentInfo contentInfo = this.DesignContainer.TouchPageInfo.Contents.Where(o => o.ContentObject.Equals(contentControl) == true).FirstOrDefault();
                if (contentInfo != null)
                {
                    contentInfo.update_Position();//위치 업데이트.
                    //FrameworkElement inSysContentControl = InSysBasicControls.InSysControlManager.GetNewInSysControl(contentInfo.ContentType, contentInfo.DesignElementProperties, ViewState.Design) as FrameworkElement;
                    TouchContentInfo copyContentInfo = this.Copy_ContentInfo(contentControl, contentInfo, this.DesignContainer.TouchPageInfo.PageLifeTimeTemp, true);
                    XElement contentItemElement = new XElement("InSysControlContentInfo", Serialize(copyContentInfo));
                    rootContentItemsElement.Add(contentItemElement);
                }
            }
            Clipboard.Clear();
            Clipboard.SetData(DataFormats.Xaml, rootContentItemsElement);

            DesignContainerViewModel.CopyState = State.Copy;
            DesignContainerViewModel.CopyNCutSourceDesignContainer = this.DesignContainer;//Cut한 다음 다른 페이지영역에서 Paste할 경우 삭제하기위한 원본 DesignContainer 저장.
        }

        private void Copy_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            if (this.DesignContainer.SelectedContentItems != null && this.DesignContainer.SelectedContentItems.Count >= 1)
                e.CanExecute = true;
            else
                e.CanExecute = false;
        }

        #endregion

        #region Paste Command
        
        private void Paste_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            int zIndexMax = 0;
            XElement rootXml = null;
            Stopwatch stopwatch = new Stopwatch();
            stopwatch.Start();

            try
            {
                if (this.DesignContainer.TouchPageInfo.Contents.Count() <= 0)
                    zIndexMax = 0;
                else
                    zIndexMax = this.DesignContainer.TouchPageInfo.Contents.Where(o => o.ContentObject != null).Max(o => Canvas.GetZIndex(o.ContentObject));//배치 순서중 제일 높은 것 찾기.
                rootXml = LoadSerializedDataFromClipBoard();
                Console.WriteLine(string.Format("Stop Watch #1: {0}", stopwatch.Elapsed.TotalMilliseconds)); stopwatch.Restart();
                if (rootXml != null)
                {
                    IEnumerable<XElement> itemsXML = rootXml.Elements("InSysControlContentInfo");
                    if (itemsXML.Count() <= 0)
                        return;

                    List<FrameworkElement> lastSelectedUIElements = new List<FrameworkElement>();

                    Console.WriteLine(string.Format("Stop Watch #2: {0}", stopwatch.Elapsed.TotalMilliseconds)); stopwatch.Restart();

                    foreach (XElement itemElement in itemsXML)
                    {
                        Console.WriteLine(string.Format("<<<<<<<<<<<< Content :")); stopwatch.Restart();

                        TouchContentInfo copySourceTouchContentInfo = DeSerialize(itemElement.Value) as TouchContentInfo;
                        if (copySourceTouchContentInfo == null)
                            continue;

                        string newElementName = InSysControlManager.CheckInSysControlName(InSysControlManager.GetControlType(copySourceTouchContentInfo.ContentType.Name), this.DesignContainer.PageDesignCanvasBox.Children);
                        FrameworkElement pasteTargetInSysContentControl = InSysControlManager.GetNewInSysControl(copySourceTouchContentInfo.ContentType, copySourceTouchContentInfo.DesignElementProperties, newElementName, ViewState.Design) as FrameworkElement;
                        if (pasteTargetInSysContentControl != null)
                        {
                            IDesignElement designeElement = pasteTargetInSysContentControl as IDesignElement;
                            this.DesignContainer.RaiseInsertContentEvent(pasteTargetInSysContentControl);//이 부분에서 약간 Delay 되는 현상이 있음. insert하면서 Property 화면 업데이트하기 때문.
                            TouchContentInfo pasteTargetTouchContentInfo = this.DesignContainer.TouchPageInfo.GetTouchContentInfo(pasteTargetInSysContentControl);
                            if (pasteTargetTouchContentInfo == null)
                                continue;

                            Console.WriteLine(string.Format("Content #1: {0}", stopwatch.Elapsed.TotalMilliseconds)); 

                            #region IDesignElement 정보 업데이트
                            designeElement.Name = newElementName;
                            designeElement.ZIndex = zIndexMax + 1;
                            pasteTargetTouchContentInfo.ContentName = newElementName;
                            this.DesignContainer.RaiseItemUpdatedPropertyEvent(pasteTargetInSysContentControl, "Playlist");
                            #endregion                            

                            Console.WriteLine(string.Format("Content #2: {0}", stopwatch.Elapsed.TotalMilliseconds)); 

                            InSysBasicControls.InSysControlManager.Init_ContentElementProperty(pasteTargetInSysContentControl, this.DesignContainer.TouchPageInfo.PageLifeTimeTemp, this.DesignContainer.TouchPageInfo.IsApplyPageLifeTime);

                            Console.WriteLine(string.Format("Content #3: {0}", stopwatch.Elapsed.TotalMilliseconds)); 

                            #region 추가 ContentControl 정보 Update

                            if (InSysControlManager.GetControlType(copySourceTouchContentInfo.ContentType.Name) != InSysControlType.InSysAudioBox)
                            {
                                #region Audio를 제외한 컨트롤 Paste할 Position 지정하기
                                if (DesignContainerViewModel.CopyNCutSourceDesignContainer.Equals(this.DesignContainer) == true)//Copy, Cut의 DesignContainer와 Paste의 DesignContainer가 동일 할 경우
                                {
                                    double left = copySourceTouchContentInfo.ContentPosition.X + pastePositionValue;
                                    double top = copySourceTouchContentInfo.ContentPosition.Y + pastePositionValue;

                                    Canvas.SetLeft(pasteTargetInSysContentControl, left);
                                    Canvas.SetTop(pasteTargetInSysContentControl, top);

                                    pasteTargetTouchContentInfo.ContentPosition = new Point(left, top);
                                }
                                else//다른 페이지에 Copy, Cut 할 경우.
                                {
                                    double left = copySourceTouchContentInfo.ContentPosition.X;
                                    double top = copySourceTouchContentInfo.ContentPosition.Y;

                                    Canvas.SetLeft(pasteTargetInSysContentControl, left);
                                    Canvas.SetTop(pasteTargetInSysContentControl, top);

                                    pasteTargetTouchContentInfo.ContentPosition = new Point(left, top);
                                }
                                #endregion //Audio를 제외한 컨트롤 Paste
                            }
                            else //Audio
                            {
                            }

                            lastSelectedUIElements.Add(pasteTargetInSysContentControl);

                            #endregion//ContentInfo Object를 PageInfo Object 정보에 Update                           
                        }
                        Console.WriteLine(string.Format("Content #4: {0}", stopwatch.Elapsed.TotalMilliseconds));
                        Console.WriteLine(string.Format(">>>>>>>>>>> Content"));
                    }

                    Console.WriteLine(string.Format("Stop Watch #3: {0}", stopwatch.Elapsed.TotalMilliseconds));

                    if (DesignContainerViewModel.CopyState == State.Cut)
                    {
                        #region Cut일 경우  처리
                        DesignContainer designeContainer = DesignContainerViewModel.CopyNCutSourceDesignContainer;
                        foreach (UIElement delItem in DesignContainerViewModel.CutItems)
                        {
                            designeContainer.RaiseDeleteContentEvent(delItem);

                            TouchContentInfo contentInfo = designeContainer.TouchPageInfo.Contents.Where(o => o.ContentObject.Equals(delItem) == true).FirstOrDefault();
                            designeContainer.TouchPageInfo.Contents.Remove(contentInfo);
                            this.DeleteControlItem(designeContainer, delItem);
                        }

                        DesignContainerViewModel.CutItems.Clear();
                        designeContainer.RaisePageUpdateEvent(null);
                        #endregion
                    }
                    else
                    {
                        this.DesignContainer.RaisePageUpdateEvent(null);
                    }

                    this.DesignContainer.releaseAllAdorner();
                    this.DesignContainer.SelectedContentItems.Clear();
                    foreach (var selItem in lastSelectedUIElements)
                    {
                        this.DesignContainer.selectItemAdorner(selItem);
                        this.DesignContainer.SelectedCurrentContentItem = selItem;
                    }

                    pastePositionValue += 15.0;
                }

                stopwatch.Stop();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + " --- " + ex.StackTrace);
            }
        }

        private void Paste_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            if (DesignContainerViewModel.CopyState != State.None)
                e.CanExecute = true;
            else
                e.CanExecute = false;
        }

        #endregion

        #region Delete Command

        private void Delete_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            InitPastePosition();
            this.Delete_SelectedControlItems();
            this.DesignContainer.RaisePageUpdateEvent(null);
        }

        private void Delete_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            if (this.DesignContainer.SelectedContentItems != null && this.DesignContainer.SelectedContentItems.Count >= 1)
                e.CanExecute = true;
            else
                e.CanExecute = false;
        }

        #endregion

        private void InitPastePosition()
        {
            pastePositionValue = 15;
        }

        #endregion //Control Cut, Copy, Paste, Delete Command

        public TouchContentInfo Copy_ContentInfo(FrameworkElement inSysContentControl, TouchContentInfo sourceContentInfo, double pageLifeTime, bool isSerialization)
        {
            TouchContentInfo newContentInfo = new TouchContentInfo();            
            if (inSysContentControl != null)
            {
                inSysContentControl.UpdateLayout();

                IDesignElement designElement = inSysContentControl as IDesignElement;

                if (designElement != null)
                {
                    #region ContentInfo 정보 Copy

                    newContentInfo.ContentType = sourceContentInfo.ContentType;
                    newContentInfo.ContentPosition = sourceContentInfo.ContentPosition;
                    newContentInfo.ContentSize = sourceContentInfo.ContentSize;
                    newContentInfo.ContentObject = inSysContentControl;                    
                    if (isSerialization == true)
                        newContentInfo.DesignElementProperties = InSysBasicControls.InSysControlManager.SerializePropertiesInIDesignElement(designElement.Properties);
                    else
                        newContentInfo.DesignElementProperties = designElement.Properties;

                    newContentInfo.ContentName = sourceContentInfo.ContentName;

                    #endregion
                }

                //this.workCanvasController.Init_SetInSysControl(this.DesignContainer.TouchPageInfo, inSysContentControl, this.workCanvasController.GetProjectInfo().PageInfos);
            }

            return newContentInfo;
        }

        private string Serialize(object objectToSerialize)
        {

            string serialString = null;
            using (System.IO.MemoryStream ms1 = new System.IO.MemoryStream())
            {
                try
                {
                    BinaryFormatter b = new BinaryFormatter();
                    b.Serialize(ms1, objectToSerialize);
                    byte[] arrayByte = ms1.ToArray();
                    serialString = Convert.ToBase64String(arrayByte);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
            return serialString;
        }

        private object DeSerialize(string serializationString)
        {
            object deserialObject = null;
            byte[] arrayByte = Convert.FromBase64String(serializationString);
            using (System.IO.MemoryStream ms1 = new System.IO.MemoryStream(arrayByte))
            {
                BinaryFormatter b = new BinaryFormatter();
                deserialObject = b.Deserialize(ms1);
            }
            return deserialObject;
        }

        private XElement LoadSerializedDataFromClipBoard()
        {
            if (Clipboard.ContainsData(DataFormats.Xaml))
            {
                String clipboardData = Clipboard.GetData(DataFormats.Xaml) as String;

                if (String.IsNullOrEmpty(clipboardData))
                    return null;
                try
                {
                    return XElement.Load(new StringReader(clipboardData));
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.StackTrace, e.Message, MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }

            return null;
        }

        public void AddNewControlContent(FrameworkElement element)
        {
            IDesignElement designElement = element as IDesignElement;
            if (designElement != null)
            {
                this.DesignContainer.TouchPageInfo.AddControlContent(element, designElement.Properties);
                //this.workCanvasController.InsertControl(element, designElement.Properties);
                //timelineControl.AddNewControlTimeline(element, designElement.IsApplyLifeTime);
            }
        }

        private void DeleteControlItem(DesignContainer designContainer, UIElement uielement)
        {
            designContainer.ReleaseElementDistance(uielement);

            switch (InSysBasicControls.InSysControlManager.GetControlType(uielement.GetType().Name))
            {
                case InSysControlType.InSysAudioBox:
                    this.DesignContainer.PageAudioBox.Children.Remove(uielement);
                    break;
                default:
                    designContainer.PageDesignCanvasBox.Children.Remove(uielement);
                    break;
            }
        }

        private void DeleteControlItems(List<UIElement> items)
        {
            foreach (UIElement uie in items)
            {
                this.DeleteControlItem(this.DesignContainer, uie);
                //this.timelineControl.DeleteControlTimeline(uie as FrameworkElement);
                this.DeleteControlInDesignView(this.DesignContainer.TouchPageInfo, uie as FrameworkElement);
            }
        }

        private void Delete_SelectedControlItems()
        {
            if (this.DesignContainer.SelectedContentItems != null && this.DesignContainer.SelectedContentItems.Count >= 1)
            {
                if (MessageBox.Show("삭제하시겠습니까?", "컨트롤 삭제", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {
                    this.DeleteControlItems(this.DesignContainer.SelectedContentItems);
                    foreach (var deleteItem in this.DesignContainer.SelectedContentItems)
                    {
                        this.DesignContainer.RaiseDeleteContentEvent(deleteItem);
                    }
                }
            }
        }

        private void DeleteControlInDesignView(TouchPageInfo updatedPageInfo, FrameworkElement deleteElement)
        {
            var items = updatedPageInfo.Contents.Where(o => o.ContentObject.Equals(deleteElement) == true);
            List<TouchContentInfo> contentInfos = items.ToList();
            foreach (var contentInfo in contentInfos)
                updatedPageInfo.Contents.Remove(contentInfo);
        }
               
        public void DesignContainer_InsertContent(object sender, RoutedEventArgs e)
        {
            if (DesignContainer == null)
                return;
            IDesignElement designElement = e.OriginalSource as IDesignElement;
            if (designElement != null)
            {
                FrameworkElement frameworkElement = e.OriginalSource as FrameworkElement;
                if (frameworkElement != null)
                {
                    switch (designElement.InSysControlType)
                    {
                        case InSysControlType.InSysAudioBox:
                            DesignContainer.PageAudioBox.Children.Add(frameworkElement);
                            break;
                        default:
                            DesignContainer.PageDesignCanvasBox.Children.Add(frameworkElement);
                            break;
                    }

                    designElement.EndTime = TimeSpan.FromSeconds(DesignContainer.TouchPageInfo.PageLifeTimeTemp);//요소 추가시 요소의 EndTime을 Screen Time의 값으로 초기화한다.

                    SelectControl(frameworkElement);

                    this.DesignContainer.TouchPageInfo.AddControlContent(frameworkElement, designElement.Properties);
                }
            }
        }

        public void DesignContainer_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (this.DesignContainer.SelectedCurrentContentItem != null)//선택된 Control이 없을 때만 처리
            {
                #region 마우스 UP시 선택 Element 속성값 업데이트
                this.DesignContainer.RaiseItemUpdatedPropertyEvent(this.DesignContainer.SelectedCurrentContentItem, "");
                #endregion
                return;
            }
            if (e.Source.GetType() == typeof(PageCanvas) || e.Source.GetType() == typeof(DesignContainer))
            {
                if (this.DesignContainer.TouchPageInfo != null)
                {
                    RaiseSelectedPageEvent(DesignContainer.TouchPageInfo.PageProperty, this.DesignContainer.PageDesignCanvasBox);//PageCanvas Property를 Property View에 전달
                }

                SetPageViewProperty();
            }
            else
            {
                RaiseSelectedContentEvent(e.Source);
            }
        }

        private void SetPageViewProperty()
        {
            InSysBasicControls.InSysControlManager.Set_ContentElementControlIsEnableProperty("Width", false);
            InSysBasicControls.InSysControlManager.Set_ContentElementControlIsEnableProperty("Height", false);
        }

        private void SelectControl(object selectObj)
        {
            if (selectObj.GetType() == typeof(DesignContainer))
            {

            }
            else
            {                
                UIElement elementControl = selectObj as UIElement;
                if (elementControl != null)
                {
                    #region SelectedContent 호출 - Content Element 선택시 처리하기 위해 콜백 함수

                    RaiseSelectedContentEvent(elementControl);
                    
                    #endregion

                    DesignContainer.SelectedCurrentContentItem = elementControl;
                    
                    IDesignElement designElement = elementControl as IDesignElement;
                    if (designElement == null)
                        return;
                    FrameworkElement frameworkElement = elementControl as FrameworkElement;
                    if (frameworkElement == null)
                        return;
                }                
            }
        }

        private void RaiseSelectedPageEvent(DependencyObject property, object obj)
        {
            RoutedEventArgs eventArgs = new RoutedEventArgs(DesignContainer.SelectedPageEvent, obj);
            DesignContainer.RaiseEvent(eventArgs);
        }

        private void RaiseSelectedContentEvent(DependencyObject property, object obj)
        {
            //ItemSelectedEventArgs newEventArgs = new ItemSelectedEventArgs(DesignContainer.SelectedContentEvent, property, obj, DesignContainer.DesignCanvas);
            RoutedEventArgs eventArgs = new RoutedEventArgs(DesignContainer.SelectedContentEvent, obj);
            DesignContainer.RaiseEvent(eventArgs);
        }

        public void RaiseSelectedContentEvent(object source)
        {
            UIElement selectedElement = this.DesignContainer.FindCanvasChild(source as DependencyObject);
            IElementProperties elementProperty = selectedElement as IElementProperties;//선택된 Element의 속성 객체를 관리하는 인터페이스 얻기
            if (elementProperty != null)
                RaiseSelectedContentEvent(elementProperty.ElementProperties, selectedElement);//속성 객체 전달
        }

        public void DesignContainer_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            this.PressedKey(e);
        }

        private void PressedKey(KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Delete)
            {
                //this.Delete_SelectedControlItems();//ApplicationCommands.Delete Command에서 호출됨.
            }
            if (this.DesignContainer.SelectedContentItems != null && this.DesignContainer.SelectedContentItems.Count >= 1)
            {
                if (e.Key >= Key.Left && e.Key <= Key.Down)
                {
                    MoveControl(e);
                }
            }
        }

        private void MoveControl(KeyEventArgs e)
        {
            int step = 5;
            if (Keyboard.Modifiers == ModifierKeys.Control)
            {
                step = 1;
            }
            if (e.Key == Key.Left)
            {
                foreach (UIElement moveElement in this.DesignContainer.SelectedContentItems)
                {
                    Canvas.SetLeft(moveElement, Canvas.GetLeft(moveElement) - step);
                    this.DesignContainer.MoveContentElementDistanceLine(moveElement, true);
                }
            }
            else if (e.Key == Key.Right)
            {
                foreach (UIElement moveElement in this.DesignContainer.SelectedContentItems)
                {
                    Canvas.SetLeft(moveElement, Canvas.GetLeft(moveElement) + step);
                    this.DesignContainer.MoveContentElementDistanceLine(moveElement, true);
                }
            }
            else if (e.Key == Key.Up)
            {
                foreach (UIElement moveElement in this.DesignContainer.SelectedContentItems)
                {
                    Canvas.SetTop(moveElement, Canvas.GetTop(moveElement) - step);
                    this.DesignContainer.MoveContentElementDistanceLine(moveElement, true);
                }
            }
            else if (e.Key == Key.Down)
            {
                foreach (UIElement moveElement in this.DesignContainer.SelectedContentItems)
                {
                    Canvas.SetTop(moveElement, Canvas.GetTop(moveElement) + step);
                    this.DesignContainer.MoveContentElementDistanceLine(moveElement, true);
                }
            }
            e.Handled = true;
        }
    }
}
