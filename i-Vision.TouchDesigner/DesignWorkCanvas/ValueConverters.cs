﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using System.Windows.Data;

namespace DesignWorkCanvas
{
    public class ZoomValueConverter : IValueConverter
    {
        #region IValueConverter Members
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                double zoomValue = (double)value;

                string retValue = string.Format("{0:F0}", zoomValue);

                return retValue;
            }

            return string.Empty;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            //throw new Exception("The method or operation is not implemented.");
            return Binding.DoNothing;
        }

        #endregion
    }
}
