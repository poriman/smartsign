﻿using System;
using UtilLib.DragDropManager;
using System.Windows;
using System.Xml;
using System.IO;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Markup;
using InSysBasicControls;
using InSysBasicControls.Commons;
using InSysBasicControls.Interfaces;
using System.Windows.Input;
using System.Collections.Generic;
using DesignWorkCanvas.Views;
using InSysTouchflowData;

namespace DesignWorkCanvas
{
    public class CanvasDropTargetAdvisor : IDropTargetAdvisor
    {
        private static DataFormat SupportedFormat = DataFormats.GetDataFormat("Samples");
        private UIElement _allowTargetOutElt;
        private UIElement _targetUI;
        private UIElement _targetContainer;

        public bool IsValidDataObject(IDataObject obj)
        {
            return obj.GetDataPresent(SupportedFormat.Name);
        }

        public void OnDropCompleted(IDataObject obj, Point dropPoint)
        {
            string serializedObject = obj.GetData(SupportedFormat.Name) as string;
            XmlReader reader = XmlReader.Create(new StringReader(serializedObject));
            FrameworkElement frameworkElement = XamlReader.Load(reader) as FrameworkElement;

            if (frameworkElement != null)
            {
                UIElement designItem = null; 
                if (TargetContainer is UserControl)
                {
                    DesignContainer designContainer = TargetContainer as DesignContainer;
                    
                    if (designContainer != null)
                    {
                        designContainer.IsElementInsertingState = true;
                        designItem = InSysControlManager.CreateInSysControlObjectFromToolBoxType((string)frameworkElement.Tag, ViewState.Design);

                        IElementProperties eleProperties1 = designItem as IElementProperties;
                        if (eleProperties1 != null)
                        {
                            eleProperties1.ElementProperties.X = dropPoint.X;
                            eleProperties1.ElementProperties.Y = dropPoint.Y;
                        }
                        else
                        {
                            Canvas.SetLeft(designItem, dropPoint.X);
                            Canvas.SetTop(designItem, dropPoint.Y);
                        }

                        IDesignElement designeElement = designItem as IDesignElement;
                        if (designeElement != null)
                        {
                            IEnumerable<UIElement> frontItems = designContainer.PageCanvas.Children.OfType<UIElement>().Where(o => InSysBasicControls.InSysControlManager.GetControlType(o.GetType().Name) != InSysControlType.None);
                            int ZIndex = 0;
                            if (frontItems != null && frontItems.Count() >= 1)
                            {
                                #region 컨트롤 ZIndex 재정렬
                                var indexOrderList = frontItems.OrderBy(o => Canvas.GetZIndex(o));
                                int order = 0;
                                indexOrderList.ToList().ForEach(o => Canvas.SetZIndex(o, order++));
                                ZIndex = indexOrderList.Select(o => Canvas.GetZIndex(o)).Max();
                                ZIndex += 1;
                                #endregion
                            }
                            designeElement.ZIndex = ZIndex;                            
                            string name = InSysControlManager.CheckInSysControlName(InSysControlManager.GetControlTypeFromToolBoxType((string)frameworkElement.Tag), designContainer.PageCanvas.Children);
                            designeElement.Name = name;
                            designContainer.RaiseInsertContentEvent(designItem);
                        }

                        designContainer.IsElementInsertingState = false;
                    }
                }
                else
                {
                    Panel otherPanel = TargetCanvas as Panel;
                    if (otherPanel != null)
                    {
                        designItem = InSysControlManager.CreateInSysControlObjectFromToolBoxType((string)frameworkElement.Tag, ViewState.Design);

                        IDesignElement designeElement = designItem as IDesignElement;
                        if (designeElement != null)
                        {
                            string name = InSysControlManager.CheckInSysControlName(InSysControlManager.GetControlTypeFromToolBoxType((string)frameworkElement.Tag), otherPanel.Children);
                            designeElement.Name = name;
                        }
                        otherPanel.Children.Add(designItem);
                    }

                }

                designItem.SetEvent_InSysControlSizeChanged(new SizeChangedEventHandler(CanvasDropTargetAdvisor_SizeChanged));//SizeChanged 이벤트 등록
                designItem.SetEvent_InSysControlMouseUp(new MouseButtonEventHandler(CanvasDropTargetAdvisor_PreviewMouseUp));
            }
        }

        /// <summary>
        /// ZoomCanvas에 등록된 InSysControl의 SizeChanged 이벤트 콜백 함수.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void CanvasDropTargetAdvisor_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (TargetCanvas is Canvas)
            {
                //ZoomCanvas zoomCanvas = TargetCanvas as ZoomCanvas;
                //zoomCanvas.RaiseChangedContentControlEvent(sender);
            }
        }

        void CanvasDropTargetAdvisor_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            PageCanvas zoomCanvas = TargetCanvas as PageCanvas;
            if (zoomCanvas != null)
            {
                zoomCanvas.ResizePanelHandler();
            }
        }

        public UIElement TargetContainer
        {
            get
            {
                return _targetContainer;
            }
            set
            {
                _targetContainer = value;
            }
        }

        public UIElement TargetCanvas
        {
            get
            {
                return _targetUI;
            }
            set
            {
                _targetUI = value;
            }
        }

        public UIElement AllowTargetOutElt
        {
            get
            {
                return _allowTargetOutElt;
            }
            set
            {
                _allowTargetOutElt = value;
            }
        }
    }
}
