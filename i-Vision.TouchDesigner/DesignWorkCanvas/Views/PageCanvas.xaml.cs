﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Media.Animation;
using System.ComponentModel;
using InSysTouchflowData;

namespace DesignWorkCanvas.Views
{
    /// <summary>
    /// Interaction logic for PageCanvas.xaml
    /// </summary>
    public partial class PageCanvas : Canvas
    {
        private bool _isFitPage = false;
        public delegate void MouseWheelDelegate(MouseWheelEventArgs e);
        public MouseWheelDelegate MouseWheelHandler;

        public delegate void ResizePanelDelegate();
        public ResizePanelDelegate ResizePanelHandler;

        #region Public enums and constants
        public enum eZoomMode
        {
            CustomSize,
            ActualSize,
            FitWidth,
            FitHeight,
            FitPage,
            FitVisible
        }

        public enum eMouseMode
        {
            None,
            Zoom,
            Pan
        }

        public enum eWheelMode
        {
            None,
            Zoom,
            Scroll,
            Rotate
        }

        const double MINZOOM = 0.1;
        const double DEFAULTLEFTRIGHTMARGIN = 4;
        const double DEFAULTTOPBOTTOMMARGIN = 3;

        #endregion

        #region Do Zoom Event

        public static readonly RoutedEvent DoZoomEvent = EventManager.RegisterRoutedEvent("DoZoom", RoutingStrategy.Bubble,
           typeof(RoutedEventHandler), typeof(PageCanvas));

        public event RoutedEventHandler DoZoom
        {
            add { AddHandler(DoZoomEvent, value); }
            remove { RemoveHandler(DoZoomEvent, value); }
        }

        private void RaiseDoZoomEvent(object source)
        {
            RoutedEventArgs newEventArg = new RoutedEventArgs(PageCanvas.DoZoomEvent, source);
            RaiseEvent(newEventArg);
        }

        #endregion

        #region Key Down Event

        public static readonly RoutedEvent KeyDownEvent = EventManager.RegisterRoutedEvent("KeyDown", RoutingStrategy.Bubble,
           typeof(RoutedEventHandler), typeof(PageCanvas));

        public event RoutedEventHandler KeyDown
        {
            add { AddHandler(KeyDownEvent, value); }
            remove { RemoveHandler(KeyDownEvent, value); }
        }

        private void RaiseKeyDownEvent(object source)
        {
            RoutedEventArgs newEventArg = new RoutedEventArgs(PageCanvas.KeyDownEvent, source);
            RaiseEvent(newEventArg);
        }

        #endregion

        static PageCanvas()
        {
            InitStaticMethod();
        }

        public PageCanvas()
        {
            translateTransform = new TranslateTransform();
            rotateTransform = new RotateTransform();
            zoomTransform = new ScaleTransform();

            transformGroup = new TransformGroup();

            InitializeComponent();

            transformGroup.Children.Add(this.rotateTransform);
            transformGroup.Children.Add(this.zoomTransform);
            transformGroup.Children.Add(this.translateTransform);

            panX = 0;
            panY = 0;
            rotateAngle = 0;
            rotateCenterX = 0;
            rotateCenterY = 0;
            AllowOutWheelAction = true;

            childSize = new Size(1, 1);
            MouseWheelHandler = new MouseWheelDelegate(DoMouseWheelHandler);
            ResizePanelHandler = new ResizePanelDelegate(OnResizePanel);
        }

        #region Dependency Properties

        // Layout
        private static DependencyProperty PaddingProperty;
        private static DependencyProperty CenterContentProperty;

        // Zoom Scale
        private static DependencyProperty MinZoomProperty;
        private static DependencyProperty MaxZoomProperty;
        private static DependencyProperty ZoomProperty;

        private static DependencyPropertyKey CanIncreaseZoomPropertyKey;
        private static DependencyPropertyKey CanDecreaseZoomPropertyKey;
        private static DependencyPropertyKey CanRotateKey;

        private static DependencyProperty ZoomTickProperty;
        private static DependencyProperty MinZoomTickProperty;
        private static DependencyProperty MaxZoomTickProperty;

        // Delta
        private static DependencyProperty ZoomIncrementProperty;
        private static DependencyProperty RotateIncrementProperty;
        private static DependencyProperty WheelZoomDeltaProperty;

        // modes
        private static DependencyProperty ZoomModeProperty;
        private static DependencyProperty MouseModeProperty;
        private static DependencyProperty WheelModeProperty;

        // animation
        private static DependencyProperty AnimationsProperty;
        private static DependencyProperty LockContentProperty;

        // Zoom 위치
        private static DependencyProperty LockZoomPointProperty;

        // PageCanvas 밖에서 Wheel 버튼 범위 허용 여부
        private static DependencyProperty AllowOutWheelActionProperty;

        //PageCanvas 영역 외부에서 Wheel 이벤트 처리
        public bool AllowOutWheelAction
        {
            set { SetValue(AllowOutWheelActionProperty, value); }
            get { return (bool)GetValue(AllowOutWheelActionProperty); }
        }

        public Thickness Padding
        {
            set { SetValue(PaddingProperty, value); }
            get { return (Thickness)GetValue(PaddingProperty); }
        }
        public bool CenterContent
        {
            set { SetValue(CenterContentProperty, value); }
            get { return (bool)GetValue(CenterContentProperty); }
        }

        public double MinZoom
        {
            set { SetValue(MinZoomProperty, value); }
            get { return (double)GetValue(MinZoomProperty); }
        }
        public double MaxZoom
        {
            set { SetValue(MaxZoomProperty, value); }
            get { return (double)GetValue(MaxZoomProperty); }
        }
        public double Zoom
        {
            set { SetValue(ZoomProperty, value); }
            get { return (double)GetValue(ZoomProperty); }
        }

        public bool CanIncreaseZoom
        {
            get { return (bool)GetValue(CanIncreaseZoomPropertyKey.DependencyProperty); }
        }
        public bool CanDecreaseZoom
        {
            get { return (bool)GetValue(CanDecreaseZoomPropertyKey.DependencyProperty); }
        }
        public bool CanRotate
        {
            get { return (bool)GetValue(CanRotateKey.DependencyProperty); }
        }

        public double ZoomTick
        {
            set { SetValue(ZoomTickProperty, value); }
            get { return (double)GetValue(ZoomTickProperty); }
        }
        public double MinZoomTick
        {
            set { SetValue(MinZoomTickProperty, value); }
            get { return (double)GetValue(MinZoomTickProperty); }
        }
        public double MaxZoomTick
        {
            set { SetValue(MaxZoomTickProperty, value); }
            get { return (double)GetValue(MaxZoomTickProperty); }
        }

        public double ZoomIncrement
        {
            set { SetValue(ZoomIncrementProperty, value); }
            get { return (double)GetValue(ZoomIncrementProperty); }
        }
        public double RotateIncrement
        {
            set { SetValue(RotateIncrementProperty, value); }
            get { return (double)GetValue(RotateIncrementProperty); }
        }

        public double WheelZoomDelta
        {
            set { SetValue(WheelZoomDeltaProperty, value); }
            get { return (double)GetValue(WheelZoomDeltaProperty); }
        }

        public PageCanvas.eWheelMode WheelMode
        {
            set { SetValue(WheelModeProperty, value); }
            get { return (PageCanvas.eWheelMode)GetValue(WheelModeProperty); }
        }

        public PageCanvas.eZoomMode ZoomMode
        {
            set { SetValue(ZoomModeProperty, value); }
            get { return (PageCanvas.eZoomMode)GetValue(ZoomModeProperty); }
        }

        public PageCanvas.eMouseMode MouseMode
        {
            set { SetValue(MouseModeProperty, value); }
            get { return (PageCanvas.eMouseMode)GetValue(MouseModeProperty); }
        }

        public bool Animations
        {
            set { SetValue(AnimationsProperty, value); }
            get { return (bool)GetValue(AnimationsProperty); }
        }
        public bool LockContent
        {
            set { SetValue(LockContentProperty, value); }
            get { return (bool)GetValue(LockContentProperty); }
        }

        //Zoom 될 때 마우스 포인터 위치 Lock 설정.
        public bool LockZoomPoint
        {
            set { SetValue(LockZoomPointProperty, value); }
            get { return (bool)GetValue(LockZoomPointProperty); }
        }

        #endregion

        #region RoutedEvent

        public static readonly RoutedEvent OutMouseWheelEvent = EventManager.RegisterRoutedEvent("OutMouseWheel", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(PageCanvas));
        public static readonly RoutedEvent InsertControlContentEvent = EventManager.RegisterRoutedEvent("InsertControlContent", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(PageCanvas));
        public static readonly RoutedEvent ChangedContentControlEvent = EventManager.RegisterRoutedEvent("ChangedContentControl", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(PageCanvas));

        // Provide CLR accessors for the event
        public event RoutedEventHandler OutMouseWheel
        {
            add { AddHandler(OutMouseWheelEvent, value); }
            remove { RemoveHandler(OutMouseWheelEvent, value); }
        }

        public event RoutedEventHandler InsertControlContent
        {
            add { AddHandler(InsertControlContentEvent, value); }
            remove { RemoveHandler(InsertControlContentEvent, value); }
        }

        public event RoutedEventHandler ChangedContentControl
        {
            add { AddHandler(ChangedContentControlEvent, value); }
            remove { RemoveHandler(ChangedContentControlEvent, value); }
        }

        private void RaiseMouseWheelEvent(MouseWheelEventArgs e)
        {
            RoutedEventArgs newEventArgs = new RoutedEventArgs(PageCanvas.OutMouseWheelEvent, e);
            RaiseEvent(newEventArgs);
        }

        public void RaiseInsertControlContentEvent(object item)
        {
            RoutedEventArgs eventArgs = new RoutedEventArgs(PageCanvas.InsertControlContentEvent, item);
            RaiseEvent(eventArgs);
        }

        public void RaiseChangedContentControlEvent(object item)
        {
            RoutedEventArgs eventArgs = new RoutedEventArgs(PageCanvas.ChangedContentControlEvent, item);
            RaiseEvent(eventArgs);
        }

        #endregion

        #region Delegates



        public void DoMouseWheelHandler(MouseWheelEventArgs e)
        {
            OnPreviewMouseWheel(e);
        }

        private void OnResizePanel()
        {
            this.ResizePanel();
            this.UpdateLayout();
        }

        #endregion

        #region Public Properties

        public bool IsZoomMode_CustomSize { set { if (value) ZoomMode = eZoomMode.CustomSize; } get { return ZoomMode == eZoomMode.CustomSize; } }
        public bool IsZoomMode_ActualSize { set { if (value) ZoomMode = eZoomMode.ActualSize; } get { return ZoomMode == eZoomMode.ActualSize; } }
        public bool IsZoomMode_FitWidth { set { if (value) ZoomMode = eZoomMode.FitWidth; } get { return ZoomMode == eZoomMode.FitWidth; } }
        public bool IsZoomMode_FitHeight { set { if (value) ZoomMode = eZoomMode.FitHeight; } get { return ZoomMode == eZoomMode.FitHeight; } }
        public bool IsZoomMode_FitPage { set { if (value) ZoomMode = eZoomMode.FitPage; } get { return ZoomMode == eZoomMode.FitPage; } }
        public bool IsZoomMode_FitVisible { set { if (value) ZoomMode = eZoomMode.FitVisible; } get { return ZoomMode == eZoomMode.FitVisible; } }

        public bool IsMouseMode_None { set { if (value) MouseMode = eMouseMode.None; } get { return MouseMode == eMouseMode.None; } }
        public bool IsMouseMode_Zoom { set { if (value) MouseMode = eMouseMode.Zoom; } get { return MouseMode == eMouseMode.Zoom; } }
        public bool IsMouseMode_Pan { set { if (value) MouseMode = eMouseMode.Pan; } get { return MouseMode == eMouseMode.Pan; } }

        public bool IsWheelMode_None
        {
            set { if (value) WheelMode = eWheelMode.None; }
            get { return WheelMode == eWheelMode.None; }
        }
        public bool IsWheelMode_Zoom
        {
            set { if (value) WheelMode = eWheelMode.Zoom; }
            get { return WheelMode == eWheelMode.Zoom; }
        }
        public bool IsWheelMode_Scroll
        {
            set { if (value) WheelMode = eWheelMode.Scroll; }
            get { return WheelMode == eWheelMode.Scroll; }
        }
        public bool IsWheelMode_Rotate
        {
            set { if (value) WheelMode = eWheelMode.Rotate; }
            get { return WheelMode == eWheelMode.Rotate; }
        }

        public bool IsMouseIn { get; set; }

        #endregion

        #region Member variables
        private Size childSize;

        private TranslateTransform translateTransform;
        private RotateTransform rotateTransform;
        private ScaleTransform zoomTransform;
        private TransformGroup transformGroup;

        private double panX, panY;
        private bool zoomCircularReference = false;

        private double rotateAngle, rotateCenterX, rotateCenterY;

        private int minMouseDelta = 1000000;
        private double scrollDelta = 25;
        private Point startMouseCapturePanel;
        private Cursor prevCursor = null;
        private double animationDuration = 300;


        #endregion

        #region Properties

        protected double ZoomFactor
        {
            get { return Zoom / 100; }
            set { Zoom = value * 100; }
        }

        #endregion

        #region Command Handler

        public static RoutedUICommand rotateClockwiseCommand;
        public static RoutedUICommand RotateClockwise
        {
            get { return rotateClockwiseCommand; }
        }

        public static RoutedUICommand rotateCounterclockwiseCommand;
        public static RoutedUICommand RotateCounterclockwise
        {
            get { return rotateCounterclockwiseCommand; }
        }


        public static RoutedUICommand rotateHomeCommand;
        public static RoutedUICommand RotateHome
        {
            get { return rotateHomeCommand; }
        }
        public static RoutedUICommand rotateReverseCommand;
        public static RoutedUICommand RotateReverse
        {
            get { return rotateReverseCommand; }
        }

        private static void CanExecuteEventHandler_IfHasContent(Object sender, CanExecuteRoutedEventArgs e)
        {
            PageCanvas z = sender as PageCanvas;
            e.CanExecute = ((z != null) && (z.Children.Count > 0));
            e.Handled = true;
        }

        private static void CanExecuteEventHandler_IfCanIncreaseZoom(Object sender, CanExecuteRoutedEventArgs e)
        {
            PageCanvas z = sender as PageCanvas;
            e.CanExecute = ((z != null) && (z.CanIncreaseZoom));
            e.Handled = true;
        }
        private static void CanExecuteEventHandler_IfCanDecreaseZoom(Object sender, CanExecuteRoutedEventArgs e)
        {
            PageCanvas z = sender as PageCanvas;
            e.CanExecute = ((z != null) && (z.CanDecreaseZoom));
            e.Handled = true;
        }

        public static void ExecutedEventHandler_IncreaseZoom(Object sender, ExecutedRoutedEventArgs e)
        {
            PageCanvas z = sender as PageCanvas;
            if (z != null) z.process_ZoomCommand(true);
        }
        public static void ExecutedEventHandler_DecreaseZoom(Object sender, ExecutedRoutedEventArgs e)
        {
            PageCanvas z = sender as PageCanvas;
            if (z != null) z.process_ZoomCommand(false);
        }
        public static void ExecutedEventHandler_RotateClockwise(Object sender, ExecutedRoutedEventArgs e)
        {
            PageCanvas z = sender as PageCanvas;
            double? param = null;
            string p = e.Parameter as string;
            if ((p != null) && (p.Length > 0))
                param = Double.Parse(p);
            if (z != null) z.process_RotateCommand(true, param);
        }
        public static void ExecutedEventHandler_RotateCounterclockwise(Object sender, ExecutedRoutedEventArgs e)
        {
            PageCanvas z = sender as PageCanvas;
            double? param = null;
            string p = e.Parameter as string;
            if ((p != null) && (p.Length > 0))
                param = Double.Parse(p);
            if (z != null) z.process_RotateCommand(false, param);
        }

        public static void ExecutedEventHandler_RotateHome(Object sender, ExecutedRoutedEventArgs e)
        {
            PageCanvas z = sender as PageCanvas;
            if (z != null) z.process_RotateHomeReverse(true);
        }
        public static void ExecutedEventHandler_RotateReverse(Object sender, ExecutedRoutedEventArgs e)
        {
            PageCanvas z = sender as PageCanvas;
            if (z != null) z.process_RotateHomeReverse(false);
        }

        private static void PropertyChanged_AMode(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            PageCanvas z = d as PageCanvas;
            if (z != null)
                z.NotifyPropertyChanged();
        }
        private static void PropertyChanged_Zoom(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            PageCanvas z = d as PageCanvas;
            if (z != null)
                z.process_PropertyChanged_Zoom(e);
        }

        #endregion

        #region public methods

        public void IncreaseZoom()
        {
            process_ZoomCommand(true);
        }
        public void DecreaseZoom()
        {
            process_ZoomCommand(false);
        }

        public void DoRotateClockwise()
        {
            process_RotateCommand(true, null);
        }
        public void DoRotateClockwise(double degrees)
        {
            process_RotateCommand(true, degrees);
        }
        public void DoRotateCounterclockwise()
        {
            process_RotateCommand(false, null);
        }
        public void DoRotateCounterclockwise(double degrees)
        {
            process_RotateCommand(false, degrees);
        }
        public void DoRotateHome()
        {
            process_RotateHomeReverse(true);
        }
        public void DoRotateReverse()
        {
            process_RotateHomeReverse(false);
        }

        public void SetZoomCanvasViewType(eMouseMode mm, eWheelMode wm, eZoomMode zm)
        {
            this.MouseMode = mm;
            this.WheelMode = wm;
            this.ZoomMode = zm;
        }

        #endregion

        #region static methods

        private static void InitStaticMethod()
        {
            // WPF properties
            PaddingProperty = DependencyProperty.Register(
                "Padding", typeof(Thickness), typeof(PageCanvas),
                new FrameworkPropertyMetadata(
                    new Thickness(DEFAULTLEFTRIGHTMARGIN, DEFAULTTOPBOTTOMMARGIN, DEFAULTLEFTRIGHTMARGIN, DEFAULTTOPBOTTOMMARGIN),
                    FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.Journal, null, null),
                null);

            CenterContentProperty = DependencyProperty.Register(
                "CenterContent", typeof(bool), typeof(PageCanvas),
                new FrameworkPropertyMetadata(true,
                    FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.Journal, null, null),
                null);

            MinZoomProperty = DependencyProperty.Register(
                "MinZoom", typeof(double), typeof(PageCanvas),
                new FrameworkPropertyMetadata(1.0,
                    FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.Journal, PropertyChanged_Zoom, CoerceMinZoom),
                new ValidateValueCallback(PageCanvas.ValidateIsPositiveNonZero));
            MaxZoomProperty = DependencyProperty.Register(
                "MaxZoom", typeof(double), typeof(PageCanvas),
                new FrameworkPropertyMetadata(1000.0,
                    FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.Journal, PropertyChanged_Zoom, CoerceMaxZoom),
                new ValidateValueCallback(PageCanvas.ValidateIsPositiveNonZero));
            ZoomProperty = DependencyProperty.Register(
                "Zoom", typeof(double), typeof(PageCanvas),
                new FrameworkPropertyMetadata(100.0,
                    FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.Journal, PropertyChanged_Zoom, CoerceZoom),
                new ValidateValueCallback(PageCanvas.ValidateIsPositiveNonZero));

            CanIncreaseZoomPropertyKey = DependencyProperty.RegisterReadOnly(
                "CanIncreaseZoom", typeof(bool), typeof(PageCanvas),
                 new FrameworkPropertyMetadata(false, null, null));

            CanDecreaseZoomPropertyKey = DependencyProperty.RegisterReadOnly(
                "CanDecreaseZoom", typeof(bool), typeof(PageCanvas),
                 new FrameworkPropertyMetadata(false, null, null));

            CanRotateKey = DependencyProperty.RegisterReadOnly(
                "CanRotate", typeof(bool), typeof(PageCanvas),
                 new FrameworkPropertyMetadata(true, null, null));

            ZoomTickProperty = DependencyProperty.Register(
                "ZoomTick", typeof(double), typeof(PageCanvas),
                new FrameworkPropertyMetadata(50.0,
                    FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.Journal, PropertyChanged_Zoom, CoerceZoomTick),
                null);
            MinZoomTickProperty = DependencyProperty.Register(
                "MinZoomTick", typeof(double), typeof(PageCanvas),
                new FrameworkPropertyMetadata(0.0,
                    FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.Journal, PropertyChanged_Zoom, CoerceMinZoomTick),
                null);
            MaxZoomTickProperty = DependencyProperty.Register(
                "MaxZoomTick", typeof(double), typeof(PageCanvas),
                new FrameworkPropertyMetadata(100.0,
                    FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.Journal, PropertyChanged_Zoom, CoerceMaxZoomTick),
                null);

            ZoomIncrementProperty = DependencyProperty.Register(
                "ZoomIncrement", typeof(double), typeof(PageCanvas),
                new FrameworkPropertyMetadata(20.0),
                new ValidateValueCallback(PageCanvas.ValidateIsPositiveNonZero));

            RotateIncrementProperty = DependencyProperty.Register(
                "RotateIncrement", typeof(double), typeof(PageCanvas),
                new FrameworkPropertyMetadata(15.0, null, CoerceRotateIncrement),
                new ValidateValueCallback(PageCanvas.ValidateIsPositiveNonZero));

            WheelZoomDeltaProperty = DependencyProperty.Register(
                "WheelZoomDelta", typeof(double), typeof(PageCanvas),
                new FrameworkPropertyMetadata(10.0),
                new ValidateValueCallback(PageCanvas.ValidateIsPositiveNonZero));

            WheelModeProperty = DependencyProperty.Register(
                "WheelMode", typeof(PageCanvas.eWheelMode), typeof(PageCanvas),
                new FrameworkPropertyMetadata(PageCanvas.eWheelMode.Zoom, PropertyChanged_AMode),
                null);

            ZoomModeProperty = DependencyProperty.Register(
                "ZoomMode", typeof(PageCanvas.eZoomMode), typeof(PageCanvas),
                new FrameworkPropertyMetadata(PageCanvas.eZoomMode.FitPage,
                 FrameworkPropertyMetadataOptions.AffectsRender | FrameworkPropertyMetadataOptions.Journal,
                 PropertyChanged_AMode, null),
                null);

            MouseModeProperty = DependencyProperty.Register(
                "MouseMode", typeof(PageCanvas.eMouseMode), typeof(PageCanvas),
                new FrameworkPropertyMetadata(PageCanvas.eMouseMode.None, PropertyChanged_AMode),
                null);


            AnimationsProperty = DependencyProperty.Register(
                "Animations", typeof(bool), typeof(PageCanvas),
                new FrameworkPropertyMetadata(true, null),
                null);

            LockContentProperty = DependencyProperty.Register(
                "LockContent", typeof(bool), typeof(PageCanvas),
                new FrameworkPropertyMetadata(false, null),
                null);

            LockZoomPointProperty = DependencyProperty.Register("LockZoomPoint", typeof(bool), typeof(PageCanvas), new FrameworkPropertyMetadata((bool)true));

            AllowOutWheelActionProperty = DependencyProperty.Register("AllowOutWheelAction", typeof(bool), typeof(PageCanvas), new FrameworkPropertyMetadata((bool)true));

            //-----------------------------------------------------------------
            // Commands
            CommandManager.RegisterClassCommandBinding(typeof(PageCanvas),
                new CommandBinding(NavigationCommands.IncreaseZoom,
                    ExecutedEventHandler_IncreaseZoom, CanExecuteEventHandler_IfCanIncreaseZoom));

            CommandManager.RegisterClassCommandBinding(typeof(PageCanvas),
                new CommandBinding(NavigationCommands.DecreaseZoom,
                    ExecutedEventHandler_DecreaseZoom, CanExecuteEventHandler_IfCanDecreaseZoom));

            InputGestureCollection rotateInputGestures = new InputGestureCollection();
            rotateInputGestures.Add(new KeyGesture(Key.R, ModifierKeys.Control));
            rotateClockwiseCommand = new RoutedUICommand("Rotate Clockwise", "RotateClockwise", typeof(PageCanvas), rotateInputGestures);
            CommandManager.RegisterClassCommandBinding(typeof(PageCanvas),
                new CommandBinding(rotateClockwiseCommand,
                    ExecutedEventHandler_RotateClockwise, CanExecuteEventHandler_IfHasContent));

            InputGestureCollection rotateCounterInputGestures = new InputGestureCollection();
            rotateCounterInputGestures.Add(new KeyGesture(Key.R, ModifierKeys.Alt));
            rotateCounterclockwiseCommand = new RoutedUICommand("Rotate Counterclockwise", "RotateCounterclockwise", typeof(PageCanvas), rotateCounterInputGestures);
            CommandManager.RegisterClassCommandBinding(typeof(PageCanvas),
                new CommandBinding(rotateCounterclockwiseCommand,
                    ExecutedEventHandler_RotateCounterclockwise, CanExecuteEventHandler_IfHasContent));

            InputGestureCollection rotateHomeInputGestures = new InputGestureCollection();
            rotateHomeInputGestures.Add(new KeyGesture(Key.Home, ModifierKeys.None));
            rotateHomeCommand = new RoutedUICommand("Rotate Home", "RotateHome", typeof(PageCanvas), rotateHomeInputGestures);
            CommandManager.RegisterClassCommandBinding(typeof(PageCanvas),
                new CommandBinding(rotateHomeCommand,
                    ExecutedEventHandler_RotateHome, CanExecuteEventHandler_IfHasContent));

            InputGestureCollection rotateReverseInputGestures = new InputGestureCollection();
            rotateReverseInputGestures.Add(new KeyGesture(Key.End, ModifierKeys.None));
            rotateReverseCommand = new RoutedUICommand("Rotate Reverse", "RotateReverse", typeof(PageCanvas), rotateReverseInputGestures);
            CommandManager.RegisterClassCommandBinding(typeof(PageCanvas),
                new CommandBinding(rotateReverseCommand,
                    ExecutedEventHandler_RotateReverse, CanExecuteEventHandler_IfHasContent));

            //-----------------------------------------------------------------
        }

        private static bool ValidateIsPositiveNonZero(object value)
        {
            double v = (double)value;
            return (v > 0.0) ? true : false;
        }
        private static object CoerceRotateIncrement(DependencyObject d, object value)
        {
            double dv = (double)value;
            if (dv <= 0) dv = 1;
            else if (dv > 359) dv = 359;
            return dv;
        }
        private static object CoerceZoom(DependencyObject d, object value)
        {
            double dv = (double)value;
            PageCanvas z = d as PageCanvas;
            if (z != null)
            {
                if (dv > z.MaxZoom)
                    dv = z.MaxZoom;
                else if (dv < z.MinZoom)
                    dv = z.MinZoom;
            }
            return dv;
        }
        private static object CoerceMinZoom(DependencyObject d, object value)
        {
            double dv = (double)value;
            PageCanvas z = d as PageCanvas;
            if (z != null)
            {
                if (dv <= MINZOOM)
                    dv = MINZOOM;
                if (dv > z.MaxZoom)
                    z.MaxZoom = dv;
                if (z.Zoom < dv)
                    z.Zoom = dv; ;
            }
            return dv;
        }
        private static object CoerceMaxZoom(DependencyObject d, object value)
        {
            double dv = (double)value;
            PageCanvas z = d as PageCanvas;
            if (z != null)
            {
                if (dv < z.MinZoom)
                    z.MinZoom = dv;
                if (z.Zoom > dv)
                    z.Zoom = dv; ;
            }
            return dv;
        }

        private static object CoerceZoomTick(DependencyObject d, object value)
        {
            double dv = (double)value;
            PageCanvas z = d as PageCanvas;
            if (z != null)
            {
                if (dv > z.MaxZoomTick)
                    dv = z.MaxZoomTick;
                else if (dv < z.MinZoomTick)
                    dv = z.MinZoomTick;
            }
            return dv;
        }

        private static object CoerceMinZoomTick(DependencyObject d, object value)
        {
            double dv = (double)value;
            PageCanvas z = d as PageCanvas;
            if (z != null)
            {
                if (dv >= z.MaxZoomTick)
                    z.MaxZoomTick = dv + 1;
            }
            return dv;
        }
        private static object CoerceMaxZoomTick(DependencyObject d, object value)
        {
            double dv = (double)value;
            PageCanvas z = d as PageCanvas;
            if (z != null)
            {
                if (dv <= z.MinZoomTick)
                    z.MinZoomTick = dv - 1;
            }
            return dv;
        }

        #endregion

        protected override void OnInitialized(EventArgs e)
        {
            base.OnInitialized(e);

            this.PreviewMouseDown += process_PreviewMouseDown;

            ApplyZoom(false);

            this.LayoutTransform = this.transformGroup;//하위 자식 element 모두 Transform한다.
            this.RenderTransform = this.translateTransform;
            /*
            this.RenderTransform = this.zoomTransform;
            
            foreach (UIElement element in base.InternalChildren)
            {
                element.RenderTransform = this.transformGroup;
            }
            */
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            Focusable = true;
        }

        protected override Size MeasureOverride(Size constraint)
        {
            //Size panelSize = new Size(150, 150);
            //if ((!Double.IsNaN(constraint.Width)) && (!Double.IsInfinity(constraint.Width)))
            //    panelSize.Width = constraint.Width;
            //if ((!Double.IsNaN(constraint.Height)) && (!Double.IsInfinity(constraint.Height)))
            //    panelSize.Height = constraint.Height;

            Size measureSize = new Size(constraint.Width / ZoomFactor, constraint.Height / ZoomFactor);
            Size panelSize = base.MeasureOverride(measureSize);

            childSize = new Size(1, 1);
            Size infSize = new Size(double.PositiveInfinity, double.PositiveInfinity);

            foreach (UIElement element in base.InternalChildren)
            {
                element.Measure(infSize);

                if (element.DesiredSize.Width > childSize.Width)
                    childSize.Width = element.DesiredSize.Width;

                if (element.DesiredSize.Height > childSize.Height)
                    childSize.Height = element.DesiredSize.Height;
            }

            return panelSize;
        }

        protected override Size ArrangeOverride(Size panelRect)
        {
            foreach (UIElement element in base.InternalChildren)
            {
                element.Arrange(new Rect(Canvas.GetLeft(element), Canvas.GetTop(element), element.DesiredSize.Width, element.DesiredSize.Height));
            }

            RecalcPage(panelRect);

            return panelRect;
        }

        // called on panel resize, zoom mode change
        protected void RecalcPage(Size panelRect)
        {
            double desiredW = 0, desiredH = 0;
            double zoomX = 0, zoomY = 0;

            double minDimension = 5;

            switch (ZoomMode)
            {
                case eZoomMode.CustomSize:
                    break;
                case eZoomMode.ActualSize:
                    ZoomFactor = 1.0;
                    panX = CalcCenterOffset(panelRect.Width, childSize.Width, Padding.Left);
                    panY = CalcCenterOffset(panelRect.Height, childSize.Height, Padding.Top);
                    ApplyZoom(false);
                    break;

                case eZoomMode.FitWidth:
                    desiredW = panelRect.Width - Padding.Left - Padding.Right;
                    if (desiredW < minDimension) desiredW = minDimension;
                    zoomX = desiredW / childSize.Width;

                    ZoomFactor = zoomX;
                    panX = Padding.Left;
                    panY = CalcCenterOffset(panelRect.Height, childSize.Height, Padding.Top);

                    ApplyZoom(false);
                    break;
                case eZoomMode.FitHeight:
                    desiredH = panelRect.Height - Padding.Top - Padding.Bottom;
                    if (desiredH < minDimension) desiredH = minDimension;
                    zoomY = desiredH / childSize.Height;

                    ZoomFactor = zoomY;
                    panX = CalcCenterOffset(panelRect.Width, childSize.Width, Padding.Left);
                    panY = Padding.Top;

                    ApplyZoom(false);
                    break;
                case eZoomMode.FitPage:
                    desiredW = panelRect.Width - Padding.Left - Padding.Right;
                    if (desiredW < minDimension) desiredW = minDimension;
                    zoomX = desiredW / childSize.Width;
                    desiredH = panelRect.Height - Padding.Top - Padding.Bottom;
                    if (desiredH < minDimension) desiredH = minDimension;
                    zoomY = desiredH / childSize.Height;

                    if (zoomX <= zoomY)
                    {
                        ZoomFactor = zoomX;
                        panX = Padding.Left;
                        panY = CalcCenterOffset(panelRect.Height, childSize.Height, Padding.Top);
                    }
                    else
                    {
                        ZoomFactor = zoomY;
                        panX = CalcCenterOffset(panelRect.Width, childSize.Width, Padding.Left);
                        panY = Padding.Top;
                    }
                    ApplyZoom(false);
                    break;
                case eZoomMode.FitVisible:
                    desiredW = panelRect.Width - Padding.Left - Padding.Right;
                    if (desiredW < minDimension) desiredW = minDimension;
                    zoomX = desiredW / childSize.Width;
                    desiredH = panelRect.Height - Padding.Top - Padding.Bottom;
                    if (desiredH < minDimension) desiredH = minDimension;
                    zoomY = desiredH / childSize.Height;

                    if (zoomX >= zoomY)
                    {
                        ZoomFactor = zoomX;
                        panX = Padding.Left;
                        panY = CalcCenterOffset(panelRect.Height, childSize.Height, Padding.Top);
                    }
                    else
                    {
                        ZoomFactor = zoomY;
                        panX = CalcCenterOffset(panelRect.Width, childSize.Width, Padding.Left);
                        panY = Padding.Top;
                    }
                    ApplyZoom(false);
                    break;
            }
        }


        protected double CalcCenterOffset(double parent, double child, double margin)
        {
            if (CenterContent)
            {
                double offset = 0;
                offset = (parent - (child * ZoomFactor)) / 2;
                if (offset > margin)
                    return offset;
            }
            return margin;
        }

        protected void ApplyZoomCommand(double delta, int factor, Point panelPoint)
        {
            if (factor > 0)
            {
                double zoom = ZoomFactor;

                for (int i = 1; i <= factor; i++)
                    zoom = zoom * delta;

                if (zoom <= MINZOOM)
                    return;

                ZoomFactor = zoom;

                ApplyZoomCommand(panelPoint);
            }
        }

        protected void ApplyZoomCommand(Point panelPoint)
        {
            Point logicalPoint = transformGroup.Inverse.Transform(panelPoint);

            ZoomMode = eZoomMode.CustomSize;

            panX = -1 * (logicalPoint.X * ZoomFactor - panelPoint.X);
            panY = -1 * (logicalPoint.Y * ZoomFactor - panelPoint.Y);

            ApplyZoom(false);
        }

        protected void ApplyRotateCommand(double delta, int factor, Point panelPoint)
        {
            if (factor > 0)
            {
                rotateAngle = (rotateAngle + (delta * factor)) % 360;
                if (rotateAngle < 0)
                    rotateAngle += 360;

                ZoomMode = eZoomMode.CustomSize;

                rotateCenterX = childSize.Width / 2;
                rotateCenterY = childSize.Height / 2;

                ApplyZoom(true);
            }
        }

        protected void ApplyScrollCommand(double delta, int factor, Point panelPoint)
        {
            if (factor > 0)
            {
                double deltaY = delta * factor;

                if (ApplyPanDelta(0, deltaY))
                    ApplyZoom(true);
            }
        }

        public void ApplyZoom(double value, Size scrollViewerSize, bool isFitPage)
        {
            this._isFitPage = isFitPage;
            if (isFitPage == true)
            {
                if (double.IsNaN(this.Width) == true || double.IsNaN(this.Height) == true)
                    return;
                double desiredW = 0, desiredH = 0;
                double zoomX = 0, zoomY = 0;
                double minDimension = 5;
                Grid grid = this.Parent as Grid;

                double height = this.Height; double width = this.Width;
                List<FrameworkElement> feList = new List<FrameworkElement>();
                foreach (UIElement fe in this.Children)
                {
                    if (InSysBasicControls.InSysControlManager.GetControlType(fe.GetType().Name) == InSysControlType.None)
                        continue;

                    feList.Add(fe as FrameworkElement);
                }

                if (feList != null && feList.Count > 0)
                {

                    double minHeight = feList.Select(o => Canvas.GetTop(o)).Max();
                    double maxHeight = feList.Select(o => Canvas.GetTop(o) + o.Height).Min();

                    if (double.IsNaN(maxHeight) == true || double.IsNaN(minHeight) == true)
                        return;

                    if (maxHeight >= 0 && maxHeight <= this.Height)
                        maxHeight = 0;

                    if (minHeight >= 0 && minHeight <= this.Height)
                        minHeight = 0;

                    if (Math.Abs(maxHeight) > Math.Abs(minHeight))
                    {
                        height = Math.Abs(maxHeight) + this.Height;
                    }
                    else
                    {
                        height = Math.Abs(minHeight) + this.Height;
                    }

                    double minWidth = feList.Select(o => Canvas.GetLeft(o)).Max();
                    double maxWidth = feList.Select(o => Canvas.GetLeft(o) + o.Width).Min();

                    if (maxWidth >= 0 && maxWidth <= this.Width)
                        maxWidth = 0;

                    if (minWidth >= 0 && minWidth <= this.Width)
                        minWidth = 0;


                    if (Math.Abs(maxWidth) > Math.Abs(minWidth))
                    {
                        width = Math.Abs(maxWidth) + this.Width;
                    }
                    else
                    {
                        width = Math.Abs(minWidth) + this.Width;
                    }
                }

                desiredW = width;// -this.Margin.Left - this.Margin.Right - this.Padding.Left - this.Padding.Right;
                if (desiredW < minDimension)
                    desiredW = minDimension;
                zoomX = scrollViewerSize.Width / desiredW;

                desiredH = height;// -this.Margin.Top - this.Margin.Bottom - this.Padding.Top - this.Padding.Bottom;
                if (desiredH < minDimension)
                    desiredH = minDimension;
                zoomY = scrollViewerSize.Height / desiredH;

                if (zoomX <= zoomY)
                {
                    ZoomFactor = zoomX;
                }
                else
                {
                    ZoomFactor = zoomY;
                }

                this.panX = 0;
                this.panY = 0;
            }
            else
            {
                ZoomFactor = value;
            }
            ApplyZoom(true);
        }

        protected void ApplyZoom(bool animate)
        {
            if ((!animate) || (!Animations))
            {
                translateTransform.BeginAnimation(TranslateTransform.XProperty, null);
                translateTransform.BeginAnimation(TranslateTransform.YProperty, null);

                zoomTransform.BeginAnimation(ScaleTransform.ScaleXProperty, null);
                zoomTransform.BeginAnimation(ScaleTransform.ScaleYProperty, null);

                rotateTransform.BeginAnimation(RotateTransform.AngleProperty, null);

                //thinkblue_20100701: 무한 스크롤되는 현상으로 주석 처리
                /*translateTransform.X = panX; 
                translateTransform.Y = panY;*/

                zoomTransform.ScaleX = ZoomFactor;
                zoomTransform.ScaleY = ZoomFactor;

                rotateTransform.Angle = rotateAngle;
                rotateTransform.CenterX = rotateCenterX;
                rotateTransform.CenterY = rotateCenterY;
            }
            else
            {
                DoubleAnimation XPropertyAnimation = MakeZoomAnimation(panX);
                DoubleAnimation YPropertyAnimation = MakeZoomAnimation(panY);
                DoubleAnimation ScaleXPropertyAnimation = MakeZoomAnimation(ZoomFactor);
                DoubleAnimation ScaleYPropertyAnimation = MakeZoomAnimation(ZoomFactor);
                DoubleAnimation AngleAnimation = MakeRotateAnimation(rotateTransform.Angle, rotateAngle);

                if (LockZoomPoint != true)//사용자 마우스 위치에 따라 Zoom 위치변경
                {
                    //thinkblue_20100701: 무한 스크롤되는 현상으로 주석 처리
                    /*translateTransform.BeginAnimation(TranslateTransform.XProperty, XPropertyAnimation);
                    translateTransform.BeginAnimation(TranslateTransform.YProperty, YPropertyAnimation);*/
                }

                zoomTransform.BeginAnimation(ScaleTransform.ScaleXProperty, ScaleXPropertyAnimation);
                zoomTransform.BeginAnimation(ScaleTransform.ScaleYProperty, ScaleYPropertyAnimation);

                rotateTransform.CenterX = rotateCenterX;
                rotateTransform.CenterY = rotateCenterY;
                rotateTransform.BeginAnimation(RotateTransform.AngleProperty, AngleAnimation);

                ResizePanel();
            }
        }


        public void ResizePanel()
        {
            if (this.Parent == null)
                return;
            if (this.ActualHeight == 0.0 || this.ActualWidth == 0.0)
                return;

            Grid grid = this.Parent as Grid;

            Thickness thickness = GetViewRectGapLength();

            Grid parentGrid = this.Parent as Grid;
            double designCanvasLRMargin = 150;
            double designCanvasTBMargin = 100;

            if (this._isFitPage == true)
            {
                designCanvasLRMargin = 50;
                designCanvasTBMargin = 10;
            }
            if (parentGrid != null)
            {
                if (thickness.Left == 0 && thickness.Right == 0 && thickness.Top == 0 && thickness.Bottom == 0)
                {
                    this.Margin = new Thickness(ZoomFactor * thickness.Left + designCanvasLRMargin, ZoomFactor * thickness.Top + designCanvasTBMargin, ZoomFactor * thickness.Right + designCanvasLRMargin, ZoomFactor * thickness.Bottom + designCanvasTBMargin);
                    parentGrid.Margin = new Thickness(1);
                }
                else
                {
                    parentGrid.Margin = new Thickness(ZoomFactor * thickness.Left + designCanvasLRMargin, ZoomFactor * thickness.Top + designCanvasTBMargin, ZoomFactor * thickness.Right + designCanvasLRMargin, ZoomFactor * thickness.Bottom + designCanvasTBMargin);
                    this.Margin = new Thickness(1);
                }
            }

            return;
            //if (thickness.Left == 0 && thickness.Right == 0)
            //    grid.Width = ZoomFactor * this.ActualWidth + ZoomFactor * 50 * 2;
            //else if (thickness.Left > thickness.Right)
            //    grid.Width = ZoomFactor * this.ActualWidth + ZoomFactor * thickness.Left * 2 + 50 * 2;
            //else
            //    grid.Width = ZoomFactor * this.ActualWidth + ZoomFactor * thickness.Right * 2 + 50 * 2;

            //if (thickness.Top == 0 && thickness.Bottom == 0)
            //    grid.Height = ZoomFactor * this.ActualHeight + ZoomFactor * 50 * 2;
            //else if (thickness.Top > thickness.Bottom)
            //    grid.Height = ZoomFactor * this.ActualHeight + ZoomFactor * thickness.Top * 2 + 50 * 2;
            //else
            //    grid.Height = ZoomFactor * this.ActualHeight + ZoomFactor * thickness.Bottom * 2 + 50 * 2;
        }

        private Thickness GetViewRectGapLength()
        {
            Thickness thickness = new Thickness(0);
            if (this.Children.Count == 0)
                return thickness;

            List<double> leftXPositions = new List<double>();
            List<double> rightXPositions = new List<double>();
            List<double> topYPositions = new List<double>();
            List<double> bottomYPositions = new List<double>();

            foreach (var child in this.Children)
            {
                double pos = 0.0;

                pos = Canvas.GetLeft(child as FrameworkElement);
                leftXPositions.Add(pos);

                pos = Canvas.GetLeft(child as FrameworkElement);
                rightXPositions.Add(pos + (child as FrameworkElement).Width);

                pos = Canvas.GetTop(child as FrameworkElement);
                topYPositions.Add(pos);

                pos = Canvas.GetTop(child as FrameworkElement);
                bottomYPositions.Add(pos + (child as FrameworkElement).Height);

            }

            double leftX = leftXPositions.Min();
            if (leftX >= 0 || leftX.Equals(double.NaN) == true)
                leftX = 0;
            leftX = Math.Abs(leftX);

            double rightX = rightXPositions.Max();
            rightX = rightX - this.ActualWidth;
            if (rightX <= 0 || rightX.Equals(double.NaN) == true)
                rightX = 0;

            double topY = topYPositions.Min();
            if (topY >= 0 || topY.Equals(double.NaN) == true)
                topY = 0;
            topY = Math.Abs(topY);

            double bottomY = bottomYPositions.Max();
            bottomY = bottomY - this.ActualHeight;
            if (bottomY <= 0 || bottomY.Equals(double.NaN) == true)
                bottomY = 0;

            return new Thickness(leftX, topY, rightX, bottomY);
        }

        private bool ApplyPanDelta(double deltaX, double deltaY)
        {
            double x = panX + deltaX;
            double y = panY + deltaY;

            switch (ZoomMode)
            {
                case eZoomMode.CustomSize:
                    break;
                case eZoomMode.ActualSize:
                    break;
                case eZoomMode.FitWidth:
                    // disable x move
                    x = panX;
                    break;
                case eZoomMode.FitHeight:
                    // disable x move
                    y = panY;
                    break;
                case eZoomMode.FitPage:
                    x = panX;
                    y = panY;
                    break;
                case eZoomMode.FitVisible:
                    break;
            }


            if ((x != panX) || (y != panY))
            {
                panX = x;
                panY = y;
                return true;
            }
            return false;
        }

        protected DoubleAnimation MakeZoomAnimation(double value)
        {
            DoubleAnimation ani = new DoubleAnimation(value, new Duration(TimeSpan.FromMilliseconds(animationDuration)));
            ani.FillBehavior = FillBehavior.HoldEnd;
            return ani;
        }

        protected double calcStartFromAngle(double from, double to)
        {
            if (from < to)
            {
                if (to - from > 180.0)
                    return from + 360.0;
            }
            else
            {
                if (from - to > 180.0)
                    return from - 360.0;
            }

            return from;
        }
        protected double calcAngleBetweenAngles(double from, double to)
        {
            // angle can go out of range - fix thanks to alinux08
            while (from < 0) from += 360;
            while (to < 0) to += 360;
            while (from >= 360) from -= 360;
            while (to >= 360) to -= 360;
            double a = (from <= to) ? to - from : from - to;
            if (a > 180)
                a = 360 - a;
            return a;
        }

        protected DoubleAnimation MakeRotateAnimation(double from, double to)
        {
            DoubleAnimation ani = new DoubleAnimation();
            ani.FillBehavior = FillBehavior.HoldEnd;
            ani.To = to;
            ani.From = calcStartFromAngle(from, to);
            ani.Duration = new Duration(
                TimeSpan.FromMilliseconds(animationDuration * calcAngleBetweenAngles(from, to) / 30));

            return ani;
        }



        private void process_PropertyChanged_Zoom(DependencyPropertyChangedEventArgs e)
        {
            if (zoomCircularReference) return;
            zoomCircularReference = true;

            if (e.Property == ZoomProperty)
            {
                calcZoomTick();
            }
            else if (e.Property == ZoomTickProperty)
            {
                calcZoomFromTick();
            }

            zoomCircularReference = false;


            bool canIncrease = (Zoom < MaxZoom);
            bool canDecrease = (Zoom > MinZoom);

            if (canIncrease != CanIncreaseZoom)
                this.SetValue(PageCanvas.CanIncreaseZoomPropertyKey, canIncrease);
            if (canDecrease != CanDecreaseZoom)
                this.SetValue(PageCanvas.CanDecreaseZoomPropertyKey, canDecrease);
        }

        private void calcZoomTick()
        {
            double logMin = Math.Log10(MinZoom);
            double logMax = Math.Log10(MaxZoom);
            double logZoom = Math.Log10(Zoom);

            if (logMax <= logMin)
                logMax = logMin + 0.01;

            double perc = (logZoom - logMin) / (logMax - logMin);
            ZoomTick = (perc * (MaxZoomTick - MinZoomTick)) + MinZoomTick;
        }
        private void calcZoomFromTick()
        {
            double logMin = Math.Log10(MinZoom);
            double logMax = Math.Log10(MaxZoom);

            if (logMax <= logMin)
                logMax = logMin + 0.01;

            double perc = (ZoomTick - MinZoomTick) / (MaxZoomTick - MinZoomTick);
            double logZoom = (perc * (logMax - logMin)) + logMin;
            Zoom = Math.Pow(10.0, logZoom);

            Point panelPoint = new Point(ActualWidth / 2, ActualHeight / 2);
            ApplyZoomCommand(panelPoint);
        }

        protected override void OnMouseEnter(MouseEventArgs e)
        {
            base.OnMouseEnter(e);
            IsMouseIn = true;
        }

        protected override void OnMouseLeave(MouseEventArgs e)
        {
            base.OnMouseLeave(e);
            IsMouseIn = false;
        }

        protected override void OnPreviewMouseWheel(MouseWheelEventArgs e)
        {
            base.OnPreviewMouseWheel(e);

            if (IsMouseIn == false)
            {
                if (AllowOutWheelAction == true)
                {
                    OnMouseWheel(e);
                }
            }
        }

        protected override void OnMouseWheel(MouseWheelEventArgs e)
        {
            base.OnMouseWheel(e);

            Point panelPoint = new Point();
            if (IsMouseIn == false && AllowOutWheelAction == true)
                panelPoint = e.GetPosition(this.Parent as UIElement);
            else
                panelPoint = e.GetPosition(this);

            double delta = 0;

            int absDelta = Math.Abs(e.Delta);
            if ((minMouseDelta > absDelta) && (absDelta > 0))
                minMouseDelta = absDelta;
            int factor = absDelta / minMouseDelta;
            if (factor < 1)
                factor = 1;

            switch (WheelMode)
            {
                case eWheelMode.Rotate:
                    {
                        delta = RotateIncrement;
                        if (e.Delta <= 0)
                            delta *= -1;
                        ApplyRotateCommand(delta, factor, panelPoint);
                    }
                    break;

                case eWheelMode.Zoom:
                    {
                        delta = (1 + (WheelZoomDelta / 100));
                        if ((e.Delta <= 0) && (delta != 0))
                            delta = 1 / delta;
                        ApplyZoomCommand(delta, factor, panelPoint);
                    }
                    break;

                case eWheelMode.Scroll:
                    {
                        delta = scrollDelta;
                        if (e.Delta <= 0)
                            delta *= -1;
                        ApplyScrollCommand(delta, factor, panelPoint);
                    }
                    break;
            }
        }

        private void process_RotateCommand(bool clockWise, double? angle)
        {
            Point panelPoint = new Point(ActualWidth / 2, ActualHeight / 2);

            double delta = RotateIncrement;
            if (angle != null)
                delta = (double)angle;
            if (!clockWise)
                delta *= -1;
            ApplyRotateCommand(delta, 1, panelPoint);

        }

        private void process_RotateHomeReverse(bool isHome)
        {
            Point panelPoint = new Point(ActualWidth / 2, ActualHeight / 2);


            rotateAngle = isHome ? 0 : 180;

            rotateCenterX = childSize.Width / 2;
            rotateCenterY = childSize.Height / 2;

            ApplyZoom(true);

        }

        private void process_ZoomCommand(bool increase)
        {
            Point panelPoint = new Point(ActualWidth / 2, ActualHeight / 2);

            double delta = (1 + (ZoomIncrement / 100));
            if (!increase)
                delta = 1 / delta;

            ApplyZoomCommand(delta, 1, panelPoint);
        }


        void process_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            base.OnPreviewMouseDown(e);
            if (LockContent)
                e.Handled = true;
        }

        protected override void OnPreviewMouseUp(MouseButtonEventArgs e)
        {
            base.OnPreviewMouseUp(e);
            if (LockContent)
                e.Handled = true;
        }

        protected override void OnPreviewMouseMove(MouseEventArgs e)
        {
            base.OnPreviewMouseMove(e);

            if (LockContent)
                e.Handled = true;
        }

        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            //RaiseZoomCanvasPropertyEvent(e.OriginalSource);
            base.OnMouseDown(e);
            switch (MouseMode)
            {
                case eMouseMode.Pan:
                    if (e.ChangedButton == MouseButton.Left)
                    {
                        if (((Keyboard.GetKeyStates(Key.LeftAlt) & KeyStates.Down) == 0) && ((Keyboard.GetKeyStates(Key.RightAlt) & KeyStates.Down) == 0))
                        {
                            prevCursor = this.Cursor;
                            if (this.Parent is UIElement)
                                startMouseCapturePanel = e.GetPosition(this.Parent as UIElement);
                            else
                                startMouseCapturePanel = e.GetPosition(this);
                            this.CaptureMouse();
                            this.Cursor = Cursors.ScrollAll;
                        }
                    }
                    break;
            }


        }

        protected override void OnMouseUp(MouseButtonEventArgs e)
        {
            base.OnMouseUp(e);
            switch (MouseMode)
            {
                case eMouseMode.Zoom:
                    {
                        Point panelPoint = e.GetPosition(this);

                        int factor = 0;
                        double delta = (1 + (ZoomIncrement / 100));
                        switch (e.ChangedButton)
                        {
                            case MouseButton.Left:
                                factor = 1;
                                break;
                            case MouseButton.Right:
                                factor = 1;
                                delta = 1 / delta;
                                break;
                        }
                        ApplyZoomCommand(delta, factor, panelPoint);
                    }
                    break;
            }
            if (IsMouseCaptured)
            {
                ReleaseMouseCapture();
                this.Cursor = prevCursor;
                prevCursor = null;
            }
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);

            return;
            Point dcurrentMousePanel = e.GetPosition(this);

            switch (MouseMode)
            {
                case eMouseMode.Zoom:
                    break;

                case eMouseMode.Pan:
                    if (this.IsMouseCaptured)
                    {
                        Point currentMousePanel = new Point();
                        if (this.Parent is UIElement)
                            currentMousePanel = e.GetPosition(this.Parent as UIElement);
                        else
                            currentMousePanel = e.GetPosition(this);

                        double deltaX = currentMousePanel.X - startMouseCapturePanel.X;
                        double deltaY = currentMousePanel.Y - startMouseCapturePanel.Y;

                        if ((deltaX != 0) || (deltaY != 0))
                        {
                            startMouseCapturePanel.X = currentMousePanel.X;
                            startMouseCapturePanel.Y = currentMousePanel.Y;

                            if (ApplyPanDelta(deltaX, deltaY))
                            {
                                ApplyZoom(false);
                            }
                        }
                    }
                    break;
            }

        }

        #region Notifiable
        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(String info)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(info));
        }
        public void NotifyPropertyChanged()
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(null));
        }
        #endregion

        public void AlignLeft(List<UIElement> uielement)
        {
            Point startPoint = new Point(0.0, 0.0);
            Point endPoint = new Point(this.Width, this.Height);
            if (uielement != null && uielement.Count == 1)
            {
                uielement.ForEach(o => Canvas.SetLeft(o, startPoint.X));
            }
            else
            {
                double minValue = uielement.Min(o => Canvas.GetLeft(o));
                uielement.ForEach(o => Canvas.SetLeft(o, minValue));
            }
            
        }

        public void AlignRight(List<UIElement> uielement)
        {
            Point startPoint = new Point(0.0, 0.0);
            Point endPoint = new Point(this.Width, this.Height);
            if (uielement != null && uielement.Count == 1)
            {
                uielement.ForEach(o => Canvas.SetLeft(o, endPoint.X - (o as FrameworkElement).Width));
            }
            else
            {
                double maxValue = uielement.Max(o => Canvas.GetLeft(o));
                uielement.ForEach(o => Canvas.SetLeft(o, maxValue));
            }            
        }

        public void AlignTop(List<UIElement> uielement)
        {
            Point startPoint = new Point(0.0, 0.0);
            Point endPoint = new Point(this.Width, this.Height);

            if (uielement != null && uielement.Count == 1)
            {
                uielement.ForEach(o => Canvas.SetTop(o, startPoint.Y));
            }
            else
            {
                double value = uielement.Min(o => Canvas.GetTop(o));
                uielement.ForEach(o => Canvas.SetTop(o, value));
            }
        }

        public void AlignBottom(List<UIElement> uielement)
        {
            Point startPoint = new Point(0.0, 0.0);
            Point endPoint = new Point(this.Width, this.Height);
            if (uielement != null && uielement.Count == 1)
            {
                uielement.ForEach(o => Canvas.SetTop(o, endPoint.Y - (o as FrameworkElement).Height));
            }
            else
            {
                double value = uielement.Max(o => Canvas.GetTop(o));
                uielement.ForEach(o => Canvas.SetTop(o, value));
            }            
        }

        public void AlignHorizontalCenter(List<UIElement> uielement)
        {
            Point startPoint = new Point(0.0, 0.0);
            Point endPoint = new Point(this.Width, this.Height);
            if (uielement != null && uielement.Count == 1)
            {
                uielement.ForEach(o => Canvas.SetLeft(o, endPoint.X / 2 - (o as FrameworkElement).Width / 2));
            }
            else
            {
                double minvalue = uielement.Min(o => Canvas.GetLeft(o));
                double maxvalue = uielement.Max(o => Canvas.GetLeft(o));
                uielement.ForEach(o => Canvas.SetLeft(o, minvalue + ((maxvalue - minvalue) / 2)));
            }   
            
        }

        public void AlignVerticalCenter(List<UIElement> uielement)
        {
            Point startPoint = new Point(0.0, 0.0);
            Point endPoint = new Point(this.Width, this.Height);
            if (uielement != null && uielement.Count == 1)
            {
                uielement.ForEach(o => Canvas.SetTop(o, endPoint.Y / 2 - (o as FrameworkElement).Height / 2));
            }
            else
            {
                double minvalue = uielement.Min(o => Canvas.GetTop(o));
                double maxvalue = uielement.Max(o => Canvas.GetTop(o));
                uielement.ForEach(o => Canvas.SetTop(o, minvalue + ((maxvalue - minvalue) / 2)));
            }   
            
        }

        public void DistributeHorizontal(List<UIElement> uielement)
        {
            this.AlignLeft(uielement);

            uielement.ForEach(o => (o as FrameworkElement).Width = this.Width);
        }

        public void DistributeVertical(List<UIElement> uielement)
        {
            this.AlignTop(uielement);

            uielement.ForEach(o => (o as FrameworkElement).Height = this.Height);
        }

        public void FixedViewSize(List<UIElement> uielement)
        {
            this.DistributeHorizontal(uielement);
            this.DistributeVertical(uielement);
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);

            RaiseKeyDownEvent(e);
        }

        public void SetBackground(Brush background)
        {
            this.Background = background;
        }

        public void SetBackground(string background)
        {
            if (System.IO.File.Exists(background) == true)
            {
                this.imageBrush.ImageSource = new BitmapImage(new Uri(background, UriKind.Absolute));
            }
            else
                throw new Exception(string.Format("\"[{0}-{1}]{2}\"파일이 존재하지 않습니다.", "PageCanvas.xaml.cs", "SetBackground Method", background)); 
        }
    }
}
