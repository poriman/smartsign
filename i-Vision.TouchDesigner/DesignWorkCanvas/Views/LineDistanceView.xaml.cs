﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Globalization;

namespace DesignWorkCanvas.Views
{
    /// <summary>
    /// Interaction logic for LineDistanceView.xaml
    /// </summary>
    public partial class LineDistanceView : UserControl, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public virtual void OnPropertyChanged(params string[] propertyNames)
        {
            if (PropertyChanged == null)
                return;

            foreach (string propertyName in propertyNames)
            {
                try
                {
                    PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
                }
                catch { }
            }
        }

        public enum DirectonType
        {
            None,
            Left,
            Top,
            Right,
            Bottom
        }

        private Point _viewPoint;
        public Point ViewPoint 
        {
            get { return _viewPoint; }
            set
            {
                _viewPoint = value;
                OnPropertyChanged("ViewPoint");
            }
        }

        private DirectonType _lineDirectonType;
        public DirectonType LineDirectonType
        {
            get 
            {
                return _lineDirectonType;
            }

            set
            {
                _lineDirectonType = value;
                OnPropertyChanged("LineDirectonType");
            }
        }

        private double _length;
        public double Length
        {
            get
            {
                return _length;                
            }

            set
            {
                this._length = value;
                OnPropertyChanged("Length");
            }
        }

        private double _x1Pos;
        public double X1Pos
        {
            get 
            {
                return _x1Pos;
            }

            set
            {
                _x1Pos = value;
                OnPropertyChanged("X1Pos");
            }
        }

        private double _y1Pos;
        public double Y1Pos
        {
            get
            {
                return _y1Pos;
            }

            set
            {
                _y1Pos = value;
                OnPropertyChanged("Y1Pos");
            }
        }

        private double _x2Pos;
        public double X2Pos
        {
            get
            {
                return _x2Pos;
            }
            set
            {
                _x2Pos = value;
                OnPropertyChanged("X2Pos");
            }
        }

        private double _y2Pos;
        public double Y2Pos
        {
            get
            {
                return _y2Pos;
            }
            set
            {
                _y2Pos = value;
                OnPropertyChanged("Y2Pos");
            }
        }

        private double _angle;
        public double Angle
        {
            get
            {                
                return _angle;
            }
            set
            {
                _angle = value;
            }
        }

        public LineDistanceView()
        {
            InitializeComponent();
            this.DataContext = this;
            _lineDirectonType = DirectonType.Left;
        }

        public void DrawingLine(double width, double height)
        {
            if (double.IsNaN(width) == true)
                width = 1;
            if (double.IsNaN(height) == true)
                height = 1;
            if (this._lineDirectonType == DirectonType.Left || this._lineDirectonType == DirectonType.Right)
            {
                this.X1Pos = 0;
                this.Y1Pos = height / 2;
                this.X2Pos = width;
                this.Y2Pos = height / 2;
                this.Length = this.Width;
            }
            else
            {
                this.X1Pos = width / 2;
                this.Y1Pos = 0;
                this.X2Pos = width / 2;
                this.Y2Pos = height;
                this.Length = this.Height;
            }

        }
    }

    public class LengthValueConverter : IValueConverter
    {
        #region IValueConverter Members
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                double length = (double)value;

                string retValue = string.Format("{0:F0}", length);

                return retValue;
            }

            return string.Empty;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion
    }
}
