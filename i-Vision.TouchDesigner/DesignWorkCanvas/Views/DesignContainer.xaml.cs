﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using InSysTouchflowData.Models.ProjectModels;
using System.Windows.Media.Media3D;
using InSysTouchflowData;
using InSysBasicControls.Interfaces;
using InSysBasicControls;
using InSysTouchflowData.Events;
using System.Globalization;
using DesignWorkCanvas.Cultures;
using System.Threading;

namespace DesignWorkCanvas.Views
{
    public partial class DesignContainer : UserControl
    {
		private NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();// NLog.LogManager.GetLogger("TouchDesignerLog");

        public SelectedInSysControlItemDelegate SelectedInSysControlItemHandler
        {
            get;
            set;
        }

        #region 변수

        private Dictionary<UIElement, List<LineDistanceView>> _elementDistanceInfo = new Dictionary<UIElement, List<LineDistanceView>>();

        // True if a drag operation is underway, else false.
        private bool isDragInProgress;

        public bool IsElementInsertingState { get; set; }

        // Stores a reference to the UIElement currently being dragged by the user.
        private UIElement elementBeingDragged;        

        // Keeps track of where the mouse cursor was when a drag operation began.		
        private Point origCursorLocation;

        // The offsets from the DragCanvas' edges when the drag operation began.
        private double origHorizOffset, origVertOffset;
        // Keeps track of which horizontal and vertical offset should be modified for the drag element.
        private bool modifyLeftOffset, modifyTopOffset;

        private AdornerLayer adornerLayer;

        //Double-Click 여부 체크 변수
        private bool isDoubleClicked = false;

        private bool _isFixedSize = true;

        private bool _isLockSliderValue = false;

        private ZoomType DesignCanvasZoomType = ZoomType.Custom;

        #region RoutedEvent

        public static readonly RoutedEvent InsertContentEvent = EventManager.RegisterRoutedEvent("InsertContent", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(DesignContainer));

        public event RoutedEventHandler InsertContent
        {
            add { AddHandler(InsertContentEvent, value); }
            remove { RemoveHandler(InsertContentEvent, value); }
        }

        public void RaiseInsertContentEvent(object item)
        {
            RoutedEventArgs eventArgs = new RoutedEventArgs(DesignContainer.InsertContentEvent, item);
            RaiseEvent(eventArgs);            
        }

        public static readonly RoutedEvent DeleteContentEvent = EventManager.RegisterRoutedEvent("DeleteContent", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(DesignContainer));

        public event RoutedEventHandler DeleteContent
        {
            add { AddHandler(DeleteContentEvent, value); }
            remove { RemoveHandler(DeleteContentEvent, value); }
        }

        public void RaiseDeleteContentEvent(object item)
        {
            RoutedEventArgs eventArgs = new RoutedEventArgs(DesignContainer.DeleteContentEvent, item);
            RaiseEvent(eventArgs);
        }

        public static readonly RoutedEvent SelectedContentEvent = EventManager.RegisterRoutedEvent("SelectedContent", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(DesignContainer));

        public event RoutedEventHandler SelectedContent
        {
            add { AddHandler(SelectedContentEvent, value); }
            remove { RemoveHandler(SelectedContentEvent, value); }
        }

        public void RaiseSelectedContentEvent(object item)
        {
            RoutedEventArgs eventArgs = new RoutedEventArgs(DesignContainer.SelectedContentEvent, item);
            RaiseEvent(eventArgs);
        }

        public static readonly RoutedEvent SelectedPageEvent = EventManager.RegisterRoutedEvent("SelectedPage", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(DesignContainer));

        public event RoutedEventHandler SelectedPage
        {
            add { AddHandler(SelectedPageEvent, value); }
            remove { RemoveHandler(SelectedPageEvent, value); }
        }

        public void RaiseSelectedPageEvent(object item)
        {
            RoutedEventArgs eventArgs = new RoutedEventArgs(DesignContainer.SelectedPageEvent, item);
            RaiseEvent(eventArgs);
        }

        public static readonly RoutedEvent PageUpdateEvent = EventManager.RegisterRoutedEvent("PageUpdate", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(DesignContainer));

        public event RoutedEventHandler PageUpdate
        {
            add { AddHandler(PageUpdateEvent, value); }
            remove { RemoveHandler(PageUpdateEvent, value); }
        }

        public void RaisePageUpdateEvent(object senderData)
        {
            RoutedEventArgs eventArgs = new RoutedEventArgs(DesignContainer.PageUpdateEvent, senderData);
            RaiseEvent(eventArgs);
        }

        public static readonly RoutedEvent ItemUpdatedPropertyEvent = EventManager.RegisterRoutedEvent("ItemUpdatedProperty", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(DesignContainer));

        public event RoutedEventHandler ItemUpdatedProperty
        {
            add { AddHandler(ItemUpdatedPropertyEvent, value); }
            remove { RemoveHandler(ItemUpdatedPropertyEvent, value); }
        }

        public void RaiseItemUpdatedPropertyEvent(object sender, string propertyName)
        {
            ItemUpdatedPropertyEventArgs eventArgs = new ItemUpdatedPropertyEventArgs(DesignContainer.ItemUpdatedPropertyEvent, sender, propertyName);
            //RoutedEventArgs eventArgs = new RoutedEventArgs(DesignContainer.ItemUpdatedPropertyEvent, senderData);
            RaiseEvent(eventArgs);
        }

        #endregion      

        /// <summary>
        /// Gets/sets whether elements in the DragCanvas should be draggable by the user.
        /// The default value is true.  This is a dependency property.
        /// </summary>
        public bool AllowDragging
        {
            get { return (bool)base.GetValue(AllowDraggingProperty); }
            set { base.SetValue(AllowDraggingProperty, value); }
        }

        /// <summary>
        /// Returns the UIElement currently being dragged, or null.
        /// </summary>
        /// <remarks>
        /// Note to inheritors: This property exposes a protected 
        /// setter which should be used to modify the drag element.
        /// </remarks>
        public UIElement ElementBeingDragged
        {
            get
            {
                if (!this.AllowDragging)
                    return null;
                else
                    return this.elementBeingDragged;
            }
            set
            {
                if (this.elementBeingDragged != null)
                    this.elementBeingDragged.ReleaseMouseCapture();

                if (!this.AllowDragging)
                    this.elementBeingDragged = null;
                else
                {
                    if (DesignContainer.GetCanBeDragged(value))
                    {
                        this.elementBeingDragged = value;
                        this.elementBeingDragged.CaptureMouse();
                    }
                    else
                        this.elementBeingDragged = null;
                }
            }
        }

        /// <summary>
        /// Gets/sets whether the user should be able to drag elements in the DragCanvas out of
        /// the viewable area.  The default value is false.  This is a dependency property.
        /// </summary>
        public bool AllowDragOutOfView
        {
            get { return (bool)GetValue(AllowDragOutOfViewProperty); }
            set { SetValue(AllowDragOutOfViewProperty, value); }
        }

        private UIElement selectedCurrentContentItem;
        public UIElement SelectedCurrentContentItem
        {
            get
            {
                return this.selectedCurrentContentItem;
            }
            set
            {
                //if (SelectedInSysControlItemHandler != null)
                //{
                //    if (value is IDesignElement)
                //    {
                //        SelectedInSysControlItemHandler(value as FrameworkElement);
                //    }
                //}
                this.selectedCurrentContentItem = value;
            }
        }

        public List<UIElement> GetSelectedContentItems()
        {
            return selectedContentItems;
        }

        public void AddSelectedContentItem(UIElement addElement)
        {
            if (selectedContentItems.Contains(addElement) != true)
                selectedContentItems.Add(addElement);
        }

        private List<UIElement> selectedContentItems;
        public List<UIElement> SelectedContentItems
        {
            get
            {
                if (adornerLayer != null)
                {
                    try
                    {
                        foreach (UIElement toAdorn in this.PageCanvas.Children)
                        {
                            InSysBasicControls.Interfaces.ISelectable item = toAdorn as InSysBasicControls.Interfaces.ISelectable;
                            if (item != null && item.IsSelected == true)
                            {
                                if (selectedContentItems.Contains(toAdorn) == true)
                                    continue;
                                selectedContentItems.Add(toAdorn);
                            }
                            else
                            {
                                if (selectedContentItems.Contains(toAdorn) == true)
                                    selectedContentItems.Remove(toAdorn);
                            }
                        }
                    }
                    catch
                    {
                    }
                }
                return selectedContentItems;
            }

            protected set
            {
                selectedContentItems = value;
            }
        }

        private TouchPageInfo _touchPageInfo;
        public TouchPageInfo TouchPageInfo
        {
            get { return _touchPageInfo; }
            private set { _touchPageInfo = value; }
        }

        #endregion

        #region 생성자

        public DesignContainer()
        {
            InitializeComponent();

            this.Loaded += (s, e) =>
            {
                InitStringResource();

                adornerLayer = AdornerLayer.GetAdornerLayer(this);
                SelectedContentItems = new List<UIElement>();

                this.PageCanvas.ResizePanel();
            };

            InSysBasicControls.InSysProperties.ElementPropertyObject.ChangedHeightPropertyCallback = new PropertyChangedDelegate(Changed_HeightProperty);
            InSysBasicControls.InSysProperties.ElementPropertyObject.ChangedWidthPropertyCallback = new PropertyChangedDelegate(Changed_WidthProperty);
            //InSysBasicControls.InSysProperties.ElementPropertyObject.ChangedXPositionPropertyCallback = new PropertyChangedDelegate(Changed_XProperty);
            //InSysBasicControls.InSysProperties.ElementPropertyObject.ChangedYPositionPropertyCallback = new PropertyChangedDelegate(Changed_YProperty);
        }

        static DesignContainer()
        {
            AllowDraggingProperty = DependencyProperty.Register(
               "AllowDragging",
               typeof(bool),
               typeof(DesignContainer),
               new PropertyMetadata(true));

            AllowDragOutOfViewProperty = DependencyProperty.Register(
                "AllowDragOutOfView",
                typeof(bool),
                typeof(DesignContainer),
                new UIPropertyMetadata(true));

            CanBeDraggedProperty = DependencyProperty.RegisterAttached(
                "CanBeDragged",
                typeof(bool),
                typeof(DesignContainer),
                new UIPropertyMetadata(true));
        }
        #endregion
      

        public PageCanvas PageDesignCanvasBox
        {
            get { return this.PageCanvas; }
        }

        public Grid PageAudioBox
        {
            get { return this.AudioBox; }
        }

        #region

        private object Changed_XProperty(object sender, object obj)
        {
            return obj;
        }

        private object Changed_YProperty(object sender, object obj)
        {
            return obj;
        }



        private object Changed_HeightProperty(object sender, DependencyPropertyChangedEventArgs obj)
        {
            double height = (double)obj.NewValue;

            string strValue = height.ToString("####");
            if (string.IsNullOrEmpty(strValue) != true)
            {
                double changedValue = Convert.ToDouble(strValue);
                FrameworkElement frameworkElement = this.SelectedCurrentContentItem as FrameworkElement;
                if (frameworkElement != null)
                {
                    //this.SelectedCurrentContentItem.ResizeElementDistanceInfo(frameworkElement.Width, height, this.PageCanvas, this._elementDistanceInfo);//컨트롤 추가시 라인 오류 발생
                }
            }

            return obj.NewValue;
        }

        private object Changed_WidthProperty(object sender, DependencyPropertyChangedEventArgs obj)
        {
            double width = (double)obj.NewValue;
            FrameworkElement frameworkElement = this.SelectedCurrentContentItem as FrameworkElement;
            if (frameworkElement != null)
            {
                //this.SelectedCurrentContentItem.ResizeElementDistanceInfo(width, frameworkElement.Height, this.PageCanvas, this._elementDistanceInfo);//컨트롤 추가시 라인 오류 발생
            }
            return obj;
        }

        #endregion

        #region Dependency Properties

        public static readonly DependencyProperty AllowDraggingProperty;
        public static readonly DependencyProperty AllowDragOutOfViewProperty;
        public static readonly DependencyProperty CanBeDraggedProperty;

        #endregion // Dependency Properties

        #region static Method

        public static bool GetCanBeDragged(UIElement uiElement)
        {
            if (uiElement == null)
                return false;

            return (bool)uiElement.GetValue(CanBeDraggedProperty);
        }

        #endregion

        #region RoutedEvent

        public static readonly RoutedEvent SendContentSelectedEvent = EventManager.RegisterRoutedEvent("SendContentSelected", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(DesignContainer));

        public event RoutedEventHandler SendContentSelected
        {
            add { AddHandler(SendContentSelectedEvent, value); }
            remove { RemoveHandler(SendContentSelectedEvent, value); }
        }

        #endregion

        #region Overrides Method

        private Point? rubberbandSelectionStartPoint = null;

        private void DesignContainerScrollViewer_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.rubberbandSelectionStartPoint = new Point?(e.GetPosition(this));
        }

        private void DesignContainerScrollViewer_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            //this.rubberbandSelectionStartPoint = null;
        }

        //RubberbandAdorner2 adornerDragSelection;
        private void DesignContainerScrollViewer_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            if (e.LeftButton != MouseButtonState.Pressed)
                this.rubberbandSelectionStartPoint = null;

            if (this.rubberbandSelectionStartPoint != null && this.rubberbandSelectionStartPoint.HasValue)
            {
                // create rubberband adorner
                AdornerLayer adornerLayer = AdornerLayer.GetAdornerLayer(this);
                if (adornerLayer != null)
                {
                    RubberbandAdorner2 adornerDragSelection = new RubberbandAdorner2(this, rubberbandSelectionStartPoint);                        
                    
                    if (adornerDragSelection != null)
                    {
                        adornerLayer.Add(adornerDragSelection);
                    }
                }
            }
        }

        private void PageCanvas_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
        }

        private void PageCanvas_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            selectContentElement(e.Source);           
        }


        private void PageCanvas_PreviewMouseMove(object sender, MouseEventArgs e)
        {
        }

        private void DesignCanvasBar_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            //e.Handled = true;
        }

        /// <summary>
        /// PreviewMouseLeftButtonDown 함수 처리
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPreviewMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            if (this.isDoubleClicked == true)//Double-Click일 경우 
            {
                this.isDoubleClicked = false;//Double-Click 해제.
                return;
            }
            base.OnPreviewMouseLeftButtonDown(e);

            try
            {
                // Walk up the visual tree from the element that was clicked, 
                // looking for an element that is a direct child of the Canvas.
                this.elementBeingDragged = this.FindCanvasChild(e.OriginalSource as DependencyObject);
                if (this.elementBeingDragged == null)
                {
                    this.PageCanvas.ResizePanel();
                    this.PageCanvas.UpdateLayout();
                    releaseAllAdorner();
                    return;
                }
                if (this.selectedContentItems != null)
                {
                    var item = this.selectedContentItems.Where(o => o.Equals(this.elementBeingDragged) == true).FirstOrDefault();
                    if (item == null)
                    {
                        //Press Control key
                        if (Keyboard.Modifiers != ModifierKeys.Control)
                        {
                            releaseAllAdorner();
                        }
                    }

                    //foreach(var tempitems in this.selectedContentItems)
                    //    tempitems.CaptureMouse();
                    this.elementBeingDragged.CaptureMouse();
                }
                else
                {
                    //Press Control key
                    if (Keyboard.Modifiers != ModifierKeys.Control)
                    {
                        releaseAllAdorner();
                    }
                    this.elementBeingDragged.CaptureMouse();
                }

                // Cache the mouse cursor location.
                this.origCursorLocation = e.GetPosition(this.PageCanvas);
                lastLocation = this.origCursorLocation;
                this.isDragInProgress = false;

                // Get the element's offsets from the four sides of the Canvas.
                double left = Canvas.GetLeft(this.elementBeingDragged);
                double right = Canvas.GetRight(this.elementBeingDragged);
                double top = Canvas.GetTop(this.elementBeingDragged);
                double bottom = Canvas.GetBottom(this.elementBeingDragged);

                // Calculate the offset deltas and determine for which sides
                // of the Canvas to adjust the offsets.
                this.origHorizOffset = ResolveOffset(left, right, out this.modifyLeftOffset);
                this.origVertOffset = ResolveOffset(top, bottom, out this.modifyTopOffset);

                // Set the Handled flag so that a control being dragged 
                // does not react to the mouse input.
                //e.Handled = true;

                this.isDragInProgress = true;
                this.selectItemAdorner(this.elementBeingDragged);

                this.SelectedCurrentContentItem = this.elementBeingDragged;
                if (this.selectedContentItems.Contains(this.elementBeingDragged) == false)
                    this.selectedContentItems.Add(this.elementBeingDragged);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message + ">> \r\n" + ex.StackTrace);
            }
        }

        protected override void OnPreviewMouseDoubleClick(MouseButtonEventArgs e)
        {
            base.OnPreviewMouseDoubleClick(e);
            this.isDoubleClicked = true;//Double-Click 했음을 표시.
        }

        Point lastLocation = new Point();
        /// <summary>
        /// OnPreviewMouseMove 함수 처리
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPreviewMouseMove(MouseEventArgs e)
        {
            base.OnPreviewMouseMove(e);

            // If no element is being dragged, there is nothing to do.
            if (!this.isDragInProgress)
                return;

            // Get the position of the mouse cursor, relative to the Canvas.
            Point cursorLocation = e.GetPosition(this.PageCanvas);
            if (this.selectedContentItems == null)
                return;

            if (this.selectedContentItems.Count > 1)
            {
                double x_intval = cursorLocation.X - lastLocation.X;
                double y_intval = cursorLocation.Y - lastLocation.Y;

                foreach (var dragItem in this.selectedContentItems)
                {
                    IElementProperties dlementProperties = dragItem as IElementProperties;
                    #region 속성창의 X, Y 좌표 표시 소수점 이하 자리수로 표시되지 않도록 처리하는 코드
                    
                    if (dlementProperties != null)
                    {                       
                        #region 멀티 선택 컨트롤 Move Drag Element
                        
                        double left = dlementProperties.ElementProperties.X;// Canvas.GetLeft(dragItem);
                        double top = dlementProperties.ElementProperties.Y;//Canvas.GetTop(dragItem);

                        dlementProperties.ElementProperties.X = left + x_intval;//Canvas.SetLeft(dragItem, left + x_intval);
                        dlementProperties.ElementProperties.Y = top + y_intval;//Canvas.SetTop(dragItem, top + y_intval);
                    }

                    #endregion //속성창의 X, Y 좌표 표시 소수점 이하 자리수로 표시되지 않도록 처리하는 코드

                    dragItem.MoveContentElementDistanceLine(this.PageCanvas, this._elementDistanceInfo, false, false);
                   
                    #endregion // Move Drag Element
                }

                lastLocation = cursorLocation;
            }
            else
            {
                if (this.elementBeingDragged == null || !this.isDragInProgress)
                    return;

                if (InSysBasicControls.InSysControlManager.GetControlType(this.elementBeingDragged.GetType().Name) == InSysControlType.None)
                    return;

                #region Mouse Up 된 상태에서 드래그 되는 현상 방지 :컨트롤 Opacity가 0일 경우 외곽선 선택시 MouseUp이벤트가 발생하지 않음
                UIElement selectedControl = this.FindCanvasChild(e.OriginalSource as DependencyObject);
                if (selectedControl == null)
                {
                    this.elementBeingDragged = null;
                    return;
                }
                #endregion

                IDesignElement designeElement = this.elementBeingDragged as IDesignElement;
                if (designeElement != null)
                {
                    if (designeElement.InSysControlType == InSysControlType.InSysAudioBox)
                    {
                        return;
                    }
                }                

                // These values will store the new offsets of the drag element.
                double newHorizontalOffset, newVerticalOffset;

                #region Calculate Offsets

                // Determine the horizontal offset.
                if (this.modifyLeftOffset)
                    newHorizontalOffset = this.origHorizOffset + (cursorLocation.X - this.origCursorLocation.X);
                else
                    newHorizontalOffset = this.origHorizOffset - (cursorLocation.X - this.origCursorLocation.X);

                // Determine the vertical offset.
                if (this.modifyTopOffset)
                    newVerticalOffset = this.origVertOffset + (cursorLocation.Y - this.origCursorLocation.Y);
                else
                    newVerticalOffset = this.origVertOffset - (cursorLocation.Y - this.origCursorLocation.Y);

                #endregion // Calculate Offsets

                if (!this.AllowDragOutOfView)
                {
                    #region Verify Drag Element Location

                    // Get the bounding rect of the drag element.
                    Rect elemRect = this.CalculateDragElementRect(newHorizontalOffset, newVerticalOffset);

                    // If the element is being dragged out of the viewable area, 
                    // determine the ideal rect location, so that the element is 
                    // within the edge(s) of the canvas.
                    bool leftAlign = elemRect.Left < 0;
                    bool rightAlign = elemRect.Right > this.PageCanvas.ActualWidth;

                    if (leftAlign)
                        newHorizontalOffset = modifyLeftOffset ? 0 : this.PageCanvas.ActualWidth - elemRect.Width;
                    else if (rightAlign)
                        newHorizontalOffset = modifyLeftOffset ? this.PageCanvas.ActualWidth - elemRect.Width : 0;

                    bool topAlign = elemRect.Top < 0;
                    bool bottomAlign = elemRect.Bottom > this.PageCanvas.ActualHeight;

                    if (topAlign)
                        newVerticalOffset = modifyTopOffset ? 0 : this.PageCanvas.ActualHeight - elemRect.Height;
                    else if (bottomAlign)
                        newVerticalOffset = modifyTopOffset ? this.PageCanvas.ActualHeight - elemRect.Height : 0;

                    #endregion // Verify Drag Element Location
                }

                #region Move Drag Element

                if (this.modifyLeftOffset)
                    Canvas.SetLeft(this.elementBeingDragged, newHorizontalOffset);
                else
                    Canvas.SetRight(this.elementBeingDragged, newHorizontalOffset);

                if (this.modifyTopOffset)
                    Canvas.SetTop(this.elementBeingDragged, newVerticalOffset);
                else
                    Canvas.SetBottom(this.elementBeingDragged, newVerticalOffset);



                #endregion // Move Drag Element

                #region 속성창의 X, Y 좌표 표시 소수점 이하 자리수로 표시되지 않도록 처리하는 코드

                IElementProperties dlementProperties = this.elementBeingDragged as IElementProperties;
                if (dlementProperties != null)
                {
                    dlementProperties.ElementProperties.X = newHorizontalOffset;
                    dlementProperties.ElementProperties.Y = newVerticalOffset;
                    //this.RaiseUpdatedPropertyEvent(new KeyValuePair<string, object>("X", designeElement.Properties["X"]));
                    //this.RaiseUpdatedPropertyEvent(designeElement.Properties.Where(o => o.Key.Equals("Y") == true).SingleOrDefault());
                }

                #endregion //속성창의 X, Y 좌표 표시 소수점 이하 자리수로 표시되지 않도록 처리하는 코드

                if(cursorLocation.Equals(this.lastLocation) == false)//위치가 변경되었을 때 호출한다.
                    this.elementBeingDragged.MoveContentElementDistanceLine(this.PageCanvas, this._elementDistanceInfo, true, false);
            }

            this.rubberbandSelectionStartPoint = null;//Move Drage시 해제.
        }

        public void MoveContentElementDistanceLine(UIElement contentElement, bool isKeyDown)
        {
            contentElement.MoveContentElementDistanceLine(this.PageCanvas, this._elementDistanceInfo, true, isKeyDown);
        }

        /// <summary>
        /// OnPreviewMouseLeftButtonUp 함수 처리
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPreviewMouseLeftButtonUp(MouseButtonEventArgs e)
        {
            base.OnPreviewMouseLeftButtonUp(e);

            try
            {
                if (this.elementBeingDragged != null)
                {
                    // Reset the field whether the left or right mouse button was 
                    // released, in case a context menu was opened on the drag element.
                    this.elementBeingDragged.ReleaseMouseCapture();
                    this.elementBeingDragged = null;
                }
                this.PageCanvas.ClearLine();//마우스 UP시 컨트롤 간 동일 좌표 표시 UI 없애기 

                this.isDragInProgress = false;

                foreach (var tempitems in this.selectedContentItems)
                    tempitems.ReleaseMouseCapture();
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message + ">> \r\n" + ex.StackTrace);
            }
        }

        protected override void OnMouseLeftButtonUp(MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonUp(e);

            UIElement selectedControl = this.FindCanvasChild(e.OriginalSource as DependencyObject);
            if (selectedControl == null)
            {
                this.selectedCurrentContentItem = null;
                return;
            }
        }

        /// <summary>
        ///  OnPreviewMouseWheel 함수 처리
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPreviewMouseWheel(MouseWheelEventArgs e)
        {
            base.OnPreviewMouseWheel(e);
            if (this.PageCanvas.IsMouseIn == false)
            {
                this.PageCanvas.MouseWheelHandler(e);

            }
        }

        protected override void OnPreviewMouseRightButtonDown(MouseButtonEventArgs e)
        {
            base.OnPreviewMouseRightButtonDown(e);           
        }

        #endregion

        #region Gerneral Method

        /// <summary>
        /// Walks up the visual tree starting with the specified DependencyObject, 
        /// looking for a UIElement which is a child of the Canvas.  If a suitable 
        /// element is not found, null is returned.  If the 'depObj' object is a 
        /// UIElement in the Canvas's Children collection, it will be returned.
        /// </summary>
        /// <param name="depObj">
        /// A DependencyObject from which the search begins.
        /// </param>
        public UIElement FindCanvasChild(DependencyObject depObj)
        {
            while (depObj != null)
            {
                // If the current object is a UIElement which is a child of the
                // Canvas, exit the loop and return it.
                UIElement elem = depObj as UIElement;
                if (elem != null)
                {
                    if (this.PageCanvas.Children.Contains(elem) == true)
                        break;
                }



                // VisualTreeHelper works with objects of type Visual or Visual3D.
                // If the current object is not derived from Visual or Visual3D,
                // then use the LogicalTreeHelper to find the parent element.
                if (depObj is Visual || depObj is Visual3D)
                    depObj = VisualTreeHelper.GetParent(depObj);
                else
                    depObj = LogicalTreeHelper.GetParent(depObj);
            }
            return depObj as UIElement;
        }

        /// <summary>
        /// Determines one component of a UIElement's location 
        /// within a Canvas (either the horizontal or vertical offset).
        /// </summary>
        /// <param name="side1">
        /// The value of an offset relative to a default side of the 
        /// Canvas (i.e. top or left).
        /// </param>
        /// <param name="side2">
        /// The value of the offset relative to the other side of the 
        /// Canvas (i.e. bottom or right).
        /// </param>
        /// <param name="useSide1">
        /// Will be set to true if the returned value should be used 
        /// for the offset from the side represented by the 'side1' 
        /// parameter.  Otherwise, it will be set to false.
        /// </param>
        private static double ResolveOffset(double side1, double side2, out bool useSide1)
        {
            // If the Canvas.Left and Canvas.Right attached properties 
            // are specified for an element, the 'Left' value is honored.
            // The 'Top' value is honored if both Canvas.Top and 
            // Canvas.Bottom are set on the same element.  If one 
            // of those attached properties is not set on an element, 
            // the default value is Double.NaN.
            useSide1 = true;
            double result;
            if (Double.IsNaN(side1))
            {
                if (Double.IsNaN(side2))
                {
                    // Both sides have no value, so set the
                    // first side to a value of zero.
                    result = 0;
                }
                else
                {
                    result = side2;
                    useSide1 = false;
                }
            }
            else
            {
                result = side1;
            }
            return result;
        }

        /// <summary>
        /// Returns a Rect which describes the bounds of the element being dragged.
        /// </summary>
        private Rect CalculateDragElementRect(double newHorizOffset, double newVertOffset)
        {
            if (this.elementBeingDragged == null)
                throw new InvalidOperationException("ElementBeingDragged is null.");

            Size elemSize = this.elementBeingDragged.RenderSize;

            double x, y;

            if (this.modifyLeftOffset)
                x = newHorizOffset;
            else
                x = this.ActualWidth - newHorizOffset - elemSize.Width;

            if (this.modifyTopOffset)
                y = newVertOffset;
            else
                y = this.ActualHeight - newVertOffset - elemSize.Height;

            Point elemLoc = new Point(x, y);

            return new Rect(elemLoc, elemSize);
        }

        #region BringToFront / SendToBack

        /// <summary>
        /// Assigns the element a z-index which will ensure that 
        /// it is in front of every other element in the Canvas.
        /// The z-index of every element whose z-index is between 
        /// the element's old and new z-index will have its z-index 
        /// decremented by one.
        /// </summary>
        /// <param name="targetElement">
        /// The element to be sent to the front of the z-order.
        /// </param>
        public void BringToFront(UIElement element)
        {
            this.UpdateZOrder(element, true);
        }

        /// <summary>
        /// Assigns the element a z-index which will ensure that 
        /// it is behind every other element in the Canvas.
        /// The z-index of every element whose z-index is between 
        /// the element's old and new z-index will have its z-index 
        /// incremented by one.
        /// </summary>
        /// <param name="targetElement">
        /// The element to be sent to the back of the z-order.
        /// </param>
        public void SendToBack(UIElement element)
        {
            this.UpdateZOrder(element, false);
        }

        /// <summary>
        /// Helper method used by the BringToFront and SendToBack methods.
        /// </summary>
        /// <param name="element">
        /// The element to bring to the front or send to the back.
        /// </param>
        /// <param name="bringToFront">
        /// Pass true if calling from BringToFront, else false.
        /// </param>
        private void UpdateZOrder(UIElement element, bool bringToFront)
        {
            #region Safety Check

            if (element == null)
                throw new ArgumentNullException("element");

            if (!this.PageCanvas.Children.Contains(element))
                throw new ArgumentException("Must be a child element of the Canvas.", "element");

            #endregion // Safety Check

            #region Calculate Z-Indici And Offset

            // Determine the Z-Index for the target UIElement.
            int elementNewZIndex = -1;
            if (bringToFront)
            {
                foreach (UIElement elem in this.PageCanvas.Children)
                    if (elem.Visibility != Visibility.Collapsed)
                        ++elementNewZIndex;
            }
            else
            {
                elementNewZIndex = 0;
            }

            // Determine if the other UIElements' Z-Index 
            // should be raised or lowered by one. 
            int offset = (elementNewZIndex == 0) ? +1 : -1;

            int elementCurrentZIndex = Canvas.GetZIndex(element);

            #endregion // Calculate Z-Indici And Offset

            #region Update Z-Indici

            // Update the Z-Index of every UIElement in the Canvas.
            foreach (UIElement childElement in this.PageCanvas.Children)
            {
                if (childElement == element)
                    Canvas.SetZIndex(element, elementNewZIndex);
                else
                {
                    int zIndex = Canvas.GetZIndex(childElement);

                    // Only modify the z-index of an element if it is  
                    // in between the target element's old and new z-index.
                    if (bringToFront && elementCurrentZIndex < zIndex ||
                        !bringToFront && zIndex < elementCurrentZIndex)
                    {
                        Canvas.SetZIndex(childElement, zIndex + offset);
                    }
                }
            }

            #endregion // Update Z-Indici
        }

        #endregion // BringToFront / SendToBack

        #region SelectionAdorner

        public void releaseAllAdorner()
        {
            List<UIElement> items = new List<UIElement>();
            foreach (UIElement toAdorn in this.PageCanvas.Children)
            {
                if (adornerLayer != null)
                    ReleaseSelectionAdorner(toAdorn);

                items.Add(toAdorn);
            }

            if (this._elementDistanceInfo.Count > 0)
                items.ForEach(o => o.ReleaseElementDistanceInfo(this.PageCanvas, this._elementDistanceInfo));

            this.selectedContentItems.Clear();
        }

        public void ReleaseSelectionAdorner(UIElement uie)
        {
            Adorner[] adorners = adornerLayer.GetAdorners(uie);

            if (adorners != null && adorners.Count() > 0)
            {
                var selAdorners = adorners.Where(o => o.GetType().Equals(typeof(SelectionAdorner)) == true);
                if (selAdorners != null)
                {
                    foreach (var selAdor in selAdorners)
                        adornerLayer.Remove(selAdor);
                }
            }

            InSysBasicControls.Interfaces.ISelectable selectItem = uie as InSysBasicControls.Interfaces.ISelectable;
            if (selectItem != null)
                selectItem.IsSelected = false;

            if (this.selectedContentItems.Contains(uie) == true)
                this.selectedContentItems.Remove(uie);

        }

        public void SetContentElementSelectionAdorner(UIElement insysContentElement)
        {
            bool isResize = false;
            IDesignElement designeElement = insysContentElement as IDesignElement;
            if (designeElement == null)
                return;

            if (designeElement.InSysControlType != InSysControlType.InSysAudioBox)
            {
                isResize = true;
            }
            else
            {
                isResize = false;
            }

            SelectionAdorner adorner = SetSelectionAdorner(insysContentElement, isResize);

            if (this.selectedContentItems.Contains(insysContentElement) == false)
            {
                this.selectedContentItems.Add(insysContentElement);
                InSysBasicControls.Interfaces.ISelectable item = insysContentElement as InSysBasicControls.Interfaces.ISelectable;
                if (item != null)
                {
                    item.IsSelected = true;
                }
                adornerLayer.Add(adorner);
            }
        }

        public SelectionAdorner SetSelectionAdorner(UIElement selectedElement, bool isResize)
        {
            double left = Canvas.GetLeft(selectedElement);
            double top = Canvas.GetTop(selectedElement);

            this.PageCanvas.Tag = this._elementDistanceInfo;
            SelectionAdorner adorner = new SelectionAdorner(this.PageCanvas, selectedElement, isResize);
            
            adorner.PreviewMouseLeftButtonUp += (s, e) =>
            {
                #region 마우스 UP시 Flow View PageItem 썸네일이미지 업데이트
                this.RaisePageUpdateEvent(null);
                #endregion

                #region 마우스 UP시 선택 Element 속성값 업데이트
                if (selectedElement != null)
                    this.RaiseItemUpdatedPropertyEvent(selectedElement, "");
                #endregion
            };
            

            Canvas.SetLeft(adorner, left);
            Canvas.SetTop(adorner, top);

            return adorner;
        }

        #endregion

        public void ReleaseElementDistance(UIElement element)
        {
            element.ReleaseElementDistanceInfo(this.PageCanvas, this._elementDistanceInfo);//컨트롤 간격 표시 제거.
        }
        #endregion

        public void selectItemAdorner(UIElement insysElement)
        {
            if (insysElement != null)
            {
                IDesignElement designeElement = insysElement as IDesignElement;
                if (designeElement != null)//InSysAudioBox는 Select 표시를 하지 않는다.
                {
                    insysElement.ReleaseElementDistanceInfo(this.PageCanvas, this._elementDistanceInfo);
                    ReleaseSelectionAdorner(insysElement);

                    if (designeElement.InSysControlType != InSysControlType.InSysAudioBox)
                    {
                        if (insysElement != null)
                            insysElement.SetElementDistanceInfo(this.PageCanvas, this._elementDistanceInfo);//컨트롤의 거리 표시
                    }
                    else
                    {
                    }

                    SetContentElementSelectionAdorner(insysElement);
                }
            }
        }

        public void OnDoAlignment(AlignmentType alignmentType)
        {
            switch (alignmentType)
            {
                case AlignmentType.FixedViewSize:
                    this.PageDesignCanvasBox.FixedViewSize(this.selectedContentItems);
                    break;
                case AlignmentType.AlignLeft:
                    this.PageDesignCanvasBox.AlignLeft(this.selectedContentItems);
                    break;
                case AlignmentType.AlignRight:
                    this.PageDesignCanvasBox.AlignRight(this.selectedContentItems);
                    break;
                case AlignmentType.AlignTop:
                    this.PageDesignCanvasBox.AlignTop(this.selectedContentItems);
                    break;
                case AlignmentType.AlignBottom:
                    this.PageDesignCanvasBox.AlignBottom(this.selectedContentItems);
                    break;
                case AlignmentType.AlignHorizontalCenter:
                    this.PageDesignCanvasBox.AlignVerticalCenter(this.selectedContentItems);
                    break;
                case AlignmentType.AlignVerticalCenter:
                    this.PageDesignCanvasBox.AlignHorizontalCenter(this.selectedContentItems);
                    break;
                case AlignmentType.DistributeHorizontal:
                    this.PageDesignCanvasBox.DistributeHorizontal(this.selectedContentItems);
                    break;
                case AlignmentType.DistributeVertical:
                    this.PageDesignCanvasBox.DistributeVertical(this.selectedContentItems);
                    break;
                default:
                    break;
            }
        }

        public void OnDoZoom(ZoomType zoomType, double zoomValue, bool isFixedSize)
        {
            if (this.DesignContainerScrollViewer.ActualWidth <= 0 || this.DesignContainerScrollViewer.ActualHeight - 70 <= 0)
                return;

            this._isLockSliderValue = true;

            this._isFixedSize = isFixedSize;
            switch (DesignCanvasZoomType)
            {
                case ZoomType.Custom:
                    this.PageDesignCanvasBox.ZoomMode = DesignWorkCanvas.Views.PageCanvas.eZoomMode.CustomSize;
                    break;
                case ZoomType.FixedPage:
                    break;
                default:
                    this.PageDesignCanvasBox.ZoomMode = DesignWorkCanvas.Views.PageCanvas.eZoomMode.CustomSize;
                    break;
            }           

            this.PageDesignCanvasBox.ApplyZoom(zoomValue, new Size(this.DesignContainerScrollViewer.ActualWidth, this.DesignContainerScrollViewer.ActualHeight - 70), isFixedSize);
            
            if (this.DesignCanvasBarZoomSlider != null)
            {
                this.DesignCanvasBarZoomSlider.Value = this.PageCanvas.Zoom;
            }
            this._isLockSliderValue = false;
        }

        private void UserControl_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (this._isFixedSize == true)
            {
                OnDoZoom(ZoomType.Custom, 0.0, this._isFixedSize);                
            }
        }

        private void DesignCanvasBarZoomSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            Slider slider = sender as Slider;
            if (slider != null)
            {
                if (this._isLockSliderValue == false)
                {
                    DesignCanvasZoomType = ZoomType.Custom;
                    OnDoZoom(ZoomType.Custom, GetPercentValue(slider.Value), this._isFixedSize);
                }
            }
        }

        private void DesignCanvasBarZoomFixed_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            SetDesignCanvasZoomFixed();
        }

        private double GetPercentValue(double sliderValue)
        {
            return sliderValue / 100;
        }

        private void DesignCanvasBarZoomTextBlock_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this._isLockSliderValue = true;
            this.DesignCanvasBarZoomSlider.Value = 100;
            DesignCanvasZoomType = ZoomType.Custom;
            OnDoZoom(ZoomType.Custom, GetPercentValue(this.DesignCanvasBarZoomSlider.Value), false);
            this._isLockSliderValue = false;
        }

        public void SetDesignCanvasZoomFixed()
        {
            DesignCanvasZoomType = ZoomType.FixedPage;
            OnDoZoom(ZoomType.Custom, GetPercentValue(this.DesignCanvasBarZoomSlider.Value), true);  
        }

        public void SetDesignCanvasZoomFixed2()
        {
            DesignCanvasZoomType = ZoomType.FixedPage;
            OnDoZoom(ZoomType.Custom, GetPercentValue(this.DesignCanvasBarZoomSlider.Value), this._isFixedSize);
        }

        private void DesignCanvasBarZoomSlider_MouseDown(object sender, MouseButtonEventArgs e)
        {
            this._isFixedSize = false;
        }

        private void DesignCanvasBarZoomSlider_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            this._isFixedSize = false;
        }

        public TouchPageInfo CreateNewPage(Guid pageID, Guid parentID, double width, double height, double pageLiftTime, string pagename, int pageDepthIndex)
        {
            TouchPageInfo pageinfo = new TouchPageInfo(pageID.ToString(), parentID.ToString(), width, height, pageLiftTime, pagename, pageDepthIndex);

            this.PageCanvas.Width = width;
            this.PageCanvas.Height = height;            
            this.PageCanvas.SetBackground(new SolidColorBrush(Colors.White));
            pageinfo.Background = this.PageCanvas.Background;            
            this._touchPageInfo = pageinfo;

            SetDesignCanvasZoomFixed();

            this.PageCanvas.UpdateLayout();

            return pageinfo;
        }

     

        public void selectContentElement(object content)
        {
            try
            {
                if (content.GetType() == typeof(PageCanvas))
                {

                    //RaiseSelectedPageEvent(content);
                }
                else if (InSysControlManager.GetControlType(content.GetType().Name) != InSysControlType.None)
                {
                    RaiseSelectedContentEvent(content);
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message + ">> \r\n" + ex.StackTrace);
            }
        }

        public Dictionary<string, object> GetPropertyValue(FrameworkElement element)
        {
            return InSysControlManager.GetPropertyValue(element);
        }

        public void SetPropertyValue(FrameworkElement element, string propertyName, out object propertyValue, object defaultValue)
        {
            InSysControlManager.SetPropertyValue(element, propertyName, out propertyValue, defaultValue);
        }

        public Dictionary<string, object> GetPageCanvasPropertyValue()
        {
            Dictionary<string, object> dictionaries = new Dictionary<string, object>();
            dictionaries.Add("PageWidth", this.PageDesignCanvasBox.Width);
            dictionaries.Add("PageHeight", this.PageDesignCanvasBox.Height);
            if (this.TouchPageInfo != null && this.TouchPageInfo.Background != null)
                dictionaries.Add("Background", System.Windows.Markup.XamlWriter.Save(this.TouchPageInfo.Background));
            else
                dictionaries.Add("Background", System.Windows.Markup.XamlWriter.Save(new SolidColorBrush(Colors.White)));
            dictionaries.Add("DisplayOrder", this.TouchPageInfo.PageDisplayOrder);
            dictionaries.Add("PageName", this.PageDesignCanvasBox.Name);
            dictionaries.Add("Opacity", 1);
            dictionaries.Add("IsApplyPageLifeTime", true);
            dictionaries.Add("PageLifeTime", this.TouchPageInfo.PageLifeTimeTemp);
            List<FrameworkElement> elementList = new List<FrameworkElement>();
            foreach (var item in this.PageDesignCanvasBox.Children)
                elementList.Add(item as FrameworkElement);
            dictionaries.Add("PageElements", elementList);
            dictionaries.Add("PageEffect", "PageEffect");
            dictionaries.Add("Type", this.PageDesignCanvasBox.GetType());
            return dictionaries;
        }

        public void BringToFront()
        {
            List<UIElement> unselectedItems = this.PageDesignCanvasBox.Children.OfType<UIElement>()
                .Where(o => InSysBasicControls.InSysControlManager.GetControlType(o.GetType().Name) != InSysControlType.None)
                .Where(o => this.SelectedContentItems.Contains(o) != true).ToList();

            var unselectedItemsOrderList = unselectedItems.OrderBy(o => Canvas.GetZIndex(o));

            foreach (UIElement item in this.SelectedContentItems)
            {
                int itemOrder = Canvas.GetZIndex(item);
                if (unselectedItemsOrderList.Where(o => Canvas.GetZIndex(o) >= itemOrder).Count() > 0)//선택된 ZIndex보다 상위의 Item이 있는지 체크.
                {
                    int maxZIndexNo = unselectedItemsOrderList.Where(o => Canvas.GetZIndex(o) >= itemOrder).Max(o => Canvas.GetZIndex(o));

                    //var unselItemList = unselectedItemsOrderList.Where(o => Canvas.GetZIndex(o) == maxZIndexNo);
                    //foreach (var unsel_item in unselItemList)
                    //{
                    //    Canvas.SetZIndex(unsel_item, maxZIndexNo + 1);
                    //    (unsel_item as IDesignElement).ZIndex = maxZIndexNo + 1;
                    //    break;
                    //}

                    Canvas.SetZIndex(item, maxZIndexNo + 1);
                    (item as IDesignElement).ZIndex = maxZIndexNo + 1;
                }
            }
            RedrawingItemZIndex();
        }

        public void BringForward()
        {
            List<UIElement> unselectedItems = this.PageDesignCanvasBox.Children.OfType<UIElement>()
                .Where(o => InSysBasicControls.InSysControlManager.GetControlType(o.GetType().Name) != InSysControlType.None)
                .Where(o => this.SelectedContentItems.Contains(o) != true).ToList();

            var unselectedItemsOrderList = unselectedItems.OrderBy(o => Canvas.GetZIndex(o));

            foreach (UIElement item in this.SelectedContentItems)
            {
                int itemOrder = Canvas.GetZIndex(item);
                if (unselectedItemsOrderList.Where(o => Canvas.GetZIndex(o) >= itemOrder).Count() > 0)//선택된 ZIndex보다 상위의 Item이 있는지 체크.
                {
                    int minZIndexNo = unselectedItemsOrderList.Where(o => Canvas.GetZIndex(o) >= itemOrder).Min(o => Canvas.GetZIndex(o));
                    var unselItemList = unselectedItemsOrderList.Where(o => Canvas.GetZIndex(o) == minZIndexNo);
                    foreach (var unsel_item in unselItemList)
                    {
                        Canvas.SetZIndex(unsel_item, minZIndexNo - 1);
                        (unsel_item as IDesignElement).ZIndex = minZIndexNo - 1;
                    }

                    Canvas.SetZIndex(item, minZIndexNo);
                    (item as IDesignElement).ZIndex = minZIndexNo;
                }
            }

            RedrawingItemZIndex();
        }

        public void SendBackward()
        {
            IEnumerable<UIElement> unselectedItems = this.PageDesignCanvasBox.Children.OfType<UIElement>()
                .Where(o => InSysBasicControls.InSysControlManager.GetControlType(o.GetType().Name) != InSysControlType.None)
                .Where(o => this.SelectedContentItems.Contains(o) != true).ToList();

            var unselectedItemsOrderList = unselectedItems.OrderBy(o => Canvas.GetZIndex(o));

            foreach (UIElement item in this.SelectedContentItems)
            {
                int itemOrder = Canvas.GetZIndex(item);
                
                if (unselectedItemsOrderList.Where(o => Canvas.GetZIndex(o) <= itemOrder).Count() > 0)//선택된 ZIndex보다 상위의 Item이 있는지 체크.
                {
                    int maxZIndexNo = unselectedItemsOrderList.Where(o => Canvas.GetZIndex(o) <= itemOrder).Max(o => Canvas.GetZIndex(o));
                    
                    var unselItemList = unselectedItemsOrderList.Where(o => Canvas.GetZIndex(o) == maxZIndexNo);
                    foreach (var unsel_item in unselItemList)
                    {
                        if (Canvas.GetZIndex(unsel_item) == itemOrder) // 값이 동일하면
                        {
                            Canvas.SetZIndex(unsel_item, maxZIndexNo);
                            (unsel_item as IDesignElement).ZIndex = maxZIndexNo;
                        }
                        else // 작어면
                        {
                            Canvas.SetZIndex(unsel_item, maxZIndexNo + 1);
                            (unsel_item as IDesignElement).ZIndex = maxZIndexNo + 1;
                        }
                        
                        break;
                    }

                    Canvas.SetZIndex(item, maxZIndexNo);
                    (item as IDesignElement).ZIndex = maxZIndexNo;
                }               
            }

            RedrawingItemZIndex();
        }

        private void RedrawingItemZIndex()
        {
            List<UIElement> allItems = this.PageDesignCanvasBox.Children.OfType<UIElement>()
                   .Where(o => InSysBasicControls.InSysControlManager.GetControlType(o.GetType().Name) != InSysControlType.None)
                   .OrderBy(o => Canvas.GetZIndex(o))
                   .ToList();

            int inx = 0;
            foreach (var temp in allItems)
            {
                Canvas.SetZIndex(temp, inx);
                (temp as IDesignElement).ZIndex = inx;
                inx++;
            }
        }

        public void SendToBack()
        {           
            //List<IDesignElement> unselectedItems = this.PageDesignCanvasBox.Children.OfType<UIElement>()
            //    .Where(o => InSysBasicControls.InSysControlManager.GetControlType(o.GetType().Name) != InSysControlType.None)
            //    .Where(o => this.SelectedContentItems.Contains(o) != true).Select(o => o as IDesignElement).OrderBy(o => o.ZIndex).ToList();
            
            //int zIndex = 0;

            //List<IDesignElement> selectedItems = this.SelectedContentItems.Select(o => o as IDesignElement).OrderBy(o => o.ZIndex).ToList();
            //int selCount = selectedItems.Count;

            //zIndex = selCount;
            //unselectedItems.ForEach(o => o.ZIndex = zIndex++);

            //zIndex = 0;
            //selectedItems.ForEach(o => o.ZIndex = zIndex++);

            List<UIElement> unselectedItems = this.PageDesignCanvasBox.Children.OfType<UIElement>()
               .Where(o => InSysBasicControls.InSysControlManager.GetControlType(o.GetType().Name) != InSysControlType.None)
               .Where(o => this.SelectedContentItems.Contains(o) != true).OrderBy(o => Canvas.GetZIndex(o)).ToList();

            int zIndex = 0;

            List<UIElement> selectedItems = this.SelectedContentItems.OrderBy(o => Canvas.GetZIndex(o)).ToList();
            int selCount = selectedItems.Count;

            zIndex = selCount;
            unselectedItems.ForEach(o => { Canvas.SetZIndex(o, zIndex); (o as IDesignElement).ZIndex = zIndex; zIndex++; });

            zIndex = 0;
            selectedItems.ForEach(o => { Canvas.SetZIndex(o, zIndex); (o as IDesignElement).ZIndex = zIndex; zIndex++; });
        }

        public void UpdateZOrder(UIElement element, int updateZOrder)
        {
            IDesignElement designElement = element as IDesignElement;
            if (designElement != null)
            {
                int currentZOrder = designElement.ZIndex;
                if (updateZOrder == currentZOrder)
                    return;

                IEnumerable<IDesignElement> Items = this.PageDesignCanvasBox.Children.OfType<UIElement>().Where(o => InSysBasicControls.InSysControlManager.GetControlType(o.GetType().Name) != InSysControlType.None).Select(o => o as IDesignElement);
                if (updateZOrder > currentZOrder)
                {
                    Items = Items.Where(o => InSysBasicControls.InSysControlManager.GetControlType(o.GetType().Name) != InSysControlType.None)
                        .Where(o => currentZOrder < o.ZIndex && o.ZIndex <= updateZOrder);
                    Items.ToList().ForEach(o => (o as IDesignElement).ZIndex--);
                    designElement.ZIndex = updateZOrder;

                }
                else
                {
                    Items = Items.Where(o => InSysBasicControls.InSysControlManager.GetControlType(o.GetType().Name) != InSysControlType.None)
                        .Where(o => currentZOrder > o.ZIndex && o.ZIndex >= updateZOrder);
                    Items.ToList().ForEach(o => (o as IDesignElement).ZIndex++);                    
                    designElement.ZIndex = updateZOrder;
                }
            }
        }

        public void Init_SetInSysControl(UIElement element, List<TouchPageInfo> pageInfoList)
        {
            List<TouchPageInfo> movePagesList = pageInfoList.Where(o => o.PageGuid.Equals(this.TouchPageInfo.PageGuid) != true).ToList();

            double page_lifetime = this.TouchPageInfo.PageLifeTimeTemp;
            bool isapply_lifetime = this.TouchPageInfo.IsApplyPageLifeTime;

            InSysBasicControls.InSysControlManager.Init_SetControlActionEvent(element, movePagesList, this.TouchPageInfo);

            InSysBasicControls.InSysControlManager.Init_ContentElementProperty(element, page_lifetime, isapply_lifetime);
        }

        public void setTouchPageInfo(TouchPageInfo touchPageInfo)
        {
            this.TouchPageInfo = touchPageInfo;
        }

        #region Language 설정

        public static void setCultureInfo(CultureInfo info)
        {
            if (info != null)
            {
                Cultures.Resources.Culture = info;
                CultureResources.ChangeCulture(info);
                Thread.CurrentThread.CurrentCulture = info;
            }
        }

        private void InitStringResource()
        {
            Cultures.CultureResources.ResourceProvider = (ObjectDataProvider)this.FindResource("Resources");

            DesignCanvasBarZoomSlider.ToolTip = Cultures.Resources.ZoomSlider;
            DesignCanvasBarZoomTextBlock.Text = Cultures.Resources.ZoomOriginalSize;
            DesignCanvasBarZoomFixed.ToolTip = Cultures.Resources.ZoomFixed;
        }

        #endregion        

      
       

       
    }
}
