﻿using System.Windows.Documents;
using System.Windows;
using System.Windows.Media;
using System.Windows.Input;
using DesignWorkCanvas.Views;

namespace DesignWorkCanvas
{
    public class RubberbandAdorner2 : Adorner
    {
        private Point? startPoint;
        private Point? endPoint;
        private Pen rubberbandPen;

        private DesignContainer designerWorkCanvas;

        public RubberbandAdorner2(DesignContainer workCanvas, Point? dragStartPoint)
            : base(workCanvas)
        {
            this.designerWorkCanvas = workCanvas;
            this.startPoint = dragStartPoint;
            rubberbandPen = new Pen(Brushes.LightSlateGray, 1);
            rubberbandPen.DashStyle = new DashStyle(new double[] { 2 }, 1);
        }

        protected override void OnMouseMove(System.Windows.Input.MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                if (!this.IsMouseCaptured)
                    this.CaptureMouse();

                endPoint = e.GetPosition(this);
                UpdateSelection();
                this.InvalidateVisual();
            }
            else
            {
                if (this.IsMouseCaptured) this.ReleaseMouseCapture();
            }

            e.Handled = true;
        }
     
        protected override void OnMouseUp(System.Windows.Input.MouseButtonEventArgs e)
        {
            ReleaseRubberbandAdorner();

            e.Handled = true;
        }

        public void ReleaseRubberbandAdorner()
        {
            if (this.IsMouseCaptured) this.ReleaseMouseCapture();

            // remove this adorner from adorner layer
            AdornerLayer adornerLayer = AdornerLayer.GetAdornerLayer(this.designerWorkCanvas);
            if (adornerLayer != null)
                adornerLayer.Remove(this);
        }

        protected override void OnRender(DrawingContext dc)
        {
            base.OnRender(dc);

            // without a background the OnMouseMove event would not be fired!
            // Alternative: implement a Canvas as a child of this adorner, like
            // the ConnectionAdorner does.
            dc.DrawRectangle(Brushes.Transparent, null, new Rect(RenderSize));

            if (this.startPoint.HasValue && this.endPoint.HasValue)
                dc.DrawRectangle(Brushes.Transparent, rubberbandPen, new Rect(this.startPoint.Value, this.endPoint.Value));
        }

        private void UpdateSelection()
        {
            Rect rubberBand = new Rect(startPoint.Value, endPoint.Value);
            foreach (UIElement item in designerWorkCanvas.PageDesignCanvasBox.Children)
            {
                Rect itemRect = VisualTreeHelper.GetDescendantBounds(item);
                Rect itemBounds = item.TransformToAncestor(designerWorkCanvas).TransformBounds(itemRect);

                if (rubberBand.Contains(itemBounds))
                {
                    designerWorkCanvas.SetContentElementSelectionAdorner(item);                  
                }
                else
                {
                    designerWorkCanvas.ReleaseSelectionAdorner(item);
                }
            }
        }
    }
}
