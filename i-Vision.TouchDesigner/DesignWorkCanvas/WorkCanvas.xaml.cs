﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Media3D;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using InSysBasicControls.Interfaces;
using DesignWorkCanvas.Views;
using System.Windows.Shapes;
using DesignWorkCanvas.ViewModels;
using InSysTouchflowData;
using InSysTouchflowData.Models.ProjectModels;
using InSysBasicControls;

namespace DesignWorkCanvas
{
    /// <summary>
    /// Interaction logic for WorkCanvas.xaml
    /// </summary>
    public partial class WorkCanvas : UserControl
    {
        public SelectedInSysControlItemDelegate SelectedInSysControlItemHandler
        {
            get;
            set;
        }

        #region 변수

        private Dictionary<UIElement, List<LineDistanceView>> _elementDistanceInfo = new Dictionary<UIElement, List<LineDistanceView>>();            

        // True if a drag operation is underway, else false.
        private bool isDragInProgress;

        // Stores a reference to the UIElement currently being dragged by the user.
        private UIElement elementBeingDragged;
       
        // Keeps track of where the mouse cursor was when a drag operation began.		
        private Point origCursorLocation;

        // The offsets from the DragCanvas' edges when the drag operation began.
        private double origHorizOffset, origVertOffset;
        // Keeps track of which horizontal and vertical offset should be modified for the drag element.
        private bool modifyLeftOffset, modifyTopOffset;

        private AdornerLayer adornerLayer;

        //Double-Click 여부 체크 변수
        private bool isDoubleClicked = false;

        private bool _isFixedSize = false;

        private bool _isFixedLock = false;

        private ZoomType DesignCanvasZoomType = ZoomType.Custom;
        
        public ZoomCanvas ZoomCanvas
        {
            get { return this.artBoard; }
        }

        /// <summary>
        /// Gets/sets whether elements in the DragCanvas should be draggable by the user.
        /// The default value is true.  This is a dependency property.
        /// </summary>
        public bool AllowDragging
        {
            get { return (bool)base.GetValue(AllowDraggingProperty); }
            set { base.SetValue(AllowDraggingProperty, value); }
        }

        /// <summary>
        /// Returns the UIElement currently being dragged, or null.
        /// </summary>
        /// <remarks>
        /// Note to inheritors: This property exposes a protected 
        /// setter which should be used to modify the drag element.
        /// </remarks>
        public UIElement ElementBeingDragged
        {
            get
            {
                if (!this.AllowDragging)
                    return null;
                else
                    return this.elementBeingDragged;
            }
            set
            {
                if (this.elementBeingDragged != null)
                    this.elementBeingDragged.ReleaseMouseCapture();

                if (!this.AllowDragging)
                    this.elementBeingDragged = null;
                else
                {
                    if (WorkCanvas.GetCanBeDragged(value))
                    {
                        this.elementBeingDragged = value;
                        this.elementBeingDragged.CaptureMouse();
                    }
                    else
                        this.elementBeingDragged = null;
                }
            }
        }
        
        /// <summary>
        /// Gets/sets whether the user should be able to drag elements in the DragCanvas out of
        /// the viewable area.  The default value is false.  This is a dependency property.
        /// </summary>
        public bool AllowDragOutOfView
        {
            get { return (bool)GetValue(AllowDragOutOfViewProperty); }
            set { SetValue(AllowDragOutOfViewProperty, value); }
        }

        private UIElement selectedCurrentContentItem;
        public UIElement SelectedCurrentContentItem
        {
            get
            {
                return this.selectedCurrentContentItem;
            }
            set
            {
                if (SelectedInSysControlItemHandler != null)
                {
                    if (value is IDesignElement)
                    {
                        SelectedInSysControlItemHandler(value as FrameworkElement);
                    }
                }
                this.selectedCurrentContentItem = value;
            }
        }

        public List<UIElement> GetSelectedContentItems()
        {
            return selectedContentItems;
        }

        public void AddSelectedContentItem(UIElement addElement)
        {
            if(selectedContentItems.Contains(addElement) != true)
                selectedContentItems.Add(addElement);
        }

        private List<UIElement> selectedContentItems;
        public List<UIElement> SelectedContentItems
        {
            get
            {
                if (adornerLayer != null)
                {
                    try
                    {
                        foreach (UIElement toAdorn in this.artBoard.Children)
                        {
                            InSysBasicControls.Interfaces.ISelectable item = toAdorn as InSysBasicControls.Interfaces.ISelectable;
                            if (item != null && item.IsSelected == true)
                            {
                                if (selectedContentItems.Contains(toAdorn) == true)
                                    continue;
                                selectedContentItems.Add(toAdorn);
                            }
                            else
                            {
                                if (selectedContentItems.Contains(toAdorn) == true)
                                    selectedContentItems.Remove(toAdorn);
                            }
                        }                        
                    }
                    catch
                    {
                    }
                }
                return selectedContentItems;
            }

            protected set
            {
                selectedContentItems = value;
            }
        }
        
        #endregion

        #region 생성자

        public WorkCanvas()
        {
            InitializeComponent();

            this.Loaded += (s, e) =>
            {
                adornerLayer = AdornerLayer.GetAdornerLayer(this);
                SelectedContentItems = new List<UIElement>();                
            };

            InSysBasicControls.InSysProperties.InSysProperty.ChangedHeightPropertyCallback = new PropertyChangedDelegate(Changed_HeightProperty);
            InSysBasicControls.InSysProperties.InSysProperty.ChangedWidthPropertyCallback = new PropertyChangedDelegate(Changed_WidthProperty);
            InSysBasicControls.InSysProperties.InSysProperty.ChangedXPositionPropertyCallback = new PropertyChangedDelegate(Changed_XProperty);
            InSysBasicControls.InSysProperties.InSysProperty.ChangedYPositionPropertyCallback = new PropertyChangedDelegate(Changed_YProperty);    
        }
        
        static WorkCanvas()
        {
            AllowDraggingProperty = DependencyProperty.Register(
               "AllowDragging",
               typeof(bool),
               typeof(WorkCanvas),
               new PropertyMetadata(true));

            AllowDragOutOfViewProperty = DependencyProperty.Register(
                "AllowDragOutOfView",
                typeof(bool),
                typeof(WorkCanvas),
                new UIPropertyMetadata(true));

            CanBeDraggedProperty = DependencyProperty.RegisterAttached(
                "CanBeDragged",
                typeof(bool),
                typeof(WorkCanvas),
                new UIPropertyMetadata(true));
        }
        #endregion

        #region

        private object Changed_XProperty(object sender, object obj)
        {
            return obj;
        }

        private object Changed_YProperty(object sender, object obj)
        {           
            return obj;
        }

       

        private object Changed_HeightProperty(object sender, object obj)
        {
            double height = (double)obj;
            FrameworkElement frameworkElement = this.SelectedCurrentContentItem as FrameworkElement;
            if (frameworkElement != null )
            {
                //this.SelectedCurrentContentItem.ResizeElementDistanceInfo(frameworkElement.Width, height, this.artBoard, this._elementDistanceInfo);//컨트롤 추가시 라인 오류 발생
            }
            
            return obj;
        }

        private object Changed_WidthProperty(object sender, object obj)
        {
            double width = (double)obj;
            FrameworkElement frameworkElement = this.SelectedCurrentContentItem as FrameworkElement;
            if (frameworkElement != null )
            {
                //this.SelectedCurrentContentItem.ResizeElementDistanceInfo(width, frameworkElement.Height, this.artBoard, this._elementDistanceInfo);//컨트롤 추가시 라인 오류 발생
            }
            return obj;
        }

        #endregion

        #region Dependency Properties

        public static readonly DependencyProperty AllowDraggingProperty;
        public static readonly DependencyProperty AllowDragOutOfViewProperty;
        public static readonly DependencyProperty CanBeDraggedProperty;

        #endregion // Dependency Properties
                     
        #region static Method

        public static bool GetCanBeDragged(UIElement uiElement)
        {
            if (uiElement == null)
                return false;

            return (bool)uiElement.GetValue(CanBeDraggedProperty);
        }

        #endregion

        #region RoutedEvent

        public static readonly RoutedEvent SendContentSelectedEvent = EventManager.RegisterRoutedEvent("SendContentSelected", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(WorkCanvas));
        
        public event RoutedEventHandler SendContentSelected
        {
            add { AddHandler(SendContentSelectedEvent, value); }
            remove { RemoveHandler(SendContentSelectedEvent, value); }
        }
      
        #endregion
      
        #region Overrides Method

        private Point? rubberbandSelectionStartPoint = null;

        private void zoomCanvasGrid_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.rubberbandSelectionStartPoint = new Point?(e.GetPosition(this));            
        }

        private void zoomCanvasGrid_PreviewMouseMove(object sender, MouseEventArgs e)
        {
            if (e.LeftButton != MouseButtonState.Pressed)
                this.rubberbandSelectionStartPoint = null;

            if (this.rubberbandSelectionStartPoint.HasValue)
            {
                // create rubberband adorner
                AdornerLayer adornerLayer = AdornerLayer.GetAdornerLayer(this);
                if (adornerLayer != null)
                {
                    RubberbandAdorner adorner = new RubberbandAdorner(this, rubberbandSelectionStartPoint);
                    if (adorner != null)
                    {
                        adornerLayer.Add(adorner);
                    }
                }
            }
        }      
                      
        /// <summary>
        /// PreviewMouseLeftButtonDown 함수 처리
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPreviewMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            if (this.isDoubleClicked == true)//Double-Click일 경우 
            {
                this.isDoubleClicked = false;//Double-Click 해제.
                return;
            }
            base.OnPreviewMouseLeftButtonDown(e);            

            this.isDragInProgress = false;

            // Cache the mouse cursor location.
            this.origCursorLocation = e.GetPosition(this.artBoard);

            // Walk up the visual tree from the element that was clicked, 
            // looking for an element that is a direct child of the Canvas.
            this.elementBeingDragged = this.FindCanvasChild(e.OriginalSource as DependencyObject);
            if (this.elementBeingDragged == null)
            {                
                this.artBoard.ResizePanel();               
                ReleaseAllAdorner();                
                return;
            }

            this.elementBeingDragged.CaptureMouse();

            //Press Control key
            if (Keyboard.Modifiers != ModifierKeys.Control)
            {
                ReleaseAllAdorner();
            }

            // Get the element's offsets from the four sides of the Canvas.
            double left = Canvas.GetLeft(this.elementBeingDragged);
            double right = Canvas.GetRight(this.elementBeingDragged);
            double top = Canvas.GetTop(this.elementBeingDragged);
            double bottom = Canvas.GetBottom(this.elementBeingDragged);

            // Calculate the offset deltas and determine for which sides
            // of the Canvas to adjust the offsets.
            this.origHorizOffset = ResolveOffset(left, right, out this.modifyLeftOffset);
            this.origVertOffset = ResolveOffset(top, bottom, out this.modifyTopOffset);

            // Set the Handled flag so that a control being dragged 
            // does not react to the mouse input.
            //e.Handled = true;

            this.isDragInProgress = true;
            this.SelectedInSysControl(this.elementBeingDragged);

            this.SelectedCurrentContentItem = this.elementBeingDragged;     
            if (this.selectedContentItems.Contains(this.elementBeingDragged) == false)
                this.selectedContentItems.Add(this.elementBeingDragged);            
        }

        protected override void  OnPreviewMouseDoubleClick(MouseButtonEventArgs e)
        {
            base.OnPreviewMouseDoubleClick(e);
            this.isDoubleClicked = true;//Double-Click 했음을 표시.
        }     
       
        /// <summary>
        /// OnPreviewMouseMove 함수 처리
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPreviewMouseMove(MouseEventArgs e)
        {
            base.OnPreviewMouseMove(e);
           
            // If no element is being dragged, there is nothing to do.
            if (!this.isDragInProgress)
                return;

            if (this.elementBeingDragged == null || !this.isDragInProgress)
                return;

            if (InSysBasicControls.InSysControlManager.GetControlType(this.elementBeingDragged.GetType().Name) == InSysBasicControls.CommonClass.InSysControlType.None)
                return;

            #region Mouse Up 된 상태에서 드래그 되는 현상 방지 :컨트롤 Opacity가 0일 경우 외곽선 선택시 MouseUp이벤트가 발생하지 않음
            UIElement selectedControl = this.FindCanvasChild(e.OriginalSource as DependencyObject);
            if (selectedControl == null)
            {
                this.elementBeingDragged = null;
                return;
            }
            #endregion

            IDesignElement designeElement = this.elementBeingDragged as IDesignElement;
            if (designeElement != null)
            {
                if (designeElement.InSysControlType == InSysBasicControls.CommonClass.InSysControlType.InSysAudioBox)
                {
                    return;
                }
            }            

            // Get the position of the mouse cursor, relative to the Canvas.
            Point cursorLocation = e.GetPosition(this.artBoard);

            // These values will store the new offsets of the drag element.
            double newHorizontalOffset, newVerticalOffset;

            #region Calculate Offsets

            // Determine the horizontal offset.
            if (this.modifyLeftOffset)
                newHorizontalOffset = this.origHorizOffset + (cursorLocation.X - this.origCursorLocation.X);
            else
                newHorizontalOffset = this.origHorizOffset - (cursorLocation.X - this.origCursorLocation.X);

            // Determine the vertical offset.
            if (this.modifyTopOffset)
                newVerticalOffset = this.origVertOffset + (cursorLocation.Y - this.origCursorLocation.Y);
            else
                newVerticalOffset = this.origVertOffset - (cursorLocation.Y - this.origCursorLocation.Y);

            #endregion // Calculate Offsets

            if (!this.AllowDragOutOfView)
            {
                #region Verify Drag Element Location

                // Get the bounding rect of the drag element.
                Rect elemRect = this.CalculateDragElementRect(newHorizontalOffset, newVerticalOffset);

                // If the element is being dragged out of the viewable area, 
                // determine the ideal rect location, so that the element is 
                // within the edge(s) of the canvas.
                bool leftAlign = elemRect.Left < 0;
                bool rightAlign = elemRect.Right > this.artBoard.ActualWidth;

                if (leftAlign)
                    newHorizontalOffset = modifyLeftOffset ? 0 : this.artBoard.ActualWidth - elemRect.Width;
                else if (rightAlign)
                    newHorizontalOffset = modifyLeftOffset ? this.artBoard.ActualWidth - elemRect.Width : 0;

                bool topAlign = elemRect.Top < 0;
                bool bottomAlign = elemRect.Bottom > this.artBoard.ActualHeight;

                if (topAlign)
                    newVerticalOffset = modifyTopOffset ? 0 : this.artBoard.ActualHeight - elemRect.Height;
                else if (bottomAlign)
                    newVerticalOffset = modifyTopOffset ? this.artBoard.ActualHeight - elemRect.Height : 0;

                #endregion // Verify Drag Element Location
            }

            if (this.selectedContentItems.Count > 1)
            {
                foreach (var dragItem in this.selectedContentItems)
                {
                    #region 멀티 선택 컨트롤 Move Drag Element
                    /* 오동작함.
                    double left = Canvas.GetLeft(dragItem);
                    double top = Canvas.GetTop(dragItem);

                    if (this.modifyLeftOffset)
                        Canvas.SetLeft(dragItem, newHorizontalOffset + left);
                    else
                        Canvas.SetRight(dragItem, newHorizontalOffset + left);

                    if (this.modifyTopOffset)
                        Canvas.SetTop(dragItem, newVerticalOffset + top);
                    else
                        Canvas.SetBottom(dragItem, newVerticalOffset + top);

                   

                    #region 속성창의 X, Y 좌표 표시 소수점 이하 자리수로 표시되지 않도록 처리하는 코드

                    IElementProperties dlementProperties = dragItem as IElementProperties;
                    if (dlementProperties != null)
                    {
                        dlementProperties.ElementProperties.X = newHorizontalOffset + left;
                        dlementProperties.ElementProperties.Y = newVerticalOffset + top;
                    }

                    #endregion //속성창의 X, Y 좌표 표시 소수점 이하 자리수로 표시되지 않도록 처리하는 코드

                    dragItem.MoveElementDistanceInfo(this.artBoard, this._elementDistanceInfo);
                     */
                    #endregion // Move Drag Element
                }
            }
            else
            {
                #region Move Drag Element

                if (this.modifyLeftOffset)
                    Canvas.SetLeft(this.elementBeingDragged, newHorizontalOffset);
                else
                    Canvas.SetRight(this.elementBeingDragged, newHorizontalOffset);

                if (this.modifyTopOffset)
                    Canvas.SetTop(this.elementBeingDragged, newVerticalOffset);
                else
                    Canvas.SetBottom(this.elementBeingDragged, newVerticalOffset);

                #endregion // Move Drag Element

                #region 속성창의 X, Y 좌표 표시 소수점 이하 자리수로 표시되지 않도록 처리하는 코드

                IElementProperties dlementProperties = this.elementBeingDragged as IElementProperties;
                if (dlementProperties != null)
                {
                    dlementProperties.ElementProperties.X = newHorizontalOffset;
                    dlementProperties.ElementProperties.Y = newVerticalOffset;
                }

                #endregion //속성창의 X, Y 좌표 표시 소수점 이하 자리수로 표시되지 않도록 처리하는 코드

                this.elementBeingDragged.MoveElementDistanceInfo(this.artBoard, this._elementDistanceInfo);
            }

            this.rubberbandSelectionStartPoint = null;//Move Drage시 해제.
        }               

        /// <summary>
        /// OnPreviewMouseLeftButtonUp 함수 처리
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPreviewMouseLeftButtonUp(MouseButtonEventArgs e)
        {
            base.OnPreviewMouseLeftButtonUp(e);

            if (this.elementBeingDragged != null)
            {
                // Reset the field whether the left or right mouse button was 
                // released, in case a context menu was opened on the drag element.
                this.elementBeingDragged.ReleaseMouseCapture();
                this.elementBeingDragged = null;
            }
            this.artBoard.ClearLine();//마우스 UP시 컨트롤 간 동일 좌표 표시 UI 없애기   
        }
              
        protected override void OnMouseLeftButtonUp(MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonUp(e);

            UIElement selectedControl = this.FindCanvasChild(e.OriginalSource as DependencyObject);
            if (selectedControl == null)
            {
                this.selectedCurrentContentItem = null;
                return;
            }

        }
        
        /// <summary>
        ///  OnPreviewMouseWheel 함수 처리
        /// </summary>
        /// <param name="e"></param>
        protected override void OnPreviewMouseWheel(MouseWheelEventArgs e)
        {
            base.OnPreviewMouseWheel(e);
            if (this.artBoard.IsMouseIn == false)
            {
                this.artBoard.MouseWheelHandler(e);
               
            }
        }

        #endregion

        #region Gerneral Method

        /// <summary>
        /// Walks up the visual tree starting with the specified DependencyObject, 
        /// looking for a UIElement which is a child of the Canvas.  If a suitable 
        /// element is not found, null is returned.  If the 'depObj' object is a 
        /// UIElement in the Canvas's Children collection, it will be returned.
        /// </summary>
        /// <param name="depObj">
        /// A DependencyObject from which the search begins.
        /// </param>
        public UIElement FindCanvasChild(DependencyObject depObj)
        {
            while (depObj != null)
            {
                // If the current object is a UIElement which is a child of the
                // Canvas, exit the loop and return it.
                UIElement elem = depObj as UIElement;
                if (elem != null)
                {
                    if ( this.artBoard.Children.Contains(elem) == true)
                        break;                   
                }

                

                // VisualTreeHelper works with objects of type Visual or Visual3D.
                // If the current object is not derived from Visual or Visual3D,
                // then use the LogicalTreeHelper to find the parent element.
                if (depObj is Visual || depObj is Visual3D)
                    depObj = VisualTreeHelper.GetParent(depObj);
                else
                    depObj = LogicalTreeHelper.GetParent(depObj);
            }
            return depObj as UIElement;
        }

        /// <summary>
        /// Determines one component of a UIElement's location 
        /// within a Canvas (either the horizontal or vertical offset).
        /// </summary>
        /// <param name="side1">
        /// The value of an offset relative to a default side of the 
        /// Canvas (i.e. top or left).
        /// </param>
        /// <param name="side2">
        /// The value of the offset relative to the other side of the 
        /// Canvas (i.e. bottom or right).
        /// </param>
        /// <param name="useSide1">
        /// Will be set to true if the returned value should be used 
        /// for the offset from the side represented by the 'side1' 
        /// parameter.  Otherwise, it will be set to false.
        /// </param>
        private static double ResolveOffset(double side1, double side2, out bool useSide1)
        {
            // If the Canvas.Left and Canvas.Right attached properties 
            // are specified for an element, the 'Left' value is honored.
            // The 'Top' value is honored if both Canvas.Top and 
            // Canvas.Bottom are set on the same element.  If one 
            // of those attached properties is not set on an element, 
            // the default value is Double.NaN.
            useSide1 = true;
            double result;
            if (Double.IsNaN(side1))
            {
                if (Double.IsNaN(side2))
                {
                    // Both sides have no value, so set the
                    // first side to a value of zero.
                    result = 0;
                }
                else
                {
                    result = side2;
                    useSide1 = false;
                }
            }
            else
            {
                result = side1;
            }
            return result;
        }
        
        /// <summary>
        /// Returns a Rect which describes the bounds of the element being dragged.
        /// </summary>
        private Rect CalculateDragElementRect(double newHorizOffset, double newVertOffset)
        {
            if (this.elementBeingDragged == null)
                throw new InvalidOperationException("ElementBeingDragged is null.");

            Size elemSize = this.elementBeingDragged.RenderSize;

            double x, y;

            if (this.modifyLeftOffset)
                x = newHorizOffset;
            else
                x = this.ActualWidth - newHorizOffset - elemSize.Width;

            if (this.modifyTopOffset)
                y = newVertOffset;
            else
                y = this.ActualHeight - newVertOffset - elemSize.Height;

            Point elemLoc = new Point(x, y);

            return new Rect(elemLoc, elemSize);
        }
        
        #region BringToFront / SendToBack

        /// <summary>
        /// Assigns the element a z-index which will ensure that 
        /// it is in front of every other element in the Canvas.
        /// The z-index of every element whose z-index is between 
        /// the element's old and new z-index will have its z-index 
        /// decremented by one.
        /// </summary>
        /// <param name="targetElement">
        /// The element to be sent to the front of the z-order.
        /// </param>
        public void BringToFront(UIElement element)
        {
            this.UpdateZOrder(element, true);
        }

        /// <summary>
        /// Assigns the element a z-index which will ensure that 
        /// it is behind every other element in the Canvas.
        /// The z-index of every element whose z-index is between 
        /// the element's old and new z-index will have its z-index 
        /// incremented by one.
        /// </summary>
        /// <param name="targetElement">
        /// The element to be sent to the back of the z-order.
        /// </param>
        public void SendToBack(UIElement element)
        {
            this.UpdateZOrder(element, false);
        }

        /// <summary>
        /// Helper method used by the BringToFront and SendToBack methods.
        /// </summary>
        /// <param name="element">
        /// The element to bring to the front or send to the back.
        /// </param>
        /// <param name="bringToFront">
        /// Pass true if calling from BringToFront, else false.
        /// </param>
        private void UpdateZOrder(UIElement element, bool bringToFront)
        {
            #region Safety Check

            if (element == null)
                throw new ArgumentNullException("element");

            if (!this.artBoard.Children.Contains(element))
                throw new ArgumentException("Must be a child element of the Canvas.", "element");

            #endregion // Safety Check

            #region Calculate Z-Indici And Offset

            // Determine the Z-Index for the target UIElement.
            int elementNewZIndex = -1;
            if (bringToFront)
            {
                foreach (UIElement elem in this.artBoard.Children)
                    if (elem.Visibility != Visibility.Collapsed)
                        ++elementNewZIndex;
            }
            else
            {
                elementNewZIndex = 0;
            }

            // Determine if the other UIElements' Z-Index 
            // should be raised or lowered by one. 
            int offset = (elementNewZIndex == 0) ? +1 : -1;

            int elementCurrentZIndex = Canvas.GetZIndex(element);

            #endregion // Calculate Z-Indici And Offset

            #region Update Z-Indici

            // Update the Z-Index of every UIElement in the Canvas.
            foreach (UIElement childElement in this.artBoard.Children)
            {
                if (childElement == element)
                    Canvas.SetZIndex(element, elementNewZIndex);
                else
                {
                    int zIndex = Canvas.GetZIndex(childElement);

                    // Only modify the z-index of an element if it is  
                    // in between the target element's old and new z-index.
                    if (bringToFront && elementCurrentZIndex < zIndex ||
                        !bringToFront && zIndex < elementCurrentZIndex)
                    {
                        Canvas.SetZIndex(childElement, zIndex + offset);
                    }
                }
            }

            #endregion // Update Z-Indici
        }

        #endregion // BringToFront / SendToBack

        #region SelectionAdorner

        public void ReleaseAllAdorner()
        {
            List<UIElement> items = new List<UIElement>();
            foreach (UIElement toAdorn in this.artBoard.Children)
            {
                if (adornerLayer != null)
                    ReleaseSelectionAdorner(toAdorn);

                items.Add(toAdorn);
            }          

            if (this._elementDistanceInfo.Count > 0)
                items.ForEach(o => o.ReleaseElementDistanceInfo(this.artBoard, this._elementDistanceInfo));

            this.selectedContentItems.Clear();
        }

        public void ReleaseSelectionAdorner(UIElement uie)
        {
            Adorner[] adorners = adornerLayer.GetAdorners(uie);

            if (adorners != null && adorners.Count() > 0)
            {
                var selAdorners = adorners.Where(o => o.GetType().Equals(typeof(SelectionAdorner)) == true);
                if (selAdorners != null)
                {
                    foreach (var selAdor in selAdorners)
                        adornerLayer.Remove(selAdor);
                }               
            }

            InSysBasicControls.Interfaces.ISelectable selectItem = uie as InSysBasicControls.Interfaces.ISelectable;
            if (selectItem != null)
                selectItem.IsSelected = false;

            if (this.selectedContentItems.Contains(uie) == true)
                this.selectedContentItems.Remove(uie);            
           
        }

        public void SetSelectionAdorner(UIElement insysElement)
        {
            bool isResize = false;
            IDesignElement designeElement = insysElement as IDesignElement;
            if (designeElement == null)
                return;

            if (designeElement.InSysControlType != InSysBasicControls.CommonClass.InSysControlType.InSysAudioBox)
            {
                isResize = true;
            }
            else
            {
                isResize = false;
            }

            double left = Canvas.GetLeft(insysElement);            
            double top = Canvas.GetTop(insysElement);           

            this.artBoard.Tag = this._elementDistanceInfo;
            SelectionAdorner adorner = new SelectionAdorner(this.artBoard, insysElement, isResize);
            
            Canvas.SetLeft(adorner, left);
            Canvas.SetTop(adorner, top);

            if (this.selectedContentItems.Contains(insysElement) == false)
            {
                this.selectedContentItems.Add(insysElement);
                InSysBasicControls.Interfaces.ISelectable item = insysElement as InSysBasicControls.Interfaces.ISelectable;
                if (item != null)
                {
                    item.IsSelected = true;
                }

                adornerLayer.Add(adorner);
            }
        }

        #endregion

        public void ReleaseElementDistance(UIElement element)
        {
            element.ReleaseElementDistanceInfo(this.artBoard, this._elementDistanceInfo);//컨트롤 간격 표시 제거.
        }
              

        #endregion

        #region 함수

        public void InsertContent(UIElement uielement, Point point)
        {
            Canvas.SetLeft(uielement, point.X);
            Canvas.SetTop(uielement, point.Y);
            this.artBoard.Children.Add(uielement);
        }

        #endregion     
                           
        public void SelectedInSysControl(UIElement insysElement)
        {
            if (insysElement != null)
            {
                IDesignElement designeElement = insysElement as IDesignElement;
                if (designeElement != null)//InSysAudioBox는 Select 표시를 하지 않는다.
                {
                    insysElement.ReleaseElementDistanceInfo(this.artBoard, this._elementDistanceInfo);
                    ReleaseSelectionAdorner(insysElement);

                    if (designeElement.InSysControlType != InSysBasicControls.CommonClass.InSysControlType.InSysAudioBox)
                    {
                        if (insysElement != null)
                            insysElement.SetElementDistanceInfo(this.artBoard, this._elementDistanceInfo);//컨트롤의 거리 표시
                    }
                    else
                    {
                    }

                    SetSelectionAdorner(insysElement);
                }
            }
        }

        public void OnDoAlignment(AlignmentType alignmentType)
        {
            switch (alignmentType)
            {
                case AlignmentType.FixedViewSize:
                    this.ZoomCanvas.FixedViewSize(this.selectedContentItems);
                    break;
                case AlignmentType.AlignLeft:
                    this.ZoomCanvas.AlignLeft(this.selectedContentItems);
                    break;
                case AlignmentType.AlignRight:
                    this.ZoomCanvas.AlignRight(this.selectedContentItems);
                    break;
                case AlignmentType.AlignTop:
                    this.ZoomCanvas.AlignTop(this.selectedContentItems);
                    break;
                case AlignmentType.AlignBottom:
                    this.ZoomCanvas.AlignBottom(this.selectedContentItems);
                    break;
                case AlignmentType.AlignHorizontalCenter:
                    this.ZoomCanvas.AlignVerticalCenter(this.selectedContentItems);
                    break;
                case AlignmentType.AlignVerticalCenter:
                    this.ZoomCanvas.AlignHorizontalCenter(this.selectedContentItems);
                    break;
                case AlignmentType.DistributeHorizontal:
                    this.ZoomCanvas.DistributeHorizontal(this.selectedContentItems);
                    break;
                case AlignmentType.DistributeVertical:
                    this.ZoomCanvas.DistributeVertical(this.selectedContentItems);
                    break;
                default:
                    break;
            }
        }

        public void OnDoZoom(ZoomType zoomType, double zoomValue, bool isFixedSize)
        {
            this._isFixedSize = isFixedSize; 
            switch (DesignCanvasZoomType)
            {
                case ZoomType.Custom:
                    this.ZoomCanvas.ZoomMode = DesignWorkCanvas.ZoomCanvas.eZoomMode.CustomSize;
                    break;
                case ZoomType.FixedPage:
                    break;
                default:
                    this.ZoomCanvas.ZoomMode = DesignWorkCanvas.ZoomCanvas.eZoomMode.CustomSize;
                    break;
            }

            
            if (this.scrollviewer.ActualWidth == 0 || this.scrollviewer.ActualHeight == 0)
                return;

            this.ZoomCanvas.ApplyZoom(zoomValue, new Size(this.scrollviewer.ActualWidth, this.scrollviewer.ActualHeight - 70), isFixedSize);
        }

        private void UserControl_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (this._isFixedSize == true)
                this.ZoomCanvas.ApplyZoom(0.0, new Size(this.scrollviewer.ActualWidth, this.scrollviewer.ActualHeight - 70), this._isFixedSize);
        }

        private void DesignCanvasBarZoomSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            Slider slider = sender as Slider;
            if (slider != null)
            {
                if (this._isFixedLock == false)
                {
                    DesignCanvasZoomType = ZoomType.Custom;
                    OnDoZoom(ZoomType.Custom, GetPercentValue(slider.Value), false);
                }
            }
        }

        private void DesignCanvasBarZoomFixed_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DesignCanvasZoomType = ZoomType.FixedPage;
            this._isFixedLock = true;
            OnDoZoom(ZoomType.Custom, GetPercentValue(this.DesignCanvasBarZoomSlider.Value), true);

            if (this.DesignCanvasBarZoomSlider != null)
            {

                this.DesignCanvasBarZoomSlider.Value = this.artBoard.Zoom;
            }

            this._isFixedLock = false;
        }

        private double GetPercentValue(double sliderValue)
        {
            return sliderValue / 100;
        }

        private void DesignCanvasBarZoomTextBlock_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.DesignCanvasBarZoomSlider.Value = 100;
            DesignCanvasZoomType = ZoomType.Custom;
            OnDoZoom(ZoomType.Custom, GetPercentValue(this.DesignCanvasBarZoomSlider.Value), false);
            
        }

        public TouchflowPageInfo CreateNewPage(double width, double height, double pageLiftTime, string pagename)
        {
            TouchflowPageInfo pageinfo = new TouchflowPageInfo(width, height, pageLiftTime, pagename);

            this.artBoard.Width = width;
            this.artBoard.Height = height;
            this.artBoard.UpdateLayout();
            this.artBoard.ResizePanel();

            return pageinfo;
        }
    }   
}
