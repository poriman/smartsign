﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DesignWorkCanvas.Views;
using System.Windows.Controls;
using System.Windows;
using System.Windows.Input;
using System.Windows.Shapes;
using System.Windows.Media;
using InSysTouchflowData;

namespace DesignWorkCanvas
{
    public static class ExtensionClass
    {
        public static void SetElementDistanceInfo(this UIElement element, Canvas canvas, Dictionary<UIElement, List<LineDistanceView>> elementDistanceList)
        {
            FrameworkElement frameworkElement = element as FrameworkElement;
            List<LineDistanceView> tempList = new List<LineDistanceView>();
            if (frameworkElement != null)
            {
                LineDistanceView topLineView = new LineDistanceView();
                topLineView.LineDirectonType = LineDistanceView.DirectonType.Top;
                topLineView.Angle = 90;
                topLineView.Height = Math.Abs(Canvas.GetTop(frameworkElement));//크기는 절대값이다. Canvas 영역을 벗을 낫을 경우를 대비.
                double x = Canvas.GetLeft(frameworkElement) + frameworkElement.Width / 2;
                double y = 0;
                topLineView.ViewPoint = new Point(x, y);
                topLineView.DrawingLine(topLineView.Width, topLineView.Height);
                canvas.Children.Add(topLineView);

                tempList.Add(topLineView);

                LineDistanceView bottomLineView = new LineDistanceView();
                bottomLineView.LineDirectonType = LineDistanceView.DirectonType.Bottom;
                bottomLineView.Angle = 270;
                bottomLineView.Height = Math.Abs(canvas.Height - Canvas.GetTop(frameworkElement) - frameworkElement.Height);//크기는 절대값이다. Canvas 영역을 벗을 낫을 경우를 대비.
                x = Canvas.GetLeft(frameworkElement) + frameworkElement.Width / 2;
                y = canvas.Height - bottomLineView.Height;
                bottomLineView.ViewPoint = new Point(x, y);
                bottomLineView.DrawingLine(bottomLineView.Width, bottomLineView.Height);
                canvas.Children.Add(bottomLineView);

                tempList.Add(bottomLineView);

                LineDistanceView leftLineView = new LineDistanceView();
                leftLineView.LineDirectonType = LineDistanceView.DirectonType.Left;

                leftLineView.Angle = 0;
                leftLineView.Width = Math.Abs(Canvas.GetLeft(frameworkElement));//크기는 절대값이다. Canvas 영역을 벗을 낫을 경우를 대비.
                x = 0;
                y = Canvas.GetTop(frameworkElement) + frameworkElement.Height / 2;
                leftLineView.ViewPoint = new Point(x, y);
                leftLineView.DrawingLine(leftLineView.Width, leftLineView.Height);
                canvas.Children.Add(leftLineView);

                tempList.Add(leftLineView);

                LineDistanceView rightLineView = new LineDistanceView();
                rightLineView.LineDirectonType = LineDistanceView.DirectonType.Right;

                rightLineView.Angle = 0;
                rightLineView.Width = Math.Abs(canvas.Width - Canvas.GetLeft(frameworkElement) - frameworkElement.Width);//크기는 절대값이다. Canvas 영역을 벗을 낫을 경우를 대비.
                x = canvas.Width - rightLineView.Width;
                y = Canvas.GetTop(frameworkElement) + frameworkElement.Height / 2;
                rightLineView.ViewPoint = new Point(x, y);
                rightLineView.DrawingLine(rightLineView.Width, rightLineView.Height);
                canvas.Children.Add(rightLineView);

                tempList.Add(rightLineView);

                if (elementDistanceList.ContainsKey(element) == false)
                    elementDistanceList.Add(element, tempList);

                //element.DisplaySamePositionBetweenControls(canvas);//선택시에는 동일 선상의 라인 표시 컨트롤은 보이지 않도록 한다.
            }
             
        }

        public static void ResizeElementDistanceInfo(this UIElement element, double changedWidth, double changedHeight, Canvas canvas, Dictionary<UIElement, List<LineDistanceView>> elementDistanceList)
        {
            if (elementDistanceList == null || elementDistanceList.Count <= 0)
                return;
            if (elementDistanceList.Keys.Contains(element) != true)
                return;

            List<LineDistanceView> lineElementList = elementDistanceList[element];
            if (lineElementList == null || lineElementList.Count <= 0)
                return;
            FrameworkElement frameworkElement = element as FrameworkElement;
           
            if (frameworkElement != null)
            {
                double x=0.0, y=0.0;
                LineDistanceView topLineView = lineElementList.Where(o=>o.LineDirectonType == LineDistanceView.DirectonType.Top).SingleOrDefault();
                if (topLineView != null)
                {
                    topLineView.LineDirectonType = LineDistanceView.DirectonType.Top;
                    topLineView.Angle = 90;
                    topLineView.Height = Math.Abs(Canvas.GetTop(frameworkElement));//크기는 절대값이다. Canvas 영역을 벗을 낫을 경우를 대비.
                    x = Canvas.GetLeft(frameworkElement) + changedWidth / 2;
                    y = 0;
                    topLineView.ViewPoint = new Point(x, y);
                    topLineView.DrawingLine(topLineView.Width, topLineView.Height);                    
                }

                LineDistanceView bottomLineView = lineElementList.Where(o => o.LineDirectonType == LineDistanceView.DirectonType.Bottom).SingleOrDefault();
                if (bottomLineView != null)
                {
                    bottomLineView.LineDirectonType = LineDistanceView.DirectonType.Bottom;
                    bottomLineView.Angle = 270;
                    bottomLineView.Height = Math.Abs(canvas.Height - Canvas.GetTop(frameworkElement) - changedHeight);//크기는 절대값이다. Canvas 영역을 벗을 낫을 경우를 대비.
                    x = Canvas.GetLeft(frameworkElement) + changedWidth / 2;
                    y = canvas.Height - bottomLineView.Height;
                    bottomLineView.ViewPoint = new Point(x, y);
                    bottomLineView.DrawingLine(bottomLineView.Width, bottomLineView.Height);
                    //canvas.Children.Add(bottomLineView);
                }

                LineDistanceView leftLineView = lineElementList.Where(o => o.LineDirectonType == LineDistanceView.DirectonType.Left).SingleOrDefault();
                if (leftLineView != null)
                {
                    leftLineView.LineDirectonType = LineDistanceView.DirectonType.Left;

                    leftLineView.Angle = 0;
                    leftLineView.Width = Math.Abs(Canvas.GetLeft(frameworkElement));//크기는 절대값이다. Canvas 영역을 벗을 낫을 경우를 대비.
                    x = 0;
                    y = Canvas.GetTop(frameworkElement) + changedHeight / 2;
                    leftLineView.ViewPoint = new Point(x, y);
                    leftLineView.DrawingLine(leftLineView.Width, leftLineView.Height);
                    //canvas.Children.Add(leftLineView);
                }

                LineDistanceView rightLineView = lineElementList.Where(o => o.LineDirectonType == LineDistanceView.DirectonType.Right).SingleOrDefault();
                 if (leftLineView != null)
                 {
                     rightLineView.LineDirectonType = LineDistanceView.DirectonType.Right;

                     rightLineView.Angle = 0;
                     rightLineView.Width = Math.Abs(canvas.Width - Canvas.GetLeft(frameworkElement) - changedWidth);//크기는 절대값이다. Canvas 영역을 벗을 낫을 경우를 대비.
                     x = canvas.Width - rightLineView.Width;
                     y = Canvas.GetTop(frameworkElement) + changedHeight / 2;
                     rightLineView.ViewPoint = new Point(x, y);
                     rightLineView.DrawingLine(rightLineView.Width, rightLineView.Height);
                     //canvas.Children.Add(rightLineView);
                 }

                 element.DisplaySamePositionBetweenControls(canvas, false, ResizeDirection.All, false);
            }

        }

        public static void ReleaseElementDistanceInfo(this UIElement element, Canvas canvas, Dictionary<UIElement, List<LineDistanceView>> elementDistanceList)
        {
            if (elementDistanceList.ContainsKey(element) == true)
            {
                foreach (LineDistanceView item in elementDistanceList[element])
                {
                    canvas.Children.Remove(item);
                }
                elementDistanceList[element].Clear();
                elementDistanceList.Remove(element);
            }
        }

        public static void ResizeContentElementDistanceLine(this UIElement element, Canvas canvas, Dictionary<UIElement, List<LineDistanceView>> elementDistanceList, ResizeDirection resizeDirection)
        {
            FrameworkElement frameworkElement = element as FrameworkElement;
            if (elementDistanceList.ContainsKey(element) == true)
            {
                foreach (LineDistanceView item in elementDistanceList[element])
                {
                    if (item.LineDirectonType == LineDistanceView.DirectonType.Top)
                    {
                        item.Height = Math.Abs(Canvas.GetTop(frameworkElement));
                        double x = Canvas.GetLeft(frameworkElement) + frameworkElement.Width / 2;
                        double y = 0;
                        item.ViewPoint = new Point(x, y);
                        item.DrawingLine(item.Width, item.Height);
                    }
                    else if (item.LineDirectonType == LineDistanceView.DirectonType.Bottom)
                    {
                        item.Height = Math.Abs(canvas.Height - Canvas.GetTop(frameworkElement) - frameworkElement.Height);
                        double x = Canvas.GetLeft(frameworkElement) + frameworkElement.Width / 2;
                        double y = canvas.Height - item.Height;
                        item.ViewPoint = new Point(x, y);
                        item.DrawingLine(item.Width, item.Height);
                    }
                    else if (item.LineDirectonType == LineDistanceView.DirectonType.Left)
                    {
                        item.Width = Math.Abs(Canvas.GetLeft(frameworkElement));
                        double x = 0;
                        double y = Canvas.GetTop(frameworkElement) + frameworkElement.Height / 2;
                        item.ViewPoint = new Point(x, y);
                        item.DrawingLine(item.Width, item.Height);
                    }
                    else if (item.LineDirectonType == LineDistanceView.DirectonType.Right)
                    {
                        item.Width = Math.Abs(canvas.Width - Canvas.GetLeft(frameworkElement) - frameworkElement.Width);
                        double x = canvas.Width - item.Width;
                        double y = Canvas.GetTop(frameworkElement) + frameworkElement.Height / 2;
                        item.ViewPoint = new Point(x, y);
                        item.DrawingLine(item.Width, item.Height);
                    }
                }
            }

            element.DisplaySamePositionBetweenControls(canvas, false, resizeDirection, false);
        }

        public static void MoveContentElementDistanceLine(this UIElement element, Canvas canvas, Dictionary<UIElement, List<LineDistanceView>> elementDistanceList, bool isShowSameLine, bool isKewDown)
        {         
             FrameworkElement frameworkElement = element as FrameworkElement;
            if (elementDistanceList.ContainsKey(element) == true)
            {
                foreach (LineDistanceView item in elementDistanceList[element])
                {
                    if (item.LineDirectonType == LineDistanceView.DirectonType.Top)
                    {
                        item.Height = Math.Abs( Canvas.GetTop(frameworkElement));
                        double x = Canvas.GetLeft(frameworkElement) + frameworkElement.Width / 2;
                        double y = 0;
                        item.ViewPoint = new Point(x, y);
                        item.DrawingLine(item.Width, item.Height);
                    }
                    else if (item.LineDirectonType == LineDistanceView.DirectonType.Bottom)
                    {
                        item.Height = Math.Abs(canvas.Height - Canvas.GetTop(frameworkElement) - frameworkElement.Height);
                        double x = Canvas.GetLeft(frameworkElement) + frameworkElement.Width / 2;
                        double y = canvas.Height - item.Height;
                        item.ViewPoint = new Point(x, y);
                        item.DrawingLine(item.Width, item.Height);
                    }
                    else if (item.LineDirectonType == LineDistanceView.DirectonType.Left)
                    {
                        item.Width = Math.Abs(Canvas.GetLeft(frameworkElement));
                        double x = 0;
                        double y = Canvas.GetTop(frameworkElement) + frameworkElement.Height / 2;
                        item.ViewPoint = new Point(x, y);
                        item.DrawingLine(item.Width, item.Height);
                    }
                    else if (item.LineDirectonType == LineDistanceView.DirectonType.Right)
                    {
                        item.Width = Math.Abs(canvas.Width - Canvas.GetLeft(frameworkElement) - frameworkElement.Width);
                        double x = canvas.Width - item.Width;
                        double y = Canvas.GetTop(frameworkElement) + frameworkElement.Height / 2;
                        item.ViewPoint = new Point(x, y);
                        item.DrawingLine(item.Width, item.Height);
                    }
                }     
            }
          
           if (isShowSameLine == true)
               element.DisplaySamePositionBetweenControls(canvas, true, ResizeDirection.All, isKewDown);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="uiElement"></param>
        /// <param name="canvas"></param>
        /// <param name="isMoveState"> true: Move, false: Resize</param>
        public static void DisplaySamePositionBetweenControls(this UIElement uiElement, Canvas canvas, bool isMoveState, ResizeDirection resizeDirection, bool isKewDown)
        {
            FrameworkElement frameworkElement = uiElement as FrameworkElement;
            if (frameworkElement != null)
            {
                canvas.ClearLine();//현재 Canvas에 표시 되어 있는 컨트롤간의 동일 라인을 제거한다.

                switch (resizeDirection)
                {
                    case ResizeDirection.Top:
                        frameworkElement.MoniterTopPosition(canvas, Canvas.GetTop(frameworkElement), isMoveState, isKewDown);
                        break;
                    case ResizeDirection.Bottom:
                        frameworkElement.MoniterBottomPosition(canvas, Canvas.GetTop(frameworkElement) + frameworkElement.Height, isMoveState, isKewDown);
                        break;
                    case ResizeDirection.Right:
                        frameworkElement.MoniterRightPosition(canvas, Canvas.GetLeft(frameworkElement) + frameworkElement.Width, isMoveState, isKewDown);
                        break;
                    case ResizeDirection.Left:
                        frameworkElement.MoniterLeftPosition(canvas, Canvas.GetLeft(frameworkElement), isMoveState, isKewDown);
                        break;
                    case ResizeDirection.TopLeft:
                        frameworkElement.MoniterLeftPosition(canvas, Canvas.GetLeft(frameworkElement), isMoveState, isKewDown);
                        frameworkElement.MoniterTopPosition(canvas, Canvas.GetTop(frameworkElement), isMoveState, isKewDown);
                        break;
                    case ResizeDirection.TopRight:
                        frameworkElement.MoniterRightPosition(canvas, Canvas.GetLeft(frameworkElement) + frameworkElement.Width, isMoveState, isKewDown);
                        frameworkElement.MoniterTopPosition(canvas, Canvas.GetTop(frameworkElement), isMoveState, isKewDown);
                        break;
                    case ResizeDirection.BottomLeft:
                        frameworkElement.MoniterLeftPosition(canvas, Canvas.GetLeft(frameworkElement), isMoveState, isKewDown);
                        frameworkElement.MoniterBottomPosition(canvas, Canvas.GetTop(frameworkElement) + frameworkElement.Height, isMoveState, isKewDown);
                        break;
                    case ResizeDirection.BottomRight:
                        frameworkElement.MoniterRightPosition(canvas, Canvas.GetLeft(frameworkElement) + frameworkElement.Width, isMoveState, isKewDown);
                        frameworkElement.MoniterBottomPosition(canvas, Canvas.GetTop(frameworkElement) + frameworkElement.Height, isMoveState, isKewDown);
                        break;
                    case ResizeDirection.All:
                        {
                            frameworkElement.MoniterLeftPosition(canvas, Canvas.GetLeft(frameworkElement), isMoveState, isKewDown);
                            frameworkElement.MoniterRightPosition(canvas, Canvas.GetLeft(frameworkElement) + frameworkElement.Width, isMoveState, isKewDown);
                            frameworkElement.MoniterTopPosition(canvas, Canvas.GetTop(frameworkElement), isMoveState, isKewDown);
                            frameworkElement.MoniterBottomPosition(canvas, Canvas.GetTop(frameworkElement) + frameworkElement.Height, isMoveState, isKewDown);
                        }
                        break;
                }
                //frameworkElement.MoniterLeftPosition(canvas, Canvas.GetLeft(frameworkElement), isMoveState);
                //frameworkElement.MoniterRightPosition(canvas, Canvas.GetLeft(frameworkElement) + frameworkElement.Width, isMoveState);
                //frameworkElement.MoniterTopPosition(canvas, Canvas.GetTop(frameworkElement), isMoveState);
                //frameworkElement.MoniterBottomPosition(canvas, Canvas.GetTop(frameworkElement) + frameworkElement.Height, isMoveState);

                uiElement.MoniterCanvasPosition(canvas, isMoveState, isKewDown);
            }
        }

        public static void ClearLine(this Canvas canvas)
        {
            List<Line> lineControls = new List<Line>();
            foreach (UIElement item in canvas.Children)
            {
                if (item.GetType() == typeof(Line))
                {
                    lineControls.Add(item as Line);
                }
            }

            lineControls.ForEach(o => canvas.Children.Remove(o));
        }      

        /// <summary>
        /// 
        /// </summary>
        /// <param name="selectedElement"></param>
        /// <param name="canvas"></param>
        /// <param name="x"></param>
        /// <param name="isMoveState"> true: Move, false: Resize</param>
        /// <returns></returns>
        public static List<Point> MoniterLeftPosition(this UIElement selectedElement, Canvas canvas, double x, bool isMoveState, bool isKeyDown)
        {
            List<Line> lineControls = new List<Line>();

            List<Point> sameControlPoints = new List<Point>();
            foreach(UIElement targetItem in canvas.Children)
            {
                if (isKeyDown == true)//키보드로 이동시 라인표시 하지 않는다.
                    continue;

                if (selectedElement.Equals(targetItem) == true)
                    continue;

                if (InSysBasicControls.InSysControlManager.GetControlType(targetItem.GetType().Name) == InSysControlType.None)
                    continue;

                FrameworkElement targetElement = targetItem as FrameworkElement;
                FrameworkElement selectedFrameworkElement = selectedElement as FrameworkElement;
                if (targetElement == null)
                    continue;

                double targetItemLeftX1 = Canvas.GetLeft(targetElement);
                double targetItemRightX2 = targetItemLeftX1 + targetElement.Width;
                double targetTopY = Canvas.GetTop(targetElement);
                double targetBottomY = targetTopY + targetElement.Height;
                double selectedItemTopY = Canvas.GetTop(selectedFrameworkElement);
                double selectedItemBottomY = selectedItemTopY + selectedFrameworkElement.Height;

                if (x - 4 < targetItemLeftX1 && targetItemLeftX1 < x + 4)//이 영역에 들어오면 ...
                {
                    Line lineControl = new Line();
                    
                    double setXValue = targetItemLeftX1;

                    Canvas.SetLeft(selectedElement, setXValue);                    
                    if (isMoveState == false)
                    {
                        double diff = x - setXValue;
                        if (diff < 0)
                            selectedFrameworkElement.Width += diff; 
                        else if (diff > 0)
                            selectedFrameworkElement.Width += diff; 
                    }

                    if (targetTopY < selectedItemTopY)
                    {
                        lineControl.CreateLine(setXValue, targetTopY, setXValue, selectedItemTopY + selectedFrameworkElement.Height);
                    }
                    else if (targetTopY >= selectedItemTopY)
                    {
                        lineControl.CreateLine(setXValue, selectedItemTopY, setXValue, targetTopY + targetElement.Height);
                    }

                    lineControls.Add(lineControl);
                    break;
                }

                // 이동시 블렌드와 같이 동작하도록 수정 - 이동시 자신의 왼쪽과 타겟의 오른쪽은 체크하지 않는다. 단, 타겟의 Top과 Bottom 영역내일 경우에는 체크한다.
                if (x - 4 < targetItemRightX2 && targetItemRightX2 < x + 4)
                {
                    if ((targetTopY <= selectedItemTopY && targetBottomY >= selectedItemTopY) || (targetTopY <= selectedItemBottomY && targetBottomY >= selectedItemBottomY))
                    {
                        Line lineControl = new Line();
                        double setXValue = targetItemRightX2;

                        Canvas.SetLeft(selectedElement, setXValue);
                        if (isMoveState == false)//Resize 시 특정 영역 내에 들어온 거리만큼 선택된 Item의 크기를 조정한다.
                        {
                            double diff = x - targetItemRightX2;
                            if (diff < 0)
                                selectedFrameworkElement.Width += diff;
                            else if (diff > 0)
                                selectedFrameworkElement.Width += diff;
                        }
                        if (targetTopY < selectedItemTopY)
                        {
                            lineControl.CreateLine(setXValue, targetTopY, setXValue, selectedItemTopY + selectedFrameworkElement.Height);
                        }
                        else if (targetTopY >= selectedItemTopY)
                        {
                            lineControl.CreateLine(setXValue, selectedItemTopY, setXValue, targetTopY + targetElement.Height);
                        }

                        lineControls.Add(lineControl);
                        break;
                    }
                    else if ((selectedItemTopY <= targetTopY && targetTopY <= selectedItemBottomY) || (selectedItemTopY <= targetBottomY && targetBottomY <= selectedItemBottomY))
                    {
                        Line lineControl = new Line();
                        double setXValue = targetItemRightX2;

                        Canvas.SetLeft(selectedElement, setXValue);
                        if (isMoveState == false)//Resize 시 특정 영역 내에 들어온 거리만큼 선택된 Item의 크기를 조정한다.
                        {
                            double diff = x - targetItemRightX2;
                            if (diff < 0)
                                selectedFrameworkElement.Width += diff;
                            else if (diff > 0)
                                selectedFrameworkElement.Width += diff;
                        }
                        lineControl.CreateLine(setXValue, selectedItemTopY, setXValue, selectedItemBottomY);

                        lineControls.Add(lineControl);
                        break;
                    }
                }
                            

                //Console.WriteLine("X: ({0} - {1}) : {2}", targetItemLeftX1, targetItemRightX2, x);
            }

            foreach (Line item in lineControls)
            {
                canvas.Children.Add(item);
            }

            return sameControlPoints;
        }

        public static List<Point> MoniterRightPosition(this UIElement selectedElement, Canvas canvas, double x, bool isMoveState, bool isKeyDown)
        {
            //bool isIn = false;
            List<Line> lineControls = new List<Line>();
            List<Point> sameControlPoints = new List<Point>();
            
            foreach (UIElement targetItem in canvas.Children)
            {
                if (isKeyDown == true)//키보드로 이동시 라인표시 하지 않는다.
                    continue;

                if (selectedElement.Equals(targetItem) == true)
                    continue;

                if (InSysBasicControls.InSysControlManager.GetControlType(targetItem.GetType().Name) == InSysControlType.None)
                    continue;

                FrameworkElement targetElement = targetItem as FrameworkElement;
                FrameworkElement selectedFrameworkElement = selectedElement as FrameworkElement;
                if (targetElement == null)
                    continue;

                double targetItemLeftX1 = Canvas.GetLeft(targetElement);
                double targetItemRightX2 = targetItemLeftX1 + targetElement.Width;
                double targetTopY = Canvas.GetTop(targetElement);
                double targetBottomY = targetTopY + targetElement.Height;
                double selectedItemTopY = Canvas.GetTop(selectedFrameworkElement);
                double selectedItemBottomY = selectedItemTopY + selectedFrameworkElement.Height;
                
                // 이동시 블렌드와 같이 동작하도록 수정 - 이동시 자신의오른쪽과 타겟의 왼쪽은 체크하지 않는다.
                if (x - 4 < targetItemLeftX1 && targetItemLeftX1 < x + 4)
                {
                    if ((targetTopY <= selectedItemTopY && targetBottomY >= selectedItemTopY) || (targetTopY <= selectedItemBottomY && targetBottomY >= selectedItemBottomY))//이영역에 들어오면 ...
                    {
                        Line lineControl = new Line();
                        double setXValue = targetItemLeftX1;

                        if (isMoveState == false)//Resize 시 특정 영역 내에 들어온 거리만큼 선택된 Item의 크기를 조정한다.
                        {
                            double diff = x - targetItemLeftX1;
                            if (diff < 0)
                                selectedFrameworkElement.Width += (-diff);
                            else if (diff > 0)
                                selectedFrameworkElement.Width += (-diff);
                        }
                        else
                        {
                            Canvas.SetLeft(selectedElement, setXValue - selectedFrameworkElement.Width);
                        }
                        if (targetTopY < selectedItemTopY)
                        {
                            lineControl.CreateLine(setXValue, targetTopY, setXValue, selectedItemTopY + selectedFrameworkElement.Height);
                        }
                        else if (targetTopY >= selectedItemTopY)
                        {
                            lineControl.CreateLine(setXValue, selectedItemTopY, setXValue, targetTopY + targetElement.Height);
                        }

                        Point point1 = new Point(x, Canvas.GetLeft(targetElement));
                        Point point2 = new Point(x, Canvas.GetLeft(targetElement));
                        sameControlPoints.Add(new Point(x, Canvas.GetLeft(targetElement)));

                        lineControls.Add(lineControl);
                        break;
                    }
                    else if ((selectedItemTopY <= targetTopY && targetTopY <= selectedItemBottomY) || (selectedItemTopY <= targetBottomY && targetBottomY <= selectedItemBottomY))
                    {
                        Line lineControl = new Line();
                        double setXValue = targetItemLeftX1;

                        if (isMoveState == false)//Resize 시 특정 영역 내에 들어온 거리만큼 선택된 Item의 크기를 조정한다.
                        {
                            double diff = x - targetItemLeftX1;
                            if (diff < 0)
                                selectedFrameworkElement.Width += (-diff);
                            else if (diff > 0)
                                selectedFrameworkElement.Width += (-diff);
                        }
                        else
                        {
                            Canvas.SetLeft(selectedElement, setXValue - selectedFrameworkElement.Width);
                        }
                        lineControl.CreateLine(setXValue, selectedItemTopY, setXValue, selectedItemBottomY);

                        Point point1 = new Point(x, Canvas.GetLeft(targetElement));
                        Point point2 = new Point(x, Canvas.GetLeft(targetElement));
                        sameControlPoints.Add(new Point(x, Canvas.GetLeft(targetElement)));

                        lineControls.Add(lineControl);
                        break;
                    }
                }
                 
                if (x - 4 < targetItemRightX2 && targetItemRightX2 < x + 4)
                {
                    Line lineControl = new Line();                   
                    double setXValue = targetItemRightX2;
                    
                    if (isMoveState == false)//Resize 시 특정 영역 내에 들어온 거리만큼 선택된 Item의 크기를 조정한다.
                    {
                        double diff = (x) - (setXValue);
                        if (diff < 0)
                            selectedFrameworkElement.Width += (-diff);
                        else if (diff > 0)
                            selectedFrameworkElement.Width += (-diff);
                    }
                    else
                    {
                        Canvas.SetLeft(selectedElement, setXValue - selectedFrameworkElement.Width);
                    }
                    if (targetTopY < selectedItemTopY)
                    {
                        lineControl.CreateLine(setXValue, targetTopY, setXValue, selectedItemTopY + selectedFrameworkElement.Height);
                    }
                    else if (targetTopY >= selectedItemTopY)
                    {
                        lineControl.CreateLine(setXValue, selectedItemTopY, setXValue, targetTopY + targetElement.Height);
                    }

                    Point point1 = new Point(x, Canvas.GetLeft(targetElement));
                    Point point2 = new Point(x, Canvas.GetLeft(targetElement));
                    sameControlPoints.Add(new Point(x, Canvas.GetLeft(targetElement)));

                    lineControls.Add(lineControl);
                    break;
                }
                else
                {
                }
            }     

            foreach (Line item in lineControls)
            {
                canvas.Children.Add(item);
            }

            return sameControlPoints;
        }

        public static List<Point> MoniterTopPosition(this UIElement selectedElement, Canvas canvas, double y, bool isMoveState, bool isKeyDown)
        {
            List<Line> lineControls = new List<Line>();

            List<Point> sameControlPoints = new List<Point>();
            foreach (UIElement targetItem in canvas.Children)
            {
                if (isKeyDown == true)//키보드로 이동시 라인표시 하지 않는다.
                    continue;

                if (selectedElement.Equals(targetItem) == true)
                    continue;

                if (InSysBasicControls.InSysControlManager.GetControlType(targetItem.GetType().Name) == InSysControlType.None)
                    continue;

                FrameworkElement targetElement = targetItem as FrameworkElement;
                FrameworkElement selectedFrameworkElement = selectedElement as FrameworkElement;
                if (targetElement == null)
                    continue;

                double targetItemTopY1 = Canvas.GetTop(targetElement);
                double targetItemBottomY2 = targetItemTopY1 + targetElement.Height;
                double targetLeftX = Canvas.GetLeft(targetElement);
                double targetRightX = targetLeftX + targetElement.Width;
                double selectedItemLeftX = Canvas.GetLeft(selectedFrameworkElement);
                double selectedItemRightX = selectedItemLeftX + selectedFrameworkElement.Width;
                
                if (y - 4 < targetItemTopY1 && targetItemTopY1 < y + 4)//이영역에 들어오면 ...
                {
                    Line lineControl = new Line();
                   
                    double setYValue = targetItemTopY1;
                    
                    Canvas.SetTop(selectedElement, setYValue);//JunYoung : 컨트롤 Resize시 위치가 바뀌는 현상을 막음
                    if (isMoveState == false)//Resize 시 특정 영역 내에 들어온 거리만큼 선택된 Item의 크기를 조정한다.
                    {
                        double diff = y - setYValue;
                        if (diff < 0)
                            selectedFrameworkElement.Height += diff;
                        else if (diff > 0)
                            selectedFrameworkElement.Height += diff;
                    }
                  
                    if (targetLeftX < selectedItemLeftX)
                    {
                        lineControl.CreateLine(targetLeftX, setYValue, selectedItemLeftX + selectedFrameworkElement.Width, setYValue);
                    }
                    else if (targetLeftX >= selectedItemLeftX)
                    {
                        lineControl.CreateLine(selectedItemLeftX, setYValue, targetLeftX + targetElement.Width, setYValue);
                    }

                    Point point1 = new Point(y, Canvas.GetTop(targetElement));
                    Point point2 = new Point(y, Canvas.GetTop(targetElement));
                    sameControlPoints.Add(new Point(y, Canvas.GetTop(targetElement)));

                    lineControls.Add(lineControl);
                    break;
                }

                // 이동시 블렌드와 같이 동작하도록 수정 - 이동시 자신의 아래쪽과 타겟의 윗쪽은 체크하지 않는다.
                if (y - 4 < targetItemBottomY2 && targetItemBottomY2 < y + 4)
                {
                    if ((targetLeftX <= selectedItemLeftX && targetRightX >= selectedItemLeftX) || (targetLeftX <= selectedItemRightX && targetRightX >= selectedItemRightX))
                    {
                        Line lineControl = new Line();
                        double setYValue = targetItemBottomY2;

                        Canvas.SetTop(selectedElement, setYValue);//JunYoung : 컨트롤 Resize시 위치가 바뀌는 현상을 막음
                        if (isMoveState == false)//Resize 시 특정 영역 내에 들어온 거리만큼 선택된 Item의 크기를 조정한다.
                        {
                            double diff = y - setYValue;
                            if (diff < 0)
                                selectedFrameworkElement.Height += diff;
                            else if (diff > 0)
                                selectedFrameworkElement.Height += diff;
                        }
                        if (targetLeftX < selectedItemLeftX)
                        {
                            lineControl.CreateLine(targetLeftX, setYValue, selectedItemLeftX + selectedFrameworkElement.Width, setYValue);
                        }
                        else if (targetLeftX >= selectedItemLeftX)
                        {
                            lineControl.CreateLine(selectedItemLeftX, setYValue, targetLeftX + targetElement.Width, setYValue);
                        }
                        lineControls.Add(lineControl);
                        break;
                    }
                    else if ((selectedItemLeftX <= targetLeftX && targetLeftX <= selectedItemRightX) || (selectedItemLeftX <= targetRightX && targetRightX <= selectedItemRightX))
                    {
                        Line lineControl = new Line();
                        double setYValue = targetItemBottomY2;

                        Canvas.SetTop(selectedElement, setYValue);//JunYoung : 컨트롤 Resize시 위치가 바뀌는 현상을 막음
                        if (isMoveState == false)//Resize 시 특정 영역 내에 들어온 거리만큼 선택된 Item의 크기를 조정한다.
                        {
                            double diff = y - setYValue;
                            if (diff < 0)
                                selectedFrameworkElement.Height += diff;
                            else if (diff > 0)
                                selectedFrameworkElement.Height += diff;
                        }
                        lineControl.CreateLine(selectedItemLeftX, setYValue, selectedItemRightX, setYValue);
                        lineControls.Add(lineControl);
                        break;
                    }
                }
                 

                //Console.WriteLine("X: ({0} - {1}) : {2}", targetItemLeftX1, targetItemRightX2, x);
            }

            foreach (Line item in lineControls)
            {
                canvas.Children.Add(item);
            }

            return sameControlPoints;
        }

        public static List<Point> MoniterBottomPosition(this UIElement selectedElement, Canvas canvas, double y, bool isMoveState, bool isKeyDown)
        {
            List<Line> lineControls = new List<Line>();

            List<Point> sameControlPoints = new List<Point>();
            foreach (UIElement targetItem in canvas.Children)
            {
                if (isKeyDown == true)//키보드로 이동시 라인표시 하지 않는다.
                    continue;

                if (selectedElement.Equals(targetItem) == true)
                    continue;

                if (InSysBasicControls.InSysControlManager.GetControlType(targetItem.GetType().Name) == InSysControlType.None)
                    continue;

                FrameworkElement targetElement = targetItem as FrameworkElement;
                FrameworkElement selectedFrameworkElement = selectedElement as FrameworkElement;
                if (targetElement == null)
                    continue;

                double targetItemTopY1 = Canvas.GetTop(targetElement);
                double targetItemBottomY2 = targetItemTopY1 + targetElement.Height;

                double targetLeftX = Canvas.GetLeft(targetElement);
                double targetRightX = targetLeftX + targetElement.Width;
                double selectedItemLeftX = Canvas.GetLeft(selectedFrameworkElement);
                double selectedItemRightX = selectedItemLeftX + selectedFrameworkElement.Width;

                // Bottom으로 이동시 블렌드와 같이 하단 부분은 라인 체크하지 않는다.
                if (y - 4 < targetItemTopY1 && targetItemTopY1 < y + 4)
                {
                    if ((targetLeftX <= selectedItemLeftX && targetRightX >= selectedItemLeftX) || (targetLeftX <= selectedItemRightX && targetRightX >= selectedItemRightX))//이영역에 들어오면 ...
                    {
                        Line lineControl = new Line();
                        double setYValue = targetItemTopY1;

                        if (isMoveState == false)//Resize 시 특정 영역 내에 들어온 거리만큼 선택된 Item의 크기를 조정한다.
                        {
                            double diff = y - setYValue;
                            if (diff < 0)
                                selectedFrameworkElement.Height += (-diff);
                            else if (diff > 0)
                                selectedFrameworkElement.Height += (-diff);
                        }
                        else
                        {
                            Canvas.SetTop(selectedElement, setYValue - selectedFrameworkElement.Height);
                        }

                        if (targetLeftX < selectedItemLeftX)
                        {
                            lineControl.CreateLine(targetLeftX, setYValue, selectedItemLeftX + selectedFrameworkElement.Width, setYValue);
                        }
                        else if (targetLeftX >= selectedItemLeftX)
                        {
                            lineControl.CreateLine(selectedItemLeftX, setYValue, targetLeftX + targetElement.Width, setYValue);
                        }

                        lineControls.Add(lineControl);
                        break;
                    }
                    else if ((selectedItemLeftX <= targetLeftX && targetLeftX <= selectedItemRightX) || (selectedItemLeftX <= targetRightX && targetRightX <= selectedItemRightX))
                    {
                        Line lineControl = new Line();
                        double setYValue = targetItemTopY1;

                        if (isMoveState == false)//Resize 시 특정 영역 내에 들어온 거리만큼 선택된 Item의 크기를 조정한다.
                        {
                            double diff = y - setYValue;
                            if (diff < 0)
                                selectedFrameworkElement.Height += (-diff);
                            else if (diff > 0)
                                selectedFrameworkElement.Height += (-diff);
                        }
                        else
                        {
                            Canvas.SetTop(selectedElement, setYValue - selectedFrameworkElement.Height);
                        }

                        lineControl.CreateLine(selectedItemLeftX, setYValue, selectedItemRightX, setYValue);
                        lineControls.Add(lineControl);
                        break;
                    }
                }
                
                if (y - 4 < targetItemBottomY2 && targetItemBottomY2 < y + 4)
                {
                    Line lineControl = new Line();                   
                    double setYValue = targetItemBottomY2;

                    if (isMoveState == false)//Resize 시 특정 영역 내에 들어온 거리만큼 선택된 Item의 크기를 조정한다.
                    {
                        double diff = y - setYValue;
                        if (diff < 0)
                            selectedFrameworkElement.Height += (-diff);
                        else if (diff > 0)
                            selectedFrameworkElement.Height += (-diff);
                    }
                    else
                    {
                        Canvas.SetTop(selectedElement, setYValue - selectedFrameworkElement.Height);
                    }
                    if (targetLeftX < selectedItemLeftX)
                    {
                        lineControl.CreateLine(targetLeftX, setYValue, selectedItemLeftX + selectedFrameworkElement.Width, setYValue);
                    }
                    else if (targetLeftX >= selectedItemLeftX)
                    {
                        lineControl.CreateLine(selectedItemLeftX, setYValue, targetLeftX + targetElement.Width, setYValue);
                    }

                    lineControls.Add(lineControl);
                    break;
                }
                else
                {
                }

                //Console.WriteLine("X: ({0} - {1}) : {2}", targetItemLeftX1, targetItemRightX2, x);
            }


            foreach (Line item in lineControls)
            {
                canvas.Children.Add(item);
            }

            return sameControlPoints;
        }

        public static List<Line> MoniterCanvasPosition(this UIElement selectedElement, Canvas canvas, bool isMoveState, bool isKeyDown)
        {
            List<Line> lineControls = new List<Line>();
            FrameworkElement selectedFrameworkElement = selectedElement as FrameworkElement;
            double left = Canvas.GetLeft(selectedFrameworkElement);
            double top = Canvas.GetTop(selectedFrameworkElement);

            if (isKeyDown == true)//키보드로 이동시 라인표시 하지 않는다.
                return lineControls;

            //Canvas의 왼쪽 경계라인이 컨트롤의 왼쪽 라인과 겹칠때
            if (left - 4 < 0 && left + 4 > 0)
            {
                Line lineControl = new Line();
                if (isMoveState == true)
                    Canvas.SetLeft(selectedFrameworkElement, 0);
                lineControl.CreateLine(0, 0, 0, canvas.Height);
                lineControls.Add(lineControl);
            }

            //Canvas의 왼쪽 경계라인이 컨트롤의 오른쪽 라인과 겹칠때
            if (left + selectedFrameworkElement.Width - 4 < 0 && left + selectedFrameworkElement.Width + 4 > 0)
            {
                Line lineControl = new Line();
                if (isMoveState == true)
                    Canvas.SetLeft(selectedFrameworkElement, -selectedFrameworkElement.Width);
                lineControl.CreateLine(0, 0, 0, canvas.Height);
                lineControls.Add(lineControl);
            }

            //Canvas의 오른쪽 경계라인이 컨트롤의 오른쪽 라인과 겹칠때
            if (left + selectedFrameworkElement.Width - 4 < canvas.Width && left + selectedFrameworkElement.Width + 4 > canvas.Width)
            {
                Line lineControl = new Line();
                if (isMoveState == true)
                    Canvas.SetLeft(selectedFrameworkElement, canvas.Width - selectedFrameworkElement.Width);
                lineControl.CreateLine(canvas.Width, 0, canvas.Width, canvas.Height);
                lineControls.Add(lineControl);
            }

            //Canvas의 오른쪽 경계라인이 컨트롤의 왼쪽 라인과 겹칠때
            if (left - 4 < canvas.Width && left + 4 > canvas.Width)
            {
                Line lineControl = new Line();
                if (isMoveState == true)
                    Canvas.SetLeft(selectedFrameworkElement, canvas.Width);
                lineControl.CreateLine(canvas.Width, 0, canvas.Width, canvas.Height);
                lineControls.Add(lineControl);
            }

            //Canvas의 위쪽 경계라인이 컨트롤의 위쪽 라인과 겹칠때
            if (top - 4 < 0 && top + 4 > 0)
            {
                Line lineControl = new Line();
                if (isMoveState == true)
                    Canvas.SetTop(selectedFrameworkElement, 0);
                lineControl.CreateLine(0, 0, canvas.Width, 0);
                lineControls.Add(lineControl);
            }

            //Canvas의 위쪽 경계라인이 컨트롤의 아래쪽 라인과 겹칠때
            if (top + selectedFrameworkElement.Height - 4 < 0 && top + selectedFrameworkElement.Height + 4 > 0)
            {
                Line lineControl = new Line();
                if (isMoveState == true)
                    Canvas.SetTop(selectedFrameworkElement, -selectedFrameworkElement.Height);
                lineControl.CreateLine(0, 0, canvas.Width, 0);
                lineControls.Add(lineControl);
            }

            //Canvas의 아래쪽 경계라인이 컨트롤의 아래쪽 라인과 겹칠때
            if (top + selectedFrameworkElement.Height - 4 < canvas.Height && top + selectedFrameworkElement.Height + 4 > canvas.Height)
            {
                Line lineControl = new Line();
                if (isMoveState == true)
                    Canvas.SetTop(selectedFrameworkElement, canvas.Height - selectedFrameworkElement.Height);
                lineControl.CreateLine(0, canvas.Height, canvas.Width, canvas.Height);
                lineControls.Add(lineControl);
            }

            //Canvas의 아래쪽 경계라인이 컨트롤의 위쪽 라인과 겹칠때
            if (top - 4 < canvas.Height && top + 4 > canvas.Height)
            {
                Line lineControl = new Line();
                if (isMoveState == true)
                    Canvas.SetTop(selectedFrameworkElement, canvas.Height);
                lineControl.CreateLine(0, canvas.Height, canvas.Width, canvas.Height);
                lineControls.Add(lineControl);
            }

            foreach (Line item in lineControls)
            {
                canvas.Children.Add(item);
            }

            return lineControls;
        }

        public static void CreateLine(this Line line, double x1, double y1, double x2, double y2)
        {
            line.Stroke = new SolidColorBrush(Colors.Red);
            line.StrokeDashArray = new DoubleCollection() { 1 };
            line.StrokeThickness = 2;            

            //Canvas.SetLeft, Canvas.SetTop 에서 좌표를 설정하기 때문에 동일한 좌표인 경우 0으로 해서 처리한다.
            if (x1.Equals(x2) == true)
            {
                line.X1 = 0;
                line.Y1 = 0;
                line.X2 = 0;
                line.Y2 = y2 - y1;
            }
            else if (y1.Equals(y2) == true)
            {
                line.X1 = 0;
                line.Y1 = 0;
                line.X2 = x2 - x1;
                line.Y2 = 0;
            }            

            Canvas.SetLeft(line, x1);
            Canvas.SetTop(line, y1);
        }

        public static void SetEvent_InSysControlSizeChanged(this UIElement uiElement, SizeChangedEventHandler handler)
        {
            FrameworkElement frameworkElement = uiElement as FrameworkElement;
            if (frameworkElement != null)
            {
                frameworkElement.SizeChanged += handler;
            }
        }

        public static void SetEvent_InSysControlMouseUp(this UIElement uiElement, MouseButtonEventHandler handler)
        {
            FrameworkElement frameworkElement = uiElement as FrameworkElement;
            if (frameworkElement != null)
            {                
                //frameworkElement.MouseUp += handler;
                frameworkElement.PreviewMouseUp += handler;
            }
        }       
    }
}
