﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using InSysTouchflowData;
using InSysTouchflowData.Models.ProjectModels;
using InSysTouchflowData.Events;

namespace DesignWorkCanvas.Controllers
{
    public class WorkCanvasController
    {
        #region Delegates
        public delegate void SelectedPageDelegate(TouchflowPageInfo pageinfo);
        public delegate void ChangedPageDelegate(TouchflowPageInfo pageinfo);
        public delegate TouchflowPageInfo CurrentPageInfoDelegate();
        public delegate object OpenProgramFileDelegate(string filepath);
        public delegate void SavePageDelegate(TouchflowPageInfo pageInfo, string filepath);
        public delegate void SaveProjectDelegate(string filepath);
        public delegate TouchflowPageInfo CreateNewPageDelegate(double width, double height, double pageLiftTime, string pagename);
        public delegate TouchflowPageInfo CopyPageDelegate(TouchflowPageInfo copyPageInfo);
        public delegate void ReleaseAdornerDelegate();
        public delegate void DoAlignmentDelegate(AlignmentType alignmentType);
        public delegate void DoZoomDelegate(ZoomType zoomType, double zoomValue, bool isFixedState);
        public delegate void InitWorkCanvasDelegate(TouchflowProjectInfo projectInfo);
        public delegate void SendContentSelectionDelegate(object control);
        public delegate void RemovePageInfoDelegate(string pageGuid);
        public delegate void AddPageInfoDelegate(TouchflowPageInfo addPageInfo, ZoomCanvas zoomCanvas);

        public InsertInSysControlDelegate InsertInSysControlHandler; 
        public ActionEventDelegate ActionEventDelegateHandler;
        public SelectedPageDelegate SelectedPageHandler;
        public ChangedPageDelegate ChangedPageHandler;
        public SendContentSelectionDelegate SelectControlHandler;
        public CurrentPageInfoDelegate CurrentPageInfoHandler;
        public OpenProgramFileDelegate OpenPageHandler;
        public OpenProgramFileDelegate OpenProjectHandler;
        public SavePageDelegate SavePageHandler;
        public SaveProjectDelegate SaveProjectHandler;
        public CreateNewPageDelegate CreateNewPageHandler;
        public CopyPageDelegate CopyPageHandler;
        public ReleaseAdornerDelegate ReleaseAdornerHandler;
        public GetProjectInfoDelegate GetProjectInfoHandler;
        public DoAlignmentDelegate DoAlignmentHandler;
        public DoZoomDelegate DoZoomHandler;
        public InitWorkCanvasDelegate InitWorkCanvasHandler;
        public SendContentSelectionDelegate SendContentSelectionHandler;
        public RemovePageInfoDelegate RemovePageInfoHandler;
        public AddPageInfoDelegate AddPageInfoHandler;        

        #endregion

        private object _PropertyGrid;

        #region singleton 생성자

        private static WorkCanvasController _instance;
        public static WorkCanvasController Instance
        {
            get { return _instance; }
        }

        static WorkCanvasController()
        {
            _instance = new WorkCanvasController();
        }

        private WorkCanvasController()
        {
        }
        
        #endregion       

        public void InsertControl(FrameworkElement frameworkElement, Dictionary<string, object> properties)
        {
            if (this.InsertInSysControlHandler != null)
            {
                this.InsertInSysControlHandler(frameworkElement, properties);
            }            
        }

        /// <summary>
        /// Main측에 화면 업데이트 이벤트를 전달한다.
        /// </summary>
        /// <param name="view"></param>
         public void UpdateDesignView(UIElement view, TouchflowPageInfo pageinfo)
        {
            if (this.ActionEventDelegateHandler != null)
            {
                DesignView_UpdateEventArgs eventArgs = new DesignView_UpdateEventArgs(view, pageinfo);
                this.ActionEventDelegateHandler(InSysDelegateType.DesignView_UpdatedPage, eventArgs);
            }
        }

         public void InsertedPageOnDesignView(TouchflowPageInfo pageinfo)
         {
             if (this.ActionEventDelegateHandler != null)
             {
                 DesignView_InsertedPageEventArgs eventArgs = new DesignView_InsertedPageEventArgs(pageinfo);
                 this.ActionEventDelegateHandler(InSysDelegateType.DesignView_InsertedPage, eventArgs);
             }
         }

         public void ChangeCurrentPage(TouchflowPageInfo pageinfo)
         {
             if (ChangedPageHandler != null)
                 ChangedPageHandler(pageinfo);
         }

         public void DisplayPageOnWorkView(TouchflowPageInfo pageinfo)
         {
             if (SelectedPageHandler != null)
                 SelectedPageHandler(pageinfo);
         }

         public void SelectControl(object selectObj)
         {
             if (SelectControlHandler != null)
                 SelectControlHandler(selectObj);
         }

         public TouchflowPageInfo CurrentPageInfo
         {
             get
             {
                 if (CurrentPageInfoHandler != null)
                 {
                     TouchflowPageInfo pageinfo = CurrentPageInfoHandler();
                     return pageinfo;
                 }
                 return null;
             }
         }

         public void SavePage(TouchflowPageInfo pageInfo, string filepath)
         {
             if (SavePageHandler != null)
             {
                 SavePageHandler(pageInfo, filepath);
             }
         }

         public void SaveProject(string filepath)
         {
             if (SaveProjectHandler != null)
             {
                 SaveProjectHandler(filepath);
             }
         }

         public object OpenPage(string filepath)
         {
             if (OpenPageHandler != null)
             {
                 return OpenPageHandler(filepath);
             }

             return null;
         }

         public object OpenProject(string filepath)
         {
             if (OpenProjectHandler != null)
             {
                 return OpenProjectHandler(filepath);
             }

             return null;
         }

         
         public TouchflowPageInfo CopyPage(TouchflowPageInfo copyPageInfo)
         {
             if (this.CopyPageHandler != null)
             {
                 TouchflowPageInfo pageinfo = this.CopyPageHandler(copyPageInfo);

                 return pageinfo;
             }
             return null;
         }

         public void RemovePageInfo(string deletePageGuid)
         {
             if (RemovePageInfoHandler != null)
             {
                 RemovePageInfoHandler(deletePageGuid);
             }
         }

         public void AddPageInfo(TouchflowPageInfo addPageInfo, ZoomCanvas zoomCanvas)
         {
             if (AddPageInfoHandler != null)
             {
                 AddPageInfoHandler(addPageInfo, zoomCanvas);
             }
         }

         public void ReleaseAllAdorner()
         {
             if (ReleaseAdornerHandler != null)
             {
                 ReleaseAdornerHandler();
             }
         }

         public void SetPropertyGrid(object propertyGrid)
         {
             this._PropertyGrid = propertyGrid;
             InSysBasicControls.InSysControlManager.SetPropertyGrid(propertyGrid);
         }

         public void Init_SetInSysControl(TouchflowPageInfo pageInfo, UIElement element, List<TouchflowPageInfo> pageInfoList)
         {
             //List<PageInfo> list = GetProjectInfo().PageInfos;
             List<TouchflowPageInfo> movePagesList = pageInfoList.Where(o => o.PageGuid.Equals(pageInfo.PageGuid) != true).ToList();

             //if (this.SendContentSelectionHandler != null)
             //    this.SendContentSelectionHandler(element);

             InSysBasicControls.InSysControlManager.Init_SetControlActionEvent(element, movePagesList, pageInfo);

             InSysBasicControls.InSysControlManager.Init_SetControlEditProperty(element, pageInfo.PageLifeTime, pageInfo.ApplyLifeTime);
         }
               

         public TouchflowProjectInfo GetProjectInfo()
         {
             if (GetProjectInfoHandler != null)
                 return this.GetProjectInfoHandler();

             return null;
         }

         public void DoAlignmentItem(AlignmentType alignmentType)
         {
             if (this.DoAlignmentHandler != null)
             {
                 this.DoAlignmentHandler(alignmentType);
             }
         }

         public void DoZoom(ZoomType zoomType, double zoomValue, bool isFixedState)
         {
             if (this.DoZoomHandler != null)
             {
                 this.DoZoomHandler(zoomType, zoomValue, isFixedState);
             }
         }

         public void InitProjectSetting(TouchflowProjectInfo projectInfo)
         {
             if (this.InitWorkCanvasHandler != null)
             {
                 this.InitWorkCanvasHandler(projectInfo);                 
             }
         }

         public bool ChangedPageName(string pageGuid, string changedPageName)
         {
            

             return false;
         }

         public TouchflowPageInfo CreateNewPage(double width, double height, double pageLiftTime, string pagename)
         {
             if (this.CreateNewPageHandler != null)
             {
                 TouchflowPageInfo pageinfo = this.CreateNewPageHandler(width, height, pageLiftTime, pagename);

                 return pageinfo;
             }
             return null;
         }
    }
}
