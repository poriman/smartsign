﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows;
using iVisionTouchTimelineBody.Util;
using System.ComponentModel;
using iVisionTouchTimelineBody.View;

namespace iVisionTouchTimelineBody.Model
{
    public delegate void ChangedContentItemDataDelegate(ContentItemData contentItemData);
    public delegate void ChangedContentItemDelegate(ContentItemBoxData contentItemBoxData, ContentItemData contentItemData);

    public class TimelineData : ViewModelBase
    {
        public ObservableCollection<ContentItemBoxData> ContentItemBoxDataList
        {
            get;
            set;
        }

        public Dictionary<string, ObservableCollection<ContentItemBoxData>> TimelineContentsDictionary
        {
            get;
            set;
        }

        public bool AddTimelineContentItemBoxData(string pageid, ContentItemBoxData itembox)
        {
            if (TimelineContentsDictionary.Keys.Contains(pageid) == true)
            {
                ObservableCollection<ContentItemBoxData> list = TimelineContentsDictionary[pageid];
                ContentItemBoxData tempitembox = list.Where(o => o.Item.Equals(itembox.Item) == true).FirstOrDefault();
                if (tempitembox == null)
                {
                    list.Add(itembox);
                }                            
            }
            else
            {
                ObservableCollection<ContentItemBoxData> list = new ObservableCollection<ContentItemBoxData>();
                list.Add(itembox);
                TimelineContentsDictionary.Add(pageid, list);
            }

            return true;
        }

        public bool UpdateTimelineContentItemBoxData(string pageid, ContentItemBoxData itembox)
        {
            if (TimelineContentsDictionary.Keys.Contains(pageid) == true)
            {
                ObservableCollection<ContentItemBoxData> list = TimelineContentsDictionary[pageid];
                ContentItemBoxData tempitembox = list.Where(o => o.Item.Equals(itembox.Item) == true).FirstOrDefault();
                if (tempitembox != null)
                {
                    tempitembox.ContentItemDatas = itembox.ContentItemDatas;
                }
                else
                {
                    return false;
                }

                return true;
            }           

            return false;
        }

        public bool DeleteTimelineContentsDictionary(string pageid, ContentItemBoxData itembox)
        {
            if (TimelineContentsDictionary.Keys.Contains(pageid) == true)
            {
                ObservableCollection<ContentItemBoxData> list = TimelineContentsDictionary[pageid];
                list.Remove(itembox);
                TimelineContentsDictionary[pageid] = list;
            }
            else
            {
                return false;
            }

            return true;
        }

        public ObservableCollection<ContentItemBoxData> GetTimelineContents(string pageid)
        {
            if (TimelineContentsDictionary.Keys.Contains(pageid) == true)
            {
                ObservableCollection<ContentItemBoxData> list = TimelineContentsDictionary[pageid];
                return list;
            }            

            return null;
        }

        public ContentItemBoxData GetTimelineContentBox(string pageid, FrameworkElement element)
        {
            if (TimelineContentsDictionary.Keys.Contains(pageid) == true)
            {
                ObservableCollection<ContentItemBoxData> list = TimelineContentsDictionary[pageid];
                ContentItemBoxData currentItemBoxData = list.Where(o => o.Item.Equals(element) == true).SingleOrDefault();
                return currentItemBoxData;
            }

            return null;
        }

        public ContentItemData GetTimelineContentItem(string pageid, ContentItemBoxData contentItemBoxData, object element)
        {
            if (TimelineContentsDictionary.Keys.Contains(pageid) == true)
            {
                ObservableCollection<ContentItemBoxData> list = TimelineContentsDictionary[pageid];
                ContentItemBoxData itemBoxData = list.Where(o => o.Equals(contentItemBoxData) == true).SingleOrDefault();

                if (itemBoxData != null)
                {
                    var contentitem = itemBoxData.ContentItemDatas.Where(o => o.Playlistitem.Equals(element) == true).FirstOrDefault();
                    if (contentitem != null)
                    {
                        return contentitem;
                    }
                }
            }

            return null;
        }

        public void SetContentItemBoxData(string pageid)
        {
            if (TimelineContentsDictionary.Keys.Contains(pageid) == true)
            {
                ObservableCollection<ContentItemBoxData> list = TimelineContentsDictionary[pageid];
                this.ContentItemBoxDataList = list;
            }        
        }

        private TimelineHandUnitType timelineHandUnitType;
        public TimelineHandUnitType TimelineHandUnitType
        {
            get { return timelineHandUnitType; }
            set { timelineHandUnitType = value; OnPropertyChanged("TimelineHandUnitType"); }
        }

        private int timelineHandDistance;
        public int TimelineHandDistance
        {
            get { return timelineHandDistance; }
            set { timelineHandDistance = value; OnPropertyChanged("TimelineHandDistance"); }
        }

        private int timelineHandIntervalTime;
        public int TimelineHandIntervalTime
        {
            get { return timelineHandIntervalTime; }
            set { timelineHandIntervalTime = value; OnPropertyChanged("TimelineHandIntervalTime"); }
        }

        private double timelineTotalTime;
        public double TimelineTotalTime
        {
            get { return timelineTotalTime; }
            set { timelineTotalTime = value; OnPropertyChanged("TimelineTotalTime"); }
        }
      
        public TimelineData()
        {
            ContentItemBoxDataList = new ObservableCollection<ContentItemBoxData>();
            TimelineContentsDictionary = new Dictionary<string, ObservableCollection<ContentItemBoxData>>();
        }

        public void SetTimelineSetting(string tmelineHandUnitType, int timelineHandDistance, int timelineHandIntervalTime, double timelineTotalTime)
        {
            switch (tmelineHandUnitType)
            {
                case "Seconds":
                    this.timelineHandUnitType = TimelineHandUnitType.Seconds;
                    break;
                case "Minutes":
                    this.timelineHandUnitType = TimelineHandUnitType.Minutes;
                    break;
                case "Hours":
                    this.timelineHandUnitType = TimelineHandUnitType.Hours;
                    break;
                default:
                    this.timelineHandUnitType = TimelineHandUnitType.Seconds;
                    break;
            }
            this.timelineHandDistance = timelineHandDistance;
            this.timelineHandIntervalTime = timelineHandIntervalTime;
            this.timelineTotalTime = timelineTotalTime;
        }
    }

    public class ContentItemBoxData : ViewModelBase
    {
        private string contentTypeName;
        public string ContentTypeName
        {
            get { return contentTypeName; }
            set { contentTypeName = value; OnPropertyChanged("ContentTypeName"); }
        }

        private string contentName;
        public string ContentName
        {
            get { return contentName; }
            set { contentName = value; OnPropertyChanged("ContentName"); }
        }

        private double contentBoxLength;
        public double ContentBoxLength
        {
            get { return contentBoxLength; }
            set
            {
                contentBoxLength = value;
                OnPropertyChanged("ContentBoxLength");
            }
        }

        private double offset;
        public double Offset
        {
            get { return offset; }
            set { offset = value; OnPropertyChanged("Offset"); }
        }

        public ObservableCollection<ContentItemData> ContentItemDatas
        {
            get;
            set;
        }

        private object item;
        public object Item
        {
            get { return item; }
            set { item = value; OnPropertyChanged("Item"); }
        }     

        public ContentItemBoxData()
        {
            ContentItemDatas = new ObservableCollection<ContentItemData>();
            offset = 0.0;
        }
    }

    public class ContentItemData : ViewModelBase
    {
        private ImageSource thumbnail;
        public ImageSource Thumbnail
        {
            get { return thumbnail; }
            set { thumbnail = value; OnPropertyChanged("Thumbnail"); }
        }

        private string contentName;
        public string ContentName
        {
            get { return contentName; }
            set { contentName = value; OnPropertyChanged("ContentName"); }
        }

        private double contentLength;
        public double ContentLength
        {
            get { return contentLength; }
            set
            { 
                contentLength = value;
                OnPropertyChanged("ContentLength");               
            }
        }

        private double totalSeconds;
        public double TotalSeconds
        {
            get { return totalSeconds; }
            set
            {
                totalSeconds = value;
                OnPropertyChanged("TotalSeconds");
            }
        }

        private string contentPlayTime;
        public string ContentPlayTime
        {
            get { return contentPlayTime; }
            set { contentPlayTime = value; OnPropertyChanged("ContentPlayTime"); }
        }

        private string contentDisplayTime;
        public string ContentDisplayTime
        {
            get { return contentDisplayTime; }
            set { contentDisplayTime = value; OnPropertyChanged("ContentDisplayTime"); }
        }

        private int orderIndex;
        public int OrderIndex
        {
            get { return orderIndex; }
            set { orderIndex = value; OnPropertyChanged("OrderIndex"); }
        }

        private object playlistitem;
        public object Playlistitem
        {
            get { return playlistitem; }
            set { playlistitem = value; OnPropertyChanged("Playlistitem"); }
        }

        private bool isContentItem = true;
        public bool IsContentItem
        {
            get { return isContentItem; }
            set { isContentItem = value; OnPropertyChanged("IsContentItem"); }
        }
    }
}
