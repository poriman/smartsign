﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Collections.ObjectModel;
using iVisionTouchTimelineBody.Model;

namespace iVisionTouchTimelineBody.ViewModel
{
    public class TimelineBodyViewModel
    {
        public ObservableCollection<ContentItemBoxData> ContentItemBoxDatas
        {
            get;
            set;
        }

        public TimelineBodyViewModel()
        {
            ContentItemBoxDatas = new ObservableCollection<ContentItemBoxData>();
        }

        public void TimelineBody_Loaded(object sender, RoutedEventArgs e)
        {
        }

    }
}
