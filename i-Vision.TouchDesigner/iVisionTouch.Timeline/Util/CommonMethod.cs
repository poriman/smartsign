﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Media;
using System.Windows.Controls;
using UtilLib.StaticMethod;
using iVisionTouchTimelineBody.View;

namespace iVisionTouchTimelineBody.Util
{
    public static class CommonMethod
    {
        /// <summary>
        /// 스크롤 컨트롤을 찾는다.
        /// </summary>
        /// <returns></returns>
        private static ScrollViewer FindScroll(DependencyObject parent)
        {
            if (parent == null)
                return null;
            ListBox listbox = UIHelper.FindChild<ListBox>(parent, "ToolboxContainer");
            if (listbox == null)
                return null;

            Border scroll_border = VisualTreeHelper.GetChild(listbox, 0) as Border;
            if (scroll_border is Border)
            {
                ScrollViewer scroll = scroll_border.Child as ScrollViewer;

                if (scroll is ScrollViewer)
                {
                    return scroll;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 오른쪽으로 스크롤한다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public static void GoRightScrollViewer(ScrollViewer sv, object parent)
        {
            if (sv == null)
            {
                sv = FindScroll(parent as DependencyObject);
            }

            if (sv != null)
                sv.LineRight();
            else
                throw new Exception("Could not find ScrollViewer.");
        }

        /// <summary>
        /// 왼쪽으로 스크롤한다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public static void GoLeftScrollViewer(ScrollViewer sv, object parent)
        {
            if (sv == null)
            {
                sv = FindScroll(parent as DependencyObject);
            }

            if (sv != null)
                sv.LineLeft();
            else
                throw new Exception("Could not find ScrollViewer.");
        }

        /// <summary>
        /// 아래오 스크롤한다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public static void GoDownScrollViewer(ScrollViewer sv, object parent)
        {
            if (sv == null)
            {
                sv = FindScroll(parent as DependencyObject);
            }

            if (sv != null)
                sv.LineDown();
            else
                throw new Exception("Could not find ScrollViewer.");
        }

        /// <summary>
        /// 위로 스크롤한다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public static void GoUpScrollViewer(ScrollViewer sv, object parent)
        {
            if (sv == null)
            {
                sv = FindScroll(parent as DependencyObject);
            }

            if (sv != null)
                sv.LineUp();
            else
                throw new Exception("Could not find ScrollViewer.");
        }

        public static string GetTimeString(double time, TimelineHandUnitType tlHandType)
        {
            string displayTime = "";
       
            switch (tlHandType)
            {
                case TimelineHandUnitType.Seconds:
                    displayTime = TimeLineTimerPoint.DisplayTimeFromSeconds(time);
                    break;
                case TimelineHandUnitType.Minutes:
                    displayTime = TimeLineTimerPoint.DisplayTimeFromMinutes(time);
                    break;
                case TimelineHandUnitType.Hours:
                    displayTime = TimeLineTimerPoint.DisplayTimeFromHours(time);
                    break;
            }

            return displayTime;
        }
    }
}
