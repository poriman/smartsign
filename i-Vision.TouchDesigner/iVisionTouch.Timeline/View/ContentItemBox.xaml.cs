﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using iVisionTouchTimelineBody.Model;
using UtilLib.StaticMethod;

namespace iVisionTouchTimelineBody.View
{
    /// <summary>
    /// Interaction logic for ContentItemBox.xaml
    /// </summary>
    public partial class ContentItemBox : UserControl
    {
        public delegate string GetContentItemTimeDelegate(double totalLength);
        public GetContentItemTimeDelegate GetContentItemTimeEventHandler;

        public delegate double GetContentLengthDelegate(double length);
        public GetContentLengthDelegate GetContentLengthEventHandler;

        public ChangedContentItemDelegate ChangedContentItemEventHandler;

        public ContentItem.SetListBoxDragNDropDelegate SetListBoxDragNDropEventHandler;

        public ContentItemBox()
        {
            InitializeComponent();          
        }

        private void contentItem_Loaded(object sender, RoutedEventArgs e)
        {
            ContentItem contentItem = sender as ContentItem;
            if (contentItem != null)
            {
                contentItem.GetDisplayPositionEventHandler = new ContentItem.GetDisplayPositionDelegate(GetContentTimeAboutCurrentPosLength);
                ContentItem.GetTimeStringDelegateEventHandler = new ContentItem.GetTimeStringDelegate(GetContentItemPlayTime);
                contentItem.GetContentLengthEventHandler = new ContentItem.GetContentLengthDelegate(GetContentPositionLength);
                contentItem.SetListBoxDragNDropEventHandler = new ContentItem.SetListBoxDragNDropDelegate(SetListBoxDragNDrop);
                contentItem.ChangedContentItemEventHandler = new ChangedContentItemDelegate(ChangedContentItem);
            }
        }

        private void ChangedContentItem(ContentItemBoxData contentItemBoxData, ContentItemData contentItemData)
        {
            if (ChangedContentItemEventHandler != null)
                ChangedContentItemEventHandler(this.DataContext as ContentItemBoxData, contentItemData);
        }

        private void SetListBoxDragNDrop(bool isDrag)
        {
            if (SetListBoxDragNDropEventHandler != null)
                SetListBoxDragNDropEventHandler(isDrag);
        }

        public string GetContentTimeAboutCurrentPosLength(double contentItemLength, int contentItemOrderIndex)
        {
            if (contentItemBoxListBox != null)
            {
                double totalLength = 0;
                string displayTime = "";
                var items = contentItemBoxListBox.Items.OfType<ContentItemData>().Where(o => o.OrderIndex <= contentItemOrderIndex).ToList();
                items.ForEach(o => totalLength += o.ContentLength);

                if (GetContentItemTimeEventHandler != null)
                {                    
                    displayTime = GetContentItemTimeEventHandler(totalLength);
                    
                    return displayTime;
                }                
            }            
            return "";
        }

        public string GetContentItemPlayTime(double contentLength)
        {
            if (GetContentItemTimeEventHandler != null)
            {
                string playtime = GetContentItemTimeEventHandler(contentLength);
                return playtime;
            }
            return "";
        }

        public double GetContentPositionLength(double length)
        {
            if (GetContentLengthEventHandler != null)
            {
                return GetContentLengthEventHandler(length);
            }

            return 0.0;
        }

        public void ChangedContentLength(double length, TimelineHandUnitType type)
        {
            if (contentItemBoxListBox != null)
            {
                var items = contentItemBoxListBox.Items.OfType<ContentItemData>();
                foreach (var item in items)
                {
                    switch (type)
                    {
                        case TimelineHandUnitType.Minutes:
                            item.ContentLength  =item.ContentLength / 60;
                            break;
                    }
                }
            }
        }

        private double previousValue = 0.0;
        public void ScrollContentItemBox(double scrollValue)
        {
            ScrollViewer findChild = UIHelper.FindChild<ScrollViewer>(contentItemBoxListBox, "timelineContentItemBoxScroolViewer");
            if (findChild != null)
            {
                if (scrollValue >= 0)
                {
                    findChild.Margin = new Thickness(-scrollValue, 0, 0, 0);
                }               

                previousValue = scrollValue;
            }
        }

        public void changeContentItemBoxSize(double left, double width)
        {
            Border control = (Border)contentItemBoxListBox.Template.FindName("Bd", contentItemBoxListBox);
            if (control != null)
            {
                ContentItemBoxData data = contentItemBoxListBox.DataContext as ContentItemBoxData;
                if (data != null)
                {
                }
                control.Width = width;
                control.Margin = new Thickness(left, 0, 0, 0);
                control.Focus();
            }
        }
       
    }
}
