﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace iVisionTouchTimelineBody.View
{
    
    /// <summary>
    /// Interaction logic for TimelineHeader.xaml
    /// </summary>
    public partial class TimelineHeader : UserControl
    {
        public delegate double GetScrollableWidthDelegate();
        public delegate double GetScrollableHorizontalOffsetDelegate();
        public delegate void ChangedTimelineHeaderPostionDelegate(double timelineOffsetValue);
        public delegate void ChangedTimelineHandUnitTypeDelegate(TimelineHandUnitType previousTimelineHandUnitType, int previousTimelineHandDistance);
        public delegate void ChangedTimelineSettingDelegate(TimelineHandUnitType tmelineHandUnitType, int timelineHandDistance, int timelineHandIntervalTime, double timelineTotalTime);

        public GetScrollableWidthDelegate GetScrollableWidthHandler;
        public GetScrollableHorizontalOffsetDelegate GetScrollableHorizontalOffsetHandler;
        public ChangedTimelineHeaderPostionDelegate ChangedTimelineHeaderPostionEventHandler;
        public ChangedTimelineHandUnitTypeDelegate ChangedTimelineHandUnitTypeEventHandler;
        public ChangedTimelineSettingDelegate ChangedTimelineSettingEventHandler;

        private int timelineSecondsHandDistance = 5;
        private int timelineSecondsHandIntervalTime = 5;
        private int timelineMinutesHandDistance = 20;
        private int timelineMinutesHandIntervalTime = 1;
        private int timelineHoursHandDistance = 30;
        private int timelineHoursHandIntervalTime = 1;
        private bool isTimelineDrawing = false;
       
        private int defaultTimelineHeaderTime = 40 * 60; //40분, 초 단위
        private int defaultMinuteTime = 73;
        private int defaultHourTime = 27;
        public int TimelineHandIntervalTime { get; set; }
        public int TimelineHandDistance { get; set; }
        public double TimelineTotalTime{ get; set; }
        public TimelineHandUnitType TimelineHandUnitType  { get; set; }
        public double TimelineLength { get; set; }
        public double ContentBoxItemMaxTime { get; set; }
        
        bool isMouseDown = false;
        Cursor previousCursor = null;
        private Point previousPoint = new Point();
        private int mouseSpeed = 10;

        public TimelineHeader()
        {
            InitializeComponent();


            TimelineTotalTime = defaultTimelineHeaderTime;
            TimelineHandDistance = timelineSecondsHandDistance;
            TimelineHandIntervalTime = timelineSecondsHandIntervalTime;
            TimelineHandUnitType = TimelineHandUnitType.Seconds;

            this.Loaded += (s, e) =>
            {
                InitStringResource();
            };
        }

        private void applyBtn_Click(object sender, RoutedEventArgs e)
        {
            int intevalTime = 1;
            int distance = 5;
            int totalTime = 300;
            int.TryParse(this.intervalTextBox.Text, out intevalTime);
            int.TryParse(this.distanceTextBox.Text, out distance);
            int.TryParse(this.totalTimeTextBox.Text, out totalTime);
            TimelineHandUnitType timelineUnitType = (TimelineHandUnitType)this.timeUnitTypeComboBox.SelectedItem;

            RefreshTimeline(totalTime, intevalTime, distance, timelineUnitType);

            this.popup1.IsOpen = false;
        }     

        public void RefreshTimeline(double totalTime, int intevalTime, int distance, TimelineHandUnitType timelineUnitType)
        {
            this.RefreshTimelineHeader(totalTime, intevalTime, distance, timelineUnitType);

            if (this.ChangedTimelineSettingEventHandler != null)
                this.ChangedTimelineSettingEventHandler(this.TimelineHandUnitType, this.TimelineHandDistance, this.TimelineHandIntervalTime, this.TimelineTotalTime);            
        }

        public double TimelineHeaderLength
        {
            get
            {
                return timelineHeaderScroolViewer.ActualWidth;
            }
        }

        public void RefreshTimelineHeader(double totalTime, int intevalTime, int distance, TimelineHandUnitType timelineUnitType)
        {
            if (isTimelineDrawing == true)//처음 한번은 Timeline을 그린다.
            {
                if (totalTime <= this.TimelineTotalTime)
                    return;

                this.TimelineTotalTime = totalTime;
                if (this.ChangedTimelineSettingEventHandler != null)//메인 윈도우로 바뀐정보를 업데이트한다.
                    this.ChangedTimelineSettingEventHandler(this.TimelineHandUnitType, this.TimelineHandDistance, this.TimelineHandIntervalTime, this.TimelineTotalTime);                
            }
            double temp = 0.0;
            if (timelineUnitType == TimelineHandUnitType.Seconds)
            {
                double length = getContentItemWidth(totalTime);// totalTime * this.TimelineHandDistance;
                if (timelineHeaderScroolViewer.ActualWidth > length)
                    temp = timelineHeaderScroolViewer.ActualWidth;
                else
                    temp = length;               
            }
            else if (timelineUnitType == TimelineHandUnitType.Minutes)
            {
                double length = (totalTime/60) * this.TimelineHandDistance;
                if (timelineHeaderScroolViewer.ActualWidth > length)
                    temp = timelineHeaderScroolViewer.ActualWidth;
                else
                    temp = length;              
            }
            else if (timelineUnitType == TimelineHandUnitType.Hours)
            {
                double length = (totalTime / (60 * 60)) * this.TimelineHandDistance;
                if (timelineHeaderScroolViewer.ActualWidth > length)
                    temp = timelineHeaderScroolViewer.ActualWidth;
                else
                    temp = length;               
            }
          
            this.TimelineHandIntervalTime = intevalTime;
            this.TimelineHandDistance = distance;
            this.TimelineHandUnitType = timelineUnitType;
            //this.TimelineTotalTime = temp / distance;

            if (this.timelineHeaderCanvas != null)
            {
                this.timelineHeaderCanvas.Children.Clear();

                int index = 0;
                for (int i = 0; i <= temp; i++)
                {
                    if (i % intevalTime == 0)
                    {
                        this.InsertTimerHand(timelineUnitType, distance, i, index++);                        
                    }
                }
                this.timelineHeaderCanvas.Width = temp;
            }

            isTimelineDrawing = true;
        }

        private void SecondMenuItem_Checked(object sender, RoutedEventArgs e)
        {
            this.MenuItemCheck(secondMenuItem, true);
            this.MenuItemCheck(minuteMenuItem, false);
            this.MenuItemCheck(hourMenuItem, false);

            int previousHandDistance = this.TimelineHandDistance;
            TimelineHandUnitType tempType = this.TimelineHandUnitType;
            this.TimelineHandDistance = this.timelineSecondsHandDistance;
            //this.TimelineHandIntervalTime = this.timelineSecondsHandIntervalTime;
            this.TimelineHandUnitType = TimelineHandUnitType.Seconds;            
            //RefreshTimelineHeader(this.TimelineTotalTime, this.TimelineHandIntervalTime, this.TimelineHandDistance, TimelineHandUnitType.Seconds);
            ChangedTimelineHandUnitType(tempType, previousHandDistance);

            if (this.ChangedTimelineSettingEventHandler != null)
                this.ChangedTimelineSettingEventHandler(this.TimelineHandUnitType, this.TimelineHandDistance, this.TimelineHandIntervalTime, this.TimelineTotalTime);
        }

        private void MinuteMenuItem_Checked(object sender, RoutedEventArgs e)
        {
            this.MenuItemCheck(secondMenuItem, false);
            this.MenuItemCheck(minuteMenuItem, true);
            this.MenuItemCheck(hourMenuItem, false);

            int previousHandDistance = this.TimelineHandDistance;
            TimelineHandUnitType tempType = this.TimelineHandUnitType;
            this.TimelineHandDistance = this.timelineMinutesHandDistance;
            //this.TimelineHandIntervalTime = this.timelineMinutesHandIntervalTime;
            this.TimelineHandUnitType = TimelineHandUnitType.Minutes;
            //RefreshTimelineHeader(this.TimelineTotalTime, this.TimelineHandIntervalTime, this.TimelineHandDistance, TimelineHandUnitType.Minutes);
            ChangedTimelineHandUnitType(tempType, previousHandDistance);

            if (this.ChangedTimelineSettingEventHandler != null)
                this.ChangedTimelineSettingEventHandler(this.TimelineHandUnitType, this.TimelineHandDistance, this.TimelineHandIntervalTime, this.TimelineTotalTime);
        }

        private void HourMenuItem_Checked(object sender, RoutedEventArgs e)
        {
            this.MenuItemCheck(secondMenuItem, false);
            this.MenuItemCheck(minuteMenuItem, false);
            this.MenuItemCheck(hourMenuItem, true);

            int previousHandDistance = this.TimelineHandDistance;
            TimelineHandUnitType tempType = this.TimelineHandUnitType;
            this.TimelineHandDistance = this.timelineHoursHandDistance;
            //this.TimelineHandIntervalTime = this.timelineHoursHandIntervalTime;
            this.TimelineHandUnitType = TimelineHandUnitType.Hours;
            //RefreshTimelineHeader(this.TimelineTotalTime, this.TimelineHandIntervalTime, this.TimelineHandDistance, TimelineHandUnitType.Hours);
            ChangedTimelineHandUnitType(tempType, previousHandDistance);

            if (this.ChangedTimelineSettingEventHandler != null)
                this.ChangedTimelineSettingEventHandler(this.TimelineHandUnitType, this.TimelineHandDistance, this.TimelineHandIntervalTime, this.TimelineTotalTime);
        }

        private void MenuItemCheck(MenuItem menuitem, bool isChecked)
        {
            if (menuitem != null)
            {
                menuitem.IsChecked = isChecked;
            }
        }

        private void ChangedTimelineHandUnitType(TimelineHandUnitType previousTimelineHandUnitType, int timelineHandDistance)
        {
            if (ChangedTimelineHandUnitTypeEventHandler != null)
            {
                ChangedTimelineHandUnitTypeEventHandler(previousTimelineHandUnitType, timelineHandDistance);
            }
        }

        private void InsertTimerHandText(double distance, int timerHandTimeValue, int timerHandIndex, double top)
        {
            TextBlock tb = new TextBlock();
            tb.Foreground = new SolidColorBrush(Colors.White);
            tb.FontSize = 10;
            tb.SetValue(Canvas.LeftProperty, timerHandIndex * distance);
            tb.SetValue(Canvas.TopProperty, top);
            tb.Text = TimeLineTimerPoint.GetTimerHandTime(this.TimelineHandUnitType, timerHandTimeValue); ;
            this.timelineHeaderCanvas.Children.Add(tb);
        }

        private void InsertTimerHand(TimelineHandUnitType timelineUnitType, double distance, int timerHandTimeValue, int timerHandIndex)
        {
            double timerHandWidth = 1;
            double timerHandHeight = 12;
            string tooltipValue = "";

            switch (timelineUnitType)
            {
                case TimelineHandUnitType.Seconds:                    
                    if (timerHandTimeValue % 60 == 0)
                    {
                        timerHandWidth = 2;
                        timerHandHeight = 12;

                        this.InsertTimerHandText(distance, timerHandTimeValue, timerHandIndex, 0.0);                       
                    }
                    else if (timerHandTimeValue % 10 == 0)
                    {
                        timerHandWidth = 1;
                        timerHandHeight = 8;

                        //if (distance * 10 >= 35)
                        //{
                        //    this.InsertTimerHandText(distance, timerHandTimeValue, timerHandIndex, 3.0);   
                        //}
                    }
                    else
                    {
                        timerHandWidth = 1;
                        timerHandHeight = 5;
                    }
                    tooltipValue = TimeLineTimerPoint.DisplayTimeFromSeconds(timerHandTimeValue);                    
                    break;
                case TimelineHandUnitType.Minutes:
                    if (timerHandTimeValue % 60 == 0)
                    {
                        timerHandWidth = 2;
                        timerHandHeight = 12;
                        this.InsertTimerHandText(distance, timerHandTimeValue, timerHandIndex, 0.0);
                    }
                    else if (timerHandTimeValue % 10 == 0)
                    {
                        timerHandWidth = 1;
                        timerHandHeight = 8;
                        if (distance * 10 >= 35)
                        {
                            this.InsertTimerHandText(distance, timerHandTimeValue, timerHandIndex, 3.0);
                        }
                    }
                    else
                    {
                        timerHandWidth = 1;
                        timerHandHeight = 5;
                    }

                    tooltipValue = TimeLineTimerPoint.DisplayTimeFromMinutes(timerHandTimeValue);
                    break;
                case TimelineHandUnitType.Hours:
                    if (timerHandTimeValue % 24 == 0)
                    {
                        timerHandWidth = 2;
                        timerHandHeight = 12;
                        this.InsertTimerHandText(distance, timerHandTimeValue, timerHandIndex, 0.0);  
                    }
                    else
                    {
                        timerHandWidth = 1;
                        timerHandHeight = 8;
                    }                    
                   
                    tooltipValue = TimeLineTimerPoint.DisplayTimeFromHours(timerHandTimeValue);
                    break;
                default:
                    break;
            }

            TimeLineTimerPoint timeline = new TimeLineTimerPoint(timelineUnitType, distance, timerHandTimeValue, timerHandIndex, timerHandWidth, timerHandHeight, tooltipValue);

            this.timelineHeaderCanvas.Children.Add(timeline);
        }

        private void settingTimelineHeader_Click(object sender, RoutedEventArgs e)
        {
            timeUnitTypeComboBox.SelectedItem = this.TimelineHandUnitType;
            this.intervalTextBox.Text = this.TimelineHandIntervalTime.ToString();
            this.distanceTextBox.Text = this.TimelineHandDistance.ToString();
            this.totalTimeTextBox.Text = this.TimelineTotalTime.ToString();

            this.popup1.IsOpen = true;
        }

        private void cancelBtn_Click(object sender, RoutedEventArgs e)
        {
            this.popup1.IsOpen = false;
        }

        private void closeBtn_Click(object sender, RoutedEventArgs e)
        {
            this.popup1.IsOpen = false;
        }

        public double GetTimelineHeaderLength()
        {
            double length = timelineHeaderScroolViewer.ActualWidth;
            return length;
        }

        private void timelineHeaderCanvas_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            //previousCursor = this.timelineHeaderCanvas.Cursor;
            isMouseDown = true;
            this.timelineHeaderCanvas.Cursor = Cursors.Hand;
            previousPoint = e.GetPosition(this);
        }

        private void timelineHeaderCanvas_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            isMouseDown = false;
            this.timelineHeaderCanvas.Cursor = previousCursor;
        }

        private void timelineHeaderCanvas_MouseMove(object sender, MouseEventArgs e)
        {
            if (isMouseDown == true)
            {
                if (GetScrollableWidthHandler != null)
                {
                    if (GetScrollableWidthHandler() == 0.0)
                        return;
                }

               
                Point point = e.GetPosition(this);
                double changedX = point.X - this.previousPoint.X;

                if (GetScrollableHorizontalOffsetHandler != null)
                {
                    if (GetScrollableHorizontalOffsetHandler() == 0.0 && changedX < 0)
                        return;
                }


                changedX = -changedX;

                if (timelineHeaderScroolViewer.ScrollableWidth == timelineHeaderScroolViewer.HorizontalOffset + changedX)
                    return;
                timelineHeaderScroolViewer.ScrollToHorizontalOffset(timelineHeaderScroolViewer.HorizontalOffset + changedX);

                if (ChangedTimelineHeaderPostionEventHandler != null)
                {                   
                    ChangedTimelineHeaderPostionEventHandler(timelineHeaderScroolViewer.HorizontalOffset + changedX);
                }
                /*
                if (changedX > 0)
                {
                    changedX = -changedX;
                    timelineHeaderScroolViewer.ScrollToHorizontalOffset(timelineHeaderScroolViewer.HorizontalOffset + changedX);
                    //this.timelineHeaderScroolViewer.LineRight();
                }
                else if (changedX < 0)
                {
                    changedX = -changedX;
                    timelineHeaderScroolViewer.ScrollToHorizontalOffset(timelineHeaderScroolViewer.HorizontalOffset + changedX);
                    //this.timelineHeaderScroolViewer.LineLeft();
                }
                */
                previousPoint = point;
                this.timelineHeaderCanvas.Cursor = previousCursor;
            }
        }

        protected override void OnMouseLeave(MouseEventArgs e)
        {
            base.OnMouseLeave(e);

            isMouseDown = false;
        }

        public void ChangeTimelineHeaderScroolViewer(double horizontalChange)
        {
            timelineHeaderScroolViewer.ScrollToHorizontalOffset(horizontalChange + timelineHeaderScroolViewer.HorizontalOffset);
        }

        int scrollSpeed = 0;
        double moveValue = 7;
        protected override void OnPreviewKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);

            if (e.Key == Key.Left)
            {
                ChangeTimelineHeaderScroolViewer(-moveValue);
                ChangedTimelineHeaderPostionEventHandler(timelineHeaderScroolViewer.HorizontalOffset - moveValue);
            }
            else if (e.Key == Key.Right)
            {
                ChangeTimelineHeaderScroolViewer(moveValue);
                ChangedTimelineHeaderPostionEventHandler(timelineHeaderScroolViewer.HorizontalOffset + moveValue);
            }
            if (scrollSpeed == 20)
                moveValue = 14;
            else if (scrollSpeed == 40)
                moveValue = 20;
            scrollSpeed++;
            e.Handled = true;
        }

        protected override void OnPreviewKeyUp(KeyEventArgs e)
        {
            base.OnPreviewKeyUp(e);
            scrollSpeed = 0;
            moveValue = 7;
        }

        private void UserControl_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            TimelineHeader timelineheader = sender as TimelineHeader;
            //this.TimelineTotalTime = getContentItemTimeSeconds(timelineheader.ActualWidth);
        }

        public double getContentItemTimeSeconds(double contentLength)
        {
            double value = Math.Floor(contentLength / this.TimelineHandDistance) * (double)this.TimelineHandIntervalTime;
            
            return value;
        }

        public double getContentItemWidth(double time)
        {
            double value = time;
            switch (this.TimelineHandUnitType)
            {
                case TimelineHandUnitType.Seconds:
                    value = (time * (double)this.TimelineHandDistance) / (double)this.TimelineHandIntervalTime;
                    break;
                case TimelineHandUnitType.Minutes:
                    value = ((time / 60) * this.TimelineHandDistance) / this.TimelineHandIntervalTime;
                    break;
                case TimelineHandUnitType.Hours:
                    value = ((time / (60 * 60)) * this.TimelineHandDistance) / this.TimelineHandIntervalTime;
                    break;
            }
            return value;
        }

        private void InitStringResource()
        {
            //this.secondMenuItem.Header = Cultures.Resources.ContentName;
            //this.minuteMenuItem.Header = Cultures.Resources.ContentPlayTime;
            //this.hourMenuItem.Header = Cultures.Resources.ContentPlayTime;
            //this.settingTimelineHeader.Header = Cultures.Resources.ContentPlayTime;
        }
    }

    public class TimeLineTimerPoint : ContentControl
    {
        private Line line;        
        private int timerHandIndex = 0;
        private int timerHandTimeValue = 0;

        public TimeLineTimerPoint(TimelineHandUnitType timelineUnitType, double distance, int timerHandTimeValue, int timerHandIndex, double timerHandWidth, double timerHandHeight, string tooltipValue)
        {
            this.timerHandIndex = timerHandIndex;
            this.timerHandTimeValue = timerHandTimeValue;
            line = new Line();
            line.Stroke = new SolidColorBrush(Color.FromArgb(0xFF, 102, 102, 102));
            
            line.X1 = 0;
            line.X2 = 0;
            line.Y1 = 0;

            line.SetValue(ToolTipService.InitialShowDelayProperty, 0);
            line.SetValue(ToolTipService.ShowDurationProperty, 10000);

            line.StrokeThickness = timerHandWidth;
            line.Y2 = timerHandHeight;
            line.ToolTip = tooltipValue;

            this.SetValue(Canvas.LeftProperty, this.timerHandIndex * distance);
            this.SetValue(Canvas.BottomProperty, 0.0);

            this.Content = line;
            
        }

        public static string GetTimerHandTime(TimelineHandUnitType timelineUnitType, double d)
        {
            if (timelineUnitType == TimelineHandUnitType.Seconds)
            {
                TimeSpan ts = TimeSpan.FromSeconds(d);

                return string.Format("{0}:{1:D2}:{2:D2}", ts.Hours, ts.Minutes, ts.Seconds);
            }
            else if (timelineUnitType == TimelineHandUnitType.Minutes)
            {
                TimeSpan ts = TimeSpan.FromMinutes(d);
                return string.Format("{0}:{1:D2}:{2:D2}", ts.Hours, ts.Minutes, ts.Seconds);
            }
            else
            {
                TimeSpan ts = TimeSpan.FromHours(d);
                return string.Format("{0} {1}", ts.Days, Cultures.Resources.Day);
            }
        }

        public static string DisplayTimeFromSeconds(double d)
        {
            TimeSpan ts = TimeSpan.FromSeconds(d);

            if (ts.Hours == 0 && ts.Minutes == 0)
            {
                return string.Format("0:0:{0}", ts.Seconds);
            }
            else if (ts.Hours == 0)
            {
                if (ts.Seconds == 0)
                    return string.Format("0:0:{0}", ts.Minutes);
                else
                    return string.Format("0:{0}:{1}", ts.Minutes, ts.Seconds);
            }
            else
            {
                if (ts.Seconds == 0)
                    return string.Format("{0}:{1}:0", ts.Hours, ts.Minutes);
                else
                    return string.Format("{0}:{1}:{2}", ts.Hours, ts.Minutes, ts.Seconds);
            }

        }

        public static string DisplayTimeFromMinutes(double d)
        {
            TimeSpan ts = TimeSpan.FromMinutes(d);

            if (ts.Hours == 0)
            {
                return string.Format("0:{0}:0", ts.Minutes);
            }
            else
            {
                if (ts.Minutes == 0)
                    return string.Format("{0}:0:0", ts.Hours, Cultures.Resources.Hour);
                else
                    return string.Format("{0}:{1}:0", ts.Hours, ts.Minutes);
            }

        }

        public static string DisplayTimeFromHours(double d)
        {
            TimeSpan ts = TimeSpan.FromHours(d);

            return string.Format("{0}:0:0", ts.Hours);
        }
    }

    public class TimeLineUnitInfo
    {
        public static TimelineHandUnitType TimelineHandUnitType;
        public static int UnitIntervalTime;
        public static int UnitDistance;

        public TimeLineUnitInfo(TimelineHandUnitType unitType, int unitIntervalTime, int unitDistance)
        {
            TimelineHandUnitType = unitType;
            UnitDistance = unitDistance;
            UnitIntervalTime = unitIntervalTime;
        }
    }

    public enum TimelineHandUnitType : int
    {
        Seconds,
        Minutes,
        Hours
    }
}
