﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Caliburn.Core;
using Caliburn.PresentationFramework;
using iVisionTouchTimelineBody.Model;
using iVisionTouchTimelineBody.Util;
using UtilLib.StaticMethod;
using System.Globalization;
using iVisionTouchTimelineBody.Cultures;
using System.Threading;

namespace iVisionTouchTimelineBody.View
{
    /// <summary>
    /// Interaction logic for ContentPanelBox.xaml
    /// </summary>
    public partial class TimelineBody : UserControl
    {
        private double timelineOffset;
        private TimelineHeader timelineHeader;
        private List<ContentItemBox> ContentItemBoxList = new List<ContentItemBox>();
        public delegate void ChangedTimelineSettingDelegate(string tmelineHandUnitType, int timelineHandDistance, int timelineHandIntervalTime, double timelineTotalTime); 

        public ChangedTimelineSettingDelegate UpdatedTimelineContentItemBoxEventHandler;

        public ChangedContentItemDelegate ChangedContentItemEventHandler;

        #region DoubleClickUserEvent : TimelineBody의 Item이 Double클릭됐을 경우 호출.

        public static readonly RoutedEvent TimelineContentDoubleClickEvent = EventManager.RegisterRoutedEvent("TimelineContentDoubleClick", RoutingStrategy.Direct, typeof(RoutedEventHandler), typeof(TimelineBody));

        public event RoutedEventHandler TimelineContentDoubleClick
        {
            add { AddHandler(TimelineContentDoubleClickEvent, value); }
            remove { RemoveHandler(TimelineContentDoubleClickEvent, value); }
        }

        private void RaiseTimelineContentDoubleClickEvent(object source)
        {
            RoutedEventArgs newEventArg = new RoutedEventArgs(TimelineBody.TimelineContentDoubleClickEvent, source);
            RaiseEvent(newEventArg);
        }

        #endregion

        #region TimelineContentSelectChangedEvent : TimelineBody의 Item이 Double클릭됐을 경우 호출.

        public static readonly RoutedEvent TimelineContentSelectChangedEvent = EventManager.RegisterRoutedEvent("TimelineContentSelectChanged", RoutingStrategy.Direct, typeof(RoutedEventHandler), typeof(TimelineBody));

        public event RoutedEventHandler TimelineContentSelectChanged
        {
            add { AddHandler(TimelineContentSelectChangedEvent, value); }
            remove { RemoveHandler(TimelineContentSelectChangedEvent, value); }
        }

        private void RaiseTimelineContentSelectChangedEvent(object source)
        {
            RoutedEventArgs newEventArg = new RoutedEventArgs(TimelineBody.TimelineContentSelectChangedEvent, source);
            RaiseEvent(newEventArg);
        }

        #endregion

        public TimelineBody()
        {
            CaliburnFramework.ConfigureCore().WithPresentationFramework().Start();

            InitializeComponent();

            this.Loaded += (s, e) =>
            {
                if (timelineHeader == null)
                {
                    timelineHeader = UIHelper.FindChild<TimelineHeader>(this, "timelineHeader");
                    InitStringResource();
                    //Init_Timeline();
                }
            };            
        }

        public void Init_Timeline()
        {
            if (timelineHeader != null)
            {
                this.timelineHeader.ChangedTimelineHeaderPostionEventHandler = new TimelineHeader.ChangedTimelineHeaderPostionDelegate(ChangedTimelineHeaderPostion);
                this.timelineHeader.ChangedTimelineHandUnitTypeEventHandler = new TimelineHeader.ChangedTimelineHandUnitTypeDelegate(ChangedTimelineHandUnitType);
                this.timelineHeader.ChangedTimelineSettingEventHandler = new TimelineHeader.ChangedTimelineSettingDelegate(UpdatedTimelineContentItemBox);
                this.timelineHeader.GetScrollableWidthHandler = new TimelineHeader.GetScrollableWidthDelegate(GetScrollableWidth);
                this.timelineHeader.GetScrollableHorizontalOffsetHandler = new TimelineHeader.GetScrollableHorizontalOffsetDelegate(GetScrollableHorizontalOffset);

                //timelineHeader.TimelineTotalTime = 300;
                //timelineHeader.TimelineHandDistance = 7;
                //timelineHeader.TimelineHandIntervalTime = 1;
                //timelineHeader.TimelineHandUnitType = TimelineHandUnitType.Seconds;

                timelineHeader.RefreshTimeline(timelineHeader.TimelineTotalTime, timelineHeader.TimelineHandIntervalTime, timelineHeader.TimelineHandDistance, timelineHeader.TimelineHandUnitType);
            }
        }

        public double TimelineBodyContentLength
        {
            get
            {
                return TimelineContentListBox.ActualWidth - 70; //Time Body의 Content Element의 타입 영역을 뺀 실제 길이 표시 부분의 크기
            }
        }

        public void Update_TimelineModelData(TimelineData timelineData)
        {
            timelineData.TimelineTotalTime = this.timelineHeader.TimelineTotalTime;
            timelineData.TimelineHandUnitType = this.timelineHeader.TimelineHandUnitType;
            timelineData.TimelineHandIntervalTime = this.timelineHeader.TimelineHandIntervalTime;
            timelineData.TimelineHandDistance = this.timelineHeader.TimelineHandDistance;
        }

        private void ChangedTimelineHeaderPostion(double timelineOffsetValue)
        {
            if (timelineOffsetValue < 0)
                timelineOffsetValue = 0;
            timelineOffset = timelineOffsetValue;
            
            //ContentItemBoxList.ForEach(o => o.ScrollContentItemBox(timelineOffsetValue));

            if (scrollViewer != null)
            {
                if (scrollViewer.ScrollableWidth == 0)
                    return;
                scrollViewer.ScrollToHorizontalOffset(timelineOffset);
            }
        }

        private double GetScrollableHorizontalOffset()
        {
            if (scrollViewer != null)
            {
                double value = scrollViewer.ScrollableWidth - scrollViewer.HorizontalOffset;
                return value;
            }
            return 0.0;
        }

        private double GetScrollableWidth()
        {
            if (scrollViewer != null)
            {
                return scrollViewer.ScrollableWidth;
            }
            return 0.0;
        }

        public double GetContentItemLength(double time)
        {
            double value = time;
            switch (this.timelineHeader.TimelineHandUnitType)
            {
                case TimelineHandUnitType.Seconds:
                    value = time * this.timelineHeader.TimelineHandDistance / this.timelineHeader.TimelineHandIntervalTime; 
                    break;
                case TimelineHandUnitType.Minutes:
                    value = (time / 60) * this.timelineHeader.TimelineHandDistance / this.timelineHeader.TimelineHandIntervalTime; 
                    break;
                case TimelineHandUnitType.Hours:
                    value = (time / (60 * 60)) * this.timelineHeader.TimelineHandDistance / this.timelineHeader.TimelineHandIntervalTime; 
                    break;
            }
            return value;
        }

        private void ChangedTimelineHandUnitType(TimelineHandUnitType previousTimelineHandUnitType, int previousTimelineHandDistance)
        {
            int temp = 1;
            switch (previousTimelineHandUnitType)
            {
                case TimelineHandUnitType.Seconds:
                    if (this.timelineHeader.TimelineHandUnitType == TimelineHandUnitType.Minutes)
                        temp = 60;
                    else
                        temp = 60 * 60;
                    break;
                case TimelineHandUnitType.Minutes:
                    if (this.timelineHeader.TimelineHandUnitType == TimelineHandUnitType.Seconds)
                        temp = 60;
                    else
                        temp = 60;
                    break;
                case TimelineHandUnitType.Hours:
                    if (this.timelineHeader.TimelineHandUnitType == TimelineHandUnitType.Seconds)
                        temp = 60 * 60;
                    else
                        temp = 60;
                    break;
            }
          
            var items = TimelineContentListBox.Items.OfType<ContentItemBoxData>();
            foreach (var item in items)
            {
                switch (this.timelineHeader.TimelineHandUnitType)
                {
                    case TimelineHandUnitType.Seconds:
                        foreach (var contentItemData in item.ContentItemDatas)
                        {
                            double contentLengthTemp = (contentItemData.ContentLength / previousTimelineHandDistance) * this.timelineHeader.TimelineHandDistance / this.timelineHeader.TimelineHandIntervalTime;
                            contentItemData.ContentLength = contentLengthTemp * temp;
                            contentItemData.TotalSeconds = (contentItemData.ContentLength / previousTimelineHandDistance);
                        }
                        break;
                    case TimelineHandUnitType.Minutes:
                        foreach (var contentItemData in item.ContentItemDatas)
                        {
                            double contentLengthTemp = (contentItemData.ContentLength / previousTimelineHandDistance) * this.timelineHeader.TimelineHandDistance / this.timelineHeader.TimelineHandIntervalTime;
                            if (previousTimelineHandUnitType == TimelineHandUnitType.Seconds)
                                contentItemData.ContentLength = contentLengthTemp / temp;
                            else
                                contentItemData.ContentLength = contentLengthTemp * temp;
                        }
                        break;
                    case TimelineHandUnitType.Hours:
                        foreach (var contentItemData in item.ContentItemDatas)
                        {
                            double contentLengthTemp = (contentItemData.ContentLength / previousTimelineHandDistance) * this.timelineHeader.TimelineHandDistance / this.timelineHeader.TimelineHandIntervalTime;
                            contentItemData.ContentLength = contentLengthTemp / temp;                            
                        }
                        break;
                }
            }
          
            if (this.timelineHeader.ContentBoxItemMaxTime > this.timelineHeader.TimelineTotalTime)
                this.timelineHeader.RefreshTimelineHeader(this.timelineHeader.ContentBoxItemMaxTime, this.timelineHeader.TimelineHandIntervalTime, this.timelineHeader.TimelineHandDistance, this.timelineHeader.TimelineHandUnitType);
            else
                this.timelineHeader.RefreshTimelineHeader(this.timelineHeader.TimelineTotalTime, this.timelineHeader.TimelineHandIntervalTime, this.timelineHeader.TimelineHandDistance, this.timelineHeader.TimelineHandUnitType);
            //this.UpdatedTimelineContentItemBox(this.timelineHeader.TimelineHandUnitType, this.timelineHeader.TimelineHandDistance, this.timelineHeader.TimelineHandIntervalTime, this.timelineHeader.TimelineTotalTime);
        }

        public void SetContentBoxItemMaxTime(double maxValue)
        {
            this.timelineHeader.ContentBoxItemMaxTime = maxValue;
        }

        private void contentItemBox_Loaded(object sender, RoutedEventArgs e)
        {
            ContentItemBox contentItemBox = sender as ContentItemBox;
            if (contentItemBox != null)
            {
                contentItemBox.GetContentItemTimeEventHandler = new ContentItemBox.GetContentItemTimeDelegate(ContentItemTimeDisplay);
                contentItemBox.GetContentLengthEventHandler = new ContentItemBox.GetContentLengthDelegate(GetContentPositionLength);
                contentItemBox.SetListBoxDragNDropEventHandler = new ContentItem.SetListBoxDragNDropDelegate(SetListBoxDragNDrop);
                contentItemBox.ChangedContentItemEventHandler = new ChangedContentItemDelegate(ChangedContentItem);

                if (ContentItemBoxList.Contains(contentItemBox) == false)
                    ContentItemBoxList.Add(contentItemBox);
            }
        }

        private void ChangedContentItem(ContentItemBoxData contentItemBoxData, ContentItemData contentItemData)
        {
            contentItemData.TotalSeconds = this.GetContentItemPlayTime(contentItemData.ContentLength);// this.GetContentItemPlayTotalSeconds(contentItemData.ContentLength);
            contentItemData.ContentPlayTime = this.ContentItemTimeDisplay(contentItemData.ContentLength);

            if (ChangedContentItemEventHandler != null)
                ChangedContentItemEventHandler(contentItemBoxData, contentItemData);
        }

        private double GetContentItemPlayTotalSeconds(double contentLength)
        {
            double value = Math.Floor(contentLength / this.timelineHeader.TimelineHandDistance) * (double)this.timelineHeader.TimelineHandIntervalTime;
            return value;
        }

        private void SetListBoxDragNDrop(bool isDrag)
        {
            TimelineContentListBox.SetValue(iVisionTouchTimelineBody.DragDropListBox.DragDropHelper.IsDragSourceProperty, isDrag);
            TimelineContentListBox.SetValue(iVisionTouchTimelineBody.DragDropListBox.DragDropHelper.IsDropTargetProperty, isDrag);
        }

        public double GetContentPositionLength(double length)
        {
            double contentLength = Math.Floor(length) - (Math.Floor(length) % this.timelineHeader.TimelineHandDistance);// this.GetContentItemPlayTotalSeconds(length);
            
            return contentLength;
        }

        public double GetTimelineHeaderLength()
        {
            double length = this.TimelineBodyContentLength;
            return length;
        }

        public double GetTimelineHeaderTimeSeconds()
        {
            double length = this.timelineHeader.GetTimelineHeaderLength();
            return length / this.timelineHeader.TimelineHandDistance;
        }               

        public string ContentItemTimeDisplay(double totalLength)
        {
            string displayTime = "";
            double timeValue = 0.0;
            timeValue = this.GetContentItemPlayTotalSeconds(totalLength);  //Math.Floor(totalLength / this.timelineHeader.TimelineHandDistance);
            //switch (timelineHeader.TimelineHandUnitType)
            //{
            //    case TimelineHandUnitType.Seconds:                    
            //        displayTime = TimeLineTimerPoint.DisplayTimeFromSeconds(timeValue);
            //        break;
            //    case TimelineHandUnitType.Minutes:
            //        displayTime = TimeLineTimerPoint.DisplayTimeFromMinutes(timeValue);
            //        break;
            //    case TimelineHandUnitType.Hours:
            //        displayTime = TimeLineTimerPoint.DisplayTimeFromHours(timeValue);
            //        break;
            //}

            displayTime = CommonMethod.GetTimeString(timeValue, timelineHeader.TimelineHandUnitType);

            return displayTime;
        }

        public string ContentItemPlalyTime(double seconds)
        {
            string displayTime = TimeLineTimerPoint.DisplayTimeFromSeconds(seconds);
            return displayTime;
        }

        public double GetContentItemPlayTime(double totalLength)
        {
            double seconds = 0.0;
            double totalTimeValue = this.GetContentItemPlayTotalSeconds(totalLength);           
            switch (this.timelineHeader.TimelineHandUnitType)
            {
                case TimelineHandUnitType.Seconds:
                    seconds = TimeSpan.FromSeconds(totalTimeValue).TotalSeconds;
                    break;
                case TimelineHandUnitType.Minutes:
                    seconds = TimeSpan.FromMinutes(totalTimeValue).TotalSeconds;
                    break;
                case TimelineHandUnitType.Hours:
                    seconds = TimeSpan.FromHours(totalTimeValue).TotalSeconds;
                    break;
            }

            return seconds;
        }

        public void UpdatedTimelineContentItemBox()
        {
            UpdatedTimelineContentItemBox(timelineHeader.TimelineHandUnitType, timelineHeader.TimelineHandDistance, timelineHeader.TimelineHandIntervalTime, timelineHeader.TimelineTotalTime); 
        }

        public void UpdatedTimelineContentItemBox(TimelineHandUnitType tmelineHandUnitType, int timelineHandDistance, int timelineHandIntervalTime, double timelineTotalTime)
        {
            if (this.UpdatedTimelineContentItemBoxEventHandler != null)
            {
                this.UpdatedTimelineContentItemBoxEventHandler(tmelineHandUnitType.ToString(), timelineHandDistance, timelineHandIntervalTime, timelineTotalTime);
            }
        }

        public void UpdatedTimelineContentItemBox(double timeLength)
        {
            int timelineTotalTime = (int)this.GetContentItemPlayTotalSeconds(timeLength);
            timelineHeader.RefreshTimeline(timelineTotalTime, timelineHeader.TimelineHandIntervalTime, timelineHeader.TimelineHandDistance, timelineHeader.TimelineHandUnitType);

            UpdatedTimelineContentItemBox(timelineHeader.TimelineHandUnitType, timelineHeader.TimelineHandDistance, timelineHeader.TimelineHandIntervalTime, timelineTotalTime);            
        }

        public void RefreshTimeline(double timeLength)
        {
            int timelineTotalTime = (int)this.GetContentItemPlayTotalSeconds(timeLength);
            timelineHeader.RefreshTimeline(timelineTotalTime, timelineHeader.TimelineHandIntervalTime, timelineHeader.TimelineHandDistance, timelineHeader.TimelineHandUnitType);
        }

        public void RefreshTimeline(int timelineTotalTime)
        {
            timelineHeader.RefreshTimeline(timelineTotalTime, timelineHeader.TimelineHandIntervalTime, timelineHeader.TimelineHandDistance, timelineHeader.TimelineHandUnitType);
        }

        public void RefreshTimeline()
        {
            timelineHeader.RefreshTimelineHeader(timelineHeader.TimelineTotalTime, timelineHeader.TimelineHandIntervalTime, timelineHeader.TimelineHandDistance, timelineHeader.TimelineHandUnitType);
        }
        
        public void SetTimelineHeader(double timeLength)
        {
            int timelineTotalTime = (int)this.GetContentItemPlayTotalSeconds(timeLength);
            timelineHeader.RefreshTimelineHeader(timelineTotalTime, timelineHeader.TimelineHandIntervalTime, timelineHeader.TimelineHandDistance, timelineHeader.TimelineHandUnitType);
        }

        public void SetTimelineHeader(int timelineTotalTime)
        {
            timelineHeader.RefreshTimelineHeader(timelineTotalTime, timelineHeader.TimelineHandIntervalTime, timelineHeader.TimelineHandDistance, timelineHeader.TimelineHandUnitType);
        }

        private void TimelineContentListBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            ListBox listbox = sender as ListBox;
            if (listbox != null)
            {
                if (listbox.SelectedItem != null)
                {
                    this.RaiseTimelineContentDoubleClickEvent(listbox.SelectedItem);
                }
            }
        }

        private void TimelineContentListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ListBox listbox = sender as ListBox;
            if (listbox != null)
            {
                if (listbox.SelectedItem != null)
                {
                    this.RaiseTimelineContentSelectChangedEvent(listbox.SelectedItem);
                }
            }
        }

        private void contentItemBox_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            ContentItemBox itembox = sender as ContentItemBox;
            if (itembox != null)
            {
                ContentItemBoxData itemboxdata = itembox.DataContext as ContentItemBoxData;
                if (itemboxdata != null)
                {
                    if (itemboxdata.Equals(TimelineContentListBox.SelectedItem) == true)
                    {
                        return;
                    }
                    //this.RaiseTimelineContentSelectChangedEvent(itemboxdata);// SelectionChanged 이벤트 호출 함수(TimelineContentListBox_SelectionChanged)에서 호출함.
                    TimelineContentListBox.SelectedItem = itemboxdata;//ListBox의 Item 선택시 SelectionChanged 이벤트 강제 호출.
                }
            }
        }

        private void contentItemBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            /*
            ContentItemBox itembox = sender as ContentItemBox;
            if (itembox != null)
            {
                ContentItemBoxData itemboxdata = itembox.DataContext as ContentItemBoxData;
                if (itemboxdata != null)
                {
                    this.RaiseTimelineContentDoubleClickEvent(itemboxdata);
                }
            }
            e.Handled = true;//ContentItem UI에서 만 더블클릭 이벤트가 발생하도록 설정한다. 이부분을 해제하게 되면 TimelineBody 의 더블클릭 이벤트가 연속으로 호출되어 PlayerEditor 화면이 두번 호출된다.
             */ 
        }
        ScrollViewer scrollViewer;
        private void ScrollViewer_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            if (this.timelineHeader != null && e.HorizontalChange != 0)
            {
                this.timelineHeader.ChangeTimelineHeaderScroolViewer(e.HorizontalChange);

                
                //sv.ScrollToHorizontalOffset(timelineOffset + e.HorizontalChange);
            }

            if (scrollViewer == null)
                scrollViewer = sender as ScrollViewer;            
        }


        public void changeContentItemBoxSize(ContentItemBoxData data, double left, double width)
        {            
            ListBoxItem ListBoxItemObj = (ListBoxItem)TimelineContentListBox.ItemContainerGenerator.ContainerFromItem(data);            

            if (ListBoxItemObj != null )
            {
                // find a ContentPresenter of that list item.. [Call FindVisualChild Method]
                ContentPresenter ContentPresenterObj = FindVisualChild<ContentPresenter>(ListBoxItemObj);

                // call FindName on the DataTemplate of that ContentPresenter
                DataTemplate DataTemplateObj = ContentPresenterObj.ContentTemplate;
                ContentItemBox contentitembox = (ContentItemBox)DataTemplateObj.FindName("contentItemBox", ContentPresenterObj);

                contentitembox.changeContentItemBoxSize(left, width);
                //contentitembox.Width = width;
                //contentitembox.Margin = new Thickness(left, 0, 0, 0);
            }
        }

        private childItem FindVisualChild<childItem>(DependencyObject obj)
            where childItem : DependencyObject
        {
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
            {
                DependencyObject child = VisualTreeHelper.GetChild(obj, i);
                if (child != null && child is childItem)
                    return (childItem)child;
                else
                {
                    childItem childOfChild = FindVisualChild<childItem>(child);
                    if (childOfChild != null)
                        return childOfChild;
                }
            }
            return null;
        }

        #region Language 설정

        public void setCultureInfo(CultureInfo info)
        {
            if (info != null)
            {
                Cultures.Resources.Culture = info;
                CultureResources.ChangeCulture(info);
                Thread.CurrentThread.CurrentCulture = info;
            }
        }

        private void InitStringResource()
        {
            Cultures.CultureResources.ResourceProvider = (ObjectDataProvider)this.FindResource("Resources");

            //CutMenuItem.Header = Cultures.Resources.menuCut;
            //CopyMenuItem.Header = Cultures.Resources.menuCopy;
            //DeleteMenuItem.Header = Cultures.Resources.menuDelete;
            //NewMenuItem.Header = Cultures.Resources.menuNew;
            //InsertMenuItem.Header = Cultures.Resources.menuInsert;
        }
        #endregion
    }
}
