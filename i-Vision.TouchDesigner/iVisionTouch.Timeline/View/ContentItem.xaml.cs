﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using iVisionTouchTimelineBody.Model;
using System.Runtime.InteropServices;
using iVisionTouchTimelineBody.Util;

namespace iVisionTouchTimelineBody.View
{
    /// <summary>
    /// Interaction logic for ContentItem.xaml
    /// </summary>
    public partial class ContentItem : UserControl
    {
        public delegate string GetDisplayPositionDelegate(double selfLength, int orderIndex);
        public GetDisplayPositionDelegate GetDisplayPositionEventHandler;

        public delegate string GetTimeStringDelegate(double posLength);
        public static GetTimeStringDelegate GetTimeStringDelegateEventHandler;

        public delegate double GetContentLengthDelegate(double length);
        public GetContentLengthDelegate GetContentLengthEventHandler;

        public delegate void SetListBoxDragNDropDelegate(bool isDrag);
        public SetListBoxDragNDropDelegate SetListBoxDragNDropEventHandler;

        public ChangedContentItemDelegate ChangedContentItemEventHandler;
        public ChangedContentItemDataDelegate ChangedContentItemDataEventHandler;

        #region Win API

        //속도 얻기용
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int SystemParametersInfo(int uAction, int uParam, out int lpvParam, int fuWinIni);

        //속도 설정용
        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern int SystemParametersInfo(int uAction, int uParam, int lpvParam, int fuWinIni);

        [System.Runtime.InteropServices.DllImportAttribute("user32.dll", EntryPoint = "SetCursorPos")]
        [return: System.Runtime.InteropServices.MarshalAsAttribute(System.Runtime.InteropServices.UnmanagedType.Bool)]
        public static extern bool SetCursorPos(int X, int Y);

        const int SPI_GETMOUSESPEED = 112;
        const int SPI_SETMOUSESPEED = 113;

        #endregion

        private bool isMouseDown = false;
        private Point previousPoint = new Point();
        private int mouseSpeed = 10;
        public int OrderIndex { get; set; }
        public double Length { get; set; }

        private bool isResizeBarMouseEnter = false;
        ContentItemData ContentItemData;
        /*
        #region SetContentPosition Event

        public static readonly RoutedEvent SetContentPositionEvent = EventManager.RegisterRoutedEvent("SetContentPosition", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(ContentItem));

        public event RoutedEventHandler SetContentPosition
        {
            add { AddHandler(SetContentPositionEvent, value); }
            remove { RemoveHandler(SetContentPositionEvent, value); }
        }

        private void RaiseSetContentPositionEvent(object source)
        {
            RoutedEventArgs newEventArg = new RoutedEventArgs(ContentItem.SetContentPositionEvent, source);
            RaiseEvent(newEventArg);
        }

        #endregion
        */

        public ContentItem()
        {
            InitializeComponent();
            InitStringResource();

            this.Loaded += (s, e) =>
            {
                ContentItemData = this.DataContext as ContentItemData;
                if (ContentItemData != null)
                {
                    this.OrderIndex = ContentItemData.OrderIndex;
                }

            };
        }

        private void ResizeBar_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            //Mouse.Capture(this.ResizeBar);
            this.CaptureMouse();
            isMouseDown = true;
            previousPoint = e.GetPosition(this);

            //사용자PC의 마우스 속도 얻기            
            //SystemParametersInfo(SPI_GETMOUSESPEED, 0, out mouseSpeed, 0);

            //사이즈 조절을 위한 마우스 속도 설정
            //SystemParametersInfo(SPI_SETMOUSESPEED, 0, 5, 0); 
            e.Handled = true;
        }

        private void ResizeBar_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {           
            ReleaseMouseDown();
            this.ReleaseMouseCapture();
            
             if (ContentItemData != null)
             {
                 double contentLength = GetContentLengthEventHandler(ContentItemData.ContentLength);
                 ContentItemData.ContentLength = contentLength;                

                 if (ChangedContentItemEventHandler != null)
                     ChangedContentItemEventHandler(null, ContentItemData);
             }             
        }

        private void ResizeBar_MouseMove(object sender, MouseEventArgs e)
        {
            if (isMouseDown == true)
            {
                Point point = e.GetPosition(this);

                if (point.X.Equals(this.previousPoint.X) == false)
                {
                    if (ContentItemData != null)
                    {
                        ContentItemData.ContentLength += (point.X - this.previousPoint.X);
                        if (GetDisplayPositionEventHandler != null)
                        {
                            string displayTime = GetDisplayPositionEventHandler(ContentItemData.ContentLength, ContentItemData.OrderIndex);

                            ContentItemData.ContentDisplayTime = displayTime;

                            if (contentLengthPopup.IsOpen != true)
                                contentLengthPopup.IsOpen = true;
                            contentLengthPopup.Placement = System.Windows.Controls.Primitives.PlacementMode.Custom;
                            contentLengthPopup.Placement = System.Windows.Controls.Primitives.PlacementMode.Right;
                        }
                    }
                }

                previousPoint = point;
            }
        }

        public static string GetContentItemPlayTime(double contentLength)
        {
            string time = "";
            if (GetTimeStringDelegateEventHandler != null)
            {
                time = GetTimeStringDelegateEventHandler(contentLength);
            }

            return time;
        }
        
        private void ResizeBar_MouseLeave(object sender, MouseEventArgs e)
        {
            if (this.isMouseDown == true)
            {
                this.isResizeBarMouseEnter = false;
                if (SetListBoxDragNDropEventHandler != null)
                    SetListBoxDragNDropEventHandler(true);
                ReleaseMouseDown();

                if (ContentItemData != null)
                {
                    double contentLength = GetContentLengthEventHandler(ContentItemData.ContentLength);
                    ContentItemData.ContentLength = contentLength;

                    if (ChangedContentItemEventHandler != null)
                        ChangedContentItemEventHandler(null, ContentItemData);
                }
            }
        }

        private void ResizeBar_MouseEnter(object sender, MouseEventArgs e)
        {
            this.isResizeBarMouseEnter = true;
            if (SetListBoxDragNDropEventHandler != null)
                SetListBoxDragNDropEventHandler(false);
        }

        private void ReleaseMouseDown()
        {           
            if(this.isMouseDown == true)
            {                
                //사이즈 조절을 위한 마우스 속도 설정
                //SystemParametersInfo(SPI_SETMOUSESPEED, 0, mouseSpeed, 0);
            }

            this.isMouseDown = false;
            contentLengthPopup.IsOpen = false;
        }

        private void InitStringResource()
        {
            this.contentNameTextBlock.Text = Cultures.Resources.ContentName;
            this.contentPlaytimeTextBlock.Text = Cultures.Resources.ContentPlayTime;
        }
    }
}
