﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace InSysDSDisplayer
{
    /// <summary>
    /// Interaction logic for TransparentTouchWindow.xaml
    /// </summary>
    public partial class TransparentTouchWindow : Window
    {
        public FrameworkElement twFrameworkElement { get; set; }
        public TransparentTouchWindow()
        {
            InitializeComponent();
        }

        public void Init(Size size, Point point)
        {
            if (size.IsEmpty == false)
            {
                this.Width = size.Width;
                this.Height = size.Height;
            }

            SolidColorBrush solid = new SolidColorBrush();
            solid.Opacity = 0.002;
            solid.Color = Color.FromRgb(16, 0, 16);
            solid.Color = Colors.Yellow;

            this.WindowStartupLocation = System.Windows.WindowStartupLocation.Manual;
            this.Left = point.X;
            this.Top = point.Y;
                        
            this.Background = solid;
        }

        public void ShowWindow(UIElement parent)
        {
            this.Show();
        }

        public void CloseWindow()
        {
            this.Close();
        }
    }
}
