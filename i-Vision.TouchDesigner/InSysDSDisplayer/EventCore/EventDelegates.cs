﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;

namespace InSysDSDisplayer.EventCore
{
    public class EventDelegates
    {
        public delegate void StartContentTouchEventDelegate(FrameworkElement eventElement, ContentProperty contentProp, params object[] objValue);
        public delegate void EndContentTouchEventDelegate(FrameworkElement eventElement, ContentProperty contentProp, params object[] objValue);
        public delegate void KeyDownContentTouchEventDelegate(FrameworkElement eventElement, ContentProperty contentProp, params object[] objValue);
        public delegate void KeyUpContentTouchEventDelegate(FrameworkElement eventElement, ContentProperty contentProp, params object[] objValue);
        public delegate void ClickContentTouchEventDelegate(FrameworkElement eventElement, ContentProperty contentProp, params object[] objValue);
        public delegate void TimeoutContentTouchEventDelegate(FrameworkElement eventElement, ContentProperty contentProp, params object[] objValue);
        public delegate void DoubleClickContentTouchEventDelegate(FrameworkElement eventElement, ContentProperty contentProp, params object[] objValue);

        public delegate void StartScreenTouchEventDelegate(Canvas eventElement, ScreenProperty screenProp, params object[] objValue);
        public delegate void EndScreenTouchEventDelegate(Canvas eventElement, ScreenProperty screenProp, params object[] objValue);
        public delegate void KeyDownScreenTouchEventDelegate(Canvas eventElement, ScreenProperty screenProp, params object[] objValue);
        public delegate void KeyUpScreenTouchEventDelegate(Canvas eventElement, ScreenProperty screenProp, params object[] objValue);
        public delegate void ClickScreenTouchEventDelegate(Canvas eventElement, ScreenProperty screenProp, params object[] objValue);
        public delegate void TimeoutScreenTouchEventDelegate(Canvas eventElement, ScreenProperty screenProp, params object[] objValue);
        public delegate void DoubleClickScreenTouchEventDelegate(Canvas eventElement, ScreenProperty screenProp, params object[] objValue);
    }

    public enum ContentProperty
    {
        Width,
        Height
    }

    public enum ScreenProperty
    {
        Width,
        Height
    }
}
