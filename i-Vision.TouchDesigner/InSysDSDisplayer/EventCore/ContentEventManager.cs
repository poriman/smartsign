﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Xml.Linq;
using TouchEventEditor.Model;
using System.IO;
using System.Windows.Markup;

namespace InSysDSDisplayer.EventCore
{
    public class ContentEventManager
    {
        List<UserTouchEvent> UserTouchEvents { get; set; }

        public EventDelegates.ClickContentTouchEventDelegate ClickContentTouchEventHandler;
        public EventDelegates.StartContentTouchEventDelegate StartContentTouchEventHandler;
        public EventDelegates.EndContentTouchEventDelegate EndContentTouchEventHandler;
        public EventDelegates.KeyDownContentTouchEventDelegate KeyDownContentTouchEventHandler;
        public EventDelegates.KeyUpContentTouchEventDelegate KeyUpContentTouchEventHandler;
        public EventDelegates.TimeoutContentTouchEventDelegate TimeoutContentTouchEventHandler;
        public EventDelegates.DoubleClickContentTouchEventDelegate DoubleClickContentTouchEventHandler;

        public ContentEventManager()
        {
            ClickContentTouchEventHandler = new EventDelegates.ClickContentTouchEventDelegate(ClickContentTouchEvent);
            StartContentTouchEventHandler = new EventDelegates.StartContentTouchEventDelegate(StartContentTouchEvent);
            EndContentTouchEventHandler = new EventDelegates.EndContentTouchEventDelegate(EndContentTouchEvent);
            KeyDownContentTouchEventHandler = new EventDelegates.KeyDownContentTouchEventDelegate(KeyDownContentTouchEvent);
            KeyUpContentTouchEventHandler = new EventDelegates.KeyUpContentTouchEventDelegate(KeyUpContentTouchEvent);
            TimeoutContentTouchEventHandler = new EventDelegates.TimeoutContentTouchEventDelegate(TimeoutContentTouchEvent);
            DoubleClickContentTouchEventHandler = new EventDelegates.DoubleClickContentTouchEventDelegate(DoubleClickContentTouchEvent);
        }

        private void ClickContentTouchEvent(FrameworkElement eventElement, ContentProperty contentProp, params object[] objValue)
        {
            switch (contentProp)
            {
                case ContentProperty.Width:
                    {
                        double d_value = (double)objValue[0];
                        SetWidth(eventElement, d_value);
                    }
                    break;
                case ContentProperty.Height:
                    {
                        double d_value = (double)objValue[0];
                        SetHeight(eventElement, d_value);
                    }
                    break;
            }
        }

        private void StartContentTouchEvent(FrameworkElement eventElement, ContentProperty contentProp, params object[] objValue)
        {
        }

        private void EndContentTouchEvent(FrameworkElement eventElement, ContentProperty contentProp, params object[] objValue)
        {
        }

        private void KeyDownContentTouchEvent(FrameworkElement eventElement, ContentProperty contentProp, params object[] objValue)
        {
        }

        private void KeyUpContentTouchEvent(FrameworkElement eventElement, ContentProperty contentProp, params object[] objValue)
        {
        }

        private void TimeoutContentTouchEvent(FrameworkElement eventElement, ContentProperty contentProp, params object[] objValue)
        {
        }

        private void DoubleClickContentTouchEvent(FrameworkElement eventElement, ContentProperty contentProp, params object[] objValue)
        {
        }

        private void SetWidth(FrameworkElement eventElement, double width)
        {
            eventElement.Width = width;
        }

        private void SetHeight(FrameworkElement eventElement, double height)
        {
            eventElement.Height = height;
        }

        public void DeTouchEvent(string touchEventName)
        {
        }
    }
}
