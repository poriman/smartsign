﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Runtime.InteropServices;
using System.Diagnostics;

namespace InSysDSDisplayer
{
    public class OnScreenKeyboad
    {
        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern bool Wow64DisableWow64FsRedirection(ref IntPtr ptr);
        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern bool Wow64RevertWow64FsRedirection(IntPtr ptr);

        private const UInt32 WM_SYSCOMMAND = 0x112;
        private const UInt32 SC_RESTORE = 0xf120;
        private const UInt32 SC_CLOSE = 0xF060;
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        static extern IntPtr SendMessage(IntPtr hWnd, UInt32 Msg, IntPtr wParam, IntPtr lParam);

        private string OnScreenKeyboadApplication = "osk.exe";
        private object keyboard = new object();

        public void ShowOnScreenKeyboard(FrameworkElement editorControl, Window owner)
        {
            lock (keyboard)
            {
                // Get the name of the On screen keyboard
                string processName = System.IO.Path.GetFileNameWithoutExtension(OnScreenKeyboadApplication);

                // Check whether the application is not running 
                var query = from process in Process.GetProcesses()
                            where process.ProcessName == processName
                            select process;

                var keyboardProcess = query.FirstOrDefault();

                // launch it if it doesn't exist
                if (keyboardProcess == null)
                {
                    IntPtr ptr = new IntPtr(); ;

                    /* System.Environment.Is64BitOperatingSystem은 Framework 4.0 이 후 지원
                    bool sucessfullyDisabledWow64Redirect = false;
                
                    // Disable x64 directory virtualization if we're on x64,
                    // otherwise keyboard launch will fail.
                    if (System.Environment.Is64BitOperatingSystem)
                    {
                        sucessfullyDisabledWow64Redirect = Wow64DisableWow64FsRedirection(ref ptr);
                    }
                    */

                    // osk.exe is in windows/system folder. So we can directky call it without path
                    CallProcess(editorControl, OnScreenKeyboadApplication);
                    
                    /* System.Environment.Is64BitOperatingSystem은 Framework 4.0 이 후 지원
                    // Re-enable directory virtualisation if it was disabled.
                    if (System.Environment.Is64BitOperatingSystem)
                        if (sucessfullyDisabledWow64Redirect)
                            Wow64RevertWow64FsRedirection(ptr);
                    */
                }
                else
                {
                    // Bring keyboard to the front if it's already running
                    var windowHandle = keyboardProcess.MainWindowHandle;
                    if (windowHandle != (IntPtr)0)
                        SendMessage(windowHandle, WM_SYSCOMMAND, new IntPtr(SC_RESTORE), new IntPtr(0));
                    else
                        CallProcess(editorControl, OnScreenKeyboadApplication);
                }
            }
        }

        private void CallProcess(FrameworkElement editorControl, string processName)
        {
            using (Process osk = new Process())
            {
                try
                {
                    osk.StartInfo.FileName = processName;
                    osk.Start();
                    osk.WaitForInputIdle(2000);
                }
                catch
                {
                }
            }
        }

        public void CloseOnScreenKeyboard()
        {
            // Get the name of the On screen keyboard
            string processName = System.IO.Path.GetFileNameWithoutExtension(OnScreenKeyboadApplication);

            // Check whether the application is not running 
            var query = from process in Process.GetProcesses()
                        where process.ProcessName == processName
                        select process;

            var keyboardProcess = query.FirstOrDefault();

            // launch it if it doesn't exist
            if (keyboardProcess != null)
            {
                lock (keyboard)
                {
                    // Bring keyboard to the front if it's already running
                    var windowHandle = keyboardProcess.MainWindowHandle;
                    SendMessage(windowHandle, WM_SYSCOMMAND, new IntPtr(SC_CLOSE), new IntPtr(0));
                }
            }
        }
    }
}
