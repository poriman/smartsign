﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using InSysDSDisplayer.TestScreenView;
using System.Windows.Interop;

namespace InSysDSDisplayer.Views
{
    /// <summary>
    /// Interaction logic for DSDisplayWindow.xaml
    /// </summary>
    public partial class PreviewWindow : Window
    {
        InSysDSDisplayer.Hooking.InputDevice id;
        int NumberOfKeyboards;
        System.Windows.Forms.Message message = new System.Windows.Forms.Message();

        public PreviewWindow()
        {
            InitializeComponent();
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                this.Close();
            }
        }

               
        private void iVisionTouchDesignerCloseBtn_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            this.Close();
        }

        private void Image_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            this.Close();
        }

        public IntPtr WndProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            if (id != null)
            {
                // I could have done one of two things here.
                // 1. Use a Message as it was used before.
                // 2. Changes the ProcessMessage method to handle all of these parameters(more work).
                //    I opted for the easy way.

                //Note: Depending on your application you may or may not want to set the handled param.

                message.HWnd = hwnd;
                message.Msg = msg;
                message.LParam = lParam;
                message.WParam = wParam;

                id.ProcessMessage(message);
            }
            //Console.WriteLine(id.ToString());
            return IntPtr.Zero;
        }

        protected override void OnSourceInitialized(EventArgs e)
        {
            base.OnSourceInitialized(e);
        }

        void StartWndProcHandler()
        {
            IntPtr hwnd = IntPtr.Zero;
            Window myWin = Application.Current.MainWindow;

            try
            {
                hwnd = new WindowInteropHelper(myWin).Handle;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            //Get the Hwnd source   
            HwndSource source = HwndSource.FromHwnd(hwnd);
            //Win32 queue sink
            source.AddHook(new HwndSourceHook(WndProc));

            id = new InSysDSDisplayer.Hooking.InputDevice(source.Handle);
            NumberOfKeyboards = id.EnumerateDevices();
           //d.KeyPressed += new InputDevice.DeviceEventHandler(_KeyPressed);
        }

        void CloseMe(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
