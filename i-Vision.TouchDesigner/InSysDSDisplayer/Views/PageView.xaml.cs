﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using InSysDSDisplayer.Models;

namespace InSysDSDisplayer.Views
{
    /// <summary>
    /// Interaction logic for ScreenPage.xaml
    /// </summary>
    public partial class PageView : Grid
    {
        private TranslateTransform translateTransform;
        private RotateTransform rotateTransform;
        private ScaleTransform scaleTransform;
        private SkewTransform skewTransform;
        private TransformGroup transformGroup;

        public double WidthRatio { get; private set; }
        public double HeightRatio { get; private set; }

        public PageView()
        {
            InitializeComponent();

            Init_Transform();

            this.SizeChanged += new SizeChangedEventHandler(PageView_SizeChanged);
        }

        void PageView_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            this.SetScaleTransform(WidthRatio, HeightRatio);  
        }

        public DisplayCanvas PageDisplayCanvas
        {
            get
            {
                if (this.RootLayout.Children.Count == 0)
                    return null;
                return this.RootLayout.Children[0] as DisplayCanvas;
            }

            set
            {
                ClearPageViewChildren();
                this.RootLayout.Children.Add(value);
            }
        }

        public void ClearPageViewChildren()
        {
            this.RootLayout.Children.Clear();
        }

        public void Init_Transform()
        {
            transformGroup = new TransformGroup();

            rotateTransform = new RotateTransform();
            scaleTransform = new ScaleTransform();
            skewTransform = new SkewTransform();
            translateTransform = new TranslateTransform();

            transformGroup.Children.Add(rotateTransform);
            transformGroup.Children.Add(scaleTransform);
            transformGroup.Children.Add(skewTransform);
            transformGroup.Children.Add(translateTransform);

            //this.RenderTransform = transformGroup;
        }

        public void SetScaleTransform(double scaleXValue, double scaleYValue)
        {
            scaleTransform.ScaleX = scaleXValue;
            scaleTransform.ScaleY = scaleYValue;
            this.LayoutTransform = scaleTransform;
        }

        public void SetScaleTransform_LessThan(double scaleValue)
        {
            scaleTransform.ScaleX = scaleTransform.ScaleY = scaleValue;
            //this.UpdateLayout();
            this.LayoutTransform = scaleTransform;
        }

        public void SetScaleTransform_MoreThan(double scaleValue)
        {
            scaleTransform.ScaleX = scaleTransform.ScaleY = scaleValue;
            //this.UpdateLayout();
            this.LayoutTransform = scaleTransform;
        }

        public void SetTranslateTransform(double translateX, double translateY)
        {
            translateTransform.X = translateX;
            translateTransform.Y = translateY;
            //this.UpdateLayout();
        }

        public void SetScaleValue(double translateX, double translateY)
        {
            this.WidthRatio = translateX;
            this.HeightRatio = translateY;
        }
    }
}
