﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using InSysBasicControls.Interfaces;
using System.Timers;
using System.Diagnostics;

namespace InSysDSDisplayer.Views
{
    /// <summary>
    /// Interaction logic for PlayCanvas.xaml
    /// </summary>
    public partial class DisplayCanvas : Canvas
    {
        private Stopwatch _Stopwatch;
        private DispatcherTimer _PageTimer;

        private TranslateTransform translateTransform;
        private RotateTransform rotateTransform;
        private ScaleTransform scaleTransform;
        private SkewTransform skewTransform;
        private TransformGroup transformGroup;

        public DisplayCanvas()
        {
            InitializeComponent();

            _Stopwatch = new Stopwatch();
            _PageTimer = new DispatcherTimer();

            Init_Transform();         

            this.Loaded += (s, e) =>
            {
                foreach (UIElement element in this.Children)
                {
                    IDesignElement designElement = element as IDesignElement;
                    if (designElement != null)
                    {
                    }
                }               
            };

            //this.SizeChanged += new SizeChangedEventHandler(DisplayCanvas_SizeChanged);
        }

        void DisplayCanvas_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            //double x_rate = e.NewSize.Width / this.Width;
            //double y_rate = e.NewSize.Height / this.Height;

            //MessageBox.Show(string.Format("{0},{1} - {2},{3} - {4},{5} - {6},{7}", e.NewSize.Width, e.NewSize.Height, this.Width, this.Height, this.ActualWidth, this.ActualHeight, e.PreviousSize.Width, e.PreviousSize.Height));

            //this.SetScaleTransform(0.7, 0.7);  
        }

        public void Init_Transform()
        {
            transformGroup = new TransformGroup();

            rotateTransform = new RotateTransform();
            scaleTransform = new ScaleTransform();
            skewTransform = new SkewTransform();
            translateTransform = new TranslateTransform();

            transformGroup.Children.Add(rotateTransform);
            transformGroup.Children.Add(scaleTransform);
            transformGroup.Children.Add(skewTransform);
            transformGroup.Children.Add(translateTransform);

            //this.RenderTransform = transformGroup;
        }

        public void SetScaleTransform(double scaleXValue, double scaleYValue)
        {
            scaleTransform.ScaleX = scaleXValue;
            scaleTransform.ScaleY = scaleYValue;            
            this.RenderTransform = scaleTransform;
        }

        public void SetScaleTransform_LessThan(double scaleValue)
        {
            scaleTransform.ScaleX = scaleTransform.ScaleY = scaleValue;           
            //this.UpdateLayout();
            this.LayoutTransform = scaleTransform;
        }

        public void SetScaleTransform_MoreThan(double scaleValue)
        {
            scaleTransform.ScaleX = scaleTransform.ScaleY = scaleValue;
            //this.UpdateLayout();
            this.LayoutTransform = scaleTransform;
        }

        public void SetTranslateTransform(double translateX, double translateY)
        {
            translateTransform.X = translateX;
            translateTransform.Y = translateY;
            //this.UpdateLayout();
        }

        public void StopLifeTime()
        {
            _PageTimer.Stop();
            _Stopwatch.Stop();
        }

        void ScreenTimer_Tick(object sender, EventArgs e)
        {
            DispatcherTimer timer = sender as DispatcherTimer;
            foreach (UIElement element in this.Children)
            {
                IDesignElement designElement = element as IDesignElement;
                if (designElement != null)
                {
                    designElement.CheckLifeTime(_Stopwatch.Elapsed);
                }              
            }
            
        }
    }

}
