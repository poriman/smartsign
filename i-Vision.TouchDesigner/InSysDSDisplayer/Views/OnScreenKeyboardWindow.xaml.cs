﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Diagnostics;
using InSysDSDisplayer.Models;

namespace InSysDSDisplayer.Views
{
    public delegate void ShowOnScreenKeyboardDelegate();

    /// <summary>
    /// Interaction logic for OnScreenKeyboardWindow.xaml
    /// </summary>
    public partial class OnScreenKeyboardWindow : Window
    {
        public bool isShowKeyboardState { get; set; }
        public bool IsTimeScheduleProgram { get; set; }
        public ShowOnScreenKeyboardDelegate ShowOnScreenKeyboardHandler;
        private DispatcherTimer _TouchProjectTimer = new DispatcherTimer();
        private Stopwatch _Stopwatch = new Stopwatch();       

        public OnScreenKeyboardWindow()
        {
            InitializeComponent();

            this._TouchProjectTimer.Interval = TimeSpan.FromMilliseconds(1000);
            this._TouchProjectTimer.Tick += new EventHandler(TouchProjectTimer_Tick);
            this.Loaded += (s, e) =>
            {
                this._TouchProjectTimer.Start();
                this._Stopwatch.Start();
            };
            this.Show();
            this.isShowKeyboardState = false;
        }

        public void HideKeyboardIconWindow()
        {
            this._TouchProjectTimer.Stop();
            this._Stopwatch.Stop();
            this._Stopwatch.Reset();
            this.Opacity = 0.0;
            //this.Hide();
            this.isShowKeyboardState = false;
        }

        public void ShowKeyboardIconWindow()
        {
            this.WindowStartupLocation = WindowStartupLocation.Manual;
            this.Left = SystemParameters.FullPrimaryScreenWidth - keyboardImg.Width;
            this.Top = SystemParameters.FullPrimaryScreenHeight - keyboardImg.Height;
            this.Width = 180;
            this.Height = 50;
            this.Opacity = 0.3;

            ResetUserTouchCheckTimer();
            //this.Show();
            this.isShowKeyboardState = true;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (ShowOnScreenKeyboardHandler != null)
                ShowOnScreenKeyboardHandler();
        }

        public void ResetUserTouchCheckTimer()
        {
            this._Stopwatch.Reset();
            this._Stopwatch.Start();
            this._TouchProjectTimer.Start();
        }

        void TouchProjectTimer_Tick(object sender, EventArgs e)
        {
            DispatcherTimer dispathcherTimer = sender as DispatcherTimer;
            if (dispathcherTimer != null)
            {
                if (this._Stopwatch.Elapsed.Seconds > 10)//Touch Project Time 을 넘겼을 경우에 대해 처리한다.
                {
                    if (this.isShowKeyboardState == true)
                        HideKeyboardIconWindow();
                }
                else
                {
                    if (this.isShowKeyboardState == false)
                    {
                        ShowKeyboardIconWindow();
                    }
                }
            }
        }
    }
}
