﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using InSysDSDisplayer.TestScreenView;

namespace InSysDSDisplayer.Views
{
    /// <summary>
    /// Interaction logic for DSDisplayView.xaml
    /// </summary>
    public partial class DSDisplayView : UserControl
    {
        private TranslateTransform translateTransform;
        private RotateTransform rotateTransform;
        private ScaleTransform scaleTransform;
        private SkewTransform skewTransform;
        private TransformGroup transformGroup;

        public DSDisplayView()
        {
            InitializeComponent();

            Init_Transform();
        }

        public void Init_Transform()
        {
            transformGroup = new TransformGroup();

            rotateTransform = new RotateTransform();
            scaleTransform = new ScaleTransform();
            skewTransform = new SkewTransform();
            translateTransform = new TranslateTransform();

            transformGroup.Children.Add(rotateTransform);
            transformGroup.Children.Add(scaleTransform);
            transformGroup.Children.Add(skewTransform);
            transformGroup.Children.Add(translateTransform);

            this.RenderTransform = transformGroup;
        }       
    }
}
