﻿using System.Windows.Controls;
using System.Windows;

namespace InSysDSDisplayer.Animations
{
	public static class ExtensionMethods
	{
        //페이지가 왼쪽으로 흘러가는 페이지 전환 애니메이션 방식
		public static void NavigateToLeft(this Frame frameContainer, Page page)
		{            
			var navigation = new NavigationAnimator();
            navigation.Navigate(frameContainer, page);
		}

        //디폴트 페이지 전환  방식
        public static void Navigate(this Frame frameContainer, Page page)
        {
            frameContainer.NavigationService.Navigate(page);
        }
	}
}
