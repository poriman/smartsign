﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using InSysDSDisplayer.Views;
using System.Windows;
using InSysDSDisplayer.Models;
using System.Windows.Controls;
using InSysBasicControls.Interfaces;
using InSysTouchflowData.Models.ProjectModels;
using System.Diagnostics;
using System.Windows.Threading;
using InSysTouchflowData;
using InSysBasicControls.Views;
using InSysBasicControls.Commons;
using System.Windows.Media;
using InSysTouchflowData.Models.ActionEvents;
using System.Windows.Input;
using System.Xml.Linq;
using TouchEventEditor.Model;
using System.IO;
using System.Windows.Markup;
using System.Threading;

namespace InSysDSDisplayer.ViewModels
{
    public delegate DisplayCanvas GetDisplayCanvasDelegate(string screenID);
    public class PageViewViewModel
    {        
        public static GetDisplayCanvasDelegate GetDisplayCanvasHandler;
        public static TouchDownDelegate TouchDownHandler;
        public ClosePreviewDelegate ClosePreviewHandler;
        public CloseViewDelegate ClosePageLifeTimeHandler;
        public GoToPageActionEventDelegate GoToPageActionEventHandler;
        public ShowHideActionEventDelegate ShowHideActionEventHandler;
        public HyperlinkActionEventDelegate HyperlinkActionEventHandler;
        public OpenFileActionEventDelegate OpenFileActionEventHandler;

        private List<TransparentTouchWindow> transparentTouchWindows = new List<TransparentTouchWindow>();
        private Stopwatch _Stopwatch;
        private DispatcherTimer _PageViewTimer;
        private static OnScreenKeyboad osk = new OnScreenKeyboad();
        public string PageDirectory { get; set; }

        private PageViewInfo _PageViewInfo;
        public PageViewInfo PageViewInfo
        {
            get { return _PageViewInfo; }
            private set { _PageViewInfo = value; }
        }

        private PageView _PageView;
        public PageView PageView
        {
            get { return _PageView; }
            set { _PageView = value; }
        }

        private bool _IsPagePlay;
        public bool IsPagePlay
        {
            get { return _IsPagePlay; }
            set { _IsPagePlay = value; }
        }

        public double LeftPos { get; set; }
        public double TopPos { get; set; }

        public static Window ParentWindow { get; set; }

        ~PageViewViewModel()
        {

        }

        public PageViewViewModel()
        {
            _PageViewInfo = new PageViewInfo();
            _PageView = new PageView();
            _IsPagePlay = false;
            _Stopwatch = new Stopwatch();
            _PageViewTimer = new DispatcherTimer();
        }

        public PageViewViewModel(DisplayCanvas contentScreen, TouchPageInfo pageInfo)
            : this()
        {
            _PageView.Width = contentScreen.Width;
            _PageView.Height = contentScreen.Height;
            _PageView.ClearPageViewChildren();
            _PageView.PageDisplayCanvas = contentScreen;
            _PageView.DataContext = this;
            PageViewInfo.PageInfo = pageInfo;

            this.InitCheckElementVisibility(_PageView.PageDisplayCanvas);
        }

        private void InitCheckElementVisibility(DisplayCanvas displayCanvas)
        {
            foreach (UIElement element in displayCanvas.Children)
            {
                IDesignElement designElement = element as IDesignElement;
                if (designElement != null)
                {
                    if (designElement.IsApplyLifeTime == true)
                    {
                        if (designElement.StartTime != TimeSpan.FromSeconds(0.0))
                        {
                            element.Visibility = System.Windows.Visibility.Hidden;
                        }
                    }
                }
            }
        }

        public void StartLifeTime()
        {
            if (this._PageViewTimer != null)
                this._PageViewTimer = new DispatcherTimer();

            double dValue = PageViewInfo.PageInfo.PageLifeTimeTemp;
            _PageViewTimer.Interval = TimeSpan.FromMilliseconds(10);
            _PageViewTimer.Tick += new EventHandler(PageViewTimer_Tick);

            CheckContentPlay(this._PageViewTimer);

            this._PageViewTimer.Start();
            _Stopwatch.Start();
        }

        public void StopLifeTime()
        {
            _PageViewTimer.Stop();
            _Stopwatch.Stop();
            _Stopwatch.Reset();
        }

        void PageViewTimer_Tick(object sender, EventArgs e)
        {
            CheckContentPlay(sender as DispatcherTimer);            
        }

        private void CheckContentPlay(DispatcherTimer dispathcherTimer)
        {
            if (PageViewInfo.PageInfo.IsApplyPageLifeTime == true)
            {
                if (dispathcherTimer != null)
                {
                    TimeSpan ts = _Stopwatch.Elapsed;
                    if (this._Stopwatch.Elapsed.TotalSeconds >= PageViewInfo.PageInfo.PageLifeTimeTemp)
                    {
                        if (ClosePageLifeTimeHandler != null)
                        {
                            ClosePageLifeTimeHandler(this);
                            dispathcherTimer.Stop();
                            return;
                        }
                    }
                }
            }

            if (_PageView.PageDisplayCanvas != null)
            {
                foreach (UIElement element in _PageView.PageDisplayCanvas.Children)
                {
                    IDesignElement designElement = element as IDesignElement;
                    if (designElement != null)
                    {
                        designElement.CheckLifeTime(_Stopwatch.Elapsed);
                        //var rectAreaElement = transparentTouchWindows.Where(o=>o.twFrameworkElement.Equals(element) == true).FirstOrDefault();
                        //if (rectAreaElement != null)
                        //{
                        //    if (designElement.ISPlay == true)
                        //    {
                        //        rectAreaElement.Visibility = Visibility.Visible;                                
                        //    }
                        //    else
                        //    {
                        //        rectAreaElement.Visibility = Visibility.Collapsed;
                        //    }
                        //}
                    }
                }
            }
        }

        public DisplayCanvas Get_PageDisplayCanvas()
        {
            return _PageView.PageDisplayCanvas;
        }

        public static void CloseOnScreenKeyboard()
        {
            if (osk == null)
                osk = new OnScreenKeyboad();
            
            osk.CloseOnScreenKeyboard();
        }

        public static void ShowOnScreenKeyboard(FrameworkElement element, Window owner)
        {
            if (osk == null)
                osk = new OnScreenKeyboad();

            osk.ShowOnScreenKeyboard(element, owner);
        }

        public static Dictionary<IDesignElement, Rect> RectAreaList = new Dictionary<IDesignElement, Rect>();
        public void Play_PageView()
        {
            RectAreaList.Clear();
            DisplayCanvas pageDisplayCanvas = Get_PageDisplayCanvas();

            pageDisplayCanvas.Children.OfType<IDesignElement>().ToList().ForEach(o => o.IsInLifeTime = false);//Page 시작전 IsInLifeTime을 false로 설정.
            StartLifeTime();
            if (pageDisplayCanvas != null)
            {
                pageDisplayCanvas.MouseDown += (s, e) =>
                {
                    CloseOnScreenKeyboard();

                    #region Display Canvas 마우스 다운시 포커스 해제
                    FrameworkElement parent = (FrameworkElement)pageDisplayCanvas.Parent;
                    while (parent != null && parent is IInputElement && !((IInputElement)parent).Focusable)
                    {
                        parent = (FrameworkElement)parent.Parent;
                    }

                    DependencyObject scope = FocusManager.GetFocusScope(pageDisplayCanvas);
                    FocusManager.SetFocusedElement(scope, parent as IInputElement);
                    #endregion

                    //if (TouchDownHandler != null)//Touch Screen에 Touch Down일 발생시.
                    //    TouchDownHandler();
                };
              
                foreach (FrameworkElement item in pageDisplayCanvas.Children)
                { 
                    IDesignElement designElement = item as IDesignElement;                    
                    if (designElement != null)
                    {
                        switch (designElement.InSysControlType)
                        {
                            case InSysControlType.InSysRectArea:
                                #region InSysRectArea 컨트롤의 이벤트를 처리하는 투명 윈도우 생성
                                {
                                    //TransparentTouchWindow tw = new TransparentTouchWindow();
                                    //tw.MouseDown += (s, e) =>
                                    //{
                                    //    if (designElement != null)
                                    //    {
                                    //        if (designElement.RaiseActionEventHandler != null)
                                    //        {
                                    //            designElement.RaiseActionEventHandler(designElement.ActionEvent);
                                    //        }
                                    //    }
                                    //};
                                    //tw.KeyUp += (s, e) =>
                                    //{
                                    //    if (e.Key == System.Windows.Input.Key.Escape)
                                    //    {
                                    //        if (ClosePreviewHandler != null)
                                    //            ClosePreviewHandler();
                                    //    }
                                    //};
                                    //tw.twFrameworkElement = item;
                                    double left = App.Current.MainWindow.Left;
                                    double titleHeight = 28;//윈도우의 타이틀 바 Height
                                    double itemLeft = designElement.X;// Canvas.GetLeft(item);
                                    double itemTop = designElement.Y;// Canvas.GetTop(item);

                                    //tw.Init(new Size(designElement.Width, designElement.Height), new Point(this.LeftPos + itemLeft + 10, this.TopPos + itemTop + titleHeight + 10));//10 만큼의 공간이 발생하는 원인은 알 수 없음.
                                    //tw.ShowWindow(pageDisplayCanvas);
                                    //tw.Visibility = Visibility.Collapsed;
                                    //transparentTouchWindows.Add(tw);

                                    Rect rect = new Rect(itemLeft, itemTop, designElement.Width, designElement.Height);
                                    RectAreaList.Add(designElement, rect);
                                }
                                
                                #endregion
                                break;
                            case InSysControlType.InSysBasicTextBox:
                                item.GotFocus += (s, e) =>
                                {
                                    ShowOnScreenKeyboard(item, ParentWindow);
                                };
                                item.LostFocus += (s, e) =>
                                {
                                    CloseOnScreenKeyboard();
                                };
                                break;
                        }

                        designElement.Play();
                    }

                    SetContentTouchEvent(item, item.GetType());
                }
            }
            
            this._IsPagePlay = true;
        }
      
        Point GetControlPosition(FrameworkElement myControl)
        {

            Point locationToScreen = App.Current.MainWindow.PointToScreen(new Point(0, 0));
            PresentationSource source = PresentationSource.FromVisual(App.Current.MainWindow);

            return source.CompositionTarget.TransformFromDevice.Transform(locationToScreen);
        }

        public void ClosePageView()
        {
            StopPageView(); 
            _PageView.ClearPageViewChildren();          
        }

        public void StopPageView()
        {
            DisplayCanvas page = Get_PageDisplayCanvas();
            if (page != null)
            {
                List<IDesignElement> children = page.Children.OfType<IDesignElement>().ToList();                
                children.ForEach(o => o.Stop());
                StopLifeTime();               
            }          

            this._IsPagePlay = false;
        }

        public string GetScreenID()
        {
            return PageViewInfo.PageInfo.PageGuid;
        }

        /// <summary>
        /// Content Touch Event 설정.
        /// </summary>
        /// <param name="insysControl"></param>
        /// <param name="type"></param>
        private void SetContentTouchEvent(FrameworkElement insysControl, Type type)
        {
            InSysControlType insysControlType = InSysBasicControls.InSysControlManager.GetControlType(type.Name);
            IDesignElement designElement = insysControl as IDesignElement;
            if (designElement != null)
            {
                if (designElement.TouchEvent != null)
                {
                    foreach (var keyvalue in designElement.TouchEvent)
                    {
                        TouchEventType evnetType = (TouchEventType)Enum.Parse(typeof(TouchEventType), keyvalue.Key);
                        switch (evnetType)
                        {
                            case TouchEventType.TC_EVT_OBJECT_CLICK:
                                {
                                    designElement.RaiseDoContentMouseClickTouchEventHandler = (sender) =>
                                    {
                                        string touchEventName = keyvalue.Value;
                                        UserTouchEvent userTouchEvent = InSysBasicControls.InSysControlManager.GetUserTouchEvent(touchEventName);
                                        if (userTouchEvent != null)
                                        {
                                            foreach (var userContentFunction in userTouchEvent.UserContentFunctionList)
                                            {
                                                string contentName = userContentFunction.ElementName;
                                                string propertyName = userContentFunction.FunctionName;
                                                //대상 스크린 비교
                                                if (userContentFunction.ScreenID.Equals(_PageViewInfo.PageInfo.PageGuid) == true)
                                                {
                                                    DisplayCanvas displayCanvas = _PageView.PageDisplayCanvas;

                                                    FrameworkElement targetElement = displayCanvas.Children.OfType<FrameworkElement>().Where(o => (o as IDesignElement).Name.Equals(contentName) == true || (o as IDesignElement).Name.ToLower().Equals(contentName.ToLower()) == true).FirstOrDefault();
                                                    if (targetElement != null)
                                                    {
                                                        ChangeTargetDesignElement(targetElement, userContentFunction);
                                                    }

                                                }
                                                else//대상 스크린이 현재 스크린이 아닐 경우
                                                {
                                                    //TouchProjectInfo touchProjectInfo = InSysBasicControls.InSysControlManager.GetTouchProjectInfoHandler();
                                                    DisplayCanvas displayCanvas = GetDisplayCanvasHandler(userContentFunction.ScreenID);// touchProjectInfo.PageInfoList.Where(o => o.PageGuid.Equals(userContentFunction.ScreenID) == true).FirstOrDefault();
                                                    if (displayCanvas != null)
                                                    {
                                                        FrameworkElement targetElement = displayCanvas.Children.OfType<FrameworkElement>().Where(o => (o as IDesignElement).Name.ToLower().Equals(contentName.ToLower()) == true).FirstOrDefault();
                                                        if (targetElement != null)
                                                        {
                                                            ChangeTargetDesignElement(targetElement, userContentFunction);
                                                        }
                                                    }
                                                }
                                            }
                                        } 
                                    };
                                }
                                break;
                            case TouchEventType.TC_EVT_OBJECT_DCLICK:
                                break;
                            case TouchEventType.TC_EVT_OBJECT_END:
                                break;
                            case TouchEventType.TC_EVT_OBJECT_KEY_DOWN:
                                break;
                            case TouchEventType.TC_EVT_OBJECT_KEY_UP:
                                break;
                            case TouchEventType.TC_EVT_OBJECT_START:
                                break;
                            case TouchEventType.TC_EVT_OBJECT_TIMEOUT:
                                break;
                            default:
                                break;
                        }
                    }
                }
                switch (insysControlType)
                {
                    case InSysControlType.InSysBasicButton:
                    case InSysControlType.InSysRectArea:
                        designElement.RaiseActionEventHandler = new RaiseActionEventDelegate(OnClicked_InSysButtonControl);                        
                        break;
                }
            }
        }

        private void ChangeTargetDesignElement(FrameworkElement targetElement, UserContentFunction userContentFunction)
        {
            IDesignElement targetDesignElement = targetElement as IDesignElement;
            if (targetDesignElement != null)
            {
                switch (userContentFunction.Function)
                {
                    case Function.Width:
                        targetDesignElement.Width = (double)userContentFunction.Value;
                        break;
                    case Function.Height:
                        targetDesignElement.Height = (double)userContentFunction.Value;
                        break;
                    case Function.Opacity:
                        targetDesignElement.Opacity = (double)userContentFunction.Value;
                        break;
                    case Function.PositionX:
                        targetDesignElement.X = (double)userContentFunction.Value;
                        break;
                    case Function.PositionY:
                        targetDesignElement.Y = (double)userContentFunction.Value;
                        break;
                    case Function.Rotation:
                        RotateTransform rotate = new RotateTransform();
                        rotate.Angle = (double)userContentFunction.Value;
                        targetElement.LayoutTransform = rotate;
                        break;
                    case Function.Foreground:
                        targetDesignElement.Foreground = (Brush)userContentFunction.Value;
                        break;
                    case Function.Background:
                        targetDesignElement.Background = (Brush)userContentFunction.Value;
                        break;
                    case Function.Move:
                        Point point = (Point)userContentFunction.Value;
                        targetDesignElement.X = point.X;
                        targetDesignElement.Y = point.Y;
                        break;
                    case Function.Resize:
                        break;
                }
            }
        }

        public void OnClicked_InSysButtonControl(object parameter)
        {
            ActionEvent actionEvent = parameter as ActionEvent;
            if (actionEvent != null)
            {
                switch (actionEvent.ActionEventType)
                {
                    case ActionEventType.GoToPage:
                        MovePageItem mpItem = (actionEvent as GoToPageEvent).SelectedPageItem;
                        if (GoToPageActionEventHandler != null)
                        {
                            GoToPageActionEventHandler(mpItem.PageID, this);
                        }
                        break;
                    case ActionEventType.ShowHide:
                        string controlName = (actionEvent as ShowHideEvent).SelectedItemName;
                        object type = (actionEvent as ShowHideEvent).Type;
                        if (string.IsNullOrEmpty(controlName) == false)
                        {
                            if (ShowHideActionEventHandler != null)
                            {
                                ShowHideActionEventHandler(controlName, type, this);                                
                            }
                        }
                        break;
                    case ActionEventType.Hyperlink:
                        string url = (actionEvent as HyperlinkEvent).URL;
                        if (string.IsNullOrEmpty(url) == false)
                        {
                            if (HyperlinkActionEventHandler != null)
                            {
                                HyperlinkActionEventHandler(url);
                            }
                        }
                        break;
                    case ActionEventType.OpenFile:
                        string openfile = (actionEvent as OpenFileEvent).OpenFile;
                        if (string.IsNullOrEmpty(openfile) == false)
                        {
                            if (OpenFileActionEventHandler != null)
                            {
                                OpenFileActionEventHandler(openfile);
                            }
                        }
                        break;                   
                }
            }

            //if (TouchDownHandler != null)//Touch Screen에 Touch Down일 발생시.
            //    TouchDownHandler();
        }
    }
}
