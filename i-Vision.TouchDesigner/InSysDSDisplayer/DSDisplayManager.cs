﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using InSysDSDisplayer.ViewModels;
using System.Windows.Controls;
using InSysDSDisplayer.Animations;
using InSysBasicControls;
using InSysBasicControls.Commons;
using InSysDSDisplayer.Views;
using InSysTouchflowData.Models.ProjectModels;
using InSysTouchflowData;
using InSysBasicControls.Interfaces;
using System.Windows.Media;
using System.IO;
using InSysTouchflowData.Models.ActionEvents;
using System.Diagnostics;
using System.Windows.Threading;
using InSysDSDisplayer.Hooking;

using System.Windows.Forms;
using System.Windows.Interop;
using Application = System.Windows.Application;
using TouchEventEditor.Model;
using System.Xml.Linq;
using System.Windows.Markup;

namespace InSysDSDisplayer
{
    public delegate void MakeTouchPageDelegate();
    public delegate void ChangedTouchPageDelegate(UIElement playlistPlayer, UIElement changedPage);
    public delegate void PassedProjectLifeTimeDelegate(UIElement playlistPlayer);

    public class DSDisplayManager
    {
        #region InputDevice
        InputDevice inputDevice;
        int NumberOfKeyboards;
        Message message = new Message();

        #endregion

        public ChangedTouchPageDelegate ChangedTouchPageHandler;
        public PassedProjectLifeTimeDelegate PassedProjectLifeTimeHandler;       
        private string _currentPlayScreenID;
        private List<PageViewViewModel> _PreviewPageViewVMList;
        private List<PageViewViewModel> _CurrentPageViewVMList = new List<PageViewViewModel>();
        //private PageViewViewModel _PreviewPageViewVM = null;
        private PageViewViewModel _CurrentPageViewVM = null;
        private PlayType _PlayType = PlayType.PreviewPage;
        public CloseViewDelegate ClosePreviewHandler;
        private PreviewWindow _previewWindow;
        private bool _isFullScreen = false;
        private TouchProjectInfo touchProjectInfo;        
        private TimeSpan touchProjectTime;
        private Stopwatch _Stopwatch = new Stopwatch();
        private DispatcherTimer _TouchProjectTimer = new DispatcherTimer();
        private OnScreenKeyboardWindow onScreenKeyboardWindow;
        public WindowState PlayerWindowState { get; set; }
        public Window PlayerWindow { get; set; }
        List<UserTouchEvent> UserTouchEvents { get; set; }
        private Size playContainerSize;
        private double widthRatio;
        private double heightRatio;

        private NLog.Logger logger = NLog.LogManager.GetCurrentClassLogger();//NLog.LogManager.GetLogger("TouchDesignerLog");
        public string TouchProjectFilePath { get; set; }
        public bool IsPreviewPlaying
        {
            get;
            set;
        }

        private enum PlayType
        {
            PreviewPage,
            PreviewProject,
            PlayProject
        }

        #region 싱글톤
       
        private static readonly DSDisplayManager _instance;         

        public static DSDisplayManager Instance
        {
            get { return _instance; }
        }

        #endregion

        #region 생성자
        private DSDisplayManager()
        {
            _DSDisplayView = new DSDisplayView();
            this._TouchProjectTimer.Interval = TimeSpan.FromMilliseconds(100);
            this._TouchProjectTimer.Tick += new EventHandler(TouchProjectTimer_Tick);

            PageViewViewModel.GetDisplayCanvasHandler = new GetDisplayCanvasDelegate(GetDisplayCanvas);
        }

        ~DSDisplayManager()
        {            
        }

        static DSDisplayManager()
        {
            _instance = new DSDisplayManager();
           
        }
        #endregion

        #region Properties

        private DSDisplayView _DSDisplayView;
        public DSDisplayView DSDisplayView
        {
            get { return _DSDisplayView; }
            set { _DSDisplayView = value; }
        }

        private List<string> pageIDList = new List<string>();
        public List<string> PageIDList
        {
            get { return pageIDList; }
            private set { pageIDList = value; }
        }

        #endregion

        #region public Member

        /// <summary>
        /// 
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="displayWindow">null 일 경우 Default로 제공해주는 PreviewWindow 사용</param>
        private void CreatePreviewWindow(Window owner, Window displayWindow, bool isFullScreen)
        {
            if (displayWindow == null)
            {
                if (this._previewWindow != null)
                    this._previewWindow = null;
                this._previewWindow = new PreviewWindow();

                this._previewWindow.Owner = owner;
                this._previewWindow.WindowStartupLocation = WindowStartupLocation.Manual;
                this._previewWindow.Background = new SolidColorBrush(Colors.Transparent);
                this._previewWindow.WindowStyle = WindowStyle.None;
                this._previewWindow.WindowState = WindowState.Maximized;
            }
            else
            {
                //this._previewWindow = displayWindow;

                this._previewWindow.WindowStyle = WindowStyle.None;
                this._previewWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
                this._previewWindow.WindowState = WindowState.Maximized;
                this._previewWindow.ResizeMode = ResizeMode.NoResize;
            }

            this._isFullScreen = isFullScreen;
            this.IsPreviewPlaying = false;
                    
            this._previewWindow.Closed += (s, e) =>
            {   
                if (owner != null)
                    owner.IsEnabled = true;
                this.ClosedPreview();

                this.IsPreviewPlaying = false;
                this.ShowHideOnScreenKeyboardWindow(false);
                this.CloseOnScreenKeyboardWindow();
                this.StopWndProcHandler();//Input Device 종료

                owner.Focus();
            };

            PageViewViewModel.ParentWindow = this._previewWindow;
            StartWndProcHandler();//Input Device 시작            
            logger.Info("StartWndProcHandler method started.");            
        }       

        private void OnClosedPreviewPage(object sendObj)
        {
            if (this._PlayType == PlayType.PreviewPage)
            {
                PageViewViewModel pageViewVM = sendObj as PageViewViewModel;
                if (pageViewVM != null)//Page Play 종료되면 반복 재생
                {
                    pageViewVM.StopPageView();
                    this.NavigateProjectPage(pageViewVM);
                }
                /* 
                if (ClosePreviewHandler != null) // Page Play 종료후 작업 화면으로 넘어가기.
                    ClosePreviewHandler(sendObj);*/
            }
            else if (this._PlayType == PlayType.PreviewProject)
            {
                PageViewViewModel pageViewVM = sendObj as PageViewViewModel;
                if (pageViewVM != null)
                {
                    int nextLayerIndex = pageViewVM.PageViewInfo.PageInfo.PageLayerIndex + 1;
                    var sameLayerIndexList = this._CurrentPageViewVMList.Where(o => o.PageViewInfo.PageInfo.PageLayerIndex == nextLayerIndex);
                    var sameParentList = sameLayerIndexList.Where(o => o.PageViewInfo.PageInfo.ParentPageGuid.Equals(pageViewVM.PageViewInfo.PageInfo.PageGuid) == true);
                    var nextPageViewVM = sameParentList.Where(o => o.PageViewInfo.PageInfo.PageDisplayOrder == 1).FirstOrDefault();

                    if (nextPageViewVM != null)
                    {
                        pageViewVM.StopPageView();
                        this.NavigateProjectPage(nextPageViewVM);
                    }
                    else
                    {
                        pageViewVM.StopPageView();
                        this.PlayRootTouchPage();
                    }
                }
            }
            else if (this._PlayType == PlayType.PlayProject)
            {
                PageViewViewModel pageViewVM = sendObj as PageViewViewModel;
                if (pageViewVM != null)
                {
                    int nextLayerIndex = pageViewVM.PageViewInfo.PageInfo.PageLayerIndex + 1;
                    var sameLayerIndexList = this._CurrentPageViewVMList.Where(o => o.PageViewInfo.PageInfo.PageLayerIndex == nextLayerIndex);
                    var sameParentList = sameLayerIndexList.Where(o => o.PageViewInfo.PageInfo.ParentPageGuid.Equals(pageViewVM.PageViewInfo.PageInfo.PageGuid) == true);
                    var nextPageViewVM = sameParentList.Where(o => o.PageViewInfo.PageInfo.PageDisplayOrder == 1).FirstOrDefault();

                    if (nextPageViewVM != null)
                    {
                        pageViewVM.StopPageView();
                        this.NavigateProjectPage(nextPageViewVM);
                    }
                }
            }
        }

        public void CloadPreviewWindow()
        {
            this.StopWndProcHandler();
            this._previewWindow.Close();
        }

        public void ClosedPreview()
        {
            foreach (PageViewViewModel pvvm in _CurrentPageViewVMList)
            {
                pvvm.ClosePageView();
            }
            _CurrentPageViewVMList.Clear();           
        }

        public void ClosedCurrentTouchSchedule()
        {
            foreach (PageViewViewModel pvvm in _CurrentPageViewVMList)
            {
                pvvm.ClosePageView();
            }

            _CurrentPageViewVMList.Clear();            
        }
       
        public void BackupCurrentTouchSchedule()
        {
            //foreach (PageViewViewModel pvvm in _CurrentPageViewVMList)
            //{
            //    pvvm.ClosePageView();
            //}

            this._PreviewPageViewVMList = new List<PageViewViewModel>();
            foreach (var vmitem in _CurrentPageViewVMList)
            {
                this._PreviewPageViewVMList.Add(vmitem);
            }            
            //_CurrentPageViewVMList.Clear();
            //this._PreviewPageViewVM = this._CurrentPageViewVM;
        }

        public void ClosedPreviousTouchScheduleProgram()
        {
            if (this._PreviewPageViewVMList != null)
            {
                foreach (PageViewViewModel pvvm in this._PreviewPageViewVMList)
                {
                    pvvm.ClosePageView();
                }
                this._PreviewPageViewVMList.Clear();
                this._PreviewPageViewVMList = null;
            }
        }

        /// <summary>
        /// DisplayWindow상에서 Play할 Page를 등록한다.
        /// </summary>
        /// <param name="contentPage">DisplayCanvas인 Page 객체</param>
        /// <param name="pageInfo">Page 정보.</param>
        /// <returns></returns>
        public PageViewViewModel AddPageViewVM(DisplayCanvas contentPage, TouchPageInfo pageInfo)
        {
            PageViewViewModel pageViewVM = new PageViewViewModel(contentPage, pageInfo);
            pageViewVM.ClosePageLifeTimeHandler = new CloseViewDelegate(OnClosedPreviewPage);
            pageViewVM.ClosePreviewHandler = new ClosePreviewDelegate(CloadPreviewWindow);
            pageViewVM.GoToPageActionEventHandler = new GoToPageActionEventDelegate(onGoToPageActionEvent);
            pageViewVM.ShowHideActionEventHandler = new ShowHideActionEventDelegate(onShowHideActionEvent);
            pageViewVM.HyperlinkActionEventHandler = new HyperlinkActionEventDelegate(onHyperlinkActionEvent);
            pageViewVM.OpenFileActionEventHandler = new OpenFileActionEventDelegate(onOpenFileActionEvent);
            
            _CurrentPageViewVMList.Add(pageViewVM);
            
            return pageViewVM;
        }

        public UIElement GetRootTouchPage()
        {
            //System.Windows.MessageBox.Show("GetRootTouchPage");
            var rootPageInfo = touchProjectInfo.PageInfoList.Where(o => o.PageLayerIndex == 0).FirstOrDefault();
            if (rootPageInfo != null)
            {
                UIElement uielement = MakeTouchPage(rootPageInfo);

                //MakeTouchPageDelegate MakeTouchPageHandler = new MakeTouchPageDelegate(MakeTouchPage);
                //MakeTouchPageHandler.BeginInvoke(new AsyncCallback(CompletedMakeTouchPageCallback), MakeTouchPageHandler);

                return uielement;
            }

            return null;
        }

        public UIElement GetTouchPage(string pageid)
        {
            var pageInfo = touchProjectInfo.PageInfoList.Where(o => o.PageGuid.ToLower().Equals(pageid.ToLower()) == true).FirstOrDefault();
            if (pageInfo != null)
            {
                UIElement uielement = MakeTouchPage(pageInfo);                
                return uielement;
            }

            return null;
        }

        private void MakeTouchPage()
        {
            foreach (var pageinfo in touchProjectInfo.PageInfoList)
            {
                var addpageinfo = _CurrentPageViewVMList.Where(o => o.PageViewInfo.PageInfo.Equals(pageinfo) == true).FirstOrDefault();
                if (addpageinfo == null)
                {
                    this.MakeTouchPage(pageinfo);
                }
            }
        }

        private static void CompletedMakeTouchPageCallback(IAsyncResult ar)
        {
            try
            {
                MakeTouchPageDelegate s1 = (MakeTouchPageDelegate)ar.AsyncState;
                s1.EndInvoke(ar);
            }
            catch
            {
            }
        }

        public void InitTouch()
        {
            this._CurrentPageViewVMList.Clear();            
        }

        public TouchProjectInfo LoadTouchProject(string projectPath)
        {
            try
            {
                touchProjectInfo = InSysTouchflowData.InSysDataManager.Load(projectPath, SerializedFormat.Binary);
                TouchProjectFilePath = projectPath;
                touchProjectTime = TimeSpan.FromSeconds(touchProjectInfo.DefaultPageLifeTime);//터치 프로젝트 라이프 타임.

                //this.StartWndProcHandler();

                return touchProjectInfo;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public Dictionary<string, object> LoadTouchProjectData(string projectPath, double scheduleTime)
        {
            try
            {
                Dictionary<string, object> touchProjectData = InSysTouchflowData.InSysDataManager.LoadTouchProjectData(projectPath, SerializedFormat.Binary);
                TouchProjectFilePath = projectPath;
                touchProjectInfo = touchProjectData["TouchflowData"] as TouchProjectInfo;
                if (touchProjectInfo != null)
                {
                    touchProjectTime = TimeSpan.FromSeconds(scheduleTime);
                    //if (touchProjectData.ContainsKey("ScheduleTime") == true)
                    //    touchProjectTime = (TimeSpan)touchProjectData["ScheduleTime"];
                    //else
                    //    touchProjectTime = (TimeSpan)touchProjectData["PlayTime"];
                }

                //this.StartWndProcHandler();
                return touchProjectData;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public void StartTouchProjectTimer()
        {
            if (this._TouchProjectTimer == null)
            {
                this._TouchProjectTimer = new DispatcherTimer();                   
            }            

            if (this._Stopwatch == null)
            {
                this._Stopwatch = new Stopwatch();
            }

            this._TouchProjectTimer.Start();
            this._Stopwatch.Reset();
            this._Stopwatch.Start();
        }

        public void StopTouchProjectTimer()
        {
            if (_TouchProjectTimer != null)
            {
                _TouchProjectTimer.Stop();
            }

            if (_Stopwatch != null)
            {
                _Stopwatch.Stop();
                _Stopwatch.Reset();
            }
        }

        public void ResetTouchStopWatch()
        {
            if (_Stopwatch != null)
            {               
                _Stopwatch.Reset();
                _Stopwatch.Start();
            }

            if (PageViewViewModel.TouchDownHandler != null)
                PageViewViewModel.TouchDownHandler();
        }

        void TouchProjectTimer_Tick(object sender, EventArgs e)
        {
            CheckTouchProjectTimer(sender as DispatcherTimer);
        }

        private void CheckTouchProjectTimer(DispatcherTimer dispathcherTimer)
        {
            if (dispathcherTimer != null)
            {
                TimeSpan ts = _Stopwatch.Elapsed;
                if (this._Stopwatch.Elapsed >= touchProjectTime)//Touch Project Time 을 넘겼을 경우에 대해 처리한다.
                {
                    StopTouchProjectTimer();
                    //종료 이벤트 전달.
                    if (PassedProjectLifeTimeHandler != null)
                    {
                        PassedProjectLifeTimeHandler(null);
                    }
                    
                    return;
                }
            }
        }

        public void SetDisplayUIByTouchflowPlayer(string openProjectPath)
        {
            touchProjectInfo = InSysTouchflowData.InSysDataManager.Load(openProjectPath, SerializedFormat.Binary);
            TouchProjectFilePath = openProjectPath;
            foreach (var pageinfo in touchProjectInfo.PageInfoList)
            {
                MakeTouchPage(pageinfo);
            }
        }
        
        public UIElement MakeTouchPage(TouchPageInfo touchPageInfo)
        {
            try
            {
                DisplayCanvas touchPage = new DisplayCanvas();
                touchPage.Width = touchProjectInfo.PageSize.Width;
                touchPage.Height = touchProjectInfo.PageSize.Height;
                touchPage.Background = touchPageInfo.Background; 
                //touchPageInfo.SetPageInfo(touchPageInfo);

                foreach (TouchContentInfo item in touchPageInfo.Contents)
                {
                    FrameworkElement inSysContentControl = InSysControlManager.GetNewSerializationInSysControl(item.ContentType, item.DesignElementProperties, ViewState.Play) as FrameworkElement;
                    if (inSysContentControl != null)
                    {
                        IDesignElement designElement = inSysContentControl as IDesignElement;
                        if (designElement != null)
                        {
                            item.ContentObject = inSysContentControl;

                            #region Playlist의 경로 정보를 가상경로(CONTENTSVALUE)를 이용하여 Opne Project시 절대경로(Content)로 변경한다.
                            if (item.DesignElementProperties.ContainsKey("Playlist") == true)
                            {
                                logger.Info(string.Format("Type: {0}, Name: {1}", designElement.InSysControlType.ToString(), designElement.Name));
                                switch (designElement.InSysControlType)
                                {
                                    case InSysControlType.InSysStreamingBox:
                                    case InSysControlType.InSysScrollText:
                                        break;
                                    case InSysControlType.InSysWebControl:
                                        break;
                                    default:
                                        {
                                            FileInfo projectFileinfo = new FileInfo(TouchProjectFilePath);
                                            if (projectFileinfo.Exists == true)
                                            {
                                                List<PlaylistItem> playlistItems = (List<PlaylistItem>)item.DesignElementProperties["Playlist"];
                                                foreach (var playlistitem in playlistItems)
                                                {
                                                    try
                                                    {
                                                        string fullpath = string.Format("{0}\\{1}", projectFileinfo.Directory.FullName, playlistitem.CONTENTSVALUE);
                                                        if (File.Exists(fullpath) == true)
                                                        {
                                                            playlistitem.Content = fullpath;
                                                        }
                                                    }
                                                    catch
                                                    {
                                                        continue;
                                                    }
                                                    finally
                                                    {
                                                        //logger.Info(string.Format("PlaylistItem: {0}", playlistitem.Content));
                                                    }
                                                }
                                            }
                                        }
                                        break;
                                }
                            }
                            #endregion

                            if (item.DesignElementProperties.Keys.Contains("ActionEvent") == true)
                                designElement.SetActionEvent(item.DesignElementProperties["ActionEvent"]);//이벤트 설정

                            #region ContentControl 타입에 따라 Insert 하는 위치 구분.
                            switch (designElement.InSysControlType)
                            {
                                case InSysControlType.InSysAudioBox:
                                    inSysContentControl.Visibility = Visibility.Collapsed;
                                    designElement.Visibility = Visibility.Collapsed;
                                    break;                               
                                default:
                                    break;
                            }

                            #endregion //ContentControl 타입에 따라 Insert 하는 위치 구분.

                            touchPage.Children.Add(inSysContentControl);
                        }
                    }
                }

                touchPage.PreviewMouseDown += (s, e) =>
                {
                    ResetTouchStopWatch();
                };
                touchPage.PreviewKeyDown += (s, e) =>
                {
                    ResetTouchStopWatch();
                };
                PageViewViewModel pageViewVM = this.AddPageViewVM(touchPage, touchPageInfo);
                this._CurrentPageViewVM = pageViewVM;//루트 Load시 처음 플레이되는PageView ViewModel 저장ㅏ. onGoToPageActionEvent () 함수가 뒤에 호출 된다.
                return pageViewVM.PageView;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
       
        /// <summary>
        /// GoTo Page Action Event
        /// </summary>
        /// <param name="movingPageID">이동할 Page ID</param>
        /// <param name="currentPageViewVM">현재 Play중인 PageViewModel</param>
        private void onGoToPageActionEvent(string movingPageID, object currentPageViewVM)
        {
            if (this._PlayType == PlayType.PreviewPage)
                return;

            if (string.IsNullOrEmpty(movingPageID) == true)
                return;

          
            var pageViewVM = this._CurrentPageViewVMList.Where(o => o.PageViewInfo.PageInfo.PageGuid.Equals(movingPageID) == true).FirstOrDefault();
            if (pageViewVM != null)
            {
                ///* 사용하지 않음.
                #region Touch Page의 UI Control을 전달한다.
                if (ChangedTouchPageHandler != null)
                {
                    ChangedTouchPageHandler(null, pageViewVM.PageView);
                    logger.Info(string.Format("send changed page({0} ({1})) to palyer.", pageViewVM.PageViewInfo.PageInfo.PageNameTemp, pageViewVM.PageViewInfo.PageInfo.PageGuid));
                }
                #endregion
                //*/
                this.NavigateProjectPage(pageViewVM);
                logger.Info(string.Format("go to page({0} ({1})).", pageViewVM.PageViewInfo.PageInfo.PageNameTemp, pageViewVM.PageViewInfo.PageInfo.PageGuid));
            }
            else
            {
                if (this.touchProjectInfo != null)
                {
                    #region 이동할 Page가 등록되어 있지 않을 경우 등록한다. - i-Vision SaaS에서 Play시 이 부분이 동작한다.
                    var pageinfo = this.touchProjectInfo.PageInfoList.Where(o => o.PageGuid.Equals(movingPageID) == true).SingleOrDefault();
                    if (pageinfo != null)
                    {
                        UIElement uielement = this.MakeTouchPage(pageinfo);

                        pageViewVM = this._CurrentPageViewVMList.Where(o => o.PageViewInfo.PageInfo.PageGuid.Equals(movingPageID) == true).FirstOrDefault();
                        if (pageViewVM != null)
                        {
                            ///* 사용하지 않음.
                            #region Touch Page의 UI Control을 전달한다.
                            if (ChangedTouchPageHandler != null)
                            {
                                ChangedTouchPageHandler(null, uielement);
                                logger.Info(string.Format("send changed page({0} ({1})) to palyer.", pageViewVM.PageView.Name, movingPageID));
                            }
                            #endregion
                            //*/
                            this.NavigateProjectPage(pageViewVM);
                            logger.Info(string.Format("go to page({0} ({1})).", pageViewVM.PageView.Name, movingPageID));
                        }
                    }
                    #endregion
                }
            }

            this._CurrentPageViewVM = pageViewVM;//현재 플레이중인 PageView ViewModel 저장.

            PageViewViewModel previewPageViewVM = currentPageViewVM as PageViewViewModel;
            if (previewPageViewVM != null)
            {
                previewPageViewVM.StopPageView();
            }
           
        }

        private void onShowHideActionEvent(object targetContentInfo, object showhideType, object currentPageViewVM)
        {
            PageViewViewModel pageVM = currentPageViewVM as PageViewViewModel;
            if (pageVM != null)
            {
                string controlName = (string)targetContentInfo;
                ShowHideType type = (ShowHideType)showhideType;
                if (string.IsNullOrEmpty(controlName) != true)
                {                   
                    foreach (UIElement child in pageVM.PageView.PageDisplayCanvas.Children)
                    {
                        IDesignElement designElement = child as IDesignElement;
                        if (designElement == null)
                            continue;
                        if (designElement.Name.Equals(controlName) == true)
                        {
                            if (designElement.IsApplyLifeTime == false)
                            {
                                SetControlVisibility(child as FrameworkElement, designElement, type);
                            }
                            else
                            {
                                if (designElement.IsInLifeTime == true)
                                {
                                    SetControlVisibility(child as FrameworkElement, designElement, type);
                                }
                            }
                            break;
                        }
                    }
                }
            }          
        }

        private void SetControlVisibility(FrameworkElement frameworkElement, IDesignElement designElement, ShowHideType type)
        {
            //designElement.UserVisibilitySet = true;//사용자 설정.
            if (type == ShowHideType.Hide)
            {
                frameworkElement.Visibility = Visibility.Hidden;
                designElement.Visibility = Visibility.Hidden;
            }
            else if (type == ShowHideType.Show)
            {
                frameworkElement.Visibility = Visibility.Visible;
                designElement.Visibility = Visibility.Visible;
            }
            else//토글 기능.
            {
                if (designElement.Visibility == Visibility.Hidden)
                {
                    frameworkElement.Visibility = Visibility.Visible;
                    designElement.Visibility = Visibility.Visible;
                }
                else
                {
                    frameworkElement.Visibility = Visibility.Hidden;
                    designElement.Visibility = Visibility.Hidden;
                }
            }
            logger.Info(string.Format("Element does \"{0}\" state", designElement.Visibility));
        }

        private void onHyperlinkActionEvent(string url)
        {
            System.Diagnostics.Process.Start(url);
        }

        private void onOpenFileActionEvent(string openfile)
        {
            System.Diagnostics.Process.Start(openfile);
        }

        public void PlayRootPage()
        {
            var rootPageView = _CurrentPageViewVMList.Where(o => o.PageViewInfo.PageInfo.PageLayerIndex == 0).FirstOrDefault();
            if (rootPageView != null)
            {
                this.NavigatePage(_DSDisplayView.DSDisplayerContainer, rootPageView);
            }            
        }

        public void PreviewPage(Window owner, DisplayCanvas contentPage, TouchPageInfo pageInfo)
        {
            this.SetOnScreenKeyboardWindow(owner);

            this.CreatePreviewWindow(owner, null, false);
            this.PreviewPage(contentPage, pageInfo);
            this._previewWindow.Show();
        }

        public void PreviewPage(DisplayCanvas contentPage, TouchPageInfo pageInfo)
        {
            PageViewViewModel pageViewVM = this.AddPageViewVM(contentPage, pageInfo);

            this.ShowHideOnScreenKeyboardWindow(true);

            this.NavigateProjectPage(pageViewVM);
            this._PlayType = PlayType.PreviewPage;

            
        }
               
        public void PreviewProject(Window owner, Window displayWindow, bool isFullScreen)
        {
            this.SetOnScreenKeyboardWindow(owner);

            this._PlayType = PlayType.PreviewProject;
            this.CreatePreviewWindow(owner, displayWindow, isFullScreen);            
            this.PlayRootTouchPage();
            this._previewWindow.Show();

            
        }

        public void PlayTouch()
        {
            this._PlayType = PlayType.PlayProject;            
            this.PlayRootTouchPage();            
        }
        
        public void PlayRootTouchPage()
        {
            var rootPageView = _CurrentPageViewVMList.Where(o => o.PageViewInfo.PageInfo.PageLayerIndex == 0).FirstOrDefault();
            if (rootPageView != null)
            {
                this.NavigateProjectPage(rootPageView);
                StartTouchProjectTimer();
               
            }                      
        }

        #endregion        

        public void PlayPageView(string pageid)
        {
            NavigatePage(_DSDisplayView.DSDisplayerContainer, pageid);
        }

        public void PlayPage(PageViewViewModel pageViewVM)
        {
            pageViewVM.Play_PageView();
            _currentPlayScreenID = pageViewVM.GetScreenID();//현재 ScreenPage ID
            //this.CurrentPageViewVM = pageViewVM;
        }

        private void NavigatePage(Border container, string screenID)
        {
            if (container == null)
                container = _DSDisplayView.DSDisplayerContainer;
          
            var targetScreen = Get_PageViewViewModel(screenID);

            if (targetScreen != null)
            {
                this.NavigatePage(container, targetScreen);
            }
        }

        public void NavigatePage(Border container, DisplayCanvas page)
        {
            if (container == null)
                container = _DSDisplayView.DSDisplayerContainer;

            var targetScreen = Get_PageViewViewModel(page);

            if (targetScreen != null)
            {
                this.NavigatePage(container, targetScreen);
            }
        }

        private void NavigatePage(Border container, PageViewViewModel screen)
        {            
            container.Child = null;            

            container.Child = screen.PageView;
            container.UpdateLayout();

            double x_rate = container.ActualWidth / screen.PageView.Width;
            double y_rate = container.ActualHeight / screen.PageView.Height;
            double scaleValue = 1.0;
            if (x_rate < 1 && y_rate < 1)
            {
                if (x_rate < y_rate)
                {
                    scaleValue = x_rate;
                }
                else
                {
                    scaleValue = y_rate;
                }
            }
            else if (x_rate > 1 && y_rate > 1)
            {
                if (x_rate < y_rate)
                {
                    scaleValue = x_rate;
                }
                else
                {
                    scaleValue = y_rate;
                }
            }
            else
            {
                if (x_rate < y_rate)
                {
                    scaleValue = x_rate;
                }
                else
                {
                    scaleValue = y_rate;
                }
            }

            double translateX = container.ActualWidth / 2 - (screen.PageView.Width / 2 * scaleValue);

            screen.PageView.PageDisplayCanvas.SetScaleTransform_LessThan(scaleValue);
            screen.PageView.PageDisplayCanvas.SetTranslateTransform(translateX, 0);

            this.PlayPage(screen);
        }

        private void NavigateProjectPage(PageViewViewModel screen)
        {
            if (this._previewWindow != null)
            {
                this._previewWindow.PreviewBorder.Child = screen.PageView;
                //this._previewWindow.Content = screen.PageView;

                double x_rate = SystemParameters.FullPrimaryScreenWidth / screen.PageView.Width;
                double y_rate = SystemParameters.FullPrimaryScreenHeight / screen.PageView.Height;

                if (this._isFullScreen == false)
                {
                    double scaleValue = 1.0;
                    if (x_rate < 1 && y_rate < 1)
                    {
                        if (x_rate < y_rate)
                        {
                            scaleValue = x_rate;
                        }
                        else
                        {
                            scaleValue = y_rate;
                        }
                    }
                    else if (x_rate > 1 && y_rate > 1)
                    {
                        if (x_rate < y_rate)
                        {
                            scaleValue = x_rate;
                        }
                        else
                        {
                            scaleValue = y_rate;
                        }
                    }
                    else
                    {
                        if (x_rate < y_rate)
                        {
                            scaleValue = x_rate;
                        }
                        else
                        {
                            scaleValue = y_rate;
                        }
                    }

                    this._previewWindow.Width = screen.PageView.Width * scaleValue + SystemParameters.ResizeFrameVerticalBorderWidth * 2;
                    this._previewWindow.Height = screen.PageView.Height * scaleValue + SystemParameters.ResizeFrameHorizontalBorderHeight * 2;

                    this._previewWindow.Left = SystemParameters.FullPrimaryScreenWidth / 2 - this._previewWindow.Width / 2;
                    this._previewWindow.Top = 0;

                    if (scaleValue < 1.0)
                    {          
                        screen.PageView.SetScaleValue(scaleValue, scaleValue);  
                        screen.PageView.SetScaleTransform_LessThan(scaleValue);
                    }
                    else
                    {
                        screen.PageView.SetScaleValue(scaleValue, scaleValue); 
                        screen.PageView.SetScaleTransform_MoreThan(scaleValue);
                    }
                }
                else
                {
                    screen.PageView.SetScaleTransform(x_rate, y_rate);
                }

                screen.LeftPos = this._previewWindow.Left;
                screen.TopPos = this._previewWindow.Top;
            }
            else //i-Vision SaaS의 Player에서 실행시 호출됨.
            {
                if (PlayerWindowState == WindowState.Normal)
                {
                    this.widthRatio = (this.playContainerSize.Width - (SystemParameters.ResizeFrameVerticalBorderWidth * 2)) / this.touchProjectInfo.PageSize.Width;
                    this.heightRatio = (this.playContainerSize.Height - (SystemParameters.ResizeFrameHorizontalBorderHeight * 2) - SystemParameters.WindowCaptionHeight) / this.touchProjectInfo.PageSize.Height;
                }
                else
                {
                    this.widthRatio = this.playContainerSize.Width / this.touchProjectInfo.PageSize.Width;
                    this.heightRatio = this.playContainerSize.Height / this.touchProjectInfo.PageSize.Height;
                }
                               
                screen.PageView.SetScaleValue(this.widthRatio, this.heightRatio);                 
                screen.PageView.SetScaleTransform(this.widthRatio, this.heightRatio);
            }

            if (string.IsNullOrEmpty(this.TouchProjectFilePath) == false)
            {
                FileInfo fi = new FileInfo(this.TouchProjectFilePath);
                InSysControlManager.LoadUserTouchEventFromXml(fi.Directory.FullName);
            }
            
            this.PlayPage(screen);
        }

        public void ChangePage(Border container, string pageid)
        {
            if (container == null)
                container = _DSDisplayView.DSDisplayerContainer;            

            var targetScreen = Get_PageViewViewModel(pageid);

            if (targetScreen != null)
            {
                container.Child = targetScreen.PageView;

                this.PlayPage(targetScreen);                
            }
        }

        public void ChangePage(Border container, DisplayCanvas page)
        {
            if (container == null)
                container = _DSDisplayView.DSDisplayerContainer;

            var targetScreen = Get_PageViewViewModel(page);

            if (targetScreen != null)
            {
                this.PlayPage(targetScreen);
                container.Child = targetScreen.PageView;                
            }
        }       

        /// <summary>
        /// Guid로 Page 찾기
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private PageViewViewModel Get_PageViewViewModel(string id)
        {
            var pageViewVM = _CurrentPageViewVMList.Where(o => o.PageViewInfo.PageInfo.PageGuid.Equals(id) == true).SingleOrDefault();

            return pageViewVM;
        }

        private PageViewViewModel Get_PageViewViewModel(Canvas page)
        {
            PageViewViewModel pageViewVM = this._CurrentPageViewVMList.Where(o => o.Get_PageDisplayCanvas().Equals(page) == true).SingleOrDefault();

            return pageViewVM;
        }       

        public void IsShowDSDisplayView(bool isShow)
        {
            if (isShow == false)
                _DSDisplayView.Visibility = Visibility.Collapsed;
            else
                _DSDisplayView.Visibility = Visibility.Visible;
        }

        public static UIElement CreateNewInSysControl(string ctrlTypeName)
        {
            //return InSysControlManager.CreateInSysControlObjectFromToolBoxType(ctrlTypeName, ViewState.Play);
            return InSysControlManager.CreateInSysControlObject(InSysControlManager.GetControlType(ctrlTypeName), ViewState.Play);
        }


        public static void CopyProperties(FrameworkElement inSysControl, FrameworkElement sourceControl)
        {
            InSysControlManager.CopyProperties(inSysControl, sourceControl);
        }

        public static void SetProperty(FrameworkElement inSysControl, FrameworkElement sourceControl, string propertyName)
        {
            InSysControlManager.SetProperty(inSysControl, sourceControl, propertyName);
        }
       
       
        public void PlayScreen(string screenID)
        {
            PageViewViewModel screenPage = Get_PageViewViewModel(screenID);
            this.PlayPage(screenPage);
        }
     
        public void PlayScreen(Canvas page)
        {
            PageViewViewModel playScreen = this.Get_PageViewViewModel(page);// this._screenPageList.Where(o => o.Value.GetScreen().Equals(screen) == true).Select(o => o.Value).SingleOrDefault();
            if (playScreen != null)
            {
                this.PlayPage(playScreen);
            }           
        }

        public void SetOnScreenKeyboardParent(Window displayWindow)
        {
            PageViewViewModel.ParentWindow = displayWindow;
        }

        public void CloseOnScreenKeyboard()
        {
            PageViewViewModel.CloseOnScreenKeyboard();
        }

        public void ShowOnScreenKeyboard(FrameworkElement element, Window owner)
        {
            PageViewViewModel.ShowOnScreenKeyboard(element, owner);
        }

        public void ResetTouchScreenScheduleTime()
        {
            ResetTouchStopWatch();
        }

        private void HookManager_KeyDown(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            ResetTouchStopWatch();
        }

        private void HookManager_MouseUp(object sender, System.Windows.Forms.MouseEventArgs e)
        {
            try
            {
                ResetTouchStopWatch();
                MonitoringRectArea(e);
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message + " - " + ex.StackTrace);
            }
        }

        public static void TouchDown(TouchDownDelegate delegateFunc)
        {
            PageViewViewModel.TouchDownHandler = delegateFunc;
        }

        public void MonitoringRectArea(System.Windows.Forms.MouseEventArgs e)
        {
            MonitoringRectArea(e.X, e.Y);
        }    
  
        public void MonitoringRectArea(double x_point, double y_point)
        {
            double x = x_point;
            double y = y_point;

            //logger.Info(string.Format("User touched X:{0}, Y:{1} points", x_point, y_point));            
            if (PageViewViewModel.RectAreaList.Count > 0)
            {
                try
                {

                    var keyValues = PageViewViewModel.RectAreaList.Where(o => IncludePoint(o.Value, new Point(x, y)) == true);
                    if (keyValues.Count() > 0)
                    {
                        int max = keyValues.Max(o => o.Key.ZIndex);
                        var keyvalue = keyValues.Where(o => o.Key.ZIndex == max).FirstOrDefault();
                        var designElement = keyvalue.Key;
                        if (designElement != null)
                        {
                            if (designElement.RaiseActionEventHandler != null)
                            {
                                designElement.RaiseActionEventHandler(designElement.ActionEvent);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    System.Windows.MessageBox.Show(ex.Message);
                }
            }
            else
            {
                //System.Windows.MessageBox.Show(PageViewViewModel.RectAreaList.Count.ToString());
            }
        }
        string sss = "";
        public bool IncludePoint(Rect rect, Point point)
        {
            var pageViewVM = _CurrentPageViewVMList.Where(o => o.PageViewInfo.PageInfo.PageGuid.Equals(this._currentPlayScreenID) == true).SingleOrDefault();

            if (pageViewVM != null && pageViewVM.PageView != null)
            {
                try
                {

                    Point visualPoint = pageViewVM.PageView.PointFromScreen(point);
                    if (rect.X <= visualPoint.X && (rect.X + rect.Width) >= visualPoint.X)
                    {
                        if (rect.Y <= visualPoint.Y && (rect.Y + rect.Height) >= visualPoint.Y)
                        {
                            logger.Info(string.Format("User touch point(X:{0}, Y:{1}) include in rectarea(X:{2}, Y:{3}, Width:{4}, Height:{5})", point.X, point.Y, rect.X, rect.Y, rect.Width, rect.Height));
                            return true;
                        }
                    }
                }
                catch (Exception)
                {
                    return false;
                }
            }

            return false;
        }

        void SetOnScreenKeyboardWindow(Window owner)
        {
            if (onScreenKeyboardWindow == null)
            {
                onScreenKeyboardWindow = new OnScreenKeyboardWindow();
                this.onScreenKeyboardWindow.Owner = owner;
                onScreenKeyboardWindow.ShowOnScreenKeyboardHandler = () =>
                {
                    PageViewViewModel.ShowOnScreenKeyboard(null, owner);                    
                };


                DSDisplayManager.TouchDown(new TouchDownDelegate(() =>
                {
                    if(onScreenKeyboardWindow != null)// 이 객체에 대한 
                        onScreenKeyboardWindow.ResetUserTouchCheckTimer();
                }
                ));
            }
        }

        void ShowHideOnScreenKeyboardWindow(bool isShow)
        {
            if (onScreenKeyboardWindow != null)
            {                
                if (isShow == true)
                {
                    onScreenKeyboardWindow.ShowKeyboardIconWindow();
                }
                else
                {
                    onScreenKeyboardWindow.HideKeyboardIconWindow();
                }
            }
        }

        void CloseOnScreenKeyboardWindow()
        {
            if (onScreenKeyboardWindow != null)
            {
                this.onScreenKeyboardWindow.Close();
                this.onScreenKeyboardWindow = null;
            }
        }

        #region InputDevice

        public IntPtr WndProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
        {
            if (inputDevice != null)
            {
                // I could have done one of two things here.
                // 1. Use a Message as it was used before.
                // 2. Changes the ProcessMessage method to handle all of these parameters(more work).
                //    I opted for the easy way.

                //Note: Depending on your application you may or may not want to set the handled param.

                message.HWnd = hwnd;
                message.Msg = msg;
                message.LParam = lParam;
                message.WParam = wParam;

                inputDevice.ProcessMessage(message);
            }
            //Console.WriteLine(id.ToString());
            return IntPtr.Zero;
        }

        public void StartWndProcHandler()
        {
            IntPtr hwnd = IntPtr.Zero;
            Window myWin = Application.Current.MainWindow;

            try
            {
                hwnd = new WindowInteropHelper(myWin).Handle;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            //Get the Hwnd source   
            HwndSource source = HwndSource.FromHwnd(hwnd);
            //Win32 queue sink
            source.AddHook(new HwndSourceHook(WndProc));

            inputDevice = new InputDevice(source.Handle);
            NumberOfKeyboards = inputDevice.EnumerateDevices();
            inputDevice.KeyPressed += new InputDevice.DeviceEventHandler(_KeyPressed);
            inputDevice.MousePressed += new InputDevice.MouseDeviceEventHandler(_MousePressed);
        }

        public void StopWndProcHandler()
        {
            if (inputDevice != null)
            {
                inputDevice.KeyPressed -= new InputDevice.DeviceEventHandler(_KeyPressed);
                inputDevice.MousePressed -= new InputDevice.MouseDeviceEventHandler(_MousePressed);

                inputDevice = null;
            }
        }

        public void _MousePressed(object sender, InputDevice.MouseControlEventArgs e)
        {
            Point point = e.Point;
            //PageView pv = (PageView)this._previewWindow.PreviewBorder.Child;
            //Point point2 = pv.PointFromScreen(point);
            //Console.WriteLine(point2.X.ToString() + "; " + point2.Y.ToString());
            
            ResetTouchStopWatch();
            MonitoringRectArea(point.X, point.Y);  
        }

        public void _KeyPressed(object sender, InputDevice.KeyControlEventArgs e)
        {
            ResetTouchStopWatch();
        }
        #endregion

        public void SizeChangedScreen(Size size)
        {
			if (this.touchProjectInfo != null)
			{

                
                        
				try
                {
                    this.playContainerSize = size;
                    if (PlayerWindowState == WindowState.Normal)
                    {
                        this.widthRatio = (this.playContainerSize.Width - (SystemParameters.ResizeFrameVerticalBorderWidth * 2)) / this.touchProjectInfo.PageSize.Width;
                        this.heightRatio = (this.playContainerSize.Height - (SystemParameters.ResizeFrameHorizontalBorderHeight * 2) - SystemParameters.WindowCaptionHeight) / this.touchProjectInfo.PageSize.Height;
                    }
                    else
                    {
                        this.widthRatio = this.playContainerSize.Width / this.touchProjectInfo.PageSize.Width;
                        this.heightRatio = this.playContainerSize.Height / this.touchProjectInfo.PageSize.Height;
                    }

					

					//this.widthRatio = (this.playContainerSize.Width - (SystemParameters.ResizeFrameVerticalBorderWidth * 2)) / this.touchProjectInfo.PageSize.Width;
					//this.heightRatio = (this.playContainerSize.Height - (SystemParameters.ResizeFrameHorizontalBorderHeight * 2) - SystemParameters.WindowCaptionHeight) / this.touchProjectInfo.PageSize.Height;

					var pageViewVM = _CurrentPageViewVMList.Where(o => o.PageViewInfo.PageInfo.PageGuid.Equals(this._currentPlayScreenID) == true).SingleOrDefault();

					if (pageViewVM != null && pageViewVM.PageView != null)
					{
						pageViewVM.PageView.SetScaleValue(this.widthRatio, this.heightRatio);
						pageViewVM.PageView.SetScaleTransform(widthRatio, heightRatio);
					}
				}
				catch { }
			}
        }

        public void SetPlayerContainerSize(Size size)
        {
            this.playContainerSize = size;            
        }

        public DisplayCanvas GetDisplayCanvas(string screenID)
        {
            DisplayCanvas displayCanvas = this._CurrentPageViewVMList.Where(o => o.PageViewInfo.PageInfo.PageGuid.Equals(screenID) == true).Select(o => o.PageView.PageDisplayCanvas).FirstOrDefault();
            if (displayCanvas != null)
            {
                return displayCanvas;
            }

            return null;
        }
    }
}
