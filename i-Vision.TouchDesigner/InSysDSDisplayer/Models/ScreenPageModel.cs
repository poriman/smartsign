﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace InSysDSDisplayer.Models
{
    public class ScreenPageInfo
    {
        public Guid ScreenID
        {
            get;
            private set;
        }

        public ScreenPageInfo()
        {
            ScreenID = Guid.NewGuid();
        }        
    }
}
