﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using InSysTouchflowData.Models.ProjectModels;

namespace InSysDSDisplayer.Models
{
    public delegate void SendTargetTSScreenIDDelegate(Guid guid, object ViewObj, bool IsNEScreenRepeat);

    public class PageViewInfo
    {
        public int DisplayOrder
        {
            get;
            set;
        }

        public int PageDepthIndex
        { 
            get; 
            set; 
        }

        public PageViewInfo()
        {
            
        }

        public TouchPageInfo PageInfo
        {
            get;
            set;
        }
    }
}
