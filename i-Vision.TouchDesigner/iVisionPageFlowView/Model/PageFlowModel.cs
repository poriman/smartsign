﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Windows.Controls;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.IO;
using System.Xml.Serialization;
using System.Linq;

namespace iVisionPageFlowView.Model
{
    public class FlowPageData : ViewModelBase
    {
        #region Members, Properites 
               
        public double pageLifeTime;        
        public double PageLifeTime
        {
            get { return pageLifeTime; }
            set{pageLifeTime = value;OnPropertyChanged("PageLifeTime");}
        }

        public bool isApplyPageLifeTime;     
        public bool IsApplyPageLifeTime
        {
            get {return isApplyPageLifeTime;}
            set { isApplyPageLifeTime = value; OnPropertyChanged("IsApplyPageLifeTime"); }
        }

        public string pageName;       
        public string PageName
        {
            get { return pageName; }
            set { pageName = value; OnPropertyChanged("PageName"); }
        }

        private int pageDepthIndex;//페이지 실행 계층 인덱스
        public int PageDepthIndex
        {
            get { return pageDepthIndex; }
            set { pageDepthIndex = value; OnPropertyChanged("PageDepthIndex"); }
        }

        private string pageGuid;
        public string PageGuid
        {
            get { return pageGuid; }
            set { pageGuid = value; OnPropertyChanged("PageGuid"); }
        }

        private string pageParentGuid;
        public string PageParentGuid
        {
            get { return pageParentGuid; }
            set { pageParentGuid = value; OnPropertyChanged("PageParentGuid"); }
        }

        private FrameworkElement content;
        public FrameworkElement Content
        {
            get { return this.content; }
            set { this.content = value; }
        }

        #endregion

        public void SetValues(string pageName, double pageLifeTime, int pageDepthIndex, string pageGuid, string pageParentGuid)
        {
            this.pageName = pageName;
            this.PageLifeTime = pageLifeTime;
            this.pageDepthIndex = pageDepthIndex;
            this.PageGuid = pageGuid;
            this.pageParentGuid = pageParentGuid;
        }
      
    }    
}
