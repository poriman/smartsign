﻿using System;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows;
using System.Windows.Media.Imaging;
using System.IO;
using System.IO.Packaging;
using System.Windows.Xps.Packaging;
using System.Windows.Xps;

namespace iVisionPageFlowView.Util
{
    public static class CommonUtils
    {
        /// <summary>
        /// Exporting canvas to PNG image
        /// </summary>
        /// <param name="path"></param>
        /// <param name="surface"></param>
        public static void ExportToPng(Uri path, Canvas surface)
        {
            if (path == null) return;

            // Save current canvas transform
            Transform transform = surface.LayoutTransform;
            // reset current transform (in case it is scaled or rotated)
            surface.LayoutTransform = null;

            // Get the size of canvas
            Size size = new Size(surface.Width, surface.Height);
            // Measure and arrange the surface
            // VERY IMPORTANT
            surface.Measure(size);
            surface.Arrange(new Rect(size));

            // Create a render bitmap and push the surface to it
            RenderTargetBitmap renderBitmap =
              new RenderTargetBitmap(
                (int)size.Width,
                (int)size.Height,
                96d,
                96d,
                PixelFormats.Pbgra32);
            renderBitmap.Render(surface);

            // Create a file stream for saving image
            using (FileStream outStream = new FileStream(path.LocalPath, FileMode.Create))
            {
                // Use png encoder for our data
                PngBitmapEncoder encoder = new PngBitmapEncoder();
                // push the rendered bitmap to it
                encoder.Frames.Add(BitmapFrame.Create(renderBitmap));
                // save the data to the stream
                encoder.Save(outStream);
            }

            // Restore previously saved layout
            surface.LayoutTransform = transform;
        }

        public static Image ExportToPng(FrameworkElement surface)
        {
            /*
            Transform transform = surface.LayoutTransform;// Save current canvas transform            
            surface.LayoutTransform = null;// reset current transform (in case it is scaled or rotated)

            Size size = new Size(surface.Width, surface.Height);// Get the size of canvas                      
            surface.Measure(size);// Measure and arrange the surface, VERY IMPORTANT
            surface.Arrange(new Rect(size));

            RenderTargetBitmap renderBitmap = new RenderTargetBitmap((int)size.Width, (int)size.Height, 96d, 96d, PixelFormats.Pbgra32);// Create a render bitmap and push the surface to it
            renderBitmap.Render(surface);

            MemoryStream outStream = new MemoryStream();
            PngBitmapEncoder encoder = new PngBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(renderBitmap));// push the rendered bitmap to it            
            encoder.Save(outStream);// save the data to the stream

            BitmapImage bimg = new BitmapImage();
            bimg.BeginInit();
            bimg.StreamSource = new MemoryStream(outStream.ToArray());
            bimg.EndInit();
            Image img = new Image();
            img.Source = bimg;
            img.Stretch = Stretch.Uniform;
            //img.Width = 200;
            surface.LayoutTransform = transform;// Restore previously saved layout

            return img;
             */
 
            Transform transform = surface.LayoutTransform;// Save current canvas transform            
            surface.LayoutTransform = null;// reset current transform (in case it is scaled or rotated)

            Size imgSize = new Size(100, (100 * surface.Height) / surface.Width);
            Size size = new Size(surface.Width, surface.Height);// Get the size of canvas                      
            surface.Measure(size);// Measure and arrange the surface, VERY IMPORTANT
            surface.Arrange(new Rect(size));
          
            RenderOptions.SetBitmapScalingMode(surface, BitmapScalingMode.Fant);
            DrawingVisual dv = new DrawingVisual();
            using (DrawingContext ctx = dv.RenderOpen())
            {
                VisualBrush vb;
                if (surface != null)
                {
                    vb = new VisualBrush(surface);                   
                    vb.Stretch = Stretch.UniformToFill;
                    vb.AlignmentX = AlignmentX.Left;
                    vb.AlignmentY = AlignmentY.Top;
                    ctx.DrawRectangle(vb, null, new Rect(0, 0, imgSize.Width, imgSize.Height));
                }
            }
            RenderTargetBitmap rtb = new RenderTargetBitmap((int)imgSize.Width, (int)imgSize.Height, 96.0, 96.0, PixelFormats.Pbgra32);
            rtb.Render(dv);

            MemoryStream outStream = new MemoryStream();
            PngBitmapEncoder encoder = new PngBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(rtb));// push the rendered bitmap to it            
            encoder.Save(outStream);// save the data to the stream

            BitmapImage bimg = new BitmapImage();
            bimg.BeginInit();
            bimg.StreamSource = new MemoryStream(outStream.ToArray());
            bimg.EndInit();
            Image img = new Image();
            img.Source = bimg;
            img.Stretch = Stretch.Uniform;

            surface.LayoutTransform = transform;

            //using (FileStream outStream2 = new FileStream("new.png", FileMode.Create))
            //{
            //    // Use png encoder for our data
            //    PngBitmapEncoder encoder2 = new PngBitmapEncoder();
            //    // push the rendered bitmap to it
            //    encoder2.Frames.Add(BitmapFrame.Create(rtb));
            //    // save the data to the stream
            //    encoder2.Save(outStream2);
            //}
            
            return img;
        }


        /// <summary>
        /// Exporting canvas to XPS document
        /// </summary>
        /// <param name="path"></param>
        /// <param name="surface"></param>
        public static void Export(Uri path, Canvas surface)
        {
            if (path == null) return;

            // Save current canvas transorm
            Transform transform = surface.LayoutTransform;
            // Temporarily reset the layout transform before saving
            surface.LayoutTransform = null;

            // Get the size of the canvas
            Size size = new Size(surface.Width, surface.Height);
            // Measure and arrange elements
            surface.Measure(size);
            surface.Arrange(new Rect(size));

            // Open new package
            Package package = Package.Open(path.LocalPath, FileMode.Create);
            // Create new xps document based on the package opened
            XpsDocument doc = new XpsDocument(package);
            // Create an instance of XpsDocumentWriter for the document
            XpsDocumentWriter writer = XpsDocument.CreateXpsDocumentWriter(doc);
            // Write the canvas (as Visual) to the document
            writer.Write(surface);
            // Close document
            doc.Close();
            // Close package
            package.Close();

            // Restore previously saved layout
            surface.LayoutTransform = transform;
        }

        /// <summary>
        /// Exporting canvas to the XAML
        /// </summary>
        /// <param name="path"></param>
        /// <param name="surface"></param>
        public static void ExportToXaml(Uri path, Canvas surface)
        {
            if (path == null) return;
            if (surface == null) return;

            string xaml = System.Windows.Markup.XamlWriter.Save(surface);
            File.WriteAllText(path.LocalPath, xaml);
        }

        public static void CloneCanvas(this Canvas origin, Canvas destination)
        {
            if (destination == null)
                throw new ArgumentNullException("destination", "Destination object must first be instantiated.");            

            foreach (var destinationProperty in destination.GetType().GetProperties())
            {
                if (origin != null && destinationProperty.CanWrite)
                {
                    origin.GetType().GetProperties().Where(x => x.CanRead && (x.Name == destinationProperty.Name && x.PropertyType == destinationProperty.PropertyType)).ToList()
                        .ForEach(x => destinationProperty.SetValue(destination, x.GetValue(origin, null), null));
                }
            }

            destination.Children.Clear();// Add하기전에 제거한다.
            foreach (FrameworkElement item in origin.Children)
            {
                FrameworkElement destItem = new FrameworkElement();
                item.CloneProperties(destination);

                double left = Canvas.GetLeft(item);
                double top = Canvas.GetTop(item);
                
                Canvas.SetLeft(destItem, left);
                Canvas.SetTop(destItem, top);

                destination.Children.Add(destItem);
            }
        }

        public static void CloneProperties(this FrameworkElement origin, FrameworkElement destination)
        {
            if (destination == null)
                throw new ArgumentNullException("destination", "Destination object must first be instantiated.");

            foreach (var destinationProperty in destination.GetType().GetProperties())
            {
                if (origin != null && destinationProperty.CanWrite)
                {

                    origin.GetType().GetProperties().Where(x => x.CanRead && (x.Name == destinationProperty.Name && x.PropertyType == destinationProperty.PropertyType)).ToList()
                        .ForEach(x => destinationProperty.SetValue(destination, x.GetValue(origin, null), null));
                }
            }
        }
    }
}
