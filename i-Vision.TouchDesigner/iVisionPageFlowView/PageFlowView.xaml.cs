﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using iVisionPageFlowView.Core;
using iVisionPageFlowView.Model;
using InSysTouchflowData.Models.ProjectModels;
using System.Globalization;
using iVisionPageFlowView.Cultures;
using System.Threading;

namespace iVisionPageFlowView
{
    /// <summary>
    /// Interaction logic for PageFlowView.xaml
    /// </summary>
    public partial class PageFlowView : DesignerCanvas
    {
        public enum State
        {
            Copy,
            Cut,
            None
        }

        public delegate void PageFlowViewCommandDelegate(ICommand command);
        public PageFlowViewCommandDelegate PageFlowViewCommandHandler;
        private FlowPageItem selectedFlowPageItem;
        private double defaultStartPositionY = 50;
        private Dictionary<int, List<FlowPageItem>> _flowDesignerItems = new Dictionary<int, List<FlowPageItem>>();//Key는 layer 인덱스.

        public State CopyNCutState = State.None;

        public PageFlowView()
        {
            InitializeComponent();

            this.CommandBindings.Add(new CommandBinding(ApplicationCommands.Cut, Cut_Executed, Cut_CanExecute));
            this.CommandBindings.Add(new CommandBinding(ApplicationCommands.Copy, Copy_Executed, Copy_CanExecute));
            this.CommandBindings.Add(new CommandBinding(ApplicationCommands.Paste, Paste_Executed, Paste_CanExecute));
            this.CommandBindings.Add(new CommandBinding(ApplicationCommands.Delete, Delete_Executed, Delete_CanExecute));
            this.CommandBindings.Add(new CommandBinding(ApplicationCommands.New, New_Executed, New_CanExecute));

            this.Loaded += (s, e) =>
            {
                InitStringResource();

                ArrangePageThumbItems();
            };
            
        }

        public FlowPageItem SelectedFlowPageItem
        {
            get { return this.selectedFlowPageItem; }
            set { this.selectedFlowPageItem = value; }
        }

        #region Command

        #region Cut Command

        private void Cut_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (PageFlowViewCommandHandler != null)
            {
                Clipboard.Clear();
                Clipboard.SetData(DataFormats.Serializable, this.selectedFlowPageItem);

                CopyNCutState = State.Cut;
                PageFlowViewCommandHandler(ApplicationCommands.Cut);
            }
        }

        private void Cut_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            if (this.selectedFlowPageItem != null)
                e.CanExecute = true;
            else
                e.CanExecute = false;
        }

        #endregion

        #region Copy Command

        private void Copy_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (PageFlowViewCommandHandler != null)
            {
                Clipboard.Clear();
                Clipboard.SetData(DataFormats.Serializable, this.selectedFlowPageItem);

                CopyNCutState = State.Copy;
                PageFlowViewCommandHandler(ApplicationCommands.Copy);
            }
        }

        private void Copy_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            if (this.selectedFlowPageItem != null)
                e.CanExecute = true;
            else
                e.CanExecute = false;
        }

        #endregion

        #region Paste Command

        private void Paste_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (PageFlowViewCommandHandler != null)
            {
                PageFlowViewCommandHandler(ApplicationCommands.Paste);
            }
        }

        private void Paste_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            if (this.selectedFlowPageItem != null)
                e.CanExecute = true;
            else
                e.CanExecute = false;
        }

        #endregion

        #region Delete Command

        private void Delete_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (PageFlowViewCommandHandler != null)
            {
                PageFlowViewCommandHandler(ApplicationCommands.Delete);
            }
        }

        private void Delete_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            if (this.selectedFlowPageItem != null)
                e.CanExecute = true;
            else
                e.CanExecute = false;
        }

        #endregion

        #region New Command

        private void New_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            try
            {
                if (PageFlowViewCommandHandler != null)
                {
                    PageFlowViewCommandHandler(ApplicationCommands.New);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void New_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            if (this.selectedFlowPageItem != null)
                e.CanExecute = true;
            else
                e.CanExecute = false;
        }

        #endregion

        #endregion

        public void ClearFlowItems()
        {
            this._flowDesignerItems.Clear();
        }

        public void CloseFlowView()
        {
            ClearFlowItems();
            this.Children.Clear();
        }
       
        public void ArrangePageThumbItems()
        {
            this.DeleteAllConnectionDesignerItem(this);

            Size designerSize = new Size(190, 60);

            int layerIndex = 0; //Layer Index는 0부터 시작한다.
            int totalLayerCount = _flowDesignerItems.Count;//전체 layer를 갯수.
            double startPosX = this.ActualWidth / 2 - (designerSize.Width * totalLayerCount) / 2;
            double startPosY = 0.0;

            if (_flowDesignerItems.Count > 0)//당연히 Page가 존재하는 상태에서 진행된다.
            {
                foreach (List<FlowPageItem> dItems in this._flowDesignerItems.Values)
                    dItems.ForEach(o => Canvas.SetTop(o, double.NaN));

                FlowPageItem rootDesignerItem = this._flowDesignerItems[layerIndex].Select(o => o).SingleOrDefault();//Layer Index가 0인 Root page의 자식부터 검색한다.

                double y = MakeDesignerItem(rootDesignerItem, designerSize, startPosX, startPosY, totalLayerCount, layerIndex);

                #region Root Layer만 존재하는 경우에 대한 처리코드
                if (layerIndex == 0 && totalLayerCount == 1)
                {
                    Canvas.SetTop(rootDesignerItem, defaultStartPositionY);
                    Canvas.SetLeft(rootDesignerItem, 100);
                    return;
                }
                #endregion //Root Layer만 존재하는 경우에 대한 처리코드

                List<FlowPageItem> childrenDesignerItem = this._flowDesignerItems[layerIndex + 1].ToList();
                if (childrenDesignerItem != null && childrenDesignerItem.Count > 0)
                {
                    double yeye = childrenDesignerItem.Min(o => Canvas.GetTop(o)) + (childrenDesignerItem.Max(o => Canvas.GetTop(o)) - childrenDesignerItem.Min(o => Canvas.GetTop(o))) / 2;

                    Canvas.SetTop(rootDesignerItem, yeye);
                    Canvas.SetLeft(rootDesignerItem, this.ActualWidth - (designerSize.Width * (totalLayerCount - (layerIndex + 1))) - startPosX);                    
                }

                double minTopValue = 0.0;
                double maxTopValue = 0.0;
                double differentValue = 100 - (this.ActualWidth - (designerSize.Width * (totalLayerCount - (layerIndex + 1))) - startPosX);
                foreach (List<FlowPageItem> dItems in this._flowDesignerItems.Values)
                {
                    dItems.ForEach(o => Canvas.SetLeft(o, Canvas.GetLeft(o) + differentValue));

                    #region 그리는 flow item중에서 제일 작은 Y 좌표와 제일 큰 Y 좌표를 구한다.
                    var minValue = dItems.Min(o => Canvas.GetTop(o));
                    if (minValue < minTopValue)
                        minTopValue = minValue;

                    var maxValue = dItems.Max(o => Canvas.GetTop(o));
                    if (maxTopValue < maxValue)
                        maxTopValue = maxValue;
                    #endregion
                }

                #region Flow View의 Widht, Height를 설정한다.

                double parentWidth = (this.Parent as FrameworkElement).ActualWidth;
                if (parentWidth > designerSize.Width * totalLayerCount + 100)
                {
                    this.Width = parentWidth;
                }
                else
                {
                    this.Width = designerSize.Width * totalLayerCount + 100;
                }
                this.Height = maxTopValue - minTopValue + designerSize.Height + (defaultStartPositionY * 2);

                #endregion

                ChangedDesignerItemPostition();
            }

            ConnectionDesignerItem();
        }
                
        public double MakeDesignerItem(FlowPageItem parentDesignerItem, Size designerSize, double startPosX, double startPosY, int totalLayerCount, int layerIndex)
        {
            int currentLayerIndex = layerIndex + 1;//현재 세로 Layer.           
            if (currentLayerIndex == totalLayerCount)//현재 검색할려는 layer index가 없는 경우 처리한다.
                return startPosY;
            if (parentDesignerItem == null)
                return startPosY;

            //자식 item을 검색한다.
            List<FlowPageItem> childrenDesignerItem = this._flowDesignerItems[currentLayerIndex].Where(o => o.ParentID.ToString().Equals(parentDesignerItem.ID.ToString()) == true).ToList();
            int childrenCount = childrenDesignerItem.Count;
            if (childrenCount == 0)
                return startPosY;

            List<FlowPageItem> tempList = this._flowDesignerItems[currentLayerIndex].Where(o => o.ParentID.ToString().Equals(parentDesignerItem.ID.ToString()) != true).ToList();
            double height = 0.0;
            double tempPosY = 0.0;
            bool isLayerExist = false;
            if (tempList.Count > 0)
            {
                double max = 0;
                double min = 0;

                List<FlowPageItem> vItem = new List<FlowPageItem>();
                foreach (FlowPageItem desigItem in tempList)
                {
                    double temp = Canvas.GetTop(desigItem);
                    if (temp.Equals(double.NaN) != true)
                        vItem.Add(desigItem);
                }
                if (vItem != null && vItem.Count > 0)
                {
                    max = vItem.Max(o => Canvas.GetTop(o));
                    min = vItem.Min(o => Canvas.GetTop(o));
                }

                if (max < 0 && min < 0)
                    height = Math.Abs(min) - Math.Abs(max);
                else
                    height = max - min;

                if (height.Equals(double.NaN) == true)
                    height = 0;

                tempPosY = min;
                if (tempPosY.Equals(double.NaN) == true)
                    tempPosY = 0;

                if (tempPosY != 0 || height != 0)
                {
                    startPosY = tempPosY + height;
                    isLayerExist = true;
                }
            }

            double layerPosX = this.ActualWidth - (designerSize.Width * (totalLayerCount - (currentLayerIndex + 1)));
            if (isLayerExist != true)
                startPosY = startPosY + designerSize.Height / 2 - (designerSize.Height * childrenCount) / 2;
            else
                startPosY = startPosY + designerSize.Height;

            //double topMaxValue = childrenDesignerItem.Max(o => Canvas.GetTop(o));
            //double topMinValue = childrenDesignerItem.Min(o => Canvas.GetTop(o));
            foreach (FlowPageItem childDesignerItem in childrenDesignerItem)
            {
                double y = MakeDesignerItem(childDesignerItem, designerSize, startPosX, startPosY, totalLayerCount, currentLayerIndex);
                if (y != startPosY)
                {
                    if (y >= startPosY)
                        startPosY = y;
                }
                Canvas.SetLeft(childDesignerItem, layerPosX - startPosX);
                Canvas.SetTop(childDesignerItem, startPosY);

                startPosY += designerSize.Height;                
            }

            if (childrenCount == 0)
                return startPosY;

            double topMaxValue = childrenDesignerItem.Max(o => Canvas.GetTop(o));
            double topMinValue = childrenDesignerItem.Min(o => Canvas.GetTop(o));

            double otherExistItemGroupHeight = 0.0;//동일 Layer에 존재하는 영역을 차지하고 있는 높이
            if (topMaxValue < 0 && topMinValue < 0)// 두개 값이 음수 일경우에 대해 높이 구하기
                otherExistItemGroupHeight = Math.Abs(topMinValue) - Math.Abs(topMaxValue);
            else
                otherExistItemGroupHeight = topMaxValue - topMinValue;

            double resizingPositionY = topMinValue + otherExistItemGroupHeight / 2;

            return resizingPositionY;
        }

        private void ChangeChildrenPosition(double diff_x, double diff_y, FlowPageItem parentDesignerItem)
        {
            if (parentDesignerItem != null)
            {
                int childLayerIndex = parentDesignerItem.PageDepthIndex + 1;
                List<FlowPageItem> childrenDesignerItems = this._flowDesignerItems[childLayerIndex].Where(o => o.ParentID.ToString().Equals(parentDesignerItem.ID.ToString()) == true).ToList();
                int childrenCount = childrenDesignerItems.Count;

                foreach (FlowPageItem childItem in childrenDesignerItems)
                {
                    Canvas.SetTop(childItem, Canvas.GetTop(childItem) + diff_y);
                    Canvas.SetLeft(childItem, Canvas.GetLeft(childItem) + diff_x);
                }
            }
        }

        public void ConnectionDesignerItem()
        {
            int totalLayerCount = _flowDesignerItems.Count;//전체 layer를 갯수.
            for (int i = 0; i < totalLayerCount; i++)
            {
                if (i == totalLayerCount - 1)
                    break;
                foreach (FlowPageItem item in this._flowDesignerItems[i])
                {
                    IEnumerable<FlowPageItem> nextItems = this._flowDesignerItems[i + 1].Where(o => o.ParentID.ToString().Equals(item.ID.ToString()) == true);
                    foreach (FlowPageItem nextItem in nextItems)
                    {
                        item.UpdateLayout();
                        nextItem.UpdateLayout();
                        ConnectBetweenItems(this, item.ID, "Right", nextItem.ID, "Left");
                    }
                }
            }
        }

        private void ConnectBetweenItems(DesignerCanvas view, Guid sourceID, string sourceConnectorName, Guid sinkID, string sinkConnectorName)
        {
            Connector sourceConnector = GetConnector(sourceID, sourceConnectorName);
            Connector sinkConnector = GetConnector(sinkID, sinkConnectorName);
            ConnectBetweenItems(view, sourceConnector, sinkConnector);
        }

        private void ConnectBetweenItems(DesignerCanvas view, Connector source, Connector sink)
        {
            Connection connection = new Connection(source, sink);
            Canvas.SetZIndex(connection, view.Children.Count);
            view.Children.Add(connection);
        }

        private void DeleteAllConnectionDesignerItem(DesignerCanvas view)
        {
            var items = view.Children.OfType<Connection>().ToList();
            foreach (Connection connection in items)
            {
                this.Children.Remove(connection);
            }
        }

        private Connector GetConnector(Guid itemID, String connectorName)
        {
            FlowPageItem designerItem = (from item in this.Children.OfType<FlowPageItem>()
                                         where item.ID == itemID
                                         select item).FirstOrDefault();

            Control connectorDecorator = designerItem.Template.FindName("PART_ConnectorDecorator", designerItem) as Control;
            connectorDecorator.ApplyTemplate();

            return connectorDecorator.Template.FindName(connectorName, connectorDecorator) as Connector;
        }

        private void GetConnectors(DependencyObject parent, List<Connector> connectors)
        {
            int childrenCount = VisualTreeHelper.GetChildrenCount(parent);
            for (int i = 0; i < childrenCount; i++)
            {
                DependencyObject child = VisualTreeHelper.GetChild(parent, i);
                if (child is Connector)
                {
                    connectors.Add(child as Connector);
                }
                else
                    GetConnectors(child, connectors);
            }
        }

        public void DeleteConnector(FlowPageItem flowpageitem)
        {
            FrameworkElement frameworkElement = flowpageitem.Template.FindName("PART_ConnectorDecorator", flowpageitem) as FrameworkElement;
            List<Connector> connectors = new List<Connector>();
            GetConnectors(frameworkElement, connectors);

            foreach (Connector connector in connectors)
            {
                foreach (Connection con in connector.Connections)
                {
                    this.Children.Remove(con);
                }
            }
        }

        private void ChangedDesignerItemPostition()
        {
            //음수 값을 가진 Top 영역만큼 옮긴다.
            List<FlowPageItem> temps = new List<FlowPageItem>();
            this._flowDesignerItems.Values.ToList().ForEach(o => temps.AddRange(o));

            double minTop = temps.Min(o => Canvas.GetTop(o));

            if (minTop < 0)
            {
                minTop = Math.Abs(minTop) + defaultStartPositionY;
                temps.ToList().ForEach(o => Canvas.SetTop(o, Canvas.GetTop(o) + minTop));
            }
            else
            {
                double y = defaultStartPositionY - minTop;
                temps.ToList().ForEach(o => Canvas.SetTop(o, Canvas.GetTop(o) + y));
            }
        }

        public void InsertFlowPage(FlowPageItem flowpageitem)
        {            
            flowpageitem.Width = 140 * 0.9;
            flowpageitem.Height = 45 * 1.0;

            SetConnectorDecoratorTemplate(flowpageitem);
            //this.SelectionItem(pagedata);

            flowpageitem.SetPageName(flowpageitem.PageName);
            flowpageitem.Focus();
            flowpageitem.MouseLeftButtonDown += (s, e) =>
            {
                selectedFlowPageItem = s as FlowPageItem;
            };

            this.Children.Add(flowpageitem);

            if (_flowDesignerItems == null)
            {
                _flowDesignerItems = new Dictionary<int, List<FlowPageItem>>();
            }

            if (_flowDesignerItems.Count == 0)
            {
                flowpageitem.DisplayOrder = 1;
                List<FlowPageItem> list = new List<FlowPageItem>();
                list.Add(flowpageitem);
                _flowDesignerItems.Add(flowpageitem.PageDepthIndex, list);
            }
            else
            {
                if (_flowDesignerItems.ContainsKey(flowpageitem.PageDepthIndex) == true)
                {
                    List<FlowPageItem> list = _flowDesignerItems[flowpageitem.PageDepthIndex];
                    if (list.Contains(flowpageitem) == false)
                        list.Add(flowpageitem);
                    if (flowpageitem.DisplayOrder == 0)
                    {
                        flowpageitem.DisplayOrder = list.Count;
                    }
                }
                else
                {
                    flowpageitem.DisplayOrder = 1;
                    List<FlowPageItem> list = new List<FlowPageItem>();
                    list.Add(flowpageitem);
                    _flowDesignerItems.Add(flowpageitem.PageDepthIndex, list);
                }
            }
        }

        public FlowPageItem FindFlowPageItem(string flowpage_id, int page_depthindex)
        {
            FlowPageItem find_flowpageitem = _flowDesignerItems[page_depthindex].Where(o => o.ID.ToString().Equals(flowpage_id) == true).SingleOrDefault();

            return find_flowpageitem;
        }

        public FlowPageItem FindFlowPageItem(string flowpage_id)
        {
            foreach (var pageitems in this._flowDesignerItems.Values)
            {
                try
                {
                    FlowPageItem find_flowpageitem = pageitems.Where(o => o.ID.ToString().Equals(flowpage_id) == true).SingleOrDefault();
                    if (find_flowpageitem != null)
                    {
                        return find_flowpageitem;
                    }
                }
                catch (Exception ex)
                {
                    var list = pageitems.Where(o => o.ID.ToString().Equals(flowpage_id) == true).ToList();
                    if (list.Count() > 1)
                    {
                        for (int i = 1; i < list.Count(); i++)
                            pageitems.Remove(list[i]);
                    }

                    return list[0];                    
                }
            }

            return null;
        }

        public List<FlowPageItem> FindChildFlowPageItem(string flowpage_id, int depth_index)
        {
            var child_items = this._flowDesignerItems.Where(o => o.Key == depth_index + 1).Select(o=>o.Value).FirstOrDefault();
            if (child_items != null && child_items.Count() > 0)
            {
                return child_items.Where(o=>o.ParentID.ToString().Equals(flowpage_id) == true).ToList();
            }

            return null;
        }

        public bool FindChildFlowPageItem(List<FlowPageItem> flowpageitems, string flowpage_id)
        {
            var items = flowpageitems.Where(o => o.ParentID.ToString().Equals(flowpage_id) == true);
            if (items != null && items.Count() > 0)
                return true;

            return false;
        }

        public FlowPageItem FindRootFlowPageItem()
        {
            FlowPageItem rootFlowPageITem = _flowDesignerItems[0].FirstOrDefault();

            return rootFlowPageITem;
        }
     
        public void SelectionItem(FlowPageItem item)
        {
            try
            {
                if (item != null)
                {
                    this.SelectionService.SelectItem(item);
                    selectedFlowPageItem = item;
                }
                else
                    throw new Exception("선택된 Flow Item이 없습니다");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void ClearSelection()
        {
            _flowDesignerItems.Values.ToList().ForEach(o => o.ForEach(s => s.IsSelected = false));
        }

        public void Selection(FlowPageItem item)
        {
            item.IsSelected = true;
            item.Focus();
        }

        public void DeletePageOnFlowView(TouchPageInfo deletePageInfo)
        {
            if (_flowDesignerItems.ContainsKey(deletePageInfo.PageLayerIndex) == true)
            {
                List<FlowPageItem> list = _flowDesignerItems[deletePageInfo.PageLayerIndex];
                foreach (FlowPageItem item in list)
                {
                    TouchPageInfo pageInfo = item.PageInfoData as TouchPageInfo;
                    if (pageInfo != null && pageInfo.PageGuid.Equals(deletePageInfo.PageGuid) == true)
                    {
                        if (_flowDesignerItems[deletePageInfo.PageLayerIndex].Contains(item) == true)
                        {
                            _flowDesignerItems[deletePageInfo.PageLayerIndex].Remove(item);
                        }
                        list.Remove(item);
                        break;
                    }
                }

                if (_flowDesignerItems[deletePageInfo.PageLayerIndex].Count == 0)
                {
                    _flowDesignerItems.Remove(deletePageInfo.PageLayerIndex);
                }
            }
        }

        public void ChangeFlowPage(FlowPageItem item, string parentPageID, int pageLayerIndex)
        {
            TouchPageInfo pageInfo = item.PageInfoData as TouchPageInfo;
            if (pageInfo != null)
            {
                if (_flowDesignerItems.Keys.Contains(pageInfo.PageLayerIndex + 1) == true)
                {
                    List<FlowPageItem> childItems = _flowDesignerItems[pageInfo.PageLayerIndex + 1].Where(o => (o.PageInfoData as TouchPageInfo).ParentPageGuid.Equals(pageInfo.PageGuid) == true).ToList();
                    foreach (FlowPageItem child in childItems)
                    {
                        TouchPageInfo childPageInfo = child.PageInfoData as TouchPageInfo;
                        ChangeFlowPage(child, childPageInfo.ParentPageGuid, pageInfo.PageLayerIndex);
                    }
                }

                _flowDesignerItems[pageInfo.PageLayerIndex].Remove(item);
                if (_flowDesignerItems[pageInfo.PageLayerIndex].Count == 0)
                {
                    _flowDesignerItems.Remove(pageInfo.PageLayerIndex);
                }

                if (_flowDesignerItems.Keys.Contains(pageLayerIndex) == true)
                {                    
                    this.ChangedFlowPageItemProperty(item, parentPageID, pageLayerIndex);
                    _flowDesignerItems[pageLayerIndex].Add(item);
                }
                else
                {
                    _flowDesignerItems.Add(pageLayerIndex, new List<FlowPageItem>() { item });
                }
            }
        }

        public void ChangedFlowPageItemProperty(FlowPageItem flowpageitem, string pageParentID, int depthindex)
        {
            flowpageitem.ParentID = Guid.Parse(pageParentID);
            flowpageitem.PageDepthIndex = depthindex;
            TouchPageInfo touchpageinfo = flowpageitem.PageInfoData as TouchPageInfo;
            if (touchpageinfo != null)
            {
                touchpageinfo.ParentPageGuid = pageParentID;
                touchpageinfo.PageLayerIndex = depthindex;
            }
        }

        public bool DeleteFlowPageItem(FlowPageItem deleteDesignerItem)
        {
            if (deleteDesignerItem != null)
            {
                TouchPageInfo deletePageInfo = deleteDesignerItem.PageInfoData as TouchPageInfo;
                if (deletePageInfo.PageLayerIndex == 0)
                {
                    MessageBox.Show("최상위 Page는 삭제 할 수 없습니다.");
                    return false;
                }
                if (deletePageInfo != null)
                {
                    List<FlowPageItem> designerItems = this.Children.OfType<FlowPageItem>().ToList();
                    FlowPageItem find_item = designerItems.Where(o => (o.PageInfoData as TouchPageInfo).PageGuid.Equals(deletePageInfo.ParentPageGuid) == true).FirstOrDefault();
                    if (find_item == null)
                    {
                        MessageBox.Show("부모 Page를 찾지 못했습니다.");
                        return false;
                    }

                    #region FlowView 내 Item Dictonary 수정

                    List<FlowPageItem> find_items = designerItems
                        .Where(o => string.IsNullOrEmpty((o.PageInfoData as TouchPageInfo).ParentPageGuid) != true)
                        .Where(o => (o.PageInfoData as TouchPageInfo).ParentPageGuid.Equals(deletePageInfo.PageGuid) == true).ToList();
                    if (find_items != null)
                    {
                        if (find_items.Count >= 1)
                        {
                            foreach (FlowPageItem item in find_items)
                            {
                                this.ChangeFlowPage(item, deletePageInfo.ParentPageGuid, deletePageInfo.PageLayerIndex);
                            }
                        }
                    }
                    #endregion //FlowView 내 Item Dictonary I수정

                    //#region Selection flow view item

                    //var parentPageInfo = (TouchPageInfo)designerItems.Where(o => (o.PageInfoData as TouchPageInfo).PageGuid.Equals(deletePageInfo.ParentPageGuid) == true).Select(o => o.PageInfoData).FirstOrDefault();
                    //if (parentPageInfo != null)
                    //{
                    //    this.SelectionItem(parentPageInfo);
                    //}

                    //#endregion

                    #region Connector 제거

                    FrameworkElement frameworkElement = deleteDesignerItem.Template.FindName("PART_ConnectorDecorator", deleteDesignerItem) as FrameworkElement;
                    List<Connector> connectors = new List<Connector>();
                    GetConnectors(frameworkElement, connectors);

                    foreach (Connector connector in connectors)
                    {
                        foreach (Connection con in connector.Connections)
                        {
                            this.Children.Remove(con);
                        }
                    }

                    #endregion //Connector 제거

                    this.DeletePageOnFlowView(deletePageInfo);                    

                    this.ArrangePageThumbItems();

                    this.Children.Remove(deleteDesignerItem);

                    return true;
                }
            }

            return false;
        }

        public void selectFlowPageItemAfterDelete(TouchPageInfo touchPageInfo)
        {
            List<FlowPageItem> designerItems = this.Children.OfType<FlowPageItem>().ToList();
            if (designerItems != null && designerItems.Count > 0)
            {
                var parentTouchPageInfo = (TouchPageInfo)designerItems.Where(o => (o.PageInfoData as TouchPageInfo).PageGuid.Equals(touchPageInfo.ParentPageGuid) == true).Select(o => o.PageInfoData).FirstOrDefault();
                if (parentTouchPageInfo != null)
                {
                    FlowPageItem item = this.Children.OfType<FlowPageItem>().Where(o => o.PageInfoData.Equals(parentTouchPageInfo) == true).FirstOrDefault();
                    item.SelectionItem();
                    this.selectedFlowPageItem = item;
                }
            }
        }

        public string getParentPageIDFromFlow(string pageid)
        {
            string parentPageID = "";
            List<FlowPageItem> flowpageitems = this.Children.OfType<FlowPageItem>().ToList();
            if (flowpageitems != null && flowpageitems.Count > 0)
            {
                var touchPageInfo = (TouchPageInfo)flowpageitems.Where(o => (o.PageInfoData as TouchPageInfo).PageGuid.Equals(pageid) == true).Select(o => o.PageInfoData).FirstOrDefault();
                if (touchPageInfo != null)
                {
                    parentPageID = touchPageInfo.ParentPageGuid;
                }
            }

            return parentPageID;
        }

        #region Language 설정

        public void setCultureInfo(CultureInfo info)
        {
            if (info != null)
            {
                Cultures.Resources.Culture = info;
                CultureResources.ChangeCulture(info);
                Thread.CurrentThread.CurrentCulture = info;
            }
        }

        private void InitStringResource()
        {
            Cultures.CultureResources.ResourceProvider = (ObjectDataProvider)this.FindResource("Resources");

            CutMenuItem.Header = Cultures.Resources.menuCut;
            CopyMenuItem.Header = Cultures.Resources.menuCopy;
            DeleteMenuItem.Header = Cultures.Resources.menuDelete;
            NewMenuItem.Header = Cultures.Resources.menuNew;
            InsertMenuItem.Header = Cultures.Resources.menuInsert;
        }

        #endregion
    }
}
