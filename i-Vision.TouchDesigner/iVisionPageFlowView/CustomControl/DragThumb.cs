﻿using System;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;
using System.Windows;
using iVisionPageFlowView.Core;

namespace iVisionPageFlowView.CustomControl
{
    public class DragThumb : Thumb
    {
        public Size CanvasSize { get; set; }
        public DragThumb()
        {
            base.DragDelta += new DragDeltaEventHandler(DragThumb_DragDelta);
        }

        void DragThumb_DragDelta(object sender, DragDeltaEventArgs e)
        {
            FlowPageItem designerItem = this.DataContext as FlowPageItem;
            DesignerCanvas designer = VisualTreeHelper.GetParent(designerItem) as DesignerCanvas;
            if (designerItem != null && designer != null && designerItem.IsSelected)
            {
                double minLeft = double.MaxValue;
                double minTop = double.MaxValue;

                // we only move DesignerItems
                var designerItems = designer.SelectionService.CurrentSelection.OfType<FlowPageItem>();

                foreach (FlowPageItem item in designerItems)
                {
                    double left = Canvas.GetLeft(item);
                    double top = Canvas.GetTop(item);

                    minLeft = double.IsNaN(left) ? 0 : Math.Min(left, minLeft);
                    minTop = double.IsNaN(top) ? 0 : Math.Min(top, minTop);
                }

                double deltaHorizontal = Math.Max(-minLeft, e.HorizontalChange);
                double deltaVertical = Math.Max(-minTop, e.VerticalChange);

                Point point = new Point();
                foreach (FlowPageItem item in designerItems)
                {
                    double left = Canvas.GetLeft(item);
                    double top = Canvas.GetTop(item);

                    if (double.IsNaN(left)) left = 0;
                    if (double.IsNaN(top)) top = 0;

                    if (left + deltaHorizontal + item.Width > CanvasSize.Width)
                        left = CanvasSize.Width - deltaHorizontal - item.Width;

                    if (top + deltaVertical + item.Height > CanvasSize.Height)
                        top = CanvasSize.Height - deltaVertical - item.Height;

                    point = new Point(left + deltaHorizontal, top + deltaVertical);
                    Canvas.SetLeft(item, point.X);
                    Canvas.SetTop(item, point.Y);

                    DesignerCommonFunc.ConnectionReplace(item);  //ksw_090515

                    designer.DesignerItemUpdate(item);   //ksw_090608
                    //designerItem.PosX = left + deltaHorizontal;
                    //designerItem.PosY = top + deltaVertical;
                }
                if (designerItems.Count() == 1)
                {
                    FlowPageItem item = designerItems.FirstOrDefault() as FlowPageItem;

                    if (item.IsSelected == true)
                    {
                        item.PosX = point.X;
                        item.PosY = point.Y;
                        designer.UpdateDesignerItem(item);
                    }
                }
                designer.InvalidateMeasure();
                e.Handled = true;
            }
            /*
            FlowPageItem designerItem = this.DataContext as FlowPageItem;
            DesignerCanvas designer = VisualTreeHelper.GetParent(designerItem) as DesignerCanvas;
            if (designerItem != null && designer != null && designerItem.IsSelected)
            {
                double minLeft = double.MaxValue;
                double minTop = double.MaxValue;

                // we only move DesignerItems
                var designerItems = designer.SelectionService.CurrentSelection.OfType<FlowPageItem>();

                foreach (FlowPageItem item in designerItems)
                {
                    double left = Canvas.GetLeft(item);
                    double top = Canvas.GetTop(item);

                    minLeft = double.IsNaN(left) ? 0 : Math.Min(left, minLeft);
                    minTop = double.IsNaN(top) ? 0 : Math.Min(top, minTop);
                }

                double deltaHorizontal = Math.Max(-minLeft, e.HorizontalChange);
                double deltaVertical = Math.Max(-minTop, e.VerticalChange);

                foreach (FlowPageItem item in designerItems)
                {
                    double left = Canvas.GetLeft(item);
                    double top = Canvas.GetTop(item);

                    if (double.IsNaN(left)) left = 0;
                    if (double.IsNaN(top)) top = 0;

                    Canvas.SetLeft(item, left + deltaHorizontal);
                    Canvas.SetTop(item, top + deltaVertical);

                    designerItem.PosX = left + deltaHorizontal;
                    designerItem.PosY = top + deltaVertical;
                }

                designer.InvalidateMeasure();
                e.Handled = true;
            }
            */ 
        }
    }
}
