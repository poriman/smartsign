﻿
namespace iVisionPageFlowView.Core
{
    // Common interface for items that can be selected
    // on the DesignerCanvas; used by FlowPageItem and Connection
    public interface ISelectable
    {
        bool IsSelected { get; set; }
    }
}
