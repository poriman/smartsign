﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Xml;

namespace iVisionPageFlowView.Core
{    
    public delegate bool CheckConnectionValidationDelegate(Connector source, Connector sink);
    public delegate void UpdateDesignerItemDelegate(FlowPageItem designerItem);
    public delegate void DesignerItemUpdateDelegate(FlowPageItem designerItem);


    public partial class DesignerCanvas : Canvas
    {
        //ksw_090421
        public CheckConnectionValidationDelegate CheckConnectionValidationCallback;
        public UpdateDesignerItemDelegate UpdateDesignerItemHandler;
        public DesignerItemUpdateDelegate ItemUpdate;

        public List<Connection> Connections;
        private Point? rubberbandSelectionStartPoint = null;        

        private SelectionService selectionService;
        public SelectionService SelectionService
        {
            get
            {
                if (selectionService == null)
                    selectionService = new SelectionService(this);

                return selectionService;
            }
        }

        protected override void OnMouseDown(MouseButtonEventArgs e)
        {
            base.OnMouseDown(e);
            if (e.Source == this)//DesignerCanvas 일 경우
            {
                // in case that this click is the start of a 
                // drag operation we cache the start point
                this.rubberbandSelectionStartPoint = new Point?(e.GetPosition(this));

                // if you click directly on the canvas all 
                // selected items are 'de-selected'

                if (UpdateDesignerItemHandler != null)//Screen UI Design View에만 정의
                {
                    this.UpdateDesignerItemHandler(null);
                }

                //SelectionService.ClearSelection();
                //Focus();                
            }
            else
            {
                bool controlMatch = (Keyboard.Modifiers & ModifierKeys.Control) == ModifierKeys.Control;
                if (controlMatch == true)
                    return;
                if (e.Source.GetType().Name.Equals("Connection") == true)
                {
                    SelectionService.ClearSelection();
                    Connection con = e.Source as Connection;
                    con.SetSelection();
                }
                //SelectionService.ClearSelection();
                //Focus();
            }
        }

        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);
            /*
            // if mouse button is not pressed we have no drag operation, ...
            if (e.LeftButton != MouseButtonState.Pressed)
                this.rubberbandSelectionStartPoint = null;

            // ... but if mouse button is pressed and start
            // point value is set we do have one
            if (this.rubberbandSelectionStartPoint.HasValue)
            {
                // create rubberband adorner
                AdornerLayer adornerLayer = AdornerLayer.GetAdornerLayer(this);
                if (adornerLayer != null)
                {
                    RubberbandAdorner adorner = new RubberbandAdorner(this, rubberbandSelectionStartPoint);
                    if (adorner != null)
                    {
                        adornerLayer.Add(adorner);
                    }
                }
            }
             */
           
        }

        //protected override Size MeasureOverride(Size constraint)
        //{
        //    Size size = new Size();

        //    foreach (UIElement element in this.InternalChildren)
        //    {
        //        double left = Canvas.GetLeft(element);
        //        double top = Canvas.GetTop(element);
        //        left = double.IsNaN(left) ? 0 : left;
        //        top = double.IsNaN(top) ? 0 : top;

        //        //measure desired size for each child
        //        element.Measure(constraint);

        //        Size desiredSize = element.DesiredSize;
        //        if (!double.IsNaN(desiredSize.Width) && !double.IsNaN(desiredSize.Height))
        //        {
        //            size.Width = Math.Max(size.Width, left + desiredSize.Width);
        //            size.Height = Math.Max(size.Height, top + desiredSize.Height);
        //        }
        //    }
        //    // add margin 
        //    size.Width += 10;
        //    size.Height += 10;
        //    return size;
        //}

        public void SetConnectorDecoratorTemplate(FlowPageItem item)
        {
            if (item.ApplyTemplate() && item.Content is UIElement)
            {
                ControlTemplate template = FlowPageItem.GetConnectorDecoratorTemplate(item.Content as UIElement);
                Control decorator = item.Template.FindName("PART_ConnectorDecorator", item) as Control;
                if (decorator != null && template != null)
                    decorator.Template = template;
            }
        }

        public bool CheckConnectionValidation(Connector source, Connector sink)
        {
            return CheckConnectionValidationCallback(source, sink);
        }

        public void UpdateDesignerItem(FlowPageItem item)
        {
            if (UpdateDesignerItemHandler != null)
                UpdateDesignerItemHandler(item);
        }

        public void DesignerItemUpdate(FlowPageItem item)
        {
            if (ItemUpdate != null)
                ItemUpdate(item);
        }

        public FlowPageItem CurrentSelectedDesignerItem
        {
            get
            {
                IEnumerable<FlowPageItem> selectionitems = this.SelectionService.CurrentSelection.OfType<FlowPageItem>();

                if (this.SelectionService != null && selectionitems != null)
                {
                    FlowPageItem designerItem = selectionitems.FirstOrDefault();
                    if (designerItem != null)
                        return designerItem;     
                }

                return null;
            }
        }       

        public List<FlowPageItem> CurrentSelectedItems
        {
            get
            {
                IEnumerable<FlowPageItem> selectionitems = this.SelectionService.CurrentSelection.OfType<FlowPageItem>();

                if (this.SelectionService != null && selectionitems != null)
                {
                    if (selectionitems.Count() > 0)                    
                        return selectionitems.ToList();                    
                }

                return new List<FlowPageItem>();
            }
        }
      
        public int CurrentSelectedItemCount
        {
            get 
            {
                IEnumerable<FlowPageItem> selectionitems = this.SelectionService.CurrentSelection.OfType<FlowPageItem>();

                if (this.SelectionService != null && selectionitems != null)
                    return selectionitems.Count();

                return 0;
            }
        }
    }
}
