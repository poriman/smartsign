﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace iVisionPageFlowView.Core
{
    public class CopyItemInfoArgs : EventArgs
    {
        private Guid _oldID;
        public Guid sourceItemID
        {
            get { return _oldID; }
            set { _oldID = value; }
        }
        private Guid _newID;
        public Guid copiedItemID
        {
            get { return _newID; }
            set { _newID = value; }
        }

        private string _newName;
        public string newName
        {
            get { return _newName; }
            set { _newName = value; }
        }

        private object _designerItem;
        public object designerItem
        {
            get { return _designerItem; }
            set { _designerItem = value; }
        }

        private object _sourceDesignerItem;
        public object SourceDesignerItem
        {
            get { return _sourceDesignerItem; }
            set { _sourceDesignerItem = value; }
        }

        public CopyItemInfoArgs(Guid oldID, Guid newID, string newName)
        {
            this._oldID = oldID;
            this._newID = newID;
            this._newName = newName;
        }

        public CopyItemInfoArgs(Guid oldID, Guid newID, object designerItem)
        {
            this._oldID = oldID;
            this._newID = newID;
            this._designerItem = designerItem;
        }

        public CopyItemInfoArgs(object sourceDesignerItem, Guid newID, object designerItem)
        {
            this._newID = newID;
            this._designerItem = designerItem;
            this.SourceDesignerItem = sourceDesignerItem;
        }
    }

    public class ClipboardArgs : EventArgs
    {
        private XElement _rootXml;
        public XElement rootXml
        {
            get { return _rootXml; }
            set { _rootXml = value; }
        }

        private List<object> _designerItem;
        public List<object> designerItem
        {
            get { return _designerItem; }
            set { _designerItem = value; }
        }

        public ClipboardArgs(List<object> designerItem, XElement rootXml)
        {
            this._designerItem = designerItem;
            this._rootXml = rootXml;
        }

      
    }

}
