﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Controls.Primitives;

namespace iVisionPageFlowView.Core
{
    public class MovePathGeometry
    {
        public string MoveDirection {get;set;}
        public PathGeometry PathGeometry { get; set; }
        public int StartIndex { get; set; }
        public int EndIndex { get; set; }
        public double MinVlaue { get; set; }
        public double MaxValue { get; set; }

        public MovePathGeometry(PathGeometry pathGeometry, string moveDirection, int startIndex, int endIndex , double minValue , double maxValue)
        {
            this.PathGeometry = pathGeometry;
            this.MoveDirection = moveDirection;
            this.StartIndex = startIndex;
            this.EndIndex = endIndex;
            this.MinVlaue = minValue;
            this.MaxValue = maxValue;
        }
    }

    public class Connection : Control, ISelectable, INotifyPropertyChanged
    {
        
        private Adorner connectionAdorner;

        #region Properties

        public Guid ID { get; set; }

        //<<ksw_090513
        bool isCaptured = false;

        private List<Point> linePoints {get; set;}
        private Point startPoint { get; set; }

        public List<MovePathGeometry> MovePathGeometrys { get; set; }

        //>>

        // source connector
        private Connector source;
        public Connector Source
        {
            get
            {
                return source;
            }
            set
            {
                if (source != value)
                {
                    if (source != null)
                    {
                        source.PropertyChanged -= new PropertyChangedEventHandler(OnConnectorPositionChanged);
                        source.Connections.Remove(this);
                    }

                    source = value;

                    if (source != null)
                    {
                        source.Connections.Add(this);
                        source.PropertyChanged += new PropertyChangedEventHandler(OnConnectorPositionChanged);
                    }

                    UpdatePathGeometry();
                }
            }
        }

        // sink connector
        private Connector sink;
        public Connector Sink
        {
            get { return sink; }
            set
            {
                if (sink != value)
                {
                    if (sink != null)
                    {
                        sink.PropertyChanged -= new PropertyChangedEventHandler(OnConnectorPositionChanged);
                        sink.Connections.Remove(this);
                    }

                    sink = value;

                    if (sink != null)
                    {
                        sink.Connections.Add(this);
                        sink.PropertyChanged += new PropertyChangedEventHandler(OnConnectorPositionChanged);
                    }
                    UpdatePathGeometry();
                }
            }
        }

        // connection path geometry
        private PathGeometry pathGeometry;
        public PathGeometry PathGeometry
        {
            get { return pathGeometry; }
            set
            {
                if (pathGeometry != value)
                {
                    pathGeometry = value;
                    UpdateAnchorPosition();
                    OnPropertyChanged("PathGeometry");
                }
            }
        }

        // between source connector position and the beginning 
        // of the path geometry we leave some space for visual reasons; 
        // so the anchor position source really marks the beginning 
        // of the path geometry on the source side
        private Point anchorPositionSource;
        public Point AnchorPositionSource
        {
            get { return anchorPositionSource; }
            set
            {
                if (anchorPositionSource != value)
                {
                    anchorPositionSource = value;
                    OnPropertyChanged("AnchorPositionSource");
                }
            }
        }

        // slope of the path at the anchor position
        // needed for the rotation angle of the arrow
        private double anchorAngleSource = 0;
        public double AnchorAngleSource
        {
            get { return anchorAngleSource; }
            set
            {
                if (anchorAngleSource != value)
                {
                    anchorAngleSource = value;
                    OnPropertyChanged("AnchorAngleSource");
                }
            }
        }

        // analogue to source side
        private Point anchorPositionSink;
        public Point AnchorPositionSink
        {
            get { return anchorPositionSink; }
            set
            {
                if (anchorPositionSink != value)
                {
                    anchorPositionSink = value;
                    OnPropertyChanged("AnchorPositionSink");
                }
            }
        }
        // analogue to source side
        private double anchorAngleSink = 0;
        public double AnchorAngleSink
        {
            get { return anchorAngleSink; }
            set
            {
                if (anchorAngleSink != value)
                {
                    anchorAngleSink = value;
                    OnPropertyChanged("AnchorAngleSink");
                }
            }
        }

        private ArrowSymbol sourceArrowSymbol = ArrowSymbol.None;
        public ArrowSymbol SourceArrowSymbol
        {
            get { return sourceArrowSymbol; }
            set
            {
                if (sourceArrowSymbol != value)
                {
                    sourceArrowSymbol = value;
                    OnPropertyChanged("SourceArrowSymbol");
                }
            }
        }

        public ArrowSymbol sinkArrowSymbol = ArrowSymbol.Arrow2;
        public ArrowSymbol SinkArrowSymbol
        {
            get { return sinkArrowSymbol; }
            set
            {
                if (sinkArrowSymbol != value)
                {
                    sinkArrowSymbol = value;
                    OnPropertyChanged("SinkArrowSymbol");
                }
            }
        }

        // specifies a point at half path length
        private Point labelPosition;
        public Point LabelPosition
        {
            get { return labelPosition; }
            set
            {
                if (labelPosition != value)
                {
                    labelPosition = value;
                    OnPropertyChanged("LabelPosition");
                }
            }
        }

        // pattern of dashes and gaps that is used to outline the connection path
        private DoubleCollection strokeDashArray;
        public DoubleCollection StrokeDashArray
        {
            get
            {
                return strokeDashArray;
            }
            set
            {
                if (strokeDashArray != value)
                {
                    strokeDashArray = value;
                    OnPropertyChanged("StrokeDashArray");
                }
            }
        }
        // if connected, the ConnectionAdorner becomes visible
        private bool isSelected;
        public bool IsSelected
        {
            get { return isSelected; }
            set
            {
                if (isSelected != value)
                {
                    isSelected = IsConnectionSelected = value;
                    OnPropertyChanged("IsSelected");
                    if (isSelected)
                        ShowAdorner();
                    else
                        HideAdorner();
                }
            }
        }
        public bool IsConnectionSelected
        {
            get { return (bool)GetValue(IsConnectionSelectedProperty); }
            set { SetValue(IsConnectionSelectedProperty, value); }
        }

        public static readonly DependencyProperty IsConnectionSelectedProperty =
          DependencyProperty.Register("IsConnectionSelected",
                                       typeof(bool),
                                       typeof(Connection),
                                       new FrameworkPropertyMetadata(false));

        #endregion

        public Connection(Connector source, Connector sink)
        {
            this.ID = Guid.NewGuid();
            this.Source = source;
            this.Sink = sink;

            strokeDashArray = new DoubleCollection(new double[] { 1, 2 });

            base.Unloaded += new RoutedEventHandler(Connection_Unloaded);
            
        }
    

        //ksw_090512
        protected override void OnMouseMove(MouseEventArgs e)
        {
            base.OnMouseMove(e);
            /*
            if (isCaptured)
            {
                if (selected_Path != null)  //  마우스로 잡은 두개의 Point 대체하여 Update
                {               
                    try
                    {
                        UpdateLInePointsMovePath(e.GetPosition(this));
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }

                    UpdateMovePathGeometry();
                }
            }
            */
            e.Handled = false;
        }

        //ksw_090513  Connection Line의 이동시 Update
        private void UpdateLInePointsMovePath(Point mousePoint)
        {
        
            List<Point> tempPoint = new List<Point>();
            
            for (int i = selected_Path.StartIndex; i <= selected_Path.EndIndex; i++)
            {
                
                if (selected_Path.MoveDirection == "X")
                {
                    if (mousePoint.X > selected_Path.MinVlaue && mousePoint.X < selected_Path.MaxValue)
                    {
                        linePoints[i] = new Point(mousePoint.X, linePoints[i].Y);
                        tempPoint.Add(linePoints[i]);
                    }
                }
                if (selected_Path.MoveDirection == "Y")
                {
                    if (mousePoint.Y > selected_Path.MinVlaue && mousePoint.Y < selected_Path.MaxValue)
                    {
                        linePoints[i] = new Point(linePoints[i].X, mousePoint.Y);
                        tempPoint.Add(linePoints[i]);
                    }
                }
            }

            UpdatePathGeometryCollection();

        }

        //ksw_090519  움직일수 있는 Line 위에서 Cursor 설정
        protected override void OnMouseEnter(MouseEventArgs e)
        {
            base.OnMouseEnter(e);
            /*
            if (MovePathGeometrys != null)
            {
                foreach (MovePathGeometry item in MovePathGeometrys)
                {
                    if (item.PathGeometry.FillContains(e.GetPosition(this), 7, ToleranceType.Absolute))
                    {
                        if (item.MoveDirection == "X")
                        {
                            this.Cursor = Cursors.SizeWE;
                            break;
                        }
                        else if (item.MoveDirection == "Y")
                        {
                            this.Cursor = Cursors.SizeNS;
                            break;
                        }
                    }
                    else
                    {
                        this.Cursor = Cursors.Hand;
                    }
                }
            }
             */ 
        }

        //ksw_090512
        protected override void OnMouseUp(MouseButtonEventArgs e)
        {
            base.OnMouseUp(e);
            /*
            if (e.LeftButton == MouseButtonState.Released)
            {
                isCaptured = false;
                this.ReleaseMouseCapture();
                selected_Path = null;
                this.Cursor = Cursors.Hand;
            }
            */
            e.Handled = false;
        }

        MovePathGeometry selected_Path = null;

        protected override void OnMouseDown(System.Windows.Input.MouseButtonEventArgs e)
        {
            base.OnMouseDown(e);
            /*
            // usual selection business
            DesignerCanvas designer = VisualTreeHelper.GetParent(this) as DesignerCanvas;
            if (designer != null)
            {
                if ((Keyboard.Modifiers & (ModifierKeys.Shift | ModifierKeys.Control)) != ModifierKeys.None)
                    if (this.IsSelected)
                    {
                        designer.SelectionService.RemoveFromSelection(this);
                    }
                    else
                    {
                        designer.SelectionService.AddToSelection(this);
                    }
                else if (!this.IsSelected)
                {
                    designer.SelectionService.SelectItem(this);
                }

                Focus();
            }

            //ksw_090512
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                isCaptured = true;
                this.CaptureMouse();

                foreach (MovePathGeometry item in MovePathGeometrys)
                {
                    if (item.PathGeometry.FillContains(e.GetPosition(this),7, ToleranceType.Absolute))
                    {
                        selected_Path = item;
                        break;
                    }
                }
            }
            */
            e.Handled = false;
        }

        public void SetSelection()
        {
            this.IsSelected = true;
            DesignerCanvas designer = VisualTreeHelper.GetParent(this) as DesignerCanvas;
            designer.SelectionService.AddToSelection(this);
        }

        public void SetUnselection()
        {
            this.IsSelected = false;
            DesignerCanvas designer = VisualTreeHelper.GetParent(this) as DesignerCanvas;
            designer.SelectionService.RemoveFromSelection(this);
        }

        void OnConnectorPositionChanged(object sender, PropertyChangedEventArgs e)
        {
            // whenever the 'Position' property of the source or sink Connector 
            // changes we must update the connection path geometry
            if (e.PropertyName.Equals("Position"))
            {
                UpdatePathGeometry();
            }
        }

        private void UpdatePathGeometry()
        {

            linePoints = null;     //ksw_090512
            MovePathGeometrys = null;   //ksw_090512

            if (Source != null && Sink != null)
            {
                PathGeometry geometry = new PathGeometry();

                linePoints = PathFinder.GetConnectionLine(Source.GetInfo(), Sink.GetInfo(), true);

                if (linePoints.Count > 0)
                {
                    PathFigure figure = new PathFigure();
                    figure.StartPoint = linePoints[0];
                    startPoint = linePoints[0]; //ksw_090512

                    linePoints.Remove(linePoints[0]);
                    figure.Segments.Add(new PolyLineSegment(linePoints, true));
                    geometry.Figures.Add(figure);

                    this.PathGeometry = geometry;

                    //ksw_090512
                    UpdatePathGeometryCollection();
                    
                }
            }
        }

        //ksw_090513
        private void UpdatePathGeometryCollection()
        {
            MovePathGeometrys = new List<MovePathGeometry>();
            int i = 0;
            bool firstDelete = true;

            if (linePoints.Count != 3 && linePoints.Count != 4)  // 일직선일때 linePints.Count가 3  ㄱ형태일때 linePints.Count가 4
            {
                string moveDirection = "";
                int startIndex = 0;
                int endIndex = 0;

                while (i < linePoints.Count - 1)
                {
                    double activityValue = 0;
                    List<Point> tempPoint = new List<Point>();

                    startIndex = i;
                    tempPoint.Add(linePoints[i]);

                    if (linePoints[i].X == linePoints[i + 1].X)
                    {
                        activityValue = linePoints[i].X;
                        moveDirection = "X";
                        for (int j = i + 1; j < linePoints.Count; j++)
                        {
                            if (linePoints[i].X != linePoints[j].X)
                            {
                                endIndex = j - 1;
                                i = j - 1;
                                break;
                            }
                            
                            tempPoint.Add(linePoints[j]);
                            
                            if (j == linePoints.Count - 1)  //마지막까지 추가 햇으면...  i 값 endIndex 값 다시 설정
                            {
                                endIndex = j;
                                i = j;
                            }
                        }

                    }
                    
                    else if (linePoints[i].Y == linePoints[i + 1].Y)
                    {
                        activityValue = linePoints[i].Y;
                        moveDirection = "Y";
                        for (int j = i + 1; j < linePoints.Count; j++)
                        {
                            if (linePoints[i].Y != linePoints[j].Y)
                            {
                                endIndex = j - 1;
                                i = j -1;
                                break;
                            }
                            tempPoint.Add(linePoints[j]);

                            if (j == linePoints.Count - 1)  //마지막까지 추가 햇으면...  i 값 endIndex 값 다시 설정
                            {
                                endIndex = j;
                                i = j;
                            }
                        }
                    }

                    if (startIndex == 0 && tempPoint.Count == 3)  //첫번째 선이 3개의 Point로 이루어져있으면 첫번째것은 삭제 안함
                    {
                        firstDelete = false;
                    }

                    //if (tempPoint.Count == 3)  //일직선 위에 Point 점이 3개 일때 중간 점을 끝점과 포인트를 같이 한다.
                    //{
                    //    Point test = new Point(linePoints[endIndex].X, linePoints[endIndex].Y);
                    //    linePoints[endIndex - 1] = test;
                    //    tempPoint.RemoveAt(1);
                    //}

                    PathGeometry geometry = new PathGeometry();

                    if (tempPoint.Count > 0)
                    {
                        PathFigure figure = new PathFigure();
                        figure.StartPoint = tempPoint[0];

                        tempPoint.Remove(tempPoint[0]);
                        figure.Segments.Add(new PolyLineSegment(tempPoint, true));
                        geometry.Figures.Add(figure);
                    }

                    #region 이동 가능한 Line의 유효범위 설정
                    double min = 0;
                    double max = 0;

                    DesignerCanvas tempCanvas = Source.ParentDesignerItem.Parent as DesignerCanvas;

                    if (moveDirection == "X")
                    {
                        if (Canvas.GetLeft(Sink.ParentDesignerItem) - Canvas.GetLeft(Source.ParentDesignerItem) > 0)  // Source 왼쪽, Sink 오른쪽
                        {
                            if (activityValue < Canvas.GetLeft(Source.ParentDesignerItem))
                            {
                                min = 0;
                                max = Canvas.GetLeft(Source.ParentDesignerItem) - 20;
                            }
                            else if (activityValue < Canvas.GetLeft(Sink.ParentDesignerItem) && activityValue > (Canvas.GetLeft(Source.ParentDesignerItem) + Source.ParentDesignerItem.Width) )
                            {
                                min = Canvas.GetLeft(Source.ParentDesignerItem) + Source.ParentDesignerItem.Width + 20;
                                max = Canvas.GetLeft(Sink.ParentDesignerItem) - 20;
                            }
                            else if (activityValue > (Canvas.GetLeft(Sink.ParentDesignerItem) + Sink.ParentDesignerItem.Width) )
                            {
                                min = Canvas.GetLeft(Sink.ParentDesignerItem) + Sink.ParentDesignerItem.Width + 20;
                                max = tempCanvas.ActualWidth;
                            }
                        }
                        else //Source 오른쪽 Sink 왼쪽
                        {
                            if (activityValue < Canvas.GetLeft(Sink.ParentDesignerItem))
                            {
                                min = 0;
                                max = Canvas.GetLeft(Sink.ParentDesignerItem) - 20;
                            }
                            else if(activityValue < Canvas.GetLeft(Source.ParentDesignerItem) && activityValue > (Canvas.GetLeft(Sink.ParentDesignerItem) + Sink.ParentDesignerItem.Width) )
                            {
                                min = Canvas.GetLeft(Sink.ParentDesignerItem) + Sink.ParentDesignerItem.Width + 20;
                                max = Canvas.GetLeft(Source.ParentDesignerItem) - 20;
                            }
                            else if (activityValue > (Canvas.GetLeft(Source.ParentDesignerItem) + Source.ParentDesignerItem.Width) )
                            {
                                min = Canvas.GetLeft(Source.ParentDesignerItem) + Source.ParentDesignerItem.Width + 20;
                                max = tempCanvas.ActualWidth;
                            }
                        }
                    }

                    if (moveDirection == "Y")
                    {
                        if (Canvas.GetTop(Sink.ParentDesignerItem) - Canvas.GetTop(Source.ParentDesignerItem) > 0)  // Source 위쪽 Sink 아래쪽
                        {
                            if(activityValue < Canvas.GetTop(Source.ParentDesignerItem))
                            {
                                min = 0;
                                max = Canvas.GetTop(Source.ParentDesignerItem) - 20;
                            }
                            else if (activityValue < Canvas.GetTop(Sink.ParentDesignerItem) && activityValue > (Canvas.GetTop(Source.ParentDesignerItem) + Source.ParentDesignerItem.Height))
                            {
                                min = Canvas.GetTop(Source.ParentDesignerItem) + Source.ParentDesignerItem.Height + 20;
                                max = Canvas.GetTop(Sink.ParentDesignerItem) - 20;
                            }
                            else if (activityValue > (Canvas.GetTop(Sink.ParentDesignerItem) + Sink.ParentDesignerItem.Height) )
                            {
                                min = Canvas.GetTop(Sink.ParentDesignerItem) + Sink.ParentDesignerItem.Height + 20;
                                max = tempCanvas.ActualHeight;
                            }
                        }
                        else  // Source 아래쪽  Sink 위쪽
                        {
                            if (activityValue < Canvas.GetTop(Sink.ParentDesignerItem))
                            {
                                min = 0;
                                max = Canvas.GetTop(Sink.ParentDesignerItem) - 20;
                            }
                            else if (activityValue < Canvas.GetTop(Source.ParentDesignerItem) && activityValue > (Canvas.GetTop(Sink.ParentDesignerItem) + Sink.ParentDesignerItem.Height))
                            {
                                min = Canvas.GetTop(Sink.ParentDesignerItem) + Sink.ParentDesignerItem.Height + 20;
                                max = Canvas.GetTop(Source.ParentDesignerItem) - 20;
                            }
                            else if (activityValue > (Canvas.GetTop(Source.ParentDesignerItem) + Source.ParentDesignerItem.Height))
                            {
                                min = Canvas.GetTop(Source.ParentDesignerItem) + Source.ParentDesignerItem.Height + 20;
                                max = tempCanvas.ActualHeight;

                            }
                        }
                    }
                    #endregion
                    MovePathGeometrys.Add(new MovePathGeometry(geometry, moveDirection, startIndex , endIndex , min , max));
                }
            }
            if (MovePathGeometrys.Count != 0)
            {
                MovePathGeometrys.RemoveAt(MovePathGeometrys.Count - 1);  //마지막 선은 움직일수 없는것임

                if (firstDelete)
                {
                    MovePathGeometrys.RemoveAt(0);
                }
            }

        }

        //ksw_090513
        private void UpdateMovePathGeometry()
        {
            if (Source != null && Sink != null)
            {
                PathGeometry geometry = new PathGeometry();

                if (linePoints.Count > 0)
                {
                    PathFigure figure = new PathFigure();
                    figure.StartPoint = startPoint;
                    
                    figure.Segments.Add(new PolyLineSegment(linePoints, true));
                    geometry.Figures.Add(figure);

                    this.PathGeometry = geometry;

                }
            }
        }

        private void UpdateAnchorPosition()
        {
            Point pathStartPoint, pathTangentAtStartPoint;
            Point pathEndPoint, pathTangentAtEndPoint;
            Point pathMidPoint, pathTangentAtMidPoint;

            // the PathGeometry.GetPointAtFractionLength method gets the point and a tangent vector 
            // on PathGeometry at the specified fraction of its length
            this.PathGeometry.GetPointAtFractionLength(0, out pathStartPoint, out pathTangentAtStartPoint);
            this.PathGeometry.GetPointAtFractionLength(1, out pathEndPoint, out pathTangentAtEndPoint);
            this.PathGeometry.GetPointAtFractionLength(0.5, out pathMidPoint, out pathTangentAtMidPoint);

            // get angle from tangent vector
            this.AnchorAngleSource = Math.Atan2(-pathTangentAtStartPoint.Y, -pathTangentAtStartPoint.X) * (180 / Math.PI);
            this.AnchorAngleSink = Math.Atan2(pathTangentAtEndPoint.Y, pathTangentAtEndPoint.X) * (180 / Math.PI);

            // add some margin on source and sink side for visual reasons only
            pathStartPoint.Offset(-pathTangentAtStartPoint.X * 5, -pathTangentAtStartPoint.Y * 5);
            pathEndPoint.Offset(pathTangentAtEndPoint.X * 5, pathTangentAtEndPoint.Y * 5);

            this.AnchorPositionSource = pathStartPoint;
            this.AnchorPositionSink = pathEndPoint;
            this.LabelPosition = pathMidPoint;
        }

        private void ShowAdorner()
        {
            // the ConnectionAdorner is created once for each Connection
            if (this.connectionAdorner == null)
            {
                DesignerCanvas designer = VisualTreeHelper.GetParent(this) as DesignerCanvas;

                AdornerLayer adornerLayer = AdornerLayer.GetAdornerLayer(this);
                if (adornerLayer != null)
                {
                    this.connectionAdorner = new ConnectionAdorner(designer, this);
                    adornerLayer.Add(this.connectionAdorner);
                }
            }
            this.connectionAdorner.Visibility = Visibility.Visible;
        }

        internal void HideAdorner()
        {
            if (this.connectionAdorner != null)
                this.connectionAdorner.Visibility = Visibility.Collapsed;
        }

        void Connection_Unloaded(object sender, RoutedEventArgs e)
        {
            // do some housekeeping when Connection is unloaded

            // remove event handler
            this.Source = null;
            this.Sink = null;

            // remove adorner
            if (this.connectionAdorner != null)
            {
                DesignerCanvas designer = VisualTreeHelper.GetParent(this) as DesignerCanvas;

                AdornerLayer adornerLayer = AdornerLayer.GetAdornerLayer(this);
                if (adornerLayer != null)
                {
                    adornerLayer.Remove(this.connectionAdorner);
                    this.connectionAdorner = null;
                }
            }
        }

        #region INotifyPropertyChanged Members

        // we could use DependencyProperties as well to inform others of property changes
        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string name)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(name));
            }
        }

        #endregion
    }

    public enum ArrowSymbol
    {
        None,
        Arrow,
        Arrow2,
        Diamond,
        SourceLine
    }
}
