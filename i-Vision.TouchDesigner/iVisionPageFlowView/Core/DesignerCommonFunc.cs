﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace iVisionPageFlowView.Core
{
    using System.Windows.Controls;
    using System.Windows;
    using System.Windows.Shapes;

    public class DesignerCommonFunc
    {
        //ksw090515
        public static void ConnectionReplace(FlowPageItem item)
        {
            DesignerCanvas designerCanvas = item.Parent as DesignerCanvas;

            IEnumerable<Connection> connections = designerCanvas.Children.OfType<Connection>();

            //FlowPageItem을 Drag & Drop 나 Resize 했을때 연결된 FlowPageItem들
            List<Connection> replaceConnetions = new List<Connection>();
            
            foreach (Connection connectionItem in connections)
            {
                if (connectionItem.Source.ParentDesignerItem.Equals(item) || connectionItem.Sink.ParentDesignerItem.Equals(item))
                {
                    Connection bestConnection = DesignerCommonFunc.GetBestDistance(connectionItem.Source.ParentDesignerItem, connectionItem.Sink.ParentDesignerItem);

                    Connector sourceConnector = DesignerCommonFunc.GetConnector(connectionItem.Source.ParentDesignerItem , bestConnection.Source.Orientation.ToString());
                    Connector sinkConnector = DesignerCommonFunc.GetConnector(connectionItem.Sink.ParentDesignerItem , bestConnection.Sink.Orientation.ToString());

                    connectionItem.Source = sourceConnector;
                    connectionItem.Sink = sinkConnector;
                }
            }

        }


        //ksw_090515
        public static Connector GetConnector(FlowPageItem item, string connectorOrientation)
        {
            Control connectorDecorator = item.Template.FindName("PART_ConnectorDecorator", item) as Control;
            connectorDecorator.ApplyTemplate();

            return connectorDecorator.Template.FindName(connectorOrientation, connectorDecorator) as Connector;
        }

        //ksw_090515
        public static Connection GetBestDistance(FlowPageItem sourceItem, FlowPageItem sinkItem)
        {
            if (sourceItem != null && sinkItem != null)
            {
                IDictionary<ConnectorOrientation, Point> sourceItemConnectorPositions = new Dictionary<ConnectorOrientation, Point>();
                IDictionary<ConnectorOrientation, Point> sinkItemConnectorPositions = new Dictionary<ConnectorOrientation, Point>();

                sourceItemConnectorPositions = GetConnectorPoint(sourceItem);
                sinkItemConnectorPositions = GetConnectorPoint(sinkItem);

                ConnectorOrientation sourceOrientation = ConnectorOrientation.None;
                ConnectorOrientation sinkOrientation = ConnectorOrientation.None;

                Point sourcePosition = new Point();
                Point sinkPositioon = new Point();

                double bestDistance = 100000;

                foreach (var source in sourceItemConnectorPositions)
                {
                    foreach (var sink in sinkItemConnectorPositions)
                    {
                        double dis = Distance(source.Value, sink.Value);
                        if (bestDistance > dis)
                        {
                            bestDistance = dis;
                            sourceOrientation = source.Key;
                            sourcePosition = source.Value;

                            sinkOrientation = sink.Key;
                            sinkPositioon = sink.Value;
                        }
                    }
                }

                Connector sourceConnector = new Connector();
                Connector sinkConnector = new Connector();

                sourceConnector.Orientation = sourceOrientation;
                sourceConnector.ParentDesignerItem = sourceItem;
                sourceConnector.Position = sourcePosition;

                sinkConnector.Orientation = sinkOrientation;
                sinkConnector.ParentDesignerItem = sinkItem;
                sinkConnector.Position = sinkPositioon;


                return new Connection(sourceConnector, sinkConnector);
            }

            return null;
        }

        //ksw_090515
        public static double Distance(Point source, Point sink)
        {
            return Point.Subtract(source, sink).Length;
        }

        //ksw_090515
        public static IDictionary<ConnectorOrientation, Point> GetConnectorPoint(FlowPageItem item)
        {
            IDictionary<ConnectorOrientation, Point> returnPoint = new Dictionary<ConnectorOrientation, Point>();
            if (item != null)
            {
                returnPoint.Add(ConnectorOrientation.Top, new Point(Canvas.GetLeft(item) + (item.Width / 2), Canvas.GetTop(item)));  // Top Connector Point
                returnPoint.Add(ConnectorOrientation.Left, new Point(Canvas.GetLeft(item), Canvas.GetTop(item) + (item.Height / 2)));  // Left Connector Point
                returnPoint.Add(ConnectorOrientation.Right, new Point(Canvas.GetLeft(item) + item.Width, Canvas.GetTop(item) + (item.Height / 2)));  // Right Connector Point
                returnPoint.Add(ConnectorOrientation.Bottom, new Point(Canvas.GetLeft(item) + (item.Width / 2), Canvas.GetTop(item) + item.Height));  // Bottom Connector Point

                return returnPoint;
            }
            return null;
        }

        //ksw_090515
        public static Point GetCenterPositionOfItem(Connector connectorInfo)
        {
            Point centerPoint = new Point();
            if (connectorInfo != null)
            {
                centerPoint.X = Canvas.GetLeft(connectorInfo.ParentDesignerItem) + (connectorInfo.ParentDesignerItem.Width / 2);
                centerPoint.Y = Canvas.GetTop(connectorInfo.ParentDesignerItem) + (connectorInfo.ParentDesignerItem.Height / 2);
            }
            return centerPoint;
        }
    }
}
