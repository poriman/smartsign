﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using iVisionPageFlowView.CustomControl;
using System.Collections.Generic;
using System.Windows.Shapes;
using System.Windows.Data;
using System.Globalization;
using NLog;

namespace iVisionPageFlowView.Core
{
    public delegate void SetDesignerItemDisplayNameDelegate(string DisplayName);
    public delegate bool DisplayNameTextBlockPropertyUpdate(TextBlock textBlock);  //ksw_090428


    //These attributes identify the types of the named parts that are used for templating
    [TemplatePart(Name = "PART_DragThumb", Type = typeof(DragThumb))]
    [TemplatePart(Name = "PART_ResizeDecorator", Type = typeof(Control))]
    [TemplatePart(Name = "PART_ConnectorDecorator", Type = typeof(Control))]
    [TemplatePart(Name = "PART_ContentPresenter", Type = typeof(ContentPresenter))]
    [TemplatePart(Name = "DesignerItemName", Type = typeof(TextBlock))]
    public class FlowPageItem : ContentControl, ISelectable, IGroupable
    {
		private Logger logger = NLog.LogManager.GetCurrentClassLogger();// LogManager.GetLogger("TouchDesignerLog");
        public SetDesignerItemDisplayNameDelegate SetDesignerItemDisplayName;
        public DisplayNameTextBlockPropertyUpdate UpdateDisplayNameProperty;

        public event EventHandler<EventArgs> ScreenUITabItemSelectEvent = delegate { };

        public class DesignerItemEventArgs : EventArgs
        {
            private object _ObjectData;
            public object ObjectData
            {
                get { return _ObjectData; }
                set { _ObjectData = value; }
            }

            public DesignerItemEventArgs(object objectData)
            {
                this._ObjectData = objectData;
            }
        }

        public double ItemFontSize { get; set; }
        public string ItemFontFamily { get; set; }
        public string ItemFontStyle { get; set; }
        public string ItemForeground { get; set; }
        public string ItemFontWeight { get; set; }
        public string ItemBackground { get; set; }
        public double ItemOpacity { get; set; }
        public int DisplayOrder { get; set; }

        //ksw_090423       
        public TextBlock TextBlockDesignerName { get; set; }
        
        public double PageLifeTime
        {
            get { return (double)GetValue(PageLifeTimeProperty); }
            set { SetValue(PageLifeTimeProperty, value); }
        }
        public static readonly DependencyProperty PageLifeTimeProperty = DependencyProperty.Register("PageLifeTime", typeof(double), typeof(FlowPageItem));
                     
        public bool IsApplyPageLifeTime
        {
            get { return (bool)GetValue(IsApplyLifeTimeProperty); }
            set { SetValue(IsApplyLifeTimeProperty, value); }
        }
        public static readonly DependencyProperty IsApplyLifeTimeProperty = DependencyProperty.Register("IsApplyPageLifeTime", typeof(bool), typeof(FlowPageItem));
                
        public string PageName
        {
            get { return (string)GetValue(PageNameProperty); }
            set { SetValue(PageNameProperty, value); }
        }
        public static readonly DependencyProperty PageNameProperty = DependencyProperty.Register("PageName", typeof(string), typeof(FlowPageItem));
                
        public int PageDepthIndex
        {
            get { return (int)GetValue(PageDepthIndexProperty); }
            set { SetValue(PageDepthIndexProperty, value); }
        }
        public static readonly DependencyProperty PageDepthIndexProperty = DependencyProperty.Register("PageDepthIndex", typeof(int), typeof(FlowPageItem));
        
        #region FlowPageItem Property
                
        private double posX;
        private double posY;
        public List<Connection> TargetConnections = new List<Connection>();
        public List<Connection> SourceConnections = new List<Connection>();


        public event EventHandler<DesignerItemEventArgs> UpdateControlNameOfDesignerItem = delegate { };
        public event EventHandler<DesignerItemEventArgs> DesignerItemLoaded = delegate { };

        public LineGeometry EndLine { get; private set; }
        public List<LineGeometry> StartLines { get; private set; }

        public GeometryGroup EndLine_Group { get; private set; }
        public List<GeometryGroup> StartLines_Group { get; private set; }
    
        private object _pageInfoData;
        public object PageInfoData
        {
            get { return _pageInfoData; }
            set { _pageInfoData = value; }
        }

        //public string DisplayName { get; set; }

        public double PosX
        {
            get { return this.posX; }
            set { posX = value; }
        }

        public double PosY
        {
            get { return this.posY; }
            set { posY = value; }
        }

        public Size CanvasSize { get; set; }


        #endregion

        #region Linking logic      

        // This method establishes a link between current thumb and specified thumb.
        // Returns a line geometry with updated positions to be processed outside.
        public LineGeometry LinkTo(FlowPageItem target)
        {
            // Create new line geometry
            LineGeometry line = new LineGeometry();
            // Save as starting line for current thumb
            this.StartLines.Add(line);
            // Save as ending line for target thumb
            target.EndLine = line;
            // Ensure both tumbs the latest layout
            this.UpdateLayout();
            target.UpdateLayout();
            // Update line position
           
            //line.StartPoint = new Point(Canvas.GetLeft(this) + this.ActualWidth / 2, Canvas.GetTop(this) + this.ActualHeight / 2);
            //line.EndPoint = new Point(Canvas.GetLeft(target) + target.ActualWidth / 2, Canvas.GetTop(target) + target.ActualHeight / 2);

            line.StartPoint = new Point(Canvas.GetLeft(this) + this.ActualWidth, Canvas.GetTop(this) + this.ActualHeight / 2);
            line.EndPoint = new Point(Canvas.GetLeft(target), Canvas.GetTop(target) + target.ActualHeight / 2);
            // return line for further processing
            return line;
        }

        // This method establishes a link between current thumb and target thumb using a predefined line geometry
        // Note: this is commonly to be used for drawing links with mouse when the line object is predefined outside this class
        public bool LinkTo(FlowPageItem target, LineGeometry line)
        {
            // Save as starting line for current thumb
            this.StartLines.Add(line);
            // Save as ending line for target thumb
            target.EndLine = line;
            // Ensure both tumbs the latest layout
            this.UpdateLayout();
            target.UpdateLayout();
            // Update line position
            line.StartPoint = new Point(Canvas.GetLeft(this) + this.ActualWidth, Canvas.GetTop(this) + this.ActualHeight / 2);
            line.EndPoint = new Point(Canvas.GetLeft(target), Canvas.GetTop(target) + target.ActualHeight / 2);
            return true;
        }

        public void UpdateLinks()
        {
            double left = Canvas.GetLeft(this);
            double top = Canvas.GetTop(this);

            for (int i = 0; i < this.StartLines.Count; i++)
                this.StartLines[i].StartPoint = new Point(left + this.ActualWidth, top + this.ActualHeight / 2);//this.StartLines[i].StartPoint = new Point(left + this.ActualWidth / 2, top + this.ActualHeight / 2);

            this.EndLine.EndPoint = new Point(left, top + this.ActualHeight / 2);                
        }


        public GeometryGroup GetGeometryGroup(FlowPageItem target)
        {
            GeometryGroup geometryGroup = new GeometryGroup();

            this.UpdateLayout();
            target.UpdateLayout();

            LineGeometry centerLine = new LineGeometry();
            centerLine.StartPoint = new Point(Canvas.GetLeft(this) + this.ActualWidth, Canvas.GetTop(this) + this.ActualHeight / 2);
            centerLine.EndPoint = new Point(Canvas.GetLeft(target), Canvas.GetTop(target) + target.ActualHeight / 2);

            geometryGroup.Children.Add(centerLine);
            geometryGroup.Children.Add(new LineGeometry(new Point(centerLine.EndPoint.X - 10, centerLine.EndPoint.Y - 5), new Point(centerLine.EndPoint.X - 1, centerLine.EndPoint.Y)));
            geometryGroup.Children.Add(new LineGeometry(new Point(centerLine.EndPoint.X - 10, centerLine.EndPoint.Y + 5), new Point(centerLine.EndPoint.X - 1, centerLine.EndPoint.Y)));

            return geometryGroup;
        }

        public void GetGeometryGroup(FlowPageItem target, GeometryGroup geometryGroup)
        {
            LineGeometry centerLine = new LineGeometry();
            centerLine.StartPoint = new Point(Canvas.GetLeft(this) + this.ActualWidth, Canvas.GetTop(this) + this.ActualHeight / 2);
            centerLine.EndPoint = new Point(Canvas.GetLeft(target), Canvas.GetTop(target) + target.ActualHeight / 2);

            geometryGroup.Children.Clear();

            geometryGroup.Children.Add(centerLine);
            geometryGroup.Children.Add(new LineGeometry(new Point(centerLine.EndPoint.X - 10, centerLine.EndPoint.Y - 5), new Point(centerLine.EndPoint.X - 1, centerLine.EndPoint.Y)));
            geometryGroup.Children.Add(new LineGeometry(new Point(centerLine.EndPoint.X - 10, centerLine.EndPoint.Y + 5), new Point(centerLine.EndPoint.X - 1, centerLine.EndPoint.Y)));
        }

        public GeometryGroup LinkTo_GeometryGroup(FlowPageItem target)
        {       
            GeometryGroup gerometryGroup = GetGeometryGroup(target);

            this.StartLines_Group.Add(gerometryGroup);            
            target.EndLine_Group = gerometryGroup;
            
            this.UpdateLayout();
            target.UpdateLayout();

            return gerometryGroup;
        }

        // This method establishes a link between current thumb and target thumb using a predefined line geometry
        // Note: this is commonly to be used for drawing links with mouse when the line object is predefined outside this class
        public bool LinkTo_GeometryGroup(FlowPageItem target, GeometryGroup geometryGroup)
        {
            GetGeometryGroup(target, geometryGroup);

            this.StartLines_Group.Add(geometryGroup);
            target.EndLine_Group = geometryGroup;

            this.UpdateLayout();
            target.UpdateLayout();
            return true;
        }

        public void UpdateLinks_GeometryGroup()
        {
            double left = Canvas.GetLeft(this);
            double top = Canvas.GetTop(this);

            for (int i = 0; i < this.StartLines_Group.Count; i++)
            {
                if (this.StartLines_Group[i].Children.Count > 0)
                {
                    LineGeometry centerLine = this.StartLines_Group[i].Children[0] as LineGeometry;
                    centerLine.StartPoint = new Point(left + this.ActualWidth, top + this.ActualHeight / 2);
                    (this.StartLines_Group[i].Children[1] as LineGeometry).StartPoint = new Point(centerLine.EndPoint.X - 10, centerLine.EndPoint.Y - 5);
                    (this.StartLines_Group[i].Children[2] as LineGeometry).StartPoint = new Point(centerLine.EndPoint.X - 10, centerLine.EndPoint.Y + 5);
                }
            }
            if (this.EndLine_Group.Children.Count > 0)
            {
                LineGeometry centerLine = this.EndLine_Group.Children[0] as LineGeometry;
                centerLine.EndPoint = new Point(left, top + this.ActualHeight / 2);
                (this.EndLine_Group.Children[1] as LineGeometry).EndPoint = new Point(centerLine.EndPoint.X - 2, centerLine.EndPoint.Y);
                (this.EndLine_Group.Children[2] as LineGeometry).EndPoint = new Point(centerLine.EndPoint.X - 2, centerLine.EndPoint.Y);
            }
        }

        #endregion

        #region ID
        private Guid id;
        public Guid ID
        {
            get { return id;}
            set { id = value; }
        }
        #endregion

        #region ParentID
        public Guid ParentID
        {
            get { return (Guid)GetValue(ParentIDProperty); }
            set { SetValue(ParentIDProperty, value); }
        }
        public static readonly DependencyProperty ParentIDProperty = DependencyProperty.Register("ParentID", typeof(Guid), typeof(FlowPageItem));
        #endregion

        #region IsGroup
        public bool IsGroup
        {
            get { return (bool)GetValue(IsGroupProperty); }
            set { SetValue(IsGroupProperty, value); }
        }
        public static readonly DependencyProperty IsGroupProperty =
            DependencyProperty.Register("IsGroup", typeof(bool), typeof(FlowPageItem));
        #endregion

        #region IsSelected Property

        public bool IsSelected
        {
            get { return (bool)GetValue(IsSelectedProperty); }
            set { SetValue(IsSelectedProperty, value); }
        }
        public static readonly DependencyProperty IsSelectedProperty =
          DependencyProperty.Register("IsSelected",
                                       typeof(bool),
                                       typeof(FlowPageItem),
                                       new FrameworkPropertyMetadata(false));

        #endregion

        #region IsVisibleConnectorDecorator Property

        public bool IsVisibleConnectorDecorator
        {
            get { return (bool)GetValue(IsVisibleConnectorDecoratorProperty); }
            set { SetValue(IsVisibleConnectorDecoratorProperty, value); }
        }
        public static readonly DependencyProperty IsVisibleConnectorDecoratorProperty =
          DependencyProperty.Register("IsVisibleConnectorDecorator",
                                       typeof(bool),
                                       typeof(FlowPageItem),
                                       new FrameworkPropertyMetadata(false));

        #endregion

        #region DragThumbTemplate Property

        // can be used to replace the default template for the DragThumb
        public static readonly DependencyProperty DragThumbTemplateProperty =
            DependencyProperty.RegisterAttached("DragThumbTemplate", typeof(ControlTemplate), typeof(FlowPageItem));

        public static ControlTemplate GetDragThumbTemplate(UIElement element)
        {
            return (ControlTemplate)element.GetValue(DragThumbTemplateProperty);
        }

        public static void SetDragThumbTemplate(UIElement element, ControlTemplate value)
        {
            element.SetValue(DragThumbTemplateProperty, value);
        }

        #endregion

        #region ConnectorDecoratorTemplate Property

        // can be used to replace the default template for the ConnectorDecorator
        public static readonly DependencyProperty ConnectorDecoratorTemplateProperty =
            DependencyProperty.RegisterAttached("ConnectorDecoratorTemplate", typeof(ControlTemplate), typeof(FlowPageItem));

        public static ControlTemplate GetConnectorDecoratorTemplate(UIElement element)
        {
            return (ControlTemplate)element.GetValue(ConnectorDecoratorTemplateProperty);
        }

        public static void SetConnectorDecoratorTemplate(UIElement element, ControlTemplate value)
        {
            element.SetValue(ConnectorDecoratorTemplateProperty, value);
        }

        #endregion

        #region IsDragConnectionOver

        // while drag connection procedure is ongoing and the mouse moves over 
        // this item this value is true; if true the ConnectorDecorator is triggered
        // to be visible, see template
        public bool IsDragConnectionOver
        {
            get { return (bool)GetValue(IsDragConnectionOverProperty); }
            set { SetValue(IsDragConnectionOverProperty, value); }
        }
        public static readonly DependencyProperty IsDragConnectionOverProperty =
            DependencyProperty.Register("IsDragConnectionOver",
                                         typeof(bool),
                                         typeof(FlowPageItem),
                                         new FrameworkPropertyMetadata(false));

        #endregion

        #region Routed event
        //cjy_20090403: FlowPageItem 더블클릭에 대한 RoutedEvent 정의
        public static readonly RoutedEvent DoubleClickEvent;
        public event RoutedEventHandler DoubleClick
        {
            add { AddHandler(FlowPageItem.DoubleClickEvent, value); }
            remove { RemoveHandler(FlowPageItem.DoubleClickEvent, value); }
        }

        private static readonly RoutedEvent SelectFlowPageItemEvent;
        public event RoutedEventHandler SelectFlowPageItem
        {
            add { AddHandler(FlowPageItem.SelectFlowPageItemEvent, value); }
            remove { RemoveHandler(FlowPageItem.SelectFlowPageItemEvent, value); }
        }

        public void RaiseSelectFlowPageItemEvent(object sender)
        {
            RoutedEventArgs eventArgs = new RoutedEventArgs(FlowPageItem.SelectFlowPageItemEvent, sender);
            RaiseEvent(eventArgs);
        }

        private static readonly RoutedEvent UpdateFlowPageItemEvent;
        public event RoutedEventHandler UpdateFlowPageItem
        {
            add { AddHandler(FlowPageItem.UpdateFlowPageItemEvent, value); }
            remove { RemoveHandler(FlowPageItem.UpdateFlowPageItemEvent, value); }
        }

        public void RaiseUpdateFlowPageItemEvent(object sender)
        {
            RoutedEventArgs eventArgs = new RoutedEventArgs(FlowPageItem.UpdateFlowPageItemEvent, sender);
            RaiseEvent(eventArgs);
        }
        #endregion

        static FlowPageItem()
        {
            // set the key to reference the style for this control
            FrameworkElement.DefaultStyleKeyProperty.OverrideMetadata(
                typeof(FlowPageItem), new FrameworkPropertyMetadata(typeof(FlowPageItem)));

            //cjy
            FlowPageItem.DoubleClickEvent = EventManager.RegisterRoutedEvent("DoubleClick", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(FlowPageItem));

            FlowPageItem.SelectFlowPageItemEvent = EventManager.RegisterRoutedEvent("SelectFlowPageItem", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(FlowPageItem));

            FlowPageItem.UpdateFlowPageItemEvent = EventManager.RegisterRoutedEvent("UpdateFlowPageItem", RoutingStrategy.Bubble, typeof(RoutedEventHandler), typeof(FlowPageItem));  
        }

        public FlowPageItem(Guid id)
        {
            this.id = id;
            this.Loaded += new RoutedEventHandler(DesignerItem_Loaded);
            this.SetDesignerItemDisplayName = new SetDesignerItemDisplayNameDelegate(SetPageName);
            this.UpdateDisplayNameProperty = new DisplayNameTextBlockPropertyUpdate(UpdateDisplayNameControl); 
            IsVisibleConnectorDecorator = true;
            _pageInfoData = new object();
            this.StartLines = new List<LineGeometry>();
            this.EndLine = new LineGeometry();

            this.StartLines_Group = new List<GeometryGroup>();
            this.EndLine_Group = new GeometryGroup();
            this.DisplayOrder = 0;
        }

        public FlowPageItem(bool IsVisibleConnectorDecorator)
            : this(Guid.NewGuid())
        {
        }

        public FlowPageItem()
            : this(true)
        {
        }
       
        public void SetVisibleConnectorDecorator(bool isVisibale)
        {
            this.IsVisibleConnectorDecorator = isVisibale;
        }

        //DesignerCanvas 위에 올려진 Shape 더블 클릭시 속성 창 팝업
        protected override void OnMouseDoubleClick(MouseButtonEventArgs e)
        {
            base.OnMouseDoubleClick(e);

            DesignerCanvas designer = VisualTreeHelper.GetParent(this) as DesignerCanvas;

            if (this.GetType().ToString() != "InSysFlowDesigner.Connection")
            {
                foreach (var item in designer.Children)
                {
                    // 속성창 팝업 매핑 및 띄우기
                    if (this == item)
                    {
                        //cjy_20090403: FlowPageItem 더블클릭에 대한 RoutedEvent 호출
                        RaiseEvent(new RoutedEventArgs(FlowPageItem.DoubleClickEvent, this));

                        break;
                    }
                }
            }
        }

        public void SelectionItem()
        {
            DesignerCanvas designer = VisualTreeHelper.GetParent(this) as DesignerCanvas;

            designer.SelectionService.ClearSelection();

            this.IsSelected = true;
            designer.SelectionService.CurrentSelection.Add(this);
            Focus();           
        }

        protected override void OnPreviewMouseDown(MouseButtonEventArgs e)
        {           
            base.OnPreviewMouseDown(e);

            SelectionItem();            
           
            /*
            //ksw_090602   선택된 아이템이 한개일때 Screen 아이템이면 ScreenUI쪽 Tab 아이템 셀렉션도 바뀌게 한다.
            if (designer.SelectionService.CurrentSelection.Count == 1)
            {
                ScreenUITabItemSelectEvent(this, new EventArgs());
            }

            e.Handled = false;
             */ 
        }

        protected override void OnMouseLeftButtonDown(MouseButtonEventArgs e)
        {
            base.OnMouseLeftButtonDown(e);

            RaiseSelectFlowPageItemEvent(this);
        }

        protected override void OnMouseRightButtonDown(MouseButtonEventArgs e)
        {
            base.OnMouseRightButtonDown(e);
            RaiseSelectFlowPageItemEvent(this);
        }

        void DesignerItem_Loaded(object sender, RoutedEventArgs e)
        {
            if (base.Template != null)
            {
                ContentPresenter contentPresenter = this.Template.FindName("PART_ContentPresenter", this) as ContentPresenter;
                if (contentPresenter != null && contentPresenter.Content != null)
                {
                    try
                    {
                        UIElement contentVisual = VisualTreeHelper.GetChild(contentPresenter, 0) as UIElement;
                        if (contentVisual != null)
                        {
                            InitializeDesignerItemName();
                            DragThumb thumb = this.Template.FindName("PART_DragThumb", this) as DragThumb;

                            if (thumb != null)
                            {
                                thumb.CanvasSize = this.CanvasSize;
                                ControlTemplate template =
                                    FlowPageItem.GetDragThumbTemplate(contentVisual) as ControlTemplate;
                                if (template != null)
                                    thumb.Template = template;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.Error(string.Format("{0} >> {1}", ex.Message, ex.StackTrace));
                    }
                }

                DesignerItemLoaded(sender, new DesignerItemEventArgs(this));
            }
        }

        public bool InitializeDesignerItemName()
        {
            try
            {               
                if (TextBlockDesignerName == null)
                {
                    TextBlockDesignerName = this.Template.FindName("PART_TextBlockName", this) as TextBlock;
                    
                    TextBlockDesignerName.Text = this.DisplayText;

                    SetPageNameToolTip(this.PageName);
                }           
                
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
        }

        private void SetPageNameToolTip(string displayName)
        {
            if (this.ToolTip == null)
            {
                ToolTip tooltip = new ToolTip();
                tooltip.Content = displayName;
                this.ToolTip = tooltip;
            }
            else
            {
                ToolTip tooltip = this.ToolTip as ToolTip;
                if (tooltip != null)
                {
                    tooltip.Content = displayName;
                }
            }

            if (TextBlockDesignerName != null)
            {
                if (TextBlockDesignerName.ToolTip == null)
                {
                    ToolTip tooltip = new ToolTip();
                    tooltip.Content = displayName;
                    TextBlockDesignerName.ToolTip = tooltip;
                }
                else
                {
                    ToolTip tooltip = TextBlockDesignerName.ToolTip as ToolTip;
                    if (tooltip != null)
                    {
                        tooltip.Content = displayName;
                    }
                }
            }

        }

        public void SetPageName(string pageName)
        {
            if (string.IsNullOrEmpty(this.PageName) != true)
            {
                if (this.PageName.Equals(pageName) == true)
                    return;
            }

            this.PageName = pageName;
            if (TextBlockDesignerName != null)
            {
                TextBlockDesignerName.Text = this.DisplayText;
                SetPageNameToolTip(pageName);
            }
        }

        public string DisplayText
        {
            get
            {
                if (this.PageName.Length > 9 )
                    return this.PageName.Substring(0, 7) + "...";
                return this.PageName;
            }
        }

        /// <summary>
        /// FlowPageItem의 Name을 나타내는 TextBlock의 속성 정보를 업데이트한다.
        /// </summary>
        /// <param name="designerItem"></param>
        /// <returns></returns>
        private bool UpdateDisplayNameControl(TextBlock textBlock)
        {
            if (TextBlockDesignerName != null)
            {
                TextBlockDesignerName.FontSize = textBlock.FontSize;
                TextBlockDesignerName.FontStyle = textBlock.FontStyle;
                TextBlockDesignerName.FontWeight =textBlock.FontWeight;
                TextBlockDesignerName.Foreground = textBlock.Foreground;
                TextBlockDesignerName.FontFamily = textBlock.FontFamily;
                TextBlockDesignerName.TextAlignment = textBlock.TextAlignment;
                TextBlockDesignerName.TextWrapping = textBlock.TextWrapping;

                TextBlockDesignerName.Visibility = Visibility.Visible;

                if (string.IsNullOrEmpty(textBlock.Text) == true)
                {
                    //MessageBox.Show("UpdateDisplayNameControl 함수");
                }

                SetPageName(textBlock.Text);
            }

            
            return true;
        }

        public void SetCanvasSizeForDragThum(double width, double height)
        {
            CanvasSize = new Size(width, height);
        }

        public void SetValues(string pageName, double pageLifeTime, int pageDepthIndex, Guid pageGuid, Guid pageParentGuid)
        {
            this.PageName = pageName;
            this.PageLifeTime = pageLifeTime;
            this.PageDepthIndex = pageDepthIndex;
            this.ID = pageGuid;
            this.ParentID = pageParentGuid;
        }
    }

    public class TitleValueConverter : IValueConverter
    {
        #region IValueConverter Members
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                string title = (string)value;
                int strCount = System.Text.Encoding.Default.GetByteCount(title);
                int strLength = title.Length;
                if (string.IsNullOrEmpty(title) == false)
                {
                    if (strCount > 12)
                        title = string.Format("{0}...", title.Substring(0, 5));
                }                        

                return title;
            }

            return string.Empty;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion
    }
}
