﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using WPFDesigner;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows;

namespace DigitalSignage.PlayControls
{
    public class TouchflowManager : Canvas
    {
        public static Screen LoadScreen(string path)
        {
            Screen screen;
            string projectPath = Path.GetDirectoryName(path) + @"\";
            string mediaPath = projectPath + @"media_files\";

            FileStream fileStream = new FileStream(path, FileMode.Open, FileAccess.Read);
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            List<Dictionary<string, object>> propertiesSet = new List<Dictionary<string, object>>();

            try
            {
                propertiesSet = binaryFormatter.Deserialize(fileStream) as List<Dictionary<string, object>>;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            screen = new Screen(Convert.ToInt32(propertiesSet[0]["Width"]), Convert.ToInt32(propertiesSet[0]["Height"]), false);
            
            return screen;
        }
    }
}
