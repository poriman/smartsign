﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Xml;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Animation;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Runtime.Serialization.Formatters.Binary;
using System.Linq;

using System.Threading;
using NLog;

using DigitalSignage.Common;
using System.Xml.Linq;
using System.Collections;

namespace DigitalSignage.PlayControls
{
    /// <summary>
    /// Interaction logic for PlaylistPlayer - control for playing IPL files
    /// </summary>
    public partial class PlaylistPlayer : Grid
    {
        #region Touch

        public bool IsLinkPlaylist = true;
        public string CurrentPaneScreenID;
        public ChangedNonTouchScreenDelegate ChangeTSViewOfPaneScreen;
        public string PlaylistFilePath { get; set; }
        private bool IsMoveScreenTSA = false;
        private bool IsScreenRepeatPlayOfTSA = false;
        private XmlNode moveScreenXmlNodeTSA = null;

        public static LoadTouchProjectDelegate LoadTouchProjectHandler;
        public static PlayTouchProjectDelegate PlayTouchProjectHandler;
        public static CloseTouchProjectDelegate CloseTouchProjectHandler;
        public static CloseTouchPlaylistPlayerDelegate CloseTouchPlaylistPlayerHandler;
        public static bool? IsCurrentPlayingTouchSchedule { get; set; }
        public bool? IsPreviousPlayingTouchSchedule { get; set; }
        private PlaylistPlayer previousPlaylistPlayer;
        public static bool IsPausePlayItem = false;//Playlisit 중 PlayItem이 Touch Project 일 경우 반복스케줄에 의해 스케줄링되지 않도록 한다. 
        #endregion

		/// <summary>
		/// 재생 목록 상태 정보
		/// </summary>
		PlaylistStatus _status = PlaylistStatus.Closed;

		/// <summary>
		/// 재생 목록 상태 정보
		/// </summary>
		public PlaylistStatus Status
		{
			get { return _status; }
			set
			{
				_status = value;
			}
		}

		private string NameOfFile = String.Empty;

		/// <summary>
		/// 상태 변경 이벤트
		/// </summary>
		public event PlaylistLoadCompletedHandler PlaylistLoadCompleted = null;

		private object semaphore = null;

        XmlDocument doc = null;

        private bool process = false;
        private Thread thread;
        private String docStart = "";
        private static Logger logger = LogManager.GetCurrentClassLogger();

		private WPFDesigner.IDesignElement primaryPane = null;
		private WPFDesigner.IDesignElement backPane = null;

		bool fade_in = true;
		bool fade_out = true;

		bool show_cursor = false;

		public bool IsFadeIn
		{
			set { fade_in = value; }
		}
		public bool IsFadeOut
		{
			set { fade_out = value; }
		}

        public PlaylistPlayer()
        {
            InitializeComponent();

		}

		public void Load(string name, bool showCursor)
		{
			show_cursor = showCursor;

			Load(name);
		}

        public void Load(string name)
        {
            doc = new XmlDocument();
            doc.Load(name);
            docStart = System.IO.Path.GetDirectoryName(name);
        }

        public void Start(TimeSpan ts)
        {
            if (doc == null) return;

            process = true;

            #region 상태 변경
            this.Status = PlaylistStatus.Loading;
            #endregion

            //Starting the UDP Server thread.
            thread = new Thread(new ParameterizedThreadStart(Process));
            thread.Name = "IPLPlayer thread";
            thread.Priority = ThreadPriority.Highest;	//	건들지말자.. 건들면 스크린 해제가 안되는 기이한 현상이 나타난다.
            thread.Start(ts);
        }

        public void Start()
        {
            Start(TimeSpan.FromSeconds(0));
        }


		delegate void PauseCB();
		public void Pause()
		{

			if (!this.Dispatcher.CheckAccess())
			{
				try
				{
					DispatcherOperation disop = this.Dispatcher.BeginInvoke(DispatcherPriority.Send,	//	건들지말자.. 건들면 스크린 해제가 안되는 기이한 현상이 나타난다.
						new PauseCB(Pause));
				}
				catch (Exception e)
				{
					logger.Error(e + "");
				}
				catch
				{
					logger.Error("Unhandled Error Exception occurred!");
				}
			}
			else
			{
				try
				{
					#region 상태 변경
					this.Status = PlaylistStatus.Paused;
					#endregion

					try
					{
						backPane.Pause();
					}
					catch { }

					try
					{
						primaryPane.Pause();
					}
					catch { }
				}
				catch { }
			}

		}

        public void Stop()
        {
            process = false;

			#region 상태 변경
			this.Status = PlaylistStatus.Closed;
			#endregion

			//	hsshin 락을 거는이유는 스크린 로드중 스레드를종료시킬수도 있기때문이다.
            if (semaphore == null)
            {
                logger.Debug("Playlist Stop Start");
                if (thread != null)
                {
                    thread.Abort();
                }
                logger.Debug("Playlist Stop End");

                return;
            }

			logger.Debug("Playlist Stop Start");
			try
			{
				stopProcess();
			}
			catch (System.Exception ex)
			{
				logger.Error("Playlist Stop Error: " + ex.Message);
			}

			if (thread != null)
			{
				thread.Abort();
			}

			logger.Debug("Playlist Stop End");

        }

        private void Process(object objTS)
        {
			logger.Debug("Playlist Thread Process Start");
			semaphore = new object();

			try
			{
                TimeSpan ts = (TimeSpan)objTS;

				while (process)
					rProcess(doc.DocumentElement, ref ts);
			}
			catch (ThreadAbortException threadAbort)
			{
				//logger.Error(threadAbort.Message);
			}
			catch (Exception err)
			{
				logger.Error(err.Message);
			}
			catch
			{
				logger.Error("Unhandled Error Exception occurred!");
			}
// 			finally
// 			{
// 				stopProcess();
// 			}

			#region 상태 변경
			this.Status = PlaylistStatus.Closed;
			#endregion

			logger.Debug("Playlist Thread Process End");
        }

        int nCallCount = 0;
        private bool rProcess(XmlNode node, ref TimeSpan tsJump)
        {
			try
			{
				if (node.Name.Equals("screen"))
				{
					string strTime = node.Attributes["time"].Value;
					string strID = node.Attributes["ID"].Value;
					TimeSpan ts = new TimeSpan(0);
					string project = null;
					if (strTime != null && strTime.Length > 0)
						ts = TimeConverter.StrToTime(strTime);

                    if (tsJump < ts)
                    {
                        ts -= tsJump;

                        if (strID != null && strID.Length > 0)
                        {
                            project = strID;

                            #region Touch

                            CurrentPaneScreenID = strID;
                            #endregion
                        }

                        if ((ts.Ticks > 0) && (project != null))
                        {
                            if (nCallCount++ > 0) ScreenStartInfoInSchedule.GetInstance.ScreenStartDT = DateTime.Now;

                            loadProject(project, tsJump);

                            DateTime dtNow = DateTime.Now;
                            while (DateTime.Now - dtNow < ts /*- TimeSpan.FromMilliseconds(1000)*/)
                            {
                                if (process == false)
                                {
                                    IsScreenRepeatPlayOfTSA = false;
                                    return true;
                                }

                                if (this.Status == PlaylistStatus.Paused)
                                {
                                    dtNow = DateTime.Now;
                                }

                                #region Touch : 클릭 이벤트 발생시 화면 변경
                                if (IsMoveScreenTSA == true)
                                {
                                    dtNow = DateTime.Now;

                                    IsMoveScreenTSA = false;
                                    strTime = this.moveScreenXmlNodeTSA.Attributes["time"].Value;
                                    strID = this.moveScreenXmlNodeTSA.Attributes["ID"].Value;

                                    Console.WriteLine(strID + ", " + strTime);

                                    ts = new TimeSpan(0);
                                    if (strTime != null && strTime.Length > 0)
                                        ts = TimeConverter.StrToTime(strTime);
                                    if (strID != null && strID.Length > 0)
                                    {
                                        project = strID;
                                        CurrentPaneScreenID = strID;//Touch : TSA 추가
                                    }

                                    if ((ts.Ticks > 0) && (project != null))
                                    {
                                        if (nCallCount++ > 0) ScreenStartInfoInSchedule.GetInstance.ScreenStartDT = DateTime.Now;

                                        loadProject(project, tsJump);
                                    }

                                    continue;
                                }

                                #endregion

                                if (IsMoveScreenTSA != true && IsScreenRepeatPlayOfTSA == true)
                                {
                                    dtNow = DateTime.Now;
                                }
                                Thread.Sleep(10);
                            }
                            /// hsshin 다음 액션까지 스크린 멈춤

                            Console.WriteLine("== 멈춤 전 시간 차: {0}", DateTime.Now - dtNow);
                            pauseScreen();
                            Console.WriteLine("== 멈춤 후 시간 차: {0}", DateTime.Now - dtNow);


                            IsScreenRepeatPlayOfTSA = false;

                            #region Touch - Touch Program이 종료시 IsPausePlayItem이 false로 Set된다. PlayItem이 Touch Project 일 경우 자동 종료 전까지 계속 유지
                            //while (IsPausePlayItem)
                            //{
                            //    Thread.Sleep(100);
                            //}
                            #endregion
                        }

                    }
                    else
                    {
                        tsJump -= ts;
                    }
				}
				else
				{
// 					if (node.Name.Equals("group"))
// 					{
// 						string strrepeat = node.Attributes["repeat"].Value;
// 						if (strrepeat != null && strrepeat.Length > 0)
// 						{
// 							repeat = Int32.Parse(strrepeat);
// 							// !!! go down to default block
// 						}
// 					}
					foreach (XmlNode sub in node.ChildNodes)
                        if (rProcess(sub, ref tsJump))
							return true;
				}
			}
			catch (ThreadAbortException )
			{
				#region 상태 변경
				this.Status = PlaylistStatus.Closed;
				#endregion
			}
			catch (Exception ex)
			{
				logger.Error(ex + "");
				#region 상태 변경
				this.Status = PlaylistStatus.Closed;
				#endregion
			}
			catch 
			{
				logger.Error("Unhandled Error Excetion occurred!");
				#region 상태 변경
				this.Status = PlaylistStatus.Closed;
				#endregion
			}
            return false;
		}

		#region Logging routines	

		private iVisionLogHelper iVisionLog = iVisionLogHelper.GetInstance;
		
		#endregion

		#region Thread routines

        delegate void pauseScreenCB();
        private void pauseScreen()
        {
            if (!this.Dispatcher.CheckAccess())
            {
                try
                {
                    lock (semaphore)
                    {
                        DispatcherOperation disop = this.Dispatcher.BeginInvoke(DispatcherPriority.Send,
                           new pauseScreenCB(pauseScreen));

                        if (disop.Wait(TimeSpan.FromSeconds(10)) != DispatcherOperationStatus.Completed)
                        {
                            logger.Error("Rendering UI is pending.");

                            try
                            {
                                disop.Abort();
                            }
                            catch { }

                            if (PlaylistLoadCompleted != null) PlaylistLoadCompleted(this, new PlaylistLoadCompletedArgs(PlaylistStatus.Pending));
                        }
                    }

                }
                catch (Exception e)
                {
                    logger.Error(e + "");
                }
                catch
                {
                    logger.Error("Unhandled Error Exception occurred!");
                }
            }
            else
            {
                try
                {
                    backPane.Pause();
                }
                catch { }
            }
        }

		delegate void stopProcessCB();
		private void stopProcess()
		{
			if (!this.Dispatcher.CheckAccess())
			{
				try
				{
					logger.Debug("stopProcess semaphore before.");
					lock (semaphore)
					{
						logger.Debug("stopProcess semaphore after.");

						DispatcherOperation disop = this.Dispatcher.BeginInvoke(DispatcherPriority.Send,	//	건들지말자.. 건들면 스크린 해제가 안되는 기이한 현상이 나타난다.
						   new stopProcessCB(stopProcess));

						if (disop.Wait(TimeSpan.FromSeconds(10)) != DispatcherOperationStatus.Completed)
						{
							logger.Error("Rendering UI is pending.");

							try
							{
								disop.Abort();
							}
							catch { }

							if (PlaylistLoadCompleted != null) PlaylistLoadCompleted(this, new PlaylistLoadCompletedArgs(PlaylistStatus.Pending));
						}
					}

				}
				catch (Exception e)
				{
					logger.Error(e + "");
				}
				catch
				{
					logger.Error("Unhandled Error Exception occurred!");
				}
			}
			else
			{
				try
				{
					bool logProcced = false;

					if (primaryPane != null)
					{
						primaryPane.Stop();
						primaryPane = null;
					}
					//	hsshin 정지 시 Screen도 정지시켜주자.
					if (primaryBuffer != null)
					{
                        var children = primaryBuffer.Children.OfType<WPFDesigner.IDesignElement>();
                        foreach (WPFDesigner.IDesignElement screen in children)
						{
							logger.Debug("Playlist screen Stop");
							screen.Stop();

							((WPFDesigner.Screen)screen).Close();
						}
						primaryBuffer.Children.Clear();
					}
					if (backBuffer != null)
					{
                        var children = backBuffer.Children.OfType<WPFDesigner.IDesignElement>();
                        foreach (WPFDesigner.IDesignElement screen in children)
						{
							logger.Debug("backBuffer screen Stop");
							screen.Stop();
							
							try
							{
								if (!logProcced && iVisionLog.ScreenFinishied != null)
								{
									iVisionLog.ScreenFinishied(NameOfFile, TimeConverter.ConvertToUTP(screen.PlayStarted), TimeConverter.ConvertToUTP(DateTime.Now));
									logProcced = true;
								}
							}
							catch { }

							((WPFDesigner.Screen)screen).Close();
						}
						backBuffer.Children.Clear();
					}

					if (backPane != null)
					{
						logger.Debug("backBuffer screen Stop");

						backPane.Stop();

						try
						{
							if (!logProcced && iVisionLog.ScreenFinishied != null)
							{
								iVisionLog.ScreenFinishied(NameOfFile, TimeConverter.ConvertToUTP(backPane.PlayStarted), TimeConverter.ConvertToUTP(DateTime.Now));
								logProcced = true;
							}
						}
						catch { }

						((WPFDesigner.Screen)backPane).Close();
						backPane = null;
					}

                    primaryBuffer.Children.Clear();
                    backBuffer.Children.Clear();

					//hsshin
					if (!logProcced && iVisionLog.ScreenLogEnded != null)
					{
						iVisionLog.ScreenLogEnded(NameOfFile);
					}
				}
				catch (Exception ee)
				{
					logger.Debug(ee.ToString());
				}
				catch
				{
					logger.Error("Unhandled Error Exception occurred!");
				}

			}
		}

		delegate void loadProjectCB(string filename, TimeSpan ts);
        public void loadProject(string filename, TimeSpan ts)
        {
			if (!this.Dispatcher.CheckAccess())
			{
				try
				{
					logger.Debug("loadProject semaphore before.");
					lock (semaphore)
					{
						if (process == false)
							return;

						logger.Debug("loadProject semaphore after.");
						DispatcherOperation o = this.Dispatcher.BeginInvoke(DispatcherPriority.Send,	//	건들지말자.. 건들면 스크린 해제가 안되는 기이한 현상이 나타난다.
						   new loadProjectCB(loadProject), filename, ts);

						DispatcherOperationStatus state = o.Wait(TimeSpan.FromSeconds(60));

						if (state == DispatcherOperationStatus.Pending || 
							state == DispatcherOperationStatus.Aborted)
						{
							#region 상태 변경
							this.Status = PlaylistStatus.Pending;
							if (PlaylistLoadCompleted != null) PlaylistLoadCompleted(this, new PlaylistLoadCompletedArgs(PlaylistStatus.Pending));
							#endregion
						}
						else
						{
                            DispatcherOperation disopChanged = Dispatcher.BeginInvoke(DispatcherPriority.Send, new Action(delegate
                            {
                                try
                                {
                                    if (fade_in) ((Storyboard)backBuffer.Resources["Up"]).Begin(backBuffer);
                                    else backBuffer.Opacity = 1.0;

                                    if (fade_out) ((Storyboard)primaryBuffer.Resources["Down"]).Begin(primaryBuffer);
                                    else
                                    {
                                        primaryBuffer.Opacity = 0.0;

                                        if (primaryBuffer != null)
                                        {
                                            foreach (WPFDesigner.IDesignElement tscreen in primaryBuffer.Children)
                                            {
                                                tscreen.Stop();
                                                ((WPFDesigner.Screen)tscreen).Close();
                                            }
                                            primaryBuffer.Children.Clear();
                                        }
                                    }

                                    // swap variables
                                    Grid swap = primaryBuffer;
                                    primaryBuffer = backBuffer;
                                    backBuffer = swap;
                                }
                                catch { }
                            }
                            ));

                            disopChanged.Wait();

							#region 상태 변경
    						this.Status = PlaylistStatus.Playing;
							if (PlaylistLoadCompleted != null) PlaylistLoadCompleted(this, new PlaylistLoadCompletedArgs(PlaylistStatus.Playing));
							#endregion
						}
					}
				}
				catch (Exception e)
				{
					logger.Error(e + "");
				}
				catch
				{
					logger.Error("Unhandled Error Exception occurred!");
				}
			}
			else
			{
				if (process == false)
					return;

				if (this.Status == PlaylistStatus.Paused) return;

				logger.Debug("Playlist Thread LoadProject Start");

				try
				{
					if (!filename.Equals(""))
					{
						logger.Info("Loading project " + filename);
                        #region Touch
                        string temp_filename = filename;
                        if ((filename.Length < 2) || (filename[1] != ':'))
                            // relative path
                            temp_filename = docStart + "\\" + filename;
                        FileInfo fi = new FileInfo(temp_filename);
                        #endregion

                        //ProgramPlayer.ChangePlayStateHandler(false, this);

                        if (fi.Extension.Equals(".tpf") != true)
                        {
                            if (backBuffer != null)
                            {
                                if (PlaylistPlayer.IsCurrentPlayingTouchSchedule == false)//이전 Play 대상이 Touch(tpf)가 아니라면 기존 Screen에 대한 처리 수행
                                {
                                    this.IsPreviousPlayingTouchSchedule = false;
                                    foreach (WPFDesigner.IDesignElement tscreen in backBuffer.Children)
                                    {
                                        tscreen.Stop();
                                        ((WPFDesigner.Screen)tscreen).Close();
                                    }                                   
                                }
                                else if (PlaylistPlayer.IsCurrentPlayingTouchSchedule == true)
                                {
                                    this.IsPreviousPlayingTouchSchedule = true;
                                    ProgramPlayer.ChangePlayStateHandler(false, this);
                                }                                
                               
                                backBuffer.Children.Clear();
                            }

                            //	hsshin
                            WPFDesigner.IDesignElement temp = backPane;

							NameOfFile = filename;

                            if ((filename.Length < 2) || (filename[1] != ':'))
                                // relative path
                                filename = docStart + "\\" + filename;

                            logger.Info("Loading screen " + filename);

							//	프로그램 Stop 호출 시 함수 빠져 나감. 
							if (process == false) return;
                            
                            WPFDesigner.Screen screen = WPFDesigner.ProjectManager.LoadScreen(filename);
                            backBuffer.Children.Add(screen);
                            screen.FillParent();

                            backPane = (WPFDesigner.IDesignElement)screen;
                            backPane.Play(ts);

                            if (temp != null)
                            {
                                try
                                {
                                    temp.Stop();
                                    ((WPFDesigner.Screen)temp).Close();
                                    temp = null;
                                }
                                catch (Exception ex)
                                {
                                    temp = null;
                                    logger.Error(ex.Message);
                                }
                            }

                            //hsshin 프로모션 체크를 위해
							if (iVisionLog.ScreenLogStarted != null)
							{
								iVisionLog.ScreenLogStarted(NameOfFile);
							}

                            #region Touch
                            //ChangeTSViewOfPaneScreen(this.CurrentPaneScreenID, this);                           
                            #endregion

                            //if (!show_cursor) System.Windows.Forms.Cursor.Hide();//조준영 테스트

                            PlaylistPlayer.IsCurrentPlayingTouchSchedule = false;
                        }
                        else
                        {                            
                            #region  Touch - Touch Designer로 부터 생성된 TPF 파일일 경우

                            #region Back Buffer Clear

                            if (backBuffer != null)
                            {
                                if (PlaylistPlayer.IsCurrentPlayingTouchSchedule == false)
                                {
                                    this.IsPreviousPlayingTouchSchedule = false;
                                    var designeElements = backBuffer.Children.OfType<WPFDesigner.IDesignElement>();
                                    foreach (WPFDesigner.IDesignElement tscreen in designeElements)
                                    {
                                        tscreen.Stop();
                                        ((WPFDesigner.Screen)tscreen).Close();
                                    }
                                }
                                else if (PlaylistPlayer.IsCurrentPlayingTouchSchedule == true)
                                {
                                    this.IsPreviousPlayingTouchSchedule = true;
                                    ProgramPlayer.ChangePlayStateHandler(false, this);
                                }
                                backBuffer.Children.Clear();
                            }
                            #endregion

                            //	hsshin
                            WPFDesigner.IDesignElement temp = backPane;

                            string NameOfFile = filename;

                            if ((filename.Length < 2) || (filename[1] != ':'))
                                // relative path
                                filename = docStart + "\\" + filename;

                            logger.Info("Loading screen " + filename);

                            WPFDesigner.Screen screen = null;
                            if (LoadTouchProjectHandler != null)
                            {
                                Grid uielement = LoadTouchProjectHandler(filename, this, backBuffer) as Grid;

                                #region 껍데기 Screen  객체를 생성한다.
                                screen = new WPFDesigner.Screen((int)uielement.Width, (int)uielement.Height, false);
                                backPane = (WPFDesigner.IDesignElement)screen;
                                #endregion

                                backBuffer.Children.Add(uielement);
                            }

                            if (screen == null)
                                backPane = new WPFDesigner.Screen(100, 100, false);

                            //if (PlayTouchProjectHandler != null)
                            //    PlayTouchProjectHandler();

                            if (temp != null)
                            {
                                try
                                {
                                    temp.Stop();
                                    ((WPFDesigner.Screen)temp).Close();
                                    temp = null;
                                }
                                catch (Exception ex)
                                {
                                    temp = null;
                                    logger.Error(ex.Message);
                                }
                            }

                            //hsshin 프로모션 체크를 위해
							if (iVisionLog.ScreenLogStarted != null)
							{
								((WPFDesigner.Screen)backPane).DataContext = NameOfFile;
								iVisionLog.ScreenLogStarted(NameOfFile);
							}


                            if (!show_cursor) System.Windows.Forms.Cursor.Hide();

                            PlaylistPlayer.IsCurrentPlayingTouchSchedule = true;
                            System.Windows.Forms.Cursor.Show();//마우스 
                            
                            #endregion
                        }

						return;
					}
				}
				catch (ThreadAbortException te)
				{
					logger.Error(te.Message);
				}
				catch (iVisionException ive)
				{
					iVisionLog.ScreenErrorLog(filename, ive.ErrorCode, ive.DetailMessage);
					this.Status = PlaylistStatus.Failed;
					if (PlaylistLoadCompleted != null) PlaylistLoadCompleted(this, new PlaylistLoadCompletedArgs(PlaylistStatus.Failed));
				}
				catch (FileNotFoundException fnfe)
				{
					iVisionLog.ScreenErrorLog(filename, ErrorCodes.IS_ERROR_INVALID_PROGRAM_ID, fnfe.Message);
					this.Status = PlaylistStatus.Failed;
					if (PlaylistLoadCompleted != null) PlaylistLoadCompleted(this, new PlaylistLoadCompletedArgs(PlaylistStatus.Failed));
				}
				catch (Exception e)
				{
					iVisionLog.ScreenErrorLog(filename, ErrorCodes.IS_ERROR_INTERNAL, e.Message);
                    iVisionLog.ScreenErrorLog(filename, ErrorCodes.IS_ERROR_INTERNAL, e.StackTrace);
					this.Status = PlaylistStatus.Failed;
					if (PlaylistLoadCompleted != null) PlaylistLoadCompleted(this, new PlaylistLoadCompletedArgs(PlaylistStatus.Failed));
				}
				catch
				{
					iVisionLog.ScreenErrorLog(filename, ErrorCodes.IS_ERROR_INTERNAL, "Unhandled Error Exception occurred!");
					this.Status = PlaylistStatus.Failed;
					if (PlaylistLoadCompleted != null) PlaylistLoadCompleted(this, new PlaylistLoadCompletedArgs(PlaylistStatus.Failed));
				}
				finally
				{
					logger.Debug("Playlist Thread LoadProject End");
				}

				_status = PlaylistStatus.Closed;

			}
        }

        #endregion

        #region Event handlers
        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
//             primaryBuffer = grid;
//             backBuffer = grid2;
        }

        private void DoubleAnimation_Completed(object sender, EventArgs e)
        {
            try
            {
				if (backBuffer != null)
				{
					foreach (WPFDesigner.IDesignElement tscreen in backBuffer.Children)
					{
						tscreen.Stop();
						((WPFDesigner.Screen)tscreen).Close();
					}
					backBuffer.Children.Clear();

				}
            }
            catch (Exception err)
            {
                logger.Error(err + "");
            }
			catch
			{
				logger.Error("Unhandled Error Exception occurred!");
			}

        }
        #endregion

        private void Grid_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            try
            {
                if (primaryBuffer != null)
                {
                    primaryBuffer.Width = this.ActualWidth;
                    primaryBuffer.Height = this.ActualHeight;
                }
                if (primaryPane != null)
                {
                    primaryPane.Width = primaryBuffer.Width;
                    primaryPane.Height = primaryBuffer.Height;
                }
                if (backBuffer != null)
                {
                    backBuffer.Width = this.ActualWidth;
                    backBuffer.Height = this.ActualHeight;
                }
                /*backScale.ScaleX = 1.0;
                backScale.ScaleY = 1.0;
                backBuffer.Children.Add(Pane);
                backBuffer.UpdateLayout();

                //backPlayer = primaryPlayer;
                //scaling
                Double scaleX = this.ActualWidth / Pane.Width;
                Double scaleY = this.ActualHeight / Pane.Height;
                //logger.Info("Scale " + scaleX + "x" + scaleY);
                //logger.Info("Scale " + designerRoot.ActualWidth + "x" + designerRoot.Width );
                backScale.ScaleX = scaleX;
                backScale.ScaleY = scaleY;*/

            }
            catch (Exception err)
            {
                logger.Error(err + "");
            }
			catch
			{
				logger.Error("Unhandled Error Exception occurred!");
			}
        }

        #region Touch

        public void TSA_DisplayScreenPlay(string playlistScreenID, bool isScreenRepeat)
        {
            XElement e = XElement.Load(new XmlNodeReader(doc));
            IEnumerable<XElement> screens = e.Elements("screen");
            foreach (XElement screen in screens)
            {
                if (screen.Attribute("ID").Value.Equals(playlistScreenID) == true)
                {
                    using (XmlReader xmlReader = screen.CreateReader())
                    {
                        if (this.CurrentPaneScreenID.Equals(playlistScreenID) == true)
                            break;

                        XmlDocument xmlDoc = new XmlDocument();
                        xmlDoc.Load(xmlReader);
                        moveScreenXmlNodeTSA = xmlDoc.DocumentElement;
                        IsScreenRepeatPlayOfTSA = isScreenRepeat;
                        IsMoveScreenTSA = true;
                    }

                    break;
                }
            }
        }

        public void TSA_DisplayScreenPlay(UIElement changedPage)
        {
            #region Back Buffer Clear

            if (backBuffer != null)
            {
                if (PlaylistPlayer.IsCurrentPlayingTouchSchedule != true)
                {
                    foreach (WPFDesigner.IDesignElement tscreen in backBuffer.Children)
                    {
                        tscreen.Stop();
                        ((WPFDesigner.Screen)tscreen).Close();
                    }
                }
                else
                {
                    if (CloseTouchProjectHandler != null)
                    {
                        CloseTouchProjectHandler(this);
                    }
                }
                backBuffer.Children.Clear();
            }
            #endregion

            WPFDesigner.IDesignElement temp = backPane;

            backBuffer.Children.Add(changedPage);

            if (temp != null)
            {
                try
                {
                    temp.Stop();
                    ((WPFDesigner.Screen)temp).Close();
                    temp = null;
                }
                catch (Exception ex)
                {
                    temp = null;
                    logger.Error(ex.Message);
                }
            }

            // swap variables
            Grid swap = primaryBuffer;
            primaryBuffer = backBuffer;
            backBuffer = swap;
        }

        public void TSA_DisplayScreenPlay(ArrayList unlinkPlaylistPlayerList, string playlistScreenID, bool isScreenRepeat)
        {
            foreach (PlaylistPlayer pp in unlinkPlaylistPlayerList)
            {
                XElement e = XElement.Load(new XmlNodeReader(pp.doc));
                IEnumerable<XElement> screens = e.Elements("screen");
                XElement screen = screens.OfType<XElement>().Where(s => s.Attribute("ID").Value.Equals(playlistScreenID) == true).SingleOrDefault();
                if (screen != null)
                {
                    using (XmlReader xmlReader = screen.CreateReader())
                    {
                        Console.WriteLine("UnLinked " + playlistScreenID.ToString() + " - " + isScreenRepeat.ToString());

                        //NEPlayer에서 종료 재생시간과 터치 스크린의 Display 제한 시간 중복 실행 방지
                        if (this.CurrentPaneScreenID.Equals(playlistScreenID) == true)
                            break;

                        XmlDocument xmlDoc = new XmlDocument();
                        xmlDoc.Load(xmlReader);
                        moveScreenXmlNodeTSA = xmlDoc.DocumentElement;
                        IsScreenRepeatPlayOfTSA = isScreenRepeat;
                        IsMoveScreenTSA = true;
                    }
                    break;
                }
            }
        }

        public bool IsPlay
        {
            get
            {
                if (backPane == null)
                    return false;

                return backPane.ISPlay;
            }
        }

        #endregion
    }

	#region 재생 목록 관련 이벤트
	/// <summary>
	/// 재생목록 상태
	/// </summary>
	public enum PlaylistStatus
	{
		/// <summary>
		/// 로딩 중
		/// </summary>
		Loading,
		/// <summary>
		/// 재생 중
		/// </summary>
		Playing,
		/// <summary>
		/// 닫힘
		/// </summary>
		Closed,
		/// <summary>
		///	실패
		/// </summary>
		Failed,
		/// <summary>
		/// 지연됨
		/// </summary>
		Pending,
		/// <summary>
		/// 멈춤
		/// </summary>
		Paused
	}

	/// <summary>
	/// 재생 목록 로드 완료 이벤트 핸들러
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	public delegate void PlaylistLoadCompletedHandler(object sender, PlaylistLoadCompletedArgs e);
	/// <summary>
	/// 재생 목록 로드 완료 아규먼트
	/// </summary>
	public class PlaylistLoadCompletedArgs : EventArgs
	{
		/// <summary>
		/// 재생 목록 상태
		/// </summary>
		public PlaylistStatus Status = PlaylistStatus.Closed;

		/// <summary>
		/// 생성자
		/// </summary>
		/// <param name="_status">재생 목록 상태 정보</param>
		public PlaylistLoadCompletedArgs(PlaylistStatus _status)
		{
			Status = _status;
		}
	}
	#endregion
}
