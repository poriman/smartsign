﻿using System;
using System.IO;
using System.Collections;
using System.Xml;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Animation;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Runtime.Serialization.Formatters.Binary;
using System.Linq;

using System.Threading;
using NLog;

using DigitalSignage.Common;
using System.Collections.Generic;

namespace DigitalSignage.PlayControls
{
    #region Touch
    public delegate void ChangedNonTouchScreenDelegate(string paneScreenID, object playlistPlayer);
    public delegate UIElement LoadTouchProjectDelegate(string loadTouchProjectFile, Grid playerGrid, Grid primaryBuffer);
    public delegate void PlayTouchProjectDelegate();
    public delegate void CloseTouchProjectDelegate(Grid playlistPlayer);
    public delegate void CloseTouchPlaylistPlayerDelegate(PlaylistPlayer playlistPlayer);
    public delegate void ChangePlayStateDelegate(bool isTouchProgram, Grid PlaylistPlayer);//Player.TimerLoo.IsPauseDefaultProgram 값을 변경하는 delegate
    #endregion

    /// <summary>
    /// Interaction logic for ProgramPlayer - control for playing IPR files
    /// </summary>
    public partial class ProgramPlayer : Grid
    {
        #region Touch
        public ChangedNonTouchScreenDelegate ChangedNonTouchScreenHandler;
        public static LoadTouchProjectDelegate LoadTouchProjectHandler;
        public static PlayTouchProjectDelegate PlayTouchProjectHandler;
        public static CloseTouchProjectDelegate CloseTouchProjectHandler;
        public static CloseTouchPlaylistPlayerDelegate CloseTouchPlaylistPlayerHandler;
        public static ChangePlayStateDelegate ChangePlayStateHandler;
        private ArrayList unlinkPlayers = new ArrayList();
        #endregion

        XmlDocument doc = null;
        private bool process = false;
        private Thread thread;
        private ArrayList players = new ArrayList();
        private String docStart = "";

		/// <summary>
		/// 프로그램 시작 동기화 루틴
		/// </summary>
//		private EventWaitHandle waitStartProgramHandler = new AutoResetEvent(false);

		/// <summary>
		/// 시작된 재생목록 개수
		/// </summary>
		private int nStartedPlaylistCount = 0;
		private ProgramStatus statusProgram = ProgramStatus.Closed;

		/// <summary>
		/// 프로그램의 상태
		/// </summary>
		public ProgramStatus Status { get { return statusProgram; } }

		private static Logger logger = LogManager.GetCurrentClassLogger();

        public ProgramPlayer()
        {
            InitializeComponent();
        }

		/// <summary>
		/// 프로그램 로드 완료 이벤트
		/// </summary>
		public event ProgramLoadCompletedHandler ProgramLoadCompleted = null;

		bool fade_in = true;
		bool fade_out = true;

		bool show_cursor = false;

		public bool IsFadeIn
		{
			set { fade_in = value; }
		}
		public bool IsFadeOut
		{
			set { fade_out = value; }
		}

		public void Load(string name, bool showCursor)
		{

			show_cursor = showCursor;

			Load(name);
		}

		public void Load(string name)
        {
			try
			{
				//	에러로 인해서 이전 프로그램이 Stop이 안된 경우를 대비 Stop을 선행한다.
				StopProcess();
			}
			catch { }

            doc = new XmlDocument();
            doc.Load(name);
            docStart = System.IO.Path.GetDirectoryName(name);

			statusProgram = ProgramStatus.Loading;
        }

        public void Start()
        {
            Start(TimeSpan.FromSeconds(0));
        }

        public void Start(TimeSpan ts)
        {
            if (doc == null) return;
            // deleting childrens

            nStartedPlaylistCount = 0;
            //waitStartProgramHandler.Reset();

            this.Children.Clear();
            //drawing elements
            if (doc.ChildNodes.Count > 0)
            {
                if (doc.DocumentElement.Name.Equals("program"))
                    rDraw(grid, doc.DocumentElement.ChildNodes[0], 0, 0);

                AddUnlinkPlaylist(grid, doc.DocumentElement.ChildNodes);//Touch
            }
            #region Touch
            PlaylistPlayer.LoadTouchProjectHandler = new LoadTouchProjectDelegate(LoadTouchProjectFunc);//by_JunYoung_Touch
            PlaylistPlayer.PlayTouchProjectHandler = new PlayTouchProjectDelegate(PlayTouchProjectFunc);
            PlaylistPlayer.CloseTouchProjectHandler = new CloseTouchProjectDelegate(CloseTouchProjectFunc);
            PlaylistPlayer.CloseTouchPlaylistPlayerHandler = new CloseTouchPlaylistPlayerDelegate(CloseTouchPlaylistPlayerFunc);
            #endregion

            for (int i = 0; i < players.Count; i++)
            {
                ((PlaylistPlayer)players[i]).IsFadeIn = fade_in;
                ((PlaylistPlayer)players[i]).IsFadeOut = fade_out;

                #region Touch
                ((PlaylistPlayer)players[i]).ChangeTSViewOfPaneScreen = new ChangedNonTouchScreenDelegate(ChangeTSViewOfPaneScreenFunc);
                #endregion

                ((PlaylistPlayer)players[i]).Start(ts);
            }
            //int nCount = 0;
            //while (nCount++ < 200)
            //{
            //    System.Windows.Forms.Application.DoEvents();

            //    if (waitStartProgramHandler.WaitOne(1, false))
            //    { break; }
            //}

            process = true;
        }

		public void Pause()
		{
			try
			{
				for (int i = 0; i < players.Count; i++)
				{
					((PlaylistPlayer)players[i]).Pause();
				}
			}
			catch { }
		}

        private void RemoveControl()
        {
            try
            {
                foreach (UIElement el in grid.Children)
                {
                    BindingOperations.ClearAllBindings(el);
                }
                grid.Children.Clear();
            }
            catch (Exception ex)
            {
                string errmsg = ex.Message.ToString();
            }
        }
		private void StopProcess()
		{
			RemoveControl();
			
            //for (int i = 0; i < players.Count; i++)
            //{
            //    ((PlaylistPlayer)players[i]).Stop();

            //}

            lock (players)
            {
                foreach (var player in players)
                {
					try
					{
						PlaylistPlayer playlistPlayer = (PlaylistPlayer)player;
						playlistPlayer.Stop();
						//CloseTouchPlaylistPlayerHandler(playlistPlayer);

						DispatcherOperation o = this.Dispatcher.BeginInvoke(DispatcherPriority.Send,	//	건들지말자.. 건들면 스크린 해제가 안되는 기이한 현상이 나타난다.
							   CloseTouchPlaylistPlayerHandler, playlistPlayer);
					}
					catch { }
                }

                statusProgram = ProgramStatus.Closed;
                players.Clear();
            }
		}

        public void Stop()
        {
 			StopProcess();
/*			thread = new Thread(new ThreadStart(StopProcess));
			thread.Name = "Stop Process";
			thread.Priority = ThreadPriority.BelowNormal;	//	건들지말자.. 건들면 스크린 해제가 안되는 기이한 현상이 나타난다.
			thread.Start();
*/
        }

		#region Logging routines
		private iVisionLogHelper iVisionLog = iVisionLogHelper.GetInstance;
		#endregion

        #region Draw functions
        private void rDraw(Grid parent, XmlNode node, int row, int column)
        {
            if (node.Name.Equals("pane"))
            {
                // this is pane element
                Border item = new Border();
                item.BorderThickness = new Thickness(0);
                item.Background = Brushes.Black;
                item.DataContext = node;
                item.Margin = new Thickness(0);
                item.ClipToBounds = true;
                parent.Children.Add(item);
                Grid.SetColumn(item, column);
                Grid.SetRow(item, row);
                item.UpdateLayout();

                if (node.Attributes["playlist"] != null)
                {
                    PlaylistPlayer player = new PlaylistPlayer();
					player.PlaylistLoadCompleted += new PlaylistLoadCompletedHandler(player_PlaylistLoadCompleted);

					String playlist = node.Attributes["playlist"].Value;
                    if ((playlist.Length < 2) || (playlist[1] != ':'))
                        // this is relative path
                        playlist = docStart +"\\"+ playlist;

                    player.PlaylistFilePath = playlist;//Touch - Playlist 파일 경로 저장.

                    player.Load(playlist, show_cursor);
                    players.Add(player);
                    item.Child = player;
                }
            }
            if (node.Name.Equals("grid"))
            {
                // this is Grid container
                Grid item = new Grid();
                item.DataContext = node;
                item.Background = Brushes.Black;
                item.Margin = new Thickness(0);
                parent.Children.Add(item);
                Grid.SetColumn(item, column);
                Grid.SetRow(item, row);
                item.UpdateLayout();


                int rc = Int32.Parse(node.Attributes["rows"].Value);
                int cc = Int32.Parse(node.Attributes["columns"].Value);
                string[] sizes = null;
                if (node.Attributes["rowsizes"] != null)
                    sizes = node.Attributes["rowsizes"].Value.Split(',');

                for (int i = 0; i < rc ; i++)
                {
                    RowDefinition def = new RowDefinition();
                    if (sizes != null)
                        def.Height = //new GridLength(Double.Parse( sizes[i / 2])/100*item.ActualHeight);
// 							new GridLength(Double.Parse(sizes[i]) / 100 * item.ActualHeight, GridUnitType.Star);
							new GridLength(Double.Parse(sizes[i]), GridUnitType.Star);
					item.RowDefinitions.Add(def);
                }

                sizes = null;
                if (node.Attributes["colsizes"] != null)
                    sizes = node.Attributes["colsizes"].Value.Split(',');
                for (int i = 0; i < cc; i++)
                {
                    ColumnDefinition def = new ColumnDefinition();
                     if (sizes != null)
// 						 def.Width = new GridLength(Double.Parse(sizes[i]) / 100 * item.ActualWidth, GridUnitType.Star);
						 def.Width = new GridLength(Double.Parse(sizes[i]), GridUnitType.Star);
					 item.ColumnDefinitions.Add(def);
                }


//                 for (int i = 0; i < node.ChildNodes.Count; i++)
//                 {
//                     rDraw(item, node.ChildNodes[i], (i / cc) * 2, (i % cc) * 2);
//                 }

				int nodeCount = 0;

				for (int nRow = 0; nRow < rc; ++nRow)
				{
					for (int nCol = 0; nCol < cc; ++nCol)
					{
						if (nodeCount < node.ChildNodes.Count)
						{
							rDraw(item, node.ChildNodes[nodeCount++], nRow, nCol);
						}
						else
							break;
					}
				}
            }
            return;
        }

        #endregion

        #region Event handlers

		/// <summary>
		/// 상태 정보 변경 이벤트
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		void player_PlaylistLoadCompleted(object sender, PlaylistLoadCompletedArgs e)
		{
			try
			{
				#region PENDING TEST
// 				if (ProgramLoadCompleted != null)
// 				{
// 					ProgramLoadCompleted(this, new ProgramLoadCompletedArgs(statusProgram = ProgramStatus.Pending));
// 					return;
// 				}
				#endregion

				if ( e.Status == PlaylistStatus.Loading) return;

				if(e.Status == PlaylistStatus.Failed)
				{
					if (ProgramLoadCompleted != null && statusProgram == ProgramStatus.Loading)
					{
						ProgramLoadCompleted(this, new ProgramLoadCompletedArgs(statusProgram = ProgramStatus.Failed));
						return;
					}
				}
				else if(e.Status == PlaylistStatus.Pending)
				{
					if(ProgramLoadCompleted != null) ProgramLoadCompleted(this, new ProgramLoadCompletedArgs(statusProgram = ProgramStatus.Pending));
					return;
				}
				
				if (++nStartedPlaylistCount >= players.Count)
				{
//					waitStartProgramHandler.Set();
					logger.Debug(String.Format("player_PlaylistLoadCompleted: {0}, {1}", nStartedPlaylistCount - 1, players.Count));
					if (ProgramLoadCompleted != null && statusProgram == ProgramStatus.Loading)
					{
						ProgramLoadCompleted(this, new ProgramLoadCompletedArgs(statusProgram = ProgramStatus.Playing));
					}
				}
			}
			catch {}
		}

		private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
        }

        private void Grid_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            try
            {
            }
            catch (Exception err)
            {
                logger.Error(err + "");
            }
        }
        #endregion

        #region Touch

        public Dictionary<string, object> TSA_GetPlaylistPlayerItem()
        {
            try
            {
                Dictionary<string, object> items = new Dictionary<string, object>();

                for (int i = 0; i < this.players.Count; i++)
                {
                    PlaylistPlayer _playlistPlayer = (PlaylistPlayer)this.players[i];
                    if (_playlistPlayer != null)
                    {
                        string _screenID = _playlistPlayer.CurrentPaneScreenID;
                        if (string.IsNullOrEmpty(_screenID) != true)
                            items.Add(_screenID, this.players[i]);
                        else
                            logger.Info("PlaylistPlayer Screen ID is Null!!!");
                    }
                }

                //<<--- Unlink PlaylistPlayer 등록
                for (int i = 0; i < this.unlinkPlayers.Count; i++)
                {
                    PlaylistPlayer _playlistPlayer = (PlaylistPlayer)this.unlinkPlayers[i];
                    if (_playlistPlayer != null)
                    {
                        string _screenID = _playlistPlayer.CurrentPaneScreenID;
                        if (string.IsNullOrEmpty(_screenID) != true)
                            items.Add(_screenID, this.unlinkPlayers[i]);
                        else
                            logger.Info("Unlink PlaylistPlayer Screen ID is Null!!!");
                    }
                }
                //>>
                return items;
            }
            catch (Exception ex)
            {
                logger.Info(ex.Message);
                return null;
            }
        }

        public void TSA_PlaylistStart(string playlistScreenID, object paneScreenView, bool isLinked, bool isScreenRepeat)
        {
            if (isLinked == true)
            {
                for (int i = 0; i < players.Count; i++)
                {
                    PlaylistPlayer player = (PlaylistPlayer)players[i];
                    if (player.Equals(paneScreenView) == true)
                    {
                        player.TSA_DisplayScreenPlay(playlistScreenID, isScreenRepeat);
                        break;
                    }
                }
            }
            else
            {
                for (int i = 0; i < players.Count; i++)
                {
                    PlaylistPlayer unlinkPlayer = (PlaylistPlayer)unlinkPlayers[i];
                    PlaylistPlayer player = players.OfType<PlaylistPlayer>().Where(w => w.Equals(paneScreenView) == true).SingleOrDefault();
                    if (player == null)
                        return;//처리 필요.

                    if (player.Equals(paneScreenView) == true)
                    {
                        player.TSA_DisplayScreenPlay(unlinkPlayers, playlistScreenID, isScreenRepeat);
                        break;
                    }
                }
            }
        }

        public void TSA_PlaylistStart(UIElement changedPage, UIElement playerScreen)
        {
            for (int i = 0; i < players.Count; i++)
            {
                PlaylistPlayer player = (PlaylistPlayer)players[i];
                if (player.Equals(playerScreen) == true)
                {
                    player.TSA_DisplayScreenPlay(changedPage);
                    break;
                }
            }
        }

        private void ChangeTSViewOfPaneScreenFunc(string paneScreenID, object playlistPlayer)
        {
            ChangedNonTouchScreenHandler(paneScreenID, playlistPlayer);
        }

        private void AddUnlinkPlaylist(Grid parent, XmlNodeList nodeList)
        {
            foreach (XmlNode node in nodeList)
            {
                if (node.Name.Equals("unlink") == true)
                {
                    if (node.Attributes["playlist"] != null)
                    {
                        PlaylistPlayer player = new PlaylistPlayer();
                        String playlist = node.Attributes["playlist"].Value;
                        if ((playlist.Length < 2) || (playlist[1] != ':'))
                            // this is relative path
                            playlist = docStart + "\\" + playlist;

                        player.PlaylistFilePath = playlist;

                        if (File.Exists(playlist) == false)
                            continue;

                        player.Load(playlist, show_cursor);
                        player.IsLinkPlaylist = false;//Unlink
                        unlinkPlayers.Add(player);
                        //item.Child = player;
                    }
                }
            }
        }

        /// <summary>
        /// by_JunYoung_Touch
        /// </summary>
        /// <param name="touchProjectName"></param>
        /// <returns></returns>
        private UIElement LoadTouchProjectFunc(string touchProjectName, Grid playerGrid, Grid primaryBuffer)
        {
            if (LoadTouchProjectHandler != null)
                return LoadTouchProjectHandler(touchProjectName, playerGrid, primaryBuffer);
            return null;
        }

        private void PlayTouchProjectFunc()
        {
            if (PlayTouchProjectHandler != null)
                PlayTouchProjectHandler();
        }

        private void CloseTouchProjectFunc(Grid playlistPlayer)
        {
            if (CloseTouchProjectHandler != null)
                CloseTouchProjectHandler(playlistPlayer);
        }

        private void CloseTouchPlaylistPlayerFunc(PlaylistPlayer playlistPlayer)
        {
            if (CloseTouchPlaylistPlayerHandler != null)
                CloseTouchPlaylistPlayerHandler(playlistPlayer);
        }

        public void SetPlayStateTouchProject(bool isTouchPlayState)
        {
            try
            {
                PlaylistPlayer.IsPausePlayItem = isTouchPlayState;
            }
            catch (Exception ex)
            {
            }
        }
        #endregion

    }

	#region 프로그램 상태 관련 이벤트
	/// <summary>
	/// 프로그램 상태
	/// </summary>
	public enum ProgramStatus
	{
		/// <summary>
		/// 로딩 중
		/// </summary>
		Loading,
		/// <summary>
		/// 재생 중
		/// </summary>
		Playing,
		/// <summary>
		/// 닫힘
		/// </summary>
		Closed,
		/// <summary>
		/// 실패
		/// </summary>
		Failed,
		/// <summary>
		/// 지연됨
		/// </summary>
		Pending
	}
	/// <summary>
	/// 프로그램 로드 완료 이벤트 핸들러
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	public delegate void ProgramLoadCompletedHandler(object sender, ProgramLoadCompletedArgs e);
	/// <summary>
	/// 프로그램 로드 완료 아규먼트
	/// </summary>
	public class ProgramLoadCompletedArgs : EventArgs
	{
		/// <summary>
		/// 프로그램 상태
		/// </summary>
		public ProgramStatus Status = ProgramStatus.Closed;

		/// <summary>
		/// 생성자
		/// </summary>
		/// <param name="_status">프로그램 상태 정보</param>
		public ProgramLoadCompletedArgs(ProgramStatus _status)
		{
			Status = _status;
		}
	}
	#endregion
}
