﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Xml;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Animation;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Runtime.Serialization.Formatters.Binary;
using System.Linq;

using System.Threading;
using NLog;

using DigitalSignage.Common;

namespace DigitalSignage.PlayControls
{
    /// <summary>
    /// Interaction logic for PlaylistPlayer - control for playing IPL files
    /// </summary>
    public partial class PlaylistPlayerPreview : Window
    {

        public PlaylistPlayerPreview()
        {
            InitializeComponent();
        }

        public void Play(string plname)
        {
			playerUI.IsFadeIn = false;
			playerUI.IsFadeOut = false;
            playerUI.Load(plname, true);
            playerUI.Start();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
			if (playerUI != null)
			{
				playerUI.Stop();
				playerUI = null;
			}
        }

		private void Window_Unloaded(object sender, RoutedEventArgs e)
		{
			if (playerUI != null)
			{
				playerUI.Stop();
				playerUI = null;
			}
		}
    }
}
