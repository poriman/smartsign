﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Xml;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Animation;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Runtime.Serialization.Formatters.Binary;
using System.Linq;

using System.Threading;
using NLog;

using DigitalSignage.Common;
using System.Xml.Linq;
using System.Collections;

namespace DigitalSignage.PlayControls
{
    /// <summary>
    /// Interaction logic for PlaylistPlayer - control for playing IPL files
    /// </summary>
    public partial class PlaylistPlayer : Grid
    {
        #region Touch

        public bool IsLinkPlaylist = true;
        public string CurrentPaneScreenID;
        public ChangedNonTouchScreenDelegate ChangeTSViewOfPaneScreen;
        public string PlaylistFilePath { get; set; }
        private bool IsMoveScreenTSA = false;
        private bool IsScreenRepeatPlayOfTSA = false;
        private XmlNode moveScreenXmlNodeTSA = null;

        public static LoadTouchProjectDelegate LoadTouchProjectHandler;
        public static PlayTouchProjectDelegate PlayTouchProjectHandler;
        public static CloseTouchProjectDelegate CloseTouchProjectHandler;
        private static bool IsPlayingTouchProject = false;
        public static bool IsPausePlayItem = false;//Playlisit 중 PlayItem이 Touch Project 일 경우 반복스케줄에 의해 스케줄링되지 않도록 한다. 
        #endregion

		/// <summary>
		/// 재생 목록 상태 정보
		/// </summary>
		PlaylistStatus _status = PlaylistStatus.Closed;

		/// <summary>
		/// 재생 목록 상태 정보
		/// </summary>
		public PlaylistStatus Status
		{
			get { return _status; }
			set
			{
				_status = value;
			}
		}

		/// <summary>
		/// 상태 변경 이벤트
		/// </summary>
		public event PlaylistLoadCompletedHandler PlaylistLoadCompleted = null;

		private object semaphore = null;

        XmlDocument doc = null;

        private bool process = false;
        private Thread thread;
        private String docStart = "";
        private static Logger logger = LogManager.GetCurrentClassLogger();

        private Grid primaryBuffer = null;
        //private ScaleTransform primaryScale = null;
        private WPFDesigner.IDesignElement primaryPane = null;

        private Grid backBuffer = null;
        //private ScaleTransform backScale = null;
        private WPFDesigner.IDesignElement backPane = null;

		bool fade_in = true;
		bool fade_out = true;

		bool show_cursor = false;

		public bool IsFadeIn
		{
			set { fade_in = value; }
		}
		public bool IsFadeOut
		{
			set { fade_out = value; }
		}

        public PlaylistPlayer()
        {
            InitializeComponent();
			primaryBuffer = grid;
			backBuffer = grid2;
		}

		public void Load(string name, bool showCursor)
		{
			show_cursor = showCursor;

			Load(name);
		}

        public void Load(string name)
        {
            doc = new XmlDocument();
            doc.Load(name);
            docStart = System.IO.Path.GetDirectoryName(name);
        }

        public void Start()
        {
            if (doc == null) return;
			
            process = true;

			#region 상태 변경
			this.Status = PlaylistStatus.Loading;
			#endregion

			semaphore = new object();
            
			//Starting the UDP Server thread.
            thread = new Thread(new ThreadStart(Process));
            thread.Name = "IPLPlayer thread";
			thread.Priority = ThreadPriority.BelowNormal;	//	건들지말자.. 건들면 스크린 해제가 안되는 기이한 현상이 나타난다.
            thread.Start();
        }


        public void Stop()
        {
            process = false;

			#region 상태 변경
			this.Status = PlaylistStatus.Closed;
			#endregion

			//	hsshin 락을 거는이유는 스크린 로드중 스레드를종료시킬수도 있기때문이다.
            if (semaphore == null)
            {
                logger.Debug("Playlist Stop Start");
                if (thread != null)
                {
                    thread.Abort();
                }
                logger.Debug("Playlist Stop End");

                return;
            }

			lock (semaphore)
			{
				logger.Debug("Playlist Stop Start");
				if (thread != null)
				{
					thread.Abort();
				}
			}

			logger.Debug("Playlist Stop End");

        }

        private void Process()
        {
			logger.Debug("Playlist Thread Process Start");

			try
			{
				while (process)
					rProcess(doc.DocumentElement);
			}
			catch (ThreadAbortException threadAbort)
			{
				logger.Error(threadAbort.Message);
			}
			catch (Exception err)
			{
				logger.Error(err.Message);
			}
			finally
			{
				stopProcess();
			}

			#region 상태 변경
			this.Status = PlaylistStatus.Closed;
			#endregion

			logger.Debug("Playlist Thread Process End");
        }

        private bool rProcess(XmlNode node)
        {
			try
			{
				if (node.Name.Equals("screen"))
				{
					string strTime = node.Attributes["time"].Value;
					string strID = node.Attributes["ID"].Value;
					TimeSpan ts = new TimeSpan(0);
					string project = null;
					if (strTime != null && strTime.Length > 0)
						ts = TimeConverter.StrToTime(strTime);                    

					if (strID != null && strID.Length > 0)
                    {
                        project = strID;

                        #region Touch

                        CurrentPaneScreenID = strID;
                        #endregion
                    }

					if ((ts.Ticks > 0) && (project != null))
					{
						logger.Debug("Playlist Thread LoadProject Start");
						lock (semaphore)
						{
							if (process == false)
								return true;

							loadProject(project);
						}
						logger.Debug("Playlist Thread LoadProject End");

						for (int i = 0; i < ts.TotalSeconds; i++)
						{
                            if (process == false)
                            {
                                IsScreenRepeatPlayOfTSA = false;
                                return true;
                            }

                            #region Touch : 클릭 이벤트 발생시 화면 변경
                            if (IsMoveScreenTSA == true)
                            {
                                i = 0;
                                IsMoveScreenTSA = false;
                                strTime = this.moveScreenXmlNodeTSA.Attributes["time"].Value;
                                strID = this.moveScreenXmlNodeTSA.Attributes["ID"].Value;

                                Console.WriteLine(strID + ", " + strTime);

                                ts = new TimeSpan(0);
                                if (strTime != null && strTime.Length > 0)
                                    ts = TimeConverter.StrToTime(strTime);
                                if (strID != null && strID.Length > 0)
                                {
                                    project = strID;
                                    CurrentPaneScreenID = strID;//Touch : TSA 추가
                                }

                                if ((ts.Ticks > 0) && (project != null))
                                    loadProject(project);

                                continue;
                            }

                            #endregion

                            if (IsMoveScreenTSA != true && IsScreenRepeatPlayOfTSA == true)
                                i = 0;

							Thread.Sleep(1000);
						}
                        IsScreenRepeatPlayOfTSA = false;

                        #region Touch - PlayItem이 Touch Project 일 경우 자동 종료 전까지 계속 유지
                        while (IsPausePlayItem)
                        {
                            Thread.Sleep(1000);
                        }
                        #endregion
                    }
				}
				else
				{
// 					if (node.Name.Equals("group"))
// 					{
// 						string strrepeat = node.Attributes["repeat"].Value;
// 						if (strrepeat != null && strrepeat.Length > 0)
// 						{
// 							repeat = Int32.Parse(strrepeat);
// 							// !!! go down to default block
// 						}
// 					}
					foreach (XmlNode sub in node.ChildNodes)
						if (rProcess(sub))
							return true;
				}
			}
			catch (ThreadAbortException )
			{
				#region 상태 변경
				this.Status = PlaylistStatus.Closed;
				#endregion
			}
			catch (Exception ex)
			{
				logger.Error(ex + "");
				#region 상태 변경
				this.Status = PlaylistStatus.Closed;
				#endregion
			}
            return false;
		}

		#region Logging routines	

		private iVisionLogHelper iVisionLog = iVisionLogHelper.GetInstance;
		
		#endregion

		#region Thread routines

		delegate void stopProcessCB();
		private void stopProcess()
		{
			if (!this.Dispatcher.CheckAccess())
			{
				try
				{
					this.Dispatcher.BeginInvoke(DispatcherPriority.Normal,	//	건들지말자.. 건들면 스크린 해제가 안되는 기이한 현상이 나타난다.
					   new stopProcessCB(stopProcess));
				}
				catch (Exception e)
				{
					logger.Error(e + "");
				}
			}
			else
			{
				try
				{
					if (primaryPane != null)
					{
						primaryPane.Stop();
						primaryPane = null;
					}
					//	hsshin 정지 시 Screen도 정지시켜주자.
					if (primaryBuffer != null)
					{
						foreach (WPFDesigner.IDesignElement screen in primaryBuffer.Children)
						{
							logger.Debug("Playlist screen Stop");
							screen.Stop();

							((WPFDesigner.Screen)screen).Close();
						}
						primaryBuffer.Children.Clear();
					}
					if (backBuffer != null)
					{
						foreach (WPFDesigner.IDesignElement screen in backBuffer.Children)
						{
							logger.Debug("backBuffer screen Stop");
							screen.Stop();
							((WPFDesigner.Screen)screen).Close();
						}
						backBuffer.Children.Clear();
					}

					if (backPane != null)
					{
						logger.Debug("backBuffer screen Stop");

						backPane.Stop();
						((WPFDesigner.Screen)backPane).Close();
						backPane = null;
					}

					grid.Children.Clear();
					grid2.Children.Clear();

					GC.Collect();
				}
				catch (Exception ee)
				{
					logger.Debug(ee.ToString());
				}


			}
		}

		delegate void loadProjectCB(string filename);
        public void loadProject(string filename)
        {
            if (!this.Dispatcher.CheckAccess())
            {
                try
                {
					this.Dispatcher.BeginInvoke(DispatcherPriority.Normal,	//	건들지말자.. 건들면 스크린 해제가 안되는 기이한 현상이 나타난다.
                       new loadProjectCB(loadProject), filename);
                }
                catch (Exception e)
                {
                    logger.Error(e + "");
                }
            }
            else
            {
                try
                {
                    //primaryBuffer.Children.Clear();
                    if (!filename.Equals(""))
                    {
                        logger.Info("Loading project " + filename);

                        #region Touch

                        string temp_filename = filename;
                        if ((filename.Length < 2) || (filename[1] != ':'))
                            // relative path
                            temp_filename = docStart + "\\" + filename;
                        FileInfo fi = new FileInfo(temp_filename);

                        #endregion

                        if (fi.Extension.Equals(".tpf") != true)
                        {
                            if (backBuffer != null)
                            {
                                if (PlaylistPlayer.IsPlayingTouchProject != true)//이전 Play 대상이 Touch(tpf)가 아니라면 기존 Screen에 대한 처리 수행
                                {
                                    foreach (WPFDesigner.IDesignElement tscreen in backBuffer.Children)
                                    {
                                        tscreen.Stop();
                                        ((WPFDesigner.Screen)tscreen).Close();

                                    }
                                }
                                else
                                {
                                    if (CloseTouchProjectHandler != null)
                                    {
                                        CloseTouchProjectHandler();
                                    }

                                }
                                backBuffer.Children.Clear();
                            }

                            //	hsshin
                            WPFDesigner.IDesignElement temp = backPane;

                            string NameOfFile = filename;

                            if ((filename.Length < 2) || (filename[1] != ':'))
                                // relative path
                                filename = docStart + "\\" + filename;

                            logger.Info("Loading screen " + filename);

                            WPFDesigner.Screen screen = WPFDesigner.ProjectManager.LoadScreen(filename);
                            backBuffer.Children.Add(screen);
                            screen.FillParent();

                            backPane = (WPFDesigner.IDesignElement)screen;
                            backPane.Play();

                            if (temp != null)
                            {
                                try
                                {
                                    temp.Stop();
                                    ((WPFDesigner.Screen)temp).Close();
                                    temp = null;
                                }
                                catch (Exception ex)
                                {
                                    temp = null;
                                    logger.Error(ex.Message);
                                }
                            }

                            //hsshin
                            if (iVisionLog.ScreenLogStarted != null)
                            {
                                ((WPFDesigner.Screen)backPane).DataContext = NameOfFile;
                                iVisionLog.ScreenLogStarted(NameOfFile);
                            }

                            if (fade_in) ((Storyboard)backBuffer.Resources["Up"]).Begin(backBuffer);
                            else backBuffer.Opacity = 1.0;

                            if (fade_out) ((Storyboard)primaryBuffer.Resources["Down"]).Begin(primaryBuffer);
                            else
                            {
                                primaryBuffer.Opacity = 0.0;

                                if (primaryBuffer != null)
                                {
                                    foreach (WPFDesigner.IDesignElement tscreen in primaryBuffer.Children)
                                    {
                                        tscreen.Stop();
                                        ((WPFDesigner.Screen)tscreen).Close();
                                    }
                                    primaryBuffer.Children.Clear();
                                }
                            }

                            // swap variables
                            Grid swap = primaryBuffer;
                            primaryBuffer = backBuffer;
                            backBuffer = swap;

                            #region Touch
                            //ChangeTSViewOfPaneScreen(this.CurrentPaneScreenID, this);                           
                            #endregion

                            //if (!show_cursor) System.Windows.Forms.Cursor.Hide();//조준영 테스트

                            PlaylistPlayer.IsPlayingTouchProject = false;
                        }
                        else
                        {                            
                            #region  Touch - Touch Designer로 부터 생성된 TPF 파일일 경우

                            #region Back Buffer Clear

                            if (backBuffer != null)
                            {
                                if (PlaylistPlayer.IsPlayingTouchProject != true)
                                {
                                    foreach (WPFDesigner.IDesignElement tscreen in backBuffer.Children)
                                    {
                                        tscreen.Stop();
                                        ((WPFDesigner.Screen)tscreen).Close();
                                    }
                                }
                                else
                                {
                                    if (CloseTouchProjectHandler != null)
                                    {
                                         CloseTouchProjectHandler();
                                    }
                                }
                                backBuffer.Children.Clear();
                            }
                            #endregion

                            //	hsshin
                            WPFDesigner.IDesignElement temp = backPane;

                            string NameOfFile = filename;

                            if ((filename.Length < 2) || (filename[1] != ':'))
                                // relative path
                                filename = docStart + "\\" + filename;

                            logger.Info("Loading screen " + filename);

                            WPFDesigner.Screen screen = null;
                            if (LoadTouchProjectHandler != null)
                            {
                                Grid uielement = LoadTouchProjectHandler(filename, backBuffer) as Grid;

                                #region 껍데기 Screen  객체를 생성한다.
                                screen = new WPFDesigner.Screen((int)uielement.Width, (int)uielement.Height, false);
                                backPane = (WPFDesigner.IDesignElement)screen;
                                #endregion

                                backBuffer.Children.Add(uielement);
                            }

                            if (screen == null)
                                backPane = new WPFDesigner.Screen(100, 100, false);

                            if (PlayTouchProjectHandler != null)
                            {
                                PlayTouchProjectHandler();
                            }

                            if (temp != null)
                            {
                                try
                                {
                                    temp.Stop();
                                    ((WPFDesigner.Screen)temp).Close();
                                    temp = null;
                                }
                                catch (Exception ex)
                                {
                                    temp = null;
                                    logger.Error(ex.Message);
                                }
                            }

                            //hsshin
                            if (iVisionLog.ScreenLogStarted != null)
                            {
                                ((WPFDesigner.Screen)backPane).DataContext = NameOfFile;
                                iVisionLog.ScreenLogStarted(NameOfFile);
                            }

                            if (fade_in) ((Storyboard)backBuffer.Resources["Up"]).Begin(backBuffer);
                            else backBuffer.Opacity = 1.0;

                            if (fade_out) ((Storyboard)primaryBuffer.Resources["Down"]).Begin(primaryBuffer);
                            else
                            {
                                primaryBuffer.Opacity = 0.0;

                                if (primaryBuffer != null)
                                {
                                    foreach (WPFDesigner.IDesignElement tscreen in primaryBuffer.Children)
                                    {
                                        tscreen.Stop();
                                        ((WPFDesigner.Screen)tscreen).Close();
                                    }
                                    primaryBuffer.Children.Clear();
                                }
                            }

                            // swap variables
                            Grid swap = primaryBuffer;
                            primaryBuffer = backBuffer;
                            backBuffer = swap;

                            if (!show_cursor) System.Windows.Forms.Cursor.Hide();

                            PlaylistPlayer.IsPlayingTouchProject = true;
                            System.Windows.Forms.Cursor.Show();//마우스 
                            
                            #endregion
                        }

						#region 상태 변경
						this.Status = PlaylistStatus.Playing;
						if (PlaylistLoadCompleted != null) PlaylistLoadCompleted(this, new PlaylistLoadCompletedArgs(PlaylistStatus.Playing));
						#endregion

						return;
					}

                }
				catch (ThreadAbortException te)
				{
					logger.Error(te.Message);
				}
				catch (iVisionException ive)
				{
					iVisionLog.ScreenErrorLog(filename, ive.ErrorCode, ive.DetailMessage);
				}
                catch (Exception e)
                {
					iVisionLog.ScreenErrorLog(filename, ErrorCodes.IS_ERROR_INTERNAL, e.Message);
                }

				_status = PlaylistStatus.Closed;
            }
        }

        #endregion

        #region Event handlers
        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
//             primaryBuffer = grid;
//             backBuffer = grid2;
        }

        private void DoubleAnimation_Completed(object sender, EventArgs e)
        {
            try
            {
				if (backBuffer != null)
				{
					foreach (WPFDesigner.IDesignElement tscreen in backBuffer.Children)
					{
						tscreen.Stop();
						((WPFDesigner.Screen)tscreen).Close();
					}
					backBuffer.Children.Clear();

				}
            }
            catch (Exception err)
            {
                logger.Error(err + "");
            }
        }
        #endregion

        private void Grid_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            try
            {
                if (primaryBuffer != null)
                {
                    primaryBuffer.Width = this.ActualWidth;
                    primaryBuffer.Height = this.ActualHeight;
                }
                if (primaryPane != null)
                {
                    primaryPane.Width = primaryBuffer.Width;
                    primaryPane.Height = primaryBuffer.Height;
                }
                if (backBuffer != null)
                {
                    backBuffer.Width = this.ActualWidth;
                    backBuffer.Height = this.ActualHeight;
                }
                /*backScale.ScaleX = 1.0;
                backScale.ScaleY = 1.0;
                backBuffer.Children.Add(Pane);
                backBuffer.UpdateLayout();

                //backPlayer = primaryPlayer;
                //scaling
                Double scaleX = this.ActualWidth / Pane.Width;
                Double scaleY = this.ActualHeight / Pane.Height;
                //logger.Info("Scale " + scaleX + "x" + scaleY);
                //logger.Info("Scale " + designerRoot.ActualWidth + "x" + designerRoot.Width );
                backScale.ScaleX = scaleX;
                backScale.ScaleY = scaleY;*/

            }
            catch (Exception err)
            {
                logger.Error(err + "");
            }
        }

        #region Touch

        public void TSA_DisplayScreenPlay(string playlistScreenID, bool isScreenRepeat)
        {
            XElement e = XElement.Load(new XmlNodeReader(doc));
            IEnumerable<XElement> screens = e.Elements("screen");
            foreach (XElement screen in screens)
            {
                if (screen.Attribute("ID").Value.Equals(playlistScreenID) == true)
                {
                    using (XmlReader xmlReader = screen.CreateReader())
                    {
                        if (this.CurrentPaneScreenID.Equals(playlistScreenID) == true)
                            break;

                        XmlDocument xmlDoc = new XmlDocument();
                        xmlDoc.Load(xmlReader);
                        moveScreenXmlNodeTSA = xmlDoc.DocumentElement;
                        IsScreenRepeatPlayOfTSA = isScreenRepeat;
                        IsMoveScreenTSA = true;
                    }

                    break;
                }
            }
        }

        public void TSA_DisplayScreenPlay(UIElement changedPage)
        {
            #region Back Buffer Clear

            if (backBuffer != null)
            {
                if (PlaylistPlayer.IsPlayingTouchProject != true)
                {
                    foreach (WPFDesigner.IDesignElement tscreen in backBuffer.Children)
                    {
                        tscreen.Stop();
                        ((WPFDesigner.Screen)tscreen).Close();
                    }
                }
                else
                {
                    if (CloseTouchProjectHandler != null)
                    {
                        CloseTouchProjectHandler();
                    }
                }
                backBuffer.Children.Clear();
            }
            #endregion

            WPFDesigner.IDesignElement temp = backPane;

            backBuffer.Children.Add(changedPage);

            if (temp != null)
            {
                try
                {
                    temp.Stop();
                    ((WPFDesigner.Screen)temp).Close();
                    temp = null;
                }
                catch (Exception ex)
                {
                    temp = null;
                    logger.Error(ex.Message);
                }
            }

            // swap variables
            Grid swap = primaryBuffer;
            primaryBuffer = backBuffer;
            backBuffer = swap;
        }

        public void TSA_DisplayScreenPlay(ArrayList unlinkPlaylistPlayerList, string playlistScreenID, bool isScreenRepeat)
        {
            foreach (PlaylistPlayer pp in unlinkPlaylistPlayerList)
            {
                XElement e = XElement.Load(new XmlNodeReader(pp.doc));
                IEnumerable<XElement> screens = e.Elements("screen");
                XElement screen = screens.OfType<XElement>().Where(s => s.Attribute("ID").Value.Equals(playlistScreenID) == true).SingleOrDefault();
                if (screen != null)
                {
                    using (XmlReader xmlReader = screen.CreateReader())
                    {
                        Console.WriteLine("UnLinked " + playlistScreenID.ToString() + " - " + isScreenRepeat.ToString());

                        //NEPlayer에서 종료 재생시간과 터치 스크린의 Display 제한 시간 중복 실행 방지
                        if (this.CurrentPaneScreenID.Equals(playlistScreenID) == true)
                            break;

                        XmlDocument xmlDoc = new XmlDocument();
                        xmlDoc.Load(xmlReader);
                        moveScreenXmlNodeTSA = xmlDoc.DocumentElement;
                        IsScreenRepeatPlayOfTSA = isScreenRepeat;
                        IsMoveScreenTSA = true;
                    }
                    break;
                }
            }
        }

        public bool IsPlay
        {
            get
            {
                if (backPane == null)
                    return false;

                return backPane.ISPlay;
            }
        }

        #endregion
    }

	#region 재생 목록 관련 이벤트
	/// <summary>
	/// 재생목록 상태
	/// </summary>
	public enum PlaylistStatus
	{
		/// <summary>
		/// 로딩 중
		/// </summary>
		Loading,
		/// <summary>
		/// 재생 중
		/// </summary>
		Playing,
		/// <summary>
		/// 닫힘
		/// </summary>
		Closed
	}

	/// <summary>
	/// 재생 목록 로드 완료 이벤트 핸들러
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	public delegate void PlaylistLoadCompletedHandler(object sender, PlaylistLoadCompletedArgs e);
	/// <summary>
	/// 재생 목록 로드 완료 아규먼트
	/// </summary>
	public class PlaylistLoadCompletedArgs : EventArgs
	{
		/// <summary>
		/// 재생 목록 상태
		/// </summary>
		public PlaylistStatus Status = PlaylistStatus.Closed;

		/// <summary>
		/// 생성자
		/// </summary>
		/// <param name="_status">재생 목록 상태 정보</param>
		public PlaylistLoadCompletedArgs(PlaylistStatus _status)
		{
			Status = _status;
		}
	}
	#endregion
}
