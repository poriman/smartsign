// -----------------------------------------------------------------------------
//
// Copyright (c) 2009 Intel Corporation. All Rights Reserved.
//
// The source code contained or described herein and all documents related to 
// the source code ("Material") are owned by Intel Corporation or its suppliers 
// or licensors. Title to the Material remains with Intel Corporation or its 
// suppliers and licensors. The Material may contain trade secrets and 
// proprietary and confidential information of Intel Corporation and its 
// suppliers and licensors, and is protected by worldwide copyright and trade 
// secret laws and treaty provisions. No part of the Material may be used, 
// copied, reproduced, modified, published, uploaded, posted, transmitted, 
// distributed, or disclosed in any way without Intel’s prior express written 
// permission. 
//
// No license under any patent, copyright, trade secret or other intellectual 
// property right is granted to or conferred upon you by disclosure or delivery 
// of the Materials, either expressly, by implication, inducement, estoppel or 
// otherwise. Any license under such intellectual property rights must be 
// express and approved by Intel in writing.
//
// * Third Party trademarks are the property of their respective owners.
//
// Unless otherwise agreed by Intel in writing, you may not remove or alter this 
// notice or any other notice embedded in Materials by Intel or Intel’s 
// suppliers or licensors in any way.
//
// -----------------------------------------------------------------------------
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.ComponentModel;
//using WSManAutomation;
using MOF_CLASSES;

//using Intel.Logging;

namespace Intel.vPro.AMT
{
    /// <summary>
    /// Used by Power Event handlers to communicate action & result for a particular power event
    /// </summary>
    public class PowerEventArgs : EventArgs
    {
        #region Private Fields
        private AmtSystem _AmtSystem;
        private AmtRemoteControlAction _Action;
        private bool _Result;
        #endregion

        #region Constructors
        /// <summary>
        /// Constructs an an event args object specific to a Power control event on the specified machine
        /// </summary>
        /// <param name="system">The Intel(R) AMT system that this power event applies to</param>
        /// <param name="action">The action that is being taken on the system</param>
        public PowerEventArgs(AmtSystem system, AmtRemoteControlAction action)
        {
            AmtSystem = system;
            Action = action;
        }

        /// <summary>
        /// Constructs an an event args object specific to a Power control event on the specified machine
        /// </summary>
        /// <param name="system">The Intel(R) AMT system that this power event applies to</param>
        /// <param name="action">The action that is being taken on the system</param>
        /// <param name="result">The result (true=success) of the remote control action taken on the
        ///     specified system</param>
        public PowerEventArgs(AmtSystem system, AmtRemoteControlAction action, bool result)
        {
            AmtSystem = system;
            Action = action;
            Result = result;
        }
        #endregion

        #region Public Properties
        /// <summary>
        /// The Intel(R) AMT System object representing the system with which this remote control event applies to
        /// </summary>
        public AmtSystem AmtSystem
        {
            get { return _AmtSystem; }
            set { _AmtSystem = value; }
        }

        /// <summary>
        /// The remote control action type for this particular event
        /// </summary>
        public AmtRemoteControlAction Action
        {
            get { return _Action; }
            set { _Action = value; }
        }

        /// <summary>
        /// Represents the outcome of the remote control event (true=succeeded)
        /// </summary>
        public bool Result
        {
            get { return _Result; }
            set { _Result = value; }
        }
        #endregion
    }

    /// <summary>
    /// All supported primary Remote Control actions
    /// </summary>
    public enum AmtRemoteControlAction
    {
        Reset = 0x10,
        PowerUp = 0x11,
        PowerDown = 0x12
    }

    /// <summary>
    /// Remote Control command support for Intel(R) AMT, including synchronous and asynchronous, thread-safe commands
    /// that can be executed over 1-to-many Intel(R) AMT systems.
    /// </summary>
    public static class AmtRemoteControl
    {
        #region CONSTANTS

        //  command line flags
        private const string POWER = "p";
        private const string CAPABILITIES = "c";
        private const string REMOTE = "r";
        private const string API_TEST = "api";
        private const string REDUCED_API_TEST = "a";
        private const string OPT_HOST = "host";
        private const string OPT_PASS = "pass";
        private const string OPT_USER = "user";
        private const string OPT_VERBOSE = "verbose";

        //  Remote control commands
        const int RC_Reset = 10;
        const int RC_PowerUp = 2;
        const int RC_PowerDown = 8;
        const int RC_PowerCycleReset = 5;
        const int RC_SetBootOptions = 33; //0x21

        //Special commands
        const int NOP = 0; // 0x00
        const int ForcePxeBoot = 1; // 0x01
        const int ForceHardDriveBoot = 2; // 0x02
        const int ForceHardDriveSafeModeBoot = 3; // 0x03
        const int ForceDiagnosticsBoot = 4; // 0x04
        const int ForceCdOrDvdBoot = 5; // 0x05
        // 06h-0BFh Reserved for future ASF definition 
        const int IntelOemCommand = 193; // 0x0C1
        // 0C1h-0FFh Reserved FOR OEM 

        //Constants
        const int NO_VALUE = -1;
        const int PT_STATUS_SUCCESS = 0;
        const int PT_STATUS_NOT_SUPPORTED = 1;

        //OEM Parameters - Intel(R) AMT proprietary boot options
        const int UseSol = 1;

        //Reserved bits for checking
        //correctness of the Boot Options settings
        const int BootOptionsReservedBits = 1817;


        //Special Command Parameters:
        //Following boot options can be defined by using the bitwise OR operator. 
        //For example: 
        //unsigned short specialCommParam = UseIderCD | ReflashBios
        const int UseIderFloppy = 1;   // use floppy as IDER boot device
        const int ReflashBios = 4;   // 1 << 2
        const int BiosSetup = 8;  // 1 << 3
        const int BiosPause = 16; // 1 << 4
        const int UseIderCD = 257;// 1 | (1 << 8) use CD/DVD as IDER boot device

        // Standard boot options
        // Following boot options can be defined by using the bitwise OR operator. 
        // For example: 
        // unsigned short bootOptions = LockPowerButton | LockKeyboard | FirmwareVerbosityVerbose
        const int LockPowerButton = 2;	// 1 << 1
        const int LockResetButton = 4;	// 1 << 2
        const int LockKeyboard = 32;	// 1 << 5
        const int LockSleepButton = 64;	// 1 << 6
        const int UserPasswordBypass = 2048;	// 1 << 11 
        const int ForceProgressEvents = 4096;	// 1 << 12

        // only one from the Firmware verbosity options can be used
        const int FirmwareVerbositySystemDefault = 0;	 // system default
        const int FirmwareVerbosityQuiet = 8192;	 // 1 << 13 minimal screen activity
        const int FirmwareVerbosityVerbose = 16384; // 1 << 14 all messages appear on the screen
        const int FirmwareVerbosityScreen = 24576; // 3 << 13 blank, no messages appear on the screen.
        const int ConfigurationDataReset = 32768; // 1 << 14

        // Reserved bits for checking
        // correctness of the Special Parameters settings
        const int SpecialCommandParametersReservedBits = 65248;


        #endregion CONSTANTS

        #region Remote Control API Enumerations
        /// <summary>
        /// The Intel IANA Number is used in case the Remote Control caps IANA # reported does not work. This is
        /// a current workaround to the issue where some system's reported IANA number is rejected by the Intel(R) AMT
        /// SDK functions
        /// </summary>
        internal const uint INTEL_IANA_NUMBER = 0x157;

        /// <summary>
        /// Remote Control event special commands. For more information, see the Intel(R) AMT SDK Documentation.
        /// </summary>
        private enum SpecialCommand
        {
            NOP = 0x00,
            ForcePxeBoot = 0x01,
            ForceHardDriveBoot = 0x02,
            ForceHardDriveSafeModeBoot = 0x03,
            ForceDiagnosticsBoot = 0x04,
            ForceCdOrDvdBoot = 0x05,
            // 06h-0BFh Reserved for future ASF definition
            // C0h Reserved
            IntelOemCommand = 0x0C1
            // 0C2h-0FFh Reserved FOR OEM
        }

        /// <summary>
        /// Remote Control event special commands parameter type. For more information, see the 
        /// Intel(R) AMT SDK Documentation.
        /// </summary>
        [Flags]
        private enum SpecialCommandsParameterType : ushort
        {
            UseIder = 0x0001,
            ReflashBios = 0x0004,
            BiosSetup = 0x0008,
            BiosPause = 0x0010,
            IderBootDevice = 0x0100
        }

        /// <summary>
        /// Remote Control event OEM Parameter type. For more information, see the Intel(R) AMT SDK Documentation.
        /// </summary>
        [Flags]
        private enum OemParametersType
        {
            UseSol = 0x0001
        }
        #endregion

        #region (Private) struct RemoteControlCommandParameters
        /// <summary>
        /// Struct contained all pertinent information describing a single remote controle event
        /// </summary>
        private struct RemoteControlCommandParameters
        {
            public AmtSystem System;
            public AmtRemoteControlAction Action;
            public byte? SpecialCommand;
            public ushort? SpecialCommandParameter;
            public ushort? BootOptions;
            public ushort? OemCommands;
        }
        #endregion

        #region Static Constructor
        /// <summary>
        /// Private static constructor that gets executed whenever this class is 1st used. It just sets up the
        /// Event handlers
        /// </summary>
        static AmtRemoteControl()
        {
            PowerChangeStartingEvent += new PowerChangedEventHandler(AmtRemoteControl_PowerChangeStartingEvent);
            PowerChangeCompletedEvent += new PowerChangedEventHandler(AmtRemoteControl_PowerChangeCompletedEvent);
        }
        #endregion

        #region Public Events
        public delegate void PowerChangedEventHandler(PowerEventArgs e);

        /// <summary>
        /// This event is invoked whenever a remote control command is about to be executed on an Intel(R) AMT
        /// system.
        /// </summary>
        public static event PowerChangedEventHandler PowerChangeStartingEvent;

        /// <summary>
        /// This event is invoked whenever a remote control command has just completed execution on an Intel(R) AMT
        /// system.
        /// </summary>
        public static event PowerChangedEventHandler PowerChangeCompletedEvent;
        #endregion

        #region Event Handlers
        /// <summary>
        /// Executes whenever a remote control command has just completed
        /// </summary>
        /// <param name="e">Event arguments to describe the remote control event</param>
        static void AmtRemoteControl_PowerChangeCompletedEvent(PowerEventArgs e)
        {
        }

        /// <summary>
        /// Executes whenever a remote control command is about to be executed.
        /// </summary>
        /// <param name="e">Event arguments to describe the remote control event</param>
        static void AmtRemoteControl_PowerChangeStartingEvent(PowerEventArgs e)
        {
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Queries the specified Intel(R) AMT system to determine the IANA Number to be used for remote control
        /// commands
        /// </summary>
        /// <param name="system">The Intel(R) AMT System to query</param>
        /// <returns>The IANA # reported by the Intel(R) AMT system to use for remote control commands</returns>
        private static uint GetIanaOemNumber(AmtSystem system)
        {
            uint _IanaOemNumber;
            uint OemDefinedCapabilities;
            ushort SpecialCommandsSupported;
            byte SystemCapabilitiesSupported;
            uint SystemFirmwareCapabilities;
            PT_STATUS status;

            // TODO: WSMAN Support for GetRemoteControlCapabilities
            if (AmtInterfaceSettings.ApiInterface == ApiInterface.UseWSMAN)
            {
                return INTEL_IANA_NUMBER;
            }
            else
            {
                RemoteControlService rcs = system.RemoteControlService;
                status = (PT_STATUS)rcs.GetRemoteControlCapabilities(out _IanaOemNumber,
                    out OemDefinedCapabilities, out SpecialCommandsSupported, out SystemCapabilitiesSupported,
                    out SystemFirmwareCapabilities);
                if (status != PT_STATUS.SUCCESS)
                {
                    throw new AmtNetworkInterfaceException("Unable to get the system capabilities", status);
                }
            }

            return _IanaOemNumber;
        }

        /// <summary>
        /// Queries the specified Intel(R) AMT system to determine whether the system may be reset via an Intel(R) AMT remote
        /// control command.
        /// </summary>
        /// <param name="system">The Intel(R) AMT System to query</param>
        /// <returns>True if you can reset this system, False otherwise</returns>
        private static bool CanReset(AmtSystem system)
        {
            uint _IanaOemNumber;
            uint OemDefinedCapabilities;
            ushort SpecialCommandsSupported;
            byte SystemCapabilitiesSupported;
            uint SystemFirmwareCapabilities;
            PT_STATUS status;
            bool supportsReset = false;

            if (AmtInterfaceSettings.ApiInterface == ApiInterface.UseWSMAN)
            {
                ArrayList enumManagCapab = null;
                enumManagCapab = system.Wsman.Enumerate(typeof(CIM_PowerManagementCapabilitiesType));
                for (IEnumerator enumObj = enumManagCapab.GetEnumerator(); enumObj.MoveNext(); )
                {
                    CIM_PowerManagementCapabilitiesType cap = (CIM_PowerManagementCapabilitiesType) enumObj.Current;
                    foreach (ushort capv in cap.PowerChangeCapabilities)
                    {
                        if (capv == 0x03)
                        {
                            supportsReset = true;
                            break;
                        }
                    }
                }
            }
            else
            {
                RemoteControlService rcs = system.RemoteControlService;
                status = (PT_STATUS)rcs.GetRemoteControlCapabilities(out _IanaOemNumber,
                    out OemDefinedCapabilities, out SpecialCommandsSupported, out SystemCapabilitiesSupported,
                    out SystemFirmwareCapabilities);
                if (status != PT_STATUS.SUCCESS)
                {
                    throw new AmtNetworkInterfaceException("Unable to get the system capabilities", status);
                }
                supportsReset = Convert.ToBoolean(((SystemCapabilitiesSupported & (uint)0x08) >> 3));
            }


            return supportsReset;
        }

        /// <summary>
        /// Queries the specified Intel(R) AMT system to determine whether the system may be powered down via an
        /// Intel(R) AMT remote control command.
        /// </summary>
        /// <param name="system">The Intel(R) AMT System to query</param>
        /// <returns>True if you can power down this system, False otherwise</returns>
        private static bool CanPowerDown(AmtSystem system)
        {
            uint _IanaOemNumber;
            uint OemDefinedCapabilities;
            ushort SpecialCommandsSupported;
            byte SystemCapabilitiesSupported;
            uint SystemFirmwareCapabilities;
            PT_STATUS status;
            bool supportsPowerDown = false;

            if (AmtInterfaceSettings.ApiInterface == ApiInterface.UseWSMAN)
            {
                ArrayList enumManagCapab = null;
                enumManagCapab = system.Wsman.Enumerate(typeof(CIM_PowerManagementCapabilitiesType));
                for (IEnumerator enumObj = enumManagCapab.GetEnumerator(); enumObj.MoveNext(); )
                {
                    CIM_PowerManagementCapabilitiesType cap = (CIM_PowerManagementCapabilitiesType)enumObj.Current;
                    foreach (ushort capv in cap.PowerChangeCapabilities)
                    {
                        if (capv == 0x04)
                        {
                            supportsPowerDown = true;
                            break;
                        }
                    }
                }
            }
            else
            {
                RemoteControlService rcs = system.RemoteControlService;
                status = (PT_STATUS)rcs.GetRemoteControlCapabilities(out _IanaOemNumber,
                    out OemDefinedCapabilities, out SpecialCommandsSupported, out SystemCapabilitiesSupported,
                    out SystemFirmwareCapabilities);
                if (status != PT_STATUS.SUCCESS)
                {
                    throw new AmtNetworkInterfaceException("Unable to get the system capabilities", status);
                }

                supportsPowerDown = Convert.ToBoolean(((SystemCapabilitiesSupported & (uint)0x02) >> 1));
            }


            return supportsPowerDown;
        }

        /// <summary>
        /// Queries the specified Intel(R) AMT system to determine whether the system may be powered up (ON) via
        /// an Intel(R) AMT remote control command.
        /// </summary>
        /// <param name="system">The Intel(R) AMT System to query</param>
        /// <returns>True if you can power up this system, False otherwise</returns>
        private static bool CanPowerUp(AmtSystem system)
        {
            uint _IanaOemNumber;
            uint OemDefinedCapabilities;
            ushort SpecialCommandsSupported;
            byte SystemCapabilitiesSupported;
            uint SystemFirmwareCapabilities;
            PT_STATUS status;
            bool supportsPowerUp = false;

            if (AmtInterfaceSettings.ApiInterface == ApiInterface.UseWSMAN)
            {
                ArrayList enumManagCapab = null;
                enumManagCapab = system.Wsman.Enumerate(typeof(CIM_PowerManagementCapabilitiesType));
                for (IEnumerator enumObj = enumManagCapab.GetEnumerator(); enumObj.MoveNext(); )
                {
                    CIM_PowerManagementCapabilitiesType cap = (CIM_PowerManagementCapabilitiesType)enumObj.Current;
                    foreach (ushort capv in cap.PowerChangeCapabilities)
                    {
                        if (capv == 0x03)
                        {
                            supportsPowerUp = true;
                            break;
                        }
                    }
                }
            }
            else
            {
                RemoteControlService rcs = system.RemoteControlService;
                status = (PT_STATUS)rcs.GetRemoteControlCapabilities(out _IanaOemNumber,
                    out OemDefinedCapabilities, out SpecialCommandsSupported, out SystemCapabilitiesSupported,
                    out SystemFirmwareCapabilities);
                if (status != PT_STATUS.SUCCESS)
                {
                    throw new AmtNetworkInterfaceException("Unable to get the system capabilities", status);
                }
                supportsPowerUp = Convert.ToBoolean(((SystemCapabilitiesSupported & (uint)0x04) >> 2));
            }

            return supportsPowerUp;
        }

        private static PowerState ConvertPowerState(ushort state)
        {
            switch (state)
            {
                case 1: return PowerState.UNKNOWN;
                case 2: return PowerState.S0_POWERED;
                case 3: return PowerState.S1_SLEEPING_WITH_CONTEXT;
                case 4: return PowerState.S3_SLEEPING_WITH_MEMORY_CONTEXT_ONLY;
                case 5: return PowerState.S5_SOFT_OFF;
                case 6: return PowerState.MECHANICAL_OFF;
                case 7: return PowerState.S4_SUSPENDED_TO_DISK;
                case 8: return PowerState.S5_SOFT_OFF;
                case 9: return PowerState.MECHANICAL_OFF;
                default:
                    return PowerState.UNKNOWN;
            }
        }

        /// <summary>
        /// Retrieves the current power state of the Intel(R) AMT system.
        /// </summary>
        /// <param name="system">The Intel(R) AMT system to query.</param>
        /// <returns>The current power state of the Intel(R) AMT system.</returns>
        public static PowerState GetSystemPowerState(AmtSystem system, int MaxTry)
        {
           //Log.WriteLine(LogPriority.Trace, "Trying to get system power state", system.HostName); 

            if (MaxTry <= 0)
            {
                throw new AmtNetworkInterfaceException("Unable to get the system power state, exceeded max tries");
            }

            MaxTry--;

            uint status;
            uint systemPowerState;
            PowerState powerState; 

            if (AmtInterfaceSettings.ApiInterface == ApiInterface.UseWSMAN)
            {
                CIM_AssociatedPowerManagementServiceType r = (CIM_AssociatedPowerManagementServiceType)
                    system.Wsman.Get(typeof(CIM_AssociatedPowerManagementServiceType));

                ArrayList enumSystemPowerstate = null;
                enumSystemPowerstate = system.Wsman.Enumerate(typeof(CIM_AssociatedPowerManagementServiceType));
                //A null response will be never achieve from the Wsman.Enumerate
                powerState = PowerState.UNKNOWN;
                for (IEnumerator enumObj = enumSystemPowerstate.GetEnumerator(); enumObj.MoveNext(); )
                {
                    CIM_AssociatedPowerManagementServiceType cur = (CIM_AssociatedPowerManagementServiceType)enumObj.Current;
                    powerState = (PowerState) ConvertPowerState(cur.PowerState);
                    break;
                }
            }
            else
            {
                try
                {
                    status = system.RemoteControlService.GetSystemPowerState(out systemPowerState);
                    if (status != 0)
                    {
                        throw new AmtNetworkInterfaceException("Unable to get the system power state.", (PT_STATUS)status);
                    }

                    powerState = (PowerState)(systemPowerState & (uint)0x000F);

                }
                catch
                {
                    powerState = GetSystemPowerState(system, MaxTry);
                }
            }
            
            return powerState;
        }


        /// <summary>
        /// Get the requested EPR (End Point Reference) of the instance representing the requested special boot source
        /// </summary>
        private static void Get_req_BootSourceSettingEPR(AmtSystem system, int specialCommand, ref EndpointReferenceType req_BootSourceSettingEPR)
        {
            String BootSource_instanceID;
            switch (specialCommand)
            {
                case ForcePxeBoot:
                    BootSource_instanceID = "Intel(r) AMT: Force PXE Boot";
                    break;
                case ForceHardDriveBoot:
                    BootSource_instanceID = "Intel(r) AMT: Force Hard-drive Boot";
                    break;
                case ForceDiagnosticsBoot:
                    BootSource_instanceID = "Intel(r) AMT: Force Diagnostic Boot";
                    break;
                case ForceCdOrDvdBoot:
                    BootSource_instanceID = "Intel(r) AMT: Force CD/DVD Boot";
                    break;
                default:
                    BootSource_instanceID = "0";
                    break;
            }

            ArrayList enumBootSourceSetting = null;
            enumBootSourceSetting = system.Wsman.Enumerate(typeof(CIM_BootSourceSettingType));
            //A null response will be never achieve from the _WinRM.Enumerate

            for (IEnumerator enumObj = enumBootSourceSetting.GetEnumerator(); enumObj.MoveNext(); )
            {
                CIM_BootSourceSettingType cur = (CIM_BootSourceSettingType)enumObj.Current;
                if (BootSource_instanceID.Equals(cur.InstanceID))
                {
                    req_BootSourceSettingEPR = system.Wsman.GetEndpointReference(cur);
                    return;
                }
            }
        }

        /// <summary>
        /// initialize AMT_BootSettibgData with all the user requests using WinRM.Put
        /// </summary>
        private static void Put_BootSettingData(AmtSystem system, int specialCommand, int oemParameter, int specialCommandParameters, int bootOptions)
        {
            AMT_BootCapabilitiesType BootCapabilitiesObj = new AMT_BootCapabilitiesType();
            BootCapabilitiesObj = (AMT_BootCapabilitiesType)system.Wsman.Get(typeof(AMT_BootCapabilitiesType));

            AMT_BootSettingDataType BootSettingDataObj = new AMT_BootSettingDataType();
            BootSettingDataObj = (AMT_BootSettingDataType)system.Wsman.Get(typeof(AMT_BootSettingDataType));

            BootSettingDataObj.UseIDER = false;
            BootSettingDataObj.IDERBootDevice = 0;
            BootSettingDataObj.BootMediaIndex = 0;

            if (specialCommand == ForceHardDriveSafeModeBoot)
            {
                if (!BootCapabilitiesObj.ForceHardDriveSafeModeBoot)
                    throw new Exception("\nForceHardDriveSafeModeBoot not supported");
                BootSettingDataObj.UseSafeMode = true;
            }
            else
                BootSettingDataObj.UseSafeMode = false;

            if (oemParameter == UseSol)
            {
                if (!BootCapabilitiesObj.SOL)
                    throw new Exception("\nSol not supported");
                BootSettingDataObj.UseSOL = true;
            }
            else
                BootSettingDataObj.UseSOL = false;

            if (specialCommandParameters == UseIderFloppy)
            {
                if (!BootCapabilitiesObj.IDER)
                    throw new Exception("\nIDER not supported");
                BootSettingDataObj.UseIDER = true;
                BootSettingDataObj.IDERBootDevice = 0;
            }
            else if (specialCommandParameters == UseIderCD)
            {
                if (!BootCapabilitiesObj.IDER)
                    throw new Exception("\nIDER not supported");
                BootSettingDataObj.UseIDER = true;
                BootSettingDataObj.IDERBootDevice = 1;
            }
            if ((specialCommandParameters & ReflashBios) == ReflashBios)
            {
                if (!BootCapabilitiesObj.BIOSReflash)
                    throw new Exception("\nBIOSReflash not supported");
                BootSettingDataObj.ReflashBIOS = true;
            }
            else
                BootSettingDataObj.ReflashBIOS = false;

            if ((specialCommandParameters & BiosPause) == BiosPause)
            {
                if (!BootCapabilitiesObj.BIOSPause)
                    throw new Exception("\nBIOSPause not supported");
                BootSettingDataObj.BIOSPause = true;
            }
            else
                BootSettingDataObj.BIOSPause = false;

            if ((specialCommandParameters & BiosSetup) == BiosSetup)
            {
                if (!BootCapabilitiesObj.BIOSSetup)
                    throw new Exception("\nBIOSSetup not supported");
                BootSettingDataObj.BIOSSetup = true;
            }
            else
                BootSettingDataObj.BIOSSetup = false;

            if ((bootOptions & LockPowerButton) == LockPowerButton)
            {
                if (!BootCapabilitiesObj.PowerButtonLock)
                    throw new Exception("\nLockPowerButton not supported");
                BootSettingDataObj.LockPowerButton = true;
            }
            else
                BootSettingDataObj.LockPowerButton = false;

            if ((bootOptions & LockResetButton) == LockResetButton)
            {
                if (!BootCapabilitiesObj.ResetButtonLock)
                    throw new Exception("\nLockResetButton not supported");
                BootSettingDataObj.LockResetButton = true;
            }
            else
                BootSettingDataObj.LockResetButton = false;

            if ((bootOptions & LockKeyboard) == LockKeyboard)
            {
                if (!BootCapabilitiesObj.KeyboardLock)
                    throw new Exception("\nLockKeyboard not supported");
                BootSettingDataObj.LockKeyboard = true;
            }
            else
                BootSettingDataObj.LockKeyboard = false;

            if ((bootOptions & LockSleepButton) == LockSleepButton)
            {
                if (!BootCapabilitiesObj.SleepButtonLock)
                    throw new Exception("\nLockSleepButton not supported");
                BootSettingDataObj.LockSleepButton = true;
            }
            else
                BootSettingDataObj.LockSleepButton = false;

            if ((bootOptions & UserPasswordBypass) == UserPasswordBypass)
            {
                if (!BootCapabilitiesObj.UserPasswordBypass)
                    throw new Exception("\nUserPasswordBypass not supported");
                BootSettingDataObj.UserPasswordBypass = true;
            }
            else
                BootSettingDataObj.UserPasswordBypass = false;

            if ((bootOptions & ForceProgressEvents) == ForceProgressEvents)
            {
                if (!BootCapabilitiesObj.ForcedProgressEvents)
                    throw new Exception("\nForceProgressEvents not supported");
                BootSettingDataObj.ForcedProgressEvents = true;
            }
            else
                BootSettingDataObj.ForcedProgressEvents = false;

            if ((bootOptions & ConfigurationDataReset) == ConfigurationDataReset)
            {
                if (!BootCapabilitiesObj.ConfigurationDataReset)
                    throw new Exception("\nConfigurationDataReset not supported");
                BootSettingDataObj.ConfigurationDataReset = true;
            }
            else
                BootSettingDataObj.ConfigurationDataReset = false;

            while (true)
            {
                if ((bootOptions & FirmwareVerbosityQuiet) == FirmwareVerbosityQuiet)
                {
                    if (!BootCapabilitiesObj.VerbosityQuiet)
                        throw new Exception("\nFirmwareVerbosityQuiet not supported");
                    BootSettingDataObj.FirmwareVerbosity = 1;
                    break;
                }
                if ((bootOptions & FirmwareVerbosityVerbose) == FirmwareVerbosityVerbose)
                {
                    if (!BootCapabilitiesObj.VerbosityVerbose)
                        throw new Exception("\nFirmwareVerbosityVerbose not supported");
                    BootSettingDataObj.FirmwareVerbosity = 2;
                    break;
                }
                if ((bootOptions & FirmwareVerbosityScreen) == FirmwareVerbosityScreen)
                {
                    if (!BootCapabilitiesObj.VerbosityScreenBlank)
                        throw new Exception("\nFirmwareVerbosityScreen not supported");
                    BootSettingDataObj.FirmwareVerbosity = 3;
                    break;
                }
                BootSettingDataObj.FirmwareVerbosity = 0;
                break;
            }

            system.Wsman.Put(BootSettingDataObj);
        }

        /// <summary>
        /// Get the requested EPR (End Point Reference) of CIM_BootConfifSetting
        /// </summary>
        private static void Get_BootConfigSettingEPR(AmtSystem system, ref EndpointReferenceType BootConfigSettingEPR)
        {
            CIM_BootConfigSettingType res =
                (CIM_BootConfigSettingType)system.Wsman.Get(typeof(CIM_BootConfigSettingType));
            BootConfigSettingEPR = system.Wsman.GetEndpointReference(res);
        }

        /// <summary>
        /// Get the requested EPR (End Point Reference) of CIM_ComputerSystem instance of the Host
        /// </summary>
        private static void Get_ComputerSystem_HostEPR(AmtSystem system, ref EndpointReferenceType ComputerSystem_HostEPR)
        {
            ArrayList enumComputerSystem = null;
            enumComputerSystem = system.Wsman.Enumerate(typeof(CIM_ComputerSystemType));
            for (IEnumerator enumObj = enumComputerSystem.GetEnumerator(); enumObj.MoveNext(); )
            {
                CIM_ComputerSystemType cur = (CIM_ComputerSystemType)enumObj.Current;
                if (cur.Name == "ManagedSystem") //looking for the Hosts instance
                {
                    ComputerSystem_HostEPR = system.Wsman.GetEndpointReference(cur);
                    return;
                }
            }
        }

        /// <summary>
        /// invoking the function: CIM_BootService.invokeSetBootConfigRole(input: EPR of BootSourceSetting, Role = 1)
        /// </summary>
        private static void Invok_SetBootConfigRole(AmtSystem system, EndpointReferenceType BootConfigSettingEPR)
        {
            CIM_BootServiceType.SetBootConfigRole_INPUT request = new CIM_BootServiceType.SetBootConfigRole_INPUT();
            request.BootConfigSetting = BootConfigSettingEPR;
            request.Role = 1;
            CIM_BootServiceType.SetBootConfigRole_OUTPUT response = (CIM_BootServiceType.SetBootConfigRole_OUTPUT)system.Wsman.Invoke(request);

            if (response.ReturnValue != PT_STATUS_SUCCESS)
            {
                if (response.ReturnValue == PT_STATUS_NOT_SUPPORTED)
                    Console.WriteLine("\nError: the requested Special command is not supported");
                throw new Exception("Failed to invoke SetBootConfigRole" +
                    " STATUS = " + response.ReturnValue);
            }
        }

        /// <summary>
        /// invoking the function: CIM_PowerManagementService.RequestPowerStateChange(input: EPR of CIM_ComputerSystem
        /// of the hostand command = the type of the requested boot)
        /// </summary>
        private static void Invok_PowerStateChange(AmtSystem system, AmtRemoteControlAction action, EndpointReferenceType ComputerSystem_HostEPR)
        {
            CIM_PowerManagementServiceType.RequestPowerStateChange_INPUT request = new CIM_PowerManagementServiceType.RequestPowerStateChange_INPUT();
            ushort command = 0;
            if (action == AmtRemoteControlAction.Reset) command = RC_Reset;
            if (action == AmtRemoteControlAction.PowerUp) command = RC_PowerUp;
            if (action == AmtRemoteControlAction.PowerDown) command = RC_PowerDown;
            request.PowerState = command;
            request.PowerStateSpecified = true;
            request.ManagedElement = ComputerSystem_HostEPR;
            request.ManagedElementSpecified = true;
            CIM_PowerManagementServiceType.RequestPowerStateChange_OUTPUT response = (CIM_PowerManagementServiceType.RequestPowerStateChange_OUTPUT)system.Wsman.Invoke(request);

            if (response.ReturnValue != PT_STATUS_SUCCESS)
            {
                if (response.ReturnValue == PT_STATUS_NOT_SUPPORTED)
                    Console.WriteLine("\nError: the requested command is not supported");
                throw new Exception("Failed to invoke PowerStateChange" +
                    " STATUS = " + response.ReturnValue);
            }
        }

        /// <summary>
        /// Helper method to send the low level remote control command to the Intel(R) AMT client. Handles known errors
        /// and exceptions and invokes the corresponding events at the appropriate times.
        /// </summary>
        /// <param name="system">The AmtSystem to send the remote control command to</param>
        /// <param name="action">The remote control command to send</param>
        /// <param name="specialCommand">Special command for remote control, or null for none</param>
        /// <param name="specialCommandParameter">Special command parameter for remote control, or null for none</param>
        /// <param name="bootOptions">Boot options for remote control, or null for none</param>
        /// <param name="OemCommands">OEM Commands for remote control, or null for none</param>
        private static void SendRemoteControlCommand(AmtSystem system, AmtRemoteControlAction action,
            byte? specialCommand, ushort? specialCommandParameter, ushort? bootOptions, ushort? OemCommands)
        {
           /*Log.Print(LogPriority.Trace, null, Audience.Internal, 
                "ENTER SendRemoteControlCommand({0},{1},{2},{3},{4},{5})",
                system.HostName, action.ToString(), specialCommand.ToString(), specialCommandParameter.ToString(),
                bootOptions.ToString(), OemCommands.ToString());
            */
            PowerChangeStartingEvent.Invoke(new PowerEventArgs(system, action));
            bool Result = true;

            // TODO: WS-MAN support for SendRemoteControlCommand
            if (AmtInterfaceSettings.ApiInterface == ApiInterface.UseWSMAN)
            {
                EndpointReferenceType req_BootSourceSettingEPR = new EndpointReferenceType();
                if (action != AmtRemoteControlAction.PowerDown)
                {
                    Put_BootSettingData(system,
                        (int) (specialCommand.HasValue ? specialCommand.Value : 0),
                        (int) (OemCommands.HasValue ? OemCommands.Value : 0),
                        (int) (specialCommandParameter.HasValue ? specialCommandParameter.Value : 0),
                        (int) (bootOptions.HasValue ? bootOptions.Value : 0));
                }

                EndpointReferenceType BootConfigSettingEPR = new EndpointReferenceType();
                Get_BootConfigSettingEPR(system, ref BootConfigSettingEPR);
                Invok_SetBootConfigRole(system, BootConfigSettingEPR);

                EndpointReferenceType ComputerSystem_HostEPR = new EndpointReferenceType();
                Get_ComputerSystem_HostEPR(system, ref ComputerSystem_HostEPR);
                Invok_PowerStateChange(system, action, ComputerSystem_HostEPR);
            }
            else
            {
                try
                {
                    PT_STATUS status;
                    status = (PT_STATUS)system.RemoteControlService.RemoteControl(
                        (byte)action,
                        AmtRemoteControl.GetIanaOemNumber(system),
                        specialCommand.HasValue ? specialCommand.Value : (byte)0,
                        specialCommand.HasValue,
                        specialCommandParameter.HasValue ? specialCommandParameter.Value : (ushort)0,
                        specialCommandParameter.HasValue,
                        bootOptions.HasValue ? bootOptions.Value : (ushort)0,
                        bootOptions.HasValue,
                        OemCommands.HasValue ? OemCommands.Value : (ushort)0,
                        OemCommands.HasValue);
                    if (status == PT_STATUS.UNSUPPORTED_OEM_NUMBER)
                    {
                        // Some systems appear to return an invalid IanaOemNumber, preventing the RemoteControl
                        // command from successfully executing. So, force the Intel IANA number, 343 (0x157).
                        status = (PT_STATUS)system.RemoteControlService.RemoteControl(
                            (byte)action,
                            INTEL_IANA_NUMBER,
                            specialCommand.HasValue ? specialCommand.Value : (byte)0,
                            specialCommand.HasValue,
                            specialCommandParameter.HasValue ? specialCommandParameter.Value : (ushort)0,
                            specialCommandParameter.HasValue,
                            bootOptions.HasValue ? bootOptions.Value : (ushort)0,
                            bootOptions.HasValue,
                            OemCommands.HasValue ? OemCommands.Value : (ushort)0,
                            OemCommands.HasValue);
                    }
                    if (status != PT_STATUS.SUCCESS)
                    {
                        Result = false;
                        throw new AmtNetworkInterfaceException("Unable to change the system power state.", status);
                    }
                }
                finally
                {
                    PowerChangeCompletedEvent.Invoke(new PowerEventArgs(system, action, Result));
                }
            }
        }
        #endregion

        #region Public Methods

        #region Reset Methods
        /// <summary>
        /// Immediately resets the specified Intel(R) AMT system remotely, setting the appropriate IDER flag. This
        /// method returns once the reset is completed.
        /// </summary>
        /// <param name="system">The Intel(R) AMT system to reset</param>
        /// <param name="useIder">true if the reset should enable IDER, false otherwise</param>
        public static void Reset(AmtSystem system, bool useIder)
        {
            try
            {
               //Log.WriteLine(LogPriority.Trace, "ENTERED Reset", "AMTLib", Audience.Internal);

                if (AmtRemoteControl.CanReset(system) == true)
                {
                    if (useIder == true)
                        SendRemoteControlCommand(
                            system,
                            AmtRemoteControlAction.Reset,
                            (byte)SpecialCommand.IntelOemCommand,
                            (ushort)(SpecialCommandsParameterType.UseIder | SpecialCommandsParameterType.IderBootDevice),
                            null,
                            (ushort)OemParametersType.UseSol);
                    else
                        SendRemoteControlCommand(system, AmtRemoteControlAction.Reset, null, null, null, null);
                }
                else
                {
                    throw new AmtNetworkInterfaceException("System cannot be reset.");
                }
            }
            catch (Exception e)
            {
                string errmsg = e.Message.ToString();
               //Log.WriteLine(LogPriority.Error, e.Message, "AMTLib", Audience.Internal);
                throw;
            }
        }

        /// <summary>
        /// Immediately resets the specified Intel(R) AMT system remotely, and returns once the reset is completed.
        /// </summary>
        /// <param name="system">The Intel(R) AMT system to reset</param>
        public static void Reset(AmtSystem system)
        {
            Reset(system, false);
        }

        /// <summary>
        /// Begins a remote reset of the specified Intel(R) AMT system, and immediately returns. The caller should use
        /// the PowerChangeCompletedEvent for notification once the reset has completed.
        /// </summary>
        /// <param name="system">The Intel(R) AMT system to reset</param>
        public static void BeginReset(AmtSystem system)
        {
            PowerChangeStartingEvent.Invoke(new PowerEventArgs(system, AmtRemoteControlAction.Reset));
            throw new NotImplementedException();
        }

        /// <summary>
        /// Ends a remote reset command for the specified Intel(R) AMT system.
        /// </summary>
        /// <param name="system">The Intel(R) AMT system to reset</param>
        public static void EndReset(AmtSystem system)
        {
            PowerChangeCompletedEvent.Invoke(new PowerEventArgs(system, AmtRemoteControlAction.Reset));
            throw new NotImplementedException();
        }
        #endregion

        #region PowerDown Methods
        /// <summary>
        /// Immediately powers down the specified Intel(R) AMT system. This method returns once the power down
        /// is completed.
        /// </summary>
        /// <param name="system">The Intel(R) AMT system to power down</param>
        public static void PowerDown(AmtSystem system)
        {
           //Log.WriteLine(LogPriority.Trace, "ENTERED PowerDown", "AMTLib", Audience.Internal);

            try
            {
                if (AmtRemoteControl.CanPowerDown(system) == true)
                {
                    SendRemoteControlCommand(system, AmtRemoteControlAction.PowerDown, null, null, null, null);
                }
                else
                {
                    throw new AmtNetworkInterfaceException("Cannot power down system.");
                }
            }
            catch (Exception e)
            {
                string errmsg = e.Message.ToString();
               //Log.WriteLine(LogPriority.Error, e.Message, "AMTLib", Audience.All);
                throw;
            }
        }
        #endregion

        #region PowerUp Methods
        /// <summary>
        /// Immediately powers up the specified Intel(R) AMT system, optionally enabled IDER. This method returns once
        /// the power up is completed.
        /// </summary>
        /// <param name="system">The Intel(R) AMT system to power up</param>
        /// <param name="useIder">true if the reset should enable IDER, false otherwise</param>
        public static void PowerUp(AmtSystem system, bool useIder)
        {
           //Log.WriteLine(LogPriority.Trace, "ENTERED PowerUp", "AMTLib", Audience.Internal);

            try
            {
                if (AmtRemoteControl.CanPowerUp(system) == true)
                {
                    if(useIder == true)
                        SendRemoteControlCommand(
                            system,
                            AmtRemoteControlAction.PowerUp, 
                            (byte) SpecialCommand.IntelOemCommand,
                            (ushort) (SpecialCommandsParameterType.UseIder | SpecialCommandsParameterType.IderBootDevice),
                            null,
                            (ushort) OemParametersType.UseSol);
                    else
                        SendRemoteControlCommand(system, AmtRemoteControlAction.PowerUp, null, null, null, null);
                }
                else
                {
                    throw new AmtNetworkInterfaceException("System power up not supported.");
                }
            }
            catch (Exception e)
            {
                string errmsg = e.Message.ToString();
               //Log.WriteLine(LogPriority.Error, e.Message, "AMTLib", Audience.Internal);
                throw;
            }
        }

        /// <summary>
        /// Immediately powers up the specified Intel(R) AMT system. This method returns once the power up is completed.
        /// </summary>
        /// <param name="system">The Intel(R) AMT system to power up</param>
        public static void PowerUp(AmtSystem system)
        {
            PowerUp(system, false);
        }

        /// <summary>
        /// Begins a remote power up command of the specified Intel(R) AMT system, and immediately returns.
        /// </summary>
        /// <param name="system">The Intel(R) AMT system to power up</param>
        /// <returns>AmtBackgroundJob object representing the pending job</returns>
        public static AmtBackgroundJob BeginPowerUp(AmtSystem system)
        {
            BackgroundWorker worker = new BackgroundWorker();
            worker.WorkerSupportsCancellation = false;
            worker.WorkerReportsProgress = false;
            worker.DoWork += new DoWorkEventHandler(RemoteControlThread_DoWork);
            worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(RemoteControlThread_RunWorkerCompleted);

            RemoteControlCommandParameters parameters = new RemoteControlCommandParameters();
            parameters.System = system;
            parameters.Action = AmtRemoteControlAction.PowerUp;
            parameters.SpecialCommand = null;
            parameters.SpecialCommandParameter = null;
            parameters.BootOptions = null;
            parameters.OemCommands = null;
            worker.RunWorkerAsync(parameters);

            return new AmtBackgroundJob(system);
        }

        /// <summary>
        /// Ends a remote power up command of the specified Intel(R) AMT system
        /// </summary>
        /// <param name="powerJob">AmtBackgroundJob object representing the pending job to end</param>
        public static void EndPowerUp(AmtBackgroundJob powerJob)
        {
            throw new ApplicationException("Not allowed.");
        }
        #endregion

        #region Asynchronous Worker methods
        /// <summary>
        /// Performs the actual work of executing the remote control command. The EventArgs contains a pointer
        /// to an object that contains all the information about the remote control command to execute.
        /// </summary>
        /// <param name="sender">Standard sender object</param>
        /// <param name="e">Standard event arguments</param>
        private static void RemoteControlThread_DoWork(object sender, DoWorkEventArgs e)
        {
            RemoteControlCommandParameters parameters = (RemoteControlCommandParameters)e.Argument;
            SendRemoteControlCommand(parameters.System,
                                     parameters.Action,
                                     parameters.SpecialCommand,
                                     parameters.SpecialCommandParameter,
                                     parameters.BootOptions,
                                     parameters.OemCommands);
        }

        /// <summary>
        /// Executes when the remote control operation has completed.
        /// </summary>
        /// <param name="sender">Standard sender object</param>
        /// <param name="e">Standard event arguments</param>
        private static void RemoteControlThread_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            // Do nothing
        }
        #endregion

        #endregion
    }
}
