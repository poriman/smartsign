﻿// -----------------------------------------------------------------------------
//
// Copyright (c) 2009 Intel Corporation. All Rights Reserved.
//
// The source code contained or described herein and all documents related to 
// the source code ("Material") are owned by Intel Corporation or its suppliers 
// or licensors. Title to the Material remains with Intel Corporation or its 
// suppliers and licensors. The Material may contain trade secrets and 
// proprietary and confidential information of Intel Corporation and its 
// suppliers and licensors, and is protected by worldwide copyright and trade 
// secret laws and treaty provisions. No part of the Material may be used, 
// copied, reproduced, modified, published, uploaded, posted, transmitted, 
// distributed, or disclosed in any way without Intel’s prior express written 
// permission. 
//
// No license under any patent, copyright, trade secret or other intellectual 
// property right is granted to or conferred upon you by disclosure or delivery 
// of the Materials, either expressly, by implication, inducement, estoppel or 
// otherwise. Any license under such intellectual property rights must be 
// express and approved by Intel in writing.
//
// * Third Party trademarks are the property of their respective owners.
//
// Unless otherwise agreed by Intel in writing, you may not remove or alter this 
// notice or any other notice embedded in Materials by Intel or Intel’s 
// suppliers or licensors in any way.
//
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace Intel.vPro.AMT
{
    public class AmtRedirectionException : ApplicationException
    {
        #region Private Fields
        private IMRResult _ResultCode = IMRResult.IMR_RES_OK;
        private string _ErrorString;
        private string _UserMessage;
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a redirection exception that is not based on an IMRRESULT error code
        /// </summary>
        /// <param name="msg">user-defined message describing the exception</param>
        public AmtRedirectionException(string msg) : base(msg)
        {
            this.Data.Add("Message", msg);
        }

        /// <summary>
        /// Creates a redirection exception based solely off of an IMRRESULT error code. Only available
        /// as an internal method since the SDK calls are not exposed outside of this DLL.
        /// </summary>
        /// <param name="resultCode">IMRRESULT code to wrap</param>
        internal AmtRedirectionException(IMRResult resultCode)
        {
            Debug.Assert((resultCode != IMRResult.IMR_RES_OK),
                "Redirection exception is invalid with Success as error code");

            _ResultCode = resultCode;
            _ErrorString = GetErrorString(_ResultCode);
            this.Data.Add("IMRResult", resultCode);
        }

        /// <summary>
        /// Creates a redirection exception based off of an IMRRESULT error code and also a user-defined
        /// message. Only available as an internal method since the SDK calls are not exposed outside
        /// of this DLL.
        /// </summary>
        /// <param name="resultCode">IMRRESULT code to wrap</param>
        /// <param name="msg">user-defined message to describe the exception event cause</param>
        internal AmtRedirectionException(IMRResult resultCode, string msg)
        {
            Debug.Assert((resultCode != IMRResult.IMR_RES_OK),
                "Redirection exception is invalid with Success as error code");

            _ResultCode = resultCode;
            _ErrorString = GetErrorString(_ResultCode);
            _UserMessage = msg;
            this.Data.Add("IMRResult", resultCode);
            this.Data.Add("Error String", _ErrorString);
            this.Data.Add("Message", msg);
        }
        #endregion

        #region Private methods
        /// <summary>
        /// returns the error string associated with a specified error code
        /// </summary>
        /// <param name="error">IMRResult error code to lookup</param>
        /// <returns>string description of IMRResult error code</returns>
        private string GetErrorString(IMRResult error)
        {
            int len = 0;
            ImrSdkWrapper.IMR_GetErrorStringLen(error, out len);
            IntPtr str = Marshal.AllocHGlobal(len);
            ImrSdkWrapper.IMR_GetErrorString(error, str);
            string errstr = Marshal.PtrToStringAnsi(str, len - 1);
            Marshal.FreeHGlobal(str);
            return errstr;
        }
        #endregion

        #region ToString() override
        /// <summary>
        /// Overridden ToString() method to display all the relevant info that was used to create the exception,
        /// including error code, error string, and user-defined message.
        /// </summary>
        /// <returns>formatted string with exception information</returns>
        public override string ToString()
        {
            if (_ResultCode == IMRResult.IMR_RES_OK)
            {
                // Return the standard formatted message
                return this.Message;
            }
            else
            {
                if (_UserMessage == null)
                {
                    // 2 parts to the message.  Error code, plus error string
                    return "(" + _ResultCode.ToString() + ") " + _ErrorString;
                }
                else
                {
                    // 3 parts to the message.  User-defined message, Plus error code, plus error string
                    return _UserMessage + " -> (" + _ResultCode.ToString() + ") " + _ErrorString;
                }
            }
        }
        #endregion
    }
}
