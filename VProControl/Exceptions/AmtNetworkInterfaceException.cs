﻿// -----------------------------------------------------------------------------
//
// Copyright (c) 2009 Intel Corporation. All Rights Reserved.
//
// The source code contained or described herein and all documents related to 
// the source code ("Material") are owned by Intel Corporation or its suppliers 
// or licensors. Title to the Material remains with Intel Corporation or its 
// suppliers and licensors. The Material may contain trade secrets and 
// proprietary and confidential information of Intel Corporation and its 
// suppliers and licensors, and is protected by worldwide copyright and trade 
// secret laws and treaty provisions. No part of the Material may be used, 
// copied, reproduced, modified, published, uploaded, posted, transmitted, 
// distributed, or disclosed in any way without Intel’s prior express written 
// permission. 
//
// No license under any patent, copyright, trade secret or other intellectual 
// property right is granted to or conferred upon you by disclosure or delivery 
// of the Materials, either expressly, by implication, inducement, estoppel or 
// otherwise. Any license under such intellectual property rights must be 
// express and approved by Intel in writing.
//
// * Third Party trademarks are the property of their respective owners.
//
// Unless otherwise agreed by Intel in writing, you may not remove or alter this 
// notice or any other notice embedded in Materials by Intel or Intel’s 
// suppliers or licensors in any way.
//
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

namespace Intel.vPro.AMT
{
    public class AmtNetworkInterfaceException : ApplicationException
    {
        #region Private Fields
        private PT_STATUS _ResultCode = PT_STATUS.SUCCESS;
        #endregion

        /// <summary>
        /// Creates a network interface exception that is not based on a PT_STATUS code
        /// </summary>
        /// <param name="msg">user-defined message describing the exception</param>
        public AmtNetworkInterfaceException(string msg)
            : base(msg)
        {
            this.Data.Add("Message", msg);
        }

        /// <summary>
        /// Creates a Network interface exception based solely off of a PT_STATUS error code. Only available
        /// as an internal method since the SDK calls are not exposed outside of this DLL.
        /// </summary>
        /// <param name="code">PT_STATUS code to wrap</param>
        internal AmtNetworkInterfaceException(PT_STATUS code)
        {
            Debug.Assert((code != PT_STATUS.SUCCESS),
                "Network interface exception is invalid with Success as error code");

            _ResultCode = code;
            this.Data.Add("IMRResult", code);
        }


        /// <summary>
        /// Creates a network interface exception based off of an PT_STATUS error code and also a user-defined
        /// message. Only available as an internal method since the SDK calls are not exposed outside
        /// of this DLL.
        /// </summary>
        /// <param name="resultCode">PT_STATUS code to wrap</param>
        /// <param name="msg">user-defined message to describe the exception event cause</param>
        internal AmtNetworkInterfaceException(string msg, PT_STATUS code)
            : base(msg)
        {
            Debug.Assert((code != PT_STATUS.SUCCESS),
                "Redirection exception is invalid with Success as error code");

            _ResultCode = code;
            this.Data.Add("Message", msg);
        }
    }
}
