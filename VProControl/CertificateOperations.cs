// -----------------------------------------------------------------------------
//
// Copyright (c) 2009 Intel Corporation. All Rights Reserved.
//
// The source code contained or described herein and all documents related to 
// the source code ("Material") are owned by Intel Corporation or its suppliers 
// or licensors. Title to the Material remains with Intel Corporation or its 
// suppliers and licensors. The Material may contain trade secrets and 
// proprietary and confidential information of Intel Corporation and its 
// suppliers and licensors, and is protected by worldwide copyright and trade 
// secret laws and treaty provisions. No part of the Material may be used, 
// copied, reproduced, modified, published, uploaded, posted, transmitted, 
// distributed, or disclosed in any way without Intel’s prior express written 
// permission. 
//
// No license under any patent, copyright, trade secret or other intellectual 
// property right is granted to or conferred upon you by disclosure or delivery 
// of the Materials, either expressly, by implication, inducement, estoppel or 
// otherwise. Any license under such intellectual property rights must be 
// express and approved by Intel in writing.
//
// * Third Party trademarks are the property of their respective owners.
//
// Unless otherwise agreed by Intel in writing, you may not remove or alter this 
// notice or any other notice embedded in Materials by Intel or Intel’s 
// suppliers or licensors in any way.
//
// -----------------------------------------------------------------------------

using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Diagnostics;

namespace Intel.vPro.AMT
{
    /// <summary>
    /// temp class to enable limited certificate operations
    /// </summary>
    internal class CertificateOperations
    {
        #region Private Fields
        private static object TrustedRootLockObj = new object();
        #endregion

        #region Public Methods
        /// <summary>
        /// Return a string containing all of the trusted root certificates in PEM format.
        /// </summary>
        /// <returns></returns>
        private static string AllTrustedRootsToPEM()
        {
            StringBuilder AllTrustedCerts = new StringBuilder();
            StoreName[] storeNames = { StoreName.My, StoreName.Root, StoreName.AuthRoot, StoreName.CertificateAuthority };
            StoreLocation[] locations = { StoreLocation.CurrentUser, StoreLocation.LocalMachine };

            foreach (StoreName store in storeNames)
            {
                foreach (StoreLocation location in locations)
                {
                    X509Store CertificateStore = new X509Store(store, location);
                    CertificateStore.Open(OpenFlags.ReadOnly);
                   
                    foreach (X509Certificate2 cert in CertificateStore.Certificates)
                    {
                        if (CertificateOperations.IsTrustedCert(cert) &&
                            CertificateOperations.IsCaCertificate(cert))
                        {
                            AllTrustedCerts.Append("\r\n").Append(GetCertificateString(cert.Subject)).Append("\r\n");
                            string b1 = Convert.ToBase64String(cert.Export(X509ContentType.Cert));
                            StringBuilder b2 = new StringBuilder();
                            while (b1.Length > 64)
                            {
                                b2.Append(b1.Substring(0, 64)).Append("\r\n");
                                b1 = b1.Substring(64);
                            }
                            b2.Append(b1).Append("\r\n");
                            AllTrustedCerts.Append("-----BEGIN CERTIFICATE-----\r\n").Append(b2.ToString()).Append("-----END CERTIFICATE-----\r\n");
                        }
                    }
                    CertificateStore.Close();
                }
                
            }



            return AllTrustedCerts.ToString();
        }

        internal static bool CertAndKeyChainToPemFile(X509Certificate2 certificate, string filename, string password)
        {
            try
            {
                string pem = CertAndKeyChainToPem(certificate, password);
                if (pem == null) return false;

                FileStream fs = new FileStream(filename, FileMode.Create);
                byte[] bytes = UTF8Encoding.UTF8.GetBytes(pem);
                fs.Write(bytes, 0, bytes.Length);
                fs.Close();
            }
            catch (Exception e)
            {
                throw new CryptographicException("Error converting certificate to PEM file.", e);
            }

            return true;
        }

        public static string CertToPem(X509Certificate2 certificate)
        {
            string b1 = Convert.ToBase64String(certificate.Export(X509ContentType.Cert));
            StringBuilder b2 = new StringBuilder();
            b2.Append("-----BEGIN CERTIFICATE-----\r\n");
            while (b1.Length > 64)
            {
                b2.Append(b1.Substring(0, 64)).Append("\r\n");
                b1 = b1.Substring(64);
            }
            b2.Append(b1).Append("\r\n-----END CERTIFICATE-----\r\n");
            return b2.ToString();
        }

        public static string CertAndKeyChainToPem(X509Certificate2 certificate, string password)
        {
            // Store the private cert
            string certpem = CertAndKeyToPem(certificate, password);

            // Store additional certs
            X509Chain c = new X509Chain();
            c.Build(certificate);
            if (c.ChainElements.Count > 1)
            {
                for (int i = 1; i < c.ChainElements.Count; i++)
                {
                    certpem += CertToPem(c.ChainElements[i].Certificate);
                }
            }

            return certpem;
        }

        public static string CertAndKeyToPem(X509Certificate2 certificate, string password)
        {
            string pemFileName = Path.GetTempFileName();
            string p12FileName = Path.GetTempFileName();

            // Save the certificate private key to file
            byte[] bytes = certificate.Export(X509ContentType.Pkcs12, "PASS");
            FileStream fs = new FileStream(p12FileName, FileMode.Create);
            fs.Write(bytes, 0, bytes.Length);
            fs.Close();

            // Convert the certificate to a PEM file
            System.Diagnostics.Process openSslProc;
            if (password == null)
            {
                openSslProc = Run("openssl", 
                    "pkcs12 -in \"" + p12FileName + "\" -out \"" + pemFileName + "\" -passin pass:PASS -nodes");
            }
            else
            {
                openSslProc = Run("openssl",
                    "pkcs12 -in \"" + p12FileName + "\" -out \"" + pemFileName + "\" -passin pass:PASS -passout pass:" + password);
            }

            openSslProc.WaitForExit();
            if (openSslProc.ExitCode != 0)
                throw new ApplicationException("OpenSSL call failed with exit code " + openSslProc.ExitCode.ToString());

            fs = new FileStream(pemFileName, FileMode.Open);
            bytes = new byte[fs.Length];
            fs.Read(bytes, 0, (int)(fs.Length));
            fs.Close();
            string pem = UTF8Encoding.UTF8.GetString(bytes);

            pem = pem.Replace("\n", "\r\n");

            File.Delete(p12FileName);
            File.Delete(pemFileName);

            return pem;
        }

        /// <summary>
        /// Save a list of all trusted root certificates to a PEM file.
        /// </summary>
        /// <param name="filename"></param>
        /// <returns>boolean success or failure</returns>
        internal static bool AllTrustedRootsToPemFile(string filename)
        {
            // Save trusted certs
            byte[] bytes = UTF8Encoding.UTF8.GetBytes(AllTrustedRootsToPEM());
            FileStream fs = new FileStream(filename, FileMode.Create);
            fs.Write(bytes, 0, bytes.Length);
            fs.Close();

            return true;
        }

        internal static bool IsTrustedCert(X509Certificate2 cert)
        {
            X509Chain chain = new X509Chain();
            chain.ChainPolicy.RevocationMode = X509RevocationMode.NoCheck;
            return (chain.Build(cert) == true && chain.ChainStatus.Length == 0);
        }

        internal static bool IsCaCertificate(X509Certificate2 cert)
        {
            // TODO - Not sure how to do this, but want to check for CA extensions.
            if (cert.Extensions.Count == 0) return true;
            foreach (X509Extension ex in cert.Extensions)
            {
                if (ex.GetType() == typeof(X509BasicConstraintsExtension))
                {
                    X509BasicConstraintsExtension ex2 = (X509BasicConstraintsExtension)ex;
                    return ex2.CertificateAuthority;
                }
            }
            return false;
        }
        #endregion

        #region Private Methods
        private static Process Run(string fileName, string args)
        {
            ProcessStartInfo info = new ProcessStartInfo(fileName, args);
            info.CreateNoWindow = true;
            info.UseShellExecute = false;
            Process processChild = Process.Start(info);
            return processChild;
        }

        private static string GetCertificateString(string certDataStr)
        {
            string s = "";

            int i = certDataStr.IndexOf("CN=");
            if (i >= 0)
            {
                s = certDataStr.Substring(i + 3);
                i = s.IndexOf(",");
                if (i > 0) s = s.Substring(0, i);
                return s;
            }

            i = certDataStr.IndexOf("O=");
            if (i >= 0)
            {
                s = certDataStr.Substring(i + 2);
                if (s[0] == '\"')
                {
                    s = s.Substring(1);
                    int j = s.IndexOf("\"");
                    if (j >= 0)
                    {
                        s = s.Substring(0, j);
                    }
                }
                else
                {
                    i = s.IndexOf(",");
                    if (i > 0) s = s.Substring(0, i);
                }
                return s;
            }

            return "Unknown";
        }
        #endregion
    }
}
