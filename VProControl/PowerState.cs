// -----------------------------------------------------------------------------
//
// Copyright (c) 2009 Intel Corporation. All Rights Reserved.
//
// The source code contained or described herein and all documents related to 
// the source code ("Material") are owned by Intel Corporation or its suppliers 
// or licensors. Title to the Material remains with Intel Corporation or its 
// suppliers and licensors. The Material may contain trade secrets and 
// proprietary and confidential information of Intel Corporation and its 
// suppliers and licensors, and is protected by worldwide copyright and trade 
// secret laws and treaty provisions. No part of the Material may be used, 
// copied, reproduced, modified, published, uploaded, posted, transmitted, 
// distributed, or disclosed in any way without Intel’s prior express written 
// permission. 
//
// No license under any patent, copyright, trade secret or other intellectual 
// property right is granted to or conferred upon you by disclosure or delivery 
// of the Materials, either expressly, by implication, inducement, estoppel or 
// otherwise. Any license under such intellectual property rights must be 
// express and approved by Intel in writing.
//
// * Third Party trademarks are the property of their respective owners.
//
// Unless otherwise agreed by Intel in writing, you may not remove or alter this 
// notice or any other notice embedded in Materials by Intel or Intel’s 
// suppliers or licensors in any way.
//
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;

namespace Intel.vPro.AMT
{
    /// <summary>
    /// Represents the current power state of a computer
    /// </summary>
    public enum PowerState
    {
        S0_POWERED = 0,
        S1_SLEEPING_WITH_CONTEXT = 1,
        S2_SLEEPING_WITHOUT_CONTEXT = 2,
        S3_SLEEPING_WITH_MEMORY_CONTEXT_ONLY = 3,
        S4_SUSPENDED_TO_DISK = 4,
        S5_SOFT_OFF = 5,
        S4_OR_S5 = 6,
        MECHANICAL_OFF = 7,
        S1_OR_S2_OR_S3 = 8,
        S1_OR_S2_OR_S3_OR_S4 = 9,
        S5_OVERRIDE = 10,
        LEGACY_ON = 11,
        LEGACY_OFF = 12,
        UNKNOWN13 = 13,
        UNKNOWN14 = 14,
        UNKNOWN15 = 15,
        ERROR_GETTING_STATE = 16,
        UNKNOWN = 254,
        DISCONNECTED = 255,
    }
}
