﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Globalization;
using RemoteControl;

namespace Intel.vPro.AMT
{
    public class CVProCtrl
    {
        public CVProCtrl()
        {
            AmtInterfaceSettings.ApiInterface = ApiInterface.UseWSMAN;
        }
        /*
         * 사용 포트 : 16992
         *     포트 : 16993 현재는 사용하지 않음(보안 접속시 사용)
         * 
         * */

        public RemoteControl.PowerState GetPowerStateWSMAN(string hostname, string AmtUser, string AmtPass)
        {
            try
            {
                RemoteControlApi api = new RemoteControlApi(hostname, AmtUser, AmtPass, false, null);
                return api.GetSystemPowerState();

            }
            catch (Exception e)
            {
                string errmsg = e.Message.ToString();
            }
            return RemoteControl.PowerState.Other;
        }

        private PowerState ConvertPowerState(RemoteControl.PowerState ps)
        {
            switch (ps)
            {
                case RemoteControl.PowerState.DeepSleep:
                    return PowerState.S1_SLEEPING_WITH_CONTEXT;
                case RemoteControl.PowerState.DiagnosticInterrupt:
                    return PowerState.UNKNOWN;
                case RemoteControl.PowerState.GracefulOff:
                    return PowerState.S5_SOFT_OFF;
                case RemoteControl.PowerState.GracefulReset:
                    return PowerState.S5_SOFT_OFF;
                case RemoteControl.PowerState.HardOff:
                    return PowerState.LEGACY_OFF;
                case RemoteControl.PowerState.HardPowerCycle:
                    return PowerState.UNKNOWN;
                case RemoteControl.PowerState.Hibernate:
                    return PowerState.UNKNOWN;
                case RemoteControl.PowerState.LightSleep:
                    return PowerState.S1_SLEEPING_WITH_CONTEXT;
                case RemoteControl.PowerState.MasterBusReset:
                    return PowerState.UNKNOWN;
                case RemoteControl.PowerState.On:
                    return PowerState.S0_POWERED;
                case RemoteControl.PowerState.Other:
                    return PowerState.S0_POWERED;
                case RemoteControl.PowerState.SoftOff:
                    return PowerState.S5_SOFT_OFF;
                case RemoteControl.PowerState.SoftPowerCycle:
                    return PowerState.S5_SOFT_OFF;
            }

            return PowerState.UNKNOWN;
        }

        public PowerState GetPowerState(string hostname, string AmtUser, string AmtPass)
        {
            try
            {
                if (AmtInterfaceSettings.ApiInterface == ApiInterface.UseWSMAN)
                {
                    return ConvertPowerState(this.GetPowerStateWSMAN(hostname, AmtUser, AmtPass));
                }

                bool UseTLS = false;
                string CertName = null;

                AmtCredentials credentials = new AmtCredentials(AmtUser, AmtPass, UseTLS, CertName, null, false);
                AmtSystem amtSystem = AmtSystem.Connect(hostname, credentials);

                Intel.vPro.AMT.PowerState CurrentState = amtSystem.PowerState;

                return CurrentState;

            }
            catch (Exception e)
            {
                string errmsg = e.Message.ToString();
            }
            return PowerState.UNKNOWN;
        }

        public bool SetPowerWSMAN(string hostname, string AmtUser, string AmtPass, bool isOn)
        {
            try
            {
                RemoteControlApi api = new RemoteControlApi(hostname, AmtUser, AmtPass, false, null);
                return api.ChangeSystemPowerState(isOn ? RemoteControl.PowerState.On : RemoteControl.PowerState.SoftOff, true) == 0;

            }
            catch (Exception e)
            {
                string errmsg = e.Message.ToString();
            }
            return false;
        }

        private bool SetPower(string hostname, string AmtUser, string AmtPass, bool isOn)
        {
            try
            {
                if (AmtInterfaceSettings.ApiInterface == ApiInterface.UseWSMAN)
                {
                    return this.SetPowerWSMAN(hostname, AmtUser, AmtPass, isOn);
                }

                bool UseTLS = false;
                string CertName = null;

                AmtCredentials credentials = new AmtCredentials(AmtUser, AmtPass, UseTLS, CertName, null, false);
                AmtSystem amtSystem = AmtSystem.Connect(hostname, credentials);

                Intel.vPro.AMT.PowerState CurrentState = amtSystem.PowerState;                

                if (true == isOn)
                {
                    if (CurrentState == Intel.vPro.AMT.PowerState.S4_SUSPENDED_TO_DISK ||
                    CurrentState == Intel.vPro.AMT.PowerState.S5_SOFT_OFF)
                    {
                        AmtRemoteControl.PowerUp(amtSystem);
                        return true;
                    }
                    else
                        return false;
                }
                else
                {
                    if (CurrentState == Intel.vPro.AMT.PowerState.S0_POWERED ||
                        CurrentState == Intel.vPro.AMT.PowerState.S1_SLEEPING_WITH_CONTEXT ||
                        CurrentState == Intel.vPro.AMT.PowerState.S2_SLEEPING_WITHOUT_CONTEXT)
                    {
                        AmtRemoteControl.PowerDown(amtSystem);
                        return true;
                    }
                    else
                        return false;
                }

            }
            catch (Exception e)
            {
                string errmsg = e.Message.ToString();
            }
            return false;
        }

        public bool SetPowerOn(string hostname, string AmtUser, string AmtPass)
        {
            return SetPower(hostname, AmtUser, AmtPass, true);
        }

        public bool SetPowerOff(string hostname, string AmtUser, string AmtPass)
        {
            return SetPower(hostname, AmtUser, AmtPass, false);
        }

        public bool CallWOL(string MacAddress, int port)
        {
            try
            {
                UdpClient client = new UdpClient();
                client.Connect(IPAddress.Broadcast, port);

                int counter = 0;
                byte[] bytes = new byte[1024];   // more than enough :-)
                //first 6 bytes should be 0xFF
                for (int y = 0; y < 6; y++)
                    bytes[counter++] = 0xFF;
                for (int y = 0; y < 16; y++)
                {
                    int i = 0;
                    for (int z = 0; z < 6; z++)
                    {
                        bytes[counter++] = byte.Parse(MacAddress.Substring(i, 2), NumberStyles.HexNumber);
                        i += 2;
                    }
                }

                client.Send(bytes, 1024);

                return true;
            }
            catch (Exception e)
            {
                string errmsg = e.Message.ToString();
            }
            return false;
        }

        public bool CallWOLByUnicast(string MacAddress, string IPv4, int port)
        {
            try
            {
                UdpClient client = new UdpClient();
                String[] arrIPv4 = IPv4.Split('.');

                byte[] arrIPBytes = new byte[4] { Convert.ToByte(arrIPv4[0], 10), Convert.ToByte(arrIPv4[1], 10), Convert.ToByte(arrIPv4[2], 10), Convert.ToByte(arrIPv4[3], 10) };
                client.Connect(new IPAddress(arrIPBytes), port);

                int counter = 0;
                byte[] bytes = new byte[1024];   // more than enough :-)
                //first 6 bytes should be 0xFF
                for (int y = 0; y < 6; y++)
                    bytes[counter++] = 0xFF;
                for (int y = 0; y < 16; y++)
                {
                    int i = 0;
                    for (int z = 0; z < 6; z++)
                    {
                        bytes[counter++] = byte.Parse(MacAddress.Substring(i, 2), NumberStyles.HexNumber);
                        i += 2;
                    }
                }

                client.Send(bytes, 1024);

                return true;
            }
            catch (Exception e)
            {
                string errmsg = e.Message.ToString();
            }
            return false;
        }

        public bool CallWOL(string MacAddress, string IPv4, int port)
        {
            try
            {
                UdpClient client = new UdpClient();
                client.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Broadcast, 0);
                String[] arrIPv4 = IPv4.Split('.');

                byte[] arrIPBytes = new byte[4] { Convert.ToByte(arrIPv4[0], 10), Convert.ToByte(arrIPv4[1], 10), Convert.ToByte(arrIPv4[2], 10), 255 };
                client.Connect(new IPAddress(arrIPBytes), port);


                int counter = 0;
                byte[] bytes = new byte[1024];   // more than enough :-)
                //first 6 bytes should be 0xFF
                for (int y = 0; y < 6; y++)
                    bytes[counter++] = 0xFF;
                for (int y = 0; y < 16; y++)
                {
                    int i = 0;
                    for (int z = 0; z < 6; z++)
                    {
                        bytes[counter++] = byte.Parse(MacAddress.Substring(i, 2), NumberStyles.HexNumber);
                        i += 2;
                    }
                }

                client.Send(bytes, 1024);

                return true;
            }
            catch (Exception e)
            {
                string errmsg = e.Message.ToString();
            }
            return false;
        }
    }
}
