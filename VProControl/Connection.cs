﻿//----------------------------------------------------------------------------
//
//  Copyright (C) Intel Corporation, 2011 - 2012.
//
//  File:       CmdLineArguments.cs 
//
//  Contents:   This file is an infrastructure for the entire WSMan sample. 
//              It contains a parser for the information inputted by the user 
//              via the command line arguments.  
//
//----------------------------------------------------------------------------



using System;
using Intel.Management.Wsman;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography;


namespace Connection
{
    /// <summary>
    /// Creates a Connection to AMT and handles conenction errors
    /// </summary>
    /// <remarks> Demonstrates the liratry task of creating a connection to AMT</remarks>
    public class Connection_setup
    {

        #region PRIVATE_DATA_MEMBERS

        // The WSManClient - connection object
        public IWsmanConnection wsmanClient = null;

        #endregion PRIVATE_DATA_MEMBERS
        
        #region CONSTRUCTORS
          /// <summary>
        /// Constructor.
        /// </summary>
        /// Creating the connection to the WSman Client.
        public Connection_setup(string ip, string username, string pwd, bool krb, MpsManager proxy)//IWSManClient wsmanClient)
        {
            wsmanClient = new WsmanConnection();
            wsmanClient.Address = ValidateIP6(ip)
                                        ? "http://[" + ip + "]:16992/wsman"
                                        : "http://" + ip + ":16992/wsman";

              if (krb)
            {
                wsmanClient.AuthenticationScheme = "Negotiate";
            }
            else
            {
                wsmanClient.Username = username;
                wsmanClient.Password = pwd;
                wsmanClient.AuthenticationScheme = "Digest";
            }
            //check if proxy enabled - Else enable proxy and set Authentication Scheme to Digest 
            Proxy_Check(proxy);
            
            //check for issues with Connection
            Connection_Check();

        }

        public Connection_setup(string ip, string username, string pwd, string clientcert, bool krb, MpsManager proxy)//IWSManClient wsmanClient)
        {
            wsmanClient = new WsmanConnection();
            wsmanClient.Address = ValidateIP6(ip)
                                       ? "https://[" + ip + "]:16993/wsman"
                                       : "https://" + ip + ":16993/wsman";

            //Client Certificate information retrieved from Store.
            wsmanClient.Options.ClientCertificate = !String.IsNullOrEmpty(clientcert)?getCertFromStore(clientcert)[0]:null;

            if (krb)
            {
                wsmanClient.AuthenticationScheme = "Negotiate";
            }
            else
            {
                wsmanClient.Username = username;
                wsmanClient.Password = pwd;
                wsmanClient.AuthenticationScheme = "Digest";
            }
            //check if proxy is enabled - else Enable and set authentication scheme to Digest
            Proxy_Check(proxy);
            Connection_Check();
        }
        
        #endregion

        #region Public Functions
        //Check if proxy is Enabled
        //if not Enable Proxy
        void Proxy_Check(MpsManager mps)
        {
            // MpsManager mps = new Intel.Management.Wsman.MpsManager();
            if (mps != null)
            {
                if (mps.Enabled)
                {
                    Console.WriteLine("MPS Enabled:{0} ", mps.Enabled); // -fore Green
                }
                else
                {
                    Console.WriteLine("MPS Enabled: False");// -fore Red
                    Console.WriteLine("Enabling Proxy now..");
                    mps.Enabled = true;
                    wsmanClient.AuthenticationScheme = "digest";
                }

                Console.WriteLine("HTTP Proxy: {0}", mps.HttpProxy);
                Console.WriteLine("SOCKS Proxy: {0}", mps.SocksProxy);

                foreach (string computer in mps.Hosts)
                {
                    Console.WriteLine("AMT Host:{0}", computer);
                }
            }

        }
        //end ProxyCheck

        //Perform a check for the conenction... 
        void Connection_Check()
        {
            try
            {
                IManagedInstance info = wsmanClient.Identify();
            }
            catch (Exception e)
            {
                Console.WriteLine("Error Occurred in Wsman Connection!");
                throw e;
            }
        
        }


        //Get Client Cert from Store.
        static public X509CertificateCollection getCertFromStore(String clientCert)
        {
            const string OID_LOCAL = "2.16.840.1.113741.1.2.2";
            const string OID_REMOTE = "2.16.840.1.113741.1.2.1";
            X509CertificateCollection certificatesCollection = new X509CertificateCollection();

            // Open CurrentUser cert store
            X509Store currentUserStore = new X509Store(StoreName.My, StoreLocation.CurrentUser);
            currentUserStore.Open(OpenFlags.ReadOnly);

            foreach (X509Certificate2 certificate in currentUserStore.Certificates)
            {
                if (certificate.Subject.Contains(clientCert))
                {
                    // Checking that the Enhanced Key Usage in the certificate is the one for AMT
                    foreach (X509Extension extension in certificate.Extensions)
                    {
                        if (extension is X509EnhancedKeyUsageExtension)
                        {
                            X509EnhancedKeyUsageExtension ex = (X509EnhancedKeyUsageExtension)extension;
                            foreach (Oid OID in ex.EnhancedKeyUsages)
                            {
                                if (OID.Value == OID_REMOTE || OID.Value == OID_LOCAL)
                                    certificatesCollection.Add(certificate);
                            }
                        }
                    }
                }
            }
            currentUserStore.Close();

            // Open LocalMachine cert store
            X509Store localMachineStore = new X509Store(StoreName.My, StoreLocation.LocalMachine);
            localMachineStore.Open(OpenFlags.ReadOnly);

            foreach (X509Certificate2 certificate in localMachineStore.Certificates)
            {
                if (certificate.Subject.Contains(clientCert))
                {
                    // Checking that the Enhanced Key Usage in the certificate is the one for AMT
                    foreach (X509Extension extension in certificate.Extensions)
                    {
                        if (extension is X509EnhancedKeyUsageExtension)
                        {
                            X509EnhancedKeyUsageExtension ex = (X509EnhancedKeyUsageExtension)extension;
                            foreach (Oid OID in ex.EnhancedKeyUsages)
                            {
                                if (OID.Value == OID_REMOTE || OID.Value == OID_LOCAL)
                                    certificatesCollection.Add(certificate);
                            }
                        }
                    }
                }
            }

            localMachineStore.Close();
            if (certificatesCollection.Count < 1)
                throw new Exception("Can not find appropriate certificate in certificate store");

            return certificatesCollection;
        }


        #endregion CONSTRUCTORS

        #region Private Functions
       
        public static bool ValidateIP6(string ip)
        {
            UriHostNameType tmpType = Uri.CheckHostName(ip);
            if (tmpType == UriHostNameType.IPv6)
            {
                return true;
            }
            return false;
        }

	    #endregion

    }

    public static class UtilitiesMethods
    {
        /// <summary>
        /// Get FW/Core Versin 
        /// </summary>
        public static string GetCoreVersion(IWsmanConnection wsmanClient)
        {
            IManagedReference softwareIdentityRef = wsmanClient.NewReference("SELECT * FROM CIM_SoftwareIdentity WHERE  InstanceID='AMT FW Core Version'");
            IManagedInstance softwareIdenetityInstance = softwareIdentityRef.Get();
            string versionString = softwareIdenetityInstance.GetProperty("VersionString").ToString();

            return versionString;
        }
    }

}
