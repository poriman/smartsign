// -----------------------------------------------------------------------------
//
// Copyright (c) 2009 Intel Corporation. All Rights Reserved.
//
// The source code contained or described herein and all documents related to 
// the source code ("Material") are owned by Intel Corporation or its suppliers 
// or licensors. Title to the Material remains with Intel Corporation or its 
// suppliers and licensors. The Material may contain trade secrets and 
// proprietary and confidential information of Intel Corporation and its 
// suppliers and licensors, and is protected by worldwide copyright and trade 
// secret laws and treaty provisions. No part of the Material may be used, 
// copied, reproduced, modified, published, uploaded, posted, transmitted, 
// distributed, or disclosed in any way without Intel’s prior express written 
// permission. 
//
// No license under any patent, copyright, trade secret or other intellectual 
// property right is granted to or conferred upon you by disclosure or delivery 
// of the Materials, either expressly, by implication, inducement, estoppel or 
// otherwise. Any license under such intellectual property rights must be 
// express and approved by Intel in writing.
//
// * Third Party trademarks are the property of their respective owners.
//
// Unless otherwise agreed by Intel in writing, you may not remove or alter this 
// notice or any other notice embedded in Materials by Intel or Intel’s 
// suppliers or licensors in any way.
//
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Security;
using System.Threading;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Net.Sockets;
using System.Text;
using System.Diagnostics;
using System.Xml.Serialization;

using MOF_CLASSES;

//using Intel.Logging; 

namespace Intel.vPro.AMT
{
    public class AmtSystem
    {
        #region Private Fields
        private const int DEFAULT_TIMEOUT = 1000;

        private string _HostName;
        private AmtCredentials _Credentials;
        private RemoteControlService _RemoteControlService;
        private RedirectionService _RedirectionService;
        private WSManClient _Wsman;
        #endregion

        #region Constructors
        public AmtSystem()
        {
        }

        public AmtSystem(string hostName)
        {
            HostName = hostName;
        }
        #endregion

        #region Public Properties
        /// <summary>
        /// Represents the host name of this Intel(R) AMT system
        /// </summary>
        public string HostName
        {
            get { return _HostName; }
            set { _HostName = value; }
        }

        public AmtCredentials Credentials
        {
            get { return _Credentials; }
            set { _Credentials = value; }
        }

        [XmlIgnore]
        public WSManClient Wsman
        {
            get { return _Wsman; }
            set { _Wsman = value; }
        }

        [XmlIgnore]
        public RemoteControlService RemoteControlService
        {
            get { return _RemoteControlService; }
            set { _RemoteControlService = value; }
        }

        [XmlIgnore]
        public RedirectionService RedirectionService
        {
            get { return _RedirectionService; }
            set { _RedirectionService = value; }
        }

        public PowerState PowerState
        {
            get
            {
                return AmtRemoteControl.GetSystemPowerState(this,3);
            }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Determines if the defined Intel(R) AMT hostname is valid (ie responds to pings).
        /// </summary>
        /// <param name="hostname">The IP address or FQDN of the target system.</param>
        /// <returns>Returns true if the address is valid. Otherwise, the return is false.</returns>
        private static bool IsHostnameValid(string hostname)
        {
            try
            {
                for (int iteration = 0; iteration < 3; iteration++)
                {
                    Ping pingSender = new Ping();
                    PingReply reply = pingSender.Send(hostname);
                    if (reply.Status == IPStatus.Success)
                        return true;
                } // END for (int iteration = 0; ...)

                return false;
            }
            catch (Exception e)
            {
                string errmsg = e.Message.ToString();
                //Log.WriteLine(LogPriority.Error, "ERROR: Unable to ping the Intel(R) AMT device. " + e.Message); 
                return false;
            }
        }

        /// <summary>
        /// Tests the Intel(R) AMT connection by authenticating a connection
        /// </summary>
        /// <param name="amtHostname"></param>
        /// <param name="useTls"></param>
        /// <returns></returns>
        public bool TestAmtConnection(string amtHostname, bool useTls)
        {
            try
            {
                // Determine if the Intel(R) AMT device is istening and responsive.
                Socket s = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

                if (useTls)
                {
                    // Connect using the Enterprise port.
                    s.Connect(amtHostname, 16993);
                }
                else
                {
                    // Connect using the SMB port.
                    s.Connect(amtHostname, 16992);
                }
                s.Close();
            }
            catch (Exception e)
            {
                string errmsg = e.Message.ToString();
                //Log.WriteLine(LogPriority.Error, "ERROR: Unable to connect with the Intel(R) AMT device. " + e.Message); 
                return false;
            }

            if (!useTls)
            {
                //
                // Determine if the device is properly configured for SMB.
                //
                try
                {
                    //
                    // Determine if the Intel(R) AMT device is present at the given port.
                    // This is ONLY concerned with SMB mode.
                    //
                    string str = null;
                    TcpClient client = new TcpClient(amtHostname, 16992);
                    NetworkStream ns = client.GetStream();
                    ns.ReadTimeout = DEFAULT_TIMEOUT;
                    ns.WriteTimeout = DEFAULT_TIMEOUT;
                    System.DateTime failTime = System.DateTime.Now.AddSeconds(5);
                    byte[] b = UTF8Encoding.UTF8.GetBytes("HEAD / HTTP/1.1\r\nHost: RemoteControl\r\n\r\n");
                    ns.Write(b, 0, b.Length);
                    byte[] buf = new byte[2000];
                    int ptr = 0;
                    string head = "";
                    try
                    {
                        while (System.DateTime.Now.CompareTo(failTime) < 0 && head.IndexOf("\r\n\r\n") < 0)
                        {
                            Thread.Sleep(100);
                            ptr = ns.Read(buf, ptr, 2000 - ptr);
                            head = UTF8Encoding.UTF8.GetString(buf, 0, ptr);
                        }
                    }
                    catch (Exception e)
                    {
                        string errmsg = e.Message.ToString();
                        //Log.WriteLine(LogPriority.Error, "Exception " + e.Message + " type " + e.GetType().ToString()); 
                        return false; // Assuming an error with the Intel(R) AMT device.
                    }
                    finally
                    {
                        ns.Close();
                        client.Close();
                        client = null;
                    }

                    if (head.IndexOf("\r\n\r\n") > 0)
                    {
                        int i = head.IndexOf("\r\nServer: ");
                        if (i >= 0)
                        {
                            str = head.Substring(i + 10);
                            i = str.IndexOf("\r\n");
                            str = str.Substring(0, i);
                        }
                        if (String.IsNullOrEmpty(str))
                        {
                            return false;
                        }
                        else if (str.StartsWith("Intel(R) Active Management Technology") == true)
                        {
                            return true;
                        }
                        else if (str.StartsWith("Intel(R) Management & Security Application") == true)
                        {
                            // TODO: Come up with better mechanism for detecting Intel(R) AMT system in SMB mode (no TLS).
                            // Forward compatibility is a must and this header information is likey to continue changing.
                            // Perhaps just use a basic SecurityAdmin command to determine if the system is present?
                            // Or just make this test optional? Or just test for the "Intel(R)" string?
                            return true;
                        }
                        else
                        {
                            //Log.WriteLine(LogPriority.Trace, "Returned Intel(R) AMT server header: " + str); 
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
                catch (Exception e)
                {
                    string errmsg = e.Message.ToString();
                    //Log.WriteLine(LogPriority.Error, "ERROR: Unable to connect with the Intel(R) AMT device. " + e.Message); 
                    return false;
                }
            } // END if (!useTls)

            return true;
        }

        /// <summary>
        /// Performs an initial check of the Intel(R) AMT system connectivity and initializes the system object and
        /// credentials. Use this method to obtain an AmtSystem object which is used by many other classes
        /// to perform Intel(R) AMT actions.
        /// </summary>
        /// <param name="hostname">Hostname of the Intel(R) AMT system to connect to</param>
        /// <param name="credentials">credentials of the Intel(R) AMT system to connect to</param>
        /// <returns>An initialized Intel(R) AMT system object</returns>
        public static AmtSystem Connect(string hostname, AmtCredentials credentials)
        {
            if (credentials.UseTls)
            {
                IPAddress tempAddress;
                if (IPAddress.TryParse(hostname, out tempAddress))
                {
                    IPHostEntry entry = Dns.GetHostEntry(tempAddress);
                    hostname = entry.HostName;

                    //Log.WriteLine(LogPriority.Info, "IP address " + tempAddress.ToString() + " mapped to hostname " + hostname); 
                }
            }

            AmtSystem newSystem = new AmtSystem(hostname);
            newSystem.Credentials = credentials;
            
            if (AmtInterfaceSettings.ApiInterface == ApiInterface.UseWSMAN)
            {
                // Initialize WSMAN interface
                newSystem.Wsman = new WSManClient(hostname, credentials.UserName, credentials.Password,
                    credentials.UseTls);
            }
            else
            {
                // Intialize EOI interfaces
                newSystem.RemoteControlService = credentials.SetupRemoteControlService(hostname, false, credentials.UseProxy);
                newSystem.RedirectionService = credentials.SetupRedirectionServiceService(hostname, false, credentials.UseProxy);
            }
            
            return newSystem;
        }
        #endregion
    }
}
