// -----------------------------------------------------------------------------
//
// Copyright (c) 2009 Intel Corporation. All Rights Reserved.
//
// The source code contained or described herein and all documents related to 
// the source code ("Material") are owned by Intel Corporation or its suppliers 
// or licensors. Title to the Material remains with Intel Corporation or its 
// suppliers and licensors. The Material may contain trade secrets and 
// proprietary and confidential information of Intel Corporation and its 
// suppliers and licensors, and is protected by worldwide copyright and trade 
// secret laws and treaty provisions. No part of the Material may be used, 
// copied, reproduced, modified, published, uploaded, posted, transmitted, 
// distributed, or disclosed in any way without Intel’s prior express written 
// permission. 
//
// No license under any patent, copyright, trade secret or other intellectual 
// property right is granted to or conferred upon you by disclosure or delivery 
// of the Materials, either expressly, by implication, inducement, estoppel or 
// otherwise. Any license under such intellectual property rights must be 
// express and approved by Intel in writing.
//
// * Third Party trademarks are the property of their respective owners.
//
// Unless otherwise agreed by Intel in writing, you may not remove or alter this 
// notice or any other notice embedded in Materials by Intel or Intel’s 
// suppliers or licensors in any way.
//
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Threading;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;
using System.Diagnostics;

//using Intel.Logging;

namespace Intel.vPro.AMT
{
    public class AmtSolSession
    {
        #region Private Fields
        /// <summary>
        /// buffer used to store SOL data read
        /// </summary>
        private IntPtr GlobalReceiveBuffer = IntPtr.Zero;

        /// <summary>
        /// size of buffer to read for SOL data
        /// </summary>
        private const int SOLBUFFER_SIZE = 2048;
        private const int SOLRECEIVEPOLL_DELAY = 200;   // Milliseconds delay between polling events

        /// <summary>
        /// thread used for watching for SOL feedback
        /// </summary>
        private Thread listenerThread = null;

        private AmtSystem _System;
        private AmtCredentials _Credentials;
        private bool _SolActive;
        private uint clientId;

        private string TrustedRootFile;
        private string PrivateCertFile; 

        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets a flag that indicates whether Serial Over LAN is active
        /// </summary>
        public bool SolActive
        {
            get { return _SolActive; }
            set { _SolActive = value; }
        }

        #endregion

        #region public class

        /// <summary>
        /// class for the SOL Timeout structure
        /// </summary>
        public class SolTout
        {
            /// note: these values come straight from the Intel(R) AMT SDK
            /// Redirection Library Design guide

            /// <summary>
            /// The client transmit overflow timeout. This is the number of
            /// milliseconds the client waits when its transmit buffer is full before
            /// starting to drop transmit bytes. A value of 0 means no timeout.
            /// Minimum value: 0
            /// Maximum value: 65535
            /// Default value: 0
            /// </summary>           
            public ushort tx_over_timeout;
            
            /// <summary>
            /// The client transmit buffering timeout. This is the number of
            /// milliseconds the client waits for its transmit buffer to become full
            /// before sending its buffered transmit bytes. A value of 0 means that
            /// the client will transmit its data only when its buffer becomes full.
            /// Minimum value: 0
            /// Maximum value: 65535
            /// Default value: 100
            /// </summary>
            public ushort tx_buf_timeout;

            /// <summary>
            /// The client heartbeat interval. This is the number of milliseconds the
            /// client waits between sending heartbeat messages to the library,
            /// indicating that the client is active. A value of 0 means that no
            /// heartbeats are sent. In this case, the library will not monitor the
            /// receive activity from the client to determine if it is active.
            /// Minimum value: 0
            /// Maximum value: 65535
            /// Default value: 5000
            /// </summary>
            public ushort hb_interval;

            /// <summary>
            /// The client’s FIFO receive flush timeout. This is the number of
            /// milliseconds the client waits when its receive FIFO is full before
            /// flushing its received data. A value of 0 means that the client never
            /// flushes its received data when it is not read by the operating system.
            /// Minimum value: 0
            /// Maximum value: 65535
            /// Default value: 100
            /// </summary>
            public ushort fifo_rx_flush_timeout;

            /// <summary>
            /// The client’s receive timeout. If this number of milliseconds passes
            /// before receiving any messages from the library, the client shuts down
            /// the SOL session. When an SOL session is open, the library
            /// periodically sends heartbeat messages to make sure that the client
            /// receive timeout does not expire (the interval between library
            /// heartbeat messages is based on the client receive timeout).
            /// Minimum value: 10000
            /// Maximum value: 65535
            /// Default value: 10000
            /// </summary>
            public ushort rx_timeout;

            public SolTout()
            {
                //initilize to default values
                tx_over_timeout = 0;
                tx_buf_timeout = 100;
                hb_interval = 5000;
                fifo_rx_flush_timeout = 100;
                rx_timeout = 10000;
            }
        }

        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new SOL session on the specified Intel(R) AMT system using the specified credentials
        /// </summary>
        /// <param name="system">The Intel(R) AMT system to start a SOL session on</param>
        /// <param name="credentials">The credentials to use to start the SOL session</param>
        /// <returns>An AMTSolSession object that represents the session</returns>
        public static AmtSolSession CreateSolSession(AmtSystem system, AmtCredentials credentials)
        {
            return new AmtSolSession(system, credentials);
        }

        /// <summary>
        /// Private constructor to create a new SOL session
        /// </summary>
        /// <param name="system">The Intel(R) AMT system to start a SOL session on</param>
        /// <param name="credentials">The credentials to use to start the SOL session</param>
        private AmtSolSession(AmtSystem system, AmtCredentials credentials)
        {
            _System = system;
            _Credentials = credentials;
        }
        #endregion

        #region Public Events
        //event handler for monitoring SOL reading
        public delegate void SerialDataHandler(AmtSolSession redirService, byte[] data, int len);
        public event SerialDataHandler SerialData;
        #endregion

        #region public SOL functions
                
        /// <summary>
        /// Function to open SOL session and start seperate thread for monitoring SOL reading
        /// using default timeout values
        /// </summary>
        public void StartSolSession()
        {
            SolTout timeout = new SolTout();
            StartSolSession(timeout);
        }

        /// <summary>
        /// Function to open SOL session and start seperate thread for monitoring SOL reading
        /// using specified timeout values
        /// </summary>
        public void StartSolSession(SolTout timeout)
        {
            clientId = ImrSdkWrapper.AddClient(_System, _Credentials);
            if (clientId == uint.MaxValue)
                throw new AmtRedirectionException("Could not add client to ImrSdk Client list");
            IMRResult r = IMRResult.IMR_RES_OK;

            TrustedRootFile = Path.Combine(ImrSdkWrapper.CertPemDir, Path.GetRandomFileName());
            PrivateCertFile = Path.Combine(ImrSdkWrapper.CertPemDir, Path.GetRandomFileName()); 

            ImrSdkWrapper.SetupCertificates(_Credentials, TrustedRootFile, PrivateCertFile);

            // Open SOL session
            if (_Credentials.UseProxy == true)
            {
                // Use SOCKS proxy
                r = SOLOpenTCPSessionEx(clientId, _Credentials.UserName, _Credentials.Password, _Credentials.Proxy, timeout);
            }
            else
            {
                // Direct connection (no proxy)
                r = SOLOpenTCPSession(clientId, _Credentials.UserName, _Credentials.Password, timeout);
            }

            if (r != IMRResult.IMR_RES_OK)
            {
                throw new AmtRedirectionException(r, "IMR_SOLOpenTCPSession() failed");
            }
            SolActive = true;

            if (GlobalReceiveBuffer == IntPtr.Zero)
                GlobalReceiveBuffer = Marshal.AllocHGlobal(SOLBUFFER_SIZE);
            listenerThread = new Thread(new ThreadStart(SOLSerialLoop));
            listenerThread.Start();
        }

        /// <summary>
        /// Function to open SOL session and start seperate thread for monitoring SOL reading. Connections
        /// attempts are retried the specified # of times on failure.
        /// </summary>
        public void StartSolSession(int numRetries, SolTout timeout)
        {
            int Tries = 0;
            while (Tries <= numRetries)
            {
                try
                {
                    StartSolSession(timeout);
                    break;
                }
                catch (Exception)
                {
                    if (Tries >= numRetries)
                    {
                        throw;
                    }
                   //Log.WriteLine(LogPriority.Info, "Retrying SOL session");
                    Tries++;
                }
            }
        }

        /// <summary>
        /// Function used to write input text out over SOL buffer
        /// </summary>
        /// <param name="input">string of text to output</param>
        public void WriteSolText(string input)
        {
            if (clientId == uint.MaxValue)
                return;

            byte[] buffer = UTF8Encoding.UTF8.GetBytes(input);
            ImrSdkWrapper.IMR_SOLSendText(clientId, buffer, buffer.Length);
        }

        /// <summary>
        /// function to stop SOL listening thread and end SOL session
        /// </summary>
        public void StopSolSession()
        {
            if (SolActive == false)
                return;
            SolActive = false;

            if (listenerThread != null)
                while (listenerThread.IsAlive)
                    Thread.Sleep(10);

            new Thread(new ThreadStart(SOLStopRedirect)).Start();
        }

        #endregion public SOL functions

        #region Private SOL functions

        /// <summary>
        /// Function to open SOL session with specified client for new TCP connection.
        /// If the client uses secure communications  TLS session will be negotiated with it
        /// </summary>
        /// <param name="clientId">the ID of the Client to open session for</param>
        /// <param name="username">the users name</param>
        /// <param name="password">the users password</param>
        /// <param name="timeout">the timeout values to use for service</param>
        /// <returns>IMRRESULT error code</returns>
        private static IMRResult SOLOpenTCPSession(uint clientId, string username, string password, SolTout timeout)
        {
            byte[] login = new byte[256];
            UTF8Encoding.UTF8.GetBytes(username, 0, username.Length, login, 0);
            UTF8Encoding.UTF8.GetBytes(password, 0, password.Length, login, 128);
            IntPtr data = IntPtr.Zero;

            //not using sol_timeout settings here
            //TODO figure out if sol_timout settings support should be supported

            //pass explicit values to avoid SOL disconnects issues.
            data = Marshal.AllocHGlobal(10);
            if (data == null)
                return IMRResult.IMR_RES_MEMALLOC_FAILED;

            byte[] rawdata = new byte[10];
            BitConverter.GetBytes((ushort)timeout.tx_over_timeout).CopyTo(rawdata, 0);
            BitConverter.GetBytes((ushort)timeout.tx_buf_timeout).CopyTo(rawdata, 2);
            BitConverter.GetBytes((ushort)timeout.hb_interval).CopyTo(rawdata, 4);
            BitConverter.GetBytes((ushort)timeout.fifo_rx_flush_timeout).CopyTo(rawdata, 6);
            BitConverter.GetBytes((ushort)timeout.rx_timeout).CopyTo(rawdata, 8);

            for (int i = 0; i < rawdata.Length; i++)
			{
			    Marshal.WriteByte(data,i, rawdata[i]); 
			}

            IMRResult r = ImrSdkWrapper.IMR_SOLOpenTCPSession(clientId, login, data, IntPtr.Zero);
            Marshal.FreeHGlobal(data);
            return r;
        }

        /// <summary>
        /// Function to open SOL session with specified client for new TCP connection.
        /// If the client uses secure communications  TLS session will be negotiated with it.
        /// Also defines a proxy connection with MPS
        /// </summary>
        /// <param name="clientId">the ID of the Client to open session for</param>
        /// <param name="username">the users name</param>
        /// <param name="password">the users password</param>
        /// <param name="proxy">SOCKSv5 proxy connection paramaters</param>
        /// <param name="timeout">the timeout values to use for service</param>
        /// <returns>IMRRESULT error code</returns>
        private IMRResult SOLOpenTCPSessionEx(uint clientId, string username, string password, ProxyInfo proxy, SolTout timeout)
        {
            byte[] paramsEx = _Credentials.CreateTCPSessionParamsEx();
            IntPtr data = IntPtr.Zero;

            //pass explicit values to avoid SOL disconnects issues.           
            data = Marshal.AllocHGlobal(10);
            if (data == null)
                return IMRResult.IMR_RES_MEMALLOC_FAILED;
           
            byte[] rawdata = new byte[10];
            BitConverter.GetBytes((ushort)timeout.tx_over_timeout).CopyTo(rawdata, 0);
            BitConverter.GetBytes((ushort)timeout.tx_buf_timeout).CopyTo(rawdata, 2);
            BitConverter.GetBytes((ushort)timeout.hb_interval).CopyTo(rawdata, 4);
            BitConverter.GetBytes((ushort)timeout.fifo_rx_flush_timeout).CopyTo(rawdata, 6);
            BitConverter.GetBytes((ushort)timeout.rx_timeout).CopyTo(rawdata, 8);

            for (int i = 0; i < rawdata.Length; i++)
            {
                Marshal.WriteByte(data, i, rawdata[i]);
            }

            IMRResult r = ImrSdkWrapper.IMR_SOLOpenTCPSessionEx(clientId, paramsEx, data, IntPtr.Zero);
            Marshal.FreeHGlobal(data);

            return r;
        }

        /// <summary>
        /// This is the function that runs on the listener thread while 
        /// SOL session is active to read data and calls callback function
        /// to pass read data back
        /// </summary>
        private void SOLSerialLoop()
        {
            try
            {
                byte[] recv;
                IMRResult r;
                while (SolActive == true)
                {
                    r = SOLReceiveText(GlobalReceiveBuffer, clientId, out recv);
                    if (r != IMRResult.IMR_RES_OK)
                    {
                        #region Error recovery
                        // Disconnection
                        if (r == IMRResult.IMR_RES_SESSION_CLOSED)
                        {
                           //Log.WriteLine(LogPriority.Error, "SOL session disconnected on " + this._System.HostName); 

                            SolActive = false;
                            listenerThread = null;

                            if (SerialData != null)
                                SerialData(this, null, 0);
                        }
                        else
                        {
                            SolActive = false;
                            listenerThread = null;
                            ImrSdkWrapper.IMR_SOLCloseSession(clientId);

                            if (SerialData != null)
                                SerialData(this, null, 0);
                        }
                        #endregion
                        return;
                    }

                    // Receive Data
                    if (recv != null)
                    {
                        // Received data. Invoke our event if there are any handlers
                        if(SerialData != null)
                            SerialData(this, recv, recv.Length);
                    }
                    else // No data received
                    {
                        // The AMT SDK redirection DLL only supports polling the Serial Over LAN receive buffer. Therefore, we need to poll every so often to look for new data
                        Thread.Sleep(SOLRECEIVEPOLL_DELAY);
                    }
                }
            }
            catch (ThreadInterruptedException) { }
            if (GlobalReceiveBuffer != IntPtr.Zero)
            {
                Marshal.FreeHGlobal(GlobalReceiveBuffer);
                GlobalReceiveBuffer = IntPtr.Zero;
            }
        }

        /// <summary>
        /// Function to retrieve SOL data from IMR library buffer
        /// </summary>
        /// <param name="GlobalReceiveBuffer">pointer to buffer to copy data to</param>
        /// <param name="clientId">ID of the client to retrieve data for</param>
        /// <param name="buf">array buffer to copy data to</param>
        /// <returns>IMRRESULT error code</returns>
        private static IMRResult SOLReceiveText(IntPtr GlobalReceiveBuffer, uint clientId, out byte[] buf)
        {
            IMRResult r;
            buf = null;
            if (GlobalReceiveBuffer == IntPtr.Zero)
            {
                return IMRResult.IMR_RES_NOT_INITIALIZED;
            }
            int len = SOLBUFFER_SIZE;
            r = ImrSdkWrapper.IMR_SOLReceiveText(clientId, GlobalReceiveBuffer, ref len);
            if (r != IMRResult.IMR_RES_OK)
            {
                return r;
            }
            if (len > 0)
            {
                buf = new byte[len];
                Marshal.Copy(GlobalReceiveBuffer, buf, 0, len);
            }
            return r;
        }

        /// <summary>
        /// ends the SOL session
        /// </summary>
        private void SOLStopRedirect()
        {
            try
            {
                listenerThread = null;
                ImrSdkWrapper.IMR_SOLCloseSession(clientId);
            }
            finally
            {
                if (File.Exists(TrustedRootFile))
                    File.Delete(TrustedRootFile);

                if (File.Exists(PrivateCertFile))
                    File.Delete(PrivateCertFile);
            }
        }

        #endregion private SOL functions
    }
}
