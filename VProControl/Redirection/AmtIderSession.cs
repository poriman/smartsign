// -----------------------------------------------------------------------------
//
// Copyright (c) 2009 Intel Corporation. All Rights Reserved.
//
// The source code contained or described herein and all documents related to 
// the source code ("Material") are owned by Intel Corporation or its suppliers 
// or licensors. Title to the Material remains with Intel Corporation or its 
// suppliers and licensors. The Material may contain trade secrets and 
// proprietary and confidential information of Intel Corporation and its 
// suppliers and licensors, and is protected by worldwide copyright and trade 
// secret laws and treaty provisions. No part of the Material may be used, 
// copied, reproduced, modified, published, uploaded, posted, transmitted, 
// distributed, or disclosed in any way without Intel’s prior express written 
// permission. 
//
// No license under any patent, copyright, trade secret or other intellectual 
// property right is granted to or conferred upon you by disclosure or delivery 
// of the Materials, either expressly, by implication, inducement, estoppel or 
// otherwise. Any license under such intellectual property rights must be 
// express and approved by Intel in writing.
//
// * Third Party trademarks are the property of their respective owners.
//
// Unless otherwise agreed by Intel in writing, you may not remove or alter this 
// notice or any other notice embedded in Materials by Intel or Intel’s 
// suppliers or licensors in any way.
//
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;


namespace Intel.vPro.AMT
{
    public class AmtIderSession
    {
        #region Private Fields
        private AmtSystem _System;
        private AmtCredentials _Credentials;
        private bool _IderActive;
        private uint clientId;

        private string TrustedRootFile;
        private string PriavteCertFile; 

        #endregion

        #region Constructors
        public static AmtIderSession CreateIderSession(AmtSystem system, AmtCredentials credentials)
        {
            return new AmtIderSession(system, credentials);
        }

        private AmtIderSession(AmtSystem system, AmtCredentials credentials)
        {
            _System = system;
            _Credentials = credentials;
        }
        #endregion

        #region Public Properties
        /// <summary>
        /// Gets or sets a flag that indicates whether IDE redirection is active
        /// </summary>
        public bool IderActive
        {
            get { return _IderActive; }
            set { _IderActive = value; }
        }

        #endregion

        #region Public class

        /// <summary>
        /// class for the IDER Timeout structure
        /// </summary>
        public class IderTout
        {
            /// note: these values come straight from the Intel(R) AMT SDK
            /// Redirection Library Design guide

            /// <summary>
            /// The client receive timeout, in milliseconds. If this much time passes
            /// before receiving any messages from the library, the client shuts down
            /// the IDER session. When an IDER session is open, the library
            /// continually sends out messages to make sure that the client’s receive
            /// timeout does not expire (the library heartbeat interval is based on the
            /// client receive timeout setting).
            /// Minimum value: 10000
            /// Maximum value: 65535
            /// Default value: 10000
            /// </summary>
            public ushort rx_timeout;

            /// <summary>
            /// The client’s command transmit timeout. This is the number of
            /// milliseconds the clients waits when sending out an IDE command. If
            /// the client does not receive a response from the library to the
            /// command within the specified amount of milliseconds, the client will
            /// close the IDER session. A value of 0 means that no command
            /// transmit timeout is used.
            /// Minimum value: 0
            /// Maximum value: 65535
            /// Default value: 0
            /// </summary>
            public ushort tx_timeout;

            /// <summary>
            /// The client heartbeat interval. This is the number of milliseconds the
            /// client waits before sending a heartbeat message to the library. A
            /// value of 0 means that no heartbeat messages are sent. In this case,
            /// the library will periodically send IDER keep-alive ping messages to
            /// the client when there is no activity, to determine if it is still alive.
            /// Minimum value: 0
            /// Maximum value: 65535
            /// Default value: 5000
            /// </summary> 
            public ushort hb_interval;


            public IderTout()
            {
                //initilize to default values
                rx_timeout = 10000;
                tx_timeout = 0;
                hb_interval = 5000; 
            }
        }

        #endregion

        #region public IDER functions
        /// <summary>
        /// function to open an IDER session and set devices to enabled for IDER
        /// </summary>
        /// <param name="drive0">string holding the name of the floppy disk drive 
        /// that will be used for IDER (eg "A:").  this can also hold the path
        /// to a binary floppy image file.  Must not be NULL.</param>
        /// <param name="drive1">string holding the name of the CD drive that
        /// will be used for IDER (eg "E:").  this can also hold a path to an
        /// ISO CD image file.  Must not be NULL.</param>
        /// <param name="state">used to specify timing options for the set device
        /// enable operation </param>
        public void StartIderSession(string drive0, string drive1, RedirectState state)
        {
            IderTout timeout = new IderTout();
            StartIderSession(drive0, drive1, state, timeout); 
        }


        /// <summary>
        /// function to open an IDER session and set devices to enabled for IDER
        /// </summary>
        /// <param name="drive0">string holding the name of the floppy disk drive 
        /// that will be used for IDER (eg "A:").  this can also hold the path
        /// to a binary floppy image file.  Must not be NULL.</param>
        /// <param name="drive1">string holding the name of the CD drive that
        /// will be used for IDER (eg "E:").  this can also hold a path to an
        /// ISO CD image file.  Must not be NULL.</param>
        /// <param name="state">used to specify timing options for the set device
        /// enable operation </param>
        public void StartIderSession(string drive0, string drive1, RedirectState state, IderTout timeout)
        {
            
            clientId = ImrSdkWrapper.AddClient(_System, _Credentials);
            if (clientId == uint.MaxValue)
                throw new AmtRedirectionException("Could not add client to ImrSdk Client list");
            IMRResult r = IMRResult.IMR_RES_OK;

            TrustedRootFile = Path.Combine(ImrSdkWrapper.CertPemDir, Path.GetRandomFileName());
            PriavteCertFile = Path.Combine(ImrSdkWrapper.CertPemDir, Path.GetRandomFileName()); 

            ImrSdkWrapper.SetupCertificates(_Credentials, TrustedRootFile, PriavteCertFile);

            //open IDER session 
            if (_Credentials.UseProxy == true)
            {
                r = IDEROpenTCPSessionEx(clientId, _Credentials.UserName, _Credentials.Password, drive0, drive1, _Credentials.Proxy, timeout);
            }
            else
            {
                r = IDEROpenTCPSession(clientId, _Credentials.UserName, _Credentials.Password, drive0, drive1, timeout);
            }

            if (r != IMRResult.IMR_RES_OK)
            {
                throw new AmtRedirectionException(r, "IMR_IDEROpenTCPSession() failed");
            }

            //set redirect flag to indicate IDER is on
            IderActive = true;

            //send command to enable the client IDE device
            IDERSetDeviceState(clientId, RedirectOperations.IDER_ENABLE, state);
        }

        /// <summary>
        /// function to open an IDER session and set devices to enabled for IDER. Will retry the connection
        /// the specified # of times before failing.
        /// </summary>
        /// <param name="drive0">string holding the name of the floppy disk drive 
        /// that will be used for IDER (eg "A:").  this can also hold the path
        /// to a binary floppy image file.  Must not be NULL.</param>
        /// <param name="drive1">string holding the name of the CD drive that
        /// will be used for IDER (eg "E:").  this can also hold a path to an
        /// ISO CD image file.  Must not be NULL.</param>
        /// <param name="state">used to specify timing options for the set device
        /// enable operation </param>
        public void StartIderSession(string drive0, string drive1, RedirectState state, int numRetries, IderTout timeout)
        {
            int Tries = 0;
            while (Tries <= numRetries)
            {
                try
                {
                    StartIderSession(drive0, drive1, state, timeout);
                    break;
                }
                catch (Exception)
                {
                    if (Tries >= numRetries)
                    {
                        throw;
                    }
                    Tries++;
                   //Log.WriteLine(LogPriority.Info, "Retrying IDER session");
                }
            }
        }

        /// <summary>
        /// Function to try and poll the active IDER session information Statistics
        /// </summary>
        /// <returns>AmtIdeStats object set with the latest IDER session statistics</returns>
        public AmtIdeStats GetRedirectionListenerState()
        {
            AmtIdeStats stats = IDERGetSessionStatistics(clientId);
            if (stats == null && IderActive == true)
            {
                IderActive = false;
            }
            if (stats != null && IderActive == true)
            {
                IderActive = true;
            }
            return stats;
        }

        /// <summary>
        /// Function to stop ider session.  Sets IDER device to disabled and
        /// attempts to close session 
        /// </summary>
        public void StopIderSession()
        {
            try
            {
                if (IderActive == true)
                {
                   //Log.WriteLine(LogPriority.Trace, "Active IDER session is being closed...", "AMTLib", Audience.Internal);
                    IDERSetDeviceState(clientId, RedirectOperations.IDER_DISABLE, RedirectState.IDER_SET_IMMEDIATELY);
                    IMRResult r;
                    r = ImrSdkWrapper.IMR_IDERCloseSession(clientId);
                    if (r == IMRResult.IMR_RES_OK)
                        IderActive = false;
                    else
                        throw new AmtRedirectionException(r, "Can't stop IDER session");
                }
            }
            finally
            {
                if (File.Exists(TrustedRootFile))
                    File.Delete(TrustedRootFile);

                if (File.Exists(PriavteCertFile))
                    File.Delete(PriavteCertFile);
            }

        }

        #endregion public IDER functions

        #region Private IDER methods
        /// <summary>
        /// Function to open IDER session with specified client for new TCP connection.
        /// If the client uses secure communications  TLS session will be negotiated with it.
        /// Also defines a proxy connection with MPS
        /// </summary>
        /// <param name="clientId">the ID of the Client to open session for</param>
        /// <param name="username">the users name</param>
        /// <param name="password">the users password</param>
        /// <param name="drive0">string holding the name of the floppy disk drive 
        /// that will be used for IDER (eg "A:").  this can also hold the path
        /// to a binary floppy image file.  Must not be NULL.</param>
        /// <param name="drive1">string holding the name of the CD drive that
        /// will be used for IDER (eg "E:").  this can also hold a path to an
        /// ISO CD image file.  Must not be NULL.</param>
        /// <param name="timeout">the timeout values to use for service</param>
        /// <returns>IMRResult return code</returns>
        private static IMRResult IDEROpenTCPSession(uint clientId, string username, string password, string drive0, string drive1, IderTout timeout)
        {
            byte[] login = new byte[256];
            UTF8Encoding.UTF8.GetBytes(username, 0, username.Length, login, 0);
            UTF8Encoding.UTF8.GetBytes(password, 0, password.Length, login, 128);

            //not using ide_timeout settings here
            //TODO figure out if ide_timeout settings support should be supported
            
            IntPtr data = IntPtr.Zero;

            data = Marshal.AllocHGlobal(6);

            byte[] rawdata = new byte[6];
            BitConverter.GetBytes((ushort)timeout.rx_timeout).CopyTo(rawdata, 0);
            BitConverter.GetBytes((ushort)timeout.tx_timeout).CopyTo(rawdata, 2);
            BitConverter.GetBytes((ushort)timeout.hb_interval).CopyTo(rawdata, 4);

            for (int i = 0; i < rawdata.Length; i++)
            {
                Marshal.WriteByte(data, i, rawdata[i]);
            }

            IMRResult r = ImrSdkWrapper.IMR_IDEROpenTCPSession(clientId, login, data, drive0, drive1);
            if (data != IntPtr.Zero) Marshal.FreeHGlobal(data);
            return r;
        }

        /// <summary>
        /// Function to open IDER session with specified client for new TCP connection.
        /// If the client uses secure communications  TLS session will be negotiated with it.
        /// Also defines a proxy connection with MPS
        /// </summary>
        /// <param name="clientId">the ID of the Client to open session for</param>
        /// <param name="username">the users name</param>
        /// <param name="password">the users password</param>
        /// <param name="drive0">string holding the name of the floppy disk drive 
        /// that will be used for IDER (eg "A:").  this can also hold the path
        /// to a binary floppy image file.  Must not be NULL.</param>
        /// <param name="drive1">string holding the name of the CD drive that
        /// will be used for IDER (eg "E:").  this can also hold a path to an
        /// ISO CD image file.  Must not be NULL.</param>
        /// <param name="proxy">SOCKSv5 proxy connection paramaters</param>
        /// <param name="timeout">the timeout values to use for service</param>
        /// <returns>IMRResult return code</returns>
        private IMRResult IDEROpenTCPSessionEx(uint clientId, string username, string password, string drive0, string drive1, ProxyInfo proxy, IderTout timeout)
        {
            byte[] paramEx = _Credentials.CreateTCPSessionParamsEx();
            
            IntPtr data = IntPtr.Zero;

            //not using ide_timeout settings here
            //TODO figure out if ide_timeout settings support should be supported

            data = Marshal.AllocHGlobal(6);

            byte[] rawdata = new byte[6];
            BitConverter.GetBytes((ushort)timeout.rx_timeout).CopyTo(rawdata, 0);
            BitConverter.GetBytes((ushort)timeout.tx_timeout).CopyTo(rawdata, 2);
            BitConverter.GetBytes((ushort)timeout.hb_interval).CopyTo(rawdata, 4);

            for (int i = 0; i < rawdata.Length; i++)
            {
                Marshal.WriteByte(data, i, rawdata[i]);
            }


            IMRResult r = ImrSdkWrapper.IMR_IDEROpenTCPSessionEx(clientId, paramEx, data, drive0, drive1);
            if (data != IntPtr.Zero) Marshal.FreeHGlobal(data);
            return r;
        }

        /// <summary>
        /// this function controls the client IDE device(s) state.  Devices can be 
        /// enabled and disabled through this function
        /// </summary>
        /// <param name="clientId">id of client to set state for</param>
        /// <param name="operation">specifes to enable or disable</param>
        /// <param name="state">specifies timing for state change</param>
        /// <returns></returns>
        private static bool IDERSetDeviceState(uint clientId, RedirectOperations operation, RedirectState state)
        {
            //note only first drive setting is used, second drive setting is 
            //not used and just set to default values
            MemoryStream MS = new MemoryStream();
            MS.Write(BitConverter.GetBytes((int)operation), 0, 4);
            MS.Write(BitConverter.GetBytes((int)state), 0, 4);
            MS.Write(BitConverter.GetBytes((int)RedirectOperations.IDER_NOP), 0, 4);
            MS.Write(BitConverter.GetBytes((int)RedirectState.IDER_SET_ONRESET), 0, 4);
            byte[] op = MS.ToArray();

            IntPtr resultbin = Marshal.AllocHGlobal(8);
            IMRResult r = ImrSdkWrapper.IMR_IDERSetDeviceState(clientId, op, resultbin);
            int ret = Marshal.ReadInt32(resultbin, 0);
            Marshal.FreeHGlobal(resultbin);
            return (r == IMRResult.IMR_RES_OK && ret == 1);
        }

        /// <summary>
        /// This function polls the active IDER session for the corresponding client
        /// and fills the stat structure with the latest data
        /// </summary>
        /// <param name="clientId">id of the client to query for</param>
        /// <returns>AmtIdeStats object set with latest stats</returns>
        private static AmtIdeStats IDERGetSessionStatistics(uint clientId)
        {
            IntPtr statsbin = Marshal.AllocHGlobal(40);
            IMRResult r = ImrSdkWrapper.IMR_IDERGetSessionStatistics(clientId, statsbin);
            if (r != IMRResult.IMR_RES_OK)
            {
                Marshal.FreeHGlobal(statsbin);
                return null;
            }

            AmtIdeStats stats = new AmtIdeStats();
            stats.error_state = Marshal.ReadInt32(statsbin, 0) == 0;
            stats.data_transfer = Marshal.ReadInt32(statsbin, 4) == 0;
            stats.num_reopen = (ushort)Marshal.ReadInt32(statsbin, 8);
            stats.num_error = (ushort)Marshal.ReadInt32(statsbin, 12);
            stats.num_reset = (ushort)Marshal.ReadInt32(statsbin, 16);
            stats.last_cmd_length = (uint)Marshal.ReadInt32(statsbin, 20);
            stats.data_sent = (uint)Marshal.ReadInt32(statsbin, 24);
            stats.data_received = (uint)Marshal.ReadInt32(statsbin, 28);
            stats.packets_sent = (uint)Marshal.ReadInt32(statsbin, 32);
            stats.packets_received = (uint)Marshal.ReadInt32(statsbin, 36);

            Marshal.FreeHGlobal(statsbin);

            return stats;
        }

        #endregion
    }
}
