﻿// ----------------------------------------------------------------------------
//
// Copyright (c) 2009 Intel Corporation. All Rights Reserved.
//
// The source code contained or described herein and all documents related to 
// the source code ("Material") are owned by Intel Corporation or its suppliers 
// or licensors. Title to the Material remains with Intel Corporation or its 
// suppliers and licensors. The Material may contain trade secrets and 
// proprietary and confidential information of Intel Corporation and its 
// suppliers and licensors, and is protected by worldwide copyright and trade 
// secret laws and treaty provisions. No part of the Material may be used, 
// copied, reproduced, modified, published, uploaded, posted, transmitted, 
// distributed, or disclosed in any way without Intel’s prior express written 
// permission. 
//
// No license under any patent, copyright, trade secret or other intellectual 
// property right is granted to or conferred upon you by disclosure or delivery 
// of the Materials, either expressly, by implication, inducement, estoppel or 
// otherwise. Any license under such intellectual property rights must be 
// express and approved by Intel in writing.
//
// * Third Party trademarks are the property of their respective owners.
//
// Unless otherwise agreed by Intel in writing, you may not remove or alter this 
// notice or any other notice embedded in Materials by Intel or Intel’s 
// suppliers or licensors in any way.
//
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using MOF_CLASSES;

namespace Intel.vPro.AMT
{
    public static class AmtRedirectionService
    {

        public static bool GetRedirectionServiceStatus(AmtSystem system, int MaxTry)
        {
            if (MaxTry <= 0)
            {
                throw new AmtNetworkInterfaceException("Unable to get the redirection status");               
            }

            MaxTry--;
            bool state = false;
            PT_STATUS status;

            try
            {
                if (AmtInterfaceSettings.ApiInterface == ApiInterface.UseWSMAN)
                {
                    AMT_RedirectionServiceType rs = (AMT_RedirectionServiceType)system.Wsman.Get(typeof(AMT_RedirectionServiceType));
                    state = rs.ListenerEnabled;
                    status = PT_STATUS.SUCCESS; 
                }
                else
                {
                    RedirectionService rs = system.RedirectionService;
                    status = (PT_STATUS)rs.GetRedirectionListenerState(out state);
                }
            }
            catch
            {
                return GetRedirectionServiceStatus(system, MaxTry);
            }

            if (status != PT_STATUS.SUCCESS)
            {
                throw new AmtNetworkInterfaceException("Unable to get the redirection status", status);
            }

            return state; 
        }

        public static void SetRedirectionServiceStatus(AmtSystem system, bool enabled, int MaxTry)
        {
            if (MaxTry <= 0)
            {
                throw new AmtNetworkInterfaceException("Unable to set the redirection status");
            }

            MaxTry--;

            PT_STATUS status;

            try
            {
                if (AmtInterfaceSettings.ApiInterface == ApiInterface.UseWSMAN)
                {
                    AMT_RedirectionServiceType rs = (AMT_RedirectionServiceType)system.Wsman.Get(typeof(AMT_RedirectionServiceType));
                    rs.ListenerEnabled = enabled;
                    system.Wsman.Put(rs);
                    status = PT_STATUS.SUCCESS;
                }
                else
                {
                    RedirectionService rs = system.RedirectionService;
                    status = (PT_STATUS)rs.SetRedirectionListenerState(enabled);
                }

                if (status != PT_STATUS.SUCCESS)
                {
                    throw new AmtNetworkInterfaceException("Unable to set the redirection status", status);
                }
            }
            catch
            {
                SetRedirectionServiceStatus(system, enabled, MaxTry); 
            }
        }

    }
}
