// -----------------------------------------------------------------------------
//
// Copyright (c) 2009 Intel Corporation. All Rights Reserved.
//
// The source code contained or described herein and all documents related to 
// the source code ("Material") are owned by Intel Corporation or its suppliers 
// or licensors. Title to the Material remains with Intel Corporation or its 
// suppliers and licensors. The Material may contain trade secrets and 
// proprietary and confidential information of Intel Corporation and its 
// suppliers and licensors, and is protected by worldwide copyright and trade 
// secret laws and treaty provisions. No part of the Material may be used, 
// copied, reproduced, modified, published, uploaded, posted, transmitted, 
// distributed, or disclosed in any way without Intel’s prior express written 
// permission. 
//
// No license under any patent, copyright, trade secret or other intellectual 
// property right is granted to or conferred upon you by disclosure or delivery 
// of the Materials, either expressly, by implication, inducement, estoppel or 
// otherwise. Any license under such intellectual property rights must be 
// express and approved by Intel in writing.
//
// * Third Party trademarks are the property of their respective owners.
//
// Unless otherwise agreed by Intel in writing, you may not remove or alter this 
// notice or any other notice embedded in Materials by Intel or Intel’s 
// suppliers or licensors in any way.
//
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;

//using Intel.Logging;

namespace Intel.vPro.AMT
{
    public class ImrSdkWrapper
    {
        #region IMRSDK Imports
        /// <summary>
        /// IMRSDK DLL functions from the Intel(R) AMT SDK.  Refer to the Redirection Library Design Guide for
        /// more info.
        /// </summary>
        [DllImport("imrsdk.dll")]
        internal static extern IMRResult IMR_Init(IntPtr versionInfo, [MarshalAs(UnmanagedType.LPStr)] string iniFilePath);
        [DllImport("imrsdk.dll")]
        internal static extern IMRResult IMR_Close();
        [DllImport("imrsdk.dll")]
        internal static extern IMRResult IMR_GetErrorStringLen(IMRResult res, out int len);
        [DllImport("imrsdk.dll")]
        internal static extern IMRResult IMR_GetErrorString(IMRResult res, IntPtr errorString);
        [DllImport("imrsdk.dll")]
        internal static extern IMRResult IMR_SetCertificateInfo([MarshalAs(UnmanagedType.LPStr)] string root_cert, [MarshalAs(UnmanagedType.LPStr)] string private_cert, [MarshalAs(UnmanagedType.LPStr)] string cert_pass);
        [DllImport("imrsdk.dll")]
        internal static extern IMRResult IMR_AddClient(IMRClientType type, [MarshalAs(UnmanagedType.LPStr)] string ip, byte[] guid, out uint clientId);
        [DllImport("imrsdk.dll")]
        internal static extern IMRResult IMR_RemoveClient(uint clientId);
        [DllImport("imrsdk.dll")]
        internal static extern IMRResult IMR_RemoveAllClients();
        [DllImport("imrsdk.dll")]
        internal static extern IMRResult IMR_GetAllClients(IntPtr client_list, out int list_size);
        [DllImport("imrsdk.dll")]
        internal static extern IMRResult IMR_GetClientInfo(uint clientId, IntPtr info);
        [DllImport("imrsdk.dll")]
        internal static extern IMRResult IMR_SOLOpenTCPSession(uint clientId, byte[] loginparams, IntPtr touts, IntPtr loopback);
        [DllImport("imrsdk.dll")]
        internal static extern IMRResult IMR_SOLOpenTCPSessionEx(uint clientId, byte[] paramsEx, IntPtr touts, IntPtr loopback);
        [DllImport("imrsdk.dll")]
        internal static extern IMRResult IMR_SOLCloseSession(uint clientId);
        [DllImport("imrsdk.dll")]
        internal static extern IMRResult IMR_SOLSendText(uint clientId, byte[] data, int len);
        [DllImport("imrsdk.dll")]
        internal static extern IMRResult IMR_SOLReceiveText(uint clientId, IntPtr data, ref int len);
        [DllImport("imrsdk.dll")]
        internal static extern IMRResult IMR_IDEROpenTCPSession(uint clientId, byte[] sessionparams, IntPtr touts, [MarshalAs(UnmanagedType.LPStr)] string drive0, [MarshalAs(UnmanagedType.LPStr)] string drive1);
        [DllImport("imrsdk.dll")]
        internal static extern IMRResult IMR_IDEROpenTCPSessionEx(uint clientId, byte[] paramsEx, IntPtr touts, [MarshalAs(UnmanagedType.LPStr)] string drive0, [MarshalAs(UnmanagedType.LPStr)] string drive1);
        [DllImport("imrsdk.dll")]
        internal static extern IMRResult IMR_IDERCloseSession(uint clientId);
        [DllImport("imrsdk.dll")]
        internal static extern IMRResult IMR_IDERClientFeatureSupported(uint clientId, IntPtr supportedfeatures);
        [DllImport("imrsdk.dll")]
        internal static extern IMRResult IMR_IDERGetDeviceState(uint clientId, IntPtr state);
        [DllImport("imrsdk.dll")]
        internal static extern IMRResult IMR_IDERSetDeviceState(uint clientId, byte[] IDERDeviceCmd, IntPtr IDERDeviceResult);
        [DllImport("imrsdk.dll")]
        internal static extern IMRResult IMR_IDERGetSessionStatistics(uint clientId, IntPtr IDERStatistics);

        #endregion

        #region Private Members

        /// <summary>
        /// static instance of this class used as a singelton object.  using this allows
        /// the class to provide static methods that can be used without explictly initilizing
        /// this class, but forces an instance of the class to be created so a destructor
        /// will be called
        /// </summary>
        private static ImrSdkWrapper _singelton;

        /// <summary>
        /// counter to count how many times library has been called to initilize
        /// </summary>
        private static int libRefCounter = 0;

        /// <summary>
        /// Global synchronization object.
        /// </summary>
        private static object thisLock = new Object();

        /// <summary>
        /// path for the temp folder where the certificate pem files should be stored
        /// </summary>
        private static string _CertPemDir = Path.Combine(Path.GetTempPath(), Path.GetFileNameWithoutExtension(Path.GetRandomFileName()));

        #endregion

        #region Public Properties
        
        public static string CertPemDir
        {
            get { return _CertPemDir; }
        }
        #endregion

        public static bool ImrSdkDllPresent
        {
            get
            {
                try
                {
                    return File.Exists("imrsdk.dll");
                }
                catch
                {
                    return false;
                }
            }
        }

        #region Constructor/Destructor

        /// <summary>
        /// private construtor so it can only be called by this class
        /// </summary>
        private ImrSdkWrapper()
        {
        }

        /// <summary>
        /// destructor, called when the singelton object is destroyed, since this is a 
        /// static object it will only be when this library is unloaded (for example
        /// when calling app is closing)
        /// </summary>
        ~ImrSdkWrapper()
        {
            IMR_Close();

            //remove any remaining certificate PEM files in the temp folder
            try
            {
                Directory.Delete(CertPemDir, true);
            }
            catch
            {
               //Log.WriteLine(LogPriority.Trace, "error deleting temp directory for SOL/IDER certificates");
            }
        }
   
        #region Internal Methods (exposed to this assembly only)
        /// <summary>
        /// adds single client to IMR Library list, uses class data members
        /// for paramaters used to add client
        /// </summary>
        /// <returns></returns>
        internal static uint AddClient(AmtSystem system, AmtCredentials connectionInfo)
        {
            //make sure IMRSDK has been initilized
            InitImrSdkWrapper(); 

            IMRResult r;
            uint clientId;

            Debug.Assert((system != null), "AmtSystem object cannot be null!");
            Debug.Assert((connectionInfo != null), "AmtCredentials object cannot be null!");

            lock (thisLock)
            {
                IMRClientType clientType = IMRClientType.CLI_TCP;
                if (connectionInfo.UseTls)
                    clientType = IMRClientType.CLI_TLS;
                r = IMR_AddClient(clientType, system.HostName, Guid.NewGuid().ToByteArray(), out clientId);
                if (r == IMRResult.IMR_RES_DUPLICATE_CLIENT)
                {
                    IPAddress tempIP;
                    if (!IPAddress.TryParse(system.HostName, out tempIP))
                        tempIP = Dns.GetHostEntry(system.HostName).AddressList[0]; 

                    uint[] clientlist = GetClientList();
                    foreach (uint id in clientlist)
                    {
                        ClientInfo c = GetClientInfo(id);
                        if (c.ip.Equals(tempIP))
                        {
                            clientId = id;
                            break;
                        }
                    } // END foreach (uint id in clientlist)

                    return clientId;
                }
                else if (r != IMRResult.IMR_RES_OK)
                {
                    // AddClient failed.
                    clientId = uint.MaxValue;
                    return clientId;
                }

            } // END lock (thisLock)

            return clientId;
        }

        /// <summary>
        /// Initializes the certificate information for the redirection DLL
        /// </summary>
        /// <param name="connectionInfo">AmtCredentials object that contains the connection info</param>
        /// <param name="TrustedRootFile">path for TLS certificate PEM file to be saved to</param>
        /// <param name="PrivateCertFile">path for MTLS certificate PEM file to be saved to</param>
        internal static void SetupCertificates(AmtCredentials connectionInfo, string TrustedRootFile, string PrivateCertFile)
        {
            //make sure the IRMSDK has been initilized
            InitImrSdkWrapper(); 

            if (connectionInfo.UseTls)
            {
                // Attempt to create the trusted root certificates file.
                IMRResult r;

               //Log.WriteLine(LogPriority.Trace, "Initializing TLS...");
                CertificateOperations.AllTrustedRootsToPemFile(TrustedRootFile);

                if (File.Exists(TrustedRootFile))
                {
                    // TLS Support. If MTLS is used, convert cert to PEM first
                    if (connectionInfo.MutualCertificate != null)
                    {
                       //Log.WriteLine(LogPriority.Trace, "Initializing Mutual TLS certificates");
                        // Convert the certificate to PEM
                        CertificateOperations.CertAndKeyChainToPemFile(
                            connectionInfo.MutualCertificate,
                            PrivateCertFile,
                            null);
                        r = ImrSdkWrapper.IMR_SetCertificateInfo(TrustedRootFile, PrivateCertFile, null);
                    }
                    else
                    {
                       //Log.WriteLine(LogPriority.Trace, "Initializing TLS certificates");
                        r = ImrSdkWrapper.IMR_SetCertificateInfo(TrustedRootFile, null, null);
                    }

                    if (r != IMRResult.IMR_RES_OK)
                    {
                        throw new AmtRedirectionException(r, "IMR_SetCertificateInfo() failed");
                    }

                }
            }
        }
        #endregion

        #region Private Methods

        /// <summary>
        /// Initializes the Intel Management Redirection library (IMRSDK.DLL)
        /// </summary>
        private static void InitImrSdkWrapper()
        {
            if (libRefCounter > 0)
            {
                libRefCounter++;
                return;
            }

            lock (thisLock)
            {
                //initilize the static singelton object with a new instance of this class
                //this will create an instance of an object, so that the destructor will be 
                //called when app is unloading
                _singelton = new ImrSdkWrapper();

                libRefCounter++;

                // This is the first instance of this DLL, so, lets initialize it.
                if (File.Exists("imrsdk.ini") == false)
                {
                    byte[] buf = UTF8Encoding.UTF8.GetBytes("[COMMON]\r\nDebug_Level=2\r\nStorage_Enabled=0\r\n");
                    FileStream f = new FileStream("imrsdk.ini", FileMode.Create);
                    f.Write(buf, 0, buf.Length);
                    f.Close();
                }

                IntPtr str = Marshal.AllocHGlobal(4);
                IMRResult r = IMR_Init(str, "imrsdk.ini");
                short v1 = Marshal.ReadInt16(str, 0);
                short v2 = Marshal.ReadInt16(str, 2);
                float verInfo = (((float)v2) / 10) + v1;
                Marshal.FreeHGlobal(str);

                Debug.Print("IMRSDK.DLL Version: {0}", verInfo);

                // Lets go ahead and remove all the clients, there should be none anyway, but since
                // this DLL keeps a client list on file, it's possible to have clients already setup.
                uint[] clientlist = GetClientList();
                if (clientlist != null && clientlist.Length != 0)
                {
                    foreach (uint id in clientlist)
                    {
                        IMR_RemoveClient(id);
                    } // END foreach (uint id in clientlist)
                }

                //if directory does not exist, create a new directory
                if (!Directory.Exists(CertPemDir))
                {
                    Directory.CreateDirectory(CertPemDir);
                }

            } // END lock (thisLock)
        }
        #endregion

        /// <summary>
        /// retrives the list of clients in IMR initilized client list
        /// </summary>
        /// <returns>list of ClientIDs from IMR client list</returns>
        private static uint[] GetClientList()
        {
            IMRResult r;
            int list_size;
            r = IMR_GetAllClients(IntPtr.Zero, out list_size);
            if (r != IMRResult.IMR_RES_OK) return null;
            IntPtr ulongarray = Marshal.AllocHGlobal(4 * list_size);
            r = IMR_GetAllClients(ulongarray, out list_size);
            if (r != IMRResult.IMR_RES_OK)
            {
                Marshal.FreeHGlobal(ulongarray);
                return null;
            }
            uint[] arr = new uint[list_size];
            for (int i = 0; i < list_size; i++)
            {
                arr[i] = (uint)Marshal.ReadInt32(ulongarray, i * 4);
            }
            Marshal.FreeHGlobal(ulongarray);
            return arr;
        }

        /// <summary>
        /// retrives the client information for the specified client
        /// </summary>
        /// <param name="clientId">clientID to retrieve client informtaion for</param>
        /// <returns>ClientInfo object set with client info</returns>
        private static ClientInfo GetClientInfo(uint clientId)
        {
            lock (thisLock)
            {
                IMRResult r;
                IntPtr info = Marshal.AllocHGlobal(148);
                r = IMR_GetClientInfo(clientId, info);
                ClientInfo ci = new ClientInfo();
                ci.type = (IMRClientType)Marshal.ReadInt32(info, 0);
                string ipstr = Marshal.PtrToStringAnsi(info, 132);
                ipstr = ipstr.Substring(4);
                ipstr = ipstr.Substring(0, ipstr.IndexOf("\0"));
                IPAddress[] addrs = System.Net.Dns.GetHostAddresses(ipstr);
                if (addrs == null || addrs.Length == 0)
                {
                    Marshal.FreeHGlobal(info);
                    return new ClientInfo();
                }
                ci.ip = addrs[0];

                byte[] x = new byte[16];
                for (int i = 0; i < 16; i++)
                {
                    x[i] = Marshal.ReadByte(info, i + 132);
                }
                ci.guid = new Guid(x);

                Marshal.FreeHGlobal(info);
                return ci;
            } // END lock (thisLock)
        }
        #endregion
    }
}
