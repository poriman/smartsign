// -----------------------------------------------------------------------------
//
// Copyright (c) 2009 Intel Corporation. All Rights Reserved.
//
// The source code contained or described herein and all documents related to 
// the source code ("Material") are owned by Intel Corporation or its suppliers 
// or licensors. Title to the Material remains with Intel Corporation or its 
// suppliers and licensors. The Material may contain trade secrets and 
// proprietary and confidential information of Intel Corporation and its 
// suppliers and licensors, and is protected by worldwide copyright and trade 
// secret laws and treaty provisions. No part of the Material may be used, 
// copied, reproduced, modified, published, uploaded, posted, transmitted, 
// distributed, or disclosed in any way without Intel’s prior express written 
// permission. 
//
// No license under any patent, copyright, trade secret or other intellectual 
// property right is granted to or conferred upon you by disclosure or delivery 
// of the Materials, either expressly, by implication, inducement, estoppel or 
// otherwise. Any license under such intellectual property rights must be 
// express and approved by Intel in writing.
//
// * Third Party trademarks are the property of their respective owners.
//
// Unless otherwise agreed by Intel in writing, you may not remove or alter this 
// notice or any other notice embedded in Materials by Intel or Intel’s 
// suppliers or licensors in any way.
//
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Runtime.InteropServices;

namespace Intel.vPro.AMT
{
    /// <summary>
    /// Return codes that can be returned by calls to the imrsdk.dll        
    /// </summary>
    internal enum IMRResult
    {
        IMR_RES_OK,                 //sucess
        IMR_RES_ERROR,              //unspecified error
        IMR_RES_INVALID_PARAMETER,  //invalid paramter
        IMR_RES_NOT_INITIALIZED,    //dll not yet initilitzed
        IMR_RES_ALREADY_INITIALIZED,//dll already initilized
        IMR_RES_MEMALLOC_FAILED,    //memory allocation has failed, not enough memory
        IMR_RES_UNSUPPORTED,        //operation is unsopported by library
        IMR_RES_CLIENT_NOT_FOUND,   //clientID does not exist in the libraries list
        IMR_RES_DUPLICATE_CLIENT,   //client already exists in the libraries list
        IMR_RES_CLIENT_NOT_ACTIVE,  //error in library: client is in list but not good
        IMR_RES_CLIENT_ACTIVE,      //error in library: client is in list but not good
        IMR_RES_SESSION_ALREADY_OPEN, //a session is already opened for the specified client
        IMR_RES_SESSION_CLOSED,     //there is no open session with the specified client
        IMR_RES_SOCKET_ERROR,       //a socket error has occurred
        IMR_RES_UNKNOWN_PROTOCOL,   //error in library: protocol handlers
        IMR_RES_PROTOCOL_ALREADY_REGISTERED,    //error in library: protocol handlers
        IMR_RES_PENDING,            //internal error has occurred while trying to send packet
        IMR_RES_UNEXPECTED_PACKET,  //an unexpected protocol packet has been received
        IMR_RES_TIMEOUT,            //a timeout has occurred while trying to execute operation
        IMR_RES_CORRUPT_PACKET,     //corrupted or malformed protocol packet has been received
        IMR_RES_OS_ERROR,           //a os error has occurred
        IMR_RES_IDER_VERSION_NOT_SUPPORTED, //the client uses n IDER protocol version incompatilbe with this library version
        IMR_RES_IDER_COMMAND_RUNNING,       //an IDER command is in progress
        IMR_RES_STORAGE_FAILURE,    //an error occurred while trying to store client info
        IMR_RES_UNKNOWN,            //an unknown library result codde was specified
        IMR_RES_AUTH_FAILED,        //authentication with the client has failed
        IMR_RES_CLIENT_TYPE_UNKNOWN,//the client type is unknown
        IMR_RES_CLIENT_BUSY,        //the client already has an open session with another application
        IMR_RES_CLIENT_UNSUPPORTED, //the library attempted to connect to a host that does not suppport IDER
        IMR_RES_CLIENT_ERROR,       //general error when trying to establish an IDER session
        IMR_RES_NOT_ENOUGH_SPACE,   //the function was provided with too small a buffer
        IMR_RES_SESSION_LOOPBACK,   //the operaiont cannot execute because SOL is in loopback mode
        IMR_RES_TLS_CONNECTION_FAILED,      //failed to establish a TLS connection with client (may be certificate)
        IMR_RES_SOCKS_ERROR,        //SOCKv5 proxy returned an error
        IMR_RES_SOCKS_AUTH_FAIL,    //library failed to conect due to authentication error
        IMR_RES_COUNT,              // must be last entry
    };

    /// <summary>
    /// PT_Status error codes returned by network interface calls
    /// </summary>
    internal enum PT_STATUS
    {
        SUCCESS = 0x0,
        INTERNAL_ERROR = 0x1,
        INVALID_PT_MODE = 0x3,
        INVALID_NAME = 0xC,
        INVALID_BYTE_COUNT = 0xF,
        NOT_PERMITTED = 0x10,
        MAX_LIMIT_REACHED = 0x17,
        INVALID_AUTH_TYPE = 0x18,
        INVALID_DHCP_MODE = 0x1A,
        INVALID_IP_ADDRESS = 0x1B,
        INVALID_DOMAIN_NAME = 0x1C,
        INVALID_PROVISIONING_STATE = 0x20,
        INVALID_TIME = 0x22,
        INVALID_INDEX = 0x23,
        INVALID_PARAMETER = 0x24,
        INVALID_NETMASK = 0x25,
        FLASH_WRITE_LIMIT_EXCEEDED = 0x26,
        NETWORK_IF_ERROR_BASE = 0x800,
        UNSUPPORTED_OEM_NUMBER = 0x801,
        UNSUPPORTED_BOOT_OPTION = 0x802,
        INVALID_COMMAND = 0x803,
        INVALID_SPECIAL_COMMAND = 0x804,
        INVALID_HANDLE = 0x805,
        INVALIDpassword = 0x806,
        INVALID_REALM = 0x807,
        STORAGE_ACL_ENTRY_IN_USE = 0x808,
        DATA_MISSING = 0x809,
        DUPLICATE = 0x80A,
        EVENTLOG_FROZEN = 0x80B,
        PKI_MISSING_KEYS = 0x80C,
        PKI_GENERATING_KEYS = 0x80D,
        INVALID_KEY = 0x80E,
        INVALID_CERT = 0x80F,
        CERT_KEY_NOT_MATCH = 0x810,
        MAX_KERB_DOMAIN_REACHED = 0x811,
        UNSUPPORTED = 0x812,
        INVALID_PRIORITY = 0x813,
        NOT_FOUND = 0x814,
        INVALID_CREDENTIALS = 0x815,
        INVALID_PASSPHRASE = 0x816,
        NO_ASSOCIATION = 0x818
    }

    /// <summary>
    /// Each client has a type (according to its capabilities), which must 
    /// be explicitly provided to the library when the client is first added.
    /// </summary>
    internal enum IMRClientType
    {
        /// <summary>
        /// An Intel(R) AMT (non-secure) client.
        /// </summary>
        CLI_TCP = 1,
        /// <summary>
        /// An Intel(R) AMT secure client.
        /// </summary>
        CLI_TLS = 2,
    };

    /// <summary>
    /// This enumeration defines possible timing options for the dll function call 
    /// IMR_IDERSetDeviceState 
    /// </summary>
    public enum RedirectState
    {
        /// <summary>
        /// Change of device at PCI reset.  This is the preferred option to
        /// synchronize IDE device enable/disable.  
        /// </summary>
        IDER_SET_ONRESET,
        /// <summary>
        /// change the specified device settings in a graceful manner.  if there
        /// is a pending IDE command request to change state will be denied
        /// </summary>
        IDER_SET_GRACEFULLY,
        /// <summary>
        /// changes the specified cable register settings imediadely, whether
        /// thereis a pending IDE command or not.
        /// </summary>
        IDER_SET_IMMEDIATELY,
    };

    /// <summary>
    /// This enumeration defines possible commands sent by the caller to the client to
    /// change the state of the IDER devices
    /// </summary>
    internal enum RedirectOperations
    {
        IDER_ENABLE,    //request from the client to enable IDER devices
        IDER_DISABLE,   //request from the client to disable IDER devices
        IDER_NOP        //request from the client to not chane IDER device state (not supported)
    };

    /// <summary>
    /// Determines if the ProxySettings strucutre should be used or not
    /// </summary>
    internal enum PROXY_TYPE
    {
        PROXY_NO_PROXY, //proxySettings structure should be ignored
        PROXY_SOCKS_V5  //proxySettings structure describes a SOCKSv5 proxy
    };

    /// <summary>
    /// Provides information for a specific client.
    /// </summary>
    [StructLayout(LayoutKind.Sequential)]
    internal struct ClientInfo
    {
        /// <summary>
        /// The client’s type (CLI_TCP, CLI_TLS).
        /// </summary>
        public IMRClientType type;
        /// <summary>
        /// A NULL-terminated string holding either the client IP address in 
        /// numbers and dots notation (nnn.nnn.nnn.nnn) or the valid hostname
        /// of the client.
        /// </summary>
        public IPAddress ip;
        /// <summary>
        /// The Globally Unique ID of the client.
        /// </summary>
        public Guid guid;
    };

    /// <summary>
    /// defines SOCKSv5 proxy settings used to connect to client via an MPS
    /// </summary>
    [Serializable]
    public class ProxyInfo
    {
        public string Server = null;   //MPS hostname or IPv4 IP address
        public ushort Port = 0;        //port used to accept SOCKSv5 connection
        public string User = null;     //proxy username
        public string Password = null; //proxy password
    }

    /// <summary>
    /// This structure is used as an output paramater when getting the IDER
    /// session statistics
    /// </summary>
    public class AmtIdeStats
    {
        public bool error_state;    //true if sessio is in error state
        public bool data_transfer;  //true if there is a read/write in progress
        public ushort num_reopen;   //number of session re-opens due to error recovery
        public uint num_error;      //number of erroroccured messages recieved
        public uint num_reset;      //number of resetoccured messages recieved
        public uint last_cmd_length;    //last data transfer length in bytes
        public uint data_sent;      //byets of data sent to the client
        public uint data_received;  //bytes of data received from client
        public uint packets_sent;   //messages sent during session
        public uint packets_received;   //messages received during sesssion
    };
}
