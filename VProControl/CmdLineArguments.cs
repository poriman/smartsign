﻿//----------------------------------------------------------------------------
//
//  Copyright (C) Intel Corporation, 2011 - 2012.
//
//  File:       CmdLineArguments.cs 
//
//  Contents:   This file is an infrastructure for the entire WSMan sample. 
//              It contains a parser for the information inputted by the user 
//              via the command line arguments.  
//
//----------------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using Intel.Management.Wsman;
using System.Text.RegularExpressions;

namespace Utils
{
    /// <summary>
    /// This class used to parse command line arguments. 
    /// Parameters can be given in the follwing format: [Parameter Value] pair
    /// Parameter starting with - or / and Value separated from a Name with a space. 
    /// </summary>
    public class CmdLineArguments
    {
        #region CONSTANTS

        //command line Flags
        public const string OPT_HOST = "host";
        public const string OPT_PASS = "pass";
        public const string OPT_USER = "user";
        public const string OPT_SECURE = "tls";
        public const string OPT_WIRELESS = "wireless";
        public const string OPT_KRB = "krb";
        public const string OPT_CERT = "certname";
        public const string OPT_VERBOSE = "verbose";
        public const string OPT_PROXY = "proxy";
        public const string OPT_PROXY_USER = "proxyuser";
        public const string OPT_PROXY_PASSWORD = "proxypass";
        public const string OPT_LOCAL = "local";

        public const string OPT_ENUMERATE = "getallcontexts";
        public const string OPT_SUBSCRIBE = "subscribe";
        public const string OPT_UNSUBSCRIBE = "unsubscribe";
        public const string OPT_IS_MACHINE_CONNECTED = "ismachineconnected";

        // Exit Codes Types
        private enum exitCodes
        {
            EXIT_SUCCESS = 0,
            EXIT_FAILURE,
            EXIT_USAGE,
            EXIT_COMMUNICATION_ERROR,
            EXIT_ARGUMENT_ERROR,
        }

        private static List<string> PreDefinedFlags = new List<string>();
        private static List<string> StandAloneArguments = new List<string>();

        static CmdLineArguments()
        {
            InitStandAloneArguments();
        }

        private static void InitStandAloneArguments()
        {
            string[] tmpFlags = new[] { OPT_HOST, OPT_PASS, OPT_USER, OPT_SECURE, OPT_WIRELESS, OPT_KRB, OPT_CERT, OPT_VERBOSE, OPT_PROXY, OPT_PROXY_USER, OPT_PROXY_PASSWORD, OPT_LOCAL, OPT_ENUMERATE, OPT_SUBSCRIBE, OPT_UNSUBSCRIBE, OPT_IS_MACHINE_CONNECTED };
            PreDefinedFlags.AddRange(tmpFlags);
            StandAloneArguments.AddRange(tmpFlags);
        }

        /// <summary>
        /// This enum will be used if the calling
        /// application would like to disable specific conditions
        /// </summary>
        [Flags]
        public enum ExclusionList
        {
            None = 0,
            /// <summary>
            /// The following value should disable the condition 
            /// of (username & pwd) | kerberos is needed
            /// Notice the next enum values should be 2, 4, 8 ... 
            /// </summary>
            DISABLE_AUTHENTICATION_CHECK = 1
        };

        #endregion

        #region PUBLIC_FUNCTIONS

        /// <summary>
        /// Creates a usage explanation string
        /// </summary>
        /// <returns> string, the usage string</returns>
        public string CreateUsage(string assemblyName, bool verbose, bool hasLocalOption)
        {
            string usage = CreateUsage(assemblyName, verbose, hasLocalOption, false);
            return usage;
        }

        /// <summary>
        /// Creates a usage explanation string
        /// </summary>
        /// <returns> string, the usage string</returns>
        public string CreateUsage(string assemblyName, bool verbose)
        {
            string usage = CreateUsage(assemblyName, verbose, false);
            return usage;
        }

        /// <summary>
        /// Creates a usage explanation string
        /// </summary>
        /// <returns> string, the usage string</returns>
        public string CreateUsage(string assemblyName, string additionalData, bool verbose)
        {
            return CreateUsage(assemblyName, additionalData, verbose, false);
        }

        /// <summary>
        /// Creates a usage explanation string
        /// </summary>
        /// <returns> string, the usage string</returns>
        public string CreateUsage(string assemblyName, string additionalData, bool verbose, bool hasWirelessOption)
        {
            string usageStr = CreateUsage(assemblyName, verbose, false, hasWirelessOption);

            usageStr += "\n" + additionalData + "\n";

            return usageStr;
        }

        public void SetExclusionList(ExclusionList exclusionList)
        {
            _exclusionList = exclusionList;
        }

        /// <summary>
        /// Creates a usage explanation string
        /// </summary>
        /// <returns> string, the usage string</returns>
        public string CreateUsage(string assemblyName, bool verbose, bool hasLocalOption, bool hasWirelessOption)
        {
            string usageStr = String.Empty;
            string optionsStr = String.Empty;
            bool optionsExist = false;
            List<string> firstOption = new List<string>();

            // Go over the options (if any)
            IDictionaryEnumerator e = _arguments.GetEnumerator();

            int maxOptionNameLength = 0;
            while (e.MoveNext())
            {
                Argument arg = _arguments[e.Key.ToString()];
                if (IsCommonArg(arg.Name) == false)
                {
                    if (arg.Name.Length > maxOptionNameLength)
                    {
                        maxOptionNameLength = arg.Name.Length;
                    }
                }
            }

            usageStr += "Usage:\n";
            usageStr += "\t " + assemblyName;
            usageStr += " <opt>";
            if (verbose)
                usageStr += " [-verbose]";
            if (hasWirelessOption)
                usageStr += " [-wireless]";
            usageStr += " -host <Hostname> [-user <user name> -pass <password>] [-krb] [-tls -certName <certName>]"
                     + " [-proxy <host:port> [-proxyUser <proxy user> -proxyPass <proxy password>]]";

            if (hasLocalOption)
                usageStr += " [-local]";

            usageStr += createUsageForStandAloneComponents();

            usageStr += "\n\n";

            e = _arguments.GetEnumerator();
            while (e.MoveNext())
            {
                Argument arg = _arguments[e.Key.ToString()];
                if (IsCommonArg(arg.Name) == false)
                {
                    optionsExist = true;
                    optionsStr += "\t -" + arg.Name;
                    // Gets the option that doesn't have a <value>
                    if (!arg.HasValue)
                    {
                        firstOption.Add(arg.Name);

                    }
                    if (arg.HasValue)
                        optionsStr += " <value>";
                    for (int i = arg.Name.Length; i <= maxOptionNameLength; i++)
                    {
                        optionsStr += " ";
                    }
                    optionsStr += ":\t" + arg.Description + "\n";
                }
            }

            if (optionsExist)
            {
                usageStr += "Where <opt> is :\n";
                usageStr += optionsStr;
            }

            if (verbose)
                usageStr += "\nIf –verbose is used the sample will display additional output information." + "\n";

            usageStr += "\nIf -user <username> -pass <password> are defined and -krb isn't defined the Digest authentication scheme is used." + "\n\n"
                   + "If –krb is used the Kerberos authentication scheme will be attempted." + "\n"
                   + "\tIf specified, the kerberos user should be given in domain\\name format." + "\n\n"
                   + "If –tls is used the sample application will perform server authentication. This option is required if the Intel AMT platform is configured for TLS." + "\n"
                   + "Use –certName <name> to specify the client certificate's Common Name (CN)." + "\n\t"
                   + "Used in TLS Mutual Authentication mode only." + "\n\t"
                   + "If this option is not specified the sample application will search the" + "\n\t"
                   + "certificate store for a client certificate matching Intel(R) AMT" + "\n\t" + "requirements." + "\n\t"
                   + "The first one found will be used for authentication." + "\n\n"
                   + "Use -proxy <host:port> when there is a proxy server between the sample application and Intel AMT." + "\n"
                   + "Use -proxyUser <proxy user> -proxyPass <proxy password> when the proxy requires these parameters." + "\n";

            if (hasLocalOption)
                usageStr += "\nIf -local is used, the sample will use the local OsAdmin credentials.\n" +
                    "Please note that if -local is specified, the sample will ignore the other authentication \nproperties.\n\n";

            if (hasWirelessOption)
                usageStr += "\nIf -wireless is used, the sample will use the wireless interface.\n" +
                            "Please note that not in all the functions are available when using the wireless interface.\n\n";

            usageStr += createDescriptionUsageForStandAloneComponents();

            usageStr += "\nExamples:\n" + GetExamples(assemblyName, firstOption.ToArray(), verbose, hasLocalOption) + "\n";
            return usageStr;
        }

        private string createDescriptionUsageForStandAloneComponents()
        {
            string optionsStr = string.Empty;

            foreach (string item in StandAloneArguments)
            {
                if (PreDefinedFlags.Contains(item))
                    continue;

                Argument arg = _arguments[item];
                optionsStr += arg.Description + "\n";
            }
            return optionsStr;
        }

        private string createUsageForStandAloneComponents()
        {
            string optionsStr = string.Empty;

            foreach (string item in StandAloneArguments)
            {
                if (PreDefinedFlags.Contains(item))
                    continue;

                optionsStr += " ";
                Argument arg = _arguments[item];
                string val = string.Empty;
                if (arg.HasValue)
                {
                    val += " <value>";
                }
                if (arg.Mandatory)
                {
                    optionsStr += "-" + item + val;
                }
                else
                {
                    optionsStr += "[" + "-" + item + val + "]";
                }
            }

            return optionsStr;
        }

        #endregion

        #region PRIVATE_FUNCTIONS

        /// <summary>
        /// Checks if the parameter name is a common argument that exists as a const. 
        /// </summary>
        /// <param name="name">string, name to check</param>
        /// <returns> bool, true if it's a common argument, false if not</returns>
        private static bool IsCommonArg(string name)
        {
            return StandAloneArguments.Contains(name);
        }

        /// <summary>
        /// Gets the 3 usage examples.
        /// </summary>
        /// <param name="assemblyName">string, the assembly name</param>
        /// <param name="options">string, the first option, or an empty string if no option exists</param>
        /// <param name="verbose">bool, true if verbose flag is on</param>
        /// <param name="hasLocalOption">bool, if local option is available</param>
        /// <returns> string, the 3 examples as a string</returns>
        private static string GetExamples(string assemblyName, string[] options, bool verbose, bool hasLocalOption)
        {
            string examples = String.Empty;
            int count = 0;
            if (options.Length > 0)
            {
                foreach (string opt in options)
                {
                    // Creates first example
                    examples += assemblyName;
                    if (!String.IsNullOrEmpty(opt))
                        examples += " -" + opt;
                    if (verbose)
                        examples += " -" + OPT_VERBOSE;
                    examples += " -" + OPT_HOST + " 192.168.0.1 -" + OPT_USER + " admin -" + OPT_PASS + " P@ssw0rd" +
                                "\n";

                    // Creates second example
                    examples += assemblyName;
                    if (!String.IsNullOrEmpty(opt))
                        examples += " -" + opt;
                    if (verbose)
                        examples += " -" + OPT_VERBOSE;
                    examples += " -" + OPT_HOST + " hostname -" + OPT_KRB + "\n";
                    // Creates third example
                    examples += assemblyName;
                    if (!String.IsNullOrEmpty(opt))
                        examples += " -" + opt;
                    if (verbose)
                        examples += " -" + OPT_VERBOSE;
                    examples += " -" + OPT_HOST + " hostname -" + OPT_USER + " admin -" + OPT_PASS + " P@ssw0rd -"
                                + OPT_SECURE + " -" + OPT_CERT + " certCommonName\n\n";

                    //check for the "-local" option
                    if (hasLocalOption)
                    {
                        examples += assemblyName;
                        if (!String.IsNullOrEmpty(opt))
                            examples += " -" + opt;
                        if (verbose)
                            examples += " -" + OPT_VERBOSE;
                        examples += " -" + OPT_HOST + " hostname -" + OPT_LOCAL;
                    }

                    count++;
                    if (count > 1) //show 2 options only... If want to display all option this line can be eliminated.
                        break;
                }
            }
            else
            {
                if (verbose)
                    examples += " -" + OPT_VERBOSE;
                examples += " -" + OPT_HOST + " 192.168.0.1 -" + OPT_USER + " admin -" + OPT_PASS + " P@ssw0rd" +
                            "\n";
                if (verbose)
                    examples += " -" + OPT_VERBOSE;
                examples += " -" + OPT_HOST + " hostname -" + OPT_KRB + "\n";

                if (verbose)
                    examples += " -" + OPT_VERBOSE;
                examples += " -" + OPT_HOST + " hostname -" + OPT_USER + " admin -" + OPT_PASS + " P@ssw0rd -"
                            + OPT_SECURE + " -" + OPT_CERT + " certCommonName\n\n";
            }
            return examples;
        }

        #endregion PRIVATE_FUNCTIONS

        /// <summary>
        /// This class represents a command line parameter (argument).
        /// </summary>
        public class Argument
        {
            /// <summary>Argument name identifer without the - , -- or / </summary>
            private string _name = "";

            /// <summary>The value of this argument.</summary>
            private string _value = "";

            /// <summary>Short description for this argument.</summary>
            private string _description = String.Empty;

            /// <summary>
            /// Constructor
            /// </summary>
            /// <param name="name">string, argument identifier</param>
            /// <param name="hasValue">bool, value required</param>
            /// <param name="mandatory">bool, is argument mandatory</param>
            /// <param name="description"></param>
            public Argument(string name, bool hasValue, bool mandatory, string description)
            {
                Selected = false;
                _name = name;
                HasValue = hasValue;
                Mandatory = mandatory;
                _description = description;
            }

            /// <summary>
            /// Constructor
            /// </summary>
            /// <param name="name">string, argument identifier</param>
            /// <param name="hasValue">bool, value required</param>
            /// <param name="mandatory">bool, is argument mandatory</param>
            public Argument(string name, bool hasValue, bool mandatory)
            {
                Selected = false;
                _name = name;
                HasValue = hasValue;
                Mandatory = mandatory;
                _description = "";
            }

            /// <summary>
            /// Constructor
            /// </summary>
            /// <param name="name">string, argument identifier</param>
            /// <param name="hasValue">bool, value required</param>
            public Argument(string name, bool hasValue)
            {
                Selected = false;
                Mandatory = false;
                _name = name;
                HasValue = hasValue;
            }

            /// <summary>
            /// Get A string representation of this class
            /// </summary>
            /// <returns>string</returns>
            public override string ToString()
            {
                return _value;
            }

            /// <summary>
            /// Property:  Argument description
            /// </summary>
            public string Description
            {
                get { return _description; }
                set { _description = value; }
            }

            /// <summary>
            /// Property:  Argument identifer without the - , -- or / 
            /// </summary>
            public string Name
            {
                get { return _name; }
                set { _name = value; }
            }

            /// <summary>
            /// Property: Indicated whether this argument requires a value
            /// </summary>
            public bool HasValue { get; set; }

            /// <summary>
            /// Property: Indicates if this argument is mandatory
            /// </summary>
            public bool Mandatory { get; set; }

            /// <summary>
            /// Property: The value of this argument.
            /// </summary>
            public string Value
            {
                get { return _value; }
                set { _value = value; }
            }

            /// <summary>
            /// Property: indicates if this argument was found in the command line
            /// </summary>
            public bool Selected { get; set; }
        }

        /// <summary>
        /// Thrown by the ApplicationCommandLine during processing
        /// </summary>
        public class Exception : System.Exception
        {
            /// <summary>
            /// Default constructor
            /// </summary>
            /// <param name="msg">string, Error message</param>
            public Exception(string msg) : base(msg) { }
        }

        /// <summary>
        /// Hashtable for arguments
        /// </summary>
        private Dictionary<string, Argument> _arguments;
        //private Hashtable _arguments;

        /// <summary>
        /// Default Constructor
        /// </summary>
        public CmdLineArguments()
        {
            //_arguments = new Hashtable();
            _arguments = new Dictionary<string, Argument>();
            _exclusionList = ExclusionList.None;
        }

        /// <summary>
        /// the following variable will contain list of 
        /// conditions that the calling application would like to disable
        /// </summary>
        private ExclusionList _exclusionList;


        /// <summary>
        /// Parse the command line arguments.
        /// </summary>
        /// <param name="args">string array, typically the string[] received from Main</param>
        public void Parse(string[] args)
        {
            // Iterate arguments
            for (int i = 0; i < args.Length; i++)
            {
                // if the argument start with an argument prefix
                if (args[i].StartsWith("-") || args[i].StartsWith("/"))
                {
                    string argName = args[i].Substring(1);
                    // Check if argument is in our _arguments list.
                    if (_arguments.ContainsKey(argName))
                    {
                        // Process it
                        Argument arg = _arguments[argName];

                        // set it to selected
                        arg.Selected = true;

                        // Check if argument requires a value.
                        if (arg.HasValue)
                        {
                            if (++i >= args.Length)
                                throw new Exception("Could not found value for " + argName + ".");

                            if (args[i].StartsWith("-") || args[i].StartsWith("/"))
                                throw new Exception("Could not found value for " + argName + ".");
                            arg.Value = args[i];
                        }
                    }
                    else
                    {
                        // Invalid argument
                        throw new Exception("Invalid argument: " + argName + ".");
                    }
                }
                else
                {
                    throw new Exception("Invalid argument: " + args[i] + ".");
                }
            }

            // Make sure we received all mandatory argument. 
            for (IDictionaryEnumerator e = _arguments.GetEnumerator(); e.MoveNext(); )
            {
                Argument arg = (Argument)e.Value;
                if (arg.Mandatory && !arg.Selected)
                    throw new Exception(arg.Name + " is not specified.");
            }
        }

        /// <summary>
        /// Returns a IWebProxy according to the proxy paramters
        /// </summary>
        /// <returns> a IWebProxy or null if proxy parameter not define</returns>
        public MpsManager GetWebProxy()
        {
            string proxyAddress = this[OPT_PROXY];
            if (proxyAddress != null)
            {
                if (!Regex.IsMatch(proxyAddress, @"\w\:\w"))
                {
                    throw new ArgumentException("proxy argument is not according to usage");
                }
                string[] splitAddress = proxyAddress.Split(':');
                int port = 0;
                if (splitAddress.Length != 2 || !int.TryParse(splitAddress[1], out port))
                {
                    throw new ArgumentException("proxy argument is not according to usage");
                }

                MpsManager proxy = new MpsManager();//WebProxy(splitAddress[0], port);
                if (this[OPT_PROXY_USER] != null && this[OPT_PROXY_PASSWORD] != null)
                {
                    proxy.HttpUser = this[OPT_PROXY_USER];
                    proxy.HttpPassword = this[OPT_PROXY_PASSWORD];
                    // proxy.Credentials = new NetworkCredential(this[OPT_PROXY_USER], this[OPT_PROXY_PASSWORD]);
                }

                proxy.AddHost(this[OPT_HOST]);
                proxy.HttpProxy = proxyAddress;
                // proxy.Enabled = true;

                return proxy;
            }
            return null;
        }


        /// <summary>
        /// Add an argument to the valid argument list.
        /// </summary>
        /// <param name="arg">Argument, Add an expected argument</param>
        public void AddArg(Argument arg)
        {
            _arguments.Add(arg.Name.ToLower(), arg);
        }

        /// <summary>
        /// Add an argument to the valid argument list.
        /// </summary>
        /// <param name="name">string, argument name. </param>
        /// <param name="hasValue">bool, is value required</param>
        /// <param name="mandatory">bool, is mandatory argument</param>
        /// <param name="description"></param>
        public void AddArg(string name, bool hasValue, bool mandatory, string description)
        {
            _arguments.Add(name.ToLower(), new Argument(name.ToLower(), hasValue, mandatory, description));
        }

        /// <summary>
        /// Add an argument to the valid argument list.
        /// </summary>
        /// <param name="name">string, argument name. </param>
        /// <param name="hasValue">bool, is value required</param>
        /// <param name="mandatory">bool, is mandatory argument</param>
        /// <param name="description"></param>
        public void AddStandAloneArg(string name, bool hasValue, bool mandatory, string description)
        {
            AddArg(new Argument(name.ToLower(), hasValue, mandatory, description));
            StandAloneArguments.Add(name.ToLower());
        }

        /// <summary>
        /// Add an argument to the valid argument list.
        /// </summary>
        /// <param name="name">string, argument name. </param>
        /// <param name="hasValue">bool, is value required</param>
        /// <param name="mandatory">bool, is mandatory argument</param>
        public void AddArg(string name, bool hasValue, bool mandatory)
        {
            AddArg(new Argument(name.ToLower(), hasValue, mandatory, ""));
        }

        /// <summary>
        /// Check whether argument was given in the command line.
        /// </summary>
        /// <param name="name">argument name</param>
        /// <returns>true if argument was selected</returns>
        public bool Selected(string name)
        {
            if (_arguments.ContainsKey(name.ToLower()))
                return _arguments[name.ToLower()].Selected;
            return false;
        }

        /// <summary>
        /// Indexer by argument name (only get)
        /// </summary>
        /// <param name="name">argument</param>
        /// <returns></returns>
        public string this[string name]
        {
            get
            {
                if (_arguments.ContainsKey(name.ToLower()))
                {
                    Argument arg = _arguments[name.ToLower()];
                    if (arg.Selected)
                    {
                        if (arg.HasValue)
                            return arg.Value;
                    }
                }
                return null;
            }
        }

        #region GENERAL UTILS FUNCTIONS

        /// <summary>
        /// User input flags
        /// </summary>
        public enum YesNoFlag
        {
            YES,
            NO
        };

        /// <summary>
        /// Perform y/n command (waiting for user input)
        /// </summary>
        /// <param name="message">The message to display</param>
        /// <returns>The user choice</returns>
        public YesNoFlag AskYesNo(string message)
        {
            bool retry = false; // Indicates if the user input is correct
            YesNoFlag userFlag = YesNoFlag.NO;

            Console.Write("\n" + message + "[Y/N]? ");

            while (!retry)
            {
                string user_choice = Console.ReadLine();

                if (String.Compare(user_choice, "y", true) == 0)
                {
                    retry = true;
                    userFlag = YesNoFlag.YES;
                }
                else if (String.Compare(user_choice, "n", true) == 0)
                {
                    retry = true;
                    userFlag = YesNoFlag.NO;
                }
                else
                    Console.WriteLine("Invalid input, try again");
            }
            return userFlag;
        }

        /// <summary>
        ///Change the Display Color for the User (waiting for user input)
        /// </summary>
        /// <param name="message">The message to display</param>
        ///<param name="color"></param>
        public void MessageDisplay_Color(string message, ConsoleColor color)
        {
            Console.ForegroundColor = color;
            Console.WriteLine("\n {0} ", message);
            Console.ForegroundColor = ConsoleColor.Gray;
        }

        #endregion

        #region INIT_FUNCTIONS

        ///<summary>
        ///Common INIT Functions for all samples
        /// </summary>
        public void init_functions()
        {
            AddArg(CmdLineArguments.OPT_HOST, true, true, String.Empty);         // -host required
            AddArg(CmdLineArguments.OPT_PASS, true, false, String.Empty);        // -pass optional
            AddArg(CmdLineArguments.OPT_USER, true, false, String.Empty);        // -user optional
            AddArg(CmdLineArguments.OPT_SECURE, false, false, String.Empty);     // -tls options
            AddArg(CmdLineArguments.OPT_WIRELESS, false, false, String.Empty);     // -tls options
            AddArg(CmdLineArguments.OPT_KRB, false, false, String.Empty);        // -kerberos options
            AddArg(CmdLineArguments.OPT_CERT, true, false, String.Empty);        // -certificate options
            AddArg(CmdLineArguments.OPT_PROXY, true, false, String.Empty);       // -proxy options
            AddArg(CmdLineArguments.OPT_VERBOSE, false, false, String.Empty);    // -verbose options
            AddArg(CmdLineArguments.OPT_PROXY_USER, true, false, String.Empty);
            AddArg(CmdLineArguments.OPT_PROXY_PASSWORD, true, false, String.Empty);

        }

        #endregion

        #region CatchTypes

        public int catchType(System.Exception e, string usage)
        {
            exitCodes exitCode;

            if (e is Exception)
            {
                MessageDisplay_Color("\n--------------CmdLineArguments Exception--------------", ConsoleColor.Red);
                MessageDisplay_Color(e.Message, ConsoleColor.Red);
                MessageDisplay_Color(usage, ConsoleColor.Gray);
                exitCode = exitCodes.EXIT_USAGE;
                return (int)exitCode;
            }

            if (e is ArgumentException)
            {
                MessageDisplay_Color("\n--------------Argument Exception--------------", ConsoleColor.Red);
                MessageDisplay_Color(e.Message, ConsoleColor.Red);
                exitCode = exitCodes.EXIT_ARGUMENT_ERROR;
                return (int)exitCode;
            }

            if (e is FormatException)
            {
                Console.WriteLine("\n--------------DateTimeFormat Exception--------------");
                Console.WriteLine(e.Message);
                exitCode = exitCodes.EXIT_FAILURE;
                return (int)exitCode;
            }
            if (e is WsmanConnectionException)
            {
                MessageDisplay_Color("\n ---------- WSMan Exception------------", ConsoleColor.Red);
                MessageDisplay_Color(e.Message, ConsoleColor.Red);
                exitCode = exitCodes.EXIT_FAILURE;
                return (int)exitCode;
            }
            if (e is WsmanUnreachableException)
            {
                MessageDisplay_Color("\n ---------- WSMan Exception------------", ConsoleColor.Red);
                MessageDisplay_Color(e.Message, ConsoleColor.Red);
                exitCode = exitCodes.EXIT_FAILURE;
                return (int)exitCode;
            }

            //General Exception - in case non of the above work.
            MessageDisplay_Color("\n--------------General Exception--------------", ConsoleColor.Red);
            MessageDisplay_Color(e.Message, ConsoleColor.Red);
            exitCode = exitCodes.EXIT_FAILURE;
            return (int)exitCode;
        }
        //end func
        #endregion
    }
}
