// -----------------------------------------------------------------------------
//
// Copyright (c) 2009 Intel Corporation. All Rights Reserved.
//
// The source code contained or described herein and all documents related to 
// the source code ("Material") are owned by Intel Corporation or its suppliers 
// or licensors. Title to the Material remains with Intel Corporation or its 
// suppliers and licensors. The Material may contain trade secrets and 
// proprietary and confidential information of Intel Corporation and its 
// suppliers and licensors, and is protected by worldwide copyright and trade 
// secret laws and treaty provisions. No part of the Material may be used, 
// copied, reproduced, modified, published, uploaded, posted, transmitted, 
// distributed, or disclosed in any way without Intel�s prior express written 
// permission. 
//
// No license under any patent, copyright, trade secret or other intellectual 
// property right is granted to or conferred upon you by disclosure or delivery 
// of the Materials, either expressly, by implication, inducement, estoppel or 
// otherwise. Any license under such intellectual property rights must be 
// express and approved by Intel in writing.
//
// * Third Party trademarks are the property of their respective owners.
//
// Unless otherwise agreed by Intel in writing, you may not remove or alter this 
// notice or any other notice embedded in Materials by Intel or Intel�s 
// suppliers or licensors in any way.
//
// -----------------------------------------------------------------------------
// This file was automatically generated from AMT_RedirectionService.mof
// Copyright (C) Intel Corporation, 2006-2008
namespace MOF_CLASSES{
	using System.Xml.Serialization;


	/// <remarks>
	/// Represents the redirection service, which encompasses the IDER and SOL redirection functionalities
	/// </remarks>
	[System.SerializableAttribute()]
	[System.Xml.Serialization.XmlTypeAttribute(Namespace="http://intel.com/wbem/wscim/1/amt-schema/1/AMT_RedirectionService")]
	[System.Xml.Serialization.XmlRootAttribute("AMT_RedirectionService", Namespace="http://intel.com/wbem/wscim/1/amt-schema/1/AMT_RedirectionService", IsNullable=false)]
	public partial class AMT_RedirectionServiceType {

		/// <remarks>
		/// A list of string elements, describing recent IDE redirection operations. The format of each element is: 'Date (MM/DD/YYYY), Time (hh:mm:ss), IP:Port'
		/// </remarks>
		[System.Xml.Serialization.XmlElementAttribute("AccessLog")]
		public string[] AccessLog;

		/// <remarks>
		/// The Caption property is a short textual description (one- line string) of the object.
		/// </remarks>
		public string Caption;

		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool CaptionSpecified;

		/// <remarks>
		/// CreationClassName indicates the name of the class or the subclass that is used in the creation of an instance. When used with the other key properties of this class, this property allows all instances of this class and its subclasses to be uniquely identified.
		/// </remarks>
		[CimKey]
		public string CreationClassName;

		/// <remarks>
		/// The Description property provides a textual description of the object.
		/// </remarks>
		public string Description;

		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool DescriptionSpecified;

		/// <remarks>
		/// A user-friendly name for the object. This property allows each instance to define a user-friendly name in addition to its key properties, identity data, and description information. 
		/// Note that the Name property of ManagedSystemElement is also defined as a user-friendly name. But, it is often subclassed to be a Key. It is not reasonable that the same property can convey both identity and a user-friendly name, without inconsistencies. Where Name exists and is not a Key (such as for instances of LogicalDevice), the same information can be present in both the Name and ElementName properties.
		/// </remarks>
		public string ElementName;

		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool ElementNameSpecified;

		/// <remarks>
		/// An enumerated value indicating an administrator's default or startup configuration for the Enabled State of an element. By default, the element is "Enabled" (value=2).
		/// </remarks>
		public ushort EnabledDefault;

		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool EnabledDefaultSpecified;

		/// <remarks>
		/// EnabledState is an integer enumeration that indicates the enabled and disabled states of an element. It can also indicate the transitions between these requested states. For example, shutting down (value=4) and starting (value=10) are transient states between enabled and disabled. The following text briefly summarizes the various enabled and disabled states: 
		/// Enabled (2) indicates that the element is or could be executing commands, will process any queued commands, and queues new requests. 
		/// Disabled (3) indicates that the element will not execute commands and will drop any new requests. 
		/// Shutting Down (4) indicates that the element is in the process of going to a Disabled state. 
		/// Not Applicable (5) indicates the element does not support being enabled or disabled. 
		/// Enabled but Offline (6) indicates that the element might be completing commands, and will drop any new requests. 
		/// Test (7) indicates that the element is in a test state. 
		/// Deferred (8) indicates that the element might be completing commands, but will queue any new requests. 
		/// Quiesce (9) indicates that the element is enabled but in a restricted mode. The behavior of the element is similar to the Enabled state, but it processes only a restricted set of commands. All other requests are queued. 
		/// Starting (10) indicates that the element is in the process of going to an Enabled state. New requests are queued.
		/// </remarks>
		public ushort EnabledState;

		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool EnabledStateSpecified;

		/// <remarks>
		/// Indicates the current health of the element. This attribute expresses the health of this element but not necessarily that of its subcomponents. The possible values are 0 to 30, where 5 means the element is entirely healthy and 30 means the element is completely non-functional. The following continuum is defined: 
		/// "Non-recoverable Error" (30) - The element has completely failed, and recovery is not possible. All functionality provided by this element has been lost. 
		/// "Critical Failure" (25) - The element is non-functional and recovery might not be possible. 
		/// "Major Failure" (20) - The element is failing. It is possible that some or all of the functionality of this component is degraded or not working. 
		/// "Minor Failure" (15) - All functionality is available but some might be degraded. 
		/// "Degraded/Warning" (10) - The element is in working order and all functionality is provided. However, the element is not working to the best of its abilities. For example, the element might not be operating at optimal performance or it might be reporting recoverable errors. 
		/// "OK" (5) - The element is fully functional and is operating within normal operational parameters and without error. 
		/// "Unknown" (0) - The implementation cannot report on HealthState at this time. 
		/// DMTF has reserved the unused portion of the continuum for additional HealthStates in the future.
		/// </remarks>
		public ushort HealthState;

		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool HealthStateSpecified;

		/// <remarks>
		/// A datetime value that indicates when the object was installed. Lack of a value does not indicate that the object is not installed.
		/// </remarks>
		public DateTime InstallDate;

		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool InstallDateSpecified;

		/// <remarks>
		/// InstanceID is an optional property that may be used to opaquely and uniquely identify an instance of this class within the scope of the instantiating Namespace. Various subclasses of this class may override this property to make it required, or a key. Such subclasses may also modify the preferred algorithms for ensuring uniqueness that are defined below.
		/// To ensure uniqueness within the NameSpace, the value of InstanceID should be constructed using the following "preferred" algorithm: 
		/// <OrgID>:<LocalID> 
		/// Where <OrgID> and <LocalID> are separated by a colon (:), and where <OrgID> must include a copyrighted, trademarked, or otherwise unique name that is owned by the business entity that is creating or defining the InstanceID or that is a registered ID assigned to the business entity by a recognized global authority. (This requirement is similar to the <Schema Name>_<Class Name> structure of Schema class names.) In addition, to ensure uniqueness, <OrgID> must not contain a colon (:). When using this algorithm, the first colon to appear in InstanceID must appear between <OrgID> and <LocalID>. 
		/// <LocalID> is chosen by the business entity and should not be reused to identify different underlying (real-world) elements. If not null and the above "preferred" algorithm is not used, the defining entity must assure that the resulting InstanceID is not reused across any InstanceIDs produced by this or other providers for the NameSpace of this instance. 
		/// If not set to null for DMTF-defined instances, the "preferred" algorithm must be used with the <OrgID> set to CIM.
		/// </remarks>
		public string InstanceID;

		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool InstanceIDSpecified;

		/// <remarks>
		/// Describes the listener state of this service. The service is operational only if it's EnabledState is set to Enabled (by the Intel� AMT Administrator) and the Listenerstate is set to TRUE (by a RedirectionRealm user)
		/// </remarks>
		public bool ListenerEnabled;

		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool ListenerEnabledSpecified;

		/// <remarks>
		/// The Name property uniquely identifies the Service and provides an indication of the functionality that is managed. This functionality is described in more detail in the Description property of the object.
		/// </remarks>
		[CimKey]
		public string Name;

		/// <remarks>
		/// Indicates the current statuses of the element. Various operational statuses are defined. Many of the enumeration's values are self-explanatory. However, a few are not and are described here in more detail. 
		/// "Stressed" indicates that the element is functioning, but needs attention. Examples of "Stressed" states are overload, overheated, and so on. 
		/// "Predictive Failure" indicates that an element is functioning nominally but predicting a failure in the near future. 
		/// "In Service" describes an element being configured, maintained, cleaned, or otherwise administered. 
		/// "No Contact" indicates that the monitoring system has knowledge of this element, but has never been able to establish communications with it. 
		/// "Lost Communication" indicates that the ManagedSystem Element is known to exist and has been contacted successfully in the past, but is currently unreachable. 
		/// "Stopped" and "Aborted" are similar, although the former implies a clean and orderly stop, while the latter implies an abrupt stop where the state and configuration of the element might need to be updated. 
		/// "Dormant" indicates that the element is inactive or quiesced. 
		/// "Supporting Entity in Error" indicates that this element might be "OK" but that another element, on which it is dependent, is in error. An example is a network service or endpoint that cannot function due to lower-layer networking problems. 
		/// "Completed" indicates that the element has completed its operation. This value should be combined with either OK, Error, or Degraded so that a client can tell if the complete operation Completed with OK (passed), Completed with Error (failed), or Completed with Degraded (the operation finished, but it did not complete OK or did not report an error). 
		/// "Power Mode" indicates that the element has additional power model information contained in the Associated PowerManagementService association. 
		/// OperationalStatus replaces the Status property on ManagedSystemElement to provide a consistent approach to enumerations, to address implementation needs for an array property, and to provide a migration path from today's environment to the future. This change was not made earlier because it required the deprecated qualifier. Due to the widespread use of the existing Status property in management applications, it is strongly recommended that providers or instrumentation provide both the Status and OperationalStatus properties. Further, the first value of OperationalStatus should contain the primary status for the element. When instrumented, Status (because it is single-valued) should also provide the primary status of the element.
		/// </remarks>
		[System.Xml.Serialization.XmlElementAttribute("OperationalStatus")]
		public ushort[] OperationalStatus;

		/// <remarks>
		/// A string that describes the enabled or disabled state of the element when the EnabledState property is set to 1 ("Other"). This property must be set to null when EnabledState is any value other than 1.
		/// </remarks>
		public string OtherEnabledState;

		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool OtherEnabledStateSpecified;

		/// <remarks>
		/// A string that provides information on how the primary owner of the Service can be reached (for example, phone number, e-mail address, and so on).
		/// </remarks>
		public string PrimaryOwnerContact;

		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool PrimaryOwnerContactSpecified;

		/// <remarks>
		/// The name of the primary owner for the service, if one is defined. The primary owner is the initial support contact for the Service.
		/// </remarks>
		public string PrimaryOwnerName;

		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool PrimaryOwnerNameSpecified;

		/// <remarks>
		/// RequestedState is an integer enumeration that indicates the last requested or desired state for the element, irrespective of the mechanism through which it was requested. The actual state of the element is represented by EnabledState. This property is provided to compare the last requested and current enabled or disabled states. Note that when EnabledState is set to 5 ("Not Applicable"), then this property has no meaning. Refer to the EnabledState property description for explanations of the values in the RequestedState enumeration. 
		/// "Unknown" (0) indicates the last requested state for the element is unknown.
		/// Note that the value "No Change" (5) has been deprecated in lieu of indicating the last requested state is "Unknown" (0). If the last requested or desired state is unknown, RequestedState should have the value "Unknown" (0), but may have the value "No Change" (5).Offline (6) indicates that the element has been requested to transition to the Enabled but Offline EnabledState. 
		/// It should be noted that there are two new values in RequestedState that build on the statuses of EnabledState. These are "Reboot" (10) and "Reset" (11). Reboot refers to doing a "Shut Down" and then moving to an "Enabled" state. Reset indicates that the element is first "Disabled" and then "Enabled". The distinction between requesting "Shut Down" and "Disabled" should also be noted. Shut Down requests an orderly transition to the Disabled state, and might involve removing power, to completely erase any existing state. The Disabled state requests an immediate disabling of the element, such that it will not execute or accept any commands or processing requests. 
		/// 
		/// This property is set as the result of a method invocation (such as Start or StopService on CIM_Service), or can be overridden and defined as WRITEable in a subclass. The method approach is considered superior to a WRITEable property, because it allows an explicit invocation of the operation and the return of a result code. 
		/// 
		/// If knowledge of the last RequestedState is not supported for the EnabledLogicalElement, the property shall be NULL or have the value 12 "Not Applicable".
		/// </remarks>
		public ushort RequestedState;

		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool RequestedStateSpecified;

		/// <remarks>
		/// Note: The use of this element is deprecated in lieu of the EnabledDefault property that is inherited from EnabledLogicalElement. The EnabledLogicalElement addresses the same semantics. The change to a uint16 data type was discussed when CIM V2.0 was defined. However, existing V1.0 implementations used the string property. To remain compatible with those implementations, StartMode was grandfathered into the schema. Use of the deprecated qualifier allows the maintenance of the existing property but also permits an improved, clarified definition using EnabledDefault. 
		/// Deprecated description: StartMode is a string value that indicates whether the Service is automatically started by a System, an Operating System, and so on, or is started only upon request.
		/// </remarks>
		public string StartMode;

		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool StartModeSpecified;

		/// <remarks>
		/// Started is a Boolean that indicates whether the Service has been started (TRUE), or stopped (FALSE).
		/// </remarks>
		public bool Started;

		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool StartedSpecified;

		/// <remarks>
		/// A string indicating the current status of the object. Various operational and non-operational statuses are defined. This property is deprecated in lieu of OperationalStatus, which includes the same semantics in its enumeration. This change is made for 3 reasons: 
		/// 1) Status is more correctly defined as an array. This definition overcomes the limitation of describing status using a single value, when it is really a multi-valued property (for example, an element might be OK AND Stopped. 
		/// 2) A MaxLen of 10 is too restrictive and leads to unclear enumerated values. 
		/// 3) The change to a uint16 data type was discussed when CIM V2.0 was defined. However, existing V1.0 implementations used the string property and did not want to modify their code. Therefore, Status was grandfathered into the Schema. Use of the deprecated qualifier allows the maintenance of the existing property, but also permits an improved definition using OperationalStatus.
		/// </remarks>
		public string Status;

		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool StatusSpecified;

		/// <remarks>
		/// Strings describing the various OperationalStatus array values. For example, if "Stopping" is the value assigned to OperationalStatus, then this property may contain an explanation as to why an object is being stopped. Note that entries in this array are correlated with those at the same array index in OperationalStatus.
		/// </remarks>
		[System.Xml.Serialization.XmlElementAttribute("StatusDescriptions")]
		public string[] StatusDescriptions;

		/// <remarks>
		/// The CreationClassName of the scoping System.
		/// </remarks>
		[CimKey]
		public string SystemCreationClassName;

		/// <remarks>
		/// The Name of the scoping System.
		/// </remarks>
		[CimKey]
		public string SystemName;

		/// <remarks>
		/// The date or time when the EnabledState of the element last changed. If the state of the element has not changed and this property is populated, then it must be set to a 0 interval value. If a state change was requested, but rejected or not yet processed, the property must not be updated.
		/// </remarks>
		public DateTime TimeOfLastStateChange;

		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool TimeOfLastStateChangeSpecified;

		/// <remarks>
		/// Requests that the state of the element be changed to the value specified in the RequestedState parameter. When the requested state change takes place, the EnabledState and RequestedState of the element will be the same. Invoking the RequestStateChange method multiple times could result in earlier requests being overwritten or lost. 
	/// If 0 is returned, then the task completed successfully and the use of ConcreteJob was not required. If 4096 (0x1000) is returned, then the task will take some time to complete, ConcreteJob will be created, and its reference returned in the output parameter Job. Any other return code indicates an error condition.
		/// </remarks>
		[System.SerializableAttribute()]
		[System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
		[System.Xml.Serialization.XmlRootAttribute(Namespace="http://intel.com/wbem/wscim/1/amt-schema/1/AMT_RedirectionService", IsNullable=false)]
		public partial class RequestStateChange_INPUT {

			/// <remarks>
			/// The state requested for the element. This information will be placed into the RequestedState property of the instance if the return code of the RequestStateChange method is 0 ('Completed with No Error'), 3 ('Timeout'), or 4096 (0x1000) ('Job Started'). Refer to the description of the EnabledState and RequestedState properties for the detailed explanations of the RequestedState values.
			/// </remarks>
			public ushort RequestedState;

			[System.Xml.Serialization.XmlIgnoreAttribute()]
			public bool RequestedStateSpecified;

			/// <remarks>
			/// A timeout period that specifies the maximum amount of time that the client expects the transition to the new state to take. The interval format must be used to specify the TimeoutPeriod. A value of 0 or a null parameter indicates that the client has no time requirements for the transition. 
			/// If this property does not contain 0 or null and the implementation does not support this parameter, a return code of 'Use Of Timeout Parameter Not Supported' must be returned.
			/// </remarks>
			public DateTime TimeoutPeriod;

			[System.Xml.Serialization.XmlIgnoreAttribute()]
			public bool TimeoutPeriodSpecified;

		}

		/// <remarks>
		/// Requests that the state of the element be changed to the value specified in the RequestedState parameter. When the requested state change takes place, the EnabledState and RequestedState of the element will be the same. Invoking the RequestStateChange method multiple times could result in earlier requests being overwritten or lost. 
	/// If 0 is returned, then the task completed successfully and the use of ConcreteJob was not required. If 4096 (0x1000) is returned, then the task will take some time to complete, ConcreteJob will be created, and its reference returned in the output parameter Job. Any other return code indicates an error condition.
		/// </remarks>
		[System.SerializableAttribute()]
		[System.Xml.Serialization.XmlTypeAttribute(AnonymousType=true)]
		[System.Xml.Serialization.XmlRootAttribute(Namespace="http://intel.com/wbem/wscim/1/amt-schema/1/AMT_RedirectionService", IsNullable=false)]
		public partial class RequestStateChange_OUTPUT {

			/// <remarks>
			/// Reference to the job (can be null if the task is completed).
			/// </remarks>
			public EndpointReferenceType Job;

			[System.Xml.Serialization.XmlIgnoreAttribute()]
			public bool JobSpecified;

				public uint ReturnValue;

		}

	}
}

