// -----------------------------------------------------------------------------
//
// Copyright (c) 2009 Intel Corporation. All Rights Reserved.
//
// The source code contained or described herein and all documents related to 
// the source code ("Material") are owned by Intel Corporation or its suppliers 
// or licensors. Title to the Material remains with Intel Corporation or its 
// suppliers and licensors. The Material may contain trade secrets and 
// proprietary and confidential information of Intel Corporation and its 
// suppliers and licensors, and is protected by worldwide copyright and trade 
// secret laws and treaty provisions. No part of the Material may be used, 
// copied, reproduced, modified, published, uploaded, posted, transmitted, 
// distributed, or disclosed in any way without Intel�s prior express written 
// permission. 
//
// No license under any patent, copyright, trade secret or other intellectual 
// property right is granted to or conferred upon you by disclosure or delivery 
// of the Materials, either expressly, by implication, inducement, estoppel or 
// otherwise. Any license under such intellectual property rights must be 
// express and approved by Intel in writing.
//
// * Third Party trademarks are the property of their respective owners.
//
// Unless otherwise agreed by Intel in writing, you may not remove or alter this 
// notice or any other notice embedded in Materials by Intel or Intel�s 
// suppliers or licensors in any way.
//
// -----------------------------------------------------------------------------
// This file was automatically generated from CIM_AssociatedPowerManagementService.mof
// Copyright (C) Intel Corporation, 2006-2008
namespace MOF_CLASSES{
	using System.Xml.Serialization;


	/// <remarks>
	/// The association between a Managed System Element and its power management service.
	/// </remarks>
	[System.SerializableAttribute()]
	[System.Xml.Serialization.XmlTypeAttribute(Namespace="http://schemas.dmtf.org/wbem/wscim/1/cim-schema/2/CIM_AssociatedPowerManagementService")]
	[System.Xml.Serialization.XmlRootAttribute("CIM_AssociatedPowerManagementService", Namespace="http://schemas.dmtf.org/wbem/wscim/1/cim-schema/2/CIM_AssociatedPowerManagementService", IsNullable=false)]
	public partial class CIM_AssociatedPowerManagementServiceType {

		/// <remarks>
		/// A string describing the additional power management state of the element, used when the PowerState is set to the value 1, "Other".
		/// </remarks>
		public string OtherPowerState;

		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool OtherPowerStateSpecified;

		/// <remarks>
		/// A string describing the additional power management state of the element, used when the RequestedPowerState is set to the value 1, "Other".
		/// </remarks>
		public string OtherRequestedPowerState;

		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool OtherRequestedPowerStateSpecified;

		/// <remarks>
		/// The time when the element will be powered on again, used when the RequestedPowerState has the value 2, "On", 5, "Power Cycle (Off - Soft)" or 6, "Power Cycle (Off - Hard)".
		/// </remarks>
		public DateTime PowerOnTime;

		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool PowerOnTimeSpecified;

		/// <remarks>
		/// The current power state of the associated Managed System Element.
		/// </remarks>
		public ushort PowerState;

		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool PowerStateSpecified;

		/// <remarks>
		/// The desired or the last requested power state of the associated Managed System Element, irrespective of the mechanism through which the request was made. If the requested power state is unknown, then the property shall have the value of 0 ("Unknown"). If the property has no meaning or is not supported, then the property shall have value 12("Not Applicable").
		/// </remarks>
		public ushort RequestedPowerState;

		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool RequestedPowerStateSpecified;

		/// <remarks>
		/// The Service that is available.
		/// </remarks>
		[CimKey]
		public EndpointReferenceType ServiceProvided;

		/// <remarks>
		/// The ManagedElement that can use the Service.
		/// </remarks>
		[CimKey]
		public EndpointReferenceType UserOfService;

	}
}

