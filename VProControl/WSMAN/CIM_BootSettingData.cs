// This file was automatically generated from CIM_BootSettingData.mof
// Copyright (C) Intel Corporation, 2006-2008
namespace MOF_CLASSES{
	using System.Xml.Serialization;


	/// <remarks>
	/// BootSettingData is a set of settings that apply to system boot. An example of usage of this class is to hold several BIOS, NVRAM, firmware or system settings, typically seen in the BIOS setup screens. These settings would need to be modified by the system as part of the boot process. Since, it is often not possible to intercept the boot process to apply these settings, users can set these a priori in the instance associated with the selected BootSourceSetting, thereby instructing the system to apply them during the next system reboot.
	/// </remarks>
	[System.SerializableAttribute()]
	[System.Xml.Serialization.XmlTypeAttribute(Namespace="http://schemas.dmtf.org/wbem/wscim/1/cim-schema/2/CIM_BootSettingData")]
	[System.Xml.Serialization.XmlRootAttribute("CIM_BootSettingData", Namespace="http://schemas.dmtf.org/wbem/wscim/1/cim-schema/2/CIM_BootSettingData", IsNullable=false)]
	public partial class CIM_BootSettingDataType {

		/// <remarks>
		/// The Caption property is a short textual description (one- line string) of the object.
		/// </remarks>
		public string Caption;

		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool CaptionSpecified;

		/// <remarks>
		/// The Description property provides a textual description of the object.
		/// </remarks>
		public string Description;

		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool DescriptionSpecified;

		/// <remarks>
		/// The user-friendly name for this instance of SettingData. In addition, the user-friendly name can be used as an index property for a search or query. (Note: The name does not have to be unique within a namespace.)
		/// </remarks>
		public string ElementName;

		/// <remarks>
		/// Within the scope of the instantiating Namespace, InstanceID opaquely and uniquely identifies an instance of this class. To ensure uniqueness within the NameSpace, the value of InstanceID should be constructed using the following "preferred" algorithm: 
		/// <OrgID>:<LocalID> 
		/// Where <OrgID> and <LocalID> are separated by a colon (:), and where <OrgID> must include a copyrighted, trademarked, or otherwise unique name that is owned by the business entity that is creating or defining the InstanceID or that is a registered ID assigned to the business entity by a recognized global authority. (This requirement is similar to the <Schema Name>_<Class Name> structure of Schema class names.) In addition, to ensure uniqueness, <OrgID> must not contain a colon (:). When using this algorithm, the first colon to appear in InstanceID must appear between <OrgID> and <LocalID>. 
		/// <LocalID> is chosen by the business entity and should not be reused to identify different underlying (real-world) elements. If the above "preferred" algorithm is not used, the defining entity must assure that the resulting InstanceID is not reused across any InstanceIDs produced by this or other providers for the NameSpace of this instance. 
		/// For DMTF-defined instances, the "preferred" algorithm must be used with the <OrgID> set to CIM.
		/// </remarks>
		[CimKey]
		public string InstanceID;

		/// <remarks>
		/// OwningEntity identifies the vendor or organization that defines the contained boot settings. In order to ensure uniqueness, the value of OwningEntity MUST be constructed using the following algorithm: 
		/// <OrgID>[:<LocalID>] 
		/// where <OrgID> MUST include a copyrighted, trademarked or otherwise unique name that is owned by the entity creating/defining the BootSettingData, or is a registered ID that is assigned to the entity by a recognized global authority (This is similar to the <Schema Name>_<Class Name> structure of Schema class names.) In addition, <OrgID> MUST NOT contain a colon (':'). 
		/// <LocalID> is optional and, when used, MUST be unique within the scope of the <OrgID>. When a <LocalID> is present, a colon MUST appear between <OrgID> and <LocalID>. For DMTF defined instances, the algorithm MUST be used with the <OrgID> set to 'CIM'.
		/// </remarks>
		public string OwningEntity;

		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool OwningEntitySpecified;

	}
}

