// -----------------------------------------------------------------------------
//
// Copyright (c) 2009 Intel Corporation. All Rights Reserved.
//
// The source code contained or described herein and all documents related to 
// the source code ("Material") are owned by Intel Corporation or its suppliers 
// or licensors. Title to the Material remains with Intel Corporation or its 
// suppliers and licensors. The Material may contain trade secrets and 
// proprietary and confidential information of Intel Corporation and its 
// suppliers and licensors, and is protected by worldwide copyright and trade 
// secret laws and treaty provisions. No part of the Material may be used, 
// copied, reproduced, modified, published, uploaded, posted, transmitted, 
// distributed, or disclosed in any way without Intel�s prior express written 
// permission. 
//
// No license under any patent, copyright, trade secret or other intellectual 
// property right is granted to or conferred upon you by disclosure or delivery 
// of the Materials, either expressly, by implication, inducement, estoppel or 
// otherwise. Any license under such intellectual property rights must be 
// express and approved by Intel in writing.
//
// * Third Party trademarks are the property of their respective owners.
//
// Unless otherwise agreed by Intel in writing, you may not remove or alter this 
// notice or any other notice embedded in Materials by Intel or Intel�s 
// suppliers or licensors in any way.
//
// -----------------------------------------------------------------------------
// This file was automatically generated from CIM_BootSourceSetting.mof
// Copyright (C) Intel Corporation, 2006-2008
namespace MOF_CLASSES{
	using System.Xml.Serialization;


	/// <remarks>
	/// A class derived from SettingData that provides the information necessary to describe a boot source. This may be optionally associated to a bootable logical device, such as a hard disk partition, or a network device. The information from this class instance is used by the boot manager, such as BIOS/EFI or OS Loader to initiate the boot process, when this instance appears in a BootConfigSetting collection.
	/// </remarks>
	[System.SerializableAttribute()]
	[System.Xml.Serialization.XmlTypeAttribute(Namespace="http://schemas.dmtf.org/wbem/wscim/1/cim-schema/2/CIM_BootSourceSetting")]
	[System.Xml.Serialization.XmlRootAttribute("CIM_BootSourceSetting", Namespace="http://schemas.dmtf.org/wbem/wscim/1/cim-schema/2/CIM_BootSourceSetting", IsNullable=false)]
	public partial class CIM_BootSourceSettingType {

		/// <remarks>
		/// A string identifying the boot source which corresponds to the string used by the BIOS to uniquely name the boot source. For example, in systems which implement the BIOS Boot Specification, the string could correspond to the descString string for entries in the IPL Table or BCV Table.
		/// </remarks>
		public string BIOSBootString;

		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool BIOSBootStringSpecified;

		/// <remarks>
		/// A string identifying the boot source. It is typically used by the instrumentation to pass to the boot manager as a selection string. This could be a string identifying the bootable device, such as "CDROM 1", or could be an implementation specific address of a bootable partition, such as the following. "fd(64)unix root=hd(40) swap=hd(41)", or "multi(0)disk(0)rdisk(0)partition(1)\WINDOWS="Microsoft Windows XP Professional"".
		/// </remarks>
		public string BootString;

		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool BootStringSpecified;

		/// <remarks>
		/// The Caption property is a short textual description (one- line string) of the object.
		/// </remarks>
		public string Caption;

		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool CaptionSpecified;

		/// <remarks>
		/// The Description property provides a textual description of the object.
		/// </remarks>
		public string Description;

		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool DescriptionSpecified;

		/// <remarks>
		/// The user-friendly name for this instance of SettingData. In addition, the user-friendly name can be used as an index property for a search or query. (Note: The name does not have to be unique within a namespace.)
		/// </remarks>
		public string ElementName;

		/// <remarks>
		/// An enumeration indicating the behavior when the attempt to boot using the boot source fails (no media, timeout). The current values in the enumeration are: 
		/// 0 = Unknown 
		/// 1 = Is Supported 
		/// 2 = Is Not Supported. 
		/// A value of 1 (Is Supported) indicates that next boot source the boot order is used. A value of 2 (Is Not Supported) indicates that the boot order is terminated and no other boot sources associated to the same CIM_BootConfigSetting are used). The default is 1 (Is Supported)
		/// </remarks>
		public ushort FailThroughSupported;

		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool FailThroughSupportedSpecified;

		/// <remarks>
		/// Within the scope of the instantiating Namespace, InstanceID opaquely and uniquely identifies an instance of this class. To ensure uniqueness within the NameSpace, the value of InstanceID should be constructed using the following "preferred" algorithm: 
		/// <OrgID>:<LocalID> 
		/// Where <OrgID> and <LocalID> are separated by a colon (:), and where <OrgID> must include a copyrighted, trademarked, or otherwise unique name that is owned by the business entity that is creating or defining the InstanceID or that is a registered ID assigned to the business entity by a recognized global authority. (This requirement is similar to the <Schema Name>_<Class Name> structure of Schema class names.) In addition, to ensure uniqueness, <OrgID> must not contain a colon (:). When using this algorithm, the first colon to appear in InstanceID must appear between <OrgID> and <LocalID>. 
		/// <LocalID> is chosen by the business entity and should not be reused to identify different underlying (real-world) elements. If the above "preferred" algorithm is not used, the defining entity must assure that the resulting InstanceID is not reused across any InstanceIDs produced by this or other providers for the NameSpace of this instance. 
		/// For DMTF-defined instances, the "preferred" algorithm must be used with the <OrgID> set to CIM.
		/// </remarks>
		[CimKey]
		public string InstanceID;

		/// <remarks>
		/// A string identifying the boot source using the format "<OrgID>:<identifier>:<index>", in which neither <OrgID>, <identifier> or <index> contains a colon (":"). The value of <OrgID> is a copyrighted, trademarked or otherwise unique name that is owned by the entity defining the <identifier>, or is a registered ID that is assigned to the entity by a recognized global authority. For DMTF defined identifiers, the <OrgID> is set to 'CIM'. The <identifiers> are "Floppy", "Hard-Disk", "CD/DVD", "Network", "PCMCIA", "USB". The value of <index> shall be a non-zero integer.
		/// </remarks>
		public string StructuredBootString;

		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool StructuredBootStringSpecified;

	}
}

