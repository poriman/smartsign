// -----------------------------------------------------------------------------
//
// Copyright (c) 2009 Intel Corporation. All Rights Reserved.
//
// The source code contained or described herein and all documents related to 
// the source code ("Material") are owned by Intel Corporation or its suppliers 
// or licensors. Title to the Material remains with Intel Corporation or its 
// suppliers and licensors. The Material may contain trade secrets and 
// proprietary and confidential information of Intel Corporation and its 
// suppliers and licensors, and is protected by worldwide copyright and trade 
// secret laws and treaty provisions. No part of the Material may be used, 
// copied, reproduced, modified, published, uploaded, posted, transmitted, 
// distributed, or disclosed in any way without Intel�s prior express written 
// permission. 
//
// No license under any patent, copyright, trade secret or other intellectual 
// property right is granted to or conferred upon you by disclosure or delivery 
// of the Materials, either expressly, by implication, inducement, estoppel or 
// otherwise. Any license under such intellectual property rights must be 
// express and approved by Intel in writing.
//
// * Third Party trademarks are the property of their respective owners.
//
// Unless otherwise agreed by Intel in writing, you may not remove or alter this 
// notice or any other notice embedded in Materials by Intel or Intel�s 
// suppliers or licensors in any way.
//
// -----------------------------------------------------------------------------
// This file was automatically generated from CIM_SoftwareIdentity.mof
// Copyright (C) Intel Corporation, 2006-2008
namespace MOF_CLASSES{
	using System.Xml.Serialization;


	/// <remarks>
	/// SoftwareIdentity provides descriptive information about a software component for asset tracking and/or installation dependency management. When the IsEntity property has the value TRUE, the instance of SoftwareIdentity represents an individually identifiable entity similar to Physical Element. SoftwareIdentity does NOT indicate whether the software is installed, executing, etc. This extra information may be provided through specialized associations to Software Identity. For instance, both InstalledSoftwareIdentity and ElementSoftwareIdentity may be used to indicate that the software identified by this class is installed. SoftwareIdentity is used when managing the software components of a ManagedElement that is the management focus. Since software may be acquired, SoftwareIdentity can be associated with a Product using the ProductSoftwareComponent relationship. The Application Model manages the deployment and installation of software via the classes, SoftwareFeatures and SoftwareElements. SoftwareFeature and SoftwareElement are used when the software component is the management focus. The deployment/installation concepts are related to the asset/identity one. In fact, a SoftwareIdentity may correspond to a Product, or to one or more SoftwareFeatures or SoftwareElements - depending on the granularity of these classes and the deployment model. The correspondence of Software Identity to Product, SoftwareFeature or SoftwareElement is indicated using the ConcreteIdentity association. Note that there may not be sufficient detail or instrumentation to instantiate ConcreteIdentity. And, if the association is instantiated, some duplication of information may result. For example, the Vendor described in the instances of Product and SoftwareIdentity MAY be the same. However, this is not necessarily true, and it is why vendor and similar information are duplicated in this class. 
	/// Note that ConcreteIdentity can also be used to describe the relationship of the software to any LogicalFiles that result from installing it. As above, there may not be sufficient detail or instrumentation to instantiate this association.
	/// </remarks>
	[System.SerializableAttribute()]
	[System.Xml.Serialization.XmlTypeAttribute(Namespace="http://schemas.dmtf.org/wbem/wscim/1/cim-schema/2/CIM_SoftwareIdentity")]
	[System.Xml.Serialization.XmlRootAttribute("CIM_SoftwareIdentity", Namespace="http://schemas.dmtf.org/wbem/wscim/1/cim-schema/2/CIM_SoftwareIdentity", IsNullable=false)]
	public partial class CIM_SoftwareIdentityType {

		/// <remarks>
		/// The build number of the software.
		/// </remarks>
		public ushort BuildNumber;

		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool BuildNumberSpecified;

		/// <remarks>
		/// The Caption property is a short textual description (one- line string) of the object.
		/// </remarks>
		public string Caption;

		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool CaptionSpecified;

		/// <remarks>
		/// An array of free-form strings providing more detailed explanations for any of the entries in the Classifications array. Note that each entry is related to one in the Classifications array located at the same index.
		/// </remarks>
		[System.Xml.Serialization.XmlElementAttribute("ClassificationDescriptions")]
		public string[] ClassificationDescriptions;

		/// <remarks>
		/// An array of enumerated integers that classify this software. For example, the software MAY be instrumentation (value=5) or firmware and diagnostic software (10 and 7). The use of value 6, Firmware/BIOS, is being deprecated. Instead, either the value 10 (Firmware) and/or 11 (BIOS/FCode) SHOULD be used. The value 13, Software Bundle, identifies a software package consisting of multiple discrete software instances that can be installed individually or together.
		/// Each contained software instance is represented by an instance of SoftwareIdentity that is associated to this instance of SoftwareIdentityinstance via a Component association.
		/// </remarks>
		[System.Xml.Serialization.XmlElementAttribute("Classifications")]
		public ushort[] Classifications;

		/// <remarks>
		/// The Description property provides a textual description of the object.
		/// </remarks>
		public string Description;

		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool DescriptionSpecified;

		/// <remarks>
		/// A user-friendly name for the object. This property allows each instance to define a user-friendly name in addition to its key properties, identity data, and description information. 
		/// Note that the Name property of ManagedSystemElement is also defined as a user-friendly name. But, it is often subclassed to be a Key. It is not reasonable that the same property can convey both identity and a user-friendly name, without inconsistencies. Where Name exists and is not a Key (such as for instances of LogicalDevice), the same information can be present in both the Name and ElementName properties.
		/// </remarks>
		public string ElementName;

		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool ElementNameSpecified;

		/// <remarks>
		/// Indicates the current health of the element. This attribute expresses the health of this element but not necessarily that of its subcomponents. The possible values are 0 to 30, where 5 means the element is entirely healthy and 30 means the element is completely non-functional. The following continuum is defined: 
		/// "Non-recoverable Error" (30) - The element has completely failed, and recovery is not possible. All functionality provided by this element has been lost. 
		/// "Critical Failure" (25) - The element is non-functional and recovery might not be possible. 
		/// "Major Failure" (20) - The element is failing. It is possible that some or all of the functionality of this component is degraded or not working. 
		/// "Minor Failure" (15) - All functionality is available but some might be degraded. 
		/// "Degraded/Warning" (10) - The element is in working order and all functionality is provided. However, the element is not working to the best of its abilities. For example, the element might not be operating at optimal performance or it might be reporting recoverable errors. 
		/// "OK" (5) - The element is fully functional and is operating within normal operational parameters and without error. 
		/// "Unknown" (0) - The implementation cannot report on HealthState at this time. 
		/// DMTF has reserved the unused portion of the continuum for additional HealthStates in the future.
		/// </remarks>
		public ushort HealthState;

		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool HealthStateSpecified;

		/// <remarks>
		/// An indexed array of fixed-form strings that provide the description of the type of information that is stored in the corresponding component of the IdentityInfoValue array. The elements of this property array describe the type of the value in the corresponding elements of the IndetityInfoValue array. When the IdentityInfoValue property is implemented, the IdentityInfoType property MUST be implemented. To insure uniqueness the IdentityInfoType property SHOULD be formatted using the following algorithm: < OrgID > : < LocalID > Where < OrgID > and < LocalID > are separated by a colon (:), and where < OrgID > MUST include a copyrighted, trademarked, or otherwise unique name that is owned by the business entity that is creating or defining the IdentityInfoType or that is a registered ID assigned to the business entity by a recognized global authority. (This requirement is similar to the < Schema Name > _ < Class Name > structure of Schema class names.) In addition, to ensure uniqueness, < OrgID > MUST NOT contain a colon (:). When using this algorithm, the first colon to appear in IdentityInfoType MUST appear between < OrgID > and < LocalID > . < LocalID > is chosen by the business entity and SHOULD NOT be reused to identify different underlying software elements.
		/// </remarks>
		[System.Xml.Serialization.XmlElementAttribute("IdentityInfoType")]
		public string[] IdentityInfoType;

		/// <remarks>
		/// IdentityInfoValue captures additional information that MAY be used by an organization to describe or identify a software instance within the context of the organization. For example, large organizations may have several ways to address or identify a particular instance of software depending on where it is stored; a catalog, a web site, or for whom it is intended; development, customer service, etc. The indexed array property IdentityInfoValue contains 0 or more strings that contain a specific identity info string value. IdentityInfoValue is mapped and indexed to IdentityInfoType. When the IdentityInfoValue property is implemented, the IdentityInfoType property MUST be implemented and shall be formatted using the algorithm provided in the IdentityInfoType property Description.
		/// </remarks>
		[System.Xml.Serialization.XmlElementAttribute("IdentityInfoValue")]
		public string[] IdentityInfoValue;

		/// <remarks>
		/// A datetime value that indicates when the object was installed. Lack of a value does not indicate that the object is not installed.
		/// </remarks>
		public DateTime InstallDate;

		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool InstallDateSpecified;

		/// <remarks>
		/// Within the scope of the instantiating Namespace, InstanceID opaquely and uniquely identifies an instance of this class. In order to ensure uniqueness within the NameSpace, the value of InstanceID SHOULD be constructed using the following 'preferred' algorithm: 
		/// <OrgID>:<LocalID> 
		/// Where <OrgID> and <LocalID> are separated by a colon ':', and where <OrgID> MUST include a copyrighted, trademarked or otherwise unique name that is owned by the business entity creating/defining the InstanceID, or is a registered ID that is assigned to the business entity by a recognized global authority (This is similar to the <Schema Name>_<Class Name> structure of Schema class names.) In addition, to ensure uniqueness <OrgID> MUST NOT contain a colon (':'). When using this algorithm, the first colon to appear in InstanceID MUST appear between <OrgID> and <LocalID>. 
		/// <LocalID> is chosen by the business entity and SHOULD not be re-used to identify different underlying (real-world) elements. If the above 'preferred' algorithm is not used, the defining entity MUST assure that the resultant InstanceID is not re-used across any InstanceIDs produced by this or other providers for this instance's NameSpace. 
		/// For DMTF defined instances, the 'preferred' algorithm MUST be used with the <OrgID> set to 'CIM'.
		/// </remarks>
		[CimKey]
		public string InstanceID;

		/// <remarks>
		/// The IsEntity property is used to indicate whether the SoftwareIdentity corresponds to a discrete copy of the software component or is being used to convey descriptive and identifying information about software that is not present in the management domain.A value of TRUE shall indicate that the SoftwareIdentity instance corresponds to a discrete copy of the software component. A value of FALSE shall indicate that the SoftwareIdentity instance does not correspond to a discrete copy of the Software.
		/// </remarks>
		public bool IsEntity;

		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool IsEntitySpecified;

		/// <remarks>
		/// The language editions supported by the software. The language codes defined in ISO 639 should be used.
		/// </remarks>
		[System.Xml.Serialization.XmlElementAttribute("Languages")]
		public string[] Languages;

		/// <remarks>
		/// The major number component of the software's version information - for example, '12' from version 12.1(3)T. This property is defined as a numeric value to allow the determination of 'newer' vs. 'older' releases. A 'newer' major release is indicated by a larger numeric value.
		/// </remarks>
		public ushort MajorVersion;

		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool MajorVersionSpecified;

		/// <remarks>
		/// Manufacturer of this software.
		/// </remarks>
		public string Manufacturer;

		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool ManufacturerSpecified;

		/// <remarks>
		/// The minor number component of the software's version information - for example, '1' from version 12.1(3)T. This property is defined as a numeric value to allow the determination of 'newer' vs. 'older' releases. A 'newer' minor release is indicated by a larger numeric value.
		/// </remarks>
		public ushort MinorVersion;

		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool MinorVersionSpecified;

		/// <remarks>
		/// The Name property defines the label by which the object is known. When subclassed, the Name property can be overridden to be a Key property.
		/// </remarks>
		public string Name;

		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool NameSpecified;

		/// <remarks>
		/// Indicates the current statuses of the element. Various operational statuses are defined. Many of the enumeration's values are self-explanatory. However, a few are not and are described here in more detail. 
		/// "Stressed" indicates that the element is functioning, but needs attention. Examples of "Stressed" states are overload, overheated, and so on. 
		/// "Predictive Failure" indicates that an element is functioning nominally but predicting a failure in the near future. 
		/// "In Service" describes an element being configured, maintained, cleaned, or otherwise administered. 
		/// "No Contact" indicates that the monitoring system has knowledge of this element, but has never been able to establish communications with it. 
		/// "Lost Communication" indicates that the ManagedSystem Element is known to exist and has been contacted successfully in the past, but is currently unreachable. 
		/// "Stopped" and "Aborted" are similar, although the former implies a clean and orderly stop, while the latter implies an abrupt stop where the state and configuration of the element might need to be updated. 
		/// "Dormant" indicates that the element is inactive or quiesced. 
		/// "Supporting Entity in Error" indicates that this element might be "OK" but that another element, on which it is dependent, is in error. An example is a network service or endpoint that cannot function due to lower-layer networking problems. 
		/// "Completed" indicates that the element has completed its operation. This value should be combined with either OK, Error, or Degraded so that a client can tell if the complete operation Completed with OK (passed), Completed with Error (failed), or Completed with Degraded (the operation finished, but it did not complete OK or did not report an error). 
		/// "Power Mode" indicates that the element has additional power model information contained in the Associated PowerManagementService association. 
		/// OperationalStatus replaces the Status property on ManagedSystemElement to provide a consistent approach to enumerations, to address implementation needs for an array property, and to provide a migration path from today's environment to the future. This change was not made earlier because it required the deprecated qualifier. Due to the widespread use of the existing Status property in management applications, it is strongly recommended that providers or instrumentation provide both the Status and OperationalStatus properties. Further, the first value of OperationalStatus should contain the primary status for the element. When instrumented, Status (because it is single-valued) should also provide the primary status of the element.
		/// </remarks>
		[System.Xml.Serialization.XmlElementAttribute("OperationalStatus")]
		public ushort[] OperationalStatus;

		/// <remarks>
		/// The date the software was released.
		/// </remarks>
		public DateTime ReleaseDate;

		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool ReleaseDateSpecified;

		/// <remarks>
		/// The revision or maintenance release component of the software's version information - for example, '3' from version 12.1(3)T. This property is defined as a numeric value to allow the determination of 'newer' vs. 'older' releases. A 'newer' revision is indicated by a larger numeric value.
		/// </remarks>
		public ushort RevisionNumber;

		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool RevisionNumberSpecified;

		/// <remarks>
		/// A manufacturer-allocated number used to identify the software.
		/// </remarks>
		public string SerialNumber;

		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool SerialNumberSpecified;

		/// <remarks>
		/// A string indicating the current status of the object. Various operational and non-operational statuses are defined. This property is deprecated in lieu of OperationalStatus, which includes the same semantics in its enumeration. This change is made for 3 reasons: 
		/// 1) Status is more correctly defined as an array. This definition overcomes the limitation of describing status using a single value, when it is really a multi-valued property (for example, an element might be OK AND Stopped. 
		/// 2) A MaxLen of 10 is too restrictive and leads to unclear enumerated values. 
		/// 3) The change to a uint16 data type was discussed when CIM V2.0 was defined. However, existing V1.0 implementations used the string property and did not want to modify their code. Therefore, Status was grandfathered into the Schema. Use of the deprecated qualifier allows the maintenance of the existing property, but also permits an improved definition using OperationalStatus.
		/// </remarks>
		public string Status;

		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool StatusSpecified;

		/// <remarks>
		/// Strings describing the various OperationalStatus array values. For example, if "Stopping" is the value assigned to OperationalStatus, then this property may contain an explanation as to why an object is being stopped. Note that entries in this array are correlated with those at the same array index in OperationalStatus.
		/// </remarks>
		[System.Xml.Serialization.XmlElementAttribute("StatusDescriptions")]
		public string[] StatusDescriptions;

		/// <remarks>
		/// The TargetOSTypes property specifies the target operating systems supported by the software. When the target operating system of the software is not listed in the enumeration values, TargetOperatingSystems[] property should be used to specify the target operating system.
		/// </remarks>
		[System.Xml.Serialization.XmlElementAttribute("TargetOSTypes")]
		public ushort[] TargetOSTypes;

		/// <remarks>
		/// Specifies the target operating systems of the software. This property should be used when a target operating system is not listed in the TargetOSTypes array values.
		/// </remarks>
		[System.Xml.Serialization.XmlElementAttribute("TargetOperatingSystems")]
		public string[] TargetOperatingSystems;

		/// <remarks>
		/// A string representing the complete software version information - for example, '12.1(3)T'. This string and the numeric major/minor/revision/build properties are complementary. Since vastly different representations and semantics exist for versions, it is not assumed that one representation is sufficient to permit a client to perform computations (i.e., the values are numeric) and a user to recognize the software's version (i.e., the values are understandable and readable). Hence, both numeric and string representations of version are provided.
		/// </remarks>
		public string VersionString;

		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool VersionStringSpecified;

	}
}

