// -----------------------------------------------------------------------------
//
// Copyright (c) 2009 Intel Corporation. All Rights Reserved.
//
// The source code contained or described herein and all documents related to 
// the source code ("Material") are owned by Intel Corporation or its suppliers 
// or licensors. Title to the Material remains with Intel Corporation or its 
// suppliers and licensors. The Material may contain trade secrets and 
// proprietary and confidential information of Intel Corporation and its 
// suppliers and licensors, and is protected by worldwide copyright and trade 
// secret laws and treaty provisions. No part of the Material may be used, 
// copied, reproduced, modified, published, uploaded, posted, transmitted, 
// distributed, or disclosed in any way without Intel�s prior express written 
// permission. 
//
// No license under any patent, copyright, trade secret or other intellectual 
// property right is granted to or conferred upon you by disclosure or delivery 
// of the Materials, either expressly, by implication, inducement, estoppel or 
// otherwise. Any license under such intellectual property rights must be 
// express and approved by Intel in writing.
//
// * Third Party trademarks are the property of their respective owners.
//
// Unless otherwise agreed by Intel in writing, you may not remove or alter this 
// notice or any other notice embedded in Materials by Intel or Intel�s 
// suppliers or licensors in any way.
//
// -----------------------------------------------------------------------------
// This file was automatically generated from CIM_PowerManagementCapabilities.mof
// Copyright (C) Intel Corporation, 2006-2008
namespace MOF_CLASSES{
	using System.Xml.Serialization;


	/// <remarks>
	/// A class derived from Capabilities that describes the power management aspects of an element (typically a system or device). The element's power management capabilities are decoupled from a PowerManagementService, since a single service could apply to multiple elements, each with specific capabilities.
	/// </remarks>
	[System.SerializableAttribute()]
	[System.Xml.Serialization.XmlTypeAttribute(Namespace="http://schemas.dmtf.org/wbem/wscim/1/cim-schema/2/CIM_PowerManagementCapabilities")]
	[System.Xml.Serialization.XmlRootAttribute("CIM_PowerManagementCapabilities", Namespace="http://schemas.dmtf.org/wbem/wscim/1/cim-schema/2/CIM_PowerManagementCapabilities", IsNullable=false)]
	public partial class CIM_PowerManagementCapabilitiesType {

		/// <remarks>
		/// The Caption property is a short textual description (one- line string) of the object.
		/// </remarks>
		public string Caption;

		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool CaptionSpecified;

		/// <remarks>
		/// The Description property provides a textual description of the object.
		/// </remarks>
		public string Description;

		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool DescriptionSpecified;

		/// <remarks>
		/// The user friendly name for this instance of Capabilities. In addition, the user friendly name can be used as a index property for a search of query. (Note: Name does not have to be unique within a namespace.)
		/// </remarks>
		public string ElementName;

		/// <remarks>
		/// Within the scope of the instantiating Namespace, InstanceID opaquely and uniquely identifies an instance of this class. In order to ensure uniqueness within the NameSpace, the value of InstanceID SHOULD be constructed using the following 'preferred' algorithm: 
		/// <OrgID>:<LocalID> 
		/// Where <OrgID> and <LocalID> are separated by a colon ':', and where <OrgID> MUST include a copyrighted, trademarked or otherwise unique name that is owned by the business entity creating/defining the InstanceID, or is a registered ID that is assigned to the business entity by a recognized global authority (This is similar to the <Schema Name>_<Class Name> structure of Schema class names.) In addition, to ensure uniqueness <OrgID> MUST NOT contain a colon (':'). When using this algorithm, the first colon to appear in InstanceID MUST appear between <OrgID> and <LocalID>. 
		/// <LocalID> is chosen by the business entity and SHOULD not be re-used to identify different underlying (real-world) elements. If the above 'preferred' algorithm is not used, the defining entity MUST assure that the resultant InstanceID is not re-used across any InstanceIDs produced by this or other providers for this instance's NameSpace. 
		/// For DMTF defined instances, the 'preferred' algorithm MUST be used with the <OrgID> set to 'CIM'.
		/// </remarks>
		[CimKey]
		public string InstanceID;

		/// <remarks>
		/// An array of strings describing an element's additional power management capabilities, used when the PowerCapabilities array includes the value 1, "Other".
		/// </remarks>
		[System.Xml.Serialization.XmlElementAttribute("OtherPowerCapabilitiesDescriptions")]
		public string[] OtherPowerCapabilitiesDescriptions;

		/// <remarks>
		/// A string describing the additional power management capabilities of the element, used when the PowerChangeCapabilities is set to the value 1, "Other".
		/// </remarks>
		public string OtherPowerChangeCapabilities;

		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool OtherPowerChangeCapabilitiesSpecified;

		/// <remarks>
		/// An enumeration indicating the specific power-related capabilities of a managed element. Since this is an array, multiple values may be specified. The current values in the enumeration are: 
		/// 0 = Unknown 
		/// 1 = Other 
		/// 2 = Power Saving Modes Entered Automatically, describing that a managed element can change its power state based on usage or other criteria 
		/// 3 = Power State Settable, indicating that the SetPowerState method is supported 
		/// 4 = Power Cycling Supported, indicating that the SetPowerState method can be invoked with the PowerState input variable set to 'Power Cycle' 
		/// 5 = Timed Power On Supported, indicating that the SetPowerState method can be invoked with the PowerState input variable set to 'Power Cycle' and the Time parameter set to a specific date and time, or interval, for power-on.
		/// </remarks>
		[System.Xml.Serialization.XmlElementAttribute("PowerCapabilities")]
		public ushort[] PowerCapabilities;

		/// <remarks>
		/// An enumeration indicating the specific power-related capabilities of a managed element. Since this is an array, multiple values may be specified. The current values in the enumeration are: 
		/// 0 = Unknown 
		/// 1 = Other 
		/// 2 = Power Saving Modes Entered Automatically, describing that a managed element can change its power state based on usage or other criteria 
		/// 3 = Power State Settable, indicating that the RequestPowerStateChange method is supported 
		/// 4 = Power Cycling Supported, indicating that the RequestPowerStateChange method can be invoked with the PowerState input variable set to 'Power Cycle (Off Soft)' 
		/// 5 = Timed Power On Supported, indicating that the RequestPowerStateChange method can be invoked with the PowerState input variable set to 'Power On' and the Time parameter set to a specific date and time, or interval, for power-on.8 = Graceful Shutdown Supported, indicating that the managed element can be sent a hardware signal requesting an orderly shutdown prior to the requested power state change.
		/// </remarks>
		[System.Xml.Serialization.XmlElementAttribute("PowerChangeCapabilities")]
		public ushort[] PowerChangeCapabilities;

		/// <remarks>
		/// An enumeration that indicates the power states supported by a managed element. Because this is an array, multiple values can be specified. The current values in the enumeration are: 
		/// 2=On, corresponding to ACPI state G0 or S0 or D0. 
		/// 3=Sleep - Light, corresponding to ACPI state G1, S1/S2, or D1. 
		/// 4=Sleep - Deep, corresponding to ACPI state G1, S3, or D2.
		/// 5=Power Cycle (Off - Soft), corresponding to ACPI state G2, S5, or D3, but where the managed element is set to return to power state "On" at a pre-determined time. 
		/// 6=Off - Hard, corresponding to ACPI state G3, S5, or D3. 
		/// 7=Hibernate (Off - Soft), corresponding to ACPI state S4, where the state of the managed element is preserved and will be recovered upon powering on. 
		/// 8=Off - Soft, corresponding to ACPI state G2, S5, or D3. 9= Power Cycle (Off-Hard), corresponds to the managed element reaching the ACPI state G3 followed by ACPI state S0. 
		/// 10=Master Bus Reset, corresponds to the system reaching ACPI state S5 followed by ACPI state S0. This is used to represent system master bus reset. 11=Diagnostic Interrupt (NMI), corresponding to the system reaching ACPI state S5 followed by ACPI state S0. This is used to represent system non-maskable interrupt. 12=Off - Soft Graceful, equivalent to Off Soft but preceded by a request to the managed element to perform an orderlyshutdown. 
		/// 13=Off - Hard Graceful, equivalent to Off Hard but preceded by a request to the managed element to perform an orderly shutdown. 
		/// 14=Master Bus Rest Graceful, equivalent to Master Bus Reset but preceded by a request to the managed element to perform an orderly shutdown. 
		/// 15=Power Cycle (Off - Soft Graceful), equivalent to Power Cycle (Off - Soft) but preceded by a request to the managed element to perform an orderly shutdown. 
		/// 16=Power Cycle (Off - Hard Graceful), equivalent to Power Cycle (Off - Hard) but preceded by a request to the managed element to perform an orderly shutdown. 
		/// ..=DMTF Reserved. 
		/// 0x7FFF..0xFFFF = Vendor Specific.
		/// </remarks>
		[System.Xml.Serialization.XmlElementAttribute("PowerStatesSupported")]
		public ushort[] PowerStatesSupported;

	}
}

