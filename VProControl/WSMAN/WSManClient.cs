// -----------------------------------------------------------------------------
//
// Copyright (c) 2009 Intel Corporation. All Rights Reserved.
//
// The source code contained or described herein and all documents related to 
// the source code ("Material") are owned by Intel Corporation or its suppliers 
// or licensors. Title to the Material remains with Intel Corporation or its 
// suppliers and licensors. The Material may contain trade secrets and 
// proprietary and confidential information of Intel Corporation and its 
// suppliers and licensors, and is protected by worldwide copyright and trade 
// secret laws and treaty provisions. No part of the Material may be used, 
// copied, reproduced, modified, published, uploaded, posted, transmitted, 
// distributed, or disclosed in any way without Intel�s prior express written 
// permission. 
//
// No license under any patent, copyright, trade secret or other intellectual 
// property right is granted to or conferred upon you by disclosure or delivery 
// of the Materials, either expressly, by implication, inducement, estoppel or 
// otherwise. Any license under such intellectual property rights must be 
// express and approved by Intel in writing.
//
// * Third Party trademarks are the property of their respective owners.
//
// Unless otherwise agreed by Intel in writing, you may not remove or alter this 
// notice or any other notice embedded in Materials by Intel or Intel�s 
// suppliers or licensors in any way.
//
//  Contents:   This file is the infrastructure for most of the C# WS-Management samples. 
//              It contains a wrapper to the Microsoft WinRm API.   
//
// -----------------------------------------------------------------------------

using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.Schema;
using System.Reflection;
using WSManAutomation;

namespace MOF_CLASSES
{
    
    public class CimKeyAttribute : Attribute
    {
    }

    /// <summary>
    /// This class is a wrapper for the Windows Remote Management (WinRM). 
    /// WinRM is the Microsoft implementation of WS-Management Protocol.
    /// </summary>
    public class WSManClient
    {
        #region DATA_MEMBERS

        // Intel(R) AMT endpoint IP address.
        private string _address; 
        private string _ip;
        private bool _secure;
        //private bool _kerberos;

        // user credentials
        private string _username = null;
        private string _password = null;

        // WS-Management session 
        private WSManClass _WSMan = null;
        private IWSManSession _Session = null;

        #endregion DATA_MEMBERS

        #region CONSTANTS

        // WS-Management namespace URI
        public const string WSMAN_NAMESPACE_URI = "http://schemas.dmtf.org/wbem/wsman/1/wsman.xsd";
        public const string WSMAN_PREFIX = "wsman";
        public const string WSMAN_URI_FIELD = "ResourceURI";
        public const string WSMAN_SELECTOR_SET_FIELD = "SelectorSet";
        public const string WSMAN_SELECTOR_FIELD = "Selector";
        public const string WSMAN_SELECTOR_NAME = "Name";
        #endregion CONSTANTS

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="ip">string, IP address of the Intel(R) AMT machine</param>
        /// <param name="username">string, username</param>
        /// <param name="password">string, password</param>
        /// <param name="secure">bool, true if using a secure tls connection</param>
        public WSManClient(String ip, String username, String password, bool secure)
        {
            _ip = ip;
            _username = username;
            _password = password;
            _secure = secure;
            _address = (_secure ? "https://" : "http://") + _ip + (_secure ? ":16993" : ":16992");
            CreateSession();
        }

        /// <summary>
        /// Property: Get/Set the IP address. 
        /// </summary>
        public string ip
        {
            get { return _ip; }
            private set { _ip = value; CreateSession(); }
        }


        /// <summary>
        /// Creates a new instance of a resource and returns the URI of the new object.
        /// </summary>
        /// <param name="resourceUri"></param>
        /// <param name="resource"></param>
        /// <returns></returns>
        public string Create(string resourceUri, string resource)
        {
            return _Session.Create(resourceUri, resource, 0);
        }

        /// <summary>
        /// Creates a new instance of a resource and returns the new object.
        /// </summary>
        /// <param name="resource"></param>
        /// <returns></returns>
        public Object Create(Object resource)
        {
            // Serialize request.
            XmlSerializer mySerializer = new XmlSerializer(resource.GetType());
            StringWriter writer = new StringWriter();
            mySerializer.Serialize(writer, resource);
            string output = Create(GetResourceUri(resource.GetType()), writer.ToString());

            
            // Deserialize response
            mySerializer = new XmlSerializer(typeof(ResourceCreatedType));
            ResourceCreatedType resCreated = (ResourceCreatedType)mySerializer.Deserialize(new StringReader(output));
            
            // Cast the ResourceCreatedType to EndpointReferenceType
            EndpointReferenceType refOut = new EndpointReferenceType();
            refOut.Address = resCreated.Address;
            refOut.PortType = resCreated.PortType;
            refOut.ReferenceParameters = resCreated.ReferenceParameters;
            refOut.ReferenceProperties = resCreated.ReferenceProperties;
            refOut.ServiceName = resCreated.ServiceName;
            refOut.Any = resCreated.Any;
            refOut.AnyAttr = resCreated.AnyAttr;

            return refOut;
        }

        /// <summary>
        /// Delete the resource specified in the resource URI 
        /// (Only if one instance of the object exists)
        /// </summary>
        /// <param name="resourceUri">string, the identifier of the resource to be deleted</param>
        public void Delete(string resourceUri)
        {
             // Delete instance
            _Session.Delete(resourceUri, 0);
        }

        /// <summary>
        /// Delete the resource.
        /// </summary>
        /// <param name="resource">EndpointReference to delete</param>
        public void Delete(EndpointReferenceType reference)
        {
            _Session.Delete(GetResourceLocator(reference), 0);
        }

        /// <summary>
        /// Enumerate resource. 
        /// </summary>
        /// <param name="resourceUri">string, The identifier of the resource to be retrived</param>
        /// <returns>ArrayList of strings containing xml response</returns>
        public ArrayList Enumerate(string resourceUri)
        {
            ArrayList container = new ArrayList();

            try
            {
                // Enumerate resource
                IWSManEnumerator EnumObj =
                    (IWSManEnumerator)_Session.Enumerate(resourceUri, "", "", 0);

                while (!EnumObj.AtEndOfStream)
                {
                    container.Add(EnumObj.ReadItem());
                }
            }
            catch (System.Runtime.InteropServices.COMException e)
            {

                // This is the WS-Management fault returned by the Intel(R) AMT device if no instances of the requested resource URI exist
                if (!e.Message.StartsWith("The client cannot connect to the remote host specified in the request."))
                {
                    throw e;
                }
            }
            return container;
        }

        /// <summary>
        /// Enumerate resource, and deserialize response according to an expected type.
        /// </summary>
        /// <remarks>
        /// Note: This method does not support inheritance, in case there are instances 
        /// of objects which inherit from the requested resource they will not be returned.
        /// It is guaranteed that the ArrayList will contain objects of Type 'type'
        /// </remarks>
        /// <param name="type">Type, the type of the resource to be retrieved</param>
        /// <returns>ArrayList of Objects from type 'type'</returns>
        public ArrayList Enumerate(Type type)
        {
            ArrayList container = new ArrayList();

            // Construct an instance of the XmlSerializer with the type
            // of object that is being deserialized.
            XmlSerializer mySerializer = new XmlSerializer(type);

            try
            {
                // Enumerate resource
                IWSManEnumerator EnumObj =
                    (IWSManEnumerator)_Session.Enumerate(GetResourceUri(type), "", "", 0);
                while (!EnumObj.AtEndOfStream)
                {
                    // Call the Deserialize method.
                    string curItem = EnumObj.ReadItem();
                    if (GetType(curItem) != type) continue;
                    container.Add(mySerializer.Deserialize(new StringReader(curItem)));
                }
            }
            catch (System.Runtime.InteropServices.COMException e)
            {

                // This is the WS-Management fault returned by the Intel(R) AMT device if no instances of the requested resource URI exist
                if (!e.Message.StartsWith("The client cannot connect to the remote host specified in the request."))
                {
                    throw e;
                }                
            }
            return container;
        }

        /// <summary>
        /// Retrieves the resource specified by 'resource' and returns an XML representation 
        /// of the current instance of the resource. 
        /// </summary>
        /// <param name="resourceUri">string, the identifier of the resource to be retrieved</param>
        /// <returns>String, an XML representation of the current instance of the resource. </returns>
        public String Get(string resourceUri)
        {

            // Get instance
            return _Session.Get(resourceUri, 0);
        }

        /// <summary>
        /// Retrieves the resource specified by resource type.
        /// </summary>
        /// <param name="type">Type, The of the resource to be retrieved</param>
        /// <returns>Object of type 'type' holding the instance of the resource</returns>
        public Object Get(Type type)
        {
            // Get instance and Desrialized it.
            XmlSerializer mySerializer = new XmlSerializer(type);
            return mySerializer.Deserialize(new StringReader(Get(GetResourceUri(type))));
        }

        /// <summary>
        /// Retrieves the resource specified 'resource' reference.
        /// </summary>
        /// <param name="resource">Endpoint Reference of the resource</param>
        /// <returns>instance of the resource</returns>
        public Object Get(EndpointReferenceType resource)
        {
            XmlSerializer mySerializer = new XmlSerializer(GetReferenceType(resource));
            return mySerializer.Deserialize(new StringReader(_Session.Get(GetResourceLocator(resource), 0)));
        }

        /// <summary>
        /// Update a resource.
        /// </summary>
        /// <param name="resourceUri">string, the identifier of the resource to update</param>
        /// <param name="content">string, an XML string holding the updated resource content</param>
        /// <returns>The URI of the updated resource.</returns>
        public void Put(string resourceUri, string content)
        {
            _Session.Put(resourceUri, content, 0);
        }

        /// <summary>
        /// Update a resource, serialize 'content' to get an XML request.
        /// </summary>
        /// <param name="content">Object, an object representing the updated resource content</param>
        /// <returns>The URI of the updated resource.</returns>
        public void Put(Object content)
        {
            XmlSerializer mySerializer = new XmlSerializer(content.GetType());
            StringWriter writer = new StringWriter();
            mySerializer.Serialize(writer, content);
            Put(GetResourceUri(content.GetType()), writer.ToString());
        }

        /// <summary>
        /// Update a resource, serialize 'content' to get an XML request.
        /// </summary>
        /// <param name="content">Object representing the updated resource content</param>
        /// <param name="reference">Specifies a particular instance of the resource</param>
        /// <returns>The URI of the updated resource</returns>
        public void Put(Object content, EndpointReferenceType reference)
        {
            XmlSerializer mySerializer = new XmlSerializer(content.GetType());
            StringWriter writer = new StringWriter();
            mySerializer.Serialize(writer, content);
            _Session.Put(GetResourceLocator(reference), writer.ToString(), 0);
        }

        /// <summary>
        /// Invokes a method and returns the results of the method call.
        /// </summary>
        /// <param name="resourceUri">string, the identifier of the resource</param>
        /// <param name="actionUri">string, the URI of the method to be invoked</param>
        /// <param name="request">string, the XML representation of the input for the method</param>
        /// <returns>The XML representation of the method output</returns>
        public String Invoke(string resourceUri,
                             string actionUri,
                             string request)
        {
            // Invoke
            return _Session.Invoke(actionUri, resourceUri, request, 0);
            
        }

        /// <summary>
        /// Invokes a method and returns the results of the method call.
        /// </summary>
        /// <param name="request">The input object for the method </param>
        /// <returns>Method output</returns>
        public Object Invoke(Object request)
        {
            XmlSerializer mySerializer = new XmlSerializer(request.GetType());
            StringWriter writer = new StringWriter();
            String actionUri = request.GetType().Name;
            if (actionUri.EndsWith("_INPUT"))
            {
                int index = actionUri.IndexOf("_INPUT");
                actionUri = actionUri.Remove(actionUri.IndexOf("_INPUT"));
            }

            mySerializer.Serialize(writer, request);
            String output = Invoke(GetResourceUri(request.GetType()),
                                    actionUri, writer.ToString());

            // Desrialized response
            mySerializer = new XmlSerializer(GetType(output));
            return mySerializer.Deserialize(new StringReader(output));
        }

        /// <summary>
        /// Invokes a method and returns the results of the method call. 
        /// </summary>
        /// <param name="request">The input object for the method </param>
        /// <param name="reference">specifies a particular instance of the resource</param>
        /// <returns></returns>
        public Object Invoke(Object request,
                             EndpointReferenceType reference)
        {
            // Invoke
            XmlSerializer mySerializer = new XmlSerializer(request.GetType());
            StringWriter writer = new StringWriter();
            String actionUri = request.GetType().Name;
            if (actionUri.EndsWith("_INPUT"))
            {
                int index = actionUri.IndexOf("_INPUT");
                actionUri = actionUri.Remove(actionUri.IndexOf("_INPUT"));
            }

            mySerializer.Serialize(writer, request);
            String output = _Session.Invoke(actionUri, GetResourceLocator(reference), writer.ToString(), 0);

            // Desrialized response
            mySerializer = new XmlSerializer(GetType(output));
            return mySerializer.Deserialize(new StringReader(output));
        }

        /// <summary>
        /// Open a Session that can then be used for subsequent network operations
        /// </summary>
        public void CreateSession()
        {
            bool _kerberos = false;

            _WSMan = new WSManClass();
            if ((_username.Length == 0) && (_password.Length == 0))
                _kerberos = true;
            int iFlags = _WSMan.SessionFlagUTF8();

            if (_kerberos == false)
            {
                iFlags += (_WSMan.SessionFlagCredUsernamePassword() +
                           _WSMan.SessionFlagUseDigest());
            }
            else
            {
                iFlags += _WSMan.SessionFlagUseKerberos();
                iFlags += _WSMan.SessionFlagEnableSPNServerPort(); //indicate port must be retained
            }

            if (!_secure)
            {
                iFlags += _WSMan.SessionFlagNoEncryption();
            }

            IWSManConnectionOptions ConnectionOptions =
                (IWSManConnectionOptions)_WSMan.CreateConnectionOptions();
            ConnectionOptions.UserName = _username;
            ConnectionOptions.Password = _password;
            _Session = (IWSManSession)_WSMan.CreateSession(_address, iFlags, 
                (_kerberos ? null : ConnectionOptions));
        }

        /// <summary>
        /// Return the Type of the resource. 
        /// </summary>
        /// <param name="resource">string, an XML representation of the resource</param>
        /// <returns>Type, resource Type</returns>
        public static Type GetType(string resource)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(resource);
            Type type = Type.GetType(doc.DocumentElement.LocalName);
            if (type == null)
            {
                string className = doc.DocumentElement.NamespaceURI;
                int classNameIndex = className.LastIndexOf("/");               
                className = className.Substring(classNameIndex + 1) +"Type+";
                string typeName = "MOF_CLASSES." + className + doc.DocumentElement.LocalName;
                type = Type.GetType(typeName);
                if (type == null)
                    type = Type.GetType("MOF_CLASSES." + doc.DocumentElement.LocalName + "Type", true);
            }
            return type;
        }

        /// <summary>
        /// Get the URI (the identifier) of the resource object
        /// </summary>
        /// <param name="type">Type, the type of the resource object</param>
        /// <returns>The identifier of the resource object</returns>
        public static string GetResourceUri(Type type)
        {
            foreach (object at in type.GetCustomAttributes(false))
            {
                if (at.GetType() == typeof(System.Xml.Serialization.XmlRootAttribute))
                {
                    return ((System.Xml.Serialization.XmlRootAttribute)at).Namespace;
                }
            }
            return "";
        }

        /// <summary>
        /// Return the Type of the endpoint reference
        /// </summary>
        /// <param name="reference">EndpointReferenceType, an endpoint reference</param>
        /// <returns>Type, the type of the endpoint reference</returns>
        public static Type GetReferenceType(EndpointReferenceType reference)
        {
            string uri = GetReferenceUri(reference);
            if (uri != null)
            {
                uri = uri.Remove(0, uri.LastIndexOf('/') + 1);
                return Type.GetType("MOF_CLASSES." + uri + "Type");
            }
            return null;
        }


        private IWSManResourceLocator GetResourceLocator(EndpointReferenceType reference)
        {
            IWSManResourceLocator locator = (IWSManResourceLocator)
                _WSMan.CreateResourceLocator(GetReferenceUri(reference));

            // Parse the reference XML and add the selector to the ResourceLocator object.
            XmlElement selectorSetXml = null;
            for (IEnumerator e = reference.ReferenceParameters.Any.GetEnumerator(); e.MoveNext(); )
            {
                if (((XmlElement)e.Current).LocalName.ToLower().Equals("selectorset"))
                {
                    selectorSetXml = ((XmlElement)e.Current);
                }
            }

            if (selectorSetXml != null)
            {
                for (IEnumerator nodes = selectorSetXml.GetEnumerator(); nodes.MoveNext(); )
                {
                    XmlNode curNode = (XmlNode)nodes.Current;
                    if (!curNode.LocalName.ToLower().Equals("selector"))
                        continue;
                    XmlAttributeCollection at = ((XmlNode)nodes.Current).Attributes;
                    for (IEnumerator cur = at.GetEnumerator(); cur.MoveNext(); )
                    {
                        XmlAttribute curAtr = (XmlAttribute)cur.Current;
                        if (curAtr.Name.ToLower().Equals("name"))
                        {
                            //If an association class
                            if (curNode.LastChild.LocalName.ToLower().Equals("endpointreference"))
                            {
                                locator.AddSelector(curAtr.Value,curNode.LastChild.InnerXml);
                            }
                            else
                            {
                                locator.AddSelector(curAtr.Value,curNode.InnerXml);
                            }
                            break;
                        }
                    }
                }
            }
            return locator;
        }
 
        /// <summary>
        /// Return the URI from the endpoint reference
        /// </summary>
        /// <param name="reference">EndpointReferenceType, an endpoint reference</param>
        /// <returns>String, the URI of the endpoint reference</returns>
        public static string GetReferenceUri(EndpointReferenceType reference)
        {
            for (IEnumerator e = reference.ReferenceParameters.Any.GetEnumerator(); e.MoveNext(); )
            {
                if (((XmlElement)e.Current).LocalName.Equals("ResourceURI"))
                {
                    return ((XmlElement)e.Current).InnerText;
                }
            }
            return null;
        }

        /// <summary>
        /// Compare two EndpointReferenceType, return true iff two object refer to the same 
        /// resource.
        /// </summary>
        /// <param name="ref1">First reference object to compare</param>
        /// <param name="ref2">Second reference object to compare</param>
        /// <returns>true if both references refer to the same resource</returns>
        public static bool CompareReference(EndpointReferenceType ref1,
                                            EndpointReferenceType ref2)
        {
            if (ref1.ReferenceParameters.Any == null || ref2.ReferenceParameters.Any == null)
                return false;

            if (ref1.ReferenceParameters.Any.Length != ref2.ReferenceParameters.Any.Length)
                return false;

            for (int i = 0; i < ref1.ReferenceParameters.Any.Length; i++)
            {
                if (ref1.ReferenceParameters.Any[i].LocalName != ref2.ReferenceParameters.Any[i].LocalName ||
                    ref1.ReferenceParameters.Any[i].InnerXml != ref2.ReferenceParameters.Any[i].InnerXml)
                    return false;
            }
            return true;
        }

        /// <summary>
        /// Get endpoint reference.
        /// </summary>
        /// <param name="resource">Object of a CIM Class</param>
        /// <returns></returns>
        public EndpointReferenceType GetEndpointReference(Object resource)
        {
            Type myObjectType = resource.GetType();

            // Create a new EndpointReferenceType object and set its value
            EndpointReferenceType objRef = new EndpointReferenceType();
            
            // Set the endpoint address.
            objRef.Address = new AttributedURI();
            objRef.Address.Value = _address;

            // Create the selector set.
            ReferenceParametersType objParams = new ReferenceParametersType();
            objParams.Any = new XmlElement[2];
            XmlDocument paramsDoc = new XmlDocument();

            // First create a ResourceURI object for that reference
            objParams.Any[0] = paramsDoc.CreateElement(WSMAN_PREFIX, WSMAN_URI_FIELD, WSMAN_NAMESPACE_URI);
            objParams.Any[0].InnerText = GetResourceUri(myObjectType);

            // Go over this class members
            //objParams.Any[1].LocalName = "SelectorSet";
            bool selectorAdded = false;
            FieldInfo[] fields = myObjectType.GetFields();
            foreach (FieldInfo fieldObj in fields)
            {
                // For each member check whether this member is a key.
                foreach (object attr in Attribute.GetCustomAttributes(fieldObj))
                {
                    if (attr.GetType() == typeof(CimKeyAttribute))
                    {
                        if (selectorAdded == false)
                        {
                            objParams.Any[1] = paramsDoc.CreateElement(WSMAN_PREFIX, WSMAN_SELECTOR_SET_FIELD, WSMAN_NAMESPACE_URI);
                            selectorAdded = true;
                        }
                        XmlElement newChild = paramsDoc.CreateElement(WSMAN_PREFIX, WSMAN_SELECTOR_FIELD, WSMAN_NAMESPACE_URI);
                        newChild.SetAttribute(WSMAN_SELECTOR_NAME, fieldObj.Name);
                        newChild.InnerText = (string)fieldObj.GetValue(resource);
                        objParams.Any[1].AppendChild(newChild);
                    }
                }
            }
            objRef.ReferenceParameters = objParams;
            return objRef;
        }


        public long GetErrorCode()
        {
            StringReader sr= new StringReader(_Session.Error);
            XmlTextReader textReader = new XmlTextReader(sr);
            textReader.Read();
            int attCount=textReader.AttributeCount;
            textReader.MoveToFirstAttribute();
            long errCode = -1;
            string strErrorCode = "";
            for (int i = 0; i < attCount; i++)
            {
                if (textReader.Name == "Code")
                {
                    strErrorCode = textReader.ReadContentAsString();
                    break;
                }
                textReader.MoveToNextAttribute();
            }
            errCode = Convert.ToInt64(strErrorCode);
            return errCode;
     
        }


        public string GetErrorStr()
        {
            StringReader sr = new StringReader(_Session.Error);
            XmlTextReader textReader = new XmlTextReader(sr);
            string errorStr = "";
            while (textReader.Read())
            {
                if (textReader.Name == "f:Message")
                {
                    errorStr = textReader.ReadElementContentAsString();
                    break;
                }

            }
            return errorStr;

        }
    }
}
