// -----------------------------------------------------------------------------
//
// Copyright (c) 2009 Intel Corporation. All Rights Reserved.
//
// The source code contained or described herein and all documents related to 
// the source code ("Material") are owned by Intel Corporation or its suppliers 
// or licensors. Title to the Material remains with Intel Corporation or its 
// suppliers and licensors. The Material may contain trade secrets and 
// proprietary and confidential information of Intel Corporation and its 
// suppliers and licensors, and is protected by worldwide copyright and trade 
// secret laws and treaty provisions. No part of the Material may be used, 
// copied, reproduced, modified, published, uploaded, posted, transmitted, 
// distributed, or disclosed in any way without Intel�s prior express written 
// permission. 
//
// No license under any patent, copyright, trade secret or other intellectual 
// property right is granted to or conferred upon you by disclosure or delivery 
// of the Materials, either expressly, by implication, inducement, estoppel or 
// otherwise. Any license under such intellectual property rights must be 
// express and approved by Intel in writing.
//
// * Third Party trademarks are the property of their respective owners.
//
// Unless otherwise agreed by Intel in writing, you may not remove or alter this 
// notice or any other notice embedded in Materials by Intel or Intel�s 
// suppliers or licensors in any way.
//
// -----------------------------------------------------------------------------
// This file was automatically generated from AMT_BootSettingData.mof
// Copyright (C) Intel Corporation, 2006-2008
namespace MOF_CLASSES{
	using System.Xml.Serialization;


	/// <remarks>
	/// The AMT_BootSettingData class represents configuration-related and operational parameters for the boot service in the Intel� AMT.
	/// </remarks>
	[System.SerializableAttribute()]
	[System.Xml.Serialization.XmlTypeAttribute(Namespace="http://intel.com/wbem/wscim/1/amt-schema/1/AMT_BootSettingData")]
	[System.Xml.Serialization.XmlRootAttribute("AMT_BootSettingData", Namespace="http://intel.com/wbem/wscim/1/amt-schema/1/AMT_BootSettingData", IsNullable=false)]
	public partial class AMT_BootSettingDataType {

		/// <remarks>
		/// When True, the BIOS pauses for user input on the next boot cycle.
		/// </remarks>
		public bool BIOSPause;

		/// <remarks>
		/// When True, the Intel� AMT firmware enters the CMOS Setup screen on the next boot cycle.
		/// </remarks>
		public bool BIOSSetup;

		/// <remarks>
		/// This property identifies the boot-media index for the managed client (when a boot source is set using the CIM_BootConfigSetting.ChangeBootOrder method). For Hard-Drive or CD/DVD boot - when the parameter value is 0, the default boot-media is booted. When the parameter value is 1, the primary boot-media is booted; when the value is 2, the secondary boot-media is booted; and so on. For PXE or diagnostics boot this property must be 0.
		/// </remarks>
		public ushort BootMediaIndex;

		/// <remarks>
		/// The Caption property is a short textual description (one- line string) of the object.
		/// </remarks>
		public string Caption;

		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool CaptionSpecified;

		/// <remarks>
		/// When True, the Intel� AMT firmware resets its non-volatile configuration data to the managed system�s Setup defaults prior to booting the system.
		/// </remarks>
		public bool ConfigurationDataReset;

		/// <remarks>
		/// The Description property provides a textual description of the object.
		/// </remarks>
		public string Description;

		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool DescriptionSpecified;

		/// <remarks>
		/// The user-friendly name for this instance of SettingData. In addition, the user-friendly name can be used as an index property for a search or query. (Note: The name does not have to be unique within a namespace.)
		/// </remarks>
		public string ElementName;

		/// <remarks>
		/// When set to a non-zero value, controls the amount of information the managed system writes to its local display.
		/// </remarks>
		public byte FirmwareVerbosity;

		/// <remarks>
		/// When True, the Intel� AMT firmware transmits all progress PET events to the alert-sending device.
		/// </remarks>
		public bool ForcedProgressEvents;

		/// <remarks>
		/// Specifies the device to use when UseIder is set. 0 - Floppy Boot, 1- CD Boot.
		/// </remarks>
		public byte IDERBootDevice;

		/// <remarks>
		/// Within the scope of the instantiating Namespace, InstanceID opaquely and uniquely identifies an instance of this class. To ensure uniqueness within the NameSpace, the value of InstanceID should be constructed using the following "preferred" algorithm: 
		/// <OrgID>:<LocalID> 
		/// Where <OrgID> and <LocalID> are separated by a colon (:), and where <OrgID> must include a copyrighted, trademarked, or otherwise unique name that is owned by the business entity that is creating or defining the InstanceID or that is a registered ID assigned to the business entity by a recognized global authority. (This requirement is similar to the <Schema Name>_<Class Name> structure of Schema class names.) In addition, to ensure uniqueness, <OrgID> must not contain a colon (:). When using this algorithm, the first colon to appear in InstanceID must appear between <OrgID> and <LocalID>. 
		/// <LocalID> is chosen by the business entity and should not be reused to identify different underlying (real-world) elements. If the above "preferred" algorithm is not used, the defining entity must assure that the resulting InstanceID is not reused across any InstanceIDs produced by this or other providers for the NameSpace of this instance. 
		/// For DMTF-defined instances, the "preferred" algorithm must be used with the <OrgID> set to CIM.
		/// </remarks>
		[CimKey]
		public string InstanceID;

		/// <remarks>
		/// When True, the Intel� AMT firmware disallows keyboard activity during its boot process.
		/// </remarks>
		public bool LockKeyboard;

		/// <remarks>
		/// When True, the Intel� AMT firmware disables the power button operation for the system, normally until the next boot cycle.
		/// </remarks>
		public bool LockPowerButton;

		/// <remarks>
		/// When True, the Intel� AMT firmware disables the reset button operation for the system, normally until the next boot cycle.
		/// </remarks>
		public bool LockResetButton;

		/// <remarks>
		/// When True, the Intel� AMT firmware disables the sleep button operation for the system, normally until the next boot cycle.
		/// </remarks>
		public bool LockSleepButton;

		/// <remarks>
		/// OwningEntity identifies the vendor or organization that defines the contained boot settings.
		/// </remarks>
		public string OwningEntity;

		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool OwningEntitySpecified;

		/// <remarks>
		/// When True, the Intel� AMT firmware reflashes the BIOS on the next boot cycle.
		/// </remarks>
		public bool ReflashBIOS;

		/// <remarks>
		/// When True, IDER is used on the next boot cycle.
		/// </remarks>
		public bool UseIDER;

		/// <remarks>
		/// When True, Serial over LAN is used on the next boot cycle.
		/// </remarks>
		public bool UseSOL;

		/// <remarks>
		/// When a Hard-drive boot source is chosen (using CIM_BootConfigSetting) and this property is set to True, the Intel� AMT firmware will boot in safe mode.
		/// </remarks>
		public bool UseSafeMode;

		/// <remarks>
		/// When True, the Intel� AMT firmware boots the system and bypasses any user or boot password that might be set in the system.
		/// </remarks>
		public bool UserPasswordBypass;

	}
}

