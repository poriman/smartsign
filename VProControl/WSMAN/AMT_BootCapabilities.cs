// -----------------------------------------------------------------------------
//
// Copyright (c) 2009 Intel Corporation. All Rights Reserved.
//
// The source code contained or described herein and all documents related to 
// the source code ("Material") are owned by Intel Corporation or its suppliers 
// or licensors. Title to the Material remains with Intel Corporation or its 
// suppliers and licensors. The Material may contain trade secrets and 
// proprietary and confidential information of Intel Corporation and its 
// suppliers and licensors, and is protected by worldwide copyright and trade 
// secret laws and treaty provisions. No part of the Material may be used, 
// copied, reproduced, modified, published, uploaded, posted, transmitted, 
// distributed, or disclosed in any way without Intel�s prior express written 
// permission. 
//
// No license under any patent, copyright, trade secret or other intellectual 
// property right is granted to or conferred upon you by disclosure or delivery 
// of the Materials, either expressly, by implication, inducement, estoppel or 
// otherwise. Any license under such intellectual property rights must be 
// express and approved by Intel in writing.
//
// * Third Party trademarks are the property of their respective owners.
//
// Unless otherwise agreed by Intel in writing, you may not remove or alter this 
// notice or any other notice embedded in Materials by Intel or Intel�s 
// suppliers or licensors in any way.
//
// -----------------------------------------------------------------------------
// This file was automatically generated from AMT_BootCapabilities.mof
// Copyright (C) Intel Corporation, 2006-2008
namespace MOF_CLASSES{
	using System.Xml.Serialization;


	/// <remarks>
	/// Boot options that the Intel?AMT device supports.
	/// </remarks>
	[System.SerializableAttribute()]
	[System.Xml.Serialization.XmlTypeAttribute(Namespace="http://intel.com/wbem/wscim/1/amt-schema/1/AMT_BootCapabilities")]
	[System.Xml.Serialization.XmlRootAttribute("AMT_BootCapabilities", Namespace="http://intel.com/wbem/wscim/1/amt-schema/1/AMT_BootCapabilities", IsNullable=false)]
	public partial class AMT_BootCapabilitiesType {

		/// <remarks>
		/// Indicates whether Intel?AMT device supports 'BIOS Pause'
		/// </remarks>
		public bool BIOSPause;

		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool BIOSPauseSpecified;

		/// <remarks>
		/// Indicates whether Intel?AMT device supports 'BIOS Reflash'
		/// </remarks>
		public bool BIOSReflash;

		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool BIOSReflashSpecified;

		/// <remarks>
		/// Indicates whether Intel?AMT device supports 'BIOS Setup'
		/// </remarks>
		public bool BIOSSetup;

		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool BIOSSetupSpecified;

		/// <remarks>
		/// The Caption property is a short textual description (one- line string) of the object.
		/// </remarks>
		public string Caption;

		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool CaptionSpecified;

		/// <remarks>
		/// Indicates whether Intel?AMT device supports 'Configuration Data Reset'
		/// </remarks>
		public bool ConfigurationDataReset;

		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool ConfigurationDataResetSpecified;

		/// <remarks>
		/// The Description property provides a textual description of the object.
		/// </remarks>
		public string Description;

		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool DescriptionSpecified;

		/// <remarks>
		/// The user friendly name for this instance of Capabilities. In addition, the user friendly name can be used as a index property for a search of query. (Note: Name does not have to be unique within a namespace.)
		/// </remarks>
		public string ElementName;

		/// <remarks>
		/// Indicates whether Intel?AMT device supports 'Force CD or DVD Boot'
		/// </remarks>
		public bool ForceCDorDVDBoot;

		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool ForceCDorDVDBootSpecified;

		/// <remarks>
		/// Indicates whether Intel?AMT device supports 'Force Diagnostic Boot'
		/// </remarks>
		public bool ForceDiagnosticBoot;

		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool ForceDiagnosticBootSpecified;

		/// <remarks>
		/// Indicates whether Intel?AMT device supports 'Force Hard Drive Boot'
		/// </remarks>
		public bool ForceHardDriveBoot;

		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool ForceHardDriveBootSpecified;

		/// <remarks>
		/// Indicates whether Intel?AMT device supports 'Force Hard Drive Safe Mode Boot'
		/// </remarks>
		public bool ForceHardDriveSafeModeBoot;

		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool ForceHardDriveSafeModeBootSpecified;

		/// <remarks>
		/// Indicates whether Intel?AMT device supports 'Force PXE Boot'
		/// </remarks>
		public bool ForcePXEBoot;

		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool ForcePXEBootSpecified;

		/// <remarks>
		/// Indicates whether Intel?AMT device supports 'Forced Progress Events'
		/// </remarks>
		public bool ForcedProgressEvents;

		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool ForcedProgressEventsSpecified;

		/// <remarks>
		/// Indicates whether Intel?AMT device supports 'IDE Redirection'
		/// </remarks>
		public bool IDER;

		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool IDERSpecified;

		/// <remarks>
		/// Within the scope of the instantiating Namespace, InstanceID opaquely and uniquely identifies an instance of this class. In order to ensure uniqueness within the NameSpace, the value of InstanceID SHOULD be constructed using the following 'preferred' algorithm: 
		/// <OrgID>:<LocalID> 
		/// Where <OrgID> and <LocalID> are separated by a colon ':', and where <OrgID> MUST include a copyrighted, trademarked or otherwise unique name that is owned by the business entity creating/defining the InstanceID, or is a registered ID that is assigned to the business entity by a recognized global authority (This is similar to the <Schema Name>_<Class Name> structure of Schema class names.) In addition, to ensure uniqueness <OrgID> MUST NOT contain a colon (':'). When using this algorithm, the first colon to appear in InstanceID MUST appear between <OrgID> and <LocalID>. 
		/// <LocalID> is chosen by the business entity and SHOULD not be re-used to identify different underlying (real-world) elements. If the above 'preferred' algorithm is not used, the defining entity MUST assure that the resultant InstanceID is not re-used across any InstanceIDs produced by this or other providers for this instance's NameSpace. 
		/// For DMTF defined instances, the 'preferred' algorithm MUST be used with the <OrgID> set to 'CIM'.
		/// </remarks>
		[CimKey]
		public string InstanceID;

		/// <remarks>
		/// Indicates whether Intel?AMT device supports 'Keyboard Lock'
		/// </remarks>
		public bool KeyboardLock;

		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool KeyboardLockSpecified;

		/// <remarks>
		/// Indicates whether Intel?AMT device supports 'Power Button Lock'
		/// </remarks>
		public bool PowerButtonLock;

		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool PowerButtonLockSpecified;

		/// <remarks>
		/// Indicates whether Intel?AMT device supports 'Reset Button Lock'
		/// </remarks>
		public bool ResetButtonLock;

		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool ResetButtonLockSpecified;

		/// <remarks>
		/// Indicates whether Intel?AMT device supports 'Serial Over Lan'
		/// </remarks>
		public bool SOL;

		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool SOLSpecified;

		/// <remarks>
		/// Indicates whether Intel?AMT device supports 'Sleep Button Lock'
		/// </remarks>
		public bool SleepButtonLock;

		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool SleepButtonLockSpecified;

		/// <remarks>
		/// Indicates whether Intel?AMT device supports 'User Password Bypass'
		/// </remarks>
		public bool UserPasswordBypass;

		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool UserPasswordBypassSpecified;

		/// <remarks>
		/// Indicates whether Intel?AMT device supports 'Verbosity/Quiet'
		/// </remarks>
		public bool VerbosityQuiet;

		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool VerbosityQuietSpecified;

		/// <remarks>
		/// Indicates whether Intel?AMT device supports 'Verbosity Screen Blank'
		/// </remarks>
		public bool VerbosityScreenBlank;

		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool VerbosityScreenBlankSpecified;

		/// <remarks>
		/// Indicates whether Intel?AMT device supports 'Verbosity/Verbose'
		/// </remarks>
		public bool VerbosityVerbose;

		[System.Xml.Serialization.XmlIgnoreAttribute()]
		public bool VerbosityVerboseSpecified;

	}
}

