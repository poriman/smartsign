// -----------------------------------------------------------------------------
//
// Copyright (c) 2009 Intel Corporation. All Rights Reserved.
//
// The source code contained or described herein and all documents related to 
// the source code ("Material") are owned by Intel Corporation or its suppliers 
// or licensors. Title to the Material remains with Intel Corporation or its 
// suppliers and licensors. The Material may contain trade secrets and 
// proprietary and confidential information of Intel Corporation and its 
// suppliers and licensors, and is protected by worldwide copyright and trade 
// secret laws and treaty provisions. No part of the Material may be used, 
// copied, reproduced, modified, published, uploaded, posted, transmitted, 
// distributed, or disclosed in any way without Intel’s prior express written 
// permission. 
//
// No license under any patent, copyright, trade secret or other intellectual 
// property right is granted to or conferred upon you by disclosure or delivery 
// of the Materials, either expressly, by implication, inducement, estoppel or 
// otherwise. Any license under such intellectual property rights must be 
// express and approved by Intel in writing.
//
// * Third Party trademarks are the property of their respective owners.
//
// Unless otherwise agreed by Intel in writing, you may not remove or alter this 
// notice or any other notice embedded in Materials by Intel or Intel’s 
// suppliers or licensors in any way.
//
// -----------------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.Net.Security;
using System.IO;
using System.Diagnostics;
using System.Collections.Specialized;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Xml.Serialization;

namespace Intel.vPro.AMT
{
    public class AmtCredentials
    {
        #region Private Fields
        private string _UserName;
        private string _Password;
        private bool _UseTls;
        private string _CertName;
        private string _CertFileName;
        private bool _MutualCertFromFile = false;
        private bool _IgnoreInvalidCert;
        private ProxyInfo _Proxy;
        private bool _UseProxy;
        private X509Certificate2 _MutualCertificate = null;
        #endregion

        #region Public Properties
        /// <summary>
        /// gets or sets the username to use for connection to Intel(R) AMT
        /// </summary>
        public string UserName
        {
            get { return _UserName; }
            set { _UserName = value; }
        }

        /// <summary>
        /// gets or sets the pass3word to use for connection to Intel(R) AMT
        /// </summary>
        public string Password
        {
            get { return _Password; }
            set { _Password = value; }
        }

        /// <summary>
        /// gets or sets a flag to indicate whether TLS is used
        /// </summary>
        public bool UseTls
        {
            get { return _UseTls; }
            set { _UseTls = value; }
        }

        /// <summary>
        /// Gets a flag to indicate whether Mutual TLS is used
        /// </summary>
        [XmlIgnore]
        public bool UseMutualTls
        {
            get
            {
                if (UseTls == false)
                    return false;

                if (MutualCertificate == null)
                    return false;
                else
                    return true;
            }
        }

        /// <summary>
        /// gets or sets the public certificate name
        /// </summary>
        public string CertName
        {
            get { return _CertName; }
            set
            {
                if ((value == null) || (value.Length == 0))
                {
                    if (_MutualCertFromFile == false)
                        MutualCertificate = null;
                    _CertName = null;
                    return;
                }

                //
                // Search the CurrentUser store for matching certificates.
                //
                X509Store certStore = new X509Store(StoreLocation.CurrentUser);
                certStore.Open(OpenFlags.ReadOnly | OpenFlags.OpenExistingOnly);
                foreach (X509Certificate2 cert in certStore.Certificates)
                {
                    StringCollection commonNames = new StringCollection();
                    string certSubject = cert.Subject;
                    int cnIndex = certSubject.IndexOf("CN=");
                    while (cnIndex >= 0)
                    {
                        certSubject = certSubject.Substring(cnIndex + 3);
                        cnIndex = certSubject.IndexOf(",");
                        if (cnIndex > 0)
                            commonNames.Add(certSubject.Substring(0, cnIndex));
                        else
                            commonNames.Add(certSubject);
                        cnIndex = certSubject.IndexOf("CN=");
                    }

                    foreach (string commonName in commonNames)
                    {
                        if (String.Compare(value, commonName, true) == 0)
                        {
                            _CertName = value;
                            MutualCertificate = cert;
                            _MutualCertFromFile = false;
                            return;
                        }
                    } // END foreach (string commonName in CommonNames)
                } // END foreach (X509Certificate2 cert in certStore.Certificates)

                //
                // Search the LocalMachine store for matching certificates.
                //
                certStore = new X509Store(StoreLocation.LocalMachine);
                certStore.Open(OpenFlags.ReadOnly | OpenFlags.OpenExistingOnly);
                foreach (X509Certificate2 cert in certStore.Certificates)
                {
                    StringCollection commonNames = new StringCollection();
                    string certSubject = cert.Subject;
                    int cnIndex = certSubject.IndexOf("CN=");
                    while (cnIndex >= 0)
                    {
                        certSubject = certSubject.Substring(cnIndex + 3);
                        cnIndex = certSubject.IndexOf(",");
                        if (cnIndex > 0)
                            commonNames.Add(certSubject.Substring(0, cnIndex));
                        else
                            commonNames.Add(certSubject);
                        cnIndex = certSubject.IndexOf("CN=");
                    }

                    foreach (string commonName in commonNames)
                    {
                        if (String.Compare(value, commonName, true) == 0)
                        {
                            _CertName = value;
                            MutualCertificate = cert;
                            _MutualCertFromFile = false;
                            return;
                        }
                    } // END foreach (string commonName in CommonNames)
                } // END foreach (X509Certificate2 cert in certStore.Certificates)

                throw new CryptographicException("Could not find " + value + " in the User or Local Machine certificate store");
            }
        }

        /// <summary>
        /// gets or sets the local file name of the certificate file
        /// </summary>
        public string CertFileName
        {
            get { return _CertFileName; }
            set
            {
                try
                {
                    _CertFileName = value;
                    if ((value == null) || (value.Length == 0))
                    {
                        if (_MutualCertFromFile == true)
                            MutualCertificate = null;
                        return;
                    }

                    MutualCertificate = new X509Certificate2(_CertFileName);
                    _MutualCertFromFile = true;
                }
                catch (CryptographicException e)
                {
                    throw new CryptographicException("Error creating certificate object. Is the certificate file correct?", e);
                }
            }
        }

        /// <summary>
        /// gets or sets a flag to indicate whether to ignore invalid server certificates.
        /// </summary>
        public bool IgnoreInvalidCert
        {
            get { return _IgnoreInvalidCert; }
            set
            {
                if(value == true)
                    ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(IgnoreCertificateErrorHandler);
                else
                    ServicePointManager.ServerCertificateValidationCallback = null;
                _IgnoreInvalidCert = value;
            }
        }

        /// <summary>
        /// gets or sets Proxy information
        /// </summary>
        public ProxyInfo Proxy
        {
            get { return _Proxy; }
            set { _Proxy = value; }
        }

        /// <summary>
        /// gets or sets a flag to indicate whether a proxy is used.
        /// </summary>
        public bool UseProxy
        {
            get { return _UseProxy; }
            set { _UseProxy = value; }
        }

        /// <summary>
        /// Certificate to use for mutual authentication.
        /// </summary>
        [XmlIgnore]
        public X509Certificate2 MutualCertificate
        {
            get { return _MutualCertificate; }
            set { _MutualCertificate = value; }
        }

        #endregion

        #region Constructors
        /// <summary>
        /// Creates an unitialized AmtCredentials object
        /// </summary>
        public AmtCredentials()
        {
        }

        /// <summary>
        /// Creates a credentials object using username, password and TLS flag
        /// </summary>
        /// <param name="userName">the username to use for connecting to the Intel(R) AMT system</param>
        /// <param name="password">the password to use for connecting to the Intel(R) AMT system</param>
        /// <param name="useTls">true=Use TLS, false otherwise</param>
        public AmtCredentials(string userName, string password, bool useTls)
        {
            UserName = userName;
            Password = password;
            UseTls = useTls;
        }

        /// <summary>
        /// Creates a credentials object using username, password, TLS and certificate information.
        /// </summary>
        /// <param name="userName">the username to use for connecting to the Intel(R) AMT system</param>
        /// <param name="password">the password to use for connecting to the Intel(R) AMT system</param>
        /// <param name="useTls">true=Use TLS, false otherwise</param>
        /// <param name="certName">Not yet supported</param>
        /// <param name="certFile">Not yet supported</param>
        /// <param name="useMPS">Not yet supported</param>
        public AmtCredentials(string userName, string password, bool useTls, string certName,
            string certFile, bool useMPS)
        {
            UserName = userName;
            Password = password;
            UseTls = useTls;
            CertName = certName;
            CertFileName = certFile;
        }
        #endregion

        /// <summary>
        /// Simply "approves" the remote Secure Sockets Layer (SSL) certificate 
        /// used for authentication by always returning true.
        /// </summary>
        /// <param name="sender">An object that contains state information for this validation.</param>
        /// <param name="certificate">The certificate used to authenticate the remote party.</param>
        /// <param name="chain">The chain of certificate authorities associated with the remote certificate.</param>
        /// <param name="sslPolicyErrors">One or more errors associated with the remote certificate.</param>
        /// <returns>Always returns true to allow the use of the certificate.</returns>
        private static bool IgnoreCertificateErrorHandler(Object sender,
            X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

        #region Public Methods
        /// <summary>
        /// Creates and configures an Intel(R) AMT RemoteControlService object.
        /// </summary>
        /// <param name="amtHostname">The IP address or FQDN of the target Intel(R) AMT system.</param>
        /// <param name="useMPS">Flag to indicate the use of a MPS (proxy server).</param>
        /// <param name="useProxy">Flag to indicate the use of the host OS proxy settings.</param>
        /// <returns>A valid and configured Intel(R) AMT RemoteControlService object.</returns>
        public RemoteControlService SetupRemoteControlService(string amtHostname, bool useMPS, bool useProxy)
        {
            const int DEFAULT_TIMEOUT = 5000;

            Debug.Assert(!String.IsNullOrEmpty(amtHostname));

            //
            // Create and initialize the Remote Control Service interface.
            //
            RemoteControlService rcs = new RemoteControlService();
            rcs.Url = String.Format("http://{0}:16992/RemoteControlService", amtHostname);
            rcs.PreAuthenticate = true;
            rcs.ConnectionGroupName = amtHostname;

            // Define the URL of the AMT device's Remote Control service.
            if (UseTls)
                rcs.Url = String.Format("https://{0}:16993/RemoteControlService", amtHostname);
            else
                rcs.Url = String.Format("http://{0}:16992/RemoteControlService", amtHostname);

            //
            // Define the credentials to use when connecting to the Intel(R) AMT device.
            //
            if (String.IsNullOrEmpty(UserName) && String.IsNullOrEmpty(Password))
            {
                // Attempt to connect and login using Kerberos with the local credentials.
                //
                // Create a new instance of CredentialCache.
                CredentialCache credentialCache = new CredentialCache();

                // Add the NetworkCredential to the CredentialCache.
                credentialCache.Add(new Uri(rcs.Url), "Negotiate", CredentialCache.DefaultNetworkCredentials);

                // Add the CredentialCache to the proxy class credentials.
                rcs.Credentials = credentialCache;

                // Setup the authentication manager.
                if (AuthenticationManager.CustomTargetNameDictionary.ContainsKey(rcs.Url) == false)
                {
                    string SPN = "HTTP/" + amtHostname + ":16993";
                    if (rcs.Url.StartsWith("https", StringComparison.CurrentCultureIgnoreCase) == true)
                        SPN = "HTTP/" + amtHostname + ":16992";
                    AuthenticationManager.CustomTargetNameDictionary.Add(rcs.Url, SPN);
                }
            }
            else if (UserName.Contains("\\\\"))
            {
                // Attempt to connect and login using Kerberos.
                int splitIndex = UserName.IndexOf('\\');
                string domain = UserName.Substring(0, splitIndex);
                UserName = UserName.Substring(splitIndex + 1);

                // Create a new instance of CredentialCache.
                CredentialCache credentialCache = new CredentialCache();

                // Create a new instance of NetworkCredential using the client
                // credentials.
                NetworkCredential credentials = new NetworkCredential(UserName, Password, domain);

                // Add the NetworkCredential to the CredentialCache.
                credentialCache.Add(new Uri(rcs.Url), "Negotiate", credentials);

                // Add the CredentialCache to the proxy class credentials.
                rcs.Credentials = credentialCache;

                // Setup the authentication manager.
                if (AuthenticationManager.CustomTargetNameDictionary.ContainsKey(rcs.Url) == false)
                {
                    string SPN = "HTTP/" + amtHostname + ":16993";
                    if (rcs.Url.StartsWith("https", StringComparison.CurrentCultureIgnoreCase) == true)
                        SPN = "HTTP/" + amtHostname + ":16992";
                    AuthenticationManager.CustomTargetNameDictionary.Add(rcs.Url, SPN);
                }
            }
            else
            {
                // Attempt to connect and login using Digest Authentication.
                // Create a new instance of CredentialCache.
                CredentialCache credentialCache = new CredentialCache();

                // Create a new instance of NetworkCredential using the client
                // credentials.
                NetworkCredential credentials = new NetworkCredential(UserName, Password);

                // Add the NetworkCredential to the CredentialCache.
                credentialCache.Add(new Uri(rcs.Url), "Digest", credentials);

                // Add the CredentialCache to the proxy class credentials.
                rcs.Credentials = credentialCache;
            }

            rcs.Timeout = DEFAULT_TIMEOUT;

            if (!useProxy)
                rcs.Proxy = null;
            else if (useMPS)
                rcs.Proxy = WebRequest.DefaultWebProxy;

            if (UseTls)
            {
                //
                // Retrieve the certificates for TLS communications.
                //
                try
                {
                    if (!String.IsNullOrEmpty(CertFileName))
                    {
                        rcs.ClientCertificates.Add(X509Certificate2.CreateFromCertFile(CertFileName));
                    }
                    else if (!String.IsNullOrEmpty(CertName))
                    {
                        //
                        // Search the CurrentUser store for matching certificates.
                        //
                        X509Store certStore = new X509Store(StoreLocation.CurrentUser);
                        certStore.Open(OpenFlags.ReadOnly | OpenFlags.OpenExistingOnly);
                        foreach (X509Certificate2 cert in certStore.Certificates)
                        {
                            StringCollection commonNames = new StringCollection();
                            string certSubject = cert.Subject;
                            int cnIndex = certSubject.IndexOf("CN=");
                            while (cnIndex >= 0)
                            {
                                certSubject = certSubject.Substring(cnIndex + 3);
                                cnIndex = certSubject.IndexOf(",");
                                if (cnIndex > 0)
                                    commonNames.Add(certSubject.Substring(0, cnIndex));
                                else
                                    commonNames.Add(certSubject);
                                cnIndex = certSubject.IndexOf("CN=");
                            }

                            foreach (string commonName in commonNames)
                            {
                                if (String.Compare(CertName, commonName, true) == 0)
                                {
                                    rcs.ClientCertificates.Add(cert);
                                    break;
                                }
                            } // END foreach (string commonName in CommonNames)
                        } // END foreach (X509Certificate2 cert in certStore.Certificates)

                        //
                        // Search the LocalMachine store for matching certificates.
                        //
                        certStore = new X509Store(StoreLocation.LocalMachine);
                        certStore.Open(OpenFlags.ReadOnly | OpenFlags.OpenExistingOnly);
                        foreach (X509Certificate2 cert in certStore.Certificates)
                        {
                            StringCollection commonNames = new StringCollection();
                            string certSubject = cert.Subject;
                            int cnIndex = certSubject.IndexOf("CN=");
                            while (cnIndex >= 0)
                            {
                                certSubject = certSubject.Substring(cnIndex + 3);
                                cnIndex = certSubject.IndexOf(",");
                                if (cnIndex > 0)
                                    commonNames.Add(certSubject.Substring(0, cnIndex));
                                else
                                    commonNames.Add(certSubject);
                                cnIndex = certSubject.IndexOf("CN=");
                            }

                            foreach (string commonName in commonNames)
                            {
                                if (String.Compare(CertName, commonName, true) == 0)
                                {
                                    rcs.ClientCertificates.Add(cert);
                                    break;
                                }
                            } // END foreach (string commonName in CommonNames)
                        } // END foreach (X509Certificate2 cert in certStore.Certificates)
                    }
                }
                catch (Exception e)
                {
                    Debug.Print("Exception: {0} (type {1})", e.Message, e.GetType());
                    Console.WriteLine("An exception occurred while retrieving the certificate information. {0}", e.Message);
                }
            } // END if (useTls)

            return rcs;
        }



        /// <summary>
        /// Creates and configures an Intel(R) AMT RedirectionService object.
        /// </summary>
        /// <param name="amtHostname">The IP address or FQDN of the target Intel(R) AMT system.</param>
        /// <param name="useMPS">Flag to indicate the use of a MPS (proxy server).</param>
        /// <param name="useProxy">Flag to indicate the use of the host OS proxy settings.</param>
        /// <returns>A valid and configured Intel(R) AMT RemoteControlService object.</returns>
        public RedirectionService SetupRedirectionServiceService(string amtHostname, bool useMPS, bool useProxy)
        {
            const int DEFAULT_TIMEOUT = 5000;

            Debug.Assert(!String.IsNullOrEmpty(amtHostname));

            //
            // Create and initialize the Remote Control Service interface.
            //
            RedirectionService rcs = new RedirectionService();
            rcs.Url = String.Format("http://{0}:16992/RedirectionService", amtHostname);
            rcs.PreAuthenticate = true;
            rcs.ConnectionGroupName = amtHostname;

            // Define the URL of the AMT device's Remote Control service.
            if (UseTls)
                rcs.Url = String.Format("https://{0}:16993/RedirectionService", amtHostname);
            else
                rcs.Url = String.Format("http://{0}:16992/RedirectionService", amtHostname);

            //
            // Define the credentials to use when connecting to the Intel(R) AMT device.
            //
            if (String.IsNullOrEmpty(UserName) && String.IsNullOrEmpty(Password))
            {
                // Attempt to connect and login using Kerberos with the local credentials.
                //
                // Create a new instance of CredentialCache.
                CredentialCache credentialCache = new CredentialCache();

                // Add the NetworkCredential to the CredentialCache.
                credentialCache.Add(new Uri(rcs.Url), "Negotiate", CredentialCache.DefaultNetworkCredentials);

                // Add the CredentialCache to the proxy class credentials.
                rcs.Credentials = credentialCache;

                // Setup the authentication manager.
                if (AuthenticationManager.CustomTargetNameDictionary.ContainsKey(rcs.Url) == false)
                {
                    string SPN = "HTTP/" + amtHostname + ":16993";
                    if (rcs.Url.StartsWith("https", StringComparison.CurrentCultureIgnoreCase) == true)
                        SPN = "HTTP/" + amtHostname + ":16992";
                    AuthenticationManager.CustomTargetNameDictionary.Add(rcs.Url, SPN);
                }
            }
            else if (UserName.Contains("\\\\"))
            {
                // Attempt to connect and login using Kerberos.
                int splitIndex = UserName.IndexOf('\\');
                string domain = UserName.Substring(0, splitIndex);
                UserName = UserName.Substring(splitIndex + 1);

                // Create a new instance of CredentialCache.
                CredentialCache credentialCache = new CredentialCache();

                // Create a new instance of NetworkCredential using the client
                // credentials.
                NetworkCredential credentials = new NetworkCredential(UserName, Password, domain);

                // Add the NetworkCredential to the CredentialCache.
                credentialCache.Add(new Uri(rcs.Url), "Negotiate", credentials);

                // Add the CredentialCache to the proxy class credentials.
                rcs.Credentials = credentialCache;

                // Setup the authentication manager.
                if (AuthenticationManager.CustomTargetNameDictionary.ContainsKey(rcs.Url) == false)
                {
                    string SPN = "HTTP/" + amtHostname + ":16993";
                    if (rcs.Url.StartsWith("https", StringComparison.CurrentCultureIgnoreCase) == true)
                        SPN = "HTTP/" + amtHostname + ":16992";
                    AuthenticationManager.CustomTargetNameDictionary.Add(rcs.Url, SPN);
                }
            }
            else
            {
                // Attempt to connect and login using Digest Authentication.
                // Create a new instance of CredentialCache.
                CredentialCache credentialCache = new CredentialCache();

                // Create a new instance of NetworkCredential using the client
                // credentials.
                NetworkCredential credentials = new NetworkCredential(UserName, Password);

                // Add the NetworkCredential to the CredentialCache.
                credentialCache.Add(new Uri(rcs.Url), "Digest", credentials);

                // Add the CredentialCache to the proxy class credentials.
                rcs.Credentials = credentialCache;
            }

            rcs.Timeout = DEFAULT_TIMEOUT;

            if (!useProxy)
                rcs.Proxy = null;
            else if (useMPS)
                rcs.Proxy = WebRequest.DefaultWebProxy;

            if (UseTls)
            {
                //
                // Retrieve the certificates for TLS communications.
                //
                try
                {
                    if (!String.IsNullOrEmpty(CertFileName))
                    {
                        rcs.ClientCertificates.Add(X509Certificate2.CreateFromCertFile(CertFileName));
                    }
                    else if (!String.IsNullOrEmpty(CertName))
                    {
                        //
                        // Search the CurrentUser store for matching certificates.
                        //
                        X509Store certStore = new X509Store(StoreLocation.CurrentUser);
                        certStore.Open(OpenFlags.ReadOnly | OpenFlags.OpenExistingOnly);
                        foreach (X509Certificate2 cert in certStore.Certificates)
                        {
                            StringCollection commonNames = new StringCollection();
                            string certSubject = cert.Subject;
                            int cnIndex = certSubject.IndexOf("CN=");
                            while (cnIndex >= 0)
                            {
                                certSubject = certSubject.Substring(cnIndex + 3);
                                cnIndex = certSubject.IndexOf(",");
                                if (cnIndex > 0)
                                    commonNames.Add(certSubject.Substring(0, cnIndex));
                                else
                                    commonNames.Add(certSubject);
                                cnIndex = certSubject.IndexOf("CN=");
                            }

                            foreach (string commonName in commonNames)
                            {
                                if (String.Compare(CertName, commonName, true) == 0)
                                {
                                    rcs.ClientCertificates.Add(cert);
                                    break;
                                }
                            } // END foreach (string commonName in CommonNames)
                        } // END foreach (X509Certificate2 cert in certStore.Certificates)

                        //
                        // Search the LocalMachine store for matching certificates.
                        //
                        certStore = new X509Store(StoreLocation.LocalMachine);
                        certStore.Open(OpenFlags.ReadOnly | OpenFlags.OpenExistingOnly);
                        foreach (X509Certificate2 cert in certStore.Certificates)
                        {
                            StringCollection commonNames = new StringCollection();
                            string certSubject = cert.Subject;
                            int cnIndex = certSubject.IndexOf("CN=");
                            while (cnIndex >= 0)
                            {
                                certSubject = certSubject.Substring(cnIndex + 3);
                                cnIndex = certSubject.IndexOf(",");
                                if (cnIndex > 0)
                                    commonNames.Add(certSubject.Substring(0, cnIndex));
                                else
                                    commonNames.Add(certSubject);
                                cnIndex = certSubject.IndexOf("CN=");
                            }

                            foreach (string commonName in commonNames)
                            {
                                if (String.Compare(CertName, commonName, true) == 0)
                                {
                                    rcs.ClientCertificates.Add(cert);
                                    break;
                                }
                            } // END foreach (string commonName in CommonNames)
                        } // END foreach (X509Certificate2 cert in certStore.Certificates)
                    }
                }
                catch (Exception e)
                {
                    Debug.Print("Exception: {0} (type {1})", e.Message, e.GetType());
                    Console.WriteLine("An exception occurred while retrieving the certificate information. {0}", e.Message);
                }
            } // END if (useTls)

            return rcs;
        }

        #endregion

        #region Internal methods
        /// <summary>
        /// Creates connection paramaters structure used for connections based on SOCKSv5 proxy settings to MPS
        /// </summary>
        /// <returns>byte array representing the connection parameters</returns>
        internal byte[] CreateTCPSessionParamsEx()
        {
            // typedef struct {
            //   int version;                   // must be 2
            //   char user_name[MAX_NAME_LEN];  // MAX_NAME_LEN = 128
            //   char user_pswd[MAX_PSWD_LEN];  // MAZ_PSWD_LEN = 128
            //   ProxySettings proxy_settings
            // } TCPSessionParamsEx;
            // 
            // typedef struct {
            //   PROXY_TYPE type;               // enum PROXY_NO_PROXY, PROXY_SOCKS_V5
            //   char server[MAX_IP_LEN];       // MAX_IP_LEN = 128
            //   int port;
            //   char user[MAX_NAME_LEN];       // MAX_NAME_LEN = 128
            //   char password[MAX_PSWD_LEN];   // MAZ_PSWD_LEN = 128
            // } ProxyType;
            const int version = 2;
            const int MAX_NAME_LEN = 128;
            const int MAX_PSWD_LEN = 128;
            const int MAX_IP_LEN = 128;
            byte[] buffer = null;

            MemoryStream MS = new MemoryStream(4 + MAX_NAME_LEN + MAX_PSWD_LEN + 4 + MAX_IP_LEN + 4 + MAX_NAME_LEN + MAX_PSWD_LEN);

            // TCPSessionParamsEx
            MS.Write(BitConverter.GetBytes(version), 0, 4);

            buffer = new byte[MAX_NAME_LEN];
            UTF8Encoding.UTF8.GetBytes(UserName, 0, Math.Min(UserName.Length, MAX_NAME_LEN), buffer, 0);
            MS.Write(buffer, 0, buffer.Length);

            buffer = new byte[MAX_PSWD_LEN];
            UTF8Encoding.UTF8.GetBytes(Password, 0, Math.Min(Password.Length, MAX_NAME_LEN), buffer, 0);
            MS.Write(buffer, 0, buffer.Length);

            // ProxyType
            if (UseProxy == false)
            {
                MS.Write(BitConverter.GetBytes((uint)PROXY_TYPE.PROXY_NO_PROXY), 0, 4);

                // Fill the remaining fields of the ProxyType struct with zeroes
                buffer = new byte[4 + MAX_NAME_LEN + MAX_PSWD_LEN];
                MS.Write(buffer, 0, buffer.Length);
            }
            else
            {
                MS.Write(BitConverter.GetBytes((uint)PROXY_TYPE.PROXY_SOCKS_V5), 0, 4);

                buffer = new byte[MAX_IP_LEN];
                UTF8Encoding.UTF8.GetBytes(Proxy.Server, 0, Math.Min(Proxy.Server.Length, MAX_IP_LEN), buffer, 0);
                MS.Write(buffer, 0, buffer.Length);

                MS.Write(BitConverter.GetBytes((uint)Proxy.Port), 0, 4);

                if (Proxy.User != null)
                {
                    buffer = new byte[MAX_NAME_LEN];
                    UTF8Encoding.UTF8.GetBytes(Proxy.User, 0, Math.Min(Proxy.User.Length, MAX_NAME_LEN), buffer, 0);
                    MS.Write(buffer, 0, buffer.Length);

                    buffer = new byte[MAX_PSWD_LEN];
                    UTF8Encoding.UTF8.GetBytes(Proxy.Password, 0, Math.Min(Proxy.Password.Length, MAX_NAME_LEN), buffer, 0);
                    MS.Write(buffer, 0, buffer.Length);
                }
                else
                {
                    // Fill the proxy name and password fields with zeroes
                    buffer = new byte[MAX_NAME_LEN + MAX_PSWD_LEN];
                    MS.Write(buffer, 0, buffer.Length);
                }
            }

            return MS.ToArray();
        }
        #endregion
    }
}
