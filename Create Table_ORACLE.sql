CREATE TABLE geo_master
(
	geocode INTEGER PRIMARY KEY,
	x FLOAT,
	y FLOAT,
	postcode VARCHAR(9),
	address VARCHAR(256),
	ref_code VARCHAR(10)
);

CREATE TABLE client_logs (pid CHAR(13), name VARCHAR2(1024), description VARCHAR2(255), logcd INTEGER, error_code INTEGER, start_dt NUMBER(19, 0), end_dt NUMBER(19, 0), uuid CHAR(36));


CREATE TABLE groups (path VARCHAR(64), username VARCHAR(32), password VARCHAR(32), gid CHAR(13) PRIMARY KEY, pid CHAR(13), name VARCHAR(32), source VARCHAR(64), parent_gid CHAR(13), depth INTEGER, is_inherited CHAR(1) DEFAULT 'N', del_yn CHAR(1) DEFAULT 'N', geocode INTEGER);

ALTER TABLE groups
	ADD CONSTRAINT parent_gid_fkey
	FOREIGN KEY(parent_gid)
	REFERENCES groups(gid);

ALTER TABLE groups
	ADD CONSTRAINT g_ref_code_fkey
	FOREIGN KEY(geocode)
	REFERENCES geo_master(geocode);

CREATE TABLE players (pid CHAR(13) PRIMARY KEY, gid CHAR(13), name VARCHAR2(1024), host VARCHAR(128), state INTEGER, lastconndt NUMBER(19,0), versionH INTEGER, versionL INTEGER, del_yn CHAR(1) DEFAULT 'N', tz_info VARCHAR(6), geocode INTEGER);

ALTER TABLE players
	ADD CONSTRAINT players_fkey
	FOREIGN KEY(gid)
	REFERENCES groups(gid);

ALTER TABLE players
	ADD CONSTRAINT p_ref_code_fkey
	FOREIGN KEY(geocode)
	REFERENCES geo_master(geocode);

CREATE TABLE player_detailinfo ( 
	pid      	CHAR(13) UNIQUE,
	"_ipv4addr"	VARCHAR(16) NULL,
	"_is_dhcp" 	VARCHAR(2) NULL,
	"_macaddr" 	VARCHAR(17) NULL,
	"_mnttype" 	INTEGER NULL,
	"_auth_id" 	VARCHAR(50) NULL,
	"_auth_pass"	VARCHAR(50) NULL
	);

ALTER TABLE player_detailinfo
	ADD CONSTRAINT foreign_pid
	FOREIGN KEY(pid)
	REFERENCES players(pid);

CREATE TABLE downloadtime (d_id NUMBER(19,0) PRIMARY KEY, starttime CHAR(8), endtime CHAR(8));
CREATE TABLE dt_players_asn (d_id NUMBER(19,0), pid CHAR(13), FOREIGN KEY(pid) REFERENCES players(pid), FOREIGN KEY(d_id) REFERENCES downloadtime(d_id), PRIMARY KEY (d_id, pid));
CREATE TABLE dt_groups_asn (d_id NUMBER(19,0), gid CHAR(13), FOREIGN KEY(gid) REFERENCES groups(gid), FOREIGN KEY(d_id) REFERENCES downloadtime(d_id), PRIMARY KEY (d_id, gid));

CREATE TABLE relaylogs
(
	relay_id NUMBER(19,0) PRIMARY KEY,
	created_ts NUMBER(19,0) NOT NULL,
	pid CHAR(13),
	client_up_ts NUMBER(19,0),
	app_srv_up_ts NUMBER(19,0),
	relay_srv_up_ts NUMBER(19,0),
	relay_srv_down_ts NUMBER(19,0),
	app_srv_down_ts NUMBER(19,0),
	client_down_ts NUMBER(19,0)
);

ALTER TABLE relaylogs
	ADD CONSTRAINT foreign_r_pid
	FOREIGN KEY(pid)
	REFERENCES players(pid);

CREATE SEQUENCE relaylogs_error_SEQUENCE
START WITH 1;

CREATE TABLE relaylogs_error
(
	id INTEGER PRIMARY KEY,
	relay_id NUMBER(19,0),
	err_tp INTEGER,
	err_cd INTEGER,
	err_ts NUMBER(19,0)
);

CREATE OR REPLACE TRIGGER relaylogs_error_trigger
BEFORE INSERT
ON relaylogs_error
REFERENCING NEW AS NEW
FOR EACH ROW
BEGIN
SELECT relaylogs_error_SEQUENCE.nextval INTO :NEW.id FROM dual;
END;

ALTER TABLE relaylogs_error
	ADD CONSTRAINT foreign_r_e_relay_id
	FOREIGN KEY(relay_id)
	REFERENCES relaylogs(relay_id);

CREATE TABLE relaylogs_error_defines
(
	err_tp INTEGER NOT NULL,
	err_cd INTEGER NOT NULL,
	err_message VARCHAR2(1024)
);

ALTER TABLE relaylogs_error_defines
	ADD PRIMARY KEY(err_tp, err_cd);

CREATE TABLE relaylogs_detail
(
	relay_id NUMBER(19,0),
	r_key VARCHAR(32),
	r_value VARCHAR(32)
);

ALTER TABLE relaylogs_detail
	ADD CONSTRAINT foreign_r_id
	FOREIGN KEY(relay_id)
	REFERENCES relaylogs(relay_id);


CREATE TABLE tasks (priority INTEGER, endtimeofday INTEGER, starttimeofday INTEGER, daysofweek INTEGER, duration INTEGER, pid CHAR(13), gid CHAR(13), starttime NUMBER(19, 0), endtime NUMBER(19, 0), etime NUMBER(19, 0), type INTEGER, state INTEGER, uuid CHAR(36) PRIMARY KEY, guuid CHAR(36), duuid CHAR(36), name VARCHAR2(1024), is_ad CHAR(1), meta_tag VARCHAR2(1024), show_flag INTEGER, tz_info VARCHAR(6));

ALTER TABLE tasks
	ADD CONSTRAINT task_g_fkey
	FOREIGN KEY(gid)
	REFERENCES groups(gid);
ALTER TABLE tasks
	ADD CONSTRAINT task_p_fkey
	FOREIGN KEY(pid)
	REFERENCES players(pid);

CREATE TABLE screens (s_id NUMBER(19, 0) PRIMARY KEY, s_starttime NUMBER(19, 0), s_endtime NUMBER(19, 0), s_width INTEGER, s_height INTEGER, s_playtime INTEGER, s_name VARCHAR(256), s_description VARCHAR(256), s_screen_type INTEGER, s_display_type INTEGER, s_is_ad CHAR(1), s_meta_tag VARCHAR2(1024), s_delete_yn CHAR(1) DEFAULT 'N', s_regdt NUMBER(19, 0));

CREATE TABLE screentask_asn (s_id NUMBER(19, 0), uuid CHAR(36), s_index INTEGER, thumb_url VARCHAR(256));

ALTER TABLE screentask_asn
	ADD CONSTRAINT s_id
	FOREIGN KEY(s_id)
	REFERENCES screens(s_id);

ALTER TABLE screentask_asn
	ADD CONSTRAINT s_uuid
	FOREIGN KEY(uuid)
	REFERENCES tasks(uuid);

CREATE SEQUENCE promotions_SEQUENCE
START WITH 1;

CREATE TABLE promotions
(
	id			INTEGER PRIMARY KEY,
	screen_id	NUMBER(19, 0) NOT NULL,
	receiver	VARCHAR(20) NOT NULL,
	reserved	VARCHAR(100),
	regdt		NUMBER(19, 0) NOT NULL,
	proceeddt	NUMBER(19, 0)
);

CREATE OR REPLACE TRIGGER promotions_trigger
BEFORE INSERT
ON promotions
REFERENCING NEW AS NEW
FOR EACH ROW
BEGIN
SELECT promotions_SEQUENCE.nextval INTO :NEW.id FROM dual;
END;

ALTER TABLE promotions
	ADD CONSTRAINT p_sid
	FOREIGN KEY(screen_id)
	REFERENCES screens(s_id);


CREATE TABLE playlogs
(
    pid CHAR(13), 
    logcd INTEGER,
    uuid CHAR(36),
    tasknm VARCHAR2(1024),
    screennm VARCHAR2(1024),
	screen_id NUMBER(19, 0),
    errorcd INTEGER,
    start_dt NUMBER(19, 0),
	end_dt NUMBER(19, 0),
	relay_id NUMBER(19, 0)
);

ALTER TABLE playlogs
	ADD CONSTRAINT t_log_fkey
	FOREIGN KEY(uuid)
	REFERENCES tasks(uuid);

ALTER TABLE playlogs
	ADD CONSTRAINT p_log_fkey
	FOREIGN KEY(pid)
	REFERENCES players(pid);

ALTER TABLE playlogs
	ADD CONSTRAINT s_log_fkey
	FOREIGN KEY(screen_id)
	REFERENCES screens(s_id);

ALTER TABLE playlogs
	ADD CONSTRAINT p_log_r_fkey
	FOREIGN KEY(relay_id)
	REFERENCES relaylogs(relay_id);

CREATE INDEX idx_playlog ON playlogs (start_dt DESC, logcd, pid, relay_id);

CREATE TABLE downloadlogs
(
    pid CHAR(13), 
    logcd INTEGER,
    uuid CHAR(36),
    tasknm VARCHAR2(1024),
    contentnm VARCHAR2(1024),
    errorcd INTEGER,
    start_dt NUMBER(19, 0),
	end_dt NUMBER(19, 0)
);

ALTER TABLE downloadlogs
	ADD CONSTRAINT d_log_fkey
	FOREIGN KEY(pid)
	REFERENCES players(pid);

CREATE INDEX idx_downloadlog ON downloadlogs (start_dt DESC, logcd, pid);

CREATE TABLE managerlogs
(
    usernm VARCHAR2(1024), 
    logcd INTEGER,
    uuid CHAR(36),
    actionnm VARCHAR2(1024),
    actiondesc VARCHAR2(1024),
    errorcd INTEGER,
    reg_dt NUMBER(19, 0)
);

CREATE INDEX idx_managerlog ON managerlogs (reg_dt DESC, logcd);

CREATE TABLE connectionlogs
(
    pid CHAR(13), 
    logcd INTEGER,
    errorcd INTEGER,
    start_dt NUMBER(19, 0),
    end_dt NUMBER(19, 0),
	reg_dt NUMBER(19, 0)
);

ALTER TABLE connectionlogs
	ADD PRIMARY KEY(pid, reg_dt);

ALTER TABLE connectionlogs
	ADD CONSTRAINT c_log_fkey
	FOREIGN KEY(pid)
	REFERENCES players(pid);

CREATE INDEX idx_connectionlog ON connectionlogs (reg_dt DESC, logcd, pid);

create table tbl_limitplayers (gid CHAR(13), cntplayers INTEGER );

CREATE TABLE SYNC_TASKS(GUUID CHAR(36) NOT NULL, PLAYERS_PID CHAR(13) NOT NULL, SYNC_TP CHAR(1) NOT NULL, EDIT_DT VARCHAR(14) NULL, REG_DT VARCHAR(14) NOT NULL, DEL_YN CHAR(1) NOT NULL, PRIMARY KEY(GUUID, PLAYERS_PID), CONSTRAINT XFK_SYNC_TASKS_PLAYERS_PID FOREIGN KEY(PLAYERS_PID) REFERENCES PLAYERS(PID));

ALTER TABLE tbl_limitplayers
	ADD CONSTRAINT limit_fkey
	FOREIGN KEY(gid)
	REFERENCES groups(gid);

CREATE SEQUENCE users_SEQUENCE
START WITH 1;

CREATE TABLE users (id INTEGER PRIMARY KEY, name VARCHAR2(1024) UNIQUE, hash VARCHAR2(1024), parent_id INTEGER, role_ids VARCHAR2(1024), is_enabled char(1) DEFAULT 'Y');

CREATE OR REPLACE TRIGGER users_trigger
BEFORE INSERT
ON users
REFERENCING NEW AS NEW
FOR EACH ROW
BEGIN
SELECT users_SEQUENCE.nextval INTO :NEW.id FROM dual;
END;

CREATE TABLE ivision_timestamps ( ts_key VARCHAR(50), ts_value NUMBER(19, 0));

insert into ivision_timestamps (ts_key, ts_value) values ('meta_tag_groups', 1);
insert into ivision_timestamps (ts_key, ts_value) values ('relaylogs', 1);

CREATE TABLE meta_tags (m_id NUMBER(19, 0) PRIMARY KEY, g_id NUMBER(19, 0), m_tagname VARCHAR(50), m_tagvalue VARCHAR(50), m_tagdescription VARCHAR(100));

CREATE TABLE meta_tag_groups (g_id NUMBER(19, 0) PRIMARY KEY, g_name VARCHAR(50), g_ui_type INTEGER, g_priority INTEGER);

ALTER TABLE meta_tags																	 
	ADD CONSTRAINT g_id																 
	FOREIGN KEY(g_id)																	 
	REFERENCES meta_tag_groups(g_id);		

CREATE SEQUENCE roles_SEQUENCE
START WITH 1;

CREATE TABLE roles (role_id NUMBER(19, 0)  PRIMARY KEY, role_name VARCHAR(100), role_level INTEGER, is_enabled VARCHAR(2) DEFAULT 'Y');

CREATE OR REPLACE TRIGGER roles_trigger
BEFORE INSERT
ON roles
REFERENCING NEW AS NEW
FOR EACH ROW
BEGIN
SELECT roles_SEQUENCE.nextval INTO :NEW.ROLE_ID FROM dual;
END;

CREATE SEQUENCE permissions_SEQUENCE
START WITH 1;

CREATE TABLE permissions (perm_id NUMBER(19, 0)  PRIMARY KEY, perm_name VARCHAR(100), parent_perm_id NUMBER(19, 0));

CREATE OR REPLACE TRIGGER permissions_trigger
BEFORE INSERT
ON permissions
REFERENCING NEW AS NEW
FOR EACH ROW
BEGIN
SELECT permissions_SEQUENCE.nextval INTO :NEW.perm_id FROM dual;
END;

CREATE SEQUENCE Conn_SEQUENCE
START WITH 1;

CREATE TABLE "datainfo" ( 
    "Id"         	varchar(18) PRIMARY KEY NOT NULL,
    "User_id"    	NUMBER(19, 0) ,
    "Template_id"	varchar(18) ,
    "_Title"     	VARCHAR(100)
);

CREATE TABLE "data_field" ( 
    "Id"         	varchar(18) PRIMARY KEY NOT NULL,
    "DataInfo_id"	varchar(18) ,
    "Value1"     	VARCHAR(100) ,
    "Value2"     	VARCHAR(100) ,
    "Value3"     	VARCHAR(100) ,
    "Value4"     	VARCHAR(100) ,
    "Value5"     	VARCHAR(100) ,
    "Value6"     	VARCHAR(100) ,
    "Value7"     	VARCHAR(100) ,
    "Value8"     	VARCHAR(100) ,
    "Value9"     	VARCHAR(100) ,
    "Value10"   	VARCHAR(100) ,
    "Value11"    	VARCHAR(100) ,
    "Value12"    	VARCHAR(100) ,
    "Value13"    	VARCHAR(100) ,
    "Value14"    	VARCHAR(100) ,
    "Value15"    	VARCHAR(100) ,
    "Value16"    	VARCHAR(100) ,
    "Value17"    	VARCHAR(100) ,
    "Value18"    	VARCHAR(100) ,
    "Value19"    	VARCHAR(100) ,
    "Value20"    	VARCHAR(100) ,
    "Value21"    	VARCHAR(100) ,
    "Value22"    	VARCHAR(100) ,
    "Value23"    	VARCHAR(100) ,
    "Value24"    	VARCHAR(100) ,
    "Value25"    	VARCHAR(100) ,
    "Value26"    	VARCHAR(100) ,
    "Value27"    	VARCHAR(100) ,
    "Value28"    	VARCHAR(100) ,
    "Value29"    	VARCHAR(100) ,
    "Value30"    	VARCHAR(100) ,
    "Field_name" 	VARCHAR(100)
);

CREATE TABLE "contents" ( 
    "id"           	varchar(25) PRIMARY KEY NOT NULL,
    "Data_field_id"	varchar(25) ,
    "_Type"        	varchar(25) ,
    "_Value"       	VARCHAR(100) ,
    "File_nm"     	VARCHAR(100) ,
    "_Path"        	VARCHAR(100) ,
    "_Desc"        	VARCHAR(100) ,
    "Data_info_id" 	varchar(25) 
);

CREATE TABLE "resultdatas" ( 
    "apply_date" 	NUMBER(19, 0) ,
    "playerid"   	varchar(25) ,
    "datafieldid"	varchar(25) ,
    "tempalteid" 	varchar(25) ,
    "data_desc"  	VARCHAR(100) ,
    "data_values"	VARCHAR(100)  
);

CREATE TABLE role_perm_ASN ( 
	role_id	NUMBER(19, 0) NOT NULL,
	perm_id	NUMBER(19, 0) NOT NULL 
	);

ALTER TABLE role_perm_ASN
	ADD CONSTRAINT role_id
	FOREIGN KEY(role_id)
	REFERENCES roles(role_id);

ALTER TABLE role_perm_ASN
	ADD CONSTRAINT perm_id
	FOREIGN KEY(perm_id)
	REFERENCES permissions(perm_id);

CREATE TABLE user_player_ASN ( 
	user_id	NUMBER(19, 0) NOT NULL,
	pid    	CHAR(13) NOT NULL 
	);

ALTER TABLE user_player_ASN
	ADD CONSTRAINT user_id
	FOREIGN KEY(user_id)
	REFERENCES users(id);

ALTER TABLE user_player_ASN
	ADD CONSTRAINT pid
	FOREIGN KEY(pid)
	REFERENCES players(pid);

CREATE TABLE user_group_ASN (
	user_id NUMBER(19, 0) NOT NULL,
	gid CHAR(13) NOT NULL);

ALTER TABLE user_group_ASN
	ADD CONSTRAINT g_user_id
	FOREIGN KEY(user_id)
	REFERENCES users(id);

ALTER TABLE user_group_ASN
	ADD CONSTRAINT gid
	FOREIGN KEY(gid)
	REFERENCES groups(gid);

insert into roles (role_id, role_name, role_level) VALUES (1,'SYSTEM ADMINISTRATOR', 0);
insert into roles (role_id, role_name, role_level) VALUES (2,'SERVICE ADMINISTRATOR', 1);
insert into roles (role_id, role_name, role_level) VALUES (3,'GROUP ADMINISTRATOR', 2);
insert into roles (role_id, role_name, role_level) VALUES (4,'PLAYER ADMINISTRATOR', 3);
insert into roles (role_id, role_name, role_level) VALUES (5,'OBSERVER', 3);
insert into roles (role_id, role_name, role_level) VALUES (6,'SCHEDULER', 3);
insert into roles (role_id, role_name, role_level) VALUES (7,'USER MANAGER', 0);
insert into roles (role_id, role_name, role_level) VALUES (8,'GROUP MANAGER', 3);
insert into roles (role_id, role_name, role_level) VALUES (9,'PLAYER MANAGER', 3);
			
insert into permissions (perm_id, perm_name, parent_perm_id) VALUES (1,'ADMINISTRATOR', -1);
insert into permissions (perm_id, perm_name, parent_perm_id) VALUES (2,'VIEW USER', -1);
insert into permissions (perm_id, perm_name, parent_perm_id) VALUES (3,'CREATE USER', -1);
insert into permissions (perm_id, perm_name, parent_perm_id) VALUES (4,'UPDATE USER', -1);
insert into permissions (perm_id, perm_name, parent_perm_id) VALUES (5,'DELETE USER', -1);
insert into permissions (perm_id, perm_name, parent_perm_id) VALUES (6,'VIEW PARENT GROUP', -1);
insert into permissions (perm_id, perm_name, parent_perm_id) VALUES (7,'VIEW OWN GROUP', -1);
insert into permissions (perm_id, perm_name, parent_perm_id) VALUES (8,'VIEW CHILD GROUP', -1);
insert into permissions (perm_id, perm_name, parent_perm_id) VALUES (9,'VIEW PLAYER', -1);
insert into permissions (perm_id, perm_name, parent_perm_id) VALUES (10,'CREATE PLAYER', -1);
insert into permissions (perm_id, perm_name, parent_perm_id) VALUES (11,'CREATE GROUP', -1);
insert into permissions (perm_id, perm_name, parent_perm_id) VALUES (12,'UPDATE PLAYER', -1);
insert into permissions (perm_id, perm_name, parent_perm_id) VALUES (13,'UPDATE GROUP', -1);
insert into permissions (perm_id, perm_name, parent_perm_id) VALUES (14,'DELETE PLAYER', -1);
insert into permissions (perm_id, perm_name, parent_perm_id) VALUES (15,'DELETE GROUP', -1);
insert into permissions (perm_id, perm_name, parent_perm_id) VALUES (16,'VIEW SCHEDULE', -1);
insert into permissions (perm_id, perm_name, parent_perm_id) VALUES (17,'VIEW PARENT SCHEDULE', -1);
insert into permissions (perm_id, perm_name, parent_perm_id) VALUES (18,'CREATE SCHEDULE', -1);
insert into permissions (perm_id, perm_name, parent_perm_id) VALUES (19,'UPDATE SCHEDULE', -1);
insert into permissions (perm_id, perm_name, parent_perm_id) VALUES (20,'DELETE SCHEDULE', -1);

/*롤과 퍼미션 연결*/
insert into role_perm_ASN (role_id, perm_id) VALUES (1,1);

insert into role_perm_ASN (role_id, perm_id) VALUES (2,2);
insert into role_perm_ASN (role_id, perm_id) VALUES (2,3);
insert into role_perm_ASN (role_id, perm_id) VALUES (2,4);
insert into role_perm_ASN (role_id, perm_id) VALUES (2,5);
insert into role_perm_ASN (role_id, perm_id) VALUES (2,7);
insert into role_perm_ASN (role_id, perm_id) VALUES (2,8);
insert into role_perm_ASN (role_id, perm_id) VALUES (2,9);
insert into role_perm_ASN (role_id, perm_id) VALUES (2,10);
insert into role_perm_ASN (role_id, perm_id) VALUES (2,11);
insert into role_perm_ASN (role_id, perm_id) VALUES (2,12);
insert into role_perm_ASN (role_id, perm_id) VALUES (2,13);
insert into role_perm_ASN (role_id, perm_id) VALUES (2,14);
insert into role_perm_ASN (role_id, perm_id) VALUES (2,15);
insert into role_perm_ASN (role_id, perm_id) VALUES (2,16);
insert into role_perm_ASN (role_id, perm_id) VALUES (2,17);
insert into role_perm_ASN (role_id, perm_id) VALUES (2,18);
insert into role_perm_ASN (role_id, perm_id) VALUES (2,19);
insert into role_perm_ASN (role_id, perm_id) VALUES (2,20);

insert into role_perm_ASN (role_id, perm_id) VALUES (3,2);
insert into role_perm_ASN (role_id, perm_id) VALUES (3,3);
insert into role_perm_ASN (role_id, perm_id) VALUES (3,4);
insert into role_perm_ASN (role_id, perm_id) VALUES (3,5);
insert into role_perm_ASN (role_id, perm_id) VALUES (3,7);
insert into role_perm_ASN (role_id, perm_id) VALUES (3,8);
insert into role_perm_ASN (role_id, perm_id) VALUES (3,9);
insert into role_perm_ASN (role_id, perm_id) VALUES (3,10);
insert into role_perm_ASN (role_id, perm_id) VALUES (3,11);
insert into role_perm_ASN (role_id, perm_id) VALUES (3,12);
insert into role_perm_ASN (role_id, perm_id) VALUES (3,13);
insert into role_perm_ASN (role_id, perm_id) VALUES (3,14);
insert into role_perm_ASN (role_id, perm_id) VALUES (3,15);
insert into role_perm_ASN (role_id, perm_id) VALUES (3,16);
insert into role_perm_ASN (role_id, perm_id) VALUES (3,17);
insert into role_perm_ASN (role_id, perm_id) VALUES (3,18);
insert into role_perm_ASN (role_id, perm_id) VALUES (3,19);
insert into role_perm_ASN (role_id, perm_id) VALUES (3,20);

insert into role_perm_ASN (role_id, perm_id) VALUES (4,2);
insert into role_perm_ASN (role_id, perm_id) VALUES (4,4);
insert into role_perm_ASN (role_id, perm_id) VALUES (4,9);
insert into role_perm_ASN (role_id, perm_id) VALUES (4,16);
insert into role_perm_ASN (role_id, perm_id) VALUES (4,17);
insert into role_perm_ASN (role_id, perm_id) VALUES (4,18);
insert into role_perm_ASN (role_id, perm_id) VALUES (4,19);
insert into role_perm_ASN (role_id, perm_id) VALUES (4,20);

insert into role_perm_ASN (role_id, perm_id) VALUES (5,6);
insert into role_perm_ASN (role_id, perm_id) VALUES (5,7);
insert into role_perm_ASN (role_id, perm_id) VALUES (5,8);
insert into role_perm_ASN (role_id, perm_id) VALUES (5,9);
insert into role_perm_ASN (role_id, perm_id) VALUES (5,16);
insert into role_perm_ASN (role_id, perm_id) VALUES (5,17);

insert into role_perm_ASN (role_id, perm_id) VALUES (6,7);
insert into role_perm_ASN (role_id, perm_id) VALUES (6,8);
insert into role_perm_ASN (role_id, perm_id) VALUES (6,9);
insert into role_perm_ASN (role_id, perm_id) VALUES (6,16);
insert into role_perm_ASN (role_id, perm_id) VALUES (6,17);
insert into role_perm_ASN (role_id, perm_id) VALUES (6,18);
insert into role_perm_ASN (role_id, perm_id) VALUES (6,19);
insert into role_perm_ASN (role_id, perm_id) VALUES (6,20);

insert into role_perm_ASN (role_id, perm_id) VALUES (7,2);
insert into role_perm_ASN (role_id, perm_id) VALUES (7,3);
insert into role_perm_ASN (role_id, perm_id) VALUES (7,4);
insert into role_perm_ASN (role_id, perm_id) VALUES (7,5);

insert into role_perm_ASN (role_id, perm_id) VALUES (8,7);
insert into role_perm_ASN (role_id, perm_id) VALUES (8,8);
insert into role_perm_ASN (role_id, perm_id) VALUES (8,11);
insert into role_perm_ASN (role_id, perm_id) VALUES (8,13);
insert into role_perm_ASN (role_id, perm_id) VALUES (8,16);

insert into role_perm_ASN (role_id, perm_id) VALUES (9,9);
insert into role_perm_ASN (role_id, perm_id) VALUES (9,10);
insert into role_perm_ASN (role_id, perm_id) VALUES (9,12);
insert into role_perm_ASN (role_id, perm_id) VALUES (9,14);

select * from roles;
select * from permissions;

update permissions SET parent_perm_id = -1;

INSERT INTO users (id, name,hash,role_ids, parent_id,is_enabled) VALUES (1, 'admin','21232f297a57a5a743894a0e4a801fc3','1','-1','Y');

CREATE TABLE downloadmaster (uuid CHAR(36) NOT NULL, pid CHAR(13) NOT NULL, reg_date NUMBER(19, 0));

ALTER TABLE downloadmaster
	ADD CONSTRAINT d_master_uuid
	FOREIGN KEY(uuid)
	REFERENCES tasks(uuid);

ALTER TABLE downloadmaster
	ADD CONSTRAINT d_master_pid
	FOREIGN KEY(pid)
	REFERENCES players(pid);

ALTER TABLE downloadmaster
	ADD PRIMARY KEY(uuid, pid);

INSERT INTO groups (gid,name,source,path,password, username,  depth) VALUES ('0000000000000','ROOT NETWORK','','', '', '', 0);

insert into user_group_asn (user_id, gid) values (1, '0000000000000');

CREATE TABLE report_normal_schedule
(
	YYYY                 CHAR(4)  NOT NULL ,
	MM                   CHAR(2)  NOT NULL ,
	DD                   CHAR(2)  NOT NULL ,
	CNT                  INTEGER  NULL ,
	pid                  CHAR(13)  NOT NULL 
);


ALTER TABLE report_normal_schedule
	ADD CONSTRAINT XPKreport_normal_schedule PRIMARY KEY  (pid ,YYYY ,MM ,DD );


CREATE TABLE report_play
(
	YYYY                 CHAR(4)  NOT NULL ,
	MM                   CHAR(2)  NOT NULL ,
	DD                   CHAR(2)  NOT NULL ,
	H24                  CHAR(2)  NOT NULL ,
	CNT                  INTEGER  NULL ,
	s_id                 NUMBER(19, 0)  NOT NULL 
);

ALTER TABLE report_play
	ADD CONSTRAINT XPKreport_play PRIMARY KEY  (s_id ,YYYY ,MM ,DD ,H24 );

CREATE TABLE report_relay
(
	YYYY                 CHAR(4)  NOT NULL ,
	MM                   CHAR(2)  NOT NULL ,
	DD                   CHAR(2)  NOT NULL ,
	H24                  CHAR(2)  NOT NULL ,
	g_id                 NUMBER(19, 0)  NOT NULL ,
	CNT                  INTEGER  NULL ,
	r_key                VARCHAR(32)  NOT NULL 
);


ALTER TABLE report_relay
	ADD CONSTRAINT XPKreport_relay PRIMARY KEY  (g_id ,r_key ,YYYY ,MM ,DD ,H24 );


CREATE TABLE report_running_time
(
	pid                  char(13)  NOT NULL ,
	YYYY                 CHAR(4)  NOT NULL ,
	MM                   CHAR(2)  NOT NULL ,
	DD                   CHAR(2)  NOT NULL ,
	H24                  CHAR(2)  NOT NULL ,
	TM                   int  NULL 
);


ALTER TABLE report_running_time
	ADD CONSTRAINT XPKreport_running_time PRIMARY KEY   (pid ,YYYY ,MM ,DD ,H24 );


CREATE TABLE report_time_schedule
(
	pid                  char(13)  NOT NULL ,
	YYYY                 CHAR(4)  NOT NULL ,
	MM                   CHAR(2)  NOT NULL ,
	DD                   CHAR(2)  NOT NULL ,
	H24                  CHAR(2)  NOT NULL ,
	type		     INTEGER  NULL,
	CNT                  INTEGER  NULL 
);

CREATE TABLE summary_players
(
	pid			char(13)  NOT NULL ,
	YYYY		CHAR(4)  NOT NULL ,
	MM			CHAR(2)  NOT NULL ,
	DD			CHAR(2)  NOT NULL ,
	H24			CHAR(2)  NOT NULL ,
	gid			char(13) NOT NULL,
	name		VARCHAR2(1024), 
	host		VARCHAR(128), 
	state		INTEGER, 
	uptime		INTEGER,
	lastconndt	NUMBER(19, 0), 
	versionH	INTEGER, 
	versionL	INTEGER,
	freespace	INTEGER,
	memoryusage INTEGER,
	cpurate		INTEGER,
	volume		INTEGER,
	serial_power VARCHAR(10),
	serial_source VARCHAR(50),
	serial_volume INTEGER
);

ALTER TABLE summary_players
	ADD CONSTRAINT XPKsummary_players PRIMARY KEY   (pid ,YYYY ,MM ,DD ,H24 );

ALTER TABLE report_time_schedule
	ADD CONSTRAINT XPKreport_time_schedule PRIMARY KEY   (pid ,YYYY ,MM ,DD ,H24 , type );



ALTER TABLE report_normal_schedule
	ADD CONSTRAINT  R_38 FOREIGN KEY (pid) REFERENCES players(pid);



ALTER TABLE report_play
	ADD CONSTRAINT  R_33 FOREIGN KEY (s_id) REFERENCES screens(s_id);



ALTER TABLE report_relay
	ADD CONSTRAINT  R_36 FOREIGN KEY (g_id) REFERENCES meta_tag_groups(g_id);



ALTER TABLE report_running_time
	ADD CONSTRAINT  R_34 FOREIGN KEY (pid) REFERENCES players(pid);




ALTER TABLE report_time_schedule
	ADD CONSTRAINT  R_37 FOREIGN KEY (pid) REFERENCES players(pid);
