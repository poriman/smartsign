﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DigitalSignage.ServerDatabase;

namespace DigitalSignage.Common
{
	public interface IDownloadList
    {
		long GetCurrTS();
		ServerDataSet.downloadmasterDataTable GetDataByGID(string duuid, string gid);
		ServerDataSet.downloadmasterDataTable GetDataByPID(string duuid, string gid, string pid);
		ServerDataSet.downloadmasterDataTable GetDataByGID(string gid, long start, long end);
		ServerDataSet.downloadmasterDataTable GetDataByPID(string gid, string pid, long start, long end);
		int InsertDownloadInformation(string duuid, string gid, string pid, long reg_date);
		int UpdateRegDate(long id, long reg_date);
    }
}
