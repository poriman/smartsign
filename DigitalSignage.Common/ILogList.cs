﻿using System;

using DigitalSignage.ServerDatabase;

namespace DigitalSignage.Common
{
    public interface ILogList
    {
		#region old
		ServerDataSet.logsDataTable GetLogsFromTo(string pid, int type, int category, long from, long to);
		ServerDataSet.logsDataTable GetAllLogs(string pid, int type, int category);
		int InsertLogs(string pid, int type, int category, string uuid, string name, string description, int code, long regdate);
		int DeleteLogsBefore(long dt);
		#endregion
	}
}