﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigitalSignage.Common
{
	/// <summary>
	/// 시간을 UTP 또는 Datatime 클래스로 변경하는 컨버터 클래스
	/// </summary>
    public class TimeConverter
    {
        protected static readonly long epochStart =
            (new DateTime(1970, 1, 1)).Ticks;
        protected static DateTime epochStartDT = new DateTime(1970, 1, 1);

		/// <summary>
		/// DataTime을 UTP로 변경
		/// </summary>
		/// <param name="dt">로컬 시간</param>
		/// <returns>UTP Tick 정보</returns>
        public static long ConvertToUTP(DateTime dt)
        {
            //converting from 100-nanosecons intervals to 1-second intervals
            return (dt.Ticks - epochStart) / 10000000;
        }

        /// <summary>
        /// YYYYMMDDH24mmSS를 UTP타임으로 변경
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static long ConvertYYYYMMDDH24mmSSToUTP(string dt)
        {
            try
            {
                DateTime datetime = new DateTime(
                    Convert.ToInt32(dt.Substring(0, 4)),
                    Convert.ToInt32(dt.Substring(4, 2)),
                    Convert.ToInt32(dt.Substring(6, 2)),
                    Convert.ToInt32(dt.Substring(8, 2)),
                    Convert.ToInt32(dt.Substring(10, 2)),
                    Convert.ToInt32(dt.Substring(12, 2)));

                return ConvertToUTP(datetime);
            }
            catch { return ConvertToUTP(DateTime.MinValue); }
                
        }

		/// <summary>
		/// UTP Tick에서 Datatime으로 변경
		/// </summary>
		/// <param name="tp">UTP Tick 정보</param>
		/// <returns>Datetime 반환</returns>
        public static DateTime ConvertFromUTP(long tp)
        {
			return epochStartDT.Add(new TimeSpan(tp * 10000000));
		}

        /// <summary>
        /// UTP타임을 YYYYMMDDH24mmSS로 변경
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static String ConvertYYYYMMDDH24mmSSFromUTP(long tp)
        {
            try
            {
                DateTime datetime = ConvertFromUTP(tp);
                return datetime.ToString("yyyyMMddHHmmss");
            }
            catch { return DateTime.MinValue.ToString("yyyyMMddHHmmss"); }

        }

        /// <summary>
        /// UTC 시간을 년 월 일 시 분 초 밀리초 형태의 문자열로 변환한다.
        /// </summary>
        /// <param name="utcTime"></param>
        /// <returns></returns>
        public static String ConvertTo17LengthDateTimeString(DateTime utcTime)
        {
            return utcTime.ToString("yyyyMMddHHmmssfff");
        }

        /// <summary>
        /// 년 월 일 시 분 초 밀리초 형태의 문자열을 UTC DateTime으로 변환한다.
        /// </summary>
        /// <param name="yyyyMMddHHmmssfff"></param>
        /// <returns></returns>
        public static DateTime Convert17LengthStringToDateTime(String yyyyMMddHHmmssfff)
        {
            try
            {
                DateTime datetime = new DateTime(
                    Convert.ToInt32(yyyyMMddHHmmssfff.Substring(0, 4)),
                    Convert.ToInt32(yyyyMMddHHmmssfff.Substring(4, 2)),
                    Convert.ToInt32(yyyyMMddHHmmssfff.Substring(6, 2)),
                    Convert.ToInt32(yyyyMMddHHmmssfff.Substring(8, 2)),
                    Convert.ToInt32(yyyyMMddHHmmssfff.Substring(10, 2)),
                    Convert.ToInt32(yyyyMMddHHmmssfff.Substring(12, 2)),
                    Convert.ToInt32(yyyyMMddHHmmssfff.Substring(14, 3)));


                return datetime;
            }
            catch { return DateTime.MinValue; }
        }

		/// <summary>
		/// time 정보를 문자열으로 변경
		/// </summary>
		/// <param name="hour">시</param>
		/// <param name="minute">분</param>
		/// <param name="seconds">초</param>
		/// <returns>변경된 문자열 (예 01:59:59)</returns>
        public static string TimeToStr(int hour, int minute, int seconds)
        {
            string res = "";
            if (hour < 10)
                res += "0" + hour;
            else
                res += hour;
            res += ":";
            if (minute < 10)
                res += "0" + minute;
            else
                res += minute;
            res += ":";
            if (seconds < 10)
                res += "0" + seconds;
            else
                res += seconds;
            return res;
        }

        public static string SecToStr(int nSec)
        {
            TimeSpan ts = TimeSpan.FromSeconds(nSec);
            return String.Format("{0:D3}{1:D2}{2:D2}{3:D2}", Convert.ToInt32(Math.Floor(ts.TotalDays)), ts.Hours, ts.Minutes, ts.Seconds);
        }

        public static int StrToSec(String dddh24mmss)
        {
            try
            {
                TimeSpan ts = new TimeSpan(Convert.ToInt32(dddh24mmss.Substring(0, 3)),
                    Convert.ToInt32(dddh24mmss.Substring(3, 2)),
                    Convert.ToInt32(dddh24mmss.Substring(5, 2)),
                    Convert.ToInt32(dddh24mmss.Substring(7, 2)));
                return (int)ts.TotalSeconds;
            }
            catch { return 0; }
        }

		/// <summary>
		/// 시간 문자열에서 TimeSpan으로 변경
		/// </summary>
		/// <param name="time">시간 문자열 (예 01:23:00)</param>
		/// <returns>TimeSpan으로 변경</returns>
        public static TimeSpan StrToTime(string time)
        {
            string[] data = time.Split(':');
            if (data.Length != 3) throw new Exception("Wrong time format(1)");
            int hour = 0, min = 0, sec = 0;
            hour = Int32.Parse(data[0]);
            min = Int32.Parse(data[1]);
            sec = Int32.Parse(data[2]);
            return new TimeSpan(hour, min, sec); ;
        }
    }
}
