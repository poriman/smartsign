﻿using System;
using DigitalSignage.ServerDatabase;
using DigitalSignage.DataBase;

namespace DigitalSignage.Common
{
    public interface IGroupList
    {
// 		int AddGroup(ServerDataSet.groupsDataTable newGroup);
// 		int DeleteGroup(ServerDataSet.groupsDataTable group);
// 		int UpdateGroup(ServerDataSet.groupsDataTable group);
// 		ServerDataSet.groupsDataTable GetGroupByID(string id);
		string GetGroupsXmlByPID(string pid);
		string GetGroupListByPID(string pid);
		string GetGroupListByGID(string gid);
		string GetGroupInfo(string gid, string attribute);

		#region old
        int AddGroups(DataSet.groupsDataTable newGroups);
        int UpdateGroups(DataSet.groupsDataTable groups);
        int DeleteGroups(DataSet.groupsDataTable groups);
		DataSet.groupsDataTable GetGroupByID(string id);
		DataSet.groupsDataTable GetGroupsByPID(string pid);
		DataSet.groupsDataTable GetGroups();
		#endregion
    }
}
