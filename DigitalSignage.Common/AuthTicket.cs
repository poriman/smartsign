﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigitalSignage.Common
{
    [Serializable]
    public class AuthTicket
    {
        public String name;
        public String passhash;
        public UserLevels level = UserLevels.UserNone;
		public string gid = "-1";
		public string pid = "-1";
		public int uid = -1;
    }
}
