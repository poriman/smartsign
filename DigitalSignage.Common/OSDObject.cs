﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigitalSignage.Common
{
    /// <summary>
    /// OSD 관리 싱글톤 객체 클래스
    /// </summary>
    public class OSDObject
    {
        private static object lockObject = new object();

        /// <summary>
        /// 생성자
        /// </summary>
        private OSDObject()
        {

        }

        public bool HasOSDObject
        {
            get
            {
                return this.OSDChanged != null;
            }
        }

        static private OSDObject _instance = null;

        /// <summary>
        /// 싱글톤 객체 반환
        /// </summary>
        static public OSDObject GetInstance
        {
            get
            {
                lock (lockObject)
                {
                    if (_instance == null)
                        _instance = new OSDObject();
                }

                return _instance;
            }
        }

        /// <summary>
        /// OSD 변경 통보
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="sOSDText"></param>
        public void OnOSDChanged(object sender, string sOSDText)
        {
            if (OSDChanged != null) OSDChanged(sender, new OSDChangedEventArgs(sOSDText));
        }

        /// <summary>
        /// OSD 변경 이벤트
        /// </summary>
        public event OSDChangeEventHandler OSDChanged = null;
    }

    public delegate void OSDChangeEventHandler(object sender, OSDChangedEventArgs e);

    public class OSDChangedEventArgs : EventArgs
    {
        public String OSDText = String.Empty;

        public OSDChangedEventArgs(string sOSDText)
        {
            OSDText = sOSDText;
        }
    }
}
