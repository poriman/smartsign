﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigitalSignage.Common
{
    /// <summary>
    /// 스케줄에 의한 스크린 정보
    /// </summary>
    public class ScreenStartInfoInSchedule
    {
        /// <summary>
        /// 인스턴스
        /// </summary>
        private static ScreenStartInfoInSchedule _instance = null;
        /// <summary>
        /// Lock 인스턴스
        /// </summary>
        private static object lockObj = new object();

        /// <summary>
        /// 생성자
        /// </summary>
        private ScreenStartInfoInSchedule()
        {
            DueTime = 1000;
            SyncThreshold = 100;
            IsUsingSeek = false;
            PauseInStarting = 0;
        }

        /// <summary>
        /// 인스턴스 
        /// </summary>
        public static ScreenStartInfoInSchedule GetInstance
        {
            get
            {
                lock (lockObj)
                {
                    if (_instance == null)
                    {
                        _instance = new ScreenStartInfoInSchedule();
                    }
                    return _instance;
                }
            }
        }

        /// <summary>
        /// Seek를 사용할지 여부
        /// </summary>
        public bool IsUsingSeek { get; set; }

        /// <summary>
        /// 처음 시작할때 Seek를 허용할지여부
        /// </summary>
        public bool IsUsingFirstSeek { get; set; }

        /// <summary>
        /// 동기화가 구동되기 위한 Threshold (ms)
        /// </summary>
        public int SyncThreshold { get; set; }

        /// <summary>
        /// 동기화 DueTime (ms)
        /// </summary>
        public int DueTime { get; set; }

        /// <summary>
        /// 동기화 조정 값
        /// </summary>
        public int AdjustValue { get; set; }

        /// <summary>
        /// 스케줄 ID
        /// </summary>
        public string ScheduleID { get; set; }

        /// <summary>
        /// 그룹 스케줄 ID
        /// </summary>
        public string GroupScheduleID { get; set; }

        /// <summary>
        /// 다운로드 ID
        /// </summary>
        public string DownloadID { get; set; }

        /// <summary>
        /// 스케줄 이름
        /// </summary>
        public string ScheduleName { get; set; }

        /// <summary>
        /// 스케줄의 재생시간 (초)
        /// </summary>
        public long ScheduleDuration { get; set; }

        /// <summary>
        /// 스크린 ID
        /// </summary>
        public string ScreenID { get; set; }

        /// <summary>
        /// 스크린 이름
        /// </summary>
        public string ScreenName { get; set; }

        /// <summary>
        /// 스크린 시작시간
        /// </summary>
        public DateTime ScreenStartDT { get; set; }

        /// <summary>
        /// 스케줄이 시작한 시간
        /// </summary>
        public DateTime ScheduleStartDT { get; set; }

        /// <summary>
        /// 스케줄 재생 조건
        /// </summary>
        public int ShowFlag {get; set;}

        /// <summary>
        /// 동기화 마스터 스케줄인지
        /// </summary>
        public bool IsSyncMaster { get { return (this.ShowFlag & (int)Common.ShowFlag.Show_SyncMaster) > 0; }}

        /// <summary>
        /// 동기화 슬래이브 스케줄 인지
        /// </summary>
        public bool IsSyncSlave { get { return (this.ShowFlag & (int)Common.ShowFlag.Show_SyncSlave) > 0; }}

        /// <summary>
        /// 동기화 스케줄 인지
        /// </summary>
        public bool IsSyncTask { get { return IsSyncMaster || IsSyncSlave; } }

        /// <summary>
        /// 시작시 몇ms를 멈췄다가 재생할지 
        /// </summary>
        public int PauseInStarting { get; set; }

        /// <summary>
        /// 스크린 시작 이벤트
        /// </summary>
        public event EventHandler ScreenStarted;

        public void OnScreenStarted(object sender)
        {
            //if(!IsSyncSlave) ScreenStartDT = DateTime.Now;

            if (ScreenStarted != null) ScreenStarted(sender, new EventArgs());
        }

        /// <summary>
        /// 스크린 멈춤 이벤트
        /// </summary>
        public event EventHandler ScreenPaused;

        public void OnScreenPaused(object sender)
        {
            if (ScreenPaused != null) ScreenPaused(sender, new EventArgs());
        }
    }
}
