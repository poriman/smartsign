﻿using System;
using DigitalSignage.ServerDatabase;
using System.Xml;

namespace DigitalSignage.Common
{
    public interface IDataCacheList
    {
		String GetUserNode(string id, string pwd);
		String GetNetworkTreeByUser(string userID);
		String GetGroupNode(string gid);
		String GetPlayerNode(string pid);

		String GetInheritedFTPInfo(string gid);
		int GetGroupDepth(string parent_gid);
		int GetPlayerDepth(string pid);

		bool AddPlayer(string pid, string gid, string name, string host);
		bool UpdatePlayer(string pid, string gid, string name);
		bool DeletePlayer(string pid);
		
		bool AddGroup(string gid, string name, string source, string username, string password, string parent_gid, string is_inherited, int depth, string pid, string path);
		bool UpdateGroup(string gid, string name, string source, string username, string password, string parent_gid, string is_inherited, int depth, string path);
		bool DeleteGroup(string gid);
	}
}
