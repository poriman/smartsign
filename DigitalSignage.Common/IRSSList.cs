﻿using System;

using Sloppycode.net;

namespace DigitalSignage.Common
{
    public interface IRSSList
    {
        int AddChannel(RssFeed newChannel);
        int DeleteChannel( int channel);
        RssFeed[] GetChannels();
        RssItems GetChannelContent(int channel);
    }
}