﻿using System;

using DigitalSignage.DataBase;

namespace DigitalSignage.Common
{
    public interface ICmdList
    {
        int Update(AuthTicket at, DataSet.RSCommandsDataTable data );
        int Delete( AuthTicket at, int id);
        DataSet.RSCommandsDataTable Get(int id);
        bool IsInUse(int id);
        DataSet.RSCommandsDataTable GetCommands();
    }
}
