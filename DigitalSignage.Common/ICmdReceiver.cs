﻿using System;

namespace DigitalSignage.Common
{
    public interface ICmdReceiver
    {
        long ProcessSimpleCommand(CmdReceiverCommands command);
		Object ProcessExpandCommand(CmdExReceiverCommands command);
    }
}

