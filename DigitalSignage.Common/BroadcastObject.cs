﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Net.Sockets;

using NLog;
using System.Net;

namespace DigitalSignage.Common
{
	/// <summary>
	/// UDP 브로드 캐스트를 통해 서버 식별
	/// </summary>
	public interface IBroadcastObject
	{
		/// <summary>
		/// 초기화
		/// </summary>
		void Initialiaze();
		
		/// <summary>
		/// 해제
		/// </summary>
		void Uninitialize();

	}

	/// <summary>
	/// 소켓 및 관련 정보를 위한 이벤트 핸들러 델리게이트
	/// </summary>
	/// <param name="sender">보내는 Object</param>
	/// <param name="e">관련 아규먼트</param>
	public delegate void SocketEventHandler(object sender, SocketEventArgs e);

	/// <summary>
	/// 소켓 및 관련 정보를 담은 클래스
	/// </summary>
	public class SocketEventArgs : EventArgs
	{
		private Socket _sock = null;
		private IPEndPoint _endpoint = null;
		private String _recvdata = "";

		/// <summary>
		/// 소켓을 가져온다.
		/// </summary>
		public Socket TargetSocket
		{
			get { return _sock; }
		}

		/// <summary>
		/// IP Address를 담은 End Point를 가져온다.
		/// </summary>
		public IPEndPoint IPEndpoint
		{
			get { return _endpoint; }
		}

		/// <summary>
		/// 받은 데이터 문자열을 가져온다.
		/// </summary>
		public String ReceivedData
		{
			get { return _recvdata; }
		}

		/// <summary>
		/// 생성자
		/// </summary>
		/// <param name="target">소켓</param>
		/// <param name="ep">전달할 IP Address를 담은 Endpoint</param>
		/// <param name="recvData">받은 데이터 문자열</param>
		public SocketEventArgs(Socket target, IPEndPoint ep, String recvData)
		{
			this._sock = target;
			this._endpoint = ep;
			this._recvdata = recvData;
		}
	}

	/// <summary>
	/// 클라이언트를 위한 브로드케스트 오브젝트 클래스
	/// </summary>
	public class BroadcastClient : IBroadcastObject, IDisposable
	{
		private static Logger logger = LogManager.GetCurrentClassLogger();

		bool bStopFlag = false;
		const int RETRY_CNT_WHEN_BROADCAST = 5;

		/// <summary>
		/// 서버 포트
		/// </summary>
		const int UDP_SERVER_PORT = 888;
		
		/// <summary>
		/// 클라이언트 포트
		/// </summary>
		const int UDP_CLIENT_PORT = 889;

		BackgroundWorker bwRecv = null;
		BackgroundWorker bwSend = null;

		/// <summary>
		/// 데이터를 받은 경우 발생하는 이벤트 
		/// </summary>
		public event SocketEventHandler RecvMessage = null;
		/// <summary>
		/// 브로드캐스트가 시작할때 발생하는 이벤트
		/// </summary>
		public event EventHandler BeginBroadCast = null;
		/// <summary>
		/// 브로드캐스트가 끝날때 발생하는 이벤트
		/// </summary>
		public event EventHandler EndBroadCast = null;

		/// <summary>
		/// 초기화 (주고 받을 백그라운드 워커 스레드를 생성)
		/// </summary>
		public void Initialiaze()
		{
			bStopFlag = false;

			if (bwRecv == null)
			{
				bwRecv = new BackgroundWorker();
				bwRecv.DoWork +=new DoWorkEventHandler(bwRecv_DoWork);
				bwRecv.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bwRecv_RunWorkerCompleted);
				bwRecv.RunWorkerAsync();
			}

			if(bwSend == null)
			{
				bwSend = new BackgroundWorker();
				bwSend.DoWork += new DoWorkEventHandler(bwSend_DoWork);
				bwSend.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bwSend_RunWorkerCompleted);
			}
		}

		/// <summary>
		/// 보내는 스레드가 완료될 시 호출될 이벤트
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		void bwSend_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			if (EndBroadCast != null) EndBroadCast(this, null);
		}

		/// <summary>
		/// 보내는 스레드 함수
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		void bwSend_DoWork(object sender, DoWorkEventArgs e)
		{
			try
			{
				if (BeginBroadCast != null)	BeginBroadCast(this, null);

				byte[] data = Encoding.ASCII.GetBytes(System.Environment.MachineName);

				for (int i = 0; i < RETRY_CNT_WHEN_BROADCAST; ++i)
				{
					if (bStopFlag) break;

					Socket sock = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
					sock.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Broadcast, 1);
					IPEndPoint iep = new IPEndPoint(IPAddress.Broadcast, UDP_SERVER_PORT);
					sock.SendTo(data, iep);
					sock.Close();

					System.Threading.Thread.Sleep(1000);
				}
			}
			catch (SocketException sockEx)
			{
				switch (sockEx.SocketErrorCode)
				{
					case SocketError.TimedOut:
						break;
					default:
						logger.Error(String.Format("Socket Errorcode: {0}, Message: {1}", sockEx.SocketErrorCode.ToString(), sockEx.Message));
						break;
				}
			}
			catch (Exception ex) { logger.Error(ex.Message); }
		}

		/// <summary>
		/// 받는 스레드가 완료될 시 호출되는 이벤트
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		void bwRecv_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			bwRecv.Dispose();
			bwRecv = null;
		}

		/// <summary>
		/// 받는 스레드 함수
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		void bwRecv_DoWork(object sender, DoWorkEventArgs e)
		{
			try
			{
				Socket recvSock = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
				IPEndPoint endPoint = new IPEndPoint(IPAddress.Any, UDP_CLIENT_PORT);

				recvSock.Bind(endPoint);
				recvSock.ReceiveTimeout = 5000;

				byte[] data = new byte[100];

				while (!bStopFlag)
				{
					try
					{
						EndPoint ep = endPoint as EndPoint;
						int nRecv = recvSock.ReceiveFrom(data, ref ep);

						if (nRecv > 0)
						{
							string sRecvedData = Encoding.ASCII.GetString(data, 0, nRecv);

							if (RecvMessage != null) RecvMessage(this, new SocketEventArgs(recvSock, ep as IPEndPoint, sRecvedData));

							logger.Info(String.Format("UDP-Recv IP: {0}, Data: {1}", ((IPEndPoint)ep).Address.ToString(), sRecvedData));
						}
					}
					catch (SocketException sockEx) 
					{
						switch (sockEx.SocketErrorCode)
						{
							case SocketError.TimedOut:
								continue;
							default:
								logger.Error(String.Format("Socket Errorcode: {0}, Message: {1}", sockEx.SocketErrorCode.ToString(), sockEx.Message));
								break;
						}
					}
					catch (Exception ex) { logger.Error(ex.Message); }
				}
				recvSock.Close();

			}
			catch (System.Exception err)
			{
				logger.Error(err.Message);
			}
		}

		/// <summary>
		/// 브로드캐스트 호출
		/// </summary>
		public void BroadCast()
		{
			if(bwSend != null)
			{
				if (!bwSend.IsBusy) bwSend.RunWorkerAsync();
			}
		}

		/// <summary>
		/// 해제 작업
		/// </summary>
		public void Uninitialize()
		{
			bStopFlag = true;
		}

		/// <summary>
		/// Dispose 오버라이드
		/// </summary>
		public void Dispose()
		{
			Uninitialize();
		}
	}

	/// <summary>
	/// 서버를 위한 브로드캐스트 오브젝트 클래스
	/// </summary>
	public class BroadcastServer : IBroadcastObject, IDisposable
	{
		private static Logger logger = LogManager.GetCurrentClassLogger();

		const int UDP_SERVER_PORT = 888;
		const int UDP_CLIENT_PORT = 889;

		BackgroundWorker bwRecv = null;

		bool bStopFlag = false;

		string server_initial_code;

		
		/// <summary>
		/// 데이터를 받은 경우 발생하는 이벤트
		/// </summary>
		public event SocketEventHandler RecvMessage = null;
		/// <summary>
		/// 서버가 시작할때 발생하는 이벤트
		/// </summary>
		public event EventHandler BeginServer = null;
		/// <summary>
		/// 서버가 끝날때 발생하는 이벤트
		/// </summary>
		public event EventHandler EndServer = null;

		/// <summary>
		/// 초기화 (받을 백그라운드 워커 스레드를 생성)
		/// </summary>
		public void Initialiaze()
		{
			bStopFlag = false;

			if (bwRecv == null)
			{
				bwRecv = new BackgroundWorker();
				bwRecv.DoWork += new DoWorkEventHandler(bwRecv_DoWork);
				bwRecv.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bwRecv_RunWorkerCompleted);
				bwRecv.RunWorkerAsync();
			}

		}

		public void Initialiaze(string initialCode)
		{
			server_initial_code = initialCode;
			Initialiaze();
		}


		/// <summary>
		/// 받는 스레드가 완료될 시 호출되는 이벤트
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		void bwRecv_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			bwRecv.Dispose();
			bwRecv = null;

			if (EndServer != null) EndServer(this, null);
		}

		/// <summary>
		/// 호스트 이름 정보를 서버에서 클라이언트로 전달.
		/// </summary>
		/// <param name="targetAddress">클라이언트 IP</param>
		public void Send(IPAddress targetAddress)
		{
			try
			{
				Socket sendSock = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
				sendSock.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, 1);
				IPEndPoint sendIep = new IPEndPoint(targetAddress, UDP_CLIENT_PORT);

				string sReturn = String.Format("{0}|{1}", System.Environment.MachineName, server_initial_code);

				byte[] sendData = Encoding.ASCII.GetBytes(sReturn);
				sendSock.SendTo(sendData, sendIep);
				sendSock.Close();

				logger.Info("Message Sent: " + sReturn);
			}
			catch (SocketException sockEx)
			{
				switch (sockEx.SocketErrorCode)
				{
					case SocketError.TimedOut:
						break;
					default:
						logger.Error(String.Format("Socket Errorcode: {0}, Message: {1}", sockEx.SocketErrorCode.ToString(), sockEx.Message));
						break;
				}
			}
			catch (Exception ex) { logger.Error(ex.Message); }
		}

		/// <summary>
		/// 받는 스레드 함수
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		void bwRecv_DoWork(object sender, DoWorkEventArgs e)
		{
			try
			{
				if (BeginServer != null) BeginServer(this, null);

				Socket recvSock = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
				IPEndPoint endPoint = new IPEndPoint(IPAddress.Any, UDP_SERVER_PORT);

				recvSock.Bind(endPoint);
				recvSock.ReceiveTimeout = 5000;

				byte[] data = new byte[100];

				while(!bStopFlag)
				{
					try
					{
						EndPoint ep = endPoint as EndPoint;
						int nRecv = recvSock.ReceiveFrom(data, ref ep);

						if (nRecv > 0)
						{
							string sRecvedData = Encoding.ASCII.GetString(data, 0, nRecv);

							logger.Info(String.Format("UDP-Recv IP: {0}, Data: {1}", endPoint.Address.ToString(), sRecvedData));

							if (RecvMessage != null) RecvMessage(this, new SocketEventArgs(recvSock, ep as IPEndPoint, sRecvedData));

							Send(((IPEndPoint)ep).Address);
						}
					}
					catch (SocketException sockEx)
					{
						switch (sockEx.SocketErrorCode)
						{
							case SocketError.TimedOut:
								continue;
							default:
								logger.Error(String.Format("Socket Errorcode: {0}, Message: {1}", sockEx.SocketErrorCode.ToString(), sockEx.Message));
								break;
						}
					}
					catch (Exception ex) { logger.Error(ex.Message); }

				}
				recvSock.Close();

			}
			catch (System.Exception err)
			{
				logger.Error(err.Message);
			}
		}

		/// <summary>
		/// 해제 작업
		/// </summary>
		public void Uninitialize()
		{
			bStopFlag = true;
		}

		/// <summary>
		/// Dispose 오버라이드
		/// </summary>
		public void Dispose()
		{
			Uninitialize();
		}
	}
}
