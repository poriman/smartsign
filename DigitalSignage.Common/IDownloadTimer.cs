﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DigitalSignage.ServerDatabase;

namespace DigitalSignage.Common
{
	public interface IDownloadTimer
    {
		ServerDataSet.downloadtimerDataTable GetDataByPID(string pid);
		ServerDataSet.downloadtimerDataTable GetDataByParents(string gid);
		ServerDataSet.downloadtimerDataTable GetDataByGIDorPID(string pid, string gid);

		int InsertDownloadTime(string pid, string start, string end);
		int UpdateDownloadTime(int id, string pid, string start, string end);
		int DeleteTimesFromPID(string pid);
    }
}
