﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

using DigitalSignage.ServerDatabase;

using NLog;

namespace DigitalSignage.Common
{
    // task description class
	/// <summary>
	/// 스케줄 데이터에 대한 Wrapping 클래스
	/// </summary>
    [Serializable]
    public class Task
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

		/// <summary>
		/// 플레이어 사용: 동기화 스케줄의 uuid
		/// </summary>
        public static Guid TaskSynchronize = new Guid("3d9f7109-6012-447b-aef7-d27abcc9252c");
		/// <summary>
		/// 플레이어 사용: 스토리지 정리에 대한 스케줄의 uuid
		/// </summary>
		public static Guid TaskClean = new Guid("011EE3AE-13C7-49cf-AC80-E22FCAEFB61F");
		/// <summary>
		/// 플레이어 사용: 플레이어 초기화에 대한 스케줄의 uuid
		/// </summary>
		public static Guid TaskInitPlayer = new Guid("9dbf5729-69ab-4ab1-bf65-242d800d76fc");
		/// <summary>
		/// 플레이어 사용: 플레이어의 상태에 관한 반환 정보에 대한 스케줄의 uuid
		/// </summary>
		public static Guid TaskReturnStatus = new Guid("cedf49f4-7c2d-4064-a2d0-4307ad245f21");
		/// <summary>
		/// 플레이어 사용: 반복 스케줄에 대한 스케줄의 uuid 
		/// </summary>
		public static Guid TaskDefaultScreen = new Guid("dd4018cd-e598-4f8d-ad02-0c574c817672");
		/// <summary>
		/// 플레이어 사용: 외부 데이터 연동 갱신에 관한 스케줄의 uuid
		/// </summary>
		public static Guid TaskRefData = new Guid("14B68BA3-721A-4429-AA28-F0FD1A8C4CB1");

		public long etime;
		/// <summary>
		/// 스케줄 타입
		/// </summary>
        public TaskCommands type;
		/// <summary>
		/// 플레이어 아이디
		/// </summary>
        public string pid;
		/// <summary>
		/// 그룹 아이디
		/// </summary>
		public string gid;
		/// <summary>
		/// 스케줄 고유 식별 아이디 (PK)
		/// </summary>
        public System.Guid uuid;
		/// <summary>
		/// 그룹 식별 아이디로써, 반복 기능(매일 반복 등)에 대해 여러 개로 나뉘어진 스케줄을 하나의 스케줄로 인식하기 위해 필요한 그룹 고유 식별 아이디
		/// </summary>
        public System.Guid guuid;
		/// <summary>
		/// 다운로드 디렉토리 고유 식별 아이디
		/// </summary>
        public System.Guid duuid;
		/// <summary>
		/// 스케줄의 유효 시작 시간 (UTP: 세계 표준시)
		/// </summary>
        public long TaskStart;
		/// <summary>
		/// 스케줄의 유효 끝 시간 (UTP: 세계 표준시)
		/// </summary>
		public long TaskEnd;
		/// <summary>
		/// 스케줄 상태
		/// </summary>
        public TaskState state;
		/// <summary>
		/// 스케줄 이름
		/// </summary>
        public string description = "Unnamed Task";
		/// <summary>
		/// 스케줄의 재생 시간
		/// </summary>
		public long duration;
		/// <summary>
		/// ※주의!! 스케줄을 표출할 요일지정 사용하지 않으니 반드시 127을 넣도록하자.
		/// </summary>
		public int daysofweek;
		/// <summary>
		/// Reserved
		/// </summary>
		public int starttimeofday;
		/// <summary>
		/// Reserved
		/// </summary>
		public int endtimeofday;
		/// <summary>
		/// 스케줄의 우선순위로써, 큰 수일수록 우선 순위가 높다. 
		/// </summary>
		public int priority;

		/// <summary>
		/// 현재 유저가 관리할 퍼미션이 있는경우 True, 없는 경우 False
		/// </summary>
		public bool is_accessable = true;

		#region 광고 관련 추가

		/// <summary>
		/// 광고 여부
		/// </summary>
		public bool isAd = false;

		/// <summary>
		/// 표출 플래그
		/// </summary>
		public int show_flag = (int)ShowFlag.Show_NonEvent;

		/// <summary>
		/// 메타 태그 정보
		/// </summary>
		public string MetaTags = String.Empty;
	
		#endregion

		/// <summary>
		///	생성자
		/// </summary>
        public Task()
        {
        }

		/// <summary>
		/// 요일 체크
		/// </summary>
		/// <param name="dayofweek"></param>
		/// <param name="selectedDayOfWeek"></param>
		/// <returns></returns>
		private bool CheckDayOfWeek(int dayofweek, DayOfWeek selectedDayOfWeek)
		{
			DayOfWeek dow = selectedDayOfWeek;
			if (dow == DayOfWeek.Monday)
				return 0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_MON);
			else if (dow == DayOfWeek.Tuesday)
				return 0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_TUE);
			else if (dow == DayOfWeek.Wednesday)
				return 0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_WED);
			else if (dow == DayOfWeek.Thursday)
				return 0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_THU);
			else if (dow == DayOfWeek.Friday)
				return 0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_FRI);
			else if (dow == DayOfWeek.Saturday)
				return 0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_SAT);
			else if (dow == DayOfWeek.Sunday)
				return 0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_SUN);

			return false;
		}

		/// <summary>
		/// 중복이 허용되는지
		/// </summary>
		public bool AllowDuplication
		{
			get
			{
				return ((show_flag & (int)ShowFlag.Show_Allow_Duplication) == (int)ShowFlag.Show_Allow_Duplication);
			}
		}

		/// <summary>
		/// 볼륨 제어 스케줄인지 ??
		/// </summary>
		/// <returns></returns>
		public bool IsVolumeControlTask
		{
			get
			{
				try
				{
					if (type != TaskCommands.CmdControlSchedule)
						return false;

					//	볼륨 컨트롤임
					return description.StartsWith("CtrlVolume [");
				}
				catch { return false; }
			}
		}

		/// <summary>
		/// 시간대 클래스
		/// </summary>
		public class TimeScope
		{
			/// <summary>
			/// 시작 시간
			/// </summary>
			public long Start;
			/// <summary>
			/// 끝 시간
			/// </summary>
			public long End;

			/// <summary>
			/// 생성자
			/// </summary>
			/// <param name="start"></param>
			/// <param name="end"></param>
			public TimeScope(long start, long end)
			{
				Start = start;
				End = end;
			}
		};

		/// <summary>
		/// 생성자, task의 Row 정보를 토대로 스케줄 클래스를 구성한다.
		/// </summary>
		/// <param name="row">스케줄의 서버 DataSet 정보</param>
		public Task(ServerDataSet.tasksRow row)
		{
			this.type = (TaskCommands)row.type;
			this.pid = row.pid == null ? null : row.pid.Trim();
            this.gid = row.gid == null ? null : row.gid.Trim();
			this.uuid = new Guid(row.uuid);
			this.TaskStart = row.starttime;
			this.TaskEnd = row.endtime;
			this.state = (TaskState)row.state;
			logger.Info("New task init " + row.guuid + ":" + row.duuid);
			if (!row.guuid.Equals(""))
				this.guuid = new Guid(row.guuid);
			this.duuid = new Guid(row.duuid);
			this.description = row.name;
			this.duration = row.duration;
			this.daysofweek = row.daysofweek;
			this.starttimeofday = row.starttimeofday;
			this.endtimeofday = row.endtimeofday;
			this.priority = row.priority;

			this.isAd = row.is_ad == "Y";
			this.MetaTags = row.meta_tag;
			this.show_flag = row.show_flag;

			this.etime = row.etime;

		}

		/// <summary>
		/// Logging을 위한 스케줄 메타데이터 정보 반환
		/// </summary>
		/// <returns></returns>
        public new String ToString()
        {
			return "Name=\"" + description + "\" Player=" + pid + "\" Group=" + gid + "\ntask id=" +
				uuid.ToString() + " Start=" + TaskStart + " End=" + TaskEnd +
				" State=" + (int)state + " duuid=" + duuid.ToString() + " duration=" + duration.ToString() + " is Ad=" + isAd.ToString() + " MetaTags=" + MetaTags + " Show_Flag=" + show_flag.ToString();
// 			return "Name=\"" + description + "\"\r\nPlayer=" + pid + "\"\r\nGroup=\"" + gid + "\"\r\nStart=" + TimeConverter.ConvertFromUTP(TaskStart) + "\r\nEnd=" + TimeConverter.ConvertFromUTP(TaskEnd);
        }

		/// <summary>
		/// 스케줄 클래스를 토대로 서버 task의 Row 정보를 구성한다.
		/// </summary>
		/// <param name="result">tasksRow 정보 반환</param>
		public void ToRow(ref ServerDataSet.tasksRow result)
		{
			result.duuid = duuid.ToString();
			result.endtime = TaskEnd;
			result.guuid = guuid.ToString();
			result.pid = pid == null ? null : pid.Trim();
            result.gid = gid == null ? null : gid.Trim();
			result.starttime = TaskStart;
			result.state = (int)state;
			result.type = (int)type;
			result.uuid = uuid.ToString();
			result.duration = (int)duration;
			result.daysofweek = daysofweek;
			result.priority = priority;
			result.starttimeofday = starttimeofday;
			result.endtimeofday = endtimeofday;
			result.is_ad = isAd ? "Y" : "N";
			result.show_flag = show_flag;
			result.meta_tag = MetaTags;

			result.etime = this.etime;
		}

		/// <summary>
		/// 중복된 시간을 반환
		/// </summary>
		/// <param name="desc"></param>
		/// <returns></returns>
		public TimeScope GetDuplicationTime(Task desc)
		{
			if (this.type != desc.type) return null;

			//	원본이 반복 패턴이 있는지 검사
			if (this.starttimeofday != this.endtimeofday)
			{
				//	대상이 반복 패턴이 있는지 검사
				if (desc.starttimeofday != desc.endtimeofday)
				{
					//	요일 중복 검색
					if ((this.daysofweek & desc.daysofweek) > 0)
					{
						// 요일이 중복된다면 시간대 검색
						if ((this.starttimeofday == desc.starttimeofday && this.endtimeofday == desc.endtimeofday) ||
							(this.starttimeofday < desc.starttimeofday && this.endtimeofday > desc.starttimeofday) ||
							(this.starttimeofday < desc.endtimeofday && this.endtimeofday > desc.endtimeofday) ||
							(desc.starttimeofday < this.starttimeofday && desc.endtimeofday > this.starttimeofday) ||
							(desc.starttimeofday < this.starttimeofday && desc.endtimeofday > this.starttimeofday))
						{
							//	시간대가 중복된다면
							return new TimeScope(Math.Max(this.starttimeofday, desc.starttimeofday), Math.Min(this.endtimeofday, desc.endtimeofday));
						}
						else return null;
					}
					else return null;
				}
				else
				{
					//	대상이 반복 패턴이 없는 경우
					DateTime dtStart = TimeConverter.ConvertFromUTP(desc.TaskStart).ToLocalTime();
					DateTime dtEnd = TimeConverter.ConvertFromUTP(desc.TaskEnd).ToLocalTime();
					//aaa더해야해
					DateTime dtthisStart = TimeConverter.ConvertFromUTP(this.TaskStart).ToLocalTime();
					DateTime dtthisEnd = TimeConverter.ConvertFromUTP(this.TaskEnd).ToLocalTime();

					dtthisStart = new DateTime(dtthisStart.Year, dtthisStart.Month, dtthisStart.Day) + TimeSpan.FromSeconds(this.starttimeofday);
					dtthisEnd = new DateTime(dtthisEnd.Year, dtthisEnd.Month, dtthisEnd.Day) + TimeSpan.FromSeconds(this.endtimeofday);

					DateTime start = dtthisStart;
					DateTime end = new DateTime(dtthisStart.Year, dtthisStart.Month, dtthisStart.Day) + TimeSpan.FromSeconds(this.endtimeofday);

					while (start <= dtthisEnd)
					{
						if ((this.daysofweek & (int)start.DayOfWeek) == 0)
						{
							start += TimeSpan.FromDays(1);
							end += TimeSpan.FromDays(1);

							continue;
						}

						if ((start == dtStart && end == dtEnd) ||
							(start < dtStart && dtStart < end) || (start < dtEnd && dtEnd < end) ||
							(dtStart < start && start < dtEnd) || (dtStart < end && end < dtEnd))
						{
							return new TimeScope(Math.Max(TimeConverter.ConvertToUTP(start), TimeConverter.ConvertToUTP(dtStart)), Math.Min(TimeConverter.ConvertToUTP(end), TimeConverter.ConvertToUTP(dtEnd)));
						}
						start += TimeSpan.FromDays(1);
						end += TimeSpan.FromDays(1);
					}

					return null;
				}
			}
			else
			{
				//	반복 패턴이 없다면
				//	대상이 반복 패턴이 있는지 검사
				if (desc.starttimeofday != desc.endtimeofday)
				{
					//	원본이 반복 패턴이 없고 대상이 반복패턴이 있는경우
					DateTime dtStart = TimeConverter.ConvertFromUTP(this.TaskStart).ToLocalTime();
					DateTime dtEnd = TimeConverter.ConvertFromUTP(this.TaskEnd).ToLocalTime();
					DateTime dtDescStart = TimeConverter.ConvertFromUTP(desc.TaskStart).ToLocalTime();
					DateTime dtDescEnd = TimeConverter.ConvertFromUTP(desc.TaskEnd).ToLocalTime();

					dtDescStart = new DateTime(dtDescStart.Year, dtDescStart.Month, dtDescStart.Day) + TimeSpan.FromSeconds(desc.starttimeofday);
					dtDescEnd = new DateTime(dtDescEnd.Year, dtDescEnd.Month, dtDescEnd.Day) + TimeSpan.FromSeconds(desc.endtimeofday);

					DateTime start = dtDescStart;
					DateTime end = new DateTime(dtDescStart.Year, dtDescStart.Month, dtDescStart.Day) + TimeSpan.FromSeconds(desc.endtimeofday);

					while (start <= dtDescEnd)
					{
						if ((desc.daysofweek & (int)start.DayOfWeek) == 0)
						{
							start += TimeSpan.FromDays(1);
							end += TimeSpan.FromDays(1);

							continue;
						}

						if ((start == dtStart && dtEnd == end) ||
							(start < dtStart && dtStart < end) || (start < dtEnd && dtEnd < end) ||
							(dtStart < start && start < dtEnd) || (dtStart < end && end < dtEnd))
						{
							return new TimeScope(Math.Max(TimeConverter.ConvertToUTP(start), TimeConverter.ConvertToUTP(dtStart)), Math.Min(TimeConverter.ConvertToUTP(end), TimeConverter.ConvertToUTP(dtEnd)));
						}
						start += TimeSpan.FromDays(1);
						end += TimeSpan.FromDays(1);
					}

					return null;
				}
				else
				{
					if ((this.TaskStart == desc.TaskStart && this.TaskEnd == desc.TaskEnd) ||
						(this.TaskStart < desc.TaskStart && desc.TaskStart < this.TaskEnd) || (this.TaskStart < desc.TaskEnd && desc.TaskEnd < this.TaskEnd) ||
					(desc.TaskStart < this.TaskStart && this.TaskStart < desc.TaskEnd) || (desc.TaskStart < this.TaskEnd && this.TaskEnd < desc.TaskEnd))
					{
						return new TimeScope(Math.Max(this.TaskStart, desc.TaskStart), Math.Min(this.TaskEnd, desc.TaskEnd));
					}
					return null;
				}
			}
		}

		public bool HasDuplicationTime(Task desc)
		{
			if (this.type != desc.type) return false;

			//	원본이 반복 패턴이 있는지 검사
			if (this.starttimeofday != this.endtimeofday)
			{
				//	대상이 반복 패턴이 있는지 검사
				if (desc.starttimeofday != desc.endtimeofday)
				{
					//	요일 중복 검색
					if ((this.daysofweek & desc.daysofweek) > 0)
					{
						// 요일이 중복된다면 시간대 검색
						if ((this.starttimeofday == desc.starttimeofday && this.endtimeofday == desc.endtimeofday) || 
							(this.starttimeofday <= desc.starttimeofday && this.endtimeofday > desc.starttimeofday) ||
							(this.starttimeofday < desc.endtimeofday && this.endtimeofday >= desc.endtimeofday) ||
							(desc.starttimeofday <= this.starttimeofday && desc.endtimeofday > this.starttimeofday) ||
							(desc.starttimeofday < this.endtimeofday && desc.endtimeofday >= this.endtimeofday))
						{
							//	시간대가 중복된다면
							return true;
						}
						else return false;
					}
					else return false;
				}
				else
				{
					//	대상이 반복 패턴이 없는 경우
					DateTime dtStart = TimeConverter.ConvertFromUTP(desc.TaskStart).ToLocalTime();
					DateTime dtEnd = TimeConverter.ConvertFromUTP(desc.TaskEnd).ToLocalTime();
					//aaa더해야해
					DateTime dtthisStart = TimeConverter.ConvertFromUTP(this.TaskStart).ToLocalTime();
					DateTime dtthisEnd = TimeConverter.ConvertFromUTP(this.TaskEnd).ToLocalTime();

					dtthisStart = new DateTime(dtthisStart.Year, dtthisStart.Month, dtthisStart.Day) + TimeSpan.FromSeconds(this.starttimeofday);
					dtthisEnd = new DateTime(dtthisEnd.Year, dtthisEnd.Month, dtthisEnd.Day) + TimeSpan.FromSeconds(this.endtimeofday);

					DateTime start = dtthisStart;
					DateTime end = new DateTime(dtthisStart.Year, dtthisStart.Month, dtthisStart.Day) + TimeSpan.FromSeconds(this.endtimeofday);

					while (start <= dtthisEnd)
					{
						if (CheckDayOfWeek(this.daysofweek, start.DayOfWeek) == false)
						{
							start += TimeSpan.FromDays(1);
							end += TimeSpan.FromDays(1);

							continue;
						}

						if ((start == dtStart && end == dtEnd) || 
							(start <= dtStart && dtStart < end) || (start < dtEnd && dtEnd <= end) ||
							(dtStart <= start && start < dtEnd) || (dtStart < end && end <= dtEnd))
						{
							return true;
						}
						start += TimeSpan.FromDays(1);
						end += TimeSpan.FromDays(1);
					}

					return false;
				}
			}
			else
			{
				//	반복 패턴이 없다면
				//	대상이 반복 패턴이 있는지 검사
				if (desc.starttimeofday != desc.endtimeofday)
				{
					//	원본이 반복 패턴이 없고 대상이 반복패턴이 있는경우
					DateTime dtStart = TimeConverter.ConvertFromUTP(this.TaskStart).ToLocalTime();
					DateTime dtEnd = TimeConverter.ConvertFromUTP(this.TaskEnd).ToLocalTime();
					DateTime dtDescStart = TimeConverter.ConvertFromUTP(desc.TaskStart).ToLocalTime();
					DateTime dtDescEnd = TimeConverter.ConvertFromUTP(desc.TaskEnd).ToLocalTime();

					dtDescStart = new DateTime(dtDescStart.Year, dtDescStart.Month, dtDescStart.Day) + TimeSpan.FromSeconds(desc.starttimeofday);
					dtDescEnd = new DateTime(dtDescEnd.Year, dtDescEnd.Month, dtDescEnd.Day) + TimeSpan.FromSeconds(desc.endtimeofday);

					DateTime start = dtDescStart;
					DateTime end = new DateTime(dtDescStart.Year, dtDescStart.Month, dtDescStart.Day) + TimeSpan.FromSeconds(desc.endtimeofday);

					while (start <= dtDescEnd)
					{
						if (CheckDayOfWeek(desc.daysofweek, start.DayOfWeek) == false)
						{
							start += TimeSpan.FromDays(1);
							end += TimeSpan.FromDays(1);

							continue;
						}

						if ((start == dtStart && dtEnd == end) ||
							(start <= dtStart && dtStart < end) || (start < dtEnd && dtEnd <= end) ||
							(dtStart <= start && start < dtEnd) || (dtStart < end && end <= dtEnd))
						{
							return true;
						}
						start += TimeSpan.FromDays(1);
						end += TimeSpan.FromDays(1);
					}

					return false;
				}
				else
				{
					if ((this.TaskStart == desc.TaskStart && this.TaskEnd == desc.TaskEnd) || 
						(this.TaskStart <= desc.TaskStart && desc.TaskStart < this.TaskEnd) || (this.TaskStart < desc.TaskEnd && desc.TaskEnd <= this.TaskEnd) ||
					(desc.TaskStart <= this.TaskStart && this.TaskStart < desc.TaskEnd) || (desc.TaskStart < this.TaskEnd && this.TaskEnd <= desc.TaskEnd))
					{
						return true;
					}
					return false;
				}
			}
		}

		/// <summary>
		/// 중복된 시간이 있는지 검사
		/// </summary>
		/// <param name="desc"></param>
		/// <param name="inDay"></param>
		/// <returns>중복이없다면 false</returns>
		public bool HasDuplicationTime(Task desc, DateTime inDay)
		{
			if (this.type != desc.type) return false;

			//	원본이 반복 패턴이 있는지 검사
			if (this.starttimeofday != this.endtimeofday)
			{
				//	대상이 반복 패턴이 있는지 검사
				if (desc.starttimeofday != desc.endtimeofday)
				{
					//	요일 중복 검색
					if ((this.daysofweek & desc.daysofweek) > 0)
					{
						// 요일이 중복된다면 시간대 검색
						if ((this.starttimeofday == desc.starttimeofday && this.endtimeofday == desc.endtimeofday) || 
							(this.starttimeofday <= desc.starttimeofday && this.endtimeofday > desc.starttimeofday) ||
							(this.starttimeofday < desc.endtimeofday && this.endtimeofday >= desc.endtimeofday) ||
							(desc.starttimeofday <= this.starttimeofday && desc.endtimeofday > this.starttimeofday) ||
							(desc.starttimeofday < this.endtimeofday && desc.endtimeofday >= this.endtimeofday))
						{
							//	시간대가 중복된다면
							return true;
						}
						else return false;
					}
					else return false;
				}
				else
				{
					//	대상이 반복 패턴이 없는 경우
					DateTime dtStart = TimeConverter.ConvertFromUTP(desc.TaskStart).ToLocalTime();
					DateTime dtEnd = TimeConverter.ConvertFromUTP(desc.TaskEnd).ToLocalTime();

					DateTime dtthisStart = new DateTime(inDay.Year, inDay.Month, inDay.Day) + TimeSpan.FromSeconds(this.starttimeofday);
					DateTime dtthisEnd = new DateTime(inDay.Year, inDay.Month, inDay.Day) + TimeSpan.FromSeconds(this.endtimeofday);

					if (CheckDayOfWeek(this.daysofweek, dtthisStart.DayOfWeek) == false)
					{
						return false;
					}

					if ((dtthisStart == dtStart && dtthisEnd == dtEnd) ||
						(dtthisStart <= dtStart && dtStart < dtthisEnd) || (dtthisStart < dtEnd && dtEnd <= dtthisEnd) ||
						(dtStart <= dtthisStart && dtthisStart < dtEnd) || (dtStart < dtthisEnd && dtthisEnd <= dtEnd))
					{
						return true;
					}

					return false;
				}
			}
			else
			{
				//	반복 패턴이 없다면
				//	대상이 반복 패턴이 있는지 검사
				if (desc.starttimeofday != desc.endtimeofday)
				{
					//	원본이 반복 패턴이 없고 대상이 반복패턴이 있는경우
					DateTime dtStart = TimeConverter.ConvertFromUTP(this.TaskStart).ToLocalTime();
					DateTime dtEnd = TimeConverter.ConvertFromUTP(this.TaskEnd).ToLocalTime();

					DateTime dtDescStart = new DateTime(inDay.Year, inDay.Month, inDay.Day) + TimeSpan.FromSeconds(desc.starttimeofday);
					DateTime dtDescEnd = new DateTime(inDay.Year, inDay.Month, inDay.Day) + TimeSpan.FromSeconds(desc.endtimeofday);

					if (CheckDayOfWeek(desc.daysofweek, dtDescStart.DayOfWeek) == false)
					{
						return false;
					}

					if ((dtDescStart == dtStart && dtEnd == dtDescEnd) ||
						(dtDescStart <= dtStart && dtStart < dtDescEnd) || (dtDescStart < dtEnd && dtEnd <= dtDescEnd) ||
						(dtStart <= dtDescStart && dtDescStart < dtEnd) || (dtStart < dtDescEnd && dtDescEnd <= dtEnd))
					{
						return true;
					}

					return false;
				}
				else
				{
					if ((this.TaskStart == desc.TaskStart && this.TaskEnd == desc.TaskEnd) || 
						(this.TaskStart <= desc.TaskStart && desc.TaskStart < this.TaskEnd) || (this.TaskStart < desc.TaskEnd && desc.TaskEnd <= this.TaskEnd) ||
					(desc.TaskStart <= this.TaskStart && this.TaskStart < desc.TaskEnd) || (desc.TaskStart < this.TaskEnd && this.TaskEnd <= desc.TaskEnd))
					{
						return true;
					}
					return false;
				}
			}
		}
    }
}