﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NLog;

namespace DigitalSignage.Common
{
	/// <summary>
	/// 싱글톤으로 제작된 iVision 로그 헬퍼
	/// </summary>
	public class iVisionLogHelper
	{
		#region == Variables ==
		private static Logger logger = LogManager.GetCurrentClassLogger();
	
		// singletone routines
		private static iVisionLogHelper instance = null;
		private static readonly object semaphore = new object();

		public TaskInfoLogCB GeneralLog = null;
		public TaskErrorLogCB GeneralLogError = null;

		public LogStartedCB ScreenLogStarted = null;
		public LogEndedCB ScreenLogEnded = null;
		public LogProcceedCB ScreenFinishied = null;
		public LogErrorCB ScreenLogError = null;

		public LogStartedCB SubtitleLogStarted = null;
		public LogEndedCB SubtitleLogEnded = null;
		public LogProcceedCB SubtitleFinishied = null;
		public LogErrorCB SubtitleLogError = null;

		#endregion

		#region == Properties ==
		
		public static iVisionLogHelper GetInstance
		{
			get
			{
				lock (semaphore)
				{
					if (instance == null)
					{
						instance = new iVisionLogHelper();
					}
					return instance;
				}
			}
		}
		#endregion

		#region == Functions ==
		public iVisionLogHelper()
		{
		}
		public void WriteInfoLog(string title, string uuid, TaskCommands type, String message)
		{
            if (GeneralLog != null) GeneralLog(title, uuid, type, message);
		}
        public void WriteErrorLog(string title, string uuid, TaskCommands type, ErrorCodes code, String message)
		{
			logger.Error(String.Format("iVisionException Name : {0}, Code : {1}, Description : {2}", title, code, message));
            if (GeneralLogError != null) GeneralLogError(title, uuid, type, code, message);
		}
		public void SubtitleStartLog(String name)
		{
			if (SubtitleLogStarted != null) SubtitleLogStarted(name);
		}
		public void SubtitleEndLog(String name)
		{
			if (SubtitleLogEnded != null) SubtitleLogEnded(name);
		}
		public void SubtitleErrorLog(String name, ErrorCodes code, String description)
		{
			logger.Error(String.Format("iVisionException Name : {0}, Code : {1}, Description : {2}", name, code, description));
			if (SubtitleLogError != null) SubtitleLogError(name, (int)code, description);
		}

		public void ScreenStartLog(String name)
		{
			if (ScreenLogStarted != null) ScreenLogStarted(name);
		}
		public void ScreenEndLog(String name)
		{
			if (ScreenLogEnded != null) ScreenLogEnded(name);
		}
		public void ScreenErrorLog(String name, ErrorCodes code, String description)
		{
			logger.Error(String.Format("iVisionException Name : {0}, Code : {1}, Description : {2}", name, code, description));
			if (ScreenLogError != null) ScreenLogError(name, (int)code, description);
		}
		#endregion
	};

	/// <summary>
	/// 로그기록시 SmartSign 관련 예외 클래스
	/// </summary>
	public class iVisionException : Exception
	{
		#region == Variables ==
		/// <summary>
		/// Error 코드 
		/// </summary>
		private ErrorCodes _code = ErrorCodes.IS_ERROR_BASE;
		private String _details = ""; 
		#endregion

		#region == Properties ==
		/// <summary>
		/// Error 코드
		/// </summary>
		public ErrorCodes ErrorCode
		{
			get { return _code; }
			set { _code = value; }
		}
		/// <summary>
		/// 상세 메시지
		/// </summary>
		public String DetailMessage
		{
			get { return _details; }
			set { _details = value; }
		}
		#endregion

		/// <summary>
		/// 생성자
		/// </summary>
		/// <param name="code">에러 코드</param>
		/// <param name="detailMessage">상세 메시지</param>
		public iVisionException(ErrorCodes code, String detailMessage)
		{
			ErrorCode = code;
			DetailMessage = detailMessage;
		}
	};

	/// <summary>
	/// 에러코드 (정리 필요, i-Frame의 내용을 고대로 복사했기때문에 별로 신뢰성이 있진않다.)
	/// </summary>
	public enum ErrorCodes
	{
		IS_ERROR_BASE = 0x00000000,
		IS_ERROR_INVALID_PSR_FILE_NAME	= IS_ERROR_BASE+1, //파일 이름 오류
		IS_ERROR_INVALID_PSR_OBJECT_NAME= IS_ERROR_BASE+2, // 객체 명 오류
		IS_ERROR_FATAL			= IS_ERROR_BASE+3, // 치명적 오류
		IS_ERROR_SYSTEM_ERROR		= IS_ERROR_BASE+4, // 시스템 오류
		IS_ERROR_INTERNAL		= IS_ERROR_BASE+5, // 내부 처리 오류
		IS_ERROR_OUT_OF_MEMORY		= IS_ERROR_BASE+6, // 메모리 생성 오류
		IS_ERROR_UNSUPPT_OSVERSION	= IS_ERROR_BASE+7, // 버전 실패
		IS_ERROR_VERSION_NOT_FOUND	= IS_ERROR_BASE+8, // 버전 조회 실패
		IS_ERROR_UNDEFINED		= IS_ERROR_BASE+10, // 정의 되지않는 요청
		IS_ERROR_ELEMENT_NOT_FOUND	= IS_ERROR_BASE+11, // 다운로드 엘리먼트 찾기 실패
		IS_ERROR_PATH_NOT_FOUND		= IS_ERROR_BASE+12, // 존재하지 않는 경로
		IS_ERROR_FILE_NOT_FOUND		= IS_ERROR_BASE+13, // 존재 하지 않는 파일
		IS_ERROR_CONTENT_NOT_FOUND	= IS_ERROR_BASE+14, // 존재 하지 않는 컨텐츠
		IS_ERROR_UNSUPPT_CONTENT	= IS_ERROR_BASE+15, // 지원 하지 않는 컨텐츠
		IS_ERROR_CONTENT_CRASHED	= IS_ERROR_BASE+16, // 손상된 컨텐츠
		IS_ERROR_INVALID_PROGRAM_ID	= IS_ERROR_BASE+17, // 프로그램 아이디 오류
		IS_ERROR_DATA_NOT_FOUND		= IS_ERROR_BASE+18, // 테이터 찾기 오류
		IS_ERROR_CODEC_NOT_FOUND	= IS_ERROR_BASE+19, // 코덱 찾기 실패
		IS_ERROR_CONTENT_EXIST		= IS_ERROR_BASE+20, // 존재 하지 않는 컨텐츠 다운로드 요청
		IS_ERROR_SCHE_NOT_FOUND		= IS_ERROR_BASE+21, //  스케쥴 파일 찾기 실패
		IS_ERROR_INVALID_SCHE_NAME	= IS_ERROR_BASE+22, // 스케쥴 파일명 오류
		IS_ERROR_DOWNELEMENT_NOT_FOUND	= IS_ERROR_BASE+23, // 다운로드 항목 찾기 실패
		IS_ERROR_CONTENT_NOT_INSERVER	= IS_ERROR_BASE+24, // 서버에 존재하지 않는 컨텐츠 요청
		IS_ERROR_UNSUPPT_COMMAND	= IS_ERROR_BASE+25, // 지원 하지않는 명령 실행
		IS_ERROR_THREAD_ALREADY_RUN	= IS_ERROR_BASE+26, // 쓰레드 생성중
		IS_ERROR_DOC_ALREADY_OPEN	= IS_ERROR_BASE+27, // 쓰레드 실행중
		IS_ERROR_TIMEOUT		= IS_ERROR_BASE+28, // 명령 실행에 대한 타임 아웃
		IS_ERROR_PROCESS_ALREADY_RUN	= IS_ERROR_BASE+29, // 실행중인 프로세스 재실행.
		IS_ERROR_TERMINATION		= IS_ERROR_BASE+30, // 프로세스 종료중
		IS_ERROR_COMMAND_UNDONE		= IS_ERROR_BASE+31, // 요청된 명령실행 미완료
		IS_ERROR_CODE_NOT_DEFINED	= IS_ERROR_BASE+32, // 정의 되지 않는 코드값 수신
		IS_ERROR_INVALID_PLAYLIST	= IS_ERROR_BASE+33, // 실행 할수 없는 task 실행 요청
		IS_ERROR_CONNECT_FAIL		= IS_ERROR_BASE+34, // 서버 연결 실패
		IS_ERROR_LINE_DISCONNECT	= IS_ERROR_BASE+35, // 네트워크 끊김
		IS_ERROR_SERVER_NOT_FOUND	= IS_ERROR_BASE+36, // 서버 아이피 또는 URL오류
		IS_ERROR_NO_MSG_DEFINE		= IS_ERROR_BASE+37, // 알수 없는 서버 수신 메시지
		IS_ERROR_CONN_SESSION_FAIL	= IS_ERROR_BASE+38, //서버 세션연결 실패 또는 연결 끊김
		IS_ERROR_CONNECT_UNINITIAL	= IS_ERROR_BASE+39, // 서버연결 초기화 하지 않은 상태에서 통신실행
		IS_ERROR_INVALID_AUTHORIZATION	= IS_ERROR_BASE+40, // 서버 인증 실패
		IS_ERROR_INVALID_SERVICE	= IS_ERROR_BASE+41, //알수없는 서비스 요청
		IS_ERROR_LOGIN_FAIL		= IS_ERROR_BASE+42, // 서버 로그인 실패
		IS_ERROR_INVALID_DST_PATH	= IS_ERROR_BASE+43, // 적용 패스 지정오류
		IS_ERROR_INVALID_DST_FILE	= IS_ERROR_BASE+44, // 적용 파일 지정 오류
		IS_STBLOG_STOP_BY_USER		= IS_ERROR_BASE+45, // 사용자 메뉴에 의한 프로그램 종료
		IS_STBLOG_STOP_BY_UNKNOWN_COMMAND= IS_ERROR_BASE+46, // 알수 없는 명령으로 프로그램 종료
		IS_STBLOG_STOP_ABNORMAL		= IS_ERROR_BASE+47, //  비정상적인 서브프로세스 종료
		IS_STBLOG_STOP_BY_AGENT = IS_ERROR_BASE + 48, // 응답이 없는 서브프로세스를 에이전트가 재시작 시킴
		IS_ERROR_FTP_UNKNOWN			= IS_ERROR_BASE+49, // 
		IS_ERROR_FTP_FAIL_TO_CONNECT	= IS_ERROR_BASE+50, // FTP에 접속에 실패 
		IS_ERROR_FTP_ALREADY_USE_BY_PROCESS		= IS_ERROR_BASE+51, // 이미 사용중인 프로세스를 다운로드 하려고 시도
		IS_ERROR_FTP_INVALID_FILE = IS_ERROR_BASE + 52, // 없는 FTP 파일을 요청함
		IS_ERROR_INVALID_TIME_SCOPE = IS_ERROR_BASE + 53,	//	시간범위가 지난 스케줄의 처리 요청

		IS_ERROR_CONN_NO_PLAYER = IS_ERROR_BASE + 1001,	//	플레이어 > 서버 접속 에러 : 플레이어가 존재하지 않습니다.
		IS_ERROR_CONN_DUPLICATED_PLAYER = IS_ERROR_BASE + 1002,	//	플레이어 > 서버 접속 에러 : 서버에게 중복된 플레이어 ID가 접속되었습니다.
		IS_ERROR_CONN_EXCESS_OF_ALLOWED_MAX_PLAYER = IS_ERROR_BASE + 1003,	//	플레이어 > 서버 접속 에러 : 플레이어가 존재하지 않습니다.
		IS_ERROR_CONN_DISCONNECTED = IS_ERROR_BASE + 1004,	//	플레이어 > 서버 접속 에러 : 플레이어에 의해 접속이 끊어졌습니다.
		IS_ERROR_CONN_TIMEOUT_DISCONNECTED_BY_SERVER = IS_ERROR_BASE + 1005,	//	플레이어 > 서버 접속 에러 : 플레이어가 응답하지 않아 서버에서 접속을 끊었습니다.

	};

    /// <summary>
    /// 타겟 광고 클라이언트 에러 코드 정의
    /// </summary>
    public enum AdTarget_Client_ErrorCodes : int
    {
        /// <summary>
        /// 일반 에러
        /// </summary>
        ErrorGeneral = 1,
        /// <summary>
        /// 서버와 접속 끊김
        /// </summary>
        ErrorDisconnected = ErrorGeneral + 1,
        /// <summary>
        /// 타임 아웃
        /// </summary>
        ErrorTimeout = ErrorGeneral + 2,
        /// <summary>
        /// 내부 에러
        /// </summary>
        ErrorInternal = ErrorGeneral + 3,
        /// <summary>
        /// PlayAgent와 Player 간 데이터 전달 에러 (닷넷리모팅)
        /// </summary>
        ErrorRemotingConnection = ErrorGeneral + 4,
        /// <summary>
        /// 정의되지 않은 형태의 데이터 수신 에러
        /// </summary>
        ErrorInvalidData = ErrorGeneral + 5,
    }

    /// <summary>
    /// 타겟 광고 DS 서버 에러 코드 정의
    /// </summary>
    public enum AdTarget_AppServer_ErrorCodes : int
    {
        /// <summary>
        /// 일반 에러
        /// </summary>
        ErrorGeneral = 1,
        /// <summary>
        /// 클라이언트 접속 끊김
        /// </summary>
        ErrorDisconnectedToClient = ErrorGeneral + 1,
        /// <summary>
        /// 릴레이 서버 접속 끊김
        /// </summary>
        ErrorDisconnectedToRelayServer = ErrorGeneral + 2,
        /// <summary>
        /// DB 서버 접속 끊김
        /// </summary>
        ErrorDisconnectedToDB = ErrorGeneral + 3,
        /// <summary>
        /// 타임 아웃
        /// </summary>
        ErrorTimeout = ErrorGeneral + 4,
        /// <summary>
        /// 내부 에러
        /// </summary>
        ErrorInternal = ErrorGeneral + 5,
        /// <summary>
        /// 데이버 분석 간 에러
        /// </summary>
        ErrorParsing = ErrorGeneral + 6,
        /// <summary>
        /// 정의되지 않은 형태의 데이터 수신 에러
        /// </summary>
        ErrorInvalidData = ErrorGeneral + 7,
    }
}