﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace DigitalSignage.SerialKey
{
	/// <summary>
	/// 서버에서 사용하는 시리얼키 관련 클래스, 제품 코드 정보를 위해 매니저나 플레이어에서도 사용함
	/// </summary>
	public class SerialKey
	{
		Byte[] arrKey = null;

		/// <summary>
		/// 제품 코드
		/// </summary>
		public enum _ProductCode
		{
			/// <summary>
			/// 임베디드 에디션
			/// </summary>
			EmbededEdition = 0,
			/// <summary>
			/// 스텐드어론 에디션
			/// </summary>
			StandaloneEdition,
			/// <summary>
			/// 네트워크 에디션
			/// </summary>
			NetworkEdition,
			/// <summary>
			/// 메뉴보드
			/// </summary>
			Menuboard,
			/// <summary>
			/// SaaS 에디션
			/// </summary>
			SoftwareAsaService,
			/// <summary>
			/// Debug 에디션
			/// </summary>
			DebugEdition,
            /// <summary>
            /// 특별 에디션
            /// </summary>
            SpecialEdition,
			/// <summary>
			/// 알수 없음
			/// </summary>
			Undefined
		};

		/// <summary>
		/// 제품 코드를 스트링 정보로 반환하는 스태틱 함수
		/// </summary>
		/// <param name="code">제품 코드 정보</param>
		/// <returns></returns>
		public static String ProductName(_ProductCode code)
		{
			switch (code) 
			{
				case _ProductCode.EmbededEdition:
					return "Embeded Edition";
				case _ProductCode.Menuboard:
					return "Menuboard";
				case _ProductCode.NetworkEdition:
					return "Network Edition";
				case _ProductCode.SoftwareAsaService:
					return "Software As a Service";
				case _ProductCode.StandaloneEdition:
					return "Standalone Edition";
				default:
					return "Undefinded";
			}
		}

		#region = 상태 관련 =
		const int SK_NOTHING = 0;
		const int SK_INITIALIZED = 1;
		const int SK_COMPLETED = 2;
		int nStatus = SK_NOTHING;

		bool IsInitialized { get { return nStatus > SK_NOTHING; } }

		#endregion

		/// <summary>
		/// ISerialKey의 내부 객체를 생성 및 초기화한다.
		/// </summary>
		/// <param name="nSizeOfKey">키의 Byte 개수.</param>
		public void Initialize(int nSizeOfKey)
		{
			nStatus = SK_INITIALIZED;

			arrKey = new Byte[nSizeOfKey + 1];
		}

		/// <summary>
		/// nIndex에 해당하는 위치 해당 Byte를 넣는다.
		/// </summary>
		/// <param name="nIndex">넣을 위치</param>
		protected void SetByte(int nIndex, Byte data)
		{
			arrKey[nIndex] = data;
		}

		protected void SetBytes(int nStartIndex, Byte[] arrData)
		{
			int nIndex = nStartIndex;
			foreach(byte data in arrData)
			{
				arrKey[nIndex++] = data;
			}
		}

		protected Byte GetByte(int nIndex)
		{
			return arrKey[nIndex];
		}

		protected Byte[] GetBytes(int nStartIndex, int nCount)
		{
			Byte[] arrBytes = new Byte[nCount];

			for(int i = 0 ; i < nCount ; ++i)
			{
				arrBytes[i] = arrKey[nStartIndex + i];
			}

			return arrBytes;
		}

		void MakeValidationCode()
		{
			Int64 i = 0;

			foreach(byte piece in arrKey)
			{
				i += Convert.ToInt64(piece);
			}

			arrKey[arrKey.Length - 1] = Convert.ToByte(i % 255);
		}

		bool IsValidCode()
		{
			Int64 result = 0;

			for (int i = 0; i < arrKey.Length - 1; ++i)
			{
				byte piece = arrKey[i];

				result += Convert.ToInt64(piece);
			}

			return arrKey[arrKey.Length - 1] == Convert.ToByte(result % 255);
		}

		/// <summary>
		/// Key 생성한다.
		/// </summary>
		/// <param name="uniqueKey">키와 조합될 문자열</param>
		/// <returns>생성된 Key를 반환한다.</returns>
		protected String MakeKey(String uniqueKey)
		{
			Encoding ascii = ASCIIEncoding.ASCII;

			MakeValidationCode();
			
			BitArray arrBits = new BitArray(arrKey);
			BitArray arrUnique = new BitArray(ascii.GetBytes(uniqueKey));

			BitArray newBits = arrBits.Xor(arrUnique);

			newBits.CopyTo(arrKey, 0);

			nStatus = SK_COMPLETED;

			return EncryptByte();
		}

		protected bool ParseKey(String serialKey, String uniqueKey)
		{
			Encoding ascii = ASCIIEncoding.ASCII;

			DecryptByte(serialKey);
			BitArray arrBits = new BitArray(arrKey);
			BitArray arrUnique = new BitArray(ascii.GetBytes(uniqueKey));

			BitArray newBits = arrUnique.Xor(arrBits);

			newBits.CopyTo(arrKey, 0);

			nStatus = SK_COMPLETED;

			return IsValidCode();
		}

		void Set5Bit(int nValue, ref BitArray arrBits, int startIndex)
		{
			try
			{
				arrBits[startIndex] = (nValue & 0x0001) == 0x0000 ? false : true;
				arrBits[startIndex + 1] = (nValue & 0x0002) == 0x0000 ? false : true;
				arrBits[startIndex + 2] = (nValue & 0x0004) == 0x0000 ? false : true;
				arrBits[startIndex + 3] = (nValue & 0x0008) == 0x0000 ? false : true;
				arrBits[startIndex + 4] = (nValue & 0x0010) == 0x0000 ? false : true;
			}
			catch {}
		}

		byte Get5Bit(int index)
		{
			int b = 0x00;

			BitArray arrBits = new BitArray(arrKey);
			
			int nStartIndex = index * 5;

			if (arrBits.Length <= nStartIndex)
				return (byte)0xFF;

			bool b1 = false;
			bool b2 = false;
			bool b3 = false;
			bool b4 = false;
			bool b5 = false;

			try { b1 = arrBits.Get(nStartIndex); }
			catch { b1 = false; }
			try { b2 = arrBits.Get(nStartIndex+1); }
			catch { b2 = false; }
			try { b3 = arrBits.Get(nStartIndex+2); }
			catch { b3 = false; }
			try { b4 = arrBits.Get(nStartIndex+3); }
			catch { b4 = false; }
			try { b5 = arrBits.Get(nStartIndex+4); }
			catch { b5 = false; }

			b = b | (b1 ? 0x01 : 0x00);
			b = b | ((b2 ? 0x01 : 0x00) << 1);
			b = b | ((b3 ? 0x01 : 0x00) << 2);
			b = b | ((b4 ? 0x01 : 0x00) << 3);
			b = b | ((b5 ? 0x01 : 0x00) << 4);

			return (byte)b;
		}
		
		void DecryptByte(String serialKey)
		{
			char[] arrChar = {'V', 'X', 'W', '1', 'A', '2', 'U', 'B', 'C', '3',
							   'S', '4', 'R', 'E', 'T', 'D', 'Z' ,'5' ,'Q', 'F',
							   '6', '8', 'Y', 'N', '7', 'G', 'O', 'M' ,'P' ,'H',
							   'K' ,'9'};
			int nMAXLenth = arrChar.Length;

			arrKey = new byte[Convert.ToInt32((serialKey.Length * 5/8))];
			
			int nLengthOfBits = (serialKey.Length * 5) - ((serialKey.Length * 5) % 8);
			BitArray newBits = new BitArray(nLengthOfBits);
			int nStartIndex = 0;
			foreach(char ch in serialKey.ToCharArray())
			{
				for(int nIndex = 0 ; nIndex < nMAXLenth ; ++nIndex)
				{
					if(arrChar[nIndex] == ch)
					{
						Set5Bit(nIndex, ref newBits, nStartIndex);
						nStartIndex += 5;
						break;
					}
				}
			}

			newBits.CopyTo(arrKey, 0);
		}

		String EncryptByte()
		{
			String sReturn = "";
			char[] arrChar = {'V', 'X', 'W', '1', 'A', '2', 'U', 'B', 'C', '3',
							   'S', '4', 'R', 'E', 'T', 'D', 'Z' ,'5' ,'Q', 'F',
							   '6', '8', 'Y', 'N', '7', 'G', 'O', 'M' ,'P' ,'H',
							   'K' ,'9'};
			int nMAXLenth = arrChar.Length;

			int nIndex = 0;
			byte b = 0xFF;
			while (0xFF != (b = Get5Bit(nIndex++)))
			{
				sReturn += arrChar[Convert.ToInt32(b)];
			}
			
			return sReturn;
		}
		
	};

	/// <summary>
	/// 플레이어 ID를 암호화하기 위한 클래스
	/// </summary>
	public class CharacterizedID/* : SerialKey*/
	{
		const string UNIQUE_KEY = "i-ViSiOn";

		char[] arrKeys = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };

		/// <summary>
		/// 암호화 루틴
		/// </summary>
		/// <param name="sID">암호화 할 아이디</param>
		/// <returns>암호화된 문자를 리턴한다.</returns>
		public String ToCharacterizedID(String sID)
		{
			#region Old Serialize
// 			if(String.IsNullOrEmpty(sID))
// 				throw new ArgumentException("argument is null.");
// 
// 			if(sID.Length != 7)
// 				throw new ArgumentException("argument length must be 7.");
// 
// 			base.Initialize(7);
// 
// 			int nCnt = 0;
// 			foreach (char ch in sID)
// 			{
// 				base.SetByte(nCnt++, Convert.ToByte(ch));
// 			}
// 
// 			return base.MakeKey(UNIQUE_KEY);
			#endregion
			long ticks = Convert.ToInt64(sID);
			const int CHAR_LENGTH = 13;

			StringBuilder sb = new StringBuilder();
			do
			{
				char ch = arrKeys[ticks % arrKeys.Length];
				sb.Insert(0, ch);
				ticks = ((long)(ticks / arrKeys.Length));

			} while (ticks > 0);

			int insert = CHAR_LENGTH - sb.Length;

			for (int i = 0; i < insert; ++i)
			{
				sb.Insert(0, "0");
			}

			return sb.ToString();

		}

		/// <summary>
		/// 암호화 루틴 (스테틱)
		/// </summary>
		/// <param name="sID">암호화할 아이디</param>
		/// <returns>암호화 된 문자를 리턴한다.</returns>
		public static String ConvertToCharacterized(String sID)
		{
			CharacterizedID instance = new CharacterizedID();
			return instance.ToCharacterizedID(sID);
		}

		/// <summary>
		/// 복호화 루틴
		/// </summary>
		/// <param name="sCharacterizedID">복호화할 아이디</param>
		/// <returns>복호화된 문자를 리턴한다.</returns>
		public String FromSerializedID(String sCharacterizedID)
		{
			#region Old Deserialize
// 			if (String.IsNullOrEmpty(sCharacterizedID))
// 				throw new ArgumentException("argument is null.");
// 
// 			base.Initialize(7);
// 
// 			if (base.ParseKey(sCharacterizedID, UNIQUE_KEY))
// 			{
// 				string sReturn = "";
// 
// 				for (int i = 0; i < 7; ++i)
// 				{
// 					sReturn += Convert.ToChar(base.GetByte(i));
// 				}
// 
// 				return sReturn;
// 			}
// 
// 			return null;
			#endregion

			long ticks = 0;
			int pow = 0;
			for (int i = sCharacterizedID.Length - 1; i >= 0; --i)
			{
				char ch = sCharacterizedID[i];
				for (int idx = 0; idx < arrKeys.Length; ++idx)
				{
					if (arrKeys[idx].Equals(ch))
					{
						ticks += (long)((double)idx * Math.Pow((double)arrKeys.Length, (double)pow));
						break;
					}
				}
				pow++;
			}
			return ticks.ToString("D19");
		}

		/// <summary>
		/// 복호화 루틴 (스테틱)
		/// </summary>
		/// <param name="sCharacterizedID">복호화할 아이디</param>
		/// <returns>복호화된 문자를 리턴한다.</returns>
		public static String ConvertToDeserializedID(String sCharacterizedID)
		{
			CharacterizedID instance = new CharacterizedID();
			return instance.FromSerializedID(sCharacterizedID);
		}
	}

	/// <summary>
	/// SmartSign 서버에 관련한 Serial Key 클래스
	/// </summary>
	public class NESerialKey : SerialKey
	{
		const string UNIQUE_KEY = "i-ViSiOn";

		/// <summary>
		/// 각 정보를 조합하여 시리얼 키를 생성한다.
		/// </summary>
		/// <param name="p_code">제품 코드</param>
		/// <param name="A">IP Address 첫번째 자리</param>
		/// <param name="B">IP Address 두번째 자리</param>
		/// <param name="C">IP Address 세번째 자리</param>
		/// <param name="D">IP Address 네번째 자리</param>
		/// <param name="MaxPlayers">사용가능한 플레이어 개수</param>
		/// <returns>시리얼 키 값</returns>
		public String MakeSerialKey(_ProductCode p_code, int A, int B, int C, int D, int MaxPlayers)
		{
			base.Initialize(7);

			base.SetByte(0, Convert.ToByte(p_code));
			base.SetByte(1, Convert.ToByte(A));
			base.SetByte(2, Convert.ToByte(B));
			base.SetByte(3, Convert.ToByte(C));
			base.SetByte(4, Convert.ToByte(D));
			base.SetBytes(5, BitConverter.GetBytes(Convert.ToInt16(MaxPlayers)));

			return base.MakeKey(UNIQUE_KEY);
		}

		/// <summary>
		/// 시리얼 키 값이 사용가능한지 체크한다.
		/// </summary>
		/// <param name="serialkey">시리얼 키</param>
		/// <returns>시리얼 키 Valid 여부</returns>
		public bool ParseSerialKey(String serialkey)
		{
			base.Initialize(7);

			bool bRet = base.ParseKey(serialkey, UNIQUE_KEY);

			return bRet;
		}

		/// <summary>
		/// 시리얼키 값의 정보를 반환 받는다.
		/// </summary>
		/// <param name="serialkey">시리얼 키</param>
		/// <param name="p_code">제품 코드 반환</param>
		/// <param name="A">IP Address 첫번째 자리 반환</param>
		/// <param name="B">IP Address 두번째 자리 반환</param>
		/// <param name="C">IP Address 세번째 자리 반환</param>
		/// <param name="D">IP Address 네번째 자리 반환</param>
		/// <param name="MaxPlayers">사용가능한 플레이어 개수 반환</param>
		/// <returns>시리얼 키 Valid 여부</returns>
		public bool ParseSerialKey(String serialkey, ref _ProductCode p_code, ref int A, ref int B, ref int C, ref int D, ref int MaxPlayers)
		{
			base.Initialize(7);

			bool bRet = base.ParseKey(serialkey, UNIQUE_KEY);

			if(bRet)
			{
				p_code = (_ProductCode)(base.GetByte(0));
				A = Convert.ToInt32(base.GetByte(1));
				B = Convert.ToInt32(base.GetByte(2));
				C = Convert.ToInt32(base.GetByte(3));
				D = Convert.ToInt32(base.GetByte(4));
				MaxPlayers = BitConverter.ToInt16(base.GetBytes(5, 2), 0);
			}

			return bRet;
		}
	}

}
