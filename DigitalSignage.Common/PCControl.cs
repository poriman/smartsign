﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Linq;
using System.Text;
using System.Threading;
using WaveLib.AudioMixer;
using VistaVolumeControl;

using NLog;

namespace DigitalSignage.Common
{
	public class PCControl
	{
		private static Logger logger = LogManager.GetCurrentClassLogger();

		#region Wave Control
		[DllImport("winmm.dll")]
		public static extern int waveOutGetVolume(IntPtr hwo, out uint dwVolume);

		[DllImport("winmm.dll")]
		public static extern int waveOutSetVolume(IntPtr hwo, uint dwVolume);
		#endregion

        static Mixers mixers = null;
        static MixerLine masterLine = null;
        static MixerLine waveLine = null;

		public static bool SetMasterVolume(int nVol)
		{
			try
			{
                if (mixers == null)
                {
                    mixers = new Mixers();
                }

                if (masterLine == null)
                {
                    masterLine = mixers.Playback.UserLines.GetMixerFirstLineByComponentType(MIXERLINE_COMPONENTTYPE.DST_SPEAKERS);
                }

                if (null != masterLine)
				{
                    if (masterLine.ContainsMute && masterLine.Mute)
                        masterLine.Mute = false;

                    masterLine.Volume = (nVol * 65535) / 100;
				}
				else
				{
					logger.Error("There is no master Line.");
					return false;
				}
				return true;
			}
			catch (Exception ex)
			{
				//	비스타인 경우
				try
				{
                    EndpointVolume ev = new EndpointVolume();
 
                    if (ev.Mute) ev.Mute = false;

                    ev.MasterVolume = (float)((float)nVol / 100.0);
                    ev.Dispose();
 
					return true;
				}
				catch (Exception exx)
				{
					logger.Error(ex.ToString());
					logger.Error(exx.ToString());
				}
			}
			return false;
		}

		public static bool SetWaveVolume(int nVol)
		{
			try
			{
                if (mixers == null)
                {
                    mixers = new Mixers();
                }

                if (waveLine == null)
                {
                    waveLine = mixers.Playback.UserLines.GetMixerFirstLineByComponentType(MIXERLINE_COMPONENTTYPE.SRC_WAVEOUT);
                }

                if (null != waveLine)
				{
                    if (waveLine.ContainsMute && waveLine.Mute)
                        waveLine.Mute = false;

                    waveLine.Volume = (nVol * 65535) / 100;
				}
				else
				{
					logger.Error("There is no wave Line.");
					return false;
				}
				return true;
			}
			catch (Exception ex)
			{
				try
				{
					waveOutSetVolume(IntPtr.Zero, (uint)((nVol * 65535) / 100));
					return true;
				}
				catch (Exception exx)
				{
					logger.Error(ex.ToString());
					logger.Error(exx.ToString());
				}
			}
			return false;
		}

		public static int GetMasterVolume()
		{
			try
			{
                if (mixers == null)
                {
                    mixers = new Mixers();
                }

                if (masterLine == null)
                {
                    masterLine = mixers.Playback.UserLines.GetMixerFirstLineByComponentType(MIXERLINE_COMPONENTTYPE.DST_SPEAKERS);
                }

                if (null != masterLine)
				{
                    if (masterLine.ContainsMute && masterLine.Mute)
						return 0;

                    return (masterLine.Volume * 100) / 65535;
				}
				else
				{
					logger.Error("There is no master Line.");
					
					return -1;
				}
			}
			catch (Exception ex)
			{
				try
				{
					EndpointVolume ev = new EndpointVolume();
					if (ev.Mute)
					{
						ev.Dispose();
						return 0;
					}

					float fVol = ev.MasterVolume;
					ev.Dispose();

					return Convert.ToInt32(fVol * 100);
				}
				catch (Exception exx)
				{
					logger.Error(ex.ToString());
					logger.Error(exx.ToString());
				} 
			}
			return -1;
		}
		
		public static int GetWaveVolume()
		{
			try
			{
                if (mixers == null)
                {
                    mixers = new Mixers();
                }

                if (waveLine == null)
                {
                    waveLine = mixers.Playback.UserLines.GetMixerFirstLineByComponentType(MIXERLINE_COMPONENTTYPE.SRC_WAVEOUT);
                }

                if (null != waveLine)
				{
                    if (waveLine.ContainsMute && waveLine.Mute)
						return 0;

                    return (waveLine.Volume * 100) / 65535;
				}
				else
				{
					logger.Error("There is no wave Line.");
					return -1;
				}
			}
			catch (Exception ex)
			{
				try
				{
					uint nVol = 0;

					waveOutGetVolume(IntPtr.Zero, out nVol);
					return (int)((nVol * 100) / 65535);
				}
				catch (Exception exx)
				{
					logger.Error(ex.ToString());
					logger.Error(exx.ToString());
				}
			}
			return -1;
		}

		public static bool PowerOff()
		{
			try
			{
				return DoExitWin(EWX_POWEROFF | EWX_FORCE);
			}
			catch (Exception ex)
			{
				logger.Error(ex.ToString());
			}
			return false;
		}

		public static bool Reboot()
		{
			try
			{
				return DoExitWin(EWX_REBOOT | EWX_FORCE);
			}
			catch (Exception ex)
			{
				logger.Error(ex.ToString());
			}
			return false;
		}

		#region PC control Import DLL
		[StructLayout(LayoutKind.Sequential, Pack = 1)]
		internal struct TokPriv1Luid
		{
			public int Count;
			public long Luid;
			public int Attr;
		}

		[DllImport("kernel32.dll", ExactSpelling = true)]
		internal static extern IntPtr GetCurrentProcess();

		[DllImport("advapi32.dll", ExactSpelling = true, SetLastError = true)]
		internal static extern bool OpenProcessToken(IntPtr h, int acc, ref IntPtr phtok);

		[DllImport("advapi32.dll", SetLastError = true)]
		internal static extern bool LookupPrivilegeValue(string host, string name, ref long pluid);

		[DllImport("advapi32.dll", ExactSpelling = true, SetLastError = true)]
		internal static extern bool AdjustTokenPrivileges(IntPtr htok, bool disall,
		ref TokPriv1Luid newst, int len, IntPtr prev, IntPtr relen);

		[DllImport("user32.dll", ExactSpelling = true, SetLastError = true)]
		internal static extern bool ExitWindowsEx(int flg, int rea);

		internal const int SE_PRIVILEGE_ENABLED = 0x00000002;
		internal const int TOKEN_QUERY = 0x00000008;
		internal const int TOKEN_ADJUST_PRIVILEGES = 0x00000020;
		internal const string SE_SHUTDOWN_NAME = "SeShutdownPrivilege";
		internal const int EWX_LOGOFF = 0x00000000;
		internal const int EWX_SHUTDOWN = 0x00000001;
		internal const int EWX_REBOOT = 0x00000002;
		internal const int EWX_FORCE = 0x00000004;
		internal const int EWX_POWEROFF = 0x00000008;
		internal const int EWX_FORCEIFHUNG = 0x00000010;

		public static Thread thread1;

		static bool DoExitWin(int flg)
		{
			bool ok;
			TokPriv1Luid tp;
			IntPtr hproc = GetCurrentProcess();
			IntPtr htok = IntPtr.Zero;
			ok = OpenProcessToken(hproc, TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, ref htok);
			tp.Count = 1;
			tp.Luid = 0;
			tp.Attr = SE_PRIVILEGE_ENABLED;
			ok = LookupPrivilegeValue(null, SE_SHUTDOWN_NAME, ref tp.Luid);
			ok = AdjustTokenPrivileges(htok, false, ref tp, 0, IntPtr.Zero, IntPtr.Zero);
			
			return ok = ExitWindowsEx(flg, 0);
		}
		#endregion
	}
}
