﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.Windows.Threading;

using System.Windows.Media.Animation;

namespace DigitalSignage.Common
{
    /// <summary>
    /// Interaction logic for Clock.xaml
    /// </summary>
    public partial class Clock : UserControl
    {
        private DispatcherTimer dt;

        public Clock()
        {
            InitializeComponent();
            // Insert code required on object creation below this point.
            dt = new DispatcherTimer();
            dt.Interval = new TimeSpan(0, 0, 1);
            dt.Tick += new EventHandler(dt_Tick);

            // set the initial angle values so hands aren't all pointing up when we start
            SetTimeAngles();

            // kick off the timer
            dt.Start();
		}

        void dt_Tick(object sender, EventArgs e)
        {
            SetTimeAngles();
        }

        private void SetTimeAngles()
        {
            int MinuteAngle = (int)DateTime.Now.Minute * 6;
            int DivMinute = MinuteAngle / 30;
            double FactorMinute = DivMinute * 2.5;
            this.Resources["dHourAsAngle"] = ((double)DateTime.Now.Hour * 30) + FactorMinute;
            this.Resources["dMinuteAsAngle"] = (double)DateTime.Now.Minute * 6;
            this.Resources["dSecondAsAngle"] = (double)DateTime.Now.Second * 6;
        }

    }
}
