﻿using System;

using DigitalSignage.DataBase;

namespace DigitalSignage.Common
{
    public interface IPlayersList
    {
		#region old
		DataSet.playersDataTable GetPlayersByGID(string gid);
		int Add(string gid, DataSet.playersDataTable newPlayers);
		int Update(string pid, String name, string gid, String hostname, String defProject, String ComPort, int ComBitrate, int ComBits, int rotate, int memoryusage, int cpurate, string all_d_progress, string piece_d_progress, string curr_screen, string curr_subtitle);
        int Delete(DataSet.playersDataTable players);
        DataSet.playersDataTable FindPlayerByHost( string host );
        DataSet.playersDataTable GetPlayerByPID( string pid );
		int DeletePlayer(string pid);
		int ChangeGroup(string oldGid, string newGid);
		bool IsAvailableAddPlayer();
		void UpdatePlayerID(string gid, string pid, string new_pid);
		void UpdateServiceIP(string gid, string pid, string new_service_ip);
		#endregion
	}
}
