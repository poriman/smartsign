﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DigitalSignage.ServerDatabase;

namespace DigitalSignage.Common
{
	/// <summary>
	/// 클라이언트가 다운로드를 마칠때마다 데이터베이스에 다운로드 완료 정보를 넣는다. 그걸 관리하는 인터페이스
	/// </summary>
	public interface IDownloadList
    {
		/// <summary>
		/// 서버에서 다운로드 정보가 갱신된 마지막 시간을 가져온다.
		/// </summary>
		/// <returns>마지막 갱신 시간 (UTP)</returns>
		long GetCurrTS();
		
		/// <summary>
		/// 다운로드 정보를 가져온다
		/// </summary>
		/// <param name="arrUuids">스케줄 목록</param>
		/// <param name="arrPids">플레이어 아이디 목록</param>
		/// <returns>다운로드 정보</returns>
		ServerDataSet.downloadmasterDataTable GetDownloadInformation(string[] arrUuids, string[] arrPids);

        /// <summary>
        /// 다운로드 정보를 가져온다
        /// </summary>
        /// <param name="arrUuids">스케줄 목록</param>
        /// <param name="arrPids">플레이어 아이디 목록</param>
        /// <returns>다운로드 정보</returns>
        ServerDataSet.DownloadSummaryDataTable GetDownloadCountFromUuid(string[] arrUuids, string[] arrPids);

		/// <summary>
		/// 다운로드 정보를 추가한다.
		/// </summary>
		/// <param name="uuid">스케줄 Guid</param>
		/// <param name="pid">플레이어 아이디</param>
		/// <param name="reg_date">등록 시간 (UTC)</param>
		/// <returns>-1 실패</returns>
		int InsertDownloadInformation(string uuid, string pid, long reg_date);

    }
}
