﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using Microsoft.Win32;

using System.Security.Cryptography;

namespace DigitalSignage.Common
{
	public enum iVisionAddition
	{
		ADD_COMP_RSS = 0,
		ADD_COMP_PPT,
		ADD_COMP_QTIME,
		ADD_COMP_WEB,
		ADD_COMP_TV,
		ADD_COMP_STREAM,
		ADD_COMP_WEATHER,
		ADD_CONTROL_SCHEDULE,
		ADD_SUBTITLE_SCHEDULE
	};
	public class KeyChecker
	{
		//	등록되는 위치이다.
		private string regstry = "{4B382C69-2C2D-419e-9718-766FE0FB6B51}";

		private static KeyChecker instance = null;

		public static KeyChecker GetInstance
		{
			get {
				if (instance == null) return instance = new KeyChecker();
				else return instance;
			}
		}

		private const string COMP_RSS = "COMPRSS";
		private const string COMP_PPT = "COMPPPT";
		private const string COMP_QTIME = "COMPQT";
		private const string COMP_TV = "COMPTV";
		private const string COMP_STREAM = "COMPSTREAM";
		private const string COMP_WEB = "COMPWEB";
		private const string COMP_WEATHER = "COMPWEATHER";
		private const string COMP_CONT_SCHED = "CONTSCHED";
		private const string COMP_SUBT_SCHED = "SUBTSCHED";

		String GetAddionTypeToString(iVisionAddition type)
		{
			switch (type)
			{
				case iVisionAddition.ADD_COMP_PPT:
					return COMP_PPT;
				case iVisionAddition.ADD_COMP_QTIME:
					return COMP_QTIME;
				case iVisionAddition.ADD_COMP_STREAM:
					return COMP_STREAM;
				case iVisionAddition.ADD_COMP_TV:
					return COMP_TV;
				case iVisionAddition.ADD_COMP_RSS:
					return COMP_RSS;
				case iVisionAddition.ADD_COMP_WEB:
					return COMP_WEB;
				case iVisionAddition.ADD_COMP_WEATHER:
					return COMP_WEATHER;
				case iVisionAddition.ADD_CONTROL_SCHEDULE:
					return COMP_CONT_SCHED;
				case iVisionAddition.ADD_SUBTITLE_SCHEDULE:
					return COMP_SUBT_SCHED;
				default:
					return null;
			}
		}

		public bool CheckKey(iVisionAddition type)
		{
			try
			{
				RegistryKey root = Registry.ClassesRoot;
				RegistryKey clsid = root.OpenSubKey("CLSID");
				RegistryKey dest = clsid.OpenSubKey(regstry);

				String executed = dest.GetValue(GetAddionTypeToString(type)).ToString();

				dest.Close();
				clsid.Close();
				root.Close();

				if (String.IsNullOrEmpty(executed)) return false;

				return IntellianCrypto.GetCrypto.Decryption(executed).Equals("0") ? false : true;

			}
			catch { }
			return false;
		}

		public bool SetKey(iVisionAddition type, bool enable)
		{
			try
			{
				RegistryKey root = Registry.ClassesRoot;
				RegistryKey clsid = root.OpenSubKey("CLSID", true);
				RegistryKey dest = clsid.OpenSubKey(regstry, true);

				dest.SetValue(GetAddionTypeToString(type), IntellianCrypto.GetCrypto.Encryption(enable == true ? "1" : "0"));
				return true;
			}
			catch { }
			return false;
		}
	}

	public class TrialChecker
	{
		//	등록되는 위치이다.
		private string regstry = "{4B382C69-2C2D-419e-9718-766FE0FB6B51}";

		private const string TRIAL = "IST";
		private const string INSTALLED = "ST";
		private const string LIMIT_DURATION = "LD";
		private const string EXECUTED = "ET";


		private string lastMessage = "";
		public string LastMessage
		{
			get { return lastMessage; }
		}

		public int LeftDays
		{
			get
			{
				try
				{
					TimeSpan leftday = (InstalledTime + TrialDuration) - DateTime.Now;

					return Convert.ToInt32(leftday.TotalDays);
				}
				catch
				{
					return -1;
				}

			}
		}
		public void CheckKey()
		{
			RegistryKey root = Registry.ClassesRoot;
			RegistryKey clsid = root.OpenSubKey("CLSID", true);
			RegistryKey dest = clsid.OpenSubKey(regstry, true);

			if(dest == null)
			{
				dest = clsid.CreateSubKey(regstry);

				IsTrial = true;
				LastExecutedTime = DateTime.Today;
				TrialDuration = new TimeSpan(30, 0, 0, 0);
				InstalledTime = DateTime.Today;
				
				dest.Close();
			}

			clsid.Close();
			root.Close();
		}

		public bool IsValid
		{
			get
			{
				try
				{
					CheckKey();

					//	트라이얼인지 체크
					if (!IsTrial)
						return true;

					//	마지막 실행시간 가져오기
					DateTime lastE = LastExecutedTime;

					//	마지막 실행시간이 크다는건 시간을 되돌렸다는 소리..
					if (lastE > DateTime.Now)
					{
						lastMessage = "Your system time had been modified by user.";
						return false;
					}

					//	마지막 실행시간 갱신
					LastExecutedTime = DateTime.Now;

					DateTime Installed = InstalledTime;
					TimeSpan till = TrialDuration;

					//	시간이 지났다
					if (Installed + till < DateTime.Now)
					{
						lastMessage = "i-Vision is expired.";
						return false;
					}

					TimeSpan ts = (Installed + till) - DateTime.Now;
					lastMessage = "I-Vision can be used during " + ts.TotalDays.ToString() + " days.";
					return true;
				}
				catch
				{
					lastMessage = "i-Vision was not installed.";
					return false;
				}

			}
		}

		public DateTime LastExecutedTime
		{
			get 
			{
				RegistryKey root = Registry.ClassesRoot;
				RegistryKey clsid = root.OpenSubKey("CLSID");
				RegistryKey dest = clsid.OpenSubKey(regstry);

				String executed = dest.GetValue(EXECUTED).ToString();


				dest.Close();
				clsid.Close();
				root.Close();

				if (String.IsNullOrEmpty(executed)) return DateTime.Now;

				return new DateTime(Convert.ToInt64(IntellianCrypto.GetCrypto.Decryption(executed)));
			}
			set 
			{
				RegistryKey root = Registry.ClassesRoot;
				RegistryKey clsid = root.OpenSubKey("CLSID", true);
				RegistryKey dest = clsid.OpenSubKey(regstry, true);

				dest.SetValue(EXECUTED, IntellianCrypto.GetCrypto.Encryption(value.Ticks.ToString()));
			}
		}

		public bool IsTrial
		{
			get 
			{
				RegistryKey root = Registry.ClassesRoot;
				RegistryKey clsid = root.OpenSubKey("CLSID");
				RegistryKey dest = clsid.OpenSubKey(regstry);

				String Istrial = dest.GetValue(TRIAL).ToString();

				dest.Close();
				clsid.Close();
				root.Close();

				if(String.IsNullOrEmpty(Istrial))
				{
					return false;
				}

				return IntellianCrypto.GetCrypto.Decryption(Istrial).Equals("0") ? false : true;
			}
			set
			{
				RegistryKey root = Registry.ClassesRoot;
				RegistryKey clsid = root.OpenSubKey("CLSID", true);
				RegistryKey dest = clsid.OpenSubKey(regstry, true);

				dest.SetValue(TRIAL, IntellianCrypto.GetCrypto.Encryption(value == true ? "1" : "0"));
			}

		}

		public TimeSpan TrialDuration
		{
			get 
			{
				RegistryKey root = Registry.ClassesRoot;
				RegistryKey clsid = root.OpenSubKey("CLSID");
				RegistryKey dest = clsid.OpenSubKey(regstry);

				String limit = dest.GetValue(LIMIT_DURATION).ToString();

				dest.Close();
				clsid.Close();
				root.Close();

				return new TimeSpan(
					Convert.ToInt32(IntellianCrypto.GetCrypto.Decryption(limit)), 0, 0, 0);
			
			}
			set
			{
				RegistryKey root = Registry.ClassesRoot;
				RegistryKey clsid = root.OpenSubKey("CLSID", true);
				RegistryKey dest = clsid.OpenSubKey(regstry, true);

				dest.SetValue(LIMIT_DURATION, IntellianCrypto.GetCrypto.Encryption(value.TotalDays.ToString()));
			}

		}
		
		public DateTime InstalledTime
		{
			get 
			{
				RegistryKey root = Registry.ClassesRoot;
				RegistryKey clsid = root.OpenSubKey("CLSID", true);
				RegistryKey dest = clsid.OpenSubKey(regstry, true);

				String installed = "";
				try
				{
					installed = dest.GetValue(INSTALLED).ToString();

				}
				catch
				{
					DateTime dt = DateTime.Now;
					dest.SetValue(INSTALLED, installed = IntellianCrypto.GetCrypto.Encryption(dt.Ticks.ToString()));
					dest.SetValue(EXECUTED, IntellianCrypto.GetCrypto.Encryption(dt.Ticks.ToString()));
				}

				dest.Close();
				clsid.Close();
				root.Close();

				return new DateTime(Convert.ToInt64(IntellianCrypto.GetCrypto.Decryption(installed)));
			}
			set
			{
				RegistryKey root = Registry.ClassesRoot;
				RegistryKey clsid = root.OpenSubKey("CLSID", true);
				RegistryKey dest = clsid.OpenSubKey(regstry, true);

				dest.SetValue(INSTALLED, IntellianCrypto.GetCrypto.Encryption(value.Ticks.ToString()));
			}

		}

		private DateTime EndTime
		{
			get 
			{
				return InstalledTime + TrialDuration;
			}
		}

		//	기본 키를 생성한다.
		public bool CreateDefaultKey()
		{
			try
			{
				RegistryKey root = Registry.ClassesRoot;
				RegistryKey clsid = root.OpenSubKey("CLSID", true);

				return clsid.CreateSubKey(regstry) != null;
			}
			catch
			{
				return false;
			}

		}
	}

	#region Crypto
	public class IntellianCrypto
	{
		private static readonly object semaphore = new object();
		private static IntellianCrypto instance = null;

		public static IntellianCrypto GetCrypto
		{
			get
			{
				lock (semaphore)
				{
					if (instance == null)
					{
						instance = new IntellianCrypto();
					}
					return instance;
				}
			}
		}
		public string KeyPhrase
		{
			set { passPhrase = value; }
		}
		string passPhrase = "!@#$Intellian"; 
		string saltValue = "s@1tValue";
		string hashAlgorithm = "SHA1";					// can be "MD5"
		int passwordIterations = 2;						// can be any number
		string initVector = "@1B2c3D4e5F6g7H8";			// must be 16 bytes
		int keySize = 256;								// can be 192 or 128

		public String Encryption(string plainText)
		{
			return Encrypt(plainText, passPhrase, saltValue, hashAlgorithm, passwordIterations, initVector, keySize);
		}
		public String Decryption(string cipherText)
		{
			return Decrypt(cipherText, passPhrase, saltValue, hashAlgorithm, passwordIterations, initVector, keySize);

		}

		#region core_logic
		/// <summary>
		/// Encrypts specified plaintext using Rijndael symmetric key algorithm
		/// and returns a base64-encoded result.
		/// </summary>
		/// <param name="plainText">
		/// Plaintext value to be encrypted.
		/// </param>
		/// <param name="passPhrase">
		/// Passphrase from which a pseudo-random password will be derived. The
		/// derived password will be used to generate the encryption key.
		/// Passphrase can be any string. In this example we assume that this
		/// passphrase is an ASCII string.
		/// </param>
		/// <param name="saltValue">
		/// Salt value used along with passphrase to generate password. Salt can
		/// be any string. In this example we assume that salt is an ASCII string.
		/// </param>
		/// <param name="hashAlgorithm">
		/// Hash algorithm used to generate password. Allowed values are: "MD5" and
		/// "SHA1". SHA1 hashes are a bit slower, but more secure than MD5 hashes.
		/// </param>
		/// <param name="passwordIterations">
		/// Number of iterations used to generate password. One or two iterations
		/// should be enough.
		/// </param>
		/// <param name="initVector">
		/// Initialization vector (or IV). This value is required to encrypt the
		/// first block of plaintext data. For RijndaelManaged class IV must be 
		/// exactly 16 ASCII characters long.
		/// </param>
		/// <param name="keySize">
		/// Size of encryption key in bits. Allowed values are: 128, 192, and 256. 
		/// Longer keys are more secure than shorter keys.
		/// </param>
		/// <returns>
		/// Encrypted value formatted as a base64-encoded string.
		/// </returns>
		public static string Encrypt(string plainText,
									 string passPhrase,
									 string saltValue,
									 string hashAlgorithm,
									 int passwordIterations,
									 string initVector,
									 int keySize)
		{
			// Convert strings into byte arrays.
			// Let us assume that strings only contain ASCII codes.
			// If strings include Unicode characters, use Unicode, UTF7, or UTF8 
			// encoding.
			byte[] initVectorBytes = Encoding.ASCII.GetBytes(initVector);
			byte[] saltValueBytes = Encoding.ASCII.GetBytes(saltValue);

			// Convert our plaintext into a byte array.
			// Let us assume that plaintext contains UTF8-encoded characters.
			byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);

			// First, we must create a password, from which the key will be derived.
			// This password will be generated from the specified passphrase and 
			// salt value. The password will be created using the specified hash 
			// algorithm. Password creation can be done in several iterations.
			PasswordDeriveBytes password = new PasswordDeriveBytes(
															passPhrase,
															saltValueBytes,
															hashAlgorithm,
															passwordIterations);

			// Use the password to generate pseudo-random bytes for the encryption
			// key. Specify the size of the key in bytes (instead of bits).
			byte[] keyBytes = password.GetBytes(keySize / 8);

			// Create uninitialized Rijndael encryption object.
			RijndaelManaged symmetricKey = new RijndaelManaged();

			// It is reasonable to set encryption mode to Cipher Block Chaining
			// (CBC). Use default options for other symmetric key parameters.
			symmetricKey.Mode = CipherMode.CBC;

			// Generate encryptor from the existing key bytes and initialization 
			// vector. Key size will be defined based on the number of the key 
			// bytes.
			ICryptoTransform encryptor = symmetricKey.CreateEncryptor(
															 keyBytes,
															 initVectorBytes);

			// Define memory stream which will be used to hold encrypted data.
			MemoryStream memoryStream = new MemoryStream();

			// Define cryptographic stream (always use Write mode for encryption).
			CryptoStream cryptoStream = new CryptoStream(memoryStream,
														 encryptor,
														 CryptoStreamMode.Write);
			// Start encrypting.
			cryptoStream.Write(plainTextBytes, 0, plainTextBytes.Length);

			// Finish encrypting.
			cryptoStream.FlushFinalBlock();

			// Convert our encrypted data from a memory stream into a byte array.
			byte[] cipherTextBytes = memoryStream.ToArray();

			// Close both streams.
			memoryStream.Close();
			cryptoStream.Close();

			// Convert encrypted data into a base64-encoded string.
			string cipherText = Convert.ToBase64String(cipherTextBytes);

			// Return encrypted string.
			return cipherText;
		}

		/// <summary>
		/// Decrypts specified ciphertext using Rijndael symmetric key algorithm.
		/// </summary>
		/// <param name="cipherText">
		/// Base64-formatted ciphertext value.
		/// </param>
		/// <param name="passPhrase">
		/// Passphrase from which a pseudo-random password will be derived. The
		/// derived password will be used to generate the encryption key.
		/// Passphrase can be any string. In this example we assume that this
		/// passphrase is an ASCII string.
		/// </param>
		/// <param name="saltValue">
		/// Salt value used along with passphrase to generate password. Salt can
		/// be any string. In this example we assume that salt is an ASCII string.
		/// </param>
		/// <param name="hashAlgorithm">
		/// Hash algorithm used to generate password. Allowed values are: "MD5" and
		/// "SHA1". SHA1 hashes are a bit slower, but more secure than MD5 hashes.
		/// </param>
		/// <param name="passwordIterations">
		/// Number of iterations used to generate password. One or two iterations
		/// should be enough.
		/// </param>
		/// <param name="initVector">
		/// Initialization vector (or IV). This value is required to encrypt the
		/// first block of plaintext data. For RijndaelManaged class IV must be
		/// exactly 16 ASCII characters long.
		/// </param>
		/// <param name="keySize">
		/// Size of encryption key in bits. Allowed values are: 128, 192, and 256.
		/// Longer keys are more secure than shorter keys.
		/// </param>
		/// <returns>
		/// Decrypted string value.
		/// </returns>
		/// <remarks>
		/// Most of the logic in this function is similar to the Encrypt
		/// logic. In order for decryption to work, all parameters of this function
		/// - except cipherText value - must match the corresponding parameters of
		/// the Encrypt function which was called to generate the
		/// ciphertext.
		/// </remarks>
		public static string Decrypt(string cipherText,
									 string passPhrase,
									 string saltValue,
									 string hashAlgorithm,
									 int passwordIterations,
									 string initVector,
									 int keySize)
		{
			// Convert strings defining encryption key characteristics into byte
			// arrays. Let us assume that strings only contain ASCII codes.
			// If strings include Unicode characters, use Unicode, UTF7, or UTF8
			// encoding.
			byte[] initVectorBytes = Encoding.ASCII.GetBytes(initVector);
			byte[] saltValueBytes = Encoding.ASCII.GetBytes(saltValue);

			// Convert our ciphertext into a byte array.
			byte[] cipherTextBytes = Convert.FromBase64String(cipherText);

			// First, we must create a password, from which the key will be 
			// derived. This password will be generated from the specified 
			// passphrase and salt value. The password will be created using
			// the specified hash algorithm. Password creation can be done in
			// several iterations.
			PasswordDeriveBytes password = new PasswordDeriveBytes(
															passPhrase,
															saltValueBytes,
															hashAlgorithm,
															passwordIterations);

			// Use the password to generate pseudo-random bytes for the encryption
			// key. Specify the size of the key in bytes (instead of bits).
			byte[] keyBytes = password.GetBytes(keySize / 8);

			// Create uninitialized Rijndael encryption object.
			RijndaelManaged symmetricKey = new RijndaelManaged();

			// It is reasonable to set encryption mode to Cipher Block Chaining
			// (CBC). Use default options for other symmetric key parameters.
			symmetricKey.Mode = CipherMode.CBC;

			// Generate decryptor from the existing key bytes and initialization 
			// vector. Key size will be defined based on the number of the key 
			// bytes.
			ICryptoTransform decryptor = symmetricKey.CreateDecryptor(
															 keyBytes,
															 initVectorBytes);

			// Define memory stream which will be used to hold encrypted data.
			MemoryStream memoryStream = new MemoryStream(cipherTextBytes);

			// Define cryptographic stream (always use Read mode for encryption).
			CryptoStream cryptoStream = new CryptoStream(memoryStream,
														  decryptor,
														  CryptoStreamMode.Read);

			// Since at this point we don't know what the size of decrypted data
			// will be, allocate the buffer long enough to hold ciphertext;
			// plaintext is never longer than ciphertext.
			byte[] plainTextBytes = new byte[cipherTextBytes.Length];

			// Start decrypting.
			int decryptedByteCount = cryptoStream.Read(plainTextBytes,
													   0,
													   plainTextBytes.Length);

			// Close both streams.
			memoryStream.Close();
			cryptoStream.Close();

			// Convert decrypted data into a string. 
			// Let us assume that the original plaintext string was UTF8-encoded.
			string plainText = Encoding.UTF8.GetString(plainTextBytes,
													   0,
													   decryptedByteCount);

			// Return decrypted string.   
			return plainText;
		}

		#endregion
	}
	#endregion

}
