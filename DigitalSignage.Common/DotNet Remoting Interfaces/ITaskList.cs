﻿using System;

using DigitalSignage.ServerDatabase;
using System.Collections.ObjectModel;

namespace DigitalSignage.Common
{
    /// <summary>
    /// 스케줄 관련 정보에 관한 인터페이스
    /// </summary>
    public interface ITaskList
    {
		/// <summary>
		/// 서버의 스케줄 데이터가 마지막으로 갱신된 시간을 가져온다.
		/// </summary>
		/// <returns>UTP 시간 정보</returns>
		long GetTasksTimestamp();

        /// <summary>
        /// guuid와 같은 스케줄을 모두 불러온다.
        /// </summary>
        /// <param name="guuid"></param>
        /// <returns></returns>
        SYNC_TASK[] GetSyncTask(string guuid);

		/// <summary>
		/// 스케줄을 등록한다.
		/// </summary>
		/// <param name="task">스케줄에 관련한 클래스</param>
		/// <returns>실패: -1</returns>
        int AddTask(Task task);

        /// <summary>
        /// Sync 스케줄을 등록한다.
        /// </summary>
        /// <param name="task">스케줄에 관련한 클래스</param>
        /// <param name="synclist">동기화를 맞출 목록</param>
        /// <returns>실패: -1</returns>
        int AddSyncTask(Task task, SYNC_TASK[] synclist);

		/// <summary>
		/// 지정한 UTP 시간 이후에 등록된 해당 플레이어, 그룹의 스케줄을 가져온다.
		/// </summary>
		/// <param name="timestamp">기준이되는 UTP 시간정보</param>
		/// <param name="pid">플레이어 아이디</param>
		/// <param name="gid">그룹 아이디</param>
		/// <returns>스케줄 정보</returns>
		ServerDataSet.tasksDataTable GetNewTasks(long timestamp, string pid, string gid);

        /// <summary>
        /// 지정한 시간 이후에 등록된 해당 플레이어의 동기화 스케줄 목록을 가져온다.
        /// </summary>
        /// <param name="timestamp">기준이되는 시간정보</param>
        /// <param name="pid">플레이어 아이디</param>
        /// <returns>싱크 스케줄 정보</returns>
        ServerDataSet.SYNC_TASKSDataTable GetNewSyncTasks(string timestamp, string pid);

		/// <summary>
		/// 해당 이름과 타입에 맞는 스케줄 중 timeestamp 이후의 스케줄을 가져온다. 
		/// </summary>
		/// <param name="timestamp">기준 시간</param>
		/// <param name="type">스케줄 타입</param>
		/// <param name="name">스케줄 이름</param>
		/// <returns></returns>
		Task[] GetTasksByNameAndType(long timestamp, int type, string name);

		/// <summary>
		/// from 에서 till 까지의 UTP 시간안에 존재하거나 걸쳐진 스케줄을 가져온다. 가져오는 기준은 해당 플레이어나 그룹의 상위 스케줄까지 가져온다.0
		/// </summary>
		/// <param name="PIDorGID">선택한 플레이어 또는 그룹의 ID</param>
		/// <param name="isPlayer">플레이어 인지 그룹인지</param>
		/// <param name="from">시작 기준</param>
		/// <param name="till">끝기준</param>
		/// <returns>스케줄 데이터</returns>
		ServerDataSet.tasksDataTable GetRecursiveTasksFrom(string PIDorGID, bool isPlayer, long from, long till);

		/// <summary>
		/// from 에서 till 까지의 UTP 시간안에 존재하거나 걸쳐진 스케줄을 가져온다.
		/// </summary>
		/// <param name="from">시작 기준</param>
		/// <param name="till">끝 기준</param>
		/// <returns>스케줄 데이터</returns>
		ServerDataSet.tasksDataTable GetTasksFrom(long from, long till);

		/// <summary>
		/// 그룹 Guid에 해당하는 모든 스케줄을 가져온다.
		/// </summary>
		/// <param name="guuid">그룹 Guid</param>
		/// <returns>스케줄 정보</returns>
        ServerDataSet.tasksDataTable GetTasksGroup( String guuid );

		/// <summary>
		/// 그룹 Guid에 해당하는 스케줄의 개수를 반환한다.
		/// </summary>
		/// <param name="guuid"></param>
		/// <returns></returns>
		int GetTasksGroupCount(String guuid);

		/// <summary>
		/// 스케줄을 삭제한다. 스케줄의 삭제기준은 guuid를 기준으로 동일한 guuid은 모두 지운다.
		/// </summary>
		/// <param name="task">스케줄</param>
		/// <returns>실패 -1</returns>
		int DeleteTask(Task task);

		/// <summary>
		/// 스케줄을 수정한다. 예전스케줄의 정보에서 uuid만 비교하며, newTask의 uuid를 뺀 나머지를 모두 수정한다.
		/// </summary>
		/// <param name="oldTask">예전 스케줄 정보</param>
		/// <param name="newTask">새로운 스케줄 정보</param>
		/// <returns>-1 실패</returns>
        int EditTasks(Task oldTask, Task newTask);

        /// <summary>
        /// 싱크 스케줄을 수정한다. 예전스케줄의 정보에서 uuid만 비교하며, newTask의 uuid를 뺀 나머지를 모두 수정한다.
        /// </summary>
        /// <param name="oldTask">예전 스케줄 정보</param>
        /// <param name="newTask">새로운 스케줄 정보</param>
        /// <param name="synclist">동기화를 맞출 목록</param>
        /// <returns></returns>
        int EditSyncTasks(Task oldTask, Task newTask, SYNC_TASK[] synclist);

		/// <summary>
		/// 반복 스케줄인 경우 해당 그룹에서 가장 작은 우선순위 숫자를 가져온다.
		/// </summary>
		/// <param name="gid">그룹 아이디</param>
		/// <returns>가장 작은 우선순위 숫자</returns>
		int GetMinPriorityFromDefaultTasks(string gid);

		/// <summary>
		/// 사용 안함
		/// </summary>
		/// <param name="task"></param>
		/// <returns></returns>
		int UpdateTask(Task task);
		
		/// <summary>
		/// 해당 그룹에 해당 타입이 생성될 수 있는지 검사한다.
		/// </summary>
		/// <param name="uuid">스케줄의 고유 식별 아이디(PK)</param>
		/// <param name="from">스케줄의 시작 시간 (UTP)</param>
		/// <param name="till">스케줄의 끝 시간 (UTP)</param>
		/// <param name="type">스케줄 타입 (DisitalSignage.Common.TaskCommands 값)</param>
		/// <param name="gid">그룹 아이디</param>
		/// <returns>성공 여부</returns>
		bool CanAddTaskToGroup(String uuid, long from, long till, long type, string gid);
		bool CanAddTaskToGroup(Task task, string gid);
		
		/// <summary>
		/// 해당 플레이어에 해당 타입의 스케줄이 생성될 수 있는지 검사한다.
		/// </summary>
		/// <param name="uuid">스케줄의 고유 식별 아이디(PK)</param>
		/// <param name="from">스케줄의 시작 시간 (UTP)</param>
		/// <param name="till">스케줄의 끝 시간 (UTP)</param>
		/// <param name="type">스케줄 타입 (DisitalSignage.Common.TaskCommands 값)</param>
		/// <param name="pid">플레이어 아이디</param>
		/// <returns></returns>
		bool CanAddTaskToPlayer(String uuid, long from, long till, long type, string pid);
		bool CanAddTaskToPlayer(Task task, string pid);
		
		/// <summary>
		/// 다운로드 Guid가 혼자쓰여서 삭제가 가능한지 검사한다.
		/// </summary>
		/// <param name="duuid">다운로드 Guid</param>
		/// <param name="ftpAddress">컨텐츠 서버 주소</param>
		/// <returns>성공 여부</returns>
		bool CanDeleteByDuuid(String duuid, string ftpAddress);

		/// <summary>
		/// 다운로드 Guid가 혼자쓰여서 삭제가 가능한지 검사한다.
		/// </summary>
		/// <param name="duuid">다운로드 Guid</param>
		/// <param name="ftpAddress">컨텐츠 서버 주소</param>
		/// <param name="ftpPath">컨텐츠 서버 Path</param>
		/// <returns>성공 여부</returns>
		bool CanDeleteByDuuid(String duuid, string ftpAddress, string ftpPath);

		/// <summary>
		/// 스케줄 상태를 변경한다.
		/// </summary>
		/// <param name="newState"></param>
		/// <param name="taskUuid"></param>
		/// <returns></returns>
		int ChangeTaskState(DigitalSignage.Common.TaskState newState, Guid taskUuid);

		/// <summary>
		/// 스케줄을 가져온다
		/// </summary>
		/// <param name="uuid"></param>
		/// <returns></returns>
		ServerDatabase.ServerDataSet.tasksDataTable GetTaskByuuid(string uuid);

		/// <summary>
		/// 스크린을 등록한다.
		/// </summary>
		/// <param name="s_id">페이지 아이디</param>
		/// <param name="s_name">이름</param>
		/// <param name="s_description">상세</param>
		/// <param name="s_width">너비</param>
		/// <param name="s_height">높이</param>
		/// <param name="playtime">재생시간</param>
		/// <param name="screen_type">페이지 타입</param>
		/// <param name="display_type">디스플레이 타입</param>
		/// <param name="s_is_ad">광고 여부</param>
		/// <param name="s_metatag">메타태그</param>
		/// <param name="ad_starttime">광고 시작 시간</param>
		/// <param name="ad_endtime">광고 끝 시간</param>
		/// <returns>성공 여부</returns>
		bool InsertScreen(long s_id, string s_name, string s_description, int s_width, int s_height, int playtime, int screen_type, int display_type, string s_is_ad, string s_metatag, long ad_starttime, long ad_endtime);

		/// <summary>
		/// 스케줄과 스크린과의 연결을 등록한다.
		/// </summary>
		/// <param name="s_id">페이지 ID</param>
		/// <param name="task_uuid">스케줄 ID</param>
		/// <param name="s_index">페이지 순서</param>
		/// <returns>성공 여부</returns>
		bool InsertTaskAndScreenMapping(long s_id, string task_uuid, int s_index);

        /// <summary>
        /// 스케줄과 스크린과의 연결을 등록한다.
        /// </summary>
        /// <param name="s_id">페이지 ID</param>
        /// <param name="task_uuid">스케줄 ID</param>
        /// <param name="s_index">페이지 순서</param>
        /// <param name="thumbnailURL"></param>
        /// <returns>성공 여부</returns>
        bool InsertTaskAndScreenMappingDetail(long s_id, string task_uuid, int s_index, string thumbnailURL);
		
        /// <summary>
		/// 스케줄과 연결된 스크린을 가져온다.
		/// </summary>
		/// <param name="uuid"></param>
		/// <returns></returns>
		ServerDatabase.ServerDataSet.screensDataTable GetScreensByUuid(string uuid);

		/// <summary>
		/// 스크린을 가져온다.
		/// </summary>
		/// <param name="screenID"></param>
		/// <returns></returns>
		ServerDatabase.ServerDataSet.screensDataTable GetScreenByID(string screenID);
	}
    /// <summary>
    /// 동기화 스케줄 클래스
    /// </summary>
    [Serializable]
    public class SYNC_TASK
    {
        /// <summary>
        /// 스케줄 ID
        /// </summary>
        public string UUID { get; set; }
        /// <summary>
        /// 다운로드 ID
        /// </summary>
        public string DUUID { get; set; }
        /// <summary>
        /// 그룹 스케줄 ID
        /// </summary>
        public string GUUID { get; set; }
        /// <summary>
        /// 마스터 여부
        /// </summary>
        public bool MasterYN { get; set; }
        /// <summary>
        /// 그룹 ID
        /// </summary>
        public string GroupID { get; set; }
        /// <summary>
        /// 플레이어 ID
        /// </summary>
        public string PlayerID { get; set; }
        /// <summary>
        /// 수정일시
        /// </summary>
        public string EditDT { get; set; }
        /// <summary>
        /// 등록일시
        /// </summary>
        public string RegDT { get; set; }
    }
}