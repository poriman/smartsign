﻿using System;

namespace DigitalSignage.Common
{
	/// <summary>
	/// 플레이어 관련 테이블 조작에 관한 인터페이스
	/// </summary>
    public interface IPlayersList
    {
        /// <summary>
		/// NPCU에 컨트롤 메시지를 날린다.
        /// </summary>
        /// <param name="PlayerID"></param>
        /// <param name="RequestCode"></param>
        /// <returns></returns>
        String NPCUControl(String PlayerID, String RequestCode);

		/// <summary>
		/// NPCU에 컨트롤 메시지를 날린다.
		/// </summary>
		/// <param name="arrPids"></param>
		/// <param name="RequestCode"></param>
		/// <returns></returns>
		String NPCUControlByPIDArray(String[] arrPids, String RequestCode);

		/// <summary>
		/// 해당 그룹 아이디에 지정된 플레이어의 제한 개수를 리턴하는 함수
		/// </summary>
		/// <param name="gid">그룹 아이디</param>
		/// <returns>플레이어의 제한 개수</returns>
		int GetLimitInfo(string gid);

		/// <summary>
		/// 해당 그룹에 플레이어의 제한 개수를 입력한다.
		/// </summary>
		/// <param name="cntPlayers">적용할 제한 개수</param>
		/// <param name="gid">적용할 그룹 아이디</param>
		/// <returns>성공 여부</returns>
		bool AddPlayerLimitInfo(int cntPlayers, string gid);
		
		/// <summary>
		/// 그룹에 할당된 플레이어의 제한 개수를 제거한다.
		/// </summary>
		/// <param name="gid">그룹 아이디</param>
		/// <returns>성공 여부</returns>
		bool DeleteLimitInfo(string gid);
		
		/// <summary>
		/// 플레이어를 추가할 수 있는지를 얻어온다.
		/// </summary>
		/// <returns>가능 여부</returns>
		bool IsAvailableAddPlayer();

		/// <summary>
		/// 해당 그룹에 플레이어를 추가할 수 있는지 얻어온다.
		/// </summary>
		/// <param name="gid">대상 그룹</param>
		/// <returns>가능 여부</returns>
		bool IsAvailableAddPlayer(string gid);

        /// <summary>
        /// 원격접속 요청
        /// </summary>
        /// <param name="connid"></param>
        /// <param name="pid"></param>
        /// <returns></returns>
        bool RunRemoteControl(string connid, string pid);
	}
}
