﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DigitalSignage.ServerDatabase;

namespace DigitalSignage.Common
{
	/// <summary>
	/// 사용자 조작에 관련한 인터페이스
	/// </summary>
    public interface IUsersList
    {
		/// <summary>
		/// 자신을 포함한 하위의 역할 리스트를 반환한다.
		/// </summary>
		/// <param name="own_role_name">현재 자신의 역할</param>
		/// <param name="own_role_level">현재 자신의 역할 레벨</param>
		/// <returns>자신을 포함한 하위 역할들의 리스트를 반환한다.</returns>
		ServerDataSet.rolesDataTable GetAvailableRoleList(string own_role_name, int own_role_level);
		
		/// <summary>
		/// 입력된 사용자에게 할당된 그룹 ID들을 반환한다.
		/// </summary>
		/// <param name="user_id">사용자 ID</param>
		/// <returns>그룹 ID의 배열</returns>
		String[] GetGroupList(long user_id);

		/// <summary>
		/// 입력된 사용자에게 할당된 플레이어 ID들을 반환한다.
		/// </summary>
		/// <param name="user_id">사용자 ID</param>
		/// <returns>플레이어 ID의 배열</returns>
		String[] GetPlayerList(long user_id);

		/// <summary>
		/// 새로운 사용자를 추가한다.
		/// </summary>
		/// <param name="name">사용자 ID</param>
		/// <param name="password">사용자 PWD 해시값</param>
		/// <param name="role_ids">할당할 역할 (구분자 |를 통해 여러개를 할당 가능하다.)</param>
		/// <param name="parent_id">부모 사용자</param>
		/// <param name="groups">할당할 그룹 ID 배열</param>
		/// <param name="players">할당할 플레이어 ID 배열</param>
		/// <returns>성공 여부</returns>
		bool AddUser(String name, String password, String role_ids, long parent_id, String[] groups, String[] players);
		
		/// <summary>
		/// 기존의 사용자를 변경한다.
		/// </summary>
		/// <param name="user_id">사용자 ID</param>
		/// <param name="password">사용자 PWD 해시값</param>
		/// <param name="role_ids">할당할 역할 (구분자 |를 통해 여러개를 할당 가능하다.)</param>
		/// <param name="parent_id">부모 사용자</param>
		/// <param name="groups">할당할 그룹 ID 배열</param>
		/// <param name="players">할당할 플레이어 ID 배열</param>
		/// <returns>성공 여부</returns>
		bool EditUser(long user_id, String password, String role_ids, long parent_id, String[] groups, String[] players);
		
		/// <summary>
		/// 기존의 사용자를 삭제한다.
		/// </summary>
		/// <param name="user_id">삭제할 사용자 Id</param>
		/// <returns>성공 여부</returns>
		bool DeleteUser(long user_id);

		/// <summary>
		/// 사용자의 활성 상태를 변경한다.
		/// </summary>
		/// <param name="user_id">사용자 Id</param>
		/// <param name="bEnabled">활성 여부</param>
		/// <returns>성공 여부</returns>
		bool ModifyEnabled(long user_id, bool bEnabled);

		/*
		/// <summary>
		/// 사용자의 접속 현황을 보고한다.
		/// </summary>
		/// <param name="user_id">사용자 id</param>
		/// <param name="computer_name">컴퓨터 이름</param>
		/// <param name="ipaddr1">IP 주소 1</param>
		/// <param name="ipaddr2">IP 주소 2</param>
		/// <param name="ipaddr3">IP 주소 3</param>
		/// <param name="version">매니저 버전 정보</param>
		/// <returns>성공 여부</returns>
		bool AddConnectionInfo(string user_id, string computer_name, string ipaddr1, string ipaddr2, string ipaddr3, string version);
		*/

		#region old
// 		/// <summary>
// 		/// 사용 안함
// 		/// </summary>
// 		/// <param name="at"></param>
// 		/// <param name="name"></param>
// 		/// <param name="password"></param>
// 		/// <param name="level"></param>
// 		/// <param name="gid"></param>
// 		/// <param name="pid"></param>
// 		/// <returns></returns>
// 		bool AddUser(AuthTicket at, String name, String password, int level, string gid, string pid);
// 
// 		/// <summary>
// 		/// 사용 안함
// 		/// </summary>
// 		/// <param name="at"></param>
// 		/// <param name="id"></param>
// 		/// <returns></returns>
// 		bool DeleteUser(AuthTicket at, int id);
//         
// 		/// <summary>
// 		/// 사용 안함
// 		/// </summary>
// 		/// <param name="at"></param>
// 		/// <returns></returns>
// 		AuthTicket GetLevel(AuthTicket at);
// 		
// 		/// <summary>
// 		/// 사용 안함
// 		/// </summary>
// 		/// <param name="at"></param>
// 		/// <returns></returns>
// 		bool EditUser(AuthTicket at, int id, String password, int level, string gid, string pid);
//         
// 		/// <summary>
// 		/// 사용 안함
// 		/// </summary>
// 		/// <param name="at"></param>
// 		/// <returns></returns>
// 		DataSet.usersDataTable GetUsersList(AuthTicket at);
		#endregion
    }

    public interface IDataInfoList
    {
        #region 템플릿 테이터 정보
        
        ServerDataSet.DataInfoDataTable GetDatainfoList(long user_id, string template_id);
        bool AddDataInfo(string id, long user_id, string template_id, string title);
        bool DelDataInfo(string id);

        #endregion 템플릿 테이터 정보
    }

    public interface IDataFieldList
    {
        ServerDataSet.Data_FieldDataTable GetDataFieldList(string datainfo_id);
        ServerDataSet.Data_FieldDataTable GetDatabyDataid(string id);
        bool AddDataField(string id, string datainfo_id, string field_name, string[] value);
        bool DelDataFieldByid(string id); // 목록중 하나하나를 지우기 위해.
        bool DelDataFieldByDatainfoid(string datainfo_id); // datainfo의 목록을 지울때 한꺼번에 해당 목록을 지우때 사용.
        bool EditDataField(string id, string datainfo_id, string[] value);

    }

    public interface IContentsList
    {
        ServerDataSet.ContentsDataTable GetDatabyDataFieldid(string datafield_id);
        ServerDataSet.ContentsDataTable GetDataByID(string id);
        bool AddContets(string id, string datafield_id, string type, string value, string file_nm, string path, string desc, string datainfo_id);
        bool DeletebyContentsId(string id); // 목록중 하나하나를 지우기 위해.
        bool DeletebyDataFieldid(string datafield_id); // datainfo의 목록을 지울때 한꺼번에 해당 목록을 지우때 사용.
        bool DeletebyDataInfoid(string datainfo_id);
        bool UpdateContents(string id, string type, string value, string file_nm, string path);
        bool UpdateContents(string value, string file_nm, string path, string id);

    }

    public interface IResultDataList
    {
        ServerDataSet.resultdatasDataTable GetResultData(long date, string player_id, string datafield_id);
        bool AddResultData(long date, string player_id, string datafield_id, string values);
        bool UpdateResultData(long date, string player_id, string datafield_id, string values);
        ServerDataSet.resultdatasDataTable GetResultDatabyDatafieldid(long from_date, long end_date, string player_id, string datafield_id);
        ServerDataSet.resultdatasDataTable GetResultDatabyTemplateid(long from_date, long end_date, string player_id, string template_id);
        bool DeleteByBetweenDate(long from_date, long end_date);
        bool DeleteByPlayerid(string plaer_id);

    }
}
