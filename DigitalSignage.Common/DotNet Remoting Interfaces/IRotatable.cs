﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media.Media3D;
using System.Windows.Media;
using System.Windows;

namespace DigitalSignage.Common
{
	/// <summary>
	/// 디자이너 사용
	/// </summary>
    public interface IDecoratable
    {
        void SetBorderBrush(Brush borderbrush);
        void SetBorderThickness(Thickness borderThickness);
        void SetBackround(Brush background);
        void SetCornerRadius(double cornerRadius);
        Brush GetBorderBrush();
        Brush GetBackgound();
        Thickness GetBorderThickness();
        double GetCornerRadius();
    }

	/// <summary>
	/// 디자이너 사용
	/// </summary>
	public interface IShape
    {
        void SetStroke(Brush brush);
        void SetFill(Brush brush);
        void SetStrokeDashArray(int[] sdarray);
        void SetStrokeDashCap(string strokedashcap);
        void SetStrokeThickness(double strokethickness);
        Brush GetStroke();
        Brush GetFill();
        int[] GetStrokeDashArray();
        string GetStrokeDashCap();
        double GetStrokeThickness();
    }

	/// <summary>
	/// 디자이너 사용
	/// </summary>
	public interface ITextContainer
    {
        void SetFontSize(double fontsize);
        void SetFontBrush(Brush fontcolor);
        void SetFontFamily(FontFamily family);
        void SetFontWeight(FontWeight fontWeight);
        double GetFontSize();
        Brush GetFontBrush();
        FontFamily GetFontFamily();
        FontWeight GetFontWeight();

		void SetDescFontSize(double fontsize);
		void SetDescFontBrush(Brush fontcolor);
		void SetDescFontFamily(FontFamily family);
		void SetDescFontWeight(FontWeight fontWeight);
		double GetDescFontSize();
		Brush GetDescFontBrush();
		FontFamily GetDescFontFamily();
		FontWeight GetDescFontWeight();
    }
}