﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DigitalSignage.ServerDatabase;

namespace DigitalSignage.Common
{
	/// <summary>
	/// 플레이어마다 다운로드를 시작하는 시간대를 지정할 수 있도록 제공하는 인터페이스
	/// </summary>
	public interface IDownloadTimer
    {
		/// <summary>
		/// 해당 플레이어의 다운로드 시간대를 조회한다.
		/// </summary>
		/// <param name="pid">플레이어 아이디</param>
		/// <returns>시간대 데이터 정보</returns>
		ServerDataSet.DownloadTime_Players_ASNDataTable GetDataByPID(string pid);

		/// <summary>
		/// 해당 그룹의 다운로드 시간대를 조회한다.
		/// </summary>
		/// <param name="pid">그룹 아이디</param>
		/// <returns>시간대 데이터 정보</returns>
		ServerDataSet.DownloadTime_Groups_ASNDataTable GetDataByGID(string gid);
				
		/// <summary>
		/// 상위 그룹의 다운로드 시간대 정보를 조회한다.
		/// </summary>
		/// <param name="gid">그룹 아이디</param>
		/// <returns>시간대 데이터 정보</returns>
		ServerDataSet.DownloadTime_Groups_ASNDataTable GetDataByParents(string gid);
		
		/// <summary>
		/// 다운로드 시간대를 추가한다.
		/// </summary>
		/// <param name="pid">플레이어 아이디</param>
		/// <param name="start">시작 시간</param>
		/// <param name="end">끝 시간</param>
		/// <returns>-1 실패</returns>
		int InsertDownloadTimeToPlayer(string pid, string start, string end);

		/// <summary>
		/// 다운로드 시간대를 추가한다.
		/// </summary>
		/// <param name="pid">그룹 아이디</param>
		/// <param name="start">시작 시간</param>
		/// <param name="end">끝 시간</param>
		/// <returns>-1 실패</returns>
		int InsertDownloadTimeToGroup(string pid, string start, string end);
		
		/// <summary>
		/// 다운로드 시간 정보를 삭제한다.
		/// </summary>
		/// <param name="pid">플레이어</param>
		/// <returns>-1 실패</returns>
		int DeleteTimesFromPID(string pid);

		/// <summary>
		/// 다운로드 시간 정보를 삭제한다.
		/// </summary>
		/// <param name="gid">그룹</param>
		/// <returns>-1 실패</returns>
		int DeleteTimesFromGID(string gid);

    }
}
