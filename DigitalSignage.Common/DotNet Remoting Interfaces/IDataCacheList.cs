﻿using System;
using DigitalSignage.ServerDatabase;
using System.Xml;

namespace DigitalSignage.Common
{
	/// <summary>
	/// 서버의 캐시 데이터를 얻어오는 닷넷 리모팅을 위한 인터페이스
	/// </summary>
    public interface IDataCacheList
    {
		/// <summary>
		/// 현재 제품 코드를 얻어오는 함수
		/// </summary>
		/// <returns>제품 코드를 반환한다.</returns>
		SerialKey.NESerialKey._ProductCode GetProductCode();

		/// <summary>
		/// 메타 정보의 XmlNode를 가져온다.
		/// </summary>
		/// <param name="ts">Client의 TimeStamp 정보</param>
		/// <returns>내려줄 메타 태그 정보가 있다면 XmlNode를 반환한다.</returns>
		string GetMetaTags(long ts);

		/// <summary>
		/// 해당하는 플레이어 ID에 해당하는 플레이어와 상위 그룹 중 최신 타임 스템프를 가져온다.
		/// </summary>
		/// <param name="pid">플레이어 ID</param>
		/// <returns></returns>
		long GetRecentTimestamp(string pid);

		/// <summary>
		/// user ID와 password를 조합하여 해당 User의 XML Node를 가져온다.
		/// </summary>
		/// <param name="id">사용자 아이디</param>
		/// <param name="pwd">사용자 패스워드 해시값</param>
		/// <returns>해당 USER 정보와 하위 USER 및 ROLE, PERMISSION을 포함한 XML NODE를 반환한다. 단, 해당하는 ID와 PWD가 일치 하지 않는 경우 NULL을 반환한다.</returns>
		String GetUserNode(string id, string pwd);
		
		/// <summary>
		/// user ID에 해당하는 모든 그룹 및 플레이어 노드 정보를 반환하는 함수.
		/// </summary>
		/// <param name="userID">사용자 아이디</param>
		/// <returns>user ID에 해당하는 모든 그룹, 하위 그룹 및 하위 플레이어 노드 정보를 반환한다.</returns>
		String GetNetworkTreeByUser(string userID);

		/// <summary>
		/// gid에 해당하는 그룹 및 하위 그룹, 하위 플레이어의 XML NODE 정보를 반환하는 함수.
		/// </summary>
		/// <param name="gid">그룹 아이디</param>
		/// <returns>gid에 해당하는 그룹 및 하위 그룹, 하위 플레이어의 XML NODE 정보를 반환.</returns>
		String GetGroupNode(string gid);
		
		/// <summary>
		/// pid에 해당하는 플레이어의 XML NODE 정보를 반환하는 함수
		/// </summary>
		/// <param name="pid">플레이어 아이디</param>
		/// <returns>pid에 해당하는 플레이어의 XML NODE 정보를 반환한다.</returns>
		String GetPlayerNode(string pid);

		/// <summary>
		/// gid에 해당하는 그룹의 상위 그룹의 컨텐츠 서버 정보를 가져온다.
		/// </summary>
		/// <param name="gid">그룹아이디</param>
		/// <returns>컨텐츠 서버의 정보를 반환하며, 포맷은 "IP|UID|PWD|PATH"와 같다. (구분자 '|') </returns>
		String GetInheritedFTPInfo(string gid);
		
		/// <summary>
		/// parent_gid에 해당하는 그룹 Depth를 반환한다
		/// </summary>
		/// <param name="parent_gid">그룹 아이디</param>
		/// <returns>그룹 Depth, 작을 수록 최상위</returns>
		int GetGroupDepth(string parent_gid);
		
		/// <summary>
		/// pid에 해당하는 상위 그룹의 Depth를 반환한다.
		/// </summary>
		/// <param name="pid">플레이어 아이디</param>
		/// <returns>pid에 해당하는 상위 그룹의 Depth를 반환</returns>
		int GetPlayerDepth(string pid);

		/// <summary>
		/// 플레이어를 새로 추가한다.
		/// </summary>
		/// <param name="gid">상위 그룹 아이디</param>
		/// <param name="name">플레이어 이름</param>
		/// <param name="host">플레이어 컴퓨터 이름</param>
		/// <returns>플레이어 아이디</returns>
		string AddPlayer(string gid, string name, string host);

		///// <summary>
		///// 플레이어를 새로 추가한다.
		///// </summary>
		///// <param name="gid">상위 그룹 아이디</param>
		///// <param name="name">플레이어 이름</param>
		///// <param name="host">플레이어 컴퓨터 이름</param>
		///// <param name="nPowerType">전원 관리 타입</param>
		///// <param name="authID">AMT 계정</param>
		///// <param name="authPass">AMT 패스워드</param>
		///// <returns>성공 여부</returns>
		//string AddPlayer(string gid, string name, string host, int nPowerType, string authID, string authPass);

		/// <summary>
		/// 플레이어를 기존정보를 유지하고 파라미터 정보를 수정한다.
		/// </summary>
		/// <param name="pid">수정할 플레이어 아이디</param>
		/// <param name="gid">그룹 아이디</param>
		/// <param name="name">플레이어 이름</param>
		/// <returns>성공 여부</returns>
		bool UpdatePlayer(string pid, string gid, string name);

		///// <summary>
		///// 플레이어 기존 정보를 유지하고 파라미터 정보를 수정한다.
		///// </summary>
		///// <param name="pid">수정할 플레이어 아이디</param>
		///// <param name="gid">그룹 아이디</param>
		///// <param name="name">플레이어 이름</param>
		///// <param name="nPowerType">전원 관리 타입</param>
		///// <param name="authID">AMT 계정</param>
		///// <param name="authPass">AMT 패스워드</param>
		///// <returns>성공 여부</returns>
		//bool UpdatePlayer(string pid, string gid, string name, int nPowerType, string authID, string authPass);
		
		/// <summary>
		/// 플레이어를 삭제한다.
		/// </summary>
		/// <param name="pid">플레이어 아이디</param>
		/// <returns>성공 여부</returns>
		bool DeletePlayer(string pid);

		/// <summary>
		/// 그룹을 새로 추가한다.
		/// </summary>
		/// <param name="name">그룹 이름</param>
		/// <param name="source">컨텐츠 서버 주소</param>
		/// <param name="username">컨텐츠 서버 아이디</param>
		/// <param name="password">컨텐츠 서버 패스워드</param>
		/// <param name="parent_gid">상위 그룹 아이디</param>
		/// <param name="is_inherited">컨텐츠 서버 정보 상위 상속 여부</param>
		/// <param name="depth">그룹의 DEPTH (작을수록 상위)</param>
		/// <param name="pid">플레이어 아이디. (쓰지 않음)</param>
		/// <param name="path">컨텐츠 서버 하위 PATH</param>
		/// <returns>성공 여부</returns>
		string AddGroup(string name, string source, string username, string password, string parent_gid, string is_inherited, int depth, string pid, string path);
		
		/// <summary>
		/// 그룹을 수정한다.
		/// </summary>
		/// <param name="gid">추가할 그룹 아이디</param>
		/// <param name="name">그룹 이름</param>
		/// <param name="source">컨텐츠 서버 주소</param>
		/// <param name="username">컨텐츠 서버 아이디</param>
		/// <param name="password">컨텐츠 서버 패스워드</param>
		/// <param name="parent_gid">상위 그룹 아이디</param>
		/// <param name="is_inherited">컨텐츠 서버 정보 상위 상속 여부</param>
		/// <param name="depth">그룹의 DEPTH</param>
		/// <param name="path">컨텐츠 서버 하위 PATH</param>
		/// <returns>성공 여부</returns>
		bool UpdateGroup(string gid, string name, string source, string username, string password, string parent_gid, string is_inherited, int depth, string path);
		
		/// <summary>
		/// 그룹을 삭제한다.
		/// </summary>
		/// <param name="gid">삭제할 그룹 아이디</param>
		/// <returns>성공 여부</returns>
		bool DeleteGroup(string gid);

        /// <summary>
        /// gid에 해당하는 하위 플레이어의 상세를 가져온다.
        /// </summary>
        /// <param name="gid"></param>
        /// <returns></returns>
        String GetPlayersByGid(string gid);

        /// <summary>
        /// 그룹의 온라인 상태 정보를 조회한다.
        /// </summary>
        /// <param name="gid"></param>
        /// <returns></returns>
        String GetGroupOnlineInfo(string gid);

		/// <summary>
		/// 지역 정보를 가져온다.
		/// </summary>
		/// <param name="geocode"></param>
		/// <returns></returns>
		ServerDatabase.ServerDataSet.geo_masterDataTable GetGeoMasterInformation(int geocode);

		/// <summary>
		/// GeoMaster 정보를 갱신한다.
		/// </summary>
		/// <param name="gid"></param>
		/// <param name="geocode"></param>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <param name="postcode"></param>
		/// <param name="address"></param>
		/// <param name="ref_code"></param>
		/// <returns></returns>
		bool UpdateGeoMasterInformation(string gid, int geocode, float x, float y, string postcode, string address, string ref_code);

		/// <summary>
		/// GeoMaster 정보를 없앤다
		/// </summary>
		/// <param name="gid"></param>
		/// <returns></returns>
		bool RemoveGeoMasterInformation(string gid);


        /// <summary>
        /// 실시간 캡쳐 파일을 기록한다.
        /// </summary>
        /// <param name="pid"></param>
        /// <param name="stream"></param>
        /// <returns></returns>
        bool SetCapture(String pid, byte[] stream);

        /// <summary>
        /// 해당 플레이어에 SMIL을 재생 요청한다.
        /// </summary>
        /// <param name="pid"></param>
        /// <param name="smil"></param>
        /// <param name="isSync"></param>
        /// <returns></returns>
        bool RunSmilToPlayer(string[] pid, string[] smil, bool isSync);

        /// <summary>
        /// 해당 그룹에 SMIL을 재생 요청한다.
        /// </summary>
        /// <param name="gid"></param>
        /// <param name="smil"></param>
        /// <param name="isSync"></param>
        /// <returns></returns>
        bool RunSmilToGroup(string gid, string[] smil, bool isSync);

        /// <summary>
        /// 해당 플레이어에 자막을 재생한다.
        /// </summary>
        /// <param name="pid"></param>
        /// <param name="subtitleText"></param>
        /// <param name="startDT"></param>
        /// <param name="endDT"></param>
        /// <returns></returns>
        bool RunSubtitleToPlayer(string[] pid, string[] subtitleText, string startDT, string endDT);

        /// <summary>
        /// 해당 그룹에 자막을 재생한다.
        /// </summary>
        /// <param name="gid"></param>
        /// <param name="subtitleText"></param>
        /// <param name="startDT"></param>
        /// <param name="endDT"></param>
        /// <returns></returns>
        bool RunSubtitleToGroup(string gid, string[] subtitleText, string startDT, string endDT);

        /// <summary>
        /// 해당 플레이어에 이벤트를 전송한다.
        /// </summary>
        /// <param name="pid"></param>
        /// <param name="eventValues"></param>
        /// <returns></returns>
        bool RunEventToPlayer(string[] pid, string[] eventValues);

        /// <summary>
        /// 해당 그룹에 이벤트를 전송한다.
        /// </summary>
        /// <param name="gid"></param>
        /// <param name="eventValues"></param>
        /// <returns></returns>
        bool RunEventToGroup(string gid, string[] eventValues);
	}
}
