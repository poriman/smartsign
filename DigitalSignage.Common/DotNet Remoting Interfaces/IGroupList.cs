﻿using System;
using DigitalSignage.ServerDatabase;

namespace DigitalSignage.Common
{
	/// <summary>
	/// 그룹 조작에 관련 인터페이스
	/// </summary>
    public interface IGroupList
    {
// 		int AddGroup(ServerDataSet.groupsDataTable newGroup);
// 		int DeleteGroup(ServerDataSet.groupsDataTable group);
// 		int UpdateGroup(ServerDataSet.groupsDataTable group);
// 		ServerDataSet.groupsDataTable GetGroupByID(string id);
		/// <summary>
		/// 해당 플레이어의 상위의 모든 컨텐츠 서버 정보를 XML 형태로 가져온다.
		/// </summary>
		/// <param name="pid">플레이어 아이디</param>
		/// <returns>해당 플레이어의 상위의 모든 컨텐츠 서버 정보를 XML 형태로 가져온다.</returns>
		string GetGroupsXmlByPID(string pid);
		/// <summary>
		/// 해당 플레이어의 상위 그룹 아이디 리스트를 가져온다.
		/// </summary>
		/// <param name="pid">플레이어 아이디</param>
		/// <returns>그룹 아이디 리스트(구분자 '|')</returns>
		string GetGroupListByPID(string pid);
		
		/// <summary>
		/// 해당 그룹의 상위 그룹 리스트를 가져온다.
		/// </summary>
		/// <param name="gid">그룹 아이디</param>
		/// <returns>그룹 아이디 리스트(구분자 '|')</returns>
		string GetGroupListByGID(string gid);

		/// <summary>
		/// 그룹 아이디와 Attribute에 해당하는 Value를 반환하는 함수.
		/// </summary>
		/// <param name="gid">그룹 아이디</param>
		/// <param name="attribute">source, name, password, path 등</param>
		/// <returns>그룹 아이디와 Attribute에 해당하는 Value</returns>
		string GetGroupInfo(string gid, string attribute);

		#region old
//         int AddGroups(DataSet.groupsDataTable newGroups);
//         int UpdateGroups(DataSet.groupsDataTable groups);
//         int DeleteGroups(DataSet.groupsDataTable groups);
// 		DataSet.groupsDataTable GetGroupByID(string id);
// 		DataSet.groupsDataTable GetGroupsByPID(string pid);
// 		DataSet.groupsDataTable GetGroups();
		#endregion
    }
}
