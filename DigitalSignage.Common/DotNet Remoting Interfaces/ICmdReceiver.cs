﻿using System;

namespace DigitalSignage.Common
{
	/// <summary>
	/// 플레이어 사용: 플레이어와 플레이어에이전트 사이의 통신 관련 인터페이스
	/// </summary>
    public interface IPlayerCmdReceiver
    {
        long ProcessSimpleCommand(CmdReceiverCommands command);
        Object ProcessExpandCommand(CmdExReceiverCommands command);

        bool SendControlCommand(CmdExReceiverCommands command, int nValue);
        bool SendEventResponse(String sMetaTags);


        bool SendEvent(String sEventValue);
        bool SendSyncSchedule(String sGroupID);

        byte[] GetScreenCapture();
    }

    /// <summary>
    /// 서버 사용: 서버와 플레이어에이전트 사이의 통신 관련 인터페이스
    /// </summary>
    public interface IServerCmdReceiver
    {
        long ProcessSimpleCommand(CmdReceiverCommands command);
        Object ProcessExpandCommand(CmdExReceiverCommands command);
    }

    /// <summary>
    /// 플레이에이전트 사용: 플레이어와 플레이어에이전트 사이의 통신 관련 인터페이스
    /// </summary>
    public interface IAgentCmdReceiver
    {
        long ProcessSimpleCommand(CmdReceiverCommands command);
        bool SendEvent(String sData);
        bool InsertPlayerError(int error_cd, string err_desc);
        bool IsOnlineToServer();
    }
}

