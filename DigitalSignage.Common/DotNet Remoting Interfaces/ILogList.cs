﻿using System;

using DigitalSignage.ServerDatabase;

namespace DigitalSignage.Common
{
	/// <summary>
	/// 로그 등록에 관한 인터페이스
	/// </summary>
    public interface ILogList
    {
		#region old
		///// <summary>
		///// 시작시간부터 끝시간까지의 로그를 가져온다.
		///// </summary>
		///// <param name="pid">플레이어 아이디</param>
		///// <param name="type">스케줄 타입 (DigitalSignage.Common.TaskCommands)</param>
		///// <param name="category">로그 카테고리 정보 (DigitalSignage.Common.LogCategory)</param>
		///// <param name="from">시작 시간 UTP</param>
		///// <param name="to">끝 시간 UTP</param>
		///// <returns>로그 데이터 정보</returns>
		//ServerDataSet.logsDataTable GetLogsFromTo(string pid, int type, int category, long from, long to);
		
		///// <summary>
		///// 모든 로그를 가져온다.
		///// </summary>
		///// <param name="pid">플레이어 아이디</param>
		///// <param name="type">스케줄 타입 (DigitalSignage.Common.TaskCommands)</param>
		///// <param name="category">로그 카테고리 정보 (DigitalSignage.Common.LogCategory)</param>
		///// <returns>로그 데이터 정보</returns>
		//ServerDataSet.logsDataTable GetAllLogs(string pid, int type, int category);
		
		///// <summary>
		///// 로그를 생성한다.
		///// </summary>
		///// <param name="pid">플레이어 아이디</param>
		///// <param name="type">스케줄 타입 (DigitalSignage.Common.TaskCommands)</param>
		///// <param name="category">로그 카테고리 정보 (DigitalSignage.Common.LogCategory)</param>
		///// <param name="uuid">스케줄의 고유 식별 아이디 (PK)</param>
		///// <param name="name">스케줄 이름</param>
		///// <param name="description">스케줄 내용</param>
		///// <param name="code">에러 코드 (DigitalSignage.Common.ErrorCodes)</param>
		///// <param name="is_ad">광고 여부</param>
		///// <param name="regdate">등록 일자</param>
		///// <returns>성공시 0</returns>
		//int InsertLogs(string pid, int type, int category, string uuid, string name, string description, int code, string is_ad, long regdate);
		
		///// <summary>
		///// 로그를 생성한다.
		///// </summary>
		///// <param name="xmlLogs">로그 정보 XML</param>
		///// <returns></returns>
		//int InsertLogs(string xmlLogs);
		
		///// <summary>
		///// 해당 시간까지의 로그를 삭제한다.
		///// </summary>
		///// <param name="dt">UTP 시간 정보</param>
		///// <returns>성공 시 0</returns>
		//int DeleteLogsBefore(long dt);
		#endregion

		/// <summary>
		/// 로그를 생성한다.
		/// </summary>
		/// <param name="xmlLogs">로그 정보 XML</param>
		/// <returns></returns>
		int InsertDetailLogs(string xmlLogs);

		/// <summary>
		/// 로그를 생성한다.
		/// </summary>
		/// <param name="pid"></param>
		/// <param name="logcd"></param>
		/// <param name="uuid"></param>
		/// <param name="name"></param>
		/// <param name="description"></param>
		/// <param name="error_code"></param>
		/// <param name="start_dt"></param>
		/// <param name="end_dt"></param>
		/// <returns></returns>
		int InsertDetailLog(string pid, int logcd, string uuid, string name, string description, int error_code, long start_dt, long end_dt);
		

		/// <summary>
		/// 로그를 조회한다
		/// </summary>
		/// <param name="player_ids"></param>
		/// <param name="codes"></param>
		/// <param name="from"></param>
		/// <param name="to"></param>
		/// <param name="limit"></param>
		/// <param name="offset"></param>
		/// <returns></returns>
		ServerDataSet.tblLogsDataTable GetDetailLogsFromTo(string[] player_ids, int[] codes, long from, long to, int limit, int offset);

        /// <summary>
        /// 로그를 조회한다
        /// </summary>
        /// <param name="uuid"></param>
        /// <param name="player_ids"></param>
        /// <param name="codes"></param>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="limit"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        ServerDataSet.tblLogsDataTable GetDetailLogsFromTo(string uuid, string[] player_ids, int[] codes, long from, long to, int limit, int offset);

        #region 보고서 관련

        /// <summary>
        /// 타겟 광고 고객 반응 보고 자료를 반환한다.
        /// </summary>
        /// <param name="yyyy"></param>
        /// <param name="mm"></param>
        /// <param name="dd"></param>
        /// <param name="g_name"></param>
        /// <returns></returns>
        ServerDataSet.targetreportsDataTable GetTargetReportDataByYYYYMMDD(int yyyy, int mm, int dd, string g_name);

        /// <summary>
        /// 타겟 광고 고객 반응 보고 자료를 반환한다.
        /// </summary>
        /// <param name="yyyy"></param>
        /// <param name="mm"></param>
        /// <param name="g_name"></param>
        /// <returns></returns>
        ServerDataSet.targetreportsDataTable GetTargetReportDataByYYYYMM(int yyyy, int mm, string g_name);

        /// <summary>
        /// 타겟 광고 고객 반응 보고 자료를 반환한다.
        /// </summary>
        /// <param name="yyyy"></param>
        /// <param name="g_name"></param>
        /// <returns></returns>
        ServerDataSet.targetreportsDataTable GetTargetReportDataByYYYY(int yyyy, string g_name);

        /// <summary>
        /// 재생 통계 보고 자료를 반환한다.
        /// </summary>
        /// <param name="yyyy"></param>
        /// <param name="mm"></param>
        /// <param name="dd"></param>
        /// <returns></returns>
        ServerDataSet.playreportsDataTable GetPlayReportDataByYYYYMMDD(int yyyy, int mm, int dd);

        /// <summary>
        /// 재생 통계 보고 자료를 반환한다.
        /// </summary>
        /// <param name="yyyy"></param>
        /// <param name="mm"></param>
        /// <returns></returns>
        ServerDataSet.playreportsDataTable GetPlayReportDataByYYYYMM(int yyyy, int mm);

        /// <summary>
        /// 재생 통계 보고 자료를 반환한다.
        /// </summary>
        /// <param name="yyyy"></param>
        /// <returns></returns>
        ServerDataSet.playreportsDataTable GetPlayReportDataByYYYY(int yyyy);

        /// <summary>
        /// DID 운영 보고서 자료를 반환한다.
        /// </summary>
        /// <param name="pids"></param>
        /// <param name="yyyy"></param>
        /// <param name="mm"></param>
        /// <param name="dd"></param>
        /// <returns></returns>
        ServerDataSet.report_running_timeDataTable GetRunningReportDataByYYYYMMDD(String[] pids, int yyyy, int mm, int dd);

        /// <summary>
        /// 일일 스케줄 보고서 자료를 반환한다.
        /// </summary>
        /// <param name="pids"></param>
        /// <param name="yyyy"></param>
        /// <param name="mm"></param>
        /// <param name="dd"></param>
        /// <returns></returns>
        ServerDataSet.report_time_scheduleDataTable GetScheduleReportDataByYYYYMMDD(String[] pids, int yyyy, int mm, int dd);

        /// <summary>
        /// 플레이어의 일일 상태 요약 자료를 반환한다.
        /// </summary>
        /// <param name="pids"></param>
        /// <param name="yyyy"></param>
        /// <param name="mm"></param>
        /// <param name="dd"></param>
        /// <returns></returns>
        ServerDataSet.summary_playersDataTable GetPlayerSummaryDataByYYYYMMDD(String[] pids, int yyyy, int mm, int dd);

        /// <summary>
        /// 통계 수집
        /// </summary>
        /// <param name="startdt"></param>
        /// <param name="enddt"></param>
        /// <param name="bDaily"></param>
        /// <param name="bHourly"></param>
        /// <returns></returns>
        bool MakeStatistics(DateTime startdt, DateTime enddt, bool bDaily, bool bHourly);

        #endregion
    }
}