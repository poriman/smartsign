﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigitalSignage.Common
{
	#region Logging 
	public delegate void GeneralInfoLogCB(string title, TaskCommands type, String message);
    public delegate void GeneralErrorLogCB(string title, TaskCommands type, ErrorCodes code, String message);

	public delegate void LogProcceedCB(String name, long start_dt, long end_dt);
	public delegate void LogStartedCB(String name);
	public delegate void LogEndedCB(String name);
	public delegate void LogErrorCB(String name, int code, String description);

    public delegate void TaskInfoLogCB(string title, string uuid, TaskCommands type, String message);
    public delegate void TaskErrorLogCB(string title, string uuid, TaskCommands type, ErrorCodes code, String message);

	/// <summary>
	/// 로그 카테고리 정보
	/// </summary>
	public enum LogCategory
	{
		CatProcceed = 0,
		CatError,
		CatDownload,
		CatEventOccuredProcceed
	};

	/// <summary>
	/// 로그 타입 정보
	/// </summary>
	public enum LogType : int
	{
		/// <summary>
		/// 처리 (재생) 시작
		/// </summary>
		LogType_Processing = 1,
		/// <summary>
		/// 처리 (재생) 완료
		/// </summary>
		LogType_Proceed = 2,
		/// <summary>
		/// 실패
		/// </summary>
		LogType_Failed = 4,

		/// <summary>
		/// 다운로드 관련
		/// </summary>
		LogType_Download = 8,
		/// <summary>
		/// 이벤트 관련
		/// </summary>
		LogType_Event = 16,
		/// <summary>
		/// 광고 관련
		/// </summary>
		LogType_Ad = 32,

		/// <summary>
		/// 반복 스케줄 관련
		/// </summary>
 		LogType_RollingSched = 64,
		/// <summary>
		/// 시간 스케줄 관련
		/// </summary>
		LogType_TimeSched = 128,
		/// <summary>
		/// 자막 스케줄 관련
		/// </summary>
		LogType_SubtitleSched = 256,
 		/// <summary>
 		/// 제어 스케줄 관련
 		/// </summary>
		LogType_ControlSched = 512,
		/// <summary>
		/// 터치 스케줄 관련
		/// </summary>
		LogType_TouchSched = 1024,
		/// <summary>
		/// 긴급 스케줄 관련
		/// </summary>
		LogType_UrgentSched = 2048,
        /// <summary>
        /// 이벤트 스케줄 관련
        /// </summary>
        LogType_EventSched = 32768, 
		/// <summary>
		/// 접속 관련
		/// </summary>
        LogType_Connection = 4096,
        /// <summary>
        /// 모니터 접속 관련
        /// </summary>
        LogType_MonConnection = 65536,
		/// <summary>
		/// 매니저 작업 생성/수정/삭제
		/// </summary>
        LogType_Managing = 8192,
        /// <summary>
        /// 타겟 광고 로그 조회
        /// </summary>
        LogType_TargetAd = 16384,
		/// <summary>
        /// 기타 (131072, 262144, 524288, 1048576, 2097152)
		/// </summary>
        LogType_Etc = 2097152,

	};

	#endregion

	/// <summary>
	/// 표출 관련 플래그
	/// </summary>
	public enum ShowFlag : int
	{
		/// <summary>
		/// 스케줄 표출하지 않음
		/// </summary>
		Show_DoNotShow = 0,
		/// <summary>
		/// 이벤트가 아닌 경우 표출
		/// </summary>
		Show_NonEvent = 1,
		/// <summary>
		/// 이벤트인 경우 표출
		/// </summary>
		Show_Event = 2,
		/// <summary>
		/// 중복을 허용
		/// </summary>
		Show_Allow_Duplication = 4,
		/// <summary>
		/// 모든 경우 해당
		/// </summary>
		Show_Default = 127,
        /// <summary>
        /// 무한 반복
        /// </summary>
        Show_Infinity = 128,
        /// <summary>
        /// 동기 스켸줄 - Master
        /// </summary>
        Show_SyncMaster = 256,
        /// <summary>
        /// 동기 스켸줄 - Slave
        /// </summary>
        Show_SyncSlave = 512,
	};

	/// <summary>
	///	스케줄을 적용할 요일 정보
	/// </summary>
	public enum TaskApplyDayOfWeek
	{
		Day_MON = 1,
		Day_TUE = 2,
		Day_WED = 4,
		Day_THU = 8,
		Day_FRI = 16,
		Day_SAT = 32,
		Day_SUN = 64
	}

    /// <summary>
    ///	파일 전송 폴더 정보
    /// </summary>
    public enum FileTransferInfo:int
    {
        /// <summary>
        /// 내 문서
        /// </summary>
        FILE_MYDOCUMENT = 1,
        /// <summary>
        ///바탕 화면
        /// </summary>
        FILE_DESKTOP = 2,
        /// <summary>
        /// 임시 저장 폴더
        /// </summary>
        FILE_TEMP = 4,
        /// <summary>
        /// 외부 데이터 폴더
        /// </summary>
        FILE_REFDATA = 8,
        /// <summary>
        /// i-Vision 폴더
        /// </summary>
        FILE_IVISION = 16,
        /// <summary>
        /// i-Vision 설치 드라이브
        /// </summary>
        FILE_INSTALLDRIVE = 32,
    }

	/// <summary>
	/// 스케줄 상태 정보
	/// </summary>
    public enum TaskState
    {
		/// <summary>
		/// 대기 상태
		/// </summary>
        StateWait = 0,
		/// <summary>
		/// 플레이어 사용: 현재 사용 중
		/// </summary>
        StateRunning = 1,
		/// <summary>
		/// 플레이어 사용: 사용 끝남
		/// </summary>
        StateProcessed = 2,
		/// <summary>
		/// 삭제 (취소) 상태
		/// </summary>
        StateCanceled = 3,
		/// <summary>
		/// 플레이어 사용: 다운로드 중
		/// </summary>
		StateDownload = 4,
		/// <summary>
		/// 수정된 상태
		/// </summary>
		StateEdit = 5,
		/// <summary>
		/// StateWait상태에서 StateRunning 상태로 변환 전의 과정 
		/// </summary>
		StateProcessing = 6,
    }

	/// <summary>
	/// 제어 스케줄의 종류
	/// </summary>
	public enum TaskControlSchedules
	{
		/// <summary>
		/// 재부팅
		/// </summary>
		CtrlReboot = 0,
		/// <summary>
		/// 전원 켜기
		/// </summary>
		CtrlPowerOn,
		/// <summary>
		/// 전원 종료
		/// </summary>
		CtrlPowerOff,
		/// <summary>
		/// 플레이어 켜기
		/// </summary>
		CtrlPlayerOn,
		/// <summary>
		/// 플레이어 끄기
		/// </summary>
		CtrlPlayerOff,
		/// <summary>
		/// 볼륨의 조정의 시작과 끝을 지정할 수있는 볼륨 제어
		/// </summary>
		CtrlVolume,
		/// <summary>
		/// 스케줄 싱크
		/// </summary>
		CtrlSyncSchedule,
		/// <summary>
		/// 시리얼포트: 전원 켜기
		/// </summary>
		CtrlSerialPowerOn,
		/// <summary>
		/// 시리얼포트: 전원 끄기
		/// </summary>
		CtrlSerialPowerOff,
		/// <summary>
		/// 시리얼포트: 자동 조절
		/// </summary>
		CtrlSerialAuto,
		/// <summary>
		/// 시리얼포트: 일반 모드
		/// </summary>
		CtrlSerialSourceTV,
		/// <summary>
		/// 시리얼포트: RGB 모드
		/// </summary>
		CtrlSerialSourceRGB,
		/// <summary>
		/// 시리얼포트: DVI 모드
		/// </summary>
		CtrlSerialSourceDVI,
		/// <summary>
		/// 시리얼포트: Component 모드
		/// </summary>
		CtrlSerialSourceComponent,
		/// <summary>
		/// 시리얼포트: 볼륨 제어
		/// </summary>
		CtrlSerialVolume,
		/// <summary>
		/// 볼륨 즉시 제어
		/// </summary>
		CtrlVolumeImmediately,
		/// <summary>
		/// 시리얼포트: 즉시 볼륨 제어
		/// </summary>
		CtrlSerialVolumeNow,
        /// <summary>
        /// 이벤트 볼륨 조절
        /// </summary>
        CtrlEventVolume,
        /// <summary>
        /// 자막 설정
        /// </summary>
        CtrlSubtitle,
		/// <summary>
		/// 밝기 조절
		/// </summary>
		CtrlBrightness,
		/// <summary>
		/// 디스크 정리
		/// </summary>
		CtrlClearDisk,
		/// <summary>
		/// 디스크 포맷
		/// </summary>
		CtrlFormatDisk,
        /// <summary>
        /// 파일 전송
        /// </summary>
        CtrlFileTransmit,		
        /// <summary>
        /// 시리얼포트: HDMI 모드
        /// </summary>
        CtrlSerialSourceHDMI,
		/// <summary>
		/// 알수 없음
		/// </summary>
		CtrlUndefined = 255
	};

	/// <summary>
	/// 스케줄의 종류
	/// </summary>
    public enum TaskCommands : int
    {
		/// <summary>
		/// 스케줄 알수 없음
		/// </summary>
        CmdUndefined = 0,
		/// <summary>
		/// 플레이어 사용: 동기화
		/// </summary>
        CmdSynchronizeNow = 1,
		/// <summary>
		/// Reserved
		/// </summary>
        CmdEcho = 2,
		/// <summary>
		/// 시간 스케줄
		/// </summary>
        CmdProgram = 3,
		/// <summary>
		/// 플레이어 사용: 플레이어 초기화
		/// </summary>
        CmdInitPlayer = 4,
		/// <summary>
		/// 플레이어 사용: 로컬 저장소 정리
		/// </summary>
        CmdClearStorage = 5,
		/// <summary>
		/// Reserved: RS232C 제어
		/// </summary>
        CmdWriteRS232 = 6,
		/// <summary>
		/// Reserved
		/// </summary>
        CmdTopProgram = 7,
		/// <summary>
		/// 자막 스케줄
		/// </summary>
        CmdSubtitles = 8,
		/// <summary>
		/// 플레이어 사용: 플레이어 상태 정보 반환
		/// </summary>
		CmdReturnStatus = 9,
		/// <summary>
		/// 반복 스케줄
		/// </summary>
		CmdDefaultProgram = 10,
		/// <summary>
		/// 플레이어 사용: 반복스케줄의 다음 프로그램 전환
		/// </summary>
		CmdNextDefaultProgram = 11,
		/// <summary>
		/// 플레이어 사용: 외부 연동 데이터 다운로드
		/// </summary>
		CmdGetRefData = 12,
		/// <summary>
		/// Reserved
		/// </summary>
		CmdScreen = 13,
		/// <summary>
		/// 제어 스케줄
		/// </summary>
		CmdControlSchedule = 14,
        /// <summary>
        /// 반복 Touch 스케줄
        /// </summary>
        CmdTouchDefaultProgram = 15,
		/// <summary>
		/// 시스템 (로그 사용)
		/// </summary>
		CmdSystem = 16,
		/// <summary>
		/// Add On 스크린 스케줄
		/// </summary>
		CmdAddOnScreen = 17,
		/// <summary>
		/// 긴급 프로그램
		/// </summary>
		CmdUrgentProgram = 18,
		/// <summary>
		/// 긴급 자막
		/// </summary>
		CmdUrgentSubtitle = 19,
        /// <summary>
        /// 이벤트 스케줄
        /// </summary>
        CmdEventSchedule = 20,

        /// <summary>
        /// 이벤트 발생 스케줄
        /// </summary>
        CmdEventOccured = 21,
        /// <summary>
        /// 특정 외부 컨텐츠 다운로드 스케줄
        /// </summary>
        CmdGeRefContents = 22,

        /// <summary>
        /// USB 재생 스크린 스케줄
        /// </summary>
        CmdUsbSchedule = 23,
	};

	/// <summary>
	/// 플레이어 사용: 플레이어 에이전트와 플레이어 간의 통신
	/// </summary>
    public enum CmdReceiverCommands
    {
        CmdEcho = 0,        //echo request
        CmdGetGID,          // Player to Server request: request of player group ID
        CmdGetTimeStamp,    // Player to Server request: get current time in server (in seconds sine Epoch start, UTC)
        CmdGetFreeSpace,    // Server to Player request: get amountof free space in player hard drive (in Kilobytes)
        CmdGetTemperature,  // Server to Player request:
        CmdGetVersion,      // all directions
        CmgGetClientPID,    // Player to Server request: get client PID identifiator (it's searching in database by client IP address)
        CmdGetUptime,       // Server to Player request: get Player uptime (in seconds)
        CmdGetRotation,     // Player to Server request: get player rotation & scalling parametrs
        CmdGetSyncState,    // Player to Server request: is time synchronisatio necessery
        CmdSyncNow,          // Server To Player: synchroinize right now
		CmdGetCpuRate,
		CmdGetMemoryUsage,
		CmdInitializeServer,
		CmdKillPlayer,
        CmdGetServerTimeUtcTicks
    }

	/// <summary>
	/// 플레이어 사용: 플레이어 에이전트와 플레이어 간의 통신
	/// </summary>
	public enum CmdExReceiverCommands
	{
		CmdExCurrentScreen = 0,
		CmdExCurrentSubtitle,
		CmdExIsPlayerOn,
		CmdExSerialStatusPower,
		CmdExSerialStatusVolume,
		CmdExSerialStatusSource,
        CmdExGSResponse,
	}

	/// <summary>
	/// 스크롤링 텍스트 컴포넌트에서 사용하는 스크롤링 방향
	/// </summary>
    public enum ScrollTextDirection
    {
		/// <summary>
		/// 왼쪽에서 오른쪽
		/// </summary>
        LeftToRight,
		/// <summary>
		/// 오른쪽에서 왼쪽
		/// </summary>
        RightToLeft,
		/// <summary>
		/// 위에서 아래
		/// </summary>
        TopToBottom,
		/// <summary>
		/// 아래에서 위
		/// </summary>
        BottomToTop,
        /// <summary>
        /// 플립 전환
        /// </summary>
        Flip
    }

	/// <summary>
	/// Reserved
	/// </summary>
    public enum UserLevels
    {
        UserAdmin = 0,
        UserGroupManager,
        UserView,
        UserMonitor,
        UserScheduler,
        UserNone
    }

	/// <summary>
	/// 파워 관리 타입
	/// </summary>
	public enum PowerMntType : int
	{
		AMT = 1,
		WoL = 2,
	}

    /// <summary>
    /// 타겟 광고 에러 타입 정의
    /// </summary>
    public enum TargetAd_ErrorType : int
    {
        TargetAd_Client_ToUpper = 1,
        TargetAd_Client_ToLower = 2,
        TargetAd_AppServer_ToUpper = 3,
        TargetAd_AppServer_ToLower = 4,
        TargetAd_RelayServer_ToUpper = 5,
        TargetAd_RelayServer_ToLower = 6,
        TargetAd_Device_ToUpper = 7,
        TargetAd_Device_ToLower = 8,
    }

	/// <summary>
	/// 플레이어 타입
	/// </summary>
	public enum PlayerType : int
	{
		/// <summary>
		/// 윈도우 플레이어
		/// </summary>
		P_Type_Windows = 101,
		/// <summary>
		/// 비터치 임베디드 플레이어
		/// </summary>
		P_Type_Embedded_NonTouch = 202,
		/// <summary>
		/// 터치 임베디드 플레이어
		/// </summary>
		P_Type_Embedded_Touch = 201,
		/// <summary>
		/// 리눅스 플레이어
		/// </summary>
		P_Type_Linux = 301,
		/// <summary>
		/// 알수 없는 플레이어
		/// </summary>
		p_Type_Unknown = 9999
	}

    #region NCPU Command Type

    /// <summary>
    /// ODT 커맨드 타입 이너멀레이터
    /// </summary>
    public enum NPCUCommandType : int
    {
        /// <summary>
        /// 접속 커맨드
        /// </summary>
        Connect = 100,
        /// <summary>
        /// 유휴 테스트
        /// </summary>
        KeepAlive = 101,
        /// <summary>
        /// 요청 커맨드
        /// </summary>
        RequestCommand = 102,
        /// <summary>
        /// 반환 커맨드
        /// </summary>
        ResponseCommand = 103,

        Undefined = -1
    }


    #endregion


    /// <summary>
    /// 동적 필드 타입
    /// </summary>
    public enum DynamicFieldType : int
    {
        /// <summary>
        /// 텍스트 입력 필드
        /// </summary>
        InputText = 1,
        /// <summary>
        /// 파일 추가 필드
        /// </summary>
        File = 0,
        /// <summary>
        /// DB 매핑 테이블 목록 필드
        /// </summary>
        ComboBoxList = 2
    }


    /// <summary>
    /// 파일 포맷 타입
    /// </summary>
    public enum FileFormatType : int
    {
        /// <summary>
        /// 이미지
        /// </summary>
        Image = 0,
        /// <summary>
        /// 동영상
        /// </summary>
        Video = 1,
        /// <summary>
        /// 플래쉬
        /// </summary>
        Flash = 2,
        /// <summary>
        /// 기타
        /// </summary>
        Etc = 255
    }

    /// <summary>
    /// 발생한 이벤트 코드 정의
    /// </summary>
    public enum EventCodes : int
    {
        /// <summary>
        /// 알수 없는 이벤트
        /// </summary>
        EventUndefined = 0,
        /// <summary>
        /// XML 이벤트 발생
        /// </summary>
        EventXmlOccurred = 1,
        /// <summary>
        /// Data 이벤트 발생
        /// </summary>
        EventDataOccured = 2,
        /// <summary>
        /// 이벤트 신호 발생
        /// </summary>
        EventSignalOccured = 3,
    }

    /// <summary>
    /// 제어 명령 요청 타입
    /// </summary>
    public enum ControlRequest : int
    {
        /// <summary>
        /// 알수 없음
        /// </summary>
        Undefinded = 0,
        /// <summary>
        /// 전원 켜기
        /// </summary>
        PowerOn = 1,
        /// <summary>
        /// 전원 끄기
        /// </summary>
        PowerOff = 2,
        /// <summary>
        /// 재시작
        /// </summary>
        Restart = 3,
        /// <summary>
        /// 볼륨 조정
        /// </summary>
        PCVol = 4, 
        /// <summary>
        /// 모니터 On
        /// </summary>
        MonPowerOn = 5,
        /// <summary>
        /// 모니터 Off
        /// </summary>
        MonPowerOff = 6,
        /// <summary>
        /// 모니터 볼륨 조정
        /// </summary>
        MonVol = 7,
    }

    /// <summary>
    /// 제어 명령
    /// </summary>
    public enum ControlResponse : int
    {
        /// <summary>
        /// 성공
        /// </summary>
        Success = 0,
        /// <summary>
        /// 실패
        /// </summary>
        Failed = 1,
        /// <summary>
        /// 내부 오류로 실패하였습니다.
        /// </summary>
        Internal_Error = 2,
        /// <summary>
        /// 전원이 이미 켜져 있습니다.
        /// </summary>
        PW_AlreadyOn = 3,
        /// <summary>
        /// 전원이 이미 꺼져 있습니다.
        /// </summary>
        PW_AlreadyOff = 4,
        /// <summary>
        /// 모니터와 시리얼 연결이 되어 있지 않습니다.
        /// </summary>
        MON_NotConnected = 5,
        /// <summary>
        /// 접속되어 있지 않습니다.
        /// </summary>
        NotConnected = 98,
        /// <summary>
        /// 응답시간이 초과하였습니다.
        /// </summary>
        Timeouted = 99,
    }

    /// <summary>
    /// 동적 메타테그 
    /// </summary>
    public enum MetaTagType : int
    {
        /// <summary>
        /// 체크 박스
        /// </summary>
        CHECKBOX = 1,
        /// <summary>
        /// 라이오 박그
        /// </summary>
        RADIOBOX = 2,
        /// <summary>
        /// 콤보박스
        /// </summary>
        COMBOBOX=3,
        /// <summary>
        /// 콤보 박스 & 텍스트 박스
        /// </summary>
        COMBOBOX_with_VALUE=4,
    }

    /// <summary>
    /// 동기화 시간 설정
    /// </summary>
    public enum TimeSyn : int
    {
        /// <summary>
        /// 기본 값
        /// </summary>
        Default = 0,
        /// <summary>
        /// 매시 0분
        /// </summary>
        EveryHour_0 = 1,
        /// <summary>
        /// 매시 10분
        /// </summary>
        EveryHour_10 = 2,
        /// <summary>
        /// 매시 20분
        /// </summary>
        EveryHour_20 = 4,
        /// <summary>
        /// 매시 30분
        /// </summary>
        EveryHour_30 = 8,
        /// <summary>
        /// 매시 40분
        /// </summary>
        EveryHour_40 = 16,
        /// <summary>
        /// 매시 50분
        /// </summary>
        EveryHour_50 = 32,
        
    }
    /// <summary>
    /// 이미지전환 효과
    /// </summary>
    public enum TransformEffect : int
    {
        /// <summary>
        /// 없음
        /// </summary>
        None,
        /// <summary>
        /// 위에서 아래로
        /// </summary>
        TopToBottom,
        /// <summary>
        /// 아래에서 위로
        /// </summary>
        BottomToTop,

        /// <summary>
        /// 왼쪽에서 오른쪽으로
        /// </summary>
        LeftToRight,
        /// <summary>
        /// 오른쪽에서 왼쪽으로
        /// </summary>
        RightToLeft,
        /// <summary>
        /// 페이드인
        /// </summary>
        FadeIn
    }
}


