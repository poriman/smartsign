﻿using System;

using DigitalSignage.DataBase;
using DigitalSignage.ServerDatabase;

namespace DigitalSignage.Common
{
    // interface for работы с очередью заданий
    public interface ITaskList
    {
		long GetTasksTimestamp();

        int AddTask(Task task);
		ServerDataSet.tasksDataTable GetNewTasks(long timestamp, string pid, string gid);
		ServerDataSet.tasksDataTable GetTasksFrom(long from, long till);
        ServerDataSet.tasksDataTable GetTasksGroup( String guuid );
        int DeleteTask(Task task);
        int EditTasks(Task oldTask, Task newTask);
		int GetMinPriorityFromDefaultTasks(string gid);

		int UpdateTask(Task task);
		bool CanAddTaskToGroup(String uuid, long from, long till, long type, string gid);
		bool CanAddTaskToPlayer(String uuid, long from, long till, long type, string pid);
		bool CanDeleteByDuuid(String duuid, string ftpAddress);
		bool CanDeleteByDuuid(String duuid, string ftpAddress, string ftpPath);
	}
}