﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigitalSignage.Common
{
	/// <summary>
	/// SmartSign 에서 사용하는 코드를 관리하는 클래스
	/// </summary>
	public class iVisionCode
	{
		#region 연결 관련
		public static readonly string CONN_SUCCESS = "0000";
		public static readonly string CONN_ERROR_NO_PLAYER = "0001";
		public static readonly string CONN_ERROR_DUPLICATED_PLAYER = "0002";
		public static readonly string CONN_ERROR_EXCESS_OF_ALLOWED_MAX_PLAYER = "0003";
		public static readonly string CONN_ERROR_DISCONNECTED = "0004";
        public static readonly string CONN_ERROR_TIMEOUT_DISCONNECTED_BY_SERVER = "0005";
        #endregion

		#region 기타 기능 관련
		public static readonly string CONF_REFDATA_INFO = "0010";
		public static readonly string CONF_DOWNLOAD_TIME_INFO = "0011";

		public static readonly string CONF_MODIFIED_PLAYER_ID = "0012";
		public static readonly string CONF_MODIFIED_GROUP_ID = "0013";
		public static readonly string CONF_MODIFIED_SERVER_IP = "0014";

		//	미디어 지역 정보 조회
		public static readonly string CONF_MEDIA_REGION_INFO = "0015";

        public static readonly string CONF_REFRESH = "0016";
		
		public static readonly string FUNC_REBOOT = "0020";
		public static readonly string FUNC_POWER_OFF = "0021";
		public static readonly string FUNC_RESTART_PLAYER = "0022";
		public static readonly string FUNC_KILL_AGENT = "0023";
		public static readonly string FUNC_SCHEDULE_REFRESH = "0024";
        public static readonly string FUNC_PCVOLUME = "0025";
        public static readonly string FUNC_MON_POWER_ON = "0026";
        public static readonly string FUNC_MON_POWER_OFF = "0027";
        public static readonly string FUNC_MON_VOLUME = "0028";
        public static readonly string FUNC_REMOTE_CONTROL = "0029";

        public static readonly string GS_RESPONSE = "0030";

        public static readonly string ETRI_RESPONSE_SMIL = "0035";
        public static readonly string ETRI_RESPONSE_SYNC_SMIL = "0036";
        public static readonly string ETRI_RESPONSE_SYNC_LIST = "0037";
        public static readonly string ETRI_RESPONSE_SUBTITLE = "0038";
        public static readonly string ETRI_RESPONSE_EVENT = "0039";

        public static readonly string IIDB_SENDFILE = "0040";
        public static readonly string IIDB_SENDMESSAGE = "0041";

        public static readonly string FUNC_START_CAPTURE = "0050";
        public static readonly string FUNC_END_CAPTURE = "0051";

		#endregion
	}
}
