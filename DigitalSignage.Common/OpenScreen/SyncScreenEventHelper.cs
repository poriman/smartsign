﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigitalSignage.Common.OpenScreen
{
    /// <summary>
    /// 오픈스크린: 동기화 스케줄 이벤트 처리 핼퍼
    /// </summary>
    public class SyncScreenEventHelper
    {
        /// <summary>
        /// 인스턴스
        /// </summary>
        private static SyncScreenEventHelper _instance = null;
        /// <summary>
        /// Lock 인스턴스
        /// </summary>
        private static object lockObj = new object();

        /// <summary>
        /// 생성자
        /// </summary>
        private SyncScreenEventHelper()
        {

        }

        /// <summary>
        /// 인스턴스 
        /// </summary>
        public static SyncScreenEventHelper GetInstance
        {
            get
            {
                lock (lockObj)
                {
                    if (_instance == null)
                    {
                        _instance = new SyncScreenEventHelper();
                    }
                    return _instance;
                }
            }
        }

        /// <summary>
        /// 스크린 이벤트 처리기
        /// </summary>
        public event SyncScreenEventHandler SyncScreenEvent;

        /// <summary>
        /// 이벤트 발생
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="TargetNM"></param>
        /// <param name="Key"></param>
        /// <param name="Value"></param>
        public void RunEvent(object sender, string TargetNM, string Key, string Value)
        {
            if(SyncScreenEvent != null)
            {
                SyncScreenEvent(sender, new SyncScreenEventArgs(TargetNM, Key, Value));
            }
        }

    }

    /// <summary>
    /// 오픈스크린 : 동기화 스크린 이벤트 핸들러
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    public delegate void SyncScreenEventHandler(object sender, SyncScreenEventArgs e);
    
    /// <summary>
    /// 오픈스크린 : 동기화 스크린 이벤트 아규먼트 클래스
    /// </summary>
    public class SyncScreenEventArgs : EventArgs
    {
        /// <summary>
        /// 생성자
        /// </summary>
        public SyncScreenEventArgs(string target, string key, string val) 
        {
            TargetName = target;
            EventKey = key;
            EventValue = val;
        }

        /// <summary>
        /// 타겟 Region 이름
        /// </summary>
        public string TargetName { get; set; }
        /// <summary>
        /// 이벤트 커맨드 키
        /// </summary>
        public string EventKey { get; set; }
        /// <summary>
        /// 이벤트 커맨드 값
        /// </summary>
        public string EventValue { get; set; }

    }
}
