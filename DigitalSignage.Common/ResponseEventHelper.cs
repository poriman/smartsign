﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NLog;

namespace DigitalSignage.Common
{
    /// <summary>
    /// 플래시 이벤트 핸들러
    /// </summary>
    public class ResponseEventHelper
    {
        #region SingleTone
        static object _lockObject = new object();

        static ResponseEventHelper _instance = null;

        /// <summary>
        /// 생성자
        /// </summary>
        private ResponseEventHelper()
        {
        }

        /// <summary>
        /// SingleTone 객체 전달
        /// </summary>
        static public ResponseEventHelper GetInstance
        {
            get
            {
                lock (_lockObject)
                {
                    if (_instance == null)
                    {
                        _instance = new ResponseEventHelper();
                    }
                    return _instance;
                }
            }
        }
        #endregion

        /// <summary>
        /// 로거
        /// </summary>
        private static Logger logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// 서버에서 플레이어로 데이터 전송 이벤트
        /// </summary>
        public event ResponseEventHandler SendMessagServerToPlayer;
        /// <summary>
        /// 메시지 전달 기능 (서버에서 플레이어로)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        public void OnSendMessageServerToPlayer(object sender, ResponseEventArgs args)
        {
            //if(!IsSyncSlave) ScreenStartDT = DateTime.Now;

            if (SendMessagServerToPlayer != null) SendMessagServerToPlayer(sender, args);
        }
    }

    /// <summary>
    /// 플래시 이벤트 핸들러
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="args"></param>
    public delegate void ResponseEventHandler(object sender, ResponseEventArgs args);
    /// <summary>
    /// 서버 이벤트 아규먼트
    /// </summary>
    public class ResponseEventArgs : EventArgs
    {
        /// <summary>
        /// 생성자
        /// </summary>
        public ResponseEventArgs() { }
        /// <summary>
        /// 장비 ID
        /// </summary>
        public string PlayerID { get; set; }
        /// <summary>
        /// 응답 코드
        /// </summary>
        public string ResponseCode { get; set; }
        /// <summary>
        /// 응답 컨텐트
        /// </summary>
        public string Content { get; set; }
    }

}
