﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DigitalSignage.DataBase;
using DigitalSignage.ServerDatabase;

namespace DigitalSignage.Common
{
    public interface IUsersList
    {
		ServerDataSet.rolesDataTable GetAvailableRoleList(string own_role_name, int own_role_level);
		String[] GetGroupList(long user_id);
		String[] GetPlayerList(long user_id);
		bool AddUser(String name, String password, String role_ids, long parent_id, String[] groups, String[] players);
		bool EditUser(long user_id, String password, String role_ids, long parent_id, String[] groups, String[] players);
		bool DeleteUser(long user_id);

		#region old
		bool AddUser(AuthTicket at, String name, String password, int level, string gid, string pid);
        bool DeleteUser(AuthTicket at, int id);
        AuthTicket GetLevel(AuthTicket at);
		bool EditUser(AuthTicket at, int id, String password, int level, string gid, string pid);
        DataSet.usersDataTable GetUsersList(AuthTicket at);
		#endregion
    }
}
