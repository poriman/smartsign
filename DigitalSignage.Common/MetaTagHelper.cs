﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

using NLog;


namespace DigitalSignage.Common
{
    /// <summary>
    /// 메타태그 비교 도우미
    /// </summary>
    public class MetaTagHelper
    {

        private static Logger logger = LogManager.GetCurrentClassLogger();

        static XmlDocument docMetatags = null;
        static object lockObj = new object();

        String _metaTags = String.Empty;

        /// <summary>
        /// 비교 타입
        /// </summary>
        public enum CompType
        {
            /// <summary>
            /// 합집합
            /// </summary>
            comp_Union,
            /// <summary>
            /// 교집합
            /// </summary>
            comp_Intersection
        }
        /// <summary>
        /// 메타 태그 그룹 타입
        /// </summary>
        public enum TagGroupType : int
        {
            /// <summary>
            /// 성별
            /// </summary>
            tagGroup_Sex = 1,
            /// <summary>
            /// 나이
            /// </summary>
            tagGroup_Age = 2,
            /// <summary>
            /// 유종
            /// </summary>
            tagGroup_EnergyType = 3,
            /// <summary>
            /// 주소
            /// </summary>
            tagGroup_Address = 4,
            /// <summary>
            /// 포인트
            /// </summary>
            tagGroup_Point = 5,
            /// <summary>
            /// 프로모션
            /// </summary>
            tagGroup_Promotion = 6,
			/// <summary>
			/// 카테고리
			/// </summary>
			tagGroup_Category = 7,
        }

        public enum TagGroupIIACType : int
        {
            /// <summary>
            /// 
            /// </summary>
            tagGroup_DisplayType = 1,
            tagGroup_ContentType = 2,
            tagGroup_SkipYN = 3,
            tagGroup_SensorYN = 4
        }

        /// <summary>
        /// 생성자
        /// </summary>
        public MetaTagHelper()
        {
            lock (lockObj)
            {
                if (docMetatags == null)
                {
                    try
                    {
                        docMetatags = new XmlDocument();
                        string sSettingPath = AppDomain.CurrentDomain.BaseDirectory + @"settings\";
                        string sMetaTagFilePath = sSettingPath + "meta_tags.xml";
                        logger.Debug("Metatag File Location : " + sMetaTagFilePath);

                        docMetatags.Load(sMetaTagFilePath);
                    }
                    catch { docMetatags = null; }
                }
            }
            Initialize();
        }

        /// <summary>
        /// 생성자: MetaTag 분석
        /// </summary>
        /// <param name="sMetaTags"></param>
        public MetaTagHelper(String sMetaTags)
        {
            lock (lockObj)
            {
                if (docMetatags == null)
                {
                    try
                    {
                        docMetatags = new XmlDocument();
                        string sSettingPath = AppDomain.CurrentDomain.BaseDirectory + @"settings\";
                        string sMetaTagFilePath = sSettingPath + "meta_tags.xml";

                        docMetatags.Load(sMetaTagFilePath);
                    }
                    catch (Exception ex) { logger.Error(ex.ToString()); docMetatags = null; }
                }
            }

            Prepare(sMetaTags);
        }

        /// <summary>
        /// 생성자: SOMO Request / &Point Response 분석
        /// </summary>
        /// <param name="sRequest"></param>
        /// <param name="sResponse"></param>
        public MetaTagHelper(String sRequest, String sResponse)
        {
            lock (lockObj)
            {
                if (docMetatags == null)
                {
                    try
                    {
                        docMetatags = new XmlDocument();
                        string sSettingPath = AppDomain.CurrentDomain.BaseDirectory + @"settings\";
                        string sMetaTagFilePath = sSettingPath + "meta_tags.xml";

                        docMetatags.Load(sMetaTagFilePath);
                    }
                    catch (Exception ex) { logger.Error(ex.ToString()); docMetatags = null; }
                }
            }

            Prepare(sRequest, sResponse);
        }

        Dictionary<String, Object> dicData = new Dictionary<string, object>();

		/// <summary>
		/// 준비 함수: Etri 메타태그 반환 정보 분석
		/// </summary>
		/// <param name="sResponse">남|12|호텔</param>
		/// <returns></returns>
		public bool EtriPrepare(String sResponse)
		{
			if (docMetatags == null) return false;

			Initialize();

			try
			{
				logger.Debug("Start Prepare!");

				String[] arrResponse = sResponse.Split('|');

				XmlNodeList list = docMetatags.SelectNodes("//child::tag_group");

				if (list != null)
				{
					logger.Debug("Select Success");
					foreach (XmlNode gnode in list)
					{
						TagGroupType nID = (TagGroupType)Convert.ToInt32(gnode.Attributes["id"].Value);

						switch (nID)
						{
							case TagGroupType.tagGroup_Sex:
								{
									#region 성별 분석
									//  성별 가져오기
									int nSex = arrResponse[0] == "남" ? 1 : 0;
									if ((nSex % 2) == 1)
									{
										XmlNode node = gnode.SelectSingleNode("child::tag[attribute::value=\"S0\"]");
										if (node != null)
										{
											dicData.Add(gnode.Attributes["name"].Value, node.Attributes["name"].Value);
										}
									}
									else
									{
										XmlNode node = gnode.SelectSingleNode("child::tag[attribute::value=\"S1\"]");
										if (node != null)
										{
											dicData.Add(gnode.Attributes["name"].Value, node.Attributes["name"].Value);
										}
									}
									#endregion
								}
								break;
							case TagGroupType.tagGroup_Age:
								{
									#region 나이 분석
									int nAge = Convert.ToInt32(arrResponse[1]);

									int nIndex = 0;
									foreach (XmlNode node in gnode.ChildNodes)
									{
										int curr_age = 0;
										//  0세부터 20세까지는 10대!
										if (nIndex++ != 0) curr_age = Convert.ToInt32(node.Attributes["value"].Value.Remove(0, 1));
										int next_age = curr_age;
										XmlNode next_node = node.NextSibling;
										try
										{
											next_age = Convert.ToInt32(next_node.Attributes["value"].Value.Remove(0, 1));
										}
										catch { next_age = Int32.MaxValue; }

										if (curr_age <= nAge && nAge < next_age)
										{
											dicData.Add(gnode.Attributes["name"].Value, node.Attributes["name"].Value);
											break;
										}
									}

									#endregion
								}
								break;
							case TagGroupType.tagGroup_Category:
								dicData.Add(gnode.Attributes["name"].Value, arrResponse[2]);
								break;
						}

					}
				}
				return true;
			}
			catch (Exception ex) { logger.Error(ex.ToString()); }

			return false;
		}

        /// <summary>
        /// 준비 함수: SOMO 요청 정보와 &Point 반환 정보 분석
        /// REQUEST의 예> 1000|12345|L1
        /// RESPONSE의 예> 12345|Y|1|78|02|152052|02|152052|50000
        /// 카드번호|등록여부|성별|출생연도|신/구우편번호구분여부|자택우편번호|신/구우편번호구분여부|직장우편번호|포인트
        /// </summary>
        /// <param name="sRequest">SOMO 요청 정보</param>
        /// <param name="sResponse">&Point 반환 정보</param>
        /// <returns></returns>
        public bool Prepare(String sRequest, String sResponse)
        {
            if (docMetatags == null) return false;

            Initialize();

            try
            {
                logger.Debug("Start Prepare!");

                String[] arrRequest = sRequest.Split('|');
                String[] arrResponse = sResponse.Split('|');

                #region 카드 번호 분석
        //        dicData.Add("CardNumber", arrResponse[0]);
                #endregion


                XmlNodeList list = docMetatags.SelectNodes("//child::tag_group");

                if (list != null)
                {
                    logger.Debug("Select Success");
                    foreach (XmlNode gnode in list)
                    {
                        TagGroupType nID = (TagGroupType)Convert.ToInt32(gnode.Attributes["id"].Value);

                        switch (nID)
                        {
                            case TagGroupType.tagGroup_Sex:
                                {
                                    #region 성별 분석
                                    //  성별 가져오기
                                    int nSex = String.IsNullOrEmpty(arrResponse[2]) ? 1 : Convert.ToInt32(arrResponse[2]);
                                    if ((nSex % 2) == 1)
                                    {
                                        XmlNode node = gnode.SelectSingleNode("child::tag[attribute::value=\"S0\"]");
                                        if (node != null)
                                        {
                                            dicData.Add(gnode.Attributes["name"].Value, node.Attributes["name"].Value);
                                        }
                                    }
                                    else
                                    {
                                        XmlNode node = gnode.SelectSingleNode("child::tag[attribute::value=\"S1\"]");
                                        if (node != null)
                                        {
                                            dicData.Add(gnode.Attributes["name"].Value, node.Attributes["name"].Value);
                                        }
                                    }
                                    #endregion
                                }
                                break;
                            case TagGroupType.tagGroup_Age:
                                {
                                    #region 나이 분석
                                    //  나이 가져오기
                                    int nSex = String.IsNullOrEmpty(arrResponse[2]) ? 1 : Convert.ToInt32(arrResponse[2]);
                                    int nTempAge = String.IsNullOrEmpty(arrResponse[3]) ? 90 : Convert.ToInt32(arrResponse[3]);
                                    int nYear = nTempAge;

                                    switch (nSex)
                                    {
                                        case 1:
                                        case 2:
                                            nYear += 1900;
                                            break;
                                        case 3:
                                        case 4:
                                            nYear += 2000;
                                            break;
                                        case 5:
                                        case 6:
                                            nYear += 2100;
                                            break;
                                        case 7:
                                        case 8:
                                            nYear += 2200;
                                            break;
                                        case 9:
                                        case 0:
                                            nYear += 2300;
                                            break;
                                    }

                                    int nAge = DateTime.Now.Year - nYear;

                                    int nIndex = 0;
                                    foreach (XmlNode node in gnode.ChildNodes)
                                    {
                                        int curr_age = 0;
                                        //  0세부터 20세까지는 10대!
                                        if (nIndex++ != 0) curr_age = Convert.ToInt32(node.Attributes["value"].Value.Remove(0, 1));
                                        int next_age = curr_age;
                                        XmlNode next_node = node.NextSibling;
                                        try
                                        {
                                            next_age = Convert.ToInt32(next_node.Attributes["value"].Value.Remove(0, 1));
                                        }
                                        catch { next_age = Int32.MaxValue; }

                                        if (curr_age <= nAge && nAge < next_age)
                                        {
                                            dicData.Add(gnode.Attributes["name"].Value, node.Attributes["name"].Value);
                                            break;
                                        }
                                    }

                                    #endregion
                                }
                                break;
                            case TagGroupType.tagGroup_EnergyType:
                                {
                                    #region 유종 분석
                                    //  유종 가져오기
                                    XmlNode node = gnode.SelectSingleNode("child::tag[attribute::value=\"L" + arrRequest[2] + "\"]");
                                    if (node != null)
                                    {
                                        dicData.Add(gnode.Attributes["name"].Value, node.Attributes["name"].Value);
                                    }
                                    #endregion
                                }
                                break;
                            case TagGroupType.tagGroup_Address:
                                {
                                    #region 주소 분석
                                    //  주소 가져오기
                                    String sPostCode = String.IsNullOrEmpty(arrResponse[5]) ? "100" : arrResponse[5].Substring(0,3);

                                    XmlNode node = gnode.SelectSingleNode("child::tag[attribute::value=\"" + sPostCode + "\"]");
                                    if (node != null)
                                    {
                                        dicData.Add(gnode.Attributes["name"].Value, node.Attributes["name"].Value);
                                    }
                                    #endregion
                                }
                                break;
                            case TagGroupType.tagGroup_Point:
                                {
                                    #region 포인트 분석
                                    //  포인트 가져오기
                                    int nPoint = String.IsNullOrEmpty(arrResponse[8]) ? 0 : Convert.ToInt32(arrResponse[8]);

                                    int nIndex = 0;
                                    foreach (XmlNode node in gnode.ChildNodes)
                                    {
                                        int curr_point = 0;
                                        //  처음은 0포인트부터
                                        if (nIndex++ != 0) Convert.ToInt32(node.Attributes["value"].Value.Remove(0, 1));
                                        int next_point = curr_point;
                                        XmlNode next_node = node.NextSibling;
                                        try
                                        {
                                            next_point = Convert.ToInt32(next_node.Attributes["value"].Value.Remove(0, 1));
                                        }
                                        catch { next_point = Int32.MaxValue; }

                                        if (curr_point <= nPoint && nPoint < next_point)
                                        {
                                            dicData.Add(gnode.Attributes["name"].Value, node.Attributes["name"].Value);
                                            break;
                                        }
                                    }
                                    #endregion
                                }
                                break;
                        }

                    }
                }
                 return true;
            }
            catch (Exception ex) { logger.Error(ex.ToString()); }

            return false;
        }

        public bool Prepare_IIAC(String sRequest, String sResponse)
        {
            if (docMetatags == null) return false;

            Initialize();

            try
            {
                logger.Debug("Start Prepare!");

                String[] arrRequest = sRequest.Split('|');
                String[] arrResponse = sResponse.Split('|');

                #region 카드 번호 분석
                //        dicData.Add("CardNumber", arrResponse[0]);
                #endregion


                XmlNodeList list = docMetatags.SelectNodes("//child::tag_group");

                if (list != null)
                {
                    logger.Debug("Select Success");
                    foreach (XmlNode gnode in list)
                    {
                        TagGroupIIACType nID = (TagGroupIIACType)Convert.ToInt32(gnode.Attributes["id"].Value);

                        switch (nID)
                        {
                            case TagGroupIIACType.tagGroup_DisplayType:
                                {
                                    #region 성별 분석
                                    //  성별 가져오기
                                    int nSex = String.IsNullOrEmpty(arrResponse[2]) ? 1 : Convert.ToInt32(arrResponse[2]);
                                    if ((nSex % 2) == 1)
                                    {
                                        XmlNode node = gnode.SelectSingleNode("child::tag[attribute::value=\"S0\"]");
                                        if (node != null)
                                        {
                                            dicData.Add(gnode.Attributes["name"].Value, node.Attributes["name"].Value);
                                        }
                                    }
                                    else
                                    {
                                        XmlNode node = gnode.SelectSingleNode("child::tag[attribute::value=\"S1\"]");
                                        if (node != null)
                                        {
                                            dicData.Add(gnode.Attributes["name"].Value, node.Attributes["name"].Value);
                                        }
                                    }
                                    #endregion
                                }
                                break;
                            case TagGroupIIACType.tagGroup_ContentType:
                                {
                                    #region 나이 분석
                                    //  나이 가져오기
                                    int nSex = String.IsNullOrEmpty(arrResponse[2]) ? 1 : Convert.ToInt32(arrResponse[2]);
                                    int nTempAge = String.IsNullOrEmpty(arrResponse[3]) ? 90 : Convert.ToInt32(arrResponse[3]);
                                    int nYear = nTempAge;

                                    switch (nSex)
                                    {
                                        case 1:
                                        case 2:
                                            nYear += 1900;
                                            break;
                                        case 3:
                                        case 4:
                                            nYear += 2000;
                                            break;
                                        case 5:
                                        case 6:
                                            nYear += 2100;
                                            break;
                                        case 7:
                                        case 8:
                                            nYear += 2200;
                                            break;
                                        case 9:
                                        case 0:
                                            nYear += 2300;
                                            break;
                                    }

                                    int nAge = DateTime.Now.Year - nYear;

                                    int nIndex = 0;
                                    foreach (XmlNode node in gnode.ChildNodes)
                                    {
                                        int curr_age = 0;
                                        //  0세부터 20세까지는 10대!
                                        if (nIndex++ != 0) curr_age = Convert.ToInt32(node.Attributes["value"].Value.Remove(0, 1));
                                        int next_age = curr_age;
                                        XmlNode next_node = node.NextSibling;
                                        try
                                        {
                                            next_age = Convert.ToInt32(next_node.Attributes["value"].Value.Remove(0, 1));
                                        }
                                        catch { next_age = Int32.MaxValue; }

                                        if (curr_age <= nAge && nAge < next_age)
                                        {
                                            dicData.Add(gnode.Attributes["name"].Value, node.Attributes["name"].Value);
                                            break;
                                        }
                                    }

                                    #endregion
                                }
                                break;
                            case TagGroupIIACType.tagGroup_SkipYN:
                                {
                                    #region 유종 분석
                                    //  유종 가져오기
                                    XmlNode node = gnode.SelectSingleNode("child::tag[attribute::value=\"L" + arrRequest[2] + "\"]");
                                    if (node != null)
                                    {
                                        dicData.Add(gnode.Attributes["name"].Value, node.Attributes["name"].Value);
                                    }
                                    #endregion
                                }
                                break;
                        }
                    }
                }
                return true;
            }
            catch (Exception ex) { logger.Error(ex.ToString()); }

            return false;
        }

        /// <summary>
        /// 준비 함수: 스케줄의 메타 태그 분석
        /// 메타태그의 예> 성별[남자, 여자], 연령[연령 모두], 유종[휘발유], 지역[지역 모두]
        /// </summary>
        /// <param name="sMetaTags"></param>
        /// <returns></returns>
        public bool Prepare(String sMetaTags)
        {
            _metaTags = sMetaTags;

            if (docMetatags == null) return false;

            Initialize();

            try
            {

                string[] arrGroups = sMetaTags.Split(']');
                foreach (String groupmesh in arrGroups)
                {
                    string data = groupmesh.Trim(new char[] { ',', ' ' });

                    if (String.IsNullOrEmpty(data)) continue;

                    string sGroupName = String.Empty;
                    string sTags = String.Empty;
                    string[] arrSplit1 = data.Split('[');
                    try
                    {
                        sGroupName = arrSplit1[0].Trim();
                        sTags = arrSplit1[1].Trim();

                        // string[] arrTags = sTags.Replace(", ", ",").Split(',');
                        dicData.Add(sGroupName, sTags);
                    }
                    catch (Exception ex) { logger.Error(ex.ToString()); }

                }
                return true;
            }
            catch (Exception ex2) { logger.Error(ex2.ToString()); }

            return false;
        }

        /// <summary>
        ///  다른 메타태그와 비교한다.{
        /// </summary>
        /// <param name="desc"></param>
        /// <returns></returns>
        public bool Contain(MetaTagHelper desc)
        {
            return Contain(desc, CompType.comp_Intersection);
        }

        /// <summary>
        /// 다른 메타태그와 비교한다.
        /// </summary>
        /// <param name="dest"></param>
        /// <param name="compareType"></param>
        /// <returns></returns>
        public bool Contain(MetaTagHelper dest, CompType compareType)
        {
            if (dest == null) return false;

            try
            {
                foreach(String key in this.DataDictionary.Keys)
                {
                    try
                    {
                        String sSrcValue = this.DataDictionary[key].ToString();
                        String sDestValue = dest.DataDictionary[key].ToString();
                        String sAllValue = String.Format("{0} 모두", key);

                        //logger.Debug(String.Format("비교 키: {0}, 원본 값: {1}, 대상 값 {2}", key, sSrcValue, sDestValue));

                        if (key.Contains("주소"))
                        {
                            if (sDestValue.Contains(sAllValue) || sDestValue.Contains(sSrcValue))
                            {
                                //logger.Debug("비교 결과 일치 및 포함!");
                                if (compareType == CompType.comp_Union) return true;
                                else continue;
                            }
                            else
                            {
                                String[] arrMetas = sDestValue.Split(',');

                                if (arrMetas.Length > 0)
                                {
                                    bool bContain = false;
                                    foreach (string meta in arrMetas)
                                    {
                                        if (sSrcValue.Contains(meta.Trim()))
                                        {
                                            bContain = true;
                                            break;
                                        }
                                    }

                                    if (bContain)
                                    {
                                        if (compareType == CompType.comp_Union) return true;
                                        else continue;
                                    }
                                }

                                //logger.Debug("비교 결과 불 일치 및 불 포함!");
                                if (compareType == CompType.comp_Union) continue;
                                else return false;
                            }
                        }
                        else
                        {
                            if (sDestValue.Contains(sAllValue) || sDestValue.Contains(sSrcValue))
                            {
                                //logger.Debug("비교 결과 일치 및 포함!");
                                if (compareType == CompType.comp_Union) return true;
                                else continue;
                            }
                            else
                            {
                                //logger.Debug("비교 결과 불 일치 및 불 포함!");
                                if (compareType == CompType.comp_Union) continue;
                                else return false;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.Error(ex.ToString());

                        if (compareType == CompType.comp_Union) continue;
                        else
                            continue;
                    }
                }

                if (compareType == CompType.comp_Union) return false;
                else return true;
            }
            catch {}

            return false;
        }

        /// <summary>
        /// 다른 메타 태그와 혼합한다.
        /// </summary>
        /// <param name="dest"></param>
        /// <returns></returns>
        public String Combine(MetaTagHelper dest)
        {
            if (dest == null) return _metaTags;

            String sReturn = String.Empty;
            try
            {
                foreach (String key in this.DataDictionary.Keys)
                {
                    try
                    {
                        String sSrcValue = this.DataDictionary[key].ToString();
                        String sDestValue = dest.DataDictionary[key].ToString();
                        String sAllValue = String.Format("{0} 모두", key);

                        if (sDestValue.Contains(sAllValue) || sSrcValue.Contains(sAllValue))
                        {
                            sReturn += String.Format("{0} [{1}], ", key, sAllValue);
                            continue;
                        }
                        else
                        {
                            string[] arrDestValues = sDestValue.Split(',');
                            foreach (string item in arrDestValues)
                            {
                                string sTag = item.Trim();
                                if(!sSrcValue.Contains(sTag))
                                {
                                    sSrcValue = String.Format("{0}, {1}", sSrcValue, sTag);
                                }
                            }

                            sReturn += String.Format("{0} [{1}], ", key, sSrcValue);
                        }
                    }
                    catch
                    {
                    }

                }

                return sReturn.Trim(new char[] { ',', ' ' });

            }
            catch { }

            return _metaTags;

        }

        /// <summary>
        /// 초기화
        /// </summary>
        void Initialize()
        {
            dicData.Clear();
        }

        /// <summary>
        /// 정리된 데이터 사전
        /// </summary>
        public Dictionary<String, Object> DataDictionary { get { return dicData; } }


        /// <summary>
        /// 오프라인 스킵 태그가 있는지 검사
        /// </summary>
        /// <returns></returns>
        public bool HasOfflineSkipTag()
        {
            try
            {
                XmlNodeList list = docMetatags.SelectNodes("//child::tag_group");

                if (list != null)
                {
                    logger.Debug("Select Success");
                    String sGroupName = String.Empty;
                    String sSkipName = String.Empty;

                    foreach (XmlNode gnode in list)
                    {
                        TagGroupIIACType nID = (TagGroupIIACType)Convert.ToInt32(gnode.Attributes["id"].Value);

                        if (nID == TagGroupIIACType.tagGroup_SkipYN)
                        {
                            sGroupName = gnode.Attributes["name"].Value;

                            XmlNode node = gnode.SelectSingleNode("child::tag[attribute::value=\"OFFLINE_SKIP\"]");
                            if (node != null)
                            {
                                sSkipName = node.Attributes["name"].Value;

                                return ( sSkipName.Equals(dicData[sGroupName])) ;
                            }

                        }
                    }
                }
            }
            catch { }

            return false;
        }
    }
}
