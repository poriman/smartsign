﻿using System;
using System.Windows.Forms;

namespace DigitalSignage.Common
{
	/// <summary>
	/// 모래시계를 표출하기 위한 클래스로써, using 키워드를 사용하면 된다. using(new WaitCursor()){ ... };
	/// </summary>
	public class WaitCursor : IDisposable
	{
		/// <summary>
		/// 예전 커서를 저장해 둘 저장소
		/// </summary>
		private Cursor m_oldCursor;

		/// <summary>
		/// 생성자로써 예전 커서를 저장해 둔다.
		/// </summary>
		public WaitCursor()
		{
			m_oldCursor = Cursor.Current;
			Cursor.Current = Cursors.WaitCursor;
		}

		/// <summary>
		/// 해제 작업.
		/// </summary>
		public void Dispose()
		{
			Cursor.Current = m_oldCursor;
		}
	}
}
