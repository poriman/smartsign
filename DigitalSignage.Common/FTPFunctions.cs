using System;
using System.IO;
using System.Collections;
using System.Text;
using System.Net;
using System.Security.Cryptography;
using System.Text.RegularExpressions;
using System.Security.Cryptography.X509Certificates;
using System.Security.Authentication;
using System.Net.Security;

using NLog;
using System.Threading;

namespace DigitalSignage.Common
{
	/// <summary>
	/// FTP 관련 상태 정보에 관한 이벤트 핸들러 델리게이트
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="e"></param>
	public delegate void StatusEventHandler(Object sender, StatusEventArgs e);
	
	/// <summary>
	/// FTP 관련 상태 정보에 관한 이벤트 파라메터 클래스
	/// </summary>
	public class StatusEventArgs : EventArgs
	{
		public StatusEventArgs(String originalSource)
		{
			_originalsource = originalSource;
		}

		private object _originalsource = null;
		public Object OriginalSource
		{
			get
			{
				return _originalsource;
			}
			set
			{
				_originalsource = value;
			}
		}
	}
	#region "FTP file info class"
	/// <summary>
	/// Represents a file or directory entry from an FTP listing
	/// </summary>
	/// <remarks>
	/// This class is used to parse the results from a detailed
	/// directory list from FTP. It supports most formats of
	/// </remarks>
	public class FTPfileInfo
	{

		//Stores extended info about FTP file

		#region "Properties"
		public string FullName
		{
			get
			{
				return Path + Filename;
			}
		}
		public string Filename
		{
			get
			{
				return _filename;
			}
		}
		public string Path
		{
			get
			{
				return _path;
			}
		}
		public DirectoryEntryTypes FileType
		{
			get
			{
				return _fileType;
			}
		}
		public long Size
		{
			get
			{
				return _size;
			}
		}
		public DateTime FileDateTime
		{
			get
			{
				return _fileDateTime;
			}
		}
		public string Permission
		{
			get
			{
				return _permission;
			}
		}
		public string Extension
		{
			get
			{
				int i = this.Filename.LastIndexOf(".");
				if (i >= 0 && i < (this.Filename.Length - 1))
				{
					return this.Filename.Substring(i + 1);
				}
				else
				{
					return "";
				}
			}
		}
		public string NameOnly
		{
			get
			{
				int i = this.Filename.LastIndexOf(".");
				if (i > 0)
				{
					return this.Filename.Substring(0, i);
				}
				else
				{
					return this.Filename;
				}
			}
		}
		private string _filename;
		private string _path;
		private DirectoryEntryTypes _fileType;
		private long _size;
		private DateTime _fileDateTime;
		private string _permission;

		#endregion

		/// <summary>
		/// Identifies entry as either File or Directory
		/// </summary>
		public enum DirectoryEntryTypes
		{
			File,
			Directory
		}

		/// <summary>
		/// Constructor taking a directory listing line and path
		/// </summary>
		/// <param name="line">The line returned from the detailed directory list</param>
		/// <param name="path">Path of the directory</param>
		/// <remarks></remarks>
		public FTPfileInfo(string line, string path)
		{
			//parse line
			Match m = GetMatchingRegex(line);
			if (m == null)
			{
				//failed
				throw (new ApplicationException("Unable to parse line: " + line));
			}
			else
			{
				_filename = m.Groups["name"].Value;
				_path = path;

				Int64.TryParse(m.Groups["size"].Value, out _size);
				//_size = System.Convert.ToInt32(m.Groups["size"].Value);

				_permission = m.Groups["permission"].Value;
				string _dir = m.Groups["dir"].Value;
				if (_dir != "" && _dir != "-")
				{
					_fileType = DirectoryEntryTypes.Directory;
				}
				else
				{
					_fileType = DirectoryEntryTypes.File;
				}

				try
				{
					_fileDateTime = DateTime.Parse(m.Groups["timestamp"].Value);
				}
				catch (Exception)
				{
					_fileDateTime = Convert.ToDateTime(null);
				}

			}
		}

		private Match GetMatchingRegex(string line)
		{
			Regex rx;
			Match m;
			for (int i = 0; i <= _ParseFormats.Length - 1; i++)
			{
				rx = new Regex(_ParseFormats[i]);
				m = rx.Match(line);
				if (m.Success)
				{
					return m;
				}
			}
			return null;
		}

		#region "Regular expressions for parsing LIST results"
		/// <summary>
		/// List of REGEX formats for different FTP server listing formats
		/// </summary>
		/// <remarks>
		/// The first three are various UNIX/LINUX formats, fourth is for MS FTP
		/// in detailed mode and the last for MS FTP in 'DOS' mode.
		/// I wish VB.NET had support for Const arrays like C# but there you go
		/// </remarks>
		private static string[] _ParseFormats = new string[] { 
            "(?<dir>[\\-d])(?<permission>([\\-r][\\-w][\\-xs]){3})\\s+\\d+\\s+\\w+\\s+\\w+\\s+(?<size>\\d+)\\s+(?<timestamp>\\w+\\s+\\d+\\s+\\d{4})\\s+(?<name>.+)", 
            "(?<dir>[\\-d])(?<permission>([\\-r][\\-w][\\-xs]){3})\\s+\\d+\\s+\\d+\\s+(?<size>\\d+)\\s+(?<timestamp>\\w+\\s+\\d+\\s+\\d{4})\\s+(?<name>.+)", 
            "(?<dir>[\\-d])(?<permission>([\\-r][\\-w][\\-xs]){3})\\s+\\d+\\s+\\d+\\s+(?<size>\\d+)\\s+(?<timestamp>\\w+\\s+\\d+\\s+\\d{1,2}:\\d{2})\\s+(?<name>.+)", 
            "(?<dir>[\\-d])(?<permission>([\\-r][\\-w][\\-xs]){3})\\s+\\d+\\s+\\w+\\s+\\w+\\s+(?<size>\\d+)\\s+(?<timestamp>\\w+\\s+\\d+\\s+\\d{1,2}:\\d{2})\\s+(?<name>.+)", 
            "(?<dir>[\\-d])(?<permission>([\\-r][\\-w][\\-xs]){3})(\\s+)(?<size>(\\d+))(\\s+)(?<ctbit>(\\w+\\s\\w+))(\\s+)(?<size2>(\\d+))\\s+(?<timestamp>\\w+\\s+\\d+\\s+\\d{2}:\\d{2})\\s+(?<name>.+)", 
            "(?<timestamp>\\d{2}\\-\\d{2}\\-\\d{2}\\s+\\d{2}:\\d{2}[Aa|Pp][mM])\\s+(?<dir>\\<\\w+\\>){0,1}(?<size>\\d+){0,1}\\s+(?<name>.+)" };
		#endregion
	}
	#endregion

	/// <summary>
	/// FTP 디렉토리 업로드 및 다운로드 관련 클래스
	/// </summary>
    public class FTPFunctions
    {
		// !!!!! TODO: class should have Events for handle progress, and progress property
		#region Progress Event

		/// <summary>
		/// 이벤트: 진행률 표시를 위한 이벤트
		/// </summary>
		public event EventHandler OnSetProgress;

		/// <summary>
		/// 이벤트: 각 파일 진행이 완료될때 마다 반환되는 이벤트
		/// </summary>
		public event EventHandler OnStepIt;
		/// <summary>
		/// 이벤트: 진행 정보가 갱신될때 반환되는 이벤트
		/// </summary>
		public event StatusEventHandler OnUpdateLabel;

		/// <summary>
		/// 다운로드 시작 델리게이트 이벤트
		/// </summary>
		public LogStartedCB FileDownloadStarted = null;
		
		/// <summary>
		/// 다운로드 완료 델리게이트 이벤트
		/// </summary>
		public LogEndedCB FileDownloadEnded = null;

		/// <summary>
		/// 다운로드 처리 완료 델리게이트 이벤트
		/// </summary>
		public LogProcceedCB FileDownloadFinished = null;

		/// <summary>
		/// 다운로드 실패 델리게이트 이벤트
		/// </summary>
		public LogErrorCB FileDownloadError = null;

		#endregion

		private static Logger logger = LogManager.GetCurrentClassLogger();
        private int progress;
        private string ftpUserID;
        private string ftpPassword;
        private string ftpServer;
        private string ftpPath;
        private int defaultTimeout = 5000;
        private int defaultRetry = 3;

		private bool use_passive_mode = true;

		private string lasterrormessage = "";

		/// <summary>
		/// 마지막 에러 메시지
		/// </summary>
		public string LastErrorMessage
		{
			get { return lasterrormessage; }
		}

		void ErrorLog(string classname, string functionname, string errorcode, string errormessage)
		{
			lasterrormessage = errormessage;
			logger.Error(String.Format("[{0}][{1}][{2}] {3}", classname, functionname, errorcode, errormessage));
		}

		/// <summary>
		/// 총 진행해야 할 파일 개수
		/// </summary>
		public int ProgressCount
		{
			get { return progress;  }
		}

		/// <summary>
		/// 생성자
		/// </summary>
		/// <param name="ftpServer">Address 정보 IP:Port 또는 IP 또는 DNS</param>
		/// <param name="path">FTP 경로</param>
		/// <param name="user">FTP 사용자 계정</param>
		/// <param name="password">FTP 사용자 암호</param>
		/// <param name="timeout">타임 아웃(밀리세컨드)</param>
        public FTPFunctions( string ftpServer, string path, string user, string password, int timeout )
        {
			progress = 0;
			this.ftpServer = ftpServer;
			ftpUserID = user;
			ftpPassword = password;
			ftpPath = path;
			defaultTimeout = timeout;
        }

		public FTPFunctions(string ftpServer, string path, string user, string password, int timeout, bool passiveMode)
		{
			progress = 0;
			this.ftpServer = ftpServer;
			ftpUserID = user;
			ftpPassword = password;
			ftpPath = path;
			defaultTimeout = timeout;
			use_passive_mode = passiveMode;
		}

		/// <summary>
		/// 접속 가능 여부 반환
		/// </summary>
		/// <returns></returns>
        public bool CheckState()
        {
            int retr = defaultRetry;
			do
			{
                FtpWebRequest reqFTP = null;

                try
				{
					// get file list from ftp project root
					StringBuilder result = new StringBuilder();
                    string urlReq = "ftp://" + ftpServer + "/";

                    reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri(urlReq));
                    {
                        reqFTP.Proxy = null;
                        reqFTP.Timeout = defaultTimeout;
                        reqFTP.UsePassive = use_passive_mode;
                        reqFTP.UseBinary = true;
                        reqFTP.Credentials = new NetworkCredential(ftpUserID, ftpPassword);
                        reqFTP.Method = WebRequestMethods.Ftp.ListDirectory;
                        using (FtpWebResponse responseDetails = (FtpWebResponse)reqFTP.GetResponse())
                        {
                            using (StreamReader readerDetails = new StreamReader(responseDetails.GetResponseStream()))
                            {
                                if (readerDetails != null && readerDetails.ReadToEnd() != null)
                                    return true;

                            }
                        }
                    }
				}
				catch (Exception e)
				{
					logger.Error(e.ToString());
				}
                finally
                {
                    try
                    {
                        reqFTP.Abort();
                    }
                    catch { }
                }

                Thread.Sleep(500);
            } while (--retr > 0);
            return false;
        }

		/// <summary>
		/// 서버 인증 관련 기능
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="certificate"></param>
		/// <param name="chain"></param>
		/// <param name="sslPolicyErrors"></param>
		/// <returns></returns>
		public static bool ValidateServerCertificate(object sender,
		   X509Certificate certificate, X509Chain chain,
		   SslPolicyErrors sslPolicyErrors)
		{
			if (sslPolicyErrors ==
			   SslPolicyErrors.RemoteCertificateChainErrors)
			{
				return false;
			}
			else if (sslPolicyErrors ==
			   SslPolicyErrors.RemoteCertificateNameMismatch)
			{
				System.Security.Policy.Zone z =
				   System.Security.Policy.Zone.CreateFromUrl
				   (((HttpWebRequest)sender).RequestUri.ToString());
				if (z.SecurityZone ==
				   System.Security.SecurityZone.Intranet ||
				   z.SecurityZone ==
				   System.Security.SecurityZone.MyComputer)
				{
					return true;
				}
				return false;
			}
			return true;
		}

		private string[] GetFileList(string path)
        {
			StringBuilder result = new StringBuilder();
			FtpWebRequest reqFTP = null;
			int retr = defaultRetry;
			string urlReq = "ftp://" + ftpServer + "/"+ (ftpPath.Equals("") ? "" : ftpPath + "/") + (path.Equals("") ? "" : path + "/");

			do
			{
                try
                {
                    reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri(urlReq));
                    reqFTP.Proxy = null;
                    reqFTP.Timeout = defaultTimeout;
                    reqFTP.UseBinary = true;
                    reqFTP.UsePassive = use_passive_mode;
                    reqFTP.Credentials = new NetworkCredential(ftpUserID, ftpPassword);
                    reqFTP.KeepAlive = true;
                    reqFTP.Method = WebRequestMethods.Ftp.ListDirectoryDetails;
                    //	hsshin SSL 지원
                    // 				  reqFTP.EnableSsl = false;
                    // 				  
                    // 				   if (reqFTP.EnableSsl) ServicePointManager.ServerCertificateValidationCallback = 
                    // 					  new RemoteCertificateValidationCallback(ValidateServerCertificate);

                    using (FtpWebResponse responseDetails = (FtpWebResponse)reqFTP.GetResponse())
                    {
                        using (StreamReader readerDetails = new StreamReader(responseDetails.GetResponseStream()))
                        {

                            ArrayList filenames = new ArrayList();
                            string line = readerDetails.ReadLine();
                            while (line != null)
                            {
                                FTPfileInfo info = new FTPfileInfo(line, "");
                                if (info.FileType == FTPfileInfo.DirectoryEntryTypes.File)
                                    filenames.Add(info.Filename);

                                line = readerDetails.ReadLine();
                            }
                            string[] res = new string[filenames.Count];
                            for (int i = 0; i < filenames.Count; i++)
                                res[i] = (string)filenames[i];

                            readerDetails.Close();
                            responseDetails.Close();

                            return res;
                        }
                    }
                }
                catch (WebException ex)
                {
                    try
                    {
                        ErrorLog("FTPFunctions", "GetFileList", ((FtpWebResponse)ex.Response).StatusCode.ToString(), ((FtpWebResponse)ex.Response).StatusDescription);
                        ex.Response.Close();
                    }
                    catch { }

                }
                catch (Exception e)
                {
                    ErrorLog("FTPFunctions", "GetFileList", "", e.Message);
                }
                finally
                {
                    try
                    {
                        reqFTP.Abort();
                    }
                    catch { }
                }
                Thread.Sleep(500);
               retr--;
            } while (retr > 0);
            return null;
        }

        private string[] GetDirectoryList(string path)
        {
            StringBuilder result = new StringBuilder();
            FtpWebRequest reqFTP = null;
            int retr = defaultRetry;
			string urlReq = "ftp://" + ftpServer + "/" + (ftpPath.Equals("") ? "" : ftpPath + "/") + (path.Equals("") ? "" : path + "/");

			do
            {
                try
                {
                    reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri(urlReq));
                    reqFTP.Proxy = null;
                    reqFTP.Timeout = defaultTimeout;
                    reqFTP.UseBinary = true;
					reqFTP.UsePassive = use_passive_mode;
                    reqFTP.Credentials = new NetworkCredential(ftpUserID, ftpPassword);
                    reqFTP.Method = WebRequestMethods.Ftp.ListDirectoryDetails;
					reqFTP.KeepAlive = true;

					string[] res = null;
                    using (FtpWebResponse responseDetails = (FtpWebResponse)reqFTP.GetResponse())
					{
						using (StreamReader readerDetails = new StreamReader(responseDetails.GetResponseStream()))
						{
							ArrayList filenames = new ArrayList();
							string line = readerDetails.ReadLine();
							while (line != null)
							{
								FTPfileInfo info = new FTPfileInfo(line, "");
								if (info.FileType == FTPfileInfo.DirectoryEntryTypes.Directory)
									filenames.Add(info.Filename);

								line = readerDetails.ReadLine();
							}
							res = new string[filenames.Count];
							for (int i = 0; i < filenames.Count; i++)
								res[i] = (string)filenames[i];

							readerDetails.Close();
						}
						responseDetails.Close();
					}
                    return res;
                }
                catch (WebException webex)
                {
					if (webex.Response != null)
					{
						logger.Error(webex + " while get list from" + path);
						ErrorLog("FTPFunctions", "GetDirectoryList", ((FtpWebResponse)webex.Response).StatusCode.ToString(), ((FtpWebResponse)webex.Response).StatusDescription);
						webex.Response.Close();
					}
				}
				catch (Exception e)
				{
					logger.Error(e + " while get list from" + path);
					ErrorLog("FTPFunctions", "GetDirectoryList", "", e.Message);
				}
                finally
                {
                    try
                    {
                        reqFTP.Abort();
                    }
                    catch { }
                }

				retr--;
                Thread.Sleep(500);
            } while (retr > 0);
            return null;
        }

        private string GetRemoteHash(string path, string fname)
        {
           string res = "";
           FtpWebRequest reqFTP = null;
           int retr = defaultRetry;
           string reqString = "ftp://" + ftpServer + "/" +
              (ftpPath.Equals("") ? "" : ftpPath + "/") +
              (path.Equals("") ? "" : path + "/") +
              fname + ".md5";
           do
           {
			   try
			   {
				   reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri(reqString));
                   reqFTP.Proxy = null;
                   reqFTP.Method = WebRequestMethods.Ftp.DownloadFile;
				   reqFTP.UsePassive = use_passive_mode;
				   reqFTP.UseBinary = true;
				   reqFTP.Credentials = new NetworkCredential(ftpUserID, ftpPassword);
				   reqFTP.KeepAlive = true;
				   using (FtpWebResponse response = (FtpWebResponse)reqFTP.GetResponse())
				   {
					   using (Stream responseStream = response.GetResponseStream())
					   {
						   using (StreamReader reader = new StreamReader(responseStream))
						   {
							   res = reader.ReadToEnd();
							   reader.Close();
						   }
						   responseStream.Close();
					   }
					   response.Close();
				   }
				   return res.ToUpper();
			   }
			   catch (WebException webex)
			   {
				   if (webex.Response != null)
				   {
					   webex.Response.Close();
					   ErrorLog("FTPFunctions", "GetRemoteHash", ((FtpWebResponse)webex.Response).StatusCode.ToString(), ((FtpWebResponse)webex.Response).StatusDescription);
				   }
				   logger.Error(webex.ToString() + " target=" + path + " - " + fname);
			   }
			   catch (Exception e)
			   {
				   logger.Error(e.Message + " target=" + path + " - " + fname);
				   ErrorLog("FTPFunctions", "GetRemoteHash", "", e.Message);
			   }
               finally
               {
                   try
                   {
                       reqFTP.Abort();
                   }
                   catch { }
               }

              retr--;
              Thread.Sleep(500);
           }
           while (retr > 0);
           return "-"; //can't get hash
        }

        // methof for write file's hash value to FTP server
        private int SetRemoteHash(string path, string fname, string hash)
        {
            FtpWebRequest reqFTP = null;
            int retr = defaultRetry;
            if (hash.Length != 32) throw new Exception("Wrong file hash size");
            string reqString = "ftp://" + ftpServer + "/" +
               (ftpPath.Equals("") ? "" : ftpPath + "/") +
               (path.Equals("") ? "" : path + "/") +
               fname + ".md5";
            do
            {
                try
                {
                    reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri( reqString ));
                    reqFTP.Proxy = null;
                    reqFTP.Method = WebRequestMethods.Ftp.UploadFile;
                    reqFTP.UseBinary = true;
					reqFTP.UsePassive = use_passive_mode;
					reqFTP.KeepAlive = true;
					reqFTP.Credentials = new NetworkCredential(ftpUserID, ftpPassword);
                    reqFTP.ContentLength = 32; //MD5 hash 32 alphanumerical chars

					using (Stream outStream = reqFTP.GetRequestStream())
					{
						logger.Info("Setting hash=\"" + hash + "\"");
						outStream.Write(Encoding.ASCII.GetBytes(hash), 0, 32);

						outStream.Close();
					}

					return 0;
                }
                catch (Exception e)
                {
                    logger.Error(e + " target = " + path + " - " + fname);
					ErrorLog("FTPFunctions", "SetRemoteHash", "", e.Message);

                }
                finally
                {
                    try
                    {
                        reqFTP.Abort();
                    }
                    catch { }
                }

                retr--;
                Thread.Sleep(500);
            }
            while (retr > 0);
            return -1;
        }

        private string GetLocalHash( string fname )
        {
			StringBuilder sb=new StringBuilder();
			try
			{
				if (!File.Exists(fname)) return sb.ToString().ToUpper();

				using (FileStream fs = new FileStream(fname, FileMode.Open))
				{
					MD5 md5 = new MD5CryptoServiceProvider();
					byte[] hash = md5.ComputeHash(fs);

					foreach (byte hex in hash) sb.Append(hex.ToString("x2"));

					fs.Close();
				}

            }
            catch (Exception e)
            {
				ErrorLog("FTPFunctions", "GetLocalHash", "", e.Message);
			}

            return sb.ToString().ToUpper();
        }

        
        private int GetFile( string path, string fname, string localPath, string lfname )
        {
			FtpWebRequest reqFTP = null;
			int retr = defaultRetry;
			string reqString = "ftp://" + ftpServer + "/" +
				(ftpPath.Equals("") ? "" : ftpPath + "/") +
				(path.Equals("") ? "" : path + "/") +
				fname;
			do
			{
				try
				{
					reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri( reqString ));
                    reqFTP.Proxy = null;
                    reqFTP.Method = WebRequestMethods.Ftp.DownloadFile;
					reqFTP.UseBinary = true;
					reqFTP.UsePassive = use_passive_mode;
					reqFTP.KeepAlive = true;
					reqFTP.Timeout = defaultTimeout;
					reqFTP.Credentials = new NetworkCredential ( ftpUserID, ftpPassword );
                    
					using (FtpWebResponse response = (FtpWebResponse)reqFTP.GetResponse())
					{
						using (Stream responseStream = response.GetResponseStream())
						{
							using (FileStream outStream = new FileStream(localPath + "\\" + lfname, FileMode.Create))
							{
								int bufferSize = 65536;
								int readed;
								byte[] buffer = new byte[bufferSize];

								do
								{
									readed = responseStream.Read(buffer, 0, bufferSize);
									outStream.Write(buffer, 0, readed);
								}
								while (readed > 0);

								outStream.Close();
							}

							responseStream.Close();
						}
						response.Close();
					}

					return 0;
				}
				catch (Exception e)
				{
					//	hsshin 파일이 이상함 
					if (FileDownloadError != null) FileDownloadError(lfname, (int)ErrorCodes.IS_ERROR_CODE_NOT_DEFINED, e.Message);

					logger.Error(e.ToString() + " target = " + path + " - " + fname + " loc=" + localPath);
				}
                finally
                {
                    try
                    {
                        reqFTP.Abort();
                    }
                    catch { }
                }


				retr--;
                Thread.Sleep(500);
            } while (retr > 0);

			return -1;
        }

		private long GetFTPFileSize(string remotePath, string remoteName)
		{
			FtpWebRequest reqFTP = null;
			long nSize = -1;
			string reqString = "ftp://" + ftpServer + "/" +
			   (ftpPath.Equals("") ? "" : ftpPath + "/") +
			   (remotePath.Equals("") ? "" : remotePath + "/") +
			   remoteName;
			try
			{
				reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri(reqString));
                reqFTP.Proxy = null;
                reqFTP.Method = WebRequestMethods.Ftp.GetFileSize;
				reqFTP.UseBinary = true;
				reqFTP.UsePassive = use_passive_mode;
				reqFTP.KeepAlive = true;
				reqFTP.Timeout = defaultTimeout;
				reqFTP.Credentials = new NetworkCredential(ftpUserID, ftpPassword);

				using (FtpWebResponse response = (FtpWebResponse)reqFTP.GetResponse())
				{
					nSize = response.ContentLength;
					using (Stream datastream = response.GetResponseStream())
					{
						string result = "";
						using (StreamReader reader = new StreamReader(datastream))
						{
							result = reader.ReadToEnd();
							reader.Close();
						}
						datastream.Close();
					}
					response.Close();
				}

				return nSize;

			}
			catch (WebException webex)
			{
				ErrorLog("FTPFunctions", "GetFileSize", ((FtpWebResponse)webex.Response).StatusCode.ToString(), ((FtpWebResponse)webex.Response).StatusDescription);
                if (webex.Response != null)
                {
                    webex.Response.Close();
                }
			}
			catch (Exception e)
			{
				ErrorLog("FTPFunctions", "GetFileSize", "", e.Message);
            }
            finally
            {
                try
                {
                    reqFTP.Abort();
                }
                catch { }
            }


			return -1;
		}

        public bool FTPFileDownload(string userid, string path, string fileName)
        {
            FtpWebRequest reqFTP = null;
            int retr = defaultRetry;
            string reqString = "ftp://" + ftpServer + "/" + userid + "/" + EncodeTo64M(fileName);
            try
            {
                reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri(reqString));
                reqFTP.Proxy = null;
                reqFTP.Method = WebRequestMethods.Ftp.DownloadFile;
                reqFTP.UseBinary = true;
                reqFTP.KeepAlive = true;
				reqFTP.UsePassive = use_passive_mode;
                reqFTP.Timeout = defaultTimeout;
                reqFTP.Credentials = new NetworkCredential(ftpUserID, ftpPassword);

                using (FtpWebResponse response = (FtpWebResponse)reqFTP.GetResponse())
                {
                    using (Stream responseStream = response.GetResponseStream())
                    {
                        using (FileStream outStream = new FileStream(path + fileName, FileMode.Create))
                        {
                            int bufferSize = 65536;
                            int readed;
                            byte[] buffer = new byte[bufferSize];

                            do
                            {
                                readed = responseStream.Read(buffer, 0, bufferSize);
                                outStream.Write(buffer, 0, readed);
                            }while (readed > 0);

                            outStream.Close();
                        }
                        responseStream.Close();
                    }
                    response.Close();
                }

                return true;
            }
            catch (Exception e)
            {
                if (FileDownloadError != null) FileDownloadError(fileName, (int)ErrorCodes.IS_ERROR_CODE_NOT_DEFINED, e.Message);
                logger.Error(e.ToString() + " target = " + path + " - " + fileName + " loc=" + path);
            }
            finally
            {
                try
                {
                    reqFTP.Abort();
                }
                catch { }
            }

            return false;
        }


        public bool HasFile (string userid, string fileName)
        {
            StringBuilder result = new StringBuilder();
            FtpWebRequest reqFTP;
            int retr = defaultRetry;
            string urlReq = "ftp://" + ftpServer + "/" + userid + "/" + EncodeTo64M(fileName);

            try
            {
                reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri(urlReq));
                reqFTP.Proxy = null;
                reqFTP.Method = WebRequestMethods.Ftp.DownloadFile;
                reqFTP.UseBinary = true;
				reqFTP.UsePassive = use_passive_mode;
                reqFTP.KeepAlive = true;
                reqFTP.Timeout = defaultTimeout;
                reqFTP.Credentials = new NetworkCredential(ftpUserID, ftpPassword);

                return true;

            }
            catch (WebException ex)
            {
                ErrorLog("FTPFunctions", "GetFileList", ((FtpWebResponse)ex.Response).StatusCode.ToString(), ((FtpWebResponse)ex.Response).StatusDescription);
                ex.Response.Close();

            }
            catch (Exception e)
            {
               ErrorLog("FTPFunctions", "GetFileList", "", e.Message);
            }

            return false;
        }

		public int PutFile(string localPath, string fname, string remotePath, string remoteName )
		{
			FtpWebRequest reqFTP = null;
			int retr = defaultRetry;
			string reqString = "ftp://" + ftpServer + "/" +
			   (ftpPath.Equals("") ? "" : ftpPath + "/") +
			   (remotePath.Equals("") ? "" : remotePath + "/") +
			   remoteName;
			do
			{
				try
				{
					FileInfo fInfo = new FileInfo(localPath + "\\" + fname);
					using (FileStream inStream = new FileStream(localPath + "\\" + fname, FileMode.Open))
					{
						reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri(reqString));
                        reqFTP.Proxy = null;
                        reqFTP.Method = WebRequestMethods.Ftp.UploadFile;
						reqFTP.KeepAlive = true;
						reqFTP.UseBinary = true;
						reqFTP.UsePassive = use_passive_mode;
						reqFTP.Timeout = defaultTimeout;
						reqFTP.Credentials = new NetworkCredential(ftpUserID, ftpPassword);
						reqFTP.ContentLength = fInfo.Length;
						using (Stream outStream = reqFTP.GetRequestStream())
						{
							long size = fInfo.Length;
							int bufferSize = 65536;
							int readed;
							byte[] buffer = new byte[bufferSize];

							while (size > 0)
							{
								readed = inStream.Read(buffer, 0, bufferSize);
								size -= readed;
								outStream.Write(buffer, 0, readed);
							}
							outStream.Close();
						}
						inStream.Close();
					}

					return 0;
				}
				catch(WebException webex)
				{
					if(null != webex.Response)
					{
						switch(((FtpWebResponse)webex.Response).StatusCode)
						{
							case FtpStatusCode.NotLoggedIn:
								ErrorLog("FTPFunctions", "PutFile", ((FtpWebResponse)webex.Response).StatusCode.ToString(), ((FtpWebResponse)webex.Response).StatusDescription);
								return -1;
							default:
								ErrorLog("FTPFunctions", "PutFile", ((FtpWebResponse)webex.Response).StatusCode.ToString(), ((FtpWebResponse)webex.Response).StatusDescription);
								break;
						}

                        webex.Response.Close();
					}
				}
				catch (Exception e)
				{
					ErrorLog("FTPFunctions", "PutFile", "", e.Message);
				}
                finally
                {
                    try
                    {
                        reqFTP.Abort();
                    }
                    catch { }
                }


				retr--;
                Thread.Sleep(500);
            } while (retr > 0);
			return -1;
		}

        public int FTPCreateDirectory(string dname, string remotePath)
        {
            return CreateDirectory(dname, remotePath);
        }

        private int CreateDirectory(string dname, string remotePath)
        {
            FtpWebRequest reqFTP = null;
            int retry = defaultRetry;
            do
            {
				try
				{
					reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri("ftp://" + ftpServer + "/" +
					   (remotePath.Equals("") ? "" : remotePath + "/") + dname));
                    reqFTP.Proxy = null;
                    reqFTP.Method = WebRequestMethods.Ftp.MakeDirectory;
					reqFTP.Credentials = new NetworkCredential(ftpUserID, ftpPassword);
					reqFTP.KeepAlive = true;
					reqFTP.UsePassive = use_passive_mode;
					reqFTP.Timeout = defaultTimeout;
					
					GetResponseString(reqFTP);

					return 0;
				}
				catch (WebException webe)
				{
					if (webe.Response != null)
					{
						switch(((FtpWebResponse)webe.Response).StatusCode)
						{
							case FtpStatusCode.ActionNotTakenFileUnavailable:
								//	디렉토리가이 이미 있는 경우.. 걍 넘어가자.
								webe.Response.Close();
								return 0;
							case FtpStatusCode.NotLoggedIn:
							ErrorLog("FTPFunctions", "CreateDirectory", ((FtpWebResponse)webe.Response).StatusCode.ToString(), ((FtpWebResponse)webe.Response).StatusDescription);
								webe.Response.Close();
								return -1;
							default:
								ErrorLog("FTPFunctions", "CreateDirectory", ((FtpWebResponse)webe.Response).StatusCode.ToString(), ((FtpWebResponse)webe.Response).StatusDescription);
								break;
						}

						webe.Response.Close();
					}

				}
				catch (Exception e)
				{
					ErrorLog("FTPFunctions", "CreateDirectory", "", e.Message);
					logger.Error("Target=" + dname + " - " + remotePath);
				}
                finally
                {
                    try
                    {
                        reqFTP.Abort();
                    }
                    catch { }
                }

                retry--;
                Thread.Sleep(500);
            }
            while ( retry > 0 );
            return -1;
        }

		private string GetResponseString(FtpWebRequest request)
		{
			string result = "";

			using (FtpWebResponse response = (FtpWebResponse)request.GetResponse())
			{
				long nSize = response.ContentLength;
				using (Stream datastream = response.GetResponseStream())
				{
					using (StreamReader reader = new StreamReader(datastream))
					{
						result = reader.ReadToEnd();
						reader.Close();
					}
					datastream.Close();
				}
				response.Close();
			}


			return result;
		}
        static public string EncodeTo64M(string source)
        {
            byte[] array = System.Text.ASCIIEncoding.UTF8.GetBytes(source);
            string res = System.Convert.ToBase64String( array );
            res = res.Replace('/', '_');
            return res;
        }

        static public string DecodeFrom64M(string encodedData)
        {
            encodedData = encodedData.Replace('_', '/');
            byte[] encoded = System.Convert.FromBase64String(encodedData);
            return System.Text.ASCIIEncoding.UTF8.GetString( encoded);
        }


		/// <summary>
		/// 로컬 디렉토리를 삭제
		/// </summary>
		/// <param name="dir">삭제할 로컬 디렉토리</param>
		/// <param name="recursive">Reserved</param>
		/// <returns>성공 여부</returns>
        public static bool RecursiveDelete(string dir, bool recursive)
        {
            bool status = false;
            //always use a try...catch to deal 
            //with any exceptions that may occur
            try
            {
                //first make sure the directory exists
                //if it doesn't and we try to delete the 
                //file an exception is thrown
                if (!System.IO.Directory.Exists(dir))
                {
                    status = false;
                    //throw the exception to be dealt with later
                    throw new DirectoryNotFoundException(dir + " cannot be found! Please retry your request");
                }
                else
                {
                    string[] files = Directory.GetFiles(dir);
                    foreach (string file in files) File.Delete(file);

                    //get all the subdirectories in the given directory
                    string[] dirs = Directory.GetDirectories(dir);
                    foreach (string sub in dirs) RecursiveDelete(sub, recursive);

                    Directory.Delete(dir);

                    //return a true status
                    status = true;
                }
            }
            catch
            {
                status = false;
            }
            return status;
        }

		/// <summary>
		/// 로컬 디렉토리 복사
		/// </summary>
		/// <param name="source">복사할 원본 디렉토리</param>
		/// <param name="dest">대상 디렉토리</param>
		/// <returns></returns>
        public static bool RecursiveCopy(string source, string dest)
        {
            bool status = false;
            //always use a try...catch to deal 
            //with any exceptions that may occur
            try
            {
                //first make sure the directory exists
                //if it doesn't and we try to delete the 
                //file an exception is thrown
                if (!System.IO.Directory.Exists(dest))
                    Directory.CreateDirectory(dest);
                string[] files = Directory.GetFiles(source);
                foreach (string file in files) 
					File.Copy(file, dest + "\\"+System.IO.Path.GetFileName(file), true);

                //get all the subdirectories in the given directory
                string[] dirs = Directory.GetDirectories(source);
				foreach (string sub in dirs)
				{
					if (false == (status = RecursiveCopy(sub, dest + "\\" + System.IO.Path.GetFileName(sub))))
						return false;
				}

                //return a true status
                status = true;
            }
            catch
            {
                status = false;
            }
            return status;
        }

		/// <summary>
		/// 로컬 디렉토리의 총 파일 개수 반환
		/// </summary>
		/// <param name="path">로컬 디렉토리</param>
		/// <returns>총 파일 개수</returns>
		public int GetRecursiveFileCount(string path)
		{
			int nTotalCount = 0;
			try
			{
				string[] files = Directory.GetFiles(path);
				nTotalCount += files.Length;

				string[] dirs = Directory.GetDirectories(path);
				foreach (string sub in dirs)
				{
					nTotalCount += GetRecursiveFileCount(sub);
				}

			}
			catch (Exception e)
			{
				ErrorLog("FTPFunctions", "GetRecursiveFileCount", "", e.Message);
				return 0;
			}
			progress = nTotalCount;
			if (OnSetProgress != null) OnSetProgress(this, null);

			return nTotalCount;
		}

		/// <summary>
		/// FTP에 디렉토리가 존재하는 지 검사
		/// </summary>
		/// <param name="dname">FTP 디렉토리 이름</param>
		/// <returns>존재 여부</returns>
		public bool FTPHasDirectory(string dname)
		{
            FtpWebRequest reqFTP = null;
            int retry = defaultRetry;
			bool bRet = false;

            try
            {
                reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri("ftp://" + ftpServer + "/" +
                   (ftpPath.Equals("") ? "" : ftpPath + "/")));
                reqFTP.Proxy = null;
                reqFTP.Method = WebRequestMethods.Ftp.ListDirectory;
                reqFTP.Credentials = new NetworkCredential(ftpUserID, ftpPassword);
                reqFTP.KeepAlive = true;
                reqFTP.UsePassive = use_passive_mode;
                reqFTP.Timeout = defaultTimeout;

                using (FtpWebResponse responseDetails = (FtpWebResponse)reqFTP.GetResponse())
                {
                    using (StreamReader readerDetails = new StreamReader(responseDetails.GetResponseStream()))
                    {
                        string line = readerDetails.ReadLine();
                        while (line != null)
                        {
                            if (line.Contains(dname))
                            {
                                bRet = true;
                                line = readerDetails.ReadToEnd();
                                break;
                            }
                            else
                                line = readerDetails.ReadLine();
                        }
                        readerDetails.Close();
                    }
                    responseDetails.Close();
                }
                return bRet;
            }
            catch { }
            finally
            {
                try
                {
                    reqFTP.Abort();
                }
                catch { }
            }

            return bRet;

		}

		/// <summary>
		/// FTP에 선택한 디렉토리를 복사한다.
		/// </summary>
		/// <param name="source">복사할 로컬 디렉토리</param>
		/// <param name="dest">대상 FTP 디렉토리</param>
		/// <param name="ftp">Reserved</param>
		/// <param name="overwrite">파일이 존재하면 덮어쓸지 여부</param>
		/// <returns>성공 여부</returns>
        public bool FTPRecursiveCopy(string source, string dest, string ftp, bool overwrite)
        {
            bool status = false;

			if (CreateDirectory(dest.ToLower(), ftpPath + (ftp.Equals("") ? "" : "/" + ftp)) != 0)
			{
				throw new Exception(lasterrormessage);
			}
			ftp += (ftp.Equals("") ? "" : "/") + dest.ToLower();
            string[] files = Directory.GetFiles(source);

            foreach (string file in files)
            {
                string filename = System.IO.Path.GetFileName(file);
				if (OnUpdateLabel != null) OnUpdateLabel(this, new StatusEventArgs(filename));

				//	thumbs.db는 추가하지 않는다.
				if (!filename.ToLower().Contains("thumbs.db"))
				{
					if (!overwrite)
					{
						long size = GetFTPFileSize(ftp, EncodeTo64M(filename));

						FileInfo fInfo = new FileInfo(file);

						if (fInfo.Length != size)
						{
							if (-1 == PutFile(source, filename, ftp, EncodeTo64M(filename)))
								return false;
						}
					}
					else if (-1 == PutFile(source, filename, ftp, EncodeTo64M(filename)))
						return false;

					if (-1 == SetRemoteHash(ftp, EncodeTo64M(filename), GetLocalHash(file)))
						return false;
				}

				//	hsshin 이벤트 구현
				if (OnStepIt != null) OnStepIt(this, null);

			}

            //get all the subdirectories in the given directory
            string[] dirs = Directory.GetDirectories(source);
			foreach (string sub in dirs)
			{
				if (false == FTPRecursiveCopy(sub, System.IO.Path.GetFileName(sub), ftp, overwrite))
					return false;
			}
            //return a true status
            status = true;

			
			return status;
        }

		/// <summary>
		/// FTP 디렉토리 안의 총 파일 개수 반환
		/// </summary>
		/// <param name="source">FTP 디렉토리</param>
		/// <returns>총 파일 개수</returns>
		public int GetFTPRecursiveFileCount(string source)
		{
			int nTotalCount = 0;
			try
			{
				string[] files = GetFileList(source);
				nTotalCount += files.Length;

				string[] dirs = GetDirectoryList(source);
				foreach (string sub in dirs)
				{
					nTotalCount += GetFTPRecursiveFileCount(source+"/"+sub);
				}

			}
			catch (Exception e)
			{
				return 0;
			}
			progress = nTotalCount;
			if (OnSetProgress != null) OnSetProgress(this, null);

			return nTotalCount;
		}

		/// <summary>
		/// FTP 디렉토리의 내용 전부를 로컬 디렉토리로 다운로드
		/// </summary>
		/// <param name="source">FTP 디렉토리 이름</param>
		/// <param name="dest">대상 로컬 디렉토리</param>
		/// <returns>성공 여부</returns>
		public bool FTPRecursiveDownload(string source, string dest)
        {
            bool status = false;
            try
            {
                // creating local directory
                if (!System.IO.Directory.Exists(dest))
                    Directory.CreateDirectory(dest);

                string[] files = GetFileList(source);
                foreach (string file in files)
                {
					//	hsshin 이벤트 구현
					if (OnStepIt != null) OnStepIt(this, null);

	
                    if (!file.EndsWith(".md5")) continue;
                    string filename = file.Substring(0, file.IndexOf(".md5"));
                    string filenameDcd = DecodeFrom64M(filename);

					//	thumbs.db는 추가하지 않는다.
					if (filenameDcd.ToLower().Contains("thumbs.db"))
						continue;

					//	hsshin 이벤트 구현
					if (OnUpdateLabel != null) OnUpdateLabel(this, new StatusEventArgs(filenameDcd));

                    string remoteHash = GetRemoteHash(source, filename);
                    string localHash = GetLocalHash(dest + filenameDcd);
                    
                    int rtr = defaultRetry;
					DateTime _started = DateTime.UtcNow;
					//if (FileDownloadStarted != null) FileDownloadStarted(filenameDcd, false);

					while (!remoteHash.Equals(localHash))
                    {
						logger.Info("Downloading data file " + filenameDcd);
						if (GetFile(source, filename, dest, filenameDcd) == 0)
                            localHash = GetLocalHash(dest + filenameDcd);
                        if (rtr-- < 0) // attempting to reload
                        {
							//	hsshin 파일이 이상함 
							if (FileDownloadError != null) FileDownloadError(filenameDcd, (int)ErrorCodes.IS_ERROR_INVALID_DST_FILE, "Wrong hash for file");

							logger.Log(LogLevel.Warn, "Wrong hash for file " + filenameDcd);
							throw new Exception();
                        }
                    }
					//if (FileDownloadEnded != null) FileDownloadEnded(filenameDcd, false);
					if (FileDownloadFinished != null) FileDownloadFinished(filenameDcd, TimeConverter.ConvertToUTP(_started.ToUniversalTime()), TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()));
                }

                //get all the subdirectories in the given directory
                string[] dirs = GetDirectoryList(source);
                foreach (string sub in dirs)
                {
					if (false == FTPRecursiveDownload(source + "\\" + sub, dest + sub + "\\"))
					{
						throw new Exception();
					}
                }
                status = true;
            }
            catch (Exception ex)
            {
				ErrorLog("FTPFunctions", "FTPRecursiveDownload", "", ex.Message);
				status = false;
            }
            return status;
        }

		/// <summary>
		/// FTP 위치의 대상 파일을 삭제
		/// </summary>
		/// <param name="dest">대상 FTP FullPath</param>
		/// <returns>성공 여부</returns>
		public bool FTPDeleteFile(string dest)
		{
			FtpWebRequest reqFTP = null;
			int retr = defaultRetry;
			string reqString = "ftp://" + ftpServer + "/" +
			   (ftpPath.Equals("") ? "" : ftpPath + "/") + dest;
			do
			{
				try
				{
					reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri(reqString));
                    reqFTP.Proxy = null;
                    reqFTP.Method = WebRequestMethods.Ftp.DeleteFile;
					reqFTP.KeepAlive = true;
					reqFTP.UsePassive = use_passive_mode;
					reqFTP.Timeout = defaultTimeout;
					reqFTP.Credentials = new NetworkCredential(ftpUserID, ftpPassword);

					using (FtpWebResponse response = (FtpWebResponse)reqFTP.GetResponse())
					{
						using (Stream ftpStream = response.GetResponseStream())
						{
							ftpStream.Close();
						}
						response.Close();
					}
					return true;
				}
				catch (WebException e)
				{
					logger.Error(e.Status.ToString() + ": " + e.Message);
                    if (e.Response != null)
                        e.Response.Close();
				}
				catch (Exception ee)
				{
					logger.Error(ee.ToString());
				}
                finally
                {
                    try
                    {
                        reqFTP.Abort();
                    }
                    catch { }
                }

                Thread.Sleep(500);
            } while (retr-- > 0);

			return false;
		}

		/// <summary>
		/// FTP 디렉토리 삭제
		/// </summary>
		/// <param name="dest">대상 FTP 디렉토리</param>
		/// <returns>성공 여부</returns>
		public bool FTPDeleteDirectory(string dest)
		{
			FtpWebRequest reqFTP = null;
			int retr = defaultRetry;
			string reqString = "ftp://" + ftpServer + "/" +
			   (ftpPath.Equals("") ? "" : ftpPath + "/") +
			   (dest.Equals("") ? "" : dest + "/");
			do
			{
				try
				{
					reqFTP = (FtpWebRequest)FtpWebRequest.Create(new Uri(reqString));
                    reqFTP.Proxy = null;
                    reqFTP.Method = WebRequestMethods.Ftp.RemoveDirectory;
					reqFTP.KeepAlive = true;
					reqFTP.UsePassive = use_passive_mode;
					reqFTP.Timeout = defaultTimeout;
					reqFTP.Credentials = new NetworkCredential(ftpUserID, ftpPassword);

					using (FtpWebResponse response = (FtpWebResponse)reqFTP.GetResponse())
					{
						using (Stream ftpStream = response.GetResponseStream())
						{
							ftpStream.Close();
						}
						response.Close();
					}
					return true;
				}
				catch (WebException e)
				{
					logger.Error(e.Status.ToString() + ": " + e.Message);
                    if (e.Response != null)
                        e.Response.Close();
				}
				catch (Exception ee)
				{
					logger.Error(ee.ToString());
                }
                finally
                {
                    try
                    {
                        reqFTP.Abort();
                    }
                    catch { }
                }

                Thread.Sleep(500);
            } while (retr-- > 0);

			return false;
		}

		/// <summary>
		/// FTP 디렉토리 내부 모두 삭제
		/// </summary>
		/// <param name="dest">대상 FTP 디렉토리</param>
		/// <returns>성공 여부</returns>
		public bool FTPRecursiveDelete(string dest)
		{
			bool status = false;
			try
			{
				//get all the subdirectories in the given directory
				string[] dirs = GetDirectoryList(dest);
				foreach (string sub in dirs)
				{
					if (false == FTPRecursiveDelete(dest + "\\" + sub))
						return false;
				}


				string[] files = GetFileList(dest);
				foreach (string file in files)
				{
					if (false == FTPDeleteFile(dest + "\\" + file))
						return false;
				}

				if (false == FTPDeleteDirectory(dest))
					return false;

				status = true;
			}
			catch (Exception ex)
			{
				ErrorLog("FTPFunctions", "FTPRecursiveDownload", "", ex.Message);
				status = false;
			}
			return status;
		}

		/// <summary>
		/// 로컬과 FTP 대상 폴더와 동일한지 체크
		/// </summary>
		/// <param name="source">로컬 디렉토리</param>
		/// <param name="dest">FTP 디렉토리</param>
		/// <returns>동일하면 True 동일하지 않으면 False</returns>
		public bool FTPRecursiveCheck(string source, string dest)
		{
			bool status = false;
			try
			{
				// creating local directory
				if (!System.IO.Directory.Exists(dest))
					return false;

				string[] files = GetFileList(source);
				foreach (string file in files)
				{
					if (!file.EndsWith(".md5")) continue;
					string filename = file.Substring(0, file.IndexOf(".md5"));
					string filenameDcd = DecodeFrom64M(filename);

					string remoteHash = GetRemoteHash(source, filename);
					string localHash = GetLocalHash(dest + filenameDcd);

					int rtr = defaultRetry;
					if (!remoteHash.Equals(localHash))
						return false;
				}

				//get all the subdirectories in the given directory
				string[] dirs = GetDirectoryList(source);
				foreach (string sub in dirs)
				{
					if (false == FTPRecursiveCheck(source + "\\" + sub, dest + sub + "\\"))
					{
						throw new Exception();
					}
				}
				status = true;
			}
			catch (Exception ex)
			{
				ErrorLog("FTPFunctions", "FTPRecursiveDownload", "", ex.Message);
				status = false;
			}
			return status;
		}
    }
}
