using System;
using System.Xml;  
using System.Configuration;
using System.Reflection;

using NLog;
using System.IO;

namespace DigitalSignage.Common
{
    public class AsyncConfigSettings
    {
        static object objLock = new object();

        public static string config_path;
		private static Logger logger = LogManager.GetCurrentClassLogger();

		public AsyncConfigSettings(string config_path)
        {
			AsyncConfigSettings.config_path = config_path;
        }

        #region Setting Base

        /// <summary>
        /// 설정을 읽어온다.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string ReadSetting(string key)
        {
            lock (objLock)
            {
                string res = "";
                // load config document for current assembly
                XmlDocument doc = loadConfigDocument();
                // retrieve appSettings node
                XmlNode node = doc.SelectSingleNode("//appSettings");
                if (node == null) return res;
                try
                {
                    // select the 'add' element that contains the key
                    XmlElement elem = (XmlElement)node.SelectSingleNode(string.Format("//add[@key='{0}']", key));
                    if (elem != null) res = elem.GetAttribute("value");
                }
                catch (Exception ex)
                {
                    logger.Error(ex.ToString());

                    throw;
                }
                return res;
            }
        }

        /// <summary>
        /// 설정을 기록한다.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public static void WriteSetting(string key, string value)
        {
            lock (objLock)
            {
                // load config document for current assembly
                XmlDocument doc = loadConfigDocument();

                // retrieve appSettings node
                XmlNode node = doc.SelectSingleNode("//appSettings");

                if (node == null)
                    throw new InvalidOperationException("appSettings section not found in config file.");

                int nRetry = 0;
                bool bRet = false;

                do
                {

                    try
                    {
                        // select the 'add' element that contains the key
                        XmlElement elem = (XmlElement)node.SelectSingleNode(string.Format("//add[@key='{0}']", key));

                        if (elem != null)
                        {
                            // add value for key
                            elem.SetAttribute("value", value);
                        }
                        else
                        {
                            // key was not found so create the 'add' element 
                            // and set it's key/value attributes 
                            elem = doc.CreateElement("add");
                            elem.SetAttribute("key", key);
                            elem.SetAttribute("value", value);
                            node.AppendChild(elem);
                        }

                        SaveDocToFile(doc);

                        break;
                    }
                    catch (Exception ex)
                    {
                        logger.Error(ex.ToString());
                    }
                    System.Threading.Thread.Sleep(100);

                } while (++nRetry < 3 && !bRet);
            }
        }

        /// <summary>
        /// 설정 파일을 저장한다.
        /// </summary>
        /// <param name="doc"></param>
        public static void SaveDocToFile(XmlDocument doc)
        {
            using (System.Threading.Mutex mutex = new System.Threading.Mutex(false, "SmartSignConfigs"))
            {
                try
                {
                    mutex.WaitOne();

                    int nRetry = 0;
                    String sFilePath = getConfigFilePath();

                    bool bSaved = false;
                    do
                    {
                        try
                        {
                            #region DISK 다이렉트로 파일 쓰기
                            using (FileStream fsFileStream = new FileStream(sFilePath + ".tmp", FileMode.Create, FileAccess.Write, FileShare.None, 1024, FileOptions.WriteThrough))
                            {
                                XmlWriterSettings settings = new XmlWriterSettings();
                                settings.Indent = true;
                                settings.OmitXmlDeclaration = false;
                                using (XmlWriter xmlWrite = XmlWriter.Create(fsFileStream, settings))
                                {
                                    doc.Save(xmlWrite);
                                }
                            }
                            #endregion

                            bSaved = true;
                        }
                        catch { bSaved = false; }

                        System.Threading.Thread.Sleep(10);

                    } while (bSaved == false && ++nRetry < 3);

                    if (bSaved)
                    {
                        System.IO.File.Copy(sFilePath + ".tmp", sFilePath, true);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    mutex.ReleaseMutex();
                    mutex.Close();
                }

            }

        }

        /// <summary>
        /// 세팅을 삭제한다.
        /// </summary>
        /// <param name="key"></param>
        public static void RemoveSetting(string key)
        {
            lock (objLock)
            {
                // load config document for current assembly
                XmlDocument doc = loadConfigDocument();

                // retrieve appSettings node
                XmlNode node = doc.SelectSingleNode("//appSettings");

                try
                {
                    if (node == null)
                        throw new InvalidOperationException("appSettings section not found in config file.");
                    else
                    {
                        // remove 'add' element with coresponding key
                        node.RemoveChild(node.SelectSingleNode(string.Format("//add[@key='{0}']", key)));

                        SaveDocToFile(doc);
                    }
                }
                catch (NullReferenceException e)
                {
                    throw new Exception(string.Format("The key {0} does not exist.", key), e);
                }
            }
        }

        /// <summary>
        /// 설정 파일을 읽어온다.
        /// </summary>
        /// <returns></returns>
        private static XmlDocument loadConfigDocument()
        {
            XmlDocument doc = null;

            int nRetry = 0;
            bool bRet = false;

            do
            {
                try
                {
                    doc = new XmlDocument();

                    using (System.Threading.Mutex mutex = new System.Threading.Mutex(false, "SmartSignConfigs"))
                    {
                        try
                        {
                            mutex.WaitOne();

                            doc.Load(getConfigFilePath());
                        }
                        catch (Exception ex)
                        {
                            try
                            {
                                doc.Load(getConfigFilePath() + ".tmp");
                            }
                            catch { throw ex; }
                        }
                        finally
                        {
                            mutex.ReleaseMutex();
                            mutex.Close();
                        }
                    }

                    XmlNode node = doc.SelectSingleNode("//appSettings");

                    bRet = true;

                    break;
                }
                catch (XmlException exml)
                {
                    logger.Error(exml.ToString());

                    //if (exml.Message.Contains("Root element is missing"))
                    {
                        // Create a document type node and  
                        // add it to the document.
                        doc = new XmlDocument();

                        XmlDocumentType doctype;
                        doctype = doc.CreateDocumentType("configuration", null, null, null);
                        doc.AppendChild(doctype);

                        // Create the root element and 
                        // add it to the document.
                        XmlNode root = doc.AppendChild(doc.CreateElement("configuration"));
                        root.AppendChild(doc.CreateElement("appSettings"));

                        SaveDocToFile(doc);
                    }
                }
                catch (System.IO.FileNotFoundException e)
                {
                    logger.Error(e.ToString());

                    doc = new XmlDocument();

                    // Create a document type node and  
                    // add it to the document.
                    XmlDocumentType doctype;
                    doctype = doc.CreateDocumentType("configuration", null, null, null);
                    doc.AppendChild(doctype);

                    // Create the root element and 
                    // add it to the document.
                    XmlNode root = doc.AppendChild(doc.CreateElement("configuration"));
                    root.AppendChild(doc.CreateElement("appSettings"));

                    SaveDocToFile(doc);
                }
                catch (System.IO.IOException ioe)
                {
                    logger.Error(ioe.ToString());
                }
                catch (Exception ex)
                {
                    logger.Error(ex.ToString());
                }

                System.Threading.Thread.Sleep(100);

            } while (++nRetry < 3 && !bRet);

            if (!bRet)
            {
                doc = new XmlDocument();

                XmlDocumentType doctype;
                doctype = doc.CreateDocumentType("configuration", null, null, null);
                doc.AppendChild(doctype);

                // Create the root element and 
                // add it to the document.
                XmlNode root = doc.AppendChild(doc.CreateElement("configuration"));
                root.AppendChild(doc.CreateElement("appSettings"));
            }

            return doc;
        }

        #endregion

        private static string getConfigFilePath()
        {
			if (String.IsNullOrEmpty(AsyncConfigSettings.config_path))
				throw new Exception("config path is empty!");

			return AsyncConfigSettings.config_path;
        }
    }
}