﻿using System;
using System.Collections.Generic;
//using System.Linq;
//using System.Text;
using System.IO;


//SQLite & DB routines
using System.Data.Common;

using NLog;
using NLog.Targets;
using NLog.Targets.Wrappers;
using System.Data.OleDb;

namespace DigitalSignage.DataBase
{

    public class LogDBLayer
    {
		private static Logger logger = LogManager.GetCurrentClassLogger();

		//	log semaphore
		private readonly object semaphore_for_log = new object();

		private OleDbConnection connection = null;
        private string dataPath = "";

		private string srcfilename = "";

		public LogDBLayer()
        {
        }

        public int openDB(string dsn)
        {
			try
			{
				logger.Info("Log Database start");
// 

				this.dataPath = dsn;
				OleDbFactory fact = OleDbFactory.Instance;
				connection = (OleDbConnection)fact.CreateConnection();
				connection.ConnectionString = dsn;
				
				srcfilename = dsn;
				
				logger.Info(connection.ConnectionString);

				connection.Open();

				logger.Info("Log Data base opened");

			}
			catch (Exception e)
			{
				logger.Error(e + "");
			}
            return 1;
        }


        public int closeDB()
        {
            connection.Close();
            connection = null;
            return 0;
        }

		public bool CopyDB(string dest)
		{
			lock (semaphore_for_log)
			{
				CompactDB();
				File.Copy(srcfilename, dest, false);

				return true;
			}
		}

		public void CompactDB()
		{
			using (OleDbCommand cmd = connection.CreateCommand())
			{
				cmd.CommandText = "VACUUM";
				cmd.ExecuteNonQuery();
			}
		}
		public LogsDataSetTableAdapters.logmasterTableAdapter CreateLogMasterDA()
        {
			lock (semaphore_for_log)
			{
				LogsDataSetTableAdapters.logmasterTableAdapter da = new LogsDataSetTableAdapters.logmasterTableAdapter();

				da.Connection = connection;
				return da;
			}
        }

		public LogsDataSetTableAdapters.logsTableAdapter CreateLogDA()
        {
			lock (semaphore_for_log)
			{
				LogsDataSetTableAdapters.logsTableAdapter da = new LogsDataSetTableAdapters.logsTableAdapter();

				da.Connection = connection;
				return da;
			}
        }

		public LogsDataSetTableAdapters.client_logsTableAdapter CreateClientLogDA()
		{
			lock (semaphore_for_log)
			{
				LogsDataSetTableAdapters.client_logsTableAdapter da = new LogsDataSetTableAdapters.client_logsTableAdapter();

				da.Connection = connection;
				return da;
			}
		}

		public DbConnection Connection
        {
            get { return connection; }
        }

		public string LogPath
		{
			get { return dataPath; }
			set 
			{ 
				dataPath = value;
				srcfilename = dataPath + "log.db3";
			}
		}

    }
}