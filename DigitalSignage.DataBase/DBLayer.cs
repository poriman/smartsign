﻿using System;
using System.Collections.Generic;
//using System.Linq;
//using System.Text;
using System.IO;


//SQLite & DB routines
using System.Data.Common;
using System.Data.SQLite;

using NLog;
using NLog.Targets;
using NLog.Targets.Wrappers;

namespace DigitalSignage.DataBase
{
	public class DBLayer
    {
		private static Logger logger = LogManager.GetCurrentClassLogger();

        private SQLiteConnection connection = null;
        private string dataPath = "";

        public DBLayer()
        {
        }

        public int openDB(string dataPath)
        {
			try
			{

				logger.Info("Database start");
// 

				this.dataPath = dataPath;
//	20081117 hsshin SQLite Class를 사용하여 DBFactory를 얻어오도록 변경
// 				DbProviderFactory fact = DbProviderFactories.GetFactory("System.Data.SQLite");
				SQLiteFactory fact = new System.Data.SQLite.SQLiteFactory();
				connection = (SQLiteConnection)fact.CreateConnection();
				connection.ConnectionString = "Data Source=" + dataPath + "playlist.db3";
				logger.Info(connection.ConnectionString);

				connection.Open();
				logger.Info("Data base opened");

				try
				{
					using (SQLiteCommand sql = new SQLiteCommand(connection))
					{
						//initializing database structure
						sql.CommandText = "SELECT * FROM tasks";
						sql.ExecuteNonQuery();
					}
					return 0;
				}
				catch (Exception e)
				{
					logger.Warn(e + "");
				}
			}
			catch (System.Data.SQLite.SQLiteException exSQLite)
			{
				logger.Error(exSQLite + "");
			}
			catch (Exception e)
			{
				logger.Error(e + "");
			}
            return 1;
        }

        public int closeDB()
        {
            connection.Close();
            connection = null;
            return 0;
        }

		public DataSetTableAdapters.groupsTableAdapter CreateGroupDA()
        {
			DataSetTableAdapters.groupsTableAdapter da = new DataSetTableAdapters.groupsTableAdapter();
			da.Connection = connection;
            return da;
        }

		public DataSetTableAdapters.tasksTableAdapter CreateTaskDA()
        {
			DataSetTableAdapters.tasksTableAdapter da = new DataSetTableAdapters.tasksTableAdapter();
			da.Connection = connection;
            return da;
        }

		public DataSetTableAdapters.playersTableAdapter CreatePlayersDA()
        {
			DataSetTableAdapters.playersTableAdapter da = new DataSetTableAdapters.playersTableAdapter();
			da.Connection = connection;
            return da;
        }

		public DataSetTableAdapters.usersTableAdapter CreateUsersDA()
        {
			DataSetTableAdapters.usersTableAdapter da = new DataSetTableAdapters.usersTableAdapter();
			da.Connection = connection;
            return da;
        }

		public DataSetTableAdapters.RSCommandsTableAdapter CreateCmdDA()
        {
			DataSetTableAdapters.RSCommandsTableAdapter da = new DataSetTableAdapters.RSCommandsTableAdapter();
			da.Connection = connection;

			return da;
        }
        
        SQLiteConnection Connection
        {
            get { return connection; }
        }
    }
}