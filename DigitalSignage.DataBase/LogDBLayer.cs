﻿using System;
using System.Collections.Generic;
//using System.Linq;
//using System.Text;
using System.IO;


//SQLite & DB routines
using System.Data.Common;
using System.Data.SQLite;

using NLog;
using NLog.Targets;
using NLog.Targets.Wrappers;

namespace DigitalSignage.DataBase
{

    public class LogDBLayer
    {
		private static Logger logger = LogManager.GetCurrentClassLogger();

		//	log semaphore
		private readonly object semaphore_for_log = new object();

        private SQLiteConnection connection = null;
        private string dataPath = "";

		private string srcfilename = "";

		public LogDBLayer()
        {
        }

        public int openDB(string dataPath)
        {
			try
			{
				logger.Info("Log Database start");
// 

				this.dataPath = dataPath;
//	20081117 hsshin SQLite Class를 사용하여 DBFactory를 얻어오도록 변경
// 				DbProviderFactory fact = DbProviderFactories.GetFactory("System.Data.SQLite");
				SQLiteFactory fact = new System.Data.SQLite.SQLiteFactory();
				connection = (SQLiteConnection)fact.CreateConnection();
				connection.ConnectionString = "Data Source=" + dataPath + "log.db3";
				
				srcfilename = dataPath + "log.db3";
				
				logger.Info(connection.ConnectionString);

				connection.Open();

				logger.Info("Log Data base opened");

			}
			catch (System.Data.SQLite.SQLiteException exSQLite)
			{
				logger.Error(exSQLite + "");
			}
			catch (Exception e)
			{
				logger.Error(e + "");
			}
            return 1;
        }

		public int openDB(string dataPath, string filename)
		{

			this.dataPath = dataPath;
			//	20081117 hsshin SQLite Class를 사용하여 DBFactory를 얻어오도록 변경
			// 				DbProviderFactory fact = DbProviderFactories.GetFactory("System.Data.SQLite");
			SQLiteFactory fact = new System.Data.SQLite.SQLiteFactory();
			connection = (SQLiteConnection)fact.CreateConnection();
			connection.ConnectionString = "Data Source=" + dataPath + filename;
			
			srcfilename = dataPath + filename;
			logger.Info(connection.ConnectionString);

			connection.Open();
			logger.Info("Log Data base opened");

			return 0;
		}
		public bool CopyDB(string dest)
		{
			lock (semaphore_for_log)
			{
				CompactDB();
				File.Copy(srcfilename, dest, false);

				return true;
			}
		}

		public void CompactDB()
		{
			using (SQLiteCommand cmd = connection.CreateCommand())
			{
				cmd.CommandText = "VACUUM";
				cmd.ExecuteNonQuery();
			}
		}

        public int closeDB()
        {
            connection.Close();
            connection = null;
            return 0;
        }

		public LogsDataSetTableAdapters.logmasterTableAdapter CreateLogMasterDA()
        {
			lock (semaphore_for_log)
			{
				LogsDataSetTableAdapters.logmasterTableAdapter da = new LogsDataSetTableAdapters.logmasterTableAdapter();

				da.Connection = connection;
				return da;
			}
        }

		public LogsDataSetTableAdapters.logsTableAdapter CreateLogDA()
        {
			lock (semaphore_for_log)
			{
				LogsDataSetTableAdapters.logsTableAdapter da = new LogsDataSetTableAdapters.logsTableAdapter();

				da.Connection = connection;
				return da;
			}
        }

		public LogsDataSetTableAdapters.client_logsTableAdapter CreateClientLogDA()
		{
			lock (semaphore_for_log)
			{
				LogsDataSetTableAdapters.client_logsTableAdapter da = new LogsDataSetTableAdapters.client_logsTableAdapter();

				da.Connection = connection;
				return da;
			}
		}
 
        SQLiteConnection Connection
        {
            get { return connection; }
        }
    }
}