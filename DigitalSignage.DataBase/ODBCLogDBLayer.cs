﻿using System;
using System.Collections.Generic;
//using System.Linq;
//using System.Text;
using System.IO;


//SQLite & DB routines
using System.Data.Common;

using NLog;
using NLog.Targets;
using NLog.Targets.Wrappers;
using System.Data.Odbc;

namespace DigitalSignage.DataBase
{

    public class LogDBLayer
    {
		private static Logger logger = LogManager.GetCurrentClassLogger();

		//	log semaphore
		private readonly object semaphore_for_log = new object();

		private OdbcConnection connection = null;
		private string conn_string = "";
        private string dataPath = "";

		private string srcfilename = "";

		public LogDBLayer()
        {
        }

		public int openDB(OdbcConnection con)
		{
			try
			{
				connection = con;

				logger.Info("Log Database starting");

			}
			catch (Exception e)
			{
				logger.Error(e + "");
			}
			return 1;
		}

        public int openDB(string dsn)
        {
			try
			{
				OdbcFactory fact = OdbcFactory.Instance;
				connection = (OdbcConnection)fact.CreateConnection();
				connection.ConnectionString = dsn;
				
				conn_string = dsn;

				logger.Info(connection.ConnectionString);

				connection.Open();
				logger.Info("Log Data base opened");

				return openDB(connection);

			}
			catch (Exception e)
			{
				logger.Error(e + "");
			}
            return 1;
        }


        public int closeDB()
        {
            connection.Close();
            connection = null;
            return 0;
        }

		public bool CopyDB(string dest)
		{
			logger.Info("Copy DB starting...");
			lock (semaphore_for_log)
			{
				try
				{
					CompactDB();
					File.Copy(srcfilename, dest, false);
					logger.Info("Copy Process Success.");
					return true;

				}
				catch (Exception e)
				{
					logger.Error(e.Message);
				}
				return false;
			}
		}

		public void CompactDB()
		{
			using (OdbcCommand cmd = GetOdbcConnection.CreateCommand())
			{
				cmd.CommandText = "VACUUM";
				cmd.ExecuteNonQuery();
			}
		}
		public LogsDataSetTableAdapters.logmasterTableAdapter CreateLogMasterDA()
        {
			lock (semaphore_for_log)
			{
				LogsDataSetTableAdapters.logmasterTableAdapter da = new LogsDataSetTableAdapters.logmasterTableAdapter();

				da.Connection = GetOdbcConnection;
				return da;
			}
        }

		public LogsDataSetTableAdapters.logsTableAdapter CreateLogDA()
        {
			lock (semaphore_for_log)
			{
				LogsDataSetTableAdapters.logsTableAdapter da = new LogsDataSetTableAdapters.logsTableAdapter();

				da.Connection = GetOdbcConnection;
				return da;
			}
        }

		public LogsDataSetTableAdapters.client_logsTableAdapter CreateClientLogDA()
		{
			lock (semaphore_for_log)
			{
				LogsDataSetTableAdapters.client_logsTableAdapter da = new LogsDataSetTableAdapters.client_logsTableAdapter();

				da.Connection = GetOdbcConnection;
				return da;
			}
		}

		public OdbcConnection GetOdbcConnection
		{
			get
			{
				if (connection == null)
				{
					//	Connection 새로 생성
					connection = (OdbcConnection)OdbcFactory.Instance.CreateConnection();
					connection.ConnectionString = conn_string;
					connection.Open();
					logger.Info("New logDB connection has been openning.");
				}
				else
				{
					//	hsshin 너무 잦은 State 호출은 좋지 않다. MSDN에 오버헤드를 발생하니 자제하라고 써있음.
					System.Data.ConnectionState state = connection.State;
					//

					if (state == System.Data.ConnectionState.Broken ||
					state == System.Data.ConnectionState.Closed)
					{
						logger.Info(String.Format("LogDB connection will be closed. (State : {0})", state.ToString()));

						//	Connection 초기화
						try
						{
							connection.Close();
							connection = null;
						}
						catch (Exception exconnection)
						{
							logger.Error(exconnection.ToString());
						}

						//	Connection 새로 생성
						connection = (OdbcConnection)OdbcFactory.Instance.CreateConnection();
						connection.ConnectionString = conn_string;
						connection.Open();
						logger.Info("New logDB connection has been openning.");
					}
				}

				return connection;
			}
		}

		public OdbcConnection Connection
        {
            get { return connection; }
        }

		public string LogPath
		{
			get { return dataPath; }
			set 
			{ 
				dataPath = value;
				srcfilename = dataPath + "log.db3";
			}
		}

    }
}