﻿using System;
using System.Collections.Generic;
//using System.Linq;
//using System.Text;
using System.IO;


//SQLite & DB routines
using System.Data.Common;

using NLog;
using NLog.Targets;
using NLog.Targets.Wrappers;
using System.Data.OleDb;

namespace DigitalSignage.DataBase
{

    public class DBLayer
    {
		private static Logger logger = LogManager.GetCurrentClassLogger();

		private OleDbConnection connection = null;
        private string dataPath = "";

		public DBLayer()
        {
        }

        public int openDB(string dsn)
        {
			try
			{
				logger.Info("Database start");
// 

				this.dataPath = dsn;
//	20081117 hsshin SQLite Class를 사용하여 DBFactory를 얻어오도록 변경
				OleDbFactory fact = OleDbFactory.Instance;
				
				connection = (OleDbConnection)fact.CreateConnection();

				connection.ConnectionString = dsn;
				logger.Info(connection.ConnectionString);

				connection.Open();
				logger.Info("Data base opened");
				try
				{
					using (OleDbCommand sql = new OleDbCommand("SELECT * FROM tasks", connection))
					{
						//initializing database structure
						sql.ExecuteNonQuery();
					}
					return 0;
				}
				catch (Exception e)
				{
					logger.Warn(e + "");
				}

			}
			catch (Exception e)
			{
				logger.Error(e + "");
			}
            return 1;
        }

        public int closeDB()
        {
            connection.Close();
            connection = null;
            return 0;
        }

		public DataSetTableAdapters.groupsTableAdapter CreateGroupDA()
        {
			DataSetTableAdapters.groupsTableAdapter da =
				new DataSetTableAdapters.groupsTableAdapter();
			da.Connection = connection;
            return da;
        }

		public DataSetTableAdapters.tasksTableAdapter CreateTaskDA()
        {
			DataSetTableAdapters.tasksTableAdapter da = new DataSetTableAdapters.tasksTableAdapter();
            da.Connection = connection;
            return da;
        }

		public DataSetTableAdapters.playersTableAdapter CreatePlayersDA()
        {
			DataSetTableAdapters.playersTableAdapter da = new DataSetTableAdapters.playersTableAdapter();
            da.Connection = connection;
            return da;
        }

		public DataSetTableAdapters.usersTableAdapter CreateUsersDA()
        {
			DataSetTableAdapters.usersTableAdapter da = new DataSetTableAdapters.usersTableAdapter();
            da.Connection = connection;
            return da;
        }

		public DataSetTableAdapters.RSCommandsTableAdapter CreateCmdDA()
        {
			DataSetTableAdapters.RSCommandsTableAdapter da = new DataSetTableAdapters.RSCommandsTableAdapter();
            da.Connection = connection;
            return da;
        }

		public DbConnection Connection
        {
            get { return connection; }
        }

		public string LogPath
		{
			get { return dataPath; }
			set { dataPath = value; }
		}
    }
}