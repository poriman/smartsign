﻿using System;
using System.Collections.Generic;
//using System.Linq;
//using System.Text;
using System.IO;


//SQLite & DB routines
using System.Data.Common;

using NLog;
using NLog.Targets;
using NLog.Targets.Wrappers;
using System.Data.Odbc;
using System.Collections.ObjectModel;

namespace DigitalSignage.DataBase
{

    public class DBLayer
    {
		private static Logger logger = LogManager.GetCurrentClassLogger();

		private OdbcConnection connection = null;
		private string conn_string = "";
        private string dataPath = "";


		public DBLayer()
        {
		}

		#region Connection Pool
		private Collection<OdbcConnection> arrConnections = null;
		private int conCursor = 0;
		private readonly object lockConnection = new object();

		private OdbcConnection GetAvailableConnection()
		{
			if (0 == arrConnections.Count)
			{
				return connection;
			}

			lock (lockConnection)
			{
				if (conCursor >= arrConnections.Count)
					conCursor = 0;

				try
				{
					OdbcConnection con = arrConnections[conCursor];
					System.Data.ConnectionState state = con.State;
					if (state == System.Data.ConnectionState.Broken || state == System.Data.ConnectionState.Closed)
					{
						con.Close();
						con.Dispose();

						OdbcFactory fact = OdbcFactory.Instance;
						con = (OdbcConnection)fact.CreateConnection();
						con.ConnectionString = conn_string;
						con.Open();
						arrConnections[conCursor] = con;
					}
					conCursor++;

					return con;

				}
				catch (Exception e)
				{
					logger.Error(e.ToString());
				}
			}

			return null;
		}
		/// <summary>
		/// Connection을 몇개 전달인자 만큼 만든다.
		/// </summary>
		/// <param name="nConnectionCount">커넥션의 개수</param>
		public bool InitializeConnectionPool(int nConnectionCount)
		{
			logger.Info("Openning connections.. amount " + nConnectionCount.ToString());

			if (String.IsNullOrEmpty(conn_string) || arrConnections != null || nConnectionCount <= 0)
				return false;

			arrConnections = new Collection<OdbcConnection>();

			for (int i = 0; i < nConnectionCount; ++i)
			{
				try
				{
					OdbcFactory fact = OdbcFactory.Instance;

					OdbcConnection con = (OdbcConnection)fact.CreateConnection();
					con.ConnectionString = conn_string;
					con.Open();
					arrConnections.Add(con);
				}
				catch (Exception e)
				{
					logger.Error(e.ToString());
				}
			}
			logger.Info("Connection pool.. Success!");
			return true;

		}

		public bool CloseConnectionPool()
		{
			logger.Info("Closing connections");
			foreach(OdbcConnection con in arrConnections)
			{
				try
				{
					con.Close();
					con.Dispose();
				}
				catch(Exception e)
				{
					logger.Error(e.ToString());
				}
			}

			arrConnections.Clear();
			arrConnections = null;

			return true;

		}
		#endregion

		public int openDB(OdbcConnection con)
		{
			try
			{
				logger.Info("Database starting");
	// 
				connection = con;

				logger.Info(connection.ConnectionString);

				try
				{
					using (OdbcCommand sql = new OdbcCommand("SELECT * FROM tasks", connection))
					{
						//initializing database structure
						sql.ExecuteNonQuery();
					}
					return 0;
				}
				catch (Exception e)
				{
					logger.Warn(e + "");
				}

			}
			catch (Exception e)
			{
				logger.Error(e + "");
			}
			return 1;
		}

        public int openDB(string dsn)
        {
			try
			{
				OdbcFactory fact = OdbcFactory.Instance;
				
				connection = (OdbcConnection)fact.CreateConnection();
				
				conn_string = dsn;
				connection.ConnectionString = dsn;
				logger.Info(conn_string);

				connection.Open();

				return openDB(connection);
			}
			catch (Exception e)
			{
				logger.Error(e + "");
			}
			return 1;

        }

        public int closeDB()
        {
            connection.Close();
            connection = null;
            return 0;
        }

		public OdbcConnection GetOdbcConnection
		{
			get {
				if(connection == null)
				{
					//	Connection 새로 생성
					connection = (OdbcConnection)OdbcFactory.Instance.CreateConnection();
					connection.ConnectionString = conn_string;
					connection.Open();
					logger.Info("New scheduleDB connection has been openning.");
				}
				else
				{
					//	hsshin 너무 잦은 State 호출은 좋지 않다. MSDN에 오버헤드를 발생하니 자제하라고 써있음.
					System.Data.ConnectionState state = connection.State;
					//

					if(state == System.Data.ConnectionState.Broken ||
					state == System.Data.ConnectionState.Closed)
					{
						logger.Info(String.Format("ScheduleDB connection will be closed. (State : {0})", state.ToString()));

						//	Connection 초기화
						try
						{
							connection.Close();
							connection = null;
						}
						catch (Exception exconnection)
						{
							logger.Error(exconnection.ToString());
						}

						//	Connection 새로 생성
						connection = (OdbcConnection)OdbcFactory.Instance.CreateConnection();
						connection.ConnectionString = conn_string;
						connection.Open();
						logger.Info("New scheduleDB connection has been openning.");
					}
				}

				return connection;
			}
		}

		public DataSetTableAdapters.groupsTableAdapter CreateGroupDA(OdbcConnection conn)
		{
			DataSetTableAdapters.groupsTableAdapter da =
				new DataSetTableAdapters.groupsTableAdapter();
			da.Connection = (conn == null ? GetAvailableConnection() : conn);
			return da;
		}

		public DataSetTableAdapters.downloadmasterTableAdapter CreateDownloadMasterDA(OdbcConnection conn)
		{
			DataSetTableAdapters.downloadmasterTableAdapter da = new DataSetTableAdapters.downloadmasterTableAdapter();
			da.Connection = (conn == null ? GetAvailableConnection() : conn);
			return da;
		}

		public DataSetTableAdapters.tasksTableAdapter CreateTaskDA(OdbcConnection conn)
		{
			DataSetTableAdapters.tasksTableAdapter da = new DataSetTableAdapters.tasksTableAdapter();
			da.Connection = (conn == null ? GetAvailableConnection() : conn);
			return da;
		}

		public DataSetTableAdapters.playersTableAdapter CreatePlayersDA(OdbcConnection conn)
		{
			DataSetTableAdapters.playersTableAdapter da = new DataSetTableAdapters.playersTableAdapter();
			da.Connection = (conn == null ? GetAvailableConnection() : conn);
			return da;
		}

		public DataSetTableAdapters.usersTableAdapter CreateUsersDA(OdbcConnection conn)
		{
			DataSetTableAdapters.usersTableAdapter da = new DataSetTableAdapters.usersTableAdapter();
			da.Connection = (conn == null ? GetAvailableConnection() : conn);
			return da;
		}

		public DataSetTableAdapters.RSCommandsTableAdapter CreateCmdDA(OdbcConnection conn)
		{
			DataSetTableAdapters.RSCommandsTableAdapter da = new DataSetTableAdapters.RSCommandsTableAdapter();
			da.Connection = (conn == null ? GetAvailableConnection() : conn);
			return da;
		}

		public DataSetTableAdapters.downloadtimerTableAdapter CreateDownloadTimerDA(OdbcConnection conn)
		{
			DataSetTableAdapters.downloadtimerTableAdapter da = new DataSetTableAdapters.downloadtimerTableAdapter();
			da.Connection = (conn == null ? GetAvailableConnection() : conn);
			return da;
		}

		public DataSetTableAdapters.groupsTableAdapter CreateGroupDA()
        {
			return CreateGroupDA(null);
        }

		public DataSetTableAdapters.downloadmasterTableAdapter CreateDownloadMasterDA()
		{
			return CreateDownloadMasterDA(null);
		}

		public DataSetTableAdapters.tasksTableAdapter CreateTaskDA()
        {
			return CreateTaskDA(null);
        }

		public DataSetTableAdapters.playersTableAdapter CreatePlayersDA()
        {
			return CreatePlayersDA(null);
        }

		public DataSetTableAdapters.usersTableAdapter CreateUsersDA()
        {
			return CreateUsersDA(null);
		}

		public DataSetTableAdapters.RSCommandsTableAdapter CreateCmdDA()
        {
			return CreateCmdDA(null);
		}

		public DataSetTableAdapters.downloadtimerTableAdapter CreateDownloadTimerDA()
		{
			return CreateDownloadTimerDA(null);
		}


		public OdbcConnection Connection
        {
            get { return connection; }
        }

		public string LogPath
		{
			get { return dataPath; }
			set { dataPath = value; }
		}
    }
}