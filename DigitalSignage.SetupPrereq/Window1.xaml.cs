﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DigitalSignage.SetupPrereq
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        public Window1()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void startUI_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string strAppDir = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetModules()[0].FullyQualifiedName);
                Process process = new Process();
                process.StartInfo.FileName = strAppDir + "\\quicktimeinstaller.exe";
                process.StartInfo.WorkingDirectory = strAppDir;
                process.Start();
                process.WaitForExit();
            }
            catch (Exception err)
            {
            }
        }
    }
}
