﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using DigitalSignage.Common;

namespace iVisionConfiguration.Library
{
	class iVisionConnector : INetworkServiceCallback, IDisposable
	{
		static readonly object semaphore = new object();

		public event EventHandler Connecting;
		public event EventHandler Connected;
		public event EventHandler Disconnected;
		public event EventHandler Failed;

		#region Error Message
		private string _lastErrorMessage = "";
		private string _serverhostname = "";
		public String LastErrorMessage
		{
			get { return _lastErrorMessage; }
		}
		#endregion


		#region .NetRemoting Service
		// remote interfaces
		ICmdReceiver serverCmdReceiver = null;

		private bool ConnectToRemoteServer(string hostname)
		{
			try
			{
				if (Connecting != null) Connecting("Remoting Service Connecting...", null);
				string fullhost = String.Format("tcp://{0}:888/CmdReceiverHost/rt", hostname);
				serverCmdReceiver = (ICmdReceiver)Activator.GetObject(
					typeof(ICmdReceiver), fullhost);

				
				serverCmdReceiver.ProcessSimpleCommand(CmdReceiverCommands.CmdEcho);

				if (Connecting != null) Connecting("Remoting Service Success!", null);
				return true;

			}
			catch (Exception ex)
			{
				if (Failed != null) Failed("Remoting Service Error...", null);

				_lastErrorMessage = ex.Message; 
			}

			return false;

		}
		#endregion

		#region WCF Connect

		NetworkServiceClient proxy = null;
		IVisionService.Topic clientInfo = null;


		public void Connect(string hostname, string playerid)
		{
			try
			{
				if (proxy != null)
					Disconnect();

				String address = hostname.Replace("tcp://", "");
				address = address.Replace("/", "");
				_serverhostname = address.Replace(":888", "");

				if (Connecting != null) Connecting(String.Format("Connecting to '{0}'...", _serverhostname), null);

// 				logger.Info("Connecting WCF Endpoint : " + address);

				EndpointAddress epAddress = new EndpointAddress("net.tcp://" + _serverhostname + ":40526/INetworkHost/tcp");

				InstanceContext context = new InstanceContext(this);
				proxy = new NetworkServiceClient(context, "", epAddress);

				clientInfo = new IVisionService.Topic();
				clientInfo.Groupid = "";
				clientInfo.Mediaid = playerid;
				clientInfo.Level = IVisionService.UserLevels.None;
				
				IAsyncResult iar = null;
				lock (semaphore)
				{
					iar = proxy.BeginConnect(this.clientInfo, new AsyncCallback(OnEndJoin), null);
				}
				
			}
			catch (Exception ex)
			{
				if (Failed != null) Failed("WCF Connection failed!", null);

				_lastErrorMessage = "It was disconnected because of the following error: " + ex.Message;
			}
		}

		public bool IsAuthored()
		{
			return proxy != null && (proxy.State == CommunicationState.Opened);
		}

		public void Dispose()
		{
			Disconnect();
		}

		public void Disconnect()
		{
			try
			{
				if (proxy != null)
				{
					lock (semaphore)
					{
						proxy.Disconnect(this.clientInfo);
					}

					_lastErrorMessage = "Disconnected";
				}
			}
			catch (Exception e)
			{
				_lastErrorMessage = e.ToString();
				proxy = null;
			}
			finally
			{
				try
				{
					proxy.Close();
					if (Disconnected != null) Disconnected("Disconnected!", null);

				}
				catch { }
				proxy = null;
			}
		}

		private void OnEndJoin(IAsyncResult iar)
		{
			try
			{
				if (proxy != null)
				{
					if (proxy.State == CommunicationState.Opened)
					{
						//	성공
						if (IsAuthored())
						{
							if (ConnectToRemoteServer(_serverhostname))
							{
								if (Connected != null) Connected("Connected!", null);
								_lastErrorMessage = "Success";
							}
						}
						else
						{
							if (Failed != null) Failed("Authoring is failed", null);

							_lastErrorMessage = "The [Authoring] is failed.";
						}
					}
					else
					{
						if (Failed != null) Failed("Server has not detected!", null);

						_lastErrorMessage = "Server has not detected!";
					}
				}
				else
				{
					if (Failed != null) Failed("Authoring is failed", null);
					_lastErrorMessage = "The [proxy] is NULL.";
				}
			}
			catch (Exception e)
			{
				if (Failed != null) Failed("Authoring is failed", null);
				_lastErrorMessage = e.ToString();
			}
		}
		#endregion

		#region Callback Members

		public void RefreshClients(IVisionService.Topic[] clients)
		{
		}

		public void Receive(IVisionService.Message msg)
		{
		}

		public void ReceiveWhisper(IVisionService.Message msg, IVisionService.Topic receiver)
		{
			if (msg != null)
			{
// 				logger.Info("Received message from server: " + msg.Result + ", " + msg.Content);
				if (msg.Result == iVisionCode.CONN_SUCCESS) //	인증 성공
				{
					if (Connecting != null) Connecting("Authoring is succeed!", null);
					_lastErrorMessage = "The authoring is succeed. (GID=" + receiver.Groupid + "PID=" + receiver.Mediaid+ ")";
					return;
				}
				else if (msg.Result == iVisionCode.CONN_ERROR_NO_PLAYER) //	DB에 없었으나 추가 됨
				{
					if (Connecting != null) Connecting("No player id in server!", null);
					_lastErrorMessage = "No media or group in the server DB. (GID=" + receiver.Groupid + "PID=" + receiver.Mediaid + ")";
				}
				else if (msg.Result == iVisionCode.CONN_ERROR_DUPLICATED_PLAYER) //	중복
				{
					if (Connecting != null) Connecting("Already authored player id!", null);
					_lastErrorMessage = "Already added, authored media. (GID=" + receiver.Groupid + "PID=" + receiver.Mediaid + ")";
				}
				else if (msg.Result == iVisionCode.CONN_ERROR_EXCESS_OF_ALLOWED_MAX_PLAYER) //	서버에서 접근 제한 (플레이어 개수 한도 초과)
				{
					if (Connecting != null) Connecting("Excess of the allowed maximum connection count!", null);
					_lastErrorMessage = "It was disconnected because of an excess of the allowed maximum count.";
				}
			}
		}

		public void EndReceiveWhisper(System.IAsyncResult result)
		{
		}

		public void IsWritingCallback(IVisionService.Topic client)
		{
		}

		public void EndIsWritingCallback(System.IAsyncResult result)
		{
		}

		public void ReceiverFile(IVisionService.FileMessage fileMsg, IVisionService.Topic receiver)
		{
		}

		public void UserJoin(IVisionService.Topic client)
		{
		}

		public void UserLeave(IVisionService.Topic client)
		{
		}

		public void EndRefreshClients(System.IAsyncResult result)
		{
		}

		public void EndReceive(System.IAsyncResult result)
		{
		}

		public void EndReceiverFile(System.IAsyncResult result)
		{
		}

		public void EndUserJoin(System.IAsyncResult result)
		{
		}

		public void EndUserLeave(System.IAsyncResult result)
		{
		}

		public System.IAsyncResult BeginReceiveWhisper(IVisionService.Message msg, IVisionService.Topic receiver, System.AsyncCallback callback, object asyncState)
		{
			throw new NotImplementedException();
		}

		public System.IAsyncResult BeginIsWritingCallback(IVisionService.Topic client, System.AsyncCallback callback, object asyncState)
		{
			throw new NotImplementedException();
		}

		public System.IAsyncResult BeginReceiverFile(IVisionService.FileMessage fileMsg, IVisionService.Topic receiver, System.AsyncCallback callback, object asyncState)
		{
			throw new NotImplementedException();
		}

		public System.IAsyncResult BeginUserJoin(IVisionService.Topic client, System.AsyncCallback callback, object asyncState)
		{
			throw new NotImplementedException();
		}

		public System.IAsyncResult BeginReceive(IVisionService.Message msg, System.AsyncCallback callback, object asyncState)
		{
			throw new NotImplementedException();
		}

		public System.IAsyncResult BeginUserLeave(IVisionService.Topic client, System.AsyncCallback callback, object asyncState)
		{
			throw new NotImplementedException();
		}

		public System.IAsyncResult BeginRefreshClients(IVisionService.Topic[] clients, System.AsyncCallback callback, object asyncState)
		{
			throw new NotImplementedException();
		}

		#endregion Callback Members

	}
}
