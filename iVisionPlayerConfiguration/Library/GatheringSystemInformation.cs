﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using System.Management;

namespace iVisionConfiguration.Library
{
	class GatheringSystemInformation
	{
		public XmlDocument Document
		{
			get { return doc; }
		}

		#region Schema Information
		const string SCHEMA_INFORMATION = "Information";
		const string SCHEMA_HARDWARE = "Hardware";
		const string SCHEMA_HW_CPU = "Processor";
		const string SCHEMA_HW_RAM = "Memory";
		const string SCHEMA_HW_VGA = "VideoController";
		const string SCHEMA_HW_SND = "SoundDevice";
		const string SCHEMA_HW_NET = "NetworkAdepter";

		const string SCHEMA_SOFTWARE = "Software";
		const string SCHEMA_SW_OS = "Platform";
		#endregion



		#region XML Schema Information
		XmlDocument doc = null;

		XmlNode schemaINFO = null;
		XmlNode schemaHW = null;
		XmlNode schemaHWCPU = null;
		XmlNode schemaHWRAM = null;
		XmlNode schemaHWVGA = null;
		XmlNode schemaHWSOUND = null;
		XmlNode schemaHWNETWORK = null;
		XmlNode schemaSW = null;
		XmlNode schemaSWOS = null;

		#endregion

		public GatheringSystemInformation(string xmlpath)
		{
			try
			{
				doc = new XmlDocument();
				doc.Load(xmlpath);
			}
			catch
			{
				MakeSchema();
				GatheringInfo();

				doc.Save(xmlpath);
			}
		}
		public GatheringSystemInformation()
		{
			doc = new XmlDocument();
		}

		public void Refresh()
		{
			GatheringInfo();
		}

		private void GatheringInfo()
		{
			AppendSystemInfo(schemaSWOS, "Win32_OperatingSystem", "Caption,CSDVersion,BuildNumber,Version");

			AppendSystemInfo(schemaHWCPU, "Win32_Processor", "Name,Caption,L2CacheSize,CurrentClockSpeed");
			AppendSystemInfo(schemaHWRAM, "Win32_MemoryArray", "EndingAddress");
			AppendSystemInfo(schemaHWVGA, "Win32_VideoController", "Name,AdapterRAM,VideoModeDescription");
			AppendSystemInfo(schemaHWSOUND, "Win32_SoundDevice", "Name,Manufacturer");
			AppendSystemInfo(schemaHWNETWORK, "Win32_NetworkAdapter", "Name,MACAddress");

		}

		// exemplary call: GetSystemInfo("Win32_Processor", "Caption,Manufacturer");
		private string GetSystemInfo(string strTable, string strProperties)
		{
			try
			{
				ManagementObjectSearcher mos = new ManagementObjectSearcher();
				mos.Query.QueryString = "SELECT " + strProperties + " FROM " + strTable;
				ManagementObjectCollection moc = mos.Get();
				string strInfo = string.Empty;
				foreach (ManagementObject mo in moc)
					foreach (PropertyData pd in mo.Properties)
						strInfo += pd.Value + ",";
				return strInfo.Substring(0, strInfo.Length - 1);
			}
			catch { return "N/A"; }
		}

		private void AppendSystemInfo(XmlNode parentNode, string strTable, string strProperties)
		{
			try
			{
				ManagementObjectSearcher mos = new ManagementObjectSearcher();
				mos.Query.QueryString = "SELECT " + strProperties + " FROM " + strTable;
				ManagementObjectCollection moc = mos.Get();
				int nCount = 0;
				foreach (ManagementObject mo in moc)
				{
					try
					{
						XmlNode piece = doc.CreateNode(XmlNodeType.Element, "DEVICE"+nCount.ToString(), "");

						foreach (PropertyData pd in mo.Properties)
						{
							XmlNode nameNode = doc.CreateNode(XmlNodeType.Element, pd.Name, "");
							nameNode.AppendChild(doc.CreateTextNode(pd.Value.ToString()));
							if(moc.Count == 1)
								parentNode.AppendChild(nameNode);
							else
								piece.AppendChild(nameNode);
						}

						if (moc.Count > 1)
						{
							parentNode.AppendChild(piece);
							nCount++;
						}
						
					}
					catch {}

				}
			}
			catch { }
		}
		private void MakeSchema()
		{
			schemaINFO = doc.CreateNode(XmlNodeType.Element, SCHEMA_INFORMATION, "");
			schemaHW = doc.CreateNode(XmlNodeType.Element, SCHEMA_HARDWARE, "");
			schemaHWCPU = doc.CreateNode(XmlNodeType.Element, SCHEMA_HW_CPU, "");
			schemaHWRAM = doc.CreateNode(XmlNodeType.Element, SCHEMA_HW_RAM, "");
			schemaHWVGA = doc.CreateNode(XmlNodeType.Element, SCHEMA_HW_VGA, "");
			schemaHWSOUND = doc.CreateNode(XmlNodeType.Element, SCHEMA_HW_SND, "");
			schemaHWNETWORK = doc.CreateNode(XmlNodeType.Element, SCHEMA_HW_NET, "");
			schemaSW = doc.CreateNode(XmlNodeType.Element, SCHEMA_SOFTWARE, "");
			schemaSWOS = doc.CreateNode(XmlNodeType.Element, SCHEMA_SW_OS, "");

			doc.AppendChild(schemaINFO);

			schemaINFO.AppendChild(schemaHW);
			schemaINFO.AppendChild(schemaSW);

			schemaHW.AppendChild(schemaHWCPU);
			schemaHW.AppendChild(schemaHWRAM);
			schemaHW.AppendChild(schemaHWVGA);
			schemaHW.AppendChild(schemaHWSOUND);
			schemaHW.AppendChild(schemaHWNETWORK);

			schemaSW.AppendChild(schemaSWOS);
		}

	}
}
