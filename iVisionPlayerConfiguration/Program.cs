﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Wizard.UI;

namespace iVisionPlayerConfiguration
{
	static class Program
	{
		/// <summary>
		/// 해당 응용 프로그램의 주 진입점입니다.
		/// </summary>
		[STAThread]
		static void Main()
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
// 			Application.Run(new PlayerConfiguration());
			PlayerConfigurationSheet wizard = new PlayerConfigurationSheet();
			wizard.Text = Properties.Resources.titlePlayerConfiguration;

			wizard.Pages.Add(new WelcomePage());
// 			wizard.Pages.Add(new ConnectionPage());
			wizard.Pages.Add(new SettingPage());
			wizard.Pages.Add(new btnUpdateInfo());
			wizard.Pages.Add(new FinishPage());
			Application.Run(wizard);
		}
	}
}
