﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace iVisionPlayerConfiguration
{
	public partial class FinishPage : Wizard.UI.WizardPage, IConfiguration
	{
		public FinishPage()
		{
			InitializeComponent();
			LoadConfig(AppDomain.CurrentDomain.BaseDirectory);
			this.SetActive += new CancelEventHandler(FinishPage_SetActive);
		}

		void FinishPage_SetActive(object sender, CancelEventArgs e)
		{
			SetWizardButtons(Wizard.UI.WizardButtons.Finish | Wizard.UI.WizardButtons.Back);
		}

		public bool LoadConfig(string path)
		{
			return true;
		}

		public bool SaveConfig(string path)
		{
			return true;
		}
	}
}
