﻿namespace iVisionPlayerConfiguration
{
	partial class SettingPage
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			this.gbPlayerSetting = new System.Windows.Forms.GroupBox();
			this.label1 = new System.Windows.Forms.Label();
			this.dataGridPlayer = new System.Windows.Forms.DataGridView();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.dataGridAgent = new System.Windows.Forms.DataGridView();
			this.colKey = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.colValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.tbUpdateURL = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.gbPlayerSetting.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridPlayer)).BeginInit();
			this.groupBox1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGridAgent)).BeginInit();
			this.groupBox2.SuspendLayout();
			this.SuspendLayout();
			// 
			// gbPlayerSetting
			// 
			this.gbPlayerSetting.Controls.Add(this.dataGridPlayer);
			this.gbPlayerSetting.Location = new System.Drawing.Point(21, 96);
			this.gbPlayerSetting.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.gbPlayerSetting.Name = "gbPlayerSetting";
			this.gbPlayerSetting.Padding = new System.Windows.Forms.Padding(5);
			this.gbPlayerSetting.Size = new System.Drawing.Size(369, 130);
			this.gbPlayerSetting.TabIndex = 1;
			this.gbPlayerSetting.TabStop = false;
			this.gbPlayerSetting.Text = "Player setting";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(18, 22);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(274, 15);
			this.label1.TabIndex = 1;
			this.label1.Text = "You can configure the player\'s settings for playing.";
			// 
			// dataGridPlayer
			// 
			this.dataGridPlayer.AllowUserToAddRows = false;
			this.dataGridPlayer.AllowUserToDeleteRows = false;
			this.dataGridPlayer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridPlayer.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colKey,
            this.colValue});
			this.dataGridPlayer.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dataGridPlayer.Location = new System.Drawing.Point(5, 21);
			this.dataGridPlayer.Name = "dataGridPlayer";
			this.dataGridPlayer.RowHeadersVisible = false;
			this.dataGridPlayer.RowTemplate.Height = 23;
			this.dataGridPlayer.Size = new System.Drawing.Size(359, 104);
			this.dataGridPlayer.TabIndex = 0;
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.dataGridAgent);
			this.groupBox1.Location = new System.Drawing.Point(21, 234);
			this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Padding = new System.Windows.Forms.Padding(5);
			this.groupBox1.Size = new System.Drawing.Size(369, 170);
			this.groupBox1.TabIndex = 2;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Agent setting";
			// 
			// dataGridAgent
			// 
			this.dataGridAgent.AllowUserToAddRows = false;
			this.dataGridAgent.AllowUserToDeleteRows = false;
			this.dataGridAgent.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dataGridAgent.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn3,
            this.dataGridViewTextBoxColumn4});
			this.dataGridAgent.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dataGridAgent.Location = new System.Drawing.Point(5, 21);
			this.dataGridAgent.Name = "dataGridAgent";
			this.dataGridAgent.RowHeadersVisible = false;
			this.dataGridAgent.RowTemplate.Height = 23;
			this.dataGridAgent.Size = new System.Drawing.Size(359, 144);
			this.dataGridAgent.TabIndex = 0;
			// 
			// colKey
			// 
			this.colKey.HeaderText = "Key";
			this.colKey.Name = "colKey";
			this.colKey.ReadOnly = true;
			this.colKey.Width = 130;
			// 
			// colValue
			// 
			this.colValue.HeaderText = "Value";
			this.colValue.Name = "colValue";
			this.colValue.Resizable = System.Windows.Forms.DataGridViewTriState.True;
			this.colValue.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
			this.colValue.Width = 150;
			// 
			// dataGridViewTextBoxColumn3
			// 
			this.dataGridViewTextBoxColumn3.HeaderText = "Key";
			this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
			this.dataGridViewTextBoxColumn3.ReadOnly = true;
			this.dataGridViewTextBoxColumn3.Width = 130;
			// 
			// dataGridViewTextBoxColumn4
			// 
			this.dataGridViewTextBoxColumn4.HeaderText = "Value";
			this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
			this.dataGridViewTextBoxColumn4.Resizable = System.Windows.Forms.DataGridViewTriState.True;
			this.dataGridViewTextBoxColumn4.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
			this.dataGridViewTextBoxColumn4.Width = 150;
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.tbUpdateURL);
			this.groupBox2.Controls.Add(this.label2);
			this.groupBox2.Location = new System.Drawing.Point(21, 40);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(369, 49);
			this.groupBox2.TabIndex = 4;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Update Setting";
			// 
			// tbUpdateURL
			// 
			this.tbUpdateURL.Location = new System.Drawing.Point(88, 19);
			this.tbUpdateURL.Name = "tbUpdateURL";
			this.tbUpdateURL.Size = new System.Drawing.Size(275, 23);
			this.tbUpdateURL.TabIndex = 1;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(6, 22);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(76, 15);
			this.label2.TabIndex = 0;
			this.label2.Text = "Updater URL:";
			// 
			// SettingPage
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.gbPlayerSetting);
			this.Name = "SettingPage";
			this.Size = new System.Drawing.Size(420, 412);
			this.gbPlayerSetting.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dataGridPlayer)).EndInit();
			this.groupBox1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dataGridAgent)).EndInit();
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.GroupBox gbPlayerSetting;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.DataGridView dataGridPlayer;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.DataGridView dataGridAgent;
		private System.Windows.Forms.DataGridViewTextBoxColumn colKey;
		private System.Windows.Forms.DataGridViewTextBoxColumn colValue;
		private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
		private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.TextBox tbUpdateURL;
		private System.Windows.Forms.Label label2;
	}
}
