﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace iVisionPlayerConfiguration
{
	public partial class SettingPage : Wizard.UI.WizardPage, IConfiguration
	{
		const string XMLFILENAME1 = "i-Vision.Player.exe.config";
		const string XMLFILENAME2 = "i-Vision.PlayAgent.exe.config";
		const string XMLFILENAME3 = "iVisionUpdater.exe.config";

		XmlDocument player_config = null;
		XmlDocument agent_config = null;
		XmlDocument updater_config = null;

		public SettingPage()
		{
			InitializeComponent();
			LoadConfig(AppDomain.CurrentDomain.BaseDirectory);
			this.SetActive += new CancelEventHandler(SettingPage_SetActive);
		}

		void SettingPage_SetActive(object sender, CancelEventArgs e)
		{
			SetWizardButtons(Wizard.UI.WizardButtons.Next | Wizard.UI.WizardButtons.Back);
		}

		public bool LoadConfig(string path)
		{
			bool bRet = true;
			try
			{
				player_config = new XmlDocument();
				player_config.Load(AppDomain.CurrentDomain.BaseDirectory + XMLFILENAME1);

				XmlNodeList list1 = player_config.SelectNodes("//child::add");

				foreach (XmlNode node in list1)
				{
					DataGridViewRow row = new DataGridViewRow();
					DataGridViewTextBoxCell cell1 = new DataGridViewTextBoxCell();
					DataGridViewTextBoxCell cell2 = new DataGridViewTextBoxCell();
					cell1.Value = node.Attributes["key"].Value;
					cell2.Value = node.Attributes["value"].Value;
					row.Cells.Add(cell1);
					row.Cells.Add(cell2);

					this.dataGridPlayer.Rows.Add(row);
				}
			}
			catch { bRet = bRet || false; }

			try
			{
				agent_config = new XmlDocument();
				agent_config.Load(AppDomain.CurrentDomain.BaseDirectory + XMLFILENAME2);

				XmlNodeList list1 = agent_config.SelectNodes("//child::add");

				foreach (XmlNode node in list1)
				{
					DataGridViewRow row = new DataGridViewRow();
					DataGridViewTextBoxCell cell1 = new DataGridViewTextBoxCell();
					DataGridViewTextBoxCell cell2 = new DataGridViewTextBoxCell();
					cell1.Value = node.Attributes["key"].Value;
					cell2.Value = node.Attributes["value"].Value;
					row.Cells.Add(cell1);
					row.Cells.Add(cell2);

					this.dataGridAgent.Rows.Add(row);
				}
			}
			catch { bRet = bRet || false; }

			try
			{
				updater_config = new XmlDocument();
				updater_config.Load(AppDomain.CurrentDomain.BaseDirectory + XMLFILENAME3);

				XmlNode node = updater_config.SelectSingleNode("//child::setting[attribute::name=\"UpdateURL\"]");
				XmlNode nodevalue = node.SelectSingleNode("child::value");
				this.tbUpdateURL.Text = nodevalue.ChildNodes[0].Value;
			}
			catch { bRet = bRet || false; }

			return bRet;
		}

		public bool SaveConfig(string path)
		{
			bool bRet = true;
			try
			{
				agent_config.Load(AppDomain.CurrentDomain.BaseDirectory + XMLFILENAME2);

				foreach (DataGridViewRow row in dataGridAgent.Rows)
				{
					XmlNode node = agent_config.SelectSingleNode(String.Format("//child::add[attribute::key=\"{0}\"]", row.Cells[0].Value));

					try
					{
						node.Attributes["value"].Value = null == row.Cells[1].Value ? "" : row.Cells[1].Value.ToString();
					}
					catch { }
				}

				agent_config.Save(AppDomain.CurrentDomain.BaseDirectory + XMLFILENAME2);
			}
			catch { bRet = bRet || false; }

			try
			{
				player_config.Load(AppDomain.CurrentDomain.BaseDirectory + XMLFILENAME1);

				foreach (DataGridViewRow row in dataGridPlayer.Rows)
				{
					XmlNode node = player_config.SelectSingleNode(String.Format("//child::add[attribute::key=\"{0}\"]", row.Cells[0].Value));

					try
					{
						node.Attributes["value"].Value = null == row.Cells[1].Value ? "" : row.Cells[1].Value.ToString();
					}
					catch { }
				}

				player_config.Save(AppDomain.CurrentDomain.BaseDirectory + XMLFILENAME1);

			}
			catch { bRet = bRet || false; }

			try
			{
				updater_config = new XmlDocument();
				updater_config.Load(AppDomain.CurrentDomain.BaseDirectory + XMLFILENAME3);

				XmlNode node = updater_config.SelectSingleNode("//child::setting[attribute::name=\"UpdateURL\"]");
				XmlNode nodevalue = node.SelectSingleNode("child::value");

				nodevalue.ChildNodes[0].Value = this.tbUpdateURL.Text;

				updater_config.Save(AppDomain.CurrentDomain.BaseDirectory + XMLFILENAME3);
			}
			catch { bRet = bRet || false; }

			return bRet;
		}
	}
}
