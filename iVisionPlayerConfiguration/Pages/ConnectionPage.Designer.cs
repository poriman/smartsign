﻿namespace iVisionPlayerConfiguration
{
	partial class ConnectionPage
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.gbServerInformation = new System.Windows.Forms.GroupBox();
			this.label4 = new System.Windows.Forms.Label();
			this.tbServerReturns = new System.Windows.Forms.TextBox();
			this.btnCheck = new System.Windows.Forms.Button();
			this.tbPlayerID = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.tbHost = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.gbServerInformation.SuspendLayout();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(18, 22);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(344, 15);
			this.label1.TabIndex = 0;
			this.label1.Text = "You can configure the connection setting for connecting server.";
			// 
			// gbServerInformation
			// 
			this.gbServerInformation.Controls.Add(this.label4);
			this.gbServerInformation.Controls.Add(this.tbServerReturns);
			this.gbServerInformation.Controls.Add(this.btnCheck);
			this.gbServerInformation.Controls.Add(this.tbPlayerID);
			this.gbServerInformation.Controls.Add(this.label3);
			this.gbServerInformation.Controls.Add(this.tbHost);
			this.gbServerInformation.Controls.Add(this.label2);
			this.gbServerInformation.Location = new System.Drawing.Point(20, 54);
			this.gbServerInformation.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.gbServerInformation.Name = "gbServerInformation";
			this.gbServerInformation.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.gbServerInformation.Size = new System.Drawing.Size(369, 324);
			this.gbServerInformation.TabIndex = 1;
			this.gbServerInformation.TabStop = false;
			this.gbServerInformation.Text = "Server Information";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(16, 112);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(82, 15);
			this.label4.TabIndex = 6;
			this.label4.Text = "Server Returns";
			// 
			// tbServerReturns
			// 
			this.tbServerReturns.Location = new System.Drawing.Point(18, 131);
			this.tbServerReturns.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.tbServerReturns.Multiline = true;
			this.tbServerReturns.Name = "tbServerReturns";
			this.tbServerReturns.ReadOnly = true;
			this.tbServerReturns.Size = new System.Drawing.Size(329, 170);
			this.tbServerReturns.TabIndex = 5;
			// 
			// btnCheck
			// 
			this.btnCheck.Location = new System.Drawing.Point(260, 64);
			this.btnCheck.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.btnCheck.Name = "btnCheck";
			this.btnCheck.Size = new System.Drawing.Size(87, 29);
			this.btnCheck.TabIndex = 4;
			this.btnCheck.Text = "Check";
			this.btnCheck.UseVisualStyleBackColor = true;
			this.btnCheck.Click += new System.EventHandler(this.btnCheck_Click);
			// 
			// tbPlayerID
			// 
			this.tbPlayerID.Location = new System.Drawing.Point(132, 66);
			this.tbPlayerID.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.tbPlayerID.MaxLength = 7;
			this.tbPlayerID.Name = "tbPlayerID";
			this.tbPlayerID.Size = new System.Drawing.Size(100, 23);
			this.tbPlayerID.TabIndex = 3;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(16, 70);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(59, 15);
			this.label3.TabIndex = 2;
			this.label3.Text = "Player ID: ";
			// 
			// tbHost
			// 
			this.tbHost.Location = new System.Drawing.Point(132, 34);
			this.tbHost.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
			this.tbHost.Name = "tbHost";
			this.tbHost.Size = new System.Drawing.Size(215, 23);
			this.tbHost.TabIndex = 1;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(16, 38);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(103, 15);
			this.label2.TabIndex = 0;
			this.label2.Text = "Server Hostname: ";
			// 
			// ConnectionPage
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
			this.Controls.Add(this.gbServerInformation);
			this.Controls.Add(this.label1);
			this.Name = "ConnectionPage";
			this.Size = new System.Drawing.Size(420, 412);
			this.gbServerInformation.ResumeLayout(false);
			this.gbServerInformation.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.GroupBox gbServerInformation;
		private System.Windows.Forms.TextBox tbServerReturns;
		private System.Windows.Forms.Button btnCheck;
		private System.Windows.Forms.TextBox tbPlayerID;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox tbHost;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label4;
	}
}
