﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using iVisionPlayerConfiguration.Library;
using DigitalSignage.Common;


namespace iVisionPlayerConfiguration
{
	public partial class ConnectionPage : Wizard.UI.WizardPage, IConfiguration
	{
		iVisionConnector conn = new iVisionConnector();


		const string XMLFILENAME1 = "config.xml";
		const string XMLFILENAME2 = "server_config.xml";
		const string XMLFILENAME3 = "async.xml";

		ConfigSettings _conf = null;
		ServerConfigSettings _server_conf = null;

		public ConnectionPage()
		{
			InitializeComponent();

			LoadConfig(AppDomain.CurrentDomain.BaseDirectory);
			this.SetActive += new CancelEventHandler(ConnectionPage_SetActive);
			this.Disposed += new EventHandler(ConnectionPage_Disposed);

			conn.Connecting += new EventHandler(conn_Connecting);
			conn.Connected += new EventHandler(conn_Connected);
			conn.Failed += new EventHandler(conn_Failed);
		}

		void ConnectionPage_Disposed(object sender, EventArgs e)
		{
			if (conn != null)
			{
				conn.Dispose();
				conn = null;
			}
		}

		void ConnectionPage_SetActive(object sender, CancelEventArgs e)
		{
			SetWizardButtons(Wizard.UI.WizardButtons.Next | Wizard.UI.WizardButtons.Back);
		}

		private void btnCheck_Click(object sender, EventArgs e)
		{
			String ErrorMessage;
			if (CheckConstraint(out ErrorMessage))
			{
				ConnectToServer();
			}
			else MessageBox.Show(ErrorMessage, "Check Connection", MessageBoxButtons.OK, MessageBoxIcon.Error);
		}

		private void ConnectToServer()
		{
			this.tbServerReturns.Text = "";

			string hostname = String.Format("tcp://{0}:888", tbHost.Text);

			conn.Connect(hostname, tbPlayerID.Text);
		}

		delegate void MyEventDelegate(object sender, EventArgs e);
		void conn_Failed(object sender, EventArgs e)
		{
			if(this.InvokeRequired)
			{
				this.Invoke(new MyEventDelegate(this.conn_Failed), new object[] {sender, e});
				return;
			}
			this.tbServerReturns.Text += sender.ToString();
			this.tbServerReturns.Text += "\r\n";
			MessageBox.Show(sender.ToString(), "Check Connection", MessageBoxButtons.OK, MessageBoxIcon.Error);
		}

		void conn_Connected(object sender, EventArgs e)
		{
			if (this.InvokeRequired)
			{
				this.Invoke(new MyEventDelegate(this.conn_Connected), new object[] { sender, e });
				return;
			}
			this.tbServerReturns.Text += sender.ToString();
			this.tbServerReturns.Text += "\r\n";
		}

		void conn_Connecting(object sender, EventArgs e)
		{
			if (this.InvokeRequired)
			{
				this.Invoke(new MyEventDelegate(this.conn_Connecting), new object[] { sender, e });
				return;
			}
			this.tbServerReturns.Text += sender.ToString();
			this.tbServerReturns.Text += "\r\n";
		}

		private bool CheckConstraint(out string sErrorMessage)
		{
			sErrorMessage = String.Empty;

			//	공백 제거
			tbHost.Text = tbHost.Text.Trim();
			tbPlayerID.Text = tbPlayerID.Text.Trim();

			//	Syntex 체크
			if (String.IsNullOrEmpty(tbHost.Text))				
			{
				sErrorMessage = "Server Hostname is empty.";
				return false;
			}

			//	Syntex 체크
			if (String.IsNullOrEmpty(tbPlayerID.Text))				
			{
				sErrorMessage = "PlayerID is empty.";
				return false;
			}

			//	Syntex 체크
			if (tbPlayerID.TextLength != 7)
			{
				sErrorMessage = "PlayerID's length must be 7.";
				return false;
			}
			return true;
		}

		public bool LoadConfig(string path)
		{
			bool bRet = true;

			string app_path = String.Format(@"{0}PlayerData\", AppDomain.CurrentDomain.BaseDirectory);

			try
			{
				_conf = new ConfigSettings(app_path + XMLFILENAME1);
				this.tbPlayerID.Text =  ConfigSettings.ReadSetting("player_id");
			}
			catch { bRet = bRet || false; }
			
			try
			{
				_server_conf = new ServerConfigSettings(app_path + XMLFILENAME2);
				this.tbHost.Text = ServerConfigSettings.ReadSetting("server_path").Replace(":888", "");
			}
			catch { bRet = bRet || false; }

			return bRet;
		}

		public bool SaveConfig(string path)
		{
			bool bRet = true;

			try
			{
				ConfigSettings.WriteSetting("player_id", this.tbPlayerID.Text);
			}
			catch { bRet = bRet || false; }

			try
			{
				ServerConfigSettings.WriteSetting("server_path", String.Format("{0}:888", this.tbHost.Text));
			}
			catch { bRet = bRet || false; }

			if(bRet)
			{
				try
				{
					string async_config_path = String.Format(@"{0}PlayerData\{1}", AppDomain.CurrentDomain.BaseDirectory, XMLFILENAME3);
					System.IO.File.Delete(async_config_path);
				}
				catch { }
			}
			return bRet;
		}
	}
}
