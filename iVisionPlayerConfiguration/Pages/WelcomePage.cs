﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace iVisionPlayerConfiguration
{
	public partial class WelcomePage : Wizard.UI.WizardPage, IConfiguration
	{
		public WelcomePage()
		{
			InitializeComponent();
			LoadConfig(AppDomain.CurrentDomain.BaseDirectory);
			this.SetActive += new CancelEventHandler(WelcomePage_SetActive);
		}

		void WelcomePage_SetActive(object sender, CancelEventArgs e)
		{
			SetWizardButtons(Wizard.UI.WizardButtons.Next);
		}

		public bool LoadConfig(string path)
		{
			return true;
		}

		public bool SaveConfig(string path)
		{
			return true;
		}
	}
}
