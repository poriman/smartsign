﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace iVisionPlayerConfiguration
{
	public partial class PlayerConfigurationSheet : Wizard.UI.WizardSheet
	{
		public PlayerConfigurationSheet()
		{
			InitializeComponent();
		}

		private void PlayerConfigurationSheet_FormClosing(object sender, FormClosingEventArgs e)
		{
			if(this.DialogResult == DialogResult.OK)
			{
				foreach (IConfiguration config in base.Pages)
				{
					config.SaveConfig(AppDomain.CurrentDomain.BaseDirectory);
				}

			}
			else if(this.DialogResult == DialogResult.Cancel)
			{
				DialogResult result = MessageBox.Show("Modified information will be canceled, continue?", "Player Configuration", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

				e.Cancel = result == DialogResult.No;
			}
		}
	}
}
