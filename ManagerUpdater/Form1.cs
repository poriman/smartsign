﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.IO;
using System.Reflection;
using System.Diagnostics;
using System.Net;
using Microsoft.Win32;
using System.Collections.ObjectModel;

using NLog;
using NLog.Targets;
using System.Xml;

namespace ManagerUpdater
{
	public partial class Form1 : Form
	{
		private static Logger logger = LogManager.GetCurrentClassLogger();

		const string UPDATE_EXT = ".cab";
		const string TEMPFOLDER = "Temp";
		string UPDATEURL = "http://update.myivision.com/ivision20/Manager/Default/";
		const string UPDATELIST = "m_update.txt";

        const string TEMPLATEFOLDER = "Template_xml";
        const string CONTENTSFOLDER = "Contents";
        const string UPDATETEMPLATE = "templatelists.xml";
        const string UPDATERESULTHEADER = "ResDataHeaderInfo.xml";
        string UPDATETEMPLATEURL = "http://update.myivision.com/ivisionSaaS/Manager/Template_xml/";
		string strAppDir = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetModules()[0].FullyQualifiedName) + "\\";

		public Form1()
		{
			InitializeComponent();
			init();
			try
			{
				UPDATEURL = Properties.Settings.Default.UpdateURL;
                UPDATETEMPLATEURL = Properties.Settings.Default.UpdateTemplateURL;
			}
			catch
			{
				UPDATEURL = "http://update.myivision.com/ivision20/Manager/Default/";
                UPDATETEMPLATEURL = "http://update.myivision.com/ivisionSaaS/Manager/Template_xml/";
			}
		}
		public void init()
		{
			try
			{
				string appData = strAppDir + "SchedulerData\\";
				// get own version
				Assembly myAssembly = Assembly.GetExecutingAssembly();
				AssemblyName myAssemblyName = myAssembly.GetName();
				long version = ((long)myAssemblyName.Version.Major << 48) |
					((long)myAssemblyName.Version.Minor << 32) |
					(myAssemblyName.Version.Build << 16) |
					myAssemblyName.Version.Revision;

				// configuring log output
				FileTarget target = new FileTarget();
				target.Layout = "${longdate}\t[${level}]\t${message}";
				target.FileName = appData + "update_log.txt";
				target.ArchiveFileName = appData + "archives/update.{#####}.txt";

				target.ArchiveEvery = FileTarget.ArchiveEveryMode.Day;
				target.MaxArchiveFiles = 31;
				target.ArchiveNumbering = FileTarget.ArchiveNumberingMode.Sequence;
				// this speeds up things when no other processes are writing to the file
				target.ConcurrentWrites = true;
				NLog.Config.SimpleConfigurator.ConfigureForTargetLogging(target, LogLevel.Debug);

				logger.Info("Updater " + ((version >> 48) & 0xFFFF) + "." +
						((version >> 32) & 0xFFFF) + "." +
						((version >> 16) & 0xFFFF) + "." + (version & 0xFFFF));
			}
			catch
			{
				
			}
		}

		private void Form1_Load(object sender, EventArgs e)
		{
			label1.Text = "Downloading list...";

			if (Directory.Exists(strAppDir + TEMPFOLDER))
			{
				RecusiveMove(strAppDir + TEMPFOLDER + "\\");
				StartAgent();
				this.Close();
				return;
			}

			if(CheckList())
			{
				RecusiveMove(strAppDir + TEMPFOLDER + "\\");
			}
            else
                Directory.Delete(strAppDir + TEMPFOLDER + "\\", true);

            if (CheckTemplateList())
            {
                RecusiveMove(strAppDir + TEMPFOLDER + "\\");
            }
            else
                Directory.Delete(strAppDir + TEMPFOLDER + "\\", true);

			label1.Text = "Starting Manager...";
			StartAgent();
			this.Close();
		}

		bool DownloadWebFile(string file_url, string dest_url)
		{
			try
			{
				WebClient client = new WebClient();

				client.UseDefaultCredentials = true;

				FileInfo info = new FileInfo(dest_url);
				if (!Directory.Exists(info.DirectoryName))
					Directory.CreateDirectory(info.DirectoryName);

				logger.Info("Downloading " + info.Name);

				client.DownloadFile(file_url, dest_url);
			}
			catch (Exception ee)
			{
				logger.Error(ee.ToString());
				return false;
			}

			return true;
		}
		bool IsVersionUP(string currVersion, string newVersion)
		{
			string[] currFileVersion = currVersion.Split('.');
			string[] newFileVersion = newVersion.Split('.');

			if (Convert.ToInt32(newFileVersion[0]) > Convert.ToInt32(currFileVersion[0]))
			{
				return true;
			}
			else if (Convert.ToInt32(newFileVersion[0]) == Convert.ToInt32(currFileVersion[0]))
			{
				if (Convert.ToInt32(newFileVersion[1]) > Convert.ToInt32(currFileVersion[1]))
				{
					return true;
				}
				else if (Convert.ToInt32(newFileVersion[1]) == Convert.ToInt32(currFileVersion[1]))
				{
					if (Convert.ToInt32(newFileVersion[2]) > Convert.ToInt32(currFileVersion[2]))
					{
						return true;
					}
					else if (Convert.ToInt32(newFileVersion[2]) == Convert.ToInt32(currFileVersion[2]))
					{
						if (Convert.ToInt32(newFileVersion[3]) > Convert.ToInt32(currFileVersion[3]))
						{
							return true;
						}
					}
				}
			}

			return false;
		}

		bool CheckList()
		{
			label1.Text = "Checking version...";
			logger.Info("Checking version...");

			//	임시 폴더 생성.
			if (!Directory.Exists(strAppDir + TEMPFOLDER))
				Directory.CreateDirectory(strAppDir + TEMPFOLDER);

            #region 매니저 다운로드
            if (!DownloadWebFile(UPDATEURL + UPDATELIST, strAppDir + TEMPFOLDER + "\\" + UPDATELIST))
				return false;

			Collection<string> arrWillUpdate = new Collection<string>();

			using (StreamReader sr = new StreamReader(strAppDir + TEMPFOLDER + "\\" + UPDATELIST))
			{
				string line = sr.ReadLine();
				do
				{
					try
					{
						string[] split = line.Split('\t');

						string filename = split[0];
						string fileversion = split[1];

						try
						{
							FileVersionInfo info = FileVersionInfo.GetVersionInfo(strAppDir + filename);

							if (IsVersionUP(info.FileVersion, fileversion))
							{
								try
								{
									string real_filename = split[2];
									arrWillUpdate.Add(real_filename);
								}
								catch
								{
									arrWillUpdate.Add(filename);
								}
							}

						}
						catch
						{
							try
							{
								string real_filename = split[2];
								arrWillUpdate.Add(real_filename);
							}
							catch
							{
								arrWillUpdate.Add(filename);
							}
						}
					}
					catch {}
				} while ((line = sr.ReadLine()) != null);
			}

			if (arrWillUpdate.Count > 0)
			{
				progressBar1.Maximum = arrWillUpdate.Count * 2;
				progressBar1.Step = 1;
			}

			foreach(string file in arrWillUpdate)
			{
				label1.Text = "Downloading : " + file;

				if (!DownloadWebFile(UPDATEURL + file + UPDATE_EXT, strAppDir + TEMPFOLDER + "\\" + file))
				{
					return false;
				}

				progressBar1.PerformStep();
            }
            #endregion 매니저 다운로드

            

            return true;
		}

        bool CheckTemplateList()
        {

            try
            {
                #region 템플릿 다운로드

                if (!Directory.Exists(strAppDir + TEMPFOLDER))
                    Directory.CreateDirectory(strAppDir + TEMPFOLDER);

                // 템플릿 헤더 정보를 가지는 XML 파일 다운로드...
                if (!DownloadWebFile(UPDATETEMPLATEURL + UPDATETEMPLATE, strAppDir + TEMPFOLDER + "\\" + TEMPLATEFOLDER + "\\" + UPDATERESULTHEADER))
                    return false;

                if (!DownloadWebFile(UPDATETEMPLATEURL + UPDATETEMPLATE, strAppDir + TEMPFOLDER + "\\" + TEMPLATEFOLDER + "\\" + UPDATETEMPLATE))
                    return false;

                string newTemplate = GetLastUpdateTime(strAppDir + TEMPFOLDER + "\\" + TEMPLATEFOLDER + "\\" + UPDATETEMPLATE);
                string oldTemplate = GetLastUpdateTime(strAppDir + TEMPLATEFOLDER + "\\" + UPDATETEMPLATE);
                if (newTemplate == oldTemplate)
                    return false;

                Collection<string> arrWillUpdate = GetLoadDownloadlist(strAppDir + TEMPFOLDER + "\\" + TEMPLATEFOLDER + "\\" + UPDATETEMPLATE);

                if (arrWillUpdate.Count > 0)
                {
                    progressBar1.Maximum = arrWillUpdate.Count * 2;
                    progressBar1.Step = 1;
                }

                foreach (string file in arrWillUpdate)
                {
                    label1.Text = "Downloading : " + file;

                    if (!DownloadWebFile(UPDATETEMPLATEURL + CONTENTSFOLDER + "/" + file, strAppDir + TEMPFOLDER + "\\" + CONTENTSFOLDER + "\\" + file))
                    {
                        return false;
                    }

                    progressBar1.PerformStep();
                }
                #endregion 템플릿 다운로드

                return true;
            }
            catch
            {

            }
            return false;
        }

        string GetLastUpdateTime(string path)
        {
            try
            {
                XmlDocument _doc = new XmlDocument();
                _doc.Load(path);
                XmlNode root = _doc.SelectSingleNode("//template_list/last_update_time");

                return root.InnerText;
            }
            catch
            {
            }
            return "";
        }

        Collection<string> GetLoadDownloadlist(string path)
        {
            Collection<string> arrWillUpdate = new Collection<string>();

            try
            {
                XmlDocument _doc = new XmlDocument();
                _doc.Load(path);
                XmlNodeList nodeList = _doc.SelectNodes("//template_list/template");

                for (int i = 0; i < nodeList.Count; i++)
                {
                    XmlNode nodes = nodeList.Item(i);
                    
                    string filename = nodes.Attributes["file_nm"].Value;
                    if (File.Exists(strAppDir + "Contents\\" + filename) == false)
                        arrWillUpdate.Add(filename);

                    filename = nodes.Attributes["thumbnail_nm"].Value;
                    if (File.Exists(strAppDir + "Contents\\" + filename) == false)
                        arrWillUpdate.Add(filename);
                }
            }
            catch
            {
            }
            return arrWillUpdate;
        }

		bool RecusiveMove(string sourceFolder)
		{
			try
			{
				logger.Info("Found Temp Folder.. Will be update!");

				string destFolder = sourceFolder.Replace("\\"+TEMPFOLDER+"\\", "\\");
				if (!Directory.Exists(destFolder))
					Directory.CreateDirectory(destFolder);

				string[] directories = Directory.GetDirectories(sourceFolder);

				foreach(string directory in directories)
				{
					if (!RecusiveMove(directory))
						return false;
				}

				string[] files = Directory.GetFiles(sourceFolder);

				foreach(string file in files)
				{
					label1.Text = "Moving : " + file;
					
					logger.Info("Updating : " + file);

					string destFile = file.Replace("\\" + TEMPFOLDER + "\\", "\\");
					if(destFile.Contains("update.txt"))
					{
						destFile.Replace("update.txt", DateTime.Now.ToShortDateString() + ".txt");
					}
					File.Copy(file, destFile, true);
					
					progressBar1.PerformStep();
				}

				Directory.Delete(sourceFolder, true);

				return true;
			}
			catch (Exception ee)
			{
				logger.Error(ee.ToString());
				return false;
			}
		}

		void StartAgent()
		{
			// get assebly path

			Process proc = new Process();
			proc.StartInfo.FileName = strAppDir + "\\i-Vision.Manager.exe";
			proc.StartInfo.WorkingDirectory = strAppDir;
			proc.Start();
		}
	}
}
