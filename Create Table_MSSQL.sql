BEGIN TRANSACTION;

create database ivision;

/*
CREATE TABLE client_logs (name VARCHAR(1024), category INT, code INT, description VARCHAR(1024), id INTEGER IDENTITY(1,1) PRIMARY KEY, reg_dt INT, type INT, uuid VARCHAR(1024), is_ad VARCHAR(2));
CREATE TABLE logmaster (category INT, id INTEGER IDENTITY(1,1) PRIMARY KEY, lastupdatedate INT, type INT, player_id char(13));
CREATE TABLE logs (name VARCHAR(1024), uuid VARCHAR(1024), code INT, description VARCHAR(1024), id INTEGER IDENTITY(1,1) PRIMARY KEY, master_id INT, reg_dt INT, is_ad VARCHAR(2));
*/

CREATE TABLE geo_master
(
	geocode INT PRIMARY KEY,
	x float,
	y float,
	postcode VARCHAR(9),
	address VARCHAR(256),
	ref_code VARCHAR(10)
);

CREATE TABLE client_logs (pid char(13), name VARCHAR(1024), description VARCHAR(1024), logcd int, error_code int, start_dt bigint, end_dt bigint, uuid CHAR(36));

CREATE TABLE users (id bigint IDENTITY(1,1) PRIMARY KEY, name VARCHAR(1024) UNIQUE, hash VARCHAR(1024), parent_id INTEGER, role_ids VARCHAR(1024), is_enabled char(1) DEFAULT 'Y');

CREATE TABLE groups (path VARCHAR(64), username VARCHAR(32), password VARCHAR(32), gid char(13) PRIMARY KEY, name VARCHAR(32), source VARCHAR(64), geocode int);

ALTER TABLE groups
	ADD parent_gid char(13) NULL,
	depth int NULL, 
	is_inherited char(1) NULL DEFAULT 'N',
	del_yn char(1) NULL DEFAULT 'N';

ALTER TABLE groups
	ADD CONSTRAINT parent_gid_fkey
	FOREIGN KEY(parent_gid)
	REFERENCES groups(gid);

ALTER TABLE groups
	ADD CONSTRAINT g_ref_code_fkey
	FOREIGN KEY(geocode)
	REFERENCES geo_master(geocode);
	
CREATE TABLE players (pid char(13) PRIMARY KEY, gid char(13), name VARCHAR(1024), host VARCHAR(128), state int, lastconndt bigint, versionH int, versionL int, del_yn char(1) default 'N', tz_info varchar(6), geocode int);

ALTER TABLE players
	ADD CONSTRAINT players_fkey
	FOREIGN KEY(gid)
	REFERENCES groups(gid);

ALTER TABLE players
	ADD CONSTRAINT p_ref_code_fkey
	FOREIGN KEY(geocode)
	REFERENCES geo_master(geocode);

CREATE TABLE player_detailinfo ( 
	pid      	char(13) UNIQUE,
	_ipv4addr	varchar(16) NULL,
	_is_dhcp 	varchar(2) NULL,
	_macaddr 	varchar(17) NULL,
	_mnttype 	int NULL, 
	_auth_id 	varchar(50) NULL,
	_auth_pass	varchar(50) NULL
	)

ALTER TABLE player_detailinfo
	ADD CONSTRAINT foreign_pid
	FOREIGN KEY(pid)
	REFERENCES players(pid);

CREATE TABLE downloadtime (d_id bigint PRIMARY KEY, starttime char(8), endtime char(8));
CREATE TABLE dt_players_asn (d_id bigint, pid char(13), FOREIGN KEY(pid) REFERENCES players(pid), FOREIGN KEY(d_id) REFERENCES downloadtime(d_id), PRIMARY KEY (d_id, pid));
CREATE TABLE dt_groups_asn (d_id bigint, gid char(13), FOREIGN KEY(gid) REFERENCES groups(gid), FOREIGN KEY(d_id) REFERENCES downloadtime(d_id), PRIMARY KEY (d_id, gid));

CREATE TABLE relaylogs
(
	relay_id bigint PRIMARY KEY,
	created_ts bigint NOT NULL,
	pid char(13),
	client_up_ts bigint,
	app_srv_up_ts bigint,
	relay_srv_up_ts bigint,
	relay_srv_down_ts bigint,
	app_srv_down_ts bigint,
	client_down_ts bigint
);

ALTER TABLE relaylogs
	ADD CONSTRAINT foreign_r_pid
	FOREIGN KEY(pid)
	REFERENCES players(pid);

CREATE TABLE relaylogs_error
(
	id int IDENTITY(1,1) PRIMARY KEY,
	relay_id bigint,
	err_tp int,
	err_cd int,
	err_ts bigint
)

ALTER TABLE relaylogs_error
	ADD CONSTRAINT foreign_r_e_relay_id
	FOREIGN KEY(relay_id)
	REFERENCES relaylogs(relay_id);

CREATE TABLE relaylogs_error_defines
(
	err_tp int NOT NULL,
	err_cd int NOT NULL,
	err_message VARCHAR(1024)
)
ALTER TABLE relaylogs_error_defines
	ADD PRIMARY KEY(err_tp, err_cd);

CREATE TABLE relaylogs_detail
(
	relay_id bigint,
	r_key VARCHAR(32),
	r_value VARCHAR(32)
);

ALTER TABLE relaylogs_detail
	ADD CONSTRAINT foreign_r_id
	FOREIGN KEY(relay_id)
	REFERENCES relaylogs(relay_id);

CREATE TABLE tasks (priority INT, endtimeofday INT, starttimeofday INT, daysofweek INT, duration INT, pid char(13), gid char(13), starttime BIGINT, endtime BIGINT, etime BIGINT, type INT, state INT, uuid char(36) PRIMARY KEY, guuid char(36), duuid char(36), name VARCHAR(1024), is_ad char(1), meta_tag VARCHAR(1024), show_flag int, tz_info varchar(6));

ALTER TABLE tasks
	ADD CONSTRAINT task_g_fkey
	FOREIGN KEY(gid)
	REFERENCES groups(gid);

ALTER TABLE tasks
	ADD CONSTRAINT task_p_fkey
	FOREIGN KEY(pid)
	REFERENCES players(pid);

CREATE TABLE screens (s_id bigint PRIMARY KEY, s_starttime bigint, s_endtime bigint, s_width int, s_height int, s_playtime int, s_name VARCHAR(256), s_description VARCHAR(256), s_screen_type int, s_display_type int, s_is_ad char(1), s_meta_tag VARCHAR(100), s_delete_yn char(1) DEFAULT 'N', s_regdt bigint);

CREATE TABLE screentask_asn (s_id bigint, uuid char(36), s_index int, thumb_url varchar(256));

ALTER TABLE screentask_asn
	ADD CONSTRAINT s_id
	FOREIGN KEY(s_id)
	REFERENCES screens(s_id);

ALTER TABLE screentask_asn
	ADD CONSTRAINT s_uuid
	FOREIGN KEY(uuid)
	REFERENCES tasks(uuid);

CREATE TABLE promotions
(
	id			int IDENTITY(1,1) PRIMARY KEY,
	screen_id	BIGINT NOT NULL,
	receiver	VARCHAR(20) NOT NULL,
	reserved	VARCHAR(100),
	regdt		BIGINT NOT NULL,
	proceeddt	BIGINT
);

ALTER TABLE promotions
	ADD CONSTRAINT p_sid
	FOREIGN KEY(screen_id)
	REFERENCES screens(s_id);

CREATE TABLE playlogs
(
    pid char(13), 
    logcd int,
    uuid char(36),
    tasknm VARCHAR(1024),
    screennm VARCHAR(1024),
	screen_id bigint,
    errorcd int,
    start_dt bigint,
	end_dt bigint,
	relay_id bigint
);

ALTER TABLE playlogs
	ADD CONSTRAINT t_log_fkey
	FOREIGN KEY(uuid)
	REFERENCES tasks(uuid);

ALTER TABLE playlogs
	ADD CONSTRAINT p_log_fkey
	FOREIGN KEY(pid)
	REFERENCES players(pid);

ALTER TABLE playlogs
	ADD CONSTRAINT s_log_fkey
	FOREIGN KEY(screen_id)
	REFERENCES screens(s_id);

ALTER TABLE playlogs
	ADD CONSTRAINT p_log_r_fkey
	FOREIGN KEY(relay_id)
	REFERENCES relaylogs(relay_id);

CREATE INDEX idx_playlog ON playlogs (pid, start_dt, logcd, relay_id);

CREATE TABLE downloadlogs
(
    pid char(13), 
    logcd int,
    uuid char(36),
    tasknm VARCHAR(1024),
    contentnm VARCHAR(1024),
    errorcd int,
    start_dt bigint,
	end_dt bigint
);

ALTER TABLE downloadlogs
	ADD CONSTRAINT d_log_fkey
	FOREIGN KEY(pid)
	REFERENCES players(pid);

CREATE INDEX idx_downloadlog ON downloadlogs (pid, start_dt, logcd);

CREATE TABLE managerlogs
(
    usernm VARCHAR(1024), 
    logcd int,
    uuid char(36),
    actionnm VARCHAR(1024),
    actiondesc VARCHAR(1024),
    errorcd int,
    reg_dt bigint
);

CREATE INDEX idx_managerlog ON managerlogs (reg_dt DESC, logcd);

CREATE TABLE connectionlogs
(
    pid char(13) NOT NULL, 
    logcd int NOT NULL,
    errorcd int,
    start_dt bigint,
    end_dt bigint,
	reg_dt bigint NOT NULL
);

ALTER TABLE connectionlogs
	ADD PRIMARY KEY(pid, logcd, reg_dt);

ALTER TABLE connectionlogs
	ADD CONSTRAINT c_log_fkey
	FOREIGN KEY(pid)
	REFERENCES players(pid);

CREATE INDEX idx_connectionlog ON connectionlogs (pid, reg_dt, logcd);

CREATE TABLE ivision_timestamps ( ts_key VARCHAR(50), ts_value bigint);

insert into ivision_timestamps (ts_key, ts_value) values ('meta_tag_groups', 1);
insert into ivision_timestamps (ts_key, ts_value) values ('relaylogs', 1);

CREATE TABLE meta_tags (m_id bigint PRIMARY KEY, g_id bigint, m_tagname VARCHAR(50), m_tagvalue VARCHAR(50), m_tagdescription VARCHAR(100));

CREATE TABLE meta_tag_groups (g_id bigint PRIMARY KEY, g_name VARCHAR(50), g_ui_type INTEGER, g_priority INTEGER);

ALTER TABLE meta_tags
	ADD CONSTRAINT g_id
	FOREIGN KEY(g_id)
	REFERENCES meta_tag_groups(g_id);

CREATE TABLE roles (role_id bigint IDENTITY(1,1) PRIMARY KEY, role_name VARCHAR(1024), role_level INTEGER, is_enabled char(1) DEFAULT 'Y');

CREATE TABLE permissions (perm_id bigint IDENTITY(1,1) PRIMARY KEY, perm_name VARCHAR(1024), parent_perm_id bigint);

create table tbl_limitplayers (gid char(13), cntplayers int );

CREATE TABLE SYNC_TASKS(GUUID CHAR(36) NOT NULL, PLAYERS_PID CHAR(13) NOT NULL, SYNC_TP CHAR(1) NOT NULL, EDIT_DT VARCHAR(14) NULL, REG_DT VARCHAR(14) NOT NULL, DEL_YN CHAR(1) NOT NULL, PRIMARY KEY(GUUID, PLAYERS_PID), CONSTRAINT XFK_SYNC_TASKS_PLAYERS_PID FOREIGN KEY(PLAYERS_PID) REFERENCES PLAYERS(PID));

ALTER TABLE tbl_limitplayers
	ADD CONSTRAINT limit_fkey
	FOREIGN KEY(gid)
	REFERENCES groups(gid);

CREATE TABLE datainfo ( 
    Id         	varchar(18) NOT NULL,
    User_id    	bigint NULL,
    Template_id	varchar(18) NULL,
    _Title     	VARCHAR(1024) NULL,
    PRIMARY KEY(Id)
);

CREATE TABLE data_field ( 
    Id         	varchar(18) NOT NULL,
    DataInfo_id	varchar(18) NULL,
    Value1     	VARCHAR(1024) NULL,
    Value2     	VARCHAR(1024) NULL,
    Value3     	VARCHAR(1024) NULL,
    Value4     	VARCHAR(1024) NULL,
    Value5     	VARCHAR(1024) NULL,
    Value6     	VARCHAR(1024) NULL,
    Value7     	VARCHAR(1024) NULL,
    Value8     	VARCHAR(1024) NULL,
    Value9     	VARCHAR(1024) NULL,
    Value10    	VARCHAR(1024) NULL,
    Value11    	VARCHAR(1024) NULL,
    Value12    	VARCHAR(1024) NULL,
    Value13    	VARCHAR(1024) NULL,
    Value14    	VARCHAR(1024) NULL,
    Value15    	VARCHAR(1024) NULL,
    Value16    	VARCHAR(1024) NULL,
    Value17    	VARCHAR(1024) NULL,
    Value18    	VARCHAR(1024) NULL,
    Value19    	VARCHAR(1024) NULL,
    Value20    	VARCHAR(1024) NULL,
    Value21    	VARCHAR(1024) NULL,
    Value22    	VARCHAR(1024) NULL,
    Value23    	VARCHAR(1024) NULL,
    Value24    	VARCHAR(1024) NULL,
    Value25    	VARCHAR(1024) NULL,
    Value26    	VARCHAR(1024) NULL,
    Value27    	VARCHAR(1024) NULL,
    Value28    	VARCHAR(1024) NULL,
    Value29    	VARCHAR(1024) NULL,
    Value30    	VARCHAR(1024) NULL,
    Field_name 	VARCHAR(1024) NULL,
    PRIMARY KEY(Id)
);

CREATE TABLE contents ( 
    id           	varchar(25) NOT NULL,
    Data_field_id	varchar(25) NULL,
    _Type        	varchar(25) NULL,
    _Value       	VARCHAR(1024) NULL,
    File_nm      	VARCHAR(1024) NULL,
    _Path        	VARCHAR(1024) NULL,
    _Desc        	VARCHAR(1024) NULL,
    Data_info_id 	varchar(25) NULL,
    PRIMARY KEY(id)
);

CREATE TABLE resultdatas ( 
    apply_date 	bigint NULL,
    playerid   	varchar(25) NULL,
    datafieldid	varchar(25) NULL,
    tempalteid 	varchar(25) NULL,
    data_desc  	VARCHAR(1024) NULL,
    data_values	VARCHAR(1024) NULL 
);

CREATE TABLE role_perm_ASN ( 
	role_id	bigint NOT NULL,
	perm_id	bigint NOT NULL 
	);

ALTER TABLE role_perm_ASN
	ADD PRIMARY KEY(role_id, perm_id);


ALTER TABLE role_perm_ASN
	ADD CONSTRAINT role_id
	FOREIGN KEY(role_id)
	REFERENCES roles(role_id);


ALTER TABLE role_perm_ASN
	ADD CONSTRAINT perm_id
	FOREIGN KEY(perm_id)
	REFERENCES permissions(perm_id);

CREATE TABLE user_player_ASN ( 
	user_id	bigint NOT NULL,
	pid    	char(13) NOT NULL 
	);

	ALTER TABLE user_player_ASN
	ADD PRIMARY KEY(user_id, pid);

ALTER TABLE user_player_ASN
	ADD CONSTRAINT user_id
	FOREIGN KEY(user_id)
	REFERENCES users(id);

ALTER TABLE user_player_ASN
	ADD CONSTRAINT pid
	FOREIGN KEY(pid)
	REFERENCES players(pid);

CREATE TABLE user_group_ASN (
	user_id bigint NOT NULL,
	gid char(13) NOT NULL);

ALTER TABLE user_group_ASN
	ADD PRIMARY KEY(user_id, gid);

ALTER TABLE user_group_ASN
	ADD CONSTRAINT g_user_id
	FOREIGN KEY(user_id)
	REFERENCES users(id);

ALTER TABLE user_group_ASN
	ADD CONSTRAINT gid
	FOREIGN KEY(gid)
	REFERENCES groups(gid);

insert into roles (role_name, role_level) VALUES ('SYSTEM ADMINISTRATOR', 0);
insert into roles (role_name, role_level) VALUES ('SERVICE ADMINISTRATOR', 1);
insert into roles (role_name, role_level) VALUES ('GROUP ADMINISTRATOR', 2);
insert into roles (role_name, role_level) VALUES ('PLAYER ADMINISTRATOR', 3);
insert into roles (role_name, role_level) VALUES ('OBSERVER', 3);
insert into roles (role_name, role_level) VALUES ('SCHEDULER', 3);
insert into roles (role_name, role_level) VALUES ('USER MANAGER', 3);
insert into roles (role_name, role_level) VALUES ('GROUP MANAGER', 3);
insert into roles (role_name, role_level) VALUES ('PLAYER MANAGER', 3);

insert into permissions (perm_name, parent_perm_id) VALUES ('ADMINISTRATOR', -1);
insert into permissions (perm_name, parent_perm_id) VALUES ('VIEW USER', -1);
insert into permissions (perm_name, parent_perm_id) VALUES ('CREATE USER', -1);
insert into permissions (perm_name, parent_perm_id) VALUES ('UPDATE USER', -1);
insert into permissions (perm_name, parent_perm_id) VALUES ('DELETE USER', -1);
insert into permissions (perm_name, parent_perm_id) VALUES ('VIEW PARENT GROUP', -1);
insert into permissions (perm_name, parent_perm_id) VALUES ('VIEW OWN GROUP', -1);
insert into permissions (perm_name, parent_perm_id) VALUES ('VIEW CHILD GROUP', -1);
insert into permissions (perm_name, parent_perm_id) VALUES ('VIEW PLAYER', -1);
insert into permissions (perm_name, parent_perm_id) VALUES ('CREATE PLAYER', -1);
insert into permissions (perm_name, parent_perm_id) VALUES ('CREATE GROUP', -1);
insert into permissions (perm_name, parent_perm_id) VALUES ('UPDATE PLAYER', -1);
insert into permissions (perm_name, parent_perm_id) VALUES ('UPDATE GROUP', -1);
insert into permissions (perm_name, parent_perm_id) VALUES ('DELETE PLAYER', -1);
insert into permissions (perm_name, parent_perm_id) VALUES ('DELETE GROUP', -1);
insert into permissions (perm_name, parent_perm_id) VALUES ('VIEW SCHEDULE', -1);
insert into permissions (perm_name, parent_perm_id) VALUES ('VIEW PARENT SCHEDULE', -1);
insert into permissions (perm_name, parent_perm_id) VALUES ('CREATE SCHEDULE', -1);
insert into permissions (perm_name, parent_perm_id) VALUES ('UPDATE SCHEDULE', -1);
insert into permissions (perm_name, parent_perm_id) VALUES ('DELETE SCHEDULE', -1);

insert into role_perm_ASN (role_id, perm_id) VALUES (1, 1);

insert into role_perm_ASN (role_id, perm_id) VALUES (2,2);
insert into role_perm_ASN (role_id, perm_id) VALUES (2,3);
insert into role_perm_ASN (role_id, perm_id) VALUES (2,4);
insert into role_perm_ASN (role_id, perm_id) VALUES (2,5);
insert into role_perm_ASN (role_id, perm_id) VALUES (2,7);
insert into role_perm_ASN (role_id, perm_id) VALUES (2,8);
insert into role_perm_ASN (role_id, perm_id) VALUES (2,9);
insert into role_perm_ASN (role_id, perm_id) VALUES (2,10);
insert into role_perm_ASN (role_id, perm_id) VALUES (2,11);
insert into role_perm_ASN (role_id, perm_id) VALUES (2,12);
insert into role_perm_ASN (role_id, perm_id) VALUES (2,13);
insert into role_perm_ASN (role_id, perm_id) VALUES (2,14);
insert into role_perm_ASN (role_id, perm_id) VALUES (2,15);
insert into role_perm_ASN (role_id, perm_id) VALUES (2,16);
insert into role_perm_ASN (role_id, perm_id) VALUES (2,17);
insert into role_perm_ASN (role_id, perm_id) VALUES (2,18);
insert into role_perm_ASN (role_id, perm_id) VALUES (2,19);
insert into role_perm_ASN (role_id, perm_id) VALUES (2,20);

insert into role_perm_ASN (role_id, perm_id) VALUES (3,2);
insert into role_perm_ASN (role_id, perm_id) VALUES (3,3);
insert into role_perm_ASN (role_id, perm_id) VALUES (3,4);
insert into role_perm_ASN (role_id, perm_id) VALUES (3,5);
insert into role_perm_ASN (role_id, perm_id) VALUES (3,7);
insert into role_perm_ASN (role_id, perm_id) VALUES (3,8);
insert into role_perm_ASN (role_id, perm_id) VALUES (3,9);
insert into role_perm_ASN (role_id, perm_id) VALUES (3,10);
insert into role_perm_ASN (role_id, perm_id) VALUES (3,11);
insert into role_perm_ASN (role_id, perm_id) VALUES (3,12);
insert into role_perm_ASN (role_id, perm_id) VALUES (3,13);
insert into role_perm_ASN (role_id, perm_id) VALUES (3,14);
insert into role_perm_ASN (role_id, perm_id) VALUES (3,15);
insert into role_perm_ASN (role_id, perm_id) VALUES (3,16);
insert into role_perm_ASN (role_id, perm_id) VALUES (3,17);
insert into role_perm_ASN (role_id, perm_id) VALUES (3,18);
insert into role_perm_ASN (role_id, perm_id) VALUES (3,19);
insert into role_perm_ASN (role_id, perm_id) VALUES (3,20);

insert into role_perm_ASN (role_id, perm_id) VALUES (4,2);
insert into role_perm_ASN (role_id, perm_id) VALUES (4,4);
insert into role_perm_ASN (role_id, perm_id) VALUES (4,9);
insert into role_perm_ASN (role_id, perm_id) VALUES (4,16);
insert into role_perm_ASN (role_id, perm_id) VALUES (4,17);
insert into role_perm_ASN (role_id, perm_id) VALUES (4,18);
insert into role_perm_ASN (role_id, perm_id) VALUES (4,19);
insert into role_perm_ASN (role_id, perm_id) VALUES (4,20);

insert into role_perm_ASN (role_id, perm_id) VALUES (5,6);
insert into role_perm_ASN (role_id, perm_id) VALUES (5,7);
insert into role_perm_ASN (role_id, perm_id) VALUES (5,8);
insert into role_perm_ASN (role_id, perm_id) VALUES (5,9);
insert into role_perm_ASN (role_id, perm_id) VALUES (5,16);
insert into role_perm_ASN (role_id, perm_id) VALUES (5,17);

insert into role_perm_ASN (role_id, perm_id) VALUES (6,7);
insert into role_perm_ASN (role_id, perm_id) VALUES (6,8);
insert into role_perm_ASN (role_id, perm_id) VALUES (6,9);
insert into role_perm_ASN (role_id, perm_id) VALUES (6,16);
insert into role_perm_ASN (role_id, perm_id) VALUES (6,17);
insert into role_perm_ASN (role_id, perm_id) VALUES (6,18);
insert into role_perm_ASN (role_id, perm_id) VALUES (6,19);
insert into role_perm_ASN (role_id, perm_id) VALUES (6,20);

insert into role_perm_ASN (role_id, perm_id) VALUES (7,2);
insert into role_perm_ASN (role_id, perm_id) VALUES (7,3);
insert into role_perm_ASN (role_id, perm_id) VALUES (7,4);
insert into role_perm_ASN (role_id, perm_id) VALUES (7,5);

insert into role_perm_ASN (role_id, perm_id) VALUES (8,7);
insert into role_perm_ASN (role_id, perm_id) VALUES (8,8);
insert into role_perm_ASN (role_id, perm_id) VALUES (8,11);
insert into role_perm_ASN (role_id, perm_id) VALUES (8,13);
insert into role_perm_ASN (role_id, perm_id) VALUES (8,16);

insert into role_perm_ASN (role_id, perm_id) VALUES (9,9);
insert into role_perm_ASN (role_id, perm_id) VALUES (9,10);
insert into role_perm_ASN (role_id, perm_id) VALUES (9,12);
insert into role_perm_ASN (role_id, perm_id) VALUES (9,14);

select * from role_perm_ASN;

insert into relaylogs_error_defines (err_tp, err_cd, err_message) values (1,1,'일반적인 에러가 발생하였습니다.');
insert into relaylogs_error_defines (err_tp, err_cd, err_message) values (1,2,'서버와 접속이 끊겼습니다.');
insert into relaylogs_error_defines (err_tp, err_cd, err_message) values (1,3,'타임 아웃이 발생하였습니다.');
insert into relaylogs_error_defines (err_tp, err_cd, err_message) values (1,4,'내부 에러가 발생하였습니다.');
insert into relaylogs_error_defines (err_tp, err_cd, err_message) values (1,5,'PlayAgent와 Player의 데이터 전달 간에 에러가 발생하였습니다.');
insert into relaylogs_error_defines (err_tp, err_cd, err_message) values (1,6,'정의되지 않은 데이터가 수신되었습니다.');

insert into relaylogs_error_defines (err_tp, err_cd, err_message) values (2,1,'일반적인 에러가 발생하였습니다.');
insert into relaylogs_error_defines (err_tp, err_cd, err_message) values (2,2,'서버와 접속이 끊겼습니다.');
insert into relaylogs_error_defines (err_tp, err_cd, err_message) values (2,3,'타임 아웃이 발생하였습니다.');
insert into relaylogs_error_defines (err_tp, err_cd, err_message) values (2,4,'내부 에러가 발생하였습니다.');
insert into relaylogs_error_defines (err_tp, err_cd, err_message) values (2,5,'PlayAgent와 Player의 데이터 전달 간에 에러가 발생하였습니다.');
insert into relaylogs_error_defines (err_tp, err_cd, err_message) values (2,6,'정의되지 않은 데이터가 수신되었습니다.');

insert into relaylogs_error_defines (err_tp, err_cd, err_message) values (3,1,'일반적인 에러가 발생하였습니다.');
insert into relaylogs_error_defines (err_tp, err_cd, err_message) values (3,2,'클라이언트와 접속이 끊겼습니다.');
insert into relaylogs_error_defines (err_tp, err_cd, err_message) values (3,3,'릴레이 서버와 접속이 끊겼습니다.');
insert into relaylogs_error_defines (err_tp, err_cd, err_message) values (3,4,'데이터베이스 서버와 접속이 끊겼습니다.');
insert into relaylogs_error_defines (err_tp, err_cd, err_message) values (3,5,'타임 아웃이 발생하였습니다.');
insert into relaylogs_error_defines (err_tp, err_cd, err_message) values (3,6,'내부 에러가 발생하였습니다.');
insert into relaylogs_error_defines (err_tp, err_cd, err_message) values (3,7,'데이터 분석에 실패하였습니다.');
insert into relaylogs_error_defines (err_tp, err_cd, err_message) values (3,8,'정의되지 않은 데이터가 수신되었습니다.');

insert into relaylogs_error_defines (err_tp, err_cd, err_message) values (4,1,'일반적인 에러가 발생하였습니다.');
insert into relaylogs_error_defines (err_tp, err_cd, err_message) values (4,2,'클라이언트와 접속이 끊겼습니다.');
insert into relaylogs_error_defines (err_tp, err_cd, err_message) values (4,3,'릴레이 서버와 접속이 끊겼습니다.');
insert into relaylogs_error_defines (err_tp, err_cd, err_message) values (4,4,'데이터베이스 서버와 접속이 끊겼습니다.');
insert into relaylogs_error_defines (err_tp, err_cd, err_message) values (4,5,'타임 아웃이 발생하였습니다.');
insert into relaylogs_error_defines (err_tp, err_cd, err_message) values (4,6,'내부 에러가 발생하였습니다.');
insert into relaylogs_error_defines (err_tp, err_cd, err_message) values (4,7,'데이터 분석에 실패하였습니다.');
insert into relaylogs_error_defines (err_tp, err_cd, err_message) values (4,8,'정의되지 않은 데이터가 수신되었습니다.');



INSERT INTO users (name,hash,role_ids, parent_id,is_enabled) VALUES ('admin','21232f297a57a5a743894a0e4a801fc3','1','-1','Y');

INSERT INTO groups (gid,name,source,path,password, username, depth) VALUES ('0000000000000','ROOT NETWORK','','', '', '', 0);

insert into user_group_asn (user_id, gid) values (1, '0000000000000');

CREATE TABLE downloadmaster (uuid char(36) NOT NULL, pid char(13) NOT NULL, reg_date BIGINT);

ALTER TABLE downloadmaster
	ADD PRIMARY KEY(uuid, pid);

ALTER TABLE downloadmaster
	ADD CONSTRAINT d_master_uuid
	FOREIGN KEY(uuid)
	REFERENCES tasks(uuid);

ALTER TABLE downloadmaster
	ADD CONSTRAINT d_master_pid
	FOREIGN KEY(pid)
	REFERENCES players(pid);

COMMIT;



CREATE TABLE report_normal_schedule
(
	YYYY                 CHAR(4)  NOT NULL ,
	MM                   CHAR(2)  NOT NULL ,
	DD                   CHAR(2)  NOT NULL ,
	CNT                  int  NULL ,
	pid                  char(13)  NOT NULL 
)
go


ALTER TABLE report_normal_schedule
	ADD CONSTRAINT XPKreport_normal_schedule PRIMARY KEY  NONCLUSTERED (pid ASC,YYYY ASC,MM ASC,DD ASC)
go


CREATE TABLE report_play
(
	YYYY                 CHAR(4)  NOT NULL ,
	MM                   CHAR(2)  NOT NULL ,
	DD                   CHAR(2)  NOT NULL ,
	H24                  CHAR(2)  NOT NULL ,
	CNT                  int  NULL ,
	s_id                 bigint  NOT NULL 
)
go


ALTER TABLE report_play
	ADD CONSTRAINT XPKreport_play PRIMARY KEY  NONCLUSTERED (s_id ASC,YYYY ASC,MM ASC,DD ASC,H24 ASC)
go


CREATE TABLE report_relay
(
	YYYY                 CHAR(4)  NOT NULL ,
	MM                   CHAR(2)  NOT NULL ,
	DD                   CHAR(2)  NOT NULL ,
	H24                  CHAR(2)  NOT NULL ,
	g_id                 bigint  NOT NULL ,
	CNT                  int  NULL ,
	r_key                VARCHAR(32)  NOT NULL 
)
go


ALTER TABLE report_relay
	ADD CONSTRAINT XPKreport_relay PRIMARY KEY  CLUSTERED (g_id ASC,r_key ASC,YYYY ASC,MM ASC,DD ASC,H24 ASC)
go


CREATE TABLE report_running_time
(
	pid                  char(13)  NOT NULL ,
	YYYY                 CHAR(4)  NOT NULL ,
	MM                   CHAR(2)  NOT NULL ,
	DD                   CHAR(2)  NOT NULL ,
	H24                  CHAR(2)  NOT NULL ,
	TM                   int  NULL 
)
go


ALTER TABLE report_running_time
	ADD CONSTRAINT XPKreport_running_time PRIMARY KEY  NONCLUSTERED (pid ASC,YYYY ASC,MM ASC,DD ASC,H24 ASC)
go


CREATE TABLE report_time_schedule
(
	pid                  char(13)  NOT NULL ,
	YYYY                 CHAR(4)  NOT NULL ,
	MM                   CHAR(2)  NOT NULL ,
	DD                   CHAR(2)  NOT NULL ,
	H24                  CHAR(2)  NOT NULL ,
	type		     int  NULL,
	CNT                  int  NULL 
)
go

CREATE TABLE summary_players
(
	pid			char(13)  NOT NULL ,
	YYYY		CHAR(4)  NOT NULL ,
	MM			CHAR(2)  NOT NULL ,
	DD			CHAR(2)  NOT NULL ,
	H24			CHAR(2)  NOT NULL ,
	gid			char(13) NOT NULL,
	name		VARCHAR(1024), 
	host		VARCHAR(128), 
	state		int, 
	uptime		int,
	lastconndt	bigint, 
	versionH	int, 
	versionL	int,
	freespace	int,
	memoryusage int,
	cpurate		int,
	volume		int,
	serial_power VARCHAR(10),
	serial_source VARCHAR(50),
	serial_volume int
);

ALTER TABLE summary_players
	ADD CONSTRAINT XPKsummary_players PRIMARY KEY  NONCLUSTERED (pid ASC,YYYY ASC,MM ASC,DD ASC,H24 ASC);

ALTER TABLE report_time_schedule
	ADD CONSTRAINT XPKreport_time_schedule PRIMARY KEY  NONCLUSTERED (pid ASC,YYYY ASC,MM ASC,DD ASC,H24 ASC, type ASC)
go



ALTER TABLE report_normal_schedule
	ADD CONSTRAINT  R_38 FOREIGN KEY (pid) REFERENCES players(pid)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go



ALTER TABLE report_play
	ADD CONSTRAINT  R_33 FOREIGN KEY (s_id) REFERENCES screens(s_id)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go



ALTER TABLE report_relay
	ADD CONSTRAINT  R_36 FOREIGN KEY (g_id) REFERENCES meta_tag_groups(g_id)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go



ALTER TABLE report_running_time
	ADD CONSTRAINT  R_34 FOREIGN KEY (pid) REFERENCES players(pid)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go



ALTER TABLE report_time_schedule
	ADD CONSTRAINT  R_37 FOREIGN KEY (pid) REFERENCES players(pid)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go

