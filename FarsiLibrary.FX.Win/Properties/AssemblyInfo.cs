﻿using System.Reflection;
using System.Runtime.InteropServices;
using System.Windows;

[assembly: AssemblyTitle("FarsiLibrary.FX.Win")]
[assembly: AssemblyDescription("Custom Control library containing FXDatePicker and Calendar controls.")]
[assembly: AssemblyProduct("FarsiLibrary.FX.Win")]
[assembly: AssemblyCopyright("Copyright Hadi Eskandari @ 2007")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyVersion("2.1.0.1038")]
[assembly: AssemblyFileVersion("2.1.0.1038")]
[assembly: ThemeInfo(ResourceDictionaryLocation.SourceAssembly, ResourceDictionaryLocation.SourceAssembly)]
[assembly: ComVisible(false)]
