﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Windows.Navigation;
using System.Runtime.InteropServices;
using System.Threading;

// .NET remoting
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;
using System.Runtime.Remoting.Channels.Tcp;

using DigitalSignage.Common;
using DigitalSignage.ServerDatabase;

using NLog;
using System.Configuration;
using System.Xml;

namespace DigitalSignage.PlayAgent
{
	class NetworkMonitor
	{
		#region SYSTEMTIME definition
		[DllImport("kernel32.dll")]
		private static extern bool SetSystemTime([In] ref SYSTEMTIME st);
		struct SYSTEMTIME
		{
			public short wYear;
			public short wMonth;
			public short wDayOfWeek;
			public short wDay;
			public short wHour;
			public short wMinute;
			public short wSecond;
			public short wMilliseconds;
		}
		#endregion

		//	hsshin WCF Connection 추가
		WCFAuthoring wcfAuth = WCFAuthoring.GetWCFObject;

		private static Logger logger = LogManager.GetCurrentClassLogger();

		private bool process = true;
		private Config config = Config.GetConfig;
		private long lastUptimeUpdate = 0;

		private Thread ul = null;
        private Thread ul2 = null;
        private Thread ul3 = null;

		private Thread thProccess = null;
		private Thread ds = null;

        private Thread capture = null;
		private DateTime dtThreadCreateTime = DateTime.Now;

		private const int WAIT_DOWNLOAD_SLEEP_SECOND = 10;
		private const int WAIT_NOT_YET_TIME_TO_DOWNLOAD_SECOND = 60;

		private static object lockObj = new object();

		private static object lockTaskState = new object();

		//	Log Db
		private ClientDataBase.LogsDataSetTableAdapters.client_logsTableAdapter client_logsTA = null;

		public void start()
		{
			this.process = true;
			//	Task 동기화 관련 쓰레드 
			thProccess = new Thread(this.proc);
			thProccess.Priority = ThreadPriority.Normal;
			thProccess.Start();
			//	다운로더 스레드를 생성한다.
			ds = new Thread(this.DownloadThread);
			ds.Priority = ThreadPriority.AboveNormal;
			ds.Start();
			//	로그업데이트하는 쓰레드를 생성한다.
			ul = new Thread(this.UploadLogThread);
			ul.Priority = ThreadPriority.BelowNormal;
			ul.Start();

            capture = new Thread(this.DoCapture);
            capture.Priority = ThreadPriority.Lowest;
            capture.Start();


            ul2 = new Thread(this.UploadTargetAdLogThread);
            ul2.Priority = ThreadPriority.BelowNormal;
            ul2.Start();

            ul3 = new Thread(this.UploadPromotionsThread);
            ul3.Priority = ThreadPriority.BelowNormal;
            ul3.Start();
		}

		public void stop()
		{
			process = false;
			Thread.Sleep(2000);

			if (ul != null) ul.Abort();
            if (ul2 != null) ul2.Abort();
            if (ul3 != null) ul3.Abort();
			if (ds != null) ds.Abort();
			if (thProccess != null) thProccess.Abort();
            if (capture != null) capture.Abort();

		}

		public void proc()
		{
			Thread thSync = null;
			Thread ts = null;

			while (process)
			{
				//	Synchronize
				if (config.HasWCFAuthoring)
				{
					try
					{
						if (thSync != null)
						{
							try
							{
								if (!thSync.IsAlive)
								{
									thSync.Abort();
									thSync = null;
								}
								else
								{
									//////////////////////////////////////////////////////////////////////////
									// 스레드가 시작된 시간이 5분이 넘으면 강제종료
									TimeSpan tsTerm = DateTime.Now - dtThreadCreateTime;
									if (tsTerm.TotalMinutes >= 5.0)
									{
										logger.Error("Synchronize Thread runed during 5 minute over. It will be terminate!");
										thSync.Abort();
										thSync = null;
									}
								}
							}
							catch {
								thSync = null;
							}
						}

						if (thSync == null)
						{
							thSync = new Thread(this.startSynchronize);
							thSync.Start();

							dtThreadCreateTime = DateTime.Now;
						}

						if (ts != null)
						{
							try
							{
								if (ts.IsAlive)
								{
									ts.Abort();
								}
							}
							catch { }
							finally 
							{
								ts = null;
							}
						}
						ts = new Thread(this.returnStatus);
						ts.Start();
					}
					catch (Exception e)
					{
						logger.Error(e.ToString());

						wcfAuth.RetryTask();
					}
				}

				Thread.Sleep(15000);
			}
		}
		#region Network Functions

        /// <summary>
        /// 동기화 로직 서버로 부터 모델 받아오기 수행
        /// </summary>
        private void startSyncScheduleSynchronize()
        {
            try
            {
                logger.Debug("[startSyncScheduleSynchronize] begin!");

                string last_ts = config.LastSyncTaskSyncFromFile;

                lock (lockObj)
                {
                    using (ServerDataSet.SYNC_TASKSDataTable newSyncData = config.ServerTaskList.GetNewSyncTasks(last_ts, config.PlayerId))
                    {
                        if (newSyncData != null)
                        {
                            if (newSyncData.Rows.Count > 0)
                                logger.Info("Found " + newSyncData.Rows.Count + " new Sync Task Data");

                            foreach (ServerDataSet.SYNC_TASKSRow row in newSyncData.Rows)
                            {
                                //  데이터 갱신
                                if (config.localTasksList.InsertOrReplaceSyncTask(row))
                                {
                                    if (last_ts.CompareTo(row.EDIT_DT) < 0) config.LastSyncTaskSyncFromFile = last_ts = row.EDIT_DT;
                                }
                            }
                            newSyncData.Dispose();
                        }
                    }

                }

                return;

            }
            catch (AccessViolationException ave)
            {
                logger.Error(ave.ToString());

                logger.Error("AccessViolationException.. PlayAgent will be terminated.");
                Environment.Exit(0);
            }
            catch (System.Net.Sockets.SocketException sockEx)
            {
                logger.Error(sockEx.Message);
            }
            catch (Exception e)
            {
                logger.Error(e.ToString());
                wcfAuth.RetryTask();
                config.HasWCFAuthoring = false;
            }
            catch
            {
                logger.Error("Unhandled Exception occured!");
                wcfAuth.RetryTask();
                config.HasWCFAuthoring = false;
            }
        }

		private void startSynchronize()
		{
            /// 동기화 스케줄 조회
            startSyncScheduleSynchronize();

            try
            {
                logger.Debug("[startSynchronize] begin!");

                // 				if (config.receiveNetworkConfiguration() == -1)
                // 				{
                // 					//	접속이 안되거나 Information이 잘못되어있음
                // 					if (config.ServerOnline == false)
                // 					{
                // 						
                // 						config.HasWCFAuthoring = false;
                // 						wcfAuth.RetryTask();
                // 					}
                // 					throw new Exception("Wrong configuration or problem of network.");
                // 				}
                // updating uptime data
                long ctime = TimeConverter.ConvertToUTP(DateTime.Now) / 60;
                if (lastUptimeUpdate == 0) lastUptimeUpdate = ctime;
                config.Uptime = ctime - lastUptimeUpdate;

                long previos_ts = config.LastSyncFromFile;

                // updating clock
                if (!config.ServerPath.Contains("127.0.0.1"))
                {
                    if (config.ShouldSyncTime)
                    {
                        SYSTEMTIME st = new SYSTEMTIME();
                        long current_ts = config.ServerCmdReceiver.ProcessSimpleCommand(CmdReceiverCommands.CmdGetTimeStamp);
                        logger.Info("Server TS=" + current_ts);

                        DateTime ctDay = TimeConverter.ConvertFromUTP(current_ts);
                        st.wYear = (short)ctDay.Year;
                        st.wMonth = (short)ctDay.Month;
                        st.wDay = (short)ctDay.Day;
                        st.wHour = (short)ctDay.Hour;
                        st.wMinute = (short)ctDay.Minute;
                        st.wSecond = (short)(ctDay.Second + 1);
                        st.wMilliseconds = (short)(ctDay.Millisecond);

                        if (!SetSystemTime(ref st))
                            logger.Error("[Synchronize to server] SetSystemTime has been failed");
                    }
                }


                ServerDataSet.tasksDataTable newTasks = null;

                lock (lockObj)
                {
                    long server_ts = config.ServerTaskList.GetTasksTimestamp();

                    if (server_ts > previos_ts)
                    {
                        // 이전에 얻어온 스케줄 이후로 얻어와야하기때문에 +1을 해줌
                        newTasks = config.ServerTaskList.GetNewTasks(previos_ts + 1, config.PlayerId, config.GroupIds);
                        config.LastSyncFromFile = server_ts;
                    }
                }

                if (newTasks != null)
                {
                    if (newTasks.Rows.Count > 0)
                        logger.Info("Found " + newTasks.Rows.Count + " new tasks");

                    long currentTick = TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime());

                    for (int i = 0; i < newTasks.Rows.Count; i++)
                    {

                        ServerDataSet.tasksRow row = (ServerDataSet.tasksRow)newTasks.Rows[i];
                        Task newTask = new Task(row);

						if ((newTask.type == TaskCommands.CmdProgram || newTask.type == TaskCommands.CmdSubtitles || newTask.type == TaskCommands.CmdDefaultProgram || newTask.type == TaskCommands.CmdTouchDefaultProgram || newTask.type == TaskCommands.CmdControlSchedule)
							&& newTask.state == TaskState.StateCanceled) //task deleting
						{
                            logger.Info("Found new task " + newTask.ToString());

                            newTask.state = TaskState.StateCanceled;
                            InsertOrReplaceTask(newTask);
                            continue;
                        }

                        //	hsshin 현재 시간보다 TASK가 종료되는 시간이 빠르면 다운 받을 필요가 없으므로 스킵한다.
                        if ((newTask.state != TaskState.StateEdit) && ((newTask.TaskStart < currentTick && newTask.TaskEnd == 0) || (newTask.TaskStart < currentTick && newTask.TaskEnd < currentTick)))
                        {
                            //	받을필요가 없으므로 넘긴다.
                            continue;
                        }

                        logger.Info("Found new task " + newTask.ToString());

						if ((newTask.type == TaskCommands.CmdProgram || newTask.type == TaskCommands.CmdSubtitles || newTask.type == TaskCommands.CmdDefaultProgram || newTask.type == TaskCommands.CmdTouchDefaultProgram)
							&& newTask.state == TaskState.StateCanceled) //task deleting
						{
                            newTask.state = TaskState.StateCanceled;
                            InsertOrReplaceTask(newTask);
                        }
                        else if (newTask.state == TaskState.StateWait)
                        {
                            if (newTask.type != TaskCommands.CmdControlSchedule)
                            {
                                newTask.state = TaskState.StateDownload;
                            }
                            else
                            {
                                //	제어스케줄은 물리적 파일이없기 때문에 스케줄을 받은 시점이 적용된 시점이 된다.
								InsertDownloadedInfoToServer(newTask.uuid.ToString());
//								UpdateDownloadedInfoToServer(newTask.duuid.ToString(), newTask.gid);
                            }

                            InsertOrReplaceTask(newTask);
                        }
                        else if (newTask.state == TaskState.StateEdit)
                        {
							//DownloadDelete(newTask.duuid.ToString());
                            InsertOrReplaceTask(newTask);
                        }
                    }

                    newTasks.Dispose();
                }

                return;
            }
            catch (AccessViolationException ave)
            {
                logger.Error(ave.ToString());

                logger.Error("AccessViolationException.. PlayAgent will be terminated.");
                Environment.Exit(0);
            }
			catch (System.Net.Sockets.SocketException sockEx)
			{
				logger.Error(sockEx.Message);
			}
            catch (Exception e)
            {
                logger.Error(e.ToString());
                wcfAuth.RetryTask();
                config.HasWCFAuthoring = false;
            }
            catch
            {
                logger.Error("Unhandled Exception occured!");
                wcfAuth.RetryTask();
                config.HasWCFAuthoring = false;
            }

		}
		public void AddNextScreen()
		{
			DigitalSignage.PlayAgent.Task t = new DigitalSignage.PlayAgent.Task();
			t.pid = config.PlayerId;
			t.gid = config.GroupId;
			t.type = TaskCommands.CmdNextDefaultProgram;
			t.uuid = DigitalSignage.PlayAgent.Task.TaskDefaultScreen;
			t.TaskStart = TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()) + 1;
			t.TaskEnd = t.TaskStart + 5;
			t.description = "CmdNextDefaultProgram";
			t.duration = 10;

			InsertOrReplaceTask(t);
		}

		//	hsshin Task가 중복되는 uuid를 가질 수 없기 때문에 중복된다면 지우고 추가해준다.
		private int InsertOrReplaceTask(DigitalSignage.PlayAgent.Task task)
		{
			int nRet = -1;
			try
			{
				lock (lockTaskState)
				{
					if (-1 == (nRet = config.localTasksList.AddTaskWithState(task)))
					{
						////	추가가 되지않음.
						config.localTasksList.CleanTask(task.uuid);
						//	다시 추가 시도
						nRet = config.localTasksList.AddTaskWithState(task);
					}
				}
			}
			catch (Exception e)
			{
				logger.Error(e.Message);
			}
			return nRet;
		}

		// returning this player's status
		private void returnStatus()
		{
			try
			{
				if (!wcfAuth.SendStatusToServer())
					wcfAuth.RetryTask();
			}
			catch (Exception e)
			{
				wcfAuth.RetryTask();
				logger.Error(e.Message);
			}
		}

        /// <summary>
        /// 로그 업로드 스레드
        /// </summary>
        private void UploadLogThread()
		{
            logger.Info("Start Log Uploader");
            Thread.Sleep(20000);
            while (process)
            {
                try
                {
                    int nRefreshtime = Convert.ToInt32(ConfigurationManager.AppSettings["Log_UploadingRefreshTime"]);
                    int nLimit = Convert.ToInt32(ConfigurationManager.AppSettings["Log_UploadingLimitTime"]);

                    if (nRefreshtime < 30)
                        nRefreshtime = 30;

                    Thread.Sleep(nRefreshtime * 1000);

                    if (config.HasWCFAuthoring && null != config.ServerLogList)
                    {
                        long serverRevision = 0;
                        try
                        {
                            serverRevision = (long)(config.ServerVersion & 0xFFFF);
                        }
                        catch
                        {
                            serverRevision = 0;
                        }


                        if (client_logsTA == null)
                            client_logsTA = config.logdbLayer.CreateClientLogDA();

                        long currentTick = TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime());

                        DigitalSignage.ClientDataBase.LogsDataSet.client_logsDataTable dt = client_logsTA.GetLogs(currentTick - nLimit, currentTick);
                        if (dt != null && dt.Rows != null && dt.Rows.Count > 0)
                        {
                            int nCount = dt.Rows.Count;
                            logger.Info(String.Format("Uploading {0} Log(s) to server...", nCount));

                            //	서버가 신버전인 경우 로그 전송 로직 변경
                            XmlDocument doc = new XmlDocument();
                            XmlElement nodeLogs = doc.CreateElement("Logs");

                            XmlAttribute attrCount = doc.CreateAttribute("Count");
                            attrCount.Value = nCount.ToString();

                            nodeLogs.Attributes.Append(attrCount);
                            foreach (DigitalSignage.ClientDataBase.LogsDataSet.client_logsRow row in dt)
                            {
                                XmlElement nodeLog = doc.CreateElement("Log");

                                XmlAttribute attr1 = doc.CreateAttribute("pid");
                                attr1.Value = config.PlayerId;
                                XmlAttribute attr2 = doc.CreateAttribute("logcd");
                                attr2.Value = row.logcd.ToString();
                                XmlAttribute attr3 = doc.CreateAttribute("uuid");
                                attr3.Value = row.uuid;
                                XmlAttribute attr4 = doc.CreateAttribute("name");
                                attr4.Value = row.name;
                                XmlAttribute attr5 = doc.CreateAttribute("description");
                                attr5.Value = row.description;
                                XmlAttribute attr6 = doc.CreateAttribute("error_code");
                                attr6.Value = row.error_code.ToString();
                                XmlAttribute attr7 = doc.CreateAttribute("screen_id");
                                attr7.Value = row.screen_id.ToString();
                                XmlAttribute attr8 = doc.CreateAttribute("relay_id");
                                attr8.Value = row.relay_id.ToString();
                                XmlAttribute attr9 = doc.CreateAttribute("start_dt");
                                attr9.Value = row.start_dt.ToString();
                                XmlAttribute attr10 = doc.CreateAttribute("end_dt");
                                attr10.Value = row.end_dt.ToString();

                                nodeLog.Attributes.Append(attr1);
                                nodeLog.Attributes.Append(attr2);
                                nodeLog.Attributes.Append(attr3);
                                nodeLog.Attributes.Append(attr4);
                                nodeLog.Attributes.Append(attr5);
                                nodeLog.Attributes.Append(attr6);
                                nodeLog.Attributes.Append(attr7);
                                nodeLog.Attributes.Append(attr8);
                                nodeLog.Attributes.Append(attr9);
                                nodeLog.Attributes.Append(attr10);

                                nodeLogs.AppendChild(nodeLog);
                            }
                            doc.AppendChild(nodeLogs);

                            if (-1 == config.ServerLogList.InsertDetailLogs(doc.OuterXml))
                            {
                                //logger.Debug(doc.OuterXml);
                                throw new Exception("Exception during Uploading logs");
                            }

                            client_logsTA.DeleteLogsBefore(currentTick);
                            dt.Dispose();

                         }


                    }
                }
                catch (Exception e)
                {
                    logger.Error(e.Message);
                }
            }
        }

        /// <summary>
        /// 타겟 광고 로그 업로드 쓰레드
        /// </summary>
        private void UploadTargetAdLogThread()
		{
			logger.Info("Start Ad Target Log Uploader");
			Thread.Sleep(20000);
			while (process)
			{
				try
				{
                    int nRefreshtime = 300;

                    try
                    {
                        nRefreshtime = Convert.ToInt32(ConfigurationManager.AppSettings["Log_TargetAdUploadingRefreshTime"]);
                    }
                    catch { nRefreshtime = 300; }

                    if (nRefreshtime < 30)
                        nRefreshtime = 30;

					Thread.Sleep(nRefreshtime * 1000);

					if(config.HasWCFAuthoring && null != config.ServerLogList)
                    {
                        #region 타겟 광고 정보 전송
                        try
                        {
                            using (DigitalSignage.ClientDataBase.LogsDataSetTableAdapters.target_ad_infoTableAdapter ta_info_DA = config.logdbLayer.CreateTargetAdInfoDA())
                            {
                                using (DigitalSignage.ClientDataBase.LogsDataSet.target_ad_infoDataTable dt = ta_info_DA.GetData())
                                {
                                    if(dt != null && dt.Rows != null && dt.Rows.Count > 0)
						            {
							            int nCount = dt.Rows.Count;
							            logger.Info(String.Format("Uploading {0} Target Ad Info Log(s) to server...", nCount));
                                        String sIdList = String.Empty;
                                        String xmlLogs = String.Format("<TargetAdInfoLogs Count=\"{0}\">", nCount);
                                        
                                        foreach(DigitalSignage.ClientDataBase.LogsDataSet.target_ad_infoRow row in dt.Rows)
                                        {
                                            xmlLogs += String.Format("<Log pid=\"{0}\" relay_id=\"{1}\" r_key=\"{2}\" r_value=\"{3}\" />", config.PlayerId, row.relay_id, row.r_key, row.r_value);
                                            sIdList += String.Format("{0}, ", row.id);
                                        }
                                        xmlLogs += "</TargetAdInfoLogs>";

                                        if (-1 == config.ServerLogList.InsertDetailLogs(xmlLogs))
								        {
									        throw new Exception("Exception during Uploading logs");
								        }
                                        
                                        this.config.logdbLayer.ExecuteNonQuery(String.Format("DELETE FROM target_ad_info WHERE id IN ({0})", sIdList.TrimEnd(new char[] {' ', ','})));
                                    }
                                    dt.Dispose();
                                }
                                ta_info_DA.Dispose();
                            }
                        }
                        catch (Exception exLog)
                        {
                            logger.Error("Target Ad Info Sending Error! : " + exLog.Message);
                        }
                        #endregion

                        #region 타겟 광고 성공 로그 전송
                        try
                        {
                            using (DigitalSignage.ClientDataBase.LogsDataSetTableAdapters.target_ad_successTableAdapter ta_success_DA = config.logdbLayer.CreateTargetAdSuccessDA())
                            {
                                using (DigitalSignage.ClientDataBase.LogsDataSet.target_ad_successDataTable dt = ta_success_DA.GetData())
                                {
                                    if (dt != null && dt.Rows != null && dt.Rows.Count > 0)
                                    {
                                        int nCount = dt.Rows.Count;
                                        logger.Info(String.Format("Uploading {0} Target Ad Success Log(s) to server...", nCount));
                                        String sIdList = String.Empty;
                                        String xmlLogs = String.Format("<TargetAdSuccessLogs Count=\"{0}\">", nCount);

                                        foreach (DigitalSignage.ClientDataBase.LogsDataSet.target_ad_successRow row in dt.Rows)
                                        {
                                            xmlLogs += String.Format("<Log pid=\"{0}\" relay_id=\"{1}\" target_tp=\"{2}\" target_ts=\"{3}\" />", config.PlayerId, row.relay_id, row.target_tp, row.target_ts);
                                            sIdList += String.Format("{0}, ", row.id);
                                        }
                                        xmlLogs += "</TargetAdSuccessLogs>";

                                        if (-1 == config.ServerLogList.InsertDetailLogs(xmlLogs))
                                        {
                                            throw new Exception("Exception during Uploading target ad success logs");
                                        }

                                        this.config.logdbLayer.ExecuteNonQuery(String.Format("DELETE FROM target_ad_success WHERE id IN ({0})", sIdList.TrimEnd(new char[] { ' ', ',' })));
                                    }
                                    dt.Dispose();
                                }
                                ta_success_DA.Dispose();
                            }
                        }
                        catch (Exception exLog)
                        {
                            logger.Error("Target Ad Success Log Sending Error! : " + exLog.Message);
                        }
                        #endregion

                        #region 타겟 광고 에러 로그 전송
                        try
                        {
                            using (DigitalSignage.ClientDataBase.LogsDataSetTableAdapters.target_ad_errorTableAdapter ta_error_DA = config.logdbLayer.CreateTargetAdErrorDA())
                            {
                                using (DigitalSignage.ClientDataBase.LogsDataSet.target_ad_errorDataTable dt = ta_error_DA.GetData())
                                {
                                    if (dt != null && dt.Rows != null && dt.Rows.Count > 0)
                                    {
                                        int nCount = dt.Rows.Count;
                                        logger.Info(String.Format("Uploading {0} Target Ad Error Log(s) to server...", nCount));
                                        String sIdList = String.Empty;
                                        String xmlLogs = String.Format("<TargetAdErrorLogs Count=\"{0}\">", nCount);

                                        foreach (DigitalSignage.ClientDataBase.LogsDataSet.target_ad_errorRow row in dt.Rows)
                                        {
                                            xmlLogs += String.Format("<Log pid=\"{0}\" relay_id=\"{1}\" error_tp=\"{2}\" error_cd=\"{3}\" error_ts=\"{4}\"/>", config.PlayerId, row.relay_id, row.error_tp, row.error_cd, row.error_ts);
                                            sIdList += String.Format("{0}, ", row.id);
                                        }
                                        xmlLogs += "</TargetAdErrorLogs>";

                                        if (-1 == config.ServerLogList.InsertDetailLogs(xmlLogs))
                                        {
                                            throw new Exception("Exception during Uploading target error logs");
                                        }

                                        this.config.logdbLayer.ExecuteNonQuery(String.Format("DELETE FROM target_ad_error WHERE id IN ({0})", sIdList.TrimEnd(new char[] { ' ', ',' })));
                                    }
                                    dt.Dispose();
                                }
                                ta_error_DA.Dispose();
                            }
                        }
                        catch (Exception exLog)
                        {
                            logger.Error("Target Ad Error Log Sending Error! : " + exLog.Message);
                        }
                        #endregion
                    }
				}
				catch (Exception e)
				{
					logger.Error(e.Message);
				}
			}
		}

        /// <summary>
        /// 프로모션 로그 업로드 쓰레드
        /// </summary>
        private void UploadPromotionsThread()
        {
            logger.Info("Start Promotion Log Uploader");
            Thread.Sleep(20000);
            while (process)
            {
                try
                {
                    int nRefreshtime = 300;

                    try
                    {
                        nRefreshtime = Convert.ToInt32(ConfigurationManager.AppSettings["Promotion_RefreshTime"]);
                    }
                    catch { nRefreshtime = 300; }

                    if (nRefreshtime < 30)
                        nRefreshtime = 30;

                    Thread.Sleep(nRefreshtime * 1000);

                    if (config.HasWCFAuthoring && null != config.ServerLogList)
                    {
                        #region 프로모션 전송
                        try
                        {
                            using (DigitalSignage.ClientDataBase.LogsDataSetTableAdapters.promotionsTableAdapter pta = config.logdbLayer.CreatePromotionInfoDA())
                            {
                                using (DigitalSignage.ClientDataBase.LogsDataSet.promotionsDataTable dt = pta.GetData())
                                {
                                    if (dt != null && dt.Rows != null && dt.Rows.Count > 0)
                                    {
                                        int nCount = dt.Rows.Count;
                                        logger.Info(String.Format("Uploading {0} Promotion(s) to server...", nCount));
                                        String sIdList = String.Empty;
                                        String xmlLogs = String.Format("<PromotionInfos Count=\"{0}\">", nCount);

                                        foreach (DigitalSignage.ClientDataBase.LogsDataSet.promotionsRow row in dt.Rows)
                                        {
                                            xmlLogs += String.Format("<Promotion screen_id=\"{0}\" receiver=\"{1}\" reserved=\"{2}\" regdt=\"{3}\"/>", row.screen_id, row.receiver, row.reserved, row.regdt);
                                            sIdList += String.Format("{0}, ", row.id);
                                        }
                                        xmlLogs += "</PromotionInfos>";

                                        if (-1 == config.ServerLogList.InsertDetailLogs(xmlLogs))
                                        {
                                            throw new Exception("Exception during Uploading promotions");
                                        }

                                        this.config.logdbLayer.ExecuteNonQuery(String.Format("DELETE FROM promotions WHERE id IN ({0})", sIdList.TrimEnd(new char[] { ' ', ',' })));
                                    }
                                    dt.Dispose();
                                }
                                pta.Dispose();
                            }
                        }
                        catch (Exception exLog)
                        {
                            logger.Error("Promotions Sending Error! : " + exLog.Message);
                        }
                        #endregion
                    }
                }
                catch (Exception e)
                {
                    logger.Error(e.Message);
                }
            }
        }

        /// <summary>
        /// 캡쳐 서비스
        /// </summary>
        public void DoCapture()
        {
            logger.Info("캡처 프로세스 시작");
            Config cfg = Config.GetConfig;
            int cntCaptureTime = 0;
            while (process)
            {
                try
                {
                    if (cntCaptureTime++ > cfg.CaptureRefreshTime)
                    {
                        cntCaptureTime = 0;

                        if (cfg.CaptureProcess)
                        {

                            var func = new Func<bool>(() =>
                            {
                                try
                                {
                                    logger.Debug("캡처 요청 중...");

                                    IPlayerCmdReceiver playerCmdExReceiver = (IPlayerCmdReceiver)Activator.GetObject(
                                         typeof(IPlayerCmdReceiver), "tcp://127.0.0.1:1889/CmdReceiverHost/rt");

                                    byte[] arrBytes = playerCmdExReceiver.GetScreenCapture();

                                    if (arrBytes != null)
                                    {
                                        if (!cfg.ServerDataCacheList.SetCapture(cfg.PlayerId, arrBytes))
                                        {
                                            cfg.CaptureProcess = false;
                                            logger.Debug("캡처 요청 종료...");
                                        }
                                    }
                                    logger.Debug("캡처 요청 완료...");
                                    return true;
                                }
                                catch (Exception e) { logger.Error(e.Message); }
                                return false;

                            });

                            MethodHelper.Timeout(func, 60000);
 

                        }
                    }
                }
                catch (System.Exception errth)
                {
                    logger.Error(errth.ToString());

                }
                Thread.Sleep(1000);
            }
            logger.Info("캡처 프로세스 종료");

        }

		#region FTP 관련
		//	FTP 관련
		int nTotalFiles = 0;
		int nFinishedFiles = 0;
		Task CurrentDownloadTask = null;

		public String AnalyzeSource(String source)
		{
			if (source.StartsWith("$"))
			{
				int nIndex = source.IndexOf(":");

				if (source.ToLower().Contains("$local"))
				{
					source = "127.0.0.1" +
						(nIndex != -1 ? source.Substring(nIndex) : "");
				}
				else if (source.ToLower().Contains("$server"))
				{
					string server = Config.GetConfig.ServerPath.ToLower();
					server = server.StartsWith("tcp://") ? server.Substring(6) : server;
					server = server.LastIndexOf(":") != -1 ? server.Substring(0, server.LastIndexOf(":")) : server;

					source = server +
						(nIndex != -1 ? source.Substring(nIndex) : "");
				}
			}

			return source;
		}

		private void DownloadThread()
		{
			int nDefaultTimeout = 10000;
			logger.Info("Start Downloader");
			while (process)
			{
				try
				{
					//10초를 기다린다.
					int nCount = 0;
					while (process && ++nCount < WAIT_DOWNLOAD_SLEEP_SECOND)
					{
						Thread.Sleep(1000);
					}

					//아직 접속이 되지 않았기때문에 반려한다.
					if (config.FTPServer == null) continue;

					Task[] downloadTasks = config.localTasksList.GetDownloadTasks();

					if (downloadTasks != null && downloadTasks.Length > 0)
					{
						logger.Info("Found tasks to be downloaded : " + downloadTasks.Length.ToString());

						wcfAuth.TotalDownLoadingTasks = downloadTasks.Length;
						wcfAuth.FinishedDownloadTasks = 0;

						GroupManager manager = GroupManager.GetInstance;

						foreach (Task task in downloadTasks)
						{
							CurrentDownloadTask = task;

							wcfAuth.DownloadingTaskName = task.description;

							string server = manager.GetGroupInfo(task.gid, "source");
							string path = manager.GetGroupInfo(task.gid, "path");
							string username = manager.GetGroupInfo(task.gid, "username");
							string password = manager.GetGroupInfo(task.gid, "password");

							FTPFunctions ftp = new FTPFunctions(AnalyzeSource(server), path, username, password, nDefaultTimeout, config.UsePassiveMode);

							ftp.OnStepIt += new EventHandler(ftp_OnStepIt);
							ftp.OnUpdateLabel += new StatusEventHandler(ftp_OnUpdateLabel);
							ftp.FileDownloadStarted = DownloadStarted;
							ftp.FileDownloadEnded = DownloadEnded;
							ftp.FileDownloadFinished = DownloadProcceed;
							ftp.FileDownloadError = DownloadErrorOccurred;

							string urlReq = "ftp://" + server + "/" + (path.Equals("") ? "" : path + "/");
							logger.Info("Connecting FTP : " + urlReq);

							if (task.type == TaskCommands.CmdGetRefData)
							{
								nTotalFiles = ftp.GetFTPRecursiveFileCount(config.RefDataSource);
								nFinishedFiles = 0;
								wcfAuth.ProgressDownloadingTask = 0;

								if (ftp.FTPRecursiveDownload(config.RefDataSource, config.AppData + config.RefDataSource + "\\"))
								{
									wcfAuth.FinishedDownloadTasks++;
									wcfAuth.DownloadingTaskName = "";
								}
								else
								{
									logger.Info("Downloading is failed : " + config.RefDataSource);
								}

								task.TaskStart = TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()) + config.RefRefreshTime;
								task.TaskEnd = task.TaskStart + 3600 * 24;
								task.state = TaskState.StateWait;

								InsertOrReplaceTask(task);

							}
							else
							{
								//	1. 이미 받아져 있는지 검사
								if (IsDownloadComplete(task.duuid.ToString()))
								{
									nTotalFiles = 1;
									nFinishedFiles = 1;
									wcfAuth.ProgressDownloadingTask = 100;
									logger.Info("This project will be passed because of already downloaded project : " + task.duuid.ToString());

									lock (lockTaskState)
									{
										TaskState state = (TaskState)config.localTasksList.GetTaskState(task.uuid);

										if (state != TaskState.StateCanceled && state != TaskState.StateProcessed)
										{
											//	성공하면 상태를 Wait 상태로 변경한다.
											config.localTasksList.ChangeTaskState(TaskState.StateWait, task.uuid);
										}
									}

									wcfAuth.FinishedDownloadTasks++;

									//	서버로 download정보를 넣는다.
									//InsertDownloadedInfoToServer(task.uuid.ToString());
//									UpdateDownloadedInfoToServer(task.duuid.ToString(), task.gid);
								}	
								else
								{
									//	Download 받을 시간을 먼저 체크
									if(config.IsAvailableTimeForDownloading())
									{
										nTotalFiles = ftp.GetFTPRecursiveFileCount(task.duuid.ToString());
										nFinishedFiles = 0;
										wcfAuth.ProgressDownloadingTask = 0;

										//	실제 Download가 일어남.
										if (ftp.FTPRecursiveDownload(task.duuid.ToString(), config.AppData + "xaml\\" + task.duuid.ToString() + "\\"))
										{

											lock (lockTaskState)
											{
												TaskState state = (TaskState)config.localTasksList.GetTaskState(task.uuid);

												if (state != TaskState.StateCanceled && state != TaskState.StateProcessed)
												{
													//	성공하면 상태를 Wait 상태로 변경한다.
													config.localTasksList.ChangeTaskState(TaskState.StateWait, task.uuid);
												}
												logger.Info("Downloaded project " + task.duuid.ToString());
											}

											wcfAuth.FinishedDownloadTasks++;

											//	서버로 download정보를 넣는다.
											if (InsertDownloadedInfoToServer(task.uuid.ToString()))
											{
												//UpdateDownloadedInfoToServer(task.duuid.ToString(), task.gid);
												DownloadComplete(task.duuid.ToString());
											}
										}
										else
										{
											logger.Info("Downloading is failed : " + task.duuid.ToString());
										}
									}
									else
									{
										//	다운받을 시간인 아님 1분 후에 재시도하도록.
										logger.Info("This project could not be downloaded. Now is not time to download.");
										
										int nWaitCount = 0;
										while (process && ++nWaitCount < WAIT_NOT_YET_TIME_TO_DOWNLOAD_SECOND)
										{
											Thread.Sleep(1000);
										}
									}
								}
							}
						}
					}
					else
					{
						wcfAuth.FinishedDownloadTasks = 0;
						wcfAuth.ProgressDownloadingTask = 0;
						wcfAuth.TotalDownLoadingTasks = 0;
						wcfAuth.DownloadingTaskName = "";
					}
				}
				catch (Exception e)
				{
					logger.Error(e.Message);
				}
			}
			logger.Info("End Downloader");
		}
		bool IsDownloadComplete(string duuid)
		{
			try
			{
				string sDest = config.AppData + "xaml\\" + duuid + "\\downloaded";

				System.IO.FileInfo fi = new System.IO.FileInfo(sDest);
				
				if (fi == null) return false;

				return fi.Exists;
			}
			catch (Exception e)
			{
				logger.Error(e.ToString());
				return false;
			}
		}


		void DownloadDelete(string duuid)
		{
			try
			{
				string sDest = config.AppData + "xaml\\" + duuid + "\\downloaded";

				System.IO.File.Delete(sDest);
			}
			catch 
			{
			}
		}

		bool DownloadComplete(string duuid)
		{
			try
			{
				string sDest = config.AppData + "xaml\\" + duuid + "\\downloaded";

				using(System.IO.FileStream fs = System.IO.File.Create(sDest))
				{
					fs.Close();
                    fs.Dispose();
				}
				return true;
			}
			catch (Exception e)
			{
				logger.Error(e.ToString());
				return false;
			}
		}

		bool InsertDownloadedInfoToServer(string uuid)
		{
			try
			{
				long now = TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime());

				int cntRetry = 0;

				while (cntRetry++ < 2)
				{
					if (0 < config.ServerDownloadList.InsertDownloadInformation(uuid, config.PlayerId, now))
					{
						return true;
					}
				}

				return false;
			}
			catch (Exception e)
			{
				logger.Error(e.Message);
			}
			return false;
		}
		
		void ftp_OnUpdateLabel(object sender, StatusEventArgs e)
		{
			try
			{
				wcfAuth.DownloadingTaskName = e.OriginalSource.ToString();
			}
			catch (Exception ex)
			{
				logger.Error(ex.Message);

			}
		}

		void ftp_OnStepIt(object sender, EventArgs e)
		{
			nFinishedFiles++;
			wcfAuth.ProgressDownloadingTask = Convert.ToDouble((double)nFinishedFiles / (double)nTotalFiles * 100.0);
		}

		private void DownloadStarted(string sName)
		{
			try
			{
				if (client_logsTA == null)
					client_logsTA = config.logdbLayer.CreateClientLogDA();
				
				long currTicks = TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime());
				client_logsTA.InsertLogs(Config.GetConfig.PlayerId, CurrentDownloadTask.uuid.ToString(), CurrentDownloadTask.description, sName, Config.MakeTaskLogType(CurrentDownloadTask.isAd, (CurrentDownloadTask.show_flag & (int)ShowFlag.Show_Event) > 0, LogType.LogType_Processing, CurrentDownloadTask.type, (int)LogType.LogType_Download), 0, currTicks, currTicks, null, null);
			}
			catch (Exception e)
			{
				logger.Error(e.Message);
			}
		}

		private void DownloadEnded(string sName)
		{
			try
			{
				if (client_logsTA == null)
					client_logsTA = config.logdbLayer.CreateClientLogDA();

				long currTicks = TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime());
                client_logsTA.InsertLogs(Config.GetConfig.PlayerId, CurrentDownloadTask.uuid.ToString(), CurrentDownloadTask.description, sName, Config.MakeTaskLogType(CurrentDownloadTask.isAd, (CurrentDownloadTask.show_flag & (int)ShowFlag.Show_Event) > 0, LogType.LogType_Proceed, CurrentDownloadTask.type, (int)LogType.LogType_Download), 0, currTicks, currTicks, null, null);
			}
			catch (Exception e)
			{
				logger.Error(e.Message);
			}
		}
		private void DownloadProcceed(string sName, long start_dt, long end_dt)
		{
			try
			{
				if (client_logsTA == null)
					client_logsTA = config.logdbLayer.CreateClientLogDA();

                client_logsTA.InsertLogs(Config.GetConfig.PlayerId, CurrentDownloadTask.uuid.ToString(), CurrentDownloadTask.description, sName, Config.MakeTaskLogType(CurrentDownloadTask.isAd, (CurrentDownloadTask.show_flag & (int)ShowFlag.Show_Event) > 0, LogType.LogType_Proceed, CurrentDownloadTask.type, (int)LogType.LogType_Download), 0, start_dt, end_dt, null, null);
			}
			catch (Exception e)
			{
				logger.Error(e.Message);
			}
		}

		private void DownloadErrorOccurred(string sName, int code, string description)
		{
			try
			{
				if (client_logsTA == null)
					client_logsTA = config.logdbLayer.CreateClientLogDA();

				long currTicks = TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime());
                client_logsTA.InsertLogs(Config.GetConfig.PlayerId, CurrentDownloadTask.uuid.ToString(), CurrentDownloadTask.description, String.Format("{0} : {1}", sName, description), Config.MakeTaskLogType(CurrentDownloadTask.isAd, (CurrentDownloadTask.show_flag & (int)ShowFlag.Show_Event) > 0, LogType.LogType_Failed, CurrentDownloadTask.type, (int)LogType.LogType_Download), code, currTicks, currTicks, null, null);
			}
			catch (Exception e)
			{
				logger.Error(e.Message);
			}
		}
		#endregion

		#endregion
	}
}
