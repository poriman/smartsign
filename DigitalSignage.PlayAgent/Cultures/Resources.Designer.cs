﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.235
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DigitalSignage.PlayAgent.Properties {
    using System;
    
    
    /// <summary>
    /// A strongly-typed resource class, for looking up localized strings, formatting them, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilderEx class via the ResXFileCodeGeneratorEx custom tool.
    // To add or remove a member, edit your .ResX file then rerun the ResXFileCodeGeneratorEx custom tool or rebuild your VS.NET project.
    // Copyright (c) Dmytro Kryvko 2006-2012 (http://dmytro.kryvko.googlepages.com/)
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("DMKSoftware.CodeGenerators.Tools.StronglyTypedResourceBuilderEx", "2.6.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
#if !SILVERLIGHT
    [global::System.Reflection.ObfuscationAttribute(Exclude=true, ApplyToMembers=true)]
#endif
    [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Naming", "CA1724:TypeNamesShouldNotMatchNamespaces")]
    public partial class Resources {
        
        private static global::System.Resources.ResourceManager _resourceManager;
        
        private static object _internalSyncObject;
        
        private static global::System.Globalization.CultureInfo _resourceCulture;
        
        /// <summary>
        /// Initializes a Resources object.
        /// </summary>
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        public Resources() {
        }
        
        /// <summary>
        /// Thread safe lock object used by this class.
        /// </summary>
        public static object InternalSyncObject {
            get {
                if (object.ReferenceEquals(_internalSyncObject, null)) {
                    global::System.Threading.Interlocked.CompareExchange(ref _internalSyncObject, new object(), null);
                }
                return _internalSyncObject;
            }
        }
        
        /// <summary>
        /// Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(_resourceManager, null)) {
                    global::System.Threading.Monitor.Enter(InternalSyncObject);
                    try {
                        if (object.ReferenceEquals(_resourceManager, null)) {
                            global::System.Threading.Interlocked.Exchange(ref _resourceManager, new global::System.Resources.ResourceManager("DigitalSignage.PlayAgent.Cultures.Resources", typeof(Resources).Assembly));
                        }
                    }
                    finally {
                        global::System.Threading.Monitor.Exit(InternalSyncObject);
                    }
                }
                return _resourceManager;
            }
        }
        
        /// <summary>
        /// Overrides the current thread's CurrentUICulture property for all
        /// resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return _resourceCulture;
            }
            set {
                _resourceCulture = value;
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'This player ID has been already used.'.
        /// </summary>
        public static string errorAlreadyUsedPlayer {
            get {
                return ResourceManager.GetString(ResourceNames.errorAlreadyUsedPlayer, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'It was disconnected because of the following error:'.
        /// </summary>
        public static string errorDisconnectedBy {
            get {
                return ResourceManager.GetString(ResourceNames.errorDisconnectedBy, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Disconnected by server.'.
        /// </summary>
        public static string errorDisconnectedByServer {
            get {
                return ResourceManager.GetString(ResourceNames.errorDisconnectedByServer, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'It was disconnected because of an excess of the allowed maximum count.'.
        /// </summary>
        public static string errorExcessOfAllowedCount {
            get {
                return ResourceManager.GetString(ResourceNames.errorExcessOfAllowedCount, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'There is no player in the server.'.
        /// </summary>
        public static string errorNoPlayerInServer {
            get {
                return ResourceManager.GetString(ResourceNames.errorNoPlayerInServer, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Check Connection'.
        /// </summary>
        public static string labelCheckConnection {
            get {
                return ResourceManager.GetString(ResourceNames.labelCheckConnection, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Checking...'.
        /// </summary>
        public static string labelChecking {
            get {
                return ResourceManager.GetString(ResourceNames.labelChecking, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Close'.
        /// </summary>
        public static string labelClose {
            get {
                return ResourceManager.GetString(ResourceNames.labelClose, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Cont. Svr Status:'.
        /// </summary>
        public static string labelContentsSvrStatus {
            get {
                return ResourceManager.GetString(ResourceNames.labelContentsSvrStatus, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'N/A'.
        /// </summary>
        public static string labelNotAvailable {
            get {
                return ResourceManager.GetString(ResourceNames.labelNotAvailable, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Offline'.
        /// </summary>
        public static string labelOffline {
            get {
                return ResourceManager.GetString(ResourceNames.labelOffline, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Online'.
        /// </summary>
        public static string labelOnline {
            get {
                return ResourceManager.GetString(ResourceNames.labelOnline, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Player ID:'.
        /// </summary>
        public static string labelPlayerID {
            get {
                return ResourceManager.GetString(ResourceNames.labelPlayerID, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Save Setting'.
        /// </summary>
        public static string labelSaveSetting {
            get {
                return ResourceManager.GetString(ResourceNames.labelSaveSetting, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Search'.
        /// </summary>
        public static string labelSearch {
            get {
                return ResourceManager.GetString(ResourceNames.labelSearch, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Server Hostname:'.
        /// </summary>
        public static string labelServerAddress {
            get {
                return ResourceManager.GetString(ResourceNames.labelServerAddress, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Server Returns'.
        /// </summary>
        public static string labelServerReturns {
            get {
                return ResourceManager.GetString(ResourceNames.labelServerReturns, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Server status:'.
        /// </summary>
        public static string labelServerStatus {
            get {
                return ResourceManager.GetString(ResourceNames.labelServerStatus, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'The End Point is not founded. Please confirm the network status or server&apos;s address, firewall.'.
        /// </summary>
        public static string mbErrorEndPointNotFound {
            get {
                return ResourceManager.GetString(ResourceNames.mbErrorEndPointNotFound, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Can&apos;t create socket 889 port(UDP) because of Firewall or Network problems. So, you can&apos;t use the [Search] button.'.
        /// </summary>
        public static string mbErrorFirewall {
            get {
                return ResourceManager.GetString(ResourceNames.mbErrorFirewall, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'The server&apos;s address format is not correct.'.
        /// </summary>
        public static string mbErrorUriFormat {
            get {
                return ResourceManager.GetString(ResourceNames.mbErrorUriFormat, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'This is first start of SmartSign Player. Please configure Server configuration.'.
        /// </summary>
        public static string mbFirstRun {
            get {
                return ResourceManager.GetString(ResourceNames.mbFirstRun, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'If connection is success, player clears playlist in order to update newest schedules. Continue?'.
        /// </summary>
        public static string mbWarnClearPlaylist {
            get {
                return ResourceManager.GetString(ResourceNames.mbWarnClearPlaylist, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to '&amp;About...'.
        /// </summary>
        public static string menuitemAbout {
            get {
                return ResourceManager.GetString(ResourceNames.menuitemAbout, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to '&amp;Configure...'.
        /// </summary>
        public static string menuitemConfigure {
            get {
                return ResourceManager.GetString(ResourceNames.menuitemConfigure, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'E&amp;xit'.
        /// </summary>
        public static string menuitemExit {
            get {
                return ResourceManager.GetString(ResourceNames.menuitemExit, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Languages'.
        /// </summary>
        public static string menuitemLanguage {
            get {
                return ResourceManager.GetString(ResourceNames.menuitemLanguage, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to '&amp;Next Program'.
        /// </summary>
        public static string menuitemNextProgram {
            get {
                return ResourceManager.GetString(ResourceNames.menuitemNextProgram, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a resource 'Player_icon'.
        /// </summary>
        public static System.Drawing.Icon Player_icon {
            get {
                return ((System.Drawing.Icon)(ResourceManager.GetObject(ResourceNames.Player_icon, _resourceCulture)));
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'About SmartSign Player'.
        /// </summary>
        public static string titleAbout {
            get {
                return ResourceManager.GetString(ResourceNames.titleAbout, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'SmartSign Player'.
        /// </summary>
        public static string titlePlayer {
            get {
                return ResourceManager.GetString(ResourceNames.titlePlayer, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Looks up a localized string similar to 'Server Configuration'.
        /// </summary>
        public static string titleServerConfiguration {
            get {
                return ResourceManager.GetString(ResourceNames.titleServerConfiguration, _resourceCulture);
            }
        }
        
        /// <summary>
        /// Lists all the resource names as constant string fields.
        /// </summary>
        public class ResourceNames {
            
            /// <summary>
            /// Stores the resource name 'errorAlreadyUsedPlayer'.
            /// </summary>
            public const string errorAlreadyUsedPlayer = "errorAlreadyUsedPlayer";
            
            /// <summary>
            /// Stores the resource name 'errorDisconnectedBy'.
            /// </summary>
            public const string errorDisconnectedBy = "errorDisconnectedBy";
            
            /// <summary>
            /// Stores the resource name 'errorDisconnectedByServer'.
            /// </summary>
            public const string errorDisconnectedByServer = "errorDisconnectedByServer";
            
            /// <summary>
            /// Stores the resource name 'errorExcessOfAllowedCount'.
            /// </summary>
            public const string errorExcessOfAllowedCount = "errorExcessOfAllowedCount";
            
            /// <summary>
            /// Stores the resource name 'errorNoPlayerInServer'.
            /// </summary>
            public const string errorNoPlayerInServer = "errorNoPlayerInServer";
            
            /// <summary>
            /// Stores the resource name 'labelCheckConnection'.
            /// </summary>
            public const string labelCheckConnection = "labelCheckConnection";
            
            /// <summary>
            /// Stores the resource name 'labelChecking'.
            /// </summary>
            public const string labelChecking = "labelChecking";
            
            /// <summary>
            /// Stores the resource name 'labelClose'.
            /// </summary>
            public const string labelClose = "labelClose";
            
            /// <summary>
            /// Stores the resource name 'labelContentsSvrStatus'.
            /// </summary>
            public const string labelContentsSvrStatus = "labelContentsSvrStatus";
            
            /// <summary>
            /// Stores the resource name 'labelNotAvailable'.
            /// </summary>
            public const string labelNotAvailable = "labelNotAvailable";
            
            /// <summary>
            /// Stores the resource name 'labelOffline'.
            /// </summary>
            public const string labelOffline = "labelOffline";
            
            /// <summary>
            /// Stores the resource name 'labelOnline'.
            /// </summary>
            public const string labelOnline = "labelOnline";
            
            /// <summary>
            /// Stores the resource name 'labelPlayerID'.
            /// </summary>
            public const string labelPlayerID = "labelPlayerID";
            
            /// <summary>
            /// Stores the resource name 'labelSaveSetting'.
            /// </summary>
            public const string labelSaveSetting = "labelSaveSetting";
            
            /// <summary>
            /// Stores the resource name 'labelSearch'.
            /// </summary>
            public const string labelSearch = "labelSearch";
            
            /// <summary>
            /// Stores the resource name 'labelServerAddress'.
            /// </summary>
            public const string labelServerAddress = "labelServerAddress";
            
            /// <summary>
            /// Stores the resource name 'labelServerReturns'.
            /// </summary>
            public const string labelServerReturns = "labelServerReturns";
            
            /// <summary>
            /// Stores the resource name 'labelServerStatus'.
            /// </summary>
            public const string labelServerStatus = "labelServerStatus";
            
            /// <summary>
            /// Stores the resource name 'mbErrorEndPointNotFound'.
            /// </summary>
            public const string mbErrorEndPointNotFound = "mbErrorEndPointNotFound";
            
            /// <summary>
            /// Stores the resource name 'mbErrorFirewall'.
            /// </summary>
            public const string mbErrorFirewall = "mbErrorFirewall";
            
            /// <summary>
            /// Stores the resource name 'mbErrorUriFormat'.
            /// </summary>
            public const string mbErrorUriFormat = "mbErrorUriFormat";
            
            /// <summary>
            /// Stores the resource name 'mbFirstRun'.
            /// </summary>
            public const string mbFirstRun = "mbFirstRun";
            
            /// <summary>
            /// Stores the resource name 'mbWarnClearPlaylist'.
            /// </summary>
            public const string mbWarnClearPlaylist = "mbWarnClearPlaylist";
            
            /// <summary>
            /// Stores the resource name 'menuitemAbout'.
            /// </summary>
            public const string menuitemAbout = "menuitemAbout";
            
            /// <summary>
            /// Stores the resource name 'menuitemConfigure'.
            /// </summary>
            public const string menuitemConfigure = "menuitemConfigure";
            
            /// <summary>
            /// Stores the resource name 'menuitemExit'.
            /// </summary>
            public const string menuitemExit = "menuitemExit";
            
            /// <summary>
            /// Stores the resource name 'menuitemLanguage'.
            /// </summary>
            public const string menuitemLanguage = "menuitemLanguage";
            
            /// <summary>
            /// Stores the resource name 'menuitemNextProgram'.
            /// </summary>
            public const string menuitemNextProgram = "menuitemNextProgram";
            
            /// <summary>
            /// Stores the resource name 'Player_icon'.
            /// </summary>
            public const string Player_icon = "Player_icon";
            
            /// <summary>
            /// Stores the resource name 'titleAbout'.
            /// </summary>
            public const string titleAbout = "titleAbout";
            
            /// <summary>
            /// Stores the resource name 'titlePlayer'.
            /// </summary>
            public const string titlePlayer = "titlePlayer";
            
            /// <summary>
            /// Stores the resource name 'titleServerConfiguration'.
            /// </summary>
            public const string titleServerConfiguration = "titleServerConfiguration";
        }
    }
}
