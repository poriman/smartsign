﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using NLog;

namespace DigitalSignage.PlayAgent
{
	public class GroupManager
	{
		private static Logger logger = LogManager.GetCurrentClassLogger();
		private static GroupManager instance = null;
		private static readonly object semaphore = new object();

		private XmlDocument docGroups = null;

		public static GroupManager GetInstance
		{
			get
			{
				lock (semaphore)
				{
					if (instance == null)
					{
						instance = new GroupManager();
					}
					return instance;
				}
			}
		}

		public String GroupDocument
		{
			set
			{
				lock (semaphore)
				{
					docGroups = new XmlDocument();
					docGroups.LoadXml(value);
				}
			}
		}

		public string GetGroupInfo(string gid, string attributeName)
		{
			lock (semaphore)
			{
				try
				{
					return docGroups.SelectSingleNode("//child::group[attribute::gid=\"" + gid + "\"]").Attributes[attributeName].Value;
				}
				catch (Exception ex)
				{
					logger.Error(ex.ToString());
				}
				return "";
			}
		}

		public string GetGroupsWithSeperartor()
		{
			XmlNodeList nodes = docGroups.SelectNodes("//group");
			string sReturn = "";
			foreach(XmlNode node in nodes)
			{
				sReturn += node.Attributes["gid"].Value;
				sReturn += "|";
			}
			return sReturn.TrimEnd('|');
		}
	}
}
