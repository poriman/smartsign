﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using NLog;

namespace DigitalSignage.PlayAgent
{
    /// <summary>
    /// 메소드를 Timeout 형태로 구현하기위한 클래스
    /// </summary>
    public class MethodHelper
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        /// <summary>
        /// 타임아웃 쓰레드 구현
        /// </summary>
        /// <typeparam name="T">리턴 타입</typeparam>
        /// <param name="func">쓰레드 함수</param>
        /// <param name="timeout">타임아웃 Millisecond</param>
        /// <returns>리턴 정보</returns>
        public static T Timeout<T>(Func<T> func, int timeout)
        {
            T result;
            TryTimeout(func, timeout, out result);
            return result;
        }
        
        /// <summary>
        /// 타임아웃 쓰레드 구현
        /// </summary>
        /// <typeparam name="T">리턴 타입</typeparam>
        /// <param name="func">쓰레드 함수</param>
        /// <param name="timeout">타임아웃 Millisecond</param>
        /// <param name="result">리턴 정보</param>
        /// <returns>성공 여부</returns>
        public static bool TryTimeout<T>(Func<T> func, int timeout, out T result)
        {
            var t = default(T);
            var thread = new Thread(() => t = func());
            thread.Start();

            var completed = thread.Join(timeout);

            if (!completed)
            {
                thread.Abort();
                logger.Error("Method Timeouted: Thread was aborted!");
            }
            result = t;
            return completed;
        }

    }
}
