﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Management;

namespace CPUInfo
{
    public class CPUInfo
    {
        PerformanceCounter pc = null;
        float percentOfCPU = 0, percentOfMemory = 0;  //Percentage of CPU & MainMemory,0∥99
        bool firstRun = true;

        public float GetCpuinfo()
        {
            float cpu = 30f;
            try
            {
                if (pc == null) pc = new PerformanceCounter("Processor", "% Processor Time", "_Total");
                if (!firstRun)
                    cpu = (float)pc.NextValue();
            }
            catch
            {
                //cpu = GetCPUUsage();//인천공항 탑승동 오류 발생으로 주석 처리함.
            }


            if (firstRun || percentOfCPU != cpu)
            {
                percentOfCPU = cpu;
            }

            if (firstRun) firstRun = false;
           

            return percentOfCPU;
        }

        /// <summary>
        /// Gets CPU Usage in % - 20130419_jy.cho: WMI 사용하여 CPU 사용율 얻어옴.
        /// </summary>
        /// <returns></returns>
        private float GetCPUUsage()
        {
            ManagementObject processor = new ManagementObject("Win32_PerfFormattedData_PerfOS_Processor.Name='_Total'");
            processor.Get();

            return float.Parse(processor.Properties["PercentProcessorTime"].Value.ToString());
        }

        public float GetMemory()
        {
            float mem = 0f;
            CWin32.MEMORY_INFO MemInfo = new CWin32.MEMORY_INFO();
            CWin32.GlobalMemoryStatus(ref  MemInfo);
            mem = (float)MemInfo.dwMemoryLoad;
            if (firstRun || percentOfMemory != mem)
            {
                percentOfMemory = mem;
            }
            return percentOfMemory;
        }
    }
}
