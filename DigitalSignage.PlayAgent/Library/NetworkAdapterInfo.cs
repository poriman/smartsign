﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Win32;
using System.Runtime.InteropServices;
using System.Xml;

namespace DigitalSignage.PlayAgent.Library
{
	public class NetworkAdapterInfo
	{
		#region Iphlpapi.dll Import
		[DllImport("Iphlpapi.dll")]
		public static extern uint GetAdaptersAddresses(uint Family, uint flags, IntPtr Reserved,
			IntPtr PAdaptersAddresses, ref uint pOutBufLen);

//		private System.Windows.Forms.Button DisplayAdapterInfo;
//		private System.Windows.Forms.TextBox AdapterInfoTextBox;

		[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
		public class IP_Adapter_Addresses
		{
			public uint Length;
			public uint IfIndex;
			public IntPtr Next;

			public IntPtr AdapterName;
			public IntPtr FirstUnicastAddress;
			public IntPtr FirstAnycastAddress;
			public IntPtr FirstMulticastAddress;
			public IntPtr FirstDnsServerAddress;

			public IntPtr DnsSuffix;
			public IntPtr Description;

			public IntPtr FriendlyName;

			[MarshalAs(UnmanagedType.ByValArray,
				 SizeConst = 8)]
			public Byte[] PhysicalAddress;

			public uint PhysicalAddressLength;
			public uint flags;
			public uint Mtu;
			public uint IfType;

			public uint OperStatus;

			public uint Ipv6IfIndex;
			public uint ZoneIndices;

			public IntPtr FirstPrefix;
		}
		#endregion

		#region Properties
		/// <summary>
		/// 네트워크 어뎁터 이름
		/// </summary>
		public String AdapterName { get; set; }
		/// <summary>
		/// IP v4 주소
		/// </summary>
		public String IP4_Address { get; set; }
		/// <summary>
		/// MAC (물리) 주소
		/// </summary>
		public String MAC_Address { get; set; }
		/// <summary>
		/// DHCP 설정 여부
		/// </summary>
		public String IsDHCP { get; set; }
		#endregion

		/// <summary>
		/// 생성자
		/// </summary>
		public NetworkAdapterInfo()
		{
			AdapterName = "Unknown";
			IP4_Address = String.Empty;
			MAC_Address = String.Empty;
			IsDHCP = "N";
		}

		/// <summary>
		/// 사용가능한 Network Adapter 정보를 가져온다.
		/// </summary>
		/// <returns>연결 가능한 Network Adapter 정보</returns>
		public static NetworkAdapterInfo GetAvailableAdapterInfo()
		{
			NetworkAdapterInfo adap_info = new NetworkAdapterInfo();

			IntPtr PAdaptersAddresses = new IntPtr();
			bool AdapterFound = false;

			uint pOutLen = 100;
			PAdaptersAddresses = Marshal.AllocHGlobal(100);

			uint ret =
				GetAdaptersAddresses(0, 0, (IntPtr)0, PAdaptersAddresses, ref pOutLen);

			// if 111 error, use 
			if (ret == 111)
			{
				Marshal.FreeHGlobal(PAdaptersAddresses);
				PAdaptersAddresses = Marshal.AllocHGlobal((int)pOutLen);
				ret = GetAdaptersAddresses(0, 0, (IntPtr)0, PAdaptersAddresses, ref pOutLen);
			}

			IP_Adapter_Addresses adds = new IP_Adapter_Addresses();

			IntPtr pTemp = PAdaptersAddresses;

			while (pTemp != (IntPtr)0)
			{
				Marshal.PtrToStructure(pTemp, adds);

				if (adds.OperStatus == 1)
				{
					string adapterName = Marshal.PtrToStringAnsi(adds.AdapterName);
					string FriendlyName = Marshal.PtrToStringAuto(adds.FriendlyName);
					string tmpString = string.Empty;

					for (int i = 0; i < 6; i++)
					{
						tmpString += string.Format("{0:X2}", adds.PhysicalAddress[i]);

						if (i < 5)
						{
							tmpString += ":";
						}
					}


					RegistryKey theLocalMachine = Registry.LocalMachine;

					RegistryKey theSystem
						= theLocalMachine.OpenSubKey(@"SYSTEM\CurrentControlSet\Services\Tcpip\Parameters\Interfaces");
					RegistryKey theInterfaceKey = theSystem.OpenSubKey(adapterName);

					if (theInterfaceKey != null)
					{

						string DhcpIPAddress = (string)theInterfaceKey.GetValue("DhcpIPAddress");
                        int IsDHCP = 0;
                        try
                        {
                            IsDHCP = (int)theInterfaceKey.GetValue("EnableDHCP");
                        }
                        catch { }
						// system is using DHCP
                        if (IsDHCP > 0 && DhcpIPAddress != null)
						{
							string tArray =
								theInterfaceKey.GetValue("DhcpIPAddress", theInterfaceKey) as string;

							if (!string.IsNullOrEmpty(tArray))
							{
								adap_info.AdapterName = FriendlyName.ToString();
								adap_info.IP4_Address = tArray;
								adap_info.MAC_Address = tmpString;
								adap_info.IsDHCP = "Y";
								AdapterFound = true;
							}
						}
						else
						{
							string[] tArray =
								theInterfaceKey.GetValue("IPAddress", theInterfaceKey) as string[];

							if (tArray != null)
							{
								adap_info.AdapterName = FriendlyName.ToString();
								adap_info.MAC_Address = tmpString;
								adap_info.IsDHCP = "N";

								for (int Interface = 0; Interface < tArray.Length; Interface++)
								{
									if (AdapterFound == false)
									{
										adap_info.IP4_Address = tArray[Interface];
										AdapterFound = true;
										break;
									}
								}
							}
						}
					}
				}

				pTemp = adds.Next;
			}

			if (AdapterFound != true)
			{
				return null;
			}

			return adap_info;
		}
	}

}
