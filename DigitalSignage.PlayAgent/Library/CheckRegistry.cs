﻿using System;
using System.Reflection;
using Microsoft.Win32;

namespace DigitalSignage.PlayAgent
{
    class CheckRegistry
    {
        private RegistryKey key = Registry.CurrentUser.CreateSubKey("Software\\Microsoft\\Windows\\CurrentVersion\\Run");
        private string keyname = "IVisionAgent";


        public CheckRegistry()
        {
            if (key.GetValue( keyname ) == null)
                selfRegister();
            key.Close();
        }

        private void selfRegister()
        {
            System.Windows.MessageBoxResult res = System.Windows.MessageBox.Show("Do you want to run IVision restore agent?", "IVision", System.Windows.MessageBoxButton.YesNo);
            if (res == System.Windows.MessageBoxResult.Yes)
            {
                Assembly myAssembly = Assembly.GetExecutingAssembly();
                key.SetValue(keyname, myAssembly.Location, RegistryValueKind.String);
            }
        }
    }


}
