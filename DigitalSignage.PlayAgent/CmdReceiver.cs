﻿using System;
using System.Reflection;
using System.Threading;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Messaging;
using System.Net;
using System.Management;

using DigitalSignage.Common;

using NLog;

namespace DigitalSignage.PlayAgent
{
    public class AgentCmdReceiver : MarshalByRefObject, IAgentCmdReceiver
	{
        Logger logger = LogManager.GetCurrentClassLogger();
        
        public long ProcessSimpleCommand(CmdReceiverCommands cmd)
		{
			long res = -1; //not implemented

			try
			{
				Config cfg = Config.GetConfig;

				switch (cmd)
				{
				case CmdReceiverCommands.CmdEcho:
					res = new Random(128).Next(128);
					break;
				case CmdReceiverCommands.CmdGetVersion:
					res = cfg.Version;
					break;
				case CmdReceiverCommands.CmgGetClientPID:
					res = Convert.ToInt64(cfg.PlayerId);
					break;
				case CmdReceiverCommands.CmdGetFreeSpace:
					{
						ManagementObject disk = new ManagementObject(String.Format("win32_logicaldisk.deviceid=\"{0}:\"", cfg.AppData.Substring(0,1)));
						disk.Get();
						res = (long)((ulong)(disk["FreeSpace"]) / 1024);
					}
					break;
				case CmdReceiverCommands.CmdGetUptime:
					res = cfg.Uptime;
					break;
				case CmdReceiverCommands.CmdGetTemperature:
					res = 0;	// No Implement
					break;
				case CmdReceiverCommands.CmdGetMemoryUsage:
					res = cfg.MemoryUsage;
					break;
				case CmdReceiverCommands.CmdGetCpuRate:
					res = cfg.CpuRate;
					break;
				}
			}
			catch (Exception e)
			{
				logger.Error(e + "");
			}
			return res; 
        }

        public bool SendEvent(String sData)
        {
            if(String.IsNullOrEmpty(sData)) return false;

            Config.GetConfig.CurrentRelayID = Convert.ToInt64(sData.Split('|')[0]);

            return WCFAuthoring.GetWCFObject.SendEventToServer(sData);
        }

        DateTime dtPlayerKilled = DateTime.Now;
        public bool InsertPlayerError(int error_cd, string err_desc)
        {
            try
            {
                try
                {
                    /// 2013.05.07. 지원하지 않는 에러및 보호된 메모리의 경우 초기화 작업
                    /// 
                    if (error_cd == (int)ErrorCodes.IS_ERROR_UNSUPPT_CONTENT ||
                        err_desc.Contains("보호된 메모리") ||
                        err_desc.Contains("protected memory"))
                    {
                        DateTime dtNow = DateTime.Now;
                        if (dtPlayerKilled + TimeSpan.FromMinutes(5) < dtNow)
                        {
                            Config.GetConfig.KillPlayer();
                            dtPlayerKilled = dtNow;

                            return true;
                        }

                    }
                }
                catch { }

                return false;
                //Config cfg = Config.GetConfig;

                //return cfg.ServerLogList.InsertPlayerError(cfg.PlayerId, error_cd, err_desc) >= 0;
            }
            catch (Exception e)
            {
                logger.Error(e + "");
            }
            return false;
        }

        public bool IsOnlineToServer()
        {
            return Config.GetConfig.WCFAuthoring;
        }



    }
}
