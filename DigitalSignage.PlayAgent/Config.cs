﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

using DigitalSignage.Common;

using NLog;
using NLog.Targets;
using System.Runtime.Remoting;
using System.Xml;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Configuration;
using DigitalSignage.PlayAgent;
using DigitalSignage.SerialKey;
using SerialPortController;


public class Config
{
	private static Logger logger = LogManager.GetCurrentClassLogger();
    
    // singletone routines
    private static Config instance = null;
	private static readonly object semaphore = new object();
	public TaskList localTasksList = null;
    //public IRSSList localRSSList = null;
	public DigitalSignage.ClientDataBase.DBLayer dbLayer = null;
	public DigitalSignage.ClientDataBase.LogDBLayer logdbLayer = null;

	private string appData;
    ConfigSettings settings = null;

	SubtitleSettings subtitle_ui = null;
	PlayerSettings player_ui = null;

    SensorSettings player_sensor = null;

	ServerConfigSettings server_settings = null;
	AsyncConfigSettings async_settings = null;
    VolumeConfigSettings volume_settings = null;

	private CPUInfo.CPUInfo cpuinfo = new CPUInfo.CPUInfo();

	/// <summary>
	/// 제품 코드
	/// </summary>
	private NESerialKey._ProductCode _productCode = NESerialKey._ProductCode.Undefined;

	public NESerialKey._ProductCode ProductCode {
		get { return _productCode; }
	}

	// lastsync
	private long lastsync = 0;
    /// <summary>
    /// 마지막 싱크 스케줄 가져온 시간
    /// </summary>
    private string lastsynctasksync = String.Empty;

    // player group id
    private string groupId = null;
    // player id
	private string playerId = null;
    // Server name
	private string serverpath = null;
    // FTP server name
    private string mediaSource = null;
    // FTP server user
    private string mediaUser = null;
    // FTP server password
    private string mediaPassword = null;
    // patgh to files on FTP server
    private string mediaPath = null;
    // player version
    private long version = 0;
    // server version
    public long ServerVersion = 0;
    // player utime
    public long Uptime = 0;
    // player should synchronise time with server
    private bool shouldSyncTime = false;
    // server state
    public bool ServerOnline = false;

	public int FTP_Timeout = 10000;

	public bool UsePassiveMode = true;

	//	WCF state
	public bool WCFAuthoring = false;

    public bool CaptureProcess = false;

    public int CaptureRefreshTime = 7;


    /// <summary>
    /// 새로운 스케줄 발생 여부
    /// </summary>
    public bool ScheduleUpdated = false;
    // RS232 parametrs
    public string RS232Port;
    public int RS232Speed;
    public int RS232Bits;

	//	외부데이터 연동 여부
	private bool _hasRefData = false;
	//	외부데이터 갱신 시간 (세컨드)
	private int _refRefreshTime = 3600;
	//	외부데이터 폴더 이름
	private string _refDataSource;

	public bool HasRefData
	{
		get { return _hasRefData; }
		set { _hasRefData = value; }
	}

	public int RefRefreshTime
	{
		get { return _refRefreshTime; }
		set { _refRefreshTime = value; }
	}

	public string RefDataSource
	{
		get { return _refDataSource; }
		set { _refDataSource = value; }
	}

    // remote interfaces
    IServerCmdReceiver serverCmdReceiver = null;
    ITaskList serverTaskList = null;
    IGroupList serverGroupList = null;
    //IRSSList serverRSSList = null;
    IPlayersList serverPlayerList = null;
	ILogList serverLogList = null;
	IDownloadList serverDownloadList = null;
	IResultDataList serverResultDataList = null;
	IDataCacheList serverDataCacheList = null;

	String downloadTimeZone = null;
	Collection<string> arrTimeZones = new Collection<string>();

	public String DownloadTimeZone
	{
		set { downloadTimeZone = value; }
	}

    public NESerialKey._ProductCode CheckProductCode()
	{
		try
		{
			// SaaS 업데이트를 위해 프로덕트 타입을 SaaS로 입력해 준다. //////////////
// 			DigitalSignage.Common.KeyChecker.GetInstance.SetProductType(SerialKey._ProductCode.SoftwareAsaService);
			//////////////////////////////////////////////////////////////////////////

			if (_productCode == SerialKey._ProductCode.Undefined)
				_productCode = DigitalSignage.Common.KeyChecker.GetInstance.CheckProductType();

			return _productCode;
		}
		catch { return NESerialKey._ProductCode.Undefined; }
	}

	public void ParseTimezones(string xml)
	{
		try
		{
			downloadTimeZone = xml;

			arrTimeZones.Clear();

			XmlDocument doc = new XmlDocument();
			doc.LoadXml(downloadTimeZone);

			foreach (XmlNode node in doc.ChildNodes)
			{
				if (node.LocalName.Contains("timezones"))
				{
					foreach (XmlNode timezonenode in node.ChildNodes)
					{
						arrTimeZones.Add(timezonenode.FirstChild.Value);
					}
					break;
				}
			}
		}
		catch
		{

		}
	}
	public bool IsAvailableTimeForDownloading()
	{
		//	아무 정보도 없으면 다운로드 받을수 있는걸로 간주한다.
		if (arrTimeZones == null || arrTimeZones.Count == 0)
			return true;

		TimeSpan ts = new TimeSpan(DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second);

		foreach(string sPiece in arrTimeZones)
		{
			try
			{
				string[] arrPiece = sPiece.Split('-');

				string[] arrStart = arrPiece[0].Split(':');
				string[] arrEnd = arrPiece[1].Split(':');

				int startH = Convert.ToInt32(arrStart[0]);
				int startM = Convert.ToInt32(arrStart[1]);
				int startS = Convert.ToInt32(arrStart[2]);

				int endH = Convert.ToInt32(arrEnd[0]);
				int endM = Convert.ToInt32(arrEnd[1]);
				int endS = Convert.ToInt32(arrEnd[2]);

				TimeSpan tsStart = new TimeSpan(startH, startM, startS);
				TimeSpan tsEnd = new TimeSpan(endH, endM, endS);

				if (tsStart <= ts && ts < tsEnd)
				{
					return true;
				}
			}
			catch {}
		}

		return false;
	}


	public Config()
    {
		String sExecutingPath = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
        appData = sExecutingPath + "\\PlayerData\\";
        if (!Directory.Exists(appData))
            Directory.CreateDirectory(appData);
        // creating directory for XAML screens
        if (!Directory.Exists(appData+"\\xaml\\"))
            Directory.CreateDirectory(appData + "\\xaml\\");

		logger.Info("Checking FTP Timeout..");

		try
		{
			FTP_Timeout = int.Parse(ConfigurationManager.AppSettings["FTP_Timeout"]);
		}
		catch
		{
			FTP_Timeout = 10000;
		}

		logger.Info("FTP Timeout: " + FTP_Timeout);


		logger.Info("Checking UsePassiveMode..");
		try
		{
			UsePassiveMode = bool.Parse(ConfigurationManager.AppSettings["UsePassiveMode"]);
		}
		catch
		{
			UsePassiveMode = true;
		}
		logger.Info("Use Passive Mode: " + UsePassiveMode);

    }

    public void init()
    {
        try
		{
			//	제품 코드를 검색한다.
			CheckProductCode();

            // get own version
            Assembly myAssembly = Assembly.GetExecutingAssembly();
            AssemblyName myAssemblyName = myAssembly.GetName();
            version = ((long)myAssemblyName.Version.Major << 48) |
                ((long)myAssemblyName.Version.Minor << 32) |
                (myAssemblyName.Version.Build << 16) |
                myAssemblyName.Version.Revision;

            // reading configuration file
            settings = new ConfigSettings(appData + "config.xml");
			server_settings = new ServerConfigSettings(appData + "server_config.xml");
			async_settings = new AsyncConfigSettings(appData + "async.xml");
            volume_settings = new VolumeConfigSettings(appData + "volume_config.xml");
            subtitle_ui = new SubtitleSettings(appData + "subtitle_ui.xml");
			player_ui = new PlayerSettings(appData + "player_ui.xml");
            player_sensor = new SensorSettings(appData + "sensor.xml");

			logger.Info( "DigitalSignage.Player "+((Version >> 48) & 0xFFFF) + "." +
                    ((Version >> 32) & 0xFFFF) + "." +
                    ((Version >> 16) & 0xFFFF) + "." + (Version & 0xFFFF));
            
			logger.Info("Server address = " + ServerPath);
			
            // opening database
            dbLayer = new DigitalSignage.ClientDataBase.DBLayer();
            dbLayer.openDB(AppData);

			logdbLayer = new DigitalSignage.ClientDataBase.LogDBLayer();
			logdbLayer.openDB(AppData);
			
            // opening task list
            localTasksList = new TaskList();

			//	메모리에 확보
			groupId = GroupIdFromFile;
			playerId = PlayerIdFromFile;
			serverpath = ServerPathFromFile;

			lastsync = LastSyncFromFile;
        }
        catch (Exception e)
        {
            logger.Error(e + " "+e.StackTrace);
        }
    }
	static int nAliveCheckCount = 0;
	public bool AliveCheckerFromPlayer()
	{
		try
		{
            IPlayerCmdReceiver playerCmdReceiver = (IPlayerCmdReceiver)Activator.GetObject(
              typeof(IPlayerCmdReceiver), "tcp://127.0.0.1:1889/CmdReceiverHost/rt");

			long echo = playerCmdReceiver.ProcessSimpleCommand(CmdReceiverCommands.CmdEcho);
			//	플레이어에서 -1을 리턴하면 삭제
			if (echo == -1) return false;
			return true;
		}
		catch(Exception err)
		{
			if(nAliveCheckCount++ > 5)
			{
				//	Player가 죽은 경우로 간주
				logger.Error(err.Message);
				nAliveCheckCount = 0;

				return false;
			}

			return true;
		}
	}

	public bool KillPlayer()
	{
		try
		{
			Process[] processlist = Process.GetProcessesByName("DS.ScreenCast");
			foreach (Process proc in processlist)
			{
				try
				{
					String fname = System.IO.Path.GetFileName(proc.MainModule.FileName).ToLower();
					if (fname.Equals("ds.screencast.exe"))
					{
						try
						{
							proc.Kill();
							proc.Dispose();
							logger.Info("Founded 'i-vision.player.exe' process. It will be closed.");
							break;
						}
						catch (Exception ex)
						{

							logger.Error("ModuleName=" + fname + ", " + ex.Message);
						}
					}
				}
				catch (Exception err)
				{

					logger.Error(err.Message);
				}
			}			
		}
		catch (Exception err)
		{
			//	Player가 죽은 경우로 간주
			logger.Error(err.Message);
			return false;
		}

		return false;
	}

    public bool KillOwnProcess()
    {
        Process proc = Process.GetCurrentProcess();
        try
        {
            String fname = System.IO.Path.GetFileName(proc.MainModule.FileName).ToLower();

            try
            {
                proc.Kill();
                proc.Dispose();
                logger.Info("Founded '{0}' process. It will be closed.", fname);
                return true;
            }
            catch (Exception ex)
            {

                logger.Error("ModuleName=" + fname + ", " + ex.Message);
            }
        }
        catch (Exception err)
        {

            logger.Error(err.Message);
        }

        return false;
    }

	public String LastErrorMessage = "";
    public int connectToServer()
    {
        try
        {
			if (WCFAuthoring == false)
			{
				throw new Exception("WCF has not been connected.");
			}
            if (!ServerPath.StartsWith("tcp://"))
                ServerPath = "tcp://" + ServerPath;
            if (!ServerPath.EndsWith("/"))
                ServerPath += "/";
            // initializing remote interfaces
			serverTaskList = (ITaskList)Activator.GetObject(
			  typeof(ITaskList), ServerPath + "TaskListHost/rt");
            serverCmdReceiver = (IServerCmdReceiver)Activator.GetObject(
                typeof(IServerCmdReceiver), ServerPath + "CmdReceiverHost/rt");
            serverGroupList = (IGroupList)Activator.GetObject(
                typeof(IGroupList), ServerPath + "GroupListHost/rt");
            //serverRSSList = (IRSSList)Activator.GetObject(
              //  typeof(IRSSList), ServerPath + "RSSListHost/rt");
            serverPlayerList = (IPlayersList)Activator.GetObject(
                typeof(IPlayersList), ServerPath + "PlayerListHost/rt");
			serverLogList = (ILogList)Activator.GetObject(
				typeof(ILogList), ServerPath + "LogListHost/rt");
			serverDownloadList = (IDownloadList)Activator.GetObject(
				typeof(IDownloadList), ServerPath + "DownloadListHost/rt");
            serverResultDataList = (IResultDataList)Activator.GetObject(
                typeof(IResultDataList), ServerPath + "ResultDataHost/rt");
			serverDataCacheList = (IDataCacheList)Activator.GetObject(
				typeof(IDataCacheList), ServerPath + "DataCacheHost/rt");
			
            /*
			SerialKey._ProductCode server = serverDataCacheList.GetProductCode();
			if (server != this.ProductCode)
			{
				LastErrorMessage = String.Format("Product code is not same with server. ({0})", server.ToString());
				ServerOnline = false;
				return -1;
			}
             * */

            //localRSSList = serverRSSList;
            ServerVersion = serverCmdReceiver.ProcessSimpleCommand(CmdReceiverCommands.CmdGetVersion);
            logger.Info("DigitalSignage.SchedulerService version " + ((ServerVersion >> 48) & 0xFFFF) + "." +
                    ((ServerVersion >> 32) & 0xFFFF) + "." +
                    ((ServerVersion >> 16) & 0xFFFF) + "." + (ServerVersion & 0xFFFF));
            return receiveNetworkConfiguration();
        }
        catch (Exception e)
        {
            logger.Error(e + "");
			LastErrorMessage = e.Message;
        }
        ServerOnline = false;
        return -1;
    }
	private void AnalyzeContentServerInfo(string grouplist)
	{
		GroupManager groupMGR = GroupManager.GetInstance;

		string[] arrGroups = grouplist.Split('|');
		int nLen = arrGroups.Length;

		if(nLen > 0)
		{
			string source = groupMGR.GetGroupInfo(arrGroups[0], "source");

			if(source.StartsWith("$"))
			{
				int nIndex = source.IndexOf(":");

				if(source.ToLower().Contains("$local"))
				{
					source = "127.0.0.1" + 
						(nIndex != -1 ? source.Substring(nIndex) : "");
				}
				else if(source.ToLower().Contains("$server"))
				{
					string server = ServerPath.ToLower();
					server = server.StartsWith("tcp://") ? server.Substring(6) : server;
					server = server.LastIndexOf(":") != -1 ? server.Substring(0, server.LastIndexOf(":")) : server;

					source = server +
						(nIndex != -1 ? source.Substring(nIndex) : "");
				}
			}

			// getting group information
			logger.Info("Group GID=" + grouplist);
			logger.Info("Player PID=" + playerId);
			mediaSource = source;
			mediaUser = groupMGR.GetGroupInfo(arrGroups[0], "username");
			mediaPassword = groupMGR.GetGroupInfo(arrGroups[0], "password");
			mediaPath = groupMGR.GetGroupInfo(arrGroups[0], "path");
		}
		else
			throw new Exception("There is no [Group Information] in serverDB. (PID=" + PlayerId + ")");
	}

    public int receiveNetworkConfiguration()
    {
        try
        {
			if (WCFAuthoring == false)
			{
				throw new Exception("WCF has not been connected.");
			}
	
			GroupManager groupMGR = GroupManager.GetInstance;
			groupMGR.GroupDocument = serverGroupList.GetGroupsXmlByPID(PlayerId);

			string grouplist = groupMGR.GetGroupsWithSeperartor();

			if(!grouplist.Equals(GroupIds))
			{
				GroupIdFromFile = grouplist;
				this.LastSyncFromFile = 0;
                this.LastSyncTaskSyncFromFile = DateTime.MinValue.ToString("yyyyMMddHHmmss");
				this.localTasksList.CleanAllTasks();
                this.localTasksList.CleanAllSyncTaskList();
				this.KillPlayer();
			}

			try
			{
				AnalyzeContentServerInfo(grouplist);

				if (serverCmdReceiver.ProcessSimpleCommand(CmdReceiverCommands.CmdGetSyncState) == 1)
					shouldSyncTime = true;
			}
			catch (Exception exx)
			{
				logger.Error(exx.ToString());
			}
            ServerOnline = true;
           
			return 0;
        }
        catch (Exception e)
        {
            logger.Error(e.Message);
        }
        ServerOnline = false;
        return -1;
    }

    public static Config GetConfig
    {
        get
        {
           lock (semaphore)
           {
               if (instance == null)
               {
                   instance = new Config();
                   instance.init();
               }
              return instance;
           }
        }
    }

	#region 제어 스케줄 관련

	public bool IsPlayerOn
	{
		get
		{
			string val = null;
			bool res = true;
			try
			{
				lock (semaphore)
				{
					val = ConfigSettings.ReadSetting("player_on");
				}

				if (!bool.TryParse(val, out res))
					return true;
			}
			catch (Exception e)
			{
				lock (semaphore)
				{
					ConfigSettings.WriteSetting("player_on", true.ToString());
				}
				logger.Warn(e + "");
			}
			return res;
		}
		set
		{
			lock (semaphore)
			{
				ConfigSettings.WriteSetting("player_on", value.ToString());
			}
		}
	}

    public int StoredWaveVolume
    {
        get
        {
            string val = null;
            int res = -1;
            try
            {
                lock (semaphore)
                {
                    val = VolumeConfigSettings.ReadSetting("stored_w_volume");
                }

                if (int.TryParse(val, out res) == false) throw new Exception();
            }
            catch (Exception e)
            {
                lock (semaphore)
                {
                    VolumeConfigSettings.WriteSetting("stored_w_volume", "-1");
                }
                logger.Warn(e + "");

                return -1;
            }
            return res;
        }
        set
        {
            lock (semaphore)
            {
                VolumeConfigSettings.WriteSetting("stored_w_volume", value + "");
            }
        }
    }

    public int StoredMasterVolume
    {
        get
        {
            string val = null;
            int res = -1;
            try
            {
                lock (semaphore)
                {
                    val = VolumeConfigSettings.ReadSetting("stored_m_volume");
                }

                if (int.TryParse(val, out res) == false) throw new Exception();
            }
            catch (Exception e)
            {
                lock (semaphore)
                {
                    VolumeConfigSettings.WriteSetting("stored_m_volume", "-1");
                }
                logger.Warn(e + "");

                return -1;
            }
            return res;
        }
        set
        {
            lock (semaphore)
            {
                VolumeConfigSettings.WriteSetting("stored_m_volume", value + "");
            }
        }
    }

    public int StoredSerialVolume
    {
        get
        {
            string val = null;
            int res = -1;
            try
            {
                lock (semaphore)
                {
                    val = VolumeConfigSettings.ReadSetting("stored_s_volume");
                }

                if (int.TryParse(val, out res) == false) throw new Exception();
            }
            catch (Exception e)
            {
                lock (semaphore)
                {
                    VolumeConfigSettings.WriteSetting("stored_s_volume", "-1");
                }
                logger.Warn(e + "");

                return -1;
            }
            return res;
        }
        set
        {
            lock (semaphore)
            {
                VolumeConfigSettings.WriteSetting("stored_s_volume", value + "");
            }
        }
    }

    public int ProcessingEventVolume
    {
        get
        {
            string val = null;
            int res = -1;
            try
            {
                lock (semaphore)
                {
                    val = VolumeConfigSettings.ReadSetting("processing_event_volume");
                }

                if (int.TryParse(val, out res) == false) throw new Exception();
            }
            catch (Exception e)
            {
                lock (semaphore)
                {
                    VolumeConfigSettings.WriteSetting("processing_event_volume", "-1");
                }
                logger.Warn(e + "");

                return -1;
            }
            return res;
        }
        set
        {
            lock (semaphore)
            {
                VolumeConfigSettings.WriteSetting("processing_event_volume", value + "");
            }
        }
    }

    public int StoredEventVolume
    {
        get
        {
            string val = null;
            int res = 5;
            try
            {
                lock (semaphore)
                {
                    val = VolumeConfigSettings.ReadSetting("stored_event_volume");
                }

                if (int.TryParse(val, out res) == false) throw new Exception();
            }
            catch (Exception e)
            {
                lock (semaphore)
                {
                    VolumeConfigSettings.WriteSetting("stored_event_volume", "5");
                }
                logger.Warn(e + "");
            }
            return res;
        }
        set
        {
            lock (semaphore)
            {
                VolumeConfigSettings.WriteSetting("stored_event_volume", value + "");
            }
        }
    }
    #endregion

	public string ServerInitialCode
	{
		get
		{
			String sReturn = null;
			lock (semaphore)
			{
				sReturn = ServerConfigSettings.ReadSetting("server_code");
			}
			return sReturn;
		}
		set
		{
			lock (semaphore)
			{
				ServerConfigSettings.WriteSetting("server_code", value);
			}
		}
	}

	public string ServerPathFromFile
	{
		get
		{
			String sReturn = null;
			lock (semaphore)
			{
				sReturn = ServerConfigSettings.ReadSetting("server_path");
			}
			return sReturn; 
		}
		set
		{
			lock (semaphore)
			{
				ServerConfigSettings.WriteSetting("server_path", value);
			}
			//	Save the WCF path
			serverpath = value;
		}
	}
	public String WCFEndpoint
	{
		set
		{
			try
			{
				Assembly executed = Assembly.GetExecutingAssembly();
				String configPath = executed.Location + ".config";

				XmlDocument xroot = new XmlDocument();
				xroot.Load(configPath);
				foreach (XmlNode configuration in xroot.ChildNodes)
				{
					if(configuration.LocalName.Contains("configuration"))
					{
						foreach (XmlNode node in configuration.ChildNodes)
						{

							if (node.LocalName.Contains("system.serviceModel"))
							{
								node.FirstChild.FirstChild.Attributes["address"].Value = value;
								xroot.Save(configPath);
								break;
							}
						}
						break;
					}
				}

			}
			catch(Exception e)
			{
				logger.Error(e.Message);
			}

		}
	}

	#region Subtitle UI Settings
	public long SubSpeed
	{
		get
		{
			long subspeed = 50;
			string val = null;

			lock (semaphore)
			{
                val = SubtitleSettings.GetInstance.ReadSetting("speed");
			}
			try
			{
				long.TryParse(val, out subspeed);
				if (String.IsNullOrEmpty(val))
				{
					subspeed = 50;
                    SubtitleSettings.GetInstance.WriteSetting("speed", subspeed + "");
				}
			}
			catch (Exception ex)
			{
				logger.Error(ex.ToString());
			}
			return subspeed;
		}
		set
		{
			lock (semaphore)
			{
                SubtitleSettings.GetInstance.WriteSetting("speed", value + "");
			}
		}
	}

    public bool IsTransparentToSubtitle
    {
        get
        {
            bool bTransparent = false;

            string val = null;

            lock (semaphore)
            {
                val = SubtitleSettings.GetInstance.ReadSetting("opt_transparent");
            }
            try
            {
                bool.TryParse(val, out bTransparent);
                if (String.IsNullOrEmpty(val))
                {
                    bTransparent = true;
                    SubtitleSettings.GetInstance.WriteSetting("opt_transparent", bTransparent + "");
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return bTransparent;
        }
        set
        {
            lock (semaphore)
            {
                SubtitleSettings.GetInstance.WriteSetting("opt_transparent", value + "");
            }

        }
    }
	public bool IsManualToSubtitle
	{
		get 
		{
			bool bManual = false;

			string val = null;

			lock (semaphore)
			{
                val = SubtitleSettings.GetInstance.ReadSetting("opt_manual");
			}
			try
			{
				bool.TryParse(val, out bManual);
				if (String.IsNullOrEmpty(val))
				{
					bManual = false;
                    SubtitleSettings.GetInstance.WriteSetting("opt_manual", bManual + "");
				}
			}
			catch (Exception ex)
			{
				logger.Error(ex.ToString());
			}
			return bManual;
		}
		set
		{
			lock (semaphore)
			{
                SubtitleSettings.GetInstance.WriteSetting("opt_manual", value + "");
			}

		}
	}
	public long SubPositionY
	{
		get
		{
			long subposY = 550;
			string val = null;

			lock (semaphore)
			{
                val = SubtitleSettings.GetInstance.ReadSetting("positionY");
			}
			try
			{
				long.TryParse(val, out subposY);
				if (String.IsNullOrEmpty(val))
				{
					subposY = 550;
                    SubtitleSettings.GetInstance.WriteSetting("positionY", subposY + "");
				}
			}
			catch (Exception ex)
			{
				logger.Error(ex.ToString());
			}

			return subposY;
		}
		set
		{
			lock (semaphore)
			{
                SubtitleSettings.GetInstance.WriteSetting("positionY", value + "");
			}
		}
	}

    public string SubBackcolor
    {
        get
        {
            string subcolor = "#000000";
            string val = null;

            lock (semaphore)
            {
                val = SubtitleSettings.GetInstance.ReadSetting("background");
            }
            try
            {
                subcolor = val;
                if (String.IsNullOrEmpty(val))
                {
                    subcolor = "#000000";
                    SubtitleSettings.GetInstance.WriteSetting("background", subcolor + "");
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }

            return subcolor;
        }
        set
        {
            lock (semaphore)
            {
                SubtitleSettings.GetInstance.WriteSetting("background", value + "");
            }
        }
    }
	#endregion

    #region 감지센서 관련
    /// <summary>
    /// 감지센서 사용 여부
    /// </summary>
    public bool UseSensor
    {
        get
        {
            bool bReturn = false;
            String sensor = String.Empty;

            lock (semaphore)
            {
                sensor = SensorSettings.GetInstance.ReadSetting("use_sensor");
            }
            try
            {
                if (!bool.TryParse(sensor, out bReturn))
                {
                    bReturn = false;

                    lock (semaphore)
                    {
                        SensorSettings.GetInstance.WriteSetting("use_sensor", bReturn.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return bReturn;
        }
        set
        {
            lock (semaphore)
            {
                SensorSettings.GetInstance.WriteSetting("use_sensor", value + "");
            }
        }
    }

    /// <summary>
    /// 감지센서 COM PORT
    /// </summary>
    public String SensorPort
    {
        get
        {
            String val = "COM3";

            lock (semaphore)
            {
                val = SensorSettings.GetInstance.ReadSetting("sensor_port");
            }
            try
            {
                if (String.IsNullOrEmpty(val))
                {
                    lock (semaphore)
                    {
                        SensorSettings.GetInstance.WriteSetting("sensor_port", val = "COM3");
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return val;
        }
        set
        {
            lock (semaphore)
            {
                SensorSettings.GetInstance.WriteSetting("sensor_port", value + "");
            }
        }
    }

    /// <summary>
    /// 감지센서 COM PORT
    /// </summary>
    public String SensorTempIDs
    {
        get
        {
            String val = "";

            lock (semaphore)
            {
                val = SensorSettings.GetInstance.ReadSetting("sensor_template_ids");
            }
            try
            {
                if (String.IsNullOrEmpty(val))
                {
                    lock (semaphore)
                    {
                        SensorSettings.GetInstance.WriteSetting("sensor_template_ids", val = "");
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return val;
        }
        set
        {
            lock (semaphore)
            {
                SensorSettings.GetInstance.WriteSetting("sensor_template_ids", value + "");
            }
        }
    }

    /// <summary>
    /// 감지센서 근접 기준 값
    /// </summary>
    public int SensorValue
    {
        get
        {
            int nValue = 80;
            String val = String.Empty;

            lock (semaphore)
            {
                val = SensorSettings.GetInstance.ReadSetting("sensor_value");
            }
            try
            {
                if (!int.TryParse(val, out nValue))
                {
                    lock (semaphore)
                    {
                        nValue = 80;
                        SensorSettings.GetInstance.WriteSetting("sensor_value", nValue.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return nValue;
        }
        set
        {
            lock (semaphore)
            {
                SensorSettings.GetInstance.WriteSetting("sensor_value", value + "");
            }
        }
    }

    /// <summary>
    /// 감지센서 모니터 OFF 기준 (초)
    /// </summary>
    public int SensorLimitTS
    {
        get
        {
            int nValue = 60;
            String val = String.Empty;

            lock (semaphore)
            {
                val = SensorSettings.GetInstance.ReadSetting("sensor_off_ts");
            }
            try
            {
                if (!int.TryParse(val, out nValue))
                {
                    lock (semaphore)
                    {
                        nValue = 60;
                        SensorSettings.GetInstance.WriteSetting("sensor_off_ts", nValue.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return nValue;
        }
        set
        {
            lock (semaphore)
            {
                SensorSettings.GetInstance.WriteSetting("sensor_off_ts", value + "");
            }
        }
    }

    #endregion


	#region Player UI Settings
	public String ComPort
	{
		get
		{
			String defaultPort = "COM1";
			string val = null;

			lock (semaphore)
			{
                val = PlayerSettings.GetInstance.ReadSetting("com_port_name");
			}
			try
			{
				if (String.IsNullOrEmpty(val))
				{
                    PlayerSettings.GetInstance.WriteSetting("com_port_name", defaultPort + "");
				}
				else defaultPort = val;
			}
			catch (Exception ex)
			{
				logger.Error(ex.ToString());
			}
			return defaultPort;
		}
		set
		{
			lock (semaphore)
			{
                PlayerSettings.GetInstance.WriteSetting("com_port_name", value + "");
			}
		}
	}

	public bool FadeStart
	{
		get
		{
			bool fade = false;
			string val = null;

			lock (semaphore)
			{
                val = PlayerSettings.GetInstance.ReadSetting("fade_start");
			}
			try
			{
				bool.TryParse(val, out fade);
				if (String.IsNullOrEmpty(val))
				{
					fade = false;
                    PlayerSettings.GetInstance.WriteSetting("fade_start", fade + "");
				}
			}
			catch (Exception ex)
			{
				logger.Error(ex.ToString());
			}
			return fade;
		}
		set
		{
			lock (semaphore)
			{
                PlayerSettings.GetInstance.WriteSetting("fade_start", value + "");
			}
		}
	}

	public bool FadeEnd
	{
		get
		{
			bool fade = false;
			string val = null;

			lock (semaphore)
			{
                val = PlayerSettings.GetInstance.ReadSetting("fade_end");
			}
			try
			{
				bool.TryParse(val, out fade);
				if (String.IsNullOrEmpty(val))
				{
					fade = false;
                    PlayerSettings.GetInstance.WriteSetting("fade_end", fade + "");
				}
			}
			catch (Exception ex)
			{
				logger.Error(ex.ToString());
			}
			return fade;
		}
		set
		{
			lock (semaphore)
			{
                PlayerSettings.GetInstance.WriteSetting("fade_end", value + "");
			}
		}
	}

	public bool IsManualToPlayer
	{
		get
		{
			bool manual = false;
			string val = null;

			lock (semaphore)
			{
                val = PlayerSettings.GetInstance.ReadSetting("opt_manual");
			}
			try
			{
				bool.TryParse(val, out manual);
				if (String.IsNullOrEmpty(val))
				{
					manual = false;
                    PlayerSettings.GetInstance.WriteSetting("opt_manual", manual + "");
				}
			}
			catch (Exception ex)
			{
				logger.Error(ex.ToString());
			}
			return manual;
		}
		set
		{
			lock (semaphore)
			{
                PlayerSettings.GetInstance.WriteSetting("opt_manual", value + "");
			}
		}
	}

	public bool ShowWindowEdge
	{
		get
		{
			bool edge = false;
			string val = null;

			lock (semaphore)
			{
                val = PlayerSettings.GetInstance.ReadSetting("show_window_edge");
			}
			try
			{
				bool.TryParse(val, out edge);
				if (String.IsNullOrEmpty(val))
				{
					edge = false;
                    PlayerSettings.GetInstance.WriteSetting("show_window_edge", edge + "");
				}
			}
			catch (Exception ex)
			{
				logger.Error(ex.ToString());
			}
			return edge;
		}
		set
		{
			lock (semaphore)
			{
                PlayerSettings.GetInstance.WriteSetting("show_window_edge", value + "");
			}
		}
	}

	public long WindowXPos
	{
		get
		{
			long posX = 0;
			string val = null;

			lock (semaphore)
			{
                val = PlayerSettings.GetInstance.ReadSetting("X");
			}
			try
			{
				long.TryParse(val, out posX);
				if(String.IsNullOrEmpty(val))
                    PlayerSettings.GetInstance.WriteSetting("X", posX + "");
			}
			catch (Exception ex)
			{
				logger.Error(ex.ToString());
			}
			return posX;
		}
		set
		{
			lock (semaphore)
			{
                PlayerSettings.GetInstance.WriteSetting("X", value + "");
			}
		}
	}
	public long WindowYPos
	{
		get
		{
			long posY = 0;
			string val = null;

			lock (semaphore)
			{
                val = PlayerSettings.GetInstance.ReadSetting("Y");
			}
			try
			{
				long.TryParse(val, out posY);
				if(String.IsNullOrEmpty(val))
                    PlayerSettings.GetInstance.WriteSetting("Y", posY + "");
			}
			catch (Exception ex)
			{
				logger.Error(ex.ToString());
			}
			return posY;
		}
		set
		{
			lock (semaphore)
			{
                PlayerSettings.GetInstance.WriteSetting("Y", value + "");
			}
		}
	}
	public long WindowWidth
	{
		get
		{
			long width = 800;
			string val = null;

			lock (semaphore)
			{
                val = PlayerSettings.GetInstance.ReadSetting("width");
			}
			try
			{
				long.TryParse(val, out width);
				if (String.IsNullOrEmpty(val))
				{
					width = 800;
                    PlayerSettings.GetInstance.WriteSetting("width", width + "");
				}
			}
			catch (Exception ex)
			{
				logger.Error(ex.ToString());
			}
			return width;
		}
		set
		{
			lock (semaphore)
			{
                PlayerSettings.GetInstance.WriteSetting("width", value + "");
			}
		}
	}
	public long WindowHeight
	{
		get
		{
			long height = 600;
			string val = null;

			lock (semaphore)
			{
                val = PlayerSettings.GetInstance.ReadSetting("height");
			}
			try
			{
				long.TryParse(val, out height);
				if (String.IsNullOrEmpty(val))
				{
					height = 600;
                    PlayerSettings.GetInstance.WriteSetting("height", height + "");
				}
			}
			catch (Exception ex)
			{
				logger.Error(ex.ToString());
			}
			return height;
		}
		set
		{
			lock (semaphore)
			{
                PlayerSettings.GetInstance.WriteSetting("height", value + "");
			}
		}
	}

	public bool WindowTopMost
	{
		get
		{
			bool topmost = false;
			string val = null;

			lock (semaphore)
			{
                val = PlayerSettings.GetInstance.ReadSetting("topmost");
			}
			try
			{
				bool.TryParse(val, out topmost);
				if (String.IsNullOrEmpty(val))
				{
					topmost = false;
                    PlayerSettings.GetInstance.WriteSetting("topmost", topmost + "");
				}
			}
			catch (Exception ex)
			{
				logger.Error(ex.ToString());
			}
			return topmost;
		}
		set
		{
			lock (semaphore)
			{
                PlayerSettings.GetInstance.WriteSetting("topmost", value + "");
			}
		}
	}

    /// <summary>
    /// 시리얼 장비 선택
    /// </summary>
	public SupportedSerialPortDevice SerialPortDevice
	{
		get
		{
			SupportedSerialPortDevice device = SupportedSerialPortDevice.deviceNone;
			string val = null;
            val = PlayerSettings.GetInstance.ReadSetting("serial_port_device");
			try
			{
				int nDevice = 0;
				if (int.TryParse(val, out nDevice))
				{
					device = (SupportedSerialPortDevice)nDevice;
				}

				if (String.IsNullOrEmpty(val))
				{
					device = SupportedSerialPortDevice.deviceNone;
                    PlayerSettings.GetInstance.WriteSetting("serial_port_device", ((int)device).ToString());
				}
			}
			catch
			{
			}
			return device;
		}
		set
		{
            PlayerSettings.GetInstance.WriteSetting("serial_port_device", (int)value + "");
		}
	}

    /// <summary>
    /// WoL 사용 여부
    /// </summary>
	public bool UsingWoL
	{
		get
		{
			bool bValue = false;
			string val = null;
            val = PlayerSettings.GetInstance.ReadSetting("UsingWoL");

			try
			{
				if (String.IsNullOrEmpty(val))
				{
                    PlayerSettings.GetInstance.WriteSetting("UsingWoL", bValue.ToString());
				}
				else
				{
					bool.TryParse(val, out bValue);
				}
			}
			catch
			{
			}
			return bValue;
		}
		set
		{
            PlayerSettings.GetInstance.WriteSetting("UsingWoL", (bool)value + "");
		}
	}

    /// <summary>
    /// AMT 사용 여부
    /// </summary>
	public bool UsingAMT
	{
		get
		{
			bool bValue = false;
			string val = null;
            val = PlayerSettings.GetInstance.ReadSetting("UsingAMT");

			try
			{
				if (String.IsNullOrEmpty(val))
				{
                    PlayerSettings.GetInstance.WriteSetting("UsingAMT", bValue.ToString());
				}
				else
				{
					bool.TryParse(val, out bValue);
				}
			}
			catch
			{
			}
			return bValue;
		}
		set
		{
            PlayerSettings.GetInstance.WriteSetting("UsingAMT", (bool)value + "");
		}
	}

    /// <summary>
    /// AMT 계정 아이디
    /// </summary>
	public String AuthID
	{
		get
		{
			string val = null;
            val = PlayerSettings.GetInstance.ReadSetting("AMT_AuthID");

			try
			{
				if (String.IsNullOrEmpty(val))
				{
                    PlayerSettings.GetInstance.WriteSetting("AMT_AuthID", "admin");
				}
			}
			catch
			{
			}
			return val;
		}
		set
		{
            PlayerSettings.GetInstance.WriteSetting("AMT_AuthID", value);
		}
	}

    /// <summary>
    /// AMt 계정 패스워드
    /// </summary>
	public String AuthPass
	{
		get
		{
			string val = null;
            val = PlayerSettings.GetInstance.ReadSetting("AMT_AuthPass");

			try
			{
				if (String.IsNullOrEmpty(val))
				{
                    PlayerSettings.GetInstance.WriteSetting("AMT_AuthPass", "Admin12#$");
				}
			}
			catch
			{
			}
			return val;
		}
		set
		{
            PlayerSettings.GetInstance.WriteSetting("AMT_AuthPass", value);
		}
	}
	#endregion


    public string ServerPath
    {
        get 
        {
			return serverpath;
        }
        set
        {
            serverpath = value;
        }
    }

	public long LastSync
	{
		get
		{
			return lastsync;
		}
		set
		{
			lastsync = value;
		}
	}
	
	public long LastSyncFromFile
    {
		get
		{
			string val = null;
			lock (semaphore)
			{
				val = AsyncConfigSettings.ReadSetting("last_sync");
			}
			lastsync = 0;
			try
			{
				long.TryParse(val, out lastsync);
			}
			catch (Exception e)
			{
				logger.Warn(e + "");
			}
			return lastsync;
		}
		set
		{
			lock (semaphore)
			{
				AsyncConfigSettings.WriteSetting("last_sync", value + "");
			}

			lastsync = value;
		}
    }

    /// <summary>
    /// 마지막 싱크 스케줄 가져온 시간
    /// </summary>
    public string LastSyncTaskSync
    {
        get
        {
            return lastsynctasksync;
        }
        set
        {
            lastsynctasksync = value;
        }
    }

    /// <summary>
    /// 마지막 싱크 스케줄 가져온 시간
    /// </summary>
    public string LastSyncTaskSyncFromFile
    {
        get
        {
            string val = null;
            lock (semaphore)
            {
                val = AsyncConfigSettings.ReadSetting("last_synctask_sync");
            }

            if (String.IsNullOrEmpty(val)) lastsynctasksync = TimeConverter.ConvertYYYYMMDDH24mmSSFromUTP(TimeConverter.ConvertToUTP(DateTime.MinValue));
            else lastsynctasksync = val;

            return lastsynctasksync;
        }
        set
        {
            lock (semaphore)
            {
                AsyncConfigSettings.WriteSetting("last_synctask_sync", value + "");
            }

            lastsynctasksync = value;
        }
    }


    public bool PreLoadingMode
    {
        get
        {
			bool res = true;
			string val = null;
            lock (semaphore)
            {
                val = PlayerSettings.GetInstance.ReadSetting("pre_loading_mode");
				if (String.IsNullOrEmpty(val))
				{
                    PlayerSettings.GetInstance.WriteSetting("pre_loading_mode", res + "");
				}
				else
				{
					try
					{
						bool.TryParse(val, out res);
					}
					catch
					{
                        PlayerSettings.GetInstance.WriteSetting("pre_loading_mode", res + "");
					}
				}
            }

            return res;
        }
        set
        {
            lock (semaphore)
            {
                PlayerSettings.GetInstance.WriteSetting("pre_loading_mode", value + "");
            }
        }
    }

    public bool RebootOnError
    {
        get
        {
            bool res = false;
            string val = null;
            lock (semaphore)
            {
                val = PlayerSettings.GetInstance.ReadSetting("reboot_on_error");
                if (String.IsNullOrEmpty(val))
                {
                    PlayerSettings.GetInstance.WriteSetting("reboot_on_error", res + "");
                }
                else
                {
                    try
                    {
                        bool.TryParse(val, out res);
                    }
                    catch
                    {
                        PlayerSettings.GetInstance.WriteSetting("reboot_on_error", res + "");
                    }
                }
            }

            return res;
        }
        set
        {
            lock (semaphore)
            {
                PlayerSettings.GetInstance.WriteSetting("reboot_on_error", value + "");
            }
        }
    }


	public bool TouchMode
	{
		get
		{
			bool res = false;
			string val = null;
			lock (semaphore)
			{
                val = PlayerSettings.GetInstance.ReadSetting("touch_mode");
				if (String.IsNullOrEmpty(val))
				{
                    PlayerSettings.GetInstance.WriteSetting("touch_mode", res + "");
				}
				else
				{
					try
					{
						bool.TryParse(val, out res);
					}
					catch
					{
                        PlayerSettings.GetInstance.WriteSetting("touch_mode", res + "");
					}
				}
			}

			return res;
		}
		set
		{
			lock (semaphore)
			{
                PlayerSettings.GetInstance.WriteSetting("touch_mode", value + "");
			}
		}
	}

    public long FirstStart
    {
        get
        {
			string val = null;
			lock (semaphore)
			{
				val = ConfigSettings.ReadSetting("first_start");
			}
            long res = 0;
            try
            {
                long.TryParse(val, out res);
            }
            catch (Exception e)
            {
                logger.Warn(e + "");
            }
            return res;
        }
        set
        {
			lock (semaphore)
			{
				ConfigSettings.WriteSetting("first_start", value + "");
			}
        }
    }
	public string GroupId
	{
		get
		{
			String[] arr = GroupIds.Split('|');
			int nLen = arr.Length;
			return arr[nLen -1];
		}
	}
	public string GroupIds
	{
		get
		{
			return groupId;
		}
		set
		{
			groupId = value;
		}
	}

	public string GroupIdFromFile
	{
		get
		{
			try
			{
				lock (semaphore)
				{
					groupId = ConfigSettings.ReadSetting("group_id");
				}
				return groupId;
			}
			catch (Exception e)
			{
				logger.Warn(e + "");
			}

			return "-1";
		}
		set
		{
			lock (semaphore)
			{
				ConfigSettings.WriteSetting("group_id", value + "");
			}
			groupId = value;
		}
	}
	public string PlayerId
	{
		get
		{
			return playerId;
		}
		set
		{
			playerId = value;
		}
	}
	public string PlayerIdFromFile
	{
		get
		{
			try
			{
                
				lock (semaphore)
				{
					playerId = ConfigSettings.ReadSetting("player_id");
				}
				return playerId;
			}
			catch (Exception e)
			{
				logger.Warn(e + "");
			}

			return "-1";
		}
		set
		{
			lock (semaphore)
			{
				ConfigSettings.WriteSetting("player_id", value + "");
			}
			playerId = value;
		}
	}
    #region Properties
    public string AppData { get { return appData; } }
    public string FTPServer { get { return mediaSource; } }
    public string FTPUser { get { return mediaUser; } }
    public string FTPPassword { get { return mediaPassword; } }
    public string FTPPath { get { return mediaPath; } }
    public long Version { get { return version; } }
    public bool ShouldSyncTime { get { return shouldSyncTime; } }
	public bool HasWCFAuthoring { get { return WCFAuthoring; } set { WCFAuthoring = value; } }

	public int CpuRate { get { return cpuinfo != null ? (int)cpuinfo.GetCpuinfo() : -1; } }
	public int MemoryUsage { get { return cpuinfo != null ? (int)cpuinfo.GetMemory() : -1; } }

	PerformanceCounter IoReadCounter = null;
	public int DiskReadIORate
	{
		get
		{
			try
			{
				if (IoReadCounter == null)
				{
					IoReadCounter = new PerformanceCounter();
					IoReadCounter.CategoryName = "PhysicalDisk";
					IoReadCounter.CounterName = "% Disk Read Time";
					IoReadCounter.InstanceName = "_Total";
				}
				return Convert.ToInt32(IoReadCounter.NextValue());
			}
			catch { return -1; }
		}
	}

	PerformanceCounter IoWriteCounter = null;
	public int DiskWriteIORate
	{
		get
		{
			try
			{
				if (IoWriteCounter == null)
				{
					IoWriteCounter = new PerformanceCounter();
					IoWriteCounter.CategoryName = "PhysicalDisk";
					IoWriteCounter.CounterName = "% Disk Write Time";
					IoWriteCounter.InstanceName = "_Total";
				}
				return Convert.ToInt32(IoWriteCounter.NextValue());
			}
			catch { return -1; }
		}
	}

    public IServerCmdReceiver ServerCmdReceiver { get { return serverCmdReceiver; } }
    public ITaskList ServerTaskList { get { return serverTaskList; } }
	public ILogList ServerLogList { get { return serverLogList; } }
	public IDownloadList ServerDownloadList { get { return serverDownloadList; } }
    public IResultDataList ServerResultDataList { get { return serverResultDataList; } }
    public IDataCacheList ServerDataCacheList { get { return serverDataCacheList; } }   
    #endregion

	public void AddNextScreen()
	{
		DigitalSignage.PlayAgent.Task t = new DigitalSignage.PlayAgent.Task();
		t.pid = PlayerId;
		t.gid = GroupId;
		t.type = TaskCommands.CmdNextDefaultProgram;
		t.uuid = DigitalSignage.PlayAgent.Task.TaskDefaultScreen;
		t.TaskStart = TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()) + 1;
		t.TaskEnd = t.TaskStart + 5;
		t.description = "CmdNextDefaultProgram";
		t.duration = 10;

		if (-1 == this.localTasksList.AddTask(t))
		{
			////	추가가 되지않음.
			this.localTasksList.CleanTask(DigitalSignage.PlayAgent.Task.TaskDefaultScreen);
			//	다시 추가 시도
			this.localTasksList.AddTask(t);
		}
	}

	#region 스케줄 동기화 로직 추가 2011.05.09.

	/// <summary>
	/// 스케줄 동기화 Task 추가
	/// </summary>
	/// <param name="taskStart"></param>
	/// <param name="timestamp">스케줄 동기화 기준 시간</param>
	public void AddSynchronizeTasks(long taskStart, long timestamp)
	{
		try
		{
			logger.Info(String.Format("Adding Synchronize Tasks from {0} on {1}", timestamp, taskStart));
			//	스케줄이 존재 하지 않는 경우, 5분 후 Synchronize 스케줄 생성
			DigitalSignage.PlayAgent.Task t = new DigitalSignage.PlayAgent.Task();
			t.type = TaskCommands.CmdSynchronizeNow;
			t.uuid = DigitalSignage.PlayAgent.Task.TaskSynchronize;
			t.pid = this.PlayerId;
			t.gid = this.GroupId;
			t.description = String.Format("CmdSynchronizeNow|{0}", timestamp);
			t.TaskStart = taskStart;
			t.TaskEnd = t.TaskStart + (3600 * 24);	// 하루 동안
			if (-1 == this.localTasksList.AddTask(t))
			{
				////	추가가 되지않음.
				this.localTasksList.CleanTask(DigitalSignage.PlayAgent.Task.TaskSynchronize);
				//	다시 추가 시도
				this.localTasksList.AddTask(t);
			}
		}
		catch (System.Exception ex)
		{
			logger.Error(ex.ToString());
		}
	}

	/// <summary>
	/// 스케줄 동기화 Task 추가
	/// </summary>
	public void AddSynchronizeTasks()
	{
		try
		{
			//	현재 표준 시간
			long curr = TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime());

			DigitalSignage.PlayAgent.Task t = this.localTasksList.GetTaskByUuid(DigitalSignage.PlayAgent.Task.TaskSynchronize.ToString());

			if (t == null)
			{
				logger.Info("SynchronizeTask is null. Will be added.");
				//	30 초 후에 추가 동기화 기준 시간 2달 전 부터
				AddSynchronizeTasks(curr + 30, TimeConverter.ConvertToUTP(DateTime.Now.AddMonths(-2).ToUniversalTime()));
			}
			else
			{
				if (curr > t.TaskEnd || t.pid != this.PlayerId || t.gid != this.GroupId)
				{
					logger.Info("SynchronizeTask was modified or expired. Will be added.");

					////	추가가 되지않음.
					this.localTasksList.CleanTask(DigitalSignage.PlayAgent.Task.TaskSynchronize);
					//	시간이 지났거나 pid, gid가 틀린 경우
					//	30 초 후에 추가 동기화 기준 시간 1달 전 부터
					AddSynchronizeTasks(curr + 30, TimeConverter.ConvertToUTP(DateTime.Now.AddMonths(-1).ToUniversalTime()));
				}
				//	시간이 지나지 않은 경우는 기다림

			}


		}
		catch (Exception e)
		{
			logger.Error(e.ToString() + "");
		}
	}
	#endregion

	/// <summary>
	/// 로그타입 생성 함수
	/// </summary>
	/// <param name="isAd"></param>
	/// <param name="isEvent"></param>
	/// <param name="currState"></param>
	/// <param name="commands"></param>
	/// <param name="nAddtionType"></param>
	/// <returns></returns>
	static public int MakeTaskLogType(bool isAd, bool isEvent, LogType currState, TaskCommands commands, int nAddtionType)
	{
		int nType = nAddtionType;

		if (isAd) nType |= (int)LogType.LogType_Ad;
		if (isEvent) nType |= (int)LogType.LogType_Event;

		nType |= (int)currState;

		switch (commands)
		{
			case TaskCommands.CmdProgram:
				nType |= (int)LogType.LogType_TimeSched;
				break;
			case TaskCommands.CmdUrgentProgram:
				nType |= (int)LogType.LogType_UrgentSched;
				break;
			case TaskCommands.CmdSubtitles:
				nType |= (int)LogType.LogType_SubtitleSched;
				break;
			case TaskCommands.CmdTouchDefaultProgram:
				nType |= (int)LogType.LogType_TouchSched;
				break;
			case TaskCommands.CmdDefaultProgram:
				nType |= (int)LogType.LogType_RollingSched;
				break;
			case TaskCommands.CmdControlSchedule:
				nType |= (int)LogType.LogType_ControlSched;
				break;
            case TaskCommands.CmdEventSchedule:
                nType |= (int)LogType.LogType_EventSched;
                break;
		}

		return nType;
	}

	/// <summary>
	/// 로그타입 생성 함수
	/// </summary>
	/// <param name="category"></param>
	/// <param name="currState"></param>
	/// <param name="nAddtionType"></param>
	/// <returns></returns>
	static public int MakeLogType(LogType category, LogType currState, int nAddtionType)
	{
		int nType = nAddtionType;
		nType |= (int)category;
		nType |= (int)currState;

		return nType;
	}

    #region Target Ad 관련
    /// <summary>
    /// 메타태그 갱신
    /// </summary>
    public void UpdateMetaTagInformation()
    {
        string sSettingPath = AppDomain.CurrentDomain.BaseDirectory + @"settings\";
        string sMetaTagFilePath = sSettingPath + "meta_tags.xml";

        long ts = -10000;
        try
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(sMetaTagFilePath);

            XmlNode node = doc.SelectSingleNode("//child:tag_groups");
            ts = Convert.ToInt64(node.Attributes["ts"].Value);
        }
        catch
        {
        }

        try
        {
            String sReturn = this.serverDataCacheList.GetMetaTags(ts);

            if (!String.IsNullOrEmpty(sReturn))
            {
                XmlDocument doc = new XmlDocument();

                doc.LoadXml(sReturn);

                if (!System.IO.Directory.Exists(sSettingPath))
                    System.IO.Directory.CreateDirectory(sSettingPath);

                doc.Save(sMetaTagFilePath);
                logger.Info("MetaTag Updated!");
            }
        }
        catch { }
    }
    /// <summary>
    /// 마지막 이벤트 요청 정보
    /// </summary>
    private String _LastEventRequest = string.Empty;

    /// <summary>
    /// 마지막 이벤트 요청 정보
    /// </summary>
    public String LastEventRequestInfo
    {
        get { return _LastEventRequest; }
        set { _LastEventRequest = value; }
    }

    /// <summary>
    /// 현재 Target 광고 Relay ID
    /// </summary>
    public long CurrentRelayID = -1;

    static Random rand = new Random(999);
    /// <summary>
    /// Target 광고 ID 생성
    /// </summary>
    /// <returns></returns>
    public long MakeRelayID()
    {
        long lRelayID = DateTime.UtcNow.Ticks;

        try
        {
            String sRelayID = lRelayID.ToString();
            String sRet = rand.Next(100, 999).ToString();
            lRelayID = Convert.ToInt64(sRelayID.Remove(0, 3).Insert(0, sRet));
        }
        catch {}

        return lRelayID;
    }

    /// <summary>
    /// 타겟 광고 성공 태그 등록 함수
    /// </summary>
    /// <param name="relay_id"></param>
    /// <param name="helper">메타태그 모음</param>
    /// <returns></returns>
    public bool WriteTargetAdInfo(long relay_id, MetaTagHelper helper)
    {
        int nRet = -1;
        try
        {
            using (DigitalSignage.ClientDataBase.LogsDataSetTableAdapters.target_ad_infoTableAdapter ta_da = this.logdbLayer.CreateTargetAdInfoDA())
            {
                foreach (String sKey in helper.DataDictionary.Keys)
                {
                    if (0 < ta_da.InsertTargetInfo(relay_id, sKey, Convert.ToString(helper.DataDictionary[sKey])))
                        nRet++;
                }

                ta_da.Dispose();

            }
        }
        catch (Exception exr)
        {
            logger.Error(exr.Message);
        }
        return nRet > 0; 
    }

    /// <summary>
    /// 타겟 광고 성공 로그 등록 함수
    /// </summary>
    /// <param name="relay_id"></param>
    /// <param name="locType">성공한 위치</param>
    /// <returns></returns>
    public bool WriteTargetAdSuccess(long relay_id, TargetAd_ErrorType locType)
    {
        int nRet = -1;
        try
        {
            logger.Info(String.Format("Target Ad Success: RelayID({0}), TargetType ({1})", relay_id, locType));

            using (DigitalSignage.ClientDataBase.LogsDataSetTableAdapters.target_ad_successTableAdapter ta_da = this.logdbLayer.CreateTargetAdSuccessDA())
            {
                nRet = ta_da.InsertTargetSuccessInfo(relay_id, (int)locType, DateTime.UtcNow.Ticks);
                ta_da.Dispose();

            }
        }
        catch (Exception exr)
        {
            logger.Error(exr.Message);
        }
        return nRet > 0;
    }
    /// <summary>
    /// 타겟 광고 에러 로그 등록 함수
    /// </summary>
    /// <param name="relay_id"></param>
    /// <param name="locType">실패한 위치</param>
    /// <param name="Errorcd">위치별 에러 코드</param>
    /// <returns></returns>
    public bool WriteTargetAdError(long relay_id, TargetAd_ErrorType locType, int Errorcd)
    {
        int nRet = -1;
        try
        {
            logger.Error(String.Format("Target Ad Error: RelayID({0}), ErrType({1}) ErrCode ({2})", relay_id, locType, Errorcd));
            using (DigitalSignage.ClientDataBase.LogsDataSetTableAdapters.target_ad_errorTableAdapter ta_da = this.logdbLayer.CreateTargetAdErrorDA())
            {
                nRet = ta_da.InsertTargetErrorInfo(relay_id, (int)locType, Errorcd, DateTime.UtcNow.Ticks);
                ta_da.Dispose();

            }
        }
        catch (Exception exr)
        {
            logger.Error(exr.Message);
        }
        return nRet > 0;
    }

    #endregion

    /// <summary>
    /// 프로모션 연동 정보
    /// </summary>
    /// <param name="screen_id"></param>
    /// <param name="cellphone"></param>
    /// <param name="AddedData"></param>
    /// <returns></returns>
    public bool WritePromotionInfo(long screen_id, String cellphone, String AddedData)
    {
        int nRet = -1;
        try
        {
            logger.Info(String.Format("Promotion Info: screen_id({0}), Cell Phone({1}) Added Data({2})", screen_id, cellphone, AddedData));

            using (DigitalSignage.ClientDataBase.LogsDataSetTableAdapters.promotionsTableAdapter ta_da = this.logdbLayer.CreatePromotionInfoDA())
            {
                nRet = ta_da.InsertPromotion(screen_id, cellphone, AddedData, DateTime.UtcNow.Ticks);
                ta_da.Dispose();
            }
        }
        catch (Exception exr)
        {
            logger.Error(exr.Message);
        }
        return nRet > 0;
    }


    public static void RunProgram(string StartupPath, string ProgramName, string Arguments)
    {
        if (File.Exists(Path.Combine(StartupPath, ProgramName)))
        {
            logger.Info(string.Format("Run Program = [{0}][{1}][{2}]", StartupPath, ProgramName, Arguments));
            System.Diagnostics.ProcessStartInfo psi = new System.Diagnostics.ProcessStartInfo();
            psi.FileName = ProgramName;
            psi.UseShellExecute = true;
            psi.WorkingDirectory = StartupPath;
            psi.Arguments = Arguments;

            System.Diagnostics.Process.Start(psi);
        }
    }

    public static void KillProcess(string processName)
    {
        Process[] ps = System.Diagnostics.Process.GetProcessesByName(processName.Replace(".exe", ""));
        if (ps.Length == 0)
        {
            logger.Info(processName + " not found");
            return;
        }

        ps[0].Kill();
    }

    /// <summary>
    /// VNC 리피터 주소
    /// </summary>
    public string RepeaterPath
    {
        get
        {
            String sRepeaterIP = String.Empty;
            try
            {
                sRepeaterIP = ConfigurationManager.AppSettings["repeater_srv_ip"];
            }
            catch { }

            if (String.IsNullOrEmpty(sRepeaterIP))
            {
                string sPath = serverpath.ToLower().StartsWith("tcp://") ? serverpath.Substring(6) : serverpath;
                return sPath.Split(':')[0];
            }
            else return sRepeaterIP;
        }
    }

}

