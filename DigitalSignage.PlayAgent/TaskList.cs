﻿using System;
using System.Collections;

//SQLite & DB routines
using DigitalSignage.ClientDataBase;
using DigitalSignage.PlayAgent;
using NLog;
using System.Data.SQLite;

// класс для работы с очередью заданий
public class TaskList
{
    private static Logger logger = LogManager.GetCurrentClassLogger();
    private Config cfg = null;

    public TaskList()
    {
        cfg = Config.GetConfig; //protection from initialization dead loop
    }

	public Task[] GetDownloadTasks()
	{
		Task[] result = null;
        try
        {
			DigitalSignage.ClientDataBase.DataSetTableAdapters.tasksTableAdapter da = cfg.dbLayer.CreateTaskDA();
			DataSet.tasksDataTable table = da.GetDownloadTasks(DigitalSignage.Common.TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()));
			if(table != null && table.Rows.Count > 0)
			{
				result = new Task[table.Rows.Count];

				for(int i = 0 ; i < table.Rows.Count ; ++i)
				{
					result[i] = new Task((DataSet.tasksRow)table.Rows[i]);
				}
				da.Dispose();
			}
		}
		catch (Exception e)
		{
			logger.Error(e.Message + "");
		}
		return result;

	}

    // getting all tasks which should be executed in a current moment
	public Task[] GetActiveTask()
    {
		System.Collections.Generic.List<Task> list = new System.Collections.Generic.List<Task>();

        try
        {
			DigitalSignage.ClientDataBase.DataSetTableAdapters.tasksTableAdapter da = cfg.dbLayer.CreateTaskDA();

			String[] arrGroups = cfg.GroupIds.Split('|');
			for (int n = 0; n < arrGroups.Length; ++n)
			{
				string group = arrGroups[n];

				using (DataSet.tasksDataTable table = da.GetData(group, cfg.PlayerId, DigitalSignage.Common.TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime())))
				{
					for (int i = 0; i < table.Rows.Count; i++)
					{
						Task task = new Task((DataSet.tasksRow)table.Rows[i]);
						logger.Debug("New Task: " + task.ToString());

						int type = ((DataSet.tasksRow)table.Rows[i]).type;
						if (type == (int)DigitalSignage.Common.TaskCommands.CmdDefaultProgram ||
							type == (int)DigitalSignage.Common.TaskCommands.CmdTouchDefaultProgram ||
							type == (int)DigitalSignage.Common.TaskCommands.CmdProgram ||
                            type == (int)DigitalSignage.Common.TaskCommands.CmdSubtitles ||
                            type == (int)DigitalSignage.Common.TaskCommands.CmdEventSchedule ||
                            type == (int)DigitalSignage.Common.TaskCommands.CmdUrgentProgram ||
							type == (int)DigitalSignage.Common.TaskCommands.CmdControlSchedule)
						{
							if (task.state == DigitalSignage.Common.TaskState.StateCanceled)
							{
								da.ChangeTaskState((int)DigitalSignage.Common.TaskState.StateProcessed, ((DataSet.tasksRow)table.Rows[i]).uuid);
							}
							else
							{
								da.ChangeTaskState((int)DigitalSignage.Common.TaskState.StateProcessing, ((DataSet.tasksRow)table.Rows[i]).uuid);
							}
						}
						else
							da.ChangeTaskState((int)DigitalSignage.Common.TaskState.StateProcessed, ((DataSet.tasksRow)table.Rows[i]).uuid);

						list.Add(task);
					}
				}
			}
			da.Dispose();

        }
        catch (Exception e)
        {
			logger.Error(e.Message + "");
        }
		return list.ToArray();
    }
	// add new task to list
	public int AddTaskWithState(Task newTask)
	{
		try
		{
			DigitalSignage.ClientDataBase.DataSetTableAdapters.tasksTableAdapter da = cfg.dbLayer.CreateTaskDA();
			int res = da.AddTaskWithState(newTask.pid, newTask.gid, (int)newTask.type, (int)newTask.state, newTask.uuid.ToString(),
				newTask.TaskStart, newTask.TaskEnd, newTask.guuid.ToString(), newTask.duuid.ToString(),
				newTask.description, (int)newTask.duration, newTask.daysofweek, newTask.priority, newTask.endtimeofday, newTask.starttimeofday, newTask.show_flag, newTask.MetaTags, newTask.isAd ? "Y" : "N");
			DataSet.tasksDataTable table = da.GetAllTasks();
			res = table.Rows.Count;
			res++;

			da.Dispose();

			return res;
		}
		catch (Exception e)
		{
			//	중복되었을경우 삭제하고 다시 저장하기때문에 로그에 남기지 않는다.
			if (false == e.Message.StartsWith("Abort due to constraint violation"))
			{
				logger.Error(e.Message);
			}
		}
		return -1;
	}

    // add new task to list
    public int AddTask(Task newTask)
    {
        try
        {
			DigitalSignage.ClientDataBase.DataSetTableAdapters.tasksTableAdapter da = cfg.dbLayer.CreateTaskDA();
            int res = da.AddTask(newTask.pid, newTask.gid, (int)newTask.type, newTask.uuid.ToString(), 
                newTask.TaskStart, newTask.TaskEnd, newTask.guuid.ToString(), newTask.duuid.ToString(),
				newTask.description, (int)newTask.duration, newTask.daysofweek, newTask.priority, newTask.endtimeofday, newTask.starttimeofday, newTask.show_flag, newTask.MetaTags, newTask.isAd ? "Y" : "N");
            DataSet.tasksDataTable table = da.GetAllTasks();
            res = table.Rows.Count;
            res++;
			
			da.Dispose();

			return res;
        }
        catch (Exception e)
        {
			//	중복되었을경우 삭제하고 다시 저장하기때문에 로그에 남기지 않는다.
			if (false == e.Message.StartsWith("Abort due to constraint violation"))
			{
				logger.Error(e.Message);
			}
        }
        return -1;
    }
	public int CleanAllTasks()
	{
		try
		{
			DigitalSignage.ClientDataBase.DataSetTableAdapters.tasksTableAdapter da = cfg.dbLayer.CreateTaskDA();
			int res = da.CleanTasks();
			da.Dispose();

			return res;
		}
		catch (Exception e)
		{
			logger.Error(e.Message + "");
		}

		return -1;
	}
	public int ChangeTaskState(DigitalSignage.Common.TaskState newState, Guid taskUuid)
    {
        try
        {
			DigitalSignage.ClientDataBase.DataSetTableAdapters.tasksTableAdapter da = cfg.dbLayer.CreateTaskDA();
            da.ChangeTaskState((int)newState, taskUuid.ToString() );
			da.Dispose();
			return 0;
        }
        catch (Exception e)
        {
			logger.Error(e.Message + "");
        }
        return -1;
    }

    public int ResetRunningTasks()
    {
        try
        {
			DigitalSignage.ClientDataBase.DataSetTableAdapters.tasksTableAdapter da = cfg.dbLayer.CreateTaskDA();
            da.ResetRunning();
			da.Dispose();
			return 0;
        }
        catch (Exception e)
        {
			logger.Error(e.Message + "");
        }
        return -1;
    }

    // getting all tasks
    public Task[] GetAllTasks()
    {
        Task[] result = null;
		DigitalSignage.ClientDataBase.DataSetTableAdapters.tasksTableAdapter da = cfg.dbLayer.CreateTaskDA();
        DataSet.tasksDataTable table = da.GetAllTasks();
        result = new Task[table.Rows.Count];
        for (int i = 0; i < table.Rows.Count; i++)
            result[i] = new Task((DataSet.tasksRow)table.Rows[i]);
		da.Dispose();
		return result;
    }

    // getting finished tasks
    public Task[] GetFinishedTasks()
    {
        Task[] result = null;
		DigitalSignage.ClientDataBase.DataSetTableAdapters.tasksTableAdapter da = cfg.dbLayer.CreateTaskDA();
		DataSet.tasksDataTable table = da.GetFinishedTasks(DigitalSignage.Common.TimeConverter.ConvertToUTP(DateTime.UtcNow.AddHours(-1)));
        result = new Task[table.Rows.Count];
		for (int i = 0; i < table.Rows.Count; i++)
		{
			result[i] = new Task((DataSet.tasksRow)table.Rows[i]);
		}
		da.Dispose();
		return result;
    }

	public DigitalSignage.PlayAgent.Task GetTaskByUuid(string uuid)
	{
		DigitalSignage.PlayAgent.Task task = null;
		try
		{
			using (DigitalSignage.ClientDataBase.DataSetTableAdapters.tasksTableAdapter da = cfg.dbLayer.CreateTaskDA())
			{
				using (DataSet.tasksDataTable table = da.GetTaskByuuid(uuid))
				{
					if (table.Rows != null && table.Rows.Count > 0)
					{
						task = new DigitalSignage.PlayAgent.Task(table.Rows[0] as DigitalSignage.ClientDataBase.DataSet.tasksRow);
					}
					table.Dispose();
					da.Dispose();
				}
			}
		}
		catch (Exception e)
		{
			logger.Error(e.Message + "");
		}
		catch
		{
			logger.Error("Unhandled Error occurred.");
		}

		return task;
	}


	public bool CanDeleteTask(string duuid)
	{
		DigitalSignage.ClientDataBase.DataSetTableAdapters.tasksTableAdapter da = cfg.dbLayer.CreateTaskDA();
		Object obj = da.GetAliveTasksFromDuuid(duuid);

		return (0 == Convert.ToInt64(obj));
	}

    public void CleanTask(Task task)
    {
		DigitalSignage.ClientDataBase.DataSetTableAdapters.tasksTableAdapter da = cfg.dbLayer.CreateTaskDA();
        da.CleanTask(task.uuid.ToString() );
		da.Dispose();
	}
	public void CleanTask(Guid uuid)
	{
		DigitalSignage.ClientDataBase.DataSetTableAdapters.tasksTableAdapter da = cfg.dbLayer.CreateTaskDA();
		da.CleanTask(uuid.ToString());
		da.Dispose();
	}
	public int GetTaskState(Guid uuid)
	{
		try
		{
			DigitalSignage.ClientDataBase.DataSetTableAdapters.tasksTableAdapter da = cfg.dbLayer.CreateTaskDA();
			DataSet.tasksDataTable table = da.GetTaskByuuid(uuid.ToString());
			da.Dispose();

			if (table.Rows.Count > 0)
			{
				return ((DataSet.tasksRow)table.Rows[0]).state;
			}
		}
		catch (Exception e)
		{
			logger.Error(e.ToString());
		}
		return 0;	//	Wait
	}

    /// <summary>
    /// 업데이트된 동기화 스케줄이 있는지 검사
    /// </summary>
    /// <param name="yyyyMMddHHmmss"></param>
    /// <returns></returns>
    public bool HasUpdatedSyncTask(String yyyyMMddHHmmss)
    {
        String sQuery = String.Empty;
        int? nCount = 0;

        try
        {
            sQuery = String.Format("SELECT COUNT(*) FROM SYNC_TASKS WHERE EDIT_DT > '{0}'", yyyyMMddHHmmss);

            using (SQLiteCommand sql = new SQLiteCommand(sQuery, cfg.dbLayer.Connection))
            {
                nCount = Convert.ToInt32(sql.ExecuteScalar());
                sql.Dispose();
            }
        }
        catch (Exception e)
        {
            logger.Error(e.Message + "");
        }

        return nCount > 0;
 
    }

    /// <summary>
    /// 싱크 스케줄 삭제
    /// </summary>
    public void DeleteSyncTaskList()
    {
        String sQuery = String.Empty;

        try
        {
            sQuery = String.Format("DELETE FROM SYNC_TASKS WHERE DEL_YN = 'Y'");

            using (SQLiteCommand sql = new SQLiteCommand(sQuery, cfg.dbLayer.Connection))
            {
                sql.ExecuteNonQuery();
                sql.Dispose();
            }
        }
        catch (Exception e)
        {
            logger.Error(e.Message + "");
        }
    }

    /// <summary>
    /// 싱크 스케줄 삭제
    /// </summary>
    public void CleanAllSyncTaskList()
    {
        String sQuery = String.Empty;

        try
        {
            sQuery = String.Format("DELETE FROM SYNC_TASKS");

            using (SQLiteCommand sql = new SQLiteCommand(sQuery, cfg.dbLayer.Connection))
            {
                sql.ExecuteNonQuery();
                sql.Dispose();
            }
        }
        catch (Exception e)
        {
            logger.Error(e.Message + "");
        }
    }

    /// <summary>
    /// 동기화 스케줄 가져오기
    /// </summary>
    /// <returns></returns>
    public DigitalSignage.ClientDataBase.DataSet.SYNC_TASKSDataTable GetSyncTaskList()
    {
        String sQuery = String.Empty;

        try
        {
            sQuery = String.Format("SELECT GUUID, PLAYERS_PID, SYNC_TP, EDIT_DT, REG_DT, IP_ADDR, DEL_YN FROM SYNC_TASKS");

            using (DigitalSignage.ClientDataBase.DataSet.SYNC_TASKSDataTable dtSync = new DataSet.SYNC_TASKSDataTable())
            { 
                using (SQLiteCommand sql = new SQLiteCommand(sQuery, cfg.dbLayer.Connection))
                {
                    dtSync.Load(sql.ExecuteReader());
                    sql.Dispose();
                }

                return dtSync;
            }
        }
        catch (Exception e)
        {
            logger.Error(e.Message + "");
        }

        return null;
    }

    /// <summary>
    /// 동기화 스케줄 생성
    /// </summary>
    /// <param name="row"></param>
    /// <returns></returns>
    public bool InsertOrReplaceSyncTask(DigitalSignage.ServerDatabase.ServerDataSet.SYNC_TASKSRow row)
    {
        String sQuery = String.Empty;

        try
        {
            System.Threading.Thread.Sleep(1);

            String sLocalNowDT = DateTime.Now.ToString("yyyyMMddHHmmss");


            using (SQLiteTransaction tran = cfg.dbLayer.Connection.BeginTransaction())
            {
                try
                {
                    sQuery = String.Format("DELETE FROM SYNC_TASKS WHERE GUUID = '{0}' AND PLAYERS_PID = '{1}'", row.GUUID, row.PLAYERS_PID);

                    using (SQLiteCommand sql = new SQLiteCommand(sQuery, cfg.dbLayer.Connection, tran))
                    {
                        sql.ExecuteNonQuery();
                        sql.Dispose();
                    }

                    sQuery = String.Format("INSERT INTO SYNC_TASKS (GUUID, PLAYERS_PID, SYNC_TP, EDIT_DT, REG_DT, IP_ADDR, DEL_YN) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}')",
                        row.GUUID, row.PLAYERS_PID, row.SYNC_TP, row.EDIT_DT, row.REG_DT, row.IP_ADDR, row.DEL_YN);


                    using (SQLiteCommand sql = new SQLiteCommand(sQuery, cfg.dbLayer.Connection, tran))
                    {
                        sql.ExecuteNonQuery();
                        sql.Dispose();
                    }

                    tran.Commit();
                    return true;
                }
                catch
                {
                    tran.Rollback();
                }

                tran.Dispose();
            }
        }
        catch (Exception e)
        {
            logger.Error(e.Message + "");
        }
        return false;
    }
}

