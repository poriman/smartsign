﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

using DigitalSignage.ClientDataBase;
using DigitalSignage.Common;

using NLog;

namespace DigitalSignage.PlayAgent
{
	// task description class
	[Serializable]
	public class Task : ICloneable
	{
		private static Logger logger = LogManager.GetCurrentClassLogger();
		public static Guid TaskSynchronize = new Guid("3d9f7109-6012-447b-aef7-d27abcc9252c");
		public static Guid TaskClean = new Guid("011EE3AE-13C7-49cf-AC80-E22FCAEFB61F");
		public static Guid TaskInitPlayer = new Guid("9dbf5729-69ab-4ab1-bf65-242d800d76fc");
		public static Guid TaskReturnStatus = new Guid("cedf49f4-7c2d-4064-a2d0-4307ad245f21");
		public static Guid TaskDefaultScreen = new Guid("dd4018cd-e598-4f8d-ad02-0c574c817672");
		public static Guid TaskRefData = new Guid("14B68BA3-721A-4429-AA28-F0FD1A8C4CB1");

		/// <summary>
		/// 이벤트 재생 모드 추가
		/// </summary>
		public bool IsEventPlay = false;

		public DigitalSignage.Common.TaskCommands type;
		// player identificator
		public string pid;
		public string gid;
		// task identificator
		public System.Guid uuid;
		// task group id
		public System.Guid guuid;
		// directory id
		public System.Guid duuid;
		public long TaskStart;
		public long TaskEnd;
		public DigitalSignage.Common.TaskState state;
		public string description = "Unnamed Task";
		public long duration;
		public int daysofweek;
		public int starttimeofday;
		public int endtimeofday;
		public int priority;

		#region 광고 관련 추가

		/// <summary>
		/// 광고 여부
		/// </summary>
		public bool isAd = false;

		/// <summary>
		/// 표출 플래그
		/// </summary>
		public int show_flag = (int)ShowFlag.Show_NonEvent;

		/// <summary>
		/// 메타 태그 정보
		/// </summary>
		public string MetaTags = String.Empty;

		#endregion

		public Task()
		{
		}

		public object Clone()
		{
			Task cloneTask = new Task();
			cloneTask.type = type;
			cloneTask.pid = pid == null ? null :pid.Trim();
            cloneTask.gid = gid == null ? null : gid.Trim();
			cloneTask.uuid = uuid;
			cloneTask.TaskStart = TaskStart;
			cloneTask.TaskEnd = TaskEnd;
			cloneTask.state = state;
			cloneTask.guuid = guuid;
			cloneTask.duuid = duuid;
			cloneTask.description = description;
			cloneTask.duration = duration;
			cloneTask.daysofweek = daysofweek;
			cloneTask.starttimeofday = starttimeofday;
			cloneTask.endtimeofday = endtimeofday;
			cloneTask.priority = priority;

			cloneTask.isAd = isAd;
			cloneTask.MetaTags = MetaTags;
			cloneTask.show_flag = show_flag;

			return cloneTask;
		}

		/// <summary>
		/// 중복이 허용되는지
		/// </summary>
		public bool AllowDuplication
		{
			get
			{
				return ((show_flag & (int)ShowFlag.Show_Allow_Duplication) == (int)ShowFlag.Show_Allow_Duplication);
			}
		}

        public bool IsSyncMaster
        {
            get
            {
                return (show_flag & (int)ShowFlag.Show_SyncMaster) > 0; 
            }
        }

        public bool IsSyncSlave
        {
            get
            {
                return (show_flag & (int)ShowFlag.Show_SyncSlave) > 0;
            }
        }

        public bool IsSyncTask
        {
            get
            {
                return IsSyncMaster || IsSyncSlave;
            }
        }


		public Task(DigitalSignage.ServerDatabase.ServerDataSet.tasksRow row)
		{
			this.type = (DigitalSignage.Common.TaskCommands)row.type;
            this.pid = row.pid == null ? null : row.pid.Trim();
            this.gid = row.gid == null ? null : row.gid.Trim();
			this.uuid = new Guid(row.uuid);
			this.TaskStart = row.starttime;
			this.TaskEnd = row.endtime;
			this.state = (DigitalSignage.Common.TaskState)row.state;
			if (!row.guuid.Equals(""))
				this.guuid = new Guid(row.guuid);
			this.duuid = new Guid(row.duuid);
			this.description = row.name;
			this.duration = row.duration;
			this.daysofweek = row.daysofweek;
			this.starttimeofday = row.starttimeofday;
			this.endtimeofday = row.endtimeofday;
			this.priority = row.priority;

			this.isAd = row.is_ad == "Y";
			this.MetaTags = row.meta_tag;
			this.show_flag = row.show_flag;
		}
// 		public Task(DigitalSignage.DataBase.DataSet.tasksRow row)
// 		{
// 			this.type = (DigitalSignage.Common.TaskCommands)row.type;
// 			this.pid = row.pid;
// 			this.gid = row.gid;
// 			this.uuid = new Guid(row.uuid);
// 			this.TaskStart = row.starttime;
// 			this.TaskEnd = row.endtime;
// 			this.state = (DigitalSignage.Common.TaskState)row.state;
// 			logger.Info("New task init " + row.guuid + ":" + row.duuid);
// 			if (!row.guuid.Equals(""))
// 				this.guuid = new Guid(row.guuid);
// 			this.duuid = new Guid(row.duuid);
// 			this.description = row.name;
// 			this.duration = row.duration;
// 			this.daysofweek = row.daysofweek;
// 			this.starttimeofday = row.starttimeofday;
// 			this.endtimeofday = row.endtimeofday;
// 			this.priority = row.priority;
// 		}

		public Task(DataSet.tasksRow row)
		{
			this.type = (DigitalSignage.Common.TaskCommands)row.type;
            this.pid = row.pid == null ? null : row.pid.Trim();
            this.gid = row.gid == null ? null : row.gid.Trim();
			this.uuid = new Guid(row.uuid);
			this.TaskStart = row.starttime;
			this.TaskEnd = row.endtime;
			this.state = (DigitalSignage.Common.TaskState)row.state;
			if (!row.guuid.Equals(""))
				this.guuid = new Guid(row.guuid);
			this.duuid = new Guid(row.duuid);
			this.description = row.name;
			this.duration = row.duration;
			this.daysofweek = row.daysofweek;
			this.starttimeofday = row.starttimeofday;
			this.endtimeofday = row.endtimeofday;
			this.priority = row.priority;

			try
			{
				this.isAd = row.is_ad == "Y";
				this.MetaTags = row.meta_tag;
				this.show_flag = Convert.ToInt32(row.show_flag);
			}
			catch
			{
				this.isAd = false;
				this.MetaTags = String.Empty;
				this.show_flag = 127;
			}
		}

		public new String ToString()
		{
			return String.Format("Name: {0}, Group ID: {1}, Player ID: {2}, Unique ID: {3}, Download ID: {4}, State: {5}, Start: {6}, End: {7}, IsAdSchedule: {8}, MetaTags: {9}",
				description, gid, string.IsNullOrEmpty(pid) ? "null" : pid, uuid.ToString(), duuid.ToString(), state.ToString(), TimeConverter.ConvertFromUTP(TaskStart).ToLocalTime(), TimeConverter.ConvertFromUTP(TaskEnd).ToLocalTime(), isAd, MetaTags);
// 			return "Name=\"" + description + "\" Player=" + pid + "\" Group=" + gid + "\ntask id=" +
// 				uuid.ToString() + " Start=" + TaskStart + " End=" + TaskEnd +
// 				" State=" + (int)state + " duuid=" + duuid.ToString() + " duration=" + duration.ToString();
			// 			return "Name=\"" + description + "\"\r\nPlayer=" + pid + "\"\r\nGroup=\"" + gid + "\"\r\nStart=" + TimeConverter.ConvertFromUTP(TaskStart) + "\r\nEnd=" + TimeConverter.ConvertFromUTP(TaskEnd);
		}

		public void ToRow(ref DataSet.tasksRow result)
		{
			result.duuid = duuid.ToString();
			result.endtime = TaskEnd;
			result.guuid = guuid.ToString();
            result.pid = pid == null ? null : pid.Trim();
            result.gid = gid == null ? null : gid.Trim();
			result.starttime = TaskStart;
			result.state = (int)state;
			result.type = (int)type;
			result.uuid = uuid.ToString();
			result.duration = (int)duration;
			result.daysofweek = daysofweek;
			result.priority = priority;
			result.starttimeofday = starttimeofday;
			result.endtimeofday = endtimeofday;
		}
	}
}
