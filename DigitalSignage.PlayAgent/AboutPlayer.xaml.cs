﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;
using System.Reflection;
using DigitalSignage.Common;

namespace DigitalSignage.PlayAgent
{
    /// <summary>
    /// Interaction logic for ContentCopying.xaml
    /// </summary>
    public partial class AboutPlayer : Window
    {
		public AboutPlayer()
        {
            InitializeComponent();

			FillInformation();
        }

		private void FillInformation()
		{
			try
			{
				Assembly myAssembly = Assembly.GetExecutingAssembly();
				AssemblyName myAssemblyName = myAssembly.GetName();
				labelVersion.Text = String.Format("v{0}.{1}.{2}.{3}", myAssemblyName.Version.Major, myAssemblyName.Version.Minor, myAssemblyName.Version.Build, myAssemblyName.Version.Revision);

				tbProductCode.Text = SerialKey.SerialKey.ProductName(Config.GetConfig.ProductCode);
			}
			catch
			{
				labelVersion.Text = "v0.0.0.0";
			}
			try
			{
				TrialChecker checker = new TrialChecker();
				if (checker.IsTrial)
				{
					panelTrial.Visibility = Visibility.Visible;
					tbLeft.Text = checker.LeftDays.ToString();
					return;
				}
			}
			catch
			{
			}
			panelTrial.Visibility = Visibility.Collapsed;

		}

		private void OkButton_Click(object sender, RoutedEventArgs e)
		{
			this.DialogResult = true;
		}
	}
}