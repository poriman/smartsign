﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
// logging routines
using NLog;

using DigitalSignage.Common;
using System.Threading;
using System.Windows.Threading;
using DigitalSignage.PlayAgent.Library;
using System.Xml;

namespace DigitalSignage.PlayAgent
{
	class WCFAuthoring : INetworkServiceCallback
	{
		private static Logger logger = LogManager.GetCurrentClassLogger();
		private Config cfg = null;
 		private static readonly object semaphore = new object();
		private string SAAS_DOMAIN = "saas.myivision.com";

		public event StateChangedEventHandler StateChanged = null;
		public event LastErrorMessageUpdatedEventHandler LastErrorMessageUpdated = null;

        public bool IsExit = false;

		#region WCF Connect
		NetworkServiceClient proxy = null;
		IVisionService.Topic clientInfo = null;

		private static WCFAuthoring instance = null;

		private string _lastErrorMessage = "";

		public String LastErrorMessage
		{
			get { return _lastErrorMessage; }
		}

		#region Reconnect Timer 
		Timer tmConnect = null;

		public void Reconnect(Object state)
		{
            try
            {
                this.Connect();
            }
            catch (System.Exception e)
            {
                logger.Error(e.ToString());
            }
// 			if (tmConnect != null)
// 			{
// 				tmConnect.Dispose();
// 				tmConnect = null;
// 			}

		}
		#endregion 

		public static WCFAuthoring GetWCFObject
		{
			get
			{
				if (instance == null)
				{
					instance = new WCFAuthoring();
					instance.Init();
				}
				return instance;
			}
		}

		public WCFAuthoring()
		{
			cfg = Config.GetConfig;
		}
		public void Init()
		{
			//	SE인 경우 자신의 위치로 구성한다.
			if(cfg.ProductCode == DigitalSignage.SerialKey.SerialKey._ProductCode.StandaloneEdition)
			{
				cfg.WCFEndpoint = "net.tcp://127.0.0.1:40526/INetworkHost/tcp";
				cfg.ServerPathFromFile = "127.0.0.1:888";
			}
			else if (cfg.ProductCode == DigitalSignage.SerialKey.SerialKey._ProductCode.SoftwareAsaService)
			{
				cfg.WCFEndpoint = String.Format("net.tcp://{0}:40526/INetworkHost/tcp", SAAS_DOMAIN);
				cfg.ServerPathFromFile = SAAS_DOMAIN + ":888";
			}

			//최초 시작 시 15초 후에 접속을 시도한다
			TimerCallback call = new TimerCallback(this.Reconnect);
			tmConnect = new Timer(call, null, 15 * 1000, 2 * 60 * 1000);
		}
		public bool ConnectUsingSuspend()
		{
			try
			{
				if (proxy != null)
					Disconnect();

				String address = cfg.ServerPath.Replace("tcp://", "");
				address = address.Replace("/", "");
				address = address.Replace(":888", "");

				logger.Info("Connecting WCF Endpoint : " + address);

				bool bRet = false;

				lock (semaphore)
				{
					EndpointAddress epAddress = new EndpointAddress("net.tcp://" + address + ":40526/INetworkHost/tcp");

					InstanceContext context = new InstanceContext(this);
					proxy = new NetworkServiceClient(context, "", epAddress);

					clientInfo = new IVisionService.Topic();
					clientInfo.Groupid = cfg.GroupId;
					clientInfo.Mediaid = cfg.PlayerId;
					clientInfo.Level = IVisionService.UserLevels.Media;

					bRet = proxy.Connect(this.clientInfo);

					if (LastErrorMessageUpdated != null) LastErrorMessageUpdated(this, new LastErrorMessageUpdatedEventArgs(ErrorCodes.IS_ERROR_BASE, "None"));
					if (StateChanged != null) StateChanged(this, new StateChangedEventArgs(false, proxy.State));
 				}
				OnEndJoin(null);

				return bRet;
			}
            catch (EndpointNotFoundException epnfe)
            {
                _lastErrorMessage = Properties.Resources.errorDisconnectedBy + Properties.Resources.mbErrorEndPointNotFound;
                if (LastErrorMessageUpdated != null) LastErrorMessageUpdated(this, new LastErrorMessageUpdatedEventArgs(ErrorCodes.IS_ERROR_CONN_SESSION_FAIL, _lastErrorMessage));
                if (StateChanged != null) StateChanged(this, new StateChangedEventArgs(false, CommunicationState.Faulted));
                logger.Error(epnfe.Message);
            }
            catch (UriFormatException ufe)
            {
                _lastErrorMessage = Properties.Resources.errorDisconnectedBy + Properties.Resources.mbErrorUriFormat;
                if (LastErrorMessageUpdated != null) LastErrorMessageUpdated(this, new LastErrorMessageUpdatedEventArgs(ErrorCodes.IS_ERROR_CONN_SESSION_FAIL, _lastErrorMessage));
                if (StateChanged != null) StateChanged(this, new StateChangedEventArgs(false, CommunicationState.Faulted));
                logger.Error(ufe.Message);
            }
            catch(ThreadAbortException tae)
            {
                logger.Error(tae.Message);
            }
            catch (NullReferenceException nre)
            {
                logger.Error(nre.Message);
            }
            catch (CommunicationObjectAbortedException coae)
            {
                _lastErrorMessage = Properties.Resources.errorDisconnectedBy + coae.Message;
                if (LastErrorMessageUpdated != null) LastErrorMessageUpdated(this, new LastErrorMessageUpdatedEventArgs(ErrorCodes.IS_ERROR_CONN_SESSION_FAIL, _lastErrorMessage));
                if (StateChanged != null) StateChanged(this, new StateChangedEventArgs(false, CommunicationState.Faulted));
                logger.Error(coae.Message);
            }
            catch (CommunicationObjectFaultedException cofe)
            {
                _lastErrorMessage = Properties.Resources.errorDisconnectedBy + cofe.Message;
                if (LastErrorMessageUpdated != null) LastErrorMessageUpdated(this, new LastErrorMessageUpdatedEventArgs(ErrorCodes.IS_ERROR_CONN_SESSION_FAIL, _lastErrorMessage));
                if (StateChanged != null) StateChanged(this, new StateChangedEventArgs(false, CommunicationState.Faulted));
                logger.Error(cofe.Message);
            }
            catch (Exception ex)
            {
                logger.Error(ex.GetType().ToString());

                _lastErrorMessage = Properties.Resources.errorDisconnectedBy + ex.Message;
                if (LastErrorMessageUpdated != null) LastErrorMessageUpdated(this, new LastErrorMessageUpdatedEventArgs(ErrorCodes.IS_ERROR_CONN_SESSION_FAIL, _lastErrorMessage));
                if (StateChanged != null) StateChanged(this, new StateChangedEventArgs(false, CommunicationState.Faulted));
                logger.Error(ex.Message);
            }
			return false;
		}
		public void Connect()
		{
			if (proxy == null || proxy.State != CommunicationState.Opened)
			{
				try
				{
					if (proxy != null)
						Disconnect();


					String address = cfg.ServerPath.Replace("tcp://", "");
					address = address.Replace("/", "");
					address = address.Replace(":888", "");

					logger.Info("Connecting WCF Endpoint : " + address);

					lock (semaphore)
					{
						EndpointAddress epAddress = new EndpointAddress("net.tcp://" + address + ":40526/INetworkHost/tcp");

						InstanceContext context = new InstanceContext(this);

						NetworkServiceClient nService = proxy = new NetworkServiceClient(context, "", epAddress);

						clientInfo = new IVisionService.Topic();
						clientInfo.Groupid = cfg.GroupId;
						clientInfo.Mediaid = cfg.PlayerId;
						clientInfo.Level = IVisionService.UserLevels.Media;

						IAsyncResult iar = nService.BeginConnect(this.clientInfo, new AsyncCallback(OnEndJoin), null);

						long lSleepCount = 0;
						while (!iar.IsCompleted)
						{
							if (IsExit)
							{
								logger.Info("PlayAgent will be terminated.");
								Environment.Exit(0);
							}
							System.Threading.Thread.Sleep(1000);
							lSleepCount += 1000;

							if (lSleepCount > 300000 /*5분*/)
							{
								// 자기 프로세스 종료
								logger.Error("WCF Connect has not returned.. PlayAgent will be terminated.");
								Environment.Exit(0);
							}
						}
						nService.EndConnect(iar);

						if (LastErrorMessageUpdated != null) LastErrorMessageUpdated(this, new LastErrorMessageUpdatedEventArgs(ErrorCodes.IS_ERROR_BASE, "None"));
						if (StateChanged != null) StateChanged(this, new StateChangedEventArgs(false, proxy.State));
					}
				}
				catch (EndpointNotFoundException epnfe)
				{
					_lastErrorMessage = Properties.Resources.errorDisconnectedBy + Properties.Resources.mbErrorEndPointNotFound;
					if (LastErrorMessageUpdated != null) LastErrorMessageUpdated(this, new LastErrorMessageUpdatedEventArgs(ErrorCodes.IS_ERROR_CONN_SESSION_FAIL, _lastErrorMessage));
					if (StateChanged != null) StateChanged(this, new StateChangedEventArgs(false, CommunicationState.Faulted));
					logger.Error(epnfe.Message);

					RetryTask();

				}
				catch (ThreadAbortException tae)
				{
					logger.Error(tae.Message);

					RetryTask();
				}
				catch (NullReferenceException nre)
				{
					logger.Error(nre.ToString());

					RetryTask();
				}
				catch (UriFormatException ufe)
				{
					_lastErrorMessage = Properties.Resources.errorDisconnectedBy + Properties.Resources.mbErrorUriFormat;
					if (LastErrorMessageUpdated != null) LastErrorMessageUpdated(this, new LastErrorMessageUpdatedEventArgs(ErrorCodes.IS_ERROR_CONN_SESSION_FAIL, _lastErrorMessage));
					if (StateChanged != null) StateChanged(this, new StateChangedEventArgs(false, CommunicationState.Faulted));
					logger.Error(ufe.Message);

					RetryTask();
				}
				catch (CommunicationObjectFaultedException cofe)
				{
					_lastErrorMessage = Properties.Resources.errorDisconnectedBy + cofe.Message;
					if (LastErrorMessageUpdated != null) LastErrorMessageUpdated(this, new LastErrorMessageUpdatedEventArgs(ErrorCodes.IS_ERROR_CONN_SESSION_FAIL, _lastErrorMessage));
					if (StateChanged != null) StateChanged(this, new StateChangedEventArgs(false, CommunicationState.Faulted));
					logger.Error(cofe.Message);

					RetryTask();
				}
				catch (CommunicationObjectAbortedException coae)
				{
					_lastErrorMessage = Properties.Resources.errorDisconnectedBy + coae.Message;
					if (LastErrorMessageUpdated != null) LastErrorMessageUpdated(this, new LastErrorMessageUpdatedEventArgs(ErrorCodes.IS_ERROR_CONN_SESSION_FAIL, _lastErrorMessage));
					if (StateChanged != null) StateChanged(this, new StateChangedEventArgs(false, CommunicationState.Faulted));
					logger.Error(coae.Message);

					RetryTask();
				}
				catch (Exception ex)
				{
					logger.Error(ex.GetType().ToString());

					_lastErrorMessage = Properties.Resources.errorDisconnectedBy + ex.Message;
					if (LastErrorMessageUpdated != null) LastErrorMessageUpdated(this, new LastErrorMessageUpdatedEventArgs(ErrorCodes.IS_ERROR_CONN_SESSION_FAIL, _lastErrorMessage));
					if (StateChanged != null) StateChanged(this, new StateChangedEventArgs(false, CommunicationState.Faulted));
					logger.Error(ex.Message);

					RetryTask();
				}
				catch
				{
					_lastErrorMessage = "Unhandled Exception Occurred.";
					if (LastErrorMessageUpdated != null) LastErrorMessageUpdated(this, new LastErrorMessageUpdatedEventArgs(ErrorCodes.IS_ERROR_CONN_SESSION_FAIL, _lastErrorMessage));
					if (StateChanged != null) StateChanged(this, new StateChangedEventArgs(false, CommunicationState.Faulted));
					logger.Error(_lastErrorMessage);

					RetryTask();
				}

			}

		}
		public bool IsAuthored()
		{
			return proxy != null && cfg.WCFAuthoring;
		}

		public void RetryTask()
		{
			try
			{
                logger.Info("RetryTask is called!");

				Disconnect();
				cfg.WCFAuthoring = false;

				//////////////////////////////////////////////////////////////////////////
				//	타이머가 여러갱 생성되지 않기 위한 보호
// 				lock (semaphore)
// 				{
					if (tmConnect == null)
					{
						TimerCallback call = new TimerCallback(this.Reconnect);
						tmConnect = new Timer(call, null, 2 * 60 * 1000, 2 * 60 * 1000);
					
						logger.Info("Will be re-connect after 2 minutes.");
					}

// 				}

			}
			catch(Exception e)
			{
				logger.Error(e.Message);
			}

		}

		public void Disconnect()
		{
			try
			{
				if (proxy != null)
				{

					if (proxy.State == CommunicationState.Opened)
					{
						clientInfo = new IVisionService.Topic();
						clientInfo.Groupid = cfg.GroupId;
						clientInfo.Mediaid = cfg.PlayerId;
						clientInfo.Level = IVisionService.UserLevels.Media;

// 					lock (semaphore) 
// 					{
						proxy.Disconnect(this.clientInfo);
// 					}   
						if (StateChanged != null) StateChanged(this, new StateChangedEventArgs(false, proxy.State));
						logger.Info("Disconnected");
					}

				}

			}
			catch (Exception e)
			{
				logger.Error(e.ToString());
			}
			finally
			{
				try
				{
					proxy.Close();

				}
				catch { }
				proxy = null;
				cfg.WCFAuthoring = false;
			}
		}

		private void OnEndJoin(IAsyncResult iar)
		{
			try
			{
                logger.Info("OnEndJoin called");

				if (proxy != null)
				{
					if (proxy.State != CommunicationState.Opened)
					{
						RetryTask();
					}
					else
					{

						//	성공
						if (IsAuthored())
						{
							if (cfg.connectToServer() == 0)
							{
								cfg.WCFAuthoring = true;
								AddInitializeTasks();

								//	서버에게 플레이어의 자세한 정보를 보낸다.
								SendDetailPlayerInfo();

								if (StateChanged != null) StateChanged(this, new StateChangedEventArgs(true, proxy.State));

							}
							else
							{
								_lastErrorMessage = cfg.LastErrorMessage;
								if (LastErrorMessageUpdated != null) LastErrorMessageUpdated(this, new LastErrorMessageUpdatedEventArgs(ErrorCodes.IS_ERROR_CONN_SESSION_FAIL, _lastErrorMessage));

								logger.Warn("The [ConnectToServer] is failed.");

								RetryTask();
							}
						}
						else
						{
							logger.Warn("The [Authoring] is failed.");

							RetryTask();
						}
					}
				}
				else
				{
					logger.Warn("The [proxy] is NULL.");

					// 다시 접속
					RetryTask();
				}
			}
			catch (Exception e)
			{
				logger.Error(e.Message);
				RetryTask();
			}
		}
		#endregion

		#region Send Status
		//	다운로드할 Task의 총 개수
		private int statTotalDownloadTasks = 0;
		public int TotalDownLoadingTasks
		{
			get { return statTotalDownloadTasks; }
			set { statTotalDownloadTasks = value; }
		}

		//	다운로드 완료한 Task의 개수
		private int statFinishedDownloadTasks = 0;
		public int FinishedDownloadTasks
		{
			get { return statFinishedDownloadTasks; }
			set { statFinishedDownloadTasks = value; }
		}

		//	현재 다운로드 중인 Task의 다운로드 진행률
		private double statProgressDownloadingTask = 100;
		public double ProgressDownloadingTask
		{
			get { return statProgressDownloadingTask; }
			set { statProgressDownloadingTask = value; }
		}
	
		//	현재 다운로드 중인 Task의 이름
		private string statNameDownloadingTask = "";
		public string DownloadingTaskName
		{
			get { return statNameDownloadingTask; }
			set { statNameDownloadingTask = value; }
		}

		/// <summary>
		///	플레이어의 자세한 정보 보내기
		/// </summary>
		/// <returns></returns>
		public bool SendDetailPlayerInfo()
		{
			bool bRet = false;
			if (proxy != null && cfg.WCFAuthoring)
			{
				try
				{
					if (proxy.State == CommunicationState.Opened)
					{
						IVisionService.Message msg = new IVisionService.Message();
						msg.Sender = new IVisionService.Topic();
						msg.Sender.Groupid = cfg.GroupId;
						msg.Sender.Mediaid = cfg.PlayerId;
						msg.Sender.Level = IVisionService.UserLevels.Media;
						msg.MsgType = IVisionService.MessageType.msg_PlayerInfo;
						
						NetworkAdapterInfo info = NetworkAdapterInfo.GetAvailableAdapterInfo();

						int nMntType = 0;

						if(cfg.UsingAMT)
 							nMntType |= ((int)PowerMntType.AMT);
						if(cfg.UsingWoL)
 							nMntType |= ((int)PowerMntType.WoL);

						msg.Content = info.IP4_Address + "|" + info.MAC_Address + "|" + info.IsDHCP + "|" + cfg.AuthID + "|" + cfg.AuthPass + "|" + nMntType;

						proxy.SendMessage(msg);
						logger.Info("Sent player's detail information to the server: " + msg.Content);

						bRet = true;
					}
				}
				catch (Exception exr) { logger.Error(exr.ToString()); }
			}

			return bRet;
		}

        /// <summary>
        ///	플레이어의 자세한 정보 보내기
        /// </summary>
        /// <returns></returns>
        public bool SendMessageToServer(IVisionService.MessageType tp, string sContents)
        {
            bool bRet = false;
            if (proxy != null && cfg.WCFAuthoring)
            {
                try
                {
                    if (proxy.State == CommunicationState.Opened)
                    {
                        IVisionService.Message msg = new IVisionService.Message();
                        msg.Sender = new IVisionService.Topic();
                        msg.Sender.Groupid = cfg.GroupId;
                        msg.Sender.Mediaid = cfg.PlayerId;
                        msg.Sender.Level = IVisionService.UserLevels.Media;
                        msg.MsgType = tp;
                        msg.Content = sContents;

                        proxy.SendMessage(msg);
                        logger.Info("SendMessage To Server: Type({0}), Contents({1})", msg.MsgType, msg.Content);

                        bRet = true;
                    }
                }
                catch (Exception exr) { logger.Error(exr.ToString()); }
            }

            return bRet;
        }

        /// <summary>
        /// 이벤트 보내기
        /// </summary>
        /// <param name="sEventData"></param>
        /// <returns></returns>
        public bool SendEventToServer(String sEventData)
        {
            bool bRet = false;
            if (proxy != null && cfg.WCFAuthoring)
            {
                try
                {
                    if (proxy.State == CommunicationState.Opened)
                    {
                        IVisionService.Message msg = new IVisionService.Message();
                        msg.Sender = new IVisionService.Topic();
                        msg.Sender.Groupid = cfg.GroupId;
                        msg.Sender.Mediaid = cfg.PlayerId;
                        msg.Sender.Level = IVisionService.UserLevels.Media;
                        msg.MsgType = IVisionService.MessageType.msg_RequestGSRelay;
                        msg.Content = sEventData;

                        proxy.SendMessage(msg);
                        logger.Info("Sent Event Message to server: " + msg.Content);

                        cfg.WriteTargetAdSuccess(cfg.CurrentRelayID, TargetAd_ErrorType.TargetAd_Client_ToUpper);

                        bRet = true;
                    }
                }
                catch (Exception exr) {
                    cfg.WriteTargetAdError(cfg.CurrentRelayID, TargetAd_ErrorType.TargetAd_Client_ToUpper, (int)AdTarget_Client_ErrorCodes.ErrorDisconnected);
                    logger.Error(exr.ToString()); 
                }
            }
            else 
            {
                cfg.WriteTargetAdError(cfg.CurrentRelayID, TargetAd_ErrorType.TargetAd_Client_ToUpper, (int)AdTarget_Client_ErrorCodes.ErrorDisconnected);
                logger.Error("SendEventToServer: This is not connected with server!"); 
            }

            return bRet;
        }

		public bool SendStatusToServer()
		{
			bool bRet = false;
			if (proxy != null && cfg.WCFAuthoring)
			{
				try
				{
					if(proxy.State == CommunicationState.Opened)
					{
						int versionH, versionL;
						long uptime, temperature, freespace;
						int memoryusage, cpurate;
						String MachineName = System.Environment.MachineName;

                        IAgentCmdReceiver agentCmdReceiver = (IAgentCmdReceiver)Activator.GetObject(
                          typeof(IAgentCmdReceiver), "tcp://127.0.0.1:1888/CmdReceiverHost/rt");

                        IPlayerCmdReceiver playerCmdExReceiver = (IPlayerCmdReceiver)Activator.GetObject(
                          typeof(IPlayerCmdReceiver), "tcp://127.0.0.1:1889/CmdReceiverHost/rt");

                        long res = agentCmdReceiver.ProcessSimpleCommand(CmdReceiverCommands.CmdGetVersion);

						versionH = (int)(res >> 32);
						versionL = (int)(res & 0xFFFFFFFF);

						// get free space
                        freespace = agentCmdReceiver.ProcessSimpleCommand(CmdReceiverCommands.CmdGetFreeSpace);
						// get uptime
                        uptime = agentCmdReceiver.ProcessSimpleCommand(CmdReceiverCommands.CmdGetUptime);
						// get temperature
                        temperature = agentCmdReceiver.ProcessSimpleCommand(CmdReceiverCommands.CmdGetTemperature);
                        cpurate = (int)agentCmdReceiver.ProcessSimpleCommand(CmdReceiverCommands.CmdGetCpuRate);
                        memoryusage = (int)agentCmdReceiver.ProcessSimpleCommand(CmdReceiverCommands.CmdGetMemoryUsage);
						Task currScreen = (Task)playerCmdExReceiver.ProcessExpandCommand(CmdExReceiverCommands.CmdExCurrentScreen);
						Task currSubtitle = (Task)playerCmdExReceiver.ProcessExpandCommand(CmdExReceiverCommands.CmdExCurrentSubtitle);

                        int volume = -1;
                        try
                        {
                            volume = PCControl.GetMasterVolume();
                        }
                        catch {}

						#region 시리얼 포트 상태
						String sSerialName = "";
						String sSerialPower = "-";
						String sSerialSource = "-";
						String sSerialVolume = "0";
						try
						{
							sSerialName = cfg.SerialPortDevice.ToString();
						} catch {}
						try
						{
							sSerialPower = playerCmdExReceiver.ProcessExpandCommand(CmdExReceiverCommands.CmdExSerialStatusPower).ToString();
						}
						catch { }
						try
						{
							sSerialSource = playerCmdExReceiver.ProcessExpandCommand(CmdExReceiverCommands.CmdExSerialStatusSource).ToString();
						}
						catch { }
						try
						{
							sSerialVolume = playerCmdExReceiver.ProcessExpandCommand(CmdExReceiverCommands.CmdExSerialStatusVolume).ToString();
						}
						catch { }
						#endregion
							
						
						//	current s/s
						bool bIsPlayerON = (bool)playerCmdExReceiver.ProcessExpandCommand(CmdExReceiverCommands.CmdExIsPlayerOn);

						string screen_title = "PLAYER OFF";
						string subtitle_title = "PLAYER OFF";

						if (bIsPlayerON)
						{
							screen_title = (currScreen == null ? "" : currScreen.description);
							subtitle_title = (currSubtitle == null ? "" : currSubtitle.description);
						}

                        

						IVisionService.Message msg = new IVisionService.Message();
						msg.Sender = new IVisionService.Topic();
						msg.Sender.Groupid = cfg.GroupId;
						msg.Sender.Mediaid = cfg.PlayerId;
						msg.Sender.Level = IVisionService.UserLevels.Media;
						msg.MsgType = IVisionService.MessageType.msg_Status;
                        msg.Content = String.Format("<Status versionH=\"{0}\" versionL=\"{1}\" freespace=\"{2}\" uptime=\"{3}\" temperature=\"{4}\" host=\"{5}\" cpurate=\"{6}\" memoryusage=\"{7}\" download1=\"{8}\" download2=\"{9}\" screen=\"{10}\" subtitle=\"{11}\" serialname=\"{12}\" serialpower=\"{13}\" serialsource=\"{14}\" serialvolume=\"{15}\" volume=\"{16}\"/>"
                            , versionH, versionL, freespace, uptime, temperature, MachineName, cpurate, memoryusage, String.Format("{0}/{1}", FinishedDownloadTasks, TotalDownLoadingTasks), String.Format("{0}/{1}", ProgressDownloadingTask, DownloadingTaskName),
                            screen_title, subtitle_title, sSerialName, sSerialPower, sSerialSource, sSerialVolume, volume);
/*
						msg.Content = versionH.ToString() + "|" +	// 버전 High정보
							versionL.ToString() + "|" +		// 버전 Low정보
							freespace.ToString() + "|" +	// 남은 용량 표시
							uptime.ToString() + "|" +		// 접속 시간 분 단위 표시
							temperature.ToString() + "|" +	// 온도 표시
							MachineName + "|" +				// 현재 클라이언트 이름 표시
							cpurate.ToString() + "|" +		// CPU 사용량 체크
							memoryusage.ToString() + "|" +	// 메모리 사용량 체크
							String.Format("{0}/{1}", FinishedDownloadTasks, TotalDownLoadingTasks) + "|" +	// 다운로드 : 완료 Task 개수 / 총 Task 개수
							String.Format("{0}/{1}", ProgressDownloadingTask, DownloadingTaskName) + "|" +	// 다운로드 : 진행 Percentage / 진행 중인 Task 이름
							screen_title + "|" +	// 현재 플레이 중인 Task
							subtitle_title + "|" +	// 현재 플레이 중인 Task
							sSerialName + "|" +	// 현재 모니터 디바이스 이름
							sSerialPower + "|" +	// 현재 모니터 전원
							sSerialSource + "|" +	// 현재 모니터 소스
							sSerialVolume;			// 현재 모니터 볼륨		
*/

// 						lock (semaphore)
// 						{
							proxy.SendMessage(msg);
							logger.Info("Sent status to the server.");

// 						}

						bRet = true;
					}
					else
					{
						logger.Error(proxy.State.ToString());
						bRet = false;

					}
				}
				catch (Exception ex)
				{
					logger.Error(ex.ToString());
					bRet = false;
				}
			}

			return bRet;
		}
		#endregion

		#region Task 관련
		private void AddRefDataTask()
		{
			try
			{
				Task t = new Task();
				t.type = TaskCommands.CmdGetRefData;
				t.uuid = Task.TaskRefData;
				t.pid = cfg.PlayerId;
				t.gid = cfg.GroupId;
				t.description = "CmdGetRefData";
				t.TaskStart = TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()) + 1;
				t.TaskEnd = t.TaskStart + 3600 * 24;

				if (-1 == cfg.localTasksList.AddTask(t))
				{
					////	추가가 되지않음.
					cfg.localTasksList.CleanTask(Task.TaskRefData);
					//	다시 추가 시도
					cfg.localTasksList.AddTask(t);
				}
			}
			catch (Exception e)
			{
				logger.Error(e.ToString() + "");
			}
		}

		public void AddInitializeTasks()
		{
			try
			{
				// clean storage
				Task t = new Task();
				t.type = TaskCommands.CmdClearStorage;
				t.uuid = Task.TaskClean;
				t.pid = cfg.PlayerId;
				t.gid = cfg.GroupId;
				t.description = "CmdClearStorage";
				t.TaskStart = TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()) + 600;
				t.TaskEnd = t.TaskStart + 600 * 2;
				if (-1 == cfg.localTasksList.AddTask(t))
				{
					////	추가가 되지않음.
					cfg.localTasksList.CleanTask(Task.TaskClean);
					//	다시 추가 시도
					cfg.localTasksList.AddTask(t);
				}

				//	동기화 로직 추가
				cfg.AddSynchronizeTasks();

			}
			catch (Exception e)
			{
				logger.Error(e.ToString() + "");
			}
		}
		#endregion

		#region Callback Members

		public void RefreshClients(IVisionService.Topic[] clients)
		{
		}

		public void Receive(IVisionService.Message msg)
		{
		}

        DateTime dtLastSyncDT = DateTime.MinValue;

		public void ReceiveWhisper(IVisionService.Message msg, IVisionService.Topic receiver)
		{
            if (msg != null)
            {
                try
                {
                    logger.Info("Received message from server: " + msg.Result + ", " + msg.Content);
                    if (msg.Result == iVisionCode.CONN_SUCCESS) //	인증 성공
                    {
                        cfg.HasWCFAuthoring = true;
                        cfg.HasRefData = false;
                        _lastErrorMessage = "Success.";

                        logger.Info("The authoring is succeed. (GID=" + cfg.GroupId + "PID=" + cfg.PlayerId + ")");

                        cfg.UpdateMetaTagInformation();
                    }
                    else if (msg.Result == iVisionCode.CONN_ERROR_NO_PLAYER) //	DB에 없었으나 추가 됨
                    {
                        Disconnect();
                        cfg.HasWCFAuthoring = false;
                        cfg.HasRefData = false;
                        _lastErrorMessage = Properties.Resources.errorNoPlayerInServer;
                        if (LastErrorMessageUpdated != null) LastErrorMessageUpdated(this, new LastErrorMessageUpdatedEventArgs(ErrorCodes.IS_ERROR_CONN_SESSION_FAIL, _lastErrorMessage));

                        logger.Info("No media or group in the server DB. (GID=" + cfg.GroupId + "PID=" + cfg.PlayerId + ")");
                    }
                    else if (msg.Result == iVisionCode.CONN_ERROR_DUPLICATED_PLAYER) //	중복
                    {
                        Disconnect();
                        cfg.HasWCFAuthoring = false;
                        cfg.HasRefData = false;
                        cfg.PlayerIdFromFile = msg.Sender.Mediaid;

                        _lastErrorMessage = Properties.Resources.errorAlreadyUsedPlayer;
                        if (LastErrorMessageUpdated != null) LastErrorMessageUpdated(this, new LastErrorMessageUpdatedEventArgs(ErrorCodes.IS_ERROR_CONN_SESSION_FAIL, _lastErrorMessage));

                        logger.Info("Already added, authored media. (GID=" + cfg.GroupId + "PID=" + cfg.PlayerId + ")");
                    }
                    else if (msg.Result == iVisionCode.CONN_ERROR_EXCESS_OF_ALLOWED_MAX_PLAYER) //	서버에서 접근 제한 (플레이어 개수 한도 초과)
                    {
                        Disconnect();
                        cfg.HasRefData = false;
                        cfg.HasWCFAuthoring = false;
                        _lastErrorMessage = Properties.Resources.errorExcessOfAllowedCount;
                        if (LastErrorMessageUpdated != null) LastErrorMessageUpdated(this, new LastErrorMessageUpdatedEventArgs(ErrorCodes.IS_ERROR_CONN_SESSION_FAIL, _lastErrorMessage));

                        logger.Info("It was disconnected because of an excess of the allowed maximum count. (GID=" + cfg.GroupId + "PID=" + cfg.PlayerId + ")");
                    }
                    else if (msg.Result == iVisionCode.CONN_ERROR_DISCONNECTED)
                    {
                        _lastErrorMessage = Properties.Resources.errorDisconnectedByServer;
                        if (LastErrorMessageUpdated != null) LastErrorMessageUpdated(this, new LastErrorMessageUpdatedEventArgs(ErrorCodes.IS_ERROR_CONN_SESSION_FAIL, _lastErrorMessage));

                        cfg.HasRefData = false;
                        cfg.HasWCFAuthoring = false;
                        RetryTask();
                    }
                    else if (msg.Result == iVisionCode.CONF_REFDATA_INFO)
                    {
                        cfg.HasRefData = false;
                        if (false == String.IsNullOrEmpty(msg.Content))
                        {
                            //	이 서버는 외부데이터가 존재한다는 의미
                            String[] array = msg.Content.Split('|');
                            if (array.Length == 2)
                            {
                                cfg.HasRefData = true;
                                cfg.RefDataSource = array[0];
                                cfg.RefRefreshTime = Convert.ToInt32(array[1]);

                                AddRefDataTask();
                            }
                        }
                    }
                    else if (msg.Result == iVisionCode.CONF_DOWNLOAD_TIME_INFO) //	Download Time Zone XML.
                    {
                        cfg.ParseTimezones(msg.Content);
                        logger.Info("Download time scope has been arrived from server : " + msg.Content);
                    }
                    else if (msg.Result == iVisionCode.CONF_MODIFIED_PLAYER_ID) // Modified Player ID.
                    {
                        logger.Info("Current PID : " + cfg.PlayerId);

                        if (!cfg.PlayerId.Equals(msg.Content))
                        {
                            cfg.PlayerIdFromFile = msg.Content;
                            cfg.LastSyncFromFile = 0;
                            cfg.LastSyncTaskSyncFromFile = DateTime.MinValue.ToString("yyyyMMddHHmmss");

                            // 							cfg.HasRefData = false;
                            // 							cfg.HasWCFAuthoring = false;
                            // 
                            cfg.KillPlayer();
                        }
                    }
                    else if (msg.Result == iVisionCode.CONF_MODIFIED_GROUP_ID) // Modified Group ID.
                    {
                        logger.Info("Current GID : " + cfg.GroupId);

                        cfg.receiveNetworkConfiguration();
                    }
                    else if (msg.Result == iVisionCode.CONF_MODIFIED_SERVER_IP)	// Modified Service IP.
                    {
                        logger.Info("Current Service IP : " + cfg.ServerPath);

                        if (!cfg.ServerPath.Equals(msg.Content + ":888"))
                        {
                            Disconnect();

                            cfg.ServerPathFromFile = msg.Content + ":888";
                            cfg.LastSyncFromFile = 0;

                            cfg.HasRefData = false;
                            cfg.HasWCFAuthoring = false;

                            cfg.KillPlayer();
                        }
                    }
                    else if (msg.Result == iVisionCode.FUNC_REBOOT)
                    {
                        PCControl.Reboot();
                    }
                    else if (msg.Result == iVisionCode.FUNC_KILL_AGENT)
                    {

                    }
                    else if (msg.Result == iVisionCode.FUNC_RESTART_PLAYER)
                    {
                        cfg.KillPlayer();
                    }
                    else if (msg.Result == iVisionCode.FUNC_REMOTE_CONTROL)
                    {
                        ///	원격 모니터링
                        Config.KillProcess("winvnc.exe");
                        /// winvnc -sc_prompt -sc_exit -id:12345 -connect 211.192.178.120::5500 -run noregistry
                        /// winvnc -sc_prompt -sc_exit -id:12345 -connect 211.192.178.61::5500 -run
                        string arg = string.Format("-sc_prompt -sc_exit -id:{0} -connect {1}::5500 -run", msg.Content, Config.GetConfig.RepeaterPath);
                        Config.RunProgram(AppDomain.CurrentDomain.BaseDirectory, "winvnc.exe", arg);
                    }

                    else if (msg.Result == iVisionCode.FUNC_POWER_OFF)
                    {
                        String[] arrData = msg.Content.Split('|');
                        int nID = Convert.ToInt32(arrData[0]);
                        int nVol = Convert.ToInt32(arrData[1]);

                        if (PCControl.PowerOff())
                        {
                            logger.Info("전원 제어 성공 : 제어 값 (POWER OFF)");
                            SendMessageToServer(IVisionService.MessageType.msg_ControlResult, String.Format("{0}|{1}", nID, (int)ControlResponse.Success));
                        }
                        else
                        {
                            logger.Info("전원 제어 실패 : 제어 값 (POWER OFF)");
                            SendMessageToServer(IVisionService.MessageType.msg_ControlResult, String.Format("{0}|{1}", nID, (int)ControlResponse.Failed));
                        }
                    }
                    else if (msg.Result == iVisionCode.FUNC_RESTART_PLAYER)
                    {

                        if (cfg.KillPlayer())
                        {
                            logger.Info("플레이어 재시작 제어 성공 : 제어 값 (RESTART PLAYER)");
                        }
                        else
                        {
                            logger.Info("플레이어 재시작 실패 : 제어 값 (RESTART PLAYER)");
                        }
                    }
                    else if (msg.Result == iVisionCode.FUNC_PCVOLUME)
                    {
                        String[] arrData = msg.Content.Split('|');
                        int nID = Convert.ToInt32(arrData[0]);
                        int nVol = Convert.ToInt32(arrData[1]);

                        if (PCControl.SetMasterVolume(nVol))
                        {
                            logger.Info("볼륨 제어 성공 : 제어 값 (" + nVol + ")");
                            SendMessageToServer(IVisionService.MessageType.msg_ControlResult, String.Format("{0}|{1}", nID, (int)ControlResponse.Success));
                        }
                        else
                        {
                            logger.Info("볼륨 제어 실패 : 제어 값 (" + nVol + ")");
                            SendMessageToServer(IVisionService.MessageType.msg_ControlResult, String.Format("{0}|{1}", nID, (int)ControlResponse.Failed));
                        }
                    }
                    else if (msg.Result == iVisionCode.FUNC_MON_POWER_OFF)
                    {
                        String[] arrData = msg.Content.Split('|');
                        int nID = Convert.ToInt32(arrData[0]);
                        int nVol = Convert.ToInt32(arrData[1]);

                        IPlayerCmdReceiver playerCmdExReceiver = (IPlayerCmdReceiver)Activator.GetObject(
                            typeof(IPlayerCmdReceiver), "tcp://127.0.0.1:1889/CmdReceiverHost/rt");

                        if (playerCmdExReceiver.SendControlCommand(CmdExReceiverCommands.CmdExSerialStatusPower, 0))
                        {
                            logger.Info("시리얼 전원 제어 성공 : 제어 값 (MON PWD OFF)");
                            SendMessageToServer(IVisionService.MessageType.msg_ControlResult, String.Format("{0}|{1}", nID, (int)ControlResponse.Success));
                        }
                        else
                        {
                            logger.Info("시리얼 전원 제어 실패 : 제어 값 (MON PWD OFF)");
                            SendMessageToServer(IVisionService.MessageType.msg_ControlResult, String.Format("{0}|{1}", nID, (int)ControlResponse.Failed));
                        }

                    }
                    else if (msg.Result == iVisionCode.FUNC_MON_POWER_ON)
                    {
                        String[] arrData = msg.Content.Split('|');
                        int nID = Convert.ToInt32(arrData[0]);
                        int nVol = Convert.ToInt32(arrData[1]);

                        IPlayerCmdReceiver playerCmdExReceiver = (IPlayerCmdReceiver)Activator.GetObject(
                            typeof(IPlayerCmdReceiver), "tcp://127.0.0.1:1889/CmdReceiverHost/rt");

                        if (playerCmdExReceiver.SendControlCommand(CmdExReceiverCommands.CmdExSerialStatusPower, 1))
                        {
                            logger.Info("시리얼 전원 제어 성공 : 제어 값 (MON PWD ON)");
                            SendMessageToServer(IVisionService.MessageType.msg_ControlResult, String.Format("{0}|{1}", nID, (int)ControlResponse.Success));
                        }
                        else
                        {
                            logger.Error("시리얼 전원 제어 실패 : 제어 값 (MON PWD ON)");
                            SendMessageToServer(IVisionService.MessageType.msg_ControlResult, String.Format("{0}|{1}", nID, (int)ControlResponse.Failed));
                        }
                    }
                    else if (msg.Result == iVisionCode.FUNC_MON_VOLUME)
                    {
                        String[] arrData = msg.Content.Split('|');
                        int nID = Convert.ToInt32(arrData[0]);
                        int nVol = Convert.ToInt32(arrData[1]);

                        IPlayerCmdReceiver playerCmdExReceiver = (IPlayerCmdReceiver)Activator.GetObject(
                            typeof(IPlayerCmdReceiver), "tcp://127.0.0.1:1889/CmdReceiverHost/rt");

                        if (playerCmdExReceiver.SendControlCommand(CmdExReceiverCommands.CmdExSerialStatusVolume, nVol))
                        {
                            logger.Info("시리얼 볼륨 제어 성공 : 제어 값 (" + nVol + ")");
                            SendMessageToServer(IVisionService.MessageType.msg_ControlResult, String.Format("{0}|{1}", nID, (int)ControlResponse.Success));
                        }
                        else
                        {
                            logger.Error("시리얼 볼륨 제어 실패 : 제어 값 (" + nVol + ")");
                            SendMessageToServer(IVisionService.MessageType.msg_ControlResult, String.Format("{0}|{1}", nID, (int)ControlResponse.Failed));
                        }
                    }
                    else if (msg.Result == iVisionCode.FUNC_START_CAPTURE)
                    {
                        /// 캡쳐 시작
                        Config.GetConfig.CaptureProcess = true;
                        logger.Info("캡처 프로세스 시작");
                    }
                    else if (msg.Result == iVisionCode.FUNC_END_CAPTURE)
                    {
                        //  캡쳐 종료
                        Config.GetConfig.CaptureProcess = false;
                        logger.Info("캡처 프로세스 종료");
                    }
                    else if (msg.Result == iVisionCode.GS_RESPONSE)
                    {
                        try
                        {
                            logger.Info("[Event Response Message] Responsed Data: " + msg.Content);

                            IPlayerCmdReceiver playerCmdExReceiver = (IPlayerCmdReceiver)Activator.GetObject(
                                typeof(IPlayerCmdReceiver), "tcp://127.0.0.1:1889/CmdReceiverHost/rt");

                            playerCmdExReceiver.SendEventResponse(msg.Content);

                            Config.GetConfig.WriteTargetAdSuccess(Config.GetConfig.CurrentRelayID, TargetAd_ErrorType.TargetAd_Client_ToLower);
                        }
                        catch (Exception ex)
                        {
                            Config.GetConfig.WriteTargetAdError(Config.GetConfig.CurrentRelayID, TargetAd_ErrorType.TargetAd_Client_ToLower, (int)AdTarget_Client_ErrorCodes.ErrorRemotingConnection);
                            logger.Info("[Event Response Message] Error Occurred: " + ex.Message);
                        }
                    }
                    else if (msg.Result == iVisionCode.ETRI_RESPONSE_SUBTITLE)
                    {
                        try
                        {
                            String sUuid = msg.Content.Split('|')[0];
                            String sSubtitleText = msg.Content.Split('|')[1];

                            #region BASE64 DECODING

                            try
                            {
                                byte[] encoded = System.Convert.FromBase64String(sSubtitleText);
                                sSubtitleText = System.Text.ASCIIEncoding.UTF8.GetString(encoded);
                            }
                            catch { sSubtitleText = String.Empty; }
                            #endregion

                            if (String.IsNullOrEmpty(sSubtitleText))
                            {
                                Task[] arrTasks = Config.GetConfig.localTasksList.GetAllTasks();
                                if (arrTasks != null)
                                {
                                    foreach (Task t in arrTasks)
                                    {
                                        if (t.type == TaskCommands.CmdSubtitles)
                                        {
                                            Config.GetConfig.localTasksList.ChangeTaskState(TaskState.StateCanceled, t.uuid);
                                        }
                                    }
                                }

                                logger.Info("긴급 자막 취소 완료 : ", sUuid);
                            }
                            else
                            {

                                String sXamlPath = Config.GetConfig.AppData + "Xaml\\";
                                if (!System.IO.Directory.Exists(sXamlPath))
                                    System.IO.Directory.CreateDirectory(sXamlPath);

                                String sDownloadPath = sXamlPath + sUuid + "\\";
                                if (!System.IO.Directory.Exists(sDownloadPath))
                                    System.IO.Directory.CreateDirectory(sDownloadPath);

                                String sSubtitleFilePath = sDownloadPath + "program.xml";

                                XmlDocument docSubtitle = new XmlDocument();
                                docSubtitle.LoadXml(String.Format("<FlowDocument FontFamily=\"Malgun Gothic\" FontStyle=\"Normal\" FontWeight=\"Bold\" FontSize=\"120\" Foreground=\"#FFFF0000\" xmlns=\"http://schemas.microsoft.com/winfx/2006/xaml/presentation\"><Paragraph>{0}</Paragraph></FlowDocument>", sSubtitleText));
                                docSubtitle.Save(sSubtitleFilePath);

                                Config.GetConfig.localTasksList.AddTaskWithState(new Task()
                                {
                                    uuid = new Guid(sUuid),
                                    duuid = new Guid(sUuid),
                                    daysofweek = 127,
                                    duration = 0,
                                    description = "긴급 자막",
                                    pid = Config.GetConfig.PlayerId,
                                    gid = Config.GetConfig.GroupId,
                                    show_flag = 127,
                                    TaskStart = TimeConverter.ConvertToUTP(DateTime.Now),
                                    TaskEnd = TimeConverter.ConvertToUTP(DateTime.Now + TimeSpan.FromMinutes(10)),
                                    guuid = new Guid(sUuid),
                                    state = TaskState.StateWait,
                                    type = TaskCommands.CmdSubtitles,
                                    starttimeofday = 0,
                                    endtimeofday = 0,
                                });

                                logger.Info("긴급 자막 추가 완료 : ", sUuid);
                            }


                        }
                        catch (Exception ex)
                        {
                            logger.Error("[ETRI_RESPONSE_SUBTITLE] Error Occurred: " + ex.Message);
                        }

                    }
                    else if (msg.Result == iVisionCode.ETRI_RESPONSE_SYNC_LIST)
                    {
                        try
                        {
                            String sGroupID = msg.Content.Split('|')[0];
                            String[] sIPList = msg.Content.Split('|')[1].Split(',');
                            int i = 0;
                            using (ServerDatabase.ServerDataSet.SYNC_TASKSDataTable dt = new ServerDatabase.ServerDataSet.SYNC_TASKSDataTable())
                            {
                                foreach (String ipaddr in sIPList)
                                {
                                    ServerDatabase.ServerDataSet.SYNC_TASKSRow newrow = dt.NewSYNC_TASKSRow();
                                    newrow.DEL_YN = "N";
                                    newrow.REG_DT = newrow.EDIT_DT = DateTime.Now.ToString("yyyyMMddHHmmss");
                                    newrow.PLAYERS_PID = "OPEN_SLAVE" + i++;
                                    newrow.SYNC_TP = "S";
                                    newrow.GUUID = sGroupID;
                                    newrow.IP_ADDR = ipaddr;

                                    Config.GetConfig.localTasksList.InsertOrReplaceSyncTask(newrow);
                                }
                                dt.Dispose();
                            }
                        }
                        catch (Exception ex) { logger.Error(ex.ToString()); }
                    }
                    else if (msg.Result == iVisionCode.ETRI_RESPONSE_SMIL || msg.Result == iVisionCode.ETRI_RESPONSE_SYNC_SMIL)
                    {
                        try
                        {
                            String sSmilPath = Config.GetConfig.AppData + "Smil\\";
                            if (!System.IO.Directory.Exists(sSmilPath))
                                System.IO.Directory.CreateDirectory(sSmilPath);

                            if (String.IsNullOrEmpty(msg.Content))
                            {
                                logger.Info("SMIL 데이터 없음");
                            }
                            else
                            {
                                string sSmilXml = msg.Content;
                                string sGroupID = String.Empty;
                                string sIsMaster = String.Empty;
                                string sFilename = "openscreen.smil";
                                string sFilename2 = "openscreen2.smil";
                                string sPlaylistFilename = "playlist.xml";
                                string sProgramFilename = "program.xml";

                                if (msg.Result == iVisionCode.ETRI_RESPONSE_SYNC_SMIL)
                                {
                                    sGroupID = msg.Content.Split('|')[0];
                                    sIsMaster = msg.Content.Split('|')[1];
                                    sSmilXml = msg.Content.Split('|')[2];

                                    sFilename2 = sFilename = String.Format("{0}.smil", sGroupID);
                                    sPlaylistFilename = String.Format("{0}.ipl", sGroupID);
                                    sProgramFilename = String.Format("{0}.xml", sGroupID);
                                }

                                XmlDocument doc = new XmlDocument();

                                #region BASE64 DECODING
                                byte[] encoded = System.Convert.FromBase64String(sSmilXml);
                                sSmilXml = System.Text.ASCIIEncoding.UTF8.GetString(encoded);
                                #endregion
                                doc.LoadXml(sSmilXml);

                                doc.Save(sSmilPath + sFilename);

                                #region Playlist.xml 생성
                                XmlDocument docPlaylist = new XmlDocument();
                                XmlElement elPlaylist = docPlaylist.CreateElement("playlist");
                                XmlElement elScreen = docPlaylist.CreateElement("screen");

                                XmlAttribute attr1 = docPlaylist.CreateAttribute("time");
                                attr1.Value = "23:59:59";
                                XmlAttribute attr2 = docPlaylist.CreateAttribute("selected");
                                attr2.Value = "1";
                                XmlAttribute attr3 = docPlaylist.CreateAttribute("ID");
                                attr3.Value = sFilename2;

                                elScreen.Attributes.Append(attr1);
                                elScreen.Attributes.Append(attr2);
                                elScreen.Attributes.Append(attr3);

                                elPlaylist.AppendChild(elScreen);

                                docPlaylist.AppendChild(elPlaylist);

                                #endregion

                                #region Program.xml 생성
                                if (docPlaylist.HasChildNodes && elPlaylist.HasChildNodes)
                                {
                                    docPlaylist.Save(sSmilPath + sPlaylistFilename);

                                    XmlDocument docProgram = new XmlDocument();
                                    docProgram.LoadXml(String.Format("<?xml version=\"1.0\"?><program> <pane playlist=\"{0}\" duration=\"23:59:59\" /> </program>", sPlaylistFilename));
                                    docProgram.Save(sSmilPath + sProgramFilename);
                                }
                                #endregion

                                logger.Info("SMIL 패킷 저장 성공 : " + msg.Content);

                                if (msg.Result == iVisionCode.ETRI_RESPONSE_SYNC_SMIL && sIsMaster == "M")
                                {
                                    /// 5초 간 동기화 재 시작 명령 무시
                                    if (DateTime.Now > dtLastSyncDT + TimeSpan.FromSeconds(5))
                                    {
                                        logger.Info("동기화 패킷 시작 전송 : {0}", sGroupID);
                                        IPlayerCmdReceiver playerCmdExReceiver = (IPlayerCmdReceiver)Activator.GetObject(
                                            typeof(IPlayerCmdReceiver), "tcp://127.0.0.1:1889/CmdReceiverHost/rt");

                                        playerCmdExReceiver.SendSyncSchedule(sGroupID);
                                        dtLastSyncDT = DateTime.Now;
                                    }

                                }
                            }

                        }
                        catch (Exception ex) { logger.Error(ex.ToString()); }
                    }
                    else if (msg.Result == iVisionCode.ETRI_RESPONSE_EVENT)
                    {
                        IPlayerCmdReceiver playerCmdExReceiver = (IPlayerCmdReceiver)Activator.GetObject(
                            typeof(IPlayerCmdReceiver), "tcp://127.0.0.1:1889/CmdReceiverHost/rt");

                        playerCmdExReceiver.SendEvent(msg.Content);
                    }
                }
                catch (Exception err) { logger.Error(err.ToString()); }
            }
		}
 
		public void EndReceiveWhisper(System.IAsyncResult result)
		{
		}

		public void IsWritingCallback(IVisionService.Topic client)
		{
		}

		public void EndIsWritingCallback(System.IAsyncResult result)
		{
		}

		public void ReceiverFile(IVisionService.FileMessage fileMsg, IVisionService.Topic receiver)
		{
		}

		public void UserJoin(IVisionService.Topic client)
		{
		}

		public void UserLeave(IVisionService.Topic client)
		{
		}

		public void EndRefreshClients(System.IAsyncResult result)
		{
		}

		public void EndReceive(System.IAsyncResult result)
		{
		}

		public void EndReceiverFile(System.IAsyncResult result)
		{
		}

		public void EndUserJoin(System.IAsyncResult result)
		{
		}

		public void EndUserLeave(System.IAsyncResult result)
		{
		}

		public System.IAsyncResult BeginReceiveWhisper(IVisionService.Message msg, IVisionService.Topic receiver, System.AsyncCallback callback, object asyncState)
		{
			throw new NotImplementedException();
		}

		public System.IAsyncResult BeginIsWritingCallback(IVisionService.Topic client, System.AsyncCallback callback, object asyncState)
		{
			throw new NotImplementedException();
		}

		public System.IAsyncResult BeginReceiverFile(IVisionService.FileMessage fileMsg, IVisionService.Topic receiver, System.AsyncCallback callback, object asyncState)
		{
			throw new NotImplementedException();
		}

		public System.IAsyncResult BeginUserJoin(IVisionService.Topic client, System.AsyncCallback callback, object asyncState)
		{
			throw new NotImplementedException();
		}

		public System.IAsyncResult BeginReceive(IVisionService.Message msg, System.AsyncCallback callback, object asyncState)
		{
			throw new NotImplementedException();
		}

		public System.IAsyncResult BeginUserLeave(IVisionService.Topic client, System.AsyncCallback callback, object asyncState)
		{
			throw new NotImplementedException();
		}

		public System.IAsyncResult BeginRefreshClients(IVisionService.Topic[] clients, System.AsyncCallback callback, object asyncState)
		{
			throw new NotImplementedException();
		}

		#endregion Callback Members
	}

	public delegate void StateChangedEventHandler(object sender, StateChangedEventArgs e);

	public class StateChangedEventArgs : EventArgs
	{
		public CommunicationState State
		{
			get;
			set;
		}
		public bool IsConnected
		{
			get;
			set;
		}

		public StateChangedEventArgs(bool _isconn, CommunicationState curr)
		{
			State = curr;
			IsConnected = _isconn;
		}
	}

	public delegate void LastErrorMessageUpdatedEventHandler(object sender, LastErrorMessageUpdatedEventArgs e);

	public class LastErrorMessageUpdatedEventArgs : EventArgs
	{
		public String LastErrorMessage
		{
			get;
			set;
		}
		public ErrorCodes errorCode
		{
			get;
			set;
		}

		public LastErrorMessageUpdatedEventArgs(ErrorCodes _errorno, String _lasterr)
		{
			LastErrorMessage = _lasterr;
			errorCode = _errorno;
		}
	}
}
