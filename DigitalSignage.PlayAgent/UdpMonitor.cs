﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using NLog;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.ComponentModel;
using System.Windows.Forms;

namespace DigitalSignage.PlayAgent
{
    class UdpMonitor
    {
        private bool process = false;
        private Thread udpThread;
        private int udpPort = 1891;
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public UdpMonitor()
        {
            try
            {
                //Starting the UDP Server thread.
                process = true;
                udpThread = new Thread(new ThreadStart(Start));
                udpThread.Priority = ThreadPriority.BelowNormal;
                udpThread.Start();
            }
            catch (Exception e)
            {
                logger.Error(e + "");
                udpThread.Abort();
            }
        }

        public void Start()
        {
            //IPHostEntry localHostEntry;
            try
            {
                int recv = 0;
                //Create a UDP socket.
                Socket soUdp = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
                IPEndPoint localIpEndPoint = new IPEndPoint(0, udpPort);
                soUdp.Bind(localIpEndPoint);
                soUdp.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReceiveTimeout, 2000);
                while (process)
                {
                    Byte[] received = new Byte[256];
                    IPEndPoint tmpIpEndPoint = new IPEndPoint(0, udpPort);
                    EndPoint remoteEP = (tmpIpEndPoint);
                    try
                    {
                        recv = soUdp.ReceiveFrom(received, ref remoteEP);
                        String dataReceived = System.Text.Encoding.ASCII.GetString(received, 0, recv);
                        //logger.Info("Received " + dataReceived);
                        if (dataReceived.Equals("DigitalSignageScreenRequest"))
                        {
                            int scrWidth = Screen.PrimaryScreen.Bounds.Width;
                            int scrHeight = Screen.PrimaryScreen.Bounds.Height;
                            Bitmap scrShot = new Bitmap(scrWidth, scrHeight);
                            Graphics gfx = Graphics.FromImage((Image)scrShot);
                            gfx.CopyFromScreen(0, 0, 0, 0, new Size(scrWidth, scrHeight));
                            MemoryStream ms = new MemoryStream();
                            scrShot.Save( ms, ImageFormat.Png );
                            //byte[] bitmapData = ms.ToArray();
                            //scrShot.
                            //saveFileDialog1.ShowDialog();
                            //scrShot.Save(saveFileDialog1.FileName + ".bmp");             
                            //Byte[] returningByte = System.Text.Encoding.ASCII.GetBytes(returningString.ToCharArray());
                            //logger.Info("Send answer to " + remoteEP.ToString());
                            soUdp.SendTo( ms.ToArray(), remoteEP);
                        }
                    }
                    catch (Exception e)
                    {
                        logger.Info(e + "");
                    }
                }
            }
            catch (Exception e)
            {
                logger.Error(e + "");
            }
        }

        public void Stop()
        {
            process = false;
            udpThread.Abort();
        }
    }
}
