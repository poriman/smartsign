﻿using System;
using System.Threading;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;
using NLog;
using Microsoft.Win32;


namespace DigitalSignage.PlayAgent
{
    class ProcessMonitor
    {
		private static Logger logger = LogManager.GetCurrentClassLogger();

		private bool process = true;
		private Config config = Config.GetConfig;
		private static readonly object semaphore = new object();
		/// <summary>
		/// Player의 CPU 시간을 기록한다.
		/// </summary>
		private TimeSpan LastPlayerTimeSpan = new TimeSpan(0, 0, 0, 0);
		/// <summary>
		/// Player가 응답이 없거나 CPU시간이 동일한 카운트를 체크한다.
		/// </summary>
		private int nNoRespondingCount = 0;

		public void start()
        {
            this.process = true;
            Thread control = new Thread(this.update);
            control.Priority = ThreadPriority.Lowest;
            control.Start();

		}


		#region Window API
		public const int WM_SYSCOMMAND = 0x0112;
		public const int SC_CLOSE = 0xF060;

		[DllImport("user32.dll")]
		public static extern IntPtr FindWindow(string className, string windowName);
		[DllImport("user32.dll")]
		public static extern int SendMessage(IntPtr hwnd, int wMsg, int wParam, int lParam);
		#endregion

		private void startPlayer()
		{
			// get assebly path
			string strAppDir = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetModules()[0].FullyQualifiedName);

			Process proc = new Process();
			proc.StartInfo.FileName = strAppDir + "\\DS.ScreenCast.exe";
			proc.StartInfo.WorkingDirectory = strAppDir;
			if (process)
			{
				logger.Info("'ds.screencast.exe' process will be started.");
				proc.Start();
			}
		}

		/// <summary>
		/// 에러 보고 서비스가 실행 중이라면 종료하도록 유도한다.
		/// </summary>
		private void stopErrorReportService()
		{
			try
			{
                using (System.ServiceProcess.ServiceController sc = new System.ServiceProcess.ServiceController("Windows Error Reporting Service"))
				{
					if (sc.Status == System.ServiceProcess.ServiceControllerStatus.Running)
					{
						sc.Stop();
						logger.Info("Error Reporting Service Stopped!");
					}
					sc.Close();
					sc.Dispose();
				}
			}
			catch (Exception ex) { logger.Error(ex.ToString()); }
			catch {}
		}

		/// <summary>
		/// 오류 보고 서비스를 실행 안하도록 레지스트리를 구성한다.
		/// </summary>
		private void ErrorReportServiceRegistryOff()
		{
			try
			{
				RegistryKey local = Registry.LocalMachine;
				RegistryKey subkey = local.OpenSubKey("SOFTWARE\\Microsoft\\PCHealth\\ErrorReporting", true);

 				subkey.SetValue("DoReport", 0);
				subkey.SetValue("ShowUI", 0);
                subkey.Flush();
                subkey.Close();
			}
			catch (System.Exception ex)
			{
				logger.Error(ex.ToString());				
			}
		}

		private void stopUpdater()
		{
            Process[] processlist = Process.GetProcessesByName("DS.Updater");

			if (processlist != null)
			{
				foreach (Process proc in processlist)
				{
					try
					{
						String fname = System.IO.Path.GetFileName(proc.MainModule.FileName).ToLower();
						if (fname.Equals("ds.updater.exe"))
						{
							try
							{
								Thread.Sleep(100);
								proc.Kill();
								proc.Dispose();
								logger.Info("Founded 'DS.Updater.exe' process. It will be closed.");
								Thread.Sleep(100);
								break;
							}
							catch (Exception ex)
							{

								logger.Error("ModuleName=" + fname + ", " + ex.Message);
							}
						}
					}
					catch (Exception err)
					{

						logger.Error(err.Message);
					}
				}
			}
		}

		private void stopPlayer()
		{
			Process[] processlist = Process.GetProcessesByName("DS.ScreenCast");
			foreach (Process proc in processlist)
			{
				try
				{
					String fname = System.IO.Path.GetFileName(proc.MainModule.FileName).ToLower();
					if (fname.Equals("ds.screencast.exe"))
					{
						try
						{
							Thread.Sleep(100);
							proc.Kill();
							proc.Dispose();
                            logger.Info("Founded 'DS.ScreenCast.exe' process. It will be closed.");
							Thread.Sleep(100);
							break;
						}
						catch (Exception ex)
						{

							logger.Error("ModuleName=" + fname + ", " + ex.Message);
						}
					}
				}
				catch (Exception err)
				{
					
					logger.Error(err.Message);
				}
			}			
		}
		private void SendCloseErrorWindow()
		{
			try
			{
				IntPtr handle = FindWindow(null, "DS.ScreenCast.exe");
				if (handle != null && handle.ToInt32() > 0)
				{
                    procErrorWindows(handle);
                    logger.Info("Founded 'DS.ScreenCast.exe' Message Window. Will be closed.");

                    stopPlayer();
				}

				handle = FindWindow(null, "DS.ScreenCast");
				if (handle != null && handle.ToInt32() > 0)
				{
                    procErrorWindows(handle);
                    logger.Info("Founded 'DS.ScreenCast' Message Window. Will be closed.");

                    stopPlayer();
				}

                handle = FindWindow(null, "ActiveMovie Window: i-Vision.Player.exe - 응용 프로그램 오류");
                if (handle != null && handle.ToInt32() > 0)
                {
                    SendMessage(handle, WM_SYSCOMMAND, SC_CLOSE, 0);
                    logger.Info("Founded 'ActiveMovie Window: i-Vision.Player.exe - 응용 프로그램 오류' Message Window. Will be closed.");

                    stopPlayer();

                    Common.PCControl.Reboot();
                }

                handle = FindWindow(null, "i-Vision.Player.exe - 응용 프로그램 오류");
                if (handle != null && handle.ToInt32() > 0)
                {
                    SendMessage(handle, WM_SYSCOMMAND, SC_CLOSE, 0);
                    logger.Info("Founded 'i-Vision.Player.exe - 응용 프로그램 오류' Message Window. Will be closed.");

                    stopPlayer();
                }

				//	PPT 인경우..
				handle = FindWindow(null, "Microsoft Office PowerPoint");
				{
					if (handle != null && handle.ToInt32() > 0)
					{
                        logger.Info("Founded 'Microsoft Office PowerPoint' Message Window. Will be closed.");
                        procErrorWindows(handle);

                        stopPlayer();
                    }
				}
                //	PPT 인경우.. hsjang 에러메시지가 아닌 일반 파워포인트가 켜져도 이 로직으로 타서 문제가 지속적으로 발생하여 주석처리함.
                handle = FindWindow(null, "Microsoft PowerPoint");
                {
                    if (handle != null && handle.ToInt32() > 0)
                    {
                        logger.Info("Founded 'Microsoft PowerPoint' Message Window. Will be closed.");
                        //procErrorWindows(handle);
                        
                        //stopPlayer();
                    }
                }
				//	Web Page Script Error
				handle = FindWindow(null, "Internet Explorer 스크립트 오류");
				{
					if (handle != null && handle.ToInt32() > 0)
					{
                        logger.Info("Founded 'Internet Explorer 스크립트 오류' Message Window. Will be closed.");
                        procErrorWindows(handle);
                    }
				}
				//	Web Page Script Error
				handle = FindWindow(null, "Internet Explorer Script Error");
				{
					if (handle != null && handle.ToInt32() > 0)
					{
                        logger.Info("Founded 'Internet Explorer 스크립트 오류' Message Window. Will be closed.");
                        procErrorWindows(handle);
                    }
				}

				//	svchost.exe - 응용 프로그램 오류
				handle = FindWindow(null, "svchost.exe - 응용 프로그램 오류");
				{
					if (handle != null && handle.ToInt32() > 0)
					{
                        logger.Info("Founded 'svchost.exe - 응용 프로그램 오류' Message Window. Will be closed.");
                        procErrorWindows(handle);
                    }
				}

				//	Microsoft Visual C++ runtime error..
				handle = FindWindow(null, "Microsoft Visual C++ Runtime Library");
				{
					if (handle != null && handle.ToInt32() > 0)
					{
                        logger.Info("Microsoft Visual C++ Runtime Library.");
                        procErrorWindows(handle);
                    }
				}

                 //	Microsoft .NET Framework error..
                 handle = FindWindow(null, "Microsoft .NET Framework");
                 {
                     if (handle != null && handle.ToInt32() > 0)
                     {
                         logger.Info("Microsoft .NET Framework Error.");
                         procErrorWindows(handle);
                     }
                 }
				//	비스타인경우..
				handle = FindWindow(null, "Microsoft Windows");
				{
					if (handle != null && handle.ToInt32() > 0)
					{
                        logger.Info("Founded 'Microsoft Windows' Message Window. Will be closed.");
                        procErrorWindows(handle);
                    }
				}
				//	비스타인경우..
				handle = FindWindow(null, "POWERPNT.exe");
				{
					if (handle != null && handle.ToInt32() > 0)
					{
						logger.Info("Founded 'POWERPNT.exe' Message Window. Will be closed.");
                        procErrorWindows(handle);

                        stopPlayer();
                    }
				}
                //	EWF 메시지 창
                handle = FindWindow(null, "Enhanced Write Filter:Help");
                {
                    if (handle != null && handle.ToInt32() > 0)
                    {
                        SendMessage(handle, WM_SYSCOMMAND, SC_CLOSE, 0);
                        logger.Info("Founded 'Enhanced Write Filter:Help' Message Window. Will be closed.");
                    }
                }

                //	svchost.exe 오류 메시지 창
                handle = FindWindow(null, "svchost.exe - 응용 프로그램 오류");
                {
                    if (handle != null && handle.ToInt32() > 0)
                    {
                        SendMessage(handle, WM_SYSCOMMAND, SC_CLOSE, 0);
                        logger.Info("Founded 'svchost.exe - 응용 프로그램 오류' Message Window. Will be closed.");
                    }
                }

                //	svchost.exe 오류 메시지 창
                handle = FindWindow(null, "svchost.exe");
                {
                    if (handle != null && handle.ToInt32() > 0)
                    {
                        SendMessage(handle, WM_SYSCOMMAND, SC_CLOSE, 0);
                        logger.Info("Founded 'svchost.exe - 응용 프로그램 오류' Message Window. Will be closed.");
                    }
                }
			}
			catch(Exception e)
			{
				logger.Error(e.Message);
			}
		}

        /// <summary>
        /// 에러 Window를 만났을때 처리
        /// </summary>
        /// <param name="handle"></param>
        private void procErrorWindows(IntPtr handle)
        {
            if (config.RebootOnError)
            {
                DigitalSignage.Common.PCControl.Reboot();
            }
            else
            {
                SendMessage(handle, WM_SYSCOMMAND, SC_CLOSE, 0);
            }
        }

        public void update()
        {
			//	에러 보고 서비스 죽이기
			stopErrorReportService();
			ErrorReportServiceRegistryOff();

            bool runned = false;
            while (process)
            {
                try
                {
                    //	각종 에러 윈도우 메시지 보내기
                    SendCloseErrorWindow();
                    //	Process 얻어오기
					Process[] procarray = Process.GetProcessesByName("DS.ScreenCast");
					if (procarray.Length > 0)
                    {
                        if (procarray[0].Responding == false)
                        {
                            nNoRespondingCount++;
                        }
                        else
                        {
                            nNoRespondingCount = 0;
                        }

                        //	Process 종료
                        if (nNoRespondingCount >= 5)
                            runned = false;
                        else
                            runned = true;

                        if (config.AliveCheckerFromPlayer() == false)
                            runned = false;
                    }
                    else
                        runned = false;

                    if (runned == false)
                    {
                        try
                        {
                            //	Process 프로세스 시작
                            stopPlayer();

                            startPlayer();
                        }
                        catch (Exception err)
                        {
                            logger.Error(err.Message);
                        }

                        Thread.Sleep(5000);
                    }
                }
                catch (System.Exception errth)
                {
                    logger.Error(errth.ToString());
                	
                }

				Thread.Sleep(4000);

            }
        }

        public void stop()
        {
            process = false;
			
			stopUpdater();
			stopPlayer();
        }
	}
}
