﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections;
using System.Windows.Forms;

using System.Runtime.InteropServices;

// .NET remoting
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;
using System.Runtime.Remoting.Channels.Tcp;

using NLog;
using NLog.Targets;

using System.Threading;
using DigitalSignage.Common;

using System.IO;
using System.Xml;
using DigitalSignage.ServerDatabase;
using System.Globalization;
using System.Reflection;
using System.Diagnostics;

namespace DigitalSignage.PlayAgent
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class Agent : Window
    {
		public NotifyIcon notifier = null;
		private System.Windows.Forms.ContextMenu contextMenu1 = new System.Windows.Forms.ContextMenu();

		ServerConfig configDlg = null;
        ProcessMonitor monitor = null;
		NetworkMonitor network = null;

		//	hsshin WCF Connection 추가
		WCFAuthoring wcfAuth = null;
        private Thread resDataThread = null;

		private static Logger logger = LogManager.GetCurrentClassLogger();

        private static bool process = true;
		private Config config = Config.GetConfig;

		Mutex _mutex = null;
		
		/// <summary>
		/// 플레이어 UpldateURL 변경
		/// </summary>
		private void CleanUpdateURL()
		{
			try
			{
				DigitalSignage.SerialKey.SerialKey._ProductCode pcode = config.ProductCode;

				XmlDocument updater_config = new XmlDocument();
				updater_config.Load(AppDomain.CurrentDomain.BaseDirectory + "DS.Updater.exe.config");

				XmlNode node = updater_config.SelectSingleNode("//child::setting[attribute::name=\"UpdateURL\"]");
				XmlNode nodevalue = node.SelectSingleNode("child::value");

				if(pcode == DigitalSignage.SerialKey.SerialKey._ProductCode.SoftwareAsaService)
				{
					if (!nodevalue.ChildNodes[0].Value.Contains("/SaaS/"))
					{
						nodevalue.ChildNodes[0].Value = "http://update.myivision.com/ivision21/SaaS/Player/Default/";
						updater_config.Save(AppDomain.CurrentDomain.BaseDirectory + "DS.Updater.exe.config");
					}
				}
				else if(pcode == DigitalSignage.SerialKey.SerialKey._ProductCode.NetworkEdition)
				{
					if (!nodevalue.ChildNodes[0].Value.Contains("/NE/"))
					{
						nodevalue.ChildNodes[0].Value = "http://update.myivision.com/ivision21/NE/Player/Default/";
						updater_config.Save(AppDomain.CurrentDomain.BaseDirectory + "DS.Updater.exe.config");
					} 
				}
			
			} catch {}
		}

		private void InitializeLogger()
		{
			String sExecutingPath = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
			string appData = sExecutingPath + "\\PlayerData\\";
			if (!Directory.Exists(appData))
				Directory.CreateDirectory(appData);

			try
			{
				// configuring log output
				FileTarget target = new FileTarget();
				target.Layout = "${longdate}\t[${level}]\t[${callsite}]\t${message}";
				//             target.Layout = "${longdate} ${logger} ${message}";
				target.FileName = appData + "PlayAgent.Log.txt";
				target.ArchiveFileName = appData + "archives/PlayAgent.Log.{#####}.txt";
				// 			target.ArchiveAboveSize = 256 * 1024 * 10; // archive files greater than 2560 KB
				target.MaxArchiveFiles = 31;
				target.ArchiveEvery = FileArchivePeriod.Day;// FileTarget.ArchiveEveryMode.Day;
				target.ArchiveNumbering = ArchiveNumberingMode.Sequence;// FileTarget.ArchiveNumberingMode.Sequence;
				// this speeds up things when no other processes are writing to the file
				target.ConcurrentWrites = true;
				NLog.Config.SimpleConfigurator.ConfigureForTargetLogging(target, LogLevel.Debug);

			}
			catch { }
		}

		public Agent()
        {
			#region Mutex - 프로그램 한개만 뜨도록
			bool bCreate = false;
			_mutex = new Mutex(true, "DigitalSignage.PlayAgent", out bCreate);
			if (!bCreate)
			{
				this.Close();
                process = false;
				return;
			}
			#endregion

			try
            {
				InitializeLogger();
                InitializeComponent();

                /*
				TrialChecker checker = new TrialChecker();
				if (!checker.IsValid)
				{
					System.Windows.MessageBox.Show(checker.LastMessage);
					System.Windows.Application.Current.Shutdown();
				}
                 * */

				try
				{
					Cultures.CultureResources temp = new DigitalSignage.PlayAgent.Cultures.CultureResources();
					Cultures.CultureResources.ChangeCulture(Properties.Settings.Default.Default_Culture);
				}
				catch (System.Exception e)
				{
					logger.Error(e.ToString());
				}

				registerService();

				wcfAuth = WCFAuthoring.GetWCFObject;
				
				monitor = new ProcessMonitor();
				monitor.start();

				network = new NetworkMonitor();
				network.start();

				resDataThread = new Thread(new ThreadStart(ResultDataThread));
				resDataThread.Start();

				Thread.Sleep(2000);
				InitializeNotifyIcon();
				
				//CleanUpdateURL();

				if (config.FirstStart == 0)
				{
					System.Windows.MessageBox.Show(Properties.Resources.mbFirstRun, Properties.Resources.titlePlayer, MessageBoxButton.OK);

					configDlg = new ServerConfig(config, false);

					configDlg.ShowDialog();
					if (configDlg.result == true)
					{
						config.ServerPath = configDlg.Hostname;
						logger.Info("New server address = " + config.ServerPath);
					}
					config.FirstStart = 1;
				}
			}
			catch (Exception err)
			{
				System.Windows.Forms.MessageBox.Show(err.Message);
			}
        }
		void notifier_MouseDoubleClick(object sender, System.Windows.Forms.MouseEventArgs e)
		{
            process = false;
            resDataThread.Abort();
			this.Close();
		}
		void menuExit_Click(object sender, EventArgs e)
		{
			if (configDlg != null && configDlg.IsLoaded == true)
			{
                process = false;
                resDataThread.Abort();
				configDlg.Close();
			}

			ReleaseObjects();

			System.Windows.Application.Current.Shutdown();	
		}

		void ReleaseObjects()
		{
			try
			{
                process = false;
                resDataThread.Abort();

				monitor.stop();
				network.stop();

				// WCF 종료
				if (wcfAuth != null)
				{
                    wcfAuth.IsExit = true;
                    if(wcfAuth.IsAuthored())
    					wcfAuth.Disconnect();
					wcfAuth = null;
				}

				if (notifier != null)
				{
					notifier.Dispose();
					notifier = null;
				}

				if (_mutex != null)
					_mutex.ReleaseMutex();

			}
			catch (Exception ex)
			{
				logger.Error(ex.Message);			
			}
		}

		void menuConfigure_Click(object sender, EventArgs e)
		{
			// 				System.Windows.Forms.Cursor.Show();
			if (configDlg == null || configDlg.IsLoaded == false)
			{
				configDlg = new ServerConfig(config, false);
				configDlg.Show();
			}
			else
				configDlg.Activate();

			// 				System.Windows.Forms.Cursor.Hide();
		}
		
		void notifier_MouseDown(object sender, System.Windows.Forms.MouseEventArgs e)
		{
			System.Windows.Forms.MessageBox.Show("하이");
		}
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
			ReleaseObjects();	
		}

		#region network routines
		private int registerService()
		{
			try
			{
				// creating providers chunks
				BinaryServerFormatterSinkProvider provider = new BinaryServerFormatterSinkProvider();
				provider.TypeFilterLevel = System.Runtime.Serialization.Formatters.TypeFilterLevel.Full;
				ClientIPProvider ipProvider = new ClientIPProvider();
				provider.Next = ipProvider;

				// regestering channel
				Hashtable props = new Hashtable();
				props.Add("port", 1888);
				TcpChannel channel = new TcpChannel(props, null, provider);
				ChannelServices.RegisterChannel(channel, false);

				// resgestering services
				RemotingConfiguration.RegisterWellKnownServiceType(
                  typeof(AgentCmdReceiver),
				  "CmdReceiverHost/rt",
				  WellKnownObjectMode.Singleton);

				logger.Info(".NET remoting handlers registered successfully");
				// 				udpListener = new UDPListener();
				//                 logger.Info("UDP listener started");
				return 0;
			}
			catch (Exception e)
			{
				logger.Error(e + "");
			}
			return -1;
		}
		#endregion

		private void InitializeNotifyIcon()
		{
			//	Configure 메뉴
			System.Windows.Forms.MenuItem menu = new System.Windows.Forms.MenuItem();
			menu.Text = Properties.Resources.menuitemConfigure;
			menu.Click += new EventHandler(menuConfigure_Click);
			contextMenu1.MenuItems.Add(menu);


			//	Language 메뉴
			menu = new System.Windows.Forms.MenuItem();
			menu.Text = Properties.Resources.menuitemLanguage;

			List<CultureInfo> arrInfos = Cultures.CultureResources.SupportedCultures;

			foreach (CultureInfo info in arrInfos)
			{
				bool bChecked = Properties.Resources.Culture != null ? Properties.Resources.Culture.DisplayName.Equals(info.DisplayName) : false;
				System.Windows.Forms.MenuItem item = new System.Windows.Forms.MenuItem();
				item.Text = info.DisplayName;
				item.Checked = bChecked;
				item.Click += new EventHandler(menuCulture_Click);
				menu.MenuItems.Add(item);
			}

			contextMenu1.MenuItems.Add(menu);

			//	Next Program 메뉴
			menu = new System.Windows.Forms.MenuItem();
			menu.Text = Properties.Resources.menuitemNextProgram;
			menu.Click += new EventHandler(menuNextProgram_Click);
			contextMenu1.MenuItems.Add(menu);

			//	About 메뉴
			//menu = new System.Windows.Forms.MenuItem();
			//menu.Text = Properties.Resources.menuitemAbout;
			//menu.Click += new EventHandler(menuAbout_Click);
			//contextMenu1.MenuItems.Add(menu);


			//	Exit 메뉴
			menu = new System.Windows.Forms.MenuItem();
			menu.Text = Properties.Resources.menuitemExit;
			menu.Click += new EventHandler(menuExit_Click);
			contextMenu1.MenuItems.Add(menu);

			notifier = new NotifyIcon();

			notifier.Icon = DigitalSignage.PlayAgent.Properties.Resources.Player_icon;
			notifier.ContextMenu = contextMenu1;
			notifier.Text = Properties.Resources.titlePlayer;

			notifier.Visible = true;
		}

		void menuNextProgram_Click(object sender, EventArgs e)
		{
			network.AddNextScreen();
		}

		void menuAbout_Click(object sender, EventArgs e)
		{
			AboutPlayer dlg = new AboutPlayer();

			dlg.ShowDialog();
		}

		void menuCulture_Click(object sender, EventArgs e)
		{
			System.Windows.Forms.MenuItem item = sender as System.Windows.Forms.MenuItem;

			if(item != null)
			{
				CultureInfo selected = null;
				foreach(CultureInfo info in Cultures.CultureResources.SupportedCultures)
				{
					if (info.DisplayName.Equals(item.Text))
						selected = info;
				}
				if (selected != null)
				{
					Cultures.CultureResources.ChangeCulture(selected);

					Properties.Settings.Default.Default_Culture = selected;
					Properties.Settings.Default.Save();

					notifier.Dispose();
					contextMenu1.Dispose();
					notifier = null;

					contextMenu1 = new System.Windows.Forms.ContextMenu();
					InitializeNotifyIcon();
				}
			}
		}

		private void Window_Unloaded(object sender, RoutedEventArgs e)
		{
			ReleaseObjects();
		}

        private void ResultDataThread()
        {
            while (process)
            {
                try
                {
                    string path = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\Macromedia\\Flash Player\\#SharedObjects\\";//VDWFLFFX\\localhost\\";

                    path = DirectoryExists(path);
                    if (!Directory.Exists(path))
                        return;

                    DirectoryInfo info = new DirectoryInfo(path);
                    FileInfo[] fileinfo = info.GetFiles();

                    int count = 0, valueCount = 0;
                    for (int i = 0; i < fileinfo.Length; i++)
                    {
                        if (fileinfo[i].Extension.Equals(".sol") && fileinfo[i].Name.Contains(DateTime.Now.ToString("yyyyMMdd") + "_"))
                        {
                            count++;
                        }
                    }
                    FlashData[] FlashValue = new FlashData[count];
                    for (int i = 0; i < fileinfo.Length; i++)
                    {
                        if (fileinfo[i].Extension.Equals(".sol") && fileinfo[i].Name.Contains(DateTime.Now.ToString("yyyyMMdd") + "_"))
                        {
                            FlashValue[valueCount] = GetFileValue(fileinfo[i].FullName);
                            valueCount++;
                        }
                    }

                    if (wcfAuth.IsAuthored())
                    {
                        foreach(FlashData data in FlashValue)
                        {
                            string values ="";
                            for(int i = 0; i< 20; i++)
                            {
                                values += string.Format("{0:00}", data.Values[i]); 
                                values += (i==19)?"":"|";
                            }

                            config.ServerResultDataList.AddResultData(long.Parse(DateTime.Now.ToString("yyyyMMdd")), config.PlayerId, data.id, values.Replace(" ", ""));
                        }

                        UpdateResultData();
                    }
                    else if (!CreatDataXML(FlashValue, GetResultFolder() + "\\SResult_" + DateTime.Now.ToString("yyyyMMdd") + ".xml"))
                    {
                        logger.Info("Creat Result Data Failed!... = " + "\\SResult_" + DateTime.Now.ToString("yyyyMMdd") + ".xml");
                    }

                }
                catch (Exception ex)
                {
                    logger.Info(ex.Message.ToString());
                }

                //5분에 한번씩 전송 하도록...
                Thread.Sleep(300000);
            }
        }

        private static string GetResultFolder()
        {
            string filename = AppDomain.CurrentDomain.BaseDirectory + "SResult\\";
            try
            {
                if (!Directory.Exists(filename))
                {
                    Directory.CreateDirectory(filename);
                }
            }
            catch
            {
            }
            return filename;
        }

        public void UpdateResultData()
        {
            try
            {
                DirectoryInfo info = new DirectoryInfo(GetResultFolder());
                FileInfo[] fileinfo = info.GetFiles();
                bool[] isApply = new bool[fileinfo.Length];
                for (int i = 0; i < fileinfo.Length; i++)
                {
                    isApply[i] = true;
                    if (fileinfo[i].Extension.ToLower().Equals(".xml") && fileinfo[i].Name.IndexOf("SResult_") > 0)
                    {
                        FlashData[] FlashValues = GetXMlValue(fileinfo[i].FullName);
                        

                        string date = fileinfo[i].Extension.ToLower().Replace("SResult_", "").Replace(".xml", "");
                        foreach (FlashData data in FlashValues)
                        {
                            string values = "";
                            for (int t = 0; t < 20; t++)
                            {
                                values += string.Format("{0:00}", data.Values[t]);
                                values += (t == 19) ? "" : "|";
                            }
                            if (false == config.ServerResultDataList.AddResultData(long.Parse(date), config.PlayerId, data.id, values))
                                isApply[i] = false;
                        }
                    }
                }
                for (int i = 0; i < fileinfo.Length; i++)
                {
                    if (isApply[i] == true)
                    {
                        File.Delete(fileinfo[i].FullName);
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Info(ex.Message.ToString());
            }
        }

        public FlashData[] GetXMlValue(string path)
        {
            FlashData[] data;
            try
            {
                if (File.Exists(path))
                {
                    XmlDocument _doc = new XmlDocument();
                    _doc.Load(path);
                    XmlNode mediainfo1 = _doc.SelectSingleNode("//MenuReport");
                    XmlNodeList nodeList = mediainfo1.ChildNodes;
                    int count = nodeList.Count;
                    data = new FlashData[count];
                    for (int i = 0; i < nodeList.Count; i++)
                    {
                        XmlNode nodes = nodeList.Item(i);
                        data[i].id = nodes.Attributes["menu_id"].Value;

                        for (int j = 0; j < nodes.ChildNodes.Count; j++)
                        {
                            XmlNode temp = nodes.ChildNodes.Item(j);
                            for (int t = 0; t < 20; t++)
                            {
                                if (temp.Name.Equals(string.Format("Value{0:00}", t + 1)))
                                    data[i].Values[t] = int.Parse(temp.InnerText);
                            }                            
                        }
                    }

                    return data;
                }
            }
            catch (Exception ex)
            {
                logger.Info("설문조사 결과 파일 검색 실패 : " + ex.Message.ToString());
            }

            return null;
        } 

        private bool CreatDataXML(FlashData[] FlashValue, string filePath)
        {
            if (File.Exists(filePath))
                File.Delete(filePath);

            XmlWriter xw = new XmlTextWriter(filePath, Encoding.UTF8);
            try
            {
                {
                    xw.WriteStartDocument();
                    xw.WriteStartElement("MenuReport");
                    xw.WriteAttributeString("Count", FlashValue.Length.ToString());
                    //foreach (FlashData f in FlashValue)
                    for (int i = 0; i < FlashValue.Length; i++)
                    {
                        FlashData f = FlashValue[i];
                        xw.WriteStartElement("menulist");
                        xw.WriteAttributeString("menu_id", f.id);
                        
                        for (int j = 0; j < 20; j++)
                        {
                            xw.WriteStartElement(string.Format("Value{0:00}", j +1));
                            xw.WriteString(f.Values[j].ToString());
                            xw.WriteEndElement();
                        }
                        
                        xw.WriteEndElement();
                    }
                    xw.WriteEndElement();
                }
                xw.WriteEndDocument();
                if (xw != null)
                {
                    xw.Close();
                }
                return true;
            }
            catch (Exception ex)
            {
                logger.Info(ex.Message.ToString());
                if (xw != null)
                    xw.Close();
            }

            return false;
        }

        private string DirectoryExists(string path)
        {
            try
            {
                DirectoryInfo info = new DirectoryInfo(path);
                foreach (DirectoryInfo dinfo in info.GetDirectories())
                {
                    if (Directory.Exists(dinfo.FullName + "\\localhost"))
                    {
                        return dinfo.FullName + "\\localhost";
                    }
                    else
                        return DirectoryExists(dinfo.FullName);
                }
            }
            catch (Exception ex)
            {
                logger.Info(ex.Message.ToString());
            }

            return "";
        }

        private FlashData GetFileValue(string path)
        {
            FlashData Data = new FlashData();
            int[] values = new int[20];
            try
            {
                for (int i = 0; i < 20; i++)
                    values[i] = 0;

                int chunkSize = 5000;
                byte[] buffer = new byte[chunkSize];

                Stream _stream = File.OpenRead(path);
                int count = 18; int num = 0;
                string topic = "";
                int iValue = 0;
                {
                    // read bytes from input stream
                    int bytesRead = _stream.Read(buffer, 0, chunkSize);
                    _stream.Close();
                    num = (int)buffer[17];
                    for (int i = 0; i < num; i++)
                        Data.id += (char)buffer[count++];
                    count += 4;
                    Data.id = "";
                    do
                    {
                        string strcount = "";
                        topic = "";
                        count++;
                        strcount += buffer[count++];
                        num = int.Parse(strcount);

                        char[] value = new char[num];
                        for (int i = 0; i < num; i++)
                            topic += (char)buffer[count++];

                        count += 3;
                        strcount = "";
                        if (topic.Equals("menu_id"))
                        {
                            for (int i = 0; i < 18; i++)
                                Data.id += (char)buffer[count++];

                            count += 1;
                        }
                        else
                        {
                            do
                            {
                                byte b = buffer[count++];
                                if (b == 0)
                                    break;
                                iValue = (int)b - 48;
                                strcount += iValue.ToString();
                            } while (true);

                            string _values = "";
                            for (int j = 0; j < 20; j++)
                            {
                                _values = string.Format("value{0:00}", j + 1);
                                if (topic.Equals(_values))
                                {
                                    values[j] = int.Parse(strcount);
                                    break;
                                }
                            }
                          }

                        if (bytesRead <= count)
                            break;
                    }
                    while (true);
                    Data.Values = values;

                    return Data;
                }
            }
            catch (Exception ex)
            {
                logger.Info(ex.Message.ToString());
            }

            return Data;
        }

        public struct FlashData
        {
            public string id;
            public int[] Values;
        };
    }
}
