using System;
using System.Xml;  
using System.Configuration;
using System.Reflection;

using NLog;

namespace DigitalSignage.PlayAgent
{
    public class PlayerSettings : SettingBase
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        private static PlayerSettings instance = null;

        public static String ConfigFilePath
        {
            get
            {
                return GetInstance.ConfigPath;
            }
            set
            {
                GetInstance.ConfigPath = value;
            }
        }

        public PlayerSettings(String config_path)
            : base(config_path)
        {
            instance = this;
        }

        public static PlayerSettings GetInstance 
        {
            get 
            {
                if(instance == null)
                {
                    instance = new PlayerSettings(PlayerSettings.ConfigFilePath);
                }

                return instance;
            }
        }
        
    }

}