using System;
using System.Xml;  
using System.Configuration;
using System.Reflection;

using NLog;

namespace DigitalSignage.PlayAgent
{
    public class SubtitleSettings : SettingBase
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        private static SubtitleSettings instance = null;

        public static String ConfigFilePath
        {
            get
            {
                return GetInstance.ConfigPath;
            }
            set
            {
                GetInstance.ConfigPath = value;
            }
        }

        public SubtitleSettings(String config_path)
            : base(config_path)
        {
            instance = this;
        }

        public static SubtitleSettings GetInstance 
        {
            get 
            {
                if(instance == null)
                {
                    instance = new SubtitleSettings(SubtitleSettings.ConfigFilePath);
                }

                return instance;
            }
        }
    }
}