using System;
using System.Xml;  
using System.Configuration;
using System.Reflection;

using NLog;

namespace DigitalSignage.PlayAgent
{
    public class SensorSettings : SettingBase
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        private static SensorSettings instance = null;

        public static String ConfigFilePath
        {
            get
            {
                return GetInstance.ConfigPath;
            }
            set
            {
                GetInstance.ConfigPath = value;
            }
        }

        public SensorSettings(String config_path)
            : base(config_path)
        {
            instance = this;
        }

        public static SensorSettings GetInstance 
        {
            get 
            {
                if(instance == null)
                {
                    instance = new SensorSettings(SensorSettings.ConfigFilePath);
                }

                return instance;
            }
        }
        
    }

}