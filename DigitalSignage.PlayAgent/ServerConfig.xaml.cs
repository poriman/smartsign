﻿using System;
using System.Threading;
using System.Collections.Generic;
using System.Windows.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;

using NLog;

using DigitalSignage.Common;
using DigitalSignage.SerialKey;

namespace DigitalSignage.PlayAgent
{
    /// <summary>
    /// Interaction logic for Serve.xaml
    /// </summary>
    partial class ServerConfig : Window
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        Config cfg;
        public bool result = false;
        private bool process = true;
        private Thread update = null;
		private Thread thCheck = null;
        private string hostname = "";

		private string SAAS_HOSTNAME = "saas.myivision.com";

		WCFAuthoring wcfObject = WCFAuthoring.GetWCFObject;

		BroadcastClient client = null;


		bool _bSearch = false;

		public ServerConfig( Config config, bool bDoSearch)
        {
            InitializeComponent();
			
			cfg = config;
			_bSearch = bDoSearch;
       }

		private void Load()
		{

			NESerialKey._ProductCode _code = cfg.ProductCode;

			switch(_code)
			{
				case DigitalSignage.SerialKey.SerialKey._ProductCode.SoftwareAsaService:
					{
						spHostPanel.Visibility = Visibility.Collapsed;
						rdHost.Height = new GridLength(0);
						Hostname = SAAS_HOSTNAME;
						break;
					}
				case DigitalSignage.SerialKey.SerialKey._ProductCode.StandaloneEdition:
					{
						spHostPanel.Visibility = Visibility.Collapsed;
						rdHost.Height = new GridLength(0);
						Hostname = "127.0.0.1";
						break;
					}
				case DigitalSignage.SerialKey.SerialKey._ProductCode.NetworkEdition:
					{
						btnSearch.Visibility = Visibility.Visible;						
						break;
					}
			}
		}

        #region properties
		public String GroupID
		{
			get
			{
				return group_id.Text;
			}
			set
			{
				group_id.Text = value;
			}
		}

		public String PlayerID
		{
			get
			{
				return media_id.Text;
			}
			set
			{
				media_id.Text = value;
			}
		}
		
		public String Hostname
        {
            get
            {
                return hostname;
            }
            set
            {
                string serverAddress = value;
                int pos = serverAddress.IndexOf(":");
                if ( pos > -1 )
                    serverAddress = serverAddress.Substring(0, pos);
                hostnameUI.Text = serverAddress;
                hostname = serverAddress + ":888";

           }
        }
        #endregion

		private void btnOKUI_Click(object sender, RoutedEventArgs e)
        {
			result = true;
			this.Close();
		}

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
				Load();

				try
				{
					wcfObject.LastErrorMessageUpdated += new LastErrorMessageUpdatedEventHandler(wcfObject_LastErrorMessageUpdated);
					wcfObject.StateChanged += new StateChangedEventHandler(wcfObject_StateChanged);
					tbServerReturns.Text = wcfObject.LastErrorMessage;

					client = new BroadcastClient();

					client.BeginBroadCast += new EventHandler(client_BeginBroadCast);
					client.EndBroadCast += new EventHandler(client_EndBroadCast);
					client.RecvMessage += new SocketEventHandler(client_RecvMessage);
					client.Initialiaze();

					if (_bSearch)
					{
						btnSearch_Click(this, null);
					}

				}
				catch
				{
					MessageBox.Show(Properties.Resources.mbErrorFirewall, Properties.Resources.titleServerConfiguration, MessageBoxButton.OK, MessageBoxImage.Error);
				}

				//	20081107 hsshin 미디어아이디, 그룹아이디 추가
				PlayerID = cfg.PlayerId;

				string server = cfg.ServerPath;
                int pos = server.IndexOf("//");
                if (pos > -1)
                    server = server.Substring(pos+2);
                pos = server.IndexOf("/");
                if (pos > -1)
                    server = server.Substring(0,pos);
                Hostname = server;
				srvstateTextUI.Text = (cfg.ServerOnline && cfg.HasWCFAuthoring ? Properties.Resources.labelOnline : Properties.Resources.labelOffline);
                ftpstate.Text = Properties.Resources.labelOffline + " ( " + cfg.FTPServer + " )";
				
				if (update == null)
				{
					update = new Thread(new ThreadStart(this.Scan));
					update.Start();
				}
            }
            catch (Exception err)
            {
                logger.Error(err.Message);
            }
        }

		void wcfObject_StateChanged(object sender, StateChangedEventArgs e)
		{
			if(e.State != System.ServiceModel.CommunicationState.Opening) StopAnimation();
		}

		void wcfObject_LastErrorMessageUpdated(object sender, LastErrorMessageUpdatedEventArgs e)
		{
			System.Windows.Forms.MethodInvoker method = delegate
			{
				tbServerReturns.Text = e.LastErrorMessage;
			};
			if (!this.Dispatcher.CheckAccess())
			{
				try
				{
					this.Dispatcher.BeginInvoke(method);
				}
				catch (Exception exx)
				{
					logger.Error(exx + "");
				}
			}
			else
			{
				method.Invoke();
			}
		}

		#region Broadcast Events
		void client_RecvMessage(object sender, SocketEventArgs e)
		{
			System.Windows.Forms.MethodInvoker method = delegate
			{
				string ipAddress = e.IPEndpoint.Address.ToString();

				bool bContains = false;
				foreach (ComboBoxItem item in hostnameUI.Items)
				{
					if (((String)item.Content).Equals(ipAddress))
					{
						bContains = true;
						break;
					}
				}

				if (!bContains)
				{
					string[] arrRecv = e.ReceivedData.Split('|');
					ComboBoxItem item = new ComboBoxItem();
					item.Content = ipAddress;
					item.DataContext = arrRecv.Length > 1 ? arrRecv[1] : e.ReceivedData;
					item.ToolTip = arrRecv[0];

					hostnameUI.Items.Add(item);
				}
			};

			if (!this.Dispatcher.CheckAccess())
			{
				try
				{
					this.Dispatcher.BeginInvoke(method);
				}
				catch (Exception exx)
				{
					logger.Error(exx + "");
				}
			}
			else
			{
				method.Invoke();
			}
		}

		void client_EndBroadCast(object sender, EventArgs e)
		{
			System.Windows.Forms.MethodInvoker method = delegate
			{
				busyAnimation.Visibility = Visibility.Collapsed;
				try
				{ if (String.IsNullOrEmpty(hostnameUI.Text)) hostnameUI.SelectedIndex = 0; }
				catch { }
			};

			if (!this.Dispatcher.CheckAccess())
			{
				try
				{
					this.Dispatcher.BeginInvoke(method);
				}
				catch (Exception exx)
				{
					logger.Error(exx + "");
				}
			}
			else
			{
				method.Invoke();
			}
		}

		void client_BeginBroadCast(object sender, EventArgs e)
		{
			System.Windows.Forms.MethodInvoker method = delegate
			{
				busyAnimation.Visibility = Visibility.Visible;
			};

			if (!this.Dispatcher.CheckAccess())
			{
				try
				{
					this.Dispatcher.BeginInvoke(method);
				}
				catch (Exception exx)
				{
					logger.Error(exx + "");
				}
			}
			else
			{
				method.Invoke();
			}
		}
		#endregion

		private void btnSaveSetting_Click(object sender, RoutedEventArgs e)
        {
			if (MessageBoxResult.No == MessageBox.Show(Properties.Resources.mbWarnClearPlaylist, Properties.Resources.titleServerConfiguration, MessageBoxButton.YesNo, MessageBoxImage.Warning))
				return;

			if (cfg.WCFAuthoring) WCFAuthoring.GetWCFObject.Disconnect();


			cfg.WCFEndpoint = String.Format("net.tcp://{0}:40526/INetworkHost/tcp", hostnameUI.Text);

			cfg.ServerPathFromFile = hostname;
			cfg.PlayerIdFromFile = PlayerID;

			try
			{
				foreach(ComboBoxItem item in this.hostnameUI.Items)
				{
					if (item.Content.Equals(hostnameUI.Text))
					{
						cfg.ServerInitialCode = item.DataContext.ToString();
						break;
					}
				}

			}
			catch{ }

			if (thCheck != null && thCheck.IsAlive)
			{
				thCheck.Abort();
				thCheck = null;

				StopAnimation();
			}
			
			thCheck = new Thread(new ThreadStart(this.checkShdlrSrv));
			thCheck.Start();
			//	성공하면 FTP도 검사한다.
			if(update == null)
			{
				update = new Thread(new ThreadStart(this.Scan));
				update.Start();
			}
        }

        #region Async & thread routines
        public void checkShdlrSrv()
        {
            // checking connection to server
            // try to open remote CmdReceiver and get server version
            try
            {
                StartAnimation();

				if (cfg.HasWCFAuthoring)
					wcfObject.Disconnect();

				if (cfg.HasWCFAuthoring = wcfObject.ConnectUsingSuspend())
				{
					IServerCmdReceiver serverCmdReceiver = null;
                    serverCmdReceiver = (IServerCmdReceiver)Activator.GetObject(
                        typeof(IServerCmdReceiver), "tcp://" + hostname + "/CmdReceiverHost/rt");
					long ServerVersion = serverCmdReceiver.ProcessSimpleCommand(CmdReceiverCommands.CmdGetVersion);
					if (ServerVersion < 1) throw new Exception("Server version check returns incorrect answer");
					SetSOnlineState();

					//	hsshin 떨어졌다 붙으면 다시 씽크를 맞춘다
					cfg.LastSyncFromFile = 0;
					cfg.localTasksList.CleanAllTasks();
                    cfg.localTasksList.CleanAllSyncTaskList();
                    wcfObject.AddInitializeTasks();

					cfg.KillPlayer();

				}
				else
					SetSOfflineState();
			}
            catch (Exception err)
            {
                SetSOfflineState();
                logger.Error(err + "");
            }
            StopAnimation();
        }

		public void checkShdlrSrvOnly()
		{
			// checking connection to server
			// try to open remote CmdReceiver and get server version
			try
			{
				StartAnimation();

				if (cfg.HasWCFAuthoring = wcfObject.ConnectUsingSuspend())
				{
                    IServerCmdReceiver serverCmdReceiver = null;
                    serverCmdReceiver = (IServerCmdReceiver)Activator.GetObject(
                        typeof(IServerCmdReceiver), "tcp://" + hostname + "/CmdReceiverHost/rt");
					long ServerVersion = serverCmdReceiver.ProcessSimpleCommand(CmdReceiverCommands.CmdGetVersion);
					if (ServerVersion < 1) throw new Exception("Server version check returns incorrect answer");
					SetSOnlineState();
				}
				else
					SetSOfflineState();

			}
			catch (Exception err)
			{
				SetSOfflineState();
				logger.Error(err + "");
			}
			finally
			{
				StopAnimation();
			}
		}

        public void Scan()
        {
            try
            {
                while (process)
                {

					CheckConfig();

                    if (cfg.FTPServer != null)
                    {
                        FTPFunctions ftp = new FTPFunctions(cfg.FTPServer, "", cfg.FTPUser, cfg.FTPPassword, cfg.FTP_Timeout);
						if (ftp.CheckState())
							SetOnlineState();
						else
							SetOfflineState();
                    }
                    else
                        SetOfflineState();
                    Thread.Sleep(3000);
                }
            }
            catch (Exception e)
            {
                logger.Error(e + "");
            }
        }

        delegate void setOnlineStateCB();
        public void SetOnlineState()
        {
            try
            {
                if (!this.Dispatcher.CheckAccess())
                {
                    this.Dispatcher.BeginInvoke(DispatcherPriority.Render,
                       new setOnlineStateCB(SetOnlineState));
                }
                else
                    ftpstate.Text = String.Format("{0} ({1})", Properties.Resources.labelOnline, cfg.FTPServer);
				
            }
            catch (Exception e)
            {
                logger.Error(e + "");
            }
        }

		delegate void checkConfigCB();
		public void CheckConfig()
		{
			try
			{
				if (!this.Dispatcher.CheckAccess())
				{
					this.Dispatcher.BeginInvoke(DispatcherPriority.Render,
					   new checkConfigCB(CheckConfig));
				}
				else
				{
					if(cfg.WCFAuthoring)
						SetSOnlineState();
					else 
						SetSOfflineState();

// 					if (!this.PlayerID.Equals(cfg.PlayerId))
// 					{
// 						this.PlayerID = cfg.PlayerId;
// 					}
// 
// 					if (!this.GroupID.Equals(cfg.GroupId))
// 					{
// 						this.GroupID = cfg.GroupId;
// 					}
// 
// 					if (!cfg.ServerPath.Contains(this.Hostname))
// 					{
// 						this.Hostname = cfg.ServerPath.Replace("tcp://", "");
// 					}
				}
			}
			catch (Exception e)
			{
				logger.Error(e + "");
			}
		}

        delegate void setOfflineStateCB();
        public void SetOfflineState()
        {
            try
            {
                if (!this.Dispatcher.CheckAccess())
                {
                    this.Dispatcher.BeginInvoke(DispatcherPriority.Render,
                       new setOfflineStateCB(SetOfflineState));
                }
                else
					ftpstate.Text = String.Format("{0} ({1})", Properties.Resources.labelOffline, cfg.FTPServer);
            }
            catch (Exception e)
            {
                logger.Error(e + "");
            }
        }

        public void SetSOnlineState()
        {
            try
            {
                if (!this.Dispatcher.CheckAccess())
                {
                    this.Dispatcher.BeginInvoke(DispatcherPriority.Render,
                       new setOfflineStateCB(SetSOnlineState));
                }
                else
                {
                    okUI.IsEnabled = true;
					srvstateTextUI.Text = Properties.Resources.labelOnline;

                }
            }
            catch (Exception e)
            {
                logger.Error(e + "");
            }
        }

        public void SetSOfflineState()
        {
            try
            {
                if (!this.Dispatcher.CheckAccess())
                {
                    this.Dispatcher.BeginInvoke(DispatcherPriority.Render,
                       new setOfflineStateCB(SetSOfflineState));
                }
                else
                {
					srvstateTextUI.Text = Properties.Resources.labelOffline;
                }
            }
            catch (Exception e)
            {
                logger.Error(e + "");
            }
        }

        public void StartAnimation()
        {
            try
            {
                if (!this.Dispatcher.CheckAccess())
                {
                    this.Dispatcher.BeginInvoke(DispatcherPriority.Render,
                       new setOfflineStateCB(StartAnimation));
                }
                else
                {
                    srvstateTextUI.Text = Properties.Resources.labelChecking;
                    Storyboard board = (Storyboard)srvstateTextUI.Resources["srvstateAnim"];
                    board.Duration = new Duration(new TimeSpan(0, 0, 2));
                    board.RepeatBehavior = new RepeatBehavior(1000);
                    board.AutoReverse = true;
                    board.Begin(srvstateTextUI);
                    btnSaveSetting.IsEnabled = false;
                    okUI.IsEnabled = false;
                }
            }
            catch (Exception e)
            {
                logger.Error(e + "");
            }
        }
        public void StopAnimation()
        {
            try
            {
                if (!this.Dispatcher.CheckAccess())
                {
                    this.Dispatcher.BeginInvoke(DispatcherPriority.Render,
                       new setOfflineStateCB(StopAnimation));
                }
                else
                {
                    Storyboard board = (Storyboard)srvstateTextUI.Resources["srvstateAnim"];
                    board.Duration = new Duration(new TimeSpan(0, 0, 0));
                    board.Begin(srvstateTextUI);
                    btnSaveSetting.IsEnabled = true;
					okUI.IsEnabled = true;

                }
            }
            catch (Exception e)
            {
                logger.Error(e + "");
            }
        }
        #endregion

        private void hostnameUI_KeyUp(object sender, KeyEventArgs e)
        {
			hostname = hostnameUI.Text + ":888";
        }

		private void hostnameUI_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			try
			{
				hostname = ((ComboBoxItem)hostnameUI.SelectedItem).Content + ":888";
			}
			catch
			{
			}
		}

		private void Window_Closing(object sender, CancelEventArgs e)
		{
			process = false;

			if (update != null && update.IsAlive)
			{
				update.Abort();
				update = null;
			}
			if(thCheck != null && thCheck.IsAlive)
			{
				thCheck.Abort();
				thCheck = null;
			}

			if(client != null)
			{
				client.Dispose();
				client = null;
			}


		}

		private void onlycheckUI_Click(object sender, RoutedEventArgs e)
		{
			if (cfg.WCFAuthoring) wcfObject.Disconnect();

			cfg.WCFEndpoint = String.Format("net.tcp://{0}:40526/INetworkHost/tcp", hostnameUI.Text);

			cfg.ServerPath = hostname;
			cfg.PlayerId = PlayerID;
			
			if (thCheck != null && thCheck.IsAlive)
			{
				thCheck.Abort();
				thCheck = null;

				StopAnimation();
			}

			thCheck = new Thread(new ThreadStart(this.checkShdlrSrvOnly));
			thCheck.Start();
			//	성공하면 FTP도 검사한다.
			if (update == null)
			{
				update = new Thread(new ThreadStart(this.Scan));
				update.Start();
			}
		}

		private void btnSearch_Click(object sender, RoutedEventArgs e)
		{
			client.BroadCast();
		}

    }
}
