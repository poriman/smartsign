﻿using System;

namespace WeatherReaderMVC
{
    public enum WindDirections { N, NNE, NE, ENE, E, ESE, SE, SSE, S, SSW, SW, WSW, W, WNW, NW, NNW };

    public class Wind
    {
        private double? speed;
        public double? Speed 
        {
            get { return speed; }
            set { speed = value; }
        }

        private WindDirections direction;
        public WindDirections Direction 
        {
            get { return direction; }
            set { direction = value; }
        }
    }
}
