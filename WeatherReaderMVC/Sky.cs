﻿using System;
using System.Windows.Controls;

namespace WeatherReaderMVC
{
    public class Sky
    {
        private string skyCondition;
        public string SkyCondition 
        {
            get { return skyCondition; }
            set { skyCondition = value; }
        }

        private String skyImageUrl;
        public String SkyImageUrl
        {
            get { return skyImageUrl; }
            set { skyImageUrl = value; }
        }
    }
}
