﻿using System;

namespace WeatherReaderMVC
{
    public class WeatherPoint : Weather
    {
        public WeatherPoint(UnitsSystems u) : base (u)
        {
        }

        private DateTime time;
        public DateTime Time
        {
            get { return time; }
            set { time = value; }
        }

        private double temperature;
        public double Temperature
        {
            get { return Math.Floor(temperature); }
            set { temperature = value; }
        }

        public string TemperatureString
        {
            get { return Temperature.ToString() + "°"; }
        }

        private double feelsLike;
        public double FeelsLike
        {
            get { return Math.Floor(feelsLike); }
            set { feelsLike = value; }
        }
    }
}
