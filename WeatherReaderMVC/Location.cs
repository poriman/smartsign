﻿using System;

namespace WeatherReaderMVC
{
    public class Location
    {
        private string fullName;
        public string FullName 
        { 
            get { return fullName; } 
            set { fullName = value; }
        }

        private string country;
        public string Country {
            get { return country; }
            set { country = value; }
        }

        private string state;
        public string State 
        {
            get { return state; } 
            set { state = value; }
        }

        private string city;
        public string City
        { 
            get { return city; } 
            set { city = value; } 
        }

        private int? zipCode;
        public int? ZipCode 
        {
            get { return zipCode; } 
            set { zipCode = value; }
        }

        private double? longitude;
        public double? Longitude 
        { 
            get { return longitude; } 
            set { longitude = value; } 
        }

        private double? latitude;
        public double? Latitude 
        {
            get { return latitude; } 
            set { latitude = value; }
        }
    }
}
