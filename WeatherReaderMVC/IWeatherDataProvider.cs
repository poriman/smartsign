﻿using System;
using System.Collections.Generic;

namespace WeatherReaderMVC
{
    public enum UnitsSystems { Imperial, Metric};

    public interface IWeatherDataProvider
    {
        List<Location> QueryLocations(string query);
        WeatherReport GetLatestWeatherReport(Location location, UnitsSystems us, bool ShowImages);
    }
}
