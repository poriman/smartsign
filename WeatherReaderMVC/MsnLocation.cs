﻿using System;

namespace WeatherReaderMVC
{
    public class MsnLocation : Location
    {
        private string locationCode;
        public string LocationCode
        { 
            get { return locationCode; } 
            set { locationCode = value; } 
        }
    }
}
