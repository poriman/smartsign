﻿using System;

namespace WeatherReaderMVC
{
    public class WeatherRange : Weather
    {

        #region Constructors

        public WeatherRange(UnitsSystems u) : base (u)
        {
        }

        #endregion

        #region WeatherProperties

        private DateTime startTime;
        public DateTime StartTime
        {
            get { return startTime; }
            set { startTime = value; }
        }

        private DateTime endTime;
        public DateTime EndTime
        {
            get { return endTime; }
            set { endTime = value; }
        }

        private double highTemperature;
        public double HighTemperature 
        {
            get { return Math.Floor(highTemperature); }
            set { highTemperature = value; }
        }

        private double lowTemperature;
        public double LowTemperature
        {
            get { return Math.Floor(lowTemperature); }
            set { lowTemperature = value; }
        }

        #endregion

        #region UIProperties

        // These properties are provided for convenient databinding in UI

        public string HighTemperatureString
        {
            get { return HighTemperature.ToString() + "°"; }
        }

        public string LowTemperatureString
        {
            get { return LowTemperature.ToString() + "°"; }
        }

        #endregion
    }
}
