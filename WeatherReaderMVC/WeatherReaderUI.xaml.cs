﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Threading;
using System.Windows.Threading;
using System.ComponentModel;
using System.Windows.Controls.Primitives;
using System.Windows.Media.Animation;
using System.Diagnostics;
using System.Windows.Documents;
using System.Windows.Shapes;
using System.Windows.Media;

using DigitalSignage.Common;
using System.Windows.Media.Imaging;

namespace WeatherReaderMVC
{
    /// <summary>
    /// Interaction logic for WeatherReaderUI.xaml
    /// </summary>
    public partial class WeatherReaderUI : UserControl, ITextContainer, IDecoratable
    {

        #region PrivateFields
        // text drawing brush
        private Brush textBrush = Brushes.White;

        private WeatherReport report;
        private IWeatherDataProvider weatherProvider;
        private ObjectDataProvider searchResultsProvider;
        //private ObjectDataProvider weatherReportProvider;
        private Location currentLocation;
        private UnitsSystems unitsSystem;

        // Members used for threading the search
		public string CurrentQuery
		{
			get { return pendingSearchQuery; }
		}
        private string pendingSearchQuery;
        // This could be considered the lock on the searchResultsProvider
        private bool aSearchRequestIsInProgress = false;
        // This could be considered the lock on the weather data update
        private bool aWeatherRequestIsInProgress = false;

        // No need to thread the weather retrieval process
        //// Members used for threading the weather
        //private bool aWeatherRequestIsInProgress = false;

        #endregion

        #region properties
        public bool _ShowImages = true;

        public bool ShowImages
        {
            get
            {
                return _ShowImages;
            }
            set
            {
                _ShowImages = value;
                if (!_ShowImages)
                {
                    backgroundImage.Source = new System.Windows.Media.Imaging.BitmapImage();
                    backgroundImage2.Source = new System.Windows.Media.Imaging.BitmapImage();
                    backgroundImage3.Source = new System.Windows.Media.Imaging.BitmapImage();
                }
                else
                {
                    backgroundImage.Source = report.BackgroundImage;
                    backgroundImage2.Source = report.BackgroundImage2;
                    backgroundImage3.Source = report.BackgroundImage3;
                }
            }
        }

        public String LocationString
        {
            get { return currentLocation.Country + "," + currentLocation.City; }
            set
            {
                int coma = value.IndexOf(',');
                if ( coma < 1 ) return;
                currentLocation.Country = value.Substring(0, coma);
                currentLocation.City = value.Substring(coma + 1);
				loadWeatherData(currentLocation.Country + ", " + currentLocation.City);
            }
        }

        public int Units
        {
            get { return (unitsSystem == UnitsSystems.Imperial ? 1 : 2); }
            set
            {
                if (value == 1)
                    unitsSystem = UnitsSystems.Imperial;
                else
                    unitsSystem = UnitsSystems.Metric;
				loadWeatherData(currentLocation.Country + ", " + currentLocation.City);
            }
        }
        #endregion

        #region Constructors

        public WeatherReaderUI()
        {   
            InitializeComponent();
            currentLocation = new MsnLocation();
            loadSettings();

            weatherProvider = new MsnWeatherDataProvider();
            searchResultsProvider = new ObjectDataProvider();
            //weatherReportProvider = new ObjectDataProvider();
            report = new WeatherReport();
//             loadWeatherData(currentLocation.City + ", " + currentLocation.Country);
            SetFontBrush(Brushes.White);
        }
        #endregion

        #region PrivateMethods

        private void loadSettings ()
        {
            ((MsnLocation)currentLocation).LocationCode = Settings1.Default.LocationCode;
            currentLocation.FullName = Settings1.Default.FullName;
            currentLocation.Country = Settings1.Default.Country;
            currentLocation.State = Settings1.Default.State;
            currentLocation.City = Settings1.Default.City;
            currentLocation.ZipCode = Settings1.Default.ZipCode;
            currentLocation.Longitude = Settings1.Default.Longitude;
            currentLocation.Latitude = Settings1.Default.Latitude;

            unitsSystem = (Settings1.Default.DegreeType == 'F') ? UnitsSystems.Imperial : UnitsSystems.Metric;
        }

        private void storeSettings()
        {
            Settings1.Default.LocationCode = ((MsnLocation)currentLocation).LocationCode;
            Settings1.Default.FullName = currentLocation.FullName;
            Settings1.Default.Country = currentLocation.Country;
            Settings1.Default.State = currentLocation.State;
            Settings1.Default.City = currentLocation.City;
            

            // TODO: Clean up the following 3 messy lines (use correct null values)
            int zip; double lon, lat;
            Settings1.Default.ZipCode = (int.TryParse(currentLocation.ZipCode.ToString(), out zip)) ? zip : -1;
            Settings1.Default.Longitude = (double.TryParse(currentLocation.Longitude.ToString(), out lon)) ? lon : -1000;
            Settings1.Default.Latitude = (double.TryParse(currentLocation.Latitude.ToString(), out lat)) ? lat : -1000;

            Settings1.Default.DegreeType = (UnitsSystems.Imperial == unitsSystem) ? 'F' : 'C';
        }

        private void WeatherReader_Unloaded(object sender, EventArgs e)
        {
            storeSettings();
            Settings1.Default.Save();
        }

        #endregion

        #region Async routines
        // This delegate enables asynchronous calls for setting
        // the weather info to control
        delegate void outNewDataCB();
        private void outNewData()
        {
            try
            {
                if (!this.Dispatcher.CheckAccess())
                {
                    this.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
                       new outNewDataCB(outNewData) );
                }
                else
                {
                    oneDayName.Text = report.Forecast[0].StartTime.DayOfWeek.ToString();
                    oneDayHi.Text = report.Forecast[0].HighTemperatureString;
                    oneDayLo.Text = report.Forecast[0].LowTemperatureString;

					BitmapImage feedSkyImage = new BitmapImage(new Uri(report.Forecast[0].SkyCondition.SkyImageUrl, UriKind.RelativeOrAbsolute));
					oneDayImage.Source = feedSkyImage;

                    twoDayName.Text = report.Forecast[1].StartTime.DayOfWeek.ToString();
                    twoDayHi.Text = report.Forecast[1].HighTemperatureString;
                    twoDayLo.Text = report.Forecast[1].LowTemperatureString;

					feedSkyImage = new BitmapImage(new Uri(report.Forecast[1].SkyCondition.SkyImageUrl, UriKind.RelativeOrAbsolute));
					twoDayImage.Source = feedSkyImage;

                    threeDayName.Text = report.Forecast[2].StartTime.DayOfWeek.ToString();
                    threeDayHi.Text = report.Forecast[2].HighTemperatureString;
                    threeDayLo.Text = report.Forecast[2].LowTemperatureString;

					feedSkyImage = new BitmapImage(new Uri(report.Forecast[2].SkyCondition.SkyImageUrl, UriKind.RelativeOrAbsolute));
					threeDayImage.Source = feedSkyImage;

                    currentTemperatureText.Text = report.LatestWeather.TemperatureString;
                    currentLocationText.Text = currentLocation.City;

                    if (_ShowImages)
                    {
                        backgroundImage.Source = report.BackgroundImage;
                        backgroundImage2.Source = report.BackgroundImage2;
                        backgroundImage3.Source = report.BackgroundImage3;
                    }
                }
            }
            catch (Exception e)
            {
                //logger.appendDebug(e + "");
            }
        }


        private BackgroundWorker backgroundWeatherRequest;
        private void loadWeatherData(string query)
        {
//             if (!aWeatherRequestIsInProgress)
//             {
				busyAnimation.Visibility = Visibility.Visible;
                
				aWeatherRequestIsInProgress = true;
                backgroundWeatherRequest = new BackgroundWorker();
                backgroundWeatherRequest.DoWork += new DoWorkEventHandler(handleWeatherRequest);
                backgroundWeatherRequest.RunWorkerAsync(query);
				backgroundWeatherRequest.RunWorkerCompleted += new RunWorkerCompletedEventHandler(backgroundWeatherRequest_RunWorkerCompleted);
//             }
        }

		void backgroundWeatherRequest_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
		{
			busyAnimation.Visibility = Visibility.Collapsed;
		}

        
        private void handleWeatherRequest(object sender, DoWorkEventArgs e)
        {
            //weatherReportProvider = FindResource("weatherReport") as ObjectDataProvider;
            List<Location> locations = weatherProvider.QueryLocations(e.Argument.ToString());
            if ( locations.Count > 0 )
                currentLocation = locations[0];
            else
            {
                // switching to default - USA,Redmond
                ((MsnLocation)currentLocation).LocationCode = Settings1.Default.LocationCode;
                currentLocation.FullName = Settings1.Default.FullName;
                currentLocation.Country = Settings1.Default.Country;
                currentLocation.State = Settings1.Default.State;
                currentLocation.City = Settings1.Default.City;
                currentLocation.ZipCode = Settings1.Default.ZipCode;
                currentLocation.Longitude = Settings1.Default.Longitude;
                currentLocation.Latitude = Settings1.Default.Latitude;
            }
			report = weatherProvider.GetLatestWeatherReport(currentLocation, unitsSystem, _ShowImages);
			aWeatherRequestIsInProgress = false;
			outNewData();
		}

		private BackgroundWorker backgroundHandleSearchAndDisplayRequest;
		public void QueryWeatherData(string query)
		{
			loadWeatherData(query);
		}

		private BackgroundWorker backgroundHandleSearchRequest;
		private void searchQueryTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            string query = ((TextBox)sender).Text.Trim();

			if (!(query.Equals(pendingSearchQuery)))
			{
				pendingSearchQuery = query;
				if (!aSearchRequestIsInProgress)
				{
					aSearchRequestIsInProgress = true;
					backgroundHandleSearchRequest = new BackgroundWorker();
					backgroundHandleSearchRequest.DoWork += new DoWorkEventHandler(handleSearchRequest);
					backgroundHandleSearchRequest.RunWorkerAsync();
				}
			}
		}

        private void handleSearchRequest(object sender, DoWorkEventArgs e)
        {
            string currentSearchQuery = pendingSearchQuery;
            searchResultsProvider.ObjectInstance = weatherProvider.QueryLocations(currentSearchQuery);
            if (currentSearchQuery.Equals(pendingSearchQuery))
                aSearchRequestIsInProgress = false;
            else
                handleSearchRequest(sender, e);
        }
        #endregion

        #region ITextContainer Members

        public void SetFontSize(double fontsize)
        {
            threeDayName.FontSize = fontsize;
            twoDayName.FontSize = fontsize;
            oneDayName.FontSize = fontsize;
            threeDayHi.FontSize = fontsize;
            threeDayLo.FontSize = fontsize;
            twoDayHi.FontSize = fontsize;
            twoDayLo.FontSize = fontsize;
            oneDayHi.FontSize = fontsize;
            oneDayLo.FontSize = fontsize;
        }

        public void SetFontBrush(Brush fontcolor)
        {
            threeDayName.Foreground = fontcolor;
            twoDayName.Foreground = fontcolor;
            oneDayName.Foreground = fontcolor;
            threeDayHi.Foreground = fontcolor;
            threeDayLo.Foreground = fontcolor;
            twoDayHi.Foreground = fontcolor;
            twoDayLo.Foreground = fontcolor;
            oneDayHi.Foreground = fontcolor;
            oneDayLo.Foreground = fontcolor;
            currentLocationText.Foreground = fontcolor;
            currentTemperatureText.Foreground = fontcolor;
        }

        public double GetFontSize()
        {
            return oneDayLo.FontSize;
        }

        public Brush GetFontBrush()
        {
            return oneDayHi.Foreground;
        }

        public void SetFontFamily(FontFamily family)
        {
            oneDayName.FontFamily = family;
            twoDayName.FontFamily = family;
            threeDayName.FontFamily = family;
            threeDayHi.FontFamily = family;
            threeDayLo.FontFamily = family;
            twoDayHi.FontFamily = family;
            twoDayLo.FontFamily = family;
            oneDayHi.FontFamily = family;
            oneDayLo.FontFamily = family;
        }

        public void SetFontWeight(FontWeight fontWeight)
        {
            oneDayName.FontWeight = fontWeight;
            twoDayName.FontWeight = fontWeight;
            threeDayName.FontWeight = fontWeight;
            threeDayHi.FontWeight = fontWeight;
            threeDayLo.FontWeight = fontWeight;
            twoDayHi.FontWeight = fontWeight;
            twoDayLo.FontWeight = fontWeight;
            oneDayHi.FontWeight = fontWeight;
            oneDayLo.FontWeight = fontWeight;
        }

        public FontFamily GetFontFamily()
        {
            return oneDayHi.FontFamily;
        }

        public FontWeight GetFontWeight()
        {
            return oneDayHi.FontWeight;
        }

		void ITextContainer.SetDescFontSize(double fontsize)
		{
			throw new NotImplementedException();
		}

		void ITextContainer.SetDescFontBrush(Brush fontcolor)
		{
			throw new NotImplementedException();
		}

		double ITextContainer.GetDescFontSize()
		{
			throw new NotImplementedException();
		}

		Brush ITextContainer.GetDescFontBrush()
		{
			throw new NotImplementedException();
		}

		FontFamily ITextContainer.GetDescFontFamily()
		{
			throw new NotImplementedException();
		}

		void ITextContainer.SetDescFontFamily(FontFamily family)
		{
			throw new NotImplementedException();
		}

		FontWeight ITextContainer.GetDescFontWeight()
		{
			throw new NotImplementedException();
		}

		void ITextContainer.SetDescFontWeight(FontWeight fontWeight)
		{
			throw new NotImplementedException();
		}
        #endregion

        #region IDecoratable
        public void SetBorderBrush(Brush borderbrush)
        {
            //mainGrid.BorderBrush = borderbrush;
        }

        public void SetBorderThickness(Thickness borderThickness)
        {
            //mainGrid.BorderThickness = borderThickness;
        }

        public void SetBackround(Brush background)
        {
            mainGrid.Background = background;
        }

        public void SetCornerRadius(double cornerRadius)
        {
            // not implemented
        }

        public Brush GetBorderBrush()
        {
            return new SolidColorBrush(Color.FromArgb(255, 255, 255, 255));
        }

        public Brush GetBackgound()
        {
            if (mainGrid.Background == null)
                return new SolidColorBrush();
            else return mainGrid.Background;
        }

        public Thickness GetBorderThickness()
        {
            return new Thickness(0);
        }

        public double GetCornerRadius()
        {
            return -1; // not implemented
        }

        #endregion
    }
}
