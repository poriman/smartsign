﻿using System;

namespace SerialPortController
{
	/// <summary>
	/// 지원 가능한 시리얼 포트 장비
	/// </summary>
	public enum SupportedSerialPortDevice : int
	{
		/// <summary>
		/// 장비 없음
		/// </summary>
		deviceNone = 0,
		/// <summary>
		/// 신도리코 프로젝터
		/// </summary>
		deviceSindoProjector = 1,
		/// <summary>
		/// LG 디스플레이
		/// </summary>
		deviceLGDisplay = 2,
		/// <summary>
		/// SPD-50c92hd 삼성 PDP
		/// </summary>
		deviceSamsungPDP_SPD_50c92hd = 3,
        /// <summary>
        /// 삼성 570DX 시리즈
        /// </summary>
        deviceSamsung_570DX = 4,
        /// <summary>
        /// 현대 IT 시리즈
        /// </summary>
        deviceHyundaiIT =5,
		/// <summary>
		/// 알수 없음
		/// </summary>
		deviceUndefined = 255
	}

	/// <summary>
	/// 시리얼 포트 상태
	/// </summary>
	public enum SerialPortStatus : int
	{
		/// <summary>
		/// 전원 상태
		/// </summary>
		statusPower,
		/// <summary>
		/// 볼륨 상태
		/// </summary>
		statusVolume,
		/// <summary>
		/// 소스 상태
		/// </summary>
		statusSource,
		/// <summary>
		/// 알수 없음
		/// </summary>
		statusUndefined = 255
	}

	/// <summary>
	/// 소스 모드
	/// </summary>
	public enum SourceMode : int
	{
		/// <summary>
		/// 일반 모드
		/// </summary>
		sourceGeneral,
		/// <summary>
		/// RGB 모드
		/// </summary>
		sourceRGB,
		/// <summary>
		/// DVI 모드
		/// </summary>
		sourceDVI,
		/// <summary>
		/// 컴포지트 모드
		/// </summary>
		sourceComposite,
		/// <summary>
		/// S-Video 모드
		/// </summary>
		sourceSVideo,
		/// <summary>
		/// HDMI 모드
		/// </summary>
		sourceHDMI,
		/// <summary>
		/// 컴포넌트 모드
		/// </summary>
		sourceComponent,
		/// <summary>
		/// 알수 없음
		/// </summary>
		undefined = 255
	}
}