﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Ports;
using System.Reflection;
using NLog;

namespace SerialPortController
{
	/// <summary>
	/// Samsung_SPD_50c92hd Serial Port 제어
	/// </summary>
	class Samsung_SPD_50c92hdControl : ISerialPortControl
	{
        private static Logger logger = LogManager.GetCurrentClassLogger();

		/// <summary>
		/// Lock 오브젝트 선언
		/// </summary>
		static readonly object lockPort = new object();

		static readonly object lockSetting = new object();

		ConfigSettings setting = null;

		SerialPort _objPort = null;
		String _comPort = "COM1";

		#region ISerialPortControl 구현부

        /// <summary>
        /// 로그 이벤트
        /// </summary>
        public event SerialLogEventHandler LogOccurred;
        
        /// <summary>
		/// Com Port 이름
		/// </summary>
		public String ComPort { get { return _comPort; } }
		/// <summary>
		/// Baud Rate 값
		/// </summary>
		public int BaudRate { get { return 9600; } }
		/// <summary>
		/// Data Bit 값
		/// </summary>
		public int DataBit { get { return 8; } }
		/// <summary>
		/// Stop Bit 값
		/// </summary>
		public StopBits StopBit { get { return StopBits.One; } }
		/// <summary>
		/// Parity 값
		/// </summary>
		public Parity ParityValue { get { return Parity.None; } }
		/// <summary>
		/// 포트가 열려있는지
		/// </summary>
		public bool IsOpen 
		{ 
			get
			{
				lock (lockPort)
				{
					if (_objPort != null)
					{
						return _objPort.IsOpen;
					}
					else return false;
				}
			}
		}

		private bool PowerStatus
		{
			get
			{
				bool result = true;
				lock (lockSetting)
				{
					if (setting != null)
					{
						try
						{
							string sVal = null;
							sVal = ConfigSettings.ReadSetting("PowerStatus");
							if (String.IsNullOrEmpty(sVal)) ConfigSettings.WriteSetting("PowerStatus", result.ToString());

							return result = Convert.ToBoolean(sVal);

						}
						catch { ConfigSettings.WriteSetting("PowerStatus", result.ToString()); }
					}
				}
				return result;
			}
			set
			{
				lock (lockSetting)
				{
					ConfigSettings.WriteSetting("PowerStatus", value.ToString());
				}
			}
		}

		private SourceMode SourceStatus
		{
			get
			{
				SourceMode result = SourceMode.sourceRGB;
				lock (lockSetting)
				{
					if (setting != null)
					{
						try
						{
							string sVal = null;
							sVal = ConfigSettings.ReadSetting("Source");
							if (String.IsNullOrEmpty(sVal)) ConfigSettings.WriteSetting("Source", ((int)result).ToString());

							return result = (SourceMode)Convert.ToInt32(sVal);

						}
						catch { ConfigSettings.WriteSetting("Source", ((int)result).ToString()); }
					}
				}
				return result;
			}
			set
			{
				lock (lockSetting)
				{
					ConfigSettings.WriteSetting("Source", ((int)value).ToString());
				}
			}
		}

		private int VolumeStatus
		{
			get
			{
				int result = 10;
				lock (lockSetting)
				{
					if (setting != null)
					{
						try
						{
							string sVal = null;
							sVal = ConfigSettings.ReadSetting("Volume");
							if (String.IsNullOrEmpty(sVal)) ConfigSettings.WriteSetting("Volume", result.ToString());

							return result = Convert.ToInt32(sVal);

						}
						catch { ConfigSettings.WriteSetting("Volume", result.ToString()); }
					}
				}
				return result;
			}
			set
			{
				lock (lockSetting)
				{
					ConfigSettings.WriteSetting("Volume", value.ToString());
				}
			}
		}

		/// <summary>
		/// 초기화
		/// </summary>
		/// <returns></returns>
		public bool Initialize()
		{
			Uninitialize();

			lock(lockPort)
			{
				if(setting == null)
				{
					String sExecutingPath = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
					setting = new ConfigSettings(String.Format("{0}\\PlayerData\\SerialStatus.xml", sExecutingPath));
                }

				_objPort = new SerialPort(ComPort, BaudRate, ParityValue, DataBit, StopBit);
				_objPort.ReadTimeout = 2000;
				_objPort.WriteTimeout = 2000;
				_objPort.Handshake = Handshake.None;
				_objPort.NewLine = "\n";

				_objPort.Open();
			}

			return IsOpen;
		}
		/// <summary>
		/// 초기화
		/// </summary>
		/// <returns></returns>
		public bool Initialize(String PortName)
		{
			_comPort = PortName;
			return Initialize();
		}

		/// <summary>
		/// 릴리즈
		/// </summary>
		public void Uninitialize()
		{
			if(IsOpen)
			{
				lock(lockPort)
				{
					_objPort.Close();
					_objPort.Dispose();
					_objPort = null;
				}
			}
		}

		private Byte MakeCheckSum(Byte[] command)
		{
			int a = ~((int)command[0] + (int)command[1] + (int)command[2] + (int)command[3] + (int)command[4] + (int)command[5]) + 1;
			Byte[] arrBytes = BitConverter.GetBytes(a);
			return arrBytes[0];
		}
        private void WriteBytes(byte[] arrBytes)
        {
            String sData = "Serial:";

            try
            {
                foreach (byte b in arrBytes)
                {

                    sData += String.Format(" {0}", b);
                }
            }
            catch { }

            logger.Debug(sData);
        }

		private bool Process(Byte[] command)
		{
			command[6] = MakeCheckSum(command);

			lock (lockPort)
			{
				_objPort.Write(command, 0, 7);
                WriteBytes(command);
				System.Threading.Thread.Sleep(500);

				if (3 <= _objPort.BytesToRead)
				{
					byte[] readBytes = new byte[_objPort.BytesToRead];
					_objPort.Read(readBytes, 0, _objPort.BytesToRead);

                    WriteBytes(readBytes);

					return readBytes[0] == 0x03 && readBytes[1] == 0x0C && readBytes[2] == 0xF1;
				}
				return false;
			}
		}

		/// <summary>
		/// 전원 켜기
		/// </summary>
		/// <returns></returns>
		public bool PowerOn()
		{
			if (_objPort == null) Initialize();
			if (!IsOpen) return false;
 			Byte[] command = new Byte[7] {0x08, 0x22, 0x00, 0x00, 0x00, 0x02, 0x00};

			bool bRet = Process(command);
			
            this.PowerStatus = true;

			return bRet;
		}
		/// <summary>
		/// 전원 끄기
		/// </summary>
		/// <returns></returns>
		public bool PowerOff()
		{
			if (_objPort == null) Initialize();
			if (!IsOpen) return false;

 			Byte[] command = new Byte[7] {0x08, 0x22, 0x00, 0x00, 0x00, 0x01, 0x00};

			bool bRet = Process(command);
			
            this.PowerStatus = false;

			return bRet;
		}

		/// <summary>
		/// 자동 조절
		/// </summary>
		/// <returns></returns>
		public bool Auto()
		{
			if (_objPort == null) Initialize();
			if (!IsOpen) return false;

 			Byte[] command = new Byte[7] {0x08, 0x22, 0x0b, 0x06, 0x00, 0x00, 0x00};

			return Process(command);
		}

		/// <summary>
		/// 볼륨 조절
		/// </summary>
		/// <param name="nValue"></param>
		/// <returns></returns>
		public bool Volume(int nValue)
		{
			if (_objPort == null) Initialize();
			if (!IsOpen) return false;

			if (this.VolumeStatus == nValue) return true;

			Byte[] command = new Byte[7] { 0x08, 0x22, 0x01, 0x00, 0x00, Convert.ToByte(nValue), 0x00 };

			bool bRet = Process(command);
			if(bRet) this.VolumeStatus = nValue;

			return bRet;
		}

		/// <summary>
		/// 소스 제어
		/// </summary>
		/// <param name="mode"></param>
		/// <returns></returns>
		public bool Source(SourceMode mode)
		{
			if (_objPort == null) Initialize();
			if (!IsOpen) return false;

			lock (lockPort)
			{
				String sReturn = String.Empty;
				switch (mode)
				{
					case SourceMode.sourceGeneral:
						{
							Byte[] command = new Byte[7] { 0x08, 0x22, 0x0a, 0x00, 0x00, 0x00, 0x00 };
							bool bRet = Process(command);
							if(bRet) this.SourceStatus = mode;

							return bRet;
						}
					case SourceMode.sourceRGB:
						{
							//	RGB가 없어 PC로 함
							Byte[] command = new Byte[7] { 0x08, 0x22, 0x0a, 0x00, 0x04, 0x00, 0x00 };
							bool bRet = Process(command);
							if(bRet) this.SourceStatus = mode;

							return bRet;
						}
					case SourceMode.sourceDVI:
						{
							Byte[] command = new Byte[7] { 0x08, 0x22, 0x0a, 0x00, 0x06, 0x00, 0x00 };
							bool bRet = Process(command);
							if(bRet) this.SourceStatus = mode;

							return bRet;
						}
					case SourceMode.sourceSVideo:
						{
							Byte[] command = new Byte[7] { 0x08, 0x22, 0x0a, 0x00, 0x02, 0x00, 0x00 };
							bool bRet = Process(command);
							if(bRet) this.SourceStatus = mode;

							return bRet;
						}
					case SourceMode.sourceComposite:
						{
							//	컴포지트가 없어 AV로 대체
							Byte[] command = new Byte[7] { 0x08, 0x22, 0x0a, 0x00, 0x01, 0x00, 0x00 };
							bool bRet = Process(command);
							if(bRet) this.SourceStatus = mode;

							return bRet;
						}
					case SourceMode.sourceComponent:
						{
							Byte[] command = new Byte[7] { 0x08, 0x22, 0x0a, 0x00, 0x03, 0x00, 0x00 };
							bool bRet = Process(command);
							if(bRet) this.SourceStatus = mode;

							return bRet;
						}

				}
				return false;
			}
		}

		/// <summary>
		/// 상태 반환
		/// </summary>
		/// <param name="status">상태를 확인할 값</param>
		/// <returns>반환된 상태</returns>
		public object Status(SerialPortStatus status)
		{
			if (_objPort == null) Initialize();
			if (!IsOpen) return false;

			String sReturn = String.Empty;
			switch (status)
			{
				case SerialPortStatus.statusPower:
					{
						bool bRet = this.PowerStatus;
						if (bRet)
						{
							if (!this.PowerOn()) return false;
						}
						else
						{
							///	2012.04.20. 상태 검사할때 파워가 꺼진 상태에는 제어 신호 보내지 않도록 구성
							return bRet;
							//if (!this.PowerOff()) return false;
						}

						return bRet;
					}
				case SerialPortStatus.statusSource:
					{
						///	2012.04.20. 상태 검사할때 파워가 꺼진 상태에는 제어 신호 보내지 않도록 구성
						bool bRet = this.PowerStatus;
						if (bRet)
						{
							SourceMode mode = this.SourceStatus;
							if (this.Source(mode))
								return mode;
							return "-";
						}
						else return "-";
					}

				case SerialPortStatus.statusVolume:
					{
						int nVol = this.VolumeStatus;
						return nVol;
					}
			}
			return false;
		}
		#endregion
	}
}
