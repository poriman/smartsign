﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Ports;
using NLog;
using NLog.Targets;
using System.Reflection;
using System.IO;
namespace SerialPortController
{
	/// <summary>
	/// Samsung 5700DX Display Serial Port 제어
	/// </summary>
	class Samsung570DXControl : ISerialPortControl
	{
		/// <summary>
		/// Lock 오브젝트 선언
		/// </summary>
		static readonly object lockPort = new object();

		public SerialPort _objPort = null;
        public String _comPort = "COM1";

		#region ISerialPortControl 구현부
		/// <summary>
		/// Com Port 이름
		/// </summary>
        public String ComPort { get { return _comPort; } set { } }
		/// <summary>
		/// Baud Rate 값
		/// </summary>
		public int BaudRate { get { return 9600; } }
		/// <summary>
		/// Data Bit 값
		/// </summary>
		public int DataBit { get { return 8; } }
		/// <summary>
		/// Stop Bit 값
		/// </summary>
		public StopBits StopBit { get { return StopBits.One; } }
		/// <summary>
		/// Parity 값
		/// </summary>
		public Parity ParityValue { get { return Parity.None; } }
		/// <summary>
		/// 포트가 열려있는지
		/// </summary>
		public bool IsOpen 
		{ 
			get
			{
				lock (lockPort)
				{
					if (_objPort != null)
					{
						return _objPort.IsOpen;
					}
					else return false;
				}
			}
		}
		/// <summary>
		/// 초기화
		/// </summary>
		/// <returns></returns>
		public bool Initialize()
		{
            try
            {
                Uninitialize();
                lock (lockPort)
                {
                    _objPort = new SerialPort(ComPort, BaudRate, ParityValue, DataBit, StopBit);
                    _objPort.ReadTimeout = 2000;
                    _objPort.WriteTimeout = 2000;
                    _objPort.Handshake = Handshake.None;
                    _objPort.NewLine = "\n";

                    _objPort.Open();
                }

                return IsOpen;
            }catch(Exception e){
                if (LogOccurred != null) LogOccurred(this, new SerialLogEventArgs(e.Message));
                return false;
            }
		}
		/// <summary>
		/// 초기화
		/// </summary>
		/// <returns></returns>
		public bool Initialize(String PortName)
		{
			_comPort = PortName;
			return Initialize();
		}

		/// <summary>
		/// 릴리즈
		/// </summary>
		public void Uninitialize()
		{
			if(IsOpen)
			{
				lock(lockPort)
				{
					_objPort.Close();
					_objPort.Dispose();
					_objPort = null;
				}
			}
		}

        private Byte MakeCheckSum(Byte[] command)
        {
            int a = 0;
            for (int i = 1; i < command.Length; ++i )
            {
                a += (int)command[i];
            }

            Byte[] arrBytes = BitConverter.GetBytes(a);
            return arrBytes[0];
        }
        string Convert_byte(byte[] command) { 
            string str=string.Empty;
            for(int i=0;i<command.Length;i++){
                str=String.Concat("0x",String.Format("{0:X} ",command[i]))+str;
            }
            return str;
        }
        private bool Process(Byte[] command)
        {
            command[command.Length - 1] = MakeCheckSum(command);

            lock (lockPort)
            {
                
                
                _objPort.Write(command, 0, command.Length);
                System.Threading.Thread.Sleep(500);
                if (LogOccurred != null)
                    LogOccurred(this, new SerialLogEventArgs("POWER_WRITE : " + Convert_byte(command)));
                
                if (8 <= _objPort.BytesToRead)
                {
                    byte[] readBytes = new byte[_objPort.BytesToRead];
                    _objPort.Read(readBytes, 0, _objPort.BytesToRead);
                    if (LogOccurred != null)
                        LogOccurred(this, new SerialLogEventArgs("POWER_READ : "+Convert_byte(readBytes)));
                    

                    return readBytes[0] == 0xAA && readBytes[1] == 0x00 && readBytes[4] == 'A';
                }
                return false;
            }
        }

		/// <summary>
		/// 전원 켜기
		/// </summary>
		/// <returns></returns>
		public bool PowerOn()
		{
			if (_objPort == null) Initialize();
			if (!IsOpen) return false;

 			Byte[] command = new Byte[6] {0xAA, 0x11, 0x00, 0x01, 0x01, 0x00};

			lock(lockPort)
			{
                return Process(command);
			}
		}
		/// <summary>
		/// 전원 끄기
		/// </summary>
		/// <returns></returns>
		public bool PowerOff()
		{
            if (_objPort == null) Initialize();
            if (!IsOpen) return false;

            Byte[] command = new Byte[6] { 0xAA, 0x11, 0x00, 0x01, 0x00, 0x00 };

            lock (lockPort)
            {
                return Process(command);
            }

		}

		/// <summary>
		/// 자동 조절
		/// </summary>
		/// <returns></returns>
		public bool Auto()
		{
            return false;
		}

		/// <summary>
		/// 볼륨 조절
		/// </summary>
		/// <param name="nValue"></param>
		/// <returns></returns>
		public bool Volume(int nValue)
		{
            if (_objPort == null) Initialize();
            if (!IsOpen) return false;

            Byte[] command = new Byte[6] { 0xAA, 0x12, 0x00, 0x01, Convert.ToByte(nValue), 0x00 };

            lock (lockPort)
            {
                return Process(command);
            }
		}

		/// <summary>
		/// 소스 제어
		/// </summary>
		/// <param name="mode"></param>
		/// <returns></returns>
		public bool Source(SourceMode mode)
		{
            if (_objPort == null) Initialize();
            if (!IsOpen) return false;

            Byte[] command = new Byte[6] { 0xAA, 0x14, 0x00, 0x01, 0x14, 0x00 };

			lock (lockPort)
			{
				String sReturn = String.Empty;
				switch (mode)
				{
					case SourceMode.sourceGeneral:
                        command[4] = 0x14;
                        return Process(command);

					case SourceMode.sourceRGB:
                        command[4] = 0x14;
                        return Process(command);

                    case SourceMode.sourceDVI:
                        command[4] = 0x18;
                        return Process(command);

                    case SourceMode.sourceSVideo:
                        command[4] = 0x04;
                        return Process(command);

                    case SourceMode.sourceComposite:
                        command[4] = 0x0C;
                        return Process(command);

                    case SourceMode.sourceComponent:
                        command[4] = 0x08;
                        return Process(command);

                }
				return false;
			}
		}

		/// <summary>
		/// 상태 반환
		/// </summary>
		/// <param name="status">상태를 확인할 값</param>
		/// <returns>반환된 상태</returns>
		public object Status(SerialPortStatus status)
		{
			if (_objPort == null) Initialize();
			if (!IsOpen) return false;

			lock (lockPort)
			{
				String sReturn = String.Empty;
				switch (status)
				{
                    case SerialPortStatus.statusPower:
                        {
                            Byte[] command = new Byte[5] { 0xAA, 0x11, 0x00, 0x00, 0x00 };

                            command[command.Length - 1] = MakeCheckSum(command);

                            _objPort.Write(command, 0, command.Length);
                            if (LogOccurred != null)
                                LogOccurred(this, new SerialLogEventArgs("STATUS_WRITE : " + Convert_byte(command)));
                            System.Threading.Thread.Sleep(500);

                            if (8 <= _objPort.BytesToRead)
                            {
                                byte[] readBytes = new byte[_objPort.BytesToRead];
                                _objPort.Read(readBytes, 0, _objPort.BytesToRead);
                                if (LogOccurred != null)
                                    LogOccurred(this, new SerialLogEventArgs("STATUS_READ : " + Convert_byte(readBytes)));
                                return readBytes[0] == 0xAA && readBytes[1] == 0x00 && readBytes[4] == 'A' && readBytes[6] == 0x01;
                            }
                            return false;
                        }
                    case SerialPortStatus.statusSource:
                        {
                            Byte[] command = new Byte[5] { 0xAA, 0x14, 0x00, 0x00, 0x00 };

                            command[command.Length - 1] = MakeCheckSum(command);

                            _objPort.Write(command, 0, command.Length);
                            if (LogOccurred != null)
                                LogOccurred(this, new SerialLogEventArgs("STATUS_WRITE : " + Convert_byte(command)));
                            System.Threading.Thread.Sleep(500);

                            if (8 <= _objPort.BytesToRead)
                            {
                                byte[] readBytes = new byte[_objPort.BytesToRead];
                                _objPort.Read(readBytes, 0, _objPort.BytesToRead);
                                if (LogOccurred != null)
                                    LogOccurred(this, new SerialLogEventArgs("STATUS_READ : " + Convert_byte(readBytes)));
                                if (readBytes[0] == 0xAA && readBytes[1] == 0x00 && readBytes[4] == 'A')
                                {
                                    switch (readBytes[6])
                                    {
                                        case 0x14:
                                            return "PC";
                                        case 0x18:
                                            return "DVI";
                                        case 0x0C:
                                            return "AV";
                                        case 0x04:
                                            return "S-VIDEO";
                                        case 0x08:
                                            return "COMPONENT";
                                        case 0x21:
                                            return "HDMI";
                                    }

                                    return "UNKNOWN";
                                }
                                else return false;
                            }

                            return "Fail";
                        }
                    case SerialPortStatus.statusVolume:
                        {
                            Byte[] command = new Byte[5] { 0xAA, 0x12, 0x00, 0x00, 0x00 };

                            command[command.Length - 1] = MakeCheckSum(command);

                            _objPort.Write(command, 0, command.Length);
                            if (LogOccurred != null)
                                LogOccurred(this, new SerialLogEventArgs("STATUS_WRITE : " + Convert_byte(command)));
                            System.Threading.Thread.Sleep(500);

                            if (8 <= _objPort.BytesToRead)
                            {
                                byte[] readBytes = new byte[_objPort.BytesToRead];
                                _objPort.Read(readBytes, 0, _objPort.BytesToRead);
                                if (LogOccurred != null)
                                    LogOccurred(this, new SerialLogEventArgs("STATUS_READ : " + Convert_byte(readBytes)));
                                if (readBytes[0] == 0xAA && readBytes[1] == 0x00 && readBytes[4] == 'A')
                                {
                                    return Convert.ToInt32(readBytes[6]);
                                }
                            }
                            else
                                return "--";
                        }
                        break;
				}
				throw new NotImplementedException();

				return false;
			}
		}

        /// <summary>
        /// 로그 이벤트
        /// </summary>
        public event SerialLogEventHandler LogOccurred;

		#endregion
	}
}
