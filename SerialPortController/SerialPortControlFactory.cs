﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SerialPortController
{
	public class SerialPortControlFactory
	{
		ISerialPortControl _controller = null;

		public SerialPortControlFactory()
		{

		}

		public SerialPortControlFactory(SupportedSerialPortDevice device)
		{
			_controller = CreateController(device);
		}

		public ISerialPortControl CreateController(SupportedSerialPortDevice device)
		{
			
			switch(device)
			{
				case SupportedSerialPortDevice.deviceNone:
					_controller = null;
					break;
				case SupportedSerialPortDevice.deviceSindoProjector:
					_controller = new SindoProjectorControl();
					break;
				case SupportedSerialPortDevice.deviceLGDisplay:
					_controller = new LGDisplayControl();
					break;
				case SupportedSerialPortDevice.deviceSamsungPDP_SPD_50c92hd:
					_controller = new Samsung_SPD_50c92hdControl();
					break;
                case SupportedSerialPortDevice.deviceSamsung_570DX:
                    _controller = new Samsung570DXControl();
                    break;
                case SupportedSerialPortDevice.deviceHyundaiIT:
                    _controller = new HyundaiDisplayControl();
                    break;
				case SupportedSerialPortDevice.deviceUndefined:
					break;
				default:
					throw new ArgumentOutOfRangeException();
					break;
			}

			return _controller;
		}
	}
}
