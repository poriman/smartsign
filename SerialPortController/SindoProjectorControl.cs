﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Ports;

namespace SerialPortController
{
	/// <summary>
	/// 신도리코 프로젝터 Serial Port 제어
	/// </summary>
	class SindoProjectorControl : ISerialPortControl
	{
		/// <summary>
		/// Lock 오브젝트 선언
		/// </summary>
		static readonly object lockPort = new object();

		SerialPort _objPort = null;
		String _comPort = "COM1";

		#region ISerialPortControl 구현부

        /// <summary>
        /// 로그 이벤트
        /// </summary>
        public event SerialLogEventHandler LogOccurred;
        
        /// <summary>
		/// Com Port 이름
		/// </summary>
		public String ComPort { get { return _comPort; } }
		/// <summary>
		/// Baud Rate 값
		/// </summary>
		public int BaudRate { get { return 115200; } }
		/// <summary>
		/// Data Bit 값
		/// </summary>
		public int DataBit { get { return 8; } }
		/// <summary>
		/// Stop Bit 값
		/// </summary>
		public StopBits StopBit { get { return StopBits.One; } }
		/// <summary>
		/// Parity 값
		/// </summary>
		public Parity ParityValue { get { return Parity.None; } }
		/// <summary>
		/// 포트가 열려있는지
		/// </summary>
		public bool IsOpen 
		{ 
			get
			{
				lock (lockPort)
				{
					if (_objPort != null)
					{
						return _objPort.IsOpen;
					}
					else return false;
				}
			}
		}
		/// <summary>
		/// 초기화
		/// </summary>
		/// <returns></returns>
		public bool Initialize()
		{
			Uninitialize();

			lock(lockPort)
			{
				_objPort = new SerialPort(ComPort, BaudRate, ParityValue, DataBit, StopBit);
				_objPort.ReadTimeout = 2000;
				_objPort.WriteTimeout = 2000;
				_objPort.Handshake = Handshake.None;
				_objPort.NewLine = "\r\n";

				_objPort.Open();
			}

			return IsOpen;
		}
		/// <summary>
		/// 초기화
		/// </summary>
		/// <returns></returns>
		public bool Initialize(String PortName)
		{
			_comPort = PortName;
			return Initialize();
		}

		/// <summary>
		/// 릴리즈
		/// </summary>
		public void Uninitialize()
		{
			if(IsOpen)
			{
				lock(lockPort)
				{
					_objPort.Close();
					_objPort.Dispose();
					_objPort = null;
				}
			}
		}
		/// <summary>
		/// 전원 켜기
		/// </summary>
		/// <returns></returns>
		public bool PowerOn()
		{
			if (_objPort == null) Initialize();
			if (!IsOpen) return false;

// 			Byte[] command = new Byte[6] {0x23, 
// 				Convert.ToByte(''),
// 			0x0D, 0x0A};

			lock(lockPort)
			{
				_objPort.WriteLine("#P1");
                if (LogOccurred != null) LogOccurred(this, new SerialLogEventArgs(String.Format("WRITE: {0}\n", "#P1")));
				System.Threading.Thread.Sleep(500);

				String sReturn = String.Empty;
				if (0 < _objPort.BytesToRead)
				{
					byte[] readBytes = new byte[_objPort.BytesToRead];
					_objPort.Read(readBytes, 0, _objPort.BytesToRead);
                    if (LogOccurred != null) LogOccurred(this, new SerialLogEventArgs(String.Format("READ: {0}\n", readBytes)));
					sReturn = System.Text.ASCIIEncoding.ASCII.GetString(readBytes);
				} 
				return sReturn.Contains("P11");
			}
		}
		/// <summary>
		/// 전원 끄기
		/// </summary>
		/// <returns></returns>
		public bool PowerOff()
		{
			if (_objPort == null) Initialize();
			if (!IsOpen) return false;

			lock (lockPort)
			{
				_objPort.WriteLine("#P0");
                if (LogOccurred != null) LogOccurred(this, new SerialLogEventArgs(String.Format("WRITE: {0}\n", "#P0")));
				System.Threading.Thread.Sleep(500);

				String sReturn = String.Empty;
				if (0 < _objPort.BytesToRead)
				{
					byte[] readBytes = new byte[_objPort.BytesToRead];
					_objPort.Read(readBytes, 0, _objPort.BytesToRead);
                    if (LogOccurred != null) LogOccurred(this, new SerialLogEventArgs(String.Format("READ: {0}\n", readBytes)));
					sReturn = System.Text.ASCIIEncoding.ASCII.GetString(readBytes);
				} 
				return sReturn.Contains("P01");
			}
		}

		/// <summary>
		/// 자동 조절
		/// </summary>
		/// <returns></returns>
		public bool Auto()
		{
			if (_objPort == null) Initialize();
			if (!IsOpen) return false;

			lock (lockPort)
			{
				_objPort.WriteLine("#AT");
                if (LogOccurred != null) LogOccurred(this, new SerialLogEventArgs(String.Format("WRITE: {0}\n", "#AT")));
				System.Threading.Thread.Sleep(500);

				String sReturn = String.Empty;
				if (0 < _objPort.BytesToRead)
				{
					byte[] readBytes = new byte[_objPort.BytesToRead];
					_objPort.Read(readBytes, 0, _objPort.BytesToRead);
                    if (LogOccurred != null) LogOccurred(this, new SerialLogEventArgs(String.Format("READ: {0}\n", readBytes)));
					sReturn = System.Text.ASCIIEncoding.ASCII.GetString(readBytes);
				} 
				return sReturn.Contains("AT_");
			}
		}

		/// <summary>
		/// 볼륨 조절
		/// </summary>
		/// <param name="nValue"></param>
		/// <returns></returns>
		public bool Volume(int nValue)
		{
			if (_objPort == null) Initialize();
			if (!IsOpen) return false;

			lock (lockPort)
			{
				throw new NotImplementedException();

				return false;
			}
		}

		/// <summary>
		/// 소스 제어
		/// </summary>
		/// <param name="mode"></param>
		/// <returns></returns>
		public bool Source(SourceMode mode)
		{
			if (_objPort == null) Initialize();
			if (!IsOpen) return false;

			lock (lockPort)
			{
				String sReturn = String.Empty;
				switch (mode)
				{
					case SourceMode.sourceGeneral:
						_objPort.WriteLine("#SR");
                        if (LogOccurred != null) LogOccurred(this, new SerialLogEventArgs(String.Format("WRITE: {0}\n", "#SR")));
						System.Threading.Thread.Sleep(500);

						if (0 < _objPort.BytesToRead)
						{
							byte[] readBytes = new byte[_objPort.BytesToRead];
							_objPort.Read(readBytes, 0, _objPort.BytesToRead);
                            if (LogOccurred != null) LogOccurred(this, new SerialLogEventArgs(String.Format("READ: {0}\n", readBytes)));
							sReturn = System.Text.ASCIIEncoding.ASCII.GetString(readBytes);
						} 
						return sReturn.Contains("SR1");
					case SourceMode.sourceRGB:
						_objPort.WriteLine("#SR");
                        if (LogOccurred != null) LogOccurred(this, new SerialLogEventArgs(String.Format("WRITE: {0}\n", "#SR")));
						System.Threading.Thread.Sleep(500);

						if (0 < _objPort.BytesToRead)
						{
							byte[] readBytes = new byte[_objPort.BytesToRead];
							_objPort.Read(readBytes, 0, _objPort.BytesToRead);
                            if (LogOccurred != null) LogOccurred(this, new SerialLogEventArgs(String.Format("READ: {0}\n", readBytes)));
							sReturn = System.Text.ASCIIEncoding.ASCII.GetString(readBytes);
						} 
						return sReturn.Contains("SR1");
					case SourceMode.sourceDVI:
						_objPort.WriteLine("#SA");
                        if (LogOccurred != null) LogOccurred(this, new SerialLogEventArgs(String.Format("WRITE: {0}\n", "#SA")));
						System.Threading.Thread.Sleep(500);

						if (0 < _objPort.BytesToRead)
						{
							byte[] readBytes = new byte[_objPort.BytesToRead];
							_objPort.Read(readBytes, 0, _objPort.BytesToRead);
                            if (LogOccurred != null) LogOccurred(this, new SerialLogEventArgs(String.Format("READ: {0}\n", readBytes)));
							sReturn = System.Text.ASCIIEncoding.ASCII.GetString(readBytes);
						} 
						return sReturn.Contains("SA1");
					case SourceMode.sourceSVideo:
						_objPort.WriteLine("#SS");
                        if (LogOccurred != null) LogOccurred(this, new SerialLogEventArgs(String.Format("WRITE: {0}\n", "#SS")));
						System.Threading.Thread.Sleep(500);

						if (0 < _objPort.BytesToRead)
						{
							byte[] readBytes = new byte[_objPort.BytesToRead];
							_objPort.Read(readBytes, 0, _objPort.BytesToRead);
                            if (LogOccurred != null) LogOccurred(this, new SerialLogEventArgs(String.Format("READ: {0}\n", readBytes)));
							sReturn = System.Text.ASCIIEncoding.ASCII.GetString(readBytes);
						} 
						return sReturn.Contains("SS1");
					case SourceMode.sourceComposite:
						_objPort.WriteLine("#SV");
                        if (LogOccurred != null) LogOccurred(this, new SerialLogEventArgs(String.Format("WRITE: {0}\n", "#SV")));
						System.Threading.Thread.Sleep(500);

						if (0 < _objPort.BytesToRead)
						{
							byte[] readBytes = new byte[_objPort.BytesToRead];
							_objPort.Read(readBytes, 0, _objPort.BytesToRead);
                            if (LogOccurred != null) LogOccurred(this, new SerialLogEventArgs(String.Format("READ: {0}\n", readBytes)));
							sReturn = System.Text.ASCIIEncoding.ASCII.GetString(readBytes);
						} 
						return sReturn.Contains("SV1");
				}

				throw new NotImplementedException();

				return false;
			}
		}

		/// <summary>
		/// 상태 반환
		/// </summary>
		/// <param name="status">상태를 확인할 값</param>
		/// <returns>반환된 상태</returns>
		public object Status(SerialPortStatus status)
		{
			if (_objPort == null) Initialize();
			if (!IsOpen) return false;

			lock (lockPort)
			{
				String sReturn = String.Empty;
				switch (status)
				{
					case SerialPortStatus.statusPower:
						_objPort.WriteLine("#QS");
                        if (LogOccurred != null) LogOccurred(this, new SerialLogEventArgs(String.Format("WRITE: {0}\n", "#QS")));
						System.Threading.Thread.Sleep(500);

						if (0 < _objPort.BytesToRead)
						{
							byte[] readBytes = new byte[_objPort.BytesToRead];
							_objPort.Read(readBytes, 0, _objPort.BytesToRead);
                            if (LogOccurred != null) LogOccurred(this, new SerialLogEventArgs(String.Format("READ: {0}\n", readBytes)));
							sReturn = System.Text.ASCIIEncoding.ASCII.GetString(readBytes);
						}

						return (sReturn.Contains("QS1") || sReturn.Contains("QS2") || sReturn.Contains("QS3") || sReturn.Contains("QS4") || sReturn.Contains("QS5") || sReturn.Contains("QS6"));

					case SerialPortStatus.statusSource:
						_objPort.WriteLine("#QR");
                        if (LogOccurred != null) LogOccurred(this, new SerialLogEventArgs(String.Format("WRITE: {0}\n", "#QR")));
						System.Threading.Thread.Sleep(500);

						if (0 < _objPort.BytesToRead)
						{
							byte[] readBytes = new byte[_objPort.BytesToRead];
							_objPort.Read(readBytes, 0, _objPort.BytesToRead);
                            if (LogOccurred != null) LogOccurred(this, new SerialLogEventArgs(String.Format("READ: {0}\n", readBytes)));
							sReturn = System.Text.ASCIIEncoding.ASCII.GetString(readBytes);
						}

						if (sReturn.Contains("QR0") || sReturn.Contains("QR8")) return "VGA";
						else if (sReturn.Contains("QR1")) return "DVI";
						else if (sReturn.Contains("QR2") || sReturn.Contains("QR9")) return "HD";
						else if (sReturn.Contains("QR3") || sReturn.Contains("QRa")) return "COMPONENT (Progressive)";
						else if (sReturn.Contains("QR4")) return "COMPOSITE";
						else if (sReturn.Contains("QR5")) return "SVIDEO";
						else if (sReturn.Contains("QR6") || sReturn.Contains("QRb")) return "COMPONENT (Interlaced)";
						else if (sReturn.Contains("QR7")) return "Image Viewer";
						else if (sReturn.Contains("QRc")) return "Network";
						else if (sReturn.Contains("QRd")) return "--";
						else return "Fail";

				}
				throw new NotImplementedException();

				return false;
			}
		}

		#endregion
	}
}
