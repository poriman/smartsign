﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Ports;

namespace SerialPortController
{
	/// <summary>
	/// LG Display Serial Port 제어
	/// </summary>
	class LGDisplayControl : ISerialPortControl
	{
		/// <summary>
		/// Lock 오브젝트 선언
		/// </summary>
		static readonly object lockPort = new object();

		SerialPort _objPort = null;
		String _comPort = "COM1";

		#region ISerialPortControl 구현부

        /// <summary>
        /// 로그 이벤트
        /// </summary>
        public event SerialLogEventHandler LogOccurred;
        
        /// <summary>
		/// Com Port 이름
		/// </summary>
		public String ComPort { get { return _comPort; } }
		/// <summary>
		/// Baud Rate 값
		/// </summary>
		public int BaudRate { get { return 9600; } }
		/// <summary>
		/// Data Bit 값
		/// </summary>
		public int DataBit { get { return 8; } }
		/// <summary>
		/// Stop Bit 값
		/// </summary>
		public StopBits StopBit { get { return StopBits.One; } }
		/// <summary>
		/// Parity 값
		/// </summary>
		public Parity ParityValue { get { return Parity.None; } }
		/// <summary>
		/// 포트가 열려있는지
		/// </summary>
		public bool IsOpen 
		{ 
			get
			{
				lock (lockPort)
				{
					if (_objPort != null)
					{
						return _objPort.IsOpen;
					}
					else return false;
				}
			}
		}
        string Convert_byte(byte[] command)
        {
            string str = string.Empty;
            for (int i = 0; i < command.Length; i++)
            {
                str = String.Concat("0x", String.Format("{0:X} ", command[i])) + str;
            }
            return str;
        }
		/// <summary>
		/// 초기화
		/// </summary>
		/// <returns></returns>
		public bool Initialize()
		{
			Uninitialize();

			lock(lockPort)
			{
				_objPort = new SerialPort(ComPort, BaudRate, ParityValue, DataBit, StopBit);
				_objPort.ReadTimeout = 2000;
				_objPort.WriteTimeout = 2000;
				_objPort.Handshake = Handshake.None;
				_objPort.NewLine = "\n";
                _objPort.Open();
                
			}

			return IsOpen;
		}
		/// <summary>
		/// 초기화
		/// </summary>
		/// <returns></returns>
		public bool Initialize(String PortName)
		{
			_comPort = PortName;
			return Initialize();
		}

		/// <summary>
		/// 릴리즈
		/// </summary>
		public void Uninitialize()
		{
			if(IsOpen)
			{
				lock(lockPort)
				{
					_objPort.Close();
					_objPort.Dispose();
					_objPort = null;
				}
			}
		}
		/// <summary>
		/// 전원 켜기
		/// </summary>
		/// <returns></returns>
		public bool PowerOn()
		{
			if (_objPort == null) Initialize();
			if (!IsOpen) return false;

// 			Byte[] command = new Byte[6] {0x23, 
// 				Convert.ToByte(''),
// 			0x0D, 0x0A};

			lock(lockPort)
			{
				_objPort.WriteLine("ka 01 01");
                if (LogOccurred != null) LogOccurred(this, new SerialLogEventArgs(String.Format("POWER_ON_WRITE : {0}", "ka 01 01")));
				System.Threading.Thread.Sleep(500);

				String sReturn = String.Empty;
				if (0 < _objPort.BytesToRead)
				{
					byte[] readBytes = new byte[_objPort.BytesToRead];
					_objPort.Read(readBytes, 0, _objPort.BytesToRead);
                    if (LogOccurred != null)
                        LogOccurred(this, new SerialLogEventArgs("POWER_ON_READ : " + Convert_byte(readBytes)));
					sReturn = System.Text.ASCIIEncoding.ASCII.GetString(readBytes);
				} 
				return sReturn.Contains("01x");
			}
		}
		/// <summary>
		/// 전원 끄기
		/// </summary>
		/// <returns></returns>
		public bool PowerOff()
		{
			if (_objPort == null) Initialize();
			if (!IsOpen) return false;

			lock (lockPort)
			{
				_objPort.WriteLine("ka 01 00");
                if (LogOccurred != null) LogOccurred(this, new SerialLogEventArgs(String.Format("POWER_OFF_WRITE : {0}", "ka 01 00")));
				System.Threading.Thread.Sleep(500);

				String sReturn = String.Empty;
				if (0 < _objPort.BytesToRead)
				{
					byte[] readBytes = new byte[_objPort.BytesToRead];
					_objPort.Read(readBytes, 0, _objPort.BytesToRead);
                    if (LogOccurred != null)
                        LogOccurred(this, new SerialLogEventArgs("POWER_OFF_READ : " + Convert_byte(readBytes)));
					sReturn = System.Text.ASCIIEncoding.ASCII.GetString(readBytes);
				} 
				return sReturn.Contains("00x");
			}
		}

		/// <summary>
		/// 자동 조절
		/// </summary>
		/// <returns></returns>
		public bool Auto()
		{
			if (_objPort == null) Initialize();
			if (!IsOpen) return false;

			lock (lockPort)
			{
				_objPort.WriteLine("ju 01 01");
                if (LogOccurred != null) LogOccurred(this, new SerialLogEventArgs(String.Format("AUTO_WRITE : {0}\n", "ju 01 01")));
				System.Threading.Thread.Sleep(500);

				String sReturn = String.Empty;
				if (0 < _objPort.BytesToRead)
				{
					byte[] readBytes = new byte[_objPort.BytesToRead];
					_objPort.Read(readBytes, 0, _objPort.BytesToRead);
                    if (LogOccurred != null)
                        LogOccurred(this, new SerialLogEventArgs("AUTO_READ : " + Convert_byte(readBytes)));
					sReturn = System.Text.ASCIIEncoding.ASCII.GetString(readBytes);
				}
				return sReturn.Contains("01x");
			}
		}

		/// <summary>
		/// 볼륨 조절
		/// </summary>
		/// <param name="nValue"></param>
		/// <returns></returns>
		public bool Volume(int nValue)
		{
			if (_objPort == null) Initialize();
			if (!IsOpen) return false;

			lock (lockPort)
			{
				int adjustedValue = (int)(((double)(nValue*64) / 100.0f));
 
				_objPort.WriteLine(String.Format("kf 01 {0}", adjustedValue.ToString("D2")));
                if (LogOccurred != null) LogOccurred(this, new SerialLogEventArgs(String.Format("VOLUME_WRITE : {0}\n", "kf 01 " + adjustedValue.ToString("D2"))));
				System.Threading.Thread.Sleep(500);

				String sReturn = String.Empty;
				if (0 < _objPort.BytesToRead)
				{
					byte[] readBytes = new byte[_objPort.BytesToRead];
					_objPort.Read(readBytes, 0, _objPort.BytesToRead);
                    if (LogOccurred != null)
                        LogOccurred(this, new SerialLogEventArgs("VOLUME_READ : " + Convert_byte(readBytes)));
					sReturn = System.Text.ASCIIEncoding.ASCII.GetString(readBytes);
				}
				return sReturn.Contains("OK");
			}
		}

		/// <summary>
		/// 소스 제어
		/// </summary>
		/// <param name="mode"></param>
		/// <returns></returns>
		public bool Source(SourceMode mode)
		{
			if (_objPort == null) Initialize();
			if (!IsOpen) return false;

			lock (lockPort)
			{
				String sReturn = String.Empty;
				switch (mode)
				{
					case SourceMode.sourceGeneral:
						_objPort.WriteLine("kb 01 00");
                        if (LogOccurred != null) LogOccurred(this, new SerialLogEventArgs(String.Format("SOURCE_WRITE : {0}\n", "kb 01 00")));
						System.Threading.Thread.Sleep(500);

						if (0 < _objPort.BytesToRead)
						{
							byte[] readBytes = new byte[_objPort.BytesToRead];
							_objPort.Read(readBytes, 0, _objPort.BytesToRead);
                            if (LogOccurred != null)
                                LogOccurred(this, new SerialLogEventArgs("SOURCE_READ : " + Convert_byte(readBytes)));
							sReturn = System.Text.ASCIIEncoding.ASCII.GetString(readBytes);
						} 
						return sReturn.Contains("00x");
					case SourceMode.sourceRGB:
						_objPort.WriteLine("kb 01 04");
                        if (LogOccurred != null) LogOccurred(this, new SerialLogEventArgs(String.Format("SOURCE_WRITE : {0}\n", "kb 01 04")));
						System.Threading.Thread.Sleep(500);

						if (0 < _objPort.BytesToRead)
						{
							byte[] readBytes = new byte[_objPort.BytesToRead];
							_objPort.Read(readBytes, 0, _objPort.BytesToRead);
                            if (LogOccurred != null)
                                LogOccurred(this, new SerialLogEventArgs("SOURCE_READ : " + Convert_byte(readBytes)));
							sReturn = System.Text.ASCIIEncoding.ASCII.GetString(readBytes);
						} 
						return sReturn.Contains("04x");
					case SourceMode.sourceDVI:
						_objPort.WriteLine("kb 01 05");
                        if (LogOccurred != null) LogOccurred(this, new SerialLogEventArgs(String.Format("SOURCE_WRITE : {0}\n", "kb 01 05")));
						System.Threading.Thread.Sleep(500);

						if (0 < _objPort.BytesToRead)
						{
							byte[] readBytes = new byte[_objPort.BytesToRead];
							_objPort.Read(readBytes, 0, _objPort.BytesToRead);
                            if (LogOccurred != null)
                                LogOccurred(this, new SerialLogEventArgs("SOURCE_READ : " + Convert_byte(readBytes)));
							sReturn = System.Text.ASCIIEncoding.ASCII.GetString(readBytes);
						} 
						return sReturn.Contains("05x");
					case SourceMode.sourceSVideo:
						_objPort.WriteLine("kb 01 03");
                        if (LogOccurred != null) LogOccurred(this, new SerialLogEventArgs(String.Format("SOURCE_WRITE : {0}\n", "kb 01 03")));
						System.Threading.Thread.Sleep(500);

						if (0 < _objPort.BytesToRead)
						{
							byte[] readBytes = new byte[_objPort.BytesToRead];
							_objPort.Read(readBytes, 0, _objPort.BytesToRead);
                            if (LogOccurred != null)
                                LogOccurred(this, new SerialLogEventArgs("SOURCE_READ : " + Convert_byte(readBytes)));
							sReturn = System.Text.ASCIIEncoding.ASCII.GetString(readBytes);
						} 
						return sReturn.Contains("03x");
					case SourceMode.sourceComposite:
						_objPort.WriteLine("kb 01 01");
                        if (LogOccurred != null) LogOccurred(this, new SerialLogEventArgs(String.Format("SOURCE_WRITE : {0}\n", "kb 01 01")));
						System.Threading.Thread.Sleep(500);

						if (0 < _objPort.BytesToRead)
						{
							byte[] readBytes = new byte[_objPort.BytesToRead];
							_objPort.Read(readBytes, 0, _objPort.BytesToRead);
                            if (LogOccurred != null)
                                LogOccurred(this, new SerialLogEventArgs("SOURCE_READ : " + Convert_byte(readBytes)));
							sReturn = System.Text.ASCIIEncoding.ASCII.GetString(readBytes);
						} 
						return sReturn.Contains("01x");
					case SourceMode.sourceComponent:
						_objPort.WriteLine("kb 01 02");
                        if (LogOccurred != null) LogOccurred(this, new SerialLogEventArgs(String.Format("SOURCE_WRITE : {0}\n", "kb 01 02")));
						System.Threading.Thread.Sleep(500);

						if (0 < _objPort.BytesToRead)
						{
							byte[] readBytes = new byte[_objPort.BytesToRead];
							_objPort.Read(readBytes, 0, _objPort.BytesToRead);
                            if (LogOccurred != null)
                                LogOccurred(this, new SerialLogEventArgs("SOURCE_READ : " + Convert_byte(readBytes)));
							sReturn = System.Text.ASCIIEncoding.ASCII.GetString(readBytes);
						}
						return sReturn.Contains("02x");
				}

				throw new NotImplementedException();

				return false;
			}
		}

		/// <summary>
		/// 상태 반환
		/// </summary>
		/// <param name="status">상태를 확인할 값</param>
		/// <returns>반환된 상태</returns>
		public object Status(SerialPortStatus status)
		{
			if (_objPort == null) Initialize();
			if (!IsOpen) return false;

			lock (lockPort)
			{
				String sReturn = String.Empty;
				switch (status)
				{
					case SerialPortStatus.statusPower:
						_objPort.WriteLine("ka 01 FF");
						System.Threading.Thread.Sleep(500);

						if (0 < _objPort.BytesToRead)
						{
							byte[] readBytes = new byte[_objPort.BytesToRead];
							_objPort.Read(readBytes, 0, _objPort.BytesToRead);
                            if (LogOccurred != null)
                                LogOccurred(this, new SerialLogEventArgs("STATUS_READ : " + Convert_byte(readBytes)));
							sReturn = System.Text.ASCIIEncoding.ASCII.GetString(readBytes);
						}

						return (sReturn.Contains("01x"));

					case SerialPortStatus.statusSource:
						_objPort.WriteLine("kb 01 FF");
						System.Threading.Thread.Sleep(500);

						if (0 < _objPort.BytesToRead)
						{
							byte[] readBytes = new byte[_objPort.BytesToRead];
							_objPort.Read(readBytes, 0, _objPort.BytesToRead);
                            if (LogOccurred != null)
                                LogOccurred(this, new SerialLogEventArgs("STATUS_READ : " + Convert_byte(readBytes)));
							sReturn = System.Text.ASCIIEncoding.ASCII.GetString(readBytes);
						}

						if (sReturn.Contains("00x")) return "TV";
						else if (sReturn.Contains("01x")) return "COMPOSITE";
						else if (sReturn.Contains("02x")) return "COMPONENT1";
						else if (sReturn.Contains("03x")) return "COMPONENT2";
						else if (sReturn.Contains("04x")) return "RGB";
						else if (sReturn.Contains("05x")) return "DVI/HDMI";
						else
						{
							if(sReturn.Length > 9)
							{
								return sReturn.Substring(7, 2);
							}
							else
								return "Fail";
						}
					case SerialPortStatus.statusVolume:
						_objPort.WriteLine("kf 01 FF");
						System.Threading.Thread.Sleep(500);

						if (0 < _objPort.BytesToRead)
						{
							byte[] readBytes = new byte[_objPort.BytesToRead];
							_objPort.Read(readBytes, 0, _objPort.BytesToRead);
                            if (LogOccurred != null)
                                LogOccurred(this, new SerialLogEventArgs("READ : " + Convert_byte(readBytes)));
							sReturn = System.Text.ASCIIEncoding.ASCII.GetString(readBytes);
						}

						if (sReturn.Contains("OK"))
						{
							try
							{
								double dValue = Convert.ToDouble(sReturn.Substring(7, 2));
								return ((int)(100.0f * dValue / 64.0f)).ToString();
							}
							catch
							{
								return "??";
							}

						}
						else
							return "--";
				}
				throw new NotImplementedException();

				return false;
			}
		}

		#endregion
	}
}
