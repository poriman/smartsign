﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Ports;

namespace SerialPortController
{
	/// <summary>
	/// HYUNDAI Display Serial Port 제어
	/// </summary>
	class HyundaiDisplayControl : ISerialPortControl
	{
		/// <summary>
		/// Lock 오브젝트 선언
		/// </summary>
		static readonly object lockPort = new object();

		SerialPort _objPort = null;
		String _comPort = "COM1";

		#region ISerialPortControl 구현부

        /// <summary>
        /// 로그 이벤트
        /// </summary>
        public event SerialLogEventHandler LogOccurred;

		/// <summary>
		/// Com Port 이름
		/// </summary>
		public String ComPort { get { return _comPort; } }
		/// <summary>
		/// Baud Rate 값
		/// </summary>
		public int BaudRate { get { return 9600; } }
		/// <summary>
		/// Data Bit 값
		/// </summary>
		public int DataBit { get { return 8; } }
		/// <summary>
		/// Stop Bit 값
		/// </summary>
		public StopBits StopBit { get { return StopBits.One; } }
		/// <summary>
		/// Parity 값
		/// </summary>
		public Parity ParityValue { get { return Parity.None; } }
		/// <summary>
		/// 포트가 열려있는지
		/// </summary>
		public bool IsOpen 
		{ 
			get
			{
				lock (lockPort)
				{
					if (_objPort != null)
					{
						return _objPort.IsOpen;
					}
					else return false;
				}
			}
		}
		/// <summary>
		/// 초기화
		/// </summary>
		/// <returns></returns>
		public bool Initialize()
		{
			Uninitialize();

			lock(lockPort)
			{
				_objPort = new SerialPort(ComPort, BaudRate, ParityValue, DataBit, StopBit);
				_objPort.ReadTimeout = 2000;
				_objPort.WriteTimeout = 2000;
				_objPort.Handshake = Handshake.None;
				_objPort.NewLine = "\n";
				_objPort.Open();
			}

			return IsOpen;
		}
		/// <summary>
		/// 초기화
		/// </summary>
		/// <returns></returns>
		public bool Initialize(String PortName)
		{
			_comPort = PortName;
			return Initialize();
		}

		/// <summary>
		/// 릴리즈
		/// </summary>
		public void Uninitialize()
		{
			if(IsOpen)
			{
				lock(lockPort)
				{
					_objPort.Close();
					_objPort.Dispose();
					_objPort = null;
				}
			}
		}
		/// <summary>
		/// 전원 켜기
		/// </summary>
		/// <returns></returns>
		public bool PowerOn()
		{
			if (_objPort == null) Initialize();
			if (!IsOpen) return false;

			lock(lockPort)
			{
                try
                {
                    _objPort.WriteLine("did:all-pn.");
                    if (LogOccurred != null) LogOccurred(this, new SerialLogEventArgs(String.Format("WRITE: {0}", "did:all-pn.\n")));
                    System.Threading.Thread.Sleep(500);

                    return true;

                }
                catch { return false; }

			}
		}
		/// <summary>
		/// 전원 끄기
		/// </summary>
		/// <returns></returns>
		public bool PowerOff()
		{
			if (_objPort == null) Initialize();
			if (!IsOpen) return false;

			lock (lockPort)
			{
                try
                {
                    _objPort.WriteLine("did:all-pf.");
                    if (LogOccurred != null) LogOccurred(this, new SerialLogEventArgs(String.Format("WRITE: {0}", "did:all-pf.\n")));
                    System.Threading.Thread.Sleep(2000);
                }
                catch { return false; };
                return true;
			}
		}

		/// <summary>
		/// 자동 조절
		/// </summary>
		/// <returns></returns>
		public bool Auto()
		{
            return false;
		}

		/// <summary>
		/// 볼륨 조절
		/// </summary>
		/// <param name="nValue"></param>
		/// <returns></returns>
		public bool Volume(int nValue)
		{
            return false;
		}

		/// <summary>
		/// 소스 제어
		/// </summary>
		/// <param name="mode"></param>
		/// <returns></returns>
		public bool Source(SourceMode mode)
		{
			if (_objPort == null) Initialize();
			if (!IsOpen) return false;

			lock (lockPort)
			{
				String sReturn = String.Empty;
				switch (mode)
				{
					case SourceMode.sourceGeneral:
                        _objPort.WriteLine("did:all-sd.");
                        if (LogOccurred != null) LogOccurred(this, new SerialLogEventArgs(String.Format("WRITE: {0}", "did:all-sd.\n")));
						System.Threading.Thread.Sleep(500);

						if (0 < _objPort.BytesToRead)
						{
							byte[] readBytes = new byte[_objPort.BytesToRead];
							_objPort.Read(readBytes, 0, _objPort.BytesToRead);
                            if (LogOccurred != null) LogOccurred(this, new SerialLogEventArgs(String.Format("READ: {0}", readBytes)));
							sReturn = System.Text.ASCIIEncoding.ASCII.GetString(readBytes);
						} 
						return sReturn.Contains("00x");
					case SourceMode.sourceRGB:
						_objPort.WriteLine("did:all-sp.");
                        if (LogOccurred != null) LogOccurred(this, new SerialLogEventArgs(String.Format("WRITE: {0}","did:all-sp.\n")));
						System.Threading.Thread.Sleep(500);

						if (0 < _objPort.BytesToRead)
						{
							byte[] readBytes = new byte[_objPort.BytesToRead];
							_objPort.Read(readBytes, 0, _objPort.BytesToRead);

							sReturn = System.Text.ASCIIEncoding.ASCII.GetString(readBytes);
						} 
						return sReturn.Contains("04x");
					case SourceMode.sourceDVI:
                        return false;
					case SourceMode.sourceSVideo:
                        return false;
					case SourceMode.sourceComposite:
                        _objPort.WriteLine("did:all-sa.");
                        if (LogOccurred != null) LogOccurred(this, new SerialLogEventArgs(String.Format("WRITE: {0}", "did:all-sa.\n")));
                        System.Threading.Thread.Sleep(500);

                        if (0 < _objPort.BytesToRead)
                        {
                            byte[] readBytes = new byte[_objPort.BytesToRead];
                            _objPort.Read(readBytes, 0, _objPort.BytesToRead);
                            if (LogOccurred != null) LogOccurred(this, new SerialLogEventArgs(String.Format("READ: {0}", readBytes)));
                            sReturn = System.Text.ASCIIEncoding.ASCII.GetString(readBytes);
                        }
                        return sReturn.Contains("02x");
                    case SourceMode.sourceComponent:
                        _objPort.WriteLine("did:all-sc.");
                        if (LogOccurred != null) LogOccurred(this, new SerialLogEventArgs(String.Format("WRITE: {0}", "did:all-sc.\n")));
						System.Threading.Thread.Sleep(500);

						if (0 < _objPort.BytesToRead)
						{
							byte[] readBytes = new byte[_objPort.BytesToRead];
							_objPort.Read(readBytes, 0, _objPort.BytesToRead);
                            if (LogOccurred != null) LogOccurred(this, new SerialLogEventArgs(String.Format("READ: {0}", readBytes)));
							sReturn = System.Text.ASCIIEncoding.ASCII.GetString(readBytes);
						}
						return sReturn.Contains("02x");
				}

				throw new NotImplementedException();

				return false;
			}
		}

		/// <summary>
		/// 상태 반환
		/// </summary>
		/// <param name="status">상태를 확인할 값</param>
		/// <returns>반환된 상태</returns>
		public object Status(SerialPortStatus status)
		{
			if (_objPort == null) Initialize();
			if (!IsOpen) return false;

			lock (lockPort)
			{
				String sReturn = String.Empty;
				switch (status)
				{
					case SerialPortStatus.statusPower:
						_objPort.WriteLine("did:01-ps.");
                        if (LogOccurred != null) LogOccurred(this, new SerialLogEventArgs(String.Format("WRITE: {0}", "did:01-ps.\n")));
						System.Threading.Thread.Sleep(500);

						if (0 < _objPort.BytesToRead)
						{
							byte[] readBytes = new byte[_objPort.BytesToRead];
							_objPort.Read(readBytes, 0, _objPort.BytesToRead);
                            if (LogOccurred != null) LogOccurred(this, new SerialLogEventArgs(String.Format("READ: {0}", readBytes)));
							sReturn = System.Text.ASCIIEncoding.ASCII.GetString(readBytes);
						}

						return (sReturn.StartsWith("on"));

					case SerialPortStatus.statusSource:
                        _objPort.WriteLine("did:01-is.");
                        if (LogOccurred != null) LogOccurred(this, new SerialLogEventArgs(String.Format("WRITE: {0}", "did:01-is.\n")));
						System.Threading.Thread.Sleep(500);

						if (0 < _objPort.BytesToRead)
						{
							byte[] readBytes = new byte[_objPort.BytesToRead];
							_objPort.Read(readBytes, 0, _objPort.BytesToRead);
                            if (LogOccurred != null) LogOccurred(this, new SerialLogEventArgs(String.Format("READ: {0}", readBytes)));
							sReturn = System.Text.ASCIIEncoding.ASCII.GetString(readBytes);
						}

						if (sReturn.Contains("input hdmi.")) return "HDMI";
						else if (sReturn.Contains("input pc.")) return "PC";
						else if (sReturn.Contains("input component.")) return "COMPONENT";
						else if (sReturn.Contains("input av.")) return "AV";
						else
						{
                            return "Fail";
						}
					case SerialPortStatus.statusVolume:
						return "--";
				}
				throw new NotImplementedException();

				return false;
			}
		}

		#endregion
	}
}
