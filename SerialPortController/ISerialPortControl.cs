﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Ports;

namespace SerialPortController
{
	/// <summary>
	/// 시리얼 포트 제어 인터페이스
	/// </summary>
	public interface ISerialPortControl
	{
		/// <summary>
		/// 포트가 열려있는지
		/// </summary>
		bool IsOpen { get; }
		/// <summary>
		/// Com Port 이름
		/// </summary>
		String ComPort { get; }
		/// <summary>
		/// Baud Rate 값
		/// </summary>
		int BaudRate { get; }
		/// <summary>
		/// Data Bit 값
		/// </summary>
		int DataBit { get; }
		/// <summary>
		/// Stop Bit 값
		/// </summary>
		StopBits StopBit { get; }
		/// <summary>
		/// Parity 값
		/// </summary>
		Parity ParityValue { get; }

		/// <summary>
		/// 초기화
		/// </summary>
		/// <returns></returns>
		bool Initialize();
		/// <summary>
		/// 초기화
		/// </summary>
		/// <returns></returns>
		bool Initialize(String PortName);
		/// <summary>
		/// 릴리즈
		/// </summary>
		void Uninitialize();
		/// <summary>
		/// 전원 켜기
		/// </summary>
		/// <returns></returns>
		bool PowerOn();
		/// <summary>
		/// 전원 끄기
		/// </summary>
		/// <returns></returns>
		bool PowerOff();
		/// <summary>
		/// 자동 조절
		/// </summary>
		/// <returns></returns>
		bool Auto();
		/// <summary>
		/// 볼륨 조절
		/// </summary>
		/// <param name="nValue"></param>
		/// <returns></returns>
		bool Volume(int nValue);
		/// <summary>
		/// 소스 제어
		/// </summary>
		/// <param name="mode"></param>
		/// <returns></returns>
		bool Source(SourceMode mode);
		/// <summary>
		/// 상태 반환
		/// </summary>
		/// <param name="status">상태를 확인할 값</param>
		/// <returns>반환된 상태</returns>
		object Status(SerialPortStatus status);

        /// <summary>
        /// 로그 이벤트
        /// </summary>
        event SerialLogEventHandler LogOccurred;
	}

    public delegate void SerialLogEventHandler(object sender, SerialLogEventArgs e);

    /// <summary>
    /// Serial Argument
    /// </summary>
    public class SerialLogEventArgs : EventArgs
    {
        public string Message
        {
            get;
            set;
        }

        public SerialLogEventArgs(string sMessage)
        {
            this.Message = sMessage;
        }
    }
}
