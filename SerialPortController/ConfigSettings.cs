using System;
using System.Xml;  
using System.Configuration;
using System.Reflection;

namespace SerialPortController
{
    class ConfigSettings
    {
        public static object lockObject = new object();

        public static string config_path;

        public ConfigSettings(string config_path)
        {
            ConfigSettings.config_path = config_path;
        }

        public static string ReadSetting(string key)
        {
            lock (lockObject)
            {
                string res = "";
                // load config document for current assembly
                XmlDocument doc = loadConfigDocument();
                // retrieve appSettings node
                XmlNode node = doc.SelectSingleNode("//appSettings");
                if (node == null) return res;
                try
                {
                    // select the 'add' element that contains the key
                    XmlElement elem = (XmlElement)node.SelectSingleNode(string.Format("//add[@key='{0}']", key));
                    if (elem != null) res = elem.GetAttribute("value");
                }
                catch (Exception ex)
                {
                    throw;
                }
                return res;
            }
        }

        public static void WriteSetting(string key, string value)
        {
            lock (lockObject)
            {
                // load config document for current assembly
                XmlDocument doc = loadConfigDocument();

                // retrieve appSettings node
                XmlNode node = doc.SelectSingleNode("//appSettings");

                if (node == null)
                    throw new InvalidOperationException("appSettings section not found in config file.");

                try
                {
                    // select the 'add' element that contains the key
                    XmlElement elem = (XmlElement)node.SelectSingleNode(string.Format("//add[@key='{0}']", key));

                    if (elem != null)
                    {
                        // add value for key
                        elem.SetAttribute("value", value);
                    }
                    else
                    {
                        // key was not found so create the 'add' element 
                        // and set it's key/value attributes 
                        elem = doc.CreateElement("add");
                        elem.SetAttribute("key", key);
                        elem.SetAttribute("value", value);
                        node.AppendChild(elem);
                    }
                    doc.Save(getConfigFilePath());
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
        }

        public static void RemoveSetting(string key)
        {
            lock (lockObject)
            {
                // load config document for current assembly
                XmlDocument doc = loadConfigDocument();

                // retrieve appSettings node
                XmlNode node = doc.SelectSingleNode("//appSettings");

                try
                {
                    if (node == null)
                        throw new InvalidOperationException("appSettings section not found in config file.");
                    else
                    {
                        // remove 'add' element with coresponding key
                        node.RemoveChild(node.SelectSingleNode(string.Format("//add[@key='{0}']", key)));
                        doc.Save(getConfigFilePath());
                    }
                }
                catch (NullReferenceException e)
                {
                    throw new Exception(string.Format("The key {0} does not exist.", key), e);
                }
            }
        }

        private static XmlDocument loadConfigDocument()
        {
            XmlDocument doc = null;
			try
			{
				doc = new XmlDocument();
				doc.Load(getConfigFilePath());
				XmlNode node = doc.SelectSingleNode("//appSettings");
			}
			catch (Exception ex)
			{
                // Create a document type node and  
                // add it to the document.
                XmlDocumentType doctype;
                doctype = doc.CreateDocumentType("configuration", null, null, null);
                doc.AppendChild(doctype);

                // Create the root element and 
                // add it to the document.
                XmlNode root = doc.AppendChild(doc.CreateElement("configuration"));
                root.AppendChild(doc.CreateElement("appSettings"));
                doc.Save(getConfigFilePath());
			}
            return doc;
        }

        private static string getConfigFilePath()
        {
			if (String.IsNullOrEmpty(ConfigSettings.config_path))
				throw new Exception("config path is empty!");

			return ConfigSettings.config_path;
        }
    }
}