﻿namespace BroadCastTester
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnRun = new System.Windows.Forms.Button();
			this.tbResult = new System.Windows.Forms.TextBox();
			this.rdServer = new System.Windows.Forms.RadioButton();
			this.rdClient = new System.Windows.Forms.RadioButton();
			this.SuspendLayout();
			// 
			// btnRun
			// 
			this.btnRun.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.btnRun.Location = new System.Drawing.Point(297, 273);
			this.btnRun.Name = "btnRun";
			this.btnRun.Size = new System.Drawing.Size(75, 23);
			this.btnRun.TabIndex = 0;
			this.btnRun.Text = "시작";
			this.btnRun.UseVisualStyleBackColor = true;
			this.btnRun.Click += new System.EventHandler(this.btnRun_Click);
			// 
			// tbResult
			// 
			this.tbResult.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
						| System.Windows.Forms.AnchorStyles.Left)
						| System.Windows.Forms.AnchorStyles.Right)));
			this.tbResult.Location = new System.Drawing.Point(12, 12);
			this.tbResult.Multiline = true;
			this.tbResult.Name = "tbResult";
			this.tbResult.Size = new System.Drawing.Size(360, 255);
			this.tbResult.TabIndex = 1;
			// 
			// rdServer
			// 
			this.rdServer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.rdServer.AutoSize = true;
			this.rdServer.Checked = true;
			this.rdServer.Location = new System.Drawing.Point(12, 276);
			this.rdServer.Name = "rdServer";
			this.rdServer.Size = new System.Drawing.Size(49, 17);
			this.rdServer.TabIndex = 2;
			this.rdServer.TabStop = true;
			this.rdServer.Text = "서버";
			this.rdServer.UseVisualStyleBackColor = true;
			// 
			// rdClient
			// 
			this.rdClient.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.rdClient.AutoSize = true;
			this.rdClient.Location = new System.Drawing.Point(67, 276);
			this.rdClient.Name = "rdClient";
			this.rdClient.Size = new System.Drawing.Size(85, 17);
			this.rdClient.TabIndex = 3;
			this.rdClient.TabStop = true;
			this.rdClient.Text = "클라이언트";
			this.rdClient.UseVisualStyleBackColor = true;
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(384, 308);
			this.Controls.Add(this.rdClient);
			this.Controls.Add(this.rdServer);
			this.Controls.Add(this.tbResult);
			this.Controls.Add(this.btnRun);
			this.Name = "Form1";
			this.Text = "브로드캐스팅 테스트";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button btnRun;
		private System.Windows.Forms.TextBox tbResult;
		private System.Windows.Forms.RadioButton rdServer;
		private System.Windows.Forms.RadioButton rdClient;
	}
}

