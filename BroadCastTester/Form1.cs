﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DigitalSignage.Common;

namespace BroadCastTester
{
	public partial class Form1 : Form
	{
		bool bStarted = false;
		IBroadcastObject objBroadcast = null;

		public Form1()
		{
			InitializeComponent();
		}

		private void btnRun_Click(object sender, EventArgs e)
		{
			bStarted = !bStarted;

			if(bStarted)
			{
				bool isServer = rdServer.Checked;

				if (isServer)
				{
					this.btnRun.Enabled = false;

					objBroadcast = new BroadcastServer();
					objBroadcast.Initialiaze();

					BroadcastServer server = objBroadcast as BroadcastServer;

					server.BeginServer += new EventHandler(server_BeginServer);
					server.EndServer += new EventHandler(server_EndServer);
					server.RecvMessage += new SocketEventHandler(client_RecvMessage);
				}
				else
				{
					objBroadcast = new BroadcastClient();
					objBroadcast.Initialiaze();

					BroadcastClient client = objBroadcast as BroadcastClient;

					client.BeginBroadCast += new EventHandler(client_BeginBroadCast);
					client.EndBroadCast += new EventHandler(client_EndBroadCast);
					client.RecvMessage += new SocketEventHandler(client_RecvMessage);
					client.BroadCast();
				}
			}
			else
			{
				objBroadcast.Uninitialize();

				BroadcastClient client = objBroadcast as BroadcastClient;
				if(client != null)
				{
					client.BeginBroadCast -= new EventHandler(client_BeginBroadCast);
					client.EndBroadCast -= new EventHandler(client_EndBroadCast);
					client.RecvMessage -= new SocketEventHandler(client_RecvMessage);

				}
				else 
				{
					BroadcastServer server = objBroadcast as BroadcastServer;

					if(server != null)
					{
						server.BeginServer -= new EventHandler(server_BeginServer);
						server.EndServer -= new EventHandler(server_EndServer);
						server.RecvMessage -= new SocketEventHandler(client_RecvMessage);
					}
				}
				objBroadcast = null;
			}
		}

		void server_EndServer(object sender, EventArgs e)
		{
			MethodInvoker method = delegate
			{
				this.btnRun.Enabled = true;
				tbResult.Text += "Server 끝\r\n";
			};

			if (InvokeRequired)
				BeginInvoke(method);
			else
				method.Invoke();
		}

		void server_BeginServer(object sender, EventArgs e)
		{
			MethodInvoker method = delegate
			{
				this.btnRun.Enabled = false;
				tbResult.Text += "Server 시작\r\n";
			};

			if (InvokeRequired)
				BeginInvoke(method);
			else
				method.Invoke();
		}

		void client_RecvMessage(object sender, SocketEventArgs e)
		{
			MethodInvoker method = delegate
			{
				tbResult.Text += String.Format("찾음 : (IP, {0}) (Message, {1})\r\n", e.IPEndpoint.Address.ToString(), e.ReceivedData);
			};

			if (InvokeRequired)
				BeginInvoke(method);
			else
				method.Invoke();
		}

		void client_EndBroadCast(object sender, EventArgs e)
		{
			MethodInvoker method = delegate
			{
				this.btnRun.Enabled = true;
				tbResult.Text += "BroadCast 끝\r\n";
			};

			if (InvokeRequired)
				BeginInvoke(method);
			else
				method.Invoke(); 
		}

		void client_BeginBroadCast(object sender, EventArgs e)
		{
			MethodInvoker method = delegate
			{
				this.btnRun.Enabled = false;
				tbResult.Text += "BroadCast 시작\r\n";
			};

			if (InvokeRequired)
				BeginInvoke(method);
			else
				method.Invoke();
		
		}
	}
}
