﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("WPFInterop")]
[assembly: AssemblyDescription("WPF Win32 Interop Render Control")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Leslie Godwin & Jeremiah Morri")]
[assembly: AssemblyProduct("WPFInterop")]
[assembly: AssemblyCopyright("Copyright © Organization 2007")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("50e11de8-0603-4b7c-b95b-909787a9f6cb")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
[assembly: AssemblyVersion("0.17.0.0")]
[assembly: AssemblyFileVersion("0.17.0.0")]
