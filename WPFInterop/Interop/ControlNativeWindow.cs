using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using Win32;

namespace WPFInterop.Interop
{
    public class ControlNativeWindow : IMessageFilter
    {
        private bool _allowPaint;

        public bool AllowPaint
        {
            get { return _allowPaint; }
            set { _allowPaint = value; }
        }

        #region IMessageFilter Members

        public bool PreFilterMessage(ref Message m)
        {
            switch (m.Msg)
            {
                case User32.WM_ERASEBKGND:
                case User32.WM_PAINT:
                    if (_allowPaint)
                        break;
                    else
                        return;
            }
        }

        #endregion
    }
}
