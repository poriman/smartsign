﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Windows;
using System.Globalization;
using System.Threading;
using System.Text.RegularExpressions;

namespace WPFDesigner
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
		public static Hashtable CommandLineArgs = new Hashtable();

        public App()
        {
            string strlanguage = ConfigurationManager.AppSettings.Get("LANGUAGE");
            if (strlanguage.Equals("0"))
                Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            else if (strlanguage.Equals("1"))
                Thread.CurrentThread.CurrentCulture = new CultureInfo("ko-KR");
          
        }

		private void Application_Startup(object sender, StartupEventArgs e)
		{
			if (e.Args.Length == 0) return;

			string arg = e.Args[0] as string;

			if(System.IO.File.Exists(arg))
			{
				// Store command line arg and value
				CommandLineArgs["open"] = arg;
			}
		}
    }
}