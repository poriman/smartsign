﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Threading;
using System.Windows.Threading;

using NLog;
using WPFDesigner.Cultures;

namespace WPFDesigner
{
    /// <summary>
    /// Interaction logic for CopyBox.xaml
    /// </summary>
    public partial class CopyBox : Window
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        DoubleAnimation animation;
        RotateTransform transform;
        //DispatcherTimer dt;
        //Thread th;

        public CopyBox()
        {
            InitializeComponent();
            animation = new DoubleAnimation();
            animation.From = 0;
            animation.To = 360;
            animation.Duration = new Duration(new TimeSpan(0, 0, 10));
            animation.RepeatBehavior = RepeatBehavior.Forever;
            transform = new RotateTransform();
            transform.CenterX = ellipse.Width / 2;
            transform.CenterY = ellipse.Height / 2;
            ellipse.RenderTransform = transform;
            transform.BeginAnimation(RotateTransform.AngleProperty, animation);
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            
        }

        public bool Copy(string from, string to)
        {
            Show();
			try
			{
				System.IO.File.Copy(from, to, true);
			}
			catch (Exception ex)
			{
				MessageBox.Show(Properties.Resources.mbUnableToCopyMedia);
				logger.Error(ex + "");
				return false;
			}
			finally
			{
				Close();
			}

            return true;
            //th = new Thread(new ParameterizedThreadStart(Copy1));
            //string[] ss = new string[2];
            //ss[0] = from;
            //ss[1] = to;
            //parameters p = new parameters();
            //p.from = from;
            //p.to = to;
            //dt = new DispatcherTimer();
            //dt.Interval = new TimeSpan(0, 0, 0, 0, 500);
            //dt.Tick += new EventHandler(dt_Tick);
            //this.Show();
            
            //th.Start(p);
            //dt.Start();
        }

        //void dt_Tick(object sender, EventArgs e)
        //{
        //    if (!th.IsAlive)
        //    {
        //        this.Close();
        //        dt = null;
        //    }
        //}

        //public void Copy1(object param)
        //{
        //    System.IO.File.Copy(((parameters)param).from, ((parameters)param).to);
        //}

        //struct parameters
        //{
        //    public string from, to;
        //}
    }
}
