﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;
using System.Xml;
using System.Collections.ObjectModel;

namespace WPFDesigner
{
    /// <summary>
    /// 새로운 페이지 생성 창
    /// </summary>
    public partial class NewProjectWindow : Window
    {
        private double dresValue = (double)(3.0 / 4.0);
		private bool isInt = true;

		private bool bIsEditMode = false;

		/// <summary>
		/// 수정모드 인지
		/// </summary>
		public bool IsEditMode { get { return bIsEditMode; } set { bIsEditMode = true; } }

		/// <summary>
		/// 생성자
		/// </summary>
        public NewProjectWindow()
        {
            InitializeComponent();
			bIsEditMode = false;
			tbProjectName.IsEnabled = true;

        }

		#region 광고 관련
		/// <summary>
		/// 광고 여부
		/// </summary>
		public bool IsAdScreen
		{
			get { return this.cbIsAd.IsChecked == null ? false : this.cbIsAd.IsChecked.Value; }
			set { this.cbIsAd.IsChecked = value; }
		}

		/// <summary>
		/// 광고 시작 시간
		/// </summary>
		public DateTime AdStartTime
		{
			get
			{
				try
				{
					DateTime dt = startDateUI.SelectedDateTime.Value;
					TimeSpan ts = startTimeUI.SelectedTime;
					return new DateTime(dt.Year, dt.Month, dt.Day, ts.Hours, ts.Minutes, ts.Seconds);
				}
				catch { return DateTime.MinValue; }
			}

			set
			{
				startDateUI.SelectedDateTime = value;
				startTimeUI.SelectedTime = new TimeSpan(value.Hour, value.Minute, value.Second);
			}
		}

		/// <summary>
		/// 광고 종료 시간
		/// </summary>
		public DateTime AdEndTime
		{
			get
			{
				try
				{
					DateTime dt = finDateUI.SelectedDateTime.Value;
					TimeSpan ts = finTimeUI.SelectedTime;
					return new DateTime(dt.Year, dt.Month, dt.Day, ts.Hours, ts.Minutes, ts.Seconds);
				}
				catch { return DateTime.MaxValue; }
			}

			set
			{
				finDateUI.SelectedDateTime = value;
				finTimeUI.SelectedTime = new TimeSpan(value.Hour, value.Minute, value.Second);
			}
		}
		/// <summary>
		/// 메타태그 반환
		/// </summary>
		public String AdMetaTags
		{
			get
			{
                return gboxMetaTag.SelectedMetaTags;
			}
            set
            {
                gboxMetaTag.SelectedMetaTags = value;
            }
		}

		/// <summary>
		/// 광고 이벤트 시에만 재생할 것인지
		/// </summary>
		public int AdEventConstraint
		{
			get 
			{
				int nPlayFlag = 0;

				if( cbAdConstraintEvent.IsChecked == true)
					nPlayFlag |= (int)DigitalSignage.Common.ShowFlag.Show_Event;

				if( cbAdConstraintNonEvent.IsChecked == true)
					nPlayFlag |= (int)DigitalSignage.Common.ShowFlag.Show_NonEvent;

				return nPlayFlag; 
			}
			set 
			{
				this.cbAdConstraintEvent.IsChecked = (value & (int)DigitalSignage.Common.ShowFlag.Show_Event) > 0;
				this.cbAdConstraintNonEvent.IsChecked = (value & (int)DigitalSignage.Common.ShowFlag.Show_NonEvent) > 0;
			}
		}

		#endregion

		/// <summary>
		/// 현재 속성 로드
		/// </summary>
		/// <param name="currScreen"></param>
		public void LoadProperties(Screen currScreen)
		{
			bIsEditMode = true;
			this.IsConnect.IsChecked = false;
			this.heightBox.IsEnabled = true;
			this.Title = Properties.Resources.menuScreenProperties;
			this.bCreate.Content = Properties.Resources.buttonOk;

			try
			{
				widthBox.Text = Convert.ToInt32(((IDesignElement)currScreen).Width).ToString();
				heightBox.Text = Convert.ToInt32(((IDesignElement)currScreen).Height).ToString();

				tbProjectName.Text = ((IDesignElement)currScreen).Name;
				tbProjectName.IsEnabled = false;

				//	광고 관련 추가
				this.IsAdScreen = currScreen.IsAdScreen;
				this.AdStartTime = currScreen.Ad_StartTime;
				this.AdEndTime = currScreen.Ad_EndTime;
				this.AdMetaTags = currScreen.Ad_MetaTags;
				this.AdEventConstraint = currScreen.Ad_EventConstraint;
			}
			catch { }
		}

		/// <summary>
		/// Width 변경 이벤트
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
        void widthBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
			try
			{
				int digit = 0;
				if (!Int32.TryParse(e.Text, out digit))
				{
					e.Handled = true;
					isInt = false;
				}
				else
					isInt = true;
			}
			catch { }
        }

		/// <summary>
		/// Width 변경
		/// </summary>
        private void SetWidth()
        {
            if (resList == null)
                return;

            switch(resList.SelectedIndex)
            {
                case 0:
                    dresValue = (double)(4.0 / 3.0);
                    break;
                case 1:
                    dresValue = (double)(3.0 / 4.0);
                    break;
                case 2:
                    dresValue = (double)(16.0 / 9.0);
                    break;
                case 3:
                    dresValue = (double)(9.0 / 16.0);
                    break;
                default:
                    dresValue = (double)(3.0 / 4.0);
                    break;
            }

            if (this.heightBox != null && this.IsConnect.IsChecked == true)
            {
				try
				{
					this.heightBox.Text = ((int)(double.Parse(widthBox.Text) * dresValue)).ToString();
				}
				catch { }
            }
        }

		/// <summary>
		/// 비율 변경 이벤트
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
        private void resList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SetWidth();
        }

		/// <summary>
		/// 너비값 변경 이벤트
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
        private void widthBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (this.heightBox != null &&  this.IsConnect.IsChecked == true && isInt)
            {
				try
				{
					this.heightBox.Text = ((int)(double.Parse(widthBox.Text) * dresValue)).ToString();
				}
				catch
				{ }
            }
        }

		/// <summary>
		/// 연결 체크 이벤트
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
        private void IsConnect_Checked(object sender, RoutedEventArgs e)
        {
            if (this.IsConnect != null)
            {
                if (this.IsConnect.IsChecked == true)
                {
                    if (heightBox != null)
                        heightBox.IsEnabled = false;
                    SetWidth();
                }
                else if (heightBox != null)
                    heightBox.IsEnabled = true;
            }
        }

		/// <summary>
		/// 유효성 체크
		/// </summary>
		/// <returns></returns>
		public bool ValidationCheck()
		{
			if (String.IsNullOrEmpty(tbProjectName.Text))
			{
				MessageBox.Show(Properties.Resources.mbErrorScreenName, Properties.Resources.titleDesignerWindow);
				tbProjectName.Focus();
				return false;
			}

			if (String.IsNullOrEmpty(widthBox.Text) || String.IsNullOrEmpty(heightBox.Text))
			{
				MessageBox.Show(Properties.Resources.mbErrorScreenSize, Properties.Resources.titleDesignerWindow);
				
				if (String.IsNullOrEmpty(widthBox.Text)) widthBox.Focus();
				else heightBox.Focus();

				return false;
			}

			if (IsAdScreen)
			{
				if (AdStartTime >= AdEndTime)
				{
					MessageBox.Show(Properties.Resources.mbErrorAdTimeRange, Properties.Resources.titleDesignerWindow);
					
					startDateUI.Focus();

					return false;
				}
			}

			if (AdEventConstraint == 0)
			{
				MessageBox.Show(Properties.Resources.mbErrorPlayConstrait, Properties.Resources.titleDesignerWindow);
				cbAdConstraintNonEvent.Focus();
				return false;
			}

			try
			{
				int nWidth = Convert.ToInt32(widthBox.Text);
				int nHeight = Convert.ToInt32(heightBox.Text);

				if (nWidth == 0 || nWidth > 10000)
				{
					widthBox.Focus();
					throw new Exception();
				}
				else if (nHeight == 0 || nHeight > 10000)
				{
					heightBox.Focus();
					throw new Exception();
				}
			}
			catch
			{
				MessageBox.Show(Properties.Resources.mbErrorScreenSize, Properties.Resources.titleDesignerWindow);

				return false;
			}


			return true;
		}

		/// <summary>
		/// 텍스트 박스 포커스 이벤트
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void tb_GotKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
		{
			try
			{
				TextBox tb = sender as TextBox;
				tb.SelectAll();
				e.Handled = true;
			}
			catch {}
		}

		/// <summary>
		/// 광고 여부 체크 이벤트
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void cbIsAd_Checked(object sender, RoutedEventArgs e)
		{
			rdAdProperties.Height = new GridLength(1, GridUnitType.Star);
		}

		/// <summary>
		/// 광고 여부 언체크 이벤트
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void cbIsAd_Unchecked(object sender, RoutedEventArgs e)
		{
			rdAdProperties.Height = new GridLength(0);
		}


		/// <summary>
		/// 윈도우 폼 로드 이벤트
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void Window_Loaded(object sender, RoutedEventArgs e)
		{
			if(!this.bIsEditMode)
			{
				DateTime dtNow = DateTime.Now;

				this.startDateUI.SelectedDateTime = this.finDateUI.SelectedDateTime = dtNow;
				this.startTimeUI.SelectedTime = this.finTimeUI.SelectedTime = new TimeSpan(dtNow.Hour, 0, 0);
			}
			
		}

		private void bCreate_Click(object sender, RoutedEventArgs e)
		{
			if (false == ValidationCheck())
			{
				e.Handled = true;
				return;
			}

			if (IsEditMode)
			{
				e.Handled = true;
				this.DialogResult = true;
				this.Close();
			}
		}

		private void cbAdConstraintEvent_Checked(object sender, RoutedEventArgs e)
		{
			gboxMetaTag.IsEnabled = true;
		}

		private void cbAdConstraintEvent_Unchecked(object sender, RoutedEventArgs e)
		{
			gboxMetaTag.IsEnabled = false;

			//gboxMetaTag.Visibility = System.Windows.Visibility.Collapsed;
		}

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!this.IsEditMode)
            {
                e.Cancel = true;
                this.Hide();
            }
        }
    }
}
