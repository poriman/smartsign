﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPFDesigner
{
    /// <summary>
    /// NumberPicker.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class NumberPicker : UserControl
    {
        private int MaxValue;
        private int MinValue;
        private bool IsEnable;
        private string CtrlName;

        private static DependencyProperty TextPropertyKey = DependencyProperty.Register("ValueText", typeof(string), typeof(NumberPicker));

        public string Name
        {
            set { CtrlName = value; }
            get { return CtrlName; }
        }

        public bool ISENABLE
        {
            set 
            { 
                IsEnable = value;
                PART_Value.IsEnabled = IsEnable;
                PART_IncreaseTime.IsEnabled = IsEnable;
                PART_DecrementTime.IsEnabled = IsEnable;
            }
            get { return IsEnable; }
        }

        public int MAXVALUE
        {
            get { return MaxValue; }
            set { MaxValue = value; }
        }

        public int MINVALUE
        {
            set { MinValue = value; }
            get { return MinValue; }
        }

        public string ValueText
        {
            get { return (string)GetValue(TextPropertyKey); }
            set { SetValue(TextPropertyKey, value); }
            /*
            set { PART_Value.Text = value; }
            get { return PART_Value.Text; }
             * */
        }

        public NumberPicker()
        {
            InitializeComponent();
        }

        public static bool IsNumeric(string value)
        {
            try
            {
                foreach (char _char in value)
                {
                    if (!Char.IsNumber(_char))
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                string errmsg = ex.Message;
            }

            return true;
        }

        private int ValidateAndSetValue(string text)
        {
            return ValidateNumber(text, MINVALUE, MAXVALUE);
        }

        private void PART_Value_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (IsNumeric(PART_Value.Text))
            {
                PART_Value.Text = ValidateAndSetValue(PART_Value.Text).ToString();
            }
            else
                PART_Value.Text = "0";
            OnChangeValue(this, null);
        }

        private void PART_IncreaseTime_Click(object sender, RoutedEventArgs e)
        {
            PART_Value.Text = IncrementDecrementNumber(PART_Value.Text, MINVALUE, MAXVALUE, true).ToString();
            OnChangeValue(this, e);
        }

        private void PART_DecrementTime_Click(object sender, RoutedEventArgs e)
        {
            PART_Value.Text = IncrementDecrementNumber(PART_Value.Text, MINVALUE, MAXVALUE, false).ToString();
            OnChangeValue(this, e);
        }

        public int IncrementDecrementNumber(string num, int minValue, int maxVal, bool increment)
        {
            int newNum = ValidateNumber(num, minValue, maxVal);
            if (increment)
                newNum = Math.Min(newNum + 1, maxVal);
            else
                newNum = Math.Max(newNum - 1, 0);
            return newNum;
        }

        public int ValidateNumber(string newNum, int minValue, int maxValue)
        {
            int num;
            if (!int.TryParse(newNum, out num))
                return 0;

            return ValidateNumber(num, minValue, maxValue);
        }

        public int ValidateNumber(int newNum, int minValue, int maxValue)
        {
            newNum = Math.Max(newNum, minValue);
            newNum = Math.Min(newNum, maxValue);

            return newNum;
        }

        public event EventHandler event_ChangeValue;
        private void OnChangeValue(object sender, RoutedEventArgs e)
        {
            if (this.event_ChangeValue != null)
                this.event_ChangeValue(sender, e);
        }
    }
}
