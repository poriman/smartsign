﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Effects;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace WPFDesigner
{
    /// <summary>
    /// Interaction logic for ExplorerThumb.xaml
    /// </summary>
    public partial class ExplorerThumb : UserControl
    {
        public string projectPath;

        public ExplorerThumb()
        {
            InitializeComponent();
        }

        public ExplorerThumb(string path)
        {
            InitializeComponent();
            FileStream fs = new FileStream(path, FileMode.Open, FileAccess.Read);
            BinaryFormatter bf = new BinaryFormatter();
            object[] projectInfo = new object[2];
            projectInfo = (object[])bf.Deserialize(fs);
            fs.Close();

            MemoryStream ms = new MemoryStream((byte[])projectInfo[1]);

            BmpBitmapDecoder dec = new BmpBitmapDecoder(ms, BitmapCreateOptions.PreservePixelFormat, BitmapCacheOption.OnDemand);

            ImageBrush brush = new ImageBrush();
            brush.ImageSource = dec.Frames[0];
            brush.Stretch = Stretch.Uniform;
            brush.AlignmentX = AlignmentX.Center;
            brush.AlignmentY = AlignmentY.Center;
            thumb.Fill = brush;

            string projectFolder = System.IO.Path.GetDirectoryName(path) + "\\";
            string projectName = System.IO.Path.GetFileName(Directory.GetFiles(projectFolder, "*.dsd")[0]);
            ToolTip = projectFolder + projectName;
            header.Text = projectName;
            projectPath = (string)ToolTip;
        }

        public ExplorerThumb(ImageBrush brush, string path)
        {
            InitializeComponent();
            thumb.Fill = brush;
            string projectFolder = System.IO.Path.GetDirectoryName(path) + "\\";
            string projectName = System.IO.Path.GetFileName(Directory.GetFiles(projectFolder, "*.dsd")[0]);
            ToolTip = projectFolder + projectName;
            header.Text = projectName;
            projectPath = (string)ToolTip;
        }

        public ExplorerThumb(byte[] brushbytes, string path)
        {
            InitializeComponent();

            MemoryStream ms = new MemoryStream(brushbytes);

            JpegBitmapDecoder dec = new JpegBitmapDecoder(ms, BitmapCreateOptions.None, BitmapCacheOption.OnDemand);

            //BmpBitmapDecoder dec = new BmpBitmapDecoder(ms, BitmapCreateOptions.PreservePixelFormat, BitmapCacheOption.OnDemand);

            ImageBrush brush = new ImageBrush();
            brush.ImageSource = dec.Frames[0];
            brush.Stretch = Stretch.Uniform;
            brush.AlignmentX = AlignmentX.Center;
            brush.AlignmentY = AlignmentY.Center;
            thumb.Fill = brush;

            string projectFolder = System.IO.Path.GetDirectoryName(path) + "\\";
            string[] pr1 = Directory.GetFiles(projectFolder, "*.dsd");
            string[] pt1 = Directory.GetFiles(projectFolder, "*.dst");
            
            string projectName = "";

            if (pr1.Count() > 0)
            {
                projectName =  System.IO.Path.GetFileName(pr1[0]);
                type.Content = Properties.Resources.labelProject;
            }
            else if(pt1.Count() > 0)
            {
                projectName = System.IO.Path.GetFileName(pt1[0]);
                type.Content = Properties.Resources.labelTemplate;
            }
            ToolTip = projectFolder + projectName;
            header.Text = projectName;
            projectPath = (string)ToolTip;
        }

        public void Select()
        {
            BitmapEffect = new OuterGlowBitmapEffect();
            OuterGlowBitmapEffect effect = BitmapEffect as OuterGlowBitmapEffect;
            effect.GlowColor = Color.FromArgb(255, 255, 255, 255);
            effect.GlowSize = 6;
        }

        public void Deselect()
        {
            BitmapEffect = null;
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {

        }
    }
}
