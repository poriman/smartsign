﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPFDesigner
{
    /// <summary>
    /// Interaction logic for PlayerControl.xaml
    /// </summary>
    public partial class PlayerControl : UserControl
    {
        bool isPlaying;
        System.Windows.Threading.DispatcherTimer durationTimer;
        DateTime dtStart = DateTime.Now;

        TimeSpan span;
        int seconds;
        Screen _screen;

        public PlayerControl()
        {
            InitializeComponent();
            isPlaying = false;
            stopButton.Opacity = 0.5;
        }
       
        public void Play(Screen screen)
        {
            if (screen == null)
            {
                return;
            }

            if (!isPlaying)
            {
                seconds = 0;
                durationTimer = new System.Windows.Threading.DispatcherTimer(System.Windows.Threading.DispatcherPriority.Background);
                durationTimer.Interval = new TimeSpan(0, 0, 1);
                durationTimer.Tick += new EventHandler(durationTimer_Tick);
                span = new TimeSpan();
                durationTimer.Start();
                isPlaying = true;
                playButton.Opacity = 0.5;
                stopButton.Opacity = 1.0;
                _screen = screen;
                dtStart = DateTime.Now;
                ((IDesignElement)_screen).Play();
            }
        }

        public void Stop()
        {
            if (isPlaying)
            {
                ((IDesignElement)_screen).Stop();
                durationTimer.Stop();
                isPlaying = false;
                stopButton.Opacity = 0.5;
                playButton.Opacity = 1.0;
                durationLabel.Content = new TimeSpan();
            }
        }

        void durationTimer_Tick(object sender, EventArgs e)
        {
            span = (TimeSpan)(DateTime.Now - dtStart);
            durationLabel.Content = span;
        }
    }
}
