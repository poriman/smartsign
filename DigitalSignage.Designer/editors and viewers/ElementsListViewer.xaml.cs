﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPFDesigner
{
    /// <summary>
    /// Represents the listed view of elements that exist on the design surface
    /// </summary>
    public partial class ElementsListViewer : UserControl
    {
		private static int _minZindex = 0;
		private static int _maxZindex = 0;

		public static int MinZIndex
		{
			get { return _minZindex; }
		}

		public static int MaxZIndex
		{
			get { return _maxZindex; }
		}


        /// <summary>
        /// Represents the list of selected items
        /// </summary>
        private List<UIElement> _selection = new List<UIElement>();

        /// <summary>
        /// Indicates whether the Ctrl key is pressed
        /// </summary>
        private bool _IsCtrlPressed;

        /// <summary>
        /// Represents the Screen
        /// </summary>
        public Screen _screen;

        /// <summary>
        /// Initializes the new ElementsListViewer class instance
        /// </summary>
        public ElementsListViewer()
        {
            InitializeComponent();
			this.PreviewKeyDown += new KeyEventHandler(UserControl_PreviewKeyDown);
			this.PreviewKeyUp += new KeyEventHandler(UserControl_PreviewKeyUp);
        }
		
        /// <summary>
        /// Adds the new item that represents the provided element
        /// </summary>
        /// <param name="el">Element to create the ElementView item from</param>
        public void Add(UIElement el)
        {
            ElementView item = new ElementView(el);
			
            item.ContextMenu = _screen.InitElementContextMenu();
            item.border.MouseLeftButtonDown += new MouseButtonEventHandler(border_MouseLeftButtonDown);
            item.border.PreviewMouseDown += new MouseButtonEventHandler(ElementView_PreviewMouseLeftButtonDown);
            item.border.MouseRightButtonDown += new MouseButtonEventHandler(border_MouseRightButtonDown);
            itemsPanel.Children.Add(item);

			int currzIndex = ((IDesignElement)el).ZIndex;

			if (currzIndex > _maxZindex) _maxZindex = currzIndex;
			if (currzIndex < _minZindex) _minZindex = currzIndex;

			ArrangeItems();
        }

		public bool IncreaseZIndex(UIElement el)
		{
			if(itemsPanel.Children.Count <= 1)
				return false;

			for (int i = 1; i < itemsPanel.Children.Count; ++i)
			{
				UIElement curr = ((ElementView)itemsPanel.Children[i])._element;
				if (curr.Equals(el))
				{
					UIElement prev = ((ElementView)itemsPanel.Children[i-1])._element;
					int zIndex = ((IDesignElement)prev).ZIndex;
					((IDesignElement)prev).ZIndex = ((IDesignElement)curr).ZIndex;
					((IDesignElement)curr).ZIndex = zIndex;

					ArrangeItems();
					return true;
				}
			}

			return false;
		}
		public bool DecreaseZIndex(UIElement el)
		{
			if(itemsPanel.Children.Count <= 1)
				return false;

			for (int i = 0; i < itemsPanel.Children.Count -1; ++i)
			{
				UIElement curr = ((ElementView)itemsPanel.Children[i])._element;
				if (curr.Equals(el))
				{
					UIElement next = ((ElementView)itemsPanel.Children[i+1])._element;
					int zIndex = ((IDesignElement)next).ZIndex;
					((IDesignElement)next).ZIndex = ((IDesignElement)curr).ZIndex;
					((IDesignElement)curr).ZIndex = zIndex;

					ArrangeItems();
					return true;
				}
			}

			return false;
		}

		public void ArrangeItems()
		{
			UIElement[] outElements = new UIElement[itemsPanel.Children.Count];
			itemsPanel.Children.CopyTo(outElements, 0);
			itemsPanel.Children.Clear();

			foreach (UIElement el in outElements.OrderByDescending(el => ((IDesignElement)(((ElementView)el)._element)).ZIndex))
			{
				itemsPanel.Children.Add(el);
			}
		}

		public void SortItems()
		{
			UIElement[] outElements = new UIElement[itemsPanel.Children.Count];
			itemsPanel.Children.CopyTo(outElements, 0);
			itemsPanel.Children.Clear();

			int nCurrZ = 10 + outElements.Length;

			foreach(UIElement el in outElements.OrderByDescending(el => ((IDesignElement)(((ElementView)el)._element)).ZIndex))
			{
				if (nCurrZ > _maxZindex) _maxZindex = nCurrZ;
				if (nCurrZ < _minZindex) _minZindex = nCurrZ;

				((IDesignElement)((ElementView)el)._element).ZIndex = nCurrZ--;
				itemsPanel.Children.Add(el);

			}
		}


		/// <summary>
		/// Removes the item what`s name is equal to the name of given element
		/// </summary>
		/// <param name="el">The element to remove the ElementView representation of</param>
		public void Remove(UIElement el)
		{
			ElementView elementViewToRemove = itemsPanel.Children.OfType<ElementView>().Single(item => item.elementName.Content.ToString() == ((IDesignElement)el).Name);
			if (elementViewToRemove != null)
			{
				itemsPanel.Children.Remove(elementViewToRemove);
				BindingOperations.ClearAllBindings(elementViewToRemove);
				elementViewToRemove.border.PreviewMouseDown -= new MouseButtonEventHandler(ElementView_PreviewMouseLeftButtonDown);
			}
		}

        /// <summary>
        /// Selects the items corresponding to given elements collection
        /// </summary>
        /// <param name="elementsToSelect">Collectiont of elements</param>
        public void Select(UIElement[] elementsToSelect)
        {
            foreach (ElementView item in itemsPanel.Children)
            {
                if (elementsToSelect.Count(el => el == item._element) == 1)
                {
                    item.Select();
                }
                else
                {
                    item.Deselect();
                }
            }
        }

        public void PlayStatus(UIElement[] elementsToSelect, bool isPlay)
        {
            if (elementsToSelect == null)
            {
                foreach (ElementView item in itemsPanel.Children)
                {
                    item.PlayStatus(isPlay);
                }
            }
            else
            {
                foreach (ElementView item in itemsPanel.Children)
                {
                    if (elementsToSelect.Count(el => el == item._element) == 1)
                    {
                        item.PlayStatus(isPlay);
                    }
                }
            }
        }

        /// <summary>
        /// Clears the items list
        /// (Should be called when closing the project)
        /// </summary>
        public void Clear()
        {
            foreach (ElementView item in itemsPanel.Children)
            {
                BindingOperations.ClearAllBindings(item);
                item._element = null;
            }
            itemsPanel.Children.Clear();
        }

        #region Event Handler

        #region Determining whether the Ctrl key is pressed or not
        void UserControl_PreviewKeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.LeftCtrl)
            {
                _IsCtrlPressed = false;
            }
        }

		void UserControl_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.LeftCtrl)
            {
                _IsCtrlPressed = true;
            }
			else if (e.Key == Key.Delete)
			{
                if (_selection == null || _selection.Count == 0) return;

                if (!_screen.IsPlay)
                {
                    try
                    {

                        foreach (IDesignElement de in _selection)
                        {

                            if (de.ISPlay) return;
                        }
                    }
                    catch
                    {
                    }

                    if (MessageBox.Show(Properties.Resources.mb_RemoveComponent, Properties.Resources.titleDesignerWindow, MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                    {
                        _screen.Remove();
                    }
                }
			}
        }
        #endregion

		void border_MouseRightButtonDown(object sender, MouseButtonEventArgs e)
		{
			if (_screen == null) return;

// 			List<UIElement> selection = new List<UIElement>();
            _selection.Clear();

			FrameworkElement parent = ((FrameworkElement)e.Source).Parent as FrameworkElement;

			while (parent.GetType() != typeof(ElementView))
			{
				parent = parent.Parent as FrameworkElement;
			}

			ElementView item = parent as ElementView;

			if (item.IsSelected)
			{
                _selection.Add(item._element);
				return;
			}
			else
			{
				item.Select();
                _selection.Add(item._element);
			}

			foreach (ElementView item_ in itemsPanel.Children)
			{
				if (item_.IsSelected && item_ != item)
				{
					item.Deselect();
                    _selection.Remove(item_._element);
				}
			}

            _screen.Select(_selection.ToArray());
			_screen.Focusable = true;
		}

		void border_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{
			if (_screen == null) return;

// 			List<UIElement> selection = new List<UIElement>();
            _selection.Clear();

			FrameworkElement parent = ((FrameworkElement)e.Source).Parent as FrameworkElement;

			while (parent.GetType() != typeof(ElementView))
			{
				parent = parent.Parent as FrameworkElement;
			}

			ElementView item = parent as ElementView;

			if (_IsCtrlPressed)
			{
				if (item.IsSelected)
				{
					item.Deselect();
					_screen.RemoveFromSelection(item._element);
                    _selection.Remove(item._element);
				}
				else
				{
					item.Select();
                    _selection.Add(item._element);
				}

				foreach (ElementView item_ in itemsPanel.Children)
				{
					if (item_.IsSelected)
					{
// 						selection.Add(item_._element);
                        _selection.Add(item_._element);
					}
				}
			}
			else
			{
				if (item.IsSelected)
				{
					item.Deselect();
// 					selection.Remove(item._element);
                    _selection.Remove(item._element);
				}
				else
				{
					item.Select();
// 					selection.Add(item._element);
                    _selection.Add(item._element);
				}

				foreach (ElementView item_ in itemsPanel.Children)
				{
					if (item_.IsSelected && item_ != item)
					{
						item.Deselect();
                        _selection.Remove(item_._element);
					}
				}
			}
            _screen.Select(_selection.ToArray());
			_screen.Focusable = true;
		}

		/// <summary>
		/// Occures when item is clicked
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		void ElementView_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{

		}

        #endregion
    }
}