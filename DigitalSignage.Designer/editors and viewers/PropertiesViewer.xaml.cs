﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.IO;
using System.Xml.Linq;
using System.Xml;
using WPFDesigner.Cultures;

namespace WPFDesigner
{
    /// <summary>
    /// Represents the user control that is able to display a set of properties and controls to change them
    /// </summary>
    public partial class PropertiesViewer : UserControl
    {
        Dictionary<string, object> _properties;
        
        CheckBox cboxMute = null;

        /// <summary>
        /// initializes the new PropertiesViewer class instance
        /// </summary>
        public PropertiesViewer()
        {
            InitializeComponent();
        }

        private List<string> GetMasterProperty(UIElement elementWithProperties)
        {
            try
            {
                List<string> PropertiesList = new List<string>();
                PropertiesList.Add("Name");
                PropertiesList.Add("Width");
                PropertiesList.Add("Height");
                PropertiesList.Add("Left");
                PropertiesList.Add("Top");
                PropertiesList.Add("HorizontalAlignment");
                PropertiesList.Add("VerticalAlignment");
                /*
                string controlName = ((FrameworkElement)elementWithProperties).ToString();
                if (controlName.Equals("WPFDesigner.RectangleComponent") ||
                    controlName.Equals("WPFDesigner.EllipseComponent") ||
                    controlName.Equals("WPFDesigner.ScrollTextComponent"))
                {
                    PropertiesList.Add("Background");
                    PropertiesList.Add("BorderBrush");
                }
                else if (controlName.Equals("WPFDesigner.DigitalClockComponent") ||
                         controlName.Equals("WPFDesigner.DateComponent"))
                {
                    PropertiesList.Add("Background");
                    PropertiesList.Add("BorderBrush");
                    PropertiesList.Add("Foreground");
                    PropertiesList.Add("FontFamily");
                    PropertiesList.Add("FontWeight");
                    PropertiesList.Add("FontSize");
                }
                else if (controlName.Equals("WPFDesigner.TextComponent"))
                {
                    PropertiesList.Add("VerticalcontentAlignment");
                }
                else if (controlName.Equals("WPFDesigner.ImageComponent"))
                {
                    PropertiesList.Add("Stretch");
                }
                else if (controlName.Equals("WPFDesigner.MediaComponent"))
                {
                    PropertiesList.Add("Volume");
                    PropertiesList.Add("Mute");
                }
                else if (controlName.Contains("WPFDesigner.AudioComponent"))
                {
                    PropertiesList.Clear();
                    PropertiesList.Add("Volume");
                    PropertiesList.Add("Mute");
                }
                else if (controlName.Equals("WPFDesigner.AnalogueClockComponent"))
                {
                    PropertiesList.Add("Background");
                    PropertiesList.Add("BorderBrush");
                    PropertiesList.Add("Foreground");
                }
                //*/
                return PropertiesList;
            }
            catch(Exception ex)
            {
                string errmsg = ex.Message.ToString();
            }

            return null;
        }
        /// <summary>
        /// Loads all available controls defined in PropertiesControls.xml
        /// </summary>  
        private void LoadPropertyControl(UIElement elementWithProperties)
        {
            string path = AppDomain.CurrentDomain.BaseDirectory + "settings\\PropertiesControls.xml";
            if (!File.Exists(path))
            {
                MessageBox.Show("Failed while loading properties controls definition. Cannot find the " + path, "Designer", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            ReMoveAllProperty();

            List<string> PropertiesList = GetMasterProperty(elementWithProperties);
            if (PropertiesList == null)
            {
                return;
            }
            string pcDef = File.ReadAllText(path);

            XElement fullControlsDefinition = XElement.Parse(pcDef);

            foreach (XElement control in fullControlsDefinition.Descendants())
            {
                bool ismaster = false;
                Label l = new Label();
                l.Name = control.Attribute("PropertyName").Value;
                ToolTip tooltip = new ToolTip();
                tooltip.Content= l.Name;
                l.ToolTip = tooltip;
                foreach (string _name in PropertiesList)
                {
                    if (l.Name.Equals(_name))
                    {
                        ismaster = true;
                        break;
                    }
                }
                Binding b = new Binding();
                b.Source = CultureResources.ResourceProvider;
                b.Path = new PropertyPath(control.Attribute("PropertyName").Value + "Label");
                b.Mode = BindingMode.OneWay;
                l.SetBinding(Label.ContentProperty, b);
                l.Style = (Style)this.Resources["Label"];
                l.Visibility = Visibility.Collapsed;

                switch (control.Name.LocalName)
                {
                    case "TextBox":
                        {
                            TextBoxControl tb = new TextBoxControl(control);
                            tb.Background = new System.Windows.Media.SolidColorBrush(Color.FromArgb(255, 255, 255, 255));
                            tb.Style = (Style)this.Resources["TextBox"];
							tb.GotKeyboardFocus += new KeyboardFocusChangedEventHandler(tb_GotKeyboardFocus);

							if (ismaster == true)
                                RightPanel.Children.Add(tb);
                            else
                                DRightPanel.Children.Add(tb);
                            break;
                        }
                    case "Slider":
                        {
                            SliderControl s = new SliderControl(control);
                            s.Style = (Style)this.Resources["Slider"];
                            s.ModifiedVolumeByUser += new EventHandler(s_ModifiedVolumeByUser);
                            if (ismaster == true)
                                RightPanel.Children.Add(s);
                            else
                                DRightPanel.Children.Add(s);
                            break;
                        }
                    case "CheckBox":
                        {
                            CheckBoxControl cb = new CheckBoxControl(control);
                            try
                            {
                                if(control.Attribute("PropertyName").Value.Equals("Mute"))
                                    cboxMute = cb;
                            }
                            catch
                            {
                            	
                            }
                            cb.Style = (Style)this.Resources["CheckBox"];
                            if (ismaster == true)
                                RightPanel.Children.Add(cb);
                            else
                                DRightPanel.Children.Add(cb);
                            break;
                        }
                    case "Label":
                        {
                            LabelControl lb = new LabelControl(control);
                            lb.Style = (Style)this.Resources["Label"];
                            if (ismaster == true)
                                RightPanel.Children.Add(lb);
                            else
                                DRightPanel.Children.Add(lb);
                            break;
                        }
                    case "ComboBox":
                        {
                            ComboBoxControl cb = new ComboBoxControl(control);
                            cb.Style = (Style)this.Resources["ComboBox"];
                            if (ismaster == true)
                                RightPanel.Children.Add(cb);
                            else
                                DRightPanel.Children.Add(cb);
                            break;
                        }
                    default: break;
                }

                if (ismaster == true)
                    LeftPanel.Children.Add(l);
                else
                    DLeftPanel.Children.Add(l);                
            }
        }

        void s_ModifiedVolumeByUser(object sender, EventArgs e)
        {
           try
           {
               if (cboxMute.IsChecked.Value) cboxMute.IsChecked = false;
           }
           catch 
           {
           }
        }

		void tb_GotKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
		{
			try
			{
				TextBox tb = sender as TextBox;
				tb.SelectAll();
				e.Handled = true;
			}
			catch { }
		}

        /// <summary>
        /// Loads and displays the element`s properties set
        /// </summary>
		/// <param name="elementWithProperties">The properties dictionary</param>
        public void Load(UIElement elementWithProperties)
        {
            _properties = ((IDesignElement)elementWithProperties).Properties;
//             if (!((FrameworkElement)elementWithProperties).ToString().Equals("WPFDesigner.Screen"))
//             {
                LoadPropertyControl(elementWithProperties);
//             }
  //          ElementName.Content = elementWithProperties.GetType().Name;//((FrameworkElement)elementWithProperties).Name;

            foreach (UIElement element in RightPanel.Children)
            {
                FrameworkElement el = element as FrameworkElement;

                if (_properties.Keys.Contains(el.Name))
                {
                    ((UIElement)el).Visibility = Visibility.Visible;
                    LeftPanel.Children.OfType<Label>().Single(l => l.Name == el.Name).Visibility = Visibility.Visible;
                    ((IPropertyControl)el).uiElement = elementWithProperties;
                }
                else
                {
                    ((UIElement)el).Visibility = Visibility.Collapsed;
                    LeftPanel.Children.OfType<Label>().Single(l=>l.Name == el.Name).Visibility = Visibility.Collapsed;
                    ((IPropertyControl)el).uiElement = null;
                }
            }

            foreach (UIElement element in DRightPanel.Children)
            {
                FrameworkElement el = element as FrameworkElement;

                if (_properties.Keys.Contains(el.Name))
                {
                    ((UIElement)el).Visibility = Visibility.Visible;
                    DLeftPanel.Children.OfType<Label>().Single(l => l.Name == el.Name).Visibility = Visibility.Visible;
                    ((IPropertyControl)el).uiElement = elementWithProperties;
                }
                else
                {
                    ((UIElement)el).Visibility = Visibility.Collapsed;
                    DLeftPanel.Children.OfType<Label>().Single(l => l.Name == el.Name).Visibility = Visibility.Collapsed;
                    ((IPropertyControl)el).uiElement = null;
                }
            }
        }

        public void Close()
        {
            ReMoveAllProperty();
        }

        private void ReMoveAllProperty()
        {
            foreach (UIElement el in RightPanel.Children)
            {
                BindingOperations.ClearAllBindings(el);
            }
            foreach (UIElement el in LeftPanel.Children)
            {
                BindingOperations.ClearAllBindings(el);
            }
            foreach (UIElement el in DRightPanel.Children)
            {
                BindingOperations.ClearAllBindings(el);
            }
            foreach (UIElement el in DLeftPanel.Children)
            {
                BindingOperations.ClearAllBindings(el);
            }
            RightPanel.Children.Clear();
            LeftPanel.Children.Clear();
            DRightPanel.Children.Clear();
            DLeftPanel.Children.Clear();
        }
    }
}
