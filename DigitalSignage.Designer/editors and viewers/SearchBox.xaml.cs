﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;

namespace WPFDesigner.components
{
	/// <summary>
	/// Interaction logic for SearchBox.xaml
	/// </summary>
	public partial class SearchBox : UserControl
	{
		Collection<string> arrKeys = new Collection<string>();
		Collection<string> arrValues = new Collection<string>();

		/// <summary>
		/// 생성자
		/// </summary>
		public SearchBox()
		{
			InitializeComponent();
		}

		/// <summary>
		/// 버튼의 텍스트를 갱신한다.
		/// </summary>
		public String ButtonText
		{
			get { return this.btnSearch.Content.ToString(); }
			set { this.btnSearch.Content = value; }
		}

        /// <summary>
        /// 체크 버튼 텍스트를 갱신한다.
        /// </summary>
        public String AllCheckText
        {
            get { return this.cbAll.Content.ToString(); }
            set { this.cbAll.Content = value; }
        }

        string _meta_group = String.Empty;

        /// <summary>
        /// 태그 그룹 이름을 지정한다
        /// </summary>
        public String GroupName
        {
            get { return _meta_group; }
            set 
            { 
                _meta_group = value;
                cbAll.Content = String.Format("{0} 모두", value);
            }


        }

        /// <summary>
        /// 검색된 값을 설정하거나 가져옵니다.
        /// </summary>
        public Collection<String> Tags
        {
            get
            {
                Collection<String> arrReturns = new Collection<string>();

                if (cbAll.IsChecked == true)
                {
                    arrReturns.Add(String.Format("{0} 모두", GroupName));
                }
                else
                {
                    foreach (TagBlock tb in wrapText.Children)
                    {
                        arrReturns.Add(tb.HeaderName);
                    }
                }

                return arrReturns;
            }
            set
            {
                String sAll = String.Format("{0} 모두", GroupName);

                foreach (string strTag in value)
                {
                    if (strTag.Equals(sAll))
                    {
                        cbAll.IsChecked = true;
                        continue;
                    }

                    TagBlock tb = new TagBlock();
                    tb.HeaderName = strTag;
                    tb.OnDeleted += new EventHandler(tb_OnDeleted);
                    //tb.OnTextChanged += new EventHandler(tb_OnTextChanged);
                    wrapText.Children.Add(tb);
                }
            }
        }


		/// <summary>
		/// 검색된 값을 설정하거나 가져옵니다.
		/// </summary>
		public String SearchResult
		{
			get 
			{
				String sReturn = String.Empty;

                if (cbAll.IsChecked == true)
                {
                    return String.Empty;
                }
                else
                {
                    foreach (TagBlock tb in wrapText.Children)
                    {
                        if (String.IsNullOrEmpty(sReturn))
                            sReturn = tb.HeaderName;
                        else
                            sReturn += String.Format(", {0}", tb.HeaderName);
                    }
                }

				return sReturn;
			}
			set 
			{
				wrapText.Children.Clear();

				if (value != null)
				{
					String[] arrList = value.Split(',');
                    String sAll = String.Format("{0} 모두", GroupName);

					foreach (string temp in arrList)
					{
						if (String.IsNullOrEmpty(temp)) continue;

                        if (temp.Equals(sAll))
                        {
                            cbAll.IsChecked = true;
                            continue;
                        }

						TagBlock tb = new TagBlock();
						tb.HeaderName = temp.Trim();
						tb.OnDeleted += new EventHandler(tb_OnDeleted);
						//tb.OnTextChanged += new EventHandler(tb_OnTextChanged);
						wrapText.Children.Add(tb);
					}
				}
			}
		}

		/// <summary>
		/// 키 리스트를 반환한다.
		/// </summary>
		public Collection<string> KeyList { get { return arrKeys; } }
		
		/// <summary>
		/// 값 리스트를 반환한다.
		/// </summary>
		public Collection<string> ValueList { get { return arrValues; } }

		
		/// <summary>
		/// 아이템 추가
		/// </summary>
		/// <param name="key"></param>
		/// <param name="value"></param>
		public void AddItem(string key, string value)
		{
			if (!arrKeys.Contains(key))
			{
				arrKeys.Add(key);
				arrValues.Add(value);
			}
		}

		/// <summary>
		/// 동일한 아이템을 찾는다
		/// </summary>
		/// <param name="keyword"></param>
		/// <returns>키|값 의 배열</returns>
		public Collection<string> FindItem(string keyword)
		{
			Collection<string> arr = new Collection<string>();

			for (int i = 0; i < arrKeys.Count; ++i)
			{

				if (arrKeys[i].Equals(keyword))
				{
					arr.Add(String.Format("{0}|{1}", arrKeys[i], arrValues[i]));
				}

			}

			return arr;
		}

		/// <summary>
		/// 동일한 아이템을 LIKE 검색으로 찾는다
		/// </summary>
		/// <param name="keyword"></param>
		/// <returns>키|값 의 배열</returns>
		public Collection<string> FindLikeItem(string keyword)
		{
			Collection<string> arr = new Collection<string>();

			for (int i = 0; i < arrKeys.Count; ++i)
			{

				if (arrKeys[i].Contains(keyword))
				{
					arr.Add(String.Format("{0}|{1}", arrKeys[i], arrValues[i]));
				}
			}

			return arr;
		}

		public void RemoveKey(string key)
		{
			int idx = arrKeys.IndexOf(key);
			if (idx >= 0)
			{
				arrKeys.RemoveAt(idx);
				arrValues.RemoveAt(idx);
			}

		}

		private void btnSearch_Click(object sender, RoutedEventArgs e)
		{
			SearchWindow window = new SearchWindow(this);

            foreach (TagBlock b in wrapText.Children)
            {
                window.AddTagBlock(b.HeaderName, String.Empty);
            }

			window.ShowDialog();

			String sReturn = window.SelectedKey;

			if (!String.IsNullOrEmpty(sReturn))
			{
                wrapText.Children.Clear();

				String[] arrList = sReturn.Split(',');

				foreach (string temp in arrList)
				{
					TagBlock tb = new TagBlock();
					tb.HeaderName = temp.Trim();
					tb.OnDeleted += new EventHandler(tb_OnDeleted);
					//tb.OnTextChanged += new EventHandler(tb_OnTextChanged);
					wrapText.Children.Add(tb);

				}
			}

		}

		void tb_OnTextChanged(object sender, EventArgs e)
		{
			throw new NotImplementedException();
		}

		void tb_OnDeleted(object sender, EventArgs e)
		{
			wrapText.Children.Remove(sender as UIElement);
		}

        private void cbAll_Checked(object sender, RoutedEventArgs e)
        {
            try
            {
                wrapText.IsEnabled = false;
                btnSearch.IsEnabled = false;
            }
            catch { }
        }

        private void cbAll_Unchecked(object sender, RoutedEventArgs e)
        {
            try
            {
                wrapText.IsEnabled = true;
                btnSearch.IsEnabled = true;
            }
            catch { }
        }
	}
}
