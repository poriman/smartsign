﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using System.IO;
using QuartzTypeLib;
using WMPLib;
using WPFDesigner.tools;
using System.Windows.Markup;
using DigitalSignage.Scheduler;

namespace WPFDesigner
{   
    /// <summary>
    /// Interaction logic for PlaylistEditor.xaml
    /// </summary>
    public partial class PlaylistEditor : Window
    {
#region Editors
        System.Windows.Forms.OpenFileDialog _openDialog;
//         ListViewDragDropManager<PlaylistItem> dragMgr;
        bool isSelected;
        public bool isCancel;
        TimeSpan duration;

        TextEditor _textEditor;

        UrlSetter _urlEditor;

        GmtSetter _gmtSetter;

		WeatherSetter _weatherSetter;
        ScrollTextSetter _stSetter;
		RssTextSetter _rtSetter;
        private IMediaFilesManager FileManager;
#endregion

        private List<PlaylistItem> _orgplaylist;
        private List<PlaylistItem> _playlist;
        private Type _elementType;

        /// <summary>
        /// Initializes the new PlaylistEditor class instance
        /// </summary>
        public PlaylistEditor()
        {
            InitializeComponent();
            isCancel = false;
//             this.dragMgr = new ListViewDragDropManager<PlaylistItem>(ContentsListBox);
//             this.dragMgr.ListView = ContentsListBox;
//             this.dragMgr.ShowDragAdorner = true;

            _openDialog = new System.Windows.Forms.OpenFileDialog();
            FileManager = ProjectManager.GetCurrentProjectManager().Pane as IMediaFilesManager;
        }      

        /// <summary>
        /// Shows the playlist editor
        /// </summary>
        /// <param name="element">The element to load the playlist of</param>
        public void ShowEditor(IDesignElement element)
        {
            isCancel = false;
            _orgplaylist        = element.Playlist;
            _playlist = new List<PlaylistItem>();
            foreach (PlaylistItem item in _orgplaylist)
            {
                PlaylistItem temp   = new PlaylistItem();
                temp.Content        = item.Content;
                temp.CONTENTSVALUE  = item.CONTENTSVALUE;
                temp.Duration       = item.Duration;
                temp.TIME           = item.TIME;

                _playlist.Add(temp);
            }            
            _elementType        = element.Type;
			elementType.Content = GetComponentStr(element.Type);
            elementName.Content = element.Name;
            ContentsListBox.ItemsSource = null;
            Fill();
            this.ShowDialog();
        }

        /// <summary>
        /// Fills the playlist items container with the loaded playlist data
        /// </summary>
        private void Fill()
        {
            try
            {
                PlaylistItemArry task = new PlaylistItemArry();                
                foreach (PlaylistItem item in _playlist)
                {
                    if(String.IsNullOrEmpty(item.CONTENTSVALUE as String))
                        item.CONTENTSVALUE = item.Content;
                    item.TIME = item.DurationAsTimeSpan;                    
                    task.Add(item);
                }

                ContentsListBox.ItemsSource = task;
            }
            catch
            {
            }
        }

        /// <summary>
        /// The event handler that is used for handling events of OK/Apply/Cancel buttons block
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OKApplyCancelhandler(object sender, RoutedEventArgs e)
        {
            Button b = sender as Button;
            switch (b.Name)
            {
                case "OK":
                    {
						if (ValidationCheck())
							Hide();
                        break;
                    }
                case "Apply":
                    {
						if (ValidationCheck())
						{
							_orgplaylist.Clear();
							List<PlaylistItem> tempArry = GetPlaylist();
							foreach (PlaylistItem item in tempArry)
							{
								PlaylistItem temp = new PlaylistItem();
								temp.Content = item.Content;
								temp.CONTENTSVALUE = item.CONTENTSVALUE;
								temp.Duration = item.Duration;
								temp.TIME = item.TIME;

								_orgplaylist.Add(temp);
							}
                        }       
                        break;
                    }
                case "Cancel":
                    {
                        isCancel = true;
                        Hide();
                        break;
                    }
                default: break;
            }
        }

		private bool ValidationCheck()
		{
			PlaylistItemArry task = ContentsListBox.ItemsSource as PlaylistItemArry;
			int nItemCount = task.Count;

			foreach (PlaylistItem item in task)
			{
				if (item.Content != Properties.Resources.labelNoMedia && item.Content != "")
				{
					if (nItemCount != 1 && item.TIME.TotalSeconds == 0)
					{
						return MessageBoxResult.Yes == MessageBox.Show(Properties.Resources.mbDurationIsUnlimit, Properties.Resources.titleDesignerWindow, MessageBoxButton.YesNo, MessageBoxImage.Warning);
					}
				}
			}

			return true;
		}

        public List<PlaylistItem> GetPlaylist()
        {
            if (false == isCancel)
            {
                _playlist.Clear();
                PlaylistItemArry task = ContentsListBox.ItemsSource as PlaylistItemArry;
                foreach (PlaylistItem item in task)
                {
                    if (item.Content != Properties.Resources.labelNoMedia && 
                        !String.IsNullOrEmpty(item.Content))
                    {
                        _playlist.Add(item);
                    }
                }
                return _playlist;
            }

            return _orgplaylist;
        }

        private void AddItem_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                AddItems();

//                 PlaylistItemArry task = ContentsListBox.ItemsSource as PlaylistItemArry;
//                 task.Add(new PlaylistItem());
//                 ContentsListBox.ItemsSource = task;
            }
            catch
            {
            }
        }

        private void AddItems()
        {
            try
            {
                PlaylistItemArry task = ContentsListBox.ItemsSource as PlaylistItemArry;

                switch (_elementType.Name)
                {
                    case "AnalogueClockComponent":
                    case "DigitalClockComponent":
                        {
                            PlaylistItem item = new PlaylistItem();

                            _gmtSetter = new GmtSetter();
                            _gmtSetter.Load(item.Content);
                            if (_gmtSetter.DialogResult == true)
                            {
                                item.Content = _gmtSetter.GetContent();
                                item.CONTENTSVALUE = GmtManager.GetTimeZoneFromIndexInString(item.Content).ToString();

                                task.Add(item);
                            }
                            
                            break;
                        }
                    case "DateComponent":
                        {
                            break;
                        }
                    case "EllipseComponent":
                        {
                            break;
                        }
                    case "ImageComponent":
                        {
                            _openDialog.Multiselect = true;
                            _openDialog.Filter = Properties.Resources.labelFilterImage;
                            _openDialog.Title = Properties.Resources.labelOpenImage;

                            if(System.Windows.Forms.DialogResult.OK == _openDialog.ShowDialog())
                            {
                                foreach (String filename in _openDialog.FileNames)
                                {
                                    if (filename != "" && File.Exists(filename))
                                    {
                                        PlaylistItem targetitem = new PlaylistItem();

                                        targetitem.Content = FileManager.AddNewFileWithModifiedName(filename);
                                        targetitem.CONTENTSVALUE = targetitem.Content;

                                        task.Add(targetitem);
                                    }
                                }
                            }

                            break;
                        }
                    case "AudioComponent":
                        {
                            string filter = Properties.Resources.labelFilterAudio;
                            OpenFileDialog(task, Properties.Resources.labelOpenAudio, filter);
                            break;
                        }
                    case "MediaComponent":
                        {
                            string filter = Properties.Resources.labelFilterVideo;
                            OpenFileDialog(task, Properties.Resources.labelOpenVideo, filter);
                            break;
                        }
                    case "RectangleComponent":
                        {
                            break;
                        }
                    case "RssComponent":
                        {

                            PlaylistItem targetitem = new PlaylistItem();

                            _rtSetter = new RssTextSetter();
                            _rtSetter.Load(ScrollTextInfoManager.DeserializeScrollTextInfo(targetitem.Content));
                            if (_rtSetter.DialogResult == true)
                            {
                                targetitem.CONTENTSVALUE = _rtSetter.GetContent();
                                if (targetitem.CONTENTSVALUE == new ScrollTextInfo())
                                {
                                    targetitem.CONTENTSVALUE = Properties.Resources.labelNoMedia;
                                    targetitem.Content = "";
                                }

                                targetitem.Content = ScrollTextInfoManager.SerializeScrollTextInfo((ScrollTextInfo)targetitem.CONTENTSVALUE);
                                targetitem.CONTENTSVALUE = targetitem.Content;

                                task.Add(targetitem);
                            }

                            break;
                        }
                    case "ScrollTextComponent":
                        {
                            PlaylistItem targetitem = new PlaylistItem();

                            _stSetter = new ScrollTextSetter();
                            _stSetter.Load(ScrollTextInfoManager.DeserializeScrollTextInfo(targetitem.Content));
                            targetitem.CONTENTSVALUE = _stSetter.GetContent();

                            if (_stSetter.DialogResult == true)
                            {

                                if (targetitem.CONTENTSVALUE == new ScrollTextInfo())
                                {
                                    targetitem.CONTENTSVALUE = Properties.Resources.labelNoMedia;
                                    targetitem.Content = "";
                                }

                                targetitem.Content = ScrollTextInfoManager.SerializeScrollTextInfo((ScrollTextInfo)targetitem.CONTENTSVALUE);
                                targetitem.CONTENTSVALUE = targetitem.Content;
                               
                                task.Add(targetitem);
                            }
                            break;
                        }
                    case "TextComponent":
                        {
                            PlaylistItem targetitem = new PlaylistItem();

                            _textEditor = new TextEditor();
                            _textEditor.Load(targetitem.Content);

                            if (_textEditor.DialogResult == true)
                            {
                                targetitem.CONTENTSVALUE = targetitem.Content = _textEditor.GetDocument();
//                                 targetitem.CONTENTSVALUE = "Text";

                                task.Add(targetitem);
                            }
                            break;
                        }
                    case "WebComponent":
                        {
                            PlaylistItem targetitem = new PlaylistItem();

                            _urlEditor = new UrlSetter();
                            _urlEditor.Load(targetitem.Content);

                            if (_urlEditor.DialogResult == true)
                            {
                                targetitem.Content = _urlEditor.GetContent().ToString();
                                targetitem.CONTENTSVALUE = targetitem.Content;
                   
                                task.Add(targetitem);
                            }
                            break;
                        }
                    case "WeatherComponent":
                        {
                            PlaylistItem targetitem = new PlaylistItem();

                            _weatherSetter = new WeatherSetter();
                            _weatherSetter.Load(targetitem.Content);
                            if (_weatherSetter.DialogResult == true)
                            {
                                targetitem.Content = _weatherSetter.GetContent();
                                targetitem.CONTENTSVALUE = targetitem.Content;
           
                                task.Add(targetitem);
                            }

                            break;
                        }
                    case "QuickTimeComponent":
                        {
                            string filter = Properties.Resources.labelFilterQuicktime;
                            OpenFileDialog(task, Properties.Resources.labelOpenQuicktime, filter);
                           
                            break;
                        }
                    case "FlashComponent":
                        {
                            string filter = Properties.Resources.labelFilterFlash;
                            OpenFileDialog(task, Properties.Resources.labelOpenFlash, filter);

                            break;
                        }
                    case "TVComponent":
                        {
                            PlaylistItem targetitem = new PlaylistItem();

                            TVChannelSelector dlg = new TVChannelSelector();
                            dlg.Load(targetitem.Content);
                            if (dlg.ISAPPLY)
                            {
                                targetitem.Content = dlg.GetContent();
                                targetitem.CONTENTSVALUE = targetitem.Content;

                                task.Add(targetitem);
                            }
                            break;
                        }
                    case "StreamingComponent":
                        {
                            PlaylistItem targetitem = new PlaylistItem();
                            
                            MediaSelector dlg = new MediaSelector();
                            dlg.Load(targetitem.Content);
                            if (dlg.ISAPPLY)
                            {
                                targetitem.Content = dlg.GetContent();
                                targetitem.CONTENTSVALUE = targetitem.Content;

                                task.Add(targetitem);
                            }
                            break;
                        }
                    case "PptComponent":
                        {
                            _openDialog.Filter = Properties.Resources.labelFilterPPT;
                            _openDialog.Multiselect = true;
                            _openDialog.Title = Properties.Resources.labelOpenPPT;

                            if(_openDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                            {
                                foreach (String sFileName in _openDialog.FileNames)
                                {
                                    if (sFileName != "" && File.Exists(sFileName))
                                    {
                                        PlaylistItem targetitem = new PlaylistItem();

                                        targetitem.Content = FileManager.AddNewFileWithModifiedName(sFileName);
                                        targetitem.CONTENTSVALUE = targetitem.Content;

                                        task.Add(targetitem);
                                    }
                                }
                            }

                            break;
                        }
                    default: break;
                }

//                 ChangeItems(oldItem, targetitem);
            }
            catch (Exception ex)
            {
                string errmsg = ex.Message.ToString();
            }
        }

        private void removeItem_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                PlaylistItem selectitem = ContentsListBox.SelectedItem as PlaylistItem;

                if (selectitem != null)
                {
                    PlaylistItemArry task = ContentsListBox.ItemsSource as PlaylistItemArry;
                    int index = 0; bool isSelect = false;
                    foreach (PlaylistItem screen in task)
                    {
                        if (screen == selectitem)
                        {
                            isSelect = true;
                            break;
                        }
                        index++;
                    }
                    if (isSelect)
                    {
                        task.RemoveAt(index);
                    }
                }
            }
            catch (Exception /*ex*/)
            {
               // logger.Error(ex + " " + ex.StackTrace);
            }
        }

        private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {
        }

        private void upItem_Click(object sender, RoutedEventArgs e)
        {
            if (ContentsListBox.SelectedItem != null)
            {
                PlaylistItem screen = (PlaylistItem)ContentsListBox.SelectedItem;
                ChangeItem(screen, true);
            }
        }

        private void downItem_Click(object sender, RoutedEventArgs e)
        {
            if (ContentsListBox.SelectedItem != null)
            {
                PlaylistItem screen = (PlaylistItem)ContentsListBox.SelectedItem;
                ChangeItem(screen, false);
            }
        }

        private void ChangeItem(PlaylistItem data, bool isUp)
        {
            try
            {
                PlaylistItemArry task = ContentsListBox.ItemsSource as PlaylistItemArry;
                int index = 0;
                bool isSelect = false;
                foreach (PlaylistItem screen in task)
                {
                    if (screen == data)
                    {
                        isSelect = true;
                        break;
                    }
                    index++;
                }
                if (isSelect)
                {
                    if (isUp)
                    {
                        if (index == 0) return;
                        task.RemoveAt(index);
                        index = index - 1;

                    }
                    else
                    {
                        if (index == task.Count - 1) return;
                        task.RemoveAt(index);
                        index = index + 1;
                    }

                    task.Insert(index, data);
                    ContentsListBox.SelectedIndex = index;
                }
            }
            catch (Exception ex)
            {
               // logger.Error(ex + " " + ex.StackTrace);
            }
        }

        private void durationUI_SelectedTimeChanged(object sender, AC.AvalonControlsLibrary.Controls.TimeSelectedChangedRoutedEventArgs e)
        {
            try
            {
                AC.AvalonControlsLibrary.Controls.TimePicker ctrl = (AC.AvalonControlsLibrary.Controls.TimePicker)sender;
                PlaylistItem item = (PlaylistItem)ctrl.DataContext;
                item.TIME = ctrl.SelectedTime;
                item.Duration = XamlWriter.Save(ctrl.SelectedTime);

				if (cboxLockDuration.IsChecked == true)
				{
					PlaylistItemArry tasks = ContentsListBox.ItemsSource as PlaylistItemArry;

					foreach (PlaylistItem i in tasks)
					{
						if (i.Equals(item)) continue;

						i.TIME = item.DurationAsTimeSpan;
						i.Duration = item.Duration;
					}
					ContentsListBox.ItemsSource = null;

					ContentsListBox.ItemsSource = tasks;
				}
            }
            catch
            { }
        }

        private void btn_Infinity_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ContentControl targetContentCtrl = (ContentControl)e.OriginalSource;
                PlaylistItem oldItem = (PlaylistItem)targetContentCtrl.DataContext;
                PlaylistItem targetitem = oldItem;
                targetitem.Duration = XamlWriter.Save(new TimeSpan(0, 0, 0));
                targetitem.TIME = new TimeSpan(0, 0, 0);
                ChangeItems(oldItem, targetitem);
            }
            catch { }
        }

		#region 컴포넌트 스트링 반환

		private String GetComponentStr(Type type)
		{
			try
			{
				switch (type.Name)
				{
					case "AnalogueClockComponent":
						{
							return Properties.Resources.toolTipAnalogueClock;
						}
					case "DigitalClockComponent":
						{
							return Properties.Resources.toolTipDigitalClock;
						}
					case "DateComponent":
						{
							return Properties.Resources.toolTipDate;
						}
					case "EllipseComponent":
						{
							return Properties.Resources.toolTipEllipse;
							break;
						}
					case "ImageComponent":
						{
							return Properties.Resources.toolTipImage;
						}
					case "AudioComponent":
						{
							return Properties.Resources.toolTipAudio;
						}
					case "MediaComponent":
						{
							return Properties.Resources.toolTipMedia;
						}
					case "RectangleComponent":
						{
							return Properties.Resources.toolTipRectangle;
							break;
						}
					case "RssComponent":
						{
							return Properties.Resources.toolTipRss;
						}
					case "ScrollTextComponent":
						{
							return Properties.Resources.toolTipScrollText;
						}
					case "TextComponent":
						{
							return Properties.Resources.toolTipText;
						}
					case "WebComponent":
						{
							return Properties.Resources.toolTipWeb;
						}
					case "WeatherComponent":
						{
							return Properties.Resources.toolTipWeather;
						}
					case "QuickTimeComponent":
						{
							return Properties.Resources.toolTipQuickTime;
						}
					case "FlashComponent":
						{
							return Properties.Resources.toolTipFlash;
						}
					case "PptComponent":
						{
							return Properties.Resources.toolTipPpt;
						}
					case "StreamingComponent":
						{
							return Properties.Resources.toolTipStreaming;
						}
					case "TVComponent":
						{
							return Properties.Resources.toolTipTV;
						}

				}
			}
			catch 
			{
			}
			return "Unknown";
		}
		#endregion

        private void filePath_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ContentControl targetContentCtrl = (ContentControl)e.OriginalSource;
                PlaylistItem oldItem = (PlaylistItem)targetContentCtrl.DataContext;
                PlaylistItem targetitem = oldItem;

                switch (_elementType.Name)
                {
                    case "AnalogueClockComponent":
                    case "DigitalClockComponent":
                        {
                            _gmtSetter = new GmtSetter();
                            _gmtSetter.Load(targetitem.Content);
                            if(_gmtSetter.DialogResult == true)
                            {
                                targetitem.Content = _gmtSetter.GetContent();
                                targetitem.CONTENTSVALUE = GmtManager.GetTimeZoneFromIndexInString(targetitem.Content).ToString();
                            }
                            break;
                        }
                    case "DateComponent":
                        {
                            break;
                        }
                    case "EllipseComponent":
                        {
                            break;
                        }
                    case "ImageComponent":
                        {
                            _openDialog.Multiselect = false;
                            _openDialog.Filter = Properties.Resources.labelFilterImage;
                            _openDialog.Title = Properties.Resources.labelOpenImage;

                            if (_openDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                            {
                                if (_openDialog.FileName != "" && File.Exists(_openDialog.FileName))
                                {
                                    targetitem.Content = FileManager.AddNewFileWithModifiedName(_openDialog.FileName);
                                    targetitem.CONTENTSVALUE = targetitem.Content;
                                }
                            }
                            break;
                        }
                    case "AudioComponent":
                        {
                            string filter = Properties.Resources.labelFilterAudio;
                            OpenFileDialog(targetitem, Properties.Resources.labelOpenAudio, filter);

                            Fill(targetitem);
                            break;
                        }
                    case "MediaComponent":
                        {
                            string filter = Properties.Resources.labelFilterVideo;
                            OpenFileDialog(targetitem, Properties.Resources.labelOpenVideo, filter);
                            Fill(targetitem);
                            break;
                        }
                    case "RectangleComponent":
                        {
                            break;
                        }
                    case "RssComponent":
						{
							_rtSetter = new RssTextSetter();
							_rtSetter.Load(ScrollTextInfoManager.DeserializeScrollTextInfo(targetitem.Content));
                            if (_rtSetter.DialogResult == true)
                            {
                                targetitem.CONTENTSVALUE = _rtSetter.GetContent();
                                if (targetitem.CONTENTSVALUE == new ScrollTextInfo())
                                {
                                    targetitem.CONTENTSVALUE = Properties.Resources.labelNoMedia;
                                    targetitem.Content = "";
                                }

                                targetitem.Content = ScrollTextInfoManager.SerializeScrollTextInfo((ScrollTextInfo)targetitem.CONTENTSVALUE);
                                targetitem.CONTENTSVALUE = targetitem.Content;
                            }
							break;
						}
                    case "ScrollTextComponent":
                        {
                            _stSetter = new ScrollTextSetter();
                            _stSetter.Load(ScrollTextInfoManager.DeserializeScrollTextInfo(targetitem.Content));

                            if (_stSetter.DialogResult == true)
                            {

                                targetitem.CONTENTSVALUE = _stSetter.GetContent();
                                if (targetitem.CONTENTSVALUE == new ScrollTextInfo())
                                {
                                    targetitem.CONTENTSVALUE = Properties.Resources.labelNoMedia;
                                    targetitem.Content = "";
                                }

                                targetitem.Content = ScrollTextInfoManager.SerializeScrollTextInfo((ScrollTextInfo)targetitem.CONTENTSVALUE);
                                targetitem.CONTENTSVALUE = targetitem.Content;
                            }
                            break;
                        }
                    case "TextComponent":
                        {
                            _textEditor = new TextEditor();
                            _textEditor.Load(targetitem.Content);

                            if (_textEditor.DialogResult == true)
                            {
                                targetitem.CONTENTSVALUE = targetitem.Content = _textEditor.GetDocument();
//                                 targetitem.CONTENTSVALUE = "Text";
                            }
                            break;
                        }
                    case "WebComponent":
                        {
                            _urlEditor = new UrlSetter();
                            _urlEditor.Load(targetitem.Content);
                            if (_urlEditor.DialogResult == true)
                            {
                                targetitem.Content = _urlEditor.GetContent().ToString();
                                targetitem.CONTENTSVALUE = targetitem.Content;
                            }
                            break;
                        }
					case "WeatherComponent":
						{
							_weatherSetter = new WeatherSetter();
							_weatherSetter.Load(targetitem.Content);
                            if (_weatherSetter.DialogResult == true)
                            {
                                targetitem.Content = _weatherSetter.GetContent();
                                targetitem.CONTENTSVALUE = targetitem.Content;
                            }
							break;
						}
                    case "QuickTimeComponent":
                        {
                            string filter = Properties.Resources.labelFilterQuicktime;
                            OpenFileDialog(targetitem, Properties.Resources.labelOpenQuicktime, filter);
                            Fill(targetitem);
                            break;
                        }
                    case "FlashComponent":
                        {
                            string filter = Properties.Resources.labelFilterFlash;
                            OpenFileDialog(targetitem, Properties.Resources.labelOpenFlash, filter);

                            Fill(targetitem);
                            break;
                        }
					case "TVComponent":
						{
							TVChannelSelector dlg = new TVChannelSelector();
							dlg.Load(targetitem.Content);
							if (dlg.ISAPPLY)
							{
								targetitem.Content = dlg.GetContent();
								targetitem.CONTENTSVALUE = targetitem.Content;

								Fill(targetitem);
							} 
							break;
						}
					case "StreamingComponent":
						{
							MediaSelector dlg = new MediaSelector();
							dlg.Load(targetitem.Content);
							if(dlg.ISAPPLY)
							{
								targetitem.Content = dlg.GetContent();
								targetitem.CONTENTSVALUE = targetitem.Content;

								Fill(targetitem);
							}
							break;
						}
                    case "PptComponent":
                        {
                            _openDialog.Multiselect = false;
                            _openDialog.Filter = Properties.Resources.labelFilterPPT;
                            _openDialog.Title = Properties.Resources.labelOpenPPT;
                            _openDialog.ShowDialog();
                            if (_openDialog.FileName != "" && File.Exists(_openDialog.FileName))
                            {
                                targetitem.Content = FileManager.AddNewFileWithModifiedName(_openDialog.FileName);
                                targetitem.CONTENTSVALUE = targetitem.Content;
                            }
                            break;
                        }
                    default: break;
                }

                ChangeItems(oldItem, targetitem);
            }
            catch (Exception ex)
            {
                string errmsg = ex.Message.ToString();
            }
          //*/
        }

        private bool ChangeItems(PlaylistItem oldData, PlaylistItem newData)
        {
            try
            {
                PlaylistItemArry task = ContentsListBox.ItemsSource as PlaylistItemArry;
                int index = 0;
                bool isSelect = false;
                foreach (PlaylistItem screen in task)
                {
                    if (screen == oldData)
                    {
                        isSelect = true;
                        break;
                    }
                    index++;
                }
                if (isSelect)
                {
                    task.RemoveAt(index);
                    task.Insert(index, newData);
                    return true;
                }
            }
            catch (Exception ex)
            {
               // logger.Error(ex + " " + ex.StackTrace);
            }
            return false;
        }

		public static PlaylistItem MakePlaylistItem(string filename)
		{
			try
			{
				string s = ((IMediaFilesManager)ProjectManager.GetCurrentProjectManager().Pane).AddNewFileWithModifiedName(filename);
				CompType comp_type = WPFDUtil.GetComponentTypeFromExtension(System.IO.Path.GetExtension(s).ToLower());

				PlaylistItem item = new PlaylistItem();

				item.CONTENTSVALUE = item.Content = s;

				TimeSpan duration = new TimeSpan();

				switch (comp_type)
				{
					case CompType.Media:
					case CompType.Quicktime:
						{
							try
							{
								FilgraphManager filterGraph = new FilgraphManager();
								filterGraph.RenderFile(filename);
								IMediaPosition pos = filterGraph as IMediaPosition;
								duration = new TimeSpan(0, 0, (int)pos.Duration);
							}
							catch
							{
								try
								{
									WindowsMediaPlayerClass wmp = new WindowsMediaPlayerClass();
									IWMPMedia media = wmp.newMedia(filename);
									duration = new TimeSpan(0, 0, Convert.ToInt32(media.duration));
									wmp.close();
									wmp = null;
								}
								catch { }
							}

						}
						break;
					case CompType.Audio:
						{
							try
							{
								MP3Header header = new MP3Header();
								header.ReadMP3Information(filename);
								duration = new TimeSpan(0, 0, header.intLength);
							}
							catch { }
						}
						break;
				}
				
				item.Duration = XamlWriter.Save(duration);

				return item;
			}
			catch
			{
			}

			return null;
		}
        private void OpenFileDialog(PlaylistItemArry arrList, string title, string filter)
        {
            System.Windows.Forms.OpenFileDialog OpenFileDialog = new System.Windows.Forms.OpenFileDialog();
            OpenFileDialog.Multiselect = true;
            OpenFileDialog.Filter = filter;// 
            OpenFileDialog.Title = title;

            if (OpenFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                foreach(String sFileName in OpenFileDialog.FileNames)
                {
                    string s = ((IMediaFilesManager)ProjectManager.GetCurrentProjectManager().Pane).AddNewFileWithModifiedName(sFileName);

                    CompType comp_type = WPFDUtil.GetComponentTypeFromExtension(System.IO.Path.GetExtension(s).ToLower());
                    switch (comp_type)
                    {
                        case CompType.Media:
                            {
                                try
                                {
                                    FilgraphManager filterGraph = new FilgraphManager();
                                    filterGraph.RenderFile(sFileName);
                                    IMediaPosition pos = filterGraph as IMediaPosition;
                                    duration = new TimeSpan(0, 0, (int)pos.Duration);
                                }
                                catch
                                {
                                    try
                                    {
                                        WindowsMediaPlayerClass wmp = new WindowsMediaPlayerClass();
                                        IWMPMedia media = wmp.newMedia(sFileName);
                                        duration = new TimeSpan(0, 0, Convert.ToInt32(media.duration));
                                        wmp.close();
                                        wmp = null;
                                    }
                                    catch { }
                                }

                            }
                            break;
                        case CompType.Quicktime:
                            {
                                try
                                {
                                    DigitalSignage.Controls.QuickTimeControl qc = new DigitalSignage.Controls.QuickTimeControl();
                                    tempcontrol.Child = qc;
                                    duration = qc.GetQuickTimeMovieDuration(sFileName);
                                    qc.Dispose();
                                    qc = null;
                                    tempcontrol.Child = null;
                                }
                                catch { }
                            }
                            break;
                        case CompType.Audio:
                            {
                                try
                                {
                                    MP3Header header = new MP3Header();
                                    header.ReadMP3Information(sFileName);
                                    duration = new TimeSpan(0, 0, header.intLength);
                                }
                                catch { }
                            }
                            break;
                        case CompType.Ppt:
                            break;
                        case CompType.Flash:
                            break;
                        case CompType.Image:
                            break;

                    }

                    PlaylistItem targetitem = new PlaylistItem();

                    targetitem.Content = s;
                    targetitem.Duration = GetDuration(duration);
                    targetitem.TIME = targetitem.DurationAsTimeSpan;

                    arrList.Add(targetitem);

                    Fill(targetitem);
                }
                
            }

        }

        private void OpenFileDialog(PlaylistItem targetitem, string title, string filter)
        {
            System.Windows.Forms.OpenFileDialog OpenFileDialog = new System.Windows.Forms.OpenFileDialog();
            OpenFileDialog.Filter = filter;// 
            OpenFileDialog.Title = title;
            OpenFileDialog.Multiselect = false;
            if (OpenFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string s = ((IMediaFilesManager)ProjectManager.GetCurrentProjectManager().Pane).AddNewFileWithModifiedName(OpenFileDialog.FileName);

				CompType comp_type = WPFDUtil.GetComponentTypeFromExtension(System.IO.Path.GetExtension(s).ToLower());
                switch(comp_type)
				{
					case CompType.Media:
					{
						try
						{
							FilgraphManager filterGraph = new FilgraphManager();
							filterGraph.RenderFile(OpenFileDialog.FileName);
							IMediaPosition pos = filterGraph as IMediaPosition;
							duration = new TimeSpan(0, 0, (int)pos.Duration);
						}
						catch 
						{
							try
							{
								WindowsMediaPlayerClass wmp = new WindowsMediaPlayerClass();
								IWMPMedia media = wmp.newMedia(OpenFileDialog.FileName);
								duration = new TimeSpan(0, 0, Convert.ToInt32(media.duration));
								wmp.close();
								wmp = null;
							}
							catch { }
						} 

					}
					break;
					case CompType.Quicktime:
					{
						try
						{
							DigitalSignage.Controls.QuickTimeControl qc = new DigitalSignage.Controls.QuickTimeControl();
							tempcontrol.Child = qc;
							duration = qc.GetQuickTimeMovieDuration(OpenFileDialog.FileName);
							qc.Dispose();
							qc = null;
							tempcontrol.Child = null;
						}
						catch { }
					}
						break;
					case CompType.Audio:
						{
							try
							{
								MP3Header header = new MP3Header();
								header.ReadMP3Information(OpenFileDialog.FileName);
								duration = new TimeSpan(0, 0, header.intLength);
							}
							catch { }
						}
						break;
					case CompType.Ppt:
						break;
					case CompType.Flash:
						break;
					case CompType.Image:
						break;

				}
                targetitem.Content = s;
                targetitem.Duration = GetDuration(duration);
                targetitem.TIME = targetitem.DurationAsTimeSpan;
            }

        }

        public string GetDuration(TimeSpan duration)
        {
            if (duration != null)
                return XamlWriter.Save(duration);
            else
                return XamlWriter.Save(new TimeSpan());
        }

        /// <summary>
        /// Fills the ItemView fields corresponding to the loaded PlaylistItem instance
        /// </summary>
        private void Fill(PlaylistItem targetitem)
        {
            switch (_elementType.Name)
            {
                case "AnalogueClockComponent":
                case "DigitalClockComponent":
                    {
                        if (targetitem.Content != "")
                        {
                            targetitem.CONTENTSVALUE = GmtManager.GetTimeZoneFromIndexInString(targetitem.Content);
                        }
                        else
                        {
                            targetitem.CONTENTSVALUE = Properties.Resources.labelNoMedia;
                        }
                        break;
                    }
                case "DateComponent":
                    {
                        break;
                    }
                case "EllipseComponent":
                    {
                        break;
                    }
                case "ImageComponent":
                    {
                        if (targetitem.Content != "")
                        {
                            targetitem.CONTENTSVALUE = targetitem.Content;
                        }
                        else
                        {
                            targetitem.CONTENTSVALUE = Properties.Resources.labelNoMedia;
                        }
                        // string images = "*.PNG;*.JPG;*.GIF;*.BMP;*.JPEG;";
                        // _openDialog.Filter = string.Format("Image files({0})|{0}", images);

						_openDialog.Filter = Properties.Resources.labelFilterImage;
                        _openDialog.Title = Properties.Resources.labelOpenImage;
                        break;
                    }
                case "AudioComponent":
                case "FlashComponent":
                case "QuickTimeComponent":
                case "MediaComponent":
				case "TVComponent":
				case "StreamingComponent":
                    {
                        if (targetitem.Content != "")
                        {
                            targetitem.CONTENTSVALUE = targetitem.Content;
                        }
                        else
                        {
                            targetitem.CONTENTSVALUE = Properties.Resources.labelNoMedia;
                        }
                        break;
                    }
                case "RectangleComponent":
                    {
                        if (targetitem.Content != "")
                        {
                            targetitem.CONTENTSVALUE = targetitem.Content;
                        }
                        else
                        {
                            targetitem.CONTENTSVALUE = Properties.Resources.labelNoMedia;
                        }
                        break;
                    }
                case "RssComponent":
                case "ScrollTextComponent":
                    {
                        if (targetitem.Content != "")
                        {
                            targetitem.CONTENTSVALUE = ScrollTextInfoManager.DeserializeScrollTextInfo(targetitem.Content);
                        }
                        else
                        {
                            targetitem.CONTENTSVALUE = Properties.Resources.labelNoMedia;
                        }
                        break;
                    }
                case "TextComponent":
                    {
                        if (targetitem.Content != "")
                        {
                            targetitem.CONTENTSVALUE = targetitem.Content;
                        }
                        else
                        {
                            targetitem.CONTENTSVALUE = Properties.Resources.labelNoMedia;
                        }
                        break;
                    }
                case "WebComponent":
                    {
                        if (targetitem.Content != "")
                        {
                            targetitem.CONTENTSVALUE = targetitem.Content;
                        }
                        else
                        {
                            targetitem.CONTENTSVALUE = "about:blank";
                            targetitem.Content = "about:blank";
                        }
                        break;
                    }
				case "WeatherComponent":
					{
						if (targetitem.Content != "")
						{
							targetitem.CONTENTSVALUE = targetitem.Content;
						}
						else
						{
							targetitem.CONTENTSVALUE = "Manual Setting";
							targetitem.Content = "Manual Setting";
						}
						break;
					}
                case "PptComponent":
                    {
                        if (targetitem.Content != "")
                        {
                            targetitem.CONTENTSVALUE = targetitem.Content;
                        }
                        else
                        {
                            targetitem.CONTENTSVALUE = Properties.Resources.labelNoMedia;
                            targetitem.Content = Properties.Resources.labelNoMedia;
                        }
                        break;
                    }
                default: break;
            }
        }
    }
}