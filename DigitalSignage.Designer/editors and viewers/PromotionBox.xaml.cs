﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;

namespace WPFDesigner.components
{
    /// <summary>
    /// Interaction logic for PromotionBox.xaml
    /// </summary>
    public partial class PromotionBox : UserControl
    {
        private Collection<Control> arrMetaTagControls = new Collection<Control>();

        Collection<string> arrKeys = new Collection<string>();
        Collection<string> arrValues = new Collection<string>();

        /// <summary>
        /// 생성자
        /// </summary>
        public PromotionBox()
        {
            InitializeComponent();
            arrMetaTagControls.Clear();
        }

        /// <summary>
        /// 타이틀 필드를 설정하거나 가져옵니다.
        /// </summary>
        public String TitleField
        {
            get { return labelTitle.Content.ToString(); }
            set {
                labelTitle.Content = value.EndsWith(":") ? value : value + " :";
            }
            
        }

        /// <summary>
        /// 아이템 추가
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void AddItem(string key, string value)
        {
            if (!arrKeys.Contains(key))
            {
                arrKeys.Add(key);
                arrValues.Add(value);

                RadioButton radio = new RadioButton();
                radio.Content = key;
                radio.VerticalAlignment = System.Windows.VerticalAlignment.Center;
                radio.DataContext = value;
                radio.Margin = new Thickness(0, 0, 10, 0);
                radio.Checked += new RoutedEventHandler(radio_Checked);
                arrMetaTagControls.Add(radio);
                spRadios.Children.Add(radio);
                radio.IsChecked = arrMetaTagControls.Count == 1;

            }
        }

        void radio_Checked(object sender, RoutedEventArgs e)
        {
            RadioButton radio = sender as RadioButton;

            try
            {

                if (radio != null)
                {
                    tbAddition.IsEnabled = labelTitle.IsEnabled = !radio.Content.Equals(arrKeys[0]);
                    
                }
            }
            catch { }
        }

        /// <summary>
        /// 데이터 결과
        /// </summary>
        public String Result
        {
            get
            {
                foreach (RadioButton rb in arrMetaTagControls)
                {
                    if (rb.IsChecked == true)
                    {
                        if (rb.Content.Equals(arrKeys[0]))
                        {
                            return arrKeys[0];
                        }
                        else
                        {
                            return String.Format("{0}, {1}", rb.Content, tbAddition.Text);
                        }
                    }
                }

                return String.Empty;
            }
            set
            {
                String sKey = String.Empty;
                String sValue = String.Empty;
                if (String.IsNullOrEmpty(value) || value.Equals(arrKeys[0]))
                {
                    sKey = arrKeys[0];
                 }
                else
                {
                    String [] arrValues = value.Split(',');

                    if (arrValues.Length > 0)
                    {
                        sValue = String.Empty;

                        try
                        {
                            sKey = arrValues[0].Trim();
                            sValue = arrValues[1].Trim();
                        }
                        catch { }
                    }
                    else
                    {
                        sKey = arrKeys[0];
                    }
                }

                foreach (RadioButton rb in arrMetaTagControls)
                {

                    if (rb.Content.Equals(sKey))
                    {
                        rb.IsChecked = true;
                        tbAddition.Text = sValue;
                        break;
                    }
                }
            }
        }
    }
}
