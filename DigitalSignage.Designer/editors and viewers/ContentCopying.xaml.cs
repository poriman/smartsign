﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;

namespace WPFDesigner.editors_and_viewers
{
    /// <summary>
    /// Interaction logic for ContentCopying.xaml
    /// </summary>
    public partial class ContentCopying : Window
    {
        FileStream readFileStream;
        FileStream writeFileStream;
        byte[] buffer;
        int readBlockSizeInBytes = 4096;
        int fileLength;

        private bool _copyComplete = false;

        private string _destination, _source;

        public ContentCopying(string source, string destination)
        {
            InitializeComponent();
            _source = source;
            _destination = destination;
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (!_copyComplete)
            {
                e.Cancel = true;
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Copy();
        }

        private delegate void ChangeProgressDelegate();
        private delegate void CloseWindowDelegate();
        ChangeProgressDelegate progressDelegate;
        CloseWindowDelegate closeWindowDelegate;

        private void ChangeProgressDelegateMethod()
        {
            progressBarCopyProgress.Value++;
        }

        private void CloseWindowDelegateMethod()
        {
            _copyComplete = true;
            this.Close();
        }

        private void Copy()
        {
            progressDelegate = ChangeProgressDelegateMethod;
            closeWindowDelegate = CloseWindowDelegateMethod;
            progressBarCopyProgress.Value = 0;

            fileLength = Convert.ToInt32(new FileInfo(_source).Length);
            buffer = new byte[fileLength];
            if (buffer.Length < 100)
            {
                File.Copy(_source, _destination);
                progressBarCopyProgress.Value = progressBarCopyProgress.Maximum;
                return;
            }
            else
            {
                readBlockSizeInBytes = buffer.Length / 100;
            }
            readFileStream = new FileStream(_source, FileMode.Open, FileAccess.Read);
            writeFileStream = new FileStream(_destination, FileMode.Create, FileAccess.ReadWrite);
            readFileStream.BeginRead(buffer, 0, readBlockSizeInBytes, new AsyncCallback(ReadCallback), new CopyInfo { Iteration = 0 });
        }

        private void ReadCallback(IAsyncResult result)
        {
            CopyInfo copyInfo = result.AsyncState as CopyInfo;

            int iteration = copyInfo.Iteration;
            int bytesRead = readFileStream.EndRead(result);
            int startIndex = iteration * readBlockSizeInBytes;

            writeFileStream.Write(buffer, startIndex, bytesRead);

            int bytesNumberWrote = startIndex + bytesRead;

            Dispatcher.Invoke(progressDelegate, null);

            if (bytesRead < readBlockSizeInBytes)
            {
                readFileStream.Close();
                writeFileStream.Close();

                Dispatcher.Invoke(closeWindowDelegate, null);
                return;
            }
            int bytesToRead;

            if (bytesNumberWrote + readBlockSizeInBytes > buffer.Length)
            {
                bytesToRead = buffer.Length - bytesNumberWrote;
            }
            else
            {
                bytesToRead = readBlockSizeInBytes;
            }

            readFileStream.BeginRead(buffer, startIndex + bytesRead, bytesToRead, ReadCallback, new CopyInfo { Iteration = ++copyInfo.Iteration });
        }

        public class CopyInfo
        {
            public int Iteration;
            public CopyInfo()
            {
                Iteration = 0;
            }
        }
    }
}