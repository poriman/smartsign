﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Markup;
using System.IO;
using System.Globalization;

namespace WPFDesigner
{
    /// <summary>
    /// Provides an editing of flow documents
    /// </summary>
    public partial class TextEditor : Window
    {
        System.Windows.Forms.ColorDialog cd;
        string loadedDoc;

        public TextEditor()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Init();
        }

        //Setups text editor
        void Init()
        {
// 			CultureInfo info = System.Threading.Thread.CurrentThread.CurrentUICulture;
// 			XmlLanguage resourceKey = XmlLanguage.GetLanguage(info != null ? info.IetfLanguageTag : "en-US");
// 			XmlLanguage enKey = XmlLanguage.GetLanguage("en-US");
// 			
// 			foreach (FontFamily ff in Fonts.SystemFontFamilies)
//             {
// 				ComboBoxItem item = new ComboBoxItem();
// 				item.Content = ff.FamilyNames[resourceKey] != null ? ff.FamilyNames[resourceKey] : ff.FamilyNames[enKey];
// 				item.DataContext = ff;
// // 				item.Content = ff;
// // 				item.FontFamily = ff;
// // 				item.FontSize = 16;
// 				te_FontFamily.Items.Add(item);
// 
// 				if (te_RichTextBox.FontFamily != null && te_RichTextBox.FontFamily.FamilyNames.Values.Contains(ff.Source))
// 					te_FontFamily.SelectedItem = item;
// 
// 				te_RichTextBox.Focus();
// 
//             }

			te_FontFamily.ItemsSource = Fonts.SystemFontFamilies;

            te_RichTextBox.SpellCheck.IsEnabled = true;
            te_RichTextBox.SpellCheck.SpellingReform = SpellingReform.PreAndPostreform;

            //Initiating color dialog that is used for setting color
            //of currently selected text
            cd = new System.Windows.Forms.ColorDialog();
            cd.AnyColor = true;
            cd.FullOpen = true;
            te_FontColor.MouseDown += new MouseButtonEventHandler(te_FontColor_MouseDown);
            te_backgroundLabel.MouseDown += new MouseButtonEventHandler(te_backgroundLabel_MouseDown);
        }

        void te_backgroundLabel_MouseDown(object sender, MouseButtonEventArgs e)
        {
            BackgroundSetter bs = new BackgroundSetter();
            bs.Load(te_backgroundLabel.Background.Clone());
            if (bs.ISApply)
            {
                te_backgroundLabel.Background = bs.GetContent();
                te_RichTextBox.Document.Background = te_backgroundLabel.Background;
            }
        }

        void te_FontColor_MouseDown(object sender, MouseButtonEventArgs e)
        {
            BackgroundSetter bs = new BackgroundSetter();
            bs.Load(te_FontColor.Background.Clone());
            if (bs.ISApply)
            {
                te_FontColor.Background = bs.GetContent();
                te_RichTextBox.Selection.ApplyPropertyValue(TextElement.ForegroundProperty, te_FontColor.Background);
            }
        }

        //Handling the context menu clicks
        private void MenuItem_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            switch (((MenuItem)sender).Name)
            {
                case "te_Copy":
                    {
                        te_RichTextBox.Copy();
                        break;
                    }
                case "te_Cut":
                    {
                        te_RichTextBox.Cut();
                        break;
                    }
                case "te_Paste":
                    {
                        te_RichTextBox.Paste();
                        break;
                    }
                default: break;
            }
        }

        //Loads a document to a text editor
        public void Load(string doc)
        {
			// "<Section xml:space=\"preserve\" xmlns=\"http://schemas.microsoft.com/winfx/2006/xaml/presentation\"><Paragraph FontSize=\"11\" FontFamily=\"Portable User Interface\" Foreground=\"#FF000000\" FontWeight=\"Normal\" FontStyle=\"Normal\" FontStretch=\"Normal\" CharacterSpacing=\"0\" Typography.AnnotationAlternates=\"0\" Typography.EastAsianExpertForms=\"False\" Typography.EastAsianLanguage=\"Normal\" Typography.EastAsianWidths=\"Normal\" Typography.StandardLigatures=\"True\" Typography.ContextualLigatures=\"True\" Typography.DiscretionaryLigatures=\"False\" Typography.HistoricalLigatures=\"False\" Typography.StandardSwashes=\"0\" Typography.ContextualSwashes=\"0\" Typography.ContextualAlternates=\"True\" Typography.StylisticAlternates=\"0\" Typography.StylisticSet1=\"False\" Typography.StylisticSet2=\"False\" Typography.StylisticSet3=\"False\" Typography.StylisticSet4=\"False\" Typography.StylisticSet5=\"False\" Typography.StylisticSet6=\"False\" Typography.StylisticSet7=\"False\" Typography.StylisticSet8=\"False\" Typography.StylisticSet9=\"False\" Typography.StylisticSet10=\"False\" Typography.StylisticSet11=\"False\" Typography.StylisticSet12=\"False\" Typography.StylisticSet13=\"False\" Typography.StylisticSet14=\"False\" Typography.StylisticSet15=\"False\" Typography.StylisticSet16=\"False\" Typography.StylisticSet17=\"False\" Typography.StylisticSet18=\"False\" Typography.StylisticSet19=\"False\" Typography.StylisticSet20=\"False\" Typography.Capitals=\"Normal\" Typography.CapitalSpacing=\"False\" Typography.Kerning=\"True\" Typography.CaseSensitiveForms=\"False\" Typography.HistoricalForms=\"False\" Typography.Fraction=\"Normal\" Typography.NumeralStyle=\"Normal\" Typography.NumeralAlignment=\"Normal\" Typography.SlashedZero=\"False\" Typography.MathematicalGreek=\"False\" Typography.Variants=\"Normal\" TextOptions.TextHintingMode=\"Fixed\" TextOptions.TextFormattingMode=\"Ideal\" TextOptions.TextRenderingMode=\"Auto\" TextAlignment=\"Left\" LineHeight=\"0\" LineStackingStrategy=\"MaxHeight\"><Run>fddfㅇㄹ</Run></Paragraph><Paragraph FontSize=\"11\" FontFamily=\"Portable User Interface\" Foreground=\"#FF000000\" FontWeight=\"Normal\" FontStyle=\"Normal\" FontStretch=\"Normal\" CharacterSpacing=\"0\" Typography.AnnotationAlternates=\"0\" Typography.EastAsianExpertForms=\"False\" Typography.EastAsianLanguage=\"Normal\" Typography.EastAsianWidths=\"Normal\" Typography.StandardLigatures=\"True\" Typography.ContextualLigatures=\"True\" Typography.DiscretionaryLigatures=\"False\" Typography.HistoricalLigatures=\"False\" Typography.StandardSwashes=\"0\" Typography.ContextualSwashes=\"0\" Typography.ContextualAlternates=\"True\" Typography.StylisticAlternates=\"0\" Typography.StylisticSet1=\"False\" Typography.StylisticSet2=\"False\" Typography.StylisticSet3=\"False\" Typography.StylisticSet4=\"False\" Typography.StylisticSet5=\"False\" Typography.StylisticSet6=\"False\" Typography.StylisticSet7=\"False\" Typography.StylisticSet8=\"False\" Typography.StylisticSet9=\"False\" Typography.StylisticSet10=\"False\" Typography.StylisticSet11=\"False\" Typography.StylisticSet12=\"False\" Typography.StylisticSet13=\"False\" Typography.StylisticSet14=\"False\" Typography.StylisticSet15=\"False\" Typography.StylisticSet16=\"False\" Typography.StylisticSet17=\"False\" Typography.StylisticSet18=\"False\" Typography.StylisticSet19=\"False\" Typography.StylisticSet20=\"False\" Typography.Capitals=\"Normal\" Typography.CapitalSpacing=\"False\" Typography.Kerning=\"True\" Typography.CaseSensitiveForms=\"False\" Typography.HistoricalForms=\"False\" Typography.Fraction=\"Normal\" Typography.NumeralStyle=\"Normal\" Typography.NumeralAlignment=\"Normal\" Typography.SlashedZero=\"False\" Typography.MathematicalGreek=\"False\" Typography.Variants=\"Normal\" TextOptions.TextHintingMode=\"Fixed\" TextOptions.TextFormattingMode=\"Ideal\" TextOptions.TextRenderingMode=\"Auto\" TextAlignment=\"Left\" LineHeight=\"0\" LineStackingStrategy=\"MaxHeight\"><Run>ㅇㄹ아러ㅏ우한ㅇ릉롱?</Run></Paragraph></Section>"
            loadedDoc = doc;
            te_RichTextBox.Document = FlowDocumentManager.DeserializeFlowDocument(loadedDoc);
            //StringReader stringReader = new StringReader("<Section xml:space=\"preserve\" xmlns=\"http://schemas.microsoft.com/winfx/2006/xaml/presentation\"><Paragraph FontSize=\"11\" FontFamily=\"Portable User Interface\" Foreground=\"#FF000000\" FontWeight=\"Normal\" FontStyle=\"Normal\" FontStretch=\"Normal\" Typography.AnnotationAlternates=\"0\" Typography.EastAsianExpertForms=\"False\" Typography.EastAsianLanguage=\"Normal\" Typography.EastAsianWidths=\"Normal\" Typography.StandardLigatures=\"True\" Typography.ContextualLigatures=\"True\" Typography.DiscretionaryLigatures=\"False\" Typography.HistoricalLigatures=\"False\" Typography.StandardSwashes=\"0\" Typography.ContextualSwashes=\"0\" Typography.ContextualAlternates=\"True\" Typography.StylisticAlternates=\"0\" Typography.StylisticSet1=\"False\" Typography.StylisticSet2=\"False\" Typography.StylisticSet3=\"False\" Typography.StylisticSet4=\"False\" Typography.StylisticSet5=\"False\" Typography.StylisticSet6=\"False\" Typography.StylisticSet7=\"False\" Typography.StylisticSet8=\"False\" Typography.StylisticSet9=\"False\" Typography.StylisticSet10=\"False\" Typography.StylisticSet11=\"False\" Typography.StylisticSet12=\"False\" Typography.StylisticSet13=\"False\" Typography.StylisticSet14=\"False\" Typography.StylisticSet15=\"False\" Typography.StylisticSet16=\"False\" Typography.StylisticSet17=\"False\" Typography.StylisticSet18=\"False\" Typography.StylisticSet19=\"False\" Typography.StylisticSet20=\"False\" Typography.Capitals=\"Normal\" Typography.CapitalSpacing=\"False\" Typography.Kerning=\"True\" Typography.CaseSensitiveForms=\"False\" Typography.HistoricalForms=\"False\" Typography.Fraction=\"Normal\" Typography.NumeralStyle=\"Normal\" Typography.NumeralAlignment=\"Normal\" Typography.SlashedZero=\"False\" Typography.MathematicalGreek=\"False\" Typography.Variants=\"Normal\" TextOptions.TextHintingMode=\"Fixed\" TextOptions.TextFormattingMode=\"Ideal\" TextOptions.TextRenderingMode=\"Auto\" TextAlignment=\"Left\" LineStackingStrategy=\"MaxHeight\"><Run></Run></Paragraph><Paragraph FontSize=\"11\" FontFamily=\"Portable User Interface\" Foreground=\"#FF000000\" FontWeight=\"Normal\" FontStyle=\"Normal\" FontStretch=\"Normal\" Typography.AnnotationAlternates=\"0\" Typography.EastAsianExpertForms=\"False\" Typography.EastAsianLanguage=\"Normal\" Typography.EastAsianWidths=\"Normal\" Typography.StandardLigatures=\"True\" Typography.ContextualLigatures=\"True\" Typography.DiscretionaryLigatures=\"False\" Typography.HistoricalLigatures=\"False\" Typography.StandardSwashes=\"0\" Typography.ContextualSwashes=\"0\" Typography.ContextualAlternates=\"True\" Typography.StylisticAlternates=\"0\" Typography.StylisticSet1=\"False\" Typography.StylisticSet2=\"False\" Typography.StylisticSet3=\"False\" Typography.StylisticSet4=\"False\" Typography.StylisticSet5=\"False\" Typography.StylisticSet6=\"False\" Typography.StylisticSet7=\"False\" Typography.StylisticSet8=\"False\" Typography.StylisticSet9=\"False\" Typography.StylisticSet10=\"False\" Typography.StylisticSet11=\"False\" Typography.StylisticSet12=\"False\" Typography.StylisticSet13=\"False\" Typography.StylisticSet14=\"False\" Typography.StylisticSet15=\"False\" Typography.StylisticSet16=\"False\" Typography.StylisticSet17=\"False\" Typography.StylisticSet18=\"False\" Typography.StylisticSet19=\"False\" Typography.StylisticSet20=\"False\" Typography.Capitals=\"Normal\" Typography.CapitalSpacing=\"False\" Typography.Kerning=\"True\" Typography.CaseSensitiveForms=\"False\" Typography.HistoricalForms=\"False\" Typography.Fraction=\"Normal\" Typography.NumeralStyle=\"Normal\" Typography.NumeralAlignment=\"Normal\" Typography.SlashedZero=\"False\" Typography.MathematicalGreek=\"False\" Typography.Variants=\"Normal\" TextOptions.TextHintingMode=\"Fixed\" TextOptions.TextFormattingMode=\"Ideal\" TextOptions.TextRenderingMode=\"Auto\" TextAlignment=\"Left\" LineStackingStrategy=\"MaxHeight\"><Run></Run></Paragraph></Section>");
            //System.Xml.XmlReader xmlReader = System.Xml.XmlReader.Create(stringReader);
            //Section sec = XamlReader.Load(xmlReader) as Section;
            //te_RichTextBox.Document = new FlowDocument();
            //while (sec.Blocks.Count > 0)
            //    te_RichTextBox.Document.Blocks.Add(sec.Blocks.FirstBlock);
			

            te_backgroundLabel.Background = te_RichTextBox.Document.Background == null ? new SolidColorBrush(Color.FromArgb(0, 255, 255, 255)) : te_RichTextBox.Document.Background ;
            if (te_RichTextBox.Document.Blocks.Count > 0)
            {
                te_FontColor.Background = te_RichTextBox.Document.Blocks.FirstBlock.Foreground;
            }
            ShowDialog();
        }

        //Returns an edited document
        public string GetDocument()
        {
            return loadedDoc;
        }

        //Unloads text editor
        public void Unload()
        {
            this.Close();
        }

        //Change font size of selected text
        private void te_FontSize_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
			try
			{
				string s = ((ComboBoxItem)te_FontSize.SelectedValue).Content.ToString();

				te_RichTextBox.Selection.ApplyPropertyValue(TextElement.FontSizeProperty, Double.Parse(s));
			}
			catch
			{
			}
        }

        //Change font family of selected text
        private void te_FontFamily_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {
			try
			{
				te_RichTextBox.Selection.ApplyPropertyValue(TextElement.FontFamilyProperty, te_FontFamily.SelectedItem/*((ComboBoxItem)te_FontFamily.SelectedValue).DataContext*/);
			}
			catch { }
        }

        private void closeButton_Click(object sender, RoutedEventArgs e)
        {

            Close();
        }

        private void okButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;

            loadedDoc = FlowDocumentManager.SerializeFlowDocument(te_RichTextBox.Document);
            Close();
        }

        private void applyButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;

            loadedDoc = FlowDocumentManager.SerializeFlowDocument(te_RichTextBox.Document);
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;

            Close();
        }

        private void btn_AddImage_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                System.Windows.Forms.OpenFileDialog OpenFileDialog = new System.Windows.Forms.OpenFileDialog();
				OpenFileDialog.Filter = "All supported|*.jpg;*.bmp;*.gif;*.png|JPEG files|*.jpg|BMP files| *.bmp|GIF files|*.gif|TIFF files|*.tiff";
                OpenFileDialog.Title = "Add an image";
                if (OpenFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    BitmapImage imageSource = new BitmapImage();
                    imageSource.BeginInit();
                    imageSource.UriSource = new Uri(OpenFileDialog.FileName);
                    imageSource.EndInit();

                    Clipboard.SetImage(imageSource);
                    te_RichTextBox.Paste();
                }
            }
            catch (ArgumentNullException aEx)
            {
                System.Windows.MessageBox.Show(aEx.Message.ToString());
            }
            catch (Exception ex)
            {
                System.Windows.MessageBox.Show(ex.Message.ToString());
            }
        }

		private void te_RichTextBox_SelectionChanged(object sender, RoutedEventArgs e)
		{
			try
			{
				if(!te_RichTextBox.Selection.IsEmpty)
				{
					var fontFamily = te_RichTextBox.Selection.GetPropertyValue(TextElement.FontFamilyProperty);

					te_FontFamily.SelectedItem = fontFamily;

					// Set font size combo
					var fontSize = te_RichTextBox.Selection.GetPropertyValue(TextElement.FontSizeProperty);
					foreach(ComboBoxItem item in te_FontSize.Items)
					{
						if (fontSize.ToString().Equals(item.Content.ToString()))
						{
							item.IsSelected = true;
							break;
						}

					}
				}
			}
			catch
			{
				
			}

		}
    }
}