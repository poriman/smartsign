﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Markup;

using QuartzTypeLib;
using WMPLib;
using WPFDesigner.tools;

namespace WPFDesigner
{
    /// <summary>
    /// Interaction logic for MediaSelector.xaml
    /// </summary>
    public partial class MediaSelector : Window
    {
        private bool IsApply = false;
        System.Windows.Forms.OpenFileDialog OpenFileDialog;
        TimeSpan duration;
        string loadedPath;

        /// <summary>
        /// Sets the filter of dialog that is used for choosing the media locally
        /// </summary>
        public string DialogFilter
        {
            set
            {
                OpenFileDialog.Filter = value;
            }
        }

        public bool ISAPPLY
        {
            set { IsApply = value; }
            get { return IsApply; }
        }

        public MediaSelector()
        {
            InitializeComponent();
            OpenFileDialog = new System.Windows.Forms.OpenFileDialog();
            OpenFileDialog.Filter = "Video (WMV, AVI, MPG/MPEG)| *.wmv; *.avi; *.mpg;*.mpeg";//|Images (BMP, JPG, TIFF, GIF)|*.bmp; *.jpg; *.jpeg; *.tiff; *.gif|Audio (WMA)|*.wma";
            OpenFileDialog.Title = "Open video";
            OpenFileDialog.FileOk += new System.ComponentModel.CancelEventHandler(OpenFileDialog_FileOk);
        }

        void OpenFileDialog_FileOk(object sender, System.ComponentModel.CancelEventArgs e)
        {
            string s = ((IMediaFilesManager)ProjectManager.GetCurrentProjectManager().Pane).AddNewFileWithModifiedName(OpenFileDialog.FileName);

            if (System.IO.Path.GetExtension(s).ToLower() == ".wmv")
            {
                try
                {
                    WindowsMediaPlayerClass wmp = new WindowsMediaPlayerClass();
                    IWMPMedia media = wmp.newMedia(OpenFileDialog.FileName);
                    duration = new TimeSpan(0, 0, Convert.ToInt32(media.duration));
                    wmp.close();
                    wmp = null;
                }
                catch
                {
                }
            }
            else if (System.IO.Path.GetExtension(s).ToLower() == ".mp3")
            {
                try
                {
                    MP3Header header = new MP3Header();
                    header.ReadMP3Information(s);
                    duration = new TimeSpan(0, 0, header.intLength);
                }
                catch { }
            }
            else
            {
                try
                {
                    FilgraphManager filterGraph = new FilgraphManager();
                    filterGraph.RenderFile(OpenFileDialog.FileName);
                    IMediaPosition pos = filterGraph as IMediaPosition;
                    duration = new TimeSpan(0, 0, (int)pos.Duration);
                }
                catch { }
                try
                {
                    DigitalSignage.Controls.QuickTimeControl qc = new DigitalSignage.Controls.QuickTimeControl();
                    duration = qc.GetQuickTimeMovieDuration(OpenFileDialog.FileName);
                    qc.Dispose();
                    qc = null;
                }
                catch { }
            }
            pathBox.Text = s;
        }

        public void Load(string mediaPath)
        {
            ISAPPLY = false;
            pathBox.Text = mediaPath;
            loadedPath = mediaPath;

			if (String.IsNullOrEmpty(pathBox.Text))
			{
				pathBox.Text = "mms://";
			}
			pathBox.Focus();
			pathBox.SelectAll();

            ShowDialog();
        }

        public string GetContent()
        {
			if (pathBox.Text.Equals("mms://"))
			{
				return loadedPath = "";
			}

			return loadedPath = pathBox.Text;
        }

        public string GetDuration()
        {
            if (duration != null)
                return XamlWriter.Save(duration);
            else
                return XamlWriter.Save(new TimeSpan());
        }

        private void okButton_Click(object sender, RoutedEventArgs e)
        {
            ISAPPLY = true;
            loadedPath = pathBox.Text;

            this.DialogResult = true;

            Close();
        }

        private void applyButton_Click(object sender, RoutedEventArgs e)
        {
            loadedPath = pathBox.Text;
            ISAPPLY = true;

            this.DialogResult = true;

        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;

            Close();
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void openDialogButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog.ShowDialog();
        }
    }
}