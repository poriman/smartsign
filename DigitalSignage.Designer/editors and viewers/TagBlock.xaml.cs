﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPFDesigner
{
	/// <summary>
	/// Interaction logic for TagBlock.xaml
	/// </summary>
	public partial class TagBlock : Border
	{
		public TagBlock()
		{
			InitializeComponent();

		//	DefaultStyleKeyProperty.OverrideMetadata(typeof(TagBlock), new FrameworkPropertyMetadata(typeof(TagBlock)));

		}

		#region Header Name

		public static readonly DependencyProperty HeaderNameProperty =
			DependencyProperty.Register("HeaderName", typeof(string), typeof(TagBlock),
				new FrameworkPropertyMetadata((string)string.Empty));

		public string HeaderName
		{
			get { return (string)GetValue(HeaderNameProperty); }
			set { SetValue(HeaderNameProperty, value); }
		}
		#endregion

		public event EventHandler OnDeleted;		

		bool bIsDelete = false;

		private void TextBlock_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{
			bIsDelete = true;
		}

		private void Border_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{
			bIsDelete = true;
		}

		private void btnDelete_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
		{
			if (bIsDelete && OnDeleted != null) OnDeleted(this, e);
			bIsDelete = false;
		}

		private void tbX_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
		{
			if (bIsDelete && OnDeleted != null) OnDeleted(this, e);
			bIsDelete = false;
		}

		private void mainBorder_MouseEnter(object sender, MouseEventArgs e)
		{
			mainBorder.Background = Brushes.Black;
			tbX.Foreground = tbEditable.Foreground = Brushes.White;
		}

		private void mainBorder_MouseLeave(object sender, MouseEventArgs e)
		{
			mainBorder.Background = Brushes.Silver;
			tbX.Foreground = tbEditable.Foreground = Brushes.Black;
		}

		private void btnDelete_MouseEnter(object sender, MouseEventArgs e)
		{
			btnDelete.Background = Brushes.White;
			tbX.Foreground = Brushes.Black;
		}

		private void btnDelete_MouseLeave(object sender, MouseEventArgs e)
		{
			btnDelete.Background = Brushes.Transparent;
			tbX.Foreground = Brushes.White;
		}

	}
}
