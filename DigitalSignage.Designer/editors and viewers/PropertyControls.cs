﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Xml;
using System.Xml.Linq;
using System.Linq;
using System.Windows.Data;
using System.Reflection;
using System.Globalization;
using System.Windows.Controls.Primitives;
using DigitalSignage.Common;
namespace WPFDesigner
{
    /// <summary>
    /// Represents the slider adapted to proeprties voiewr`s needs
    /// </summary>
    public class SliderControl : Slider, IPropertyControl
    {
        private UIElement _element;

        private bool _isLoading = false;

        public event EventHandler ModifiedVolumeByUser = null;

        /// <summary>
        /// Gets of sets the UIElement
        /// </summary>
        public UIElement uiElement
        {
            get
            {
                return _element;
            } 

            set
            {
                _element = value;
                if (_element == null) return;
                
                _isLoading = true;
                Value = Convert.ToDouble(((IDesignElement)_element).Properties[Name].ToString());
                _isLoading = false;
            }
        }

        /// <summary>
        /// Initializes the new SliderControl class instance
        /// </summary>
        /// <param name="definition">Definition of control`s settings</param>
        public SliderControl(XElement definition)
            : base()
        {
            Name = definition.Attribute("PropertyName").Value;
            Minimum = Double.Parse(definition.Attribute("Minimum").Value);
            Maximum = Double.Parse(definition.Attribute("Maximum").Value);
            SmallChange = Double.Parse(definition.Attribute("SmallChange").Value);
            LargeChange = Double.Parse(definition.Attribute("LargeChange").Value);

			if (Name.Contains("Volume"))
				this.AutoToolTipPlacement = AutoToolTipPlacement.None;
			else
				this.AutoToolTipPlacement = AutoToolTipPlacement.TopLeft;
            AutoToolTipPrecision = 2;
            this.ValueChanged += new RoutedPropertyChangedEventHandler<double>(SliderControl_ValueChanged);
            this.Visibility = Visibility.Collapsed;
        }

        void SliderControl_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (_element == null) return;
            PropertySetter.SetProperty(_element as IDesignElement, "IDesignElement", Name, Value);
            if (_element.GetType() != typeof(Screen))
                ((Screen)((FrameworkElement)_element).Parent).IsChanged = true;
            else ((Screen)_element).IsChanged = true;

            if (!_isLoading && Name.Equals("Volume") && ModifiedVolumeByUser != null) ModifiedVolumeByUser(this, null); 
        }

        public void Dispose()
        {
            this.ValueChanged -= new RoutedPropertyChangedEventHandler<double>(SliderControl_ValueChanged);
        }
    }

    /// <summary>
    /// Represents the ckeck box adapted to proeprties voiewr`s needs
    /// </summary>
    public class CheckBoxControl : CheckBox, IPropertyControl
    {
        private UIElement _element;

        /// <summary>
        /// Gets of sets the UIElement
        /// </summary>
        public UIElement uiElement
        {
            get
            {
                return _element;
            }

            set
            {
                _element = value;
                if (_element == null) return;
                IsChecked = Convert.ToBoolean(((IDesignElement)_element).Properties[Name]);
            }
        }

        /// <summary>
        /// Initializes the new CheckBoxControl class instance
        /// </summary>
        /// <param name="definition">Definition of control`s settings</param>
        public CheckBoxControl(XElement definition)
            : base()
        {
            Name = definition.Attribute("PropertyName").Value;
            this.Checked += new RoutedEventHandler(CheckBoxControl_Checked);
            this.Unchecked += new RoutedEventHandler(CheckBoxControl_Unchecked);
            this.Visibility = Visibility.Collapsed;
        }

        void CheckBoxControl_Unchecked(object sender, RoutedEventArgs e)
        {
            PropertySetter.SetProperty(_element as IDesignElement, "IDesignElement", Name, false);
            if (_element.GetType() != typeof(Screen))
                ((Screen)((FrameworkElement)_element).Parent).IsChanged = true;
            else ((Screen)_element).IsChanged = true;
        }

        void CheckBoxControl_Checked(object sender, RoutedEventArgs e)
        {
            PropertySetter.SetProperty(_element as IDesignElement, "IDesignElement", Name, true);
            if (_element.GetType() != typeof(Screen))
                ((Screen)((FrameworkElement)_element).Parent).IsChanged = true;
            else ((Screen)_element).IsChanged = true;
        }

        public void Dispose()
        {
            this.Checked -= new RoutedEventHandler(CheckBoxControl_Checked);
            this.Unchecked -= new RoutedEventHandler(CheckBoxControl_Unchecked);
            _element = null;
        }
    }

    /// <summary>
    /// Represents the label adapted to proeprties voiewr`s needs
    /// </summary>
    public class LabelControl : Label, IPropertyControl
    {
        private UIElement _element;

        /// <summary>
        /// Gets of sets the UIElement
        /// </summary>
        public UIElement uiElement
        {
            get
            {
                return _element;
            }
            set
            {
                _element = value;
                if (_element == null) return;
				this.BorderThickness = new Thickness(1);
				this.BorderBrush = Brushes.Silver;
                Background = ((IDesignElement)_element).Properties[Name] as Brush;
            }
        }

        /// <summary>
        /// Initializes the new LabelControl class instance
        /// </summary>
        /// <param name="definition">Definition of control`s settings</param>
        public LabelControl(XElement definition)
            : base()
        {
            Name = definition.Attribute("PropertyName").Value;
            Content = "       ";
            this.MouseDown += new System.Windows.Input.MouseButtonEventHandler(LabelControl_MouseDown);
            this.Visibility = Visibility.Collapsed;
        }

        void LabelControl_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (_element == null) return;
            BackgroundSetter bs = new BackgroundSetter();
			try
			{
				bs.Load(this.Background.Clone());
			}
			catch { bs.Load(null); };

            if (bs.ISApply)
            {
                this.Background = bs.GetContent();
                PropertySetter.SetProperty(_element as IDesignElement, "IDesignElement", Name, Background);
                if (_element.GetType() != typeof(Screen))
                    ((Screen)((FrameworkElement)_element).Parent).IsChanged = true;
                else
                    ((Screen)_element).IsChanged = true;
            }
        }

        public void Dispose()
        {
            this.MouseDown -= new System.Windows.Input.MouseButtonEventHandler(LabelControl_MouseDown);
        }
    }

    /// <summary>
    /// Represents the text box adapted to proeprties voiewr`s needs
    /// </summary>
    public class TextBoxControl : TextBox, IPropertyControl
    {
        /// <summary>
        /// Chars that are restricted in elements` names
        /// </summary>
        string[] unresolvedNameSymbols;
        private UIElement _element;

        /// <summary>
        /// Gets of sets the UIElement
        /// </summary>
        public UIElement uiElement
        {
            get
            {
                return _element;
            }
            set
            {
                _element = value;
                if (_element == null) return;
                BindingOperations.ClearAllBindings(this);
                //Text = ((IDesignElement)_element).Properties[Name].ToString();

                #region Binding maintance
                Binding b = new Binding();
                b.Source = _element;
                b.Mode = BindingMode.OneWay;
                switch (Name)
                {
                    case "Width":
                        {
                            b.Path = new PropertyPath(FrameworkElement.WidthProperty);
//                             DesignElementConvert converter = new DesignElementConvert();
//                             b.Converter = converter;
                            this.SetBinding(TextBox.TextProperty, b);
                            break;
                        }
                    case "Left":
                        {
                            b.Path = new PropertyPath(InkCanvas.LeftProperty);
                            DesignElementConvert converter = new DesignElementConvert();
                            b.Converter = converter;
                            this.SetBinding(TextBox.TextProperty, b);
                            break;
                        }
                    case "Top":
                        {
                            b.Path = new PropertyPath(InkCanvas.TopProperty);
                            DesignElementConvert converter = new DesignElementConvert();
                            b.Converter = converter;
                            this.SetBinding(TextBox.TextProperty, b);
                            break;
                        }
                    case "Height":
                        {
                            b.Path = new PropertyPath(FrameworkElement.HeightProperty);
                            DesignElementConvert converter = new DesignElementConvert();
                            b.Converter = converter;
                            this.SetBinding(TextBox.TextProperty, b);
                            break;
                        }
                    case "StrokesLength":
                        {
                            Text = "";
                            foreach (int i in ((IDesignElement)_element).StrokesLength)
                            {
                                Text += i.ToString() + " ";
                            }
                            break;
                        }
					case "RefreshInterval":
						{
							Text = Convert.ToInt32(((IDesignElement)_element).RefreshInterval.TotalMinutes).ToString();
							break;
						}
                    case "Name":
                        {
                            b.Path = new PropertyPath(FrameworkElement.NameProperty);
                            this.SetBinding(TextBox.TextProperty, b);

                            if (_element is Screen) this.IsEnabled = false;

                            break;
                        }
                    case "ZIndex":
                        {
                            b.Path = new PropertyPath(Canvas.ZIndexProperty);
                            this.SetBinding(TextBox.TextProperty, b);
                            break;
                        }
                    default: break;
                }
                #endregion
            }
        }

        /// <summary>
        /// Initializes the new TextBoxControl class instance
        /// </summary>
        /// <param name="definition">Definition of control`s settings</param>
        public TextBoxControl(XElement definition)
            : base()
        {
            unresolvedNameSymbols = new string[] { " ", "`", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", ",", ".", "/", "\"", "\'", "=", "+", "-", "_", "|", ":", ";" };
            this.Name = definition.Attribute("PropertyName").Value;
            this.KeyDown += new System.Windows.Input.KeyEventHandler(TextBoxControl_KeyDown);
            this.PreviewTextInput += new System.Windows.Input.TextCompositionEventHandler(TextBoxControl_PreviewTextInput);
            this.TextChanged += new TextChangedEventHandler(TextBoxControl_TextChanged);
            this.Visibility = Visibility.Collapsed;
			
			this.LostKeyboardFocus += new System.Windows.Input.KeyboardFocusChangedEventHandler(TextBoxControl_LostKeyboardFocus);
			this.LostFocus += new RoutedEventHandler(TextBoxControl_LostFocus);
			
			if(this.Name.Equals("ZIndex"))
            {
                this.IsEnabled = false;
            }
        }

		void TextBoxControl_LostFocus(object sender, RoutedEventArgs e)
		{
			try
			{
				Double dNum = Convert.ToDouble(Text);
				if (dNum > 99999)
				{
					Text = "99999";
					this.SelectionStart = 5;
					PropertySetter.SetProperty(_element as IDesignElement, "IDesignElement", Name, dNum);

				}
				else
					PropertySetter.SetProperty(_element as IDesignElement, "IDesignElement", Name, dNum);

			}
			catch (System.Exception /*err*/)
			{
				object objReturn = PropertyGetter.GetProperty(_element as IDesignElement, "IDesignElement", Name);

				if (objReturn == null) Text = "";
				else Text = objReturn.ToString();
			}
		}

		void TextBoxControl_LostKeyboardFocus(object sender, System.Windows.Input.KeyboardFocusChangedEventArgs e)
		{
			try
			{
				Double dNum = Convert.ToDouble(Text);
				if (dNum > 99999)
				{
					Text = "99999";
					this.SelectionStart = 5;
					PropertySetter.SetProperty(_element as IDesignElement, "IDesignElement", Name, dNum);

				}
				else
					PropertySetter.SetProperty(_element as IDesignElement, "IDesignElement", Name, dNum);

			}
			catch (System.Exception /*err*/)
			{
				object objReturn = PropertyGetter.GetProperty(_element as IDesignElement, "IDesignElement", Name);

				if (objReturn == null) Text = "";
				else Text = objReturn.ToString();
			}
		}

        void TextBoxControl_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (_element == null) return;
            if (Text == "") return;

            try
            {
                switch (Name)
                {
                    case "Name":
                        {
                            PropertySetter.SetProperty(_element as IDesignElement, "IDesignElement", Name, Text.Replace(" ", ""));
                            break;
                        }
                    case "ZIndex":
                        {
                            PropertySetter.SetProperty(_element as IDesignElement, "IDesignElement", Name, Convert.ToInt32(Text));
                            break;
                        }
                    case "StrokesLength":
                        {
                            PropertySetter.SetProperty(_element as IDesignElement, "IDesignElement", Name, Int32Collection.Parse(Text).ToArray());
                            break;
                        }
					case "RefreshInterval":
						{
							PropertySetter.SetProperty(_element as IDesignElement, "IDesignElement", Name, new TimeSpan(0, Convert.ToInt32(Text), 0));
							break;
						}
                    case "Left":
                    case "Height":
                    case "Top":
                    case "Width":
                        {
                            Double dNum = Convert.ToDouble(Text);
                            if(dNum > 99999)
                            {
                                Text = "99999";
                                this.SelectionStart = 5;
                            }
//                             else
//                                 PropertySetter.SetProperty(_element as IDesignElement, "IDesignElement", Name, dNum);

                            break;
                        }
                    default:
                        {
                            break;
                        }
                }
            }
            catch (Exception /*ex*/)
            {
// 				MessageBox.Show(ex.ToString());
				return; }

            try
            {

                if (_element.GetType() != typeof(Screen))
                {
                    ((Screen)((FrameworkElement)_element).Parent).IsChanged = true;
                }
                else ((Screen)_element).IsChanged = true;
            }
            catch { }
        }

        void TextBoxControl_PreviewTextInput(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            switch (Name)
            {
                case "StrokesLength":
                    {
                        int a;

                        if (e.Text == " " || Int32.TryParse(e.Text, out a))
                        {
                            e.Handled = false;
                        }
                        else e.Handled = true;
                        break;
                    }
                case "BorderCorner":
                    {
                        int i;
                        if (!Int32.TryParse(e.Text, out i))
                            e.Handled = true;
                        else e.Handled = false;

                        break;
                    }
				case "RefreshInterval":
					{
						int i;
						if (!Int32.TryParse(e.Text, out i))
							e.Handled = true;
						else e.Handled = false;

						break;
					}
				case "Name":
                    {
                        if (!unresolvedNameSymbols.Contains(e.Text))
                        {
                            e.Handled = false;
                        }
                        else e.Handled = true;
                        break;
                    }
                default: break;
            }
        }

        void TextBoxControl_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key != System.Windows.Input.Key.Enter) return;
            if (_element == null) return;
            if (Text == "") return;
            try
            {
                switch (Name)
                {
                    case "Name":
                        {
                            PropertySetter.SetProperty(_element as IDesignElement, "IDesignElement", Name, Text.Replace(" ", ""));
                            break;
                        }
                    case "ZIndex":
                        {
                            PropertySetter.SetProperty(_element as IDesignElement, "IDesignElement", Name, Convert.ToInt32(Text));
                            break;
                        }
                    case "StrokesLength":
                        {
                            PropertySetter.SetProperty(_element as IDesignElement, "IDesignElement", Name, Int32Collection.Parse(Text).ToArray());
                            break;
                        }
					case "RefreshInterval":
						{
							PropertySetter.SetProperty(_element as IDesignElement, "RefreshInterval", Name, new TimeSpan(0, Convert.ToInt32(Text), 0));
							break;
						}
                    case "Left":
                    case "Height":
                    case "Top":
                    case "Width":
                        {
                            PropertySetter.SetProperty(_element as IDesignElement, "IDesignElement", Name, Convert.ToDouble(Text));
                            break;
                        }
                    default:
                        {
                            break;
                        }
                }
            }
            catch
            { return; }

            if (_element.GetType() != typeof(Screen))
                ((Screen)((FrameworkElement)_element).Parent).IsChanged = true;
            else ((Screen)_element).IsChanged = true;
        }

        public void Dispose()
        {
            this.PreviewTextInput -= new System.Windows.Input.TextCompositionEventHandler(TextBoxControl_PreviewTextInput);
        }
    }

    /// <summary>
    /// Represents the text box adapted to proeprties voiewr`s needs
    /// </summary>
    public class ComboBoxControl : ComboBox, IPropertyControl
    {
        private UIElement _element;

        /// <summary>
        /// Gets of sets the UIElement
        /// </summary>
        public UIElement uiElement
        {
            get
            {
                return _element;
            }

            set
            {
                _element = value;
                if (_element == null) return;
                SelectedItem = ((IDesignElement)_element).Properties[Name];
            }
        }

        /// <summary>
        /// Initializes the new ComboBoxControl class instance
        /// </summary>
        /// <param name="definition">Definition of control`s settings</param>
        public ComboBoxControl(XElement definition)
            : base()
        {
            this.Name = definition.Attribute("PropertyName").Value;

            switch (Name)
            {
                case "HorizontalAlignment":
                    {
                        Items.Add(HorizontalAlignment.Left);
                        Items.Add(HorizontalAlignment.Center);
                        Items.Add(HorizontalAlignment.Right);
                        Items.Add(HorizontalAlignment.Stretch);
                        break;
                    }
                case "VerticalContentAlignment":
                case "VerticalAlignment":
                    {
                        Items.Add(VerticalAlignment.Top);
                        Items.Add(VerticalAlignment.Center);
                        Items.Add(VerticalAlignment.Bottom);
                        Items.Add(VerticalAlignment.Stretch);
                        break;
                    }
                case "StrokeDashCap":
                    {
                        Items.Add(PenLineCap.Flat);
                        Items.Add(PenLineCap.Round);
                        Items.Add(PenLineCap.Square);
                        Items.Add(PenLineCap.Triangle);
                        break;
                    }
                case "FontFamily":
                    {
                        foreach (FontFamily ff in Fonts.SystemFontFamilies)
                        {
// 							ComboBoxItem item = new ComboBoxItem();
// 							item.Content = ff;
// 							item.FontFamily = ff;
// 							item.FontSize = 16;
// 							Items.Add(item); 
							Items.Add(ff);
                        }
                        break;
                    }
                case "FontWeight":
                    {
                        Items.Add(FontWeights.Normal);
                        Items.Add(FontWeights.Bold);
                        break;
                    }
                case "Stretch":
                    {
                        Items.Add(Stretch.None);
                        Items.Add(Stretch.Uniform);
                        Items.Add(Stretch.UniformToFill);
                        Items.Add(Stretch.Fill);
                        break;
                    }
                case "AspectRatio":
                    {
                        Items.Add(new Point(4, 3));
                        Items.Add(new Point(9, 16));
                        Items.Add(new Point(16, 9));
                        break;
                    }
                case "TransEffect":
                    {
                        Items.Add(TransformEffect.None);
                        Items.Add(TransformEffect.TopToBottom);
                        Items.Add(TransformEffect.BottomToTop);
                        Items.Add(TransformEffect.LeftToRight);
                        Items.Add(TransformEffect.RightToLeft);
                        Items.Add(TransformEffect.FadeIn);
                        break;
                    }
                default: break;
            }
            SelectionChanged += new SelectionChangedEventHandler(ComboBoxControl_SelectionChanged);
            this.Visibility = Visibility.Collapsed;
        }

        void ComboBoxControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (_element == null) return;

// 			if (Name.CompareTo("FontFamily") == 0 && SelectedItem.GetType() == typeof(ComboBoxItem))
// 			{
// 				PropertySetter.SetProperty(_element as IDesignElement, "IDesignElement", Name, ((ComboBoxItem)SelectedItem).Content);
// 			
// 			}
// 			else
				PropertySetter.SetProperty(_element as IDesignElement, "IDesignElement", Name, SelectedItem);
           
			if (_element.GetType() != typeof(Screen))
                ((Screen)((FrameworkElement)_element).Parent).IsChanged = true;
            else ((Screen)_element).IsChanged = true;
        }

        public void Dispose()
        {
            SelectionChanged -= new SelectionChangedEventHandler(ComboBoxControl_SelectionChanged);
        }
    }

    /// <summary>
    /// Represents the interface that allows to get the acces to
    /// control`s property that it cnanges and the method
    /// to dispose the control
    /// </summary>
    public interface IPropertyControl
    {
        /// <summary>
        /// Gets or sets the property that can be changed by this control
        /// </summary>
        UIElement uiElement { get; set; }

        void Dispose();
    }
}