﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using System.Xml;

namespace WPFDesigner.components
{
    /// <summary>
    /// Interaction logic for MetatagControl.xaml
    /// </summary>
    public partial class MetatagControl : UserControl
    {
        private Collection<Control> arrMetaTagControls = new Collection<Control>();

        private String sMetaValues = String.Empty;
        private String sMetaTags = String.Empty;

        /// <summary>
        /// XML에서 파싱된 메타 태그
        /// </summary>
        public String MetaTags { get { return sMetaTags; } }
        /// <summary>
        /// XML에서 파싱된 메타 태그의 값
        /// </summary>
        public String MetaValues { get { return sMetaValues; } }

        /// <summary>
        /// 메타태그 반환
        /// </summary>
        public String SelectedMetaTags
        {
            get
            {
                try
                {
                    String sReturn = String.Empty;
                    foreach (Border sp in spMataTags.Children)
                    {
                        String sGroupName = sp.DataContext as String;
                        String sTags = String.Empty;

                        int nChecked = 0;

                        if (sp.Child is ListBox)
                        {
                            int nIndex = 0;
                            foreach (Control con in ((ListBox)sp.Child).Items)
                            {
                                if (con is CheckBox)
                                {
                                    CheckBox cb = con as CheckBox;
                                    if (cb.IsChecked == true)
                                    {
                                        if (nIndex == 0)
                                        {
                                            sTags = String.Format("{0} 모두", sGroupName);
                                            break;
                                        }

                                        sTags = String.IsNullOrEmpty(sTags)
                                            ? cb.Content.ToString() : String.Format("{0}, {1}", sTags, cb.Content);
                                        nChecked++;

                                        
                                    }
                                }
                                else if (con is RadioButton)
                                {
                                    RadioButton rb = con as RadioButton;
                                    if (rb.IsChecked == true)
                                    {
                                        if (nIndex == 0)
                                        {
                                            sTags = String.Format("{0} 모두", sGroupName);
                                            break;
                                        }

                                        sTags = String.IsNullOrEmpty(sTags)
                                            ? rb.Content.ToString() : String.Format("{0}, {1}", sTags, rb.Content);
                                        nChecked++;
                                    }
                                }
                                nIndex++;
                            }
                        }
                        else if (sp.Child is SearchBox)
                        {
                            components.SearchBox sb = sp.Child as components.SearchBox;

                            if (!String.IsNullOrEmpty(sb.SearchResult))
                            {
                                sTags = String.IsNullOrEmpty(sTags)
                                    ? sb.SearchResult : String.Format("{0}, {1}", sTags, sb.SearchResult);
                            }
                        }
                        else if (sp.Child is PromotionBox)
                        {
                           components.PromotionBox pb = sp.Child as components.PromotionBox;

                            if (!String.IsNullOrEmpty(pb.Result))
                            {
                                sTags = pb.Result;
                            }
                        }

                        if (String.IsNullOrEmpty(sTags))
                            sTags = String.Format("{0} 모두", sGroupName);

                        sReturn = String.IsNullOrEmpty(sReturn) ? String.Format("{0}[{1}]", sGroupName, sTags) :
                            String.Format("{0}, {1}[{2}]", sReturn, sGroupName, sTags);
                    }

                    return sReturn;
                }
                catch { }

                return String.Empty;
            }
            set
			{
				try
				{

                    string[] arrGroups = value.Split(']');
                    foreach (String groupmesh in arrGroups)
                    {
                        string data = groupmesh.Trim(new char[] {',', ' '});
                        
                        if (String.IsNullOrEmpty(data)) continue;

                        string sGroupName = String.Empty;
                        string sTags = String.Empty;
                        string[] arrSplit1 = data.Split('[');

                        try
                        {
                            sGroupName = arrSplit1[0].Trim();
                            sTags = arrSplit1[1].Trim();

                            Border fe = FindControl(ref spMataTags, sGroupName) as Border;

                            if (fe != null)
                            {
                                string[] arrTags = sTags.Replace(", ", ",").Split(',');

                                if (arrTags.Length > 0)
                                {
                                    if (fe.Child is ListBox)
                                    {
                                        ListBox lb = fe.Child as ListBox;
                                        if (arrTags.Contains(String.Format("{0} 모두", sGroupName)))
                                        {
                                            if ((lb.Items[0] is CheckBox))
                                            {
                                                CheckBox cb = lb.Items[0] as CheckBox;
                                                cb.IsChecked = true;
                                            }
                                            else if (lb.Items[0] is RadioButton)
                                            {
                                                RadioButton rb = lb.Items[0] as RadioButton;
                                                rb.IsChecked = true;
                                            }

                                            continue;
                                        }

                                        foreach (Control cont in ((ListBox)fe.Child).Items)
                                        {
                                            
                                            if (cont is CheckBox)
                                            {
                                                CheckBox cb = cont as CheckBox;

                                                if (arrTags.Contains(cb.Content as String))
                                                {
                                                    cb.IsChecked = true;
                                                }
                                            }
                                            else if (cont is RadioButton)
                                            {
                                                RadioButton rb = cont as RadioButton;
                                                if (arrTags.Contains(rb.Content as String))
                                                {
                                                    rb.IsChecked = true;
                                                }
                                            }
                                        }
                                    }
                                    else if (fe.Child is components.SearchBox)
                                    {
                                        components.SearchBox sb = fe.Child as components.SearchBox;

                                        sb.SearchResult = String.IsNullOrEmpty(sb.SearchResult)
                                        ? sTags : String.Format("{0}, {1}", sb.SearchResult, sTags);
                                    }
                                    else if (fe.Child is components.PromotionBox)
                                    {
                                        components.PromotionBox pb = fe.Child as components.PromotionBox;

                                        pb.Result = sTags;
                                    }

                                }
                            }

                        }
                        catch { continue; }

                    }
				}
				catch { }
			}
        }
        
        /// <summary>
        /// 컨트롤 찾기
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        FrameworkElement FindControl(ref WrapPanel parent, string name)
        {
            FrameworkElement contReturn = null;

            try
            {
                foreach (FrameworkElement cont in parent.Children)
                {
                    if (name.Equals(cont.DataContext))
                        return cont;
                }
            }
            catch { }

            return contReturn;
        }

        /// <summary>
        /// 생성자
        /// </summary>
        public MetatagControl()
        {
            InitializeComponent();

            LoadMetaTagXml();

        }

        /// <summary>
        /// 메타 태그 XML을 읽어온다
        /// </summary>
        private void LoadMetaTagXml()
        {
            //AddDummyMetaTags();

            string path = AppDomain.CurrentDomain.BaseDirectory + "settings\\meta_tags.xml";
            if (!System.IO.File.Exists(path))
            {
                MessageBox.Show(String.Format(Properties.Resources.mbErrorMetaTagDefinition, path), Properties.Resources.titleDesignerWindow, MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            try
            {
                arrMetaTagControls.Clear();

                XmlDocument doc = new XmlDocument();
                doc.Load(path);

                XmlNodeList tag_groups = doc.SelectNodes("//child::tag_group");

                if (tag_groups != null)
                {
                    foreach (XmlNode node in tag_groups)
                    {
                        XmlNodeList tags = node.SelectNodes("child::tag");

                        if (tags != null)
                        {
                            Border bd = new Border();
                            //StackPanel sp1 = new StackPanel();
                            //sp1.Orientation = Orientation.Horizontal;
                            bd.VerticalAlignment = System.Windows.VerticalAlignment.Center;
                            bd.Margin = new Thickness(5);
                            bd.MinHeight = 25;
                            bd.DataContext = node.Attributes["name"].Value;

                            string uiType = node.Attributes["ui_type"].Value;

                            components.SearchBox sbox = null;
                            components.PromotionBox pbox = null;
                            ListBox lBox = null;

                            if (uiType.Equals("SearchBox"))
                            {
                                sbox = new components.SearchBox();
                                sbox.Width = 500;
                                sbox.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
                                sbox.ButtonText = Properties.Resources.labelAddAddress;
                                sbox.GroupName = node.Attributes["name"].Value;
                                sbox.AllCheckText = Properties.Resources.labelAllAddress;

                                bd.Child = sbox;
                                arrMetaTagControls.Add(sbox);
                            }
                            else if (uiType.Equals("PromotionBox"))
                            {
                                pbox = new components.PromotionBox();
                                pbox.Width = 500;
                                pbox.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
                                bd.Child = pbox;
                                arrMetaTagControls.Add(pbox);
                            }
                            else
                            {
                                lBox = new ListBox();
                                lBox.HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch;
                                lBox.Width = 160;
                                lBox.Height = 80;

                                if (uiType.Equals("CheckBox"))
                                {
                                    CheckBox box = new CheckBox();
                                    box.Content = Properties.Resources.labelAll ;
                                    box.VerticalAlignment = System.Windows.VerticalAlignment.Center;
                                    box.Margin = new Thickness(5, 0, 0, 0);
                                    lBox.Items.Add(box);
                                }
                                else if (uiType.Equals("RadioBox"))
                                {
                                    RadioButton radio = new RadioButton();
                                    radio.Content = Properties.Resources.labelAll;
                                    radio.VerticalAlignment = System.Windows.VerticalAlignment.Center;
                                    radio.Margin = new Thickness(5, 0, 0, 0);
                                }

                                bd.Child = lBox;
                            }
                          

                            foreach (XmlNode tagnode in tags)
                            {
                                Control cont = null;
                                switch (uiType)
                                {
                                    case "CheckBox":
                                        {
                                            CheckBox box = new CheckBox();
                                            box.Content = tagnode.Attributes["name"].Value;
                                            box.VerticalAlignment = System.Windows.VerticalAlignment.Center;
                                            box.DataContext = tagnode.Attributes["value"].Value;
                                            box.Margin = new Thickness(5, 0, 0, 0);

                                            cont = box;
                                        }
                                        break;
                                    case "RadioBox":
                                        {
                                            RadioButton radio = new RadioButton();
                                            radio.Content = tagnode.Attributes["name"].Value;
                                            radio.VerticalAlignment = System.Windows.VerticalAlignment.Center;
                                            radio.DataContext = tagnode.Attributes["value"].Value;
                                            radio.Margin = new Thickness(5, 0, 0, 0);

                                            cont = radio;
                                        }
                                        break;
                                    case "SearchBox":
                                        {
                                            if (sbox != null)
                                            {
                                                sbox.AddItem(tagnode.Attributes["name"].Value, tagnode.Attributes["value"].Value);
                                            }
                                        }
                                        break;
                                    case "PromotionBox":
                                        {
                                            if (pbox != null)
                                            {
                                                pbox.AddItem(tagnode.Attributes["name"].Value, tagnode.Attributes["value"].Value);
                                            }

                                            break;
                                        }
                                }

                                if (cont != null)
                                {
                                    arrMetaTagControls.Add(cont);
                                    if (lBox != null) lBox.Items.Add(cont);
                                }
                            }
                            this.spMataTags.Children.Add(bd);
                        }
                    }
                }
            }
            catch { }
        }

        /// <summary>
        /// 더미 메타 태그 추가 샘플
        /// </summary>
        private void AddDummyMetaTags()
        {
            StackPanel sp1 = new StackPanel();
            sp1.Orientation = Orientation.Horizontal;
            sp1.Margin = new Thickness(5, 0, 0, 0);

            CheckBox dummy1 = new CheckBox();
            dummy1.Content = "남자";
            dummy1.Margin = new Thickness(0, 0, 5, 0);
            CheckBox dummy2 = new CheckBox();
            dummy2.Content = "여자";
            dummy2.Margin = new Thickness(0, 0, 5, 0);

            sp1.Children.Add(dummy1);
            sp1.Children.Add(dummy2);

            spMataTags.Children.Add(sp1);

            StackPanel sp2 = new StackPanel();
            sp2.Orientation = Orientation.Horizontal;
            sp2.Margin = new Thickness(5, 0, 0, 0);

            CheckBox dummy10 = new CheckBox();
            dummy10.Content = "10대";
            dummy10.Margin = new Thickness(0, 0, 5, 0);
            CheckBox dummy11 = new CheckBox();
            dummy11.Content = "20대";
            dummy11.Margin = new Thickness(0, 0, 5, 0);
            CheckBox dummy12 = new CheckBox();
            dummy12.Content = "30대";
            dummy12.Margin = new Thickness(0, 0, 5, 0);
            CheckBox dummy13 = new CheckBox();
            dummy13.Content = "40대";
            dummy13.Margin = new Thickness(0, 0, 5, 0);
            CheckBox dummy14 = new CheckBox();
            dummy14.Content = "50대";
            dummy14.Margin = new Thickness(0, 0, 5, 0);
            CheckBox dummy15 = new CheckBox();
            dummy15.Content = "60대";
            dummy15.Margin = new Thickness(0, 0, 5, 0);
            CheckBox dummy16 = new CheckBox();
            dummy16.Content = "70대";
            dummy16.Margin = new Thickness(0, 0, 5, 0);
            CheckBox dummy17 = new CheckBox();
            dummy17.Content = "80대 이상";
            dummy17.Margin = new Thickness(0, 0, 5, 0);

            sp2.Children.Add(dummy10);
            sp2.Children.Add(dummy11);
            sp2.Children.Add(dummy12);
            sp2.Children.Add(dummy13);
            sp2.Children.Add(dummy14);
            sp2.Children.Add(dummy15);
            sp2.Children.Add(dummy16);
            sp2.Children.Add(dummy17);

            spMataTags.Children.Add(sp2);
        }
    }
}
