﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPFDesigner
{
    /// <summary>
    /// Interaction logic for ElementView.xaml
    /// </summary>
    public partial class ElementView : UserControl
    {
        /// <summary>
        /// Represents Screen
        /// </summary>
        Screen _screen;

        /// <summary>
        /// Indicates whether the item is selected or not
        /// </summary>
        public bool IsSelected;

        /// <summary>
        /// Represents the element
        /// </summary>
        public UIElement _element;

        /// <summary>
        /// Initializes the new ElementView class instance
        /// </summary>
        public ElementView()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Initializes the new ElementView class instance based on the given element
        /// </summary>
        /// <param name="element">UIElement</param>
        public ElementView(UIElement element)
        {
            InitializeComponent();
            IsSelected = false;
            FrameworkElement felement = element as FrameworkElement;

            #region Binding the item title to the element`s name property
            Binding b = new Binding();
            b.Source = felement;
            b.Path = new PropertyPath("Name");
            b.Mode = BindingMode.OneWay;
            elementName.SetBinding(Label.ContentProperty, b);
            #endregion

            _screen = felement.Parent as Screen;
            _element = element;

            #region Getting the icon of the element
			try
			{
				BitmapImage bi = new BitmapImage();
				bi.BeginInit();
				bi.UriSource = new Uri("pack://application:,,,/images/16" + element.GetType().Name + ".png");
				bi.EndInit();
				icon.Source = bi;
			}
			catch
			{ }
            #endregion

			#region Binding the item`s tooltip to the element`s name property
			b = new Binding();
			b.Source = elementName;
			b.Path = new PropertyPath("Content");
			b.Mode = BindingMode.OneWay;
			this.SetBinding(FrameworkElement.ToolTipProperty, b);
			#endregion

            BitmapImage bi2 = new BitmapImage();
            bi2.BeginInit();
            bi2.UriSource = new Uri("pack://application:,,,/images/play.png");
            bi2.EndInit();

            Play.Source = bi2;
        }

        private void this_MouseDown(object sender, MouseButtonEventArgs e)
        {
            
        }

        /// <summary>
        /// Highlights the item
        /// </summary>
        public void Select()
        {
            border.Background = (SolidColorBrush)this.Resources["Highlited"];
            IsSelected = true;
        }

        /// <summary>
        /// De-highlights the item
        /// </summary>
        public void Deselect()
        {
            border.Background = (SolidColorBrush)this.Resources["NotHighlighted"];
            IsSelected = false;
        }

        public void PlayStatus(bool isPlay)
        {
            BitmapImage bi2 = new BitmapImage();
            bi2.BeginInit();            
            if (isPlay == true)
            {
                bi2.UriSource = new Uri("pack://application:,,,/images/stop.png");
            }
            else
            {
                bi2.UriSource = new Uri("pack://application:,,,/images/play.png");
            }
            bi2.EndInit();
            Play.Source = bi2;
        }

        private void Play_MouseDown(object sender, MouseButtonEventArgs e)
        {
			if (_screen != null && _screen.IsPlay)
				return;

            IDesignElement el = _element as IDesignElement;
            if (el.ISPlay == true)
            {
				try
				{
					el.Stop();
				}
				catch
				{ }
                PlayStatus(el.ISPlay);
            }
            else
            {
                DigitalSignage.Common.ScreenStartInfoInSchedule.GetInstance.ScreenStartDT = DateTime.Now;

				try
				{
					el.Play();
				}
				catch { }
                PlayStatus(el.ISPlay);
            }
        }
    }
}