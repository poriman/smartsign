﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using ThoughtWorks.QRCode.Codec;
using ThoughtWorks.QRCode.Codec.Data;
using ThoughtWorks.QRCode.Codec.Util;
using System.Drawing;
using System.IO;
using System.Drawing.Imaging;

namespace WPFDesigner
{
    /// <summary>
    /// QRCodeEditor.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class QRCodeEditor : Window
    {
        private string strCont = "";
        bool isApply = false;
        public string GetContent()
        {
            return adressTextBox.Text;
        }

        public bool ISAPPLY
        {
            get { return isApply; }
        }
        public QRCodeEditor()
        {
            InitializeComponent();
            adressTextBox.Text = "";
        }

        public void Load(string url)
        {
            if (url.Length > 0)
            {
                adressTextBox.Text = url;
                QRCodeEncoder qrCodeEncoder = new QRCodeEncoder();
                qrCodeEncoder.QRCodeEncodeMode = QRCodeEncoder.ENCODE_MODE.BYTE;
                qrCodeEncoder.QRCodeScale = 4;
                qrCodeEncoder.QRCodeVersion = 7;
                qrCodeEncoder.QRCodeErrorCorrect = QRCodeEncoder.ERROR_CORRECTION.M;

                System.Drawing.Image _image;
                String data = adressTextBox.Text;
                _image = qrCodeEncoder.Encode(data);
                image.ImageSource = Image2BitmapImage(_image);
            }

            this.ShowDialog();
        }

        private BitmapImage Image2BitmapImage(System.Drawing.Image image) 
        {
            BitmapImage bi = new BitmapImage();
            bi.BeginInit();
            MemoryStream ms = new MemoryStream();
            image.Save(ms, ImageFormat.Png);
            ms.Seek(0, SeekOrigin.Begin);
            bi.StreamSource = ms;
            bi.EndInit();

            return bi;
        }

        private void applyButton_Click(object sender, RoutedEventArgs e)
        {
            if (adressTextBox.Text.Trim() == String.Empty)
            {
                MessageBox.Show("URL를 입력 하십시오.");
                return;
            }
            isApply = true;
            this.DialogResult = true;
            Close();
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            Close();
        }

        private void tb_GotKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
        {

        }

        private void createButton_Click(object sender, RoutedEventArgs e)
        {
            if (adressTextBox.Text.Trim() == String.Empty)
            {
                MessageBox.Show("URL를 입력 하십시오.");
                return;
            }

            QRCodeEncoder qrCodeEncoder = new QRCodeEncoder();
            String encoding = adressTextBox.Text;
            qrCodeEncoder.QRCodeEncodeMode = QRCodeEncoder.ENCODE_MODE.BYTE;
            qrCodeEncoder.QRCodeScale = 4;
            qrCodeEncoder.QRCodeVersion = 7;
            qrCodeEncoder.QRCodeErrorCorrect = QRCodeEncoder.ERROR_CORRECTION.M;

            System.Drawing.Image  _image;
            _image = qrCodeEncoder.Encode(encoding);
            image.ImageSource = Image2BitmapImage(_image);

        }
    }
}
