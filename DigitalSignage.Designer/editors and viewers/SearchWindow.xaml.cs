﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Collections.ObjectModel;

namespace WPFDesigner.components
{
	/// <summary>
	/// Interaction logic for SearchWindow.xaml
	/// </summary>
	public partial class SearchWindow : Window
	{
		SearchBox _objSearchBox = null;

		public String SelectedValue
		{
			get
			{
				try
				{
					if (wrapAdded.Children.Count == 0)
					{
						ListBoxItem item = lbSearchResult.SelectedItem as ListBoxItem;

						return (string)item.DataContext;
					}
					else
					{
						String sReturn = String.Empty;
						foreach (TagBlock tb in wrapAdded.Children)
						{
							if (String.IsNullOrEmpty(sReturn))
								sReturn = (string)tb.DataContext;
							else
								sReturn += String.Format(", {0}", tb.DataContext);
						}

						return sReturn;
					}
				}
				catch { return null; }
			}
		}

		public String SelectedKey
		{
			get
			{
				try
				{
					if (wrapAdded.Children.Count == 0)
					{
						ListBoxItem item = lbSearchResult.SelectedItem as ListBoxItem;

						return (string)item.Content;
					}
					else
					{
						String sReturn = String.Empty;
						foreach (TagBlock tb in wrapAdded.Children)
						{
							if (String.IsNullOrEmpty(sReturn))
								sReturn = tb.HeaderName;
							else
								sReturn += String.Format(", {0}", tb.HeaderName);
						}

						return sReturn;
					}
				}
				catch { return null; }
			}
		}


		public SearchWindow(SearchBox objSearchBox)
		{
			InitializeComponent();

			_objSearchBox = objSearchBox;
		}

		private void lbSearchResult_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			//this.Close();
		}

		void item_MouseDoubleClick(object sender, MouseButtonEventArgs e)
		{
			if (e.ChangedButton == MouseButton.Left) AddSelectedItem();
			
		}
		
		private void lbSearchResult_KeyDown(object sender, KeyEventArgs e)
		{
			if(e.Key == Key.Enter)
				AddSelectedItem();
		}
        
        private void AddSelectedItem()
		{
			ListBoxItem item = lbSearchResult.SelectedItem as ListBoxItem;

			if (item != null && item.IsFocused)
			{
                AddTagBlock(item.Content, item.DataContext);

			}
		}

        public void AddTagBlock(object tag, object value)
        {
            foreach (TagBlock b in wrapAdded.Children)
            {
                if (b.HeaderName.Equals(tag))
                    return;
            }

            TagBlock tb = new TagBlock();
            tb.HeaderName = (string)tag;
            tb.DataContext = (string)value;
            tb.OnDeleted += new EventHandler(tb_OnDeleted);
            wrapAdded.Children.Add(tb);
        }

		void tb_OnDeleted(object sender, EventArgs e)
		{
			wrapAdded.Children.Remove(sender as UIElement);
		}

		private void btnSearch_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				lbSearchResult.Items.Clear();

				String sKeyword = tbSearch.Text;
				if (String.IsNullOrEmpty(sKeyword))
				{
					tbNoResult.Visibility = Visibility.Visible;
				}
				else
				{
					Collection<String> arrResult = _objSearchBox.FindLikeItem(sKeyword);

					if (arrResult != null && arrResult.Count > 0)
					{

						tbNoResult.Visibility = Visibility.Hidden;
						foreach (string keyvalue in arrResult)
						{
							ListBoxItem item = new ListBoxItem();
							String[] arrData = keyvalue.Split('|');
							item.Content = arrData[0];
							item.DataContext = arrData[1];
							item.PreviewMouseDoubleClick += new MouseButtonEventHandler(item_MouseDoubleClick);
							lbSearchResult.Items.Add(item);
						}
					}
					else
					{
						tbNoResult.Visibility = Visibility.Visible;
					}
				}
			}
			catch { }
		}



		private void Button_Click(object sender, RoutedEventArgs e)
		{
			this.Close();
		}

		private void Window_Loaded(object sender, RoutedEventArgs e)
		{
			tbSearch.Focus();
		}

		private void tbSearch_TextChanged(object sender, TextChangedEventArgs e)
		{
			try
			{
				lbSearchResult.Items.Clear();

				String sKeyword = tbSearch.Text;
				if (String.IsNullOrEmpty(sKeyword))
				{
					tbNoResult.Visibility = Visibility.Visible;
				}
				else
				{
					Collection<String> arrResult = _objSearchBox.FindLikeItem(sKeyword);

					if (arrResult != null && arrResult.Count > 0)
					{

						tbNoResult.Visibility = Visibility.Hidden;
						foreach (string keyvalue in arrResult)
						{
							ListBoxItem item = new ListBoxItem();
							String[] arrData = keyvalue.Split('|');
							item.Content = arrData[0];
							item.DataContext = arrData[1];
							item.PreviewMouseDoubleClick += new MouseButtonEventHandler(item_MouseDoubleClick);
							lbSearchResult.Items.Add(item);
						}
					}
					else
					{
						tbNoResult.Visibility = Visibility.Visible;
					}
				}
			}
			catch { }

		}

	}
}
