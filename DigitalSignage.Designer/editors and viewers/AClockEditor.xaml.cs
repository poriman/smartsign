﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using DigitalSignage.Common;

namespace WPFDesigner
{
    /// <summary>
    /// Interaction logic for AClockEditor.xaml
    /// </summary>
    public partial class AClockEditor : Window
    {
        AClockStyle loadedStyle, currentStyle;
        AnalogueClockComponent clock;

        public AClockEditor()
        {
            InitializeComponent();

            hHandFill.MouseDown += new MouseButtonEventHandler(colorLabel_MouseDown);
            mHandFill.MouseDown += new MouseButtonEventHandler(colorLabel_MouseDown);
            sHandFill.MouseDown += new MouseButtonEventHandler(colorLabel_MouseDown);
            centerFill.MouseDown += new MouseButtonEventHandler(colorLabel_MouseDown);
            faceStrokesFill.MouseDown += new MouseButtonEventHandler(colorLabel_MouseDown);

            hHandStroke.MouseDown += new MouseButtonEventHandler(colorLabel_MouseDown);
            mHandStroke.MouseDown += new MouseButtonEventHandler(colorLabel_MouseDown);
            sHandStroke.MouseDown += new MouseButtonEventHandler(colorLabel_MouseDown);
            centerStroke.MouseDown += new MouseButtonEventHandler(colorLabel_MouseDown);
            faceStrokeStroke.MouseDown += new MouseButtonEventHandler(colorLabel_MouseDown);

            clock = new AnalogueClockComponent();            
        }

        void colorLabel_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Label l = sender as Label;
            BackgroundSetter bs = new BackgroundSetter();
            bs.Load(l.Background);
            l.Background = bs.GetContent();
            UpdateStyle();
        }

        public void Load(AClockStyle style)
        {
            loadedStyle = style;
            currentStyle = style;
            UpdateControls();
            ShowDialog();
        }

        private void UpdateControls()
        {
            hHandFill.Background = loadedStyle.GetFill(AClockStyle.ClockElement.HourHand);
            hHandStroke.Background = loadedStyle.GetStroke(AClockStyle.ClockElement.HourHand);
            
            mHandFill.Background = loadedStyle.GetFill(AClockStyle.ClockElement.Minuteshand);
            mHandStroke.Background = loadedStyle.GetStroke(AClockStyle.ClockElement.Minuteshand);

            sHandFill.Background = loadedStyle.GetFill(AClockStyle.ClockElement.SecondsHand);
            sHandStroke.Background = loadedStyle.GetStroke(AClockStyle.ClockElement.SecondsHand);

            centerFill.Background = loadedStyle.GetFill(AClockStyle.ClockElement.CenterCircle);
            centerStroke.Background = loadedStyle.GetStroke(AClockStyle.ClockElement.CenterCircle);

            faceStrokesFill.Background = loadedStyle.GetFill(AClockStyle.ClockElement.FaceStrokes);
            faceStrokeStroke.Background = loadedStyle.GetStroke(AClockStyle.ClockElement.FaceStrokes);

            centerStrokeThickness.Value = loadedStyle.GetStrokeThickness(AClockStyle.ClockElement.CenterCircle);
            hHandStrokeThickness.Value = loadedStyle.GetStrokeThickness(AClockStyle.ClockElement.HourHand);
            mHandStrokeThickness.Value = loadedStyle.GetStrokeThickness(AClockStyle.ClockElement.Minuteshand);
            sHandStrokeThickness.Value = loadedStyle.GetStrokeThickness(AClockStyle.ClockElement.SecondsHand);
            faceStrokeThickness.Value = loadedStyle.GetStrokeThickness(AClockStyle.ClockElement.FaceStrokes);
        }

        private void UpdateStyle()
        {
            currentStyle.SetFill(AClockStyle.ClockElement.HourHand, hHandFill.Background);
            currentStyle.SetFill(AClockStyle.ClockElement.Minuteshand, mHandFill.Background);
            currentStyle.SetFill(AClockStyle.ClockElement.SecondsHand, sHandFill.Background);
            currentStyle.SetFill(AClockStyle.ClockElement.CenterCircle, centerFill.Background);
            currentStyle.SetFill(AClockStyle.ClockElement.FaceStrokes, faceStrokesFill.Background);

            currentStyle.SetStroke(AClockStyle.ClockElement.HourHand, hHandStroke.Background);
            currentStyle.SetStroke(AClockStyle.ClockElement.Minuteshand, mHandStroke.Background);
            currentStyle.SetStroke(AClockStyle.ClockElement.SecondsHand, sHandStroke.Background);
            currentStyle.SetStroke(AClockStyle.ClockElement.CenterCircle, centerStroke.Background);
            currentStyle.SetStroke(AClockStyle.ClockElement.FaceStrokes, faceStrokeStroke.Background);

            currentStyle.SetStrokeThickness(AClockStyle.ClockElement.HourHand, hHandStrokeThickness.Value);
            currentStyle.SetStrokeThickness(AClockStyle.ClockElement.Minuteshand, mHandStrokeThickness.Value);
            currentStyle.SetStrokeThickness(AClockStyle.ClockElement.SecondsHand, sHandStrokeThickness.Value);
            currentStyle.SetStrokeThickness(AClockStyle.ClockElement.CenterCircle, centerStrokeThickness.Value);
            currentStyle.SetStrokeThickness(AClockStyle.ClockElement.FaceStrokes, faceStrokeThickness.Value);

            
        }

        public AClockStyle GetContent()
        {
            return loadedStyle;
        }

        private void okButton_Click(object sender, RoutedEventArgs e)
        {
            loadedStyle = currentStyle;
            Close();
        }

        private void applyButton_Click(object sender, RoutedEventArgs e)
        {
            loadedStyle = currentStyle;
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            previewCanvas.Children.Add(clock);
        }

        private void hHandStrokeThickness_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            currentStyle.SetStrokeThickness(AClockStyle.ClockElement.HourHand, e.NewValue);
            UpdateStyle();
        }

        private void mHandStrokeThickness_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            currentStyle.SetStrokeThickness(AClockStyle.ClockElement.Minuteshand, e.NewValue);
            UpdateStyle();
        }

        private void sHandStrokeThickness_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            currentStyle.SetStrokeThickness(AClockStyle.ClockElement.SecondsHand, e.NewValue);
            UpdateStyle();
        }

        private void centerStrokeThickness_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            currentStyle.SetStrokeThickness(AClockStyle.ClockElement.CenterCircle, e.NewValue);
            UpdateStyle();
        }

        private void faceStrokeThickness_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            currentStyle.SetStrokeThickness(AClockStyle.ClockElement.FaceStrokes, e.NewValue);
            UpdateStyle();
        }
    }
}
