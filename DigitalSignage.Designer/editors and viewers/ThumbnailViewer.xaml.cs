﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;
using System.Windows.Media.Converters;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Threading;
using System.ComponentModel;
using System.Threading;
using NLog;
using System.Configuration;
using System.Reflection;

namespace WPFDesigner
{
    /// <summary>
    /// Interaction logic for ThumbnailViewer.xaml
    /// </summary>
    public partial class ThumbnailViewer : Window
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        ExplorerThumb selectedThumb;
        public bool AllowClose;
        BackgroundWorker thumbWorker;
        string currentPath;

        public string SelectedProject
        {
            get
            {
                return projectPathBox.Text;
            }
        }

        public ThumbnailViewer()
        {
            InitializeComponent();
            showOnStartup.IsChecked = Properties.Settings.Default.ShowExplorerOnStartup;
            showOnStartup.Unchecked += new RoutedEventHandler(showOnStartup_Unchecked);
            showOnStartup.Checked +=new RoutedEventHandler(showOnStartup_Checked);
            AllowClose = false;
            directoriesTree.SelectedItemChanged += new RoutedPropertyChangedEventHandler<object>(directoriesTree_SelectedItemChanged);
            this.Closing += new System.ComponentModel.CancelEventHandler(ThumbnailViewer_Closing);
            this.PreviewKeyDown += new KeyEventHandler(ThumbnailViewer_PreviewKeyDown);
        }

        void ThumbnailViewer_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape && thumbWorker != null)
            {
                thumbWorker.CancelAsync();
            }
        }

        void ThumbnailViewer_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = !AllowClose;
        }

        void directoriesTree_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (directoriesTree.SelectedItem == null) { return; }
            if (((DirectoryViewItem)directoriesTree.SelectedItem).fullPath == currentPath) { return; }
            if (thumbWorker != null) { thumbWorker.Dispose(); }

            FillItem((DirectoryViewItem)directoriesTree.SelectedItem);
            UpdateThumbnailsDisplay(((DirectoryViewItem)directoriesTree.SelectedItem).fullPath);
            currentPath = ((DirectoryViewItem)directoriesTree.SelectedItem).fullPath;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {

        }

        /// <summary>
        /// Shows the Projects Explorer
        /// </summary>
        public void ShowExplorer()
        {
            InitTreeView();
            this.ShowDialog();
        }

        private void InitTreeView()
        {
            directoriesTree.Items.Clear();
            foreach (string drive in Directory.GetLogicalDrives())
            {
                DirectoryViewItem it = CreateNodeFromString(drive, drive);
                if (!directoriesTree.Items.Contains(it))
                    directoriesTree.Items.Add(CreateNodeFromString(drive, drive));
            }
            OpenProjectsFolder();
        }

        private DirectoryViewItem CreateNodeFromString(string header, string fullPath)
        {
            DirectoryViewItem item = new DirectoryViewItem();
            item.Foreground = new SolidColorBrush(Color.FromArgb(255, 255, 255, 255));
            item.Header = header;
            item.fullPath = fullPath;
            item.IsExpanded = true;
            return item;
        }

        private void FillItem(DirectoryViewItem item)
        {
            if (item == null)
                return;
            item.Items.Clear();
            try
            {
                foreach (string path in Directory.GetDirectories(item.fullPath, "*.*", SearchOption.TopDirectoryOnly))
                {
                    string header = System.IO.Path.GetFileName(path);
                    if (!item.Items.Contains(CreateNodeFromString(header, path)))
                        item.Items.Add(CreateNodeFromString(header, path));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Unable to read directory", "Reason: " + ex.Message);
            }
        }

        #region Generating thumbnails
        private void UpdateThumbnailsDisplay(string path)
        {
            UpdateProjectsCount(0);
            string[] folders;
            thumbsPanel.Children.Clear();

            thumbWorker = new BackgroundWorker();
            thumbWorker.WorkerReportsProgress = true;
            thumbWorker.WorkerSupportsCancellation = true;

            thumbWorker.ProgressChanged += delegate(object w, ProgressChangedEventArgs e)
            {
                ExplorerThumb th = GetThumbnail((ExplorerItemInfo)e.UserState);
                if (thumbsPanel.Children.OfType<ExplorerThumb>().Count<ExplorerThumb>(el => el.projectPath == th.projectPath) == 0)
                {
                    IncrementProjectsCount();
                    thumbsPanel.Children.Add(th);
                }
            };

            thumbWorker.RunWorkerCompleted += delegate(object w, RunWorkerCompletedEventArgs e)
            {
                if (thumbsPanel.Children.Count > 0)
                {
                    selectedThumb = thumbsPanel.Children[0] as ExplorerThumb;
                    selectedThumb.Select();
                    if (selectedThumb != null && thumbsPanel.Children.Contains(selectedThumb))
                    {
                        projectPathBox.Text = selectedThumb.projectPath;
                    }
                    else
                    {
                        projectPathBox.Text = "";
                    }
                    UpdateThumbsDisplay();
                }
            };

            thumbWorker.DoWork += delegate(object w, DoWorkEventArgs e)
            {
                BackgroundWorker worker = w as BackgroundWorker;
                string[] Folders = e.Argument as string[], settingsFilesList;
                string settingsFilePath;

                foreach (string folder in Folders)
                {
                    try
                    {
                        settingsFilesList = Directory.GetFiles(folder, "*.settings");
                        if (worker.CancellationPending)
                        {
                            e.Cancel = true;
                            return;
                        }
                        if (settingsFilesList.Count() == 1)
                        {
                            settingsFilePath = settingsFilesList[0];
                            FileStream fs = new FileStream(settingsFilePath, FileMode.Open, FileAccess.Read);
                            BinaryFormatter bf = new BinaryFormatter();
                            object[] projectInfo = new object[2];
                            projectInfo = (object[])bf.Deserialize(fs);
                            fs.Close();
                            ExplorerItemInfo info = new ExplorerItemInfo((byte[])projectInfo[1], settingsFilePath);
                            worker.ReportProgress(0, info);
                            Thread.Sleep(200);
                        }
                    }
                    catch (Exception ex)
                    {
                        logger.Error(ex + "");
                        break;
                    }
                }
            };

            try
            {
                folders = Directory.GetDirectories(path, "*.*", SearchOption.TopDirectoryOnly);
                thumbWorker.RunWorkerAsync(folders);
            }
            catch (Exception ex)
            {
                logger.Error(ex + "");
            }
        }
        #endregion

        private void UpdateProjectsCount(int value)
        {
            projectsCount.Text = value.ToString();
        }

        private void UpdateThumbsDisplay()
        {
            for (int i = 0; i < thumbsPanel.Children.Count; )
            {
                if (!((ExplorerThumb)thumbsPanel.Children[i]).projectPath.Contains(currentPath))
                {
                    thumbsPanel.Children.RemoveAt(i);
                }
                else i++;
            }
            UpdateProjectsCount(thumbsPanel.Children.Count);
        }

        private void IncrementProjectsCount()
        {
            projectsCount.Text = (Int32.Parse(projectsCount.Text) + 1).ToString();
        }

        private ExplorerThumb GetThumbnail(ExplorerItemInfo info)
        {
            ExplorerThumb thumb = new ExplorerThumb(info._brushBytes, info._path);
            thumb.PreviewMouseDown += new MouseButtonEventHandler(thumb_PreviewMouseDown);
            return thumb;
        }

        void thumb_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (selectedThumb == null)
                selectedThumb = thumbsPanel.Children[0] as ExplorerThumb;
            else
            {
                selectedThumb.Deselect();
                selectedThumb = sender as ExplorerThumb;
                selectedThumb.Select();
                projectPathBox.Text = selectedThumb.projectPath;
            }
        }

        private void openProjectsFolder_Click(object sender, RoutedEventArgs e)
        {
            OpenProjectsFolder();
        }

        private void OpenProjectsFolder()
        {
			String sExecutingPath = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

			string path = sExecutingPath + "\\DesignerData\\Screens";
            string[] nodes = path.Split('\\');

            if (!Directory.Exists(path))
            {
                try
                {
                    Directory.CreateDirectory(path);
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                    MessageBox.Show("Unable to create Projects directory. Check permissions.");
                    return;
                }
            }

            DirectoryViewItem it = directoriesTree.Items.OfType<DirectoryViewItem>().Single(el => el.fullPath == nodes[0] + "\\");
            it.IsSelected = true;
            it.IsExpanded = true;

            for (int i = 1; i < nodes.Count(); i++)
            {
                ((DirectoryViewItem)directoriesTree.SelectedItem).Items.OfType<DirectoryViewItem>().Single(el => (string)el.Header == nodes[i]).IsSelected = true;
            }
            UpdateThumbnailsDisplay(path);
        }

        private void openProject_Click(object sender, RoutedEventArgs e)
        {
            if (selectedThumb == null)
            {
                MessageBox.Show("No thumb was selected");
                return;
            }
            if (thumbWorker != null)
            {
                thumbWorker.CancelAsync();
                thumbWorker.Dispose();
            }
            this.Hide();
        }

        private void close_Click(object sender, RoutedEventArgs e)
        {
            if (thumbWorker != null)
            {
                thumbWorker.CancelAsync();
                thumbWorker.Dispose();
            }
            projectPathBox.Text = "";
            this.Hide();
        }

        private void showOnStartup_Checked(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.ShowExplorerOnStartup = true;
            Properties.Settings.Default.Save();
        }

        void showOnStartup_Unchecked(object sender, RoutedEventArgs e)
        {
            Properties.Settings.Default.ShowExplorerOnStartup = false;
            Properties.Settings.Default.Save();
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
			String sExecutingPath = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
			string path = sExecutingPath + "\\DesignerData\\Templates";
            string[] nodes = path.Split('\\');

            if (!Directory.Exists(path))
            {
                try
                {
                    Directory.CreateDirectory(path);
                }
                catch (Exception ex)
                {
                    logger.Error(ex);
                    MessageBox.Show("Unable to create Templates directory. Check permissions.");
                    return;
                }
            }

            DirectoryViewItem it = directoriesTree.Items.OfType<DirectoryViewItem>().Single(el => el.fullPath == nodes[0] + "\\");
            it.IsSelected = true;
            it.IsExpanded = true;

            for (int i = 1; i < nodes.Count(); i++)
            {
                ((DirectoryViewItem)directoriesTree.SelectedItem).Items.OfType<DirectoryViewItem>().Single(el => (string)el.Header == nodes[i]).IsSelected = true;
            }
            UpdateThumbnailsDisplay(path);
        }
    }

    public class ExplorerItemInfo
    {
        public byte[] _brushBytes;
        public string _path;
        
        public ExplorerItemInfo(byte[] brushBytes, string path)
        {
            _brushBytes = brushBytes;
            _path = path;
        }
    }

    public class DirectoryViewItem : TreeViewItem
    {
        public string fullPath;
    }
}