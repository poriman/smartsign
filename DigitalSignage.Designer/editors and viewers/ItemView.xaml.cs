﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml;
using System.IO;
using System.Windows.Markup;
using System.Windows.Media.Effects;
using QuartzTypeLib;
using WMPLib;
using WPFDesigner.tools;

namespace WPFDesigner
{
    /// <summary>
    /// Represents a visual display of PlaylistItem
    /// </summary>
    public partial class ItemView : UserControl
    {
        #region Editors
        System.Windows.Forms.OpenFileDialog _openDialog;

//        bool isSelected;

        TimeSpan duration;

        TextEditor _textEditor;

        UrlSetter _urlEditor;

        GmtSetter _gmtSetter;

        ScrollTextSetter _stSetter;
        #endregion

        /// <summary>
        /// Playlist item
        /// </summary>
        private PlaylistItem _item;

        /// <summary>
        /// Represents the file manager
        /// </summary>
        private IMediaFilesManager FileManager;

        private PlaylistEditor _parent;

        /// <summary>
        /// The type of element
        /// </summary>
        private Type _type;

        /// <summary>
        /// The TimeSpan
        /// </summary>
        private TimeSpan _durationTimeSpan;
        /// <summary>
        /// Initializes the new ItemView class instance
        /// </summary>
        /// <param name="item">PlaylistItem to get the view of</param>
		/// <param name="elementType">The type of element</param>
		/// <param name="parent">PlaylistEditor object</param>
		public ItemView(PlaylistItem item, Type elementType, PlaylistEditor parent)
        {
            InitializeComponent();
            _item = item;
            _type = elementType;
            _durationTimeSpan = _item.DurationAsTimeSpan;
            _openDialog = new System.Windows.Forms.OpenFileDialog();
            FileManager = ProjectManager.GetCurrentProjectManager().Pane as IMediaFilesManager;
            Fill();
            _parent = parent;            
        }

		public ItemView(string content, PlaylistItem item, Type elementType, PlaylistEditor parent)
		{
			InitializeComponent();
			_item = item;
			_type = elementType;
			_durationTimeSpan = _item.DurationAsTimeSpan;
			_openDialog = new System.Windows.Forms.OpenFileDialog();
			FileManager = ProjectManager.GetCurrentProjectManager().Pane as IMediaFilesManager;
			Fill();
			_parent = parent;

			contentLabel.Content = _item.Content = content;
		}

        void changeContentRect_MouseDown(object sender, MouseButtonEventArgs e)
        {
            try
            {
                switch (_type.Name)
                {
                    case "AnalogueClockComponent":
                    case "DigitalClockComponent":
                        {
                            _gmtSetter = new GmtSetter();
                            _gmtSetter.Load(_item.Content);
                            _item.Content = _gmtSetter.GetContent();
                            contentLabel.Content = GmtManager.GetTimeZoneFromIndexInString(_item.Content);
                            break;
                        }
                    case "DateComponent":
                        {
                            break;
                        }
                    case "EllipseComponent":
                        {
                            break;
                        }
                    case "ImageComponent":
                        {
                            _openDialog.ShowDialog();
                            if (_openDialog.FileName != "" && File.Exists(_openDialog.FileName))
                            {
                                _item.Content = FileManager.AddNewFileWithModifiedName(_openDialog.FileName);
                                contentLabel.Content = _item.Content;
                            }
                            break;
                        }
                    case "QRCodeComponent":
                        {
                            //_openDialog.ShowDialog();
                            //if (_openDialog.FileName != "" && File.Exists(_openDialog.FileName))
                            //{
                            //    _item.Content = FileManager.AddNewFileWithModifiedName(_openDialog.FileName);
                            //    contentLabel.Content = _item.Content;
                            //}
                            break;
                        }
                    case "AudioComponent":
                        {
                            string filter = "MP3|*.mp3|WMA|*.wma|ogg|*.ogg";
                            OpenFileDialog("Open Audio", filter);

                            Fill();
                            break;
                        }
                    case "MediaComponent":
                        {
                            string filter = "Video (WMV, AVI, MPG/MPEG)| *.wmv; *.avi; *.mpg;*.mpeg";
                            OpenFileDialog("Open Video", filter);
                            Fill();
                            break;
                        }
                    case "RectangleComponent":
                        {
                            break;
                        }
                    case "RssComponent":
                    case "ScrollTextComponent":
                        {
                            _stSetter = new ScrollTextSetter();
                            _stSetter.Load(ScrollTextInfoManager.DeserializeScrollTextInfo(_item.Content));
                            contentLabel.Content = _stSetter.GetContent();
                            if (contentLabel.Content == new ScrollTextInfo())
                            {
                                contentLabel.Content = "No media";
                                _item.Content = "";
                            }
                            _item.Content = ScrollTextInfoManager.SerializeScrollTextInfo((ScrollTextInfo)contentLabel.Content);

                            break;
                        }
                    case "TextComponent":
                        {
                            _textEditor = new TextEditor();
                            _textEditor.Load(_item.Content);
                            _item.Content = _textEditor.GetDocument();
                            contentLabel.Content = "Text";
                            break;
                        }
                    case "WebComponent":
                        {
                            _urlEditor = new UrlSetter();
                            _urlEditor.Load(_item.Content);
                            _item.Content = _urlEditor.GetContent().ToString();
                            contentLabel.Content = _item.Content;
                            break;
                        }
                    case "QuickTimeComponent":
                        {
                            string filter = "MOV|*.mov;*.HDMov";
                            OpenFileDialog("Open QuickTime", filter);
                            Fill();
                            break;
                        }
                    case "FlashComponent":
                        {
                            string filter = "SWF|*.swf";
                            OpenFileDialog("Open Flash", filter);

                            Fill();
                            break;
                        }
                    case "PptComponent":
                        {
                            _openDialog.Filter = "PowerPoint|*.ppt";
                            _openDialog.ShowDialog();
                            if (_openDialog.FileName != "" && File.Exists(_openDialog.FileName))
                            {
                                _item.Content = FileManager.AddNewFileWithModifiedName(_openDialog.FileName);
                                contentLabel.Content = _item.Content;
                            }
                            break;
                        }
                    default: break;
                }
            }
            catch (Exception ex)
            {
                string errmsg = ex.Message.ToString();
            }
        }

        private void OpenFileDialog(string title, string filter)
        {
            System.Windows.Forms.OpenFileDialog OpenFileDialog = new System.Windows.Forms.OpenFileDialog();
            OpenFileDialog.Filter = filter;// 
            OpenFileDialog.Title = title;
            if (OpenFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string s = ((IMediaFilesManager)ProjectManager.GetCurrentProjectManager().Pane).AddNewFileWithModifiedName(OpenFileDialog.FileName);
                
                if (System.IO.Path.GetExtension(s).ToLower() == ".wmv")
                {
                    try
                    {
                        WindowsMediaPlayerClass wmp = new WindowsMediaPlayerClass();
                        IWMPMedia media = wmp.newMedia(OpenFileDialog.FileName);
                        duration = new TimeSpan(0, 0, Convert.ToInt32(media.duration));
                        wmp.close();
                        wmp = null;
                    }
                    catch
                    {
                    }
                }
                else if (System.IO.Path.GetExtension(s).ToLower() == ".mp3")
                {
                    try
                    {
                        MP3Header header = new MP3Header();
                        header.ReadMP3Information(OpenFileDialog.FileName);
                        duration = new TimeSpan(0, 0, header.intLength);
                    }
                    catch { }
                }
                else
                {
                    try
                    {
                        FilgraphManager filterGraph = new FilgraphManager();
                        filterGraph.RenderFile(OpenFileDialog.FileName);
                        IMediaPosition pos = filterGraph as IMediaPosition;
                        duration = new TimeSpan(0, 0, (int)pos.Duration);
                    }
                    catch { }
                    try
                    {
                        DigitalSignage.Controls.QuickTimeControl qc = new DigitalSignage.Controls.QuickTimeControl();
                        duration = qc.GetQuickTimeMovieDuration(OpenFileDialog.FileName);
                        qc.Dispose();
                        qc = null;
                    }
                    catch { }
                }

                _item.Content = s;
                _item.Duration = GetDuration(duration);
                _durationTimeSpan = _item.DurationAsTimeSpan;
            }
        }

        public string GetDuration(TimeSpan duration)
        {
            if (duration != null)
                return XamlWriter.Save(duration);
            else
                return XamlWriter.Save(new TimeSpan());
        }

        /// <summary>
        /// Fills the ItemView fields corresponding to the loaded PlaylistItem instance
        /// </summary>
        private void Fill()
        {
            daysBox.ValueText = _durationTimeSpan.Days.ToString();
            hoursBox.ValueText = _durationTimeSpan.Hours.ToString();
            minutesBox.ValueText = _durationTimeSpan.Minutes.ToString();
            secondsBox.ValueText = _durationTimeSpan.Seconds.ToString();

            switch (_type.Name)
            {
                case "AnalogueClockComponent":
                case "DigitalClockComponent":
                    {
                        if (_item.Content != "")
                        {
                            contentLabel.Content = GmtManager.GetTimeZoneFromIndexInString(_item.Content);
                        }
                        else
                        {
                            contentLabel.Content = "No media";
                        }
                        break;
                    }
                case "DateComponent":
                    {
                        break;
                    }
                case "EllipseComponent":
                    {
                        break;
                    }
                case "QRCodeComponent":
                    {
                    }
                    break;
                case "ImageComponent":
                    {
                        if (_item.Content != "")
                        {
                            contentLabel.Content = _item.Content;
                        }
                        else
                        {
                            contentLabel.Content = "No media";
                        }
                       // string images = "*.PNG;*.JPG;*.GIF;*.BMP;*.JPEG;";
                       // _openDialog.Filter = string.Format("Image files({0})|{0}", images);

						_openDialog.Filter = "All supported|*.jpg;*.bmp;*.gif;*.png|JPEG files|*.jpg|BMP files| *.bmp|GIF files|*.gif|PNG files|*.png";
						_openDialog.Title = "Open an image";
                        break;
                    }
                case "AudioComponent":
                case "FlashComponent":
                case "QuickTimeComponent":
                case "MediaComponent":
                    {
                        if (_item.Content != "")
                        {
                            contentLabel.Content = _item.Content;
                        }
                        else
                        {
                            contentLabel.Content = "No media";
                        }
                        break;
                    }
                case "RectangleComponent":
                    {
                        if (_item.Content != "")
                        {
                            contentLabel.Content = _item.Content;
                        }
                        else
                        {
                            contentLabel.Content = "No media";
                        }
                        break;
                    }
                case "RssComponent":
                case "ScrollTextComponent":
                    {
                        if (_item.Content != "")
                        {
                            contentLabel.Content = ScrollTextInfoManager.DeserializeScrollTextInfo(_item.Content);
                        }
                        else
                        {
                            contentLabel.Content = "No media";
                        }
                        break;
                    }
                case "TextComponent":
                    {
                        if (_item.Content != "")
                        {
                            contentLabel.Content = "Text";
                        }
                        else
                        {
                            contentLabel.Content = "No media";
                        }
                        break;
                    }
                case "WebComponent":
                    {
                        if (_item.Content != "")
                        {
                            contentLabel.Content = _item.Content;
                        }
                        else
                        {
                            contentLabel.Content = "about:blank";
                            _item.Content = "about:blank";
                        }
                        break;
                    }
                case "PptComponent":
                    {
                        if (_item.Content != "")
                        {
                            contentLabel.Content = _item.Content;
                        }
                        else
                        {
                            contentLabel.Content = "No media";
                            _item.Content = "No media";
                        }
                        break;
                    }
                default: break;
            }
        }

        /// <summary>
        /// Returns the PlaylistItem instance that corresponds to the content of this ItemView control
        /// </summary>
        /// <returns>PlaylistItem instance</returns>
        public PlaylistItem GetPlaylistItem()
        {
            CheckDurationBoxes();
            _item.Duration = XamlWriter.Save(new TimeSpan(Convert.ToInt32(daysBox.ValueText), Convert.ToInt32(hoursBox.ValueText), Convert.ToInt32(minutesBox.ValueText), Convert.ToInt32(secondsBox.ValueText)));
            return _item;
        }

        /// <summary>
        /// Checks the duration setup text boxes for validity
        /// </summary>
        private void CheckDurationBoxes()
        {
            int check = 0;
            if (!Int32.TryParse(daysBox.ValueText, out check))
            {
                daysBox.ValueText = "0";
            }
            if (!Int32.TryParse(hoursBox.ValueText, out check))
            {
                hoursBox.ValueText = "0";
            }
            if (!Int32.TryParse(minutesBox.ValueText, out check))
            {
                minutesBox.ValueText = "0";
            }
            if (!Int32.TryParse(secondsBox.ValueText, out check))
            {
                secondsBox.ValueText = "0";
            }
        }

        private void Border_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            DropShadowBitmapEffect effect = new DropShadowBitmapEffect();
            effect.Color = Color.FromArgb(0, 0, 255, 0);
            effect.Direction = 500;
            effect.ShadowDepth = 2;
            Mainborder.BitmapEffect = effect;

          //  _parent._selectedItem = this;
          //  _parent.SelectItemChange(this);
        }

        public void SelectChange()
        {
            Mainborder.BitmapEffect = null;
        }

        private void rectangle1_MouseDown(object sender, MouseButtonEventArgs e)
        {
            daysBox.ValueText = "0";
            hoursBox.ValueText = "0";
            minutesBox.ValueText = "0";
            secondsBox.ValueText = "0";
        }
    }
}