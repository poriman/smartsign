﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Net;
using DigitalSignage.Controls;
using NLog;

namespace WPFDesigner
{
    /// <summary>
    /// Interaction logic for UrlSetter.xaml
    /// </summary>
    public partial class UrlSetter : Window
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        UriBuilder builder;
        WebControl previewFrame;
        string loadedContent;

        public UrlSetter()
        {
            InitializeComponent();
            previewFrame = new WebControl();
            host.Child = previewFrame;
            this.PreviewKeyDown += new KeyEventHandler(UrlSetter_PreviewKeyDown);
            adressTextBox.PreviewKeyDown += new KeyEventHandler(adressTextBox_PreviewKeyDown);
        }

        void adressTextBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                try
                {
					if (rbScript.IsChecked == false)
					{
						builder = new UriBuilder(adressTextBox.Text);
						previewFrame.Navigate(builder.Uri);
						e.Handled = true;
					}
                }
                catch (Exception ex)
                {
                    adressTextBox.Text = "";
                    logger.Error(ex + "");
                }
            }
        }

        void UrlSetter_PreviewKeyDown(object sender, KeyEventArgs e)
        {
        }

        public void Load(string content)
        {
            try
            {
				if (content.StartsWith("Address://"))
				{
					rbAddress.IsChecked = true;

					content = content.Substring(10);

					builder = new UriBuilder(content);
					adressTextBox.Text = builder.Uri.AbsoluteUri;
					loadedContent = content;
				}
				else if (content.StartsWith("File://"))
				{
					rbFile.IsChecked = true;

					adressTextBox.Text = loadedContent = content.Substring(7);
				}
				else if (content.StartsWith("Script://"))
				{
					rbScript.IsChecked = true;

					adressTextBox.Text = loadedContent = content.Substring(9);
				}
				else
				{
					rbAddress.IsChecked = true;

					builder = new UriBuilder(content);
					adressTextBox.Text = builder.Uri.AbsoluteUri;
					loadedContent = content;
				}
            }
            catch (Exception ex)
            {
				rbAddress.IsChecked = true;

                builder = new UriBuilder();
                logger.Error(ex + "");
            }
            this.ShowDialog();
        }

        private void Close_Click(object sender, RoutedEventArgs e)
        {
            previewFrame.Navigate(new Uri("about:blank"));
            previewFrame = null;
            this.Close();
        }

        private void navigateButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
				if(rbAddress.IsChecked == true)
				{
					builder = new UriBuilder(adressTextBox.Text);
					previewFrame.Navigate(builder.Uri);

				}
				else if(rbFile.IsChecked == true)
				{
					IMediaFilesManager FileManager = ProjectManager.GetCurrentProjectManager().Pane as IMediaFilesManager;
					builder = new UriBuilder(FileManager.GetFullFilePathByName(adressTextBox.Text));
					previewFrame.Navigate(builder.Uri);
				}
				else
				{
					previewFrame.NavigateScript(adressTextBox.Text);
				}
            }
            catch (Exception ex)
            {
                adressTextBox.Text = "";
                logger.Error(ex + "");
            }
        }

        private void stopButton_Click(object sender, RoutedEventArgs e)
        {
            
        }

        public object GetContent()
        {
			if (rbAddress.IsChecked == true)
			{
				return String.Format("Address://{0}", loadedContent);
			}
			else if (rbFile.IsChecked == true)
			{
				return String.Format("File://{0}", loadedContent);
			}
			else
			{
				return String.Format("Script://{0}", loadedContent);
			}
        }

        private void refreshButton_Click(object sender, RoutedEventArgs e)
        {
        }

        private void okButton_Click(object sender, RoutedEventArgs e)
        {
            loadedContent = adressTextBox.Text;
            previewFrame.Navigate(new Uri("about:blank"));
// 			previewFrame.Dispose();
//             previewFrame = null;
            this.DialogResult = true;

            Close();
        }

        private void applyButton_Click(object sender, RoutedEventArgs e)
        {
            loadedContent = adressTextBox.Text;

            this.DialogResult = true;


        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            previewFrame.Navigate(new Uri("about:blank"));
// 			previewFrame.Dispose();
// 			previewFrame = null;

            this.DialogResult = false;

            Close();
        }

		private void tb_GotKeyboardFocus(object sender, KeyboardFocusChangedEventArgs e)
		{
			try
			{
				TextBox tb = sender as TextBox;
				tb.SelectAll();
				e.Handled = true;
			}
			catch { }
		}


		private void rbAddress_Checked(object sender, RoutedEventArgs e)
		{
			try
			{
				lbUrl.Content = String.Format("{0}:", Properties.Resources.labelAddress);
				adressTextBox.Text = String.Empty;
				adressTextBox.Width = 400;
				rdTextBox.Height = new GridLength(32);
				adressTextBox.Height = 25;
				adressTextBox.VerticalScrollBarVisibility = ScrollBarVisibility.Hidden;
				adressTextBox.AcceptsReturn = false;
				adressTextBox.AcceptsTab = false;
				adressTextBox.TextWrapping = TextWrapping.NoWrap;
				adressTextBox.IsReadOnly = false;
				adressTextBox.VerticalContentAlignment = System.Windows.VerticalAlignment.Center;
			}
			catch { }
		}

		private void rbScript_Checked(object sender, RoutedEventArgs e)
		{
			try
			{
				lbUrl.Content = String.Format("{0}:", Properties.Resources.labelScript);
				adressTextBox.Text = String.Empty;
				adressTextBox.Width = 400;
				rdTextBox.Height = new GridLength(107);
				adressTextBox.Height = 100;
				adressTextBox.MaxLines = 1000;
				adressTextBox.VerticalScrollBarVisibility = ScrollBarVisibility.Auto;
				adressTextBox.AcceptsReturn = true;
				adressTextBox.AcceptsTab = true;
				adressTextBox.TextWrapping = TextWrapping.WrapWithOverflow;
				adressTextBox.IsReadOnly = false;
				adressTextBox.VerticalContentAlignment = System.Windows.VerticalAlignment.Top;
			}
			catch { }
		}

		private void rbFile_Click(object sender, RoutedEventArgs e)
		{
			try
			{
				lbUrl.Content = String.Format("{0}:", Properties.Resources.labelFile);
				adressTextBox.Text = String.Empty;

				adressTextBox.Width = 400;
				rdTextBox.Height = new GridLength(32);
				adressTextBox.Height = 25;
				adressTextBox.VerticalScrollBarVisibility = ScrollBarVisibility.Hidden;
				adressTextBox.AcceptsReturn = false;
				adressTextBox.AcceptsTab = false;
				adressTextBox.TextWrapping = TextWrapping.NoWrap;
				adressTextBox.IsReadOnly = true;
				adressTextBox.VerticalContentAlignment = System.Windows.VerticalAlignment.Center;
			}
			catch { }

			System.Windows.Forms.OpenFileDialog dlg = new System.Windows.Forms.OpenFileDialog();
			dlg.Filter = "All supported|*.html;*.htm;*.mht;*.mhtml";
			if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
			{
				IMediaFilesManager FileManager = ProjectManager.GetCurrentProjectManager().Pane as IMediaFilesManager;
				adressTextBox.Text = FileManager.AddNewFileWithModifiedName(dlg.FileName);
			}
		}
    }
}