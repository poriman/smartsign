﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Xml;
using System.IO;
using System.Windows.Markup;

namespace WPFDesigner
{
    /// <summary>
    /// Interaction logic for GmtSetter.xaml
    /// </summary>
    public partial class GmtSetter : Window
    {
        string loadedTimeZone;
        List<TimeZoneInfo> timeZones;

        public GmtSetter()
        {
            InitializeComponent();
            timeZones = new List<TimeZoneInfo>(TimeZoneInfo.GetSystemTimeZones());
            
			TimeZoneInfo info = TimeZoneInfo.Local;

			foreach (TimeZoneInfo timezone in timeZones)
            {
                timezoneBox.Items.Add(timezone);
				if(timezone.Id == info.Id)
				{
					timezoneBox.SelectedItem = timezone;
				}
			}
        }

        public void Load(string timeZone)
        {
            loadedTimeZone = timeZone;
			TimeZoneInfo info = GmtManager.GetTimeZoneFromIndexInString(timeZone);
			if (info != null) timezoneBox.SelectedItem = info;

            this.ShowDialog();
        }

        private void bOk_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;

            loadedTimeZone = GmtManager.GetStringIndexOfTimeZone(timezoneBox.SelectedItem as TimeZoneInfo);
            this.Close();
        }

        public string GetContent()
        {
            return loadedTimeZone;
        }

        private void bCancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }

        private void bApply_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;

            loadedTimeZone = GmtManager.GetStringIndexOfTimeZone(timezoneBox.SelectedItem as TimeZoneInfo);
        }
    }
}
