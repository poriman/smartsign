﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows;
using System.Windows.Controls;

namespace WPFDesigner
{
    public partial class QuicktimeHost : Form
    {
        private IDesignElement _element;

        public QuicktimeHost(IDesignElement quickTimeComponent)
        {
            _element = quickTimeComponent;
            InitializeComponent();
            Width = Convert.ToInt32(_element.Width);
            Height = Convert.ToInt32(_element.Height);
            Opacity = _element.Opacity;
            System.Windows.Point location = ((UIElement)_element).TranslatePoint(new System.Windows.Point(0 + Convert.ToInt32(System.Windows.Application.Current.MainWindow.Left), 0 + Convert.ToInt32(System.Windows.Application.Current.MainWindow.Top)), System.Windows.Application.Current.MainWindow);
            Left = Convert.ToInt32(location.X);
            quickTime.BackColor = Color.FromArgb(0, 0, 0, 0);
            Top = Convert.ToInt32(location.Y) + 26;
            Show();
            System.Windows.Application.Current.MainWindow.Activate();
        }

        /// <summary>
        /// Starts playing mov file located under the specified path
        /// </summary>
        /// <param name="moviePath">Swf file path</param>
        public void Play(string moviePath)
        {
            quickTime.Play(moviePath);
        }

        /// <summary>
        /// Stops mov file playback
        /// </summary>
        public void Stop()
        {
            quickTime.Stop();
        }
    }
}
