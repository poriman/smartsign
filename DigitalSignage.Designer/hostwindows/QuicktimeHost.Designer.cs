﻿namespace WPFDesigner
{
    partial class QuicktimeHost
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.quickTime = new DigitalSignage.Controls.QuickTimeControl();
            this.SuspendLayout();
            // 
            // quickTime
            // 
            this.quickTime.Dock = System.Windows.Forms.DockStyle.Fill;
            this.quickTime.Location = new System.Drawing.Point(0, 0);
            this.quickTime.Name = "quickTime";
            this.quickTime.Size = new System.Drawing.Size(292, 266);
            this.quickTime.TabIndex = 0;
            // 
            // QuicktimeHost
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlText;
            this.ClientSize = new System.Drawing.Size(292, 266);
            this.Controls.Add(this.quickTime);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "QuicktimeHost";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.TransparencyKey = System.Drawing.SystemColors.ControlText;
            this.ResumeLayout(false);

        }

        #endregion

        private DigitalSignage.Controls.QuickTimeControl quickTime;
    }
}