﻿namespace WPFDesigner
{
    partial class FlashHost
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.flash = new DigitalSignage.Controls.FlashControl();
			this.SuspendLayout();
			// 
			// flash
			// 
			this.flash.BackColor = System.Drawing.Color.MediumAquamarine;
			this.flash.Dock = System.Windows.Forms.DockStyle.Fill;
			this.flash.ImeMode = System.Windows.Forms.ImeMode.NoControl;
			this.flash.Location = new System.Drawing.Point(0, 0);
			this.flash.Margin = new System.Windows.Forms.Padding(0);
			this.flash.Name = "flash";
			this.flash.Size = new System.Drawing.Size(233, 184);
			this.flash.TabIndex = 0;
			// 
			// FlashHost
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.SystemColors.ControlText;
			this.ClientSize = new System.Drawing.Size(233, 184);
			this.ControlBox = false;
			this.Controls.Add(this.flash);
			this.DoubleBuffered = true;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Name = "FlashHost";
			this.ShowIcon = false;
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			this.TopMost = true;
			this.ResumeLayout(false);

        }

        #endregion

        private DigitalSignage.Controls.FlashControl flash;
    }
}