﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows.Controls;
using System.Windows;

namespace WPFDesigner
{
    public partial class FlashHost : Form
    {
        private IDesignElement _element;

        /// <summary>
        /// Initializes the new FlashHost
        /// </summary>
        /// <param name="flashComponent"></param>
        public FlashHost(IDesignElement flashComponent)
        {
            _element = flashComponent;

            InitializeComponent();

            Width = Convert.ToInt32(_element.Width);
            Height = Convert.ToInt32(_element.Height);

            Opacity = _element.Opacity;
// 			System.Windows.Point location = ((UIElement)_element).TranslatePoint(new System.Windows.Point(0 + Convert.ToInt32(System.Windows.Application.Current.MainWindow.Left), 0 + Convert.ToInt32(System.Windows.Application.Current.MainWindow.Top)), System.Windows.Application.Current.MainWindow);
			System.Windows.Point location = ((UIElement)_element).PointToScreen(new System.Windows.Point(0, 0));
			Left = Convert.ToInt32(location.X);

// 			SetStyle(ControlStyles.SupportsTransparentBackColor, true);
// 			BackColor = Color.Transparent;

			Top = Convert.ToInt32(location.Y);
            Show();
            System.Windows.Application.Current.MainWindow.Activate();
        }
        
        /// <summary>
        /// Starts playing swf file located under the specified path
        /// </summary>
        /// <param name="moviePath">Swf file path</param>
        public void Play(string moviePath)
        {
            flash.Play(moviePath);
        }

        /// <summary>
        /// Stops swf file playback
        /// </summary>
        public void Stop()
        {
            flash.Stop();
        }

// 		protected override CreateParams CreateParams
// 		{
// 			get
// 			{
// 				CreateParams cp = base.CreateParams;
// 				cp.ExStyle |= 0x00000020;
// 				return cp;
// 			}
// 		}
    }
}
