﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.Threading;
using System.Windows.Threading;
using DigitalSignage.Controls;
using System.Windows.Media.Animation;
using System.Globalization;
using System.ComponentModel;
using System.Diagnostics;
using System.Configuration;
using NLog;
using WPFDesigner.Cultures;
using System.Reflection;
using System.Xml;
using Microsoft.Windows.Controls.Ribbon;
using DigitalSignage.Common;

using WinInterop = System.Windows.Interop;
using System.Runtime.InteropServices;
using System.Collections.ObjectModel;

namespace WPFDesigner
{
    /// <summary>
    /// Represents the main operating designer window
    /// </summary>
    public partial class DesignerWindow : RibbonWindow, IProjectManagerContainer
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private List<UIElement> _selection = new List<UIElement>();
        UIElement UiE;
		public ProjectManager manager;
		PlayerControl playerControl = null;
        NewProjectWindow newProjectControl = null;
        bool isPressed = false ;
        string currentPressedName = String.Empty;
        System.Windows.Forms.OpenFileDialog openProjectDialog = null;
        System.Windows.Forms.SaveFileDialog saveProjectDialog = null;

        double scrollOffset = 0;
        Image dragImage = null;
        bool IsDropCancelled = false;

		#region WINAPI
		/// <summary>
		/// POINT aka POINTAPI
		/// </summary>
		[StructLayout(LayoutKind.Sequential)]
		public struct POINT
		{
			/// <summary>
			/// x coordinate of point.
			/// </summary>
			public int x;
			/// <summary>
			/// y coordinate of point.
			/// </summary>
			public int y;

			/// <summary>
			/// Construct a point of coordinates (x,y).
			/// </summary>
			public POINT(int x, int y)
			{
				this.x = x;
				this.y = y;
			}
		}

		[StructLayout(LayoutKind.Sequential)]
		public struct MINMAXINFO
		{
			public POINT ptReserved;
			public POINT ptMaxSize;
			public POINT ptMaxPosition;
			public POINT ptMinTrackSize;
			public POINT ptMaxTrackSize;
		}; 

       /// <summary>
        /// </summary>
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
        public class MONITORINFO
        {
            /// <summary>
            /// </summary>            
            public int cbSize = Marshal.SizeOf(typeof(MONITORINFO));

            /// <summary>
            /// </summary>            
            public RECT rcMonitor = new RECT();

            /// <summary>
            /// </summary>            
            public RECT rcWork = new RECT();

            /// <summary>
            /// </summary>            
            public int dwFlags = 0;
        }


        /// <summary> Win32 </summary>
        [StructLayout(LayoutKind.Sequential, Pack = 0)]
        public struct RECT
        {
            /// <summary> Win32 </summary>
            public int left;
            /// <summary> Win32 </summary>
            public int top;
            /// <summary> Win32 </summary>
            public int right;
            /// <summary> Win32 </summary>
            public int bottom;

            /// <summary> Win32 </summary>
            public static readonly RECT Empty = new RECT();

            /// <summary> Win32 </summary>
            public int Width
            {
                get { return Math.Abs(right - left); }  // Abs needed for BIDI OS
            }
            /// <summary> Win32 </summary>
            public int Height
            {
                get { return bottom - top; }
            }

            /// <summary> Win32 </summary>
            public RECT(int left, int top, int right, int bottom)
            {
                this.left = left;
                this.top = top;
                this.right = right;
                this.bottom = bottom;
            }


            /// <summary> Win32 </summary>
            public RECT(RECT rcSrc)
            {
                this.left = rcSrc.left;
                this.top = rcSrc.top;
                this.right = rcSrc.right;
                this.bottom = rcSrc.bottom;
            }

            /// <summary> Win32 </summary>
            public bool IsEmpty
            {
                get
                {
                    // BUGBUG : On Bidi OS (hebrew arabic) left > right
                    return left >= right || top >= bottom;
                }
            }
            /// <summary> Return a user friendly representation of this struct </summary>
            public override string ToString()
            {
                if (this == RECT.Empty) { return "RECT {Empty}"; }
                return "RECT { left : " + left + " / top : " + top + " / right : " + right + " / bottom : " + bottom + " }";
            }

            /// <summary> Determine if 2 RECT are equal (deep compare) </summary>
            public override bool Equals(object obj)
            {
                if (!(obj is Rect)) { return false; }
                return (this == (RECT)obj);
            }

            /// <summary>Return the HashCode for this struct (not garanteed to be unique)</summary>
            public override int GetHashCode()
            {
                return left.GetHashCode() + top.GetHashCode() + right.GetHashCode() + bottom.GetHashCode();
            }


            /// <summary> Determine if 2 RECT are equal (deep compare)</summary>
            public static bool operator ==(RECT rect1, RECT rect2)
            {
                return (rect1.left == rect2.left && rect1.top == rect2.top && rect1.right == rect2.right && rect1.bottom == rect2.bottom);
            }

            /// <summary> Determine if 2 RECT are different(deep compare)</summary>
            public static bool operator !=(RECT rect1, RECT rect2)
            {
                return !(rect1 == rect2);
            }


        }

        [DllImport("user32")]
        internal static extern bool GetMonitorInfo(IntPtr hMonitor, MONITORINFO   lpmi);

        /// <summary>
        /// 
        /// </summary>
        [DllImport("User32")]
        internal static extern IntPtr MonitorFromWindow(IntPtr handle, int flags);

		private static System.IntPtr WindowProc(
	  System.IntPtr hwnd,
	  int msg,
	  System.IntPtr wParam,
	  System.IntPtr lParam,
	  ref bool handled)
		{
			switch (msg)
			{
				case 0x0024:/* WM_GETMINMAXINFO */
					WmGetMinMaxInfo(hwnd, lParam);
					handled = true;
					break;
			}

			return (System.IntPtr)0;
		}

		private static void WmGetMinMaxInfo(System.IntPtr hwnd, System.IntPtr lParam)
		{
			MINMAXINFO mmi = (MINMAXINFO)Marshal.PtrToStructure(lParam, typeof(MINMAXINFO));

			// Adjust the maximized size and position to fit the work area of the correct monitor
			int MONITOR_DEFAULTTONEAREST = 0x00000002;
			System.IntPtr monitor = MonitorFromWindow(hwnd, MONITOR_DEFAULTTONEAREST);           

			if (monitor != System.IntPtr.Zero)
			{
				try
				{
					MONITORINFO monitorInfo = new MONITORINFO();
					GetMonitorInfo(monitor, monitorInfo);

					RECT rcWorkArea = monitorInfo.rcWork;
					RECT rcMonitorArea = monitorInfo.rcMonitor;

                    int MARGIN = 0;// Math.Abs(rcWorkArea.left) - Math.Abs(mmi.ptMaxPosition.x);

                    //if (MARGIN > 50)
                    //    return;

					mmi.ptMaxPosition.x = Math.Abs(rcWorkArea.left - rcMonitorArea.left) - MARGIN;
					mmi.ptMaxPosition.y = Math.Abs(rcWorkArea.top - rcMonitorArea.top) - MARGIN;                   


					mmi.ptMaxSize.x = Math.Abs(rcWorkArea.right - rcWorkArea.left) + MARGIN * 2;
					mmi.ptMaxSize.y = Math.Abs(rcWorkArea.bottom - rcWorkArea.top) + (MARGIN > 2 ? 2 : MARGIN);
				}
				catch { }
			}
			
			Marshal.StructureToPtr(mmi, lParam, true);
		}
	
		#endregion

		#region Enabling/disabling menu items on project creating/closing/opening
		bool IsJustLoaded
        {
            set
            {
                if (value)
                {
                    menuFile_New.IsEnabled = true;
                    menuFile_Open.IsEnabled = true;
                    menu_File_Close.IsEnabled = false;
                    menuFile_Save.IsEnabled = false;
                    menuFile_SaveTo.IsEnabled = false;
					menu_Screen_Properties.IsEnabled = false;
                    tProperties.IsEnabled = false;
				}
            }
        }

        bool IsProjectOpenedOrCreated
        {
            set
            {
                if (value)
                {
                    menu_File_Close.IsEnabled = true;
                    menuFile_New.IsEnabled = false;
                    menuFile_Open.IsEnabled = false;
                    menuFile_Save.IsEnabled = true;
                    menuFile_SaveTo.IsEnabled = true;
					menu_Screen_Properties.IsEnabled = true;
                    tProperties.IsEnabled = true;
                }
            }
        }

        bool IsProjectClosed
        {
            set
            {
                if (value)
                {
                    menu_File_Close.IsEnabled = false;
                    menuFile_New.IsEnabled = true;
                    menuFile_Open.IsEnabled = true;
                    menuFile_Save.IsEnabled = false;
                    menuFile_SaveTo.IsEnabled = false;
 					menu_Screen_Properties.IsEnabled = false;
                    tProperties.IsEnabled = false;
                }
            }
        }
        #endregion


        /// <summary>
        /// Initializes the new DesignerWindow instance
        /// </summary>
        public DesignerWindow()
        {
			try
			{
				CultureInfo info = Properties.Settings.Default.DefaultCulture;
				if (info != null)
				{
					Properties.Resources.Culture = info;
					CultureResources.ChangeCulture(info);
					Thread.CurrentThread.CurrentCulture = info;
				}

				InitializeComponent();

                #region 언어 메뉴
                foreach(CultureInfo cinfo in CultureResources.SupportedCultures)
                {
                    MenuItem item = new MenuItem();
                    item.Header = cinfo;
                    item.IsChecked = info.Equals(cinfo);

                    menuLanguages.Items.Add(item);
                }
                #endregion

                /*
				TrialChecker checker = new TrialChecker();
				if (!checker.IsValid)
				{
					MessageBox.Show(checker.LastMessage);

					this.Visibility = Visibility.Hidden;
					Application.Current.Shutdown();
				}
                */

				this.SourceInitialized += new EventHandler(DesignerWindow_SourceInitialized);

			}
			catch (System.Exception e)
			{
				logger.Error(e.ToString());
				MessageBox.Show(e.Message);
				Application.Current.Shutdown();
			}
        }

		void DesignerWindow_SourceInitialized(object sender, EventArgs e)
		{
			System.IntPtr handle = (new WinInterop.WindowInteropHelper(this)).Handle;
			WinInterop.HwndSource.FromHwnd(handle).AddHook(new WinInterop.HwndSourceHook(WindowProc));
		}

        /// <summary>
        /// Prepares some maintaining after DesignerWindow is loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void WDMainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            if (!CheckApplicationCriticalDirectories())
            {
                Close();
            }           

//             explorer = new ThumbnailViewer();

            isPressed = false;
			String sExecutingPath = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
			Assembly ass = Assembly.GetExecutingAssembly();

            #region	Open & Save Dialog Initialize
//             openProjectDialog = new System.Windows.Forms.OpenFileDialog();
// 			openProjectDialog.Filter = Properties.Resources.labelFilter;
// 			openProjectDialog.Title = Properties.Resources.titleOpenDialog;
// 			openProjectDialog.InitialDirectory = sExecutingPath + "\\DesignerData\\Screens";
//             openProjectDialog.FileOk += new System.ComponentModel.CancelEventHandler(openProjectDialog_FileOk);

            saveProjectDialog = new System.Windows.Forms.SaveFileDialog();
			saveProjectDialog.Filter = Properties.Resources.labelFilter;
			saveProjectDialog.Title = Properties.Resources.titleSaveDialog;
			saveProjectDialog.InitialDirectory = sExecutingPath + "\\DesignerData\\Screens";
            saveProjectDialog.FileOk += new System.ComponentModel.CancelEventHandler(saveProjectDialog_FileOk);

            #endregion

            #region Event Registration
            this.Closing += new System.ComponentModel.CancelEventHandler(Window1_Closing);
//             this.MouseUp += new MouseButtonEventHandler(DesignerWindow_MouseUp);
//             this.PreviewMouseMove += new MouseEventHandler(DesignerWindow_PreviewMouseMove);
//             this.PreviewMouseUp += new MouseButtonEventHandler(DesignerWindow_PreviewMouseUp);
            //((System.Windows.Media.Animation.Storyboard)Resources["startup"]).Completed += new EventHandler(DesignerWindow_Completed);
            this.KeyDown += new KeyEventHandler(DesignerWindow_KeyDown);
            #endregion

            scrollOffset = 0;
            IsDropCancelled = false;

            playerControl = new PlayerControl();
            playerGrid.Children.Add(playerControl);
            playerControl.playButton.MouseDown += new MouseButtonEventHandler(playButton_MouseDown);
            playerControl.stopButton.MouseDown += new MouseButtonEventHandler(stopButton_MouseDown);

            IsJustLoaded = true;
            zoomSlider.ValueChanged += new RoutedPropertyChangedEventHandler<double>(zoomSlider_ValueChanged);

            manager = new ProjectManager();

            newProjectControl = new NewProjectWindow();
			newProjectControl.bCreate.Click += new RoutedEventHandler(bCreate_Click);
			newProjectControl.bCancel.Click += new RoutedEventHandler(bCancel_Click);

			this.WindowState = WindowState.Maximized;

			#region ADDON 컴포넌트
// 			KeyChecker.GetInstance.SetKey(iVisionAddition.ADD_COMP_PPT, true);
// 			KeyChecker.GetInstance.SetKey(iVisionAddition.ADD_COMP_QTIME, true);
// 			KeyChecker.GetInstance.SetKey(iVisionAddition.ADD_COMP_RSS, true);
// 			KeyChecker.GetInstance.SetKey(iVisionAddition.ADD_COMP_TV, true);
// 			KeyChecker.GetInstance.SetKey(iVisionAddition.ADD_COMP_WEB, true);
// 			KeyChecker.GetInstance.SetKey(iVisionAddition.ADD_COMP_STREAM, true);

			bool bPPT = KeyChecker.GetInstance.CheckKey(iVisionAddition.ADD_COMP_PPT);
			bool bQT = KeyChecker.GetInstance.CheckKey(iVisionAddition.ADD_COMP_QTIME);
			bool bRSS = KeyChecker.GetInstance.CheckKey(iVisionAddition.ADD_COMP_RSS);
			bool bTV = KeyChecker.GetInstance.CheckKey(iVisionAddition.ADD_COMP_TV);
			bool bWEB = KeyChecker.GetInstance.CheckKey(iVisionAddition.ADD_COMP_WEB);
			bool bSTREAM = KeyChecker.GetInstance.CheckKey(iVisionAddition.ADD_COMP_STREAM);
			bool bWeather = KeyChecker.GetInstance.CheckKey(iVisionAddition.ADD_COMP_WEATHER);

			addppt.Visibility = bPPT ? Visibility.Visible : Visibility.Collapsed;
			addquicktime.Visibility = bQT ? Visibility.Visible : Visibility.Collapsed;
			addrsstext.Visibility = bRSS ? Visibility.Visible : Visibility.Collapsed;
			addtv.Visibility = bTV ? Visibility.Visible : Visibility.Collapsed;
			//addwebpage.Visibility = bWEB ? Visibility.Visible : Visibility.Collapsed;
			addstreaming.Visibility = bSTREAM ? Visibility.Visible : Visibility.Collapsed;
			addweather.Visibility = bWeather ? Visibility.Visible : Visibility.Collapsed;
			#endregion

			#region 파라메터가 있는경우 파일 열기
			string sPath = App.CommandLineArgs["open"] as string;
			if (!String.IsNullOrEmpty(sPath))
			{
				LoadProject(sPath);
			}
			#endregion

			MostRecentFiles.GetInstance.MAX_LENGTH = 5;
			recentItemList.ItemsSource = MostRecentFiles.GetInstance;
		}

		#region New Screen Events
		void bCancel_Click(object sender, RoutedEventArgs e)
		{
			newProjectControl.Hide();
		}

		void bCreate_Click(object sender, RoutedEventArgs e)
		{
			if (false == newProjectControl.ValidationCheck())
			{
				MessageBox.Show(Properties.Resources.mbErrorCreate, Properties.Resources.titleDesignerWindow);
				return;
			}
			newProjectControl.Hide();
			manager.CreateProject(newProjectControl.tbProjectName.Text, Int32.Parse(newProjectControl.widthBox.Text), Int32.Parse(newProjectControl.heightBox.Text));// new Point(Int32.Parse(asp[0]), Int32.Parse(asp[1])));
			if (manager.Pane != null)
			{
				IsProjectOpenedOrCreated = true;
				manager.Pane.IsAdScreen = newProjectControl.IsAdScreen;
				manager.Pane.Ad_EventConstraint = newProjectControl.AdEventConstraint;
				manager.Pane.Ad_StartTime = newProjectControl.AdStartTime;
				manager.Pane.Ad_EndTime = newProjectControl.AdEndTime;
				manager.Pane.Ad_MetaTags = newProjectControl.AdMetaTags;
				manager.Pane.OnOpenScreenProperties += new EventHandler(Pane_OnOpenScreenProperties);
			}
			manager.InitZoomControl(zoomSlider);

			//	줌을 페이지 실제 크기에 맞춘다.
			FitZoomInActualScreenSize();

			MainRibbonPanel.Title = newProjectControl.tbProjectName.Text + ".bin - " + Properties.Resources.titleDesignerWindow;
		}

		void Pane_OnOpenScreenProperties(object sender, EventArgs e)
		{
			ShowScreenProperties();
		}
		#endregion

        /// <summary>
        /// Displays the percentage zoom value
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void zoomSlider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            string value = (e.NewValue * 100).ToString();
            if (value.Contains('.'))
            {
                zoomBox.Text = value.Remove(value.LastIndexOf('.')) + "%";
            }
            else zoomBox.Text = value + "%";
        }

		delegate void procedureCB();
		void ProcedureOpen()
		{

			using(System.Windows.Forms.OpenFileDialog dlg = new System.Windows.Forms.OpenFileDialog())
			{
				dlg.Filter = Properties.Resources.labelFilter;
				dlg.Title = Properties.Resources.titleOpenDialog;
				// 			dlg.InitialDirectory = sExecutingPath + "\\DesignerData\\Screens";
				dlg.FileOk += new System.ComponentModel.CancelEventHandler(openProjectDialog_FileOk);

				dlg.ShowDialog();
				dlg.Dispose();
			}
		}

        #region Shortcuts maintaining
        void cb_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            if (e.Command.Equals(RibbonCommands.New))
            {
				if (manager.Pane == null || manager.CloseProject())
				{
					MainRibbonPanel.Title = Properties.Resources.titleDesignerWindow;
					IsProjectClosed = true;

					if (newProjectControl == null) newProjectControl = new NewProjectWindow();
					newProjectControl.Owner = this;
					newProjectControl.ShowDialog();
				}
            }
			if (e.Command.Equals(RibbonCommands.Close))
            {
                if (manager.Pane == null) return;
				if (manager.CloseProject())
				{
					MainRibbonPanel.Title = Properties.Resources.titleDesignerWindow;
					IsProjectClosed = true;
				}

				MainRibbonPanel.SelectedTab.Focus();
				MainRibbonPanel.Focus();

				return;
            }
			if (e.Command.Equals(RibbonCommands.Open))
            {
				if (manager.Pane == null || manager.CloseProject())
				{
					MainRibbonPanel.Title = Properties.Resources.titleDesignerWindow;
					IsProjectClosed = true;

					this.Dispatcher.Invoke(new procedureCB(this.ProcedureOpen), null);

// 					openProjectDialog.ShowDialog();
				}
			}
			if (e.Command.Equals(RibbonCommands.Save))
            {
                if (manager.Pane == null) return;
                manager.SaveProject(manager.ProjectPath);
            }
			if (e.Command.Equals(RibbonCommands.SaveAs))
            {
                if (manager.Pane == null) return;
                saveProjectDialog.ShowDialog();
            }

			if(e.Command.Equals(RibbonCommands.SaveAtDesktop))
			{
				if (manager.Pane == null) return;
				manager.SaveProjectAs(String.Format("{0}\\{1}.bin", Environment.GetFolderPath(Environment.SpecialFolder.Desktop), manager.ProjectName));
			}
			if(e.Command.Equals(RibbonCommands.SaveAtMyDocument))
			{
				if (manager.Pane == null) return;

				manager.SaveProjectAs(String.Format("{0}\\{1}.bin", Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), manager.ProjectName));
			}

			if (e.Command.Equals(RibbonCommands.Exit))
			{
				if (manager.CloseProject())
				{
					MainRibbonPanel.Title = Properties.Resources.titleDesignerWindow;
					IsProjectClosed = true;
				}

				Close();
			}

			if (e.Command.Equals(RibbonCommands.Title))
			{
				if (manager.CloseProject())
				{
					MainRibbonPanel.Title = Properties.Resources.titleDesignerWindow;
					IsProjectClosed = true;
				}

				Close();
			}

			if (e.Command.Equals(RibbonCommands.ScreenProperties))
			{
				NewProjectWindow screenProperties = new NewProjectWindow();
				ShowScreenProperties();

			}

			if (e.Command.Equals(RibbonCommands.About))
			{
				//////////////////////////////////////////////////////////////////////////
// 				ContentsBrowser cbw = new ContentsBrowser();
// 				cbw.Show(null);
				//////////////////////////////////////////////////////////////////////////
				AboutDesigner about = new AboutDesigner();
				about.Owner = this;
				about.ShowDialog();
				MainRibbonPanel.SelectedTab.Focus();

			}
			e.Handled = true;

			MainRibbonPanel.Focus();
			
        }

        void cb_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }
        #endregion

		/// <summary>
		/// 페이지 속성창을 연다.
		/// </summary>
		private void ShowScreenProperties()
		{
			NewProjectWindow screenProperties = new NewProjectWindow();
			screenProperties.IsEditMode = true;
			screenProperties.LoadProperties(manager.Pane);
            screenProperties.ShowDialog();
            if (true == screenProperties.DialogResult)
			{
				manager.Pane.IsAdScreen = screenProperties.IsAdScreen;
				manager.Pane.Ad_EventConstraint = screenProperties.AdEventConstraint;
				manager.Pane.Ad_StartTime = screenProperties.AdStartTime;
				manager.Pane.Ad_EndTime = screenProperties.AdEndTime;
				manager.Pane.Ad_MetaTags = screenProperties.AdMetaTags;
				((IDesignElement)manager.Pane).Width = Int32.Parse(screenProperties.widthBox.Text);
				((IDesignElement)manager.Pane).Height = Int32.Parse(screenProperties.heightBox.Text);
			}
		}


        /// <summary>
        /// Checks the 'media' and 'settings' directories in the designer root directory
        /// </summary>
        /// <returns>If check succeeded returns true, otherwise - false</returns>
        private bool CheckApplicationCriticalDirectories()
        {
			String sExecutingPath = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
			string path = sExecutingPath + "\\DesignerData\\Screens";
			if (!Directory.Exists(sExecutingPath + "\\DesignerData"))
			{
                try
                {
					Directory.CreateDirectory(sExecutingPath + "\\DesignerData");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(Properties.Resources.mbUnableToCreateProjectssFolder + ex.Message, Properties.Resources.titleDesignerWindow, MessageBoxButton.OK, MessageBoxImage.Warning);
                    return false;
                }
            }
			
			if (!Directory.Exists(path))
            {
                try
                {
					Directory.CreateDirectory(path);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(Properties.Resources.mbUnableToCreateProjectssFolder + ex.Message, Properties.Resources.titleDesignerWindow, MessageBoxButton.OK, MessageBoxImage.Warning);
                    return false;
                }
            }

            path = AppDomain.CurrentDomain.BaseDirectory + "settings\\";
            if (!Directory.Exists(path))
            {
                MessageBox.Show(Properties.Resources.mbUnableToLocateSettingFolder, Properties.Resources.titleDesignerWindow);
                return false;
            }
            return true;
        }

        /// <summary>
        /// Saves the project to directory specified in the save project dialog
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void saveProjectDialog_FileOk(object sender, System.ComponentModel.CancelEventArgs e)
        {
            manager.SaveProjectAs(saveProjectDialog.FileName);
			MainRibbonPanel.Title = System.IO.Path.GetFileName(saveProjectDialog.FileName) + " - " + Properties.Resources.titleDesignerWindow;
		}

        /// <summary>
        /// Opens the project selected in open project dialog
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void openProjectDialog_FileOk(object sender, System.ComponentModel.CancelEventArgs e)
        {
			System.Windows.Forms.OpenFileDialog dlg = sender as System.Windows.Forms.OpenFileDialog;
			if (dlg != null)
			{
				LoadProject(dlg.FileName);
			}
        }

		/// <summary>
		/// 프로젝트 로드
		/// </summary>
		/// <param name="fullpath"></param>
		void LoadProject(string fullpath)
		{
            bool isLoad=false;
			try
			{
				
                isLoad=manager.LoadProject(fullpath);
                if (isLoad)
                {
				    manager.InitZoomControl(zoomSlider);
                    FitZoomInActualScreenSize();
				    MainRibbonPanel.Title = System.IO.Path.GetFileName(fullpath) + " - " + Properties.Resources.titleDesignerWindow;
			        IsProjectOpenedOrCreated = true;
			        if(manager.Pane != null)
				        manager.Pane.OnOpenScreenProperties += new EventHandler(Pane_OnOpenScreenProperties);
                }
			}
			catch (System.Exception e)
			{
				MessageBox.Show(e.Message);
			}
			//	줌을 페이지 실제 크기에 맞춘다.
		}

        #region Preview maintance
        void stopButton_MouseDown(object sender, MouseButtonEventArgs e)
        {
            menu_File_Close.IsEnabled = true;
            menuFile_Quit.IsEnabled = true;
			try
			{
				playerControl.Stop();
			}
			catch { }
        }

        public void playButton_MouseDown(object sender, MouseButtonEventArgs e)
        {
            menu_File_Close.IsEnabled = false;
            menuFile_Quit.IsEnabled = false;
			try
			{
                ScreenStartInfoInSchedule.GetInstance.ScreenStartDT = DateTime.Now;
				playerControl.Play(manager.Pane);
			}
			catch { }
		}
        #endregion


        #region Adding new elements
		void cb_Components_Executed(object sender, ExecutedRoutedEventArgs e)
		{
			isPressed = false;
			AddNewElement(((RibbonButton)e.OriginalSource).Name);
		}

		void AddElementWithData(Type type, string datafile)
		{
			if (manager == null || manager.Pane == null)
			{
				return;
			}
			IDesignElement element = manager.Pane.Add(type, ElementsListViewer.MaxZIndex + 1);
			if (element != null)
			{
				element.Playlist.Add(PlaylistEditor.MakePlaylistItem(datafile));
			}
		}
		void AddElementWithData(Type type, Point position, string datafile)
		{
			if (manager == null || manager.Pane == null)
			{
				return;
			}
			IDesignElement element = manager.Pane.Add(type, position);
			if(element != null)
			{
				element.Playlist.Add(PlaylistEditor.MakePlaylistItem(datafile));
			}
		}

        void AddNewElement(string name, Point location)
        {
            
            if (manager == null)
            {
                return;
            }
            if (manager.Pane == null) return;

            switch (name)
            {
                case "addtext":
                    {
                        manager.Pane.Add(typeof(TextComponent), location);
                        break;
                    }
                case "addimage":
                    {
                        manager.Pane.Add(typeof(ImageComponent), location);
                        break;
                    }
                case "addmedia":
                    {
                        manager.Pane.Add(typeof(MediaComponent), location);
                        break;
                    }
                case "addwebpage":
                    {
                        
                        manager.Pane.Add(typeof(WebComponent), location);
                        
                        break;
                    }
                case "addscrolltext":
                    {
                        manager.Pane.Add(typeof(ScrollTextComponent), location);
                        break;
                    }
                case "addrsstext":
                    {
                        manager.Pane.Add(typeof(RssComponent), location);
                        break;
                    }
                case "addclock":
                    {
                        manager.Pane.Add(typeof(AnalogueClockComponent), location);
                        break;
                    }
                case "adddigitalclock":
                    {
                        manager.Pane.Add(typeof(DigitalClockComponent), location);
                        break;
                    }
                case "adddate":
                    {
                        manager.Pane.Add(typeof(DateComponent), location);
                        break;
                    }
                case "addellipse":
                    {
                        manager.Pane.Add(typeof(EllipseComponent), location);
                        break;
                    }
                case "addrectangle":
                    {
                        manager.Pane.Add(typeof(RectangleComponent), location);
                        break;
                    }
                case "addquicktime":
                    {
                        manager.Pane.Add(typeof(QuickTimeComponent), location);
                        break;
                    }
                case "addppt":
                    {
                        manager.Pane.Add(typeof(PptComponent), location);
                        break;
                    }
				case "addweather":
					{
						manager.Pane.Add(typeof(WeatherComponent), location);
						break;
					}
				case "addflash":
                    {
                        manager.Pane.Add(typeof(FlashComponent), location);
                        break;
                    }
				case "addstreaming":
					{
						manager.Pane.Add(typeof(StreamingComponent), location);
						break;
					}
				case "addtv":
					{
						manager.Pane.Add(typeof(TVComponent), location);
						break;
					}
				case "addiptv":
                    {
                        //Manager.AddNewDesignElement(typeof(IPTVComponent), new Point(100, 100));
                        break;
                    }
                case "addaudio":
                    {
                        manager.Pane.Add(typeof(AudioComponent), location);
                        break;
                    }
                default: break;
            }
        }

        void AddNewElement(string name)
        {
            if (manager == null)
            {
                return;
            }
            if (manager.Pane == null) return;

            switch (name.ToLower())
            {
                case "addtext":
                    {
                        UiE = (UIElement)manager.Pane.Add(typeof(TextComponent), ElementsListViewer.MaxZIndex + 1);
                        break;
                    }
                case "addimage":
                    {
                        UiE = (UIElement)manager.Pane.Add(typeof(ImageComponent), ElementsListViewer.MaxZIndex + 1);
                        break;
                    }
                case "addmedia":
                    {
                        UiE = (UIElement)manager.Pane.Add(typeof(MediaComponent), ElementsListViewer.MaxZIndex + 1);
                        break;
                    }
                case "addwebpage":
                    {
                        UiE = (UIElement)manager.Pane.Add(typeof(WebComponent), ElementsListViewer.MaxZIndex + 1);
                        break;
                    }
                case "addscrolltext":
                    {
                        UiE = (UIElement)manager.Pane.Add(typeof(ScrollTextComponent), ElementsListViewer.MaxZIndex + 1);
                        break;
                    }
                case "addrsstext":
                    {
                        UiE = (UIElement)manager.Pane.Add(typeof(RssComponent), ElementsListViewer.MaxZIndex + 1);
                        break;
                    }
                case "addclock":
                    {
                        UiE = (UIElement)manager.Pane.Add(typeof(AnalogueClockComponent), ElementsListViewer.MaxZIndex + 1);
                        break;
                    }
                case "adddigitalclock":
                    {
                        UiE = (UIElement)manager.Pane.Add(typeof(DigitalClockComponent), ElementsListViewer.MaxZIndex + 1);
                        break;
                    }
                case "adddate":
                    {
                        UiE = (UIElement)manager.Pane.Add(typeof(DateComponent), ElementsListViewer.MaxZIndex + 1);
                        break;
                    }
                case "addellipse":
                    {
                        UiE = (UIElement)manager.Pane.Add(typeof(EllipseComponent), ElementsListViewer.MaxZIndex + 1);
                        break;
                    }
                case "addrectangle":
                    {
                        UiE = (UIElement)manager.Pane.Add(typeof(RectangleComponent), ElementsListViewer.MaxZIndex + 1);
                        break;
                    }
                case "addquicktime":
                    {
						try
						{
                            UiE = (UIElement)manager.Pane.Add(typeof(QuickTimeComponent), ElementsListViewer.MaxZIndex + 1);
						}
						catch
						{
							MessageBox.Show(Properties.Resources.mbErrorQuickTime, Properties.Resources.titleDesignerWindow, MessageBoxButton.OK, MessageBoxImage.Error);
						}
                        break;
                    }
                case "addppt":
					{
						try
						{
                            UiE = (UIElement)manager.Pane.Add(typeof(PptComponent), ElementsListViewer.MaxZIndex + 1);
						}
						catch
						{
							MessageBox.Show(Properties.Resources.mbErrorPPT, Properties.Resources.titleDesignerWindow, MessageBoxButton.OK, MessageBoxImage.Error);
						}
						break;
					}
				case "addweather":
					{
                        UiE = (UIElement)manager.Pane.Add(typeof(WeatherComponent), ElementsListViewer.MaxZIndex + 1);
						break;
					}
				case "addflash":
					{
						try
						{
                            UiE = (UIElement)manager.Pane.Add(typeof(FlashComponent), ElementsListViewer.MaxZIndex + 1);
						}
						catch
						{
							MessageBox.Show(Properties.Resources.mbErrorFlash, Properties.Resources.titleDesignerWindow, MessageBoxButton.OK, MessageBoxImage.Error);
						}
						break; 
					}
				case "addtv":
					{
						try
						{
                            UiE = (UIElement)manager.Pane.Add(typeof(TVComponent), ElementsListViewer.MaxZIndex + 1);
						}
						catch
						{
							MessageBox.Show(Properties.Resources.mbErrorTV, Properties.Resources.titleDesignerWindow, MessageBoxButton.OK, MessageBoxImage.Error);
						}
						break;
					}
				case "addstreaming":
					{
                        UiE = (UIElement)manager.Pane.Add(typeof(StreamingComponent), ElementsListViewer.MaxZIndex + 1);
						break;
					}

				case "addiptv":
                    {
                        //Manager.AddNewDesignElement(typeof(IPTVComponent), new Point(100, 100));
                        break;
                    }
                case "addaudio":
                    {
                        UiE = (UIElement)manager.Pane.Add(typeof(AudioComponent), ElementsListViewer.MaxZIndex + 1);
                        break;
                    }
                default: break;
            }
            UI_Selection(UiE); 
        }


        void DesignerWindow_KeyDown(object sender, KeyEventArgs e)
        {
//            if (e.Key == Key.Escape)
//            {

////                 IsDropCancelled = true;
////                 if (dragImage == null) return;
////                 if (DesignerGrid.Children.Contains(dragImage))
////                     DesignerGrid.Children.Remove(dragImage);
////                 dragImage.Source = null;
////                 dragImage = null;
//            }
        }
        #endregion

        /// <summary>
        /// Prepares some maintaning when the DesignerWindow is closing
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void Window1_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (manager != null)
            {
                if (!manager.CloseProject())
                {
                    e.Cancel = true;
                    return;
                }
            }
            if (newProjectControl != null)
            {
				newProjectControl.bCreate.Click -= new RoutedEventHandler(bCreate_Click);
				newProjectControl.bCancel.Click -= new RoutedEventHandler(bCancel_Click);
                
				newProjectControl.Close();
                newProjectControl = null;
            }

            if (Screen.PlaylistEditor != null)
                Screen.PlaylistEditor.Close();

            Process.GetCurrentProcess().Kill();
        }

        /// <summary>
        /// Shows the Projects Explorer up
        /// </summary>
        private void OpenExplorer()
        {
// 			explorer.ShowExplorer();
        }

		/// <summary>
		/// 줌을 페이지 크기에 맞춘다.
		/// </summary>
		private void FitZoomInActualScreenSize()
		{
			double dZoom = zoomSlider.Value;
			while (manager.Pane != null && (MainBorder.ActualWidth < (manager.Pane.Width * dZoom) || MainBorder.ActualHeight < (manager.Pane.Height * dZoom)))
			{
				dZoom = zoomSlider.Value;
				if (dZoom == 0.1)
					break;

				zoomSlider.Value = dZoom - 0.1;
			}
		}

        #region NewProjectControl event handlers
        void bCreate_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
			if (false == newProjectControl.ValidationCheck())
			{
				MessageBox.Show(Properties.Resources.mbErrorCreate, Properties.Resources.titleDesignerWindow);
				return ;
			}
            newProjectControl.Hide();
            manager.CreateProject(newProjectControl.tbProjectName.Text, Int32.Parse(newProjectControl.widthBox.Text), Int32.Parse(newProjectControl.heightBox.Text));// new Point(Int32.Parse(asp[0]), Int32.Parse(asp[1])));
            if (manager.Pane != null)
            {
                IsProjectOpenedOrCreated = true;
            }
            manager.InitZoomControl(zoomSlider);

			//	줌을 페이지 실제 크기에 맞춘다.
			FitZoomInActualScreenSize();

            MainRibbonPanel.Title = newProjectControl.tbProjectName.Text + ".bin - " + Properties.Resources.titleDesignerWindow;
            
        }

        void bCancel_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            newProjectControl.Hide();
        }
        #endregion


        #region IProjectManagerContainer Members

        ProjectManager IProjectManagerContainer.GetProjectManager()
        {
            return manager;
        }

        #endregion

        public static string GetRootPath()
        {
            return System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetModules()[0].FullyQualifiedName) + "\\";
        }

        public static int[] GetCustomColors()
        {
            try
            {
                string[] customcolors = ConfigurationManager.AppSettings.Get("CustomColors").Split(',');
                if (customcolors.Length > 0)
                {
                    int[] arr = new int[customcolors.Length];
                    int index = 0;
                    foreach (string str in customcolors)
                    {
                        if (str.Length == 0)
                            return null;
                        arr[index] = int.Parse(str);
                        index++;
                    }

                    return arr;
                }
            }
            catch (Exception ex)
            {
                string errmsg = ex.Message.ToString();
              //  System.Windows.MessageBox.Show(errmsg);
            }
            return null;
        }

        public static void SetAppSeting(string key, string value)
        {
            try
            {
                ConfigurationManager.AppSettings.Set(key, value);

                string path = GetRootPath() + "WPFDesigner.exe.config";
                if (File.Exists(path))
                {
                    XmlDocument _doc = new XmlDocument();
                    _doc.Load(path);
                    XmlNode appSetting = _doc.SelectSingleNode("//appSettings");
                    foreach (XmlNode node in appSetting.ChildNodes)
                    {
                        if (node.Attributes["key"].InnerText.Equals(key))
                        {
                            node.Attributes["value"].InnerText = value;
                            break;
                        }
                    }
                    _doc.Save(path);
                }
            }
            catch (Exception ex)
            {
                string errmsg = ex.Message.ToString();
              //  System.Windows.MessageBox.Show(errmsg);
            }
        }

		/// <summary>
		/// Changing the UI culture of Scheduler
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void menuLanguages_Selected(object sender, RoutedEventArgs e)
		{
			MenuItem parent = e.Source as MenuItem;
			MenuItem selected = e.OriginalSource as MenuItem;
			CultureInfo info = selected.Header as CultureInfo;

			if (Properties.Resources.Culture != null && !Properties.Resources.Culture.Equals(info))
			{
				Thread.CurrentThread.CurrentCulture = info;
				Thread.CurrentThread.CurrentUICulture = info;
				CultureResources.ChangeCulture(info);

				//	Setting 파일에 저장
				Properties.Settings.Default.DefaultCulture = info;
				Properties.Settings.Default.Save();

                #region 메뉴 체크 로직
                try
                {
                    foreach (MenuItem item in ((RibbonDropDownButton)sender).Items)
                    {
                        item.IsChecked = info.Equals(item.Header);
                    }
                }
                catch { }
                #endregion
// 				selected.IsChecked = true;
			}
		}

		private void Expand_Click(object sender, RoutedEventArgs e)
		{
			if (centerSplitter.IsEnabled)
			{
				grabAngle.Angle = -180;
				PropertiesColumn.Width = new GridLength(0);
				centerSplitter.IsEnabled = false;
			}
			else
			{
				grabAngle.Angle = 0;
				PropertiesColumn.Width = new GridLength(280);
				centerSplitter.IsEnabled = true;
			}
		}

		private void MainGrid_DragEnter(object sender, DragEventArgs e)
		{
			if (manager.Pane == null) return;
			
			if( e.Data == null) 
			{
				e.Effects = DragDropEffects.None;
				e.Handled = true;
				return;
			}
			else
			{
				if(e.Data.GetDataPresent(DataFormats.FileDrop) && this.IsFocused)
				{
					e.Effects = DragDropEffects.Copy;
					e.Handled = true;
					return;
				}
			}

			e.Effects = DragDropEffects.None;
			e.Handled = true;
		}

		private void MainGrid_Drop(object sender, DragEventArgs e)
		{
			if (manager.Pane == null) return;

			try
			{
				if (e.Data.GetDataPresent(DataFormats.FileDrop))
				{
					string[] arrFiles = e.Data.GetData(DataFormats.FileDrop) as string[];

					if (arrFiles != null)
					{
						foreach (string filename in arrFiles)
						{
							CompType type = WPFDUtil.GetComponentTypeFromExtension(System.IO.Path.GetExtension(filename).ToLower());

							if (type == CompType.Unknown)
								continue;

							if (arrFiles.Length == 1)
								AddElementWithData(WPFDUtil.GetComponentTypeFromCompType(type), e.GetPosition(MainGrid.Child), filename);
							else
								AddElementWithData(WPFDUtil.GetComponentTypeFromCompType(type), filename);

							e.Handled = true;
						}
					}
				}
			}
			catch (System.Exception ex)
			{
				logger.Error(ex.ToString());
			}
		}

		#region MostRecentFileSelected
		private void ribbonApplicationMenu_MostRecentFileSelected(object sender, MostRecentFileSelectedEventArgs e)
		{ 
// 			MessageBox.Show(((MostRecentFile)e.SelectedItem).Name, "Most Recent File Selected:");
            try
            {
                manager.CloseProject();
                this.LoadProject(((MostRecentFile)e.SelectedItem).FullFileName);
            }
            catch { };
		}
		#endregion

		private void Grid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
		{
			if (manager.Pane == null) return;

			if (MainScrollViewer.Equals(e.OriginalSource))
			{
				manager.Pane.Unselect();
				e.Handled = true;
			}
		}

        private void MainScrollViewer_PreviewDrop(object sender, DragEventArgs e)
        {
            string FileName=FileDragAndDrop(e);
            if (menuFile_Open.IsEnabled)
            {
                if (FileName.ToLower().EndsWith(".bin"))
                    LoadProject(FileName.ToString());
                else
                    MessageBox.Show("bin파일이 아닙니다.");
            }
            else if (FileName.ToLower().EndsWith(".bin"))
                MessageBox.Show("사용중인 프로젝트를 먼저 닫아주세요.");
        }

        /// <summary>
        /// 드랍되는 파일 경로
        /// </summary>
        private string FileDragAndDrop(DragEventArgs e)
        {
            try
            {
                string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);
                string FileName = string.Empty;
                foreach (string file in files)
                {
                    FileName = file.ToString();
                }
                return FileName;
            }
            catch { }
            return String.Empty;
        }

        private void UI_Selection(UIElement UiE)
        {
            _selection.Clear();
            manager.Pane.Focus();
            _selection.Add(UiE);
            manager.Pane.Select(_selection.ToArray());
        }
    }
}