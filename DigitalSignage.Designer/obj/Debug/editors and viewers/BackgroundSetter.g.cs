﻿#pragma checksum "..\..\..\editors and viewers\BackgroundSetter.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "B3F015F77FE08582497A3C60EC46CA6A"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18444
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using WPFDesigner.Properties;


namespace WPFDesigner {
    
    
    /// <summary>
    /// BackgroundSetter
    /// </summary>
    public partial class BackgroundSetter : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 18 "..\..\..\editors and viewers\BackgroundSetter.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TabControl tabControl;
        
        #line default
        #line hidden
        
        
        #line 19 "..\..\..\editors and viewers\BackgroundSetter.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TabItem ImageBGTabItem;
        
        #line default
        #line hidden
        
        
        #line 21 "..\..\..\editors and viewers\BackgroundSetter.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button OpenImageB;
        
        #line default
        #line hidden
        
        
        #line 23 "..\..\..\editors and viewers\BackgroundSetter.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox TileModeCB;
        
        #line default
        #line hidden
        
        
        #line 24 "..\..\..\editors and viewers\BackgroundSetter.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox VAlignCB;
        
        #line default
        #line hidden
        
        
        #line 25 "..\..\..\editors and viewers\BackgroundSetter.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox HAlignCB;
        
        #line default
        #line hidden
        
        
        #line 26 "..\..\..\editors and viewers\BackgroundSetter.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox StretchModeCB;
        
        #line default
        #line hidden
        
        
        #line 27 "..\..\..\editors and viewers\BackgroundSetter.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Slider imageOpacity;
        
        #line default
        #line hidden
        
        
        #line 29 "..\..\..\editors and viewers\BackgroundSetter.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label1;
        
        #line default
        #line hidden
        
        
        #line 30 "..\..\..\editors and viewers\BackgroundSetter.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label2;
        
        #line default
        #line hidden
        
        
        #line 31 "..\..\..\editors and viewers\BackgroundSetter.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label3;
        
        #line default
        #line hidden
        
        
        #line 32 "..\..\..\editors and viewers\BackgroundSetter.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label4;
        
        #line default
        #line hidden
        
        
        #line 36 "..\..\..\editors and viewers\BackgroundSetter.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TabItem ColorBGTabItem;
        
        #line default
        #line hidden
        
        
        #line 38 "..\..\..\editors and viewers\BackgroundSetter.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Slider opacitySlider;
        
        #line default
        #line hidden
        
        
        #line 39 "..\..\..\editors and viewers\BackgroundSetter.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label5;
        
        #line default
        #line hidden
        
        
        #line 40 "..\..\..\editors and viewers\BackgroundSetter.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox gradientDirection;
        
        #line default
        #line hidden
        
        
        #line 45 "..\..\..\editors and viewers\BackgroundSetter.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button setTransparent;
        
        #line default
        #line hidden
        
        
        #line 46 "..\..\..\editors and viewers\BackgroundSetter.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label label6;
        
        #line default
        #line hidden
        
        
        #line 47 "..\..\..\editors and viewers\BackgroundSetter.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox gradientStyle;
        
        #line default
        #line hidden
        
        
        #line 52 "..\..\..\editors and viewers\BackgroundSetter.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton solidColorRB;
        
        #line default
        #line hidden
        
        
        #line 53 "..\..\..\editors and viewers\BackgroundSetter.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label solidLabel;
        
        #line default
        #line hidden
        
        
        #line 56 "..\..\..\editors and viewers\BackgroundSetter.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.RadioButton gradientRB;
        
        #line default
        #line hidden
        
        
        #line 58 "..\..\..\editors and viewers\BackgroundSetter.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label gradientLabel;
        
        #line default
        #line hidden
        
        
        #line 60 "..\..\..\editors and viewers\BackgroundSetter.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border stopsPanelBorder;
        
        #line default
        #line hidden
        
        
        #line 61 "..\..\..\editors and viewers\BackgroundSetter.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.StackPanel stopsPanel;
        
        #line default
        #line hidden
        
        
        #line 63 "..\..\..\editors and viewers\BackgroundSetter.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button addStop;
        
        #line default
        #line hidden
        
        
        #line 64 "..\..\..\editors and viewers\BackgroundSetter.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button removeStop;
        
        #line default
        #line hidden
        
        
        #line 69 "..\..\..\editors and viewers\BackgroundSetter.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.InkCanvas PreviewBackground;
        
        #line default
        #line hidden
        
        
        #line 70 "..\..\..\editors and viewers\BackgroundSetter.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Shapes.Rectangle backgroundRect;
        
        #line default
        #line hidden
        
        
        #line 72 "..\..\..\editors and viewers\BackgroundSetter.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button button1;
        
        #line default
        #line hidden
        
        
        #line 73 "..\..\..\editors and viewers\BackgroundSetter.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button button2;
        
        #line default
        #line hidden
        
        
        #line 74 "..\..\..\editors and viewers\BackgroundSetter.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button button3;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/WPFDesigner;component/editors%20and%20viewers/backgroundsetter.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\editors and viewers\BackgroundSetter.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 5 "..\..\..\editors and viewers\BackgroundSetter.xaml"
            ((WPFDesigner.BackgroundSetter)(target)).Loaded += new System.Windows.RoutedEventHandler(this.Window_Loaded);
            
            #line default
            #line hidden
            return;
            case 2:
            this.tabControl = ((System.Windows.Controls.TabControl)(target));
            return;
            case 3:
            this.ImageBGTabItem = ((System.Windows.Controls.TabItem)(target));
            return;
            case 4:
            this.OpenImageB = ((System.Windows.Controls.Button)(target));
            
            #line 21 "..\..\..\editors and viewers\BackgroundSetter.xaml"
            this.OpenImageB.Click += new System.Windows.RoutedEventHandler(this.OpenImageB_Click);
            
            #line default
            #line hidden
            return;
            case 5:
            this.TileModeCB = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 6:
            this.VAlignCB = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 7:
            this.HAlignCB = ((System.Windows.Controls.ComboBox)(target));
            return;
            case 8:
            this.StretchModeCB = ((System.Windows.Controls.ComboBox)(target));
            
            #line 26 "..\..\..\editors and viewers\BackgroundSetter.xaml"
            this.StretchModeCB.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.StretchModeCB_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 9:
            this.imageOpacity = ((System.Windows.Controls.Slider)(target));
            
            #line 27 "..\..\..\editors and viewers\BackgroundSetter.xaml"
            this.imageOpacity.ValueChanged += new System.Windows.RoutedPropertyChangedEventHandler<double>(this.imageOpacity_ValueChanged);
            
            #line default
            #line hidden
            return;
            case 10:
            this.label1 = ((System.Windows.Controls.Label)(target));
            return;
            case 11:
            this.label2 = ((System.Windows.Controls.Label)(target));
            return;
            case 12:
            this.label3 = ((System.Windows.Controls.Label)(target));
            return;
            case 13:
            this.label4 = ((System.Windows.Controls.Label)(target));
            return;
            case 14:
            this.ColorBGTabItem = ((System.Windows.Controls.TabItem)(target));
            return;
            case 15:
            this.opacitySlider = ((System.Windows.Controls.Slider)(target));
            
            #line 38 "..\..\..\editors and viewers\BackgroundSetter.xaml"
            this.opacitySlider.ValueChanged += new System.Windows.RoutedPropertyChangedEventHandler<double>(this.opacitySlider_ValueChanged);
            
            #line default
            #line hidden
            return;
            case 16:
            this.label5 = ((System.Windows.Controls.Label)(target));
            return;
            case 17:
            this.gradientDirection = ((System.Windows.Controls.ComboBox)(target));
            
            #line 40 "..\..\..\editors and viewers\BackgroundSetter.xaml"
            this.gradientDirection.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.gradientDirection_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 18:
            this.setTransparent = ((System.Windows.Controls.Button)(target));
            
            #line 45 "..\..\..\editors and viewers\BackgroundSetter.xaml"
            this.setTransparent.Click += new System.Windows.RoutedEventHandler(this.setTransparent_Click);
            
            #line default
            #line hidden
            return;
            case 19:
            this.label6 = ((System.Windows.Controls.Label)(target));
            return;
            case 20:
            this.gradientStyle = ((System.Windows.Controls.ComboBox)(target));
            
            #line 47 "..\..\..\editors and viewers\BackgroundSetter.xaml"
            this.gradientStyle.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.gradientStyle_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 21:
            this.solidColorRB = ((System.Windows.Controls.RadioButton)(target));
            
            #line 52 "..\..\..\editors and viewers\BackgroundSetter.xaml"
            this.solidColorRB.Checked += new System.Windows.RoutedEventHandler(this.solidColorRB_Checked);
            
            #line default
            #line hidden
            return;
            case 22:
            this.solidLabel = ((System.Windows.Controls.Label)(target));
            return;
            case 23:
            this.gradientRB = ((System.Windows.Controls.RadioButton)(target));
            
            #line 56 "..\..\..\editors and viewers\BackgroundSetter.xaml"
            this.gradientRB.Checked += new System.Windows.RoutedEventHandler(this.gradientRB_Checked);
            
            #line default
            #line hidden
            return;
            case 24:
            this.gradientLabel = ((System.Windows.Controls.Label)(target));
            return;
            case 25:
            this.stopsPanelBorder = ((System.Windows.Controls.Border)(target));
            return;
            case 26:
            this.stopsPanel = ((System.Windows.Controls.StackPanel)(target));
            return;
            case 27:
            this.addStop = ((System.Windows.Controls.Button)(target));
            
            #line 63 "..\..\..\editors and viewers\BackgroundSetter.xaml"
            this.addStop.Click += new System.Windows.RoutedEventHandler(this.addStop_Click);
            
            #line default
            #line hidden
            return;
            case 28:
            this.removeStop = ((System.Windows.Controls.Button)(target));
            
            #line 64 "..\..\..\editors and viewers\BackgroundSetter.xaml"
            this.removeStop.Click += new System.Windows.RoutedEventHandler(this.removeStop_Click);
            
            #line default
            #line hidden
            return;
            case 29:
            this.PreviewBackground = ((System.Windows.Controls.InkCanvas)(target));
            return;
            case 30:
            this.backgroundRect = ((System.Windows.Shapes.Rectangle)(target));
            return;
            case 31:
            this.button1 = ((System.Windows.Controls.Button)(target));
            
            #line 72 "..\..\..\editors and viewers\BackgroundSetter.xaml"
            this.button1.Click += new System.Windows.RoutedEventHandler(this.button1_Click);
            
            #line default
            #line hidden
            return;
            case 32:
            this.button2 = ((System.Windows.Controls.Button)(target));
            
            #line 73 "..\..\..\editors and viewers\BackgroundSetter.xaml"
            this.button2.Click += new System.Windows.RoutedEventHandler(this.button2_Click);
            
            #line default
            #line hidden
            return;
            case 33:
            this.button3 = ((System.Windows.Controls.Button)(target));
            
            #line 74 "..\..\..\editors and viewers\BackgroundSetter.xaml"
            this.button3.Click += new System.Windows.RoutedEventHandler(this.button3_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

