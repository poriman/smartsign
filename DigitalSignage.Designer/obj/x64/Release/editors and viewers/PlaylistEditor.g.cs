﻿#pragma checksum "..\..\..\..\editors and viewers\PlaylistEditor.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "09BACEE7CBD577581D839DAB43F62AA3"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.269
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using AC.AvalonControlsLibrary.Controls;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms.Integration;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using WPFDesigner;
using WPFDesigner.Properties;


namespace WPFDesigner {
    
    
    /// <summary>
    /// PlaylistEditor
    /// </summary>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
    public partial class PlaylistEditor : System.Windows.Window, System.Windows.Markup.IComponentConnector, System.Windows.Markup.IStyleConnector {
        
        
        #line 109 "..\..\..\..\editors and viewers\PlaylistEditor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label elementType;
        
        #line default
        #line hidden
        
        
        #line 111 "..\..\..\..\editors and viewers\PlaylistEditor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label elementName;
        
        #line default
        #line hidden
        
        
        #line 118 "..\..\..\..\editors and viewers\PlaylistEditor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListView ContentsListBox;
        
        #line default
        #line hidden
        
        
        #line 124 "..\..\..\..\editors and viewers\PlaylistEditor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox cboxLockDuration;
        
        #line default
        #line hidden
        
        
        #line 160 "..\..\..\..\editors and viewers\PlaylistEditor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button addItem;
        
        #line default
        #line hidden
        
        
        #line 161 "..\..\..\..\editors and viewers\PlaylistEditor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button removeItem;
        
        #line default
        #line hidden
        
        
        #line 162 "..\..\..\..\editors and viewers\PlaylistEditor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button upItem;
        
        #line default
        #line hidden
        
        
        #line 163 "..\..\..\..\editors and viewers\PlaylistEditor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button downItem;
        
        #line default
        #line hidden
        
        
        #line 168 "..\..\..\..\editors and viewers\PlaylistEditor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button OK;
        
        #line default
        #line hidden
        
        
        #line 169 "..\..\..\..\editors and viewers\PlaylistEditor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Apply;
        
        #line default
        #line hidden
        
        
        #line 170 "..\..\..\..\editors and viewers\PlaylistEditor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button Cancel;
        
        #line default
        #line hidden
        
        
        #line 172 "..\..\..\..\editors and viewers\PlaylistEditor.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Forms.Integration.WindowsFormsHost tempcontrol;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/WPFDesigner;component/editors%20and%20viewers/playlisteditor.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\editors and viewers\PlaylistEditor.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            
            #line 6 "..\..\..\..\editors and viewers\PlaylistEditor.xaml"
            ((WPFDesigner.PlaylistEditor)(target)).SizeChanged += new System.Windows.SizeChangedEventHandler(this.Window_SizeChanged);
            
            #line default
            #line hidden
            return;
            case 2:
            this.elementType = ((System.Windows.Controls.Label)(target));
            return;
            case 3:
            this.elementName = ((System.Windows.Controls.Label)(target));
            return;
            case 4:
            this.ContentsListBox = ((System.Windows.Controls.ListView)(target));
            return;
            case 5:
            this.cboxLockDuration = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 9:
            this.addItem = ((System.Windows.Controls.Button)(target));
            
            #line 160 "..\..\..\..\editors and viewers\PlaylistEditor.xaml"
            this.addItem.Click += new System.Windows.RoutedEventHandler(this.AddItem_Click);
            
            #line default
            #line hidden
            return;
            case 10:
            this.removeItem = ((System.Windows.Controls.Button)(target));
            
            #line 161 "..\..\..\..\editors and viewers\PlaylistEditor.xaml"
            this.removeItem.Click += new System.Windows.RoutedEventHandler(this.removeItem_Click);
            
            #line default
            #line hidden
            return;
            case 11:
            this.upItem = ((System.Windows.Controls.Button)(target));
            
            #line 162 "..\..\..\..\editors and viewers\PlaylistEditor.xaml"
            this.upItem.Click += new System.Windows.RoutedEventHandler(this.upItem_Click);
            
            #line default
            #line hidden
            return;
            case 12:
            this.downItem = ((System.Windows.Controls.Button)(target));
            
            #line 163 "..\..\..\..\editors and viewers\PlaylistEditor.xaml"
            this.downItem.Click += new System.Windows.RoutedEventHandler(this.downItem_Click);
            
            #line default
            #line hidden
            return;
            case 13:
            this.OK = ((System.Windows.Controls.Button)(target));
            
            #line 168 "..\..\..\..\editors and viewers\PlaylistEditor.xaml"
            this.OK.Click += new System.Windows.RoutedEventHandler(this.OKApplyCancelhandler);
            
            #line default
            #line hidden
            return;
            case 14:
            this.Apply = ((System.Windows.Controls.Button)(target));
            
            #line 169 "..\..\..\..\editors and viewers\PlaylistEditor.xaml"
            this.Apply.Click += new System.Windows.RoutedEventHandler(this.OKApplyCancelhandler);
            
            #line default
            #line hidden
            return;
            case 15:
            this.Cancel = ((System.Windows.Controls.Button)(target));
            
            #line 170 "..\..\..\..\editors and viewers\PlaylistEditor.xaml"
            this.Cancel.Click += new System.Windows.RoutedEventHandler(this.OKApplyCancelhandler);
            
            #line default
            #line hidden
            return;
            case 16:
            this.tempcontrol = ((System.Windows.Forms.Integration.WindowsFormsHost)(target));
            return;
            }
            this._contentLoaded = true;
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        void System.Windows.Markup.IStyleConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 6:
            
            #line 132 "..\..\..\..\editors and viewers\PlaylistEditor.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.btn_Infinity_Click);
            
            #line default
            #line hidden
            break;
            case 7:
            
            #line 136 "..\..\..\..\editors and viewers\PlaylistEditor.xaml"
            ((AC.AvalonControlsLibrary.Controls.TimePicker)(target)).SelectedTimeChanged += new AC.AvalonControlsLibrary.Controls.TimeSelectedChangedEventHandler(this.durationUI_SelectedTimeChanged);
            
            #line default
            #line hidden
            break;
            case 8:
            
            #line 147 "..\..\..\..\editors and viewers\PlaylistEditor.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.filePath_Click);
            
            #line default
            #line hidden
            break;
            }
        }
    }
}

