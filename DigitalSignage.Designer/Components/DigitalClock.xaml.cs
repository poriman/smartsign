﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Media.Animation;
using System.Windows.Threading;
using System.Globalization;
using DigitalSignage.Common;

namespace WPFDesigner
{
    /// <summary>
    /// Interaction logic for DigitalClock.xaml
    /// </summary>
    public partial class DigitalClock : UserControl
    {
        DispatcherTimer clockTimer;
//        TimeSpan ts;
        
        public DigitalClock()
        {
            InitializeComponent();
            gmt = TimeZoneInfo.Local;
            clockTimer = new DispatcherTimer();
            clockTimer.Interval = new TimeSpan(0, 0, 1);
            clockTimer.Tick += new EventHandler(clockTimer_Tick);
        }

        public TimeZoneInfo gmt;

        public void Start()
        {
			UpdateClock();
            clockTimer.Start();
        }
		private void UpdateClock()
		{
			DateTime dtTime = DateTime.UtcNow + gmt.BaseUtcOffset;
			TimeSpan ts2 = new TimeSpan(dtTime.Hour, dtTime.Minute, dtTime.Second);
// 			ts = new TimeSpan(DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second) - TimeZoneInfo.Local.BaseUtcOffset + gmt.BaseUtcOffset;
// 			TimeSpan ts2 = new TimeSpan(ts.Hours, ts.Minutes, ts.Seconds);
			timeLabel.Content = ts2;
		}

        void clockTimer_Tick(object sender, EventArgs e)
        {
			UpdateClock();
        }

        public void Stop()
        {
            clockTimer.Stop();
			timeLabel.Content = "00:00:00";
        }
    }
}
