﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DigitalSignage.Controls;
using DigitalSignage.Common;
using TVComponents;
using System.Runtime.InteropServices;

namespace WPFDesigner
{
    /// <summary>
    /// Interaction logic for TVComponent.xaml
    /// </summary>
    public partial class TVComponent : UserControl, IDesignElement
    {
		[DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
		public static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int x, int y, int cx, int cy, int flags);

		IntPtr HWND_TOP = IntPtr.Zero;
		
		private List<PlaylistItem> _playlist;
        private bool _IsPlay;
        PlaylistPlayer player;

        String Channel_values;
		/// <summary>
        /// Represents the set of changable properties
        /// </summary>
        Dictionary<string, object> _properties;
        TVComponents.ChannelList _channellist = null;
        TVComponents.TVControl ctrl_tv = null;

		public TVComponent()
        {
            InitializeComponent();

			_IsPlay = false;            
            _playlist = new List<PlaylistItem>();

			try
			{
				#region Device Setting And ChannelList Setting

				string pathToChannelXMLFile = AppDomain.CurrentDomain.BaseDirectory + @"settings\ChannelList.xml";
                _channellist = new TVComponents.ChannelList(pathToChannelXMLFile);

				#endregion

			}
			catch (Exception e)
			{
				iVisionLogHelper helper = iVisionLogHelper.GetInstance;
				helper.ScreenErrorLog("TV Component", ErrorCodes.IS_ERROR_UNSUPPT_CONTENT, e.Message);
			}

            SizeChanged += new SizeChangedEventHandler(FlashComponent_SizeChanged);
        }


        void FlashComponent_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            Host.Width = Math.Ceiling(this.Width);
            Host.Height = Math.Ceiling(this.Height);

            Binding b = new Binding();
            b.Source = GetElement;
            b.Mode = BindingMode.OneWay;
            PositionConvert converter = new PositionConvert();
            b.Converter = converter;
            ctrlName.SetBinding(TextBlock.TextProperty, b);
        }

        public IDesignElement GetElement
        {
            get
            {
                return this as IDesignElement;
            }
        }

        #region IDesignElement Members
        DigitalSignage.Common.TransformEffect IDesignElement.TransEffect
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }
        Dictionary<string, object> IDesignElement.Properties
        {
            get
            {
                ((IDesignElement)this).InitProperties();
                return _properties;
            }
            set
            {
                _properties = value;
                foreach (string name in _properties.Keys)
                {
                    PropertySetter.SetProperty(this, "IDesignElement", name, _properties[name]);
                }
            }
        }

		/// <summary>
		/// 재생이 시작된 시간
		/// </summary>
		public DateTime PlayStarted { get; set; }

        void IDesignElement.Play(TimeSpan ts)
        {
			if (Host != null)
			{
				SetWindowPos(Host.Handle, HWND_TOP, 0, 0, 0, 0, 3);
			}

           

			_IsPlay = true;
			//	경계라인
			sepLine.Visibility = Visibility.Collapsed;
            ctrlName.Visibility = Visibility.Collapsed;
			Host.Visibility = Visibility.Visible;

            ctrl_tv = new TVComponents.TVControl();// TVComponents.TVControl.Instance;
            Host.Child = ctrl_tv;
            //ctrl_tv.Dock = System.Windows.Forms.DockStyle.Fill;

            player = new PlaylistPlayer(this);
            player.Start(ts);
        }

        void IDesignElement.Play()
        {
            ((IDesignElement)this).Play(TimeSpan.Zero);
        }

        void IDesignElement.Seek(TimeSpan ts)
        {
            if (player != null) player.Seek(ts);
        }



		void IDesignElement.Pause()
		{
			try
			{
				player.Stop();
			}
			catch { }
		}

        bool IDesignElement.ISPlay
        {
            get { return _IsPlay; }
        }

        void IDesignElement.Stop()
        {
			_IsPlay = false;
			//	경계라인
			sepLine.Visibility = Visibility.Visible;
			ctrlName.Visibility = Visibility.Visible;
            if(player != null) player.Stop();
            if (ctrl_tv != null)
            {
                ctrl_tv.Stop();
            }
			if (Host != null) Host.Visibility = Visibility.Collapsed;
            GC.Collect();
            this.Visibility = System.Windows.Visibility.Visible;
        }

        void IDesignElement.InitProperties()
        {
            _properties = new Dictionary<string, object>();
            IDesignElement child = this as IDesignElement;

            _properties.Add("Name", child.Name);
            _properties.Add("Width", child.Width);
            _properties.Add("Height", child.Height);
            _properties.Add("Left", child.Left);
            _properties.Add("Top", child.Top);
            _properties.Add("HorizontalAlignment", child.HorizontalAlignment);
            _properties.Add("VerticalAlignment", child.VerticalAlignment);
            _properties.Add("ZIndex", child.ZIndex);

            _properties.Add("Type", child.Type);
            _properties.Add("Playlist", child.Playlist);
            _properties.Add("IsMuted", child.Mute);
            _properties.Add("Volume", child.Volume);
        }

        string IDesignElement.Name
        {
            get
            {
                return Name;
            }
            set
            {
                Name = value;

                Binding b = new Binding();
                b.Source = GetElement;
                b.Mode = BindingMode.OneWay;
                PositionConvert converter = new PositionConvert();
                b.Converter = converter;
                ctrlName.SetBinding(TextBlock.TextProperty, b);
            }
        }

        double IDesignElement.Width
        {
            get
            {
                return ActualWidth;
            }
            set
            {
				Width = Math.Ceiling(value);

				Host.Width = Math.Ceiling(value);
            }
        }

        double IDesignElement.Height
        {
            get
            {
                return ActualHeight;
            }
            set
            {
				Height = Math.Ceiling(value);
				Host.Height = Math.Ceiling(value);
            }
        }

        double IDesignElement.Left
        {
            get
            {
                return (double)this.GetValue(InkCanvas.LeftProperty);
            }
            set
            {
				this.SetValue(InkCanvas.LeftProperty, Math.Floor(value));
            }
        }

        double IDesignElement.Top
        {
            get
            {
                return (double)this.GetValue(InkCanvas.TopProperty);
            }
            set
            {
				SetValue(InkCanvas.TopProperty, Math.Floor(value));
            }
        }

        double IDesignElement.Bottom
        {
            get
            {
                return (double)this.GetValue(InkCanvas.BottomProperty);
            }
            set
            {
				this.SetValue(InkCanvas.BottomProperty, Math.Ceiling(value));
            }
        }

        double IDesignElement.Right
        {
            get
            {
                return (double)this.GetValue(InkCanvas.RightProperty);
            }
            set
            {
				SetValue(InkCanvas.RightProperty, Math.Ceiling(value));
            }
        }

        int IDesignElement.ZIndex
        {
            get
            {
                return (int)this.GetValue(Canvas.ZIndexProperty);
            }
            set
            {
                this.SetValue(Canvas.ZIndexProperty, value);
            }
        }

        Brush IDesignElement.BorderBrush
        {
            get;
            set;
        }

        double IDesignElement.BorderThickness
        {
            get;
            set;
        }

        double IDesignElement.BorderCorner
        {
            get;
            set;
        }

        double IDesignElement.Opacity
        {
            get
            {
                return Opacity;
            }
            set
            {
                Opacity = value;
            }
        }

        HorizontalAlignment IDesignElement.HorizontalAlignment
        {
            get
            {
                return this.HorizontalAlignment;
            }
            set
            {
                this.HorizontalAlignment = value;
            }
        }

        VerticalAlignment IDesignElement.VerticalAlignment
        {
            get
            {
                return this.VerticalAlignment;
            }
            set
            {
                this.VerticalAlignment = value;
            }
        }

        Stretch IDesignElement.Stretch
        {
            get;
            set;
        }

        double IDesignElement.Volume
        {
            get;
            set;
        }

		TimeSpan IDesignElement.RefreshInterval
		{
            get;
            set;
		}

        bool IDesignElement.Mute
        {
            get;
            set;
        }

        Brush IDesignElement.Background
        {
            get;
            set;
        }

        FontFamily IDesignElement.FontFamily
        {
            get;
            set;
        }

        double IDesignElement.FontSize
        {
            get;
            set;
        }

        FontWeight IDesignElement.FontWeight
        {
            get;
            set;
        }

        Brush IDesignElement.Foreground
        {
            get;
            set;
        }

        int[] IDesignElement.StrokesLength
        {
            get;
            set;
        }

        PenLineCap IDesignElement.StrokeDashCap
        {
            get;
            set;
        }

        char IDesignElement.SeparatorChar
        {
            get;
            set;
        }

        Point IDesignElement.AspectRatio
        {
            get;
            set;
        }

        Type IDesignElement.Type
        {
            get
            {
                return GetType();
            }
        }

        List<PlaylistItem> IDesignElement.Playlist
        {
            get
            {
                return _playlist;
            }
            set
            {
                _playlist = value;
            }
        }

        string IDesignElement.Content
        {
            get
            {
                return Channel_values;
            }
            set
            {
                this.UpdateLayout();
				if (ctrl_tv != null)
					ParcingNPlay(value);

                Channel_values = value;
            }
        }

		void ParcingNPlay(String values)
		{
			String[] arrValues = values.Split(';');

            TVComponents.Channel ch = null;

            if (arrValues[0] == "1")
            {
                ch = new TVComponents.Channel()
                {
                    SourceType = AVSOURCE.SRC_COMPONENT,
                    MajorNum = 0,
                    MinorNum = 0,
                    ChannelName = "COMPONENT"
                };
            }
            else if (arrValues[0] == "2")
            {
                ch = new TVComponents.Channel()
                {
                    SourceType = AVSOURCE.SRC_SVIDEO,
                    MajorNum = 0,
                    MinorNum = 0,
                    ChannelName = "SVIDEO"
                };
            }
            else
            {
                ch = this._channellist.FindChannel(arrValues[1]);
            }

            ctrl_tv.ChannelInfo = ch;
            ctrl_tv.Play((int)Host.Width, (int)Host.Height);
        }

        IMediaFilesManager IDesignElement.FileManager
        {
            get { return Parent as IMediaFilesManager; }
        }

		System.Drawing.Bitmap IDesignElement.GetThumbnail(int cx, int cy)
		{
			if (ctrl_tv != null)
				return ctrl_tv.Thumbnail(cx, cy);

			return null;
		}

        #endregion

		private void UserControl_Unloaded(object sender, RoutedEventArgs e)
		{
			try
			{
				if(Host != null) Host.Child = null;
				if (ctrl_tv != null)
				{
					try
					{
						ctrl_tv.Stop();
					}
					catch { }
					try
					{
						ctrl_tv.Dispose();
					}
					catch {}
					ctrl_tv = null;

				}
				if(Host != null)
				{
					Host.Dispose();
					Host = null;
				}

			}
			catch
			{
			}
		}
    }
}