﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DigitalSignage.Controls;
using DigitalSignage.Common;

using System.Runtime.InteropServices;

namespace WPFDesigner
{
    /// <summary>
    /// Interaction logic for FlashComponent.xaml
    /// </summary>
    public partial class FlashComponent : UserControl, IDesignElement
    {
		[DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
		public static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int x, int y, int cx, int cy, int flags);

		IntPtr HWND_TOP = IntPtr.Zero;

		private List<PlaylistItem> _playlist;
        private bool _IsPlay;
        PlaylistPlayer player;
        //FlashHost _hwnd;
//         FlashControl flashViewer;
        /// <summary>
        /// Represents the set of changable properties
        /// </summary>
        Dictionary<string, object> _properties;

        public FlashComponent()
        {
            InitializeComponent();
//             flashViewer = new FlashControl();    
//             Host.Child = flashViewer;
            _IsPlay = false;            
            _playlist = new List<PlaylistItem>();

			//	hsshin 외부데이터에 들어가는 refdata 경로를 넣는다
			this.flashViewer.RefDataPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + @"\PlayerData\refData\";
			
			try
			{
				this.flashViewer.OnContextOpen += new EventHandler(flashViewer_OnContextOpen);
			}
			catch { }

            SizeChanged += new SizeChangedEventHandler(FlashComponent_SizeChanged);
        }

		void flashViewer_OnContextOpen(object sender, EventArgs e)
		{
			if (ContextMenu != null)
			{
				this.ContextMenu.IsOpen = true;
			}
		}


        void FlashComponent_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            Host.Width = Math.Ceiling(Width);
            Host.Height = Math.Ceiling(Height);

            Binding b = new Binding();
            b.Source = GetElement;
            b.Mode = BindingMode.OneWay;
            PositionConvert converter = new PositionConvert();
            b.Converter = converter;
            ctrlName.SetBinding(TextBlock.TextProperty, b);
        }

        public IDesignElement GetElement
        {
            get
            {
                return this as IDesignElement;
            }
        }

        #region IDesignElement Members
        DigitalSignage.Common.TransformEffect IDesignElement.TransEffect
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }
        Dictionary<string, object> IDesignElement.Properties
        {
            get
            {
                ((IDesignElement)this).InitProperties();
                return _properties;
            }
            set
            {
                _properties = value;
                foreach (string name in _properties.Keys)
                {
                    PropertySetter.SetProperty(this, "IDesignElement", name, _properties[name]);
                }
            }
        }

		/// <summary>
		/// 재생이 시작된 시간
		/// </summary>
		public DateTime PlayStarted { get; set; }

        void IDesignElement.Play(TimeSpan ts)
        {
			if (Host != null)
			{
				// Unfortunately, we need to pinvoke into SetWindowPos API to get this working.
				SetWindowPos(Host.Handle, HWND_TOP, 0, 0, 0, 0, 3);
			}

			_IsPlay = true;
			//	경계라인
			sepLine.Visibility = Visibility.Collapsed;
            ctrlName.Visibility = Visibility.Collapsed;

            player = new PlaylistPlayer(this);
            player.Start(ts);

            Host.Visibility = Visibility.Visible;
        }

        void IDesignElement.Play()
        {
            ((IDesignElement)this).Play(TimeSpan.Zero);
        }

        void IDesignElement.Seek(TimeSpan ts)
        {
            if (player != null) player.Seek(ts);
        }


		void IDesignElement.Pause()
		{
			try
			{
				player.Stop();
				flashViewer.Pause();
			}
			catch { }
		}


        bool IDesignElement.ISPlay
        {
            get { return _IsPlay; }
        }

        void IDesignElement.Stop()
        {
			_IsPlay = false;
			//	경계라인
			sepLine.Visibility = Visibility.Visible;
			ctrlName.Visibility = Visibility.Visible;
			if (Host != null) Host.Visibility = Visibility.Collapsed;
            if(player != null) player.Stop();
			if (flashViewer != null)
			{
				flashViewer.Stop();
				flashViewer.Clear();
			}
        }

        void IDesignElement.InitProperties()
        {
            _properties = new Dictionary<string, object>();
            IDesignElement child = this as IDesignElement;

            _properties.Add("Name", child.Name);
            _properties.Add("Width", child.Width);
            _properties.Add("Height", child.Height);
            _properties.Add("Left", child.Left);
            _properties.Add("Top", child.Top);
			_properties.Add("ZIndex", child.ZIndex);
			_properties.Add("HorizontalAlignment", child.HorizontalAlignment);
            _properties.Add("VerticalAlignment", child.VerticalAlignment);
            _properties.Add("Type", child.Type);
            _properties.Add("Playlist", child.Playlist);
//             _properties.Add("Opacity", child.Opacity);
        }

        string IDesignElement.Name
        {
            get
            {
                return Name;
            }
            set
            {
                Name = value;

                Binding b = new Binding();
                b.Source = GetElement;
                b.Mode = BindingMode.OneWay;
                PositionConvert converter = new PositionConvert();
                b.Converter = converter;
                ctrlName.SetBinding(TextBlock.TextProperty, b);
            }
        }

        double IDesignElement.Width
        {
            get
            {
                return ActualWidth;
            }
            set
            {
				Width = Math.Ceiling(value);

				Host.Width = Math.Ceiling(value);
            }
        }

        double IDesignElement.Height
        {
            get
            {
                return ActualHeight;
            }
            set
            {
				Height = Math.Ceiling(value);
				Host.Height = Math.Ceiling(value);
            }
        }

        double IDesignElement.Left
        {
            get
            {
                return (double)this.GetValue(InkCanvas.LeftProperty);
            }
            set
            {
				this.SetValue(InkCanvas.LeftProperty, Math.Floor(value));
            }
        }

        double IDesignElement.Top
        {
            get
            {
                return (double)this.GetValue(InkCanvas.TopProperty);
            }
            set
            {
				SetValue(InkCanvas.TopProperty, Math.Floor(value));
            }
        }

        double IDesignElement.Bottom
        {
            get
            {
                return (double)this.GetValue(InkCanvas.BottomProperty);
            }
            set
            {
				this.SetValue(InkCanvas.BottomProperty, Math.Ceiling(value));
            }
        }

        double IDesignElement.Right
        {
            get
            {
                return (double)this.GetValue(InkCanvas.RightProperty);
            }
            set
            {
				SetValue(InkCanvas.RightProperty, Math.Ceiling(value));
            }
        }

        int IDesignElement.ZIndex
        {
            get
            {
                return (int)this.GetValue(Canvas.ZIndexProperty);
            }
            set
            {
                this.SetValue(Canvas.ZIndexProperty, value);
            }
        }

        Brush IDesignElement.BorderBrush
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        double IDesignElement.BorderThickness
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        double IDesignElement.BorderCorner
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

		TimeSpan IDesignElement.RefreshInterval
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
				throw new NotImplementedException();
			}
		}

        double IDesignElement.Opacity
        {
            get
            {
                return Opacity;
            }
            set
            {
                Opacity = value;
            }
        }

        HorizontalAlignment IDesignElement.HorizontalAlignment
        {
            get
            {
                return this.HorizontalAlignment;
            }
            set
            {
                this.HorizontalAlignment = value;
            }
        }

        VerticalAlignment IDesignElement.VerticalAlignment
        {
            get
            {
                return this.VerticalAlignment;
            }
            set
            {
                this.VerticalAlignment = value;
            }
        }

        Stretch IDesignElement.Stretch
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        double IDesignElement.Volume
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        bool IDesignElement.Mute
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        Brush IDesignElement.Background
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        FontFamily IDesignElement.FontFamily
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        double IDesignElement.FontSize
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        FontWeight IDesignElement.FontWeight
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        Brush IDesignElement.Foreground
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        int[] IDesignElement.StrokesLength
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        PenLineCap IDesignElement.StrokeDashCap
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        char IDesignElement.SeparatorChar
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        Point IDesignElement.AspectRatio
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        Type IDesignElement.Type
        {
            get
            {
                return GetType();
            }
        }

        List<PlaylistItem> IDesignElement.Playlist
        {
            get
            {
                return _playlist;
            }
            set
            {
                _playlist = value;
            }
        }

        string IDesignElement.Content
        {
            get
            {
                return "";
            }
            set
            {
                string path = (Parent as IMediaFilesManager).GetFullFilePathByName(value);
                flashViewer.Stop();
                if (flashViewer != null)
                    flashViewer.Play(path);
                /*
                if (_hwnd == null) return;
                  _hwnd.Play((Parent as IMediaFilesManager).GetFullFilePathByName(value));
                 * */
            }
        }

        IMediaFilesManager IDesignElement.FileManager
        {
            get { return Parent as IMediaFilesManager; }
        }

		System.Drawing.Bitmap IDesignElement.GetThumbnail(int cx, int cy)
		{
			if (flashViewer != null)
				return flashViewer.Thumbnail(cx, cy);

			return null;
		}

        #endregion

		private void UserControl_Unloaded(object sender, RoutedEventArgs e)
		{
			try
			{
				if(Host != null) Host.Child = null;
				if(flashViewer != null)
				{
					try
					{
						flashViewer.Stop();
					}
					catch { }
					try
					{
						flashViewer.Dispose();
					}
					catch {}
					flashViewer = null;

				}
				if(Host != null)
				{
					Host.Dispose();
					Host = null;
				}

			}
			catch
			{
			}
		}
    }
}