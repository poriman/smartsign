﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Media.Media3D;
using DigitalSignage.Common;
using System.Reflection;
using System.IO;

using System.Threading;
using System.Windows.Media.Animation;
using System.Windows.Threading;

namespace WPFDesigner
{
    /// <summary>
    /// Represents an image
    /// </summary>
    public partial class ImageComponent : UserControl, IDesignElement
    {
        Window layerWindow = null;

        /// <summary>
        /// Represents the playlist of this element
        /// </summary>
        private List<PlaylistItem> _playlist;
        private bool _IsPlay;
        PlaylistPlayer player;
       // BitmapImage _bi;

 //       bool isFirst = false;
        /// <summary>
        /// Represents the set of changable properties
        /// </summary>
        Dictionary<string, object> _properties;

        /// <summary>
        /// Initializes the new ImageComponent class instance
        /// </summary>
        public ImageComponent()
        {
            InitializeComponent();
            _IsPlay = false;
            ((IDesignElement)this).InitProperties();
            this.SizeChanged += new SizeChangedEventHandler(ImageComponent_SizeChanged);
            sub_rect.Width = this.ActualWidth;
            sub_rect.Height = this.ActualHeight;
            rect.StrokeThickness = 0.0;
            rect.Stroke = new SolidColorBrush(Color.FromArgb(255, 0, 0, 0));
            sub_rect.StrokeThickness = 0.0;
            sub_rect.Stroke = new SolidColorBrush(Color.FromArgb(255, 0, 0, 0));
			rect.Visibility = Visibility.Hidden;

			_playlist = new List<PlaylistItem>();
            Width = 150;
            Height = 150;
        }

        void ImageComponent_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            Binding b = new Binding();
            b.Source = GetElement;
            b.Mode = BindingMode.OneWay;
            PositionConvert converter = new PositionConvert();
            b.Converter = converter;
            ctrlName.SetBinding(TextBlock.TextProperty, b);

            #region Windowed 모드

            if (Screen.IsLayeredComponent)
            {
                try
                {
                    layerWindow.Width = this.ActualWidth;
                    layerWindow.Height = this.ActualHeight;
                }
                catch { }
            }

            #endregion
        }

        public IDesignElement GetElement
        {
            get
            {
                return this as IDesignElement;
            }
        }

        #region IDesignElement Members

        TransformEffect IDesignElement.TransEffect
        {
            get;
            set;
        }

		/// <summary>
		/// 재생이 시작된 시간
		/// </summary>
		public DateTime PlayStarted { get; set; }

        void IDesignElement.Play(TimeSpan ts)
        {
			if (!_IsPlay)
			{

                #region Windowed 모드

                if (Screen.IsLayeredComponent)
                {
                    layerWindow = new Window();
                    layerWindow.ShowInTaskbar = true;
                    layerWindow.Topmost = true;
                    layerWindow.WindowStyle = WindowStyle.None;
                    layerWindow.Background = Brushes.Transparent;
                    layerWindow.AllowsTransparency = true;

                    Point relativePoint = this.PointToScreen(new Point(0, 0));
                    layerWindow.Left = relativePoint.X;
                    layerWindow.Top = relativePoint.Y;
                    layerWindow.Width = mainGrid.ActualWidth;
                    layerWindow.Height = mainGrid.ActualHeight;
                    UIElement ue = subGrid.Children[0];
                    subGrid.Children.Clear();

                    layerWindow.Content = ue;
                    layerWindow.Show();
                }
                #endregion

				//	경계라인
				sepLine.Visibility = Visibility.Collapsed;

				player = new PlaylistPlayer(this);
				player.Start(ts);
				ctrlName.Visibility = Visibility.Collapsed;
				_IsPlay = true;

            }
        }

        void IDesignElement.Play()
        {
            ((IDesignElement)this).Play(TimeSpan.Zero);
        }

        void IDesignElement.Seek(TimeSpan ts)
        {
            if (player != null) player.Seek(ts);
        }


		void IDesignElement.Pause()
		{
			try
			{
				player.Stop();
			}
			catch { }
		}


        bool IDesignElement.ISPlay
        {
            get { return _IsPlay; }
        }

        void IDesignElement.Stop()
        {
            #region Windowed 모드 종료

            if (Screen.IsLayeredComponent)
            {
                UIElement ue = layerWindow.Content as UIElement;
                if (ue != null)
                {
                    layerWindow.Content = null;

                    subGrid.Children.Add(ue);

                    layerWindow.Close();
                    layerWindow = null;
                }
            }
            #endregion

            //	경계라인
			sepLine.Visibility = Visibility.Visible;
			
			if (player != null) player.Stop();
            ctrlName.Visibility = Visibility.Visible;
			image.ImageSource = null;
            sub_image.ImageSource = null;
			_IsPlay = false;
        }

        Dictionary<string, object> IDesignElement.Properties
        {
            get
            {
                ((IDesignElement)this).InitProperties();
                return _properties;
            }
            set
            {
                _properties = value;
                foreach (string name in _properties.Keys)
                {
                    PropertySetter.SetProperty(this, "IDesignElement", name, _properties[name]);
                }
            }
        }

        void IDesignElement.InitProperties()
        {
            _properties = new Dictionary<string, object>();
            IDesignElement child = this as IDesignElement;

            _properties.Add("Name", child.Name);
            _properties.Add("Width", child.Width);
            _properties.Add("Height", child.Height);
            _properties.Add("Left", child.Left);
            _properties.Add("Top", child.Top);
            _properties.Add("HorizontalAlignment", child.HorizontalAlignment);
            _properties.Add("VerticalAlignment", child.VerticalAlignment);
            _properties.Add("Opacity", child.Opacity);
            _properties.Add("ZIndex", child.ZIndex);
            _properties.Add("BorderBrush", child.BorderBrush);
            _properties.Add("BorderThickness", child.BorderThickness);
            _properties.Add("BorderCorner", child.BorderCorner);
            _properties.Add("Stretch", child.Stretch);
            _properties.Add("Type", child.Type);
            _properties.Add("Playlist", child.Playlist);
            _properties.Add("TransEffect", child.TransEffect);
        }

        string IDesignElement.Name
        {
            get
            {
                return Name;
            }
            set
            {
                Name = value;

                Binding b = new Binding();
                b.Source = GetElement;
                b.Mode = BindingMode.OneWay;
                PositionConvert converter = new PositionConvert();
                b.Converter = converter;
                ctrlName.SetBinding(TextBlock.TextProperty, b);
            }
        }

        double IDesignElement.Width
        {
            get
            {
                return ActualWidth;
            }
            set
            {
                Width = value;
            }
        }

        double IDesignElement.Height
        {
            get
            {
                return ActualHeight;
            }
            set
            {
                Height = value;
            }
        }

        double IDesignElement.Left
        {
            get
            {
                return (double)this.GetValue(InkCanvas.LeftProperty);
            }
            set
            {
                this.SetValue(InkCanvas.LeftProperty, value);
            }
        }

        double IDesignElement.Top
        {
            get
            {
                return (double)this.GetValue(InkCanvas.TopProperty);
            }
            set
            {
                SetValue(InkCanvas.TopProperty, value);
            }
        }

        double IDesignElement.Bottom
        {
            get
            {
                return (double)this.GetValue(InkCanvas.BottomProperty);
            }
            set
            {
                this.SetValue(InkCanvas.BottomProperty, value);
            }
        }

        double IDesignElement.Right
        {
            get
            {
                return (double)this.GetValue(InkCanvas.RightProperty);
            }
            set
            {
                SetValue(InkCanvas.RightProperty, value);
            }
        }

        int IDesignElement.ZIndex
        {
            get
            {
                return (int)this.GetValue(Canvas.ZIndexProperty);
            }
            set
            {
                this.SetValue(Canvas.ZIndexProperty, value);
            }
        }

        Brush IDesignElement.BorderBrush
        {
            get
            {
                return this.sub_rect.Stroke;
            }
            set
            {
                this.sub_rect.Stroke = value;
                this.rect.Stroke = value;
            }
        }

        double IDesignElement.BorderThickness
        {
            get
            {
                return sub_rect.StrokeThickness;
            }
            set
            {
                sub_rect.StrokeThickness = value;
                rect.StrokeThickness = value;
            }
        }

        double IDesignElement.BorderCorner
        {
            get
            {
                return sub_rect.RadiusX;
            }
            set
            {
                sub_rect.RadiusX = sub_rect.RadiusY = value;
                rect.RadiusX = rect.RadiusY = value;
				sepLine.RadiusX = sepLine.RadiusY = value;

            }
        }

        double IDesignElement.Opacity
        {
            get
            {
                return this.Opacity;
            }
            set
            {
                this.Opacity = value;
            }
        }

        Stretch IDesignElement.Stretch
        {
            get
            {
                return image.Stretch;
            }
            set
            {
				image.Stretch = value;
			}
        }

        HorizontalAlignment IDesignElement.HorizontalAlignment
        {
            get
            {
                return this.HorizontalAlignment;
            }
            set
            {
                this.HorizontalAlignment = value;
            }
        }

        VerticalAlignment IDesignElement.VerticalAlignment
        {
            get
            {
                return this.VerticalAlignment;
            }
            set
            {
                this.VerticalAlignment = value;
            }
        }

        double IDesignElement.Volume
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        bool IDesignElement.Mute
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

		TimeSpan IDesignElement.RefreshInterval
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
				throw new NotImplementedException();
			}
		}

        Brush IDesignElement.Background
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        FontFamily IDesignElement.FontFamily
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        double IDesignElement.FontSize
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        FontWeight IDesignElement.FontWeight
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        Brush IDesignElement.Foreground
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        int[] IDesignElement.StrokesLength
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        PenLineCap IDesignElement.StrokeDashCap
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        char IDesignElement.SeparatorChar
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        Point IDesignElement.AspectRatio
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        Type IDesignElement.Type
        {
            get
            {
                return GetType();
            }
        }

        List<PlaylistItem> IDesignElement.Playlist
        {
            get
            {
                return _playlist;
            }
            set
            {
                _playlist = value;
            }
        }

        void MyDoubleAnimation_Completed(object sender, EventArgs e)
        {
            image.ImageSource = null;          
        }

        public void DoubleAnimation_Top()
        {
            //더블 애니메이션 하나 설정 했다. 
            DoubleAnimation MyDoubleAnimation = new DoubleAnimation();

            MyDoubleAnimation.From = -this.Height;
            MyDoubleAnimation.To = 0;
            //가속도값 설정하기 
            BounceEase be = new BounceEase();
            be.Bounciness = 15;
            be.Bounces = 1;

            CubicEase ease = new CubicEase();
            ease.EasingMode = EasingMode.EaseIn;
            MyDoubleAnimation.EasingFunction = ease;

            MyDoubleAnimation.Duration = new Duration(TimeSpan.FromSeconds(0.5));
            //애니메이션 효과를 적용한 후에는 속성 값을 변경하기 
            MyDoubleAnimation.FillBehavior = FillBehavior.Stop;
            MyDoubleAnimation.Completed += new EventHandler(MyDoubleAnimation_Completed);
            sub_rect.BeginAnimation(Canvas.TopProperty, MyDoubleAnimation);
        }

        public void DoubleAnimation_Bottom()
        {
            //더블 애니메이션 하나 설정 했다. 
            DoubleAnimation MyDoubleAnimation = new DoubleAnimation();
            MyDoubleAnimation.From = -this.Height;
            MyDoubleAnimation.To = 0;
            //가속도값 설정하기 
            BounceEase be = new BounceEase();
            be.Bounciness = 15;
            be.Bounces = 1;

            CubicEase ease = new CubicEase();
            ease.EasingMode = EasingMode.EaseIn;
            MyDoubleAnimation.EasingFunction = ease;

            MyDoubleAnimation.Duration = new Duration(TimeSpan.FromSeconds(0.5));
            //애니메이션 효과를 적용한 후에는 속성 값을 변경하기 
            MyDoubleAnimation.FillBehavior = FillBehavior.Stop;
            MyDoubleAnimation.Completed += new EventHandler(MyDoubleAnimation_Completed);
            sub_rect.BeginAnimation(Canvas.BottomProperty, MyDoubleAnimation);
        }

        public void DoubleAnimation_Left()
        {
            //더블 애니메이션 하나 설정 했다. 
            DoubleAnimation MyDoubleAnimation = new DoubleAnimation();
            //Image IMG = MyEllipseCanvas.Children[0] as Image;
            MyDoubleAnimation.From = -this.Width;
            MyDoubleAnimation.To = 0;
            //가속도값 설정하기 
            BounceEase be = new BounceEase();
            be.Bounciness = 15;
            be.Bounces = 1;

            CubicEase ease = new CubicEase();
            ease.EasingMode = EasingMode.EaseIn;
            MyDoubleAnimation.EasingFunction = ease;

            MyDoubleAnimation.Duration = new Duration(TimeSpan.FromSeconds(0.5));
            //애니메이션 효과를 적용한 후에는 속성 값을 변경하기 
            //애니메이션 효과를 적용한 후에는 속성 값을 변경하기 
            MyDoubleAnimation.FillBehavior = FillBehavior.Stop;
            MyDoubleAnimation.Completed += new EventHandler(MyDoubleAnimation_Completed);

            sub_rect.BeginAnimation(Canvas.LeftProperty, MyDoubleAnimation);

        }

        public void DoubleAnimation_Right()
        {
            //더블 애니메이션 하나 설정 했다. 
            DoubleAnimation MyDoubleAnimation = new DoubleAnimation();
            MyDoubleAnimation.From = -this.Width;
            MyDoubleAnimation.To = 0;
            //가속도값 설정하기 
            BounceEase be = new BounceEase();
            be.Bounciness = 15;
            be.Bounces = 1;

            CubicEase ease = new CubicEase();
            ease.EasingMode = EasingMode.EaseIn;
            MyDoubleAnimation.EasingFunction = ease;

            MyDoubleAnimation.Duration = new Duration(TimeSpan.FromSeconds(0.5));
            //애니메이션 효과를 적용한 후에는 속성 값을 변경하기 
            //애니메이션 효과를 적용한 후에는 속성 값을 변경하기 
            MyDoubleAnimation.FillBehavior = FillBehavior.Stop; 
            MyDoubleAnimation.Completed += new EventHandler(MyDoubleAnimation_Completed);

            sub_rect.BeginAnimation(Canvas.RightProperty, MyDoubleAnimation);
        }

        public void DoubleAnimation_Fadein()
        {
            //더블 애니메이션 하나 설정 했다. 
            DoubleAnimation MyDoubleAnimation = new DoubleAnimation();
            MyDoubleAnimation.From = 0;
            MyDoubleAnimation.To = 1;

            MyDoubleAnimation.Duration = new Duration(TimeSpan.FromSeconds(0.5));
            //애니메이션 효과를 적용한 후에는 속성 값을 변경하기 
            //애니메이션 효과를 적용한 후에는 속성 값을 변경하기 
            MyDoubleAnimation.FillBehavior = FillBehavior.Stop;
            MyDoubleAnimation.Completed += new EventHandler(MyDoubleAnimation_Completed);
            sub_rect.BeginAnimation(Canvas.OpacityProperty, MyDoubleAnimation);

            //더블 애니메이션 하나 설정 했다. 
            DoubleAnimation MyDoubleAnimation2 = new DoubleAnimation();
            MyDoubleAnimation2.From = 1;
            MyDoubleAnimation2.To = 0;

            MyDoubleAnimation2.Duration = new Duration(TimeSpan.FromSeconds(0.5));
            //애니메이션 효과를 적용한 후에는 속성 값을 변경하기 
            //애니메이션 효과를 적용한 후에는 속성 값을 변경하기 
            MyDoubleAnimation2.FillBehavior = FillBehavior.Stop;
            rect.BeginAnimation(Canvas.OpacityProperty, MyDoubleAnimation2);
        }

        string IDesignElement.Content
        {
			get
			{
				return ((BitmapImage)sub_image.ImageSource).UriSource.LocalPath;
			}
            set
            {
                Dispatcher.Invoke(DispatcherPriority.Normal, new Action(delegate
                {
                    BitmapImage _bi = new BitmapImage();

                    if (value == "")
                    {
                        ctrlName.Visibility = Visibility.Visible;
                        image.ImageSource = null;
                        return;
                    }

                    _bi.DecodePixelHeight = Convert.ToInt32(rect.ActualHeight);
                    _bi.DecodePixelWidth = Convert.ToInt32(rect.ActualWidth);

                    _bi.BeginInit();
                    _bi.UriSource = new Uri(((IDesignElement)this).FileManager.GetFullFilePathByName(value), UriKind.RelativeOrAbsolute);
                    _bi.EndInit();

                    image.ImageSource = sub_image.ImageSource;

                    sub_image.ImageSource = _bi;
                    sub_rect.Width = this.Width;
                    sub_rect.Height = this.Height;

                    rect.Width = this.Width;
                    rect.Height = this.Height;
                        
                    rect.Visibility = Visibility.Visible;
                    ApplyTransformEffect();
                }));
            }
        }

        IMediaFilesManager IDesignElement.FileManager
        {
            get { return Parent as IMediaFilesManager; }
		}

		System.Drawing.Bitmap IDesignElement.GetThumbnail(int cx, int cy)
		{
			return null;
		}
		#endregion

        void ApplyTransformEffect()
        {
            switch (this.GetElement.TransEffect)
            {
                case TransformEffect.TopToBottom:
                    DoubleAnimation_Top();
                    break;
                case TransformEffect.BottomToTop:
                    DoubleAnimation_Bottom();
                    break;
                case TransformEffect.LeftToRight:
                    DoubleAnimation_Left();
                    break;
                case TransformEffect.RightToLeft:
                    DoubleAnimation_Right();
                    break;
                case TransformEffect.FadeIn:
                    DoubleAnimation_Fadein();
                    break;

                default:
                    image.ImageSource = null;
                    break;
            }
        }
	}
}