﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using DigitalSignage.Common;
using System.Globalization;

namespace WPFDesigner
{
    /// <summary>
    /// Interaction logic for DateDisplay.xaml
    /// </summary>
    public partial class DateDisplay
    {
        DispatcherTimer dateTimer;
        public DateDisplay()
        {
            InitializeComponent();
            dateTimer = new DispatcherTimer();
            dateTimer.Interval = new TimeSpan(0, 1, 0);
            dateTimer.Tick += new EventHandler(dateTimer_Tick);
            dateLabel.Content = DateTime.Today.ToLongDateString();
        }

        void dateTimer_Tick(object sender, EventArgs e)
        {                   
            dateLabel.Content = DateTime.Today.ToLongDateString();
        }

        public void Start()
        {
            dateTimer.Start();
        }

        public void Stop()
        {
            dateTimer.Stop();
        }
    }
}
