﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms.Integration;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Media.Media3D;
using DigitalSignage.Common;
using DigitalSignage.Controls;

using System.Runtime.InteropServices;

using NLog;

namespace WPFDesigner
{
    /// <summary>
    /// Interaction logic for WebComponent.xaml
    /// </summary>
    public partial class WebComponent : UserControl, IDesignElement
    {
		[DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
		public static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int x, int y, int cx, int cy, int flags);

		IntPtr HWND_TOP = IntPtr.Zero;

		double zoomfactor = 1.0;

        private List<PlaylistItem> _playlist;
        private bool _IsPlay;
        PlaylistPlayer player;
//         private WebControl web;

        /// <summary>
        /// Represents the set of changable properties
        /// </summary>
        Dictionary<string, object> _properties;

        public WebComponent()
        {
            InitializeComponent();
            _IsPlay = false;
            //rect.Stroke = new SolidColorBrush(Color.FromArgb(255, 0, 0, 0));
            //rect.StrokeThickness = 1.0;
            _playlist = new List<PlaylistItem>();

			//	hsshin
//             web = new WebControl();

            this.SizeChanged += new SizeChangedEventHandler(WebComponent_SizeChanged);
			this.LayoutUpdated += new EventHandler(WebComponent_LayoutUpdated);
        }
		void Zooming()
		{
			try
			{
				Screen owner = ProjectManager.GetCurrentProjectManager().Pane as Screen;
				if (!zoomfactor.Equals(owner.ZoomFactor))
				{
					zoomfactor = owner.ZoomFactor;
					web.Zoom((int)(zoomfactor * 100));
				}
			}
			catch {}
		}
		void WebComponent_LayoutUpdated(object sender, EventArgs e)
		{
			Zooming();
		}

        void WebComponent_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            Binding b = new Binding();
            b.Source = GetElement;
            b.Mode = BindingMode.OneWay;
            PositionConvert converter = new PositionConvert();
            b.Converter = converter;
            ctrlName.SetBinding(TextBlock.TextProperty, b);
        }

        public IDesignElement GetElement
        {
            get
            {
                return this as IDesignElement;
            }
        }

        #region IDesignElement Members
        DigitalSignage.Common.TransformEffect IDesignElement.TransEffect
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }
        Dictionary<string, object> IDesignElement.Properties
        {
            get
            {
                ((IDesignElement)this).InitProperties();
                return _properties;
            }
            set
            {
                _properties = value;
                foreach (string name in _properties.Keys)
                {
                    PropertySetter.SetProperty(this, "IDesignElement", name, _properties[name]);
                }
            }
        }

		/// <summary>
		/// 재생이 시작된 시간
		/// </summary>
		public DateTime PlayStarted { get; set; }

        void IDesignElement.Play(TimeSpan ts)
        {
			if (!_IsPlay)
			{
				if (host != null)
				{
					SetWindowPos(host.Handle, HWND_TOP, 0, 0, 0, 0, 3);
				}

				_IsPlay = true;
				//	경계라인
				sepLine.Visibility = Visibility.Collapsed;

				ctrlName.Visibility = Visibility.Collapsed;

				player = new PlaylistPlayer(this);
				player.Start(ts);

				host.Visibility = Visibility.Visible;
			}
        }

        void IDesignElement.Play()
        {
            ((IDesignElement)this).Play(TimeSpan.Zero);
        }

        void IDesignElement.Seek(TimeSpan ts)
        {
            if (player != null) player.Seek(ts);
        }

		void IDesignElement.Pause()
		{
			try
			{
				player.Stop();
			}
			catch { }
		}

        bool IDesignElement.ISPlay
        {
            get { return _IsPlay; }
        }

        void IDesignElement.Stop()
        {
			_IsPlay = false;
			//	경계라인
			sepLine.Visibility = Visibility.Visible;

            ctrlName.Visibility = Visibility.Visible;
            player.Stop();
			web.Stop();
			host.Visibility = Visibility.Collapsed;
// 			web.Navigate(new Uri("about:blank"));
        }

        void IDesignElement.InitProperties()
        {
            _properties = new Dictionary<string, object>();
            IDesignElement child = this as IDesignElement;
            
            _properties.Add("Name", child.Name);
            _properties.Add("Width", child.Width);
            _properties.Add("Height", child.Height);
            _properties.Add("Left", child.Left);
            _properties.Add("Top", child.Top);
			_properties.Add("ZIndex", child.ZIndex);
			_properties.Add("HorizontalAlignment", child.HorizontalAlignment);
            _properties.Add("VerticalAlignment", child.VerticalAlignment);
//             _properties.Add("Opacity", child.Opacity);
//             _properties.Add("ZIndex", child.ZIndex);
            _properties.Add("Type", child.Type);
            _properties.Add("Playlist", child.Playlist);
        }

        string IDesignElement.Name
        {
            get
            {
                return Name;
            }
            set
            {
                Name = value;

                Binding b = new Binding();
                b.Source = GetElement;
                b.Mode = BindingMode.OneWay;
                PositionConvert converter = new PositionConvert();
                b.Converter = converter;
                ctrlName.SetBinding(TextBlock.TextProperty, b);
            }
        }

        double IDesignElement.Width
        {
            get
            {
                return ActualWidth;
            }
            set
            {
				Width = Math.Ceiling(value);
            }
        }

        double IDesignElement.Height
        {
            get
            {
                return ActualHeight;
            }
            set
            {
				Height = Math.Ceiling(value);
            }
        }

        double IDesignElement.Left
        {
            get
            {
                return (double)this.GetValue(InkCanvas.LeftProperty);
            }
            set
            {
				this.SetValue(InkCanvas.LeftProperty, Math.Floor(value));
            }
        }

        double IDesignElement.Top
        {
            get
            {
                return (double)this.GetValue(InkCanvas.TopProperty);
            }
            set
            {
				SetValue(InkCanvas.TopProperty, Math.Floor(value));
            }
        }

        double IDesignElement.Bottom
        {
            get
            {
                return (double)this.GetValue(InkCanvas.BottomProperty);
            }
            set
            {
				this.SetValue(InkCanvas.BottomProperty, Math.Ceiling(value));
            }
        }

        double IDesignElement.Right
        {
            get
            {
                return (double)this.GetValue(InkCanvas.RightProperty);
            }
            set
            {
				SetValue(InkCanvas.RightProperty, Math.Ceiling(value));
            }
        }

        int IDesignElement.ZIndex
        {
            get
            {
                return (int)this.GetValue(Canvas.ZIndexProperty);
            }
            set
            {
                this.SetValue(Canvas.ZIndexProperty, value);
            }
        }

        Brush IDesignElement.BorderBrush
        {
            get
            {
                return new SolidColorBrush();// this.rect.Stroke;
            }
            set
            {
                //this.rect.Stroke = value;
            }
        }

        double IDesignElement.BorderThickness
        {
            get
            {
                return 0.0; //rect.StrokeThickness;
            }
            set
            {
                //rect.StrokeThickness = value;
            }
        }

        double IDesignElement.BorderCorner
        {
            get
            {
                return 0.0;// rect.RadiusX;
            }
            set
            {
                //rect.RadiusX = rect.RadiusY = value;
            }
        }

        double IDesignElement.Opacity
        {
            get
            {
                return this.Opacity;
            }
            set
            {
                this.Opacity = value;
            }
        }

        HorizontalAlignment IDesignElement.HorizontalAlignment
        {
            get
            {
                return this.HorizontalAlignment;
            }
            set
            {
                this.HorizontalAlignment = value;
            }
        }

        VerticalAlignment IDesignElement.VerticalAlignment
        {
            get
            {
                return this.VerticalAlignment;
            }
            set
            {
                this.VerticalAlignment = value;
            }
        }

        Stretch IDesignElement.Stretch
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        double IDesignElement.Volume
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

		TimeSpan IDesignElement.RefreshInterval
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
				throw new NotImplementedException();
			}
		}

        bool IDesignElement.Mute
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        Brush IDesignElement.Background
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        FontFamily IDesignElement.FontFamily
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        double IDesignElement.FontSize
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        FontWeight IDesignElement.FontWeight
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        Brush IDesignElement.Foreground
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        int[] IDesignElement.StrokesLength
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        PenLineCap IDesignElement.StrokeDashCap
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        char IDesignElement.SeparatorChar
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        Point IDesignElement.AspectRatio
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        Type IDesignElement.Type
        {
            get
            {
                return GetType();
            }
        }

        List<PlaylistItem> IDesignElement.Playlist
        {
            get
            {
                return _playlist;
            }
            set
            {
                _playlist = value;
            }
        }

        string IDesignElement.Content
        {
            get
            {
                return "browser content";
            }
            set
            {
				Zooming();

                if (value == "")
                {
                    ctrlName.Visibility = Visibility.Visible;
                    web.Navigate(new Uri("about:blank"));
                    return;
                }

				if (value.StartsWith("Address://"))
				{
					UriBuilder builder = new UriBuilder(value.Substring(10));
					web.Navigate(builder.Uri);
				}
				else if (value.StartsWith("File://"))
				{
					IMediaFilesManager FileManager = ProjectManager.GetCurrentProjectManager().Pane as IMediaFilesManager;
					String content = FileManager.GetFullFilePathByName(value.Substring(7));
					UriBuilder builder = new UriBuilder(content);
					web.Navigate(builder.Uri);

				}
				else if (value.StartsWith("Script://"))
				{
					web.NavigateScript(value.Substring(9));

				}
				else
				{
					web.Navigate(new UriBuilder(value).Uri);
				}
            }
        }

        IMediaFilesManager IDesignElement.FileManager
        {
            get { return Parent as IMediaFilesManager; }
        }

		System.Drawing.Bitmap IDesignElement.GetThumbnail(int cx, int cy)
		{
			try
			{
				System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(cx, cy, System.Drawing.Imaging.PixelFormat.Format32bppArgb);
				using (System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(bmp))
				{
					Point pos = this.PointToScreen(new Point(0, 0));
					g.CopyFromScreen(new System.Drawing.Point((int)pos.X, (int)pos.Y),
						new System.Drawing.Point(0, 0),
						new System.Drawing.Size((int)this.ActualWidth, (int)this.ActualHeight), System.Drawing.CopyPixelOperation.SourceCopy);
				}

				return bmp;
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
			return null;
		}

        #endregion

		private void UserControl_Unloaded(object sender, RoutedEventArgs e)
		{
			try
			{
				web.Dispose();
				web = null;

				if (host != null)
				{
					host.Child = null;
					host.Dispose();
					host = null;
				}
			}
			catch 
			{
				
			}

		}
    }
}