﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPFDesigner.components
{
    /// <summary>
    /// Interaction logic for CustomTextComponent.xaml
    /// </summary>
    public partial class CustomTextComponent : UserControl, IDesignElement
    {
        /// <summary>
        /// Represents the playlist of this element
        /// </summary>
        private List<PlaylistItem> _playlist;
        private bool _IsPlay;
        PlaylistPlayer player;

        /// <summary>
        /// Represents the set of changable properties
        /// </summary>
        Dictionary<string, object> _properties;

        FlowDocument doc;

        public CustomTextComponent()
        {
            InitializeComponent();
            _IsPlay = false;
            doc = new FlowDocument();
            rtb.Document = doc;
            this.SizeChanged += new SizeChangedEventHandler(CustomTextComponent_SizeChanged);
        }

        void CustomTextComponent_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            Binding b = new Binding();
            b.Source = GetElement;
            b.Mode = BindingMode.OneWay;
            PositionConvert converter = new PositionConvert();
            b.Converter = converter;
            ctrlName.SetBinding(TextBlock.TextProperty, b);
        }

        public IDesignElement GetElement
        {
            get
            {
                return this as IDesignElement;
            }
        }

        #region IDesignElement Members
        DigitalSignage.Common.TransformEffect IDesignElement.TransEffect
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }
		/// <summary>
		/// 재생이 시작된 시간
		/// </summary>
		public DateTime PlayStarted { get; set; }

        void IDesignElement.Play(TimeSpan ts)
        {
			if (!_IsPlay)
			{
				player = new PlaylistPlayer(this);
				player.Start(ts);
				ctrlName.Visibility = Visibility.Collapsed;
				_IsPlay = true;
			}
        }

        void IDesignElement.Play()
        {
            ((IDesignElement)this).Play(TimeSpan.Zero);
        }

        void IDesignElement.Seek(TimeSpan ts)
        {
            if (player != null) player.Seek(ts);
        }


		void IDesignElement.Pause()
		{
			try
			{
				player.Stop();
			}
			catch { }
		}


        bool IDesignElement.ISPlay
        {
            get { return _IsPlay; }
        }

        void IDesignElement.Stop()
        {
            if (player != null) player.Stop();
            ctrlName.Visibility = Visibility.Visible;
            _IsPlay = false;
            GC.Collect();
        }

        Dictionary<string, object> IDesignElement.Properties
        {
            get
            {
                ((IDesignElement)this).InitProperties();
                return _properties;
            }
            set
            {
                _properties = value;
                foreach (string name in _properties.Keys)
                {
                    PropertySetter.SetProperty(this, "IDesignElement", name, _properties[name]);
                }
            }
        }

        void IDesignElement.InitProperties()
        {
            _properties = new Dictionary<string, object>();
            IDesignElement child = this as IDesignElement;

            _properties.Add("Name", child.Name);
            _properties.Add("Width", child.Width);
            _properties.Add("Height", child.Height);
            _properties.Add("Left", child.Left);
            _properties.Add("Top", child.Top);
            _properties.Add("HorizontalAlignment", child.HorizontalAlignment);
            _properties.Add("VerticalAlignment", child.VerticalAlignment);
            _properties.Add("Opacity", child.Opacity);
            _properties.Add("ZIndex", child.ZIndex);
            _properties.Add("BorderBrush", child.BorderBrush);
            _properties.Add("BorderThickness", child.BorderThickness);
            _properties.Add("BorderCorner", child.BorderCorner);
            _properties.Add("Stretch", child.Stretch);
            _properties.Add("Type", child.Type);
            _properties.Add("Playlist", child.Playlist);
        }

        string IDesignElement.Name
        {
            get
            {
                return Name;
            }
            set
            {
                Name = value;

                Binding b = new Binding();
                b.Source = GetElement;
                b.Mode = BindingMode.OneWay;
                PositionConvert converter = new PositionConvert();
                b.Converter = converter;
                ctrlName.SetBinding(TextBlock.TextProperty, b);
            }
        }

        double IDesignElement.Width
        {
            get
            {
                return ActualWidth;
            }
            set
            {
                Width = value;
            }
        }

        double IDesignElement.Height
        {
            get
            {
                return ActualHeight;
            }
            set
            {
                Height = value;
            }
        }

        double IDesignElement.Left
        {
            get
            {
                return (double)this.GetValue(InkCanvas.LeftProperty);
            }
            set
            {
                this.SetValue(InkCanvas.LeftProperty, value);
            }
        }

        double IDesignElement.Top
        {
            get
            {
                return (double)this.GetValue(InkCanvas.TopProperty);
            }
            set
            {
                SetValue(InkCanvas.TopProperty, value);
            }
        }

        double IDesignElement.Bottom
        {
            get
            {
                return (double)this.GetValue(InkCanvas.BottomProperty);
            }
            set
            {
                this.SetValue(InkCanvas.BottomProperty, value);
            }
        }

        double IDesignElement.Right
        {
            get
            {
                return (double)this.GetValue(InkCanvas.RightProperty);
            }
            set
            {
                SetValue(InkCanvas.RightProperty, value);
            }
        }

        int IDesignElement.ZIndex
        {
            get
            {
                return (int)this.GetValue(Canvas.ZIndexProperty);
            }
            set
            {
                this.SetValue(Canvas.ZIndexProperty, value);
            }
        }

        Brush IDesignElement.BorderBrush
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

		TimeSpan IDesignElement.RefreshInterval
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
				throw new NotImplementedException();
			}
		}
        double IDesignElement.BorderThickness
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        double IDesignElement.BorderCorner
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        HorizontalAlignment IDesignElement.HorizontalAlignment
        {
            get
            {
                return this.HorizontalAlignment;
            }
            set
            {
                this.HorizontalAlignment = value;
            }
        }

        VerticalAlignment IDesignElement.VerticalAlignment
        {
            get
            {
                return this.VerticalAlignment;
            }
            set
            {
                this.VerticalAlignment = value;
            }
        }

        double IDesignElement.Opacity
        {
            get
            {
                return Opacity;
            }
            set
            {
                Opacity = value;
            }
        }

        Stretch IDesignElement.Stretch
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        double IDesignElement.Volume
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        bool IDesignElement.Mute
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        Brush IDesignElement.Background
        {
            get
            {
                return Background;
            }
            set
            {
                Background = value;
            }
        }

        FontFamily IDesignElement.FontFamily
        {
            get
            {
                return doc.FontFamily;
            }
            set
            {
                doc.FontFamily = value;
            }
        }

        double IDesignElement.FontSize
        {
            get
            {
                return doc.FontSize;
            }
            set
            {
                doc.FontSize = value;
            }
        }

        FontWeight IDesignElement.FontWeight
        {
            get
            {
                return doc.FontWeight;
            }
            set
            {
                doc.FontWeight = value;
            }
        }

        Brush IDesignElement.Foreground
        {
            get
            {
                return doc.Foreground;
            }
            set
            {
                doc.Foreground = value;
            }
        }

        int[] IDesignElement.StrokesLength
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        PenLineCap IDesignElement.StrokeDashCap
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        char IDesignElement.SeparatorChar
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        Point IDesignElement.AspectRatio
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        Type IDesignElement.Type
        {
            get { return GetType(); }
        }

        List<PlaylistItem> IDesignElement.Playlist
        {
            get
            {
                return _playlist;
            }
            set
            {
                _playlist = value;
            }
        }

        string IDesignElement.Content
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
				throw new NotImplementedException();
			}
        }

        IMediaFilesManager IDesignElement.FileManager
        {
            get { return (IMediaFilesManager)Parent; }
        }

		System.Drawing.Bitmap IDesignElement.GetThumbnail(int cx, int cy)
		{
			return null;
		}

        #endregion
    }
}
