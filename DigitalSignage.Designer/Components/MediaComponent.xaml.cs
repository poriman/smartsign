﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Media.Media3D;
using System.Windows.Markup;
using System.Xml;
using System.IO;

using DigitalSignage.Common;

using System.Runtime.InteropServices;
using System.Windows.Forms.Integration;
using DigitalSignage.Controls;
using System.Windows.Threading;
using System.Reflection;
using System.Windows.Media.Animation;

namespace WPFDesigner
{
    /// <summary>
    /// Interaction logic for MediaComponent.xaml
    /// </summary>
    public partial class MediaComponent : UserControl, IDesignElement
    {
		[DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
		public static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int x, int y, int cx, int cy, int flags);

		IntPtr HWND_TOP = IntPtr.Zero;

		private List<PlaylistItem> _playlist;
        private bool _IsPlay;
        PlaylistPlayer player;

        /// <summary>
        /// Represents the set of changable properties
        /// </summary>
        Dictionary<string, object> _properties;

		//	미디어 컴포넌트들
		MediaElement _element_media = null;
        MediaElement _element_media2 = null;
		WindowsFormsHost Host = null;
		AxMediaControl media = null;

		//public EventWaitHandle waitMediaOpenHandler = new AutoResetEvent(false);

// 		//	버퍼링 타이머
// 		DispatcherTimer tm_buffering = null;

		//	hsshin 스트리밍 컨텐츠인지?
		private bool _isStreaming = false;
		public bool IsStreaming
		{
			get { return _isStreaming; }
			set { _isStreaming = value; }
		}

		private bool _useWPF = false;
		protected bool UseWPF
		{
			get { return _useWPF; }
			set { _useWPF = value; }
		}

        public MediaComponent(bool bUseWPF)
        {
            InitializeComponent();
            
            _IsPlay = false;
            sub_rect.Stroke = new SolidColorBrush(Color.FromArgb(255, 0, 0, 0));
            sub_rect.StrokeThickness = 0.0;

			if(UseWPF)
			{
				#region MEDIAELEMENT_SOURCE 시작
				_element_media = new MediaElement();
				_element_media.OverridesDefaultStyle = false;
				_element_media.Stretch = Stretch.Fill;
				_element_media.Volume = 1;

				VisualBrush brush = new VisualBrush(_element_media);
                sub_rect.Fill = brush;

				_element_media.LoadedBehavior = MediaState.Manual;
				_element_media.MediaEnded += new RoutedEventHandler(media_MediaEnded);
                _element_media.MediaOpened += new RoutedEventHandler(_element_media_MediaOpened);
                _element_media.MediaFailed += new EventHandler<ExceptionRoutedEventArgs>(_element_media_MediaFailed);

// 				_element_media.BufferingStarted += new RoutedEventHandler(_element_media_BufferingStarted);
// 				_element_media.BufferingEnded += new RoutedEventHandler(_element_media_BufferingEnded);
				#endregion MEDIAELEMENT_SOURCE 끝

                #region MEDIAELEMENT_SOURCE 시작
                _element_media2 = new MediaElement();
                _element_media2.OverridesDefaultStyle = false;
                _element_media2.Stretch = Stretch.Fill;
                _element_media2.Volume = 1;

                _element_media2.LoadedBehavior = MediaState.Manual;
                _element_media2.MediaEnded += new RoutedEventHandler(media_MediaEnded);
                _element_media2.MediaOpened += new RoutedEventHandler(_element_media_MediaOpened);
                _element_media2.MediaFailed += new EventHandler<ExceptionRoutedEventArgs>(_element_media_MediaFailed);
                #endregion MEDIAELEMENT_SOURCE 끝
            }
			else
			{
				Host = new WindowsFormsHost();
				Host.Visibility = Visibility.Hidden;
				Host.Background = Brushes.Black;

                sub_stackpanel.Children.Add(Host);
				media = new AxMediaControl();
				Host.Child = media;

				#region DIRECTSHOW_SOURCE 시작
				media.OnStateChanged += new DigitalSignage.Controls.AxMediaControl.StatusEventHandler(media_OnStateChanged);
				#endregion DIRECTSHOW_SOURCE 끝
			}

			_playlist = new List<PlaylistItem>();
            this.SizeChanged += new SizeChangedEventHandler(MediaComponent_SizeChanged);
			
		}

		public MediaComponent(bool bUseWPF, bool bIsStreaming)
		{
            InitializeComponent();
			_isStreaming = bIsStreaming;
            UseWPF = bUseWPF;

            _IsPlay = false;
            rect.Stroke = new SolidColorBrush(Color.FromArgb(255, 0, 0, 0));
            rect.StrokeThickness = 0.0;

			if (UseWPF)
			{
				#region MEDIAELEMENT_SOURCE 시작
				_element_media = new MediaElement();
				_element_media.OverridesDefaultStyle = false;
				_element_media.Stretch = Stretch.Fill;
				_element_media.Volume = 1;

				VisualBrush brush = new VisualBrush(_element_media);
				rect.Fill = brush;

				_element_media.LoadedBehavior = MediaState.Manual;
                _element_media.MediaOpened += new RoutedEventHandler(_element_media_MediaOpened);
                _element_media.MediaFailed += new EventHandler<ExceptionRoutedEventArgs>(_element_media_MediaFailed);
				_element_media.MediaEnded += new RoutedEventHandler(media_MediaEnded);

				#endregion MEDIAELEMENT_SOURCE 끝

                #region MEDIAELEMENT_SOURCE 시작
                _element_media2 = new MediaElement();
                _element_media2.OverridesDefaultStyle = false;
                _element_media2.Stretch = Stretch.Fill;
                _element_media2.Volume = 1;

                _element_media2.LoadedBehavior = MediaState.Manual;
                _element_media2.MediaEnded += new RoutedEventHandler(media_MediaEnded);
                _element_media2.MediaOpened += new RoutedEventHandler(_element_media_MediaOpened);
                _element_media2.MediaFailed += new EventHandler<ExceptionRoutedEventArgs>(_element_media_MediaFailed);
                #endregion MEDIAELEMENT_SOURCE 끝
			}
			else
			{
				Host = new WindowsFormsHost();
				Host.Visibility = Visibility.Hidden;
				Host.Background = Brushes.Black;

                sub_stackpanel.Children.Add(Host);
				media = new AxMediaControl();
				Host.Child = media;

				#region DIRECTSHOW_SOURCE 시작
				media.OnStateChanged += new DigitalSignage.Controls.AxMediaControl.StatusEventHandler(media_OnStateChanged);
				#endregion DIRECTSHOW_SOURCE 끝
			}

			_playlist = new List<PlaylistItem>();
            this.SizeChanged += new SizeChangedEventHandler(MediaComponent_SizeChanged);
		}



		#region DIRECTSHOW_SOURCE 시작
		void media_OnStateChanged(DigitalSignage.Controls.MediaStatus status)
		{
			switch(status)
			{
				case DigitalSignage.Controls.MediaStatus.Played:
					if (Host != null) Host.Visibility = Visibility.Visible;
					break;
				default:
					if (this.Host != null) Host.Visibility = Visibility.Hidden;
					break;
			}
		}
		#endregion DIRECTSHOW_SOURCE 끝

		void MediaComponent_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            rect.Width = Width;
            rect.Height = Height;
			#region DIRECTSHOW_SOURCE 시작
			if(!UseWPF)
			{
				Host.Width = Width;
				Host.Height = Height;
			}
			#endregion DIRECTSHOW_SOURCE 끝

			Binding b = new Binding();
            b.Source = GetElement;
            b.Mode = BindingMode.OneWay;
            PositionConvert converter = new PositionConvert();
            b.Converter = converter;
            ctrlName.SetBinding(TextBlock.TextProperty, b);
        }

        public IDesignElement GetElement
        {
            get
            {
                return this as IDesignElement;
            }
        }

        void media_MediaEnded(object sender, RoutedEventArgs e)
        {
			#region MEDIAELEMENT_SOURCE 시작
			_element_media.Position = new TimeSpan(0, 0, 0);
			_element_media.Play();
            #endregion MEDIAELEMENT_SOURCE 끝
		}

        #region IDesignElement Members

        DateTime _playStarted = DateTime.Now;

		/// <summary>
		/// 재생이 시작된 시간
		/// </summary>
        public DateTime PlayStarted
        {
            get
            {
                return _playStarted;
            }
            set
            {
                _playStarted = value;
                if (!UseWPF)
                {
                    if (media != null) media.PlayStarted = value;
                }

            }
        }

        void IDesignElement.Play(TimeSpan ts)
        {
			if (!_IsPlay)
			{

				_IsPlay = true;
				//	경계라인
				sepLine.Visibility = Visibility.Collapsed;

				if(!UseWPF)
				{
					#region DIRECTSHOW_SOURCE 시작
					rect.Visibility = Visibility.Collapsed;

					if (Host != null)
					{
						SetWindowPos(Host.Handle, HWND_TOP, 0, 0, 0, 0, 3);
					}

					#endregion DIRECTSHOW_SOURCE 끝
				}
				else
				{
					#region MEDIAELEMENT_SOURCE 시작
						rect.Visibility = Visibility.Visible;
					#endregion MEDIAELEMENT_SOURCE 끝

				}

				player = new PlaylistPlayer(this);
				player.Start(ts);
				ctrlName.Visibility = Visibility.Collapsed;
			}
        }

        void IDesignElement.Play()
        {
            ((IDesignElement)this).Play(TimeSpan.Zero);
        }

        void IDesignElement.Seek(TimeSpan ts)
        {
            if (player != null) player.Seek(ts);
        }



		void IDesignElement.Pause()
		{
			try
			{
				player.Stop();

				if (!UseWPF)
				{
					#region DIRECTSHOW_SOURCE 시작
					if (media != null) media.Pause();

					#endregion DIRECTSHOW_SOURCE 끝

				}
				else
				{
					#region MEDIAELEMENT_SOURCE 시작

					if (_element_media != null)
					{
						_element_media.Pause();
					}

					#endregion MEDIAELEMENT_SOURCE 끝
				}
			}
			catch { }
		}

        TransformEffect IDesignElement.TransEffect
        {
            get;
            set;
        }
        bool IDesignElement.ISPlay
        {
            get { return _IsPlay; }
        }

        void IDesignElement.Stop()
        {
			_IsPlay = false;

			//	경계라인
			sepLine.Visibility = Visibility.Visible;
			rect.Visibility = Visibility.Visible;

			if (player != null) player.Stop();
			ctrlName.Visibility = Visibility.Visible;

			#region DIRECTSHOW_SOURCE 시작
			if(media != null)	media.Stop();

			#endregion DIRECTSHOW_SOURCE 끝

			#region MEDIAELEMENT_SOURCE 시작

			if (_element_media != null)
			{
                closeTimer();
                
                _element_media.Stop();
				_element_media.Close();
				_element_media.Source = null;
			}
			
			#endregion MEDIAELEMENT_SOURCE 끝
            rect.DataContext = null;
            sub_stackpanel.DataContext = null;
            sub_rect.DataContext = null;

		}

		Dictionary<string, object> IDesignElement.Properties
        {
            get
            {
                ((IDesignElement)this).InitProperties();
                return _properties;
            }
            set
            {
                _properties = value;
                foreach (string name in _properties.Keys)
                {
                    PropertySetter.SetProperty(this, "IDesignElement", name, _properties[name]);
                }
            }
        }

        void IDesignElement.InitProperties()
        {
            _properties = new Dictionary<string, object>();
            IDesignElement child = this as IDesignElement;

            _properties.Add("Name", child.Name);
            _properties.Add("Width", child.Width);
            _properties.Add("Height", child.Height);
            _properties.Add("Left", child.Left);
            _properties.Add("Top", child.Top);
            _properties.Add("HorizontalAlignment", child.HorizontalAlignment);
            _properties.Add("VerticalAlignment", child.VerticalAlignment);
			_properties.Add("ZIndex", child.ZIndex);
            _properties.Add("TransEffect", child.TransEffect);

			if(UseWPF)
			{
				#region MEDIAELEMENT_SOURCE 시작
				_properties.Add("Opacity", child.Opacity);
				_properties.Add("BorderBrush", child.BorderBrush);
				_properties.Add("BorderThickness", child.BorderThickness);
				_properties.Add("BorderCorner", child.BorderCorner);
				#endregion MEDIAELEMENT_SOURCE 끝
			}

			_properties.Add("Type", child.Type);
            _properties.Add("Playlist", child.Playlist);
            _properties.Add("Mute", child.Mute);
            _properties.Add("Volume", child.Volume);
            
        }

        string IDesignElement.Name
        {
            get
            {
                return Name;
            }
            set
            {
                Name = value;

                Binding b = new Binding();
                b.Source = GetElement;
                b.Mode = BindingMode.OneWay;
                PositionConvert converter = new PositionConvert();
                b.Converter = converter;
                ctrlName.SetBinding(TextBlock.TextProperty, b);
            }
        }

        double IDesignElement.Width
        {
            get
            {
                return ActualWidth;
            }
            set
            {
                Width = Math.Ceiling(value);
            }
        }

        double IDesignElement.Height
        {
            get
            {
                return ActualHeight;
            }
            set
            {
                Height = Math.Ceiling(value);
            }
        }

        double IDesignElement.Left
        {
            get
            {
                return (double)this.GetValue(InkCanvas.LeftProperty);
            }
            set
            {
                this.SetValue(InkCanvas.LeftProperty, Math.Floor(value));
            }
        }

        double IDesignElement.Top
        {
            get
            {
                return (double)this.GetValue(InkCanvas.TopProperty);
            }
            set
            {
				SetValue(InkCanvas.TopProperty, Math.Floor(value));
            }
        }

        double IDesignElement.Bottom
        {
            get
            {
                return (double)this.GetValue(InkCanvas.BottomProperty);
            }
            set
            {
				this.SetValue(InkCanvas.BottomProperty, Math.Ceiling(value));
            }
        }

        double IDesignElement.Right
        {
            get
            {
                return (double)this.GetValue(InkCanvas.RightProperty);
            }
            set
            {
				SetValue(InkCanvas.RightProperty, Math.Ceiling(value));
            }
        }

        int IDesignElement.ZIndex
        {
            get
            {
                return (int)this.GetValue(Canvas.ZIndexProperty);
            }
            set
            {
                this.SetValue(Canvas.ZIndexProperty, value);
            }
        }

        Brush IDesignElement.BorderBrush
        {
            get
            {
                return this.rect.Stroke;
            }
            set
            {
                this.rect.Stroke = value;
            }
        }

        double IDesignElement.BorderThickness
        {
            get
            {
                return rect.StrokeThickness;
            }
            set
            {
                rect.StrokeThickness = value;
            }
        }

        double IDesignElement.BorderCorner
        {
            get
            {
                return rect.RadiusX;
            }
            set
            {
				rect.RadiusX = rect.RadiusY = value;
				sepLine.RadiusX = sepLine.RadiusY = value;
			}
        }

        double IDesignElement.Opacity
        {
            get
            {
                return this.Opacity;
            }
            set
            {
                this.Opacity = value;
            }
        }

        Stretch IDesignElement.Stretch
        {
            get
			{
				if(UseWPF)
				{
					#region MEDIAELEMENT_SOURCE 시작

					return _element_media.Stretch;

					#endregion MEDIAELEMENT_SOURCE 끝
				}
				else
				{
					#region DIRECTSHOW_SOURCE 시작

					return Stretch.Fill;

					#endregion DIRECTSHOW_SOURCE 끝
				}
			}
            set
            {
				#region MEDIAELEMENT_SOURCE 시작

				if (UseWPF)
				{
					_element_media.Stretch = value;
				}

				#endregion MEDIAELEMENT_SOURCE 끝
            }
        }

        HorizontalAlignment IDesignElement.HorizontalAlignment
        {
            get
            {
                return this.HorizontalAlignment;
            }
            set
            {
                this.HorizontalAlignment = value;
            }
        }

        VerticalAlignment IDesignElement.VerticalAlignment
        {
            get
            {
                return this.VerticalAlignment;
            }
            set
            {
                this.VerticalAlignment = value;
            }
        }

        double IDesignElement.Volume
        {
            get
            {
				if (UseWPF)
				{
					return _element_media.Volume;
				}
				else
				{
					return media.Volume;
				}
            }
            set
            {
				if (UseWPF)
				{
					_element_media.Volume = value;
				}
				else
				{
					media.Volume = value;
				}
            }
        }

        bool IDesignElement.Mute
        {
            get
            {
				if (UseWPF)
				{
					return _element_media.IsMuted;
				}
				else
					return media.IsMuted;
            }
            set
            {
				if (UseWPF)
				{
					_element_media.IsMuted = value;
				}
				else
				{
					media.IsMuted = value;
				}
            }
        }

        Brush IDesignElement.Background
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        FontFamily IDesignElement.FontFamily
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        double IDesignElement.FontSize
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        FontWeight IDesignElement.FontWeight
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        Brush IDesignElement.Foreground
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        int[] IDesignElement.StrokesLength
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

		TimeSpan IDesignElement.RefreshInterval
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
				throw new NotImplementedException();
			}
		}

        PenLineCap IDesignElement.StrokeDashCap
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        char IDesignElement.SeparatorChar
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        Point IDesignElement.AspectRatio
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        Type IDesignElement.Type
        {
            get
            {
                return GetType();
            }
        }

        List<PlaylistItem> IDesignElement.Playlist
        {
            get
            {
                return _playlist;
            }
            set
            {
                _playlist = value;
            }
		}


        void _element_media_MediaFailed(object sender, ExceptionRoutedEventArgs e)
        {
            try
            {
                _contentLoaded = true;
//				waitMediaOpenHandler.Set();

                iVisionLogHelper.GetInstance.ScreenErrorLog(this.Content.ToString(), ErrorCodes.IS_ERROR_INTERNAL, e.ErrorException.Message);
            }
            catch { }
        }

        void _element_media_MediaOpened(object sender, RoutedEventArgs e)
        {
            try
            {
                _contentLoaded = true;
//				waitMediaOpenHandler.Set();

                closeTimer();

                if (ScreenStartInfoInSchedule.GetInstance.IsUsingSeek)
                {
                    //tmSeeking = new System.Threading.Timer(rateCallBack, null, ScreenStartInfoInSchedule.GetInstance.DueTime, 100);
                    tmSeeking = new DispatcherTimer();
                    tmSeeking.Tick += new EventHandler(tmSeeking_Tick);
                    tmSeeking.Interval = new TimeSpan(0, 0, 0, 0, 100);
                    tmSeeking.Start();

                }
            }
            catch { }
        }

        DispatcherTimer tmLoadingAgent = null;

        void runLoadingTimer()
        {
            tmLoadingAgent = new DispatcherTimer();
            tmLoadingAgent.Tick += new EventHandler(tmLoadingAgent_Tick);
            tmLoadingAgent.Interval = new TimeSpan(0, 0, 0, 5);
            tmLoadingAgent.Start();
        }

        void tmLoadingAgent_Tick(object sender, EventArgs e)
        {
            try
            {
                if (!_contentLoaded && !AppDomain.CurrentDomain.FriendlyName.ToLower().Contains("designer"))
                {
                    Application.Current.Shutdown();
                }
            }
            catch { }
            finally
            {
                try
                {
                    tmLoadingAgent.Stop();
                }
                catch { }
                finally { tmLoadingAgent = null; }
            }
        }


        DispatcherTimer tmSeeking = null;
        double dCurrentRate = 1.0;

		string IDesignElement.Content
		{
			get
			{
				if(UseWPF)
					return _element_media2.Source.AbsolutePath;
				else
					return media.CurrentFilePath;
			}
			set
			{
                Dispatcher.Invoke(DispatcherPriority.Normal, new Action(delegate
                {
                    if (UseWPF)
                    {
                        #region MEDIAELEMENT_SOURCE 시작
                        if (value == "")
                        {
                            ctrlName.Visibility = Visibility.Visible;
                            _element_media.Stop();
                            _element_media.Close();
                            _element_media.Source = null;
                            return;
                        }
                        _element_media2.Source = _element_media.Source;
                        rect.DataContext = sub_rect.DataContext;
                        sub_rect.DataContext = _element_media2;
                        sub_rect.Width = this.Width;
                        sub_rect.Height = this.Height;
                        rect.Width = this.Width;
                        rect.Height = this.Height;
                        rect.Visibility = Visibility.Visible;
                        ApplyTransformEffect();
                        if (Uri.IsWellFormedUriString(value, UriKind.Absolute))
                        {
                            _element_media.Stop();
                            _element_media.Close();
                            _element_media.Source = new Uri(value);

                            runLoadingTimer();
                        }
                        else
                        {
                            Uri uri = new Uri(((IDesignElement)this).FileManager.GetFullFilePathByName(value));
                            //if (_element_media.Source != null && uri.Equals(_element_media.Source))
                            //{
                            //    //_element_media.Stop();
                            //    _element_media.Position = TimeSpan.Zero;
                            //}
                            //else
                            {
                                _element_media.Stop();
                                _element_media.Close();
                                _element_media.Source = uri;

                                runLoadingTimer();
                            }
                        }

                        _contentLoaded = false;
                        //waitMediaOpenHandler.Reset();

                        if (DateTime.Now > ScreenStartInfoInSchedule.GetInstance.ScheduleStartDT)
                        {
                            Seeking();
                        }
                        _element_media.Play();
                        
                        

                        /*
                        DateTime dtNow = DateTime.Now;

                        while (!waitMediaOpenHandler.WaitOne(10))
                        {
                            System.Windows.Forms.Application.DoEvents();
                            if ((DateTime.Now - dtNow).TotalSeconds > 2.0)
                            {
                                try
                                {
                                    iVisionLogHelper.GetInstance.ScreenErrorLog(this.Content.ToString(), ErrorCodes.IS_ERROR_TIMEOUT, "Content Loading Timeouted.");
                                }
                                catch { }
                                break;
                            }
                        }*/

                        #endregion MEDIAELEMENT_SOURCE 끝
                    }
                    else
                    {
                        #region DIRECTSHOW_SOURCE 시작

                        if (value == "")
                        {
                            ctrlName.Visibility = Visibility.Visible;
                            rect.DataContext = null;
                            media.Stop();
                            return;
                        }
                        
                        if (IsStreaming)
                        {
                            media.Play(value, this.PlayStarted);

                        }
                        else
                        {
                            string filePath = ((IDesignElement)this).FileManager.GetFullFilePathByName(value);
                            media.Play(filePath, this.PlayStarted);
                        }
                        rect.DataContext = sub_stackpanel.DataContext;
                        sub_stackpanel.DataContext = media;

                        rect.Visibility = Visibility.Visible;
                        ApplyTransformEffect();
                        
                        #endregion DIRECTSHOW_SOURCE 끝

                    }
                    
                }));
			}
		}

        void ApplyTransformEffect()
        {
            switch (this.GetElement.TransEffect)
            {
                case TransformEffect.TopToBottom:
                    DoubleAnimation_Top();
                    break;
                case TransformEffect.BottomToTop:
                    DoubleAnimation_Bottom();
                    break;
                case TransformEffect.LeftToRight:
                    DoubleAnimation_Left();
                    break;
                case TransformEffect.RightToLeft:
                    DoubleAnimation_Right();
                    break;
                case TransformEffect.FadeIn:
                    DoubleAnimation_Fadein();
                    break;

                default:
                    rect.DataContext = null;
                    break;
            }
        }

        void MyDoubleAnimation_Completed(object sender, EventArgs e)
        {
            rect.DataContext = null;
        }

        public void DoubleAnimation_Top()
        {
            //더블 애니메이션 하나 설정 했다. 
            DoubleAnimation MyDoubleAnimation = new DoubleAnimation();

            MyDoubleAnimation.From = -this.Height;
            MyDoubleAnimation.To = 0;
            //가속도값 설정하기 
            BounceEase be = new BounceEase();
            be.Bounciness = 15;
            be.Bounces = 1;

            CubicEase ease = new CubicEase();
            ease.EasingMode = EasingMode.EaseIn;
            MyDoubleAnimation.EasingFunction = ease;

            MyDoubleAnimation.Duration = new Duration(TimeSpan.FromSeconds(1));
            //애니메이션 효과를 적용한 후에는 속성 값을 변경하기 
            MyDoubleAnimation.FillBehavior = FillBehavior.Stop;
            MyDoubleAnimation.Completed += new EventHandler(MyDoubleAnimation_Completed);
            if (_useWPF)
                sub_rect.BeginAnimation(Canvas.TopProperty, MyDoubleAnimation);
            else
                sub_stackpanel.BeginAnimation(Canvas.TopProperty, MyDoubleAnimation);
        }

        public void DoubleAnimation_Bottom()
        {
            //더블 애니메이션 하나 설정 했다. 
            DoubleAnimation MyDoubleAnimation = new DoubleAnimation();
            MyDoubleAnimation.From = -this.Height;
            MyDoubleAnimation.To = 0;
            //가속도값 설정하기 
            BounceEase be = new BounceEase();
            be.Bounciness = 15;
            be.Bounces = 1;

            CubicEase ease = new CubicEase();
            ease.EasingMode = EasingMode.EaseIn;
            MyDoubleAnimation.EasingFunction = ease;

            MyDoubleAnimation.Duration = new Duration(TimeSpan.FromSeconds(1));
            //애니메이션 효과를 적용한 후에는 속성 값을 변경하기 
            MyDoubleAnimation.FillBehavior = FillBehavior.Stop;
            MyDoubleAnimation.Completed += new EventHandler(MyDoubleAnimation_Completed);
            if (_useWPF)
                sub_rect.BeginAnimation(Canvas.BottomProperty, MyDoubleAnimation);
            else
                sub_stackpanel.BeginAnimation(Canvas.BottomProperty, MyDoubleAnimation);
        }

        public void DoubleAnimation_Left()
        {
            //더블 애니메이션 하나 설정 했다. 
            DoubleAnimation MyDoubleAnimation = new DoubleAnimation();
            //Image IMG = MyEllipseCanvas.Children[0] as Image;
            MyDoubleAnimation.From = -this.Width;
            MyDoubleAnimation.To = 0;
            //가속도값 설정하기 
            BounceEase be = new BounceEase();
            be.Bounciness = 15;
            be.Bounces = 1;

            CubicEase ease = new CubicEase();
            ease.EasingMode = EasingMode.EaseIn;
            MyDoubleAnimation.EasingFunction = ease;

            MyDoubleAnimation.Duration = new Duration(TimeSpan.FromSeconds(1));
            //애니메이션 효과를 적용한 후에는 속성 값을 변경하기 
            //애니메이션 효과를 적용한 후에는 속성 값을 변경하기 
            MyDoubleAnimation.FillBehavior = FillBehavior.Stop;
            MyDoubleAnimation.Completed += new EventHandler(MyDoubleAnimation_Completed);
            if(_useWPF)
                sub_rect.BeginAnimation(Canvas.LeftProperty, MyDoubleAnimation);
            else
                sub_stackpanel.BeginAnimation(Canvas.LeftProperty, MyDoubleAnimation);
        }

        public void DoubleAnimation_Right()
        {
            //더블 애니메이션 하나 설정 했다. 
            DoubleAnimation MyDoubleAnimation = new DoubleAnimation();
            MyDoubleAnimation.From = -this.Width;
            MyDoubleAnimation.To = 0;
            //가속도값 설정하기 
            BounceEase be = new BounceEase();
            be.Bounciness = 15;
            be.Bounces = 1;

            CubicEase ease = new CubicEase();
            ease.EasingMode = EasingMode.EaseIn;
            MyDoubleAnimation.EasingFunction = ease;

            MyDoubleAnimation.Duration = new Duration(TimeSpan.FromSeconds(1));
            //애니메이션 효과를 적용한 후에는 속성 값을 변경하기 
            //애니메이션 효과를 적용한 후에는 속성 값을 변경하기 
            MyDoubleAnimation.FillBehavior = FillBehavior.Stop;
            MyDoubleAnimation.Completed += new EventHandler(MyDoubleAnimation_Completed);
            if (_useWPF)
                sub_rect.BeginAnimation(Canvas.RightProperty, MyDoubleAnimation);
            else
                sub_stackpanel.BeginAnimation(Canvas.RightProperty, MyDoubleAnimation);
        }

        public void DoubleAnimation_Fadein()
        {
            //더블 애니메이션 하나 설정 했다. 
            DoubleAnimation MyDoubleAnimation = new DoubleAnimation();
            MyDoubleAnimation.From = 0;
            MyDoubleAnimation.To = 1;

            MyDoubleAnimation.Duration = new Duration(TimeSpan.FromSeconds(1));
            //애니메이션 효과를 적용한 후에는 속성 값을 변경하기 
            //애니메이션 효과를 적용한 후에는 속성 값을 변경하기 
            MyDoubleAnimation.FillBehavior = FillBehavior.Stop;
            MyDoubleAnimation.Completed += new EventHandler(MyDoubleAnimation_Completed);
            if (_useWPF)
                sub_rect.BeginAnimation(Canvas.OpacityProperty, MyDoubleAnimation);
            else
                sub_stackpanel.BeginAnimation(Canvas.OpacityProperty, MyDoubleAnimation);

            //더블 애니메이션 하나 설정 했다. 
            DoubleAnimation MyDoubleAnimation2 = new DoubleAnimation();
            MyDoubleAnimation2.From = 1;
            MyDoubleAnimation2.To = 0;

            MyDoubleAnimation2.Duration = new Duration(TimeSpan.FromSeconds(1));
            //애니메이션 효과를 적용한 후에는 속성 값을 변경하기 
            //애니메이션 효과를 적용한 후에는 속성 값을 변경하기 
            MyDoubleAnimation2.FillBehavior = FillBehavior.Stop;
            rect.BeginAnimation(Canvas.OpacityProperty, MyDoubleAnimation2);
        }

        private MediaState GetMediaState
        {
            get
            {
                try
                {
                    FieldInfo hlp = typeof(MediaElement).GetField("_helper", BindingFlags.NonPublic | BindingFlags.Instance);
                    object helperObject = hlp.GetValue(_element_media);
                    FieldInfo stateField = helperObject.GetType().GetField("_currentState", BindingFlags.NonPublic | BindingFlags.Instance);
                    MediaState state = (MediaState)stateField.GetValue(helperObject);
                    return state;
                }
                catch { return MediaState.Manual; }
            }
        }

        void tmSeeking_Tick(object sender, EventArgs e)
        {
            rateCallBack(null);
        }

        DateTime dtControlTM = DateTime.Now;

        void rateCallBack(object state)
        {
            try
            {
                if (ScreenStartInfoInSchedule.GetInstance.IsSyncTask)
                {
                    if ((DateTime.Now - dtControlTM).TotalSeconds < 5) return;

                    int threshold = DigitalSignage.Common.ScreenStartInfoInSchedule.GetInstance.SyncThreshold;
                    int adjustValue = 0/*Math.Abs(DigitalSignage.Common.ScreenStartInfoInSchedule.GetInstance.AdjustValue)*/;

                    TimeSpan curr_ts = _element_media.Position;
                    TimeSpan ts = TimeSpan.FromMilliseconds((DateTime.Now - this.PlayStarted).TotalMilliseconds % _element_media.NaturalDuration.TimeSpan.TotalMilliseconds);

                    double delay = ts.TotalMilliseconds - curr_ts.TotalMilliseconds;

                    Console.Write("{0}, ", (int)delay);

                    if (delay < -adjustValue && (dCurrentRate > 1.0))
                    {
                        DateTime dtNow = DateTime.Now;
                        _element_media.SpeedRatio = dCurrentRate = 1.0;
                        WriteLog("SetSpeedRate Function Delay: {0}", (DateTime.Now - dtNow).TotalMilliseconds);
                        WriteLog("Set Speed Rate: {0}", dCurrentRate);

                        //dtControlTM = DateTime.Now;
                    }
                    else if (Math.Abs(delay) > threshold && dCurrentRate <= 1.0)
                    {

                        if (Math.Abs(delay) > 1000)
                        {
                            WriteLog("Current Position: {0}, Delay: {1}", curr_ts, delay);
                            WriteLog("Set Position: {0}", ts);

                            _element_media.Position = ts;

                            //dtControlTM = DateTime.Now;

                        }
                        else
                        {

                            if (delay > 0)
                            {
                                _element_media.SpeedRatio = dCurrentRate = 1.05;
                                
                                 WriteLog("Set Speed Rate: {0}", dCurrentRate);
                            }
                            else
                            {

                                 _element_media.SpeedRatio = dCurrentRate = 0.95;

                                WriteLog("Set Speed Rate: {0}", dCurrentRate);

                            }
                        }
                    }

                }
            }
            catch { }
        }

        /// <summary>
        /// 타이머 종료
        /// </summary>
        void closeTimer()
        {
            if (tmSeeking != null)
            {
                try
                {
                    tmSeeking.Stop();
                    //tmSeeking.Dispose();
               }
                catch { }
                finally
                {
                    tmSeeking = null;
                }
            }
        }

        /// <summary>
        /// Seeking 함수
        /// </summary>
        void Seeking()
        {
            try
            {
                if (ScreenStartInfoInSchedule.GetInstance.IsSyncTask)
                {
                    int threshold = DigitalSignage.Common.ScreenStartInfoInSchedule.GetInstance.SyncThreshold;
                    int adjustValue = Math.Abs(DigitalSignage.Common.ScreenStartInfoInSchedule.GetInstance.AdjustValue);

                    TimeSpan curr_ts = _element_media.Position;
                    TimeSpan ts = TimeSpan.FromMilliseconds((DateTime.Now - this.PlayStarted).TotalMilliseconds % _element_media.NaturalDuration.TimeSpan.TotalMilliseconds);

                    double delay = ts.TotalMilliseconds - curr_ts.TotalMilliseconds;

                    Console.Write("{0}, ", delay);

                    if (Math.Abs(delay) > threshold)
                    {
                        WriteLog("Current Position: {0}, Delay: {1}", curr_ts, delay);
                        WriteLog("Set Position: {0}", ts);

                        _element_media.Position = ts;
                    }
                }
            }
            catch { }
            finally
            {
            }
        }

        private void WriteLog(string format, params object[] listParam)
        {
            //logger.Debug(format, listParam);
            Console.WriteLine(format, listParam);
        }

		IMediaFilesManager IDesignElement.FileManager
        {
            get { return Parent as IMediaFilesManager; }
		}

		System.Drawing.Bitmap IDesignElement.GetThumbnail(int cx, int cy)
		{
			if(UseWPF)
			{
				#region MEDIAELEMENT_SOURCE 시작
				return null;
				#endregion
			}
			else
			{
				#region DIRECTSHOW_SOURCE 시작
				if (media != null)
				{
					return media.Thumbnail(cx, cy);
				}

				return null;
				#endregion
			}
		}

		#endregion

		private void UserControl_Unloaded(object sender, RoutedEventArgs e)
		{
			#region DIRECTSHOW_SOURCE 시작
			if (media != null && !media.IsDisposed)
			{
				try
				{
					media.Stop();
				}
				catch { }
				try
				{
					media.Dispose();
				}
				catch { }
			}
			media = null;
			if(Host != null)
			{
				Host.Child = null;
				Host.Dispose();
				Host = null;
			}
			#endregion DIRECTSHOW_SOURCE 끝
		
			if(_element_media != null)
			{
				_element_media.Stop();
				_element_media.Close();
				_element_media.Source = null;
				_element_media = null;
			}
			if(rect.Fill != null)
			{
				rect.Fill = null;
			}
		}
	}

	public class StreamingComponent : MediaComponent
	{
		public StreamingComponent(bool bUseWPF) : base(bUseWPF, true)
		{
// 			this.IsStreaming = true;
// 			this.UseWPF = bUseWPF;
		}
	}
}