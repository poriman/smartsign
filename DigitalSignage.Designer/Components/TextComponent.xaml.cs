﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Markup;
using System.Xml;
using System.Xml.Linq;
using System.IO;

using DigitalSignage.Common;
using System.Windows.Media.Animation;

namespace WPFDesigner
{
    /// <summary>
    /// Interaction logic for TextComponent.xaml
    /// </summary>
    public partial class TextComponent : UserControl, IDesignElement
    {
        /// <summary>
        /// Represents the playlist of this element
        /// </summary>
        private List<PlaylistItem> _playlist;
        private bool _IsPlay;
        PlaylistPlayer player;

        /// <summary>
        /// Represents the set of changable properties
        /// </summary>
        Dictionary<string, object> _properties;

        public TextComponent()
        {
            InitializeComponent();
            _IsPlay = false;
            _playlist = new List<PlaylistItem>();
            rtb.Document.Background = new SolidColorBrush(Color.FromArgb(0, 255, 255, 255));
            rect.BorderBrush = new SolidColorBrush(Color.FromArgb(255, 0, 0, 0));
            rect.Background = new SolidColorBrush(Color.FromArgb(0, 255, 255, 255));
            rect.BorderThickness = new Thickness(0);
            this.SizeChanged += new SizeChangedEventHandler(TextComponent_SizeChanged);
        }

        void TextComponent_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            Binding b = new Binding();
            b.Source = GetElement;
            b.Mode = BindingMode.OneWay;
            PositionConvert converter = new PositionConvert();
            b.Converter = converter;
            ctrlName.SetBinding(TextBlock.TextProperty, b);
        }

        public IDesignElement GetElement
        {
            get
            {
                return this as IDesignElement;
            }
        }

        #region IDesignElement Members
        DigitalSignage.Common.TransformEffect IDesignElement.TransEffect
        {
            get;
            set;
        }
        Dictionary<string, object> IDesignElement.Properties
        {
            get
            {
                ((IDesignElement)this).InitProperties();
                return _properties;
            }
            set
            {
                _properties = value;
                foreach (string name in _properties.Keys)
                {
                    PropertySetter.SetProperty(this, "IDesignElement", name, _properties[name]);
                }
            }
        }
		/// <summary>
		/// 재생이 시작된 시간
		/// </summary>
		public DateTime PlayStarted { get; set; }

		void IDesignElement.Play(TimeSpan ts)
		{
			if (!_IsPlay)
			{
				//	경계라인
				sepLine.Visibility = Visibility.Collapsed;

				player = new PlaylistPlayer(this);
				player.Start(ts);
				ctrlName.Visibility = Visibility.Collapsed;
				_IsPlay = true;
			}
		}

        void IDesignElement.Play()
        {
            ((IDesignElement)this).Play(TimeSpan.Zero);
        }

        void IDesignElement.Seek(TimeSpan ts)
        {
            if (player != null) player.Seek(ts);
        }



		void IDesignElement.Pause()
		{
			try
			{
				player.Stop();
			}
			catch { }
		}

        bool IDesignElement.ISPlay
        {
            get { return _IsPlay; }
        }

        void IDesignElement.Stop()
        {
			//	경계라인
			sepLine.Visibility = Visibility.Visible;

			player.Stop();
            ctrlName.Visibility = Visibility.Visible;
            rtb.Document = new FlowDocument();
            sub_rtb.Document = new FlowDocument();
            _IsPlay = false;
            GC.Collect();
        }

        void IDesignElement.InitProperties()
        {
            _properties = new Dictionary<string, object>();
            IDesignElement child = this as IDesignElement;

            _properties.Add("Name", child.Name);
            _properties.Add("Width", child.Width);
            _properties.Add("Height", child.Height);
            _properties.Add("Left", child.Left);
            _properties.Add("Top", child.Top);
            _properties.Add("HorizontalAlignment", child.HorizontalAlignment);
            _properties.Add("VerticalAlignment", child.VerticalAlignment);
            _properties.Add("VerticalContentAlignment", child.VerticalContentAlignment);
            _properties.Add("Opacity", child.Opacity);
            _properties.Add("ZIndex", child.ZIndex);
            _properties.Add("BorderBrush", child.BorderBrush);
            _properties.Add("BorderThickness", child.BorderThickness);
            _properties.Add("Background", child.Background);
            _properties.Add("BorderCorner", child.BorderCorner);
            _properties.Add("Type", child.Type);
            _properties.Add("Playlist", child.Playlist);
            _properties.Add("TransEffect", child.TransEffect);
        }

        string IDesignElement.Name
        {
            get
            {
                return Name;
            }
            set
            {
                Name = value;

                Binding b = new Binding();
                b.Source = GetElement;
                b.Mode = BindingMode.OneWay;
                PositionConvert converter = new PositionConvert();
                b.Converter = converter;
                ctrlName.SetBinding(TextBlock.TextProperty, b);
            }
        }

        double IDesignElement.Width
        {
            get
            {
                return ActualWidth;
            }
            set
            {
                Width = value;
            }
        }

        double IDesignElement.Height
        {
            get
            {
                return ActualHeight;
            }
            set
            {
                Height = value;
            }
        }

        double IDesignElement.Left
        {
            get
            {
                return (double)this.GetValue(InkCanvas.LeftProperty);
            }
            set
            {
                this.SetValue(InkCanvas.LeftProperty, value);
            }
        }

        double IDesignElement.Top
        {
            get
            {
                return (double)this.GetValue(InkCanvas.TopProperty);
            }
            set
            {
                SetValue(InkCanvas.TopProperty, value);
            }
        }

        double IDesignElement.Bottom
        {
            get
            {
                return (double)this.GetValue(InkCanvas.BottomProperty);
            }
            set
            {
                this.SetValue(InkCanvas.BottomProperty, value);
            }
        }

        double IDesignElement.Right
        {
            get
            {
                return (double)this.GetValue(InkCanvas.RightProperty);
            }
            set
            {
                SetValue(InkCanvas.RightProperty, value);
            }
        }

        int IDesignElement.ZIndex
        {
            get
            {
                return (int)this.GetValue(Canvas.ZIndexProperty);
            }
            set
            {
                this.SetValue(Canvas.ZIndexProperty, value);
            }
        }

        Brush IDesignElement.BorderBrush
        {
            get
            {
                return this.rect.BorderBrush;
            }
            set
            {
                this.rect.BorderBrush = value;
            }
        }

        double IDesignElement.BorderThickness
        {
            get
            {
                return rect.BorderThickness.Bottom;
            }
            set
            {
                rect.BorderThickness = new Thickness(value);
            }
        }

        Brush IDesignElement.Background
        {
            get
            {
                return rect.Background;
            }
            set
            {
                rect.Background = value;
            }
        }

        double IDesignElement.BorderCorner
        {
            get
            {
                return rect.CornerRadius.BottomLeft;
            }
            set
            {
                rect.CornerRadius = new CornerRadius(value);
				sepLine.RadiusX = sepLine.RadiusY = value;

            }
        }

        double IDesignElement.Opacity
        {
            get
            {
                return this.Opacity;
            }
            set
            {
                this.Opacity = value;
            }
        }

        Stretch IDesignElement.Stretch
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        HorizontalAlignment IDesignElement.HorizontalAlignment
        {
            get
            {
                return this.HorizontalAlignment;
            }
            set
            {
                this.HorizontalAlignment = value;
            }
        }

        VerticalAlignment IDesignElement.VerticalAlignment
        {
            get
            {
                return this.VerticalAlignment;
            }
            set
            {
                this.VerticalAlignment = value;
            }
        }

        double IDesignElement.Volume
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        bool IDesignElement.Mute
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

		TimeSpan IDesignElement.RefreshInterval
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
				throw new NotImplementedException();
			}
		}

        FontFamily IDesignElement.FontFamily
        {
            get
            {
                rtb.SelectAll();

                return rtb.Selection.GetPropertyValue(TextElement.FontFamilyProperty) as FontFamily;
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        double IDesignElement.FontSize
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        FontWeight IDesignElement.FontWeight
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        Brush IDesignElement.Foreground
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        int[] IDesignElement.StrokesLength
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        PenLineCap IDesignElement.StrokeDashCap
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        char IDesignElement.SeparatorChar
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        Point IDesignElement.AspectRatio
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        Type IDesignElement.Type
        {
            get
            {
                return GetType();
            }
        }

        List<PlaylistItem> IDesignElement.Playlist
        {
            get
            {
                return _playlist;
            }
            set
            {
                _playlist = value;
                rtb.UpdateLayout();
            }
        }

        void MyDoubleAnimation_Completed(object sender, EventArgs e)
        {
            rtb.Document = new FlowDocument();
        }

        public void DoubleAnimation_Top()
        {
            //더블 애니메이션 하나 설정 했다. 
            DoubleAnimation MyDoubleAnimation = new DoubleAnimation();

            MyDoubleAnimation.From = -this.Height;
            MyDoubleAnimation.To = 0;
            //가속도값 설정하기 
            BounceEase be = new BounceEase();
            be.Bounciness = 15;
            be.Bounces = 1;

            CubicEase ease = new CubicEase();
            ease.EasingMode = EasingMode.EaseIn;
            MyDoubleAnimation.EasingFunction = ease;

            MyDoubleAnimation.Duration = new Duration(TimeSpan.FromSeconds(0.5));
            //애니메이션 효과를 적용한 후에는 속성 값을 변경하기 
            MyDoubleAnimation.FillBehavior = FillBehavior.Stop;
            MyDoubleAnimation.Completed += new EventHandler(MyDoubleAnimation_Completed);
            sub_rect.BeginAnimation(Canvas.TopProperty, MyDoubleAnimation);
        }

        public void DoubleAnimation_Bottom()
        {
            //더블 애니메이션 하나 설정 했다. 
            DoubleAnimation MyDoubleAnimation = new DoubleAnimation();
            MyDoubleAnimation.From = -this.Height;
            MyDoubleAnimation.To = 0;
            //가속도값 설정하기 
            BounceEase be = new BounceEase();
            be.Bounciness = 15;
            be.Bounces = 1;

            CubicEase ease = new CubicEase();
            ease.EasingMode = EasingMode.EaseIn;
            MyDoubleAnimation.EasingFunction = ease;

            MyDoubleAnimation.Duration = new Duration(TimeSpan.FromSeconds(0.5));
            //애니메이션 효과를 적용한 후에는 속성 값을 변경하기 
            MyDoubleAnimation.FillBehavior = FillBehavior.Stop;
            MyDoubleAnimation.Completed += new EventHandler(MyDoubleAnimation_Completed);
            sub_rect.BeginAnimation(Canvas.BottomProperty, MyDoubleAnimation);
        }

        public void DoubleAnimation_Left()
        {
            //더블 애니메이션 하나 설정 했다. 
            DoubleAnimation MyDoubleAnimation = new DoubleAnimation();
            //Image IMG = MyEllipseCanvas.Children[0] as Image;
            MyDoubleAnimation.From = -this.Width;
            MyDoubleAnimation.To = 0;
            //가속도값 설정하기 
            BounceEase be = new BounceEase();
            be.Bounciness = 15;
            be.Bounces = 1;

            CubicEase ease = new CubicEase();
            ease.EasingMode = EasingMode.EaseIn;
            MyDoubleAnimation.EasingFunction = ease;

            MyDoubleAnimation.Duration = new Duration(TimeSpan.FromSeconds(0.5));
            //애니메이션 효과를 적용한 후에는 속성 값을 변경하기 
            //애니메이션 효과를 적용한 후에는 속성 값을 변경하기 
            MyDoubleAnimation.FillBehavior = FillBehavior.Stop;
            MyDoubleAnimation.Completed += new EventHandler(MyDoubleAnimation_Completed);

            sub_rect.BeginAnimation(Canvas.LeftProperty, MyDoubleAnimation);

        }

        public void DoubleAnimation_Right()
        {
            //더블 애니메이션 하나 설정 했다. 
            DoubleAnimation MyDoubleAnimation = new DoubleAnimation();
            MyDoubleAnimation.From = -this.Width;
            MyDoubleAnimation.To = 0;
            //가속도값 설정하기 
            BounceEase be = new BounceEase();
            be.Bounciness = 15;
            be.Bounces = 1;

            CubicEase ease = new CubicEase();
            ease.EasingMode = EasingMode.EaseIn;
            MyDoubleAnimation.EasingFunction = ease;

            MyDoubleAnimation.Duration = new Duration(TimeSpan.FromSeconds(0.5));
            //애니메이션 효과를 적용한 후에는 속성 값을 변경하기 
            //애니메이션 효과를 적용한 후에는 속성 값을 변경하기 
            MyDoubleAnimation.FillBehavior = FillBehavior.Stop;
            MyDoubleAnimation.Completed += new EventHandler(MyDoubleAnimation_Completed);

            sub_rect.BeginAnimation(Canvas.RightProperty, MyDoubleAnimation);
        }

        public void DoubleAnimation_Fadein()
        {
            //더블 애니메이션 하나 설정 했다. 
            DoubleAnimation MyDoubleAnimation = new DoubleAnimation();
            MyDoubleAnimation.From = 0;
            MyDoubleAnimation.To = 1;

            MyDoubleAnimation.Duration = new Duration(TimeSpan.FromSeconds(0.5));
            //애니메이션 효과를 적용한 후에는 속성 값을 변경하기 
            //애니메이션 효과를 적용한 후에는 속성 값을 변경하기 
            MyDoubleAnimation.FillBehavior = FillBehavior.Stop;
            MyDoubleAnimation.Completed += new EventHandler(MyDoubleAnimation_Completed);
            sub_rect.BeginAnimation(Canvas.OpacityProperty, MyDoubleAnimation);

            //더블 애니메이션 하나 설정 했다. 
            DoubleAnimation MyDoubleAnimation2 = new DoubleAnimation();
            MyDoubleAnimation2.From = 1;
            MyDoubleAnimation2.To = 0;

            MyDoubleAnimation2.Duration = new Duration(TimeSpan.FromSeconds(0.5));
            //애니메이션 효과를 적용한 후에는 속성 값을 변경하기 
            //애니메이션 효과를 적용한 후에는 속성 값을 변경하기 
            MyDoubleAnimation2.FillBehavior = FillBehavior.Stop;
            rect.BeginAnimation(Canvas.OpacityProperty, MyDoubleAnimation2);
        }

        void ApplyTransformEffect()
        {
            switch (this.GetElement.TransEffect)
            {
                case TransformEffect.TopToBottom:
                    DoubleAnimation_Top();
                    break;
                case TransformEffect.BottomToTop:
                    DoubleAnimation_Bottom();
                    break;
                case TransformEffect.LeftToRight:
                    DoubleAnimation_Left();
                    break;
                case TransformEffect.RightToLeft:
                    DoubleAnimation_Right();
                    break;
                case TransformEffect.FadeIn:
                    DoubleAnimation_Fadein();
                    break;

                default:
                    rtb.Document = new FlowDocument();
                    break;
            }
        }

        string IDesignElement.Content
        {
            get
            {
                return FlowDocumentManager.SerializeFlowDocument(sub_rtb.Document);
            }
            set
            {
                if (value == "")
                {
                    ctrlName.Visibility = Visibility.Visible;
                    rtb.Document = new FlowDocument();
                    return;
                }
                StringBuilder sb = new StringBuilder(value);
                XElement def = XElement.Parse(sb.ToString());
                foreach (XElement el in def.Descendants())
                {
                    if (el.Name.LocalName == "ImageBrush")
                    {
                        el.Attribute("ImageSource").Value = ((IDesignElement)this).FileManager.GetFullFilePathByName(System.IO.Path.GetFileName(el.Attribute("ImageSource").Value));
                    }
                }
                rtb.DataContext = sub_rtb.DataContext;

                sub_rtb.Document = FlowDocumentManager.DeserializeFlowDocument(def.ToString());
                
                sub_rect.Width = this.Width;
                sub_rect.Height = this.Height;

                rect.Width = this.Width;
                rect.Height = this.Height;

                rect.Visibility = Visibility.Visible;
                ApplyTransformEffect();

                rtb.UpdateLayout();
            }
        }

        IMediaFilesManager IDesignElement.FileManager
        {
            get { return Parent as IMediaFilesManager; }
        }

        VerticalAlignment IDesignElement.VerticalContentAlignment
        {
            get
            {
                return rtb.VerticalAlignment;
            }
            set
            {
                rtb.VerticalAlignment = value;
            }
        }

        HorizontalAlignment IDesignElement.HorizontalContentAlignment
        {
            get
            {
                return rtb.HorizontalAlignment;
            }
            set
            {
                rtb.HorizontalAlignment = value;
            }
        }

		System.Drawing.Bitmap IDesignElement.GetThumbnail(int cx, int cy)
		{
			return null;
		}

        #endregion
    }
}
