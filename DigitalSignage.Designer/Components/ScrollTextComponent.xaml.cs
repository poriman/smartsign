﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Media.Media3D;
using DigitalSignage.Common;
using System.Xml;
using System.Xml.Linq;

namespace WPFDesigner
{
    /// <summary>
    /// Interaction logic for ScrollTextComponent.xaml
    /// </summary>
    public partial class ScrollTextComponent : UserControl, IDesignElement
    {
        private List<PlaylistItem> _playlist;
        private bool _IsPlay;
        PlaylistPlayer player;

        /// <summary>
        /// Represents the set of changable properties
        /// </summary>
        Dictionary<string, object> _properties;

        public ScrollTextComponent()
        {
            InitializeComponent();
            _IsPlay = false;
            _playlist = new List<PlaylistItem>();
            BorderThickness = new Thickness(0);
            BorderBrush = new SolidColorBrush(Color.FromArgb(255, 0, 0, 0));
            this.SizeChanged += new SizeChangedEventHandler(ScrollTextComponent_SizeChanged);
        }

        void ScrollTextComponent_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            Binding b = new Binding();
            b.Source = GetElement;
            b.Mode = BindingMode.OneWay;
            PositionConvert converter = new PositionConvert();
            b.Converter = converter;
            ctrlName.SetBinding(TextBlock.TextProperty, b);
        }

        public IDesignElement GetElement
        {
            get
            {
                return this as IDesignElement;
            }
        }

        public void Load(ScrollTextInfo info)
        {
            scroll.Load(info);
        }

        #region IDesignElement Members
        DigitalSignage.Common.TransformEffect IDesignElement.TransEffect
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }
        Dictionary<string, object> IDesignElement.Properties
        {
            get
            {
                ((IDesignElement)this).InitProperties();
                return _properties;
            }
            set
            {
                _properties = value;
                foreach (string name in _properties.Keys)
                {
                    PropertySetter.SetProperty(this, "IDesignElement", name, _properties[name]);
                }
            }
        }

		/// <summary>
		/// 재생이 시작된 시간
		/// </summary>
		public DateTime PlayStarted { get; set; }

        void IDesignElement.Play(TimeSpan ts)
        {
			if (!_IsPlay)
			{
				//	경계라인
				sepLine.Visibility = Visibility.Collapsed;

				ctrlName.Visibility = Visibility.Collapsed;
				player = new PlaylistPlayer(this);
				player.Start(ts);
				_IsPlay = true;
			}
        }

        void IDesignElement.Play()
        {
            ((IDesignElement)this).Play(TimeSpan.Zero);
        }

        void IDesignElement.Seek(TimeSpan ts)
        {
            if (player != null) player.Seek(ts);
        }

		void IDesignElement.Pause()
		{
			try
			{
				player.Stop();
			}
			catch { }
		}

        bool IDesignElement.ISPlay
        {
            get { return _IsPlay; }
        }

        void IDesignElement.Stop()
        {
			//	경계라인
			sepLine.Visibility = Visibility.Visible;
			ctrlName.Visibility = Visibility.Visible;
            player.Stop();
            scroll.Stop();
            _IsPlay = false;
            GC.Collect();
        }

        void IDesignElement.InitProperties()
        {
            _properties = new Dictionary<string, object>();
            IDesignElement child = this as IDesignElement;

            _properties.Add("Name", child.Name);
            _properties.Add("Width", child.Width);
            _properties.Add("Height", child.Height);
            _properties.Add("Left", child.Left);
            _properties.Add("Top", child.Top);
            _properties.Add("HorizontalAlignment", child.HorizontalAlignment);
            _properties.Add("VerticalAlignment", child.VerticalAlignment);
            _properties.Add("ZIndex", child.ZIndex);
            _properties.Add("Opacity", child.Opacity);
            _properties.Add("BorderBrush", child.BorderBrush);
            _properties.Add("BorderThickness", child.BorderThickness);
            _properties.Add("Background", child.Background);
            //_properties.Add("Foreground", child.Foreground);
            //_properties.Add("FontFamily", child.FontFamily);
            //_properties.Add("FontSize", child.FontSize);
            //_properties.Add("FontWeight", child.FontWeight);
            _properties.Add("Type", child.Type);
            _properties.Add("Playlist", child.Playlist);
        }

        string IDesignElement.Name
        {
            get
            {
                return Name;
            }
            set
            {
                Name = value;

                Binding b = new Binding();
                b.Source = GetElement;
                b.Mode = BindingMode.OneWay;
                PositionConvert converter = new PositionConvert();
                b.Converter = converter;
                ctrlName.SetBinding(TextBlock.TextProperty, b);
            }
        }

        double IDesignElement.Width
        {
            get
            {
                return ActualWidth;
            }
            set
            {
                Width = value;
            }
        }

        double IDesignElement.Height
        {
            get
            {
                return ActualHeight;
            }
            set
            {
                Height = value;
            }
        }

        double IDesignElement.Left
        {
            get
            {
                return (double)this.GetValue(InkCanvas.LeftProperty);
            }
            set
            {
                this.SetValue(InkCanvas.LeftProperty, value);
            }
        }

        double IDesignElement.Top
        {
            get
            {
                return (double)this.GetValue(InkCanvas.TopProperty);
            }
            set
            {
                SetValue(InkCanvas.TopProperty, value);
            }
        }

        double IDesignElement.Bottom
        {
            get
            {
                return (double)this.GetValue(InkCanvas.BottomProperty);
            }
            set
            {
                this.SetValue(InkCanvas.BottomProperty, value);
            }
        }

        double IDesignElement.Right
        {
            get
            {
                return (double)this.GetValue(InkCanvas.RightProperty);
            }
            set
            {
                SetValue(InkCanvas.RightProperty, value);
            }
        }

        int IDesignElement.ZIndex
        {
            get
            {
                return (int)this.GetValue(Canvas.ZIndexProperty);
            }
            set
            {
                this.SetValue(Canvas.ZIndexProperty, value);
            }
        }

        Brush IDesignElement.BorderBrush
        {
            get
            {
                return BorderBrush;
            }
            set
            {
                BorderBrush = value;
            }
        }

        double IDesignElement.BorderThickness
        {
            get
            {
                return BorderThickness.Bottom;
            }
            set
            {
                BorderThickness = new Thickness(value);
            }
        }

        double IDesignElement.BorderCorner
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

		TimeSpan IDesignElement.RefreshInterval
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
				throw new NotImplementedException();
			}
		}

        HorizontalAlignment IDesignElement.HorizontalAlignment
        {
            get
            {
                return HorizontalAlignment;
            }
            set
            {
                HorizontalAlignment = value;
            }
        }

        VerticalAlignment IDesignElement.VerticalAlignment
        {
            get
            {
                return VerticalAlignment;
            }
            set
            {
                VerticalAlignment = value;
            }
        }

        double IDesignElement.Opacity
        {
            get
            {
                return Opacity;
            }
            set
            {
                Opacity = value;
            }
        }

        Stretch IDesignElement.Stretch
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        double IDesignElement.Volume
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        bool IDesignElement.Mute
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        Brush IDesignElement.Background
        {
            get
            {
                return scroll.Background;
            }
            set
            {
                scroll.Background = value;
            }
        }

        FontFamily IDesignElement.FontFamily
        {
            get
            {
                return ((ITextContainer)scroll).GetFontFamily();
            }
            set
            {
                ((ITextContainer)scroll).SetFontFamily(value);
            }
        }

        double IDesignElement.FontSize
        {
            get
            {
                return ((ITextContainer)scroll).GetFontSize();
            }
            set
            {
                ((ITextContainer)scroll).SetFontSize(value);
            }
        }

        FontWeight IDesignElement.FontWeight
        {
            get
            {
                return ((ITextContainer)scroll).GetFontWeight();
            }
            set
            {
                ((ITextContainer)scroll).SetFontWeight(value);
            }
        }

        Brush IDesignElement.Foreground
        {
            get
            {
                return scroll.scrollingLabel.Foreground;
            }
            set
            {
                scroll.scrollingLabel.Foreground = value;
            }
        }

        int[] IDesignElement.StrokesLength
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        PenLineCap IDesignElement.StrokeDashCap
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        char IDesignElement.SeparatorChar
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        Point IDesignElement.AspectRatio
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        Type IDesignElement.Type
        {
            get
            {
                return GetType();
            }
        }

        List<PlaylistItem> IDesignElement.Playlist
        {
            get
            {
                return _playlist;
            }
            set
            {
                _playlist = value;
            }
        }

        string IDesignElement.Content
        {
            get
            {
                return ScrollTextInfoManager.SerializeScrollTextInfo(scroll.Info);
            }
            set
            {
                StringBuilder sb = new StringBuilder(value);
                XElement def = XElement.Parse(sb.ToString());
                foreach (XElement el in def.Descendants())
                {
                    if (el.Name.LocalName == "ImageBrush")
                    {
                        el.Attribute("ImageSource").Value = ((IDesignElement)this).FileManager.GetFullFilePathByName(System.IO.Path.GetFileName(el.Attribute("ImageSource").Value));
                    }
                }
                scroll.Load(ScrollTextInfoManager.DeserializeScrollTextInfo(def.ToString()));
                scroll.Start();
            }
        }
    
        IMediaFilesManager IDesignElement.FileManager
        {
            get { return Parent as IMediaFilesManager; }
        }

		System.Drawing.Bitmap IDesignElement.GetThumbnail(int cx, int cy)
		{
			return null;
		}

		#endregion
    }
}