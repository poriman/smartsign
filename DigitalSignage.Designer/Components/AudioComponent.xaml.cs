﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DigitalSignage.Common;

namespace WPFDesigner
{
    /// <summary>
    /// Interaction logic for AudioComponent.xaml
    /// </summary>
    public partial class AudioComponent : UserControl, IDesignElement
    {
        private List<PlaylistItem> _playlist;
        private bool _IsPlay;
        private PlaylistPlayer _player;
        private MediaPlayer _audioPlayer;

        /// <summary>
        /// Represents the set of changable properties
        /// </summary>
        Dictionary<string, object> _properties;

        public AudioComponent()
        {
            InitializeComponent();
            _IsPlay = false;
            _audioPlayer = new MediaPlayer();
            _audioPlayer.MediaEnded += new EventHandler(_audioPlayer_MediaEnded);
			_audioPlayer.Volume = 1;
            _playlist = new List<PlaylistItem>();
        }

        void _audioPlayer_MediaEnded(object sender, EventArgs e)
        {
            _audioPlayer.Position = new TimeSpan(0, 0, 0);
            _audioPlayer.Play();
        }

        #region IDesignElement Members

        TransformEffect IDesignElement.TransEffect
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        bool IDesignElement.ISPlay
        {
            get { return _IsPlay; }
        }

        public Dictionary<string, object> Properties
        {
            get
            {
                ((IDesignElement)this).InitProperties();
                return _properties;
            }
            set
            {
                _properties = value;
                _properties = value;
                foreach (string name in _properties.Keys)
                {
                    PropertySetter.SetProperty(this, "IDesignElement", name, _properties[name]);
                }
            }
        }

		/// <summary>
		/// 재생이 시작된 시간
		/// </summary>
		public DateTime PlayStarted { get; set; }

        public void Play(TimeSpan ts)
        {
            Visibility = Visibility.Collapsed;
            _player = new PlaylistPlayer(this as IDesignElement);
            _player.Start(ts);
            _IsPlay = true;
        }

        void IDesignElement.Play()
        {
            ((IDesignElement)this).Play(TimeSpan.Zero);
        }

        void IDesignElement.Seek(TimeSpan ts)
        {
            if (_player != null) _player.Seek(ts);
        }

        public void Pause()
		{
			try
			{
				_player.Stop();

				_audioPlayer.Pause();
			}
			catch { }
		}

        public void Stop()
        {
            _audioPlayer.Stop();
            Visibility = Visibility.Visible;
            _player.Stop();
            _IsPlay = false;
        }

        public void InitProperties()
        {
            _properties = new Dictionary<string, object>();
            IDesignElement child = this as IDesignElement;
            _properties.Add("Name", child.Name);
            _properties.Add("Volume", child.Volume);
            _properties.Add("Mute", child.Mute);
			_properties.Add("ZIndex", child.ZIndex);
            _properties.Add("Playlist", child.Playlist);
            _properties.Add("Type", child.Type);
		}

		/// <summary>
		/// Gets or sets the horizontal ratio.
		/// </summary>
		public double HorizontalRatio
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
				throw new NotImplementedException();
			}
		}

		TimeSpan IDesignElement.RefreshInterval
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
				throw new NotImplementedException();
			}
		}
		/// <summary>
		/// Gets or sets the vertical ratio.
		/// </summary>
		public double VerticalRatio
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
				throw new NotImplementedException();
			}
		}

        double IDesignElement.Left
        {
            get
            {
                return (double)this.GetValue(InkCanvas.LeftProperty);
            }
            set
            {
                this.SetValue(InkCanvas.LeftProperty, value);
            }
        }

        double IDesignElement.Top
        {
            get
            {
                return (double)this.GetValue(InkCanvas.TopProperty);
            }
            set
            {
                SetValue(InkCanvas.TopProperty, value);
            }
        }

        double IDesignElement.Bottom
        {
            get
            {
                return (double)this.GetValue(InkCanvas.BottomProperty);
            }
            set
            {
                this.SetValue(InkCanvas.BottomProperty, value);
            }
        }

        double IDesignElement.Right
        {
            get
            {
                return (double)this.GetValue(InkCanvas.RightProperty);
            }
            set
            {
                SetValue(InkCanvas.RightProperty, value);
            }
        }

        public int ZIndex
        {
            get
            {
				return (int)this.GetValue(Canvas.ZIndexProperty);
			}
            set
            {
				SetValue(Canvas.ZIndexProperty, value);
            }
        }

        public new double BorderThickness
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public double BorderCorner
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public Stretch Stretch
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public double Volume
        {
            get
            {
                return _audioPlayer.Volume;
            }
            set
            {
                _audioPlayer.Volume = value;
            }
        }

        public bool Mute
        {
            get
            {
                return _audioPlayer.IsMuted;
            }
            set
            {
                _audioPlayer.IsMuted = value;
            }
        }

        public int[] StrokesLength
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public PenLineCap StrokeDashCap
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public char SeparatorChar
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public Point AspectRatio
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public Type Type
        {
            get { return GetType(); }
        }

        public List<PlaylistItem> Playlist
        {
            get
            {
                return _playlist;
            }
            set
            {
                _playlist = value;
            }
        }

        public new string Content
        {
            get
            {
                return _audioPlayer.Source.AbsolutePath;
            }
            set
            {
                _audioPlayer.Open(new Uri(((IDesignElement)this).FileManager.GetFullFilePathByName(value)));
                _audioPlayer.Play();
            }
        }

        public IMediaFilesManager FileManager
        {
            get { return ((IMediaFilesManager)this.Parent); }
        }

		System.Drawing.Bitmap IDesignElement.GetThumbnail(int cx, int cy)
		{
			return null;
		}

        #endregion

		private void UserControl_Unloaded(object sender, RoutedEventArgs e)
		{
			if(_audioPlayer != null)
			{
				_audioPlayer.Stop();
				_audioPlayer.Close();

				_audioPlayer = null;
			}
		}
    }
}
