﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WPFDesigner
{
    /// <summary>
    /// Interaction logic for ScrollTextWindow.xaml
    /// </summary>
    public partial class ScrollTextWindow : Window
    {
        ScrollTextComponent scroll;

        public ScrollTextWindow()
        {
            InitializeComponent();
            scroll = new ScrollTextComponent();
            mainGrid.Children.Add(scroll);
        }

        /// <summary>
        /// Gets or sets the properties set of scrolling text
        /// </summary>
        public Dictionary<string, object> Properties
        {
            get
            {
                return ((IDesignElement)scroll).Properties;
            }
            set
            {
                ((IDesignElement)scroll).Properties = value;
            }
        }

        public void ShowAndStart(ScrollTextInfo info)
        {
            this.Show();
            scroll.Load(info);
            //scroll.Start();
        }

        public void StopAndHide()
        {
            //scroll.Stop();
            this.Close();
        }
    }
}
