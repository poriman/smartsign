﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Media.Animation;
using System.Windows.Threading;
using DigitalSignage.Common;
using Sloppycode.net;

using NLog;

namespace WPFDesigner
{
    /// <summary>
    /// Interaction logic for RSSTextPage.xaml
    /// </summary>
    public partial class RSSText : UserControl, ITextContainer, IDecoratable
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        DoubleAnimation animation;
        //TimeSpan duration;
        TranslateTransform transformation;
        ScrollTextDirection direction;
        int speed;
        string url;
        DispatcherTimer dt;
        int tickCount = 0;
        Thread load;
        Brush fontBrush;
        FontWeight fontWeight;
        // font configuration
        private double fontSize;
        private FontFamily fontFamily;

        // separators
        BitmapImage separatorImg;
        char separatorChar = '-';

        public RSSText()
        {
            try
            {
                // defaults
                fontSize = 20;
                fontBrush = new SolidColorBrush(Color.FromArgb(255, 0, 0, 0));
                fontFamily = new FontFamily("NanumGothic");

                InitializeComponent();
                animation = new DoubleAnimation();
                //            duration = new TimeSpan();
            }
            catch (Exception ex)
            {
                logger.Error(ex + "");
            }
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {

        }

		/// <summary>
		/// Loads the scrolling text component parameters
		/// </summary>
		/// <param name="stInfo">Information that will be scrolling</param>
		public void Load(ScrollTextInfo stInfo)
        {
            try
            {
                url = (string)stInfo.content;
                this.direction = stInfo.direction;
                //Label label = ((Label)(((Canvas)this.Content).Children[0]));
                //if ( label.Content.Equals("") )
                //label.Content = url;
                UpdateLayout();
                //duration = CalculateDurationFromSpeed(stInfo.speed);
                speed = stInfo.speed;
                //this.Background = stInfo.background;
            }
            catch (Exception ex)
            {
                logger.Error(ex + "");
            }
        }

        TimeSpan CalculateDurationFromSpeed(int speed)
        {
            TimeSpan ts = new TimeSpan();
            try
            {
                StackPanel content = ((Canvas)this.Content).Children[0] as StackPanel;
                int nsp = 0;
                if ((direction == ScrollTextDirection.BottomToTop) ||
                    (direction == ScrollTextDirection.TopToBottom))
                    nsp = (int)((content.ActualHeight + ActualHeight) / speed);
                if ((direction == ScrollTextDirection.LeftToRight) ||
                    (direction == ScrollTextDirection.RightToLeft))
                    nsp = (int)((content.ActualWidth + ActualWidth) / speed);
                ts = new TimeSpan(0, 0, nsp);
            }
            catch (Exception ex)
            {
                logger.Error(ex + "");
            }
            return ts;
        }

        /// <summary>
        /// Starts the text scrolling
        /// </summary>
        public void Start()
        {
            try
            {
                UpdateLayout();
                //duration = ;
                animation = new DoubleAnimation();
                transformation = new TranslateTransform();
                animation.Duration = new Duration(CalculateDurationFromSpeed(speed));
                animation.RepeatBehavior = RepeatBehavior.Forever;
                ((StackPanel)(((Canvas)this.Content).Children[0])).RenderTransform = transformation;
                recalAnimation();

                dt = new DispatcherTimer();
                dt.Interval = new TimeSpan(0, 0, 20);
                dt.Tick += new EventHandler(dt_Tick);
                dt.Start();
                load = new Thread(this.UpdateChannelContent);
                load.IsBackground = true;
                load.Start();
            }
            catch (Exception ex)
            {
                logger.Error(ex + "");
            }
        }

        public void Stop()
        {
            try
            {
                animation = new DoubleAnimation();
                transformation.BeginAnimation(TranslateTransform.XProperty, animation);
                transformation.BeginAnimation(TranslateTransform.YProperty, animation);
                dt.Stop();
            }
            catch (Exception ex)
            {
                logger.Error(ex + "");
            }
        }

        void UpdateChannelContent()
        {
            try
            {
                RssReader rssReader = new RssReader();
                rssReader.RdfMode = false;
                RssFeed feed = rssReader.Retrieve(url);
                if (feed.ErrorMessage == null || feed.ErrorMessage == "")
                    outResults(feed);
            }
            catch (Exception ex)
            {
                logger.Error(ex + "");
            }
            return;
        }

        void dt_Tick(object sender, EventArgs e)
        {
            try
            {
                tickCount--;
                if (tickCount < 0)
                {
                    if (!load.IsAlive)
                    {
                        load = new Thread(this.UpdateChannelContent);
                        load.IsBackground = true;
                        load.Start();
                    }
                    tickCount = 12;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex + "");
            }
        }

        // This delegate enables asynchronous calls for setting
        // the text property on a TextBox control.
        delegate void outResultsCB( RssFeed feed);

        private void outResults( RssFeed feed )
        {
            try
            {
                if (!this.Dispatcher.CheckAccess())
                {
                    this.Dispatcher.BeginInvoke(DispatcherPriority.Render,
                       new outResultsCB(outResults), feed );
                }
                else
                {
                    ((StackPanel)((Canvas)this.Content).Children[0]).Children.Clear();
                    for (int i = 0; i < feed.Items.Count; i++)
                    {
                        Label lable = new Label();
                        lable.FontSize = fontSize;
                        lable.Foreground = fontBrush;
                        lable.FontWeight = this.fontWeight;
                        lable.FontFamily = fontFamily;
                        lable.Content = feed.Items[i].Title + " - " + feed.Items[i].Description;
                        if ( separatorChar != '\0' )
                            lable.Content += " "+separatorChar+" ";
                        ((StackPanel)((Canvas)this.Content).Children[0]).Children.Add(lable);
                        if (separatorImg != null)
                        {
                            Image img = new Image();
                            img.Source = separatorImg;
                            ((StackPanel)((Canvas)this.Content).Children[0]).Children.Add( img );
                        }
                    }
                    recalAnimation();
                    UpdateLayout();
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex + "");
            }
        }

        public BitmapImage SeparatorImg
        {
            get { return separatorImg; }
            set 
            {
                separatorChar = '\0';
                separatorImg = value;
            }
        }

        public char SeparatorChar
        {
            get { return separatorChar; }
            set
            {
                separatorImg = null;
                separatorChar = value;
            }
        }

        private void recalAnimation()
        {
            try
            {
                int width = (int)((StackPanel)((Canvas)this.Content).Children[0]).ActualWidth;
                int height = (int)((StackPanel)((Canvas)this.Content).Children[0]).ActualHeight;

                switch (direction)
                {
                    case ScrollTextDirection.LeftToRight:
                        animation.From = 0;
                        animation.To = width;
                        transformation.BeginAnimation(TranslateTransform.XProperty, animation);
                        break;
                    case ScrollTextDirection.RightToLeft:
                        animation.From = this.ActualWidth;
                        animation.To = -width;
                        transformation.BeginAnimation(TranslateTransform.XProperty, animation);
                        break;
                    case ScrollTextDirection.TopToBottom:
                        animation.From = -height;
                        animation.To = height;
                        transformation.BeginAnimation(TranslateTransform.YProperty, animation);
                        break;
                    case ScrollTextDirection.BottomToTop:
                        animation.From = height;
                        animation.To = -height;
                        transformation.BeginAnimation(TranslateTransform.YProperty, animation);
                        break;
                    default: break;
                }
                animation.Duration = CalculateDurationFromSpeed(speed);

            }
            catch (Exception ex)
            {
                logger.Error(ex + "");
            }
        }

        #region IDecorable
        public void SetBorderBrush(Brush borderbrush)
        {
            this.BorderBrush = borderbrush;
        }

        public void SetBorderThickness(Thickness borderThickness)
        {
            this.BorderThickness = borderThickness;
        }

        public void SetBackround(Brush background)
        {
            this.Background = background;
        }

        public void SetCornerRadius(double cornerRadius)
        {
            // not implemented
        }

        public Brush GetBorderBrush()
        {
            if (BorderBrush != null)
                return BorderBrush;
            else return new SolidColorBrush(Color.FromArgb(255, 255, 255, 255));
        }

        public Brush GetBackgound()
        {
            return Background;
        }

        public Thickness GetBorderThickness()
        {
            return BorderThickness;
        }

        public double GetCornerRadius()
        {
            return -1; // not implemented
        }

        #endregion

        #region ITextContainer
        public void SetFontSize(double fontsize)
        {
            this.fontSize = fontsize;
        }

        public void SetFontBrush(Brush fontcolor)
        {
            fontBrush = fontcolor;
        }

        public double GetFontSize()
        {
            return fontSize;
        }

        public Brush GetFontBrush()
        {
            return fontBrush;
        }

        FontFamily ITextContainer.GetFontFamily()
        {
            return fontFamily;
        }

        void ITextContainer.SetFontFamily(FontFamily family)
        {
            fontFamily = family;
        }

        void ITextContainer.SetFontWeight(FontWeight fontWeight)
        {
            this.fontWeight = fontWeight;
        }

        FontWeight ITextContainer.GetFontWeight()
        {
            return this.fontWeight;
        }
		void ITextContainer.SetDescFontSize(double fontsize)
		{
			throw new NotImplementedException();
		}

		void ITextContainer.SetDescFontBrush(Brush fontcolor)
		{
			throw new NotImplementedException();
		}

		double ITextContainer.GetDescFontSize()
		{
			throw new NotImplementedException();
		}

		Brush ITextContainer.GetDescFontBrush()
		{
			throw new NotImplementedException();
		}

		FontFamily ITextContainer.GetDescFontFamily()
		{
			throw new NotImplementedException();
		}

		void ITextContainer.SetDescFontFamily(FontFamily family)
		{
			throw new NotImplementedException();
		}

		FontWeight ITextContainer.GetDescFontWeight()
		{
			throw new NotImplementedException();
		}

		void ITextContainer.SetDescFontWeight(FontWeight fontWeight)
		{
			throw new NotImplementedException();
		}
        #endregion
    }
}