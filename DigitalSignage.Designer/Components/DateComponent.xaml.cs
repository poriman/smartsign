﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Media.Media3D;
using DigitalSignage.Common;

namespace WPFDesigner
{
    /// <summary>
    /// Interaction logic for DateComponent.xaml
    /// </summary>
    public partial class DateComponent : IDesignElement
    {
        List<PlaylistItem> _playlist;
        private bool _IsPlay;
        /// <summary>
        /// Represents the set of changable properties
        /// </summary>
        Dictionary<string, object> _properties;

        public DateComponent()
        {
            InitializeComponent();
            _IsPlay = false;
            Rect.Stroke = new SolidColorBrush(Color.FromArgb(255, 128, 128, 128));
            Rect.StrokeThickness = 2;
            this.SizeChanged += new SizeChangedEventHandler(DateComponent_SizeChanged);
        }

        void DateComponent_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            Binding b = new Binding();
            b.Source = GetElement;
            b.Mode = BindingMode.OneWay;
            PositionConvert converter = new PositionConvert();
            b.Converter = converter;
            ctrlName.SetBinding(TextBlock.TextProperty, b);
        }

        public IDesignElement GetElement
        {
            get
            {
                return this as IDesignElement;
            }
        }

        #region IDesignElement members
        DigitalSignage.Common.TransformEffect IDesignElement.TransEffect
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }
        Dictionary<string, object> IDesignElement.Properties
        {
            get
            {
                ((IDesignElement)this).InitProperties();
                return _properties;
            }
            set
            {
                _properties = value;
                foreach (string name in _properties.Keys)
                {
                    PropertySetter.SetProperty(this, "IDesignElement", name, _properties[name]);
                }
            }
        }

		/// <summary>
		/// 재생이 시작된 시간
		/// </summary>
		public DateTime PlayStarted { get; set; }

        void IDesignElement.Play(TimeSpan ts)
        {
			if (!_IsPlay)
			{
				ctrlName.Visibility = Visibility.Collapsed;
				Date.Start();
				_IsPlay = true;
			}
        }

        void IDesignElement.Play()
        {
            ((IDesignElement)this).Play(TimeSpan.Zero);
        }

        void IDesignElement.Seek(TimeSpan ts)
        {
        }

        void IDesignElement.Pause()
		{

		}


        bool IDesignElement.ISPlay
        {
            get { return _IsPlay; }
        }

        void IDesignElement.Stop()
        {
            ctrlName.Visibility = Visibility.Visible;
            Date.Stop();
            _IsPlay = false;
            GC.Collect();
        }

        void IDesignElement.InitProperties()
        {
            _properties = new Dictionary<string, object>();
            IDesignElement child = this as IDesignElement;

            _properties.Add("Name", child.Name);
            _properties.Add("Width", child.Width);
            _properties.Add("Height", child.Height);
            _properties.Add("Left", child.Left);
            _properties.Add("Top", child.Top);
            _properties.Add("HorizontalAlignment", child.HorizontalAlignment);
            _properties.Add("VerticalAlignment", child.VerticalAlignment);
            _properties.Add("ZIndex", child.ZIndex);
            _properties.Add("Opacity", child.Opacity);
            _properties.Add("BorderBrush", child.BorderBrush);
            _properties.Add("BorderThickness", child.BorderThickness);
            _properties.Add("StrokeDashCap", child.StrokeDashCap);
            _properties.Add("StrokesLength", child.StrokesLength);
            _properties.Add("Background", child.Background);
            _properties.Add("FontSize", child.FontSize);
            _properties.Add("FontWeight", child.FontWeight);
            _properties.Add("FontFamily", child.FontFamily);
            _properties.Add("Foreground", child.Foreground);
            _properties.Add("BorderCorner", child.BorderCorner);
            _properties.Add("Type", child.Type);
        }

        string IDesignElement.Name
        {
            get
            {
                return Name;
            }
            set
            {
                Name = value;

                Binding b = new Binding();
                b.Source = GetElement;
                b.Mode = BindingMode.OneWay;
                PositionConvert converter = new PositionConvert();
                b.Converter = converter;
                ctrlName.SetBinding(TextBlock.TextProperty, b);
            }
        }

        double IDesignElement.Width
        {
            get
            {
                return ActualWidth;
            }
            set
            {
                Width = value;
            }
        }

        double IDesignElement.Height
        {
            get
            {
                return ActualHeight;
            }
            set
            {
                Height = value;
            }
        }

        double IDesignElement.Left
        {
            get
            {
                return (double)this.GetValue(InkCanvas.LeftProperty);
            }
            set
            {
                this.SetValue(InkCanvas.LeftProperty, value);
            }
        }

        double IDesignElement.Top
        {
            get
            {
                return (double)this.GetValue(InkCanvas.TopProperty);
            }
            set
            {
                SetValue(InkCanvas.TopProperty, value);
            }
        }

        double IDesignElement.Bottom
        {
            get
            {
                return (double)this.GetValue(InkCanvas.BottomProperty);
            }
            set
            {
                this.SetValue(InkCanvas.BottomProperty, value);
            }
        }

        double IDesignElement.Right
        {
            get
            {
                return (double)this.GetValue(InkCanvas.RightProperty);
            }
            set
            {
                SetValue(InkCanvas.RightProperty, value);
            }
        }

        int IDesignElement.ZIndex
        {
            get
            {
                return (int)this.GetValue(Canvas.ZIndexProperty);
            }
            set
            {
                this.SetValue(Canvas.ZIndexProperty, value);
            }
        }

        Brush IDesignElement.BorderBrush
        {
            get
            {
                return Rect.Stroke;
            }
            set
            {
                Rect.Stroke = value;
            }
        }

        double IDesignElement.BorderThickness
        {
            get
            {
                return Rect.StrokeThickness;
            }
            set
            {
                Rect.StrokeThickness = value;
            }
        }

        double IDesignElement.BorderCorner
        {
            get
            {
                return Rect.RadiusX;
            }
            set
            {
                Rect.RadiusX = Rect.RadiusY = value;
            }
        }

        HorizontalAlignment IDesignElement.HorizontalAlignment
        {
            get
            {
                return this.HorizontalAlignment;
            }
            set
            {
                this.HorizontalAlignment = value;
            }
        }

        VerticalAlignment IDesignElement.VerticalAlignment
        {
            get
            {
                return this.VerticalAlignment;
            }
            set
            {
                this.VerticalAlignment = value;
            }
        }

        double IDesignElement.Opacity
        {
            get
            {
                return this.Opacity;
            }
            set
            {
                this.Opacity = value;
            }
        }

        Stretch IDesignElement.Stretch
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        double IDesignElement.Volume
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        bool IDesignElement.Mute
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

		TimeSpan IDesignElement.RefreshInterval
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
				throw new NotImplementedException();
			}
		}

        Brush IDesignElement.Background
        {
            get
            {
                return Date.Background;
            }
            set
            {
                Date.Background = value;
            }
        }

        FontFamily IDesignElement.FontFamily
        {
            get
            {
                return Date.dateLabel.FontFamily;
            }
            set
            {
                Date.dateLabel.FontFamily = value;
            }
        }

        double IDesignElement.FontSize
        {
            get
            {
                return Date.dateLabel.FontSize;
            }
            set
            {
                Date.dateLabel.FontSize = value;
            }
        }

        FontWeight IDesignElement.FontWeight
        {
            get
            {
                return Date.dateLabel.FontWeight;
            }
            set
            {
                Date.dateLabel.FontWeight = value;
            }
        }

        Brush IDesignElement.Foreground
        {
            get
            {
                return Date.dateLabel.Foreground;
            }
            set
            {
                Date.dateLabel.Foreground = value;
            }
        }

        int[] IDesignElement.StrokesLength
        {
            get
            {
                int[] sdarray = new int[Rect.StrokeDashArray.Count];
                int i = 0;
                foreach (double d in Rect.StrokeDashArray)
                {
                    sdarray[i] = Convert.ToInt32(d);
                    i++;
                }
                return sdarray;
            }
            set
            {
                Rect.StrokeDashArray.Clear();
                for (int i = 0; i < value.Count(); i++)
                    Rect.StrokeDashArray.Add(value[i]);
            }
        }

        PenLineCap IDesignElement.StrokeDashCap
        {
            get
            {
                return Rect.StrokeDashCap;
            }
            set
            {
                Rect.StrokeDashCap = value;
            }
        }

        char IDesignElement.SeparatorChar
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        Point IDesignElement.AspectRatio
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        Type IDesignElement.Type
        {
            get
            {
                return GetType();
            }
        }

        List<PlaylistItem> IDesignElement.Playlist
        {
            get
            {
                return _playlist;
            }
            set
            {
                _playlist = value;
            }
        }

        string IDesignElement.Content
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        IMediaFilesManager IDesignElement.FileManager
        {
            get { return Parent as IMediaFilesManager; }
        }

		System.Drawing.Bitmap IDesignElement.GetThumbnail(int cx, int cy)
		{
			return null;
		}

        #endregion
    }
}
