﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Media.Media3D;
using System.Windows.Markup;
using System.Xml;
using System.IO;

using DigitalSignage.Common;

using System.Runtime.InteropServices;
using System.Windows.Forms.Integration;
using DigitalSignage.Controls;
using System.Windows.Threading;
using System.Threading;

namespace WPFDesigner
{
    /// <summary>
    /// Interaction logic for MediaComponent.xaml
    /// </summary>
    public partial class MultimediaComponent : UserControl, IDesignElement
    {
		[DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
		public static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int x, int y, int cx, int cy, int flags);

		IntPtr HWND_TOP = IntPtr.Zero;
            
		private List<PlaylistItem> _playlist;
        private bool _IsPlay;
        PlaylistPlayer player;
        private CompType playCompType = CompType.Unknown;

        /// <summary>
        /// Represents the set of changable properties
        /// </summary>
        Dictionary<string, object> _properties;

		//	미디어 컴포넌트들
		MediaElement _element_media = null;
		WindowsFormsHost mediaHost = null;
		AxMediaControl media = null;

		public EventWaitHandle waitMediaOpenHandler = new AutoResetEvent(false);

// 		//	버퍼링 타이머
// 		DispatcherTimer tm_buffering = null;

		//	hsshin 스트리밍 컨텐츠인지?
		private bool _isStreaming = false;
		public bool IsStreaming
		{
			get { return _isStreaming; }
			set { _isStreaming = value; }
		}

		private bool _useWPF = false;
		protected bool UseWPF
		{
			get { return _useWPF; }
			set { _useWPF = value; }
		}

        public MultimediaComponent(bool bUseWPF)
        {
            InitializeComponent();

            //Video 초기화
			UseWPF = bUseWPF;

            _IsPlay = false;
            rect.Stroke = new SolidColorBrush(Color.FromArgb(255, 0, 0, 0));
            rect.StrokeThickness = 0.0;

            #region 미디어 초기화 작업 - UseWPF에 따라 MediaElement 또는 DirectShow 컨트롤 사용

            if (UseWPF)
			{
				#region MEDIAELEMENT_SOURCE 시작
				_element_media = new MediaElement();
				_element_media.OverridesDefaultStyle = false;
				_element_media.Stretch = Stretch.Fill;
				_element_media.Volume = 1;

				VisualBrush brush = new VisualBrush(_element_media);
				rect.Fill = brush;

				_element_media.LoadedBehavior = MediaState.Manual;
				_element_media.MediaEnded += new RoutedEventHandler(media_MediaEnded);
                _element_media.MediaOpened += new RoutedEventHandler(_element_media_MediaOpened);
                _element_media.MediaFailed += new EventHandler<ExceptionRoutedEventArgs>(_element_media_MediaFailed);

// 				_element_media.BufferingStarted += new RoutedEventHandler(_element_media_BufferingStarted);
// 				_element_media.BufferingEnded += new RoutedEventHandler(_element_media_BufferingEnded);
				#endregion MEDIAELEMENT_SOURCE 끝
			}
			else
			{
				mediaHost = new WindowsFormsHost();
				mediaHost.Visibility = Visibility.Hidden;
				mediaHost.Background = Brushes.Black;
              
				multimediaGrid.Children.Add(mediaHost);
				media = new AxMediaControl();
				mediaHost.Child = media;

				#region DIRECTSHOW_SOURCE 시작
				media.OnStateChanged += new DigitalSignage.Controls.AxMediaControl.StatusEventHandler(media_OnStateChanged);
				#endregion DIRECTSHOW_SOURCE 끝
            }
            #endregion
            
            //Flash 초기화
            this.flashViewer.RefDataPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + @"\PlayerData\refData\";

            try
            {
                this.flashViewer.OnContextOpen += new EventHandler(flashViewer_OnContextOpen);
            }
            catch { }

            _playlist = new List<PlaylistItem>();

            this.SizeChanged += new SizeChangedEventHandler(MediaComponent_SizeChanged);
			
		}

        void flashViewer_OnContextOpen(object sender, EventArgs e)
        {
            if (ContextMenu != null)
            {
                this.ContextMenu.IsOpen = true;
            }
        }
		/*
		void _element_media_BufferingStarted(object sender, RoutedEventArgs e)
		{
			tbBuffering.Visibility = Visibility.Visible;
			if (tm_buffering != null)
			{
				tm_buffering.Stop();
				tm_buffering = null;
			}

			tm_buffering = new DispatcherTimer();
			tm_buffering.Interval = new TimeSpan(0, 0, 1);
			tm_buffering.Tick += new EventHandler(tm_buffering_Tick);
		}
		
		void _element_media_BufferingEnded(object sender, RoutedEventArgs e)
		{
			tbBuffering.Visibility = Visibility.Collapsed;

			if (tm_buffering != null)
			{
				tm_buffering.Stop();
				tm_buffering = null;
			}
		}

		void tm_buffering_Tick(object sender, EventArgs e)
		{
			try
			{
				if (_element_media != null && _element_media.IsBuffering)
				{
					tbBuffering.Text = String.Format("Buffering.. {0}", _element_media.BufferingProgress);
				}
			}
			catch {}
		}
		*/
		public MultimediaComponent(bool bUseWPF, bool bIsStreaming)
		{
            InitializeComponent();
			_isStreaming = bIsStreaming;
			UseWPF = bUseWPF;

            _IsPlay = false;
            rect.Stroke = new SolidColorBrush(Color.FromArgb(255, 0, 0, 0));
            rect.StrokeThickness = 0.0;

            //Video 초기화
			if (UseWPF)
			{
				#region MEDIAELEMENT_SOURCE 시작
				_element_media = new MediaElement();
				_element_media.OverridesDefaultStyle = false;
				_element_media.Stretch = Stretch.Fill;
				_element_media.Volume = 1;

				VisualBrush brush = new VisualBrush(_element_media);
				rect.Fill = brush;

				_element_media.LoadedBehavior = MediaState.Manual;
                _element_media.MediaOpened += new RoutedEventHandler(_element_media_MediaOpened);
                _element_media.MediaFailed += new EventHandler<ExceptionRoutedEventArgs>(_element_media_MediaFailed);
				_element_media.MediaEnded += new RoutedEventHandler(media_MediaEnded);

				#endregion MEDIAELEMENT_SOURCE 끝
			}
			else
			{
				mediaHost = new WindowsFormsHost();
				mediaHost.Visibility = Visibility.Hidden;
				mediaHost.Background = Brushes.Black;

				multimediaGrid.Children.Add(mediaHost);
				media = new AxMediaControl();
				mediaHost.Child = media;

				#region DIRECTSHOW_SOURCE 시작
				media.OnStateChanged += new DigitalSignage.Controls.AxMediaControl.StatusEventHandler(media_OnStateChanged);
				#endregion DIRECTSHOW_SOURCE 끝
			}

            //Flash 초기화
            this.flashViewer.RefDataPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + @"\PlayerData\refData\";

            try
            {
                this.flashViewer.OnContextOpen += new EventHandler(flashViewer_OnContextOpen);
            }
            catch { }

			_playlist = new List<PlaylistItem>();
            this.SizeChanged += new SizeChangedEventHandler(MediaComponent_SizeChanged);
		}



		#region DIRECTSHOW_SOURCE 시작
		void media_OnStateChanged(DigitalSignage.Controls.MediaStatus status)
		{
			switch(status)
			{
				case DigitalSignage.Controls.MediaStatus.Played:
					if (mediaHost != null) mediaHost.Visibility = Visibility.Visible;
					break;
				default:
					if (this.mediaHost != null) mediaHost.Visibility = Visibility.Hidden;
					break;
			}
		}
		#endregion DIRECTSHOW_SOURCE 끝

		void MediaComponent_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            rect.Width = this.width;
            rect.Height = this.height;
			#region DIRECTSHOW_SOURCE 시작
			if(!UseWPF)
			{
				mediaHost.Width = Width;
				mediaHost.Height = Height;
			}
			#endregion DIRECTSHOW_SOURCE 끝

            flashHost.Width = Math.Ceiling(Width);
            flashHost.Height = Math.Ceiling(Height);

			Binding b = new Binding();
            b.Source = GetElement;
            b.Mode = BindingMode.OneWay;
            PositionConvert converter = new PositionConvert();
            b.Converter = converter;
            ctrlName.SetBinding(TextBlock.TextProperty, b);
        }

        public IDesignElement GetElement
        {
            get
            {
                return this as IDesignElement;
            }
        }

        void media_MediaEnded(object sender, RoutedEventArgs e)
        {
			#region MEDIAELEMENT_SOURCE 시작
			_element_media.Position = new TimeSpan(0, 0, 0);
			_element_media.Play();
			#endregion MEDIAELEMENT_SOURCE 끝
		}

        #region IDesignElement Members

		/// <summary>
		/// 재생이 시작된 시간
		/// </summary>
		public DateTime PlayStarted { get; set; }

        void IDesignElement.Play()
        {
			if (!_IsPlay)
			{
				_IsPlay = true;
				//	경계라인
				sepLine.Visibility = Visibility.Collapsed;
                rect.Visibility = System.Windows.Visibility.Collapsed;

				if(!UseWPF)
				{
					#region DIRECTSHOW_SOURCE 시작
					rect.Visibility = Visibility.Collapsed;

					if (mediaHost != null)
					{
						SetWindowPos(mediaHost.Handle, HWND_TOP, 0, 0, 0, 0, 3);
					}

					#endregion DIRECTSHOW_SOURCE 끝
				}
				else
				{
					#region MEDIAELEMENT_SOURCE 시작
						rect.Visibility = Visibility.Visible;
					#endregion MEDIAELEMENT_SOURCE 끝
				}

				player = new PlaylistPlayer(this);
				player.Start();
                ctrlName.Visibility = Visibility.Collapsed;
				PlayStarted = DateTime.UtcNow;
			}
        }

		void IDesignElement.Pause()
		{
			try
			{
				player.Stop();

				if (!UseWPF)
				{
					#region DIRECTSHOW_SOURCE 시작
					if (media != null) media.Pause();

					#endregion DIRECTSHOW_SOURCE 끝

				}
				else
				{
					#region MEDIAELEMENT_SOURCE 시작

					if (_element_media != null)
					{
						_element_media.Pause();
					}

					#endregion MEDIAELEMENT_SOURCE 끝
				}
			}
			catch { }
		}


        bool IDesignElement.ISPlay
        {
            get { return _IsPlay; }
        }

        void IDesignElement.Stop()
        {
            if (player != null) player.Stop();

            #region DIRECTSHOW_SOURCE 시작
            if (media != null) media.Stop();

            #endregion DIRECTSHOW_SOURCE 끝

            #region MEDIAELEMENT_SOURCE 시작

            if (_element_media != null)
            {
                _element_media.Stop();
                _element_media.Close();
                _element_media.Source = null;
            }

            #endregion MEDIAELEMENT_SOURCE 끝

            image.ImageSource = null;

            if (flashHost != null) flashHost.Visibility = Visibility.Collapsed;
            if (flashViewer != null)
            {
                flashViewer.Stop();
                flashViewer.Clear();
            }
            _IsPlay = false;
            GC.Collect();

            this.VisibilityMediaControl(CompType.Unknown);
            playCompType = CompType.Unknown;
            this.Visibility = System.Windows.Visibility.Visible;
		}

		Dictionary<string, object> IDesignElement.Properties
        {
            get
            {
                ((IDesignElement)this).InitProperties();
                return _properties;
            }
            set
            {
                _properties = value;
                foreach (string name in _properties.Keys)
                {
                    PropertySetter.SetProperty(this, "IDesignElement", name, _properties[name]);
                }
            }
        }

        void IDesignElement.InitProperties()
        {
            _properties = new Dictionary<string, object>();
            IDesignElement child = this as IDesignElement;

            _properties.Add("Name", child.Name);
            _properties.Add("Width", child.Width);
            _properties.Add("Height", child.Height);
            _properties.Add("Left", child.Left);
            _properties.Add("Top", child.Top);
            _properties.Add("HorizontalAlignment", child.HorizontalAlignment);
            _properties.Add("VerticalAlignment", child.VerticalAlignment);
			_properties.Add("ZIndex", child.ZIndex);
          
			if(UseWPF)
			{
				#region MEDIAELEMENT_SOURCE 시작
				_properties.Add("Opacity", child.Opacity);
				_properties.Add("BorderBrush", child.BorderBrush);
				_properties.Add("BorderThickness", child.BorderThickness);
				_properties.Add("BorderCorner", child.BorderCorner);
				#endregion MEDIAELEMENT_SOURCE 끝
			}

			_properties.Add("Type", child.Type);
            _properties.Add("Playlist", child.Playlist);
            _properties.Add("IsMuted", child.Mute);
            _properties.Add("Volume", child.Volume);

            _properties.Add("Opacity", child.Opacity);           
            _properties.Add("BorderBrush", child.BorderBrush);
            _properties.Add("BorderThickness", child.BorderThickness);
            _properties.Add("BorderCorner", child.BorderCorner);
            _properties.Add("Stretch", child.Stretch);
            _properties.Add("StartTime", child.StartTime);
            _properties.Add("EndTime", child.EndTime);
            _properties.Add("IsLifeTime", child.IsLifeTime);
        }

        private string name = "";
        string IDesignElement.Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;

                Binding b = new Binding();
                b.Source = GetElement;
                b.Mode = BindingMode.OneWay;
                PositionConvert converter = new PositionConvert();
                b.Converter = converter;
                ctrlName.SetBinding(TextBlock.TextProperty, b);
            }
        }

        private double width;
        double IDesignElement.Width
        {
            get
            {
                return this.width;
            }
            set
            {
                this.width = Math.Ceiling(value);
                Width = value;
            }
        }

        private double height;
        double IDesignElement.Height
        {
            get
            {
                return this.height;
            }
            set
            {
                this.height = Math.Ceiling(value);
                Height = this.height;
            }
        }
        
        double IDesignElement.Left
        {
            get
            {
                return (double)this.GetValue(InkCanvas.LeftProperty);
            }
            set
            {
                this.SetValue(InkCanvas.LeftProperty, Math.Floor(value));
            }
        }

        double IDesignElement.Top
        {
            get
            {
                return (double)this.GetValue(InkCanvas.TopProperty);
            }
            set
            {
				SetValue(InkCanvas.TopProperty, Math.Floor(value));
            }
        }

        double IDesignElement.Bottom
        {
            get
            {
                return (double)this.GetValue(InkCanvas.BottomProperty);
            }
            set
            {
				this.SetValue(InkCanvas.BottomProperty, Math.Ceiling(value));
            }
        }

        double IDesignElement.Right
        {
            get
            {
                return (double)this.GetValue(InkCanvas.RightProperty);
            }
            set
            {
				SetValue(InkCanvas.RightProperty, Math.Ceiling(value));
            }
        }

        int IDesignElement.ZIndex
        {
            get
            {
                return (int)this.GetValue(Canvas.ZIndexProperty);
            }
            set
            {
                this.SetValue(Canvas.ZIndexProperty, value);
            }
        }

        Brush IDesignElement.BorderBrush
        {
            get
            {
                return this.rect.Stroke;
            }
            set
            {
                this.rect.Stroke = value;
            }
        }

        double IDesignElement.BorderThickness
        {
            get
            {
                return rect.StrokeThickness;
            }
            set
            {
                rect.StrokeThickness = value;
            }
        }

        double IDesignElement.BorderCorner
        {
            get
            {
                return rect.RadiusX;
            }
            set
            {
				rect.RadiusX = rect.RadiusY = value;
				sepLine.RadiusX = sepLine.RadiusY = value;
			}
        }

        double IDesignElement.Opacity
        {
            get
            {
                return this.Opacity;
            }
            set
            {
                this.Opacity = value;
            }
        }

        Stretch IDesignElement.Stretch
        {
            get
			{
				if(UseWPF)
				{
					#region MEDIAELEMENT_SOURCE 시작

					return _element_media.Stretch;

					#endregion MEDIAELEMENT_SOURCE 끝
				}
				else
				{
					#region DIRECTSHOW_SOURCE 시작

					return Stretch.Fill;

					#endregion DIRECTSHOW_SOURCE 끝
				}
			}
            set
            {
				#region MEDIAELEMENT_SOURCE 시작

				if (UseWPF)
				{
					_element_media.Stretch = value;
				}

				#endregion MEDIAELEMENT_SOURCE 끝
            }
        }

        HorizontalAlignment IDesignElement.HorizontalAlignment
        {
            get
            {
                return this.HorizontalAlignment;
            }
            set
            {
                this.HorizontalAlignment = value;
            }
        }

        VerticalAlignment IDesignElement.VerticalAlignment
        {
            get
            {
                return this.VerticalAlignment;
            }
            set
            {
                this.VerticalAlignment = value;
            }
        }

        double IDesignElement.Volume
        {
            get
            {
				if (UseWPF)
				{
					return _element_media.Volume;
				}
				else
				{
					return media.Volume;
				}
            }
            set
            {
				if (UseWPF)
				{
					_element_media.Volume = value;
				}
				else
				{
					media.Volume = value;
				}
            }
        }

        bool IDesignElement.Mute
        {
            get
            {
				if (UseWPF)
				{
					return _element_media.IsMuted;
				}
				else
					return media.IsMuted;
            }
            set
            {
				if (UseWPF)
				{
					_element_media.IsMuted = value;
				}
				else
				{
					media.IsMuted = value;
				}
            }
        }

        Brush IDesignElement.Background
        {
            get;
            set;
        }

        FontFamily IDesignElement.FontFamily
        {
            get;
            set;
        }

        double IDesignElement.FontSize
        {
            get;
            set;
        }

        FontWeight IDesignElement.FontWeight
        {
            get;
            set;
        }

        Brush IDesignElement.Foreground
        {
            get;
            set;
        }

        int[] IDesignElement.StrokesLength
        {
            get;
            set;
        }

		TimeSpan IDesignElement.RefreshInterval
		{
            get;
            set;
		}

        PenLineCap IDesignElement.StrokeDashCap
        {
            get;
            set;
        }

        char IDesignElement.SeparatorChar
        {
            get;
            set;
        }

        Point IDesignElement.AspectRatio
        {
            get;
            set;
        }

        Type IDesignElement.Type
        {
            get
            {
                return GetType();
            }
        }

        List<PlaylistItem> IDesignElement.Playlist
        {
            get
            {
                return _playlist;
            }
            set
            {
                _playlist = value;
            }
		}


        void _element_media_MediaFailed(object sender, ExceptionRoutedEventArgs e)
        {
            try
            {
                _contentLoaded = true;
				waitMediaOpenHandler.Set();

                iVisionLogHelper.GetInstance.ScreenErrorLog(this.Content.ToString(), ErrorCodes.IS_ERROR_INTERNAL, e.ErrorException.Message);
            }
            catch { }
        }

        void _element_media_MediaOpened(object sender, RoutedEventArgs e)
        {
            try
            {
                _contentLoaded = true;
				waitMediaOpenHandler.Set();
            }
            catch { }
        }

		string IDesignElement.Content
		{
			get
			{
				if(UseWPF)
					return _element_media.Source.AbsolutePath;
				else
					return media.CurrentFilePath;
			}
			set
			{
                string contentFile = value as string;
                CompType fileType = this.GetFileType(contentFile);
                playCompType = fileType;
                
                switch (fileType)
                {
                    case CompType.Media:
                        {
                            if (UseWPF)
                            {
                                #region MEDIAELEMENT_SOURCE 시작
                                if (value == "")
                                {
                                    ctrlName.Visibility = Visibility.Visible;
                                    _element_media.Stop();
                                    _element_media.Source = null;
                                    return;
                                }

                                if (Uri.IsWellFormedUriString(value, UriKind.Absolute))
                                {
                                    _element_media.Stop();
                                    _element_media.Source = new Uri(value);
                                }
                                else
                                {
                                    Uri uri = new Uri(((IDesignElement)this).FileManager.GetFullFilePathByName(value));
                                    if (_element_media.Source != null && uri.Equals(_element_media.Source))
                                    {
                                        _element_media.Position = new TimeSpan(0, 0, 0);
                                    }
                                    else
                                    {
                                        _element_media.Stop();
                                        _element_media.Source = uri;
                                    }
                                }

                                _contentLoaded = false;
                                waitMediaOpenHandler.Reset();

                                _element_media.Play();


                                DateTime dtNow = DateTime.Now;

                                while (!waitMediaOpenHandler.WaitOne(10))
                                {
                                    System.Windows.Forms.Application.DoEvents();
                                    if ((DateTime.Now - dtNow).TotalSeconds > 2.0)
                                    {
                                        try
                                        {
                                            iVisionLogHelper.GetInstance.ScreenErrorLog(this.Content.ToString(), ErrorCodes.IS_ERROR_TIMEOUT, "Content Loading Timeouted.");
                                        }
                                        catch { }
                                        break;
                                    }
                                }

                                #endregion MEDIAELEMENT_SOURCE 끝
                            }
                            else
                            {
                                #region DIRECTSHOW_SOURCE 시작

                                if (value == "")
                                {
                                    ctrlName.Visibility = Visibility.Visible;
                                    media.Stop();
                                    return;
                                }

                                if (IsStreaming)
                                {
                                    media.Play(value);
                                }
                                else
                                {
                                    string filePath = ((IDesignElement)this).FileManager.GetFullFilePathByName(value);
                                    media.Play(filePath);
                                }

                                #endregion DIRECTSHOW_SOURCE 끝
                            }

                            if (flashViewer != null)
                            {
                                flashViewer.Stop();
                                flashViewer.Clear();
                            }

                            image.ImageSource = null;
                            GC.Collect();
                        }
                        break;
                    case CompType.Flash:
                        {  
                            string path = (Parent as IMediaFilesManager).GetFullFilePathByName(value);

                            if (flashViewer != null)
                            {
                                flashViewer.Stop();
                                flashViewer.Play(path);
                            }

                            image.ImageSource = null;
                            GC.Collect();

                            #region DIRECTSHOW_SOURCE 시작
                            if (media != null) media.Stop();

                            #endregion DIRECTSHOW_SOURCE 끝

                            #region MEDIAELEMENT_SOURCE 시작

                            if (_element_media != null)
                            {
                                _element_media.Stop();
                                _element_media.Close();
                                _element_media.Source = null;
                            }

                            #endregion MEDIAELEMENT_SOURCE 끝

                        }
                        break;
                    case CompType.Image:
                        {
                            BitmapImage _bi = new BitmapImage();

                            if (value == "")
                            {
                                ctrlName.Visibility = Visibility.Visible;
                                image.ImageSource = null;
                                return;
                            }

                            _bi.DecodePixelHeight = Convert.ToInt32(rect.ActualHeight);
                            _bi.DecodePixelWidth = Convert.ToInt32(rect.ActualWidth);

                            _bi.BeginInit();
                            _bi.UriSource = new Uri(((IDesignElement)this).FileManager.GetFullFilePathByName(value), UriKind.RelativeOrAbsolute);
                            _bi.EndInit();
                            image.ImageSource = _bi;

                            #region DIRECTSHOW_SOURCE 시작
                            if (media != null) media.Stop();

                            #endregion DIRECTSHOW_SOURCE 끝

                            #region MEDIAELEMENT_SOURCE 시작

                            if (_element_media != null)
                            {
                                _element_media.Stop();
                                _element_media.Close();
                                _element_media.Source = null;
                            }

                            #endregion MEDIAELEMENT_SOURCE 끝

                            if (flashViewer != null)
                            {
                                flashViewer.Stop();
                                flashViewer.Clear();
                            }
                        }
                        break;
                }
                VisibilityMediaControl(fileType);
			}
        }

        bool IDesignElement.IsLifeTime
        {
            get;
            set;
        }

        private TimeSpan startTime;
        TimeSpan IDesignElement.StartTime
        {
            get { return this.startTime; }
            set { this.startTime = value; }
        }

        private TimeSpan endTime;
        TimeSpan IDesignElement.EndTime
        {
            get { return this.endTime; }
            set { this.endTime = value; }
        }

		IMediaFilesManager IDesignElement.FileManager
        {
            get { return Parent as IMediaFilesManager; }
		}

		System.Drawing.Bitmap IDesignElement.GetThumbnail(int cx, int cy)
		{
            if (this.playCompType == CompType.Media)
            {
                if (UseWPF)
                {
                    #region MEDIAELEMENT_SOURCE 시작
                    return null;
                    #endregion
                }
                else
                {
                    #region DIRECTSHOW_SOURCE 시작
                    if (media != null)
                    {
                        return media.Thumbnail(cx, cy);
                    }

                    return null;
                    #endregion
                }
            }
            else if (this.playCompType == CompType.Image)
            {
                if (flashViewer != null)
                    return flashViewer.Thumbnail(cx, cy);

                return null;
            }
            else if (this.playCompType == CompType.Flash)
            {
                return null;
            }
            else
                return null;
		}

		#endregion

		private void UserControl_Unloaded(object sender, RoutedEventArgs e)
		{
			#region DIRECTSHOW_SOURCE 시작
			if (media != null && !media.IsDisposed)
			{
				try
				{
					media.Stop();
				}
				catch { }
				try
				{
					media.Dispose();
				}
				catch { }
			}
			media = null;
			if(mediaHost != null)
			{
				mediaHost.Child = null;
				mediaHost.Dispose();
				mediaHost = null;
			}
			#endregion DIRECTSHOW_SOURCE 끝
		
			if(_element_media != null)
			{
				_element_media.Stop();
				_element_media.Close();
				_element_media.Source = null;
				_element_media = null;
			}
			if(rect.Fill != null)
			{
				rect.Fill = null;
			}

            try
            {
                if (flashHost != null) flashHost.Child = null;
                if (flashViewer != null)
                {
                    try
                    {
                        flashViewer.Stop();
                    }
                    catch { }
                    try
                    {
                        flashViewer.Dispose();
                    }
                    catch { }
                    flashViewer = null;

                }
                if (flashHost != null)
                {
                    flashHost.Dispose();
                    flashHost = null;
                }

            }
            catch
            {
            }
           
		}

        private CompType GetFileType(string contentPath)
        {
            // 함수 호출전에 validation 체크
            try
            {
                if (string.IsNullOrEmpty(contentPath) == true)
                    return CompType.Unknown;

                FileInfo fi = new FileInfo(contentPath);

                if (Properties.Resources.labelFilterVideo.Contains(fi.Extension))
                {
                    return CompType.Media;
                }
                if (Properties.Resources.labelFilterImage.Contains(fi.Extension))
                {
                    return CompType.Image;
                }
                if (Properties.Resources.labelFilterFlash.Contains(fi.Extension))
                {
                    return CompType.Flash;
                }

                return CompType.Unknown;
            }
            catch
            {
                return CompType.Unknown;
            }
        }

        private void VisibilityMediaControl(CompType filetype)
        {
            if (filetype == CompType.Media)
            {
                _IsPlay = false;

                //	경계라인
                sepLine.Visibility = Visibility.Collapsed;                
                ctrlName.Visibility = Visibility.Collapsed;
                flashHost.Visibility = System.Windows.Visibility.Collapsed;
                rect.Visibility = System.Windows.Visibility.Collapsed;

                mediaHost.Visibility = System.Windows.Visibility.Visible;
            }
            else if (filetype == CompType.Image)
            {
                //	경계라인
                sepLine.Visibility = Visibility.Collapsed;
                ctrlName.Visibility = Visibility.Collapsed;
                mediaHost.Visibility = System.Windows.Visibility.Collapsed;
                flashHost.Visibility = System.Windows.Visibility.Collapsed;

                //_IsPlay = false;
                rect.Visibility = System.Windows.Visibility.Visible;
                GC.Collect();
            }
            else if (filetype == CompType.Flash)
            {
                //	경계라인
                sepLine.Visibility = Visibility.Collapsed;
                ctrlName.Visibility = Visibility.Collapsed;
                mediaHost.Visibility = System.Windows.Visibility.Collapsed;
                rect.Visibility = System.Windows.Visibility.Collapsed;

                if (flashHost != null) 
                    flashHost.Visibility = Visibility.Visible;
            }
            else
            {
                _IsPlay = false;
                sepLine.Visibility = Visibility.Visible;
                ctrlName.Visibility = Visibility.Visible;

                if (flashHost != null) flashHost.Visibility = Visibility.Collapsed;
                if (player != null) player.Stop();
                if (flashViewer != null)
                {
                    flashViewer.Stop();
                    flashViewer.Clear();
                }

                #region DIRECTSHOW_SOURCE 시작
                if (media != null) media.Stop();

                #endregion DIRECTSHOW_SOURCE 끝

                #region MEDIAELEMENT_SOURCE 시작

                if (_element_media != null)
                {
                    _element_media.Stop();
                    _element_media.Close();
                    _element_media.Source = null;
                }

                #endregion MEDIAELEMENT_SOURCE 끝

                if (player != null) 
                    player.Stop();
                image.ImageSource = null;

            }
        }
	}
}