﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WPFDesigner
{
	public enum CompType
	{
		Image,
		Media,
		Streaming,
		Quicktime,
		Ppt,
		Text,
		Scrolling_text,
		Flash,
		Rss,
		Web,
		Rectagle,
		Digitalclock,
		Ellipse,
		Audio,
		Analogue,
		Unknown
	};

	public class WPFDUtil
	{
		public static Type GetComponentTypeFromCompType(CompType type)
		{
			try
			{
				switch(type)
				{
					case CompType.Image:
						return typeof(ImageComponent);
					case CompType.Media:
						return typeof(MediaComponent);
					case CompType.Streaming:
						return typeof(StreamingComponent);
					case CompType.Quicktime:
						return typeof(QuickTimeComponent);
					case CompType.Ppt:
						return typeof(PptComponent);
					case CompType.Text:
						return typeof(TextComponent);
					case CompType.Scrolling_text:
						return typeof(ScrollTextComponent);
					case CompType.Flash:
						return typeof(FlashComponent);
					case CompType.Rss:
						return typeof(RssComponent);
					case CompType.Web:
						return typeof(WebComponent);
					case CompType.Rectagle:
						return typeof(RectangleComponent);
					case CompType.Digitalclock:
						return typeof(DigitalClockComponent);
					case CompType.Ellipse:
						return typeof(EllipseComponent);
					case CompType.Audio:
						return typeof(AudioComponent);
					case CompType.Analogue:
						return typeof(AnalogueClockComponent);
					case CompType.Unknown:
						return null;
				}
			}
			catch
			{
			}

			return null;
		}
		public static CompType GetComponentTypeFromExtension(string sExtension)
		{
			try
			{
				string cleared_ext = sExtension.StartsWith(".") ? sExtension.ToLower().TrimStart('.') : sExtension.ToLower();

				if (cleared_ext.Equals("avi") ||
					cleared_ext.Equals("wmv") ||
					cleared_ext.Equals("mp4") ||
					cleared_ext.Equals("mpeg") ||
					cleared_ext.Equals("mpg") ||
					cleared_ext.Equals("mkv") ||
					cleared_ext.Equals("flv") ||
					cleared_ext.Equals("asf") ||
					cleared_ext.Equals("png"))
					return CompType.Media;
				else if (cleared_ext.Equals("swf"))
					return CompType.Flash;
				else if (cleared_ext.Equals("png") ||
					cleared_ext.Equals("bmp") ||
					cleared_ext.Equals("jpeg") ||
					cleared_ext.Equals("jpg") ||
					cleared_ext.Equals("jpe") ||
					cleared_ext.Equals("jfif") ||
					cleared_ext.Equals("gif") ||
					cleared_ext.Equals("png") ||
					cleared_ext.Equals("jpg") ||
					cleared_ext.Equals("tiff") ||
					cleared_ext.Equals("tif"))
					return CompType.Image; 
				else if (cleared_ext.Equals("mov") ||
					cleared_ext.Equals("hdmov"))
					return CompType.Quicktime;
				else if (cleared_ext.Equals("mp3") ||
					cleared_ext.Equals("wma") ||
					cleared_ext.Equals("ogg"))
					return CompType.Audio; 
				else if (cleared_ext.Equals("ppt"))
					return CompType.Ppt;
			}
			catch
			{
			}
			return CompType.Unknown;
		}
	}
}
