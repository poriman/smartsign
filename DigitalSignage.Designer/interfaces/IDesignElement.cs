﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Collections.Generic;
using System.Windows.Media;
using DigitalSignage.Common;

namespace WPFDesigner
{
    /// <summary>
    /// The basic interface that must be implemented by all design elements
    /// </summary>
    public interface IDesignElement
    {
        Dictionary<string, object> Properties { get; set; }

        void Play();
        void Play(TimeSpan ts);

        void Seek(TimeSpan ts);

        void Stop();
		void Pause();

        void InitProperties();

        #region Properties section

		/// <summary>
		/// 재생이 시작된 시간
		/// </summary>
		DateTime PlayStarted { get; set; }

        /// <summary>
        /// Gets or sets the name
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// 플레이 유무를 리턴함.
        /// </summary>
        bool ISPlay { get;}

        /// <summary>
        /// Gets or sets the width
        /// </summary>
        double Width { get; set; }

        /// <summary>
        /// Gets or sets the height
        /// </summary>
        double Height { get; set; }


        /// <summary>
        /// Gets or sets the left margin
        /// </summary>
        double Left { get; set; }


        /// <summary>
        /// Gets or sets the top margin
        /// </summary>
        double Top { get; set; }


        /// <summary>
        /// Gets or sets the Right margin
        /// </summary>
        double Right { get; set; }


        /// <summary>
        /// Gets or sets the Bottom margin
        /// </summary>
        double Bottom { get; set; }

        /// <summary>
        /// Gets or sets the z index
        /// </summary>
        int ZIndex { get; set; }

        /// <summary>
        /// Gets or sets the border brush
        /// </summary>
        Brush BorderBrush { get; set; }

        /// <summary>
        /// Gets or sets the border thickness
        /// </summary>
        double BorderThickness { get; set; }

        /// <summary>
        /// Gets or sets the border corner
        /// </summary>
        double BorderCorner { get; set; }

        /// <summary>
        /// Gets or sets the horizontal alignment
        /// </summary>
        HorizontalAlignment HorizontalAlignment { get; set; }

        /// <summary>
        /// Gets or sets the vertical alignment
        /// </summary>
        VerticalAlignment VerticalAlignment { get; set; }

        /// <summary>
        /// Gets or sets the vertical alignment of content
        /// </summary>
        VerticalAlignment VerticalContentAlignment { get; set; }

        /// <summary>
        /// Gets or sets the horizontal alignment of content
        /// </summary>
        HorizontalAlignment HorizontalContentAlignment { get; set; }

        /// <summary>
        /// Gets or sets the opacity
        /// </summary>
        double Opacity { get; set; }

        /// <summary>
        /// Gets or sets the strtch method
        /// </summary>
        Stretch Stretch { get; set; }

        /// <summary>
        /// Gets or sets the volume (from 0.0 to 1.0)
        /// </summary>
        double Volume { get; set; }

        /// <summary>
        /// Gets or sets the mute
        /// </summary>
        bool Mute { get; set; }

        /// <summary>
        /// Gets or sets the background
        /// </summary>
        Brush Background { get; set; }

        /// <summary>
        /// Gets or sets the font family
        /// </summary>
        FontFamily FontFamily { get; set; }

        /// <summary>
        /// Gets or sets the font size
        /// </summary>
        double FontSize { get; set; }

        /// <summary>
        /// Gets or sets the font weight
        /// </summary>
        FontWeight FontWeight { get; set; }

        /// <summary>
        /// Gets or sets the foreground
        /// </summary>
        Brush Foreground { get; set; }

        /// <summary>
        /// Gets or sets the strokes length of border
        /// </summary>
        int[] StrokesLength { get; set; }

        /// <summary>
        /// Gets or sets the pen line cap of border strokes
        /// </summary>
        PenLineCap StrokeDashCap { get; set; }

        /// <summary>
        /// Gets or sets the separator char
        /// </summary>
        char SeparatorChar { get; set; }

        /// <summary>
        /// Gets or sets the aspect ratio (of Pane)
        /// </summary>
        Point AspectRatio { get; set; }

        /// <summary>
        /// Gets or sets the type of element
        /// </summary>
        Type Type { get; }

        /// <summary>
        /// Gets or sets the playlist of element
        /// </summary>
        List<PlaylistItem> Playlist { get; set; }

        /// <summary>
        /// Gets or sets the content that element is currently displaying
        /// </summary>
        string Content { get; set; }

        /// <summary>
        /// Gets the parent of the element
        /// </summary>
        IMediaFilesManager FileManager { get; }

		/// <summary>
		/// 갱신 시간을 반환한다.
		/// </summary>
		TimeSpan RefreshInterval { get; set; }

        /// <summary>
        /// 이미지 전환 효과
        /// </summary>
        TransformEffect TransEffect { get; set; }

		/// <summary>
		/// Gets the thumbnail from forms
		/// </summary>
		System.Drawing.Bitmap GetThumbnail(int cx, int cy);
		
		#endregion
    }
}