﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WPFDesigner
{
    public interface IProjectManagerContainer
    {        
        ProjectManager GetProjectManager();
    }
}
