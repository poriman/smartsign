﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace WPFDesigner
{
    public interface IMediaFilesManager
    {
        /// <summary>
        /// Scans the specified media folder and adds all found files to the list
        /// </summary>
        /// <param name="mediaPath">The path to the media of current project</param>
        void InitMediaFilesList(string mediaPath);

        /// <summary>
        /// Adds the specified file to the current project`s files list
        /// </summary>
        /// <param name="filePath">The source path of the file</param>
        /// <returns>The modified file name that must be used instead of old file name</returns>
        string AddNewFileWithModifiedName(string filePath);

        /// <summary>
        /// Returns the full path to the file with specified file name
        /// </summary>
        /// <param name="name">A name of file to get the full path of</param>
        /// <returns>The full path of the file located in the project`s media folder</returns>
        string GetFullFilePathByName(string name);
    }
}
