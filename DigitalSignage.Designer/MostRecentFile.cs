﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Xml;

namespace WPFDesigner
{
	/// <summary>
	/// 최근 연 파일 리스트
	/// </summary>
	public class MostRecentFiles : ObservableCollection<MostRecentFile>
	{
		private static MostRecentFiles instance = null;
		public int MAX_LENGTH = 5;
		private string FILENAME = "MostRecentList.xml";

		public static MostRecentFiles GetInstance
		{
			get {
				if(instance == null)
				{
					instance = new MostRecentFiles();
					instance.init();
				}
				return instance;
			}
		}
		
		/// <summary>
		///	생성자
		/// </summary>
		public MostRecentFiles()
		{
		}

		public void init()
		{
			try
			{
				XmlDocument doc = new XmlDocument();
				doc.Load(AppDomain.CurrentDomain.BaseDirectory + FILENAME);
				XmlNodeList list = doc.SelectNodes("//child::recentfile");

				foreach (XmlNode node in list)
				{
					Push(new MostRecentFile(node.Attributes["fullname"].Value, true), false);
				}
			}
			catch
			{ }
		}

		/// <summary>
		/// Xml 파일로 보관
		/// </summary>
		private void SaveXml()
		{
			try
			{
				XmlDocument doc = new XmlDocument();
				XmlNode root = doc.CreateNode(XmlNodeType.Element, "recents", "");
				doc.AppendChild(root);
				foreach (MostRecentFile file in this)
				{
					XmlNode recentNode = doc.CreateNode(XmlNodeType.Element, "recentfile", "");
					XmlAttribute attr1 = doc.CreateAttribute("fullname");
					attr1.Value = file.FullFileName;
					recentNode.Attributes.Append(attr1);
					root.AppendChild(recentNode);
				}
				doc.Save(AppDomain.CurrentDomain.BaseDirectory + FILENAME);
			}
			catch
			{
			}
		}

		/// <summary>
		/// 최근 파일 넣기
		/// </summary>
		/// <param name="MRF"></param>
		/// <param name="doSave">파일로 보관 여부</param>
		public void Push(MostRecentFile MRF, bool doSave)
		{
			var item = from abc in this
					   where abc.Name.Equals(MRF.Name)
					   select abc;

			if (item.Count() > 0)
			{
				this.MoveItem(this.IndexOf(item.First<MostRecentFile>()), 0);
			}
			else
			{
				this.Insert(0, MRF);

				for (int i = this.Count - 1; i > MAX_LENGTH - 1; --i)
				{
					this.RemoveAt(i);
				}
			}

			if(doSave)
			{
				SaveXml();
			}
		}

		/// <summary>
		/// 최근 파일 넣기
		/// </summary>
		/// <param name="MRF"></param>
		public void Push(MostRecentFile MRF)
		{
			this.Push(MRF, true);
		}
	}

	/// <summary>
	/// 최근 연 파일
	/// </summary>
	public class MostRecentFile
	{
		/// <summary>
		/// 생성자
		/// </summary>
		/// <param name="fullname">파일의 풀패스</param>
		/// <param name="isFixed">고정인지??</param>
		public MostRecentFile(string fullname, bool isFixed)
		{
			FullFileName = fullname;
			Name = System.IO.Path.GetFileName(fullname);
			IsFixed = isFixed;
		}
		/// <summary>
		/// 파일 프로젝트 이름 프로퍼티
		/// </summary>
		public string Name
		{
			get;
			set;
		}
		/// <summary>
		/// 경로+파일 이름 프로퍼티
		/// </summary>
		public string FullFileName
		{
			get;
			set;
		}

		/// <summary>
		/// 고정 여부 프로퍼티??
		/// </summary>
		public bool IsFixed
		{
			get { return _isFixed; }
			set { _isFixed = value; }
		}

		bool _isFixed;
	}
}
