﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace WPFDesigner
{
    /// <summary>
    /// Provides static method for setting the property value of object that implements IDesignElement interface
    /// </summary>
    public class PropertySetter
    {
        /// <summary>
        /// Sets the specified property by the specified object, interface and property value
        /// </summary>
        public static void SetProperty(IDesignElement child, string interfaceName, string propertyName, object value)
        {
            PropertyInfo[] props = child.GetType().GetInterface(interfaceName).GetProperties();
            
            foreach (PropertyInfo p in props)
            {
                if (p.Name == propertyName && p.CanWrite)
                {
                    try
                    {
                        p.SetValue(child, value, null);
                    }
                    catch
                    {
                        return;
                    }
                }
            }
        }
    }
	public class PropertyGetter
	{
		/// <summary>
		/// Sets the specified property by the specified object, interface and property value
		/// </summary>
		public static object GetProperty(IDesignElement child, string interfaceName, string propertyName)
		{
			PropertyInfo[] props = child.GetType().GetInterface(interfaceName).GetProperties();

			foreach (PropertyInfo p in props)
			{
				if (p.Name == propertyName)
				{
					try
					{
						return p.GetValue(child, null);
					}
					catch
					{
						return null;
					}
				}
			}

			return null;
		}
	}
}
