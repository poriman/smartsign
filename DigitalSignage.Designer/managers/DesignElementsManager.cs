using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections;
using System.Windows.Controls;
using System.Windows.Markup;
using System.Windows;
using System.Xml.Linq;
using System.IO;
using System.Windows.Documents;
using System.Windows.Media;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Shapes;
using DigitalSignage.Controls;
using DigitalSignage.Common;
using System.Windows.Media.Imaging;
using System.Windows.Media.Converters;
using NLog;
using WeatherReaderMVC;
using System.Windows.Data;
using WPFDesigner.Cultures;

namespace WPFDesigner
{
    public class DesignElementsManager
    {
    //    private static Logger logger = LogManager.GetCurrentClassLogger();

    //    #region declarations
    //    /// <summary>
    //    /// A dictionary of design elements
    //    /// and their attributes such as PermanentAdorner and PropertiesManager
    //    /// </summary>
    //    public Dictionary<UIElement, DesignElementAttributes> ElementsList;

    //    bool focused, ctrlMode, isdragging;

    //    int globalZIndex;

    //    Binding menuHeaderBinding;

    //    double maxtop, maxheight;

    //    public static Dictionary<string, PlayList> serializable;

    //    public static Dictionary<ElementDefinition, PlayList> serialized;

    //    public static bool IsScreenChanged;

    //    public static string projectPath;

    //    Dictionary<Type, int> elementsCount;

    //    Dictionary<Type, int> copiesCount;

    //    /// <summary>
    //    /// Indicates whether to add the next element with 3D support
    //    /// </summary>
    //    public bool AddNextElementIn2DMode;

    //    /// <summary>
    //    /// Used fod representing currently selected element
    //    /// </summary>
    //    public UIElement selectedElement, lastAdded, elementToCopy;

    //    #region Variables that provide copying/pasting of element(s)
    //    PlayList playlistCopy;
    //    ElementDefinition copyDefinition;
    //    List<UIElement> selection;
    //    Dictionary<UIElement, PlayList> copyList;
    //    #endregion

    //    /// <summary>
    //    /// Element that is used as a container of design elements
    //    /// </summary>
    //    public DesignRootD DesignSurface;

    //    /// <summary>
    //    /// The dictionary that provides the saving/loading layers information into designer
    //    /// </summary>
    //    Dictionary<string, LayerDefinition> layersdef;

    //    /// <summary>
    //    /// Variables used for proper resizing of design surface according to aspect
    //    /// ratio defined while new project creation
    //    /// </summary>
    //    Point designSurfaceAspectRatio, start, current;

    //    /// <summary>
    //    /// A dictionary that represents a collection of
    //    /// Type items with corresponding context menu
    //    /// </summary>
    //    Dictionary<Type, ContextMenu> contextMenus;

    //    /// <summary>
    //    /// An element that is used for visual performance of design elements list
    //    /// </summary>
    //    public ComboBox itemsList;

    //    #region
    //    //Represents an image
    //    ImageComponent image;

    //    //Image image;

    //    ScrollTextComponent ScrollTextComponent;

    //    RssComponent rsstext;

    //    //WeatherComponent weather;

    //    //Represents a block of editable text
    //    TextComponent richtextbox;

    //    WebComponent frame;

    //    MediaComponent mediaelement;

    //    RectangleComponent rectangle;

    //    EllipseComponent EllipseComponent;

    //    DigitalClockComponent DigitalClockComponent;

    //    AnalogueClockComponent clock;

    //    DateComponent DateComponent;
    //    PptComponent pptComponent;
    //    FlashComponent flashComponent;
    //    IPTVComponent iptvComponent;

    //    UIElement layerElement, elementCopy;
    //    #endregion

    //    /// <summary>
    //    /// Represents a playlist of currently selected element
    //    /// </summary>
    //    PlayListManager playListManager;

    //    /// <summary>
    //    /// A usercontrol that displays the layers
    //    /// </summary>
    //    LayersControl lControl;

    //    /// <summary>
    //    /// Represents an actions history
    //    /// </summary>
    //    public HistoryManager historyManager;

    //    public bool IsInPlayerMode;

    //    /// <summary>
    //    /// Represents the alignment types that can be applied to elements in selection
    //    /// </summary>
    //    public enum Alignment
    //    {
    //        Left,
    //        Right,
    //        Center
    //    }
    //    #endregion
    //    public Pane p;

    //     <summary>
    //     Creates a new instance of DesignElementsManager class
    //     </summary>
    //     <param name="container">Container that is used for adding new design elements</param>
    //    public DesignElementsManager(Point aspect)
    //    {
    //        //Init();
    //        p = new Pane(new Point(16, 9));
    //        ((DesignerWindow)Application.Current.MainWindow).MainGrid.Children.Add(p);
    //        Grid.SetRow(p, 1);
    //        PropertiesViewer viewer = new PropertiesViewer();
    //        ((DesignerWindow)Application.Current.MainWindow).propertiesScrollViever.Content = viewer;
    //        p.propertiesViewer = viewer;
    //    }

    //    public DesignElementsManager(Screen pane)
    //    {
    //        ((DesignerWindow)Application.Current.MainWindow).MainGrid.Children.Clear();
    //        PropertiesViewer viewer = ((DesignerWindow)Application.Current.MainWindow).propertiesScrollViever.Content as PropertiesViewer;
    //        if (viewer != null)
    //            viewer.Close();

    //        ((DesignerWindow)Application.Current.MainWindow).propertiesScrollViever.Content = null;
    //        if (p != null) p.Close();
    //        p = null;

    //        ((DesignerWindow)Application.Current.MainWindow).MainGrid.Children.Add(pane);
    //        Grid.SetRow(pane, 1);
    //        viewer = new PropertiesViewer();
    //        ((DesignerWindow)Application.Current.MainWindow).propertiesScrollViever.Content = viewer;
    //        pane.propertiesViewer = viewer;
    //        p = pane;
    //    }

    //    public void Close()
    //    {
    //        try
    //        {
    //            if (p != null && ((DesignerWindow)Application.Current.MainWindow).MainGrid.Children.Contains(p))
    //                ((DesignerWindow)Application.Current.MainWindow).MainGrid.Children.Remove(p);
    //            if (((DesignerWindow)Application.Current.MainWindow).propertiesScrollViever.Content != null)
    //            {
    //                PropertiesViewer viewer = ((DesignerWindow)Application.Current.MainWindow).propertiesScrollViever.Content as PropertiesViewer;
    //                viewer.Close();
    //            }
    //            ((DesignerWindow)Application.Current.MainWindow).propertiesScrollViever.Content = null;
    //            p = null;
    //        }
    //        catch
    //        { }
    //    }

    //    /// <summary>
    //    /// Maintains current DesignElementsManager instance
    //    /// </summary>
    //    void Init()
    //    {
    //        //Creating elements list
    //        ElementsList = new Dictionary<UIElement, DesignElementAttributes>();

    //        ctrlMode = false;

    //        elementsCount = new Dictionary<Type, int>();
    //        copiesCount = new Dictionary<Type, int>();

    //        #region
    //        elementsCount.Add(typeof(ImageComponent), 0);
    //        elementsCount.Add(typeof(TextComponent), 0);
    //        elementsCount.Add(typeof(WebComponent), 0);
    //        elementsCount.Add(typeof(ScrollTextComponent), 0);
    //        elementsCount.Add(typeof(DateComponent), 0);
    //        elementsCount.Add(typeof(MediaComponent), 0);
    //        elementsCount.Add(typeof(AnalogueClockComponent), 0);
    //        elementsCount.Add(typeof(DigitalClockComponent), 0);
    //        elementsCount.Add(typeof(EllipseComponent), 0);
    //        elementsCount.Add(typeof(RectangleComponent), 0);
    //        elementsCount.Add(typeof(RssComponent), 0);
    //        elementsCount.Add(typeof(WeatherComponent), 0);
    //        elementsCount.Add(typeof(PptComponent), 0);
    //        elementsCount.Add(typeof(FlashComponent), 0);
    //        elementsCount.Add(typeof(IPTVComponent), 0);

    //        copiesCount.Add(typeof(ImageComponent), 0);
    //        copiesCount.Add(typeof(TextComponent), 0);
    //        copiesCount.Add(typeof(WebComponent), 0);
    //        copiesCount.Add(typeof(ScrollTextComponent), 0);
    //        copiesCount.Add(typeof(DateComponent), 0);
    //        copiesCount.Add(typeof(MediaComponent), 0);
    //        copiesCount.Add(typeof(AnalogueClockComponent), 0);
    //        copiesCount.Add(typeof(DigitalClockComponent), 0);
    //        copiesCount.Add(typeof(EllipseComponent), 0);
    //        copiesCount.Add(typeof(RectangleComponent), 0);
    //        copiesCount.Add(typeof(RssComponent), 0);
    //        copiesCount.Add(typeof(WeatherComponent), 0);
    //        copiesCount.Add(typeof(PptComponent), 0);
    //        copiesCount.Add(typeof(FlashComponent), 0);
    //        copiesCount.Add(typeof(IPTVComponent), 0);
    //        #endregion

    //        selection = new List<UIElement>();
    //        copyList = new Dictionary<UIElement, PlayList>();
    //        focused = false;

    //        //Adding a handlers to provide a proper removing of design elements 
    //        Application.Current.MainWindow.PreviewKeyDown += new System.Windows.Input.KeyEventHandler(MainWindow_PreviewKeyDown);
    //        Application.Current.MainWindow.KeyUp += new System.Windows.Input.KeyEventHandler(MainWindow_KeyUp);
    //        itemsList = new ComboBox();
    //        itemsList.SelectionChanged += new SelectionChangedEventHandler(itemsList_SelectionChanged);
    //        //Init dialogs
    //        InitDialogs();

    //        //Load context menus
    //        LoadContextMenus();

    //        //Initiating new PlayListManager instance
    //        playListManager = new PlayListManager();
    //        isdragging = false;
    //        historyManager = new HistoryManager();

    //        globalZIndex = 1;

    //        lControl = new LayersControl(this);

    //        //((DesignerWindow)Application.Current.MainWindow).layersGrid.Children.Add(lControl);
    //        DesignElementsManager.IsScreenChanged = true;
    //    }

    //    /// <summary>
    //    /// Creates a new ink canvas instance that is currently used as a design items container
    //    /// </summary>
    //    void CreateDesignSurface()
    //    {
    //        DesignSurface = new DesignRootD();
    //        DesignSurface.EditingMode = InkCanvasEditingMode.Select;
    //        DesignSurface.Background = new SolidColorBrush(Color.FromArgb(255, 255, 255, 255));
    //        if (designSurfaceAspectRatio.X > designSurfaceAspectRatio.Y)
    //        {
    //            DesignSurface.Width = ((DesignerWindow)Application.Current.MainWindow).MainGrid.ActualWidth;
    //            DesignSurface.Height = DesignSurface.Width / designSurfaceAspectRatio.X * designSurfaceAspectRatio.Y;
    //        }
    //        else
    //        {
    //            DesignSurface.Height = ((DesignerWindow)Application.Current.MainWindow).MainGrid.ActualHeight;
    //            DesignSurface.Width = DesignSurface.Height / designSurfaceAspectRatio.Y * designSurfaceAspectRatio.X;
    //        }
    //        DesignSurface.Margin = new Thickness(0, 0, 0, 0);
    //        DesignSurface.MoveEnabled = true;
    //        Grid.SetRow(DesignSurface, 1);
    //        DesignSurface.Name = "DesignRootD";
    //        ((DesignerWindow)Application.Current.MainWindow).MainGrid.Children.Add(DesignSurface);
    //        ((DesignerWindow)Application.Current.MainWindow).MainGrid.SizeChanged += new SizeChangedEventHandler(MainGrid_SizeChanged);
    //        SignEvents(DesignSurface);
    //        MainGrid_SizeChanged(((DesignerWindow)Application.Current.MainWindow).MainGrid, null);
    //    }

    //    void DesignSurface_SizeChanged(object sender, SizeChangedEventArgs e)
    //    {
    //        ///
    //    }

    //    public void LoadDesignSurface(DesignRootD designsurface, PlayList playlist)
    //    {
    //        DesignSurface = designsurface;
    //        DesignSurface.Children.Clear();
    //        DesignSurface.EditingMode = InkCanvasEditingMode.Select;
    //        ElementsList.Add(DesignSurface, new DesignElementAttributes(DesignSurface, playlist));
    //        DesignSurface.Margin = new Thickness(0, 0, 0, 0);
    //        DesignSurface.MoveEnabled = true;
    //        Grid.SetRow(DesignSurface, 1);
    //        DesignSurface.ClipToBounds = false;
    //        ((DesignerWindow)Application.Current.MainWindow).MainGrid.Children.Add(DesignSurface);
    //        ((DesignerWindow)Application.Current.MainWindow).MainGrid.SizeChanged += new SizeChangedEventHandler(MainGrid_SizeChanged);

    //        SignEvents(DesignSurface);

    //        if (DesignSurface.Width / DesignSurface.Height > 1.2 && DesignSurface.Width / DesignSurface.Height < 1.4)
    //        {
    //            designSurfaceAspectRatio = new Point(4, 3);
    //        }
    //        else if (DesignSurface.Width / DesignSurface.Height > 1.6 && DesignSurface.Width / DesignSurface.Height < 1.8)
    //        {
    //            designSurfaceAspectRatio = new Point(16, 9);
    //        }
    //        else designSurfaceAspectRatio = new Point(9, 16);

    //        selectedElement = DesignSurface;
    //        UpdatePropertiesDisplay();
    //    }

    //    void DesignSurface_MouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
    //    {
    //        isdragging = false;
    //    }

    //    void DesignSurface_MouseRightButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
    //    {

    //    }

    //    void DesignSurface_PreviewMouseRightButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
    //    {

    //    }

    //    /// <summary>
    //    /// Adds a new element to design root
    //    /// </summary>
    //    /// <param name="NewElementType">A type of new element</param>
    //    /// <param name="coords">Coordinates where to add new element</param>
    //    public void AddNewDesignElement(Type NewElementType, Point coords)
    //    {
    //        //switch (NewElementType.Name)
    //        //{
    //        //    case "DesignRootD":
    //        //        {
    //        //            CreateDesignSurface();
    //        //            ElementsList.Add(DesignSurface, new DesignElementAttributes(DesignSurface));
    //        //            DesignSurface.ContextMenu = contextMenus[DesignSurface.GetType()];
    //        //            DesignSurface.ContextMenu.Opened += new RoutedEventHandler(ContextMenu_Opened);
    //        //            selectedElement = DesignSurface;
    //        //            break;
    //        //        }
    //        //    case "ImageComponent":
    //        //        {
    //        //            image = new ImageComponent();
    //        //            layerElement = image;
    //        //            break;
    //        //        }
    //        //    case "MediaComponent":
    //        //        {
    //        //            try
    //        //            {
    //        //                mediaelement = new MediaComponent(!AddNextElementIn2DMode);
    //        //                layerElement = mediaelement;
    //        //            }
    //        //            catch (Exception ex)
    //        //            {
    //        //                logger.Error(ex);
    //        //                MessageBox.Show("Failed while adding design element", "Designer", MessageBoxButton.OK, MessageBoxImage.Warning);
    //        //                //File.AppendAllText(AppDomain.CurrentDomain.BaseDirectory + "addinglog.txt", "Failed adding: MediaComponent\n" + ex.Message + "\n" + ex.InnerException + "\n" + ex.StackTrace + "\n" + "\n");
    //        //                return;
    //        //            }
    //        //            break;
    //        //        }
    //        //    case "TextComponent":
    //        //        {
    //        //            richtextbox = new TextComponent(!AddNextElementIn2DMode);
    //        //            layerElement = richtextbox;
    //        //            break;
    //        //        }
    //        //    case "WebComponent":
    //        //        {
    //        //            frame = new WebComponent(!AddNextElementIn2DMode);
    //        //            layerElement = frame;
    //        //            break;
    //        //        }
    //        //    case "ScrollTextComponent":
    //        //        {
    //        //            ScrollTextComponent = new ScrollTextComponent(!AddNextElementIn2DMode);
    //        //            layerElement = ScrollTextComponent;
    //        //            break;
    //        //        }
    //        //    case "RssComponent":
    //        //        {
    //        //            rsstext = new RssComponent(!AddNextElementIn2DMode);
    //        //            layerElement = rsstext;
    //        //            break;
    //        //        }
    //        //    case "WeatherComponent":
    //        //        {
    //        //            weather = new WeatherComponent(!AddNextElementIn2DMode);
    //        //            layerElement = weather;
    //        //            break;
    //        //        }
    //        //    case "DigitalClockComponent":
    //        //        {
    //        //            DigitalClockComponent = new DigitalClockComponent(!AddNextElementIn2DMode);
    //        //            layerElement = DigitalClockComponent;
    //        //            break;
    //        //        }
    //        //    case "WorldComponent":
    //        //        {
    //        //            WorldComponent = new WorldComponent();
    //        //            layerElement = WorldComponent;
    //        //            break;
    //        //        }
    //        //    case "AnalogueClockComponent":
    //        //        {
    //        //            clock = new AnalogueClockComponent(!AddNextElementIn2DMode);
    //        //            layerElement = clock;
    //        //            break;
    //        //        }
    //        //    case "DateComponent":
    //        //        {
    //        //            DateComponent = new DateComponent(!AddNextElementIn2DMode);
    //        //            layerElement = DateComponent;
    //        //            break;
    //        //        }
    //        //    case "RectangleComponent":
    //        //        {
    //        //            rectangle = new RectangleComponent(!AddNextElementIn2DMode);
    //        //            layerElement = rectangle;
    //        //            break;
    //        //        }
    //        //    case "EllipseComponent":
    //        //        {
    //        //            EllipseComponent = new EllipseComponent(!AddNextElementIn2DMode);
    //        //            layerElement = EllipseComponent;
    //        //            break;
    //        //        }
    //        //    case "PptComponent":
    //        //        {
    //        //            try
    //        //            {
    //        //                pptComponent = new PptComponent();
    //        //                layerElement = pptComponent;
    //        //            }
    //        //            catch (Exception ex)
    //        //            {
    //        //                logger.Error(ex);
    //        //                MessageBox.Show("Unable to add PowerPoint component due to incopatibility with currently running system", "Designer", MessageBoxButton.OK, MessageBoxImage.Warning);
    //        //                //File.AppendAllText(AppDomain.CurrentDomain.BaseDirectory + "addinglog.txt", "Failed adding: PptComponent\n" + ex.Message + "\n" + ex.InnerException + "\n" + ex.StackTrace + "\n" + "\n");
    //        //                return;
    //        //            }

    //        //            break;
    //        //        }
    //        //    case "FlashComponent":
    //        //        {
    //        //            try
    //        //            {
    //        //                flashComponent = new FlashComponent();
    //        //                layerElement = flashComponent;
    //        //            }
    //        //            catch (Exception ex)
    //        //            {
    //        //                logger.Error(ex);
    //        //                MessageBox.Show("Unable to add Flash component due to incopatibility with currently running system or Adobe Flash Player is not installed properly", "Designer", MessageBoxButton.OK, MessageBoxImage.Warning);
    //        //                //File.AppendAllText(AppDomain.CurrentDomain.BaseDirectory + "addinglog.txt", "Failed adding: FlashComponent\n" + ex.Message + "\n" + ex.InnerException + "\n" + ex.StackTrace + "\n" + "\n");
    //        //                return;
    //        //            }
    //        //            break;
    //        //        }
    //        //    case "IPTVComponent":
    //        //        {
    //        //            try
    //        //            {
    //        //                iptvComponent = new IPTVComponent();
    //        //                layerElement = iptvComponent;
    //        //            }
    //        //            catch (Exception ex)
    //        //            {
    //        //                logger.Error(ex);
    //        //                MessageBox.Show("Unable to add IPTV component due to incopatibility with currently running system", "Designer", MessageBoxButton.OK, MessageBoxImage.Warning);
    //        //                //File.AppendAllText(AppDomain.CurrentDomain.BaseDirectory + "addinglog.txt", "Failed adding: IPTVComponent\n" + ex.Message + "\n" + ex.InnerException + "\n" + ex.StackTrace + "\n" + "\n");
    //        //                return;
    //        //            }
    //        //            break;
    //        //        }
    //        //    default: break;
    //        //}

    //        //UpdateHistoryDisplay();

    //        //if (layerElement != null)
    //        //{
    //        //    ((FrameworkElement)layerElement).Name = layerElement.GetType().Name + elementsCount[layerElement.GetType()].ToString();
    //        //    elementsCount[layerElement.GetType()]++;
    //        //    layerElement.SetValue(Canvas.ZIndexProperty, globalZIndex);
    //        //    DesignSurface.Children.Add(layerElement);
    //        //    ElementsList.Add(layerElement, new DesignElementAttributes(layerElement));

    //        //    #region MyRegion
    //        //    if (lastAdded != null && coords.X == 100 && coords.Y == 100)
    //        //    {
    //        //        double lastleft = InkCanvas.GetLeft(lastAdded), lastwidth = ((FrameworkElement)lastAdded).Width,
    //        //            lasttop = InkCanvas.GetTop(lastAdded), lastheight = ((FrameworkElement)lastAdded).Height;

    //        //        if (maxheight < lastheight)
    //        //            maxheight = lastheight;
    //        //        if (maxheight < ((FrameworkElement)layerElement).Height)
    //        //            maxheight = ((FrameworkElement)layerElement).Height;

    //        //        if (lastleft + lastwidth <= DesignSurface.Width)
    //        //        {
    //        //            DesignRootD.SetLeft(layerElement, coords.X + lastleft + lastwidth / 2);
    //        //            DesignRootD.SetTop(layerElement, maxtop);
    //        //        }
    //        //        else
    //        //        {
    //        //            DesignRootD.SetLeft(layerElement, 0);
    //        //            DesignRootD.SetTop(layerElement, lasttop + lastheight / 2);
    //        //            maxtop = DesignRootD.GetTop(layerElement) + maxheight;
    //        //        }
    //        //    }
    //        //    else
    //        //    {
    //        //        DesignRootD.SetLeft(layerElement, coords.X - ((FrameworkElement)layerElement).Width / 2);
    //        //        DesignRootD.SetTop(layerElement, coords.Y - ((FrameworkElement)layerElement).Height / 2);
    //        //    }
    //        //    #endregion

    //        //    ((FrameworkElement)layerElement).ContextMenu = contextMenus[layerElement.GetType()];

    //        //    historyManager.ApplyHistoryTerm(new HistoryItem(HistoryItem.PreparedAction.Added, layerElement), this);

    //        //    lControl.Add(layerElement);

    //        //    SignEvents(layerElement);

    //        //    UpdateElementsList(itemsList);
    //        //    lastAdded = layerElement;
    //        //    UpdateLayout(layerElement);
    //        //}
    //        //AddNextElementIn2DMode = false;
    //        //DesignElementsManager.IsScreenChanged = true;
    //    }

    //    /// <summary>
    //    /// Adds the designer-needed event handlers to the design element
    //    /// </summary>
    //    /// <param name="el">A UIElement to which add the event handlers</param>
    //    public void SignEvents(UIElement el)
    //    {
    //        if (el.GetType() != typeof(DesignRootD))
    //        {
    //            el.PreviewMouseRightButtonDown += new System.Windows.Input.MouseButtonEventHandler(Element_PreviewMouseRightButtonDown);
    //            el.PreviewMouseDown += new System.Windows.Input.MouseButtonEventHandler(ElementSelected_PreviewMouseDown);
    //            el.PreviewMouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(element_PreviewMouseLeftButtonDown);
    //            el.PreviewMouseLeftButtonUp += new System.Windows.Input.MouseButtonEventHandler(element_PreviewMouseLeftButtonUp);
    //            el.MouseMove += new System.Windows.Input.MouseEventHandler(element_MouseMove);

    //            if (el.GetType() != typeof(EllipseComponent) &&
    //                    el.GetType() != typeof(RectangleComponent) &&
    //                    el.GetType() != typeof(DateComponent))
    //                ((Control)el).MouseDoubleClick += new System.Windows.Input.MouseButtonEventHandler(Element_MouseDoubleClick);
    //        }
    //        else
    //        {
    //            ((DesignRootD)el).SelectionChanged += new EventHandler(DesignSurface_SelectionChanged);
    //            el.PreviewKeyDown += new System.Windows.Input.KeyEventHandler(DesignSurface_PreviewKeyDown);
    //            el.GotFocus += new RoutedEventHandler(DesignSurface_GotFocus);
    //            el.LostFocus += new RoutedEventHandler(DesignSurface_LostFocus);
    //            el.MouseRightButtonDown += new System.Windows.Input.MouseButtonEventHandler(DesignSurface_MouseRightButtonDown);
    //            el.MouseMove += new System.Windows.Input.MouseEventHandler(DesignSurface_MouseMove);
    //            el.MouseLeftButtonUp += new System.Windows.Input.MouseButtonEventHandler(DesignSurface_MouseLeftButtonUp);
    //            ((DesignRootD)el).SizeChanged += new SizeChangedEventHandler(DesignSurface_SizeChanged);
    //        }
    //    }

    //    /// <summary>
    //    /// Removes the designer-needed event handlers
    //    /// </summary>
    //    /// <param name="el">The UIElement which event handlers must be cleared</param>
    //    public void UnSignEvents(UIElement el)
    //    {
    //        if (el.GetType() != typeof(DesignRootD))
    //        {
    //            el.PreviewMouseRightButtonDown -= new System.Windows.Input.MouseButtonEventHandler(Element_PreviewMouseRightButtonDown);
    //            el.PreviewMouseDown -= new System.Windows.Input.MouseButtonEventHandler(ElementSelected_PreviewMouseDown);
    //            el.PreviewMouseLeftButtonDown -= new System.Windows.Input.MouseButtonEventHandler(element_PreviewMouseLeftButtonDown);
    //            el.PreviewMouseLeftButtonUp -= new System.Windows.Input.MouseButtonEventHandler(element_PreviewMouseLeftButtonUp);
    //            el.MouseMove -= new System.Windows.Input.MouseEventHandler(element_MouseMove);

    //            if (el.GetType() != typeof(EllipseComponent) &&
    //                    el.GetType() != typeof(RectangleComponent) &&
    //                    el.GetType() != typeof(DateComponent))
    //                ((Control)el).MouseDoubleClick -= new System.Windows.Input.MouseButtonEventHandler(Element_MouseDoubleClick);
    //        }
    //        else
    //        {
    //            ((DesignRootD)el).SelectionChanged -= new EventHandler(DesignSurface_SelectionChanged);
    //            el.PreviewKeyDown -= new System.Windows.Input.KeyEventHandler(DesignSurface_PreviewKeyDown);
    //            el.GotFocus -= new RoutedEventHandler(DesignSurface_GotFocus);
    //            el.LostFocus -= new RoutedEventHandler(DesignSurface_LostFocus);
    //            el.MouseRightButtonDown -= new System.Windows.Input.MouseButtonEventHandler(DesignSurface_MouseRightButtonDown);
    //            el.MouseMove -= new System.Windows.Input.MouseEventHandler(DesignSurface_MouseMove);
    //            el.MouseLeftButtonUp -= new System.Windows.Input.MouseButtonEventHandler(DesignSurface_MouseLeftButtonUp);
    //            ((DesignRootD)el).SizeChanged -= new SizeChangedEventHandler(DesignSurface_SizeChanged);
    //        }
    //    }

    //    /// <summary>
    //    /// Ensures whether the element is fully on the design surface and moves it if it is not
    //    /// </summary>
    //    /// <param name="el">An UIElement to ckeck</param>
    //    public void UpdateLayout(UIElement el)
    //    {
    //        double top = (double)el.GetValue(InkCanvas.TopProperty), left = (double)el.GetValue(InkCanvas.LeftProperty),
    //            height = ((FrameworkElement)el).Height, width = ((FrameworkElement)el).Width;
    //        if (top + height > DesignSurface.Height)
    //        {
    //            top += (DesignSurface.Height - (top + height));
    //        }
    //        if (left + width > DesignSurface.Width)
    //        {
    //            left += (DesignSurface.Width - (left + width));
    //        }
    //        if (top < 0)
    //        {
    //            top = 0;
    //        }
    //        if (left < 0)
    //        {
    //            left = 0;
    //        }
    //        el.SetValue(InkCanvas.TopProperty, top);
    //        el.SetValue(InkCanvas.LeftProperty, left);
    //    }

    //    /// <summary>
    //    /// Loads the new element into designer
    //    /// </summary>
    //    /// <param name="element">An UIElement to load</param>
    //    /// <param name="playlist">Element`s playlist</param>
    //    /// <param name="isCopy">Indicates whether this is a copy of some other element</param>
    //    public void LoadDesignElement(UIElement element, PlayList playlist, bool isCopy)
    //    {
    //        ((FrameworkElement)element).ContextMenu = contextMenus[element.GetType()];
    //        if (element.GetType() == typeof(DesignRootD))
    //        {
    //            LoadDesignSurface((DesignRootD)element, playlist);
    //            ElementsList[DesignSurface].propertiesManager.GetPlayListManager().ApplyContent(0);
    //        }

    //        else
    //        {
    //            ElementsList.Add(element, new DesignElementAttributes(element, playlist));

    //            ElementsList[element].propertiesManager.GetPlayListManager().ApplyContent(0);
    //            if (element.GetType() != typeof(DesignRootD))
    //            {
    //                DesignSurface.Children.Add(element);

    //                UpdateElementsList(itemsList);
    //                if (!isCopy)
    //                {
    //                    elementsCount[element.GetType()]++;
    //                }

    //                SignEvents(element);
    //            }
    //        }
    //    }

    //    /// <summary>
    //    /// Copies the selected elements
    //    /// </summary>
    //    public void Copy()
    //    {
    //        copyList = new Dictionary<UIElement, PlayList>();
    //        for (int i = 0; i < selection.Count; i++)
    //        {
    //            playlistCopy = ElementsList[selection[i]].propertiesManager.GetPlayListManager();
    //            playlistCopy.SwitchToSerializableMode();
    //            MemoryStream ms = new MemoryStream();
    //            BinaryFormatter bF = new BinaryFormatter();
    //            bF.Serialize(ms, playlistCopy);
    //            ms.Position = 0;
    //            playlistCopy = (PlayList)bF.Deserialize(ms);
    //            ms.Close();
    //            playlistCopy.SwitchToNonSerializableMode(DesignerWindow.mediaLocation);
    //            ElementsList[selection[i]].propertiesManager.GetPlayListManager().SwitchToNonSerializableMode(DesignerWindow.mediaLocation);
    //            ElementsList[selection[i]].propertiesManager.GetPlayListManager().element = selection[i];
    //            copyDefinition = new ElementDefinition(selection[i]);
    //            elementCopy = copyDefinition.Load(DesignerWindow.mediaLocation);
    //            elementCopy.SetValue(InkCanvas.TopProperty, (double)elementCopy.GetValue(InkCanvas.TopProperty) + 40.0);
    //            elementCopy.SetValue(InkCanvas.LeftProperty, (double)elementCopy.GetValue(InkCanvas.LeftProperty) + 40.0);
    //            copyList.Add(elementCopy, playlistCopy);
    //        }
    //    }

    //    /// <summary>
    //    /// Pastes the selection
    //    /// </summary>
    //    public void Paste()
    //    {
    //        selection.Clear();

    //        string s;

    //        for (int i = 0; i < copyList.Count; i++)
    //        {
    //            if (((FrameworkElement)copyList.ElementAt(i).Key).Name.Contains("_copy"))
    //            {
    //                int c = ((FrameworkElement)copyList.ElementAt(i).Key).Name.Length - 1;
    //                while (((FrameworkElement)copyList.ElementAt(i).Key).Name[c] != 'y')
    //                {
    //                    c--;
    //                }
    //                s = ((FrameworkElement)copyList.ElementAt(i).Key).Name.Remove(c + 1);
    //                ((FrameworkElement)copyList.ElementAt(i).Key).Name = s + copiesCount[copyList.ElementAt(i).Key.GetType()];
    //                copiesCount[copyList.ElementAt(i).Key.GetType()]++;
    //            }
    //            else
    //                ((FrameworkElement)copyList.ElementAt(i).Key).Name = ((FrameworkElement)copyList.ElementAt(i).Key).Name
    //                    + "_copy" + copiesCount[copyList.ElementAt(i).Key.GetType()];

    //            copiesCount[copyList.ElementAt(i).Key.GetType()]++;
    //            copyList.ElementAt(i).Value.element = copyList.ElementAt(i).Key;
    //            lControl.Add(copyList.ElementAt(i).Key);
    //            LoadDesignElement(copyList.ElementAt(i).Key, copyList.ElementAt(i).Value, true);
    //            selection.Add(copyList.ElementAt(i).Key);
    //        }

    //        Select(selection.ToArray());
    //        Copy();
    //        DesignElementsManager.IsScreenChanged = true;
    //    }

    //    /// <summary>
    //    /// Aligns the selected elements by the specified Alignment value 
    //    /// </summary>
    //    /// <param name="alignment">Alignment of the selection</param>
    //    public void Align(Alignment alignment)
    //    {
    //        if (selection.Count < 2) return;

    //        switch (alignment)
    //        {
    //            case Alignment.Left:
    //                {
    //                    double minLeft = GetMinimumLeft();
    //                    foreach (UIElement el in selection)
    //                    {
    //                        el.SetValue(InkCanvas.LeftProperty, minLeft);
    //                    }
    //                    break;
    //                }
    //            case Alignment.Center:
    //                {
    //                    double midLeftAxis = GetMidLeft();

    //                    foreach (UIElement el in selection)
    //                    {
    //                        el.SetValue(InkCanvas.LeftProperty, midLeftAxis - ((double)el.GetValue(FrameworkElement.WidthProperty)) / 2);
    //                    }
    //                    break;
    //                }
    //            case Alignment.Right:
    //                {
    //                    double maxleft = GetMaximumLeft();
    //                    foreach (UIElement el in selection)
    //                    {
    //                        el.SetValue(InkCanvas.LeftProperty, maxleft - (double)el.GetValue(FrameworkElement.WidthProperty));
    //                    }
    //                    break;
    //                }
    //            default: break;
    //        }
    //        DesignElementsManager.IsScreenChanged = true;
    //    }

    //    #region Alignment maintaining
    //    double GetMinimumLeft()
    //    {
    //        double minleft = (double)selection[0].GetValue(InkCanvas.LeftProperty);

    //        for (int i = 1; i < selection.Count; i++)
    //        {
    //            if (minleft > (double)selection[i].GetValue(InkCanvas.LeftProperty))
    //            {
    //                minleft = (double)selection[i].GetValue(InkCanvas.LeftProperty);
    //            }
    //        }
    //        return minleft;
    //    }

    //    double GetMaximumLeft()
    //    {
    //        double maxleft = (double)selection[0].GetValue(InkCanvas.LeftProperty) + ((FrameworkElement)selection[0]).Width;

    //        for (int i = 1; i < selection.Count; i++)
    //        {
    //            if (maxleft < (double)selection[i].GetValue(InkCanvas.LeftProperty) + ((FrameworkElement)selection[i]).Width)
    //            {
    //                maxleft = (double)selection[i].GetValue(InkCanvas.LeftProperty) + ((FrameworkElement)selection[i]).Width;
    //            }
    //        }
    //        return maxleft;
    //    }

    //    double GetMidLeft()
    //    {
    //        return GetMinimumLeft() + (GetMaximumLeft() - GetMinimumLeft()) / 2;
    //    }
    //    #endregion

    //    void element_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
    //    {
    //        isdragging = false;
    //    }

    //    void element_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
    //    {
    //        if (isdragging)
    //        {
    //            current = (Point)(e.GetPosition(DesignSurface) - start);
    //            InkCanvas.SetLeft(selectedElement, InkCanvas.GetLeft(selectedElement) + current.X);
    //            InkCanvas.SetTop(selectedElement, InkCanvas.GetTop(selectedElement) + current.Y);
    //            start = e.GetPosition(DesignSurface);
    //            DesignElementsManager.IsScreenChanged = true;
    //        }
    //    }

    //    /// <summary>
    //    /// Selectes the specified design elements collection on the design surface
    //    /// </summary>
    //    /// <param name="elementsToSelect">UIElement collection to select</param>
    //    public void Select(UIElement[] elementsToSelect)
    //    {
    //        if (DesignSurface == null) return;
    //        if (elementsToSelect.Count() == 0)
    //        {
    //            DesignSurface.Select(new UIElement[] { });
    //            selectedElement = DesignSurface;
    //            UpdatePropertiesDisplay();
    //            return;
    //        }

    //        DesignSurface.Select(elementsToSelect);

    //        if (elementsToSelect.Count() == 1)
    //        {
    //            selectedElement = elementsToSelect[0];
    //            UpdatePropertiesDisplay();
    //        }
    //        else
    //        {
    //            selectedElement = DesignSurface;
    //            UpdatePropertiesDisplay();
    //        }
    //    }

    //    void element_PreviewMouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
    //    {
    //        isdragging = false;
    //    }

    //    void element_PreviewMouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
    //    {
    //        if (IsInPlayerMode) return;

    //        if (!ctrlMode)
    //        {
    //            selection.Clear();
    //            selection.Add((UIElement)sender);
    //            Select(new UIElement[] { selection[0] });

    //            selectedElement = (UIElement)sender;
    //        }
    //        else if (!selection.Contains((UIElement)sender))
    //        {
    //            selection.Add((UIElement)sender);
    //            Select(selection.ToArray());
    //            selectedElement = DesignSurface;
    //        }

    //        if (((DesignerWindow)Application.Current.MainWindow).propertiesScrollViever.Content != null)
    //            ((DesignerWindow)Application.Current.MainWindow).propertiesScrollViever.Content = null;
    //        ((DesignerWindow)Application.Current.MainWindow).propertiesScrollViever.Content = ElementsList[(UIElement)sender].propertiesManager.GetProperties();

    //        isdragging = true;
    //        start = e.GetPosition(DesignSurface);
    //    }

    //    void ContextMenu_Opened(object sender, RoutedEventArgs e)
    //    {
    //        selectedElement = DesignSurface;
    //        DesignSurface.Select(new UIElement[] { });
    //        UpdatePropertiesDisplay();
    //    }

    //    /// <summary>
    //    /// Removes a specified element
    //    /// </summary>
    //    /// <param name="ElementToRemove">Element that must be removed</param>
    //    public void Remove()
    //    {
    //        if (selectedElement.GetType() != typeof(DesignRootD))
    //        {
    //            lControl.Remove(selectedElement);
    //            historyManager.ApplyHistoryTerm(new HistoryItem(HistoryItem.PreparedAction.Removed, selectedElement), this);
    //            ElementsList.Remove(selectedElement);
    //            DesignSurface.Children.Remove(selectedElement);
    //            itemsList.Items.Remove(((FrameworkElement)selectedElement).Name);
    //            UnSignEvents(selectedElement);
    //            selectedElement = DesignSurface;
    //            UpdatePropertiesDisplay();
    //            UpdateHistoryDisplay();
    //        }
    //        else if (selection.Count > 0)
    //        {
    //            for (int i = 0; i < selection.Count; i++)
    //            {
    //                lControl.Remove(selection[i]);
    //                historyManager.ApplyHistoryTerm(new HistoryItem(HistoryItem.PreparedAction.Removed, selection[i]), this);
    //                ElementsList.Remove(selection[i]);
    //                DesignSurface.Children.Remove(selection[i]);
    //                UnSignEvents(selection[i]);
    //                itemsList.Items.Remove(((FrameworkElement)selection[i]).Name);
    //                UpdateHistoryDisplay();

    //            }
    //            selectedElement = DesignSurface;
    //            UpdatePropertiesDisplay();
    //        }
    //        DesignElementsManager.IsScreenChanged = true;
    //    }

    //    /// <summary>
    //    /// Selects all design elements located on the design surface
    //    /// </summary>
    //    public void SelectAll()
    //    {
    //        selection.Clear();

    //        foreach (UIElement el in ElementsList.Keys)
    //        {
    //            if (el.GetType() != typeof(DesignRootD))
    //                selection.Add(el);
    //        }

    //        DesignSurface.SelectionChanged -= new EventHandler(DesignSurface_SelectionChanged);
    //        Select(selection.ToArray());
    //        DesignSurface.SelectionChanged += new EventHandler(DesignSurface_SelectionChanged);
    //    }

    //    /// <summary>
    //    /// Cleans up the resources used by the project and closes it
    //    /// </summary>
    //    public void Clear()
    //    {
    //        //CleanUpMediaDirectory();
    //        //ClearProperties();
    //        //historyManager.Release();

    //        //foreach (UIElement el in ElementsList.Keys)
    //        //{
    //        //    BindingOperations.ClearAllBindings((FrameworkElement)el);
    //        //}

    //        //foreach (Type type in contextMenus.Keys)
    //        //{
    //        //    BindingOperations.ClearAllBindings(contextMenus[type]);
    //        //}

    //        //foreach (UIElement el in ElementsList.Keys)
    //        //{
    //        //    UnSignEvents(el);

    //        //    System.Windows.Data.BindingOperations.ClearAllBindings(el);

    //        //    ElementsList[el].propertiesManager.CleanUp();
    //        //    ElementsList[el].propertiesManager = null;

    //        //    ((IDesignElement)el).Release();
    //        //}

    //        //contextMenus.Clear();

    //        //ElementsList.Clear();

    //        //((DesignerWindow)Application.Current.MainWindow).MainGrid.Children.Remove(DesignSurface);
    //        //((DesignerWindow)Application.Current.MainWindow).layersGrid.Children.Clear();

    //        //DesignSurface.Children.Clear();

    //        //DesignSurface = null;
    //        //ElementsList = null;
    //        //GC.Collect();
    //        //Application.Current.MainWindow.PreviewKeyDown -= new System.Windows.Input.KeyEventHandler(MainWindow_PreviewKeyDown);
    //        //Application.Current.MainWindow.KeyUp -= new System.Windows.Input.KeyEventHandler(MainWindow_KeyUp);
    //        //((DesignerWindow)Application.Current.MainWindow).MainGrid.SizeChanged -= new SizeChangedEventHandler(MainGrid_SizeChanged);
    //        //if (playListManager != null)
    //        //    playListManager.Close();
    //    }

    //    /// <summary>
    //    /// Removes the unused media files
    //    /// </summary>
    //    private void CleanUpMediaDirectory()
    //    {
    //        try
    //        {
    //            #region Getting the files lists
    //            string[] files = Directory.GetFiles(DesignerWindow.mediaLocation);
    //            List<string> existingFiles_FullPaths = new List<string>(files);

    //            List<string> usedFiles = new List<string>();

    //            foreach (UIElement el in ElementsList.Keys)
    //            {
    //                usedFiles.AddRange(ElementsList[el].propertiesManager.GetPlayListManager().GetUsedMediaFiles());
    //            }
    //            #endregion

    //            //foreach (string file in usedFiles)
    //            //{
    //            //    file.Replace("%20", " ");
    //            //}

    //            for (int i = 0; i < usedFiles.Count; i++)
    //            {
    //                usedFiles[i] = usedFiles[i].Replace("%20", " ");
    //            }

    //            foreach (string file in existingFiles_FullPaths)
    //            {
    //                if (!usedFiles.Contains(System.IO.Path.GetFileName(file)))
    //                {
    //                    try
    //                    {
    //                        File.Delete(file);
    //                    }
    //                    catch (Exception ex)
    //                    {
    //                        logger.Error(ex);
    //                        //File.AppendAllText(AppDomain.CurrentDomain.BaseDirectory + "cleanuplog.txt", "\n" + ex.Message + "\n" + ex.StackTrace + "\n" + ex.InnerException);
    //                    }
    //                }
    //            }
    //        }
    //        catch (Exception ex)
    //        {
    //            logger.Error(ex);
    //            //File.AppendAllText(AppDomain.CurrentDomain.BaseDirectory + "cleanuplog.txt", "\n" + ex.Message + "\n" + ex.StackTrace + "\n" + ex.InnerException);
    //            return;
    //        }
    //    }

    //    /// <summary>
    //    /// Updates the display of history box
    //    /// </summary>
    //    public void UpdateHistoryDisplay()
    //    {
    //        //((DesignerWindow)Application.Current.MainWindow).historyListBox.Items.Clear();
    //        //foreach (HistoryItem item in historyManager.GetHistoryList())
    //        //{
    //        //    ((DesignerWindow)Application.Current.MainWindow).historyListBox.Items.Add(item);
    //        //}
    //    }

    //    /// <summary>
    //    /// Updates a properties dislpay basing on selectedElement
    //    /// </summary>
    //    public void UpdatePropertiesDisplay()
    //    {
    //        if (((DesignerWindow)Application.Current.MainWindow).propertiesScrollViever.Content != null)
    //            ((DesignerWindow)Application.Current.MainWindow).propertiesScrollViever.Content = null;
    //        ((DesignerWindow)Application.Current.MainWindow).propertiesScrollViever.Content = ElementsList[selectedElement].propertiesManager.GetProperties();
    //        //if (((DesignerWindow)Application.Current.MainWindow).PropertiesGrid.Children.Count > 0)
    //        //    ((DesignerWindow)Application.Current.MainWindow).PropertiesGrid.Children.RemoveAt(0);
    //        //((DesignerWindow)Application.Current.MainWindow).PropertiesGrid.Children.Add(ElementsList[selectedElement].propertiesManager.GetProperties());
    //    }

    //    /// <summary>
    //    /// Clears properties display
    //    /// </summary>
    //    public void ClearProperties()
    //    {
    //        ((DesignerWindow)Application.Current.MainWindow).propertiesScrollViever.Content = null;
    //    }

    //    /// <summary>
    //    /// Loads context menus for each type of design elements from XML-file with context menu items definition
    //    /// </summary>
    //    void LoadContextMenus()
    //    {
    //        #region Initiating context menu
    //        contextMenus = new Dictionary<Type, ContextMenu>();
    //        contextMenus.Add(typeof(ImageComponent), new ContextMenu());
    //        contextMenus.Add(typeof(TextComponent), new ContextMenu());
    //        contextMenus.Add(typeof(MediaComponent), new ContextMenu());
    //        contextMenus.Add(typeof(DesignRootD), new ContextMenu());
    //        contextMenus.Add(typeof(WebComponent), new ContextMenu());
    //        contextMenus.Add(typeof(ScrollTextComponent), new ContextMenu());
    //        contextMenus.Add(typeof(DigitalClockComponent), new ContextMenu());
    //        contextMenus.Add(typeof(AnalogueClockComponent), new ContextMenu());
    //        contextMenus.Add(typeof(RssComponent), new ContextMenu());
    //        contextMenus.Add(typeof(DateComponent), new ContextMenu());
    //        contextMenus.Add(typeof(RectangleComponent), new ContextMenu());
    //        contextMenus.Add(typeof(EllipseComponent), new ContextMenu());
    //        contextMenus.Add(typeof(WeatherComponent), new ContextMenu());
    //        contextMenus.Add(typeof(FlashComponent), new ContextMenu());
    //        contextMenus.Add(typeof(PptComponent), new ContextMenu());
    //        contextMenus.Add(typeof(IPTVComponent), new ContextMenu());
    //        #endregion

    //        XElement contextMenuDefinition;

    //        try
    //        {
    //            contextMenuDefinition = XElement.Parse(File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + "\\settings\\ElementContextMenus.xml"));
    //        }
    //        catch (Exception ex)
    //        {
    //            MessageBox.Show("Reason: " + ex.Message, "Failed loading context menus");
    //            return;
    //        }

    //        IEnumerable<XElement> elementContextMenus = from el in contextMenuDefinition.Descendants()
    //                                                    where el.HasElements
    //                                                    select el;
    //        foreach (XElement menuname in elementContextMenus)
    //        {
    //            IEnumerable<XElement> menuitems = from el in menuname.Descendants()
    //                                              select el;

    //            foreach (XElement submenu in menuitems)
    //            {
    //                switch (menuname.Name.ToString())
    //                {
    //                    case "ImageComponent":
    //                        {
    //                            contextMenus[typeof(ImageComponent)].Items.Add(GetNewMenuItem(submenu));
    //                            break;
    //                        }
    //                    case "TextComponent":
    //                        {
    //                            contextMenus[typeof(TextComponent)].Items.Add(GetNewMenuItem(submenu));
    //                            break;
    //                        }
    //                    case "MediaComponent":
    //                        {
    //                            contextMenus[typeof(MediaComponent)].Items.Add(GetNewMenuItem(submenu));
    //                            break;
    //                        }
    //                    case "DesignRootD":
    //                        {
    //                            contextMenus[typeof(DesignRootD)].Items.Add(GetNewMenuItem(submenu));
    //                            break;
    //                        }
    //                    case "WebComponent":
    //                        {
    //                            contextMenus[typeof(WebComponent)].Items.Add(GetNewMenuItem(submenu));
    //                            break;
    //                        }
    //                    case "ScrollTextComponent":
    //                        {
    //                            contextMenus[typeof(ScrollTextComponent)].Items.Add(GetNewMenuItem(submenu));
    //                            break;
    //                        }
    //                    case "RssComponent":
    //                        {
    //                            contextMenus[typeof(RssComponent)].Items.Add(GetNewMenuItem(submenu));
    //                            break;
    //                        }
    //                    case "DigitalClockComponent":
    //                        {
    //                            contextMenus[typeof(DigitalClockComponent)].Items.Add(GetNewMenuItem(submenu));
    //                            break;
    //                        }

    //                    case "AnalogueClockComponent":
    //                        {
    //                            contextMenus[typeof(AnalogueClockComponent)].Items.Add(GetNewMenuItem(submenu));
    //                            break;
    //                        }
    //                    case "DateComponent":
    //                        {
    //                            contextMenus[typeof(DateComponent)].Items.Add(GetNewMenuItem(submenu));
    //                            break;
    //                        }
    //                    case "EllipseComponent":
    //                        {
    //                            contextMenus[typeof(EllipseComponent)].Items.Add(GetNewMenuItem(submenu));
    //                            break;
    //                        }
    //                    case "RectangleComponent":
    //                        {
    //                            contextMenus[typeof(RectangleComponent)].Items.Add(GetNewMenuItem(submenu));
    //                            break;
    //                        }
    //                    case "WeatherComponent":
    //                        {
    //                            contextMenus[typeof(WeatherComponent)].Items.Add(GetNewMenuItem(submenu));
    //                            break;
    //                        }
    //                    case "FlashComponent":
    //                        {
    //                            contextMenus[typeof(FlashComponent)].Items.Add(GetNewMenuItem(submenu));
    //                            break;
    //                        }
    //                    case "PptComponent":
    //                        {
    //                            contextMenus[typeof(PptComponent)].Items.Add(GetNewMenuItem(submenu));
    //                            break;
    //                        }
    //                    case "IPTVComponent":
    //                        {
    //                            contextMenus[typeof(IPTVComponent)].Items.Add(GetNewMenuItem(submenu));
    //                            break;
    //                        }
    //                    default: break;
    //                }
    //            }
    //        }
    //    }

    //    /// <summary>
    //    /// Creates a new menu item based on it`s XML definition
    //    /// </summary>
    //    /// <param name="midef">XElement that represents a menu item properties definition</param>
    //    /// <returns>MenuItem instance</returns>
    //    MenuItem GetNewMenuItem(XElement midef)
    //    {
    //        MenuItem item = new MenuItem();
    //        //item.Header = midef.Attribute("Header").Value;
    //        item.Name = midef.Attribute("Name").Value;
    //        menuHeaderBinding = new Binding();
    //        menuHeaderBinding.Source = CultureResources.ResourceProvider;
    //        menuHeaderBinding.Mode = BindingMode.OneWay;

    //        switch (item.Name)
    //        {
    //            case "Remove":
    //                {
    //                    item.PreviewMouseDown += new System.Windows.Input.MouseButtonEventHandler(Remove_MenuItem_PreviewMouseDown);
    //                    menuHeaderBinding.Path = new PropertyPath("menuItem" + item.Name);
    //                    break;
    //                }
    //            case "UpOneLevel":
    //                {
    //                    item.PreviewMouseDown += new System.Windows.Input.MouseButtonEventHandler(UpOneLevel_MenuItem_PreviewMouseDown);
    //                    menuHeaderBinding.Path = new PropertyPath("menuItem" + item.Name);
    //                    break;
    //                }
    //            case "DownOneLevel":
    //                {
    //                    item.PreviewMouseDown += new System.Windows.Input.MouseButtonEventHandler(DownOneLevel_MenuItem_PreviewMouseDown);
    //                    menuHeaderBinding.Path = new PropertyPath("menuItem" + item.Name);
    //                    break;
    //                }
    //            case "Play":
    //                {
    //                    item.PreviewMouseDown += new System.Windows.Input.MouseButtonEventHandler(Play_MenuItem_PreviewMouseDown);
    //                    menuHeaderBinding.Path = new PropertyPath("menuItem" + item.Name);
    //                    break;
    //                }
    //            case "Stop":
    //                {
    //                    item.PreviewMouseDown += new System.Windows.Input.MouseButtonEventHandler(Stop_MenuItem_PreviewMouseDown);
    //                    menuHeaderBinding.Path = new PropertyPath("menuItem" + item.Name);
    //                    break;
    //                }
    //            case "PlayList":
    //                {
    //                    item.PreviewMouseDown += new System.Windows.Input.MouseButtonEventHandler(PlayList_MenuItem_PreviewMouseDown);
    //                    menuHeaderBinding.Path = new PropertyPath("menuItem" + item.Name);
    //                    break;
    //                }
    //            #region Deprecated
    //            //case "AddText":
    //            //    {
    //            //        item.PreviewMouseDown += new System.Windows.Input.MouseButtonEventHandler(AddElement_MenuItem_PreviewMouseDown);
    //            //        break;
    //            //    }
    //            //case "AddMedia":
    //            //    {
    //            //        item.PreviewMouseDown += new System.Windows.Input.MouseButtonEventHandler(AddElement_MenuItem_PreviewMouseDown);
    //            //        break;
    //            //    }
    //            //case "AddFrame":
    //            //    {
    //            //        item.PreviewMouseDown += new System.Windows.Input.MouseButtonEventHandler(AddElement_MenuItem_PreviewMouseDown);
    //            //        break;
    //            //    }
    //            //case "AddImage":
    //            //    {
    //            //        item.PreviewMouseDown += new System.Windows.Input.MouseButtonEventHandler(AddElement_MenuItem_PreviewMouseDown);
    //            //        break;
    //            //    }
    //            //case "AddScrollText":
    //            //    {
    //            //        item.PreviewMouseDown += new System.Windows.Input.MouseButtonEventHandler(AddElement_MenuItem_PreviewMouseDown);
    //            //        break;
    //            //    }
    //            //case "AddRSSText":
    //            //    {
    //            //        item.PreviewMouseDown += new System.Windows.Input.MouseButtonEventHandler(AddElement_MenuItem_PreviewMouseDown);
    //            //        break;
    //            //    }
    //            //case "AddWeather":
    //            //    {
    //            //        item.PreviewMouseDown += new System.Windows.Input.MouseButtonEventHandler(AddElement_MenuItem_PreviewMouseDown);
    //            //        break;
    //            //    }
    //            //case "AddClock":
    //            //    {
    //            //        item.PreviewMouseDown += new System.Windows.Input.MouseButtonEventHandler(AddElement_MenuItem_PreviewMouseDown);
    //            //        break;
    //            //    }
    //            //case "AddDigitalClock":
    //            //    {
    //            //        item.PreviewMouseDown += new System.Windows.Input.MouseButtonEventHandler(AddElement_MenuItem_PreviewMouseDown);
    //            //        break;
    //            //    }
    //            //case "AddWorld":
    //            //    {
    //            //        item.PreviewMouseDown += new System.Windows.Input.MouseButtonEventHandler(AddElement_MenuItem_PreviewMouseDown);
    //            //        break;
    //            //    }
    //            //case "AddDate":
    //            //    {
    //            //        item.PreviewMouseDown += new System.Windows.Input.MouseButtonEventHandler(AddElement_MenuItem_PreviewMouseDown);
    //            //        break;
    //            //    }
    //            //case "AddWeatherReaderUI":
    //            //    {
    //            //        item.PreviewMouseDown += new System.Windows.Input.MouseButtonEventHandler(AddElement_MenuItem_PreviewMouseDown);
    //            //        break;
    //            //    }
    //            //case "AddRssText":
    //            //    {
    //            //        item.PreviewMouseDown += new System.Windows.Input.MouseButtonEventHandler(AddElement_MenuItem_PreviewMouseDown);
    //            //        break;
    //            //    }
    //            //case "AddEllipse":
    //            //    {
    //            //        item.PreviewMouseDown += new System.Windows.Input.MouseButtonEventHandler(AddElement_MenuItem_PreviewMouseDown);
    //            //        break;
    //            //    }
    //            //case "AddFlash":
    //            //    {
    //            //        item.PreviewMouseDown += new System.Windows.Input.MouseButtonEventHandler(AddElement_MenuItem_PreviewMouseDown);
    //            //        break;
    //            //    }
    //            //case "AddPpt":
    //            //    {
    //            //        item.PreviewMouseDown += new System.Windows.Input.MouseButtonEventHandler(AddElement_MenuItem_PreviewMouseDown);
    //            //        break;
    //            //    }
    //            //case "AddIptv":
    //            //    {
    //            //        item.PreviewMouseDown += new System.Windows.Input.MouseButtonEventHandler(AddElement_MenuItem_PreviewMouseDown);
    //            //        break;
    //            //    }
    //            //case "AddRectangle":
    //            //    {
    //            //        item.PreviewMouseDown += new System.Windows.Input.MouseButtonEventHandler(AddElement_MenuItem_PreviewMouseDown);
    //            //        break;
    //            //    } 
    //            #endregion
    //            case "Select":
    //                {
    //                    item.Items.Add(itemsList);
    //                    menuHeaderBinding.Path = new PropertyPath("menuItem" + item.Name);
    //                    break;
    //                }
    //            case "StartScroll":
    //                {
    //                    item.PreviewMouseDown += new System.Windows.Input.MouseButtonEventHandler(StartScroll_MenuItem_PreviewMouseDown);
    //                    menuHeaderBinding.Path = new PropertyPath("menuItemStart");
    //                    break;
    //                }
    //            case "StopScroll":
    //                {
    //                    item.PreviewMouseDown += new System.Windows.Input.MouseButtonEventHandler(StopScroll_MenuItem_PreviewMouseDown);
    //                    menuHeaderBinding.Path = new PropertyPath("menuItemStop");
    //                    break;
    //                }
    //            case "StartClock":
    //                {
    //                    item.PreviewMouseDown += new System.Windows.Input.MouseButtonEventHandler(StartClock_MenuItem_PreviewMouseDown);
    //                    menuHeaderBinding.Path = new PropertyPath("menuItemStart");
    //                    break;
    //                }
    //            case "StopClock":
    //                {
    //                    item.PreviewMouseDown += new System.Windows.Input.MouseButtonEventHandler(StopClock_MenuItem_PreviewMouseDown);
    //                    menuHeaderBinding.Path = new PropertyPath("menuItemStop");
    //                    break;
    //                }
    //            case "StartRssText":
    //                {
    //                    item.PreviewMouseDown += new System.Windows.Input.MouseButtonEventHandler(StartRssText_MenuItem_PreviewMouseDown);
    //                    menuHeaderBinding.Path = new PropertyPath("menuItemStart");
    //                    break;
    //                }
    //            case "StopRssText":
    //                {
    //                    item.PreviewMouseDown += new System.Windows.Input.MouseButtonEventHandler(StopRssText_MenuItem_PreviewMouseDown);
    //                    menuHeaderBinding.Path = new PropertyPath("menuItemStop");
    //                    break;
    //                }
    //            default: break;
    //        }
    //        item.SetBinding(MenuItem.HeaderProperty, menuHeaderBinding);
    //        return item;
    //    }

    //    /// <summary>
    //    /// Inits dialog boxes
    //    /// </summary>
    //    void InitDialogs()
    //    {
    //        //if (!Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "\\projects"))
    //        //    Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "\\projects");
    //    }

    //    #region Event handlers

    //    #region Designer-spesific section
    //    private void DesignSurface_SelectionChanged(object sender, EventArgs e)
    //    {
    //        if (IsInPlayerMode)
    //        {
    //            return;
    //        }

    //        if (ctrlMode)
    //        {
    //            DesignSurface.SelectionChanged -= new EventHandler(DesignSurface_SelectionChanged);
    //            foreach (UIElement el in DesignSurface.GetSelectedElements())
    //            {
    //                if (!selection.Contains(el))
    //                    selection.Add(el);
    //            }
    //            selectedElement = DesignSurface;
    //            UpdatePropertiesDisplay();
    //            DesignSurface.Select(selection.ToArray());
    //            DesignSurface.SelectionChanged += new EventHandler(DesignSurface_SelectionChanged);
    //        }

    //        else if (!ctrlMode)
    //        {
    //            if (DesignSurface.GetSelectedElements().Count == 1)
    //            {
    //                selection.Clear();
    //                selectedElement = DesignSurface.GetSelectedElements()[0];
    //                selection.Add(selectedElement);
    //                UpdatePropertiesDisplay();
    //            }
    //            else if (DesignSurface.GetSelectedElements().Count == 0)
    //            {
    //                selectedElement = DesignSurface;
    //                UpdatePropertiesDisplay();
    //            }
    //            else if (DesignSurface.GetSelectedElements().Count > 1)
    //            {
    //                selection.Clear();
    //                selectedElement = DesignSurface;
    //                foreach (UIElement el in DesignSurface.GetSelectedElements())
    //                {
    //                    if (!selection.Contains(el))
    //                        selection.Add(el);
    //                }
    //                UpdatePropertiesDisplay();
    //            }
    //        }
    //    }

    //    void Element_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
    //    {
    //        isdragging = false;
    //        playListManager.Load(ElementsList[selectedElement].propertiesManager.GetPlayListManager().ElementPlayList, selectedElement);
    //        ElementsList[selectedElement].propertiesManager.SetPlayList(playListManager.GetPlayList());
    //    }

    //    void itemsList_SelectionChanged(object sender, SelectionChangedEventArgs e)
    //    {
    //        try
    //        {
    //            DesignSurface.Focus();
    //            DesignSurface.Select(new UIElement[] { ElementsList.Keys.Single<UIElement>(el => ((FrameworkElement)el).Name == itemsList.SelectedValue.ToString()) });
    //        }

    //        catch (Exception ex)
    //        {
    //            logger.Error(ex + "");
    //        }
    //    }

    //    void Element_PreviewMouseRightButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
    //    {
    //        if (selectedElement != (UIElement)sender)
    //            e.Handled = true;
    //    }

    //    void element_PreviewKeyDown(object sender, System.Windows.Input.KeyEventArgs e)
    //    {

    //    }

    //    void ElementSelected_PreviewMouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
    //    {
    //        //DesignSurface.Select(new UIElement[] { (UIElement)sender });
    //        //selectedElement = (UIElement)sender;

    //        //if (((DesignerWindow)Application.Current.MainWindow).PropertiesGrid.Children.Count > 0)
    //        //    ((DesignerWindow)Application.Current.MainWindow).PropertiesGrid.Children.RemoveAt(0);
    //        //((DesignerWindow)Application.Current.MainWindow).PropertiesGrid.Children.Add(ElementsList[(UIElement)sender].propertiesManager.GetProperties());
    //    }

    //    void ElementSelected_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
    //    {

    //    }

    //    void DesignSurface_MouseMove(object sender, System.Windows.Input.MouseEventArgs e)
    //    {
    //        if (isdragging && selectedElement.GetType().Name != "DesignRootD")
    //        {
    //            current = (Point)(e.GetPosition(DesignSurface) - start);
    //            InkCanvas.SetLeft(selectedElement, InkCanvas.GetLeft(selectedElement) + current.X);
    //            InkCanvas.SetTop(selectedElement, InkCanvas.GetTop(selectedElement) + current.Y);
    //            start = e.GetPosition(DesignSurface);
    //        }
    //    }

    //    void DesignSurface_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
    //    {
    //        if (isdragging && selectedElement.GetType().Name != "DesignRootD")
    //        {
    //            isdragging = false;
    //        }
    //    }

    //    void MainWindow_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
    //    {
    //        if (e.Key == System.Windows.Input.Key.LeftCtrl)
    //        {
    //            ctrlMode = false;
    //        }
    //    }

    //    void MainWindow_PreviewKeyDown(object sender, System.Windows.Input.KeyEventArgs e)
    //    {
    //        if (!IsInPlayerMode)
    //        {
    //            if (e.Key == System.Windows.Input.Key.LeftCtrl || e.Key == System.Windows.Input.Key.RightCtrl)
    //            {
    //                ctrlMode = true;
    //                e.Handled = true;
    //            }
    //            else if (e.Key == System.Windows.Input.Key.Z && ctrlMode)
    //            {
    //                historyManager.Undo(this);//(ElementsList, DesignSurface);
    //                e.Handled = true;
    //            }
    //            else if (e.Key == System.Windows.Input.Key.Y && ctrlMode)
    //            {
    //                historyManager.Redo(this);//(ElementsList, DesignSurface);
    //                e.Handled = true;
    //            }
    //            else if (e.Key == System.Windows.Input.Key.C && ctrlMode)
    //            {
    //                Copy();
    //                e.Handled = true;
    //            }
    //            else if (e.Key == System.Windows.Input.Key.V && ctrlMode)
    //            {
    //                Paste();
    //                e.Handled = true;
    //            }
    //            UpdateHistoryDisplay();
    //        }
    //    }

    //    void MainGrid_SizeChanged(object sender, SizeChangedEventArgs e)
    //    {
    //        Grid grid = sender as Grid;
    //        double maxWidth = ((DesignerWindow)Application.Current.MainWindow).MainGrid.ActualWidth;
    //        double maxHeight = ((DesignerWindow)Application.Current.MainWindow).MainGrid.ActualHeight;

    //        if (designSurfaceAspectRatio.X > designSurfaceAspectRatio.Y)
    //        {
    //            DesignSurface.Width = ((DesignerWindow)Application.Current.MainWindow).MainGrid.ActualWidth;
    //            DesignSurface.Height = DesignSurface.Width / designSurfaceAspectRatio.X * designSurfaceAspectRatio.Y;

    //            if (DesignSurface.Width > maxWidth)
    //            {
    //                DesignSurface.Width = maxWidth;
    //                DesignSurface.Height = DesignSurface.Width / designSurfaceAspectRatio.X * designSurfaceAspectRatio.Y;
    //            }
    //            if (DesignSurface.Height > maxHeight)
    //            {
    //                DesignSurface.Height = maxHeight;
    //                DesignSurface.Width = DesignSurface.Height / designSurfaceAspectRatio.Y * designSurfaceAspectRatio.X;
    //            }
    //        }
    //        else
    //        {
    //            DesignSurface.Height = ((DesignerWindow)Application.Current.MainWindow).MainGrid.ActualHeight;
    //            DesignSurface.Width = DesignSurface.Height / designSurfaceAspectRatio.Y * designSurfaceAspectRatio.X;

    //            if (DesignSurface.Width > maxWidth)
    //            {
    //                DesignSurface.Width = maxWidth;
    //                DesignSurface.Height = DesignSurface.Width / designSurfaceAspectRatio.X * designSurfaceAspectRatio.Y;
    //            }
    //            if (DesignSurface.Height > maxHeight)
    //            {
    //                DesignSurface.Height = maxHeight;
    //                DesignSurface.Width = DesignSurface.Height / designSurfaceAspectRatio.Y * designSurfaceAspectRatio.X;
    //            }
    //        }
    //    }

    //    void DesignSurface_LostFocus(object sender, RoutedEventArgs e)
    //    {
    //        focused = false;
    //    }

    //    void DesignSurface_GotFocus(object sender, RoutedEventArgs e)
    //    {
    //        focused = true;
    //    }

    //    void DesignSurface_PreviewKeyDown(object sender, System.Windows.Input.KeyEventArgs e)
    //    {
    //        if (!IsInPlayerMode)
    //        {
    //            if (focused && e.Key == System.Windows.Input.Key.Delete)
    //            {
    //                Remove();
    //                e.Handled = true;
    //            }
    //            else if (e.Key == System.Windows.Input.Key.C)
    //            {
    //                e.Handled = true;
    //            }
    //            else if (e.Key == System.Windows.Input.Key.V)
    //            {
    //                e.Handled = true;
    //            }
    //            else
    //                e.Handled = false;
    //        }
    //        else e.Handled = true;
    //    }
    //    #endregion

    //    #region Context menus section
    //    void Stop_MenuItem_PreviewMouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
    //    {
    //        if (selectedElement.GetType() == typeof(MediaComponent))
    //        {
    //            //if (((MediaComponent)selectedElement).LoadedBehavior != MediaState.Manual)
    //            //    ((MediaComponent)selectedElement).LoadedBehavior = MediaState.Manual;

    //        }
    //    }

    //    void Play_MenuItem_PreviewMouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
    //    {
    //        if (selectedElement.GetType() == typeof(MediaComponent))
    //        {
    //            //if (((MediaComponent)selectedElement).LoadedBehavior != MediaState.Manual)
    //            //    ((MediaComponent)selectedElement).LoadedBehavior = MediaState.Manual;

    //        }
    //    }

    //    void Remove_MenuItem_PreviewMouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
    //    {
    //        Remove();
    //    }

    //    void UpOneLevel_MenuItem_PreviewMouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
    //    {
    //        if (Canvas.GetZIndex(selectedElement) > globalZIndex)
    //            globalZIndex++;
    //        Canvas.SetZIndex(selectedElement, globalZIndex);
    //    }

    //    void DownOneLevel_MenuItem_PreviewMouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
    //    {
    //        if (Canvas.GetZIndex(selectedElement) == globalZIndex)
    //            globalZIndex--;
    //        Canvas.SetZIndex(selectedElement, Canvas.GetZIndex(selectedElement) - 1);
    //    }

    //    /// <summary>
    //    /// Event handlers of menu items that are for adding new design elements
    //    /// </summary>
    //    /// <param name="sender"></param>
    //    /// <param name="e"></param>
    //    void AddElement_MenuItem_PreviewMouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
    //    {
    //        switch (((MenuItem)sender).Name)
    //        {
    //            case "AddText":
    //                {
    //                    AddNewDesignElement(typeof(TextComponent), e.GetPosition(DesignSurface));
    //                    break;
    //                }
    //            case "AddImage":
    //                {
    //                    AddNewDesignElement(typeof(ImageComponent), e.GetPosition(DesignSurface));
    //                    break;
    //                }
    //            case "AddMedia":
    //                {
    //                    AddNewDesignElement(typeof(MediaComponent), e.GetPosition(DesignSurface));
    //                    break;
    //                }
    //            case "AddScrollText":
    //                {
    //                    AddNewDesignElement(typeof(ScrollTextComponent), e.GetPosition(DesignSurface));
    //                    break;
    //                }
    //            case "AddRssText":
    //                {
    //                    AddNewDesignElement(typeof(RssComponent), e.GetPosition(DesignSurface));
    //                    break;
    //                }
    //            case "AddWeather":
    //                {
    //                    AddNewDesignElement(typeof(WeatherComponent), e.GetPosition(DesignSurface));
    //                    break;
    //                }
    //            case "AddDate":
    //                {
    //                    AddNewDesignElement(typeof(DateComponent), e.GetPosition(DesignSurface));
    //                    break;
    //                }
    //            case "AddClock":
    //                {
    //                    AddNewDesignElement(typeof(AnalogueClockComponent), e.GetPosition(DesignSurface));
    //                    break;
    //                }
    //            case "AddDigitalClock":
    //                {
    //                    AddNewDesignElement(typeof(DigitalClockComponent), e.GetPosition(DesignSurface));
    //                    break;
    //                }
    //            case "AddFrame":
    //                {
    //                    AddNewDesignElement(typeof(WebComponent), e.GetPosition(DesignSurface));
    //                    break;
    //                }
    //            case "AddRssLine":
    //                {
    //                    AddNewDesignElement(typeof(RssLine), e.GetPosition(DesignSurface));
    //                    break;
    //                }
    //            case "AddRectangle":
    //                {
    //                    AddNewDesignElement(typeof(RectangleComponent), e.GetPosition(DesignSurface));
    //                    break;
    //                }
    //            case "AddEllipse":
    //                {
    //                    AddNewDesignElement(typeof(EllipseComponent), e.GetPosition(DesignSurface));
    //                    break;
    //                }
    //            case "AddWeatherReaderUI":
    //                {
    //                    AddNewDesignElement(typeof(WeatherComponent), e.GetPosition(DesignSurface));
    //                    break;
    //                }
    //            case "AddFlash":
    //                {
    //                    AddNewDesignElement(typeof(FlashComponent), e.GetPosition(DesignSurface));
    //                    break;
    //                }
    //            case "AddPpt":
    //                {
    //                    AddNewDesignElement(typeof(PptComponent), e.GetPosition(DesignSurface));
    //                    break;
    //                }
    //            case "AddIptv":
    //                {
    //                    AddNewDesignElement(typeof(IPTVComponent), e.GetPosition(DesignSurface));
    //                    break;
    //                }
    //            default: break;
    //        }
    //    }

    //    void PlayList_MenuItem_PreviewMouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
    //    {
    //        playListManager.Load(ElementsList[selectedElement].propertiesManager.GetPlayListManager().ElementPlayList, selectedElement);
    //        ElementsList[selectedElement].propertiesManager.SetPlayList(playListManager.GetPlayList());
    //    }

    //    void StopRssText_MenuItem_PreviewMouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
    //    {
    //        //((RssComponent)selectedElement).Stop();
    //    }

    //    void StartRssText_MenuItem_PreviewMouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
    //    {
    //        //((RssComponent)selectedElement).Start();
    //    }

    //    void Navigate_MenuItem_PreviewMouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
    //    {
    //        //((WebComponent)selectedElement).Navigate(((WebComponent)selectedElement).Source);
    //    }

    //    void StopScroll_MenuItem_PreviewMouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
    //    {

    //    }

    //    void StartScroll_MenuItem_PreviewMouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
    //    {

    //    }

    //    void StartRSS_MenuItem_PreviewMouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
    //    {
    //        //((RssComponent)selectedElement).Start();
    //    }

    //    void StopRSS_MenuItem_PreviewMouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
    //    {
    //        //((RssComponent)selectedElement).Stop();
    //    }

    //    void StopClock_MenuItem_PreviewMouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
    //    {

    //    }

    //    void StartClock_MenuItem_PreviewMouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
    //    {
    //        //if (selectedElement.GetType().Name == "AnalogueClockComponent")
    //        //    ((AnalogueClockComponent)selectedElement).Start();
    //        //else
    //        //    ((DigitalClockComponent)selectedElement).Start();
    //    }


    //    #endregion
    //    #endregion

    //    /// <summary>
    //    /// Updates a list of items in itemsList
    //    /// </summary>
    //    /// <param name="itemsList">A combobox with design elements list</param>
    //    void UpdateElementsList(ComboBox itemsList)
    //    {
    //        itemsList.Items.Clear();
    //        foreach (UIElement item in ElementsList.Keys)
    //        {
    //            itemsList.Items.Add(((FrameworkElement)item).Name);
    //        }
    //    }

    //    public static Dictionary<UIElement, PlayList> GetScreen(DesignElementsManager m)
    //    {
    //        return new Dictionary<UIElement, PlayList>();
    //    }

    //    public static void ResetSurface(DesignElementsManager manager)
    //    {
    //        //manager.DesignSurface.EditingMode = InkCanvasEditingMode.Select;
    //        //manager.DesignSurface.MoveEnabled = true;
    //        //manager.DesignSurface.ContextMenu = manager.contextMenus[manager.DesignSurface.GetType()];

    //        //foreach (UIElement el in manager.DesignSurface.Children)
    //        //{
    //        //    ((FrameworkElement)el).ContextMenu = manager.contextMenus[el.GetType()];
    //        //}
    //    }

    //    public void SaveScreen(string directory, string fname)
    //    {
    //        serialized = new Dictionary<ElementDefinition, PlayList>();

    //        foreach (UIElement key in ElementsList.Keys)
    //        {
    //            ElementsList[key].propertiesManager.GetPlayListManager().UpdateMediaPaths(DesignerWindow.mediaLocation);
    //            ElementsList[key].propertiesManager.GetPlayListManager().SwitchToSerializableMode();
    //            serialized.Add(new ElementDefinition(key), ElementsList[key].propertiesManager.GetPlayListManager());
    //        }

    //        BinaryFormatter bf = new BinaryFormatter();
    //        FileStream fs = new FileStream(directory + "\\" + fname + ".dsd", FileMode.Create, FileAccess.ReadWrite);
    //        bf.Serialize(fs, serialized);
    //        fs.Close();

    //        foreach (UIElement key in ElementsList.Keys)
    //        {
    //            ElementsList[key].propertiesManager.GetPlayListManager().SwitchToNonSerializableMode(DesignerWindow.mediaLocation);
    //        }

    //        foreach (UIElement key in ElementsList.Keys)
    //        {
    //            if (ElementsList[key].propertiesManager.GetPlayListManager().elementname == ((FrameworkElement)key).Name)
    //            {
    //                ElementsList[key].propertiesManager.GetPlayListManager().element = (UIElement)key;
    //            }
    //        }

    //        object[] projectinfo = new object[2];

    //        bf = new BinaryFormatter();
    //        fs = new FileStream(directory + "\\" + fname + ".settings", FileMode.Create, FileAccess.Write);
    //        layersdef = new Dictionary<string, LayerDefinition>();

    //        foreach (Layer l in lControl.layersList)
    //        {
    //            layersdef.Add(l.name, new LayerDefinition(l));
    //        }

    //        projectinfo[0] = layersdef;

    //        double thumbWidth, thumbHeight;

    //        if (designSurfaceAspectRatio.X > designSurfaceAspectRatio.Y)
    //        {
    //            thumbWidth = 200;
    //            thumbHeight = 200 / designSurfaceAspectRatio.X * designSurfaceAspectRatio.Y;
    //        }
    //        else
    //        {
    //            thumbHeight = 200;
    //            thumbWidth = 200 / designSurfaceAspectRatio.Y * designSurfaceAspectRatio.X;
    //        }
    //        DesignSurface.UpdateLayout();
            //RenderTargetBitmap renderTarget = new RenderTargetBitmap((int)DesignSurface.Width, (int)DesignSurface.Height, 96d, 96d, PixelFormats.Pbgra32);
            //renderTarget.Render(DesignSurface);
            //JpegBitmapEncoder encoder = new JpegBitmapEncoder();
            //encoder.Frames.Add(BitmapFrame.Create(renderTarget));

            //MemoryStream ms = new MemoryStream();
            //encoder.Save(ms);
            //ms.Position = 0;

    //        projectinfo[1] = ms.ToArray();
    //        ms.Close();

    //        bf.Serialize(fs, projectinfo);
    //        fs.Close();
    //        DesignElementsManager.IsScreenChanged = false;
    //    }

    //    public void SaveScreenAsTemplate(string fname)
    //    {
// 		String sExecutingPath = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
		//        string directory = sExecutingPath + "\\DesignerData\\Templates\\" + fname + "\\";
    //        if (!Directory.Exists(directory))
    //            Directory.CreateDirectory(directory);

    //        serialized = new Dictionary<ElementDefinition, PlayList>();

    //        foreach (UIElement key in ElementsList.Keys)
    //        {
    //            ElementsList[key].propertiesManager.GetPlayListManager().UpdateMediaPaths(DesignerWindow.mediaLocation);
    //            ElementsList[key].propertiesManager.GetPlayListManager().SwitchToSerializableMode();
    //            serialized.Add(new ElementDefinition(key), ElementsList[key].propertiesManager.GetPlayListManager());
    //        }

    //        BinaryFormatter bf = new BinaryFormatter();
    //        FileStream fs = new FileStream(directory + fname + ".dst", FileMode.Create, FileAccess.ReadWrite);
    //        bf.Serialize(fs, serialized);
    //        fs.Close();

    //        foreach (UIElement key in ElementsList.Keys)
    //        {
    //            ElementsList[key].propertiesManager.GetPlayListManager().SwitchToNonSerializableMode(DesignerWindow.mediaLocation);
    //        }

    //        foreach (UIElement key in ElementsList.Keys)
    //        {
    //            if (ElementsList[key].propertiesManager.GetPlayListManager().elementname == ((FrameworkElement)key).Name)
    //            {
    //                ElementsList[key].propertiesManager.GetPlayListManager().element = (UIElement)key;
    //            }
    //        }

    //        object[] projectinfo = new object[2];

    //        bf = new BinaryFormatter();
    //        fs = new FileStream(directory + fname + ".settings", FileMode.Create, FileAccess.Write);
    //        layersdef = new Dictionary<string, LayerDefinition>();

    //        foreach (Layer l in lControl.layersList)
    //        {
    //            layersdef.Add(l.name, new LayerDefinition(l));
    //        }

    //        projectinfo[0] = layersdef;

    //        double thumbWidth, thumbHeight;

    //        if (designSurfaceAspectRatio.X > designSurfaceAspectRatio.Y)
    //        {
    //            thumbWidth = 200;
    //            thumbHeight = 200 / designSurfaceAspectRatio.X * designSurfaceAspectRatio.Y;
    //        }
    //        else
    //        {
    //            thumbHeight = 200;
    //            thumbWidth = 200 / designSurfaceAspectRatio.Y * designSurfaceAspectRatio.X;
    //        }
    //        DesignSurface.UpdateLayout();
    //        RenderTargetBitmap renderTarget = new RenderTargetBitmap((int)DesignSurface.Width, (int)DesignSurface.Height, 96d, 96d, PixelFormats.Pbgra32);
    //        renderTarget.Render(DesignSurface);
    //        JpegBitmapEncoder encoder = new JpegBitmapEncoder();
    //        encoder.Frames.Add(BitmapFrame.Create(renderTarget));

    //        MemoryStream ms = new MemoryStream();
    //        encoder.Save(ms);
    //        ms.Position = 0;

    //        projectinfo[1] = ms.ToArray();
    //        ms.Close();

    //        bf.Serialize(fs, projectinfo);
    //        fs.Close();
    //    }

    //    public void LoadTemplate(string fileName)
    //    {
    //        FileStream fs;
    //        BinaryFormatter bf;

    //        serialized = new Dictionary<ElementDefinition, PlayList>();

    //        fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
    //        bf = new BinaryFormatter();
    //        serialized = (Dictionary<ElementDefinition, PlayList>)bf.Deserialize(fs);
    //        fs.Flush();
    //        fs.Close();
    //        Dictionary<UIElement, PlayList> elementsList = new Dictionary<UIElement, PlayList>();

    //        foreach (ElementDefinition key in serialized.Keys)
    //        {
    //            serialized[key].element = key.Load(DesignerWindow.mediaLocation);
    //            serialized[key].SwitchToNonSerializableMode(DesignerWindow.mediaLocation);

    //            try
    //            {
    //                LoadDesignElement((UIElement)serialized[key].element, (PlayList)serialized[key], false);
    //            }
    //            catch (Exception ex)
    //            {
    //                MessageBox.Show(ex.Message);
    //            }
    //        }

    //        foreach (UIElement el in ElementsList.Keys)
    //        {
    //            ElementsList[el].propertiesManager.GetPlayListManager().UpdateMediaPaths(DesignerWindow.mediaLocation);
    //            ElementsList[el].propertiesManager.GetPlayListManager().PreInit();
    //        }

    //        LoadLayers(fileName);

    //        DesignElementsManager.IsScreenChanged = false;
    //    }

    //    public void LoadScreen(string fileName)
    //    {
    //        FileStream fs;
    //        BinaryFormatter bf;

    //        serialized = new Dictionary<ElementDefinition, PlayList>();

    //        fs = new FileStream(fileName, FileMode.Open, FileAccess.Read);
    //        bf = new BinaryFormatter();
    //        serialized = (Dictionary<ElementDefinition, PlayList>)bf.Deserialize(fs);
    //        fs.Flush();
    //        fs.Close();
    //        Dictionary<UIElement, PlayList> elementsList = new Dictionary<UIElement, PlayList>();

    //        foreach (ElementDefinition key in serialized.Keys)
    //        {
    //            serialized[key].element = key.Load(DesignerWindow.mediaLocation);
    //            serialized[key].SwitchToNonSerializableMode(DesignerWindow.mediaLocation);

    //            try
    //            {
    //                LoadDesignElement((UIElement)serialized[key].element, (PlayList)serialized[key], false);
    //            }
    //            catch (Exception ex)
    //            {
    //                MessageBox.Show(ex.Message);
    //            }
    //        }

    //        foreach (UIElement el in ElementsList.Keys)
    //        {
    //            ElementsList[el].propertiesManager.GetPlayListManager().UpdateMediaPaths(DesignerWindow.mediaLocation);
    //            ElementsList[el].propertiesManager.GetPlayListManager().PreInit();
    //        }

    //        LoadLayers(fileName);
    //    }

    //    void LoadLayers(string fileName)
    //    {
    //        string layerdefpath = System.IO.Path.GetDirectoryName(fileName) + "\\" + System.IO.Path.GetFileNameWithoutExtension(fileName) + ".settings";
    //        FileStream fs = new FileStream(layerdefpath, FileMode.Open, FileAccess.Read);
    //        BinaryFormatter bf = new BinaryFormatter();
    //        object[] projectinfo = new object[2];
    //        projectinfo = (object[])bf.Deserialize(fs);
    //        //layersdef = new Dictionary<string, LayerDefinition>();
    //        //layersdef = (Dictionary<string, LayerDefinition>)bf.Deserialize(fs);
    //        fs.Close();
    //        layersdef = new Dictionary<string, LayerDefinition>();
    //        layersdef = (Dictionary<string, LayerDefinition>)projectinfo[0];

    //        lControl.layersList.Clear();
    //        lControl.layersListBox.Items.Clear();

    //        foreach (string s in layersdef.Keys)
    //        {
    //            //Layer l = layersdef[s].LoadLayerProperties();

    //            //foreach (UIElement el in ElementsList.Keys)
    //            //{
    //            //    if (el.GetType().Name != "DesignRootD" && ((ILayerElement)el).GetLayerID() == l.name)
    //            //    {
    //            //        if (l.visibility)
    //            //            el.Visibility = Visibility.Visible;
    //            //        else el.Visibility = Visibility.Hidden;
    //            //        l.layerElements.Add(el);
    //            //    }
    //            //}

    //            //lControl.AddLayer(l);
    //        }
    //    }
    //}

    // <summary>
    // Class that represents design element properties and adorner
    // </summary>
    //[Serializable]
    //public class DesignElementAttributes
    //{
    //    /// <summary>
    //    /// Properties manager of design element
    //    /// </summary>
    //    public PropertiesManager propertiesManager;

    //    /// <summary>
    //    /// Creates a new instance of DesignElementAttributes class
    //    /// </summary>
    //    /// <param name="element">Design element</param>
    //    public DesignElementAttributes(UIElement element)
    //    {
    //        propertiesManager = new PropertiesManager(element);
    //    }

    //    public DesignElementAttributes(UIElement element, PlayList playlist)
    //    {
    //        propertiesManager = new PropertiesManager(element, playlist);
    //    }
    }
}