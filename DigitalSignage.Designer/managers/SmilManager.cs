﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;

namespace WPFDesigner
{
	/// <summary>
	/// Smil 관련 매니저 클래스
	/// </summary>
	class SmilManager
	{
		static public Screen ImportFromSmil(string filepath, string mediaPath, bool isdesignMode, ElementsListViewer elementListViewerObject)
		{
			Smil.Library.Smil smil = Smil.Library.Smil.LoadObjectFromSmilFile(filepath);

			bool _isAd = false;
			int _showFlag = (int)DigitalSignage.Common.ShowFlag.Show_Event;
			DateTime _starttime = DateTime.MinValue;
			DateTime _endtime = DateTime.MaxValue;
			string _metaTags = String.Empty;
			string _screen_ID = DateTime.Now.ToString("yyyyMMddHHmmssffff");
            int _viewLeftPos = 0;
            int _viewTopPos = 0;

			String _profile = Properties.Resources.comboitemDefault;


			bool bAuthor = false;
			foreach (Smil.Library.Meta meta in smil.head.arrMetaCollection)
			{
				if (meta.name.Equals("author"))
				{
					if (!meta.content.Equals(Properties.Resources.titleMetaKey))
						throw new NotSupportedException(Properties.Resources.mbNotSupportedSMILFile);
					bAuthor = true;
					continue;
				}
				else if (meta.name.Equals("isAd"))
				{
					try
					{
						_isAd = Convert.ToBoolean(meta.content);
					} 
					catch { }

					continue;
				}
				else if (meta.name.Equals("showFlag"))
				{
					try
					{
						_showFlag = Convert.ToInt32(meta.content);
					}
					catch { }

					continue;
				}
				else if (meta.name.Equals("AdStartTime"))
				{
					try
					{
						_starttime = Convert.ToDateTime(meta.content);
					}
					catch { }

					continue;
				}
				else if (meta.name.Equals("AdEndTime"))
				{
					try
					{
						_endtime = Convert.ToDateTime(meta.content);
					}
					catch { }

					continue;
				}
				else if (meta.name.Equals("MetaTags"))
				{
					try
					{
						_metaTags = meta.content;
					}
					catch { }

					continue;
				}

				else if (meta.name.Equals("ScreenID"))
				{
					try
					{
						_screen_ID = meta.content;
					}
					catch { }

					continue;
				}

				else if (meta.name.Equals("Profile"))
				{
					try
					{
						_profile = meta.content;
					}
					catch { }

					continue;
                }
                else if (meta.name.Equals("ViewLeftPos"))
                {
                    try
                    {
                        _viewLeftPos = Convert.ToInt32(meta.content);
                    }
                    catch { }

                    continue;
                }
                else if (meta.name.Equals("ViewTopPos"))
                {
                    try
                    {
                        _viewTopPos = Convert.ToInt32(meta.content);
                    }
                    catch { }

                    continue;
                }
				
			}
			if (!bAuthor)
			{
				throw new NotSupportedException(Properties.Resources.mbNotSupportedSMILFile);
			}

			Smil.Library.RootLayout rl = smil.head.layout.arrCollection.Single(el => el is Smil.Library.RootLayout) as Smil.Library.RootLayout;


			Dictionary<string, object> screen_props = new Dictionary<string, object>();

			double screen_width = Smil.Library.Convert.ToDouble(rl.width);
			double screen_height = Smil.Library.Convert.ToDouble(rl.height);
			screen_props.Add("Width", screen_width);
			screen_props.Add("Height", screen_height);
			screen_props.Add("Background", rl.background_color);


			Screen screenObject = new Screen((int)screen_width, (int)screen_height, isdesignMode);

			screenObject.IsAdScreen = _isAd;
			screenObject.Ad_EventConstraint = _showFlag;
			screenObject.Ad_StartTime = _starttime;
			screenObject.Ad_EndTime = _endtime;
			screenObject.Ad_MetaTags = _metaTags;
			screenObject.ScreenID = _screen_ID;
			screenObject.Profile = _profile;
            screenObject.ViewLeftPos = _viewLeftPos;
            screenObject.ViewTopPos = _viewTopPos;

			if(isdesignMode) screenObject.elementsListViewer = elementListViewerObject;
			((IMediaFilesManager)screenObject).InitMediaFilesList(mediaPath);
			
			((IDesignElement)screenObject).Properties = ProjectManager.DeserializeProperties(screen_props, mediaPath);          
			Smil.Library.Par par = smil.body.arrCollection.Single(el => el is Smil.Library.Par) as Smil.Library.Par;

			if(isdesignMode) screenObject.ToTalPlayTime = smil.TotalDuration();

			foreach (object objSeq in par.arrCollection)
			{
				if (objSeq is Smil.Library.Seq)
				{
					Dictionary<string, object> props = new Dictionary<string, object>();

					Smil.Library.ParamGroup pg = null;
					Smil.Library.Region region = null;
					List<PlaylistItem> arrPlaylist = new List<PlaylistItem>();
					IDesignElement addedElement = null;

					int nPlaylistIndex = 0;
					foreach (Smil.Library.MediaObject mo in ((Smil.Library.Seq)objSeq).arrCollection)
					{
						try
						{
							if (nPlaylistIndex++ == 0)
							{
								pg = smil.head.arrParamGroupCollection.Single(el => el.id == mo.paramGroup) as Smil.Library.ParamGroup;

								region = smil.head.layout.arrCollection.Single(el => el.id == mo.region) as Smil.Library.Region;
								props.Add("Left", Smil.Library.Convert.ToDouble(region.left));
								props.Add("Top", Smil.Library.Convert.ToDouble(region.top));
								props.Add("Width", Smil.Library.Convert.ToDouble(region.width));
								props.Add("Height", Smil.Library.Convert.ToDouble(region.height));

								Smil.Library.Param param = pg.arrParamCollection.Single(el => el.name.Equals("Type")) as Smil.Library.Param;
								addedElement = screenObject.Add(Type.GetType(param.Value));
								
								foreach (Smil.Library.Param p in pg.arrParamCollection)
								{
									if (p.type == "System.RuntimeType")
									{
										props.Add(p.name, Type.GetType(p.Value));
									}
									else
									{
										Type conversionType = Type.GetType(p.type);
                                        if (conversionType == null)
                                        {
                                            if (p.type.Equals("System.Windows.Media.Stretch"))
                                            {
                                                switch (p.Value)
                                                {
                                                    case "Fill":
                                                        props.Add(p.name, System.Windows.Media.Stretch.Fill);
                                                        break;
                                                    case "Uniform":
                                                        props.Add(p.name, System.Windows.Media.Stretch.Uniform);
                                                        break;
                                                    case "UniformToFill":
                                                        props.Add(p.name, System.Windows.Media.Stretch.UniformToFill);
                                                        break;
                                                    default:
                                                        props.Add(p.name, System.Windows.Media.Stretch.None);
                                                        break;
                                                }
                                            }
                                            else if (p.type.Equals("DigitalSignage.Common.TransformEffect"))
                                            {
                                                switch (p.Value)
                                                {
                                                    case "None":
                                                        props.Add(p.name, DigitalSignage.Common.TransformEffect.None);
                                                        break;
                                                    case "LeftToRight":
                                                        props.Add(p.name, DigitalSignage.Common.TransformEffect.LeftToRight);
                                                        break;
                                                    case "RightToLeft":
                                                        props.Add(p.name, DigitalSignage.Common.TransformEffect.RightToLeft);
                                                        break;
                                                    case "TopToBottom":
                                                        props.Add(p.name, DigitalSignage.Common.TransformEffect.TopToBottom);
                                                        break;
                                                    case "BottomToTop":
                                                        props.Add(p.name, DigitalSignage.Common.TransformEffect.BottomToTop);
                                                        break;
                                                    case "FadeIn":
                                                        props.Add(p.name, DigitalSignage.Common.TransformEffect.FadeIn);
                                                        break;
                                                    default:
                                                        props.Add(p.name, DigitalSignage.Common.TransformEffect.None);
                                                        break;
                                                }
                                            }

                                        }
                                        else
                                        {
                                            try
                                            {
                                                props.Add(p.name, Convert.ChangeType(p.Value, conversionType));
                                            }
                                            catch (Exception ex)
                                            {
                                                Console.WriteLine("Smil.Library.Param p in pg.arrParamCollection)" + ex.ToString());
                                            }
                                        }
									}
								}
							}

							if (mo is Smil.Library.Text)
							{
								arrPlaylist.Add(new PlaylistItem(((Smil.Library.Text)mo).Content, TimeSpan.FromSeconds(Smil.Library.Convert.ToInt32(mo.dur))));
							}
							else
							{
								arrPlaylist.Add(new PlaylistItem(mo.src.Replace(@".\media_files\", ""), TimeSpan.FromSeconds(Smil.Library.Convert.ToInt32(mo.dur))));
							}
				
						}
						catch (Exception ex) {
							Console.WriteLine(ex.ToString());
						}

					}
					props.Add("Playlist", arrPlaylist);
					if (addedElement != null)	addedElement.Properties = ProjectManager.DeserializeProperties(props, mediaPath);
					//((IDesignElement)screenObject.Children[nIndex]).Properties = props;
			
				}
			}

			return screenObject;
		}

		/// <summary>
		/// Screen Object를 Smil로 Export하기
		/// </summary>
		/// <param name="filepath">저장할 경로와 파일 이름</param>
		/// <param name="screenObject">저장할 스크린 오브젝트</param>
		/// <param name="ProjectName">저장할 프로젝트 이름</param>
		/// <returns>성공 여부</returns>
		static public bool ExportToSmil(string filepath, Screen screenObject, String ProjectName)
		{
			Smil.Library.Smil smil = new Smil.Library.Smil();
			smil.title = ProjectName;
			smil.head = new Smil.Library.Head();
			smil.body = new Smil.Library.Body();
			smil.head.layout = new Smil.Library.Layout();
			Smil.Library.Meta meta1 = new Smil.Library.Meta();
			meta1.name = "author";
			meta1.content = "i-Vision Designer";

			Smil.Library.Meta meta2 = new Smil.Library.Meta();
			meta2.name = "title";
			meta2.content = ProjectName;

			smil.head.arrMetaCollection.Add(meta1);
			smil.head.arrMetaCollection.Add(meta2);

			try
			{
				Smil.Library.Meta meta = new Smil.Library.Meta();
				meta.name = "isAd";
				meta.content = screenObject.IsAdScreen.ToString();
				smil.head.arrMetaCollection.Add(meta);
			}
			catch { }

			try
			{
				Smil.Library.Meta meta = new Smil.Library.Meta();
				meta.name = "showFlag";
				meta.content = screenObject.Ad_EventConstraint.ToString();
				smil.head.arrMetaCollection.Add(meta);
			}
			catch { }

			try
			{
				Smil.Library.Meta meta = new Smil.Library.Meta();
				meta.name = "AdStartTime";
				meta.content = screenObject.Ad_StartTime.ToString();
				smil.head.arrMetaCollection.Add(meta);
			}
			catch { }

			try
			{
				Smil.Library.Meta meta = new Smil.Library.Meta();
				meta.name = "AdEndTime";
				meta.content = screenObject.Ad_EndTime.ToString();
				smil.head.arrMetaCollection.Add(meta);
			}
			catch { }

			try
			{
				Smil.Library.Meta meta = new Smil.Library.Meta();
				meta.name = "MetaTags";
				meta.content = screenObject.Ad_MetaTags.ToString();
				smil.head.arrMetaCollection.Add(meta);
			}
			catch { }

			try
			{
				Smil.Library.Meta meta = new Smil.Library.Meta();
				meta.name = "ScreenID";
				meta.content = screenObject.ScreenID.ToString();
				smil.head.arrMetaCollection.Add(meta);
			}
			catch { }

			try
			{
				Smil.Library.Meta meta = new Smil.Library.Meta();
				meta.name = "Profile";
				meta.content = screenObject.Profile.ToString();
				smil.head.arrMetaCollection.Add(meta);
			}
			catch { }

            #region 오픈스크린

            try
            {
                Smil.Library.Meta meta = new Smil.Library.Meta();
                meta.name = "ViewLeftPos";
                meta.content = screenObject.ViewLeftPos.ToString();
                smil.head.arrMetaCollection.Add(meta);
            }
            catch { }

            try
            {
                Smil.Library.Meta meta = new Smil.Library.Meta();
                meta.name = "ViewTopPos";
                meta.content = screenObject.ViewTopPos.ToString();
                smil.head.arrMetaCollection.Add(meta);
            }
            catch { }

            #endregion


            Smil.Library.Par parent = new Smil.Library.Par();
			
			parent.repeatDur = "indefinite";
			parent.dur = String.Format("{0}s", screenObject.ToTalPlayTime.TotalSeconds);

			//	Screen 레이아웃 정보
			Smil.Library.RootLayout rl = new Smil.Library.RootLayout();

			int nIndex = 0;
			foreach (Dictionary<string, object> comp in screenObject.OverallProperties)
			{

				if (nIndex++ == 0)
				{
					rl.height = String.Format("{0}px", comp["Height"]);
					rl.width = String.Format("{0}px", comp["Width"]);
					rl.background_color = comp["Background"].ToString();
					continue;
				}
				Smil.Library.ParamGroup groupParam = new Smil.Library.ParamGroup();

				Smil.Library.Region region = new Smil.Library.Region();

				foreach (String key in comp.Keys)
				{
					switch (key)
					{
						case "Name":
							groupParam.id = String.Format("param_{0}", comp[key]);
							region.id = String.Format("region_{0}", comp[key]);
							break;
						case "Width":
							region.width = String.Format("{0}px", comp[key]);
							break;
						case "Height":
							region.height = String.Format("{0}px", comp[key]);
							break;
						case "Left":
							region.left = String.Format("{0}px", comp[key]);
							break;
						case "Top":
							region.top = String.Format("{0}px", comp[key]);
							break;
						case "Playlist":
							{
								Smil.Library.Seq seq = new Smil.Library.Seq();
								seq.repeatDur = "indefinite";
								seq.readIndex = Convert.ToInt32(comp["ZIndex"]);
								foreach (PlaylistItem item in comp[key] as List<PlaylistItem>)
								{
									Smil.Library.MediaObject objRef = GetMediaObjectFromType(comp["Type"] as Type);

									objRef.dur = item.IsInfinity ? "indefinite" : (item.DurationAsTimeSpan.TotalSeconds.ToString() + "s");
									Type t = comp["Type"] as Type;
									if (IsFileType(t))
										objRef.src = String.Format(@".\media_files\{0}", item.Content);
									else if (objRef is Smil.Library.Text)
									{
										((Smil.Library.Text)objRef).Content = item.Content;
									}
									else
										objRef.src = item.Content;

									objRef.paramGroup = groupParam.id;
									objRef.region = region.id;

									seq.arrCollection.Add(objRef);
								}
								bool bAdd = false;
								for (int i = 0; i < parent.arrCollection.Count; ++i)
								{
									Smil.Library.Seq temp = parent.arrCollection[i] as Smil.Library.Seq;

									if (temp != null && temp.readIndex > seq.readIndex)
									{
										parent.arrCollection.Insert(i, seq);
										smil.head.layout.arrCollection.Insert(i, region);
										bAdd = true;
										break;
									}
								}

								if (!bAdd)
								{
									parent.arrCollection.Add(seq);
									smil.head.layout.arrCollection.Add(region);

								}
								break;
							}
						default:
							{
								Smil.Library.Param param = new Smil.Library.Param();
								param.name = key;
                                if (comp[key] == null)
                                {
                                    param.Value = "";
                                    param.type = "";
                                }
                                else
                                {
                                    param.Value = comp[key].ToString();
                                    param.type = comp[key].GetType().ToString();
                                }
								groupParam.arrParamCollection.Add(param);
								break;
							}
					}
				}
				smil.head.arrParamGroupCollection.Add(groupParam);
			}

			smil.body.arrCollection.Add(parent);
			if (smil.head.layout.arrCollection.Count == 0)
				smil.head.layout.arrCollection.Add(rl);
			else
				smil.head.layout.arrCollection.Insert(0, rl);

			smil.SaveToSmil(filepath);

			return true;

//			return false;
		}

		/// <summary>
		/// 현재 타입이 파일 타입인지 아닌지 결정
		/// </summary>
		/// <param name="elementType"></param>
		/// <returns></returns>
		public static bool IsFileType(Type elementType)
		{
			if (elementType.Equals(typeof(AudioComponent)) ||
				elementType.Equals(typeof(FlashComponent)) ||
				elementType.Equals(typeof(ImageComponent)) ||
				elementType.Equals(typeof(MediaComponent)) ||
				elementType.Equals(typeof(PptComponent)) ||
				elementType.Equals(typeof(QuickTimeComponent)))
				return true;

			return false;
		}
		/// <summary>
		/// 현재 타입이 URL 타입인지 아닌지 결정
		/// </summary>
		/// <param name="elementType"></param>
		/// <returns></returns>
		public static bool IsURLType(Type elementType)
		{
			if (elementType.Equals(typeof(StreamingComponent)) ||
				elementType.Equals(typeof(RssComponent)) ||
				elementType.Equals(typeof(WebComponent)) ||
				elementType.Equals(typeof(MediaComponent)))
				return true;

			return false;
		}
		/// <summary>
		/// 타입별 미디어 오브젝트 반환
		/// </summary>
		/// <param name="elementType"></param>
		/// <returns></returns>
		public static Smil.Library.MediaObject GetMediaObjectFromType(Type elementType)
		{
			if (elementType.Equals(typeof(EllipseComponent)))
				return new Smil.Library.Animation();
			else if (elementType.Equals(typeof(AudioComponent)))
				return new Smil.Library.Audio();
			else if (elementType.Equals(typeof(DateComponent)))
				return new Smil.Library.Ref();
			else if (elementType.Equals(typeof(AnalogueClockComponent)))
				return new Smil.Library.Ref();
			else if (elementType.Equals(typeof(DigitalClockComponent)))
				return new Smil.Library.Ref();
			else if (elementType.Equals(typeof(FlashComponent)))
				return new Smil.Library.Ref();
			else if (elementType.Equals(typeof(ImageComponent)))
				return new Smil.Library.Img();
			else if (elementType.Equals(typeof(MediaComponent)))
				return new Smil.Library.Video();
			else if (elementType.Equals(typeof(PptComponent)))
				return new Smil.Library.Ref();
			else if (elementType.Equals(typeof(QuickTimeComponent)))
				return new Smil.Library.Ref();
			else if (elementType.Equals(typeof(RectangleComponent)))
				return new Smil.Library.Animation();
			else if (elementType.Equals(typeof(RssComponent)))
				return new Smil.Library.Ref();
			else if (elementType.Equals(typeof(ScrollTextComponent)))
				return new Smil.Library.Ref();
			else if (elementType.Equals(typeof(TextComponent)))
				return new Smil.Library.Text();
			else if (elementType.Equals(typeof(WebComponent)))
				return new Smil.Library.Ref();
			else if (elementType.Equals(typeof(WeatherComponent)))
				return new Smil.Library.Ref();
			else if (elementType.Equals(typeof(StreamingComponent)))
				return new Smil.Library.Video();
			else if (elementType.Equals(typeof(TVComponent)))
				return new Smil.Library.Ref();
			else
				return new Smil.Library.Ref();
		}
	}
}
