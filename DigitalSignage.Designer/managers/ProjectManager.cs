﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Markup;
using System.Xml;
using System.Xml.Linq;
using System.IO;
using System.Collections;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Converters;
using System.Windows.Threading;
using System.Reflection;

namespace WPFDesigner
{
    public class ProjectManager
    {
        /// <summary>
        /// The name of project
        /// </summary>
        public string ProjectName;

        /// <summary>
        /// Represents a zoom factor before saving the thumbnail
        /// </summary>
        private double _initialZoom;

        /// <summary>
        /// Path to the project
        /// </summary>
        public string ProjectPath;

        /// <summary>
        /// Path to project`s media files
        /// </summary>
        public string MediaPath;

        /// <summary>
        /// Represents the design surface
        /// </summary>
        public Screen Pane;

        public static string mediaPath;

        /// <summary>
        /// Represents the designer properties grid
        /// </summary>
        PropertiesViewer propertiesViewer;

        /// <summary>
        /// Smil프로젝트 파일인지 
        /// </summary>
        public bool IsSmilFile
        {
            get;
            set;
        }

        /// <summary>
        /// Represents the container for the Pane
        /// </summary>
        public Border ScreenScrollViewer
        {
            get
            {
                return ((DesignerWindow)Application.Current.MainWindow).MainGrid;
            }
        }

        /// <summary>
        /// Represents the container for the Pane
        /// </summary>
        public Border ScreenBorderViewer
        {
            get
            {
                return ((DesignerWindow)Application.Current.MainWindow).MainBorder;
            }
        }
        /// <summary>
        /// Represents the container for the propertiesViewer
        /// </summary>
        public ScrollViewer PropertiesScrollViewer
        {
            get
            {
                return ((DesignerWindow)Application.Current.MainWindow).propertiesScrollViever;
            }
        }

        /// <summary>
        /// Represents the design elements list viewer
        /// </summary>
        public ElementsListViewer ElementsListViewer
        {
            get
            {
                return ((DesignerWindow)Application.Current.MainWindow).elementsListViewer;
            }
        }

        /// <summary>
        /// Gets the path to the media files folder that are used by default 
        /// </summary>
        public static string defaultMediaPath
        {
            get
            {
                return AppDomain.CurrentDomain.BaseDirectory + @"media\";
            }
        }

        /// <summary>
        /// Gets the projects folder path
        /// </summary>
        public static string ProjectsFolder
        {
            get
            {
				String sExecutingPath = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
				return sExecutingPath + @"\DesignerData\Screens\";
            }
        }

        /// <summary>
        /// Initializes the new ProjectManager class instances
        /// </summary>
        public ProjectManager()
        {
            IsSmilFile = true;
        }

        /// <summary>
        /// Creates a new project with the specified name and pane aspect ratio
        /// </summary>
        /// <param name="projectName">The name of new project</param>
        /// <param name="aspect">Apsect ratio of pane`s width/height</param>
        public void CreateProject(string projectName, Point aspect)
        {
            CloseProject();

            ProjectPath = ProjectsFolder + projectName + @"\";
            ProjectName = projectName;
            MediaPath = ProjectPath + @"media_files\";

            mediaPath = MediaPath;

            #region Directories check
            if (Directory.Exists(ProjectPath))
            {
                MessageBoxResult result = MessageBox.Show("Project with the same name already exists. Overwrite it?", "Designer", MessageBoxButton.YesNo, MessageBoxImage.Warning);
                if (result == MessageBoxResult.Yes)
                {
                    try
                    {
                        if (Directory.Exists(MediaPath))
                        {
                            foreach (string file in Directory.GetFiles(MediaPath))
                            {
                                File.Delete(file);
                            }
                            Directory.Delete(mediaPath);
                        }
                        foreach (string file in Directory.GetFiles(ProjectPath))
                        {
                            File.Delete(file);
                        }
                        Directory.Delete(ProjectPath);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Cannot remove the existing folder." + Environment.NewLine + "Reason: " + ex.Message + "\n\rChoose another name.", "Designer", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return;
                    }
                }
                else
                {
                    return;
                }
            }
            else
            {
                try
                {
                    Directory.CreateDirectory(ProjectPath);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Cannot create the project folder." + Environment.NewLine + "Reason: " + ex.Message + "\n\rChoose another name.", "Designer", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
            }

            try
            {
                Directory.CreateDirectory(MediaPath);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Cannot create the media folder folder." + Environment.NewLine + "Reason: " + ex.Message, "Designer", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            #endregion

            Pane = new Screen(aspect);
            ScreenScrollViewer.Child = Pane;
            //ParentGrid.Children.Add(Pane);
            propertiesViewer = new PropertiesViewer();
            PropertiesScrollViewer.Content = propertiesViewer;
            Pane.propertiesViewer = propertiesViewer;
            ((IMediaFilesManager)Pane).InitMediaFilesList(MediaPath);
            Pane.elementsListViewer = ElementsListViewer;
        }

		/// <summary>
		/// Creates a new project with the specified name and pane aspect ratio
		/// </summary>
		/// <param name="projectName"></param>
		/// <param name="width"></param>
		/// <param name="height"></param>
		public void CreateProject(string projectName, int width, int height)
        {
            CloseProject();

            ProjectPath = ProjectsFolder + projectName + @"\";
            ProjectName = projectName;
            MediaPath = ProjectPath + @"media_files\";

            mediaPath = MediaPath;

            #region Directories check
            if (Directory.Exists(ProjectPath))
            {
                MessageBoxResult result = MessageBox.Show(Properties.Resources.mbOverwriteProject, "Designer", MessageBoxButton.YesNo, MessageBoxImage.Warning);
                if (result == MessageBoxResult.Yes)
                {
                    try
                    {
                        if (Directory.Exists(MediaPath))
                        {
                            foreach (string file in Directory.GetFiles(MediaPath))
                            {
                                File.Delete(file);
                            }
                            Directory.Delete(mediaPath);
                        }
                        foreach (string file in Directory.GetFiles(ProjectPath))
                        {
                            File.Delete(file);
                        }
                        Directory.Delete(ProjectPath);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(Properties.Resources.mbCannotRemoveDirectory1 + ex.Message + Properties.Resources.mbCannotRemoveDirectory2, "Designer", MessageBoxButton.OK, MessageBoxImage.Warning);
                        return;
                    }
                }
                else
                {
                    return;
                }
            }
            else
            {
                try
                {
                    Directory.CreateDirectory(ProjectPath);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Cannot create the project folder." + Environment.NewLine + "Reason: " + ex.Message + "\n\rChoose another name.", "Designer", MessageBoxButton.OK, MessageBoxImage.Warning);
                    return;
                }
            }

            try
            {
                Directory.CreateDirectory(MediaPath);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Cannot create the media folder folder." + Environment.NewLine + "Reason: " + ex.Message, "Designer", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            #endregion

			Pane = new Screen(width, height, true);
//             ((IDesignElement)Pane).Name = projectName;

            ScreenScrollViewer.Child = Pane;
            //ParentGrid.Children.Add(Pane);
            propertiesViewer = new PropertiesViewer();
            PropertiesScrollViewer.Content = propertiesViewer;
            Pane.propertiesViewer = propertiesViewer;
            ((IMediaFilesManager)Pane).InitMediaFilesList(MediaPath);
            Pane.elementsListViewer = ElementsListViewer;
        }

		public bool HasPlayingContents()
		{
			try
			{
				if (Pane.IsPlay)
					return true;

				foreach (IDesignElement element in Pane.Children)
				{
					if (element.ISPlay)
						return true;
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}

			return false;
		}

		private bool StopAllComponents()
		{
			try
			{
				if (Pane == null)
					return true;

				if (Pane.IsPlay)
				{
					((IDesignElement)Pane).Stop();
					return true;
				}

				foreach (IDesignElement element in Pane.Children)
				{
					if (element.ISPlay)
					{
						element.Stop();
					}
				}
				return true;
			}
			catch { return false; }
		
		}

		public void UpdatePlaytime()
		{
			int totalPlaytime = 0;
			try
			{
				foreach (IDesignElement element in Pane.Children)
				{
					try
					{
						int item_playtime = 0;
						foreach (PlaylistItem item in element.Playlist)
						{
							if (item.IsInfinity)
							{
								item_playtime = 0;
								break;
							}

							item_playtime += Convert.ToInt32(item.TIME.TotalSeconds);
						}

						if (totalPlaytime < item_playtime) totalPlaytime = item_playtime;
					}
					catch(Exception e)
					{
						MessageBox.Show(e.ToString());
					}
				}
			}
			catch(Exception ex)
			{
				MessageBox.Show(ex.ToString());
			}

			if(Pane != null) Pane.ToTalPlayTime = new TimeSpan(0, 0, totalPlaytime);
		}

		private bool IsUsingContent(IDesignElement element, string mediapath)
		{
			bool bImageContent = IsImageContent(mediapath);

			if (bImageContent)
			{
				FileInfo info = new FileInfo(mediapath);
				string filename = info.Name;

				if (element.Type == typeof(TextComponent) ||
					element.Type == typeof(ScrollTextComponent))
				{
					try
					{
						foreach (PlaylistItem item in element.Playlist)
						{
							if (item.Content.Contains(filename))
							{
								return true;
							}
						}
					}
					catch { }

				}
				else if(element.Type == typeof(ImageComponent))
				{
					try
					{
						foreach (PlaylistItem item in element.Playlist)
						{
							if (mediapath.Contains(item.Content))
							{
								return true;
							}
						}
					}
					catch { }
				}

				try
				{
					if (element.Background.GetType() == typeof(ImageBrush))
					{
						if ((((ImageBrush)element.Background).ImageSource).ToString().Contains(filename))
							return true;
					}
				}
				catch /*(Exception ex)*/ {/*MessageBox.Show(ex.ToString());*/ }

				try
				{
					if (element.BorderBrush.GetType() == typeof(ImageBrush))
					{
						if ((((ImageBrush)element.BorderBrush).ImageSource).ToString().Contains(filename))
							return true;
					}
				}
				catch /*(Exception ex)*/ {/*MessageBox.Show(ex.ToString());*/ }

				try
				{
					if (element.Foreground.GetType() == typeof(ImageBrush))
					{
						if ((((ImageBrush)element.Foreground).ImageSource).ToString().Contains(filename))
							return true;
					}
				}
				catch /*(Exception ex)*/ { /*MessageBox.Show(ex.ToString());*/ }
			}

			else //	일반 컨텐츠
			{
				try
				{
					foreach (PlaylistItem item in element.Playlist)
					{
						if (mediapath.Contains(item.Content))
						{
							return true;
						}
					}
				}
				catch {}
			}

			return false;
		}

		public void RemoveUnusedMedias()
		{
			try
			{
				for (int i = Pane.MediaFilesList.Count - 1; i >= 0; --i)
				{
					string media = Pane.MediaFilesList[i];

					try
					{
						bool bUse = false;
						IDesignElement elScreen = Pane as IDesignElement;

						if(elScreen != null)
						{
							if (bUse = IsUsingContent(elScreen, media))
							{
								continue;
							}

						}

						foreach (IDesignElement element in Pane.Children)
						{
							try
							{
								if(bUse = IsUsingContent(element, media))
								{
									break;
								}
							}
							catch
							{}
						}

						if (!bUse)
						{
							try
							{
								File.Delete(media);
								Pane.MediaFilesList.Remove(media);
							}
							catch /*(Exception e)*/ { /*MessageBox.Show(e.Message);*/ }
						}
					}
					catch
					{}

				}
			}
			catch
			{

			}
		}
		private bool IsImageContent(string content)
		{
			string lowercase = content.ToLower();
			if (lowercase.EndsWith(".bmp") ||
				lowercase.EndsWith(".png") ||
				lowercase.EndsWith(".gif") ||
				lowercase.EndsWith(".tif") ||
				lowercase.EndsWith(".jpg") ||
				lowercase.EndsWith(".jpeg"))
				return true;

			return false;
		}

		public void RemoveUnusedMedias(Screen screenPane)
		{
			try
			{
				for (int i = screenPane.MediaFilesList.Count - 1; i >= 0; --i)
				{
					string media = screenPane.MediaFilesList[i];

					try
					{
						bool bUse = false;

						IDesignElement elScreen = Pane as IDesignElement;

						if (elScreen != null)
						{
							if (bUse = IsUsingContent(elScreen, media))
							{
								continue;
							}
						}

						foreach (IDesignElement element in screenPane.Children)
						{
							try
							{
								if(bUse = IsUsingContent(element, media))
									break;
							}
							catch
							{ }
						}

						if (!bUse)
						{
							try
							{
								File.Delete(media);
								screenPane.MediaFilesList.Remove(media);
							}
							catch /*(Exception e)*/ { /*MessageBox.Show(e.Message);*/ }
						}
					}
					catch
					{ }

				}
			}
			catch
			{

			}
		}

        /// <summary>
        /// Saves the project (Screen instance) into the file located under the specified path
        /// </summary>
        /// <param name="path">The path to save the Pane to</param>
        public void SaveProject(string path)
        {
			string oldScreenID = Pane.ScreenID;
			try
            {
				if (!StopAllComponents())
				{
					MessageBox.Show(Properties.Resources.mbErrorPlaying, Properties.Resources.titleDesignerWindow, MessageBoxButton.OK, MessageBoxImage.Stop);
					return;
				}

				UpdatePlaytime();

				RemoveUnusedMedias();

				///	새로운 스크린 ID를 생성한다.
				Pane.ScreenID = DateTime.Now.ToString("yyyyMMddHHmmssfff");
                if (!IsSmilFile)
                {
                    using (FileStream fileStream = new FileStream(path + ProjectName + ".bin", FileMode.Create, FileAccess.Write))
                    {
                        BinaryFormatter binaryFotmatter = new BinaryFormatter();
                        binaryFotmatter.Serialize(fileStream, Pane.OverallProperties);
                        fileStream.Close();
                    }                   

                    IsSmilFile = false;

                    MostRecentFiles.GetInstance.Push(new MostRecentFile(this.ProjectPath + this.ProjectName + ".bin", true));
                }
                else
                {
                    SmilManager.ExportToSmil(path + ProjectName + ".smil", Pane, ProjectName);
                    IsSmilFile = true;

                    MostRecentFiles.GetInstance.Push(new MostRecentFile(this.ProjectPath + this.ProjectName + ".smil", true));
                }

                Pane.IsChanged = false;
                SaveThumbnail();


            }
            catch (ArgumentNullException ex)
            {
				Pane.ScreenID = oldScreenID;
                MessageBox.Show(ex.Message.ToString());
            }
            catch (SerializationException ex)
            {
				Pane.ScreenID = oldScreenID;
				MessageBox.Show(ex.Message.ToString());
            }
			catch (Exception e)
			{
				Pane.ScreenID = oldScreenID;
				MessageBox.Show(e.Message);
			}
        }

        /// <summary>
        /// Saves the Screen to the specified path
        /// </summary>
        /// <param name="path">Path to save screen to</param>
        public void SaveProjectAs(string path)
        {
			string oldScreenID = Pane.ScreenID;

            bool bIsOldScreenSmil = IsSmilFile;

			try
			{
				
				if (!StopAllComponents())
				{
					MessageBox.Show(Properties.Resources.mbErrorPlaying, Properties.Resources.titleDesignerWindow, MessageBoxButton.OK, MessageBoxImage.Stop);
					return;
				}

				string oldProjectName = this.ProjectName;
				string oldProjectPath = this.ProjectPath;
				string oldMediaPath = this.MediaPath;

				UpdatePlaytime();

				string newProjectName = Path.GetFileNameWithoutExtension(path);
				string newProjectPath = String.Empty;

				if(!File.Exists(path))
// 				if (Path.GetDirectoryName(path).Substring(path.LastIndexOf('\\')) != newProjectName)
				{
					newProjectPath = Path.GetDirectoryName(path) + @"\" + newProjectName + @"\";
				}
				else
				{
					newProjectPath = Path.GetDirectoryName(path) + @"\";
				}

				if (!Directory.Exists(newProjectPath))
				{
					Directory.CreateDirectory(newProjectPath);
				}

				string newMediaPath = newProjectPath + @"media_files\";

				if(!oldMediaPath.Equals(newMediaPath))
				{
					if (!Directory.Exists(newMediaPath))
					{
						Directory.CreateDirectory(newMediaPath);
					}

					foreach (string file in Directory.GetFiles(MediaPath))
					{
						bool bUse = false;
						foreach (string media in Pane.MediaFilesList)
						{
							if (file.Equals(media))
							{
								bUse = true;
								break;
							}
						}

						if (bUse) File.Copy(file, newMediaPath + Path.GetFileName(file), true);
					}
				}

				///	새로운 스크린 ID를 생성한다.
				Pane.ScreenID = DateTime.Now.ToString("yyyyMMddHHmmssfff");

                if (path.ToLower().EndsWith(".bin"))
                {

                    using (FileStream fileStream = new FileStream(newProjectPath + newProjectName + ".bin", FileMode.Create, FileAccess.Write))
                    {
                        BinaryFormatter binaryFotmatter = new BinaryFormatter();
                        binaryFotmatter.Serialize(fileStream, Pane.OverallProperties);
                        fileStream.Close();
                    }
                    IsSmilFile = false;

                    MostRecentFiles.GetInstance.Push(new MostRecentFile(this.ProjectPath + this.ProjectName + ".bin", true));
                }
                else
                {
                    SmilManager.ExportToSmil(newProjectPath + newProjectName + ".smil", Pane, newProjectName);
                    IsSmilFile = true;

                    MostRecentFiles.GetInstance.Push(new MostRecentFile(this.ProjectPath + this.ProjectName + ".smil", true));

                }

				ProjectName = newProjectName;
				ProjectPath = newProjectPath;
				MediaPath = newMediaPath;

				((IMediaFilesManager)Pane).InitMediaFilesList(newMediaPath);

				Pane.MediaPath = newMediaPath;
				Pane.IsChanged = false;

				RemoveUnusedMedias();

				#region 이전 스크린 파일 정리
				try
				{
					Screen oldScreen = LoadScreen(oldProjectPath + oldProjectName + ".bin");
					RemoveUnusedMedias(oldScreen);
				}
				catch {}

				#endregion

				SaveThumbnail();				

			}
			catch (ArgumentNullException ex)
			{
				Pane.ScreenID = oldScreenID;
				MessageBox.Show(ex.Message.ToString());
			}
			catch (SerializationException ex)
			{
				Pane.ScreenID = oldScreenID;
				MessageBox.Show(ex.Message.ToString());
			}
			catch (Exception e)
			{
				Pane.ScreenID = oldScreenID;
				MessageBox.Show(e.Message);
			}
        }

        /// <summary>
        /// Closes current project
        /// </summary>
        public bool CloseProject()
        {
            if (Pane != null)
            {
                if (Pane.IsChanged)
                {
					MessageBoxResult result = MessageBox.Show(Properties.Resources.mbScreenChanged, Properties.Resources.titleDesignerWindow, MessageBoxButton.YesNoCancel, MessageBoxImage.Warning);
                    if (result == MessageBoxResult.Cancel)
                    {
                        return false;
                    }
                    if (result == MessageBoxResult.Yes)
                    {
                        SaveProject(ProjectPath);
                    }
                }
                Pane.Close();
                Pane = null;
                propertiesViewer.Close();
                PropertiesScrollViewer.Content = null;

                IsSmilFile = true;
            }
            ScreenScrollViewer.Child = null;
            return true;
            //ParentGrid.Children.Clear();
        }

        /// <summary>
        /// Loads the project from the specified file
        /// </summary>
        /// <param name="path">Project file path</param>
        /// <returns>The loaded Pane class instance</returns>
        public bool LoadProject(string path)
        {
            CloseProject();

            string lowcase = path.ToLower();

            if (lowcase.EndsWith(".bin"))
            {
                return LoadProjectFromBin(path);
            }
            else if (lowcase.EndsWith(".smil"))
            {
                return LoadProjectFromSmil(path);
            }
            else
            {
                throw new NotSupportedException("Not Supported Extansion of Screen Project.");                
            }
        }

        /// <summary>
        /// Loads the project from the specified file
        /// </summary>
        /// <param name="path">Project file path</param>
        /// <returns>The loaded Pane class instance</returns>
        public bool LoadProjectFromSmil(string path)
        {
            ProjectPath = Path.GetDirectoryName(path) + @"\";
            MediaPath = ProjectPath + @"media_files\";
            ProjectName = Path.GetFileNameWithoutExtension(path);

            Pane = SmilManager.ImportFromSmil(path, MediaPath, true, ElementsListViewer);

            if (Pane.elementsListViewer != null)
                Pane.elementsListViewer.SortItems();

            ScreenScrollViewer.Child = Pane;
            //ParentGrid.Children.Add(Pane);
            propertiesViewer = new PropertiesViewer();
            PropertiesScrollViewer.Content = propertiesViewer;
            Pane.propertiesViewer = propertiesViewer;
            Pane.IsChanged = false;
            ((IMediaFilesManager)Pane).InitMediaFilesList(MediaPath);

            MostRecentFiles.GetInstance.Push(new MostRecentFile(this.ProjectPath + this.ProjectName + ".smil", true));
            IsSmilFile = true;

            return true;
        }

        /// <summary>
        /// Loads the project from the specified file
        /// </summary>
        /// <param name="path">Project file path</param>
        /// <returns>The loaded Pane class instance</returns>
        public bool LoadProjectFromBin(string path)
        {
            ProjectPath = Path.GetDirectoryName(path) + @"\";
            MediaPath = ProjectPath + @"media_files\";
            ProjectName = Path.GetFileNameWithoutExtension(path);
            FileStream fileStream = new FileStream(path, FileMode.Open, FileAccess.Read);
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            List<Dictionary<string, object>> propertiesSet = new List<Dictionary<string, object>>();

            try
            {
                propertiesSet = binaryFormatter.Deserialize(fileStream) as List<Dictionary<string, object>>;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                fileStream.Close();
                fileStream = null;
                return false;
            }
            finally
            {
                if (fileStream != null)
                    fileStream.Close();

            }

            Pane = new Screen(Convert.ToInt32(propertiesSet[0]["Width"]), Convert.ToInt32(propertiesSet[0]["Height"]), true);

            try
            {
                Pane.ToTalPlayTime = (TimeSpan)propertiesSet[0]["PlayTime"];
                Pane.IsAdScreen = (bool)propertiesSet[0]["isAd"];
                Pane.Ad_EventConstraint = (int)propertiesSet[0]["showFlag"];
                Pane.Ad_StartTime = (DateTime)propertiesSet[0]["AdStartTime"];
                Pane.Ad_EndTime = (DateTime)propertiesSet[0]["AdEndTime"];
                Pane.Ad_MetaTags = (String)propertiesSet[0]["MetaTags"];
            }
            catch
            {
                Pane.ToTalPlayTime = new TimeSpan(0);
                return false;
            }

            try
            {
                Pane.Profile = (String)propertiesSet[0]["Profile"];
            }
            catch { 
                Pane.Profile = Properties.Resources.comboitemDefault;
                //return false;
            }

            try
            {
                Pane.ScreenID = (String)propertiesSet[0]["ScreenID"];
            }
            catch { }

            Pane.elementsListViewer = ElementsListViewer;
            ((IMediaFilesManager)Pane).InitMediaFilesList(MediaPath);

            int count = 0;

            try
            {
                ((IDesignElement)Pane).Properties = ProjectManager.DeserializeProperties(propertiesSet[0], ProjectPath + "media_files\\");
                for (int i = 1; i < propertiesSet.Count; i++)
                {
                    Dictionary<string, object> props = ProjectManager.DeserializeProperties(propertiesSet[i], ProjectPath + "media_files\\");
                    try
                    {
                        Pane.Add(props.Single(el => el.Key == "Type").Value as Type);
                    }
                    catch
                    { }
                    try
                    {
                        ((IDesignElement)Pane.Children[count]).Properties = props;
                    }
                    catch
                    { }
                    count++;
                }

                if (Pane.elementsListViewer != null)
                    Pane.elementsListViewer.SortItems();
            }
            catch { }

            ScreenScrollViewer.Child = Pane;
            //ParentGrid.Children.Add(Pane);
            propertiesViewer = new PropertiesViewer();
            PropertiesScrollViewer.Content = propertiesViewer;
            Pane.propertiesViewer = propertiesViewer;
            Pane.IsChanged = false;
            ((IMediaFilesManager)Pane).InitMediaFilesList(MediaPath);

            MostRecentFiles.GetInstance.Push(new MostRecentFile(this.ProjectPath + this.ProjectName + ".bin", true));
            IsSmilFile = false;

            return true;
        }

        /// <summary>
        /// Static method that provides the loading of Screen from the file 
        /// located under the specified path
        /// </summary>
        /// <param name="path">Path to file in what the screen is saved</param>
        /// <returns>WPFDesigner.Screen class instance loaded from the specified file</returns>
        public static Screen LoadScreen(string path)
        {
            string lowcase = path.ToLower();

            if (lowcase.EndsWith(".bin"))
            {
                return LoadScreenFromBIN(path);
            }
            else if (lowcase.EndsWith(".smil"))
            {
                return LoadScreenFromSMIL(path);
            }
            else
            {
                throw new NotSupportedException("Not Supported Extansion of Screen Project.");
            }
        }

        public static Screen LoadScreenFromSMIL(string path)
        {
            Screen screen;
            string projectPath = Path.GetDirectoryName(path) + @"\";
            string mediaPath = projectPath + @"media_files\";

            screen = SmilManager.ImportFromSmil(path, mediaPath, false, null);

            ((IMediaFilesManager)screen).InitMediaFilesList(mediaPath);
            screen.IsInDesginMode = false;
            return screen;
        }

        public static Screen LoadScreenFromBIN(string path)
        {
            Screen screen;
            string projectPath = Path.GetDirectoryName(path) + @"\";
            string mediaPath = projectPath + @"media_files\";

            FileStream fileStream = new FileStream(path, FileMode.Open, FileAccess.Read);
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            List<Dictionary<string, object>> propertiesSet = new List<Dictionary<string, object>>();

            try
            {
                propertiesSet = binaryFormatter.Deserialize(fileStream) as List<Dictionary<string, object>>;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            screen = new Screen(Convert.ToInt32(propertiesSet[0]["Width"]), Convert.ToInt32(propertiesSet[0]["Height"]), false);
            ((IMediaFilesManager)screen).InitMediaFilesList(mediaPath);

            int count = 0;

            ((IDesignElement)screen).Properties = ProjectManager.DeserializeProperties(propertiesSet[0], projectPath + "media_files\\");
            for (int i = 1; i < propertiesSet.Count; i++)
            {
                Dictionary<string, object> props = ProjectManager.DeserializeProperties(propertiesSet[i], projectPath + "media_files\\");
                try
                {
                    screen.Add(props.Single(el => el.Key == "Type").Value as Type);
                }
                catch
                { }
                try
                {
                    ((IDesignElement)screen.Children[count]).Properties = props;
                }
                catch
                { }
                count++;
            }
            ((IMediaFilesManager)screen).InitMediaFilesList(mediaPath);
            screen.IsInDesginMode = false;
            return screen;


        }

        /// <summary>
        /// Serializes the specified properties set (makes all the non-common type properties values to type of string)
        /// </summary>
        /// <param name="properties">The properties set to serialize</param>
        /// <returns>Serialized properties set</returns>
        public static Dictionary<string, object> SerializeProperties(Dictionary<string, object> properties)
        {
            Dictionary<string, object> serializedProperties = new Dictionary<string, object>();
            foreach (string key in properties.Keys)
            {
                if (properties[key] == null)
                {
                    serializedProperties.Add(key, properties[key]);
                    continue;
                }

                switch (properties[key].GetType().Name)
                {
                    case "SolidColorBrush":
                    case "LinearGradientBrush":
                    case "ImageBrush":
                    case "Brush":
                    case "Thickness":
                    case "HorizontalAlignment":
                    case "VerticalAlignment":
                    case "FontFamily":
                    case "FontWeight":
                    case "Point":
                    case "Type":
                        {
                            serializedProperties.Add(key, XamlWriter.Save(properties[key]));
                            break;
                        }
                    default:
                        {
                            serializedProperties.Add(key, properties[key]);
                            break;
                        }
                }
            }
            return serializedProperties;
        }

        /// <summary>
        /// Deserializes the given properties dictionary and updates image brushes, etc. basing 
        /// on the supplies media files path to avoid FileNotFound exceptions while deserializeng brushes
        /// </summary>
        /// <param name="properties">A serialized properties set</param>
        /// <param name="mediaPath">A path to update image brushes with</param>
        /// <returns>A set of deserialized properties that can be applied to any 
        /// design element that implements the IDesignElement interface</returns>
        public static Dictionary<string, object> DeserializeProperties(Dictionary<string, object> properties, string mediaPath)
        {
            Dictionary<string, object> deserializedProperties = new Dictionary<string, object>();

            foreach (string key in properties.Keys)
            {
                if (properties[key] == null)
                {
                    deserializedProperties.Add(key, properties[key]);
                    continue;
                }
                switch (properties[key].GetType().Name)
                {
                    case "String":
                        {
                            string tempKey = properties[key].ToString();
                            if (!String.IsNullOrEmpty(tempKey) && tempKey[0] == '<')
                            {
                                XElement element = XElement.Parse(tempKey);

                                switch (element.Name.LocalName)
                                {
                                    case "ImageBrush":
                                        {
                                            if (element.Attributes("ImageSource").Count() == 1)
                                            {
                                                element.Attribute("ImageSource").Value = mediaPath + System.IO.Path.GetFileName(element.Attribute("ImageSource").Value);
                                            }
                                            break;
                                        }
                                    default: break;
                                }

                                tempKey = element.ToString();
                                StringReader stringReader = new StringReader(tempKey);
                                XmlReader xmlReader = XmlReader.Create(stringReader);
                                deserializedProperties.Add(key, XamlReader.Load(xmlReader));
                            }
                            else
                            {
                                deserializedProperties.Add(key, tempKey);
                            }
                            break;
                        }
                    default:
                        {
                            deserializedProperties.Add(key, properties[key]);
                            break;
                        }
                }
            }

            return deserializedProperties;
        }

        public void InitZoomControl(UIElement el)
        {
            if (Pane != null)
            {
                Pane.InitZoomControl(el);
            }
        }

        /// <summary>
        /// Gets the ProjectManager instance
        /// </summary>
        /// <returns>ProjectManager</returns>
        public static ProjectManager GetCurrentProjectManager()
        {
            return ((IProjectManagerContainer)Application.Current.MainWindow).GetProjectManager();
        }

        /// <summary>
        /// 줌을 스크린 크기에 맞춘다.
        /// </summary>
        private void ZoomInActualScreenSize()
        {
            double dZoom = Pane.ZoomFactor;
            while (Pane != null && (ScreenBorderViewer.ActualWidth < (Pane.Width * dZoom) || ScreenBorderViewer.ActualHeight < (Pane.Height * dZoom)))
            {
                
                dZoom = Pane.ZoomFactor;
                if (dZoom <= 0.1)
                    break;

                Pane.ZoomTo(dZoom - 0.1);
            }
        }

        /// <summary>
        /// Saves the thumbnail of the screen
        /// </summary>
        public void SaveThumbnail()
        {
            if (Pane == null) return;
            _initialZoom = Pane.ZoomFactor;

            ZoomInActualScreenSize();

            Pane.PlayStarted = DateTime.Now;
            DigitalSignage.Common.ScreenStartInfoInSchedule.GetInstance.ScreenStartDT = DateTime.Now;

            foreach (UIElement el in Pane.Children)
            {
                IDesignElement element = el as IDesignElement;
                if (element.Playlist != null && element.Playlist.Count > 0)
                    if (
                    element.GetType() == typeof(ImageComponent)
					|| element.GetType() == typeof(MediaComponent)
					|| element.GetType() == typeof(FlashComponent)
					|| element.GetType() == typeof(PptComponent)
					|| element.GetType() == typeof(QuickTimeComponent)
					|| element.GetType() == typeof(WebComponent)
                    || element.GetType() == typeof(TextComponent))
                    {
						try
						{
							element.Play();
						}
						catch
						{ }
//                         element.Content = element.Playlist[0].Content;
                    }
            }
            DispatcherTimer dt = new DispatcherTimer();
            dt.Interval = new TimeSpan(0, 0, 2);
            dt.Tick += new EventHandler(dt_Tick);
            dt.Start();
        }

        void dt_Tick(object sender, EventArgs e)
        {
            try
            {
                ((DispatcherTimer)sender).Stop();
                Pane.UpdateLayout();

                int screenWidth = (int)(Pane.RenderSize.Width * Pane.ZoomFactor);
                int screenHeight = (int)(Pane.RenderSize.Height * Pane.ZoomFactor);

                Matrix m = PresentationSource.FromVisual(Application.Current.MainWindow).CompositionTarget.TransformToDevice;
                RenderTargetBitmap renderTarget = new RenderTargetBitmap(screenWidth, screenHeight, m.M11 * 96.0, m.M22 * 96.0, PixelFormats.Pbgra32);
                renderTarget.Render(Pane);
                PngBitmapEncoder encoder = new PngBitmapEncoder();
                encoder.Frames.Add(BitmapFrame.Create(renderTarget));

				using (FileStream fs = new FileStream(ProjectPath + "tempthumb.png", FileMode.Create, FileAccess.Write))
				{
					encoder.Save(fs);
					fs.Flush();
					fs.Close();
				}

				System.Drawing.Image tempThumb = System.Drawing.Image.FromFile(ProjectPath + "tempthumb.png");
				
				using (System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(tempThumb))
				{
					//	hsshin 테스트
					foreach (UIElement el in Pane.Children)
					{
						IDesignElement element = el as IDesignElement;
						if (element.Playlist != null && element.Playlist.Count > 0)
							if (element.GetType() == typeof(FlashComponent) ||
								element.GetType() == typeof(MediaComponent) ||
								element.GetType() == typeof(QuickTimeComponent) ||
								element.GetType() == typeof(WebComponent) ||
								element.GetType() == typeof(PptComponent))
							{
								try
								{
                                    System.Drawing.Bitmap bit = element.GetThumbnail(Convert.ToInt32(element.Width * Pane.ZoomFactor), Convert.ToInt32(element.Height * Pane.ZoomFactor));
									if (bit != null)
									{
                                        g.DrawImage(bit, (float)(element.Left * Pane.ZoomFactor), (float)(element.Top * Pane.ZoomFactor));
									}

								}
								catch
								{

								}
							}
					}
					tempThumb.Save(ProjectPath + "tempthumb2.png", System.Drawing.Imaging.ImageFormat.Png);
				}
				tempThumb.Dispose();
				File.Delete(ProjectPath + "tempthumb.png");
				File.Move(ProjectPath + "tempthumb2.png", ProjectPath + "tempthumb.png");

				using (FileStream fs2 = new FileStream(ProjectPath + "thumb.png", FileMode.Create, FileAccess.Write))
				{
					BitmapImage bi = new BitmapImage();
					bi.BeginInit();
					if (screenWidth > screenHeight)
					{
						bi.DecodePixelWidth = 200;
						bi.DecodePixelHeight = Convert.ToInt32(200 * screenHeight / screenWidth);
					}
					else
					{
						bi.DecodePixelHeight = 200;
						bi.DecodePixelWidth = Convert.ToInt32(200 * screenWidth / screenHeight);
					}

					using (FileStream fs3 = new FileStream(ProjectPath + "tempthumb.png", FileMode.Open, FileAccess.Read))
					{
						bi.StreamSource = fs3;
						bi.EndInit();

						PngBitmapEncoder enc = new PngBitmapEncoder();
						enc.Frames.Add(BitmapFrame.Create(bi));
						enc.Save(fs2);
						fs2.Flush();
						fs2.Close();

						bi = null;
						fs3.Flush();
						fs3.Close();
						bi = null;
					}
				}



                try
                {
                    File.Delete(ProjectPath + "tempthumb.png");
                }
				catch { }
            }
			catch { }

			if (Pane != null)
			{
				try
				{

					foreach (UIElement el in Pane.Children)
					{
						IDesignElement element = el as IDesignElement;
						if (element.Playlist != null && element.Playlist.Count > 0)
							if (
							element.GetType() == typeof(ImageComponent)
							|| element.GetType() == typeof(MediaComponent)
							|| element.GetType() == typeof(WebComponent)
							|| element.GetType() == typeof(FlashComponent)
							|| element.GetType() == typeof(PptComponent)
							|| element.GetType() == typeof(QuickTimeComponent)
							|| element.GetType() == typeof(TextComponent))
							{
								try
								{
									element.Stop();
								}
								catch
								{ }
	//                             element.Content = "";
							}
					}
					Pane.ZoomTo(_initialZoom);
				}
				catch {}

				MessageBox.Show(Properties.Resources.mbSavedProject, Properties.Resources.titleDesignerWindow);
            }
            else return;
        }
    }
}