﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Markup;
using System.IO;
using System.Xml;
using System.Windows.Documents;

namespace WPFDesigner
{
    /// <summary>
    /// Manages the clock`s Gmt
    /// </summary>
    public class GmtManager
    {
        /// <summary>
        /// Returns TimeZoneInfo from system time zones by given index in string
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public static TimeZoneInfo GetTimeZoneFromIndexInString(string index)
        {
            int ind = 0;
            if (Int32.TryParse(index, out ind))
            {
                return TimeZoneInfo.GetSystemTimeZones()[ind];
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Returns the string index of the specified TimeZoneInfo
        /// </summary>
        /// <param name="info"></param>
        /// <returns></returns>
        public static string GetStringIndexOfTimeZone(TimeZoneInfo info)
        {
            try
            {
                return TimeZoneInfo.GetSystemTimeZones().IndexOf(info).ToString();
            }
            catch { return null; }
        }
    }

    /// <summary>
    /// Manages the serializing/deserializing of ScrolTextInfo class
    /// </summary>
    public class ScrollTextInfoManager
    {
        /// <summary>
        /// Deserializes the ScrollTextInfo classs instance from string
        /// </summary>
        /// <param name="infoDefinition">The string representation of serialized ScrollTextInfo class instance</param>
        /// <returns>The ScrollTextInfo instance</returns>
        public static ScrollTextInfo DeserializeScrollTextInfo(string infoDefinition)
        {
            if (infoDefinition == "")
            {
                return new ScrollTextInfo();
            }
            try
            {
                StringReader sr = new StringReader(infoDefinition);
                XmlReader xr = XmlReader.Create(sr);
                return (ScrollTextInfo)XamlReader.Load(xr);
            }
            catch
            {
                return new ScrollTextInfo();
            }
        }

        /// <summary>
        /// Serializes the specified ScrollTextInfo class instance into string
        /// </summary>
        /// <param name="info">ScrollTextInfo to serialized</param>
        /// <returns>String representation of the given ScrolTextInfo instance</returns>
        public static string SerializeScrollTextInfo(ScrollTextInfo info)
        {
            return XamlWriter.Save(info);
        }

        /// <summary>
        /// Updates the 
        /// </summary>
        /// <param name="infoDefinition"></param>
        /// <param name="mediaPath"></param>
        public static void UpdateImageBrushesOfScrollTextInfo(string infoDefinition, string mediaPath)
        {

        }
    }

    /// <summary>
    /// Manages the FlowDocument serializing
    /// </summary>
    public class FlowDocumentManager
    {
        /// <summary>
        /// Deserializes the FlowDocument from its string representation using XAMLReader
        /// </summary>
        /// <param name="serializedDocument">FlowDocument serialized w/ XAMLWriter</param>
        /// <returns>FlowDocument</returns>
        public static FlowDocument DeserializeFlowDocument(string serializedDocument)
        {
            if (serializedDocument != "")
            {
                StringReader reader = new StringReader(serializedDocument);
                XmlReader xmlReader = XmlReader.Create(reader);
                return (FlowDocument)XamlReader.Load(xmlReader);
            }
            else
            {
                return new FlowDocument();
            }
        }
        
        /// <summary>
        /// Serializes the FlowDocument w/ XAMLWriter
        /// </summary>
        /// <param name="doc">FlowDocument to serialize</param>
        /// <returns>The string representation of serialized FlowDocument instance</returns>
        public static string SerializeFlowDocument(FlowDocument doc)
        {
            if (doc != null)
            {
                return XamlWriter.Save(doc);
            }
            else
            {
                return String.Empty;
            }
        }
    }
}