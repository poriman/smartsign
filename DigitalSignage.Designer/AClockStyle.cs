﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Windows.Markup;
using System.Xml;
using System.Xml.Linq;
using System.IO;

namespace WPFDesigner
{
    [Serializable]
    public class AClockStyle
    {
        public int templateID;
        string sHandFill, sHandStroke, mHandFill, mHandStroke, hHandFill, hHandStroke, centerFill, centerStroke, faceStrokesStroke, faceStrokeFill;
        double sHandStrokeThickness, mHandStrokeThickness, hHandStrokeThickness, centerStrokeThickness, faceStrokesStrokeThickness;

        public enum ClockElement
        {
            SecondsHand,
            Minuteshand,
            HourHand,
            CenterCircle,
            FaceStrokes
        }

        /// <summary>
        /// Initializes the new AClockStyle instance
        /// </summary>
        public AClockStyle()
        {
            hHandFill = XamlWriter.Save(new SolidColorBrush(Color.FromArgb(255, 255, 255, 255)));
            mHandFill = XamlWriter.Save(new SolidColorBrush(Color.FromArgb(255, 255, 255, 255)));
            sHandFill = XamlWriter.Save(new SolidColorBrush(Color.FromArgb(255, 255, 255, 255)));
            centerFill = XamlWriter.Save(new SolidColorBrush(Color.FromArgb(255, 255, 255, 255)));
            faceStrokeFill = XamlWriter.Save(new SolidColorBrush(Color.FromArgb(255, 255, 255, 255)));

            hHandStroke = XamlWriter.Save(new SolidColorBrush(Color.FromArgb(255, 0, 0, 0)));
            mHandStroke = XamlWriter.Save(new SolidColorBrush(Color.FromArgb(255, 0, 0, 0)));
            sHandStroke = XamlWriter.Save(new SolidColorBrush(Color.FromArgb(255, 0, 0, 0)));
            centerStroke = XamlWriter.Save(new SolidColorBrush(Color.FromArgb(255, 0, 0, 0)));
            faceStrokesStroke = XamlWriter.Save(new SolidColorBrush(Color.FromArgb(255, 255, 255, 255)));

            hHandStrokeThickness = sHandStrokeThickness = mHandStrokeThickness = centerStrokeThickness = faceStrokesStrokeThickness = 1.0;
        }

        /// <summary>
        /// Sets the fill of the specified ClockElement
        /// </summary>
        /// <param name="hand">A ClockElement enumeration value</param>
        /// <param name="handFill">A brush to fill the ClockElement with</param>
        public void SetFill(ClockElement element, Brush handFill)
        {
            switch (element)
            {
                case ClockElement.HourHand:
                    {
                        hHandFill = XamlWriter.Save(handFill);
                        break;
                    }
                case ClockElement.Minuteshand:
                    {
                        mHandFill = XamlWriter.Save(handFill);
                        break;
                    }
                case ClockElement.SecondsHand:
                    {
                        sHandFill = XamlWriter.Save(handFill);
                        break;
                    }
                case ClockElement.CenterCircle:
                    {
                        centerFill = XamlWriter.Save(handFill);
                        break;
                    }
                case ClockElement.FaceStrokes:
                    {
                        faceStrokeFill = XamlWriter.Save(handFill);
                        break;
                    }
                default: break;
            }
        }

        /// <summary>
        /// Sets the stroke of the specified ClockElement
        /// </summary>
        /// <param name="hand">A ClockElement enumeration value</param>
        /// <param name="handFill">A brush to set the ClockElement stroke with</param>
        public void SetStroke(ClockElement hand, Brush handStroke)
        {
            switch (hand)
            {
                case ClockElement.HourHand:
                    {
                        hHandStroke = XamlWriter.Save(handStroke);
                        break;
                    }
                case ClockElement.Minuteshand:
                    {
                        mHandStroke = XamlWriter.Save(handStroke);
                        break;
                    }
                case ClockElement.SecondsHand:
                    {
                        sHandStroke = XamlWriter.Save(handStroke);
                        break;
                    }
                case ClockElement.CenterCircle:
                    {
                        centerStroke = XamlWriter.Save(handStroke);
                        break;
                    }
                case ClockElement.FaceStrokes:
                    {
                        faceStrokesStroke = XamlWriter.Save(handStroke);
                        break;
                    }
                default: break;
            }
        }

        /// <summary>
        /// Sets the stroke thickness of the specified ClockElement
        /// </summary>
        /// <param name="hand">A ClockElement enumeration value</param>
        /// <param name="handFill">A new stroke thickness value of the ClockElement</param>
        public void SetStrokeThickness(ClockElement hand, double strokeThickness)
        {
            switch (hand)
            {
                case ClockElement.HourHand:
                    {
                        hHandStrokeThickness = strokeThickness;
                        break;
                    }
                case ClockElement.Minuteshand:
                    {
                        mHandStrokeThickness = strokeThickness;
                        break;
                    }
                case ClockElement.SecondsHand:
                    {
                        sHandStrokeThickness = strokeThickness;
                        break;
                    }
                case ClockElement.CenterCircle:
                    {
                        centerStrokeThickness = strokeThickness;
                        break;
                    }
                case ClockElement.FaceStrokes:
                    {
                        faceStrokesStrokeThickness = strokeThickness;
                        break;
                    }
                default: break;
            }
        }

        /// <summary>
        /// Returns the fill brush of the specified ClockElement
        /// </summary>
        /// <param name="hand">A ClockElement enumeration value</param>
        /// <returns>The ClockElement`s fill</returns>
        public Brush GetFill(ClockElement hand)
        {
            switch (hand)
            {
                case ClockElement.HourHand:
                    {
                        return GetBrushFromString(hHandFill);
                    }
                case ClockElement.Minuteshand:
                    {
                        return GetBrushFromString(mHandFill);
                    }
                case ClockElement.SecondsHand:
                    {
                        return GetBrushFromString(sHandFill);
                    }
                case ClockElement.CenterCircle:
                    {
                        return GetBrushFromString(centerFill);
                    }
                case ClockElement.FaceStrokes:
                    {
                        return GetBrushFromString(faceStrokeFill);
                    }
                default:
                    return new SolidColorBrush();
            }
        }

        /// <summary>
        /// Returns the stroke brush of the specified ClockElement
        /// </summary>
        /// <param name="hand">A ClockElement enumeration value</param>
        /// <returns>The ClockElement`s stroke brush</returns>
        public Brush GetStroke(ClockElement hand)
        {            
            switch (hand)
            {
                case ClockElement.HourHand:
                    {
                        return GetBrushFromString(hHandStroke);
                    }
                case ClockElement.Minuteshand:
                    {
                        return GetBrushFromString(mHandStroke);
                    }
                case ClockElement.SecondsHand:
                    {
                        return GetBrushFromString(sHandStroke);
                    }
                case ClockElement.CenterCircle:
                    {
                        return GetBrushFromString(centerStroke);
                    }
                case ClockElement.FaceStrokes:
                    {
                        return GetBrushFromString(faceStrokesStroke);
                    }
                default:
                    return new SolidColorBrush();
            }
        }

        /// <summary>
        /// Returns the stroke thickness of the specified ClockElement
        /// </summary>
        /// <param name="hand">A ClockElement enumeration value</param>
        /// <returns>The ClockElement`s stroke thickness</returns>
        public double GetStrokeThickness(ClockElement hand)
        {
            switch (hand)
            {
                case ClockElement.HourHand:
                    {
                        return hHandStrokeThickness;
                    }
                case ClockElement.Minuteshand:
                    {
                        return mHandStrokeThickness;
                    }
                case ClockElement.SecondsHand:
                    {
                        return sHandStrokeThickness;
                    }
                case ClockElement.CenterCircle:
                    {
                        return centerStrokeThickness;
                    }
                case ClockElement.FaceStrokes:
                    {
                        return faceStrokesStrokeThickness;
                    }
                default:
                    return 1.0;
            }
        }

        /// <summary>
        /// Updates the brushes of ClockElements style whether they are ImageBrushes
        /// </summary>
        /// <param name="mediaLocation">The new location of images used for ImageBrushes of current style</param>
        public void UpdateImageBrushes(string mediaLocation)
        {
            XElement brushDef;
            
            brushDef = XElement.Parse(hHandFill);
            if (brushDef.Name.LocalName == "ImageBrush")
            {
                brushDef.Attribute("ImageSource").Value = mediaLocation + System.IO.Path.GetFileName(brushDef.Attribute("ImageSource").Value);
            }
            hHandFill = brushDef.ToString();

            brushDef = XElement.Parse(mHandFill);
            if (brushDef.Name.LocalName == "ImageBrush")
            {
                brushDef.Attribute("ImageSource").Value = mediaLocation + System.IO.Path.GetFileName(brushDef.Attribute("ImageSource").Value);
            }
            mHandFill = brushDef.ToString();

            brushDef = XElement.Parse(sHandFill);
            if (brushDef.Name.LocalName == "ImageBrush")
            {
                brushDef.Attribute("ImageSource").Value = mediaLocation + System.IO.Path.GetFileName(brushDef.Attribute("ImageSource").Value);
            }
            sHandFill = brushDef.ToString();

            brushDef = XElement.Parse(centerFill);
            if (brushDef.Name.LocalName == "ImageBrush")
            {
                brushDef.Attribute("ImageSource").Value = mediaLocation + System.IO.Path.GetFileName(brushDef.Attribute("ImageSource").Value);
            }
            centerFill = brushDef.ToString();

            brushDef = XElement.Parse(faceStrokeFill);
            if (brushDef.Name.LocalName == "ImageBrush")
            {
                brushDef.Attribute("ImageSource").Value = mediaLocation + System.IO.Path.GetFileName(brushDef.Attribute("ImageSource").Value);
            }
            faceStrokeFill = brushDef.ToString();

            brushDef = XElement.Parse(hHandStroke);
            if (brushDef.Name.LocalName == "ImageBrush")
            {
                brushDef.Attribute("ImageSource").Value = mediaLocation + System.IO.Path.GetFileName(brushDef.Attribute("ImageSource").Value);
            }
            hHandStroke = brushDef.ToString();

            brushDef = XElement.Parse(mHandStroke);
            if (brushDef.Name.LocalName == "ImageBrush")
            {
                brushDef.Attribute("ImageSource").Value = mediaLocation + System.IO.Path.GetFileName(brushDef.Attribute("ImageSource").Value);
            }
            mHandStroke = brushDef.ToString();

            brushDef = XElement.Parse(sHandStroke);
            if (brushDef.Name.LocalName == "ImageBrush")
            {
                brushDef.Attribute("ImageSource").Value = mediaLocation + System.IO.Path.GetFileName(brushDef.Attribute("ImageSource").Value);
            }
            sHandStroke = brushDef.ToString();

            brushDef = XElement.Parse(centerStroke);
            if (brushDef.Name.LocalName == "ImageBrush")
            {
                brushDef.Attribute("ImageSource").Value = mediaLocation + System.IO.Path.GetFileName(brushDef.Attribute("ImageSource").Value);
            }
            centerStroke = brushDef.ToString();

            brushDef = XElement.Parse(faceStrokesStroke);
            if (brushDef.Name.LocalName == "ImageBrush")
            {
                brushDef.Attribute("ImageSource").Value = mediaLocation + System.IO.Path.GetFileName(brushDef.Attribute("ImageSource").Value);
            }
            faceStrokesStroke = brushDef.ToString();
        }

        public void ApplyStyle(System.Windows.Shapes.Path hHand, System.Windows.Shapes.Path mHand, System.Windows.Shapes.Path sHand, System.Windows.Shapes.Ellipse centerCircle, System.Windows.Shapes.Rectangle[] faceStrokes)
        {
            hHand.Fill = GetBrushFromString(hHandFill);
            hHand.Stroke = GetBrushFromString(hHandStroke);
            hHand.StrokeThickness = hHandStrokeThickness;

            mHand.Fill = GetBrushFromString(mHandFill);
            mHand.Stroke = GetBrushFromString(mHandStroke);
            mHand.StrokeThickness = mHandStrokeThickness;

            sHand.Fill = GetBrushFromString(sHandFill);
            sHand.Stroke = GetBrushFromString(sHandStroke);
            sHand.StrokeThickness = sHandStrokeThickness;

            centerCircle.Fill = GetBrushFromString(centerFill);
            centerCircle.Stroke = GetBrushFromString(centerStroke);
            centerCircle.StrokeThickness = centerStrokeThickness;

            foreach (System.Windows.Shapes.Rectangle rect in faceStrokes)
            {
                rect.Stroke = GetBrushFromString(faceStrokesStroke);
                rect.Fill = GetBrushFromString(faceStrokeFill);
                rect.StrokeThickness = faceStrokesStrokeThickness;
            }
        }

        Brush GetBrushFromString(string brushStringRepresentation)
        {
            StringReader sr = new StringReader(brushStringRepresentation);
            XmlReader xr = XmlReader.Create(sr);
            return (Brush)XamlReader.Load(xr);
        }
    }
}