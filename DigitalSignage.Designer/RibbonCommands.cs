﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Windows.Controls.Ribbon;
using System.Windows.Media;
using System.Windows;

namespace WPFDesigner
{
	public static class RibbonCommands
	{
		#region File Commands
		public static RibbonCommand New
		{
			get
			{
				RibbonCommand command = (RibbonCommand)Application.Current.Resources["rNew"];
				command.LabelTitle = Properties.Resources.menuFileNew;
				command.ToolTipTitle = Properties.Resources.menuFileNew;
// 				command.LargeImageSource = "images/AnalogueClockComponent.png";
				command.ToolTipDescription = Properties.Resources.descNewProject;
				return command;
			}
		}
		public static RibbonCommand Open
		{
			get
			{
				RibbonCommand command = (RibbonCommand)Application.Current.Resources["rOpen"];
				command.LabelTitle = Properties.Resources.menuFileOpen;
				command.ToolTipTitle = Properties.Resources.menuFileOpen;
				command.ToolTipDescription = Properties.Resources.descOpenProject;
				return command;
			}
		}
		public static RibbonCommand SaveGroup
		{
			get
			{
				RibbonCommand command = (RibbonCommand)Application.Current.Resources["rSave"];
				command.LabelTitle = Properties.Resources.menuFileSave;
				command.ToolTipTitle = Properties.Resources.menuFileSave;
				return command;
			}
		}

		public static RibbonCommand Save
		{
			get
			{
				RibbonCommand command = (RibbonCommand)Application.Current.Resources["rSave"];
				command.LabelTitle = Properties.Resources.menuFileSave;
				command.ToolTipTitle = Properties.Resources.menuFileSave;
				command.LabelDescription = command.ToolTipDescription = Properties.Resources.descSaveProject;
				return command;
			}
		}
		public static RibbonCommand SaveAs
		{
			get
			{
				RibbonCommand command = (RibbonCommand)Application.Current.Resources["rSaveAs"];
				command.LabelTitle = Properties.Resources.menuFileSaveAs;
				command.ToolTipTitle = Properties.Resources.menuFileSaveAs;
				command.LabelDescription = command.ToolTipDescription = Properties.Resources.descSaveAs;
				return command;
			}
		}
		public static RibbonCommand SaveAtMyDocument
		{
			get
			{
				RibbonCommand command = (RibbonCommand)Application.Current.Resources["rSaveAtMyDocument"];
				command.LabelTitle = Properties.Resources.menuFileSaveAtMyDocument;
				command.ToolTipTitle = Properties.Resources.menuFileSaveAtMyDocument;
				command.LabelDescription = command.ToolTipDescription = Properties.Resources.descSaveAtMyDocument;
				return command;
			}
		}
		public static RibbonCommand SaveAtDesktop
		{
			get
			{
				RibbonCommand command = (RibbonCommand)Application.Current.Resources["rSaveAtDesktop"];
				command.LabelTitle = Properties.Resources.menuFileSaveAtDesktop;
				command.ToolTipTitle = Properties.Resources.menuFileSaveAtDesktop;
				command.LabelDescription = command.ToolTipDescription = Properties.Resources.descSaveAtDesktop;
				return command;
			}
		}
		public static RibbonCommand Close
		{
			get
			{
				RibbonCommand command = (RibbonCommand)Application.Current.Resources["rClose"];
				command.LabelTitle = Properties.Resources.menuFileClose;
				command.ToolTipTitle = Properties.Resources.menuFileClose;
				command.ToolTipDescription = Properties.Resources.descCloseProject;
				return command;
			}
		}

		public static RibbonCommand Title
		{
			get
			{
				RibbonCommand command = (RibbonCommand)Application.Current.Resources["rTitle"];
				command.LabelTitle = Properties.Resources.menuFileQuit;
				command.ToolTipTitle = Properties.Resources.menuFileQuit;
				command.ToolTipDescription = Properties.Resources.descExit;
				return command;
			}
		}

		public static RibbonCommand ScreenProperties
		{
			get
			{
				RibbonCommand command = (RibbonCommand)Application.Current.Resources["rScreenProperties"];
				command.LabelTitle = Properties.Resources.menuScreenProperties;
				command.ToolTipTitle = Properties.Resources.menuScreenProperties;
				command.ToolTipDescription = Properties.Resources.descScreenProperties;
				return command;
			}
		}

		public static RibbonCommand Exit
		{
			get
			{
				RibbonCommand command = (RibbonCommand)Application.Current.Resources["rExit"];
				command.LabelTitle = Properties.Resources.menuFileQuit;
				command.ToolTipTitle = Properties.Resources.menuFileQuit;
				command.ToolTipDescription = Properties.Resources.descExit;
				return command;
			}
		}

		public static RibbonCommand ProjectGroup
		{
			get
			{
				RibbonCommand command = (RibbonCommand)Application.Current.Resources["rProjectGroup"];
				command.LabelTitle = Properties.Resources.menuGroupProject;
				command.ToolTipTitle = Properties.Resources.menuGroupProject;
				return command;
			}
		}
		#endregion

		#region Component Commands
		public static RibbonCommand GeneralGroup
		{
			get
			{
				RibbonCommand command = (RibbonCommand)Application.Current.Resources["rGeneralGroup"];
				command.LabelTitle = Properties.Resources.menuGroupGeneral;
				command.ToolTipTitle = Properties.Resources.menuGroupGeneral;
				return command;
			}
		}

		public static RibbonCommand ExpandGroup
		{
			get
			{
				RibbonCommand command = (RibbonCommand)Application.Current.Resources["rExpandGroup"];
				command.LabelTitle = Properties.Resources.menuGroupExpand;
				command.ToolTipTitle = Properties.Resources.menuGroupExpand;
				return command;
			}
		}

		public static RibbonCommand RealtimeGroup
		{
			get
			{
				RibbonCommand command = (RibbonCommand)Application.Current.Resources["rRealtimeGroup"];
				command.LabelTitle = Properties.Resources.menuGroupRealtime;
				command.ToolTipTitle = Properties.Resources.menuGroupRealtime;
				return command;
			}
		}
		public static RibbonCommand Rectangle
		{
			get
			{
				RibbonCommand command = (RibbonCommand)Application.Current.Resources["rRectangle"];
				command.LabelTitle = Properties.Resources.toolTipRectangle;
				command.ToolTipTitle = Properties.Resources.toolTipRectangle;
				command.ToolTipDescription = Properties.Resources.descRectangle;
				return command;
			}
		}

		public static RibbonCommand Ellipse
		{
			get
			{
				RibbonCommand command = (RibbonCommand)Application.Current.Resources["rEllipse"];
				command.LabelTitle = Properties.Resources.toolTipEllipse;
				command.ToolTipTitle = Properties.Resources.toolTipEllipse;
				command.ToolTipDescription = Properties.Resources.descEllipse;
				return command;
			}
		}

		public static RibbonCommand Text
		{
			get
			{
				RibbonCommand command = (RibbonCommand)Application.Current.Resources["rText"];
				command.LabelTitle = Properties.Resources.toolTipText;
				command.ToolTipTitle = Properties.Resources.toolTipText;
				command.ToolTipDescription = Properties.Resources.descText;
				return command;
			}
		}

		public static RibbonCommand ScrollText
		{
			get
			{
				RibbonCommand command = (RibbonCommand)Application.Current.Resources["rScrollText"];
				command.LabelTitle = Properties.Resources.toolTipScrollText;
				command.ToolTipTitle = Properties.Resources.toolTipScrollText;
				command.ToolTipDescription = Properties.Resources.descScrollText;
				return command;
			}
		}

		public static RibbonCommand Web
		{
			get
			{
				RibbonCommand command = (RibbonCommand)Application.Current.Resources["rWeb"];
				command.LabelTitle = Properties.Resources.toolTipWeb;
				command.ToolTipTitle = Properties.Resources.toolTipWeb;
				command.ToolTipDescription = Properties.Resources.descWeb;
				return command;
			}
		}
		public static RibbonCommand Weather
		{
			get
			{
				RibbonCommand command = (RibbonCommand)Application.Current.Resources["rWeather"];
				command.LabelTitle = Properties.Resources.toolTipWeather;
				command.ToolTipTitle = Properties.Resources.toolTipWeather;
				command.ToolTipDescription = Properties.Resources.descWeather;
				return command;
			}
		}
		public static RibbonCommand DigitalClock
		{
			get
			{
				RibbonCommand command = (RibbonCommand)Application.Current.Resources["rDigitalClock"];
				command.LabelTitle = Properties.Resources.toolTipDigitalClock;
				command.ToolTipTitle = Properties.Resources.toolTipDigitalClock;
				command.ToolTipDescription = Properties.Resources.descDigitalClock;
				return command;
			}
		}

		public static RibbonCommand AnalogueClock
		{
			get
			{
				RibbonCommand command = (RibbonCommand)Application.Current.Resources["rAnalogueClock"];
				command.LabelTitle = Properties.Resources.toolTipAnalogueClock;
				command.ToolTipTitle = Properties.Resources.toolTipAnalogueClock;
				command.ToolTipDescription = Properties.Resources.descAnalogueClock;
				return command;
			}
		}

		public static RibbonCommand Date
		{
			get
			{
				RibbonCommand command = (RibbonCommand)Application.Current.Resources["rDate"];
				command.LabelTitle = Properties.Resources.toolTipDate;
				command.ToolTipTitle = Properties.Resources.toolTipDate;
				command.ToolTipDescription = Properties.Resources.descDate;
				return command;
			}
		}

		public static RibbonCommand Media
		{
			get
			{
				RibbonCommand command = (RibbonCommand)Application.Current.Resources["rMedia"];
				command.LabelTitle = Properties.Resources.toolTipMedia;
				command.ToolTipTitle = Properties.Resources.toolTipMedia;
				command.ToolTipDescription = Properties.Resources.descMedia;
				return command;
			}
		}

		public static RibbonCommand Streaming
		{
			get
			{
				RibbonCommand command = (RibbonCommand)Application.Current.Resources["rStreaming"];
				command.LabelTitle = Properties.Resources.toolTipStreaming;
				command.ToolTipTitle = Properties.Resources.toolTipStreaming;
				command.ToolTipDescription = Properties.Resources.descStreaming;
				return command;
			}
		}

		public static RibbonCommand Image
		{
			get
			{
				RibbonCommand command = (RibbonCommand)Application.Current.Resources["rImage"];
				command.LabelTitle = Properties.Resources.toolTipImage;
				command.ToolTipTitle = Properties.Resources.toolTipImage;
				command.ToolTipDescription = Properties.Resources.descImage;
				return command;
			}
		}

		public static RibbonCommand Rss
		{
			get
			{
				RibbonCommand command = (RibbonCommand)Application.Current.Resources["rRss"];
				command.LabelTitle = Properties.Resources.toolTipRss;
				command.ToolTipTitle = Properties.Resources.toolTipRss;
				command.ToolTipDescription = Properties.Resources.descRss;
				return command;
			}
		}

		public static RibbonCommand QuickTime
		{
			get
			{
				RibbonCommand command = (RibbonCommand)Application.Current.Resources["rQuickTime"];
				command.LabelTitle = Properties.Resources.toolTipQuickTime;
				command.ToolTipTitle = Properties.Resources.toolTipQuickTime;
				command.ToolTipDescription = Properties.Resources.descQuickTime;
				return command;
			}
		}

		public static RibbonCommand Flash
		{
			get
			{
				RibbonCommand command = (RibbonCommand)Application.Current.Resources["rFlash"];
				command.LabelTitle = Properties.Resources.toolTipFlash;
				command.ToolTipTitle = Properties.Resources.toolTipFlash;
				command.ToolTipDescription = Properties.Resources.descFlash;
				return command;
			}
		}

		public static RibbonCommand TV
		{
			get
			{
				RibbonCommand command = (RibbonCommand)Application.Current.Resources["rTV"];
				command.LabelTitle = Properties.Resources.toolTipTV;
				command.ToolTipTitle = Properties.Resources.toolTipTV;
				command.ToolTipDescription = Properties.Resources.descTV;
				return command;
			}
		}
		public static RibbonCommand Audio
		{
			get
			{
				RibbonCommand command = (RibbonCommand)Application.Current.Resources["rAudio"];
				command.LabelTitle = Properties.Resources.toolTipAudio;
				command.ToolTipTitle = Properties.Resources.toolTipAudio;
				command.ToolTipDescription = Properties.Resources.descAudio;
				return command;
			}
		}

		public static RibbonCommand PPT
		{
			get
			{
				RibbonCommand command = (RibbonCommand)Application.Current.Resources["rPPT"];
				command.LabelTitle = Properties.Resources.toolTipPpt;
				command.ToolTipTitle = Properties.Resources.toolTipPpt;
				command.ToolTipDescription = Properties.Resources.descPpt;
				return command;
			}
		}
		#endregion

		#region View Commands

		public static RibbonCommand LanguageGroup
		{
			get
			{
				RibbonCommand command = (RibbonCommand)Application.Current.Resources["rLanguageGroup"];
				command.LabelTitle = Properties.Resources.menuGroupManagement;
				command.ToolTipTitle = Properties.Resources.menuGroupManagement;
				return command;
			}
		}

		public static RibbonCommand LanguageSelector
		{
			get
			{
				RibbonCommand command = (RibbonCommand)Application.Current.Resources["rLanguageSelector"];
				command.LabelTitle = Properties.Resources.menuitemLanguage;
				command.ToolTipTitle = Properties.Resources.menuitemLanguage;
				command.ToolTipDescription = Properties.Resources.descLanguage;
				return command;
			}
		}

		public static RibbonCommand About
		{
			get
			{
				RibbonCommand command = (RibbonCommand)Application.Current.Resources["rAbout"];
				command.LabelTitle = Properties.Resources.titleAbout;
				command.ToolTipTitle = Properties.Resources.titleAbout;
				command.ToolTipDescription = Properties.Resources.descAbout;
				return command;
			}
		}
		#endregion
	}
}
