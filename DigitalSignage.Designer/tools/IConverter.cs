﻿using System;
using System.Windows;
using System.Windows.Data;
using System.Globalization;
using System.Collections.ObjectModel;
using System.Windows.Media.Imaging;
using System.IO;

namespace WPFDesigner
{
    class DesignElementConvert : IValueConverter
    {
        #region IValueConverter 멤버

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                double dValue = (double)value;
				return double.Parse(dValue.ToString("F", CultureInfo.InvariantCulture));
// 				return dValue;
			}
            else
            {
                return 0;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }

        #endregion
    }

    class PositionConvert : IValueConverter
    {
        #region IValueConverter 멤버

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                IDesignElement elemnt = value as IDesignElement;
                string ret = string.Format("{0}({1}*{2})", elemnt.Name, elemnt.Width, elemnt.Height);
                return ret;
            }
            else
            {
                return 0;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }

        #endregion
    }

    class PlayImageConvert : IValueConverter
    {

        #region IValueConverter 멤버

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value != null)
            {
                bool isRun = (bool)value;
                string path = "";
                if (true == isRun)
                {
                    path = @"images/stop.png";
                }
                else
                {
					path = @"images/play.png";
                }

                return path;
            }
            else
            {
				return @"images/play.png";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }

        #endregion
    }
}
