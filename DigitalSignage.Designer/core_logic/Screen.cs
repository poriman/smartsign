﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Xml.Linq;
using System.IO;
using System.Windows.Media;
using System.Windows.Shapes;
using WPFDesigner.Cultures;
using System.Windows.Media.Effects;
using System.Diagnostics;
using DigitalSignage.Common;
using System.Windows.Media.Imaging;
using System.Configuration;
using NLog;
using System.Windows.Media.Animation;

namespace WPFDesigner
{
    /// <summary>
    /// Represents a pane that can be used as a design surface
    /// </summary>
    public class Screen : InkCanvas, IDesignElement, IMediaFilesManager
    {
        #region Declarations

        public static bool IsLayeredComponent
        {
            get
            {
                try
                {
                    return Convert.ToBoolean(ConfigurationManager.AppSettings["LayeredComponent"]);
                }
                catch { }

                return false;
            }
        }

		/// <summary>
        /// Represents the set of editable properties of design surface
        /// (can be retrieved when casting design surface to IDesignElement interface)
        /// </summary>
        private Dictionary<string, object> _properties;

		/// <summary>
		/// Represents the list of copied elements
		/// </summary>
		private List<Dictionary<string, object>> _copiedPropertiesSet;

		/// <summary>
		/// Represents the list of used media files paths
		/// </summary>
		public List<string> MediaFilesList;

        /// <summary>
        /// Indicates whether the Ctrl button is pressed
        /// </summary>
        private bool _isCtrlPressed;

        private bool _isShiftPressed;

        /// <summary>
        /// Indicates whether the screen is focused or not
        /// (to process such key presses as Delete f.e.)
        /// </summary>
        private bool _isFocused;

        /// <summary>
        /// Represents the static PlaylistEditor instance
        /// </summary>
        public static PlaylistEditor PlaylistEditor;

        /// <summary>
        /// Represents the control that manages the zooming of screen
        /// (is set through the Projectmanager.InitZoomControl(UIElement el) method)
        /// </summary>
        private UIElement _zoomControl;

        private TimeSpan _TotalPlayTime;

		private Point _aspectRatio;
		
        /// <summary>
        /// Is used to avoid moving of the element when closing the PlaylistEditor
        /// </summary>
        private bool _PlaylistEditorJustClosed;

        /// <summary>
        /// Represents the currently selected elements on the design surface
        /// </summary>
        private List<UIElement> _selection;

        /// <summary>
        /// Represents the current position of element that is dragged
        /// </summary>
        private Point _currentPosition;

        /// <summary>
        /// Represents the starting position of element that is dragged
        /// </summary>
        private Point _startPosition;

        /// <summary>
        /// Indicates that some element is dragging now
        /// (or the mouse is actually moving above the design surface)
        /// </summary>
        private bool _isDragMode;

        /// <summary>
        /// Represents the zoom transformation of the screen
        /// </summary>
        private ScaleTransform _zoomTransform;

        /// <summary>
        /// Represents the path to media files
        /// </summary>
        public string MediaPath;

        /// <summary>
        /// Indicates whether the screen design was changed
        /// </summary>
        public bool IsChanged;

        /// <summary>
        /// Used for naming of elements
        /// </summary>
        Dictionary<Type, int> _elementsCount;

        /// <summary>
        /// Used for automatic placement of elements when they are not 
        /// added with specified coordinates
        /// </summary>
        private double _lastTop, _lastLeft;

		/// <summary>
		/// 페이지 속성 열기 이벤크
		/// </summary>
		public event EventHandler OnOpenScreenProperties = null;
 
        /// <summary>
        /// Represents a context menu of the current Screen class instance
        /// </summary>
        private ContextMenu _screenContextMenu;

		/// <summary>
		/// Represents the aspect ratio of design surface
		/// </summary>
		public Point AspectRatio
		{
			get
			{
				return _aspectRatio;
			}
			set
			{
				_aspectRatio = value;
				RecalculateSize();
			}
		}

        /// <summary>
        /// Returns all the required information for saving the project (Pane)
        /// </summary>
        public List<Dictionary<string, object>> OverallProperties
        {
            get
            {
                List<Dictionary<string, object>> temp = new List<Dictionary<string, object>>();
                temp.Add(ProjectManager.SerializeProperties(((IDesignElement)this).Properties));
                foreach (UIElement element in Children.OfType<IDesignElement>().OrderByDescending(el => el.ZIndex) )
                {
                    temp.Add(ProjectManager.SerializeProperties(((IDesignElement)element).Properties));
                }
                return temp;
            }
            set
            {
                ((IDesignElement)this).Properties = ProjectManager.DeserializeProperties(value[0], ProjectManager.mediaPath);
                for (int i = 1; i < value.Count; i++)
                {
                    ((IDesignElement)Children[i]).Properties = ProjectManager.DeserializeProperties(value[i], ProjectManager.mediaPath);
                }
            }
        }

        private PropertiesViewer _propertiesViewer;

        /// <summary>
        /// Represents the properties viewer that displays selected element`s properties
        /// </summary>
        public PropertiesViewer propertiesViewer
        {
            set
            {
                _propertiesViewer = value;
                _propertiesViewer.Load(this);
            }
            get
            {
                return _propertiesViewer;
            }
        }

        public TimeSpan ToTalPlayTime
        {
            set { _TotalPlayTime = value; }
            get { return _TotalPlayTime; }
        }
        /// <summary>
        /// Gets of sets the ElementsListViewer of the screen instance
        /// </summary>
        public ElementsListViewer elementsListViewer
        {
            set
            {
                _elementsListViewer = value;
                _elementsListViewer._screen = this;
            }
            get
            {
                return _elementsListViewer;
            }
        }
        private ElementsListViewer _elementsListViewer;


        /// <summary>
        /// Gets the current zoom factor of the screen instance
        /// </summary>
        public double ZoomFactor
        {
            get
            {
                return _zoomAspect;
            }
        }

        /// <summary>
        /// represents the zoom aspect of the screen
        /// </summary>
        private double _zoomAspect;

        /// <summary>
        /// Represents the step of zoom-in/zoom-out operations
        /// </summary>
        private double _zoomStep;

        /// <summary>
        /// Represents the maximum zoom value
        /// </summary>
        private double _maxZoom;

        /// <summary>
        /// Represents the minimum zoom value
        /// </summary>
        private double _minZoom;

        private String _screenPath = String.Empty;
        public String ScreenPath
        {
            get { return _screenPath; }
            set
            {
                _screenPath = value;
            }
        }

        public bool IsSmilFile
        {
            get { return _screenPath.ToLower().EndsWith(".smil"); }
        }

        public bool IsBinFile
        {
            get { return _screenPath.ToLower().EndsWith(".bin"); }

        }

        /// <summary>
        /// Gets or sets the value that indicates whether the pane is in design mode
        /// or in the player mode
        /// </summary>
        public bool IsInDesginMode
        {
            get
            {
				//	재생 중이 라면 제어가 불가능해야한다.
				if (isPlay) return false;

                return _isInDesignMode;
            }
            set
            {
                _isInDesignMode = value;
                if (value)
                {
                    this.MoveEnabled = true;
                    
                    this.EditingMode = InkCanvasEditingMode.Select;
                    IsEnabled = true;
                    if (_propertiesViewer != null)
                    {
                        _propertiesViewer.IsEnabled = true;
                    }
                    if (_zoomControl != null)
                        _zoomControl.IsEnabled = true;
                    ContextMenu = _screenContextMenu;
                }
                else
                {
                    this.MoveEnabled = false;
                    this.EditingMode = InkCanvasEditingMode.None;
                    //IsEnabled = false;
                    if (_propertiesViewer != null)
                    {
                        _propertiesViewer.IsEnabled = false;
                    }
                    if (_zoomControl != null)
                        _zoomControl.IsEnabled = false;
                    ContextMenu = null;
                }
            }
        }
        private bool _isInDesignMode;

        /// <summary>
        /// Represents the context menu for elements
        /// </summary>
//        private ContextMenu _elementContextMenu;

        private Dictionary<Type, int> _copiesCountDictionaryByType;
        #endregion

        /// <summary>
        /// 로컬 로깅 Instance
        /// </summary>
        private static Logger logger = LogManager.GetCurrentClassLogger();

		/// <summary>
        /// Initializes new Screen class instance
        /// </summary>
        /// <param name="aspect"></param>
        public Screen(Point aspect)
            : base()
        {
            this.Unloaded += new RoutedEventHandler(Screen_Unloaded);

            _aspectRatio = aspect;

			InitializeScreen(this.IsInDesginMode);
        }

        void Screen_Unloaded(object sender, RoutedEventArgs e)
        {
            try
            {
                // OpenScreen : Sync 스케줄 이벤트 처리기 해제
                DigitalSignage.Common.OpenScreen.SyncScreenEventHelper.GetInstance.SyncScreenEvent -= new DigitalSignage.Common.OpenScreen.SyncScreenEventHandler(GetInstance_SyncScreenEvent);
            }
            catch { }

        }

		/// <summary>
		/// Initializes new Screen class instance
		/// </summary>
		/// <param name="width"></param>
		/// <param name="height"></param>
		/// <param name="isInDesignMode"></param>
        public Screen(int width, int height, bool isInDesignMode)
            : base()
        {
            this.Unloaded += new RoutedEventHandler(Screen_Unloaded);

            ((IDesignElement)this).Width = width;
            ((IDesignElement)this).Height = height;

			InitializeScreen(isInDesignMode);
			IsInDesginMode = isInDesignMode;
        }

        /// <summary>
        /// 소멸자
        /// </summary>
        ~Screen()
        {
            try
            {
                // OpenScreen : Sync 스케줄 이벤트 처리기 해제
                DigitalSignage.Common.OpenScreen.SyncScreenEventHelper.GetInstance.SyncScreenEvent -= new DigitalSignage.Common.OpenScreen.SyncScreenEventHandler(GetInstance_SyncScreenEvent);
            }
            catch { }
        }

		/// <summary>
		/// Initialize to zoom properties.
		/// </summary>
		void InitializeZoolAspect()
		{
			_zoomStep = 0.1;
			_maxZoom = 2.0;
			_minZoom = 0.1;
			_zoomAspect = 1.0;
			_zoomTransform = new ScaleTransform(_zoomAspect, _zoomAspect);
			_zoomTransform.CenterX = ActualWidth / 2;
			_zoomTransform.CenterY = ActualHeight / 2;
			//this.RenderTransform = _zoomTransform;

			this.LayoutTransform = _zoomTransform;
		}


		/// <summary>
		/// Screen Initialize
		/// </summary>
		/// <param name="isInDesignMode"></param>
		void InitializeScreen(bool isInDesignMode)
		{
            // OpenScreen : Sync 스케줄 이벤트 처리기 등록

            DigitalSignage.Common.OpenScreen.SyncScreenEventHelper.GetInstance.SyncScreenEvent += new DigitalSignage.Common.OpenScreen.SyncScreenEventHandler(GetInstance_SyncScreenEvent);
            

			this.ClipToBounds = true;

			_TotalPlayTime = new TimeSpan(0);

			_isCtrlPressed = false;
			_isShiftPressed = false;

			VerticalAlignment = VerticalAlignment.Center;
			HorizontalAlignment = HorizontalAlignment.Center;

			((IDesignElement)this).Name = this.GetType().Name;
			((IDesignElement)this).Background = new SolidColorBrush(Color.FromArgb(255, 255, 255, 255));

			_isFocused = true;

			this.Margin = new Thickness();
			_selection = new List<UIElement>();
			this.MoveEnabled = true;
			this.EditingMode = InkCanvasEditingMode.Select;
			DefaultDrawingAttributes.Color = Color.FromArgb(0, 0, 0, 0);

			_elementsCount = new Dictionary<Type, int>();
			_PlaylistEditorJustClosed = false;

			InitializeZoolAspect();

			if (isInDesignMode)
			{
				this.PreviewKeyDown += new System.Windows.Input.KeyEventHandler(MainWindow_PreviewKeyDown);
				this.PreviewKeyUp += new System.Windows.Input.KeyEventHandler(MainWindow_PreviewKeyUp);
				// 				this.PreviewKeyDown += new System.Windows.Input.KeyEventHandler(Pane_PreviewKeyDown);
				this.SelectionChanged += new EventHandler(Pane_SelectionChanged);
				this.SelectionResizing += new InkCanvasSelectionEditingEventHandler(Screen_SelectionResizing);
                this.SelectionResized += new EventHandler(Screen_SelectionResized);
                this.SelectionMoving += new InkCanvasSelectionEditingEventHandler(Screen_SelectionMoving);
                this.SelectionMoved += new EventHandler(Screen_SelectionMoved);
                this.PreviewMouseDown += new System.Windows.Input.MouseButtonEventHandler(Pane_PreviewMouseDown);
				this.PreviewMouseUp += new System.Windows.Input.MouseButtonEventHandler(Pane_PreviewMouseUp);
				this.PreviewMouseMove += new System.Windows.Input.MouseEventHandler(Pane_PreviewMouseMove);

				this.GotFocus += new RoutedEventHandler(Screen_GotFocus);
				this.LostFocus += new RoutedEventHandler(Screen_LostFocus);

				//                 Application.Current.MainWindow.PreviewKeyDown += new System.Windows.Input.KeyEventHandler(MainWindow_PreviewKeyDown);
				//                 Application.Current.MainWindow.PreviewKeyUp += new System.Windows.Input.KeyEventHandler(MainWindow_PreviewKeyUp);
				this.PreviewMouseWheel += new System.Windows.Input.MouseWheelEventHandler(Screen_PreviewMouseWheel);

				InitContextMenus();

				InitCopiesList();
			}

			this.SizeChanged += new SizeChangedEventHandler(this_SizeChanged);
		}

        void Screen_SelectionResized(object sender, EventArgs e)
        {
            try
            {
                if (GetSelectedElements().Count == 1)
                {
                    _propertiesViewer.Load(_selection[0]);
                }
            }
            catch { }
        }

        void Screen_SelectionMoved(object sender, EventArgs e)
        {
            try
            {
                if (GetSelectedElements().Count == 1)
                {
                    _propertiesViewer.Load(_selection[0]);
                }
            }
            catch {}
        }

        /// <summary>
        /// Initializes the Dictionary that containes counts of each design element type
        /// </summary>
        private void InitCopiesList()
        {
            _copiesCountDictionaryByType = new Dictionary<Type, int>();
            _copiesCountDictionaryByType.Add(typeof(EllipseComponent), 0);
            _copiesCountDictionaryByType.Add(typeof(DateComponent), 0);
            _copiesCountDictionaryByType.Add(typeof(AnalogueClockComponent), 0);
            _copiesCountDictionaryByType.Add(typeof(DigitalClockComponent), 0);
            _copiesCountDictionaryByType.Add(typeof(FlashComponent), 0);
            _copiesCountDictionaryByType.Add(typeof(ImageComponent), 0);
            _copiesCountDictionaryByType.Add(typeof(MediaComponent), 0);
            _copiesCountDictionaryByType.Add(typeof(PptComponent), 0);
            _copiesCountDictionaryByType.Add(typeof(QuickTimeComponent), 0);
            _copiesCountDictionaryByType.Add(typeof(RectangleComponent), 0);
            _copiesCountDictionaryByType.Add(typeof(RssComponent), 0);
            _copiesCountDictionaryByType.Add(typeof(ScrollTextComponent), 0);
            _copiesCountDictionaryByType.Add(typeof(TextComponent), 0);
            _copiesCountDictionaryByType.Add(typeof(WebComponent), 0);
			_copiesCountDictionaryByType.Add(typeof(WeatherComponent), 0);
			_copiesCountDictionaryByType.Add(typeof(AudioComponent), 0);
			_copiesCountDictionaryByType.Add(typeof(StreamingComponent), 0);
			_copiesCountDictionaryByType.Add(typeof(TVComponent), 0);
		}

        /// <summary>
        /// Inits the context menus for pane and elements
        /// </summary>
        private void InitContextMenus()
        {
			_screenContextMenu = InitScreenContextMenu();
        }

		private ContextMenu InitScreenContextMenu()
		{
			XElement generalMenuDef;
			ContextMenu contextmenu = null;

			string pathToContextMenusXMLFile = AppDomain.CurrentDomain.BaseDirectory + @"settings\contextmenus.xml";
			if (!File.Exists(pathToContextMenusXMLFile))
			{
				MessageBox.Show(@"Cannot find the file \settings\ContextMenus.xml. No context menus will be loaded.", "Designer", MessageBoxButton.OK, MessageBoxImage.Warning);
				pathToContextMenusXMLFile = "";
				return contextmenu;
			}

			try
			{
				generalMenuDef = XElement.Parse(File.ReadAllText(pathToContextMenusXMLFile));
			}
			catch
			{
				MessageBox.Show(@"Cannot load context menus. The file \settings\COntextMenus.xml is no in the correct format.", "Designer", MessageBoxButton.OK, MessageBoxImage.Warning);
				pathToContextMenusXMLFile = "";
				return contextmenu;
			}
			contextmenu = new ContextMenu();

			XElement paneMenu = generalMenuDef.Descendants().Single(el => el.Name == "Screen");
			foreach (XElement paneMenuItem in paneMenu.Elements())
			{
				contextmenu.Items.Add(CreateMenuItemFromDef(paneMenuItem));
			}

			return contextmenu;
		}

		public ContextMenu InitElementContextMenu()
		{
			XElement generalMenuDef;
			ContextMenu contextmenu = null;

			string pathToContextMenusXMLFile = AppDomain.CurrentDomain.BaseDirectory + @"settings\contextmenus.xml";
			if (!File.Exists(pathToContextMenusXMLFile))
			{
				MessageBox.Show(@"Cannot find the file \settings\ContextMenus.xml. No context menus will be loaded.", "Designer", MessageBoxButton.OK, MessageBoxImage.Warning);
				pathToContextMenusXMLFile = "";
				return contextmenu;
			}

			try
			{
				generalMenuDef = XElement.Parse(File.ReadAllText(pathToContextMenusXMLFile));
			}
			catch
			{
				MessageBox.Show(@"Cannot load context menus. The file \settings\COntextMenus.xml is no in the correct format.", "Designer", MessageBoxButton.OK, MessageBoxImage.Warning);
				pathToContextMenusXMLFile = "";
				return contextmenu;
			}
			contextmenu = new ContextMenu();

			XElement elementsMenu = generalMenuDef.Descendants().Single(el => el.Name == "Element");
			foreach (XElement elementMenuItem in elementsMenu.Elements()/*elementsMenu.Descendants()*/)
			{
				contextmenu.Items.Add(CreateMenuItemFromDef(elementMenuItem));
			}

			return contextmenu;
		}

		private System.Windows.Controls.Image GetIconImage(String source)
		{
			try
			{
				System.Windows.Controls.Image iconf = new System.Windows.Controls.Image();

				BitmapImage _bi = new BitmapImage();

				
				_bi.BeginInit();
				_bi.UriSource = new Uri("pack://application:,,,/" + source);
				_bi.EndInit();

				iconf.Source = _bi;

				iconf.Width = 16;
				iconf.Height = 16;

				return iconf;
			}
			catch (Exception e) 
			{
				MessageBox.Show(e.Message);
			}
			return null;
		}
        /// <summary>
        /// Creates the context MenuItem class instance based on the XML definition
        /// </summary>
        /// <param name="def">The XML definition of context menu item</param>
        /// <returns>The new MenuItem class instance</returns>
        private Control CreateMenuItemFromDef(XElement def)
        {
			Control menuitem = null;
			
			if (def.Name == "Separator")
			{
				menuitem = new Separator();
			}
			else
			{
				menuitem = new MenuItem();
				Binding headerBinding = new Binding();
				headerBinding.Source = CultureResources.ResourceProvider;
				headerBinding.Path = new PropertyPath(def.Attribute("ResourcesKey").Value);
				headerBinding.Mode = BindingMode.OneWay;
				menuitem.SetBinding(MenuItem.HeaderProperty, headerBinding);

				try
				{
					String source = def.Attribute("Icon").Value;
					if(!String.IsNullOrEmpty(source))
					{

						((MenuItem)menuitem).Icon = GetIconImage(source);
					}
				}
				catch
				{}

				menuitem.Name = def.Attribute("Name").Value;
				menuitem.PreviewMouseLeftButtonUp += new System.Windows.Input.MouseButtonEventHandler(menuitem_PreviewMouseLeftButtonUp);
			}

			foreach (XElement elementMenuItem in def.Elements())
			{
				((MenuItem)menuitem).Items.Add(CreateMenuItemFromDef(elementMenuItem));
			}
			return menuitem;
        }


        /// <summary>
        /// Indicates whether element`s center is inside the lasso selection
        /// </summary>
        /// <param name="el">UIElement to check</param>
        /// <param name="selectionStart">A point that represents the selection start (left-upper corner)</param>
        /// <param name="selectionEnd">A point that represents the selection end (right-bottom corner)</param>
        /// <returns>True if element`s center is contatianed within selection, otherwise returns false</returns>
        private bool IsElementInSelection(UIElement el, Point selectionStart, Point selectionEnd)
        {
            Point elementCenter = new Point();
            elementCenter.X = InkCanvas.GetLeft(el) + ((FrameworkElement)el).Width / 2;
            elementCenter.Y = InkCanvas.GetTop(el) + ((FrameworkElement)el).Height / 2;

            if (elementCenter.X > selectionStart.X && elementCenter.Y > selectionStart.Y && elementCenter.X < selectionEnd.X && elementCenter.Y < selectionEnd.Y)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Adds the specified element to the pane
        /// </summary>
        /// <param name="newElement">Element to add</param>
		public IDesignElement Add(UIElement newElement)
        {
			if (!IsInDesginMode) return newElement as IDesignElement;

            //this.Children.Add(newElement);
            //SignElementEvents(newElement);

			AppendContextmenu(newElement as FrameworkElement);
			
			return newElement as IDesignElement;
        }

        /// <summary>
        /// Adds a new element to the pane at the specified point
        /// </summary>
        /// <param name="newElement">Element to add</param>
        /// <param name="position">The point with X and Y coordinates at which the new element must ne placed</param>
		public IDesignElement Add(UIElement newElement, Point position)
        {
            this.Children.Add(newElement);
            Canvas.SetTop(newElement, position.Y);
            Canvas.SetLeft(newElement, position.X);
            SignElementEvents(newElement);

			if (!IsInDesginMode) return newElement as IDesignElement;

			Binding toolTipToNameBinding = new Binding();
			toolTipToNameBinding.Source = (UIElement)newElement;
			toolTipToNameBinding.Path = new PropertyPath("Name");
			toolTipToNameBinding.Mode = BindingMode.OneWay;
			((FrameworkElement)newElement).SetBinding(FrameworkElement.ToolTipProperty, toolTipToNameBinding);
			//	hsshin

			IDesignElement element = newElement as IDesignElement;
			AppendContextmenu(newElement as FrameworkElement);
			CheckElementLayout(element);

			if (_elementsListViewer != null)
			{
				element.ZIndex = ElementsListViewer.MaxZIndex + 1;
				_elementsListViewer.Add(newElement as UIElement);
			}

			return element as IDesignElement;
		}

		public string TypeToString(Type elementType)
		{
			string sType;

			if (elementType.Equals(typeof(EllipseComponent)))
				sType = Properties.Resources.toolTipEllipse;
			else if (elementType.Equals(typeof(AudioComponent)))
				sType = Properties.Resources.toolTipAudio;
			else if (elementType.Equals(typeof(DateComponent)))
				sType = Properties.Resources.toolTipDate;
			else if (elementType.Equals(typeof(AnalogueClockComponent)))
				sType = Properties.Resources.toolTipAnalogueClock;
			else if (elementType.Equals(typeof(DigitalClockComponent)))
				sType = Properties.Resources.toolTipDigitalClock;
			else if (elementType.Equals(typeof(FlashComponent)))
				sType = Properties.Resources.toolTipFlash;
			else if (elementType.Equals(typeof(ImageComponent)))
				sType = Properties.Resources.toolTipImage;
			else if (elementType.Equals(typeof(MediaComponent)))
				sType = Properties.Resources.toolTipMedia;
			else if (elementType.Equals(typeof(PptComponent)))
				sType = Properties.Resources.toolTipPpt;
			else if (elementType.Equals(typeof(QuickTimeComponent)))
				sType = Properties.Resources.toolTipQuickTime;
			else if (elementType.Equals(typeof(RectangleComponent)))
				sType = Properties.Resources.toolTipRectangle;
			else if (elementType.Equals(typeof(RssComponent)))
				sType = Properties.Resources.toolTipRss;
			else if (elementType.Equals(typeof(ScrollTextComponent)))
				sType = Properties.Resources.toolTipScrollText;
			else if (elementType.Equals(typeof(TextComponent)))
				sType = Properties.Resources.toolTipText;
			else if (elementType.Equals(typeof(WebComponent)))
				sType = Properties.Resources.toolTipWeb;
			else if (elementType.Equals(typeof(WeatherComponent)))
				sType = Properties.Resources.toolTipWeather;
			else if (elementType.Equals(typeof(StreamingComponent)))
				sType = Properties.Resources.toolTipStreaming;
			else if (elementType.Equals(typeof(TVComponent)))
				sType = Properties.Resources.toolTipTV;
			else
				sType = "Unknown";
			sType = sType.Replace(' ', '_');
			sType = sType.Replace("\r\n", "_");
			return  sType;
		}


		public IDesignElement Add(Type elementType, int zIndex)
		{
			IDesignElement element = DefineIDesignElementFromType(elementType);

			element.Left = _lastLeft;

			if (_lastLeft + 150 < Width)
				_lastLeft += 150;
			else
			{
				_lastLeft = 0;
				element.Left = 0;
				if (_lastTop + 150 < Height)
					_lastTop += 150;
				else _lastTop = 0;
			}

			element.Top = _lastTop;

			element.HorizontalAlignment = HorizontalAlignment.Stretch;
			element.VerticalAlignment = VerticalAlignment.Stretch;

			if (!_elementsCount.Keys.Contains(element.GetType()))
			{
				element.Name = TypeToString(element.GetType()) + (1).ToString();
				_elementsCount.Add(element.GetType(), 2);
			}
			else
			{
				element.Name = TypeToString(element.GetType()) + _elementsCount[element.GetType()].ToString();
				_elementsCount[element.GetType()]++;
			}

			Children.Add(element as UIElement);
			SignElementEvents(element as UIElement);

			if (IsInDesginMode)
			{
				Binding toolTipToNameBinding = new Binding();
				toolTipToNameBinding.Source = (UIElement)element;
				toolTipToNameBinding.Path = new PropertyPath("Name");
				toolTipToNameBinding.Mode = BindingMode.OneWay;
				((FrameworkElement)element).SetBinding(FrameworkElement.ToolTipProperty, toolTipToNameBinding);
				//	hsshin
				AppendContextmenu(element as FrameworkElement);
			}

			CheckElementLayout(element);

			if (_elementsListViewer != null)
			{
				element.ZIndex = ElementsListViewer.MaxZIndex + 1;
				_elementsListViewer.Add(element as UIElement);
			}

			return element;
		}

        /// <summary>
        /// Adds the new element based on the provided type 
        /// </summary>
        /// <param name="elementType">The type of element to add</param>
		public IDesignElement Add(Type elementType)
        {
			return Add(elementType, 0);
        }

        /// <summary>
        /// Adds the new element based on the provided type 
        /// </summary>
        /// <param name="elementType">The type of element to add</param>
		public IDesignElement Add(Type elementType, Point position)
        {
            IDesignElement element = DefineIDesignElementFromType(elementType);

            element.Left = position.X - 75;
            element.Top = position.Y - 75;

			element.HorizontalAlignment = HorizontalAlignment.Stretch;
			element.VerticalAlignment = VerticalAlignment.Stretch;

			if (!_elementsCount.Keys.Contains(element.GetType()))
			{
				element.Name = TypeToString(element.GetType()) + (1).ToString();
				_elementsCount.Add(element.GetType(), 2);
			}
			else
			{
				element.Name = TypeToString(element.GetType()) + _elementsCount[element.GetType()].ToString();
				_elementsCount[element.GetType()]++;
			}

            Children.Add(element as UIElement);
            SignElementEvents(element as UIElement);

            CheckElementLayout(element);

            if (_elementsListViewer != null)
            {
				element.ZIndex = ElementsListViewer.MaxZIndex + 1;
				_elementsListViewer.Add(element as UIElement);
            }

			if (IsInDesginMode)
			{
				Binding toolTipToNameBinding = new Binding();
				toolTipToNameBinding.Source = (UIElement)element;
				toolTipToNameBinding.Path = new PropertyPath("Name");
				toolTipToNameBinding.Mode = BindingMode.OneWay;
				((FrameworkElement)element).SetBinding(FrameworkElement.ToolTipProperty, toolTipToNameBinding);

				AppendContextmenu(element as FrameworkElement);
			}

			return element;
		}

        /// <summary>
        /// Gets the element by given type
        /// </summary>
        /// <param name="elementType">The Type</param>
        /// <returns>The element defined by given Type</returns>
        private IDesignElement DefineIDesignElementFromType(Type elementType)
        {
            #region Defining the element

			bool bUseWPF = false;
			try
			{
				bUseWPF = Convert.ToBoolean(ConfigurationManager.AppSettings["UseWPF"]);
			}
			catch { bUseWPF = false; }

            switch (elementType.Name)
            {
                case "AnalogueClockComponent":
                    {
                        AnalogueClockComponent acc = new AnalogueClockComponent();
                        return acc as IDesignElement;
                    }
                case "DateComponent":
                    {
                        DateComponent dc = new DateComponent();
                        return dc as IDesignElement;
                    }
                case "DigitalClockComponent":
                    {
                        DigitalClockComponent dc = new DigitalClockComponent();
                        return dc as IDesignElement;
                    }
                case "EllipseComponent":
                    {
                        EllipseComponent ec = new EllipseComponent();
                        return ec as IDesignElement;
                    }
                case "ImageComponent":
                    {
                        ImageComponent ic = new ImageComponent();
                        return ic as IDesignElement;
                    }
                case "MediaComponent":
                    {
                        MediaComponent mc = new MediaComponent(bUseWPF);
                        return mc as IDesignElement;
                    }                
				case "StreamingComponent":
                    {
                        StreamingComponent sc = new StreamingComponent(bUseWPF);
                        return sc as IDesignElement;
                    }
                case "RectangleComponent":
                    {
                        RectangleComponent rc = new RectangleComponent();
                        return rc as IDesignElement;
                    }
                case "RssComponent":
                    {
                        RssComponent rssc = new RssComponent();
                        return rssc as IDesignElement;
                    }
                case "ScrollTextComponent":
                    {
                        ScrollTextComponent stc = new ScrollTextComponent();
                        return stc as IDesignElement;
                    }
                case "TextComponent":
                    {
                        TextComponent tc = new TextComponent();
                        return tc as IDesignElement;
                    }
                case "WebComponent":
                    {
                        WebComponent wc = new WebComponent();
                        return wc as IDesignElement;
                    }
				case "WeatherComponent":
					{
						WeatherComponent wc = new WeatherComponent();
						return wc as IDesignElement;
					}
                case "FlashComponent":
                    {
                        FlashComponent fc;
                        try
                        {
                            fc = new FlashComponent();
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Cannot create the Flash component. Reason: " + ex.Message + ". Check the Adobe Flash Player installation.", "Designer");
                            return null as IDesignElement;
                        }
                        return fc as IDesignElement;
                    }
				case "TVComponent":
					{
						TVComponent tc;
						try
						{
							tc = new TVComponent();
						}
						catch (Exception ex)
						{
							MessageBox.Show("Cannot create the TV component. Reason: " + ex.Message + ". Check your tv tuner card.", "Designer");
							return null as IDesignElement;
						}
						return tc as IDesignElement;
					}
                case "QuickTimeComponent":
                    {
                        QuickTimeComponent qc;
                        try
                        {
                            qc = new QuickTimeComponent();
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Cannot create the QuickTime component. Reason: " + ex.Message + ". Check the QuickTime Alternative 2.60 installation.", "Designer");
                            return null as IDesignElement;
                        }
                        return qc as IDesignElement;
                    }
                case "AudioComponent":
                    {
                        AudioComponent ac = new AudioComponent();
                        return ac as IDesignElement;
                    }
                case "PptComponent":
                    {
                        PptComponent ppt;
                        try
                        {
                            ppt = new PptComponent();
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show("Cannot create the PowerPoint component. Reason: " + ex.Message + ". Check the EDraw Office Viewer installation.", "Designer");
                            return null as IDesignElement;
                        }
                        return ppt as IDesignElement;
                    }
                default:
                    {
                        AnalogueClockComponent acc = new AnalogueClockComponent();
                        return acc as IDesignElement;
                    }
            }
            #endregion
        }

        /// <summary>
        /// Sets the corresponding context menu to the specified element
        /// </summary>
        /// <param name="element">Element to add the context menu to</param>
        private void AppendContextmenu(FrameworkElement element)
        {
			if(IsInDesginMode)
			{
				// 			element.ContextMenu = _elementContextMenu;
				element.ContextMenu = InitElementContextMenu();
			}
		}

        #region Signing/Unsigning elements` event handlers

        /// <summary>
        /// Signs all neccessary event handlers of specified element
        /// </summary>
        /// <param name="el">The UIElement to sign events of</param>
        private void SignElementEvents(UIElement el)
        {
            ((Control)el).PreviewMouseDoubleClick += new System.Windows.Input.MouseButtonEventHandler(element_PreviewMouseDoubleClick);
            el.PreviewMouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(element_PreviewMouseLeftButtonDown);
            el.PreviewMouseLeftButtonUp += new System.Windows.Input.MouseButtonEventHandler(element_PreviewMouseLeftButtonUp);
		}

        /// <summary>
        /// Unsigns all element`s event handlers
        /// </summary>
        /// <param name="el">The UIElement to unsign events of</param>
        private void UnsignElementEvents(UIElement el)
        {
            ((Control)el).PreviewMouseDoubleClick -= new System.Windows.Input.MouseButtonEventHandler(element_PreviewMouseDoubleClick);
            el.PreviewMouseLeftButtonDown -= new System.Windows.Input.MouseButtonEventHandler(element_PreviewMouseLeftButtonDown);
            el.PreviewMouseLeftButtonUp -= new System.Windows.Input.MouseButtonEventHandler(element_PreviewMouseLeftButtonUp);
        }
        #endregion

        /// <summary>
        /// Removes the specified child
        /// </summary>
		/// <param name="elementToRemove">The child to remove</param>
        public void Remove(UIElement elementToRemove)
        {
            _selection.Clear();
            _selection.Add(elementToRemove);
            Remove();
            if (_elementsListViewer != null)
            {
                _elementsListViewer.Remove(elementToRemove);
            }
        }

        /// <summary>
        /// Moves the selection by 1px in the direction
        /// specified by providing the arrow key pressed
        /// </summary>
        /// <param name="key"></param>
        private void SizeByArrowKey(System.Windows.Input.Key key)
        {
            switch (key)
            {
                case System.Windows.Input.Key.Left:
                    {
                        foreach (UIElement el in _selection)
                        {
                            ((IDesignElement)el).Width -= 1.0;
                        }
                        break;
                    }
                case System.Windows.Input.Key.Right:
                    {
                        foreach (UIElement el in _selection)
                        {
                            ((IDesignElement)el).Width += 1.0;
                        }
                        break;
                    }
                case System.Windows.Input.Key.Up:
                    {
                        foreach (UIElement el in _selection)
                        {
                            ((IDesignElement)el).Height -= 1.0;
                        }
                        break;
                    }
                case System.Windows.Input.Key.Down:
                    {
                        foreach (UIElement el in _selection)
                        {
                            ((IDesignElement)el).Height += 1.0;
                        }
                        break;
                    }
                default: break;
            }
        }

        /// <summary>
        /// Moves the selection by 1px in the direction
        /// specified by providing the arrow key pressed
        /// </summary>
        /// <param name="key"></param>
        private void MoveSelectionByArrowKey(System.Windows.Input.Key key)
        {
            switch (key)
            {
                case System.Windows.Input.Key.Left:
                    {
                        foreach (UIElement el in _selection)
                        {
                            ((IDesignElement)el).Left -= 1.0;
                        }
                        break;
                    }
                case System.Windows.Input.Key.Right:
                    {
                        foreach (UIElement el in _selection)
                        {
                            ((IDesignElement)el).Left += 1.0;
                        }
                        break;
                    }
                case System.Windows.Input.Key.Up:
                    {
                        foreach (UIElement el in _selection)
                        {
                            ((IDesignElement)el).Top -= 1.0;
                        }
                        break;
                    }
                case System.Windows.Input.Key.Down:
                    {
                        foreach (UIElement el in _selection)
                        {
                            ((IDesignElement)el).Top += 1.0;
                        }
                        break;
                    }
                default: break;
            }
        }

        /// <summary>
        /// Checks the element position and if part of it is located 
        /// beyond the sreen bound then moves it to the nearest bound
        /// </summary>
        /// <param name="element">Element to check/correct layout</param>
        private void CheckElementLayout(IDesignElement element)
        {
            UpdateLayout();
            if (element.Top > Height - 10)
            {
                element.Top = Height - element.Height;
            }

            if (element.Left > Width - 10)
            {
                element.Left = Width - element.Width;
            }

            if (element.Left < 0 && element.Left < -element.Width + 10)
            {
                element.Left = 0;
            }

            if (element.Top < 0 && element.Top < -element.Height + 10)
            {
                element.Top = 0;
            }
        }

        /// <summary>
        /// Removes the selected elements
        /// </summary>
        public void Remove()
        {
            foreach (UIElement el in _selection)
            {
                if (_elementsListViewer != null)
                {
                    _elementsListViewer.Remove(el);
                }
                UnsignElementEvents(el);
                Children.Remove(el);
            }
            _selection.Clear();
            _propertiesViewer.Load(this);
        }

        /// <summary>
        /// Resizes the pane according to its parent`s size change
        /// </summary>
        private void RecalculateSize()
        {
            if (Parent == null) return;
            if (_aspectRatio == null || _aspectRatio == new Point()) return;
            FrameworkElement parent = Parent as FrameworkElement;

            double maxWidth = parent.ActualWidth;
            double maxHeight = parent.ActualHeight;

            if (_aspectRatio.X > _aspectRatio.Y)
            {
                this.Width = parent.ActualWidth;
                this.Height = this.Width / _aspectRatio.X * _aspectRatio.Y;

                if (this.Width > maxWidth)
                {
                    this.Width = maxWidth;
                    this.Height = this.Width / _aspectRatio.X * _aspectRatio.Y;
                }
                if (this.Height > maxHeight)
                {
                    this.Height = maxHeight;
                    this.Width = this.Height / _aspectRatio.Y * _aspectRatio.X;
                }
            }
            else
            {
                this.Height = parent.ActualHeight;
                this.Width = this.Height / _aspectRatio.Y * _aspectRatio.X;

                if (this.Width > maxWidth)
                {
                    this.Width = maxWidth;
                    this.Height = this.Width / _aspectRatio.X * _aspectRatio.Y;
                }
                if (this.Height > maxHeight)
                {
                    this.Height = maxHeight;
                    this.Width = this.Height / _aspectRatio.Y * _aspectRatio.X;
                }
            }
        }

        /// <summary>
        /// Removes the specified element from the screen`s inner selection collection
        /// </summary>
        /// <param name="el">Element to remove</param>
        public void RemoveFromSelection(UIElement el)
        {
            if (_selection.Contains(el))
            {
                _selection.Remove(el);
            }
        }

        /// <summary>
        /// Rearranges all containing elements ragarding to their alignment options and size changes of pane
        /// </summary>
        private void RearrangeElements(SizeChangedEventArgs e)
        {
            double widthDelta = e.NewSize.Width - e.PreviousSize.Width;
            double heightDelta = e.NewSize.Height - e.PreviousSize.Height;

			double widthRatio = e.PreviousSize.Width == 0 ? 1.0 : e.NewSize.Width / e.PreviousSize.Width;
			double heightRatio = e.PreviousSize.Height == 0 ? 1.0 : e.NewSize.Height / e.PreviousSize.Height;
			if (widthDelta == ActualWidth || heightDelta == ActualHeight) return;
            foreach (UIElement el in this.Children)
            {
                IDesignElement child = el as IDesignElement;

                if (e.WidthChanged)
                {
                    switch (((IDesignElement)el).HorizontalAlignment)
                    {
                        case HorizontalAlignment.Left:
                            {
                                break;
                            }
                        case HorizontalAlignment.Right:
                            {
                                child.Left += widthDelta;
                                break;
                            }
                        case HorizontalAlignment.Stretch:
                            {
								
// 								//	hsshin
								child.Left *= widthRatio;
								child.Width *= widthRatio;

//	origin
//                                 if (widthDelta < 0)
//                                 {
//                                     if (child.Width >= Math.Abs(widthDelta))
//                                         child.Width += widthDelta;
//                                     else child.Width = 0;
//                                 }
//                                 else
//                                 {
//                                     child.Width += widthDelta;
//                                 }
                                break;
                            }
                        case HorizontalAlignment.Center:
                            {
                                child.Left += widthDelta / 2;
                                break;
                            }
                        default: break;
                    }
                }

                if (e.HeightChanged)
                {
                    switch (((IDesignElement)el).VerticalAlignment)
                    {
                        case VerticalAlignment.Top:
                            {
                                break;
                            }
                        case VerticalAlignment.Bottom:
                            {
                                child.Top += heightDelta;
                                break;
                            }
                        case VerticalAlignment.Stretch:
                            {
// 								//	hsshin
								child.Top *= heightRatio;
								child.Height *= heightRatio;
//	origin
//                                 if (heightDelta < 0)
//                                 {
//                                     if (child.Height >= Math.Abs(heightDelta))
//                                         child.Height += heightDelta;
//                                     else child.Height = 0;
//                                 }
//                                 else
//                                 {
//                                     child.Height += heightDelta;
//                                 }

                                break;
                            }
                        case VerticalAlignment.Center:
                            {
                                child.Top += heightDelta / 2;
                                break;
                            }
                        default: break;
                    }
                }
            }
        }

        /// <summary>
        /// Releases all resources used by this Screen class instance
        /// </summary>
        public void Close()
        {
            if (_elementsListViewer != null)
                _elementsListViewer.Clear();
            foreach (UIElement el in Children)
            {
                UnsignElementEvents(el);
//                 if (!_isInDesignMode)
//                 {
					try
					{
						((IDesignElement)el).Stop();
					}
					catch { }
//                 }
            }
            Children.Clear();
            if (_zoomControl != null)
            {
                switch (_zoomControl.GetType().Name)
                {
                    case "Slider":
                        {
                            ((Slider)_zoomControl).ValueChanged -= new RoutedPropertyChangedEventHandler<double>(Screen_ValueChanged);
                            break;
                        }
                    case "ComboBox":
                        {
                            ((ComboBox)_zoomControl).SelectionChanged -= new SelectionChangedEventHandler(Screen_SelectionChanged);
                            break;
                        }
                    default: break;
                }
                _zoomControl.IsEnabled = false;
                _zoomControl = null;
            }
            this.Background = null;
            try
            {
                this.SizeChanged -= new SizeChangedEventHandler(this_SizeChanged);
                if (IsInDesginMode)
                {
					this.PreviewKeyDown += new System.Windows.Input.KeyEventHandler(MainWindow_PreviewKeyDown);
					this.PreviewKeyUp += new System.Windows.Input.KeyEventHandler(MainWindow_PreviewKeyUp);
// 					this.PreviewKeyDown -= new System.Windows.Input.KeyEventHandler(Pane_PreviewKeyDown);
                    this.SelectionChanged -= new EventHandler(Pane_SelectionChanged);
					this.SelectionResizing -= new InkCanvasSelectionEditingEventHandler(Screen_SelectionResizing);
                    this.SelectionResized -= new EventHandler(Screen_SelectionResized);
                    this.SelectionMoving -= new InkCanvasSelectionEditingEventHandler(Screen_SelectionMoving);
                    this.SelectionMoved -= new EventHandler(Screen_SelectionMoved);
                    this.PreviewMouseDown -= new System.Windows.Input.MouseButtonEventHandler(Pane_PreviewMouseDown);
                    this.PreviewMouseUp -= new System.Windows.Input.MouseButtonEventHandler(Pane_PreviewMouseUp);
                    this.PreviewMouseMove -= new System.Windows.Input.MouseEventHandler(Pane_PreviewMouseMove);

                    this.GotFocus -= new RoutedEventHandler(Screen_GotFocus);
                    this.LostFocus -= new RoutedEventHandler(Screen_LostFocus);

//                     Application.Current.MainWindow.PreviewKeyDown -= new System.Windows.Input.KeyEventHandler(MainWindow_PreviewKeyDown);
//                     Application.Current.MainWindow.PreviewKeyUp -= new System.Windows.Input.KeyEventHandler(MainWindow_PreviewKeyUp);
                    this.PreviewMouseWheel -= new System.Windows.Input.MouseWheelEventHandler(Screen_PreviewMouseWheel);
                }
            }
            catch { }
        }

        /// <summary>
        /// Copies the selected elements
        /// </summary>
        private void Copy()
        {
			try
			{
				_copiedPropertiesSet = new List<Dictionary<string, object>>();
				foreach (UIElement el in _selection)
				{
					IDesignElement ide = el as IDesignElement;
					_copiedPropertiesSet.Add(ide.Properties);
				}
			}
			catch (Exception e)
			{
				MessageBox.Show(e.Message);
			}
		}

        /// <summary>
        /// Pastes the copied elements
        /// </summary>
        private new void Paste()
        {
			try
			{
				if (_copiedPropertiesSet == null) return;
				int startCount = Children.Count;
				int count = Children.Count;

				foreach (Dictionary<string, object> propertiesSet in _copiedPropertiesSet)
				{
					try
					{
						propertiesSet["Left"] = (double)propertiesSet["Left"] + 20.0;
						propertiesSet["Top"] = (double)propertiesSet["Top"] + 20.0;
					}
					catch
					{
						//	오디오 컴포넌트는 위 속성이 없다.
					}

					Add(propertiesSet["Type"] as Type, ElementsListViewer.MaxZIndex + 1);
					IDesignElement child = ((IDesignElement)Children[count]);

					child.Properties = propertiesSet;

					if (child.Playlist != null)
					{
						List<PlaylistItem> tempPlaylist = new List<PlaylistItem>();
						foreach (PlaylistItem item in child.Playlist)
						{
							tempPlaylist.Add(item);
						}

						child.Playlist = tempPlaylist;
						//	hsshin 클리어를 하면 재생목록이 카피되지않는다.
// 						tempPlaylist.Clear();
					}
					int tempIndex = 0, tempCopyNumberStartIndex = child.Name.Length;

					while (Int32.TryParse(child.Name[tempCopyNumberStartIndex - 1].ToString(), out tempIndex))
					{
						tempCopyNumberStartIndex--;
					}
					try
					{
						if (child.Name.Substring(tempCopyNumberStartIndex - 4, 4).ToLower() == "copy")
						{
							child.Name = child.Name.Substring(0, tempCopyNumberStartIndex) + _copiesCountDictionaryByType[child.Type].ToString();
						}
						else
						{
							child.Name = child.Name.Substring(0, tempCopyNumberStartIndex) + "Copy" + _copiesCountDictionaryByType[child.Type].ToString();
						}
					}
					catch
					{
						child.Name = child.Name.Substring(0, tempCopyNumberStartIndex) + "Copy" + _copiesCountDictionaryByType[child.Type].ToString();
					}

					_copiesCountDictionaryByType[child.Type]++;
					count++;
				}

				_selection.Clear();

				for (int i = startCount; i < count; i++)
				{
					_selection.Add(Children[i]);
				}

				Select(_selection.ToArray());
				Copy();
			}
			catch (Exception e)
			{
				MessageBox.Show(e.Message);
			}
        }

		public void Unselect()
		{
			_selection.Clear();
			Select(_selection.ToArray());
		}

        /// <summary>
        /// Forces the Screen to fill its parent control with self (the parent MUST BE a Grid).
        /// </summary>
        public void FillParent()
        {
            if (Parent == null && Parent.GetType() != typeof(Grid))
            {
                return;
            }
            Grid parentGrid = Parent as Grid;

            double parentInitialWidth = parentGrid.ActualWidth;
            double parentInitialHeight = parentGrid.ActualHeight;
            parentGrid.Width = ((IDesignElement)this).Width;
            parentGrid.Height = ((IDesignElement)this).Height;
            parentGrid.UpdateLayout();
            HorizontalAlignment = HorizontalAlignment.Stretch;
            VerticalAlignment = VerticalAlignment.Stretch;
            Width = Double.NaN;
            Height = Double.NaN;
			MinWidth = Math.Round(parentInitialWidth);
			MinHeight = Math.Round(parentInitialHeight);
			parentGrid.Width = parentInitialWidth;
			parentGrid.Height = parentInitialHeight;
			UpdateLayout();
       }

        /// <summary>
        /// Gets the maximum playlist length
        /// </summary>
        /// <returns></returns>
        public TimeSpan GetMaxPlaylistLength()
        {
            TimeSpan duration = new TimeSpan();
            foreach (UIElement el in Children)
            {
                IDesignElement ide_ = el as IDesignElement;
                TimeSpan duration_ = PlaylistPlayer.GetTotalPlayingTime(ide_.Playlist);
                if (duration.TotalSeconds < duration_.TotalSeconds)
                {
                    duration = duration_;
                }
            }
            return duration;
        }

        #region Screen scaling maintaining
        public void InitZoomControl(UIElement el)
        {
            switch (el.GetType().Name)
            {
                case "Slider":
                    {
                        Slider slider = el as Slider;
                        slider.Minimum = _minZoom;
                        slider.Maximum = _maxZoom;
                        slider.SmallChange = _zoomStep;
                        slider.Value = _zoomAspect;
                        slider.LargeChange = _zoomStep;
                        _zoomControl = slider;
                        ((Slider)_zoomControl).ValueChanged += new RoutedPropertyChangedEventHandler<double>(Screen_ValueChanged);
                        break;
                    }
                case "ComboBox":
                    {
                        ComboBox cb = el as ComboBox;
                        for (double i = _minZoom; i < _maxZoom; i += _zoomStep)
                        {
                            cb.Items.Add(i);
                        }
                        _zoomControl = cb;
                        ((ComboBox)_zoomControl).SelectionChanged += new SelectionChangedEventHandler(Screen_SelectionChanged);
                        break;
                    }
                default: break;
            }
            el.IsEnabled = true;
        }

        void Screen_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ComboBox cb = sender as ComboBox;
            ZoomTo((double)cb.SelectedValue);
        }

        void Screen_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            ZoomTo(((Slider)sender).Value);
			e.Handled = true;
        }

        /// <summary>
        /// Scales the screen by _zoomStep
        /// </summary>
        public void ZoomIn()
        {
            _zoomAspect += _zoomStep;
            if (_zoomAspect > _maxZoom)
                _zoomAspect = _maxZoom;

            _zoomTransform.ScaleX = _zoomAspect;
            _zoomTransform.ScaleY = _zoomAspect;

            switch (_zoomControl.GetType().Name)
            {
                case "Slider":
                    {
                        ((Slider)_zoomControl).ValueChanged -= new RoutedPropertyChangedEventHandler<double>(Screen_ValueChanged);
                        ((Slider)_zoomControl).Value = _zoomAspect;
                        ((Slider)_zoomControl).ValueChanged += new RoutedPropertyChangedEventHandler<double>(Screen_ValueChanged);
                        break;
                    }
                case "ComboBox":
                    {
                        ((ComboBox)_zoomControl).SelectedItem = _zoomAspect;
                        break;
                    }
                default: break;
            }
        }

        /// <summary>
        /// Scales the screen by _zoomStep
        /// </summary>
        public void ZoomOut()
        {
            _zoomAspect -= _zoomStep;
            if (_zoomAspect < _minZoom)
                _zoomAspect = _minZoom;

            _zoomTransform.ScaleX = _zoomAspect;
            _zoomTransform.ScaleY = _zoomAspect;

            switch (_zoomControl.GetType().Name)
            {
                case "Slider":
                    {
                        ((Slider)_zoomControl).ValueChanged -= new RoutedPropertyChangedEventHandler<double>(Screen_ValueChanged);
                        ((Slider)_zoomControl).Value = _zoomAspect;
                        ((Slider)_zoomControl).ValueChanged += new RoutedPropertyChangedEventHandler<double>(Screen_ValueChanged);
                        break;
                    }
                case "ComboBox":
                    {
                        ((ComboBox)_zoomControl).SelectedItem = _zoomAspect;
                        break;
                    }
                default: break;
            }
        }

        public void ZoomTo(double zoomValue)
        {
            _zoomAspect = zoomValue;
            _zoomTransform.ScaleX = zoomValue;
            _zoomTransform.ScaleY = zoomValue;
        }

        /// <summary>
        /// Resets the screen scale to 1.0
        /// </summary>
        public void ResetZoom()
        {
            _zoomAspect = 1.0;
            _zoomTransform.ScaleX = _zoomAspect;
            _zoomTransform.ScaleY = _zoomAspect;
        }
        #endregion

        #region Context menu items event handler
        void menuitem_PreviewMouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            MenuItem mi = sender as MenuItem;
            switch (mi.Name.ToLower())
            {
                case "selectall":
                    {
                        _selection.Clear();
                        foreach (UIElement el in Children)
                        {
                            _selection.Add(el);
                        }
                        Select(_selection.ToArray());
                        break;
                    }
                case "clear":
                    {
                        if (!this.IsPlay)
                        {
                            if (MessageBox.Show(Properties.Resources.mb_RemoveAllComponent, Properties.Resources.titleDesignerWindow, MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                            {
                                _elementsListViewer.Clear();
                                Children.Clear();
                                _selection.Clear();
                            }
                        }
                        break;
                    }
                case "alignleft":
                    {
                        foreach (UIElement el in _selection)
                        {
                            ((IDesignElement)el).Left = 0;
//                             ((IDesignElement)el).HorizontalAlignment = HorizontalAlignment.Left;
                        }
                        break;
                    }
                case "aligncenter":
                    {
                        foreach (UIElement el in _selection)
                        {
                            ((IDesignElement)el).Left = Width / 2 - ((IDesignElement)el).Width / 2;
//                             ((IDesignElement)el).HorizontalAlignment = HorizontalAlignment.Center;
                        }
                        break;
                    }
                case "alignright":
                    {
                        foreach (UIElement el in _selection)
                        {
                            ((IDesignElement)el).Left = Width - ((IDesignElement)el).Width;
//                             ((IDesignElement)el).HorizontalAlignment = HorizontalAlignment.Right;
                        }
                        break;
                    }
                case "aligntop":
                    {
                        foreach (UIElement el in _selection)
                        {
                            ((IDesignElement)el).Top = 0;
//                             ((IDesignElement)el).VerticalAlignment = VerticalAlignment.Top;
                        }
                        break;
                    }
				case "alignvcenter":
					{
						foreach (UIElement el in _selection)
						{
							((IDesignElement)el).Top = Height / 2 - ((IDesignElement)el).Height / 2;
// 							((IDesignElement)el).VerticalAlignment = VerticalAlignment.Center;
						}
						break;
					}
                case "alignbottom":
                    {
                        foreach (UIElement el in _selection)
                        {
                            ((IDesignElement)el).Top = Height - ((IDesignElement)el).Height;
//                             ((IDesignElement)el).VerticalAlignment = VerticalAlignment.Bottom;
                        }
                        break;
                    }
                case "increasezindex":
                    {
                        foreach (UIElement el in _selection)
                        {
							this.elementsListViewer.IncreaseZIndex(el);
//                             ((IDesignElement)el).ZIndex += 1;
                        }
                        break;
                    }
                case "decreasezindex":
                    {
                        foreach (UIElement el in _selection)
                        {
							this.elementsListViewer.DecreaseZIndex(el);
// 							((IDesignElement)el).ZIndex -= 1;
                        }
                        break;
                    }
                case "remove":
                    {
                        if (_selection == null || _selection.Count == 0) return;

                        if (!this.IsPlay)
                        {
                            try
                            {
                                foreach (IDesignElement de in _selection)
                                {
                                    if (de.ISPlay) return;
                                }
                            }
                            catch
                            {
                            }

                            if (MessageBox.Show(Properties.Resources.mb_RemoveComponent, Properties.Resources.titleDesignerWindow, MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                            {
                                foreach (UIElement el in _selection)
                                {
                                    Children.Remove(el);

                                    if (_elementsListViewer != null)
                                    {
                                        _elementsListViewer.Remove(el);
                                    }
                                }
                                _selection = GetSelectedElements().ToList();
                                _propertiesViewer.Load(this);
                            }
                        }
                        break;
                    }
                case "fixelementslayout":
                    {
                        foreach (UIElement el in Children)
                        {
                            CheckElementLayout((IDesignElement)el);
                        }
                        break;
                    }
				case "preview":
					{
						if (_selection.Count() == 1)
						{
							IDesignElement el = _selection[0] as IDesignElement;
							if(el.ISPlay)
							{
                                _elementsListViewer.PlayStatus(_selection.ToArray(), false);
// 								FrameworkElement frmel = _selection[0] as FrameworkElement;
// 								((MenuItem)sender).Sou
								el.Stop();
							}
							else
							{
                                _elementsListViewer.PlayStatus(_selection.ToArray(), true);
								el.Play();
							}
                            
						}
						break;
					}
				case "properties":
					{
						if (_selection.Count() == 1)
						{
							element_PreviewMouseDoubleClick((object)_selection[0], null);
						}
					}
					break;
				case "zoom50":
					{
						Slider sc = _zoomControl as Slider;
						if(sc != null)
						{
							sc.Value = 0.5;
							ZoomTo(0.5);
						}
					}
					break;
				case "zoom75":
					{
						Slider sc = _zoomControl as Slider;
						if (sc != null)
						{
							sc.Value = 0.75;
							ZoomTo(0.75);
						}
					}
					break;
				case "zoom100":
					{
						Slider sc = _zoomControl as Slider;
						if (sc != null)
						{
							sc.Value = 1.0;
							ZoomTo(1.0);
						}
					}
					break;
				case "zoom200":
					{
						Slider sc = _zoomControl as Slider;
						if (sc != null)
						{
							sc.Value = 2.0;
							ZoomTo(2.0);
						}
					}
					break;
                case "fitwidth":
                    {
                        foreach (UIElement el in _selection)
                        {
                            ((IDesignElement)el).Left = 0;
                            ((IDesignElement)el).Width = ((IDesignElement)this).Width;
                        }
                        break;
                    }
                case "fitheight":
                    {
                        foreach (UIElement el in _selection)
                        {
                            ((IDesignElement)el).Top = 0;
                            ((IDesignElement)el).Height = ((IDesignElement)this).Height;
                        }
                        break;
                    }
                case "fitboth":
                    {
                        foreach (UIElement el in _selection)
                        {
                            ((IDesignElement)el).Top = 0;
                            ((IDesignElement)el).Left = 0;
                            ((IDesignElement)el).Width = ((IDesignElement)this).Width;
                            ((IDesignElement)el).Height = ((IDesignElement)this).Height;
                        }
                        break;
                    }
				case "screenproperties":
					{
						if (OnOpenScreenProperties != null) OnOpenScreenProperties(this, e);
						break;
					}
				default: break;
            }
        }
        #endregion

        #region Screen event handlers

        void Screen_SelectionResizing(object sender, InkCanvasSelectionEditingEventArgs e)
        {
	        if (IsInDesginMode)
	        {
		        Rect newRect = e.NewRectangle;
		        if (newRect.Width > this.Width)
			        newRect.Width = this.Width;

		        if (newRect.Height > this.Height)
			        newRect.Height = this.Height;

		        if (newRect.Left < 0)
		        {
			        newRect.X = 0;
			        newRect.Width = e.OldRectangle.X + e.OldRectangle.Width;
		        }
		        if (newRect.Top < 0)
		        {
			        newRect.Y = 0;
			        newRect.Height = e.OldRectangle.Y + e.OldRectangle.Height;
		        }

		        if (newRect.Right > this.ActualWidth)
		        {
			        newRect.Width = this.ActualWidth - newRect.X;
		        }
		        if (newRect.Bottom > this.ActualHeight)
		        {
			        newRect.Height = this.ActualHeight - newRect.Y;
		        }

		        e.NewRectangle = newRect;
	        }
        }

        void Screen_SelectionMoving(object sender, InkCanvasSelectionEditingEventArgs e)
        {
	        if(IsInDesginMode)
	        {
		        Rect newRect = e.NewRectangle;
		        if (newRect.Width > this.Width)
			        newRect.Width = this.Width;

		        if (newRect.Height > this.Height)
			        newRect.Height = this.Height;

		        if (newRect.Left < 0)
			        newRect.X = 0;

		        if (newRect.Top < 0)
			        newRect.Y = 0;

		        if (newRect.Right > this.ActualWidth)
			        newRect.X = this.ActualWidth - newRect.Width;

		        if (newRect.Bottom > this.ActualHeight)
			        newRect.Y = this.ActualHeight - newRect.Height;

		        e.NewRectangle = newRect;
	        }
        }

        private void Pane_SelectionChanged(object sender, EventArgs e)
        {
            if (_isCtrlPressed)
            {
                SelectionChanged -= new EventHandler(Pane_SelectionChanged);
                foreach (UIElement el in GetSelectedElements())
                {
                    if (!_selection.Contains(el))
                    {
                        _selection.Add(el);
                    }
                }
                _propertiesViewer.Load(this);
                Select(_selection.ToArray());
                SelectionChanged += new EventHandler(Pane_SelectionChanged);
            }
            else if (!_isCtrlPressed)
            {
                _selection.Clear();

                if (GetSelectedElements().Count == 1)
                {
                    _selection.Add(GetSelectedElements()[0]);
                    _propertiesViewer.Load(_selection[0]);
                }
                else if (GetSelectedElements().Count == 0)
                {
                    _propertiesViewer.Load(this);
                }
                else if (GetSelectedElements().Count > 1)
                {
                    foreach (UIElement el in GetSelectedElements())
                    {
                        if (!_selection.Contains(el))
                            _selection.Add(el);
                    }
                    _propertiesViewer.Load(this);
                }
            }

            if (_elementsListViewer != null)
            {
                _elementsListViewer.Select(_selection.ToArray());
            }
        }

        private void Pane_PreviewMouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
			
	        if (IsInDesginMode && e.RightButton == System.Windows.Input.MouseButtonState.Pressed)
	        {
		        if(_selection.Count > 1)
		        {
			        //	하나 이상이 선택된 경우 마우스 포지션에 해당하는 선택 하나만 다시 설정
			        foreach(UIElement el in _selection)
			        {
				        Rect rect = new Rect(((IDesignElement)el).Left, ((IDesignElement)el).Top, ((IDesignElement)el).Width, ((IDesignElement)el).Height);
				        if(rect.Contains(e.GetPosition(this)))
				        {
					        _selection.Clear();
					        _selection.Add(el);
					        _propertiesViewer.Load(_selection[0]);
					        Select(_selection.ToArray());

					        return;
				        }
			        }

			        _selection.Clear();
		        }
		        else
		        {
			        //	하나를 선택했을경우 마우스 포인트가 포함될경우 리턴
			        if(_selection.Count == 1)
			        {
				        UIElement el = _selection[0];

				        Rect rect = new Rect(((IDesignElement)el).Left, ((IDesignElement)el).Top, ((IDesignElement)el).Width, ((IDesignElement)el).Height);
				        if (!rect.Contains(e.GetPosition(this)))
				        {
					        _selection.Clear();
				        }
				        else
				        {
					        return;
				        }

			        }
		        }

		        //	포지션이 선택 영역에 없는 경우 이리로 오며 현재 스크린의 모든 Element들을 검사하여 포함되는 경우 선택을 해 준다.
		        foreach (UIElement el in Children.OfType<IDesignElement>().OrderByDescending(el => el.ZIndex))
		        {
			        Rect rect = new Rect(((IDesignElement)el).Left, ((IDesignElement)el).Top, ((IDesignElement)el).Width, ((IDesignElement)el).Height);
			        if (rect.Contains(e.GetPosition(this)))
			        {
				        _selection.Add(el);
				        _propertiesViewer.Load(_selection[0]);
				        Select(_selection.ToArray());
				        break;
			        }
		        }
		        e.Handled = true;
	        }
	        else if(IsInDesginMode && e.LeftButton == System.Windows.Input.MouseButtonState.Pressed)
	        {
		        e.Handled = false;
	        }

        }

        Point _capturedPoint = new Point(0,0);
        private void Pane_PreviewMouseMove(object sender, System.Windows.Input.MouseEventArgs e)
        {
        // 			base.OnPreviewMouseMove(e);
	        if (IsInDesginMode && _isDragMode && _selection.Count() == 1)
	        {

		        UIElement selectedElement = _selection[0] as UIElement;
		        IDesignElement selectDesign = _selection[0] as IDesignElement;
		        _currentPosition = (Point)(e.GetPosition(this));

		        if (selectDesign.Width > this.Width)
			        selectDesign.Width = this.Width;

		        if (selectDesign.Height > this.Height)
			        selectDesign.Height = this.Height;

		        double el_left = _currentPosition.X - _capturedPoint.X;
		        double el_top = _currentPosition.Y - _capturedPoint.Y;
		        double el_right = el_left + selectDesign.Width;
		        double el_bottom = el_top + selectDesign.Height;

		        //	가로 움직임
		        if (el_left < 0)
		        {
			        InkCanvas.SetLeft(selectedElement, 0);
		        }
		        else if (el_right > this.Width)
		        {
			        InkCanvas.SetLeft(selectedElement, this.Width - selectDesign.Width);
		        }
		        else
			        InkCanvas.SetLeft(selectedElement, el_left);

		        //	세로 움직임
		        if (el_top < 0)
		        {
			        InkCanvas.SetTop(selectedElement, 0);
		        }
		        else if (el_bottom > this.Height)
		        {
			        InkCanvas.SetTop(selectedElement, this.Height - selectDesign.Height);
		        }
		        else
			        InkCanvas.SetTop(selectedElement, el_top);


		        return;
	        }

        // 			if (_isDragMode && _selection.Count() == 1)
        //             {
        //                 UIElement selectedElement = _selection[0] as UIElement;
        //                 IDesignElement selectDesign = _selection[0] as IDesignElement;
        //                 _currentPosition = (Point)(e.GetPosition(this) - _startPosition);
        // 
        // 				if (selectDesign.Width > this.Width)
        // 					selectDesign.Width = this.Width;
        // 
        // 				if (selectDesign.Height > this.Height)
        // 					selectDesign.Height = this.Height;
        // 
        // 				Debug.WriteLine(string.Format("Left {0}, Right {1}, Top {2}, Bottm {3}", InkCanvas.GetTop(selectedElement) + _currentPosition.Y,
        //                 InkCanvas.GetLeft(selectedElement) + selectDesign.Width, InkCanvas.GetTop(selectedElement) + _currentPosition.Y, InkCanvas.GetTop(selectedElement) + selectDesign.Height));
        // 
        // 
        //                 if ((InkCanvas.GetLeft(selectedElement) + _currentPosition.X) < 0)
        //                 {
        //                     InkCanvas.SetLeft(selectedElement, 0);
        //                 }
        //                 else if ((InkCanvas.GetTop(selectedElement) + _currentPosition.Y) < 0)
        //                 {
        //                     InkCanvas.SetTop(selectedElement, 0);
        //                 }
        //                 else if ((InkCanvas.GetLeft(selectedElement) + selectDesign.Width) > this.Width )
        //                 {
        //                     InkCanvas.SetLeft(selectedElement, this.Width - selectDesign.Width);
        //                 }
        //                 else if ((InkCanvas.GetTop(selectedElement) + selectDesign.Height) > this.Height)
        //                 {
        //                     InkCanvas.SetTop(selectedElement, this.Height - selectDesign.Height);
        //                 }
        //                 else
        //                 {
        //                     InkCanvas.SetLeft(selectedElement, InkCanvas.GetLeft(selectedElement) + _currentPosition.X);
        //                     InkCanvas.SetTop(selectedElement, InkCanvas.GetTop(selectedElement) + _currentPosition.Y);
        //                 }
        // 
        //                 _startPosition = e.GetPosition(this);
        // 				e.Handled = true;
        //                 return;
        //             }
        //             if (_lassoMode && GetSelectedElements().Count < 2)
        //             {
        //                 //DrawLasso(e);
        //             }
        }

        private void Pane_PreviewMouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            _isDragMode = false;
            
            //DeleteLasso();
        }

        private void Pane_PreviewKeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.Delete)
            {
                if (_selection == null || _selection.Count == 0) return;

                if (!this.IsPlay)
                {
                    try
                    {
                        foreach (IDesignElement de in _selection)
                        {
                            if (de.ISPlay) return;
                        }
                    }
                    catch
                    {
                    }
                    if (MessageBox.Show(Properties.Resources.mb_RemoveComponent, Properties.Resources.titleDesignerWindow, MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                    {
                        Remove();
                    }
                }
		        e.Handled = true;
            }
        }

        private void this_SizeChanged(object sender, SizeChangedEventArgs e)
        {
	        this.UpdateLayout();
            if (Parent != null)
            {
                RecalculateSize();
            }
            RearrangeElements(e);
	        e.Handled = true;
        }

        private void Screen_PreviewMouseWheel(object sender, System.Windows.Input.MouseWheelEventArgs e)
        {
            if (!_isCtrlPressed) return;
            if (e.Delta > 0)
            {
                ZoomIn();
            }
            else
            {
                ZoomOut();
            }
        }

        #region Determining whether the Screen got focus or lost it
        void Screen_LostFocus(object sender, RoutedEventArgs e)
        {
            _isFocused = false;
        }

        void Screen_GotFocus(object sender, RoutedEventArgs e)
        {
            _isFocused = true;
        }
        #endregion

        #region Determining whether Ctrl is pressed or not (uses application main window) and processing of other pressed keys
        void MainWindow_PreviewKeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.LeftCtrl)
            {
                _isCtrlPressed = true;
            }
            if (e.Key == System.Windows.Input.Key.LeftShift || e.Key == System.Windows.Input.Key.RightShift)
            {
                _isShiftPressed = true;
            }
            if (e.Key == System.Windows.Input.Key.Delete)
            {
                if (_isFocused)
                {
			        e.Handled = true;
                    if (_selection == null || _selection.Count == 0) return;

                    if (!this.IsPlay)
                    {
                        try
                        {
                            foreach (IDesignElement de in _selection)
                            {
                                if (de.ISPlay) return;
                            }
                        }
                        catch
                        {
                        }

                        if (MessageBox.Show(Properties.Resources.mb_RemoveComponent, Properties.Resources.titleDesignerWindow, MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                        {
                            Remove();
                        }
                    }
                }
            }
            if (e.Key == System.Windows.Input.Key.C && _isCtrlPressed)
            {
		        if (_isFocused)
		        {
			        Copy();
			        e.Handled = true;
		        }
            }
            if (e.Key == System.Windows.Input.Key.V && _isCtrlPressed)
            {
		        if (_isFocused)
		        {
			        Paste();
			        e.Handled = true;
		        }
            }

            if (_isShiftPressed && (e.Key == System.Windows.Input.Key.Left ||
                e.Key == System.Windows.Input.Key.Right ||
                e.Key == System.Windows.Input.Key.Up ||
                e.Key == System.Windows.Input.Key.Down))
            {
                if (_isFocused)
                {
                    SizeByArrowKey(e.Key);
			        e.Handled = true;
		        }
            }
            else if (e.Key == System.Windows.Input.Key.Left ||
                e.Key == System.Windows.Input.Key.Right ||
                e.Key == System.Windows.Input.Key.Up ||
                e.Key == System.Windows.Input.Key.Down)
            {
                if (_isFocused)
                {
                    MoveSelectionByArrowKey(e.Key);
			        e.Handled = true;
		        }
            }
        }

        void MainWindow_PreviewKeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == System.Windows.Input.Key.LeftCtrl)
            {
                _isCtrlPressed = false;
            }

            if (e.Key == System.Windows.Input.Key.LeftShift || e.Key == System.Windows.Input.Key.RightShift)
            {
                _isShiftPressed = false;
            }
        }
        #endregion
        #endregion

        #region Elements event handlers
        private void element_PreviewMouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            try
            {
				if (!IsInDesginMode) return;

				if (this.IsPlay)
				{
					MessageBox.Show(Properties.Resources.mbErrorModifyPlaying, Properties.Resources.titleDesignerWindow, MessageBoxButton.OK, MessageBoxImage.Error);
					return;
				} 
				



                UIElement selection = sender as UIElement;
                _selection.Clear();
                _selection.Add(selection);
                if (((IDesignElement)selection).Playlist == null) return;

                if (PlaylistEditor != null)
                {
                    try
                    {
                        PlaylistEditor.Close();
                    }
                    catch { }
                    PlaylistEditor = null;
                }

                PlaylistEditor = new PlaylistEditor();

                PlaylistEditor.ShowEditor(selection as IDesignElement);
                ((IDesignElement)selection).Playlist = PlaylistEditor.GetPlaylist();

                _selection.Clear();
                
                _PlaylistEditorJustClosed = true;
                PlaylistEditor = null;
            }
            catch (Exception ex)
            {
                string errmsg = ex.Message.ToString();
            }
        }

        private void element_PreviewMouseLeftButtonDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (!IsInDesginMode) return;

			if (!_isCtrlPressed)
			{
				_selection.Clear();
				_selection.Add(sender as UIElement);
				Select(_selection.ToArray());
			}

			if (PlaylistEditor == null && !_PlaylistEditorJustClosed)
            {
                _isDragMode = true;
                _startPosition = e.GetPosition(this);
				_capturedPoint = e.GetPosition((UIElement)sender);

				((UIElement)sender).CaptureMouse();
            }
            _PlaylistEditorJustClosed = false;
            _propertiesViewer.Load(sender as UIElement);
        }

         private void element_PreviewMouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (!IsInDesginMode) return;
            _isDragMode = false;

			((UIElement)sender).ReleaseMouseCapture();

        }
        #endregion

        #region IDesignElement Members

		bool isPlay = false;

		public bool IsPlay
		{
			get { return isPlay; }
		}

        Dictionary<string, object> IDesignElement.Properties
        {
            get
            {
                ((IDesignElement)this).InitProperties();
                return _properties;
            }
            set
            {
                _properties = value;
                foreach (string name in _properties.Keys)
                {
                    PropertySetter.SetProperty(this, "IDesignElement", name, _properties[name]);
                }
            }
        }

		/// <summary>
		/// 재생이 시작된 시간
		/// </summary>
		public DateTime PlayStarted { get; set; }

		void IDesignElement.Play(TimeSpan ts)
		{
			if (!isPlay)
			{
				isPlay = true;
				// 			if (ContextMenu != null) ContextMenu.IsEnabled = false;
				if (_elementsListViewer != null)
					_elementsListViewer.PlayStatus(null, isPlay);

                #region 스크린 정보 갱신
                /// 재생 시작

                DigitalSignage.Common.ScreenStartInfoInSchedule.GetInstance.ScreenID = this.ScreenID.ToString();
                DigitalSignage.Common.ScreenStartInfoInSchedule.GetInstance.ScreenName = this.Name;


                DigitalSignage.Common.ScreenStartInfoInSchedule.GetInstance.OnScreenStarted(this);

                PlayStarted = DigitalSignage.Common.ScreenStartInfoInSchedule.GetInstance.ScreenStartDT;
                #endregion

                foreach (UIElement el in (Children.OfType<IDesignElement>()).OrderBy(el => el.ZIndex))
				{
					if (((IDesignElement)el).ISPlay)
						((IDesignElement)el).Stop();
					((IDesignElement)el).Play(ts);
				}
			}
		}

        void IDesignElement.Play()
        {
            ((IDesignElement)this).Play(TimeSpan.Zero);
        }

        void IDesignElement.Seek(TimeSpan ts)
        {
            foreach (UIElement el in (Children.OfType<IDesignElement>()).OrderBy(el => el.ZIndex))
            {
                if (((IDesignElement)el).ISPlay)
                    ((IDesignElement)el).Seek(ts);
            }
        }



		void IDesignElement.Pause()
		{
			try
			{
				foreach (UIElement el in (Children.OfType<IDesignElement>()).OrderBy(el => el.ZIndex))
				{
					((IDesignElement)el).Pause();
				}
			}
			catch { }
		}

        bool IDesignElement.ISPlay
        {
            get
            {
				return isPlay;
            }

        }
        DigitalSignage.Common.TransformEffect IDesignElement.TransEffect
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }
        void IDesignElement.Stop()
        {
            if (isPlay)
            {
                isPlay = false;

                if (_elementsListViewer != null)
                    _elementsListViewer.PlayStatus(null, isPlay);

                // 			if (ContextMenu != null) ContextMenu.IsEnabled = true;
                foreach (UIElement el in Children)
                {
					if(_isInDesignMode)
					{
						try
						{
							((IDesignElement)el).Stop();
						}
						catch
						{ }
					}
					else
						((IDesignElement)el).Stop();
                }

            }
            GC.Collect();
        }

		#region Ad Screen Properties
		bool _isAd = false;
		int _showFlag = (int)DigitalSignage.Common.ShowFlag.Show_Event;
		DateTime _starttime = DateTime.MinValue;
		DateTime _endtime = DateTime.MaxValue;
		string _metaTags = String.Empty;

		string _screen_ID = DateTime.Now.ToString("yyyyMMddHHmmssfff");

        #region 오픈스크린
        int _viewLeftPos = 0;
        int _viewTopPost = 0;

        /// <summary>
        /// 동기화 전체 화면상 현재 스크린의 좌측 시작 위치
        /// </summary>
        public int ViewLeftPos
        {
            get { return _viewLeftPos; }
            set { _viewLeftPos = value; }
        }
        /// <summary>
        /// 동기화 전체 화면상 현재 스크린의 상단 시작 위치
        /// </summary>
        public int ViewTopPos
        {
            get { return _viewTopPost; }
            set { _viewTopPost = value; }
        }

        #endregion

        /// <summary>
		/// 스크린의 고유 ID
		/// </summary>
		public string ScreenID
		{
			get { return _screen_ID; }
			set { _screen_ID = value; }
		}

        /// <summary>
        /// 스크린의 프로파일
        /// </summary>
        public String Profile = Properties.Resources.comboitemDefault;

		/// <summary>
		/// 광고 스크린 여부
		/// </summary>
		public bool IsAdScreen
		{
			get { return _isAd; }
			set { _isAd = value;}
		}

		/// <summary>
		/// 광고 시작 시간
		/// </summary>
		public DateTime Ad_StartTime
		{
			get
			{
				return _starttime;
			}
			set
			{
				_starttime = value;
			}
		}

		/// <summary>
		/// 광고 종료 시간
		/// </summary>
		public DateTime Ad_EndTime
		{
			get
			{
				return _endtime;
			}
			set
			{
				_endtime = value;
			}
		}

		/// <summary>
		/// 광고 메타 태그
		/// </summary>
		public string Ad_MetaTags
		{
			get { return _metaTags; }
			set { _metaTags = value; }
		}

		/// <summary>
		/// 광고 재생 조건
		/// </summary>
		public int Ad_EventConstraint
		{
			get { return _showFlag; }
			set { _showFlag = value; }
		}


		#endregion

		void IDesignElement.InitProperties()
        {
            _properties = new Dictionary<string, object>();
            IDesignElement child = this as IDesignElement;
//             _properties.Add("Name", child.Name);
			_properties.Add("ScreenID", _screen_ID);
            _properties.Add("Width", child.Width);
            _properties.Add("Height", child.Height);
            _properties.Add("Background", child.Background);
            _properties.Add("PlayTime", _TotalPlayTime);
			_properties.Add("isAd", _isAd);
			_properties.Add("showFlag", _showFlag);
			_properties.Add("AdStartTime", _starttime);
			_properties.Add("AdEndTime", _endtime);
			_properties.Add("MetaTags", _metaTags);

		}

		/// <summary>
		/// Gets or sets the horizontal ratio.
		/// </summary>
		double _horizontalRatio = 1.0;
		public double HorizontalRatio
		{
			get
			{
				return _horizontalRatio;
			}
			set
			{
				_horizontalRatio = value;
			}
		}

		/// <summary>
		/// Gets or sets the vertical ratio.
		/// </summary>
		double _verticalRatio = 1.0;
		public double VerticalRatio
		{
			get
			{
				return _verticalRatio;
			}
			set
			{
				_verticalRatio = value;
			}
		}

        string IDesignElement.Name
        {
            get
            {
                return this.Name;
            }
            set
            {
                this.Name = value;
            }
        }

        #region Properties that are not used by this class

        double IDesignElement.Width
        {
            get
            {
                return ActualWidth;
            }
            set
            {
				if (value < 40) value = 40;

                Width = value;
            }
        }

        double IDesignElement.Height
        {
            get
            {
                return ActualHeight;
            }
            set
            {
				if (value < 40) value = 40;

				Height = value;
            }
        }

        double IDesignElement.Left
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        double IDesignElement.Top
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        double IDesignElement.Bottom
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        double IDesignElement.Right
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        int IDesignElement.ZIndex
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        System.Windows.Media.Brush IDesignElement.BorderBrush
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        double IDesignElement.BorderThickness
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        double IDesignElement.BorderCorner
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        HorizontalAlignment IDesignElement.HorizontalAlignment
        {
            get
            {
                return HorizontalAlignment;
            }
            set
            {
                HorizontalAlignment = value;
            }
        }

        VerticalAlignment IDesignElement.VerticalAlignment
        {
            get
            {
                return VerticalAlignment;
            }
            set
            {
                VerticalAlignment = value;
            }
        }

        double IDesignElement.Opacity
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        System.Windows.Media.Stretch IDesignElement.Stretch
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        double IDesignElement.Volume
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        bool IDesignElement.Mute
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        System.Windows.Media.Brush IDesignElement.Background
        {
            get
            {
                return this.Background;
            }
            set
            {
                this.Background = value;
            }
        }

        System.Windows.Media.FontFamily IDesignElement.FontFamily
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        double IDesignElement.FontSize
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        FontWeight IDesignElement.FontWeight
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        System.Windows.Media.Brush IDesignElement.Foreground
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        int[] IDesignElement.StrokesLength
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        System.Windows.Media.PenLineCap IDesignElement.StrokeDashCap
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        char IDesignElement.SeparatorChar
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }
        #endregion

        Point IDesignElement.AspectRatio
        {
            get
            {
                return AspectRatio;
            }
            set
            {
                AspectRatio = value;
            }
        }

        Type IDesignElement.Type
        {
            get
            {
                return GetType();
            }
        }

        List<PlaylistItem> IDesignElement.Playlist
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }
       
        string IDesignElement.Content
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

		TimeSpan IDesignElement.RefreshInterval
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
				throw new NotImplementedException();
			}
		}

        IMediaFilesManager IDesignElement.FileManager
        {
            get { return this as IMediaFilesManager; }
        }

        VerticalAlignment IDesignElement.VerticalContentAlignment
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        HorizontalAlignment IDesignElement.HorizontalContentAlignment
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

		System.Drawing.Bitmap IDesignElement.GetThumbnail(int cx, int cy)
		{
			return null;
		}

        #endregion

        #region IMediaFilesManager Members

        void IMediaFilesManager.InitMediaFilesList(string mediaPath)
        {
            if (mediaPath[mediaPath.Length - 1].ToString() != @"\")
            {
                MediaPath = mediaPath + @"\";
            }
            else
            {
                MediaPath = mediaPath;
            }

            if (!System.IO.Directory.Exists(MediaPath)) System.IO.Directory.CreateDirectory(MediaPath);

            MediaFilesList = new List<string>();
            foreach (string fileName in Directory.GetFiles(MediaPath))
            {
                MediaFilesList.Add(fileName);
            }
            string subMediaPath = AppDomain.CurrentDomain.BaseDirectory + "\\PlayerData\\Content\\";
            if (!System.IO.Directory.Exists(subMediaPath)) System.IO.Directory.CreateDirectory(subMediaPath);

        }

        string IMediaFilesManager.AddNewFileWithModifiedName(string filePath)
        {
			try
			{
				string fullNewName = System.IO.Path.GetFileName(filePath);
				string fullNewPath = MediaPath + fullNewName;
                DriveInfo _info = new DriveInfo(MediaPath);
                FileInfo _fileInfo = new FileInfo(filePath);
                if (_fileInfo.Length >= _info.AvailableFreeSpace)
                {
					MessageBox.Show(Properties.Resources.mbErrorSpace);
                    return null;
                }

				if (!File.Exists(fullNewPath))
				{
					try
					{
						CopyBox copybox = new CopyBox();
						copybox.Show();
						copybox.Copy(filePath, fullNewPath);
						copybox.Close();
					}
					catch (System.UnauthorizedAccessException)
					{
						return "No media";
					}
					catch (Exception ex)
					{
						MessageBox.Show(ex.Message);
						return null;

					}
				}
				else
				{
					//	동일한 파일이 있다면..
					try
					{
                        if (filePath == fullNewPath)//bmwe3 14.3.11 선택한 파일과 복사할 파일의 경로가 동일하면 skip 처리
                            return fullNewName;

						MessageBoxResult _result = MessageBox.Show(Properties.Resources.mbFileOverwrite, Properties.Resources.titleDesignerWindow,
							MessageBoxButton.YesNoCancel, MessageBoxImage.Warning);

						switch(_result)
						{
							case MessageBoxResult.Yes:
								{
									try
									{
										CopyBox copybox = new CopyBox();
										copybox.Show();
										copybox.Copy(filePath, fullNewPath);
										copybox.Close();

									}
									catch (System.UnauthorizedAccessException)
									{
										return "No media";
									}
									catch (Exception ex)
									{
										MessageBox.Show(ex.Message);
										return null;
									}
									break;
								}
							case MessageBoxResult.No:
								break;
							case MessageBoxResult.Cancel:
								return null;
						}
					}
					catch
					{
						return null;
					}

				}
				if (!MediaFilesList.Contains(fullNewPath))
				{
					MediaFilesList.Add(fullNewPath);
				}
				return fullNewName;
			}
			catch (Exception e)
			{
				MessageBox.Show(e.Message);
			}
			return null;
        }

        string IMediaFilesManager.GetFullFilePathByName(string name)
        {
            string fullPath = String.Empty;

            try
            {
                if (System.IO.File.Exists(name))
                {
                    return name;
                }
                else if (System.IO.File.Exists(AppDomain.CurrentDomain.BaseDirectory + @"\PlayerData\Content\" + name))
                {
                    return AppDomain.CurrentDomain.BaseDirectory + @"\PlayerData\Content\" + name;
                }
                else
                {
                    throw new Exception("No Files");
                }
            }
            catch
            {

                foreach (string filePath in MediaFilesList)
                {
                    if (System.IO.Path.GetFileName(filePath) == name)
                    {
                        fullPath = filePath;
                    }
                }
                if (String.IsNullOrEmpty(fullPath))
                {
                    try
                    {
                        string secondPath = AppDomain.CurrentDomain.BaseDirectory + @"\PlayerData\Content\" + System.IO.Path.GetFileName(name);
                        if (System.IO.File.Exists(secondPath))
                        {
                            return fullPath = secondPath;
                        }
                    }
                    catch { fullPath = String.Empty; }
                }
            }
			if (String.IsNullOrEmpty(fullPath))
			{
				throw new iVisionException(ErrorCodes.IS_ERROR_CONTENT_NOT_FOUND, String.Format("The '{0}' file is not found.", name));
			}
            return fullPath;
        }
        #endregion


        int nCurrentLeft = int.MaxValue;
        int nCurrentTop = int.MaxValue;
        int nCurrentWidth = int.MaxValue;
        int nCurrentHeight = int.MaxValue;

        /// <summary>
        /// OpenScreen : 스크린 내 이벤트 처리 로직
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void GetInstance_SyncScreenEvent(object sender, DigitalSignage.Common.OpenScreen.SyncScreenEventArgs e)
        {
            try
            {
                lock (this)
                {
                    if (this.IsPlay)
                    {
                        /// 오픈스크린 Region 찾아서 이동 처리 로직 추가!
                        /// 
                        logger.Debug("- OpenScreen : 스크린 내 Sync 이벤트 발생 - 대상 : {0}, 키 : {1}, 값 : {2}", e.TargetName, e.EventKey, e.EventValue);

                        System.Windows.Forms.MethodInvoker method = delegate
                        {
                            try
                            {
                                foreach (UIElement el in (Children.OfType<IDesignElement>()).OrderBy(el => el.ZIndex))
                                {
                                    IDesignElement de = el as IDesignElement;

                                    logger.Debug("- OpenScreen : 스크린 내 IDesignElement 검색 중 - 이름 : {0}", de == null ? "NULL" : de.Name);

                                    if (de != null && de.Name == e.TargetName)
                                    {
                                        logger.Debug("- OpenScreen : 스크린 내 동일 이름 찾기 성공");

                                        switch (e.EventKey)
                                        {
                                            case "Left":
                                                //de.Left = Convert.ToInt32(e.EventValue);
                                                if (nCurrentLeft != (Convert.ToInt32(e.EventValue) - ViewLeftPos))
                                                {
                                                    AnimationEffect(el, InkCanvas.LeftProperty, de.Left, nCurrentLeft = (Convert.ToInt32(e.EventValue) - ViewLeftPos));
                                                }
                                                break;
                                            case "Top":
                                                if (nCurrentTop != (Convert.ToInt32(e.EventValue) - ViewTopPos))
                                                {
                                                    //de.Top = Convert.ToInt32(e.EventValue);
                                                    AnimationEffect(el, InkCanvas.TopProperty, de.Top, nCurrentTop = (Convert.ToInt32(e.EventValue) - ViewTopPos));
                                                }
                                                break;
                                            case "Width":
                                                //de.Width = Convert.ToInt32(e.EventValue);
                                                if (nCurrentWidth != Convert.ToInt32(e.EventValue))
                                                {
                                                    AnimationEffect(el, InkCanvas.WidthProperty, de.Width, nCurrentWidth = Convert.ToInt32(e.EventValue));
                                                }
                                                break;
                                            case "Height":
                                                //de.Height = Convert.ToInt32(e.EventValue);
                                                if (nCurrentHeight != Convert.ToInt32(e.EventValue))
                                                {
                                                    AnimationEffect(el, InkCanvas.HeightProperty, de.Height, nCurrentHeight = Convert.ToInt32(e.EventValue));
                                                }
                                                break;
                                        }

                                        logger.Debug("- OpenScreen : 스크린 내 이벤트 처리 성공");

                                    }
                                }
                            }
                            catch (Exception exr)
                            {
                                logger.Error("OpenScreen : {0}", exr.ToString());
                            }
                        };

                        if (!this.Dispatcher.CheckAccess())
                        {
                            try
                            {
                                this.Dispatcher.BeginInvoke(method);
                            }
                            catch (Exception exx)
                            {
                                logger.Error(exx + "");
                            }
                        }
                        else
                        {
                            method.Invoke();
                        }
                    }
                    else
                    {
                        // OpenScreen : Sync 스케줄 이벤트 처리기 해제
                        DigitalSignage.Common.OpenScreen.SyncScreenEventHelper.GetInstance.SyncScreenEvent -= new DigitalSignage.Common.OpenScreen.SyncScreenEventHandler(GetInstance_SyncScreenEvent);

                    }
                }

            }
            catch { }

        }

        public void AnimationEffect(UIElement target, DependencyProperty prop, double from, double to)
        {
            logger.Debug("- 오픈스크린: From: {0}, To: {1}", from, to);
            //가속도값 설정하기 
            CubicEase ease = new CubicEase();
            ease.EasingMode = EasingMode.EaseIn;

            //더블 애니메이션 하나 설정 했다. 
            DoubleAnimation MyDoubleAnimation = new DoubleAnimation();
            //    new DoubleAnimation(from, to, new Duration(TimeSpan.FromSeconds(0.5)), FillBehavior.Stop);
            MyDoubleAnimation.From = from;
            MyDoubleAnimation.To = to;
            MyDoubleAnimation.EasingFunction = ease;
            MyDoubleAnimation.Duration = new Duration(TimeSpan.FromSeconds(0.5));
            MyDoubleAnimation.FillBehavior = FillBehavior.HoldEnd;
            MyDoubleAnimation.IsCumulative = false;
//            MyDoubleAnimation.Completed += new EventHandler(MyDoubleAnimation_Completed);

            target.BeginAnimation(prop, MyDoubleAnimation);
        }
    }
}