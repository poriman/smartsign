﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Markup;
using System.IO;
using System.Windows.Media.Animation;
using System.Xml;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Xml.Linq;
using DigitalSignage.Common;
using System.Threading;

namespace WPFDesigner
{
    [Serializable]
    public class PlaylistPlayer
    {
		#region Logging routines
		private iVisionLogHelper iVisionLog = iVisionLogHelper.GetInstance;
		#endregion

		private object semaphore = null;
		bool bIsStop = false;

        /// <summary>
        /// Represents the playlist of element
        /// </summary>
        private List<PlaylistItem> _playlist;
        /// <summary>
        /// Represents the element
        /// </summary>
        private IDesignElement _designElement;

        /// <summary>
        /// Represents the timer uset for playing content
        /// </summary>
        private DispatcherTimer _timer;

        /// <summary>
        /// Indicates the index of current content
        /// </summary>
        private int _currentContentIndex;

        /// <summary>
        /// Represents the maximum content index (the items count in playlist - 1)
        /// </summary>
        private int _maxContentIndex;

        /// <summary>
        /// 한사이클 총 재생시간
        /// </summary>
        private int _totalPlaytimeSec = 0;

        /// <summary>
        /// 마지막 기준 시간
        /// </summary>
        private DateTime _lastBaseDT = DateTime.MinValue;

        /// <summary>
        /// 컨텐트 바꿀 시간
        /// </summary>
        private DateTime _contChangeDT = DateTime.MinValue;

        private bool _isFirstTime = true;

        /// <summary>
        /// Initializes the new PLaylistPlayer class instance
        /// </summary>
		/// <param name="element">The element to play</param>
        public PlaylistPlayer(IDesignElement element)
        {
            _designElement = element;
            _playlist = element.Playlist;
            _currentContentIndex = 0;
            _maxContentIndex = _playlist.Count - 1;

            /// 총 재생 시간 구하기
            try
            {
                foreach (PlaylistItem item in _playlist)
                {
                    _totalPlaytimeSec += (int)item.DurationAsTimeSpan.TotalSeconds;
                }
            }
            catch { }

        }

        public void Seek(TimeSpan ts)
        {

        }

        /// <summary>
        /// Starts the loaded playlist playback
        /// </summary>
        /// <param name="ts"></param>
        public void Start(TimeSpan ts)
        {
            if (_playlist.Count == 0) return;

            _timer = new DispatcherTimer();
            _timer.Interval = TimeSpan.FromMilliseconds(10);
            _timer.Tick += new EventHandler(_timer_Tick);

			bIsStop = false;
			semaphore = new object();

            CheckContent();

            _isFirstTime = false;

            if (_playlist[_currentContentIndex].DurationAsTimeSpan.TotalSeconds > 0)
            {
                /// 시간 주기가 있는 경우
                _timer.Start();
            }

            
        }

        /// <summary>
        /// 컨텐트를 시작할지 말지를 결정한다.
        /// </summary>
        void CheckContent()
        {
            if (ScreenStartInfoInSchedule.GetInstance.ScreenStartDT != _lastBaseDT)
            {
                #region 갱신
                _lastBaseDT = ScreenStartInfoInSchedule.GetInstance.ScreenStartDT;

                DateTime dtNow = DateTime.Now;
                TimeSpan tsbetween = dtNow - _lastBaseDT;

                Double lTotalSec = (double)tsbetween.TotalSeconds;
                Double dModSec = _totalPlaytimeSec == 0 ? lTotalSec : (double)(lTotalSec % (double)_totalPlaytimeSec);

                for (int i = 0; i < _playlist.Count; ++i)
                {

                    int nContentSec = (int)_playlist[i].DurationAsTimeSpan.TotalSeconds;
                    if (nContentSec != 0 && dModSec >= nContentSec)
                    {
                        dModSec -= nContentSec;
                    }
                    else
                    {
                        int tempIndex = _currentContentIndex;
                        _currentContentIndex = i;

                        if (nContentSec == 0)
                        {
                            _contChangeDT = DateTime.MaxValue;
                        }
                        else
                        {
                            _contChangeDT = dtNow + TimeSpan.FromSeconds(nContentSec - dModSec);
                        }

                        #region 재생
                        try
                        {
                            _designElement.PlayStarted = dtNow - TimeSpan.FromSeconds(dModSec);
                            if (_isFirstTime || tempIndex != _currentContentIndex)
                            {
                                Console.WriteLine("_designElement.Content 전: {0}", DateTime.Now.ToString("HH:mm:ss.fff"));
                                _designElement.Content = _playlist[_currentContentIndex].Content;
                                Console.WriteLine(_designElement.Content);
                                Console.WriteLine("_designElement.Content 후: {0}", DateTime.Now.ToString("HH:mm:ss.fff"));
                            }
                        }
                        catch (iVisionException ive)
                        {
                            iVisionLog.ScreenErrorLog(_playlist[_currentContentIndex].Content, ive.ErrorCode, ive.DetailMessage);
                        }
                        catch (Exception e)
                        {
                            iVisionLog.ScreenErrorLog(_playlist[_currentContentIndex].Content, ErrorCodes.IS_ERROR_UNSUPPT_CONTENT, e.Message);
                        }
                        #endregion

                        break;
                    }
                }
                #endregion
            }

            else if (_contChangeDT <= DateTime.Now)
            {
                #region 변경
                _currentContentIndex++;
                if (_currentContentIndex > _maxContentIndex)
                {
                    _currentContentIndex = 0;
                }

                DateTime _playStarted = DateTime.Now;

                if (_playlist[_currentContentIndex].DurationAsTimeSpan.TotalSeconds > 0)
                {
                    lock (semaphore)
                    {
                        if (bIsStop) return;

                        if(!_timer.IsEnabled)
                            _timer.Start();

                        _playStarted = _contChangeDT;
                        _contChangeDT = _playStarted + _playlist[_currentContentIndex].DurationAsTimeSpan;
                    }
                }
                else
                {
                    _contChangeDT = DateTime.MaxValue;
                }

                #region 재생
                try
                {
                    _designElement.PlayStarted = _playStarted;
                    _designElement.Content = _playlist[_currentContentIndex].Content;
                    
                }
                catch (iVisionException ive)
                {
                    iVisionLog.ScreenErrorLog(_playlist[_currentContentIndex].Content, ive.ErrorCode, ive.DetailMessage);
                }
                catch (Exception ex)
                {
                    iVisionLog.ScreenErrorLog(_playlist[_currentContentIndex].Content, ErrorCodes.IS_ERROR_UNSUPPT_CONTENT, ex.Message);
                }
                #endregion
                #endregion
            }
        }

        /// <summary>
        /// Is used to load next content and occures when timer ticks
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void _timer_Tick(object sender, EventArgs e)
        {
			lock (semaphore)
			{
                if (bIsStop)
                {
                    _timer.Stop();
                }
			}

            CheckContent();
        }

        /// <summary>
        /// Stops the loaded playlist playback
        /// </summary>
        public void Stop()
        {
			if (_playlist.Count == 0) return;

			lock (semaphore)
			{
				_timer.Stop();
				_currentContentIndex = 0;
				bIsStop = true;
			}
        }

        /// <summary>
        /// Static method that calculates and returns the total playback time of specified playlist
        /// </summary>
        /// <param name="playlist">The playlist to calculate total length of</param>
        /// <returns>Total playback time of the playlist</returns>
        public static TimeSpan GetTotalPlayingTime(List<PlaylistItem> playlist)
        {
            TimeSpan timeSpan = new TimeSpan();
            foreach (PlaylistItem item in playlist)
            {
                timeSpan.Add(item.DurationAsTimeSpan);
            }
            return timeSpan;
        }
    }
}