﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Xml;

namespace TVComponents
{
    public class ChannelList
    {
        Collection<Channel> arrChannels = new Collection<Channel>();

        string _filepath = "";

        public ChannelList(String xmlPath)
        {
            Initialize();
            LoadFromXml(_filepath = xmlPath);
        }

        void LoadFromXml(string xmlPath)
        {
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(xmlPath);

                XmlNodeList nodeChannels = doc.SelectNodes("//child::Channel");

                if (nodeChannels == null)
                    return;

                foreach (XmlNode node in nodeChannels)
                {
                    Channel ch = Channel.GetChannelFromXml(node);
                    arrChannels.Add(ch);
                }
            }
            catch
            {
            }
        }

        void Initialize()
        {
            Clear();
        }

        public void Clear()
        {
            arrChannels.Clear();
        }

        public void AddChannel(Channel ch)
        {
            arrChannels.Add(ch);
        }
        public void SortAddChannel(Channel ch)
        {
            if (arrChannels.Count > 0)
            {
                bool binsert = false;
                for (int i = 0; i < arrChannels.Count; i++)
                {
                    Channel _ch = arrChannels[i];
                    if (_ch.MajorNum > ch.MajorNum)
                    {
                        arrChannels.Insert(i, ch);
                        binsert = true;
                        break;
                    }
                }
                if(binsert == false)
                    AddChannel(ch);
            }
            else
                AddChannel(ch);
        }

        public void UpdateChannel(int nIndex, String name)
        {
            arrChannels[nIndex].ChannelName = name;
        }

        public Channel[] GetChannels()
        {
            return arrChannels.ToArray<Channel>();
        }
        public Channel GetChannel(int nIndex)
        {
            return arrChannels[nIndex];
        }

        public Channel FindChannel(string ch_name)
        {
            foreach (Channel ch in arrChannels)
            {
                if (ch.ChannelName.Equals(ch_name))
                    return ch;
            }

            return null;
        }

        public Channel FindChannel(AVSOURCE chtype, string ch_name)
        {
            foreach (Channel ch in arrChannels)
            {
                if (ch.SourceType == chtype && ch.ChannelName.Equals(ch_name))
                    return ch;
            }

            return null;
        }

        public void Save()
        {
            XmlDocument doc = new XmlDocument();

            XmlNode nodeChannels = doc.CreateNode(XmlNodeType.Element, "Channels", "");

            foreach (Channel ch in arrChannels)
            {
                nodeChannels.AppendChild(ch.ConvertToXml(doc));
            }

            doc.AppendChild(nodeChannels);

            doc.Save(_filepath);
        }
    }
}
