﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace TVComponents
{
    public class Channel
    {
        protected string name = "No Name";
        protected int ChMajor;
        protected int ChMinor;
        protected AVSOURCE type;

        public Channel() { }

        public string ChannelName
        {
            set { name = value; }
            get { return name; }
        }

        public int MajorNum
        {
            set { ChMajor = value; }
            get { return ChMajor; }
        }

        public int MinorNum
        {
            set { ChMinor = value; }
            get { return ChMinor; }
        }

        public AVSOURCE SourceType
        {
            set { type = value; }
            get { return type; }
        }

        public XmlNode ConvertToXml(XmlDocument doc)
        {
            XmlNode nodeChannel = doc.CreateNode(XmlNodeType.Element, "Channel", "");

            XmlAttribute attr1 = doc.CreateAttribute("Name");
            attr1.Value = this.ChannelName;
            XmlAttribute attr2 = doc.CreateAttribute("ChMajor");
            attr2.Value = this.MajorNum.ToString();
            XmlAttribute attr3 = doc.CreateAttribute("ChMinor");
            attr3.Value = this.MinorNum.ToString();
            XmlAttribute attr4 = doc.CreateAttribute("type");
            attr4.Value = this.SourceType.ToString();

            nodeChannel.Attributes.Append(attr1);
            nodeChannel.Attributes.Append(attr2);
            nodeChannel.Attributes.Append(attr3);
            nodeChannel.Attributes.Append(attr4);

            return nodeChannel;
        }

        public static Channel GetChannelFromXml(XmlNode node)
        {
            Channel ch = new Channel();

            ch.ChannelName = node.Attributes["Name"].Value;
            ch.MajorNum = Convert.ToInt32(node.Attributes["ChMajor"].Value);
            ch.MinorNum = Convert.ToInt32(node.Attributes["ChMinor"].Value);
            switch (node.Attributes["type"].Value)
            {
                case "SRC_DTVC": ch.SourceType = AVSOURCE.SRC_DTVC; break;
                case "SRC_DTVA": ch.SourceType = AVSOURCE.SRC_DTVA; break;
                case "SRC_ATVC":
                default: ch.SourceType = AVSOURCE.SRC_ATVC; break;
            }

            return ch;
        }
    }
}
