﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Drawing;

namespace TVComponents
{
    public enum AVSOURCE : int
    {
        SRC_NONE = 0,
        SRC_DTVC,
        SRC_DTVA,
        SRC_ATVC,
        SRC_ATVA,
        SRC_VIDEO,
        SRC_SVIDEO,
        SRC_COMPONENT,
        SRC_HDMIV,
        SRC_LINEIN,
        SRC_SPDIF,
        SRC_HDMIA
    };

    public enum CWINSTYLE
    {
        CWS_CHILD = 0x80,
        CWS_POPUP = 0x40,
        CWS_SIZE = 0x01,
        CWS_MOVE = 0x02
    };

    public enum BARTYPE
    {
        BAR_TITLE,
        BAR_CONTROL,
        BAR_STATUS
    };

    public enum BARSTATUS
    {
        BAR_NONE,
        BAR_FIX,
        BAR_FLOAT
    };

    public enum OSDFLAG : int
    {
        OSD_SRC_DISABLE = 1,
        OSD_CH_DISABLE = 2,
        OSD_VOL_DISABLE = 4,
        ERR_BOX_DISABLE = 8
    };

    public enum _RC_DATA : int
    {
        RCD_MIN = 0,
        RCD_TV_ON,				// 0x01
        RCD_PC_ON,				// 0x02
        RCD_NUM1,				// 0x03
        RCD_NUM2,				// 0x04
        RCD_NUM3,				// 0x05
        RCD_NUM4,				// 0x06
        RCD_NUM5,				// 0x07
        RCD_NUM6,				// 0x08
        RCD_NUM7,				// 0x09
        RCD_NUM8,				// 0x0A
        RCD_NUM9,				// 0x0B
        RCD_NUM0,				// 0x0C
        RCD_CH_UP,				// 0x0D
        RCD_CH_DOWN,			// 0x0E
        RCD_VOL_UP,			    // 0x0F
        RCD_VOL_DOWN,			// 0x10
        RCD_MUTE,				// 0x11
        RCD_UP,				    // 0x12
        RCD_DOWN,				// 0x13
        RCD_LEFT,				// 0x14
        RCD_RIGHT,				// 0x15
        RCD_OK,				    // 0x16
        RCD_PREVIOUS_CHANNEL,	// 0x17
        RCD_FULL_SCREEN,		// 0x18
        RCD_SNAPSHOT,			// 0x19
        RCD_RECORD,			    // 0x1A
        RCD_PLAY,				// 0x1B
        RCD_STOP,				// 0x1C
        RCD_REWIND,			    // 0x1D
        RCD_FASTFORWARD,		// 0x1E
        RCD_PREVIOUS_TRACK,	    // 0x1F	
        RCD_NEXT_TRACK,		    // 0x20
        RCD_HIDE,				// 0x21
        RCD_MENU,				// 0x22
        RCD_SLEEP,				// 0x23
        RCD_FUNCTION,			// 0x24
        RCD_LAUNCHER_ALT,		// 0x25
        RCD_SWITCHER_ALT,		// 0x26
        RCD_CLOSE_ALT,			// 0x27
        RCD_RATIO,				// 0x28
        RCD_EPG,				// 0x29
        RCD_TIMESHIFT,			// 0x2A
        RCD_SOURCE,			    // 0x2B
        RCD_OPEN,				// 0x2C
        RCD_TAB,				// 0x2D
        RCD_LAUNCHER,			// 0x2E
        RCD_SWITCHER,			// 0x2F
        RCD_CLOSE,				// 0x30
        RCD_AUDIO,				// 0x31
        RCD_USER1,				// 0x32
        RCD_USER2,				// 0x33
        RCD_USER3,				// 0x34
        RCD_USER4,				// 0x35
        RCD_DATA_MAX
    };

    public struct MSG
    {
        IntPtr hwnd;
        uint message;
        int wParam;
        int lParam;
        int time;
        Point pt;
    };
    public enum CAPWIN_STATUS : int
    {
        CAPWIN_CREATED = 1,
        CAPWIN_STARTED = 2,
        CAPWIN_MOVING = 4,
        CAPWIN_SIZING = 8,
        REMOCON_CREATED = 16,
    };

    [StructLayout(LayoutKind.Sequential)]
    public struct COLORREF
    {
        public byte R;
        public byte G;
        public byte B;
    }

    [StructLayout(
    LayoutKind.Sequential,      //must specify a layout
    CharSet = CharSet.Ansi)]    //if you intend to use char
    public struct API_CHITEM
    {
	    public int RfCh;
        public int ChMajor;
        public int ChMinor;
        public int Scrambled;
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 32)]
        public char[] ChName;
    };

    public struct CHANNELITEM
    {
        public API_CHITEM ChInfo;
        public AVSOURCE type;
    };

    internal static class SkyApiClass
    {
        [DllImport("SkySdkApi.dll", EntryPoint = "_CreateApiModule@4", ExactSpelling = true, CallingConvention = CallingConvention.StdCall)]
        public static extern bool CreateApiModule(IntPtr ppObject);

        [DllImport("SkySdkApi.dll", EntryPoint = "_DestroyApiModule@4")]
        public static extern void DestroyApiModule(IntPtr ppObject);
        
        [DllImport("SkySdkApi.dll", EntryPoint = "_ApiPreTranslateMessage@4")]
        internal static extern void ApiPreTranslateMessage([Out, MarshalAs(UnmanagedType.Struct)] out MSG pMsg);

        [DllImport("SkySdkApi.dll", EntryPoint = "_CreateCapWindow@32")]
        internal static extern IntPtr CreateCapWindow(IntPtr hParent,
                                       CWINSTYLE CWinStyle,
                                       int BdWd,
                                       COLORREF BdCol,
                                       int Sx, int Sy, int Width, int Height);

        [DllImport("SkySdkApi.dll", EntryPoint = "_DestroyCapWindow@0")]
        internal static extern void DestroyCapWindow();

        [DllImport("SkySdkApi.dll", EntryPoint = "_StartCapture@4")]
        internal static extern void StartCapture([MarshalAs(UnmanagedType.LPStr)] String lpCmdLine);

        [DllImport("SkySdkApi.dll", EntryPoint = "_StopCapture@0")]
        internal static extern void StopCapture();

        [DllImport("SkySdkApi.dll", EntryPoint = "_QuerySource@4")]
        internal static extern uint QuerySource(AVSOURCE Source);

        [DllImport("SkySdkApi.dll", EntryPoint = "_SetSource@4")]
        internal static extern uint SetSource(AVSOURCE Source);

        [DllImport("SkySdkApi.dll", EntryPoint = "_ChangeSource@4")]
        internal static extern uint ChangeSource(AVSOURCE Source);

        [DllImport("SkySdkApi.dll", EntryPoint = "_ChannelUp@0")]
        internal static extern uint ChannelUp();

        [DllImport("SkySdkApi.dll", EntryPoint = "_ChannelDown@0")]
        internal static extern uint ChannelDown();

        [DllImport("SkySdkApi.dll", EntryPoint = "_ChangeChannel@8")]
        internal static extern uint ChangeChannel(int ChMajor, int ChMinor);

        [DllImport("SkySdkApi.dll", EntryPoint = "_SetVolume@4")]
        internal static extern uint SetVolume(int Vol);				// 0(min) ~ 100(max)

        [DllImport("SkySdkApi.dll", EntryPoint = "_VolumeUp@0")]
        internal static extern uint VolumeUp();						// Volume + 1

        [DllImport("SkySdkApi.dll", EntryPoint = "_VolumeDown@0")]
        internal static extern uint VolumeDown();						// Volume - 1

        [DllImport("SkySdkApi.dll", EntryPoint = "_VolumeMute@4")]
        internal static extern uint VolumeMute(int OnOff);			// if OnOff is TRUE,  mute on

        [DllImport("SkySdkApi.dll", EntryPoint = "_ReqCcData@8")]// if OnOff is FALSE, mute off
        internal static extern uint ReqCcData(IntPtr CcNotify, int CcMsg);

        [DllImport("SkySdkApi.dll", EntryPoint = "_SetBarControl@8")]
        internal static extern void SetBarControl(BARTYPE BarType, BARSTATUS View);

        [DllImport("SkySdkApi.dll", EntryPoint = "_OsdDisplayMask@4")]
        internal static extern void OsdDisplayMask(int OsdFlag);

        [DllImport("SkySdkApi.dll", EntryPoint = "_CreateRemoCon@8")]
        internal static extern uint CreateRemoCon(IntPtr hWndRmt, uint MsgRmt);

        [DllImport("SkySdkApi.dll", EntryPoint = "_DestroyRemoCon@0")]
        internal static extern void DestroyRemoCon();

        [DllImport("SkySdkApi.dll", EntryPoint = "_BypassRmtData@4")]
        internal static extern void BypassRmtData(IntPtr RmtData);

        [DllImport("SkySdkApi.dll", EntryPoint = "_GetChListCounter@8")]
        internal static extern int GetChListCounter(AVSOURCE TvSrc,out int nCnt);

        [DllImport("SkySdkApi.dll", EntryPoint = "_GetChListData@12")]
        internal static extern int GetChListData(AVSOURCE TvSrc, int ChInx,  out API_CHITEM pItem);
    }
}
