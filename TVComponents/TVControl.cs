﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace TVComponents
{
    public partial class TVControl : UserControl
    {
        public const int WM_SYSCOMMAND = 0x0444;
        public const int SC_TVScreen = 0x0445;
        public const int SC_PCScreen = 0x0446;

        public bool isDevicePC = true;
        AVSOURCE m_CurVideoSrc = AVSOURCE.SRC_ATVC;
        AVSOURCE m_CurAudioSrc = AVSOURCE.SRC_LINEIN;
        IntPtr m_CaphWnd;
        int nRunStatus = 0;
        uint m_RmtNotifyMsg = 0;

        Channel chanelInfo;
        private static int _ControlCount = 0;
        private static int _PlayCount = 0;
        private static object lockObj = new object();

        static TVControl instance = null;
        static bool isPlay = false;
        IntPtr HWND_TOP = IntPtr.Zero;

        public TVControl()
        {
            InitializeComponent();

            chanelInfo = new Channel();
            chanelInfo.SourceType = AVSOURCE.SRC_ATVC;
            this.Disposed += new EventHandler(TVControl_Disposed);
            _ControlCount++;
        }

        public static TVControl Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new TVControl();
                }
                else
                {
                    instance.Stop();
                    
                    instance = new TVControl();
                }

                return instance;
            }
        }

        void TVControl_Disposed(object sender, EventArgs e)
        {
            try
            {
                if(_ControlCount == 1)
                    Stop();

                if (!this.IsDisposed)
                {
                    _ControlCount--;
                }
            }
            catch
            {

            }
        }

        public Channel ChannelInfo
        {
            get { return chanelInfo;}
            set { chanelInfo = value; }
        }

        public void Play()
        {
            lock (lockObj)
            {
                {
                    try
                    {
                        m_RmtNotifyMsg = WIN32.RegisterWindowMessage("SDK_RMT_DATA");

                        bool isok = SkyApiClass.CreateApiModule((IntPtr)null);

                        //MessageBox.Show(String.Format("CreateApiModule: {0}", isok));

                        SkyApiClass.OsdDisplayMask((int)OSDFLAG.ERR_BOX_DISABLE);

                        if (SkyApiClass.QuerySource(AVSOURCE.SRC_DTVC) == 1)
                        {
                            m_CurVideoSrc = AVSOURCE.SRC_DTVC;
                            m_CurAudioSrc = AVSOURCE.SRC_LINEIN;
                        }
                        else if (SkyApiClass.QuerySource(AVSOURCE.SRC_HDMIV) == 1)
                        {
                            m_CurVideoSrc = AVSOURCE.SRC_HDMIV;
                            m_CurAudioSrc = AVSOURCE.SRC_HDMIA;
                        }

                        COLORREF rgb;
                        rgb.B = 0;
                        rgb.G = 0;
                        rgb.R = 0;

                        m_CaphWnd = SkyApiClass.CreateCapWindow(this.Handle, CWINSTYLE.CWS_CHILD | CWINSTYLE.CWS_SIZE, 0, rgb, 0, 0, this.Width + 1, this.Height);
                        nRunStatus += (int)CAPWIN_STATUS.CAPWIN_CREATED;

                        //MessageBox.Show(String.Format("CreateCapWindow: {0}", m_CaphWnd));

                        SkyApiClass.SetSource(ChannelInfo.SourceType);

                        //MessageBox.Show(String.Format("SetSource: {0}", ChannelInfo.SourceType));


                        SkyApiClass.StartCapture("");
                        WIN32.ShowWindow(m_CaphWnd, 5);
                        nRunStatus += (int)CAPWIN_STATUS.CAPWIN_STARTED;
                        //MessageBox.Show(String.Format("StartCapture: {0}", ""));

                        SkyApiClass.CreateRemoCon(this.Handle, m_RmtNotifyMsg);
                        nRunStatus += (int)CAPWIN_STATUS.REMOCON_CREATED;

                        //MessageBox.Show(String.Format("CreateRemoCon: {0}", m_RmtNotifyMsg));

                        SkyApiClass.ChangeChannel(ChannelInfo.MajorNum, ChannelInfo.MinorNum);

                        //MessageBox.Show(String.Format("ChangeChannel: {0}-{1}", ChannelInfo.MajorNum, ChannelInfo.MinorNum));

                        isPlay = true;
                    }
                    catch
                    {
                    }
                }
            }
        }

        public void ChangeSource(AVSOURCE source)
        {
            lock (lockObj)
            {
                try
                {
                    SkyApiClass.ChangeSource(source);
                }
                catch
                {
                }
            }
        }

        public void Play(int width, int height)
        {
            lock (lockObj)
            {
                {
                    try
                    {
                        m_RmtNotifyMsg = WIN32.RegisterWindowMessage("SDK_RMT_DATA");

                        bool isok = SkyApiClass.CreateApiModule((IntPtr)null);

                        SkyApiClass.OsdDisplayMask((int)OSDFLAG.ERR_BOX_DISABLE);

                        if (SkyApiClass.QuerySource(AVSOURCE.SRC_DTVC) == 1)
                        {
                            m_CurVideoSrc = AVSOURCE.SRC_DTVC;
                            m_CurAudioSrc = AVSOURCE.SRC_LINEIN;
                        }
                        else if (SkyApiClass.QuerySource(AVSOURCE.SRC_HDMIV) == 1)
                        {
                            m_CurVideoSrc = AVSOURCE.SRC_HDMIV;
                            m_CurAudioSrc = AVSOURCE.SRC_HDMIA;
                        }

                        COLORREF rgb;
                        rgb.B = 0;
                        rgb.G = 0;
                        rgb.R = 0;

                        m_CaphWnd = SkyApiClass.CreateCapWindow(this.Handle, CWINSTYLE.CWS_CHILD | CWINSTYLE.CWS_SIZE, 0, rgb, 0, 0, width + 1, height);
                        nRunStatus += (int)CAPWIN_STATUS.CAPWIN_CREATED;

                        SkyApiClass.ChangeSource(ChannelInfo.SourceType);

                        SkyApiClass.StartCapture("");
                        WIN32.ShowWindow(m_CaphWnd, 5);

                        WIN32.SetWindowPos(m_CaphWnd, HWND_TOP, 0, 0, 0, 0, 3);

                        System.Threading.Thread.Sleep(1000);
                        nRunStatus += (int)CAPWIN_STATUS.CAPWIN_STARTED;

                        SkyApiClass.CreateRemoCon(this.Handle, m_RmtNotifyMsg);
                        nRunStatus += (int)CAPWIN_STATUS.REMOCON_CREATED;

                        SkyApiClass.ChangeChannel(ChannelInfo.MajorNum, ChannelInfo.MinorNum);

                        isPlay = true;
                    }
                    catch
                    {
                    }
                }
            }
        }

        public void Stop()
        {
            lock (lockObj)
            {
                bool end = false;
                try
                {
                    if ((nRunStatus & (int)CAPWIN_STATUS.CAPWIN_STARTED) > 0)
                    {
                        SkyApiClass.StopCapture();
                        nRunStatus -= (int)CAPWIN_STATUS.CAPWIN_STARTED;
                    }
                    if ((nRunStatus & (int)CAPWIN_STATUS.CAPWIN_CREATED) > 0)
                    {
                        SkyApiClass.DestroyCapWindow();
                        nRunStatus -= (int)CAPWIN_STATUS.CAPWIN_CREATED;
                        end = true;
                    }
                    if ((nRunStatus & (int)CAPWIN_STATUS.REMOCON_CREATED) > 0)
                    {
                        SkyApiClass.DestroyRemoCon();
                        nRunStatus -= (int)CAPWIN_STATUS.REMOCON_CREATED;
                    }
                 }
                catch
                {
                }

                if(end)
                    SkyApiClass.DestroyApiModule((IntPtr)null);
            }
        }

        protected override void WndProc(ref Message m)
        {
            if (m.Msg == m_RmtNotifyMsg)
            {
                m.Result = IntPtr.Zero;
                if ((nRunStatus & (int)CAPWIN_STATUS.CAPWIN_STARTED) > 0)
                {
                    lock (lockObj)
                    {
                        if (3 <= m.WParam.ToInt32() && m.WParam.ToInt32() <= 17)
                            SkyApiClass.BypassRmtData(m.WParam);
                        else if (24 == m.WParam.ToInt32())
                        {
                            try
                            {
                                IntPtr handle = WIN32.FindWindow(null, "SmartSign Player");
                                if (handle != null && handle.ToInt32() > 0)
                                {
                                    if (isDevicePC == true)
                                    {
                                        isDevicePC = false;
                                        WIN32.SendMessage(handle, WM_SYSCOMMAND, SC_TVScreen, 0);
                                    }
                                    else
                                    {
                                        isDevicePC = true;
                                        WIN32.SendMessage(handle, WM_SYSCOMMAND, SC_PCScreen, 0);
                                    }
                                }
                            }
                            catch
                            {
                            }
                        }
                        else
                        {
                        }

                    }
                }
                return;
            }

            base.WndProc(ref m);
        }

        public System.Drawing.Bitmap Thumbnail(int cx, int cy)
        {
            return null;
        }

        public ChannelList GetChannelList(TVComponents.AVSOURCE type, string path)
        {
            if ((nRunStatus & (int)CAPWIN_STATUS.CAPWIN_CREATED) > 0)
            {
                ChannelList list = new ChannelList(path);
                list.Clear();

                try
                {                        
                    int count = 0;
                    SkyApiClass.GetChListCounter(type, out count);

                    MessageBox.Show(String.Format("count: {0}", count));

                    for (int i = 0; i < count; i++)
                    {
                        API_CHITEM item;
                        SkyApiClass.GetChListData(type, i, out item);
                        //MessageBox.Show(String.Format("return: {0}:{1}", i, SkyApiClass.GetChListData(type, i, out item)));
                        Channel ch = new Channel();
                        ch.ChannelName = new string(item.ChName);
                        ch.MajorNum = item.ChMajor;
                        ch.MinorNum = item.ChMinor;
                        ch.SourceType = type;

                        list.SortAddChannel(ch);
                    }
                    return list;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

            }
            return null;
        }
    }
}
