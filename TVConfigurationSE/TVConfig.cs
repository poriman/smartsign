﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace TVConfigurationSE
{
    public partial class TVConfig : Form
    {
        TVComponents.ChannelList _channellist;
        string pathToChannelXMLFile = "";
        TVComponents.Channel channelinfo;
        

        public TVConfig()
        {
            InitializeComponent();
            channelinfo = new TVComponents.Channel();
            this.comboBox1.SelectedIndex = 0;
            pathToChannelXMLFile = AppDomain.CurrentDomain.BaseDirectory + @"settings\ChannelList.xml";
            if (System.IO.File.Exists(pathToChannelXMLFile))
                LoadChannels(pathToChannelXMLFile);


            
        }

        private void but_Play_Click(object sender, EventArgs e)
        {            
            channelinfo.MajorNum = int.Parse(txt_mj.Text);
            channelinfo.MinorNum = int.Parse(txt_mi.Text);            

            this.tvControl2.ChannelInfo = channelinfo;

            tvControl2.Play();
        }

        private void but_Stop_Click(object sender, EventArgs e)
        {
            tvControl2.Stop();
        }

        private void LoadChannels(string filepath)
        {
            _channellist = new TVComponents.ChannelList(pathToChannelXMLFile);
            if (_channellist != null)
            {
                foreach (TVComponents.Channel item in _channellist.GetChannels())
                {
                    DataGridViewRow row = new DataGridViewRow();
                    DataGridViewCell cell1 = new DataGridViewTextBoxCell();
                    DataGridViewCell cell2 = new DataGridViewTextBoxCell();
                    DataGridViewCell cell3 = new DataGridViewTextBoxCell();
                    DataGridViewCell cell4 = new DataGridViewTextBoxCell();
                    cell1.Value = item.MajorNum;
                    cell2.Value = item.MinorNum;
                    cell3.Value = (item.ChannelName != null) ? item.ChannelName : "";
                    cell4.Value = item.SourceType;
                    row.Cells.Add(cell1);
                    row.Cells.Add(cell2);
                    row.Cells.Add(cell3);
                    row.Cells.Add(cell4);
                    this.dataGridChannel.Rows.Add(row);
                }
            }
        }

        private void but_channelList_Click(object sender, EventArgs e)
        {
            dataGridChannel.Rows.Clear();

            _channellist = tvControl2.GetChannelList(channelinfo.SourceType, pathToChannelXMLFile);

            if (_channellist != null)
            {
                foreach (TVComponents.Channel item in _channellist.GetChannels())
                {
                    DataGridViewRow row = new DataGridViewRow();
                    DataGridViewCell cell1 = new DataGridViewTextBoxCell();
                    DataGridViewCell cell2 = new DataGridViewTextBoxCell();
                    DataGridViewCell cell3 = new DataGridViewTextBoxCell();
                    DataGridViewCell cell4 = new DataGridViewTextBoxCell();
                    cell1.Value = item.MajorNum;
                    cell2.Value = item.MinorNum;
                    cell3.Value = (item.ChannelName != null) ? item.ChannelName : "";
                    cell4.Value = item.SourceType;
                    row.Cells.Add(cell1);
                    row.Cells.Add(cell2);
                    row.Cells.Add(cell3);
                    row.Cells.Add(cell4);
                    this.dataGridChannel.Rows.Add(row);
                }
            }
        }

        private void dataGridChannel_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            _channellist.UpdateChannel(e.RowIndex, dataGridChannel.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());
        }

        private void button1_Click(object sender, EventArgs e)
        {
            _channellist.Save();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (this.comboBox1.SelectedIndex)
            {
                case 0: channelinfo.SourceType = TVComponents.AVSOURCE.SRC_ATVC; break;
                case 1: channelinfo.SourceType = TVComponents.AVSOURCE.SRC_DTVC; break;
                case 2: channelinfo.SourceType = TVComponents.AVSOURCE.SRC_DTVA; break;
            }
        }

        UInt32 WM_SYSCOMMAND = 0x0112;
        UInt32 SC_TVSCREEN = 0x0118;
        UInt32 SC_PCSCREEN = 0x0119;
        protected override void WndProc(ref Message m)
        {
            if (m.Msg == WM_SYSCOMMAND)
            {
                if (m.WParam.ToInt32() == SC_TVSCREEN)
                {
                    MessageBox.Show("SC_TVSCREEN");
                    return;
                }
                else if (m.WParam.ToInt32() == SC_PCSCREEN)
                {
                    MessageBox.Show("SC_PCSCREEN");
                    return;
                }
            }

            base.WndProc(ref m);
        }

        [DllImport("user32.dll")]
        public static extern IntPtr FindWindow(string className, string windowName);
        [DllImport("user32.dll")]
        public static extern int SendMessage(IntPtr hwnd, int wMsg, int wParam, int lParam);
        private void button2_Click(object sender, EventArgs e)
        {
            IntPtr handle = FindWindow(null, "SmartSign Player");
            if (handle != null && handle.ToInt32() > 0)
            {
            }
        }

        private void btnChangeSource_Click(object sender, EventArgs e)
        {
            tvControl2.ChangeSource(channelinfo.SourceType);
        }
    }
}
