﻿namespace TVConfigurationSE
{
    partial class TVConfig
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다.
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            TVComponents.Channel channel1 = new TVComponents.Channel();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TVConfig));
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.button1 = new System.Windows.Forms.Button();
            this.but_channelList = new System.Windows.Forms.Button();
            this.but_Stop = new System.Windows.Forms.Button();
            this.but_Play = new System.Windows.Forms.Button();
            this.txt_mj = new System.Windows.Forms.TextBox();
            this.txt_mi = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.dataGridChannel = new System.Windows.Forms.DataGridView();
            this.colChannel = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.minor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colType = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tvControl2 = new TVComponents.TVControl();
            this.btnChangeSource = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridChannel)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.tvControl2);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(993, 538);
            this.splitContainer1.SplitterDistance = 669;
            this.splitContainer1.TabIndex = 1;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.btnChangeSource);
            this.splitContainer2.Panel1.Controls.Add(this.button1);
            this.splitContainer2.Panel1.Controls.Add(this.but_channelList);
            this.splitContainer2.Panel1.Controls.Add(this.but_Stop);
            this.splitContainer2.Panel1.Controls.Add(this.but_Play);
            this.splitContainer2.Panel1.Controls.Add(this.txt_mj);
            this.splitContainer2.Panel1.Controls.Add(this.txt_mi);
            this.splitContainer2.Panel1.Controls.Add(this.label1);
            this.splitContainer2.Panel1.Controls.Add(this.comboBox1);
            this.splitContainer2.Panel1.Controls.Add(this.label3);
            this.splitContainer2.Panel1.Controls.Add(this.label2);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.dataGridChannel);
            this.splitContainer2.Size = new System.Drawing.Size(320, 538);
            this.splitContainer2.SplitterDistance = 213;
            this.splitContainer2.TabIndex = 6;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(195, 172);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(108, 26);
            this.button1.TabIndex = 9;
            this.button1.Text = "채널 리스트 저장";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // but_channelList
            // 
            this.but_channelList.Location = new System.Drawing.Point(72, 172);
            this.but_channelList.Name = "but_channelList";
            this.but_channelList.Size = new System.Drawing.Size(117, 26);
            this.but_channelList.TabIndex = 8;
            this.but_channelList.Text = "채널 리스트 검색";
            this.but_channelList.UseVisualStyleBackColor = true;
            this.but_channelList.Click += new System.EventHandler(this.but_channelList_Click);
            // 
            // but_Stop
            // 
            this.but_Stop.Location = new System.Drawing.Point(228, 140);
            this.but_Stop.Name = "but_Stop";
            this.but_Stop.Size = new System.Drawing.Size(75, 26);
            this.but_Stop.TabIndex = 7;
            this.but_Stop.Text = "Stop";
            this.but_Stop.UseVisualStyleBackColor = true;
            this.but_Stop.Click += new System.EventHandler(this.but_Stop_Click);
            // 
            // but_Play
            // 
            this.but_Play.Location = new System.Drawing.Point(228, 108);
            this.but_Play.Name = "but_Play";
            this.but_Play.Size = new System.Drawing.Size(75, 26);
            this.but_Play.TabIndex = 6;
            this.but_Play.Text = "Play";
            this.but_Play.UseVisualStyleBackColor = true;
            this.but_Play.Click += new System.EventHandler(this.but_Play_Click);
            // 
            // txt_mj
            // 
            this.txt_mj.Location = new System.Drawing.Point(182, 50);
            this.txt_mj.Name = "txt_mj";
            this.txt_mj.Size = new System.Drawing.Size(121, 21);
            this.txt_mj.TabIndex = 4;
            this.txt_mj.Text = "11";
            // 
            // txt_mi
            // 
            this.txt_mi.Location = new System.Drawing.Point(182, 77);
            this.txt_mi.Name = "txt_mi";
            this.txt_mi.Size = new System.Drawing.Size(121, 21);
            this.txt_mi.TabIndex = 5;
            this.txt_mi.Text = "0";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 12);
            this.label1.TabIndex = 0;
            this.label1.Text = "타입";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "ATV(케이블)",
            "DTV(케이블)",
            "DTV(공중파)"});
            this.comboBox1.Location = new System.Drawing.Point(93, 14);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(210, 20);
            this.comboBox1.TabIndex = 1;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 81);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 12);
            this.label3.TabIndex = 3;
            this.label3.Text = "마이너 채널";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 12);
            this.label2.TabIndex = 2;
            this.label2.Text = "메이저 채널";
            // 
            // dataGridChannel
            // 
            this.dataGridChannel.AllowUserToAddRows = false;
            this.dataGridChannel.AllowUserToDeleteRows = false;
            this.dataGridChannel.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridChannel.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colChannel,
            this.minor,
            this.colName,
            this.colType});
            this.dataGridChannel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridChannel.Location = new System.Drawing.Point(0, 0);
            this.dataGridChannel.MultiSelect = false;
            this.dataGridChannel.Name = "dataGridChannel";
            this.dataGridChannel.RowHeadersWidth = 20;
            this.dataGridChannel.RowTemplate.Height = 23;
            this.dataGridChannel.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridChannel.Size = new System.Drawing.Size(320, 321);
            this.dataGridChannel.TabIndex = 4;
            this.dataGridChannel.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridChannel_CellEndEdit);
            // 
            // colChannel
            // 
            this.colChannel.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.colChannel.HeaderText = "CH.";
            this.colChannel.MaxInputLength = 4;
            this.colChannel.MinimumWidth = 10;
            this.colChannel.Name = "colChannel";
            this.colChannel.ReadOnly = true;
            this.colChannel.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.colChannel.Width = 50;
            // 
            // minor
            // 
            this.minor.HeaderText = "CH-minor";
            this.minor.MaxInputLength = 2;
            this.minor.Name = "minor";
            this.minor.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.minor.Width = 30;
            // 
            // colName
            // 
            this.colName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.colName.HeaderText = "이름";
            this.colName.MaxInputLength = 30;
            this.colName.Name = "colName";
            this.colName.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // colType
            // 
            this.colType.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.None;
            this.colType.HeaderText = "타입";
            this.colType.Name = "colType";
            this.colType.ReadOnly = true;
            this.colType.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.colType.Width = 60;
            // 
            // tvControl2
            // 
            this.tvControl2.BackColor = System.Drawing.Color.Black;
            channel1.ChannelName = "No Name";
            channel1.MajorNum = 0;
            channel1.MinorNum = 0;
            channel1.SourceType = TVComponents.AVSOURCE.SRC_ATVC;
            this.tvControl2.ChannelInfo = channel1;
            this.tvControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tvControl2.Location = new System.Drawing.Point(0, 0);
            this.tvControl2.Name = "tvControl2";
            this.tvControl2.Size = new System.Drawing.Size(669, 538);
            this.tvControl2.TabIndex = 1;
            // 
            // btnChangeSource
            // 
            this.btnChangeSource.Location = new System.Drawing.Point(72, 140);
            this.btnChangeSource.Name = "btnChangeSource";
            this.btnChangeSource.Size = new System.Drawing.Size(117, 26);
            this.btnChangeSource.TabIndex = 10;
            this.btnChangeSource.Text = "Change Source";
            this.btnChangeSource.UseVisualStyleBackColor = true;
            this.btnChangeSource.Click += new System.EventHandler(this.btnChangeSource_Click);
            // 
            // TVConfig
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(993, 538);
            this.Controls.Add(this.splitContainer1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "TVConfig";
            this.Text = "TV Setting";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel1.PerformLayout();
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridChannel)).EndInit();
            this.ResumeLayout(false);

        }
        #endregion
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.Button but_channelList;
        private System.Windows.Forms.Button but_Stop;
        private System.Windows.Forms.Button but_Play;
        private System.Windows.Forms.TextBox txt_mj;
        private System.Windows.Forms.TextBox txt_mi;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView dataGridChannel;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridViewTextBoxColumn colChannel;
        private System.Windows.Forms.DataGridViewTextBoxColumn minor;
        private System.Windows.Forms.DataGridViewTextBoxColumn colName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colType;
        private TVComponents.TVControl tvControl2;
        private System.Windows.Forms.Button btnChangeSource;
    }
}

