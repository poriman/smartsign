﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Odbc;
using System.Linq;
using System.Text;
using NLog;

namespace DigitalSignage.ServerDatabase
{
	public class DBLayer
	{
        public enum DBTypeInfo
        {
            MSSQL,
            MYSQL,
            ORACLE,
            SQLITE,
            ETC
        }

        public bool IsRunningSystem = false;

		private static Logger logger = LogManager.GetCurrentClassLogger();

		private OdbcConnection connection = null;
		private string conn_string = "";
		private string log_conn_string = "";
		private string dataPath = "";

        /// <summary>
        /// 풀 세션 타임 아웃 (기본 30분 제한)
        /// </summary>
        public int PoolTimeout = 1800000;

        /// <summary>
        /// 세션을 빌리는 제한 시간 (기본 30분 제한)
        /// </summary>
        public int RentTimeout = 1800000;
        
        /// <summary>
        /// 빌리는 제한 시간 체크
        /// </summary>
        private System.ComponentModel.BackgroundWorker bwRentTimeChecker = null;

		public string ConnectionString { get { return conn_string; } }
		public string LogConnectionString { get { return log_conn_string; } }

		private int nConnectionPoolCount = 0;
		private int nLogConnectionPoolCount = 0;

		public string LogPath
		{
			get { return dataPath; }
			set { dataPath = value; }
		}

        private DBTypeInfo _tpdb = DBTypeInfo.SQLITE;
        private DBTypeInfo _tplogdb = DBTypeInfo.SQLITE;


        public DBTypeInfo DBType
        {
            get
            {
                return _tpdb;
            }
        }

        public DBTypeInfo LogDBType
        {
            get
            {
                return _tplogdb;
            }
        }

		#region Connection Pool
		private Collection<OdbcConnection> arrConnections = null;

		private Collection<OdbcConnection> arrLogConnections = null;

        private Dictionary<OdbcConnection, DateTime> dicUsedConnections = new Dictionary<OdbcConnection, DateTime>();

        private readonly object lockConnection = new object();

		private readonly object lockAdd = new object();


		/// <summary>
		/// 새로운 커넥션 생성
		/// </summary>
		/// <returns></returns>
		public OdbcConnection GetNewConnection()
		{
			OdbcConnection con = null;
			try
			{
				OdbcFactory fact = OdbcFactory.Instance;
				con = (OdbcConnection)fact.CreateConnection();
				con.ConnectionString = conn_string;
				con.StateChange += new System.Data.StateChangeEventHandler(con_StateChange);
				con.Open();
			}
			catch (Exception e)
			{
				logger.Error(e.ToString());
				try
				{
					con.Dispose();
				}
				catch { }
				finally
				{
					con = null;
				}
			}
			return con;
		}

        /// <summary>
        /// 새로운 커넥션 생성
        /// </summary>
        /// <returns></returns>
        public OdbcConnection GetNewLogConnection()
		{
			OdbcConnection con = null;
			try
			{
				OdbcFactory fact = OdbcFactory.Instance;
				con = (OdbcConnection)fact.CreateConnection();
				con.ConnectionString = log_conn_string;
                con.StateChange += new System.Data.StateChangeEventHandler(con_LogStateChange);
				con.Open();
			}
			catch (Exception e)
			{
				logger.Error(e.ToString());
				try
				{
					con.Dispose();
				}
				catch { }
				finally
				{
					con = null;
				}
			}
			return con;
		}

        void con_StateChange(object sender, System.Data.StateChangeEventArgs e)
        {

            lock (lockConnection)
            {
                try
                {
                    switch (e.CurrentState)
                    {
                        case System.Data.ConnectionState.Connecting:
                        case System.Data.ConnectionState.Executing:
                        case System.Data.ConnectionState.Fetching:
                            break;
                        case System.Data.ConnectionState.Broken:
                        case System.Data.ConnectionState.Closed:
                            dicUsedConnections.Remove(sender as OdbcConnection);
                            arrConnections.Remove(sender as OdbcConnection);
                            break;
                        case System.Data.ConnectionState.Open:
                            break;
                    }

					if (arrConnections.Count > nConnectionPoolCount - 5)
						logger.Debug("Current Connections:  {0}", arrConnections.Count);

                }
                catch (Exception ex) { logger.Error(ex.Message); }
            }
        }


        void con_LogStateChange(object sender, System.Data.StateChangeEventArgs e)
        {
			lock (lockConnection)
            {
				try
				{
					switch (e.CurrentState)
					{
						case System.Data.ConnectionState.Connecting:
						case System.Data.ConnectionState.Executing:
						case System.Data.ConnectionState.Fetching:
							break;
						case System.Data.ConnectionState.Broken:
						case System.Data.ConnectionState.Closed:
							dicUsedConnections.Remove(sender as OdbcConnection);
							arrLogConnections.Remove(sender as OdbcConnection);
							break;
						case System.Data.ConnectionState.Open:
							break;
					}

					if(arrLogConnections.Count > nLogConnectionPoolCount - 5)
						logger.Debug("Current Log Connections:  {0}", arrLogConnections.Count);



                }
                catch (Exception ex) { logger.Error(ex.Message); }
            }
        }

		/// <summary>
		/// 사용가능한 로그 커넥션 반환
		/// </summary>
		/// <returns></returns>
		public OdbcConnection GetAvailableLogConnection()
		{
			int nNow = 0;
			while (nNow < PoolTimeout)
			{
				lock (lockAdd)
				{
					if (nLogConnectionPoolCount == 0 || arrLogConnections.Count < nLogConnectionPoolCount)
					{
						OdbcConnection conn = GetNewLogConnection();
						if (conn != null)
						{
							lock (lockConnection)
							{
								arrLogConnections.Add(conn);
								dicUsedConnections.Add(conn, DateTime.Now);
								return conn;
							}
						}
					}
				}
				System.Threading.Thread.Sleep(10);
				nNow += 10;
			}

			logger.Error("Making Log Connection Error: Timeouted!");

			return null;
		}

		/// <summary>
		/// 사용가능한 커넥션 반환
		/// </summary>
		/// <returns></returns>
		public OdbcConnection GetAvailableConnection()
		{
			int nNow = 0;
			while (nNow < PoolTimeout)
			{
				lock (lockAdd)
				{
					if (nConnectionPoolCount == 0 || arrConnections.Count < nConnectionPoolCount)
					{
						OdbcConnection conn = GetNewConnection();
						if (conn != null)
						{
							lock (lockConnection)
							{
								arrConnections.Add(conn);
								dicUsedConnections.Add(conn, DateTime.Now);
								return conn;
							}
						}
					}
				}
				System.Threading.Thread.Sleep(10);
				nNow += 10;
			}

			logger.Error("Making Connection Error: Timeouted!");

			return null;
		}

		/// <summary>
		/// Connection을 몇개 전달인자 만큼 만든다.
		/// </summary>
		/// <param name="nConnectionCount">커넥션의 개수</param>
		public bool InitializeConnectionPool(int nConnectionCount)
		{
			IsRunningSystem = true;

			logger.Info("Openning connections.. amount " + nConnectionCount.ToString());

			if (String.IsNullOrEmpty(conn_string) || arrConnections != null )
				return false;

			arrConnections = new Collection<OdbcConnection>();
			arrLogConnections = new Collection<OdbcConnection>();
  
            dicUsedConnections = new Dictionary<OdbcConnection, DateTime>();

			bool bRet = false;

			#region Connections
			try
			{
				OdbcFactory fact = OdbcFactory.Instance;

				using (OdbcConnection con = this.GetNewConnection())
				{

					try
					{
						String driver = con.Driver.ToLower();
						logger.Info("ODBC DB Driver: " + driver);
						if (driver.Contains("sqlite")) _tpdb = DBTypeInfo.SQLITE;
						else if (driver.Contains("msorc") || driver.Contains("ora")) _tpdb = DBTypeInfo.ORACLE;
						else if (driver.Contains("myodbc")) _tpdb = DBTypeInfo.MYSQL;
						else if (driver.Contains("sqlsrv") || driver.Contains("sqlncl")) _tpdb = DBTypeInfo.MSSQL;
						else _tpdb = DBTypeInfo.ETC;
					}
					catch { _tpdb = DBTypeInfo.ETC; }

					con.Close();
				}

				bRet = true;
			}
			catch (Exception e)
			{
				logger.Error(e.ToString());
				bRet = false;

				return false;
			}
			#endregion

			#region Log Connections
			try
			{
				OdbcFactory fact = OdbcFactory.Instance;

				using (OdbcConnection con = this.GetNewLogConnection())
				{
					try
					{
						String driver = con.Driver.ToLower();
						logger.Info("ODBC Log DB Driver: " + driver);
						if (driver.Contains("sqlite")) _tplogdb = DBTypeInfo.SQLITE;
						else if (driver.Contains("msorc") || driver.Contains("ora")) _tplogdb = DBTypeInfo.ORACLE;
						else if (driver.Contains("myodbc")) _tplogdb = DBTypeInfo.MYSQL;
						else if (driver.Contains("sqlsrv") || driver.Contains("sqlncl")) _tplogdb = DBTypeInfo.MSSQL;
						else _tplogdb = DBTypeInfo.ETC;
					}
					catch { _tplogdb = DBTypeInfo.ETC; }
					
					con.Close();
				}

				bRet = true;
 			}
			catch (Exception e)
			{
				logger.Error(e.ToString()); 
				bRet = false;

				return false;
			}
			#endregion

            logger.Info("ODBC DB Type: " + _tpdb.ToString());
            logger.Info("ODBC Log DB Type: " + _tplogdb.ToString());

            try
            {
                if (bwRentTimeChecker == null)
                {
                    bwRentTimeChecker = new System.ComponentModel.BackgroundWorker();
                    bwRentTimeChecker.DoWork += new System.ComponentModel.DoWorkEventHandler(bwRentTimeChecker_DoWork);
                    bwRentTimeChecker.WorkerSupportsCancellation = true;
                    bwRentTimeChecker.RunWorkerAsync();
                }
            }
            catch { }

			if (bRet)
			{
				nLogConnectionPoolCount = nConnectionPoolCount = 
					(nConnectionCount == 0 ? 0 : (nConnectionCount < 5 ? 5 : nConnectionCount));

				logger.Info("Connection pool.. Success!");
			}

			return bRet;
		}

        void bwRentTimeChecker_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            logger.Info("Connection Pool Rent Timeout Checker Started!!");
            while (!bwRentTimeChecker.CancellationPending)
            {
                try
                {
                    System.Threading.Thread.Sleep(5000);

					lock (lockConnection)
					{
						DateTime dtNow = DateTime.Now;
						List<OdbcConnection> arrRemove = new List<OdbcConnection>();

						foreach (OdbcConnection conn in dicUsedConnections.Keys)
						{
							try
							{
								if ((dtNow - dicUsedConnections[conn]).TotalMilliseconds > RentTimeout)
								{
									arrRemove.Add(conn);
								}
							}
							catch { }
						}
						foreach (OdbcConnection conn in arrRemove)
						{
							logger.Error("Connection Rent Time is timeouted!");
							dicUsedConnections.Remove(conn);
							conn.Close();
						}
					}
						
                }
                catch (Exception ex) { logger.Error(ex.ToString()); }

			}

			logger.Info("Connection Pool Rent Timeout Checker Ended!!");

        }

        public void Reconnect()
        {
            for (int i = arrConnections.Count - 1; i >= 0 ; --i)
            {
                try
                {
                    OdbcConnection con = arrConnections[i];
                    con.Close();
					con.Dispose();
                }
                catch (Exception e)
                {
                    logger.Error(e.ToString());
                }
            }

			for (int i = arrLogConnections.Count - 1; i >= 0; --i)
			{
                try
                {
                    OdbcConnection con = arrLogConnections[i];
                    con.Close();
                    con.Dispose();
                }
                catch (Exception e)
                {
                    logger.Error(e.ToString());
                }
            }
        }

		public bool CloseConnectionPool()
		{
			logger.Info("Closing connections");
            IsRunningSystem = false;

            try
            {
                if(bwRentTimeChecker.IsBusy)
                    bwRentTimeChecker.CancelAsync();

                bwRentTimeChecker.Dispose();
            }
            catch { }
            finally { bwRentTimeChecker = null; }


			for (int i = arrConnections.Count - 1; i >= 0; --i)
			{
				try
				{
					OdbcConnection con = arrConnections[i];
					con.Close();
					con.Dispose();
				}
				catch (Exception e)
				{
					logger.Error(e.ToString());
				}
			}

			for (int i = arrLogConnections.Count - 1; i >= 0; --i)
			{
				try
				{
					OdbcConnection con = arrLogConnections[i];
					con.Close();
					con.Dispose();
				}
				catch (Exception e)
				{
					logger.Error(e.ToString());
				}
			}

			arrConnections.Clear();
			arrConnections = null;


			arrLogConnections.Clear();
			arrLogConnections = null;

            dicUsedConnections.Clear();
            dicUsedConnections = null;

			return true;

		}
		#endregion

		public int openDB(OdbcConnection con)
		{
			try
			{

				//// configuring log output
				//FileTarget target = new FileTarget();
				//target.Layout = "${longdate}\t[${level}]\t[${callsite}]\t${message}";
				//target.FileName = dataPath + "logfiledb.txt";
				//target.ArchiveFileName = dataPath + "archives/logdb.{#####}.txt";
				//target.MaxArchiveFiles = 31;
				//target.ArchiveEvery = FileArchivePeriod.Day;// FileTarget.ArchiveEveryMode.Day;

				//target.ArchiveNumbering = ArchiveNumberingMode.Sequence;// FileTarget.ArchiveNumberingMode.Sequence;
				//// this speeds up things when no other processes are writing to the file
				//target.ConcurrentWrites = true;

				//NLog.Config.SimpleConfigurator.ConfigureForTargetLogging(target, LogLevel.Debug);

				logger.Info("Database starting");
				// 
				connection = con;

				logger.Info(connection.ConnectionString);

				try
				{
					using (OdbcCommand sql = new OdbcCommand("SELECT * FROM tasks", connection))
					{
						//initializing database structure
						sql.ExecuteNonQuery();
					}
					return 0;
				}
				catch (Exception e)
				{
					logger.Warn(e + "");
				}

			}
			catch (Exception e)
			{
				logger.Error(e + "");
			}
			return 1;
		}

		public int openDB(string dsn)
		{
			return openDB(dsn, dsn);
		}

		public int openDB(string dsn, string log_dsn)
		{
			try
			{
				OdbcFactory fact = OdbcFactory.Instance;

				connection = (OdbcConnection)fact.CreateConnection();

				conn_string = dsn;
				log_conn_string = log_dsn;

				connection.ConnectionString = dsn;
				logger.Info(conn_string);

				connection.Open();

				return openDB(connection);
			}
			catch (Exception e)
			{
				logger.Error(e + "");
			}
			return 1;

		}

		public int closeDB()
		{
			connection.Close();
			connection = null;

			return 0;
		}

		public ServerDataSetTableAdapters.role_perm_ASNTableAdapter Createrole_perm_ASN_DA(OdbcConnection conn)
		{
			ServerDataSetTableAdapters.role_perm_ASNTableAdapter da =
				new ServerDataSetTableAdapters.role_perm_ASNTableAdapter();
			da.Connection = (conn == null ? GetAvailableConnection() : conn);
            da.Disposed += new EventHandler(da_Disposed);
			return da;
		}

		public ServerDataSetTableAdapters.role_perm_ASNTableAdapter Createrole_perm_ASN_DA()
		{
			return Createrole_perm_ASN_DA(null);
		}

		public ServerDataSetTableAdapters.groupsTableAdapter CreateGroup_DA(OdbcConnection conn)
		{
			ServerDataSetTableAdapters.groupsTableAdapter da =
				new ServerDataSetTableAdapters.groupsTableAdapter();
			da.Connection = (conn == null ? GetAvailableConnection() : conn);
            da.Disposed += new EventHandler(da_Disposed);
            return da;
		}

		public ServerDataSetTableAdapters.groupsTableAdapter CreateGroup_DA()
		{
			return CreateGroup_DA(null);
		}

		public ServerDataSetTableAdapters.playersTableAdapter CreatePlayer_DA(OdbcConnection conn)
		{
			ServerDataSetTableAdapters.playersTableAdapter da =
				new ServerDataSetTableAdapters.playersTableAdapter();
			da.Connection = (conn == null ? GetAvailableConnection() : conn);
            da.Disposed += new EventHandler(da_Disposed);
            return da;
		}

		public ServerDataSetTableAdapters.playersTableAdapter CreatePlayer_DA()
		{
			return CreatePlayer_DA(null);
		}

		public ServerDataSetTableAdapters.players_detail_ASNTableAdapter CreatePlayerDetailASN_DA(OdbcConnection conn)
		{
			ServerDataSetTableAdapters.players_detail_ASNTableAdapter da =
				new ServerDataSetTableAdapters.players_detail_ASNTableAdapter();
			da.Connection = (conn == null ? GetAvailableConnection() : conn);
            da.Disposed += new EventHandler(da_Disposed);
            return da;
		}

		public ServerDataSetTableAdapters.players_detail_ASNTableAdapter CreatePlayerDetailASN_DA()
		{
			return CreatePlayerDetailASN_DA(null);
		}

		public ServerDataSetTableAdapters.usersTableAdapter CreateUser_DA(OdbcConnection conn)
		{
			ServerDataSetTableAdapters.usersTableAdapter da =
				new ServerDataSetTableAdapters.usersTableAdapter();
			da.Connection = (conn == null ? GetAvailableConnection() : conn);
            da.Disposed += new EventHandler(da_Disposed);
            return da;
		}

		public ServerDataSetTableAdapters.usersTableAdapter CreateUser_DA()
		{
			return CreateUser_DA(null);
		}

		public ServerDataSetTableAdapters.rolesTableAdapter CreateRole_DA(OdbcConnection conn)
		{
			ServerDataSetTableAdapters.rolesTableAdapter da =
				new ServerDataSetTableAdapters.rolesTableAdapter();
			da.Connection = (conn == null ? GetAvailableConnection() : conn);
            da.Disposed += new EventHandler(da_Disposed);
            return da;
		}

		public ServerDataSetTableAdapters.rolesTableAdapter CreateRole_DA()
		{
			return CreateRole_DA(null);
		}

		public ServerDataSetTableAdapters.user_group_ASNTableAdapter CreateUser_GroupASN_DA(OdbcConnection conn)
		{
			ServerDataSetTableAdapters.user_group_ASNTableAdapter da =
				new ServerDataSetTableAdapters.user_group_ASNTableAdapter();
			da.Connection = (conn == null ? GetAvailableConnection() : conn);
            da.Disposed += new EventHandler(da_Disposed);
            return da;
		}

		public ServerDataSetTableAdapters.user_group_ASNTableAdapter CreateUser_GroupASN_DA()
		{
			return CreateUser_GroupASN_DA(null);
		}

		public ServerDataSetTableAdapters.user_player_ASNTableAdapter CreateUser_PlayerASN_DA(OdbcConnection conn)
		{
			ServerDataSetTableAdapters.user_player_ASNTableAdapter da =
				new ServerDataSetTableAdapters.user_player_ASNTableAdapter();
			da.Connection = (conn == null ? GetAvailableConnection() : conn);
            da.Disposed += new EventHandler(da_Disposed);
            return da;
		}

		public ServerDataSetTableAdapters.user_player_ASNTableAdapter CreateUser_PlayerASN_DA()
		{
			return CreateUser_PlayerASN_DA(null);
		}

		public ServerDataSetTableAdapters.tasksTableAdapter CreateTaskDA(OdbcConnection conn)
		{
			ServerDataSetTableAdapters.tasksTableAdapter da =
				new ServerDataSetTableAdapters.tasksTableAdapter();
			da.Connection = (conn == null ? GetAvailableConnection() : conn);
            da.Disposed += new EventHandler(da_Disposed);
            return da;
		}

		public ServerDataSetTableAdapters.tasksTableAdapter CreateTaskDA()
		{
			return CreateTaskDA(null);
		}

		public ServerDataSetTableAdapters.screensTableAdapter CreateScreenDA(OdbcConnection conn)
		{
			ServerDataSetTableAdapters.screensTableAdapter da =
				new ServerDataSetTableAdapters.screensTableAdapter();
			da.Connection = (conn == null ? GetAvailableConnection() : conn);
            da.Disposed += new EventHandler(da_Disposed);
            return da;
		}

		public ServerDataSetTableAdapters.screensTableAdapter CreateScreenDA()
		{
			return CreateScreenDA(null);
		}

		public ServerDataSetTableAdapters.ScreenTaskASNTableAdapter CreateScreenTaskASN_DA(OdbcConnection conn)
		{
			ServerDataSetTableAdapters.ScreenTaskASNTableAdapter da =
				new ServerDataSetTableAdapters.ScreenTaskASNTableAdapter();
			da.Connection = (conn == null ? GetAvailableConnection() : conn);
            da.Disposed += new EventHandler(da_Disposed);
            return da;
		}

		public ServerDataSetTableAdapters.ScreenTaskASNTableAdapter CreateScreenTaskASN_DA()
		{
			return CreateScreenTaskASN_DA(null);
		}

		public ServerDataSetTableAdapters.downloadmasterTableAdapter CreateDownloadMasterDA(OdbcConnection conn)
		{
			ServerDataSetTableAdapters.downloadmasterTableAdapter da =
				new ServerDataSetTableAdapters.downloadmasterTableAdapter();
			da.Connection = (conn == null ? GetAvailableConnection() : conn);
            da.Disposed += new EventHandler(da_Disposed);
            return da;
		}

		public ServerDataSetTableAdapters.DownloadedScheduleInfoTableAdapter CreateDownloadedSchedInfoDA()
		{
			return CreateDownloadedSchedInfoDA(null);
		}
		public ServerDataSetTableAdapters.DownloadedScheduleInfoTableAdapter CreateDownloadedSchedInfoDA(OdbcConnection conn)
		{
			ServerDataSetTableAdapters.DownloadedScheduleInfoTableAdapter da =
				new ServerDataSetTableAdapters.DownloadedScheduleInfoTableAdapter();
			da.Connection = (conn == null ? GetAvailableConnection() : conn);
            da.Disposed += new EventHandler(da_Disposed);
            return da;
		}

		public ServerDataSetTableAdapters.downloadmasterTableAdapter CreateDownloadMasterDA()
		{
			return CreateDownloadMasterDA(null);
		}


        public ServerDataSetTableAdapters.DataInfoTableAdapter CreateDataInfoConnectDA(OdbcConnection conn)
        {
            ServerDataSetTableAdapters.DataInfoTableAdapter da =
                new ServerDataSetTableAdapters.DataInfoTableAdapter();
            da.Connection = (conn == null ? GetAvailableConnection() : conn);
            da.Disposed += new EventHandler(da_Disposed);
            return da;

        }

        public ServerDataSetTableAdapters.DataInfoTableAdapter CreateDataInfoConnectDA()
        {
            return CreateDataInfoConnectDA(null);
        }

        public ServerDataSetTableAdapters.Data_FieldTableAdapter CreateDataFieldConnectDA(OdbcConnection conn)
        {
            ServerDataSetTableAdapters.Data_FieldTableAdapter da =
                new ServerDataSetTableAdapters.Data_FieldTableAdapter();
            da.Connection = (conn == null ? GetAvailableConnection() : conn);
            da.Disposed += new EventHandler(da_Disposed);
            return da;
        }

        public ServerDataSetTableAdapters.Data_FieldTableAdapter CreateDataFieldConnectDA()
        {
            return CreateDataFieldConnectDA(null);
        }

        public ServerDataSetTableAdapters.ContentsTableAdapter CreateContentsConnectDA(OdbcConnection conn)
        {
            ServerDataSetTableAdapters.ContentsTableAdapter da =
                new ServerDataSetTableAdapters.ContentsTableAdapter();
            da.Connection = (conn == null ? GetAvailableConnection() : conn);
            da.Disposed += new EventHandler(da_Disposed);
            return da;
        }

        public ServerDataSetTableAdapters.ContentsTableAdapter CreateContentsConnectDA()
        {
            return CreateContentsConnectDA(null);
        }


        public ServerDataSetTableAdapters.resultdatasTableAdapter CreateResultDataConnectDA(OdbcConnection conn)
        {
            ServerDataSetTableAdapters.resultdatasTableAdapter da =
                new ServerDataSetTableAdapters.resultdatasTableAdapter();
            da.Connection = (conn == null ? GetAvailableConnection() : conn);
            da.Disposed += new EventHandler(da_Disposed);
            return da;
        }

        public ServerDataSetTableAdapters.resultdatasTableAdapter CreateResultDataConnectDA()
        {
            return CreateResultDataConnectDA(null);
        }

		public ServerDataSetTableAdapters.LimitPlayersTableAdapter CreateLimitPlayersDA(OdbcConnection conn)
		{
			ServerDataSetTableAdapters.LimitPlayersTableAdapter da =
				new ServerDataSetTableAdapters.LimitPlayersTableAdapter();
			da.Connection = (conn == null ? GetAvailableConnection() : conn);
            da.Disposed += new EventHandler(da_Disposed);
            return da;
		}

		public ServerDataSetTableAdapters.LimitPlayersTableAdapter CreateLimitPlayersDA()
		{
			return CreateLimitPlayersDA(null);
		}

		public ServerDataSetTableAdapters.DownloadTime_Groups_ASNTableAdapter CreateDTGroupsAsnDA(OdbcConnection conn)
		{
			ServerDataSetTableAdapters.DownloadTime_Groups_ASNTableAdapter da =
				new ServerDataSetTableAdapters.DownloadTime_Groups_ASNTableAdapter();
			da.Connection = (conn == null ? GetAvailableConnection() : conn);
            da.Disposed += new EventHandler(da_Disposed);
            return da;
		}

		public ServerDataSetTableAdapters.DownloadTime_Groups_ASNTableAdapter CreateDTGroupsAsnDA()
		{
			return CreateDTGroupsAsnDA(null);
		}

		public ServerDataSetTableAdapters.DownloadTime_Players_ASNTableAdapter CreateDTPlayersAsnDA(OdbcConnection conn)
		{
			ServerDataSetTableAdapters.DownloadTime_Players_ASNTableAdapter da =
				new ServerDataSetTableAdapters.DownloadTime_Players_ASNTableAdapter();
			da.Connection = (conn == null ? GetAvailableConnection() : conn);
            da.Disposed += new EventHandler(da_Disposed);
            return da;
		}

		public ServerDataSetTableAdapters.DownloadTime_Players_ASNTableAdapter CreateDTPlayersAsnDA()
		{
			return CreateDTPlayersAsnDA(null);
		}

		public ServerDataSetTableAdapters.playerdetailinfoTableAdapter CreatePlayerDetailInfoDA(OdbcConnection conn)
		{
			ServerDataSetTableAdapters.playerdetailinfoTableAdapter da =
				new ServerDataSetTableAdapters.playerdetailinfoTableAdapter();
			da.Connection = (conn == null ? GetAvailableConnection() : conn);
            da.Disposed += new EventHandler(da_Disposed);
            return da;
		}

		public ServerDataSetTableAdapters.playerdetailinfoTableAdapter CreatePlayerDetailInfoDA()
		{
			return CreatePlayerDetailInfoDA(null);
		}

		public ServerDataSetTableAdapters.meta_tag_groupsTableAdapter CreateMetaTagGroupInfoDA(OdbcConnection conn)
		{
			ServerDataSetTableAdapters.meta_tag_groupsTableAdapter da =
				new ServerDataSetTableAdapters.meta_tag_groupsTableAdapter();
			da.Connection = (conn == null ? GetAvailableConnection() : conn);
            da.Disposed += new EventHandler(da_Disposed);
            return da;
		}
	
		public ServerDataSetTableAdapters.meta_tag_groupsTableAdapter CreateMetaTagGroupInfoDA()
		{
			return CreateMetaTagGroupInfoDA(null);
		}

		public ServerDataSetTableAdapters.meta_tagsTableAdapter CreateMetaTagsInfoDA(OdbcConnection conn)
		{
			ServerDataSetTableAdapters.meta_tagsTableAdapter da =
				new ServerDataSetTableAdapters.meta_tagsTableAdapter();
			da.Connection = (conn == null ? GetAvailableConnection() : conn);
            da.Disposed += new EventHandler(da_Disposed);
            return da;
		}	
	
		public ServerDataSetTableAdapters.meta_tagsTableAdapter CreateMetaTagsInfoDA()
		{
			return CreateMetaTagsInfoDA(null);
		}

		public ServerDataSetTableAdapters.timestampsTableAdapter CreateTimestampsInfoDA(OdbcConnection conn)
		{
			ServerDataSetTableAdapters.timestampsTableAdapter da =
				new ServerDataSetTableAdapters.timestampsTableAdapter();
			da.Connection = (conn == null ? GetAvailableConnection() : conn);
            da.Disposed += new EventHandler(da_Disposed);
            return da;
		}

		public ServerDataSetTableAdapters.timestampsTableAdapter CreateTimestampsInfoDA()
		{
			return CreateTimestampsInfoDA(null);
		}

		public ServerDataSetTableAdapters.ConnectionLogsTableAdapter CreateConnectionLogsInfoDA(OdbcConnection conn)
		{
			ServerDataSetTableAdapters.ConnectionLogsTableAdapter da =
				new ServerDataSetTableAdapters.ConnectionLogsTableAdapter();

			da.Connection = (conn == null ? GetAvailableLogConnection() : conn);
            da.Disposed += new EventHandler(da_Disposed);
            return da;
		}

		public ServerDataSetTableAdapters.ConnectionLogsTableAdapter CreateConnectionLogsInfoDA(bool bIsSelect)
		{
			ServerDataSetTableAdapters.ConnectionLogsTableAdapter da =
				new ServerDataSetTableAdapters.ConnectionLogsTableAdapter();

			da.Connection = bIsSelect ? GetAvailableConnection() : GetAvailableLogConnection();
            da.Disposed += new EventHandler(da_Disposed);
            return da;
		}

		public ServerDataSetTableAdapters.ConnectionLogsTableAdapter CreateConnectionLogsInfoDA()
		{
			return CreateConnectionLogsInfoDA(null);
		}

		public ServerDataSetTableAdapters.PlayLogsTableAdapter CreatePlayLogsInfoDA(OdbcConnection conn)
		{
			ServerDataSetTableAdapters.PlayLogsTableAdapter da =
				new ServerDataSetTableAdapters.PlayLogsTableAdapter();

			da.Connection = (conn == null ? GetAvailableLogConnection() : conn);
            da.Disposed += new EventHandler(da_Disposed);
            return da;
		}

		public ServerDataSetTableAdapters.PlayLogsTableAdapter CreatePlayLogsInfoDA(bool bIsSelect)
		{
			ServerDataSetTableAdapters.PlayLogsTableAdapter da =
				new ServerDataSetTableAdapters.PlayLogsTableAdapter();

			da.Connection = bIsSelect ? GetAvailableConnection() : GetAvailableLogConnection();
            da.Disposed += new EventHandler(da_Disposed);
            return da;
		}

		public ServerDataSetTableAdapters.PlayLogsTableAdapter CreatePlayLogsInfoDA()
		{
			return CreatePlayLogsInfoDA(null);
		}

		public ServerDataSetTableAdapters.DownloadLogsTableAdapter CreateDownloadLogsInfoDA(OdbcConnection conn)
		{
			ServerDataSetTableAdapters.DownloadLogsTableAdapter da =
				new ServerDataSetTableAdapters.DownloadLogsTableAdapter();

			da.Connection = (conn == null ? GetAvailableConnection() : conn);
            da.Disposed += new EventHandler(da_Disposed);
            return da;
		}

		public ServerDataSetTableAdapters.DownloadLogsTableAdapter CreateDownloadLogsInfoDA(bool bIsSelect)
		{
			ServerDataSetTableAdapters.DownloadLogsTableAdapter da =
				new ServerDataSetTableAdapters.DownloadLogsTableAdapter();

			da.Connection = bIsSelect ? GetAvailableConnection() : GetAvailableLogConnection();
            da.Disposed += new EventHandler(da_Disposed);
            return da;
		}

		public ServerDataSetTableAdapters.DownloadLogsTableAdapter CreateDownloadLogsInfoDA()
		{
			return CreateDownloadLogsInfoDA(null);
		}
		public ServerDataSetTableAdapters.ManagerLogsTableAdapter CreateManagerLogsInfoDA(OdbcConnection conn)
		{
			ServerDataSetTableAdapters.ManagerLogsTableAdapter da =
				new ServerDataSetTableAdapters.ManagerLogsTableAdapter();

			da.Connection = (conn == null ? GetAvailableConnection() : conn);
            da.Disposed += new EventHandler(da_Disposed);
            return da;
		}

		public ServerDataSetTableAdapters.ManagerLogsTableAdapter CreateManagerLogsInfoDA(bool bIsSelect)
		{
			ServerDataSetTableAdapters.ManagerLogsTableAdapter da =
				new ServerDataSetTableAdapters.ManagerLogsTableAdapter();

			da.Connection = bIsSelect ? GetAvailableConnection() : GetAvailableLogConnection();
            da.Disposed += new EventHandler(da_Disposed);
            return da;
		}

		public ServerDataSetTableAdapters.ManagerLogsTableAdapter CreateManagerLogsInfoDA()
		{
			return CreateManagerLogsInfoDA(null);
		}

        public ServerDataSetTableAdapters.playreportsTableAdapter CreatePlayReportInfoDA(OdbcConnection conn)
		{
			ServerDataSetTableAdapters.playreportsTableAdapter da =
				new ServerDataSetTableAdapters.playreportsTableAdapter();

			da.Connection = (conn == null ? GetAvailableConnection() : conn);
            da.Disposed += new EventHandler(da_Disposed);
            return da;
		}

		public ServerDataSetTableAdapters.playreportsTableAdapter CreatePlayReportInfoDA()
		{
			return CreatePlayReportInfoDA(null);
		}

        public ServerDataSetTableAdapters.targetreportsTableAdapter CreateTargetReportInfoDA(OdbcConnection conn)
		{
			ServerDataSetTableAdapters.targetreportsTableAdapter da =
				new ServerDataSetTableAdapters.targetreportsTableAdapter();

			da.Connection = (conn == null ? GetAvailableConnection() : conn);
            da.Disposed += new EventHandler(da_Disposed);
            return da;
		}

        public ServerDataSetTableAdapters.targetreportsTableAdapter CreateTargetReportInfoDA()
		{
            return CreateTargetReportInfoDA(null);
		}

        public ServerDataSetTableAdapters.report_running_timeTableAdapter CreateRunningReportInfoDA(OdbcConnection conn)
        {
            ServerDataSetTableAdapters.report_running_timeTableAdapter da =
                new ServerDataSetTableAdapters.report_running_timeTableAdapter();

			da.Connection = (conn == null ? GetAvailableConnection() : conn);
            da.Disposed += new EventHandler(da_Disposed);
            return da;
        }

        public ServerDataSetTableAdapters.report_running_timeTableAdapter CreateRunningReportInfoDA()
        {
            return CreateRunningReportInfoDA(null);
        }

        public ServerDataSetTableAdapters.report_time_scheduleTableAdapter CreateTimeScheduleReportInfoDA(OdbcConnection conn)
        {
            ServerDataSetTableAdapters.report_time_scheduleTableAdapter da =
                new ServerDataSetTableAdapters.report_time_scheduleTableAdapter();

			da.Connection = (conn == null ? GetAvailableConnection() : conn);
            da.Disposed += new EventHandler(da_Disposed);
            return da;
        }

        public ServerDataSetTableAdapters.report_time_scheduleTableAdapter CreateTimeScheduleReportInfoDA()
        {
            return CreateTimeScheduleReportInfoDA(null);
        }


        /// <summary>
        /// 테이블 아답터를 만들때 마다 꼭 넣어주자
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void da_Disposed(object sender, EventArgs e)
        {
            try
            {
                if (sender is ServerDataSetTableAdapters.role_perm_ASNTableAdapter)
                {
                    ((ServerDataSetTableAdapters.role_perm_ASNTableAdapter)sender).Connection.Close();
                }
                else if (sender is ServerDataSetTableAdapters.groupsTableAdapter)
                {
                    ((ServerDataSetTableAdapters.groupsTableAdapter)sender).Connection.Close();
                }
                else if (sender is ServerDataSetTableAdapters.playersTableAdapter)
                {
                    ((ServerDataSetTableAdapters.playersTableAdapter)sender).Connection.Close();
                }
                else if (sender is ServerDataSetTableAdapters.players_detail_ASNTableAdapter)
                {
                    ((ServerDataSetTableAdapters.players_detail_ASNTableAdapter)sender).Connection.Close();
                }
                else if (sender is ServerDataSetTableAdapters.usersTableAdapter)
                {
                    ((ServerDataSetTableAdapters.usersTableAdapter)sender).Connection.Close();
                }
                else if (sender is ServerDataSetTableAdapters.rolesTableAdapter)
                {
                    ((ServerDataSetTableAdapters.rolesTableAdapter)sender).Connection.Close();
                }
                else if (sender is ServerDataSetTableAdapters.user_group_ASNTableAdapter)
                {
                    ((ServerDataSetTableAdapters.user_group_ASNTableAdapter)sender).Connection.Close();
                }
                else if (sender is ServerDataSetTableAdapters.user_player_ASNTableAdapter)
                {
                    ((ServerDataSetTableAdapters.user_player_ASNTableAdapter)sender).Connection.Close();
                }
                else if (sender is ServerDataSetTableAdapters.tasksTableAdapter)
                {
                    ((ServerDataSetTableAdapters.tasksTableAdapter)sender).Connection.Close();
                }
                else if (sender is ServerDataSetTableAdapters.screensTableAdapter)
                {
                    ((ServerDataSetTableAdapters.screensTableAdapter)sender).Connection.Close();
                }
                else if (sender is ServerDataSetTableAdapters.ScreenTaskASNTableAdapter)
                {
                    ((ServerDataSetTableAdapters.ScreenTaskASNTableAdapter)sender).Connection.Close();
                }
                else if (sender is ServerDataSetTableAdapters.downloadmasterTableAdapter)
                {
                    ((ServerDataSetTableAdapters.downloadmasterTableAdapter)sender).Connection.Close();
                }
                else if (sender is ServerDataSetTableAdapters.DownloadedScheduleInfoTableAdapter)
                {
                    ((ServerDataSetTableAdapters.DownloadedScheduleInfoTableAdapter)sender).Connection.Close();
                }
                else if (sender is ServerDataSetTableAdapters.downloadmasterTableAdapter)
                {
                    ((ServerDataSetTableAdapters.downloadmasterTableAdapter)sender).Connection.Close();
                }
                else if (sender is ServerDataSetTableAdapters.DataInfoTableAdapter)
                {
                    ((ServerDataSetTableAdapters.DataInfoTableAdapter)sender).Connection.Close();
                }
                else if (sender is ServerDataSetTableAdapters.Data_FieldTableAdapter)
                {
                    ((ServerDataSetTableAdapters.Data_FieldTableAdapter)sender).Connection.Close();
                }
                else if (sender is ServerDataSetTableAdapters.ContentsTableAdapter)
                {
                    ((ServerDataSetTableAdapters.ContentsTableAdapter)sender).Connection.Close();
                }
                else if (sender is ServerDataSetTableAdapters.resultdatasTableAdapter)
                {
                    ((ServerDataSetTableAdapters.resultdatasTableAdapter)sender).Connection.Close();
                }
                else if (sender is ServerDataSetTableAdapters.LimitPlayersTableAdapter)
                {
                    ((ServerDataSetTableAdapters.LimitPlayersTableAdapter)sender).Connection.Close();
                }
                else if (sender is ServerDataSetTableAdapters.DownloadTime_Groups_ASNTableAdapter)
                {
                    ((ServerDataSetTableAdapters.DownloadTime_Groups_ASNTableAdapter)sender).Connection.Close();
                }
                else if (sender is ServerDataSetTableAdapters.DownloadTime_Players_ASNTableAdapter)
                {
                    ((ServerDataSetTableAdapters.DownloadTime_Players_ASNTableAdapter)sender).Connection.Close();
                }
                else if (sender is ServerDataSetTableAdapters.playerdetailinfoTableAdapter)
                {
                    ((ServerDataSetTableAdapters.playerdetailinfoTableAdapter)sender).Connection.Close();
                }
                else if (sender is ServerDataSetTableAdapters.meta_tag_groupsTableAdapter)
                {
                    ((ServerDataSetTableAdapters.meta_tag_groupsTableAdapter)sender).Connection.Close();
                }
                else if (sender is ServerDataSetTableAdapters.meta_tagsTableAdapter)
                {
                    ((ServerDataSetTableAdapters.meta_tagsTableAdapter)sender).Connection.Close();
                }
                else if (sender is ServerDataSetTableAdapters.timestampsTableAdapter)
                {
                    ((ServerDataSetTableAdapters.timestampsTableAdapter)sender).Connection.Close();
                }
                else if (sender is ServerDataSetTableAdapters.ConnectionLogsTableAdapter)
                {
                    ((ServerDataSetTableAdapters.ConnectionLogsTableAdapter)sender).Connection.Close();
                }
                else if (sender is ServerDataSetTableAdapters.PlayLogsTableAdapter)
                {
                    ((ServerDataSetTableAdapters.PlayLogsTableAdapter)sender).Connection.Close();
                }
                else if (sender is ServerDataSetTableAdapters.DownloadLogsTableAdapter)
                {
                    ((ServerDataSetTableAdapters.DownloadLogsTableAdapter)sender).Connection.Close();
                }
                else if (sender is ServerDataSetTableAdapters.ManagerLogsTableAdapter)
                {
                    ((ServerDataSetTableAdapters.ManagerLogsTableAdapter)sender).Connection.Close();
                }
                else if (sender is ServerDataSetTableAdapters.playreportsTableAdapter)
                {
                    ((ServerDataSetTableAdapters.playreportsTableAdapter)sender).Connection.Close();
                }
                else if (sender is ServerDataSetTableAdapters.targetreportsTableAdapter)
                {
                    ((ServerDataSetTableAdapters.targetreportsTableAdapter)sender).Connection.Close();
                }
                else if (sender is ServerDataSetTableAdapters.report_running_timeTableAdapter)
                {
                    ((ServerDataSetTableAdapters.report_running_timeTableAdapter)sender).Connection.Close();
                }
                else if (sender is ServerDataSetTableAdapters.report_time_scheduleTableAdapter)
                {
                    ((ServerDataSetTableAdapters.report_time_scheduleTableAdapter)sender).Connection.Close();
                }
                else logger.Error("Connection Dispose is not Implemented!!!!!!!!!!");
            }
            catch (Exception ex) { logger.Error(ex.Message); }
        }

       
	}
}
