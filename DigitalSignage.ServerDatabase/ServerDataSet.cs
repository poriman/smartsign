﻿namespace DigitalSignage.ServerDatabase {
    
    
    public partial class ServerDataSet {
		partial class geo_masterDataTable
		{
		}
	
		partial class playerdetailinfoDataTable
		{
		}
	
		partial class user_group_ASNDataTable
		{
		}
	}
}

namespace DigitalSignage.ServerDatabase.ServerDataSetTableAdapters {
    partial class summary_playersTableAdapter
    {
        protected override void Dispose(bool disposing)
        {
            try
            {
                this.Connection.Close();
            }
            catch { }

            base.Dispose(disposing);
        }
    }

    partial class PlayLogsTableAdapter
    {
        protected override void Dispose(bool disposing)
        {
            try
            {
                this.Connection.Close();
            }
            catch { }

            base.Dispose(disposing);
        }
    }

    partial class ManagerLogsTableAdapter
    {
        protected override void Dispose(bool disposing)
        {
            try
            {
                this.Connection.Close();
            }
            catch { }

            base.Dispose(disposing);
        }
    }

    partial class DownloadLogsTableAdapter
    {
        protected override void Dispose(bool disposing)
        {
            try
            {
                this.Connection.Close();
            }
            catch { }

            base.Dispose(disposing);
        }
    }

    partial class ConnectionLogsTableAdapter
    {
        protected override void Dispose(bool disposing)
        {
            try
            {
                this.Connection.Close();
            }
            catch { }

            base.Dispose(disposing);
        }
    }

    partial class timestampsTableAdapter
    {
        protected override void Dispose(bool disposing)
        {
            try
            {
                this.Connection.Close();
            }
            catch { }

            base.Dispose(disposing);
        }
    }

    partial class meta_tag_groupsTableAdapter
    {
        protected override void Dispose(bool disposing)
        {
            try
            {
                this.Connection.Close();
            }
            catch { }

            base.Dispose(disposing);
        }
    }

    partial class meta_tagsTableAdapter
    {
        protected override void Dispose(bool disposing)
        {
            try
            {
                this.Connection.Close();
            }
            catch { }

            base.Dispose(disposing);
        }
    }

    partial class ScreenTaskASNTableAdapter
    {
        protected override void Dispose(bool disposing)
        {
            try
            {
                this.Connection.Close();
            }
            catch { }

            base.Dispose(disposing);
        }
    }

    partial class LimitPlayersTableAdapter
    {
        protected override void Dispose(bool disposing)
        {
            try
            {
                this.Connection.Close();
            }
            catch { }

            base.Dispose(disposing);
        }
    }

    partial class ContentsTableAdapter
    {
        protected override void Dispose(bool disposing)
        {
            try
            {
                this.Connection.Close();
            }
            catch { }

            base.Dispose(disposing);
        }
    }

    partial class Data_FieldTableAdapter
    {
        protected override void Dispose(bool disposing)
        {
            try
            {
                this.Connection.Close();
            }
            catch { }

            base.Dispose(disposing);
        }
    }

    partial class DataInfoTableAdapter
    {
        protected override void Dispose(bool disposing)
        {
            try
            {
                this.Connection.Close();
            }
            catch { }

            base.Dispose(disposing);
        }
    }

    partial class RSSFeedsTableAdapter
    {
        protected override void Dispose(bool disposing)
        {
            try
            {
                this.Connection.Close();
            }
            catch { }

            base.Dispose(disposing);
        }
    }

    partial class RSSChannelsTableAdapter
    {
        protected override void Dispose(bool disposing)
        {
            try
            {
                this.Connection.Close();
            }
            catch { }

            base.Dispose(disposing);
        }
    }

    partial class RSCommandsTableAdapter
    {
        protected override void Dispose(bool disposing)
        {
            try
            {
                this.Connection.Close();
            }
            catch { }

            base.Dispose(disposing);
        }
    }

    partial class report_time_scheduleTableAdapter
    {
        protected override void Dispose(bool disposing)
        {
            try
            {
                this.Connection.Close();
            }
            catch { }

            base.Dispose(disposing);
        }
    }

    partial class report_running_timeTableAdapter
    {
        protected override void Dispose(bool disposing)
        {
            try
            {
                this.Connection.Close();
            }
            catch { }

            base.Dispose(disposing);
        }
    }

    partial class rolesTableAdapter
    {
        protected override void Dispose(bool disposing)
        {
            try
            {
                this.Connection.Close();
            }
            catch { }

            base.Dispose(disposing);
        }
    }

    partial class targetreportsTableAdapter
    {
        protected override void Dispose(bool disposing)
        {
            try
            {
                this.Connection.Close();
            }
            catch { }

            base.Dispose(disposing);
        }
    }

    partial class role_perm_ASNTableAdapter
    {
        protected override void Dispose(bool disposing)
        {
            try
            {
                this.Connection.Close();
            }
            catch { }

            base.Dispose(disposing);
        }
    }

    partial class playreportsTableAdapter
    {
        protected override void Dispose(bool disposing)
        {
            try
            {
                this.Connection.Close();
            }
            catch { }

            base.Dispose(disposing);
        }
    }

    partial class permissionsTableAdapter
    {
        protected override void Dispose(bool disposing)
        {
            try
            {
                this.Connection.Close();
            }
            catch { }

            base.Dispose(disposing);
        }
    }

    partial class playersTableAdapter
    {
        protected override void Dispose(bool disposing)
        {
            try
            {
                this.Connection.Close();
            }
            catch { }

            base.Dispose(disposing);
        }
    }

    partial class groupsTableAdapter
    {
        protected override void Dispose(bool disposing)
        {
            try
            {
                this.Connection.Close();
            }
            catch { }

            base.Dispose(disposing);
        }
    }

    partial class DownloadTime_Players_ASNTableAdapter
    {
        protected override void Dispose(bool disposing)
        {
            try
            {
                this.Connection.Close();
            }
            catch { }

            base.Dispose(disposing);
        }
    }

    partial class DownloadTime_Groups_ASNTableAdapter
    {
        protected override void Dispose(bool disposing)
        {
            try
            {
                this.Connection.Close();
            }
            catch { }

            base.Dispose(disposing);
        }
    }

    partial class user_player_ASNTableAdapter
    {
        protected override void Dispose(bool disposing)
        {
            try
            {
                this.Connection.Close();
            }
            catch { }

            base.Dispose(disposing);
        }
    }

    partial class usersTableAdapter
    {
        protected override void Dispose(bool disposing)
        {
            try
            {
                this.Connection.Close();
            }
            catch { }

            base.Dispose(disposing);
        }
    }

    partial class user_group_ASNTableAdapter
    {
        protected override void Dispose(bool disposing)
        {
            try
            {
                this.Connection.Close();
            }
            catch { }

            base.Dispose(disposing);
        }
    }

    partial class DownloadedScheduleInfoTableAdapter
    {
        protected override void Dispose(bool disposing)
        {
            try
            {
                this.Connection.Close();
            }
            catch { }

            base.Dispose(disposing);
        }
    }

    partial class downloadmasterTableAdapter
    {
        protected override void Dispose(bool disposing)
        {
            try
            {
                this.Connection.Close();
            }
            catch { }

            base.Dispose(disposing);
        }
    }

    partial class tasksTableAdapter
    {
        protected override void Dispose(bool disposing)
        {
            try
            {
                this.Connection.Close();
            }
            catch { }

            base.Dispose(disposing);
        }
    }

	partial class screensTableAdapter
	{
        protected override void Dispose(bool disposing)
        {
            try
            {
                this.Connection.Close();
            }
            catch { }

            base.Dispose(disposing);
        }
    }

	partial class players_detail_ASNTableAdapter
	{
        protected override void Dispose(bool disposing)
        {
            try
            {
                this.Connection.Close();
            }
            catch { }

            base.Dispose(disposing);
        }
    }

	partial class playerdetailinfoTableAdapter
	{
        protected override void Dispose(bool disposing)
        {
            try
            {
                this.Connection.Close();
            }
            catch { }

            base.Dispose(disposing);
        }
    }
    
    
    public partial class resultdatasTableAdapter {
        protected override void Dispose(bool disposing)
        {
            try
            {
                this.Connection.Close();
            }
            catch { }

            base.Dispose(disposing);
        }
    }
}
