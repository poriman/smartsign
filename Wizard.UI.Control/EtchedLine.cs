﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Wizard.UI.Control
{
	public partial class EtchedLine : UserControl
	{
		Color _darkColor = SystemColors.ControlDark;
		Color _lightColor = SystemColors.ControlLightLight;

		public EtchedLine()
		{
			InitializeComponent();

			// Avoid receiving the focus.
			SetStyle(ControlStyles.Selectable, false);
		}
		private void EtchedLine_Paint(object sender, PaintEventArgs e)
		{
			base.OnPaint(e);

			Brush lightBrush = new SolidBrush(_lightColor);
			Brush darkBrush = new SolidBrush(_darkColor);
			Pen lightPen = new Pen(lightBrush, 1);
			Pen darkPen = new Pen(darkBrush, 1);

			e.Graphics.DrawLine(darkPen, 0, 0, this.Width, 0);
			e.Graphics.DrawLine(lightPen, 0, 1, this.Width, 1);

		}

		private void EtchedLine_Resize(object sender, EventArgs e)
		{
			base.OnResize(e);

			Refresh();
		}

		[Category("Appearance")]
		Color DarkColor
		{

			get { return _darkColor; }

			set
			{
				_darkColor = value;
				Refresh();
			}
		}

		[Category("Appearance")]
		Color LightColor
		{
			get { return _lightColor; }

			set
			{
				_lightColor = value;
				Refresh();
			}
		}
	}
}
