﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections.ObjectModel;

using System.IO;
using System.Reflection;
using System.Diagnostics;
using System.Net;
using Microsoft.Win32;

using NLog;
using NLog.Targets;
using System.Threading;

namespace iVisionUpdater
{
	public partial class Form1 : Form
	{
		private static Logger logger = LogManager.GetCurrentClassLogger();

		const string UPDATE_EXT = ".cab";
		const string TEMPFOLDER = "Temp";
// 		const string UPDATEURL = "http://updater.intelliansys.co.kr/SmartSign/";
		string UPDATEURL = "http://update.myivision.com/SmartSign/";
		const string UPDATELIST = "update.txt";
		string strAppDir = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetModules()[0].FullyQualifiedName) + "\\";
		
		System.Windows.Forms.Timer tm = null;


		public Form1()
		{
			InitializeComponent();
			init();
			try
			{
				UPDATEURL = Properties.Settings.Default.UpdateURL;
			}
			catch
			{
				UPDATEURL = "http://update.myivision.com/ivisionNE/Default/";
			}
		}

		public void init()
		{
			try
			{
				string appData = strAppDir + "PlayerData\\";
				// get own version
				Assembly myAssembly = Assembly.GetExecutingAssembly();
				AssemblyName myAssemblyName = myAssembly.GetName();
				long version = ((long)myAssemblyName.Version.Major << 48) |
					((long)myAssemblyName.Version.Minor << 32) |
					(myAssemblyName.Version.Build << 16) |
					myAssemblyName.Version.Revision;

				// configuring log output
				FileTarget target = new FileTarget();
				target.Layout = "${longdate}\t[${level}]\t${message}";
				target.FileName = appData + "update_log.txt";
				target.ArchiveFileName = appData + "archives/update.{#####}.txt";

				target.ArchiveEvery = FileArchivePeriod.Day;
				target.MaxArchiveFiles = 31;
				target.ArchiveNumbering = ArchiveNumberingMode.Sequence;
				// this speeds up things when no other processes are writing to the file
				target.ConcurrentWrites = true;
				NLog.Config.SimpleConfigurator.ConfigureForTargetLogging(target, LogLevel.Debug);

				logger.Info("Updater " + ((version >> 48) & 0xFFFF) + "." +
						((version >> 32) & 0xFFFF) + "." +
						((version >> 16) & 0xFFFF) + "." + (version & 0xFFFF));
			}
			catch
			{
				
			}
		}

		private void Form1_Load(object sender, EventArgs e)
		{
			label1.Text = "Downloading list...";

			if (Directory.Exists(strAppDir + TEMPFOLDER))
			{
				RecusiveMove(strAppDir + TEMPFOLDER + "\\");
				StartAgent();
				this.Close();
				return;
			}

			if(CheckList())
			{
				RecusiveMove(strAppDir + TEMPFOLDER + "\\");
				StartAgent();
				this.Close();
				return;
			}
			else
			{
				StartAgent();
				this.Hide();

				logger.Info("Retry after 60 seconds.");
				//	5qns 후에 다시시도한다.
				tm = new System.Windows.Forms.Timer();
				tm.Tick += new EventHandler(tm_Tick);
				tm.Interval = 60000;
				tm.Start();
			}

			label1.Text = "Starting Agent...";
			//	프로세스 실행.
// 			this.Close();
		}

		void tm_Tick(object sender, EventArgs e)
		{
			tm.Stop();
			tm = null;

			Thread.Sleep(60 * 1000);
			CheckList();

			this.Close();
		}
		private void GetUpdateURL()
		{
// 			RegistryKey reg = new RegistryKey();
			
		}
		bool DownloadWebFile(string file_url, string dest_url)
		{
			try
			{
				WebClient client = new WebClient();

				client.UseDefaultCredentials = true;

				FileInfo info = new FileInfo(dest_url);
				if (!Directory.Exists(info.DirectoryName))
					Directory.CreateDirectory(info.DirectoryName);

				logger.Info("Downloading " + info.Name);

				client.DownloadFile(file_url, dest_url);
			}
			catch (Exception ee)
			{
				logger.Error(ee.ToString());
				return false;
			}

			return true;
		}
		bool IsVersionUP(string currVersion, string newVersion)
		{
			string[] currFileVersion = currVersion.Split('.');
			string[] newFileVersion = newVersion.Split('.');

			if (Convert.ToInt32(newFileVersion[0]) > Convert.ToInt32(currFileVersion[0]))
			{
				return true;
			}
			else if (Convert.ToInt32(newFileVersion[0]) == Convert.ToInt32(currFileVersion[0]))
			{
				if (Convert.ToInt32(newFileVersion[1]) > Convert.ToInt32(currFileVersion[1]))
				{
					return true;
				}
				else if (Convert.ToInt32(newFileVersion[1]) == Convert.ToInt32(currFileVersion[1]))
				{
					if (Convert.ToInt32(newFileVersion[2]) > Convert.ToInt32(currFileVersion[2]))
					{
						return true;
					}
					else if (Convert.ToInt32(newFileVersion[2]) == Convert.ToInt32(currFileVersion[2]))
					{
						if (Convert.ToInt32(newFileVersion[3]) > Convert.ToInt32(currFileVersion[3]))
						{
							return true;
						}
					}
				}
			}

			return false;
		}

		bool CheckList()
		{
			label1.Text = "Checking version...";
			logger.Info("Checking version...");

			//	임시 폴더 생성.
			if (!Directory.Exists(strAppDir + TEMPFOLDER))
				Directory.CreateDirectory(strAppDir + TEMPFOLDER);

			if (!DownloadWebFile(UPDATEURL + UPDATELIST, strAppDir + TEMPFOLDER + "\\" + UPDATELIST))
				return false;

			Collection<string> arrWillUpdate = new Collection<string>();

			using (StreamReader sr = new StreamReader(strAppDir + TEMPFOLDER + "\\" + UPDATELIST))
			{
				string line = sr.ReadLine();
				do
				{
					try
					{
						string[] split = line.Split('\t');

						string filename = split[0];
						string fileversion = split[1];

						try
						{
							FileVersionInfo info = FileVersionInfo.GetVersionInfo(strAppDir + filename);

							if (IsVersionUP(info.FileVersion, fileversion))
							{
								try
								{
									string real_filename = split[2];
									arrWillUpdate.Add(real_filename);
								}
								catch
								{
									arrWillUpdate.Add(filename);
								}
							}

						}
						catch
						{
							try
							{
								string real_filename = split[2];
								arrWillUpdate.Add(real_filename);
							}
							catch
							{
								arrWillUpdate.Add(filename);
							}
						}
					}
					catch {}
				} while ((line = sr.ReadLine()) != null);
			}

			if (arrWillUpdate.Count > 0)
			{
				progressBar1.Maximum = arrWillUpdate.Count * 2;
				progressBar1.Step = 1;
			}

			foreach(string file in arrWillUpdate)
			{
				label1.Text = "Downloading : " + file;

				if (!DownloadWebFile(UPDATEURL + file + UPDATE_EXT, strAppDir + TEMPFOLDER + "\\" + file))
				{
					return false;
				}

				progressBar1.PerformStep();
			}

			return true;
		}


		bool RecusiveMove(string sourceFolder)
		{
			try
			{
				logger.Info("Found Temp Folder.. Will be update!");

				string destFolder = sourceFolder.Replace("\\"+TEMPFOLDER+"\\", "\\");
				if (!Directory.Exists(destFolder))
					Directory.CreateDirectory(destFolder);

				string[] directories = Directory.GetDirectories(sourceFolder);

				foreach(string directory in directories)
				{
					if (!RecusiveMove(directory))
						return false;
				}

				string[] files = Directory.GetFiles(sourceFolder);

				foreach(string file in files)
				{
					label1.Text = "Moving : " + file;
					
					logger.Info("Updating : " + file);

					string destFile = file.Replace("\\" + TEMPFOLDER + "\\", "\\");
					if(destFile.Contains("update.txt"))
					{
						destFile.Replace("update.txt", DateTime.Now.ToShortDateString() + ".txt");
					}
					File.Copy(file, destFile, true);
					
					progressBar1.PerformStep();
				}

				Directory.Delete(sourceFolder, true);

				return true;
			}
			catch (Exception ee)
			{
				logger.Error(ee.ToString());
				return false;
			}
		}

		void StartAgent()
		{
			// get assebly path

			Process proc = new Process();
            proc.StartInfo.FileName = strAppDir + "\\DS.ScreenAgent.exe";
			proc.StartInfo.WorkingDirectory = strAppDir;
			proc.Start();
		}
	}
}
