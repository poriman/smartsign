﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace iVisionUpdater
{
    /// <summary>
    /// Update Xml 파일 분석하는 클래스
    /// </summary>
    public class UpdateXmlParser
    {
        XmlDocument _doc = new XmlDocument();

        /// <summary>
        /// 생성자
        /// </summary>
        /// <param name="sFileName">XML 파일</param>
        public UpdateXmlParser(String sFileName)
        {
            _doc.Load(sFileName);
        }

        /// <summary>
        /// 패키지 리스트 가져오기
        /// </summary>
        public XmlNodeList Packages
        {
            get { return _doc.SelectNodes("//child::Package"); }
        }

        public XmlNodeList Updates
        {
            get { return _doc.SelectNodes("//child::Update"); }
        }

        XmlNodeList GetUpdateFromPackageName(String sPackageName)
        {
            return _doc.SelectNodes("//child::Update[attribute::Name=\""+ sPackageName +"\"]");
        }

        public List<XmlNode> GetUpdateFrom(String sPackageName, String sCurrVersion)
        {
            List<XmlNode> arrNodes = new List<XmlNode>();

            XmlNodeList list = GetUpdateFromPackageName(sPackageName);
            
            if(list != null)
            {
                foreach(XmlNode node in list)
                {
                    String MinVer = node.Attributes["MinVersion"].Value;
                    String MaxVer = node.Attributes["MaxVersion"].Value;

                    if(CanUpdate(sCurrVersion, MinVer, MaxVer))
                    {
                        arrNodes.Add(node);
                    }
                }
            }

            return arrNodes;
        }

        static long ConvertVersionToLong(String version)
        {
            string[] arrVer = version.Split('.');
            long lReturn = 0;
            int nIndex = 0;
            double dPow = 3.0;

            while (nIndex < arrVer.Length)
            {
                long lNum = 65535;
                try
                {
                    lNum = Convert.ToInt16(arrVer[nIndex]);
                }
                catch (IndexOutOfRangeException) {
                    break;
                }
                catch {lNum = 65535;}

                lReturn += lNum * (long)Math.Pow(65535, dPow);

                nIndex++;
                dPow--;

            }

            return lReturn;

        }

        public static bool CanUpdate(String currVer, String minVer, String maxVer)
        {
            long lCurr = ConvertVersionToLong(currVer);
            long lMin = ConvertVersionToLong(minVer);
            long lMax = ConvertVersionToLong(maxVer);

            return lMin <= lCurr && lMax >= lCurr;

        }
    }
}
