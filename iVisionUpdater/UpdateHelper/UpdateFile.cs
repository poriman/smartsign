﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Reflection;
using NLog;

namespace iVisionUpdater
{
    public class UpdateFile
    {
        static Logger logger = LogManager.GetCurrentClassLogger();

        const string TEMPFOLDER = "Temp";
        const string BACKUPFOLDER = "Backup";
        const string UPDATEFOLDER = "Updates";

        String sAppDir = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetModules()[0].FullyQualifiedName) + "\\";

        public List<String> PackageNames = null;

        public String PackageName
        {
            get;
            set;
        }
        public String CabPath
        {
            get;
            set;
        }
        public String BackupPath
        {
            get;
            set;
        }
        public String ExtractPath
        {
            get;
            set;
        }

        public String DestPath
        {
            get;
            set;
        }

        public String FilePath
        {
            get;
            set;
        }

        public String CabName
        {
            get;
            set;
        }

        public UpdateFile(String sUpdateFile)
        {
            FilePath = sUpdateFile;

            BackupPath = sAppDir + BACKUPFOLDER + "\\";
            DestPath = sAppDir;
            ExtractPath = sAppDir + UPDATEFOLDER + "\\";

            PackageNames = new List<String>();

        }

        public bool BackupFiles()
        {
            if (!Directory.Exists(BackupPath))
                Directory.CreateDirectory(BackupPath);

            if (!Directory.Exists(BackupPath + CabName))
                Directory.CreateDirectory(BackupPath + CabName);
            else
            {
                Directory.Delete(BackupPath + CabName, true);
                Directory.CreateDirectory(BackupPath + CabName);
            }

            if (!RecursiveBackup(CabPath, BackupPath + CabName + "\\", CabPath))
            {
                RecursiveDelete(BackupPath + CabName);
                return false;
            }
            
            return true;
        }

        public bool RecoveryFiles()
        {
            return RecursiveCopy(BackupPath + CabName, DestPath);

        }

        bool RecursiveBackup(String sSource, String sDest, String sReplacePath)
        {
            if (!Directory.Exists(sDest))
                Directory.CreateDirectory(sDest);

            string[] directories = Directory.GetDirectories(sSource);

            foreach (string directory in directories)
            {
                DirectoryInfo dic = new DirectoryInfo(directory);
                if (dic.Exists)
                {
                    if (!RecursiveBackup(directory, sDest + "\\" + dic.Name, sReplacePath))
                        return false;
                }
            }

            string[] files = Directory.GetFiles(sSource);

            foreach (string file in files)
            {
                try
                {
                    String sSourceFile = file.Replace(sReplacePath, DestPath);
                    String sDestFile = file.Replace(sReplacePath, sDest);
                    logger.Info(String.Format("Backup Files..[{0}] to [{1}]", sSourceFile, sDestFile));

                    File.Copy(sSourceFile, sDestFile, true);
                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message);
                }
            }

            return true;
        }

        public static bool RecursiveCopy(String sSource, String sDest)
        {
            if (!Directory.Exists(sDest))
                Directory.CreateDirectory(sDest);

            
            string[] directories = Directory.GetDirectories(sSource);

            foreach (string directory in directories)
            {
                DirectoryInfo dic = new DirectoryInfo(directory);
                if(dic.Exists)
                {
                    if (!RecursiveCopy(directory, sDest + "\\" + dic.Name))
                        return false;
                }
            }

            string[] files = Directory.GetFiles(sSource);

            foreach (string file in files)
            {
                FileInfo finfo = new FileInfo(file);
                if(finfo.Exists)
                {
                    try
                    {
                        logger.Info(String.Format("Copy Files..[{0}] to [{1}]", file, sDest + "\\" + finfo.Name));
                        File.Copy(file, sDest + "\\" + finfo.Name, true);

                    }
                    catch (Exception e) 
                    {
                        logger.Error(e.Message);
                        return false; 
                    }
                }
            }

            return true;


        }
        public static void RecursiveDelete(String sPath)
        {
            DirectoryInfo dic = new DirectoryInfo(sPath);
            if(dic.Exists)
            {
                dic.Delete(true);
            }
        }

        public bool UpdateFiles()
        {
            return RecursiveCopy(CabPath, DestPath);
        }

        /// <summary>
        /// 파일 압축 풀기
        /// </summary>
        /// <returns></returns>
        public bool ExtractFiles()
        {
            if (!Directory.Exists(ExtractPath))
                Directory.CreateDirectory(ExtractPath);
            else
            {
                Directory.Delete(ExtractPath, true);
                Directory.CreateDirectory(ExtractPath);
            }

            CabLib.Extract extCab = new CabLib.Extract();

            FileInfo fiCab = new FileInfo(FilePath);
            if (fiCab.Exists)
            {
                String extCabDirectory = String.Format("{0}\\{1}\\", ExtractPath, CabName = fiCab.Name);

                if (!Directory.Exists(extCabDirectory))
                    Directory.CreateDirectory(extCabDirectory);

                extCab.ExtractFile(FilePath, extCabDirectory);

                CabPath = extCabDirectory;

                extCab.CleanUp();
            }
            else
            {
                extCab.CleanUp();

                return false;
            }

            return true;
        }

    }
}
