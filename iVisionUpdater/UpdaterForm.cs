﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Globalization;
using System.Threading;
using NLog;
using System.Reflection;
using NLog.Targets;
using System.Diagnostics;
using System.Xml;
using System.Net;
using System.IO;

namespace iVisionUpdater
{
    public partial class UpdaterForm : Form
    {
        #region Enums
        /// <summary>
        /// 업데이터 상태 정보
        /// </summary>
        enum UpdateState
        {
            /// <summary>
            /// 상태 없음
            /// </summary>
            None,
            /// <summary>
            /// 업데이트 목록 분석
            /// </summary>
            ListParsing,
            /// <summary>
            /// CAB 파일 다운로드
            /// </summary>
            CabDownloading,
            /// <summary>
            /// CAB 파일 압축 해제
            /// </summary>
            CabExtracting,
            /// <summary>
            /// 기존 버전 백업
            /// </summary>
            BackUpOldVersion,
            /// <summary>
            /// 파일 업데이트
            /// </summary>
            FileUpdating,
            /// <summary>
            /// 백업 복구
            /// </summary>
            BackupRecovering,
            /// <summary>
            /// 업데이트 완료
            /// </summary>
            UpdateCompleting,
            /// <summary>
            /// 업데이트 중 에러 발생
            /// </summary>
            ErrorOccurred
        };
        #endregion

        #region Attributes
        public NotifyIcon notifier = null;
        private System.Windows.Forms.ContextMenu contextMenu1 = new System.Windows.Forms.ContextMenu();

        Mutex _mutex = null;

        List<CultureInfo> arrLaguageInfo = new List<CultureInfo>();
        BackgroundWorker bwUpdater = null;
        BackgroundWorker bwWatchProcess = null;

        Logger logger = LogManager.GetCurrentClassLogger();

        bool _bWatchAgent = true;

        static private object lockUpdater = new object();

        UpdateState currState = UpdateState.None;
        UpdateXmlParser _parser = null;
//         List<String> _arrDownloadedCabFile = new List<string>();
        List<UpdateFile> _arrDownloadedCabFile = new List<UpdateFile>();

        const string TEMPFOLDER = "Temp";
        const string BACKUPFOLDER = "Backup";
        const string UPDATEFOLDER = "Updates";

        string UPDATEURL = "http://update.myivision.com/ivisionNE/";
        const string UPDATELIST = "update.xml";
        string strAppDir = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetModules()[0].FullyQualifiedName) + "\\";

        #endregion

        #region Constructor
        public UpdaterForm()
        {
            #region Mutex - ÇÁ·Î±×·¥ ÇÑ°³¸¸ ¶ßµµ·Ï
            bool bCreate = false;
            _mutex = new Mutex(true, "DigitalSignage.PlayUpdater", out bCreate);
            if (!bCreate)
            {
                this.Close();
                return;
            }
            #endregion

            try
            {
                ChangeCulture(Properties.Settings.Default.Default_Culture);
            }
            catch {
            }

            arrLaguageInfo.Add(new CultureInfo("ko-KR"));
            arrLaguageInfo.Add(new CultureInfo("en-US"));

            InitializeComponent();

            labelUpdating.Text = Properties.Resources.labelUpdating;
            
            //  ·Î±× Configuration
            InitLogger();

            try
            {
                UPDATEURL = Properties.Settings.Default.UpdateURL;
            }
            catch
            {
                UPDATEURL = "http://update.myivision.com/ivision30/Default/";
            }
        }
        #endregion
 
        #region Events
        #region TrayIcon Events

        /// <summary>
        /// 업데이트 클릭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void menuUpdate_Click(object sender, EventArgs e)
        {
            ProcessUpdater();
        }

        /// <summary>
        /// 언어 변경 클릭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void menuCulture_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.MenuItem item = sender as System.Windows.Forms.MenuItem;

            if (item != null)
            {
                try
                {
                    ChangeCulture(arrLaguageInfo[item.Index]);

                    Properties.Settings.Default.Default_Culture = arrLaguageInfo[item.Index];
                    Properties.Settings.Default.Save();

                    notifier.Dispose();
                    contextMenu1.Dispose();
                    notifier = null;

                    contextMenu1 = new System.Windows.Forms.ContextMenu();
                    InitializeNotifyIcon();
                }
                catch (Exception ex){
                    logger.Error(ex.Message);
                }
            }
        }

        /// <summary>
        /// 종료 클릭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void menuExit_Click(object sender, EventArgs e)
        {
            ReleaseObjects();

            this.Close();
        }

        /// <summary>
        /// 플레이어 감시 클릭
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void menuitemWatchAgent_Click(object sender, EventArgs e)
        {
            _bWatchAgent = !_bWatchAgent;

            Properties.Settings.Default.WatchMode = _bWatchAgent;
            Properties.Settings.Default.Save();

            notifier.Dispose();
            contextMenu1.Dispose();
            notifier = null;

            contextMenu1 = new System.Windows.Forms.ContextMenu();
            InitializeNotifyIcon();

            InitializeWatcher();
        }
        #endregion

        /// <summary>
        /// 업데이터 폼 로드 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UpdaterForm_Load(object sender, EventArgs e)
        {
            this.Hide();

            _bWatchAgent = true;
            try
            {
                _bWatchAgent = Properties.Settings.Default.WatchMode;
            }
            catch
            {
                try
                {
                    Properties.Settings.Default.WatchMode = true;
                    Properties.Settings.Default.Save();
                }
                catch { }
            }

            InitializeNotifyIcon();
            InitializeWatcher();
            InitializeUpdater();

        }

        /// <summary>
        /// 업데이터 폼 종료 이벤트
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UpdaterForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            ReleaseObjects();
        }

        /// <summary>
        /// 업데이트 시작
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void bwUpdater_DoWork(object sender, DoWorkEventArgs e)
        {
            if (e.Cancel) return;

            DeleteOldFiles(strAppDir + TEMPFOLDER, 31);
            DeleteOldFiles(strAppDir + BACKUPFOLDER, 31);
            DeleteOldFiles(strAppDir + UPDATEFOLDER, 31);

            //  10초뒤에 업데이트 시작
            long lCountSec = 0;
            while (lCountSec++ < 10)
            {
                if (e.Cancel) return;
                Thread.Sleep(1000);
            }

            while(!e.Cancel)
            {
                try
                {
                    ProcessUpdater();

                    long lSecRefresh = 120 * 60;
                    try
                    {
                        lSecRefresh = Properties.Settings.Default.UpdateRefreshMinute * 60;
                    }
                    catch
                    {
                    	try
                    	{
                            Properties.Settings.Default.UpdateRefreshMinute = 120;
                            Properties.Settings.Default.Save();
                    	}
                    	catch {}
                    }
                    String sUpdateURL = Properties.Settings.Default.UpdateURL;

                    long lSec = 0;
                    while (lSec++ < lSecRefresh)
                    {
                        if (e.Cancel) return;
                        Thread.Sleep(1000);
                    }
                }
                catch (System.Exception err)
                {
                    logger.Error(err.Message);
                    
                    long lSec = 0;
                    while (lSec++ < 10)
                    {
                        if (e.Cancel) return;
                        Thread.Sleep(1000);
                    }
                }


                
            }
        }

        /// <summary>
        /// 프로세스 감시 시작
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void bwWatchProcess_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                while(_bWatchAgent)
                {
                    long lSecWatcher = 5;
                    try
                    {
                        lSecWatcher = Properties.Settings.Default.WatchRefreshSecond;
                    }
                    catch
                    {
                    	try
                    	{
                            Properties.Settings.Default.WatchRefreshSecond = 5;
                            Properties.Settings.Default.Save();
                    	}
                    	catch {}
                    }

                    String[] arrPrcessList = "DS.ScreenAgent.exe".Split('|');

                    try
                    {
                        arrPrcessList = Properties.Settings.Default.WatchProcesses.Split('|');
                    }
                    catch
                    {
                        try
                        {
                            Properties.Settings.Default.WatchProcesses = "DS.ScreenAgent.exe";
                            Properties.Settings.Default.Save();
                        }
                        catch { }
                    }
                    
                    long lSec = 0;
                    while (lSec++ < lSecWatcher)
                    {
                        if (e.Cancel) return;
                        Thread.Sleep(1000);
                    }

                    lock (lockUpdater)
                    {
                        if (arrPrcessList.Length > 0)
                        {
                            foreach (String sProcess in arrPrcessList)
                            {
                                Process proc = FindProcess(sProcess);

                                if (proc == null)
                                {
                                    RunProcess(sProcess);
                                }
                            }
                        }
                    }
                    
                }
            }
            catch (System.Exception ex)
            {
                logger.Error(ex.Message);
            }

        }
        #endregion

        #region Functions

        #region Initialize
        /// <summary>
        /// 로그 Configuration 초기화
        /// </summary>
        private void InitLogger()
        {
            try
            {
                string strAppDir = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetModules()[0].FullyQualifiedName);
                string appData = strAppDir + "\\PlayerData\\";
                // get own version
                Assembly myAssembly = Assembly.GetExecutingAssembly();
                AssemblyName myAssemblyName = myAssembly.GetName();
                long version = ((long)myAssemblyName.Version.Major << 48) |
                    ((long)myAssemblyName.Version.Minor << 32) |
                    (myAssemblyName.Version.Build << 16) |
                    myAssemblyName.Version.Revision;

                // configuring log output
                FileTarget target = new FileTarget();
                target.Layout = "${longdate}\t[${level}]\t${message}";
                target.FileName = appData + "update_log.txt";
                target.ArchiveFileName = appData + "archives/update.{#####}.txt";

				target.ArchiveEvery = FileArchivePeriod.Day;
				target.MaxArchiveFiles = 31;
				target.ArchiveNumbering = ArchiveNumberingMode.Sequence;
				// this speeds up things when no other processes are writing to the file
                target.ConcurrentWrites = true;
                NLog.Config.SimpleConfigurator.ConfigureForTargetLogging(target, LogLevel.Debug);

                logger.Info("Updater " + ((version >> 48) & 0xFFFF) + "." +
                        ((version >> 32) & 0xFFFF) + "." +
                        ((version >> 16) & 0xFFFF) + "." + (version & 0xFFFF));
            }
            catch
            {

            }
        }


        private void InitializeNotifyIcon()
        {
            //	Configure 메뉴
//             System.Windows.Forms.MenuItem menu = new System.Windows.Forms.MenuItem();
//             menu.Text = Properties.Resources.menuitemConfigure;
//             menu.Click += new EventHandler(menuConfigure_Click);
//             contextMenu1.MenuItems.Add(menu);

            System.Windows.Forms.MenuItem menu = new System.Windows.Forms.MenuItem();
            menu.Text = Properties.Resources.menuitemUpdate;
            menu.Click += new EventHandler(menuUpdate_Click);
            contextMenu1.MenuItems.Add(menu);


            //	Language 메뉴
            menu = new System.Windows.Forms.MenuItem();
            menu.Text = Properties.Resources.menuitemLanguage;


            foreach (CultureInfo info in arrLaguageInfo)
            {
                bool bChecked = Properties.Resources.Culture != null ? Properties.Resources.Culture.DisplayName.Equals(info.DisplayName) : false;
                System.Windows.Forms.MenuItem item = new System.Windows.Forms.MenuItem();
                item.Text = info.DisplayName;
                item.Checked = bChecked;
                item.Click += new EventHandler(menuCulture_Click);
                menu.MenuItems.Add(item);
            }

            contextMenu1.MenuItems.Add(menu);

            //	Watch Agent 메뉴
            menu = new System.Windows.Forms.MenuItem();
            menu.Checked = _bWatchAgent;
            menu.Text = Properties.Resources.menuitemWatchAgent;
            menu.Click += new EventHandler(menuitemWatchAgent_Click);
            contextMenu1.MenuItems.Add(menu);


            //	Exit 메뉴
            menu = new System.Windows.Forms.MenuItem();
            menu.Text = Properties.Resources.menuitemExit;
            menu.Click += new EventHandler(menuExit_Click);
            contextMenu1.MenuItems.Add(menu);

            notifier = new NotifyIcon();

            notifier.Icon = this.Icon;
            notifier.ContextMenu = contextMenu1;
            notifier.Text = Properties.Resources.titleUpdater;

            notifier.Visible = true;

            logger.Info("Notify Icon Initialized.");
        }

        private void InitializeUpdater()
        {
            if (bwUpdater != null)
            {
                try
                {
                    bwUpdater.CancelAsync();
                    bwUpdater.Dispose();
                }
                catch { }
                finally
                {
                    bwUpdater = null;
                }
            }

            bwUpdater = new BackgroundWorker();
            bwUpdater.WorkerSupportsCancellation = true;
            bwUpdater.DoWork += new DoWorkEventHandler(bwUpdater_DoWork);
            bwUpdater.RunWorkerAsync();
        }

        private void InitializeWatcher()
        {
            if (bwWatchProcess != null)
            {
                try
                {
                    bwWatchProcess.CancelAsync();
                    bwWatchProcess.Dispose();
                }
                catch { }
                finally
                {
                    bwWatchProcess = null;
                }
            }

            if (_bWatchAgent)
            {
                bwWatchProcess = new BackgroundWorker();
                bwWatchProcess.WorkerSupportsCancellation = true;
                bwWatchProcess.DoWork += new DoWorkEventHandler(bwWatchProcess_DoWork);
                bwWatchProcess.RunWorkerAsync();
            }
        }
        #endregion

        #region Uninitialize
        void ReleaseObjects()
        {
            try
            {

                if (notifier != null)
                {
                    notifier.Dispose();
                    notifier = null;
                }

                if (_mutex != null)
                    _mutex.ReleaseMutex();

            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }
        }
        #endregion

        void DeleteOldFiles(String sDirectory, int nDays)
        {
            try
            {
                String[] arrDirectories = Directory.GetDirectories(sDirectory);

                if(arrDirectories != null && arrDirectories.Length > 0)
                {
                    foreach (String sDic in arrDirectories)
                    {
                        try
                        {
                            DirectoryInfo dinfo = new DirectoryInfo(sDic);

                            if ((DateTime.Now - dinfo.CreationTime).TotalDays > nDays)
                            {
                               logger.Info(String.Format("Deleting old directories : {0}", sDic));
                               dinfo.Delete(true);
                            }
                        }
                        catch (System.Exception err)
                        {
                            logger.Error(err.Message);
                        }

                    }
                }

                String[] arrFiles = Directory.GetFiles(sDirectory);

                if (arrFiles != null && arrFiles.Length > 0)
                {
                    foreach (String sFile in arrFiles)
                    {
                        try
                        {
                            FileInfo finfo = new FileInfo(sFile);

                            if ((DateTime.Now - finfo.CreationTime).TotalDays > nDays)
                            {
                               logger.Info(String.Format("Deleting old files : {0}", sFile));
                               finfo.Delete();
                            }
                        }
                        catch (System.Exception err)
                        {
                            logger.Error(err.Message);
                        }                   
                    }
                }

            }
            catch (System.Exception e)
            {
                logger.Error(e.Message);
            }
        }

        void ProcessUpdater()
        {
            lock (lockUpdater) 
            {
                _arrDownloadedCabFile.Clear();

                // 1.  에서 다운로드 받을 XML 가져오기
                // 2. 가져온 XML 파싱
                logger.Info("Update StateChanged: List Parsing...");
                // 3. 다운로드 목록 작성
                // 4. CAB 파일 다운로드
                if (ParsingListAndDownloadUpdateFile())
                {
                    // 6. 파일 백업 및 CAB 파일 적용
                    UpdateVersions();
                }
                else
                {
                    currState = UpdateState.None;
                    logger.Info("Update StateChanged: None...");
                }

            }
        }


        /// <summary>
        /// 업데이트 로직 실행
        /// </summary>
        /// <returns></returns>
        bool UpdateVersions()
        {
            foreach(UpdateFile _cabfile in _arrDownloadedCabFile)
            {
                try
                {
                    // 5. CAB 파일 압축 해제
                    currState = UpdateState.CabExtracting;
                    logger.Info("Update StateChanged: Cab File Extracting...");

                    if (_cabfile.ExtractFiles())
                    {
                        logger.Info("Extracting Success : " + _cabfile.FilePath);

                        try
                        {
                            currState = UpdateState.BackUpOldVersion;
                            logger.Info("Update StateChanged: Backup Old Versions...");

                            if (_cabfile.BackupFiles())
                            {
                                currState = UpdateState.FileUpdating;
                                logger.Info("Update StateChanged: File Updating...");

                                //  관련 프로세스 종료
                                StopProcesses(_cabfile.PackageNames);

                                if (_cabfile.PackageName.Equals("Updater"))
                                {
                                    RunProcess("iVision.CopyFiles.exe", _cabfile.CabName);
                                    Thread.Sleep(1000);
                                }
                                else
                                {
                                    Thread.Sleep(1000);

                                    if (_cabfile.UpdateFiles())
                                    {
                                        logger.Info("Update Success : " + _cabfile.FilePath);
                                        currState = UpdateState.UpdateCompleting;
                                    }
                                    else
                                    {
                                        try
                                        {
                                            currState = UpdateState.BackupRecovering;
                                            logger.Info("Update StateChanged: Backup Versions Recovering...");

                                            _cabfile.RecoveryFiles();
                                        }
                                        catch (System.Exception e)
                                        {
                                            currState = UpdateState.ErrorOccurred;
                                            logger.Error("Recovery Error : " + _cabfile.BackupPath + _cabfile.CabName);
                                        }
                                    }
                                }
                            }
                        }
                        catch (System.Exception ex)
                        {
                            logger.Error("General Error : " + ex.Message);
                        }

                    }
                    else
                    {
                        currState = UpdateState.ErrorOccurred;

                        logger.Error("Extracting Cab File : " + _cabfile.FilePath);
                    }
                }
                catch (System.Exception e)
                {
                    currState = UpdateState.ErrorOccurred;
                    logger.Error("Extracting Cab File : " + _cabfile.FilePath + " : " + e.Message);
                }
                
            }


            return false;
        }

 
        /// <summary>
        /// 리스트 파일 다운로드/분석/체크
        /// </summary>
        /// <returns></returns>
        bool ParsingListAndDownloadUpdateFile()
        {
            currState = UpdateState.ListParsing;

            //	임시 폴더 생성.
            if (!Directory.Exists(strAppDir + TEMPFOLDER))
                Directory.CreateDirectory(strAppDir + TEMPFOLDER);

            String sUpdateFile = strAppDir + TEMPFOLDER + "\\" + UPDATELIST;

            if (File.Exists(sUpdateFile))
                File.Delete(sUpdateFile);

            if (!DownloadWebFile(UPDATEURL + UPDATELIST, sUpdateFile))
            {
                logger.Error("Update List File Download Failed...");
                return false;
            }

            _parser = new iVisionUpdater.UpdateXmlParser(sUpdateFile);
            foreach (XmlNode nodePackage in _parser.Packages)
            {
                String sPackageName = nodePackage.Attributes["Name"].Value;
                String sPackageFileName = nodePackage.Attributes["FileName"].Value;

                if (System.IO.File.Exists(strAppDir + sPackageFileName))
                {
                    FileVersionInfo info = FileVersionInfo.GetVersionInfo(strAppDir + sPackageFileName);

                    List<XmlNode> updateNodes = _parser.GetUpdateFrom(sPackageName, info.FileVersion);
                    if(updateNodes != null && updateNodes.Count > 0)
                    {
                        logger.Info(String.Format("Update Found: Package {0}, Count {1}", sPackageName, updateNodes.Count));

                        foreach (XmlNode nodeUpdate in updateNodes)
                        {
                            String sDestFile = strAppDir + TEMPFOLDER + String.Format("\\{0}_{1}.cab", sPackageName, nodeUpdate.Attributes["Version"].Value);
                            currState = UpdateState.CabDownloading;

                            if (DownloadWebFile(nodeUpdate.Attributes["Url"].Value, sDestFile))
                            {
                                String[] arrPackages = nodePackage.Attributes["Associates"].Value.Split('|');

                                UpdateFile piece = new UpdateFile(sDestFile);
                                foreach(String _file in arrPackages)
                                {
                                    if (!String.IsNullOrEmpty(_file)) piece.PackageNames.Add(_file);
                                }

                                piece.PackageName = nodePackage.Attributes["Name"].Value;
                               
                                _arrDownloadedCabFile.Add(piece);
                                
                                logger.Info(String.Format("Download Success: Version {0}, URL {1}", nodeUpdate.Attributes["Version"].Value, nodeUpdate.Attributes["Url"].Value));
                            }
                            else
                                logger.Error(String.Format("Download Failed: Version {0}, URL {1}", nodeUpdate.Attributes["Version"].Value, nodeUpdate.Attributes["Url"].Value));
                        }
                    }
                }
            }

            return true;
        }

        void ChangeCulture(CultureInfo info)
        {
            Properties.Resources.Culture = info;
            Thread.CurrentThread.CurrentCulture = Thread.CurrentThread.CurrentUICulture = info;
        }

        /// <summary>
        /// 프로세스 찾기
        /// </summary>
        /// <param name="sFilename"></param>
        /// <returns></returns>
        Process FindProcess(string sFilename)
        {
            String sProcessName = sFilename.ToLower().Replace(".exe", "");
            Process[] processlist = Process.GetProcessesByName(sProcessName);
            foreach (Process proc in processlist)
            {
                try
                {
                    String fname = System.IO.Path.GetFileName(proc.MainModule.FileName).ToLower();
                    if (fname.Equals(sFilename.ToLower()))
                    {
                        return proc;
                    }
                }
                catch (Exception err)
                {
                    logger.Error(err.Message);
                }
            }
            return null;
        }

        /// <summary>
        /// 프로세스 실행
        /// </summary>
        /// <param name="sFilename"></param>
        void RunProcess(String sFilename)
        {
            try
            {
                string strAppDir = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetModules()[0].FullyQualifiedName);

                Process proc = new Process();
                proc.StartInfo.FileName = strAppDir + "\\" + sFilename;
                proc.StartInfo.WorkingDirectory = strAppDir;
                proc.Start();

                logger.Info(sFilename + " process will be started.");

            }
            catch (System.Exception e)
            {
                logger.Error(e.Message);
            }

        }

        /// <summary>
        /// 프로세스 실행
        /// </summary>
        /// <param name="sFilename"></param>
        /// <param name="sArgument"></param>
        void RunProcess(String sFilename, String sArgument)
        {
            try
            {
                string strAppDir = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetModules()[0].FullyQualifiedName);

                Process proc = new Process();
                proc.StartInfo.FileName = strAppDir + "\\" + sFilename;
                proc.StartInfo.WorkingDirectory = strAppDir;
                proc.StartInfo.Arguments = sArgument;
                proc.Start();

                logger.Info(sFilename + " process will be started.");

            }
            catch (System.Exception e)
            {
                logger.Error(e.Message);
            }

        }

        /// <summary>
        /// 프로세스 종료
        /// </summary>
        /// <param name="proc"></param>
        private void StopProcess(Process proc)
        {
            try
            {
                if (proc == null) return;

                String fname = proc.ProcessName;


                Thread.Sleep(100);
                proc.Kill();
                proc.Dispose();
                logger.Info("Founded '" + fname + "' process. It will be closed.");
                Thread.Sleep(100);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }
        }

        private void StopProcesses (List<String> packageNames)
        {
            foreach (String _file in packageNames)
            {
                try
                {
                    StopProcess(FindProcess(_file));
                }
                catch
                {
                }
            }

        }

        /// <summary>
        /// Web에서 파일을 다운로드 함
        /// </summary>
        /// <param name="file_url"></param>
        /// <param name="dest_url"></param>
        /// <returns></returns>
        bool DownloadWebFile(string file_url, string dest_url)
        {
            try
            {
                WebClient client = new WebClient();

                client.UseDefaultCredentials = true;

                FileInfo info = new FileInfo(dest_url);
                if (!Directory.Exists(info.DirectoryName))
                    Directory.CreateDirectory(info.DirectoryName);

                logger.Info("Downloading " + info.Name);

                client.DownloadFile(file_url, dest_url);
            }
            catch (Exception ee)
            {
                logger.Error(ee.ToString());
                return false;
            }

            return true;
        }
        #endregion
    }
}
