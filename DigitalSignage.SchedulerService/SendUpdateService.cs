﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NLog;
using DigitalSignage.EventReciever;
using DigitalSignage.Common;
using System.ComponentModel;
using System.Threading;

namespace DigitalSignage.SchedulerService
{
    public class SendUpdateService
    {
        static readonly object lockObject = new object();

        /// <summary>
        /// 업데이트 세션
        /// </summary>
        public class UpdateSession
        {
            /// <summary>
            /// 생성자
            /// </summary>
            /// <param name="gid"></param>
            /// <param name="pid"></param>
            public UpdateSession(String gid, String pid)
            {
                if (String.IsNullOrEmpty(gid)) GID = String.Empty;
                else GID = gid;
                if (String.IsNullOrEmpty(pid)) PID = String.Empty;
                else PID = pid;
                
                RequestDT = DateTime.Now;
            }

            public String GID = String.Empty;
            public String PID = String.Empty;
            public DateTime RequestDT = DateTime.Now;
        };

        private List<UpdateSession> arrUpdateList = new List<UpdateSession>();


        private static Logger logger = LogManager.GetCurrentClassLogger();

        #region Single Tone 객체 관련
        private static SendUpdateService instance = null;
        private static readonly object objLock = new object();

        /// <summary>
        /// 싱글톤 객체 인스턴스
        /// </summary>
        public static SendUpdateService GetInstance
        {
            get
            {
                lock (objLock)
                {
                    if (instance == null)
                    {
                        instance = new SendUpdateService();
                    }
                    return instance;
                }
            }
        }
        #endregion

        int nModuleID = -1;

        BackgroundWorker _thProcess = null;

        /// <summary>
        /// 초기화 함수
        /// </summary>
        public void Initialize()
        {
            if (_thProcess == null)
            {
                _thProcess = new BackgroundWorker();
                _thProcess.WorkerSupportsCancellation = true;
                _thProcess.DoWork += new DoWorkEventHandler(_thProcessing_DoWork);

            }

            if (_thProcess != null && !_thProcess.IsBusy)
            {
                _thProcess.RunWorkerAsync();

            }

            logger.Info("SendUpdateService is started.");
        }

        /// <summary>
        /// 처리 루틴
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void _thProcessing_DoWork(object sender, DoWorkEventArgs e)
        {
            logger.Info("SendUpdateService Background Worker is started.");

            while (!e.Cancel)
            {
                try
                {
                    while (arrUpdateList.Count > 0)
                    {
                        UpdateSession s = null;

                        lock (lockObject)
                        {
                            s = arrUpdateList[0];
                            arrUpdateList.Remove(s);
                        }

                        Config.GetConfig.SendUpdateScheduleMethod(s.GID, s.PID);

                    }

                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message);
                }
                System.Threading.Thread.Sleep(500);
            }
        }

        /// <summary>
        /// 업데이트 세션 추가
        /// </summary>
        /// <param name="gid"></param>
        /// <param name="pid"></param>
        public void AddSession(String gid, string pid)
        {
            lock (lockObject)
            {
                foreach (UpdateSession s in arrUpdateList)
                {
                    if (s.GID == gid && s.PID == pid)
                    {
                        return;
                    }
                }
                arrUpdateList.Add(new UpdateSession(gid, pid));
            }
        }

        /// <summary>
        /// 소멸자 함수
        /// </summary>
        public void Uninitialize()
        {

            try
            {
                if (_thProcess != null)
                {
                    _thProcess.CancelAsync();
                }
            }
            catch { }

            logger.Info("SendUpdateService is ended.");

        }

    }
}
