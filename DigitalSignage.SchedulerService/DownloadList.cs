﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;

using DigitalSignage.Common;
using DigitalSignage.DataBase;
using NLog;
//SQLite & DB routines
using System.Data.Common;

namespace DigitalSignage.SchedulerService
{
	public class DownloadList : MarshalByRefObject, IDownloadList
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

		public DataSet.downloadmasterDataTable GetDataByGID(string duuid, string gid)
		{
			Config cfg = Config.GetConfig;

			try
			{
				using (DataBase.DataSetTableAdapters.downloadmasterTableAdapter da = cfg.dbLayer.CreateDownloadMasterDA())
				{
					using (DataSet.downloadmasterDataTable dt = da.GetDataByGID(duuid, gid))
					{
						da.Dispose();
						return dt;
					}
				}
			}
			catch(Exception e)
			{
				logger.Error(e.Message);
			}

			return null;
		}
		public DataSet.downloadmasterDataTable GetDataByGID(string gid, long start, long end)
		{
			Config cfg = Config.GetConfig;

			try
			{
				using (DataBase.DataSetTableAdapters.downloadmasterTableAdapter da = cfg.dbLayer.CreateDownloadMasterDA())
				{
					using (DataSet.downloadmasterDataTable dt = da.GetDataByGIDBetweenTime(gid, start, end, start, end, start, end))
					{
						da.Dispose();
						return dt;
					}
				}

			}
			catch (Exception e)
			{
				logger.Error(e.Message);
			}

			return null;
		}

		public DataSet.downloadmasterDataTable GetDataByPID(string duuid, string gid, string pid)
		{
			Config cfg = Config.GetConfig;

			try
			{
				using (DataBase.DataSetTableAdapters.downloadmasterTableAdapter da = cfg.dbLayer.CreateDownloadMasterDA())
				{
					using (DataSet.downloadmasterDataTable dt = da.GetDataByPID(duuid, gid, pid))
					{
						da.Dispose();
						return dt;
					}
				}

			}
			catch (Exception e)
			{
				logger.Error(e.Message);
			}

			return null;
		}

		public DataSet.downloadmasterDataTable GetDataByPID(string gid, string pid, long start, long end)
		{
			Config cfg = Config.GetConfig;

			try
			{
				using (DataBase.DataSetTableAdapters.downloadmasterTableAdapter da = cfg.dbLayer.CreateDownloadMasterDA())
				{
					using (DataSet.downloadmasterDataTable dt = da.GetDataByPIDBetweenTime(gid, pid, start, end, start, end, start, end))
					{
						da.Dispose();
						return dt;
					}
				}

			}
			catch (Exception e)
			{
				logger.Error(e.Message);
			}

			return null;
		}

		public int InsertDownloadInformation(string duuid, string gid, string pid, long reg_date)
		{
			Config cfg = Config.GetConfig;

			try
			{
				int nRet = 0;
				using (DataBase.DataSetTableAdapters.downloadmasterTableAdapter da = cfg.dbLayer.CreateDownloadMasterDA())
				{
					nRet = da.InsertDownloadedPlayer(duuid, gid, pid, reg_date);
					da.Dispose();
				}

				return nRet;
			}
			catch (Exception e)
			{
				logger.Error(e.Message);
			}

			return -1;
		}

		public int UpdateRegDate(int id, long reg_date)
		{
			Config cfg = Config.GetConfig;

			try
			{
				int nRet = 0;
				using (DataBase.DataSetTableAdapters.downloadmasterTableAdapter da = cfg.dbLayer.CreateDownloadMasterDA())
				{
					nRet = da.UpdateRegDate(reg_date, id);
					da.Dispose();
				}

				return nRet;
			}
			catch (Exception e)
			{
				logger.Error(e.Message);
			}

			return -1;
		}

    }
}
