﻿using System;
using System.Data.Common;
//using System.Data;

using NLog;

using DigitalSignage.DataBase;
using DigitalSignage.Common;

namespace DigitalSignage.SchedulerService
{
    class GroupList : MarshalByRefObject, IGroupList
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public DataSet.groupsDataTable GetGroupByID(int id)
        {
            Config cfg = Config.GetConfig;
            try
            {
		DataSet.groupsDataTable table = cfg.dbLayer.CreateGroupViewDA(id);
            }
            catch (Exception e)
            {
                logger.Log(LogLevel.Error, e + "");
            }
            return null;
        }

        public int AddGroups(DataSet.groupsDataTable newGroup)
        {
            Config cfg = Config.GetConfig;
            try
            {
                 da = cfg.dbLayer.CreateGroupDA(id);		 
            }
            catch (Exception e)
            {
                logger.Log(LogLevel.Error, e + "");
            }
            return -1; //not implemented
        }

        public int UpdateGroups(DataSet.groupsDataTable group)
        {
            return -1; //not implemented
        }

        public int DeleteGroups(DataSet.groupsDataTable group)
        {
            return -1; //not implemented
        }
    }
}
