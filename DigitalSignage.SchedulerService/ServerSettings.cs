﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using NLog;

namespace DigitalSignage.SchedulerService
{
	class ServerSettings
	{
		private static Logger logger = LogManager.GetCurrentClassLogger();

		#region Service Setting Values

		/// <summary>
		/// 서버의 고유 guid를 리턴한다.
		/// </summary>
		static public string Initial_Code
		{
			get
			{
				string initial = "";
				try
				{
					initial = DigitalSignage.Common.ConfigSettings.ReadSetting("initialized_code");
					if(String.IsNullOrEmpty(initial))
					{
						initial = Guid.NewGuid().ToString();
						DigitalSignage.Common.ConfigSettings.WriteSetting("initialized_code", initial);
					}
				}
				catch
				{
					initial = Guid.NewGuid().ToString();
					DigitalSignage.Common.ConfigSettings.WriteSetting("initialized_code", initial);
				}
				return initial;
			}
		}


		/// <summary>
		/// 최대 클라이언트 연결 개수
		/// </summary>
		static public int MaxConnections
		{
			get
			{
				int nMaxConnection = 1000;
				try
				{
					nMaxConnection = int.Parse(ConfigurationManager.AppSettings["max_connection"].ToString());
				}
				catch { nMaxConnection = 1000; }
				
				return nMaxConnection;
			}
		}

		/// <summary>
		/// WCF Timeout 대기 시간(초)
		/// </summary>
		static public int Send_Timeout
		{
			get
			{
				int iSeconds = 5;
				try
				{
					iSeconds = int.Parse(ConfigurationManager.AppSettings["sendouttime"].ToString());
				}
				catch { iSeconds = 5; }
				
				return iSeconds;
			}
		}

        /// <summary>
        /// DB 세션 타임 아웃(밀리초)
        /// </summary>
        static public int DBSessionTimeout
        {
            get
            {
                int iSeconds = 3600000;
                try
                {
                    iSeconds = int.Parse(ConfigurationManager.AppSettings["db_session_timeout"].ToString());
                }
                catch { iSeconds = 3600000; }

                return iSeconds;
            }
        }


		/// <summary>
		/// WCF 메시지 서비스 주소
		/// </summary>
		static public string Message_Address
		{
			get
			{
				string addr = "net.tcp://localhost:40526/INetworkHost/";
				try
				{
					addr = ConfigurationManager.AppSettings["addr"];
				}
				catch
				{ addr = "net.tcp://localhost:40526/INetworkHost/"; }

				return addr;
			}
		}

		static public string File_Address
		{
			get
			{
				string addr = "net.tcp://localhost:40526/INetworkHost/";
				try
				{
					addr = ConfigurationManager.AppSettings["addr_file"];
				}
				catch
				{ addr = "net.tcp://localhost:40525/INetworkHost/mex"; }

				return addr;
			}
		}

		static public int Achiving_Log_Refresh_Time
		{
			get	{
				int nRefreshLog = 86400;
				try
				{
					nRefreshLog = int.Parse(ConfigurationManager.AppSettings["achivinglog_refreshtime"].ToString());
				}
				catch { nRefreshLog = 86400; }

				return nRefreshLog;
			}
		}

		static public int DB_Connections
		{
			get
			{
				int nDBConnections = 20;
				try
				{
					nDBConnections = int.Parse(ConfigurationManager.AppSettings["db_connections"].ToString());
				}
				catch { nDBConnections = 20; }

				return nDBConnections;
			}
		}

        static public bool TimeSyncMode
        {
            get
            {
                bool btimesync = true;
                try
                {
                    btimesync = bool.Parse(ConfigurationManager.AppSettings["timesync"].ToString());
                }
                catch { btimesync = true; }

                return btimesync;
            }
        }

		static public string Connection_String
		{
			get
			{
				string conn_string = "Driver=SQLite ODBC Driver";
				try
				{
					conn_string = ConfigurationManager.AppSettings["connection_string"];
				}
				catch
				{ conn_string = "Driver=SQLite ODBC Driver"; }

				return conn_string;
			}
		}

		static public string Log_Connection_String
		{
			get
			{
				string conn_string = "Driver=SQLite ODBC Driver";
				try
				{
					conn_string = ConfigurationManager.AppSettings["log_connection_string"];
				}
				catch
				{ conn_string = "Driver=SQLite ODBC Driver"; }

				return String.IsNullOrEmpty(conn_string) ? "Driver=SQLite ODBC Driver" : conn_string;
			}
		}

		static public string Serial_Key
		{
			get
			{
				string key = "";
				try
				{
					key = ConfigurationManager.AppSettings["serial_key"];
				}
				catch
				{ key = ""; }

				return key;
			}
		}

        /// <summary>
        /// 섬네일 사용 여부
        /// </summary>
        static public bool IsSupportedThumbnail
        {
            get
            {
                bool key = false;
                try
                {
                    key = Convert.ToBoolean(ConfigurationManager.AppSettings["is_thumb_supported"]);
                }
                catch
                { key = false; }

                return key;
            }
        }

        /// <summary>
        /// 섬네일 가져오는 URL 정보
        /// </summary>
        static public string ThumbGetURL
        {
            get
            {
                string key = "";
                try
                {
                    key = ConfigurationManager.AppSettings["thumb_get_url"];
                }
                catch
                { key = ""; }

                return key;
            }
        }

        /// <summary>
        /// 섬네일 넣는 URL (FTP)
        /// </summary>
        static public string ThumbSetURL
        {
            get
            {
                string key = "";
                try
                {
                    key = ConfigurationManager.AppSettings["thumb_set_url"];
                }
                catch
                { key = ""; }

                return key;
            }
        }

        /// <summary>
        /// 섬네일 넣는 URL (FTP)
        /// </summary>
        static public string ThumbSetAuthID
        {
            get
            {
                string key = "";
                try
                {
                    key = ConfigurationManager.AppSettings["thumb_set_auth_id"];
                }
                catch
                { key = ""; }

                return key;
            }
        }
        /// <summary>
        /// 섬네일 넣는 URL (FTP)
        /// </summary>
        static public string ThumbSetAuthPassword
        {
            get
            {
                string key = "";
                try
                {
                    key = ConfigurationManager.AppSettings["thumb_set_auth_pass"];
                }
                catch
                { key = ""; }

                return key;
            }
        }
        /// <summary>
        /// 릴레이 서버 인증 ID
        /// </summary>
        static public string RelayServerAuthID
        {
            get
            {
                string key = "";
                try
                {
                    key = ConfigurationManager.AppSettings["relay_auth_id"];
                }
                catch
                { key = "rider"; }

                return key;
            }
        }

        /// <summary>
        /// 릴레이 서버 인증 Password
        /// </summary>
        static public string RelayServerAuthPassword
        {
            get
            {
                string key = "";
                try
                {
                    key = ConfigurationManager.AppSettings["relay_auth_pass"];
                }
                catch
                { key = "3203"; }

                return key;
            }
        }

        /// <summary>
        /// NPCU 서비스 리스너 포트
        /// </summary>
        static public int NPCUServicePort
        {
            get
            {
                int key = 9999;
                try
                {
                    key = Convert.ToInt32(ConfigurationManager.AppSettings["NPCU_service_port"]);
                }
                catch
                { key = 9999; }

                return key;
            }
        }
        /// <summary>
        /// NPCU 서비스 전송 방식 (tcp / udp)
        /// </summary>
        static public string NPCUServiceMethod
        {
            get
            {
                string key = "tcp";
                try
                {
                    key = ConfigurationManager.AppSettings["NPCU_service_method"];
                }
                catch
                { key = "tcp"; }

                return key;
            }
        }

		static public bool IsDailyReportEnabled
		{
			get
			{
				bool bEnabled = true;
				try
				{
                    bEnabled = bool.Parse(ConfigurationManager.AppSettings["is_daily_report_enabled"].ToString());
				}
                catch { bEnabled = true; }

                return bEnabled;
			}
		}

        static public bool IsHourlyReportEnabled
        {
            get
            {
                bool bEnabled = true;
                try
                {
                    bEnabled = bool.Parse(ConfigurationManager.AppSettings["is_hourly_report_enabled"].ToString());
                }
                catch { bEnabled = true; }

                return bEnabled;
            }
        }

		/// <summary>
		/// 로그 보관 일수
		/// </summary>
		static public int DaysToKeepLogs
		{
			get
			{
				int key = 0;
				try
				{
					key = Convert.ToInt32(ConfigurationManager.AppSettings["days_to_keep_logs"]);
				}
				catch
				{ key = 0; }

				return key;
			}
		}
        #endregion	
    }
}
