﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Threading;

using System.IO;

// .NET remoting
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;
using System.Runtime.Remoting.Channels.Tcp;

using NLog;

// using DigitalSignage.DataBase;
using DigitalSignage.Common;
using System.Data.Odbc;

namespace DigitalSignage.SchedulerService
{
	public class ServerThread
	{
		//private Config cfg = Config.GetConfig;
		private bool _process = false;
		private static Logger logger = LogManager.GetCurrentClassLogger();
		private Config cfg = Config.GetConfig;
		private Thread thLogManager = null;


    
		public int Start()
		{
			_process = true;
			//starting control thread
			Thread control = new Thread(this.ControlThread);
			control.IsBackground = true;
			control.Start();

			thLogManager = new Thread(this.thLogCleaner);
			thLogManager.Start();

			logger.Info("ServerThread started");
			return 0;
		}

		public int Stop()
		{
			_process = false;
			cfg.release();
			logger.Info("ServerThread ended");
			return 0;
		}
		private void Initialize_NetRemoting_Objects()
		{
			// creating providers chunks
			BinaryServerFormatterSinkProvider provider = new BinaryServerFormatterSinkProvider();
			provider.TypeFilterLevel = System.Runtime.Serialization.Formatters.TypeFilterLevel.Full;
			ClientIPProvider ipProvider = new ClientIPProvider();
			provider.Next = ipProvider;

			// regestering channel
			Hashtable props = new Hashtable();
			props.Add("port", 888);
			TcpChannel channel = new TcpChannel(props, null, provider);
			ChannelServices.RegisterChannel(channel, false);

			// resgestering services
			RemotingConfiguration.RegisterWellKnownServiceType(
				typeof(ServerCmdReceiver),
				"CmdReceiverHost/rt",
				WellKnownObjectMode.SingleCall);

			RemotingConfiguration.RegisterWellKnownServiceType(
				typeof(LogsList),
				"LogListHost/rt",
				WellKnownObjectMode.SingleCall);

			RemotingConfiguration.RegisterWellKnownServiceType(
				typeof(TaskList),
				"TaskListHost/rt",
				WellKnownObjectMode.SingleCall);

			RemotingConfiguration.RegisterWellKnownServiceType(
				typeof(GroupList),
				"GroupListHost/rt",
				WellKnownObjectMode.SingleCall);

			RemotingConfiguration.RegisterWellKnownServiceType(
				typeof(PlayersList),
				"PlayerListHost/rt",
				WellKnownObjectMode.SingleCall);

			RemotingConfiguration.RegisterWellKnownServiceType(
				typeof(UsersList),
				"UsersListHost/rt",
				WellKnownObjectMode.SingleCall);

			RemotingConfiguration.RegisterWellKnownServiceType(
				typeof(DownloadList),
				"DownloadListHost/rt",
				WellKnownObjectMode.SingleCall);

			RemotingConfiguration.RegisterWellKnownServiceType(
				typeof(DownloadTimer),
				"DownloadTimerHost/rt",
				WellKnownObjectMode.SingleCall);

			RemotingConfiguration.RegisterWellKnownServiceType(
				typeof(DataCacheList),
				"DataCacheHost/rt",
				WellKnownObjectMode.SingleCall);

			RemotingConfiguration.RegisterWellKnownServiceType(
				typeof(DataInfoList),
				"DataInfoHost/rt",
				WellKnownObjectMode.SingleCall);

			RemotingConfiguration.RegisterWellKnownServiceType(
				typeof(DataFieldList),
				"DataFieldHost/rt",
				WellKnownObjectMode.SingleCall);

			RemotingConfiguration.RegisterWellKnownServiceType(
				typeof(ContentsList),
				"ContentsHost/rt",
				WellKnownObjectMode.SingleCall);

			RemotingConfiguration.RegisterWellKnownServiceType(
				typeof(ResultDataList),
				"ResultDataHost/rt",
				WellKnownObjectMode.SingleCall);

			logger.Info(".NET remoting handlers registered successfuly");
		}

		private int procLogAchiving()
		{
		// 			logger.Info("Log Backup starting...");
		// 
		// 			string archiveLogFolder = cfg.AppData + "ArchiveLog\\";
		// 
		// 			if (!Directory.Exists(cfg.AppData))
		// 				Directory.CreateDirectory(cfg.AppData);
		// 			if (!Directory.Exists(archiveLogFolder))
		// 				Directory.CreateDirectory(archiveLogFolder);
		// 
		// 			if (File.Exists(cfg.AppData + "log.db3"))
		// 			{
		// 				while(true)
		// 				{
		// 					try
		// 					{
		// 						long currTime = 0;
		// 							
		// 						currTime = TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime());
		// 						
		// 						cfg.logdbLayer.CopyDB(archiveLogFolder + currTime.ToString("D15"));
		// 
		// 						logger.Info("archiving logs are succeeded!");
		// 
		// 						//	Data 삭제
		// 						int rows = 0;
		// 						if (0 <= (rows = cfg.localLogsList.DeleteLogsBefore(currTime)))
		// 							logger.Info("Deleting logs are succeeded! " + rows.ToString());
		// 						else
		// 						{
		// 							logger.Info("Deleting logs are failed!");
		// 						}
		// 
		// 						break;
		// 					}
		// 					catch
		// 					{
		// 						Thread.Sleep(60000);
		// 					}
		// 				}
		// 				return 0;
		// 			}
		// 			else
		// 			{
		// 				logger.Error("There is no file : " + cfg.AppData + "log.db3");
		// 			}

			return -1;
		}

		private void ResetStatusOfAllPlayers()
		{
		// 			try
		// 			{
		// 				using (DataBase.DataSetTableAdapters.playersTableAdapter pta = Config.GetConfig.dbLayer.CreatePlayersDA())
		// 				{
		// 					int nRet = pta.ResetState();
		// 
		// 					logger.Info(nRet.ToString() + " players' status has been reset.");
		// 					pta.Dispose();
		// 				}
		// 			}
		// 			catch (Exception ex)
		// 			{
		// 				logger.Error(ex.ToString());
		// 			}
		// 
		}

		private void ControlThread()
		{
			//	Logger Player Status Reset
			try
			{
				Initialize_NetRemoting_Objects();

				Thread.Sleep(5000);

				ResetStatusOfAllPlayers();

			}
			catch (Exception ex)
			{
				logger.Error(ex.ToString());
			}
		}

		/// <summary>
		/// 로그 삭제 스레드
		/// </summary>
		private void thLogCleaner()
		{
			logger.Info("Log Cleanning Service started...");

			int nKeepLogDays = ServerSettings.DaysToKeepLogs;

							
			String sQuery = String.Empty;

			while (_process)
			{
				try
				{
					if(nKeepLogDays > 0)
					{
						DateTime dtNow = DateTime.Now;

						if (dtNow.Hour == 0 && dtNow.Minute == 0 && dtNow.Second == 0)
						{
							Config cfg = Config.GetConfig;
							using (OdbcConnection conn = cfg.serverDBLayer.GetAvailableLogConnection())
							{
								logger.Info(String.Format("Delete Log Job Start! - {0} Days to keep", nKeepLogDays));
								long lUniversalNow = TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime() - TimeSpan.FromDays(nKeepLogDays));

								sQuery = String.Format("DELETE FROM playlogs WHERE start_dt < {0};", lUniversalNow);

								int nRet = 0;
								using (OdbcCommand comm = new OdbcCommand(sQuery, conn))
								{
									nRet = comm.ExecuteNonQuery();
									comm.Dispose();
								}

								logger.Info(String.Format("Delete Log Job Finished! - {0} logs were deleted.", nRet));
							}
						}
					}
				}
				catch (Exception ex) { logger.Error(ex.Message); }

				Thread.Sleep(900);
			}

			logger.Info("Log Cleanning Service ended...");

		}
	}
}
