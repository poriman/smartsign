﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace DigitalSignage.SchedulerService
{
	public class SearchManager
	{
		static SearchManager instance = null;

		static public SearchManager GetInstance { 
			get {
				if (instance == null)
					instance = new SearchManager();
				return instance;
			} 
		}

		public string GetAvailablePlayerID
		{
			get
			{
				#region Global Unique ID Search Logic

// 				string playerID = Config.GetConfig.CachedPlayerID;
// 
// 				CacheObject ins = CacheObject.GetInstance;
// 
// 				XmlNode node = null;
// 				
// 				while (null != (node = ins.FindPlayerNode(playerID)))
// 				{
// 					playerID = IncreaseID(playerID);
// 				}
// 
// 				Config.GetConfig.CachedPlayerID = IncreaseID(playerID);
// 
// 				return playerID;

				#endregion

				#region Global Unique ID Search Logic

				return SerialKey.CharacterizedID.ConvertToCharacterized(DateTime.UtcNow.Ticks.ToString("D19"));
	
				#endregion

			}
		}

		public string GetAvailableGroupID
		{
			get
			{
				#region Global Unique ID Search Logic

// 				string groupID = Config.GetConfig.CachedGroupID;
// 
// 				CacheObject ins = CacheObject.GetInstance;
// 
// 				XmlNode node = null;
// 
// 				while (null != (node = ins.FindGroupNode(groupID)))
// 				{
// 					groupID = IncreaseID(groupID);
// 				}
// 
// 				Config.GetConfig.CachedGroupID = IncreaseID(groupID);
// 
// 				return groupID;

				#endregion

				#region Global Unique ID Search Logic

				return SerialKey.CharacterizedID.ConvertToCharacterized(DateTime.UtcNow.Ticks.ToString("D19"));

				#endregion

			}
		}

		char[] arrPieces = new char[10] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
		public string IncreaseID(string origin)
		{
			ulong ulIdx = GetIndex(origin);
			return GetValue(++ulIdx, origin.Length);
		}

		private ulong GetIndex(string origin)
		{
			ulong ulIdx = 0;
			int nLocation = 0;
			for(int i = origin.Length -1 ; i >= 0; --i)
			{
				char ch = origin[i];

				ulIdx += (ulong)(FindIndexFromArray(ch) * (ulong)(Math.Pow(arrPieces.Length, nLocation++)));
			}

			return ulIdx;
		}

		private ulong FindIndexFromArray(char ch)
		{
			ulong nIdx = 0;
			foreach (char tmp in arrPieces)
			{
				if (tmp == ch) return nIdx;
				nIdx++;
			}

			return 0;
		}

		private string GetValue(ulong index, int originLength)
		{
			StringBuilder sb = new StringBuilder();

			ulong tmp = index;
			for (int i = 0; i < originLength; ++i)
			{
				int mod = (int)(tmp % (ulong)arrPieces.Length);
				sb.Insert(0, arrPieces[mod]);
				tmp = (ulong)(tmp / (ulong)arrPieces.Length);
			}

			return sb.ToString();
		}
	}
}
