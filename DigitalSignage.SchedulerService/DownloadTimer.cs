﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;

using DigitalSignage.Common;
using DigitalSignage.DataBase;
using NLog;
//SQLite & DB routines
using System.Data.Common;

namespace DigitalSignage.SchedulerService
{
	public class DownloadTimer : MarshalByRefObject, IDownloadTimer
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

		public DataSet.downloadtimerDataTable GetDataByPID(string pid)
		{
			Config cfg = Config.GetConfig;

			try
			{
				using (DataBase.DataSetTableAdapters.downloadtimerTableAdapter da = cfg.dbLayer.CreateDownloadTimerDA())
				{
					using (DataSet.downloadtimerDataTable dt = da.GetDataByPID(pid))
					{
						da.Dispose();
						return dt;
					}
				}
			}
			catch(Exception e)
			{
				logger.Error(e.Message);
			}

			return null;
		}

		public DataSet.downloadtimerDataTable GetDataByGIDorPID(string pid, string gid)
		{
			Config cfg = Config.GetConfig;

			try
			{
				using (DataBase.DataSetTableAdapters.downloadtimerTableAdapter da = cfg.dbLayer.CreateDownloadTimerDA())
				{
					using (DataSet.downloadtimerDataTable dt = da.GetDataByPIDorGID(pid, gid))
					{
						da.Dispose();
						return dt;
					}

				}

			}
			catch (Exception e)
			{
				logger.Error(e.Message);
			}

			return null;
		}

		public int InsertDownloadTime(string pid, string start, string end)
		{
			Config cfg = Config.GetConfig;

			try
			{
				int nRet = 0;
				using (DataBase.DataSetTableAdapters.downloadtimerTableAdapter da = cfg.dbLayer.CreateDownloadTimerDA())
				{
					nRet = da.InsertDownloadTime(pid, start, end);
					da.Dispose();
				}

				return nRet;
			}
			catch (Exception e)
			{
				logger.Error(e.Message);
			}

			return -1;
		}

		public int UpdateDownloadTime(int id, string pid, string start, string end)
		{
			Config cfg = Config.GetConfig;

			try
			{
				int nRet = 0;
				using (DataBase.DataSetTableAdapters.downloadtimerTableAdapter da = cfg.dbLayer.CreateDownloadTimerDA())
				{
					nRet = da.UpdateDownloadTime(pid, start, end, id);
					da.Dispose();
				}

				return nRet;
			}
			catch (Exception e)
			{
				logger.Error(e.Message);
			}

			return -1;
		}

		public int DeleteTimesFromPID(string pid)
		{
			Config cfg = Config.GetConfig;

			try
			{
				int nRet = 0;
				using (DataBase.DataSetTableAdapters.downloadtimerTableAdapter da = cfg.dbLayer.CreateDownloadTimerDA())
				{
					nRet = da.DeleteTimesFromPID(pid);
					da.Dispose();
				}
				return nRet;
			}
			catch (Exception e)
			{
				logger.Error(e.Message);
			}

			return -1;
		}
    }
}
