﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NLog;
using System.Data.Odbc;
using DigitalSignage.Common;
using System.Data;
using System.Collections.ObjectModel;
using System.Xml;

namespace DigitalSignage.SchedulerService.Statistics
{
	/// <summary>
	/// 스케줄 통계 리포트 클래스
	/// </summary>
	public class ScheduleReport : IReport
	{
		private static Logger logger = LogManager.GetCurrentClassLogger();

		#region IReport Interface Implementation
		/// <summary>
		/// 일별 리포트 만들기
		/// </summary>
		/// <param name="YYYY"></param>
		/// <param name="MM"></param>
		/// <param name="DD"></param>
		/// <returns></returns>
		public bool MakeDailyReport(int YYYY, int MM, int DD)
		{
			logger.Info("Starting to make a schedule report statistics: Y:{0} M:{1} D:{2}", YYYY, MM, DD);


			Config cfg = Config.GetConfig;

			String sSelectQuery = String.Empty;
			String sInsertQuery = String.Empty;

			bool bRet = false;

			try
			{
				using (OdbcConnection conn = cfg.serverDBLayer.GetAvailableLogConnection())
				{
					for (int i = 0; i < 24; ++i)
					{
						/// 1. 스케줄 전체 조회
						/// 2. 플레이어 쿼리 
						/// 3. 플레이어 스케줄 조회
						/// 4. 그룹 스케줄 조회
						DateTime dtlocalStartTime = new DateTime(YYYY, MM, DD, i, 0, 0);
						long start_dt = TimeConverter.ConvertToUTP(dtlocalStartTime.ToUniversalTime());
						long end_dt = start_dt + 3599;

						/// 스케줄 전체 조회
						sSelectQuery = String.Format("SELECT  daysofweek, duration, duuid, endtime, endtimeofday, etime, gid, guuid, name, pid, priority, starttime, starttimeofday, state, type, uuid, is_ad, meta_tag, show_flag FROM tasks WHERE (starttime BETWEEN {0} AND {1}) AND (state < 3 OR state = 5) OR (state < 3 OR state = 5) AND (endtime BETWEEN {0} AND {1}) OR (state < 3 OR state = 5) AND ({0} BETWEEN starttime AND endtime) OR (state < 3 OR state = 5) AND ({1} BETWEEN starttime AND endtime)", start_dt, end_dt);

						using (OdbcCommand comm = new OdbcCommand(sSelectQuery, conn))
						{
							using (DataTable dtTasks = new DataTable())
							{
								using (IDataReader dataReader = comm.ExecuteReader())
								{
									dtTasks.Load(dataReader);
									dataReader.Dispose();
								}

								string playerQuery = "select pid , gid from players where del_yn='N'";

								using (OdbcCommand comm2 = new OdbcCommand(playerQuery, conn))
								{

                                    using (IDataReader dataReader = comm2.ExecuteReader())
									{
										while (dataReader.Read())
										{
											int nCntTimeSched = 0;
											int nCntRollingSched = 0;

											//  플레이어 ID
											string pid = CommonUnit.GetString(dataReader["pid"]);
											Collection<string> arrGids = new Collection<string>();

											string gid = CommonUnit.GetString(dataReader["gid"]);

											#region 그룹 ID 리스트 작성

											try
											{
												XmlNode node = CacheObject.GetInstance.FindGroupNode(gid);

												XmlNode parent_node = node;
												do
												{
													try
													{
														arrGids.Add(parent_node.Attributes["gid"].Value);
													}
													catch
													{
													}
												} while ((parent_node = parent_node.ParentNode) != null);
											}
											catch (Exception ex)
											{
												logger.Error(ex.ToString());
											}
											#endregion

											/// 스케줄 검증
											foreach (DataRow task in dtTasks.Rows)
											{
												int type = Convert.ToInt32(task["type"]);
												int state = Convert.ToInt32(task["state"]);

												String desc_pid = task["pid"] as String;
												String desc_gid = task["gid"] as String;

												/// 반복 스케줄 또는 시간 스케줄이 아닌 경우
												if (type != 3 && type != 10) continue;
												/// 스케줄이 삭제된 경우
												if (state == 3) continue;

												/// 해당 스케줄이 그룹 ID에 포함되거나 플레이어 ID에 포함되는 경우
												if ((arrGids.Contains(desc_gid) && String.IsNullOrEmpty(desc_pid)) || desc_pid == pid)
												{
													int starttimeofday = Convert.ToInt32(task["starttimeofday"]);
													int endtimeofday = Convert.ToInt32(task["endtimeofday"]);
													int daysofweek = Convert.ToInt32(task["daysofweek"]);

													if (CheckContainTimeofDay(dtlocalStartTime, starttimeofday, endtimeofday)
														&& CheckDayOfWeek(dtlocalStartTime, daysofweek))
													{
														if (type == 3) nCntTimeSched++;
														else nCntRollingSched++;
													}
												}
											}

											try
											{
												sInsertQuery = String.Format("insert into report_time_schedule(YYYY,MM,DD,H24,pid,type,cnt) values('{0}','{1}','{2}','{3}','{4}',3,{5})"
													, YYYY
                                                    , MM >= 10 ? MM.ToString() : "0" + MM.ToString()
                                                    , DD >= 10 ? DD.ToString() : "0" + DD.ToString()
                                                    , i >= 10 ? i.ToString() : "0" + i.ToString()
													, pid
													, nCntTimeSched);
												//	시간 스케줄 삽입 
												using (OdbcCommand comm3 = new OdbcCommand(sInsertQuery, conn))
												{
													comm3.ExecuteNonQuery();
												}

												sInsertQuery = String.Format("insert into report_time_schedule(YYYY,MM,DD,H24,pid,type,cnt) values('{0}','{1}','{2}','{3}','{4}',10,{5})"
													, YYYY
                                                    , MM >= 10 ? MM.ToString() : "0" + MM.ToString()
                                                    , DD >= 10 ? DD.ToString() : "0" + DD.ToString()
                                                    , i >= 10 ? i.ToString() : "0" + i.ToString()
													, pid
													, nCntRollingSched);
												//	반복 스케줄 삽입 
												using (OdbcCommand comm3 = new OdbcCommand(sInsertQuery, conn))
												{
													comm3.ExecuteNonQuery();
												}
											}
											catch (Exception exInsert)
											{
												logger.Error(exInsert.Message + "Insert Query: " + sInsertQuery);
											}
										}
									}
								}
							}
							comm.Dispose();
						}
						bRet = true;
					}
				}
			}
			catch (Exception exSelect)
			{
				logger.Error(exSelect.Message + "Select Query: " + sSelectQuery);
			} 


			logger.Info("Ending to make a schedule report statistics: Y:{0} M:{1} D:{2}", YYYY, MM, DD);

			return bRet;
		}
		/// <summary>
		/// 시간별 리포트 만들기
		/// </summary>
		/// <param name="YYYY"></param>
		/// <param name="MM"></param>
		/// <param name="DD"></param>
		/// <param name="HH"></param>
		/// <returns></returns>
		public bool MakeHourlyReport(int YYYY, int MM, int DD, int HH)
		{
			logger.Info("Starting to make a schedule report statistics: Y:{0} M:{1} D:{2} H:{3}", YYYY, MM, DD, HH);

			Config cfg = Config.GetConfig;

			String sSelectQuery = String.Empty;
			String sInsertQuery = String.Empty;

			bool bRet = false;

			try
			{
				using (OdbcConnection conn = cfg.serverDBLayer.GetAvailableLogConnection())
				{

					/// 1. 스케줄 전체 조회
					/// 2. 플레이어 쿼리 
					/// 3. 플레이어 스케줄 조회
					/// 4. 그룹 스케줄 조회
					DateTime dtlocalStartTime = new DateTime(YYYY, MM, DD, HH, 0, 0);
					long start_dt = TimeConverter.ConvertToUTP(dtlocalStartTime.ToUniversalTime());
					long end_dt = start_dt + 3599;

					/// 스케줄 전체 조회
					sSelectQuery = String.Format("SELECT  daysofweek, duration, duuid, endtime, endtimeofday, etime, gid, guuid, name, pid, priority, starttime, starttimeofday, state, type, uuid, is_ad, meta_tag, show_flag FROM tasks WHERE (starttime BETWEEN {0} AND {1}) AND (state < 3 OR state = 5) OR (state < 3 OR state = 5) AND (endtime BETWEEN {0} AND {1}) OR (state < 3 OR state = 5) AND ({0} BETWEEN starttime AND endtime) OR (state < 3 OR state = 5) AND ({1} BETWEEN starttime AND endtime)", start_dt, end_dt);

					using (OdbcCommand comm = new OdbcCommand(sSelectQuery, conn))
					{
						using (DataTable dtTasks = new DataTable())
						{
							using (IDataReader dataReader = comm.ExecuteReader())
							{
								dtTasks.Load(dataReader);
								dataReader.Dispose();
							}

							string playerQuery = "select pid , gid from players where del_yn='N'";

							using (OdbcCommand comm2 = new OdbcCommand(playerQuery, conn))
							{

                                using (IDataReader dataReader = comm2.ExecuteReader())
								{
									while (dataReader.Read())
									{
										int nCntTimeSched = 0;
										int nCntRollingSched = 0;

										//  플레이어 ID
										string pid = CommonUnit.GetString(dataReader["pid"]);
										Collection<string> arrGids = new Collection<string>();

										string gid = CommonUnit.GetString(dataReader["gid"]);

										#region 그룹 ID 리스트 작성

										try
										{
											XmlNode node = CacheObject.GetInstance.FindGroupNode(gid);

											XmlNode parent_node = node;
											do
											{
												try
												{
													arrGids.Add(parent_node.Attributes["gid"].Value);
												}
												catch
												{
												}
											} while ((parent_node = parent_node.ParentNode) != null);
										}
										catch (Exception ex)
										{
											logger.Error(ex.ToString());
										}
										#endregion

										/// 스케줄 검증
										foreach (DataRow task in dtTasks.Rows)
										{
											int type = Convert.ToInt32(task["type"]);
											int state = Convert.ToInt32(task["state"]);

											String desc_pid = task["pid"] as String;
											String desc_gid = task["gid"] as String;

											/// 반복 스케줄 또는 시간 스케줄이 아닌 경우
											if (type != 3 && type != 10) continue;
											/// 스케줄이 삭제된 경우
											if (state == 3) continue;

											/// 해당 스케줄이 그룹 ID에 포함되거나 플레이어 ID에 포함되는 경우
											if ((arrGids.Contains(desc_gid) && String.IsNullOrEmpty(desc_pid)) || desc_pid == pid)
											{
												int starttimeofday = Convert.ToInt32(task["starttimeofday"]);
												int endtimeofday = Convert.ToInt32(task["endtimeofday"]);
												int daysofweek = Convert.ToInt32(task["daysofweek"]);

												if (CheckContainTimeofDay(dtlocalStartTime, starttimeofday, endtimeofday)
													&& CheckDayOfWeek(dtlocalStartTime, daysofweek))
												{
													if (type == 3) nCntTimeSched++;
													else nCntRollingSched++;
												}
											}
										}

										try
										{
											sInsertQuery = String.Format("insert into report_time_schedule(YYYY,MM,DD,H24,pid,type,cnt) values('{0}','{1}','{2}','{3}','{4}',3,{5})"
												, YYYY
                                                , MM >= 10 ? MM.ToString() : "0" + MM.ToString()
                                                , DD >= 10 ? DD.ToString() : "0" + DD.ToString()
                                                , HH >= 10 ? HH.ToString() : "0" + HH.ToString()
												, pid
												, nCntTimeSched);
											//	시간 스케줄 삽입 
											using (OdbcCommand comm3 = new OdbcCommand(sInsertQuery, conn))
											{
												comm3.ExecuteNonQuery();
											}

											sInsertQuery = String.Format("insert into report_time_schedule(YYYY,MM,DD,H24,pid,type,cnt) values('{0}','{1}','{2}','{3}','{4}',10,{5})"
												, YYYY
                                                , MM >= 10 ? MM.ToString() : "0" + MM.ToString()
                                                , DD >= 10 ? DD.ToString() : "0" + DD.ToString()
                                                , HH >= 10 ? HH.ToString() : "0" + HH.ToString()
												, pid
												, nCntRollingSched);
											//	반복 스케줄 삽입 
											using (OdbcCommand comm3 = new OdbcCommand(sInsertQuery, conn))
											{
												comm3.ExecuteNonQuery();
											}
										}
										catch (Exception exInsert)
										{
											logger.Error(exInsert.Message + "Insert Query: " + sInsertQuery);
										}
									}
								}
							}
						}
						comm.Dispose();
					}
					bRet = true;
				}
			}
			catch (Exception exSelect)
			{
				logger.Error(exSelect.Message + "Select Query: " + sSelectQuery);
			} 

			logger.Info("Ending to make a schedule report statistics: Y:{0} M:{1} D:{2} H:{3}", YYYY, MM, DD, HH);

			return bRet;
		}
		#endregion

		/// <summary>
		/// 요일 체크
		/// </summary>
		/// <param name="dtLocal"></param>
		/// <param name="dayofweek"></param>
		/// <returns></returns>
		private bool CheckDayOfWeek(DateTime dtLocal, int dayofweek)
		{
			DayOfWeek dow = dtLocal.DayOfWeek;
			if (dow == DayOfWeek.Monday)
				return 0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_MON);
			else if (dow == DayOfWeek.Tuesday)
				return 0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_TUE);
			else if (dow == DayOfWeek.Wednesday)
				return 0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_WED);
			else if (dow == DayOfWeek.Thursday)
				return 0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_THU);
			else if (dow == DayOfWeek.Friday)
				return 0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_FRI);
			else if (dow == DayOfWeek.Saturday)
				return 0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_SAT);
			else if (dow == DayOfWeek.Sunday)
				return 0 < (dayofweek & (int)TaskApplyDayOfWeek.Day_SUN);
			else
			{
				logger.Error("Exception : invalid DayOfWeek. : " + dayofweek.ToString());
			}
			return false;
		}

		/// <summary>
		/// dtLocal 시간이 TimeOfDay에 포함되는지
		/// </summary>
		/// <param name="dtLocal"></param>
		/// <param name="nStartTime"></param>
		/// <param name="nEndTime"></param>
		/// <returns></returns>
		private bool CheckContainTimeofDay(DateTime dtLocal, int nStartTime, int nEndTime)
		{
			//	둘다 같은 값이라면 Whole Day로 간주
			if (nStartTime == nEndTime) return true;

			int nCurr = dtLocal.Second + (dtLocal.Minute * 60) + (dtLocal.Hour * 3600);

			if (nStartTime < nEndTime)
			{
				//	시간대가 하루에 국한되는 경우

				return nCurr >= nStartTime && nCurr < nEndTime;
			}
			else
			{
				//	시간대가 오늘부터 다음 날로 넘어 간 경우
				return nCurr < nStartTime || nCurr >= nEndTime;
			}
		}

		/// <summary>
		///	스케줄을 적용할 요일 정보
		/// </summary>
		private enum TaskApplyDayOfWeek
		{
			Day_MON = 1,
			Day_TUE = 2,
			Day_WED = 4,
			Day_THU = 8,
			Day_FRI = 16,
			Day_SAT = 32,
			Day_SUN = 64
		}
	}
}
