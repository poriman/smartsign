﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NLog;
using System.Data.Odbc;
using DigitalSignage.Common;
using System.Data;

namespace DigitalSignage.SchedulerService.Statistics
{
	/// <summary>
	/// 재생 통계 리포트 클래스
	/// </summary>
	public class PlayReport : IReport
	{
		private static Logger logger = LogManager.GetCurrentClassLogger();

		#region IReport Interface Implementation
		/// <summary>
		/// 일별 리포트 만들기
		/// </summary>
		/// <param name="YYYY"></param>
		/// <param name="MM"></param>
		/// <param name="DD"></param>
		/// <returns></returns>
		public bool MakeDailyReport(int YYYY, int MM, int DD)
		{
			logger.Info("Starting to make a play report statistics: Y:{0} M:{1} D:{2}", YYYY, MM, DD);

			bool bRet = false;
			Config cfg = Config.GetConfig;

			String sSelectQuery = String.Empty;
			String sInsertQuery = String.Empty;
			try
			{
				using (OdbcConnection conn = cfg.serverDBLayer.GetAvailableLogConnection())
				{
					for (int i = 0; i < 24; ++i)
					{

						long _st_tm = TimeConverter.ConvertToUTP(new DateTime(YYYY, MM, DD, i, 0, 0).ToUniversalTime());
						long _ed_tm = TimeConverter.ConvertToUTP(new DateTime(YYYY, MM, DD, i, 59, 59).ToUniversalTime());

						sSelectQuery = String.Format("SELECT screen_id,count(screen_id) as CNT FROM playlogs where start_dt > {0} and start_dt <= {1} and screen_id is not null group by screen_id", _st_tm, _ed_tm);

						using (OdbcCommand comm = new OdbcCommand(sSelectQuery, conn))
						{
							using (IDataReader dataReader = comm.ExecuteReader())
							{

								while (dataReader.Read())
								{
									long s_id = CommonUnit.GetInt64(dataReader["screen_id"]);
									//string uuid = CommonUnit.GetString(dataReader["uuid"]);
									int cnt = CommonUnit.GetInt(dataReader["CNT"]);

									try
									{

										sInsertQuery = String.Format("INSERT INTO report_play(YYYY,MM,DD,H24,CNT,s_id) VALUES ('{0}','{1}','{2}','{3}',{4},{5})"
											, YYYY
											, MM >= 10 ? MM.ToString() : "0" + MM.ToString()
                                            , DD >= 10 ? DD.ToString() : "0" + DD.ToString()
                                            , i >= 10 ? i.ToString() : "0" + i.ToString()
											, cnt
											, s_id);

										using (OdbcCommand comm2 = new OdbcCommand(sInsertQuery, conn))
										{
											comm2.ExecuteNonQuery();
										}
									}
									catch (Exception exInsert)
									{
										logger.Error(exInsert.Message + "Insert Query: " + sInsertQuery);
									}
								}
							}
							comm.Dispose();
						}
					}
					bRet = true;
				}
			}
			catch (Exception exSelect)
			{
				logger.Error(exSelect.Message + "Select Query: " + sSelectQuery);
			}

			logger.Info("Ending to make a play report statistics: Y:{0} M:{1} D:{2}", YYYY, MM, DD);

			return bRet;
		}
		/// <summary>
		/// 시간별 리포트 만들기
		/// </summary>
		/// <param name="YYYY"></param>
		/// <param name="MM"></param>
		/// <param name="DD"></param>
		/// <param name="HH"></param>
		/// <returns></returns>
		public bool MakeHourlyReport(int YYYY, int MM, int DD, int HH)
		{
			logger.Info("Starting to make a play report statistics: Y:{0} M:{1} D:{2} H:{3}", YYYY, MM, DD, HH);

			bool bRet = false;
			Config cfg = Config.GetConfig;

			String sSelectQuery = String.Empty;
			String sInsertQuery = String.Empty;
			try
			{
				using (OdbcConnection conn = cfg.serverDBLayer.GetAvailableLogConnection())
				{
					long _st_tm = TimeConverter.ConvertToUTP(new DateTime(YYYY, MM, DD, HH, 0, 0).ToUniversalTime());
					long _ed_tm = TimeConverter.ConvertToUTP(new DateTime(YYYY, MM, DD, HH, 59, 59).ToUniversalTime());

					sSelectQuery = String.Format("SELECT screen_id,count(screen_id) as CNT FROM playlogs where start_dt > {0} and start_dt <= {1} and screen_id is not null group by screen_id", _st_tm, _ed_tm);

					using (OdbcCommand comm = new OdbcCommand(sSelectQuery, conn))
					{
						using (IDataReader dataReader = comm.ExecuteReader())
						{

							while (dataReader.Read())
							{
								long s_id = CommonUnit.GetInt64(dataReader["screen_id"]);
								//string uuid = CommonUnit.GetString(dataReader["uuid"]);
								int cnt = CommonUnit.GetInt(dataReader["CNT"]);

								try
								{

									sInsertQuery = String.Format("INSERT INTO report_play(YYYY,MM,DD,H24,CNT,s_id) VALUES ('{0}','{1}','{2}','{3}',{4},{5})"
										, YYYY
                                        , MM >= 10 ? MM.ToString() : "0" + MM.ToString()
                                        , DD >= 10 ? DD.ToString() : "0" + DD.ToString()
                                        , HH >= 10 ? HH.ToString() : "0" + HH.ToString()
										, cnt
										, s_id);

									using (OdbcCommand comm2 = new OdbcCommand(sInsertQuery, conn))
									{
										comm2.ExecuteNonQuery();
									}
								}
								catch (Exception exInsert)
								{
									logger.Error(exInsert.Message + "Insert Query: " + sInsertQuery);
								}
							}
						}
						comm.Dispose();
					}
					bRet = true;
				}
			}
			catch (Exception exSelect)
			{
				logger.Error(exSelect.Message + "Select Query: " + sSelectQuery);
			}

			logger.Info("Ending to make a play report statistics: Y:{0} M:{1} D:{2} H:{3}", YYYY, MM, DD, HH);

			return bRet;
		}
		#endregion
	}
}
