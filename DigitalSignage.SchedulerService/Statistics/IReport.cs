﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DigitalSignage.SchedulerService.Statistics
{
	/// <summary>
	/// 리포트 인터페이스
	/// </summary>
	interface IReport
	{
		/// <summary>
		/// 일별 리포트 만들기
		/// </summary>
		/// <param name="YYYY"></param>
		/// <param name="MM"></param>
		/// <param name="DD"></param>
		/// <returns></returns>
		bool MakeDailyReport(int YYYY, int MM, int DD);
		/// <summary>
		/// 시간별 리포트 만들기
		/// </summary>
		/// <param name="YYYY"></param>
		/// <param name="MM"></param>
		/// <param name="DD"></param>
		/// <param name="HH"></param>
		/// <returns></returns>
		bool MakeHourlyReport(int YYYY, int MM, int DD, int HH);
	}
}
