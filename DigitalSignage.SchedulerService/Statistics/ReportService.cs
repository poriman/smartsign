﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using NLog;
using System.Threading;

namespace DigitalSignage.SchedulerService.Statistics
{
    public class ReportService
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        BackgroundWorker bwReporting = null;

        public bool Start()
        {
            logger.Info("Starting Reporting Service....");
            try
            {
                if (bwReporting == null)
                {
                    bwReporting = new BackgroundWorker();
                    bwReporting.WorkerSupportsCancellation = true;
                    bwReporting.DoWork += new DoWorkEventHandler(bwReporting_DoWork);
                }

                bwReporting.RunWorkerAsync();

                return true;
            }
            catch (Exception ex) { logger.Error(ex.ToString()); }

            return false;
        }

        void bwReporting_DoWork(object sender, DoWorkEventArgs e)
        {
            bool hasHourly = ServerSettings.IsHourlyReportEnabled;
            bool hasDaily = ServerSettings.IsDailyReportEnabled;

            logger.Info("Reporting Target: Hourly Enabled: {0}, Daily Enabled: {1}", hasHourly, hasDaily);

            List<IReport> dailyReports = new List<IReport>();
            List<IReport> hourlyReports = new List<IReport>();

            dailyReports.Add(new PlayReport());
            dailyReports.Add(new RelayReport());
            dailyReports.Add(new RunningReport());

            hourlyReports.Add(new ScheduleReport());
            hourlyReports.Add(new PlayerSummary());

            DateTime prevTime = DateTime.Now;

            Thread.Sleep(10000);
            while (!bwReporting.CancellationPending)
            {
                DateTime now = DateTime.Now;

                if ((now - prevTime).TotalSeconds < 1)
                {
                    logger.Info("Pass Reporting");
                    Thread.Sleep(1000);
                    continue;
                }


                if (hasHourly && now.Minute == 0 && now.Second == 0)
                {
                    foreach (IReport rpt in hourlyReports)
                    {
                        try
                        {
                            DateTime dt = now - TimeSpan.FromHours(1);

                            rpt.MakeHourlyReport(dt.Year, dt.Month, dt.Day, dt.Hour);
                        }
                        catch (Exception ex)
                        {
                            logger.Error(ex.Message);
                        }
                    }

                    logger.Info("Reporting is Done.");

                    prevTime = now;

                }

                if (hasDaily && now.Hour == 0 && now.Minute == 0 && now.Second == 0)
                {
                    foreach (IReport rpt in dailyReports)
                    {
                        try
                        {
                            DateTime dt = now - TimeSpan.FromDays(1);
                            rpt.MakeDailyReport(dt.Year, dt.Month, dt.Day);
                        }
                        catch (Exception ex)
                        {
                            logger.Error(ex.Message);
                        }

                    }

                    logger.Info("Reporting is Done.");

                    prevTime = now;

                }

                Thread.Sleep(900);

            }
        }

        public bool Stop()
        {
            logger.Info("Stopped Reporting Service....");
            if (bwReporting != null)
            {
                try
                {
                    bwReporting.CancelAsync();

                    bwReporting.Dispose();
                    bwReporting = null;

                    return true;
                }
                catch (Exception ex) { logger.Error(ex.ToString()); }

                return false;
            }

            return true;
        }
    }
}
