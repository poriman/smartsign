﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NLog;

namespace DigitalSignage.SchedulerService.Statistics
{
	public class CommonUnit
	{
		public CommonUnit()
		{
		}

		private static Logger logger = LogManager.GetCurrentClassLogger();

		public static string GetString(object o)
		{
			try
			{
				if (o != System.DBNull.Value)
				{
					return (string)o;
				}
				else
				{
					return "";
				}
			}
			catch
			{
				return "";
			}
		}

		public static int GetInt(object o)
		{
			try
			{
				if (o != System.DBNull.Value)
				{
					return Convert.ToInt32(o);
				}
				else
				{
					return 0;
				}
			}
			catch (Exception e)
			{
				logger.Error(e.Message);

				return 0;
			}
		}

		public static Int64 GetInt64(object o)
		{
			try
			{
				if (o != System.DBNull.Value)
				{
					return Convert.ToInt64(o);
				}
				else
				{
					return 0;
				}
			}
			catch
			{
				return 0;
			}
		}
	}
}
