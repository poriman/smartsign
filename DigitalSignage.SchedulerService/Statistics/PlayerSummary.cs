﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NLog;
using System.Data.Odbc;
using DigitalSignage.Common;
using System.Data;
using System.Xml;

namespace DigitalSignage.SchedulerService.Statistics
{
    public class PlayerSummary : IReport
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        #region IReport Interface Implementation
        /// <summary>
        /// 일별 리포트 만들기
        /// </summary>
        /// <param name="YYYY"></param>
        /// <param name="MM"></param>
        /// <param name="DD"></param>
        /// <returns></returns>
        public bool MakeDailyReport(int YYYY, int MM, int DD)
        {
            logger.Info("Starting to make a player summary snapshot: Y:{0} M:{1} D:{2}", YYYY, MM, DD);

            bool bRet = false;
            Config cfg = Config.GetConfig;

            String sInsertQuery = String.Empty;

            try
            {
                using (OdbcConnection conn = cfg.serverDBLayer.GetAvailableLogConnection())
                {
                    XmlNode root = CacheObject.GetInstance.FindGroupNode("0000000000000");

                    if (root != null)
                    {
                        XmlNodeList listPlayers = root.SelectNodes("descendant::player[attribute::del_yn=\"N\"]");

                        foreach (XmlNode player in listPlayers)
                        {
                            try
                            {
                                sInsertQuery = String.Format("INSERT INTO summary_players (pid, YYYY, MM, DD, H24, gid, name, host, state, uptime, lastconndt, versionH, versionL, freespace, memoryusage, cpurate, volume, serial_power, serial_source, serial_volume) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', {8}, {9}, {10}, {11}, {12}, {13}, {14}, {15}, '{16}', '{17}', {18})",
                                    player.Attributes["pid"].Value,
                                    YYYY,
                                    MM >= 10 ? MM.ToString() : "0" + MM.ToString(),
                                    DD >= 10 ? DD.ToString() : "0" + DD.ToString(),
                                    "00",
                                    player.Attributes["gid"].Value,
                                    player.Attributes["name"].Value,
                                    player.Attributes["host"].Value,
                                    player.Attributes["state"].Value,
                                    player.Attributes["lastconndt"].Value,
                                    player.Attributes["versionH"].Value,
                                    player.Attributes["versionL"].Value,
                                    player.Attributes["freespace"].Value,
                                    player.Attributes["memoryusage"].Value,
                                    player.Attributes["cpurate"].Value,
                                    player.Attributes["curr_volume"].Value,
                                    player.Attributes["_serial_stat_power"].Value,
                                    player.Attributes["_serial_stat_source"].Value,
                                    player.Attributes["_serial_stat_volume"].Value);

                                using (OdbcCommand comm = new OdbcCommand(sInsertQuery, conn))
                                {
                                    comm.ExecuteNonQuery();
                                }
                            }
                            catch (Exception exInsert)
                            {
                                logger.Error(exInsert.Message + "Insert Query: " + sInsertQuery);
                            }
                        }

                    }

                    bRet = true;
                }
            }
            catch (Exception exSelect)
            {
                logger.Error(exSelect.Message );
            }

            logger.Info("Ending to make a player summary snapshot: Y:{0} M:{1} D:{2}", YYYY, MM, DD);

            return bRet;
        }
        /// <summary>
        /// 시간별 리포트 만들기
        /// </summary>
        /// <param name="YYYY"></param>
        /// <param name="MM"></param>
        /// <param name="DD"></param>
        /// <param name="HH"></param>
        /// <returns></returns>
        public bool MakeHourlyReport(int YYYY, int MM, int DD, int HH)
        {
            logger.Info("Starting to make a player summary snapshot: Y:{0} M:{1} D:{2} H:{3}", YYYY, MM, DD, HH);

            bool bRet = false;
            Config cfg = Config.GetConfig;

            //  시간 보정
            HH = HH + 1;

            String sInsertQuery = String.Empty;
            try
            {
                using (OdbcConnection conn = cfg.serverDBLayer.GetAvailableLogConnection())
                {
                    XmlNode root = CacheObject.GetInstance.FindGroupNode("0000000000000");

                    if (root != null)
                    {
                        XmlNodeList listPlayers = root.SelectNodes("descendant::player[attribute::del_yn=\"N\"]");

                        foreach (XmlNode player in listPlayers)
                        {
                            try
                            {
                                int nState = 0;
                                int nUptime = 0;
                                long lLastConndt = -1;
                                int nVerH = 0;
                                int nVerL = 0;
                                int nFreespace = -1;
                                int nMemoryUsage = -1;
                                int nCpurate = -1;
                                int nCurrVolume = -1;
                                int nSerialVolume = -1;

                                try { nState = Convert.ToInt32(player.Attributes["state"].Value); } catch {}
                                try { nUptime = Convert.ToInt32(player.Attributes["uptime"].Value); } catch {}
                                try { lLastConndt = Convert.ToInt64(player.Attributes["lastconndt"].Value); } catch {}
                                try { nVerH = Convert.ToInt32(player.Attributes["versionH"].Value); } catch {}
                                try { nVerL = Convert.ToInt32(player.Attributes["versionL"].Value); } catch {}
                                try { nFreespace = Convert.ToInt32(player.Attributes["freespace"].Value); } catch {}
                                try { nMemoryUsage = Convert.ToInt32(player.Attributes["memoryusage"].Value); } catch {}
                                try { nCpurate = Convert.ToInt32(player.Attributes["cpurate"].Value); } catch {}
                                try { nCurrVolume = Convert.ToInt32(player.Attributes["curr_volume"].Value); } catch {}
                                try { nSerialVolume = Convert.ToInt32(player.Attributes["_serial_stat_volume"].Value); } catch {}

                                sInsertQuery = String.Format("INSERT INTO summary_players (pid, YYYY, MM, DD, H24, gid, name, host, state, uptime, lastconndt, versionH, versionL, freespace, memoryusage, cpurate, volume, serial_power, serial_source, serial_volume) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', {8}, {9}, {10}, {11}, {12}, {13}, {14}, {15}, {16}, '{17}', '{18}', {19})",
                                    player.Attributes["pid"].Value,
                                    YYYY,
                                    MM >= 10 ? MM.ToString() : "0" + MM.ToString(),
                                    DD >= 10 ? DD.ToString() : "0" + DD.ToString(),
                                    HH >= 10 ? HH.ToString() : "0" + HH.ToString(),
                                    player.Attributes["gid"].Value,
                                    player.Attributes["name"].Value,
                                    player.Attributes["host"].Value,
                                    nState,
                                    nUptime,
                                    lLastConndt,
                                    nVerH,
                                    nVerL,
                                    nFreespace,
                                    nMemoryUsage,
                                    nCpurate,
                                    nCurrVolume,
                                    player.Attributes["_serial_stat_power"].Value,
                                    player.Attributes["_serial_stat_source"].Value,
                                    nSerialVolume);

                                using (OdbcCommand comm = new OdbcCommand(sInsertQuery, conn))
                                {
                                    comm.ExecuteNonQuery();
                                }
                            }
                            catch (Exception exInsert)
                            {
                                logger.Error(exInsert.Message + "Insert Query: " + sInsertQuery);
                            }
                        }

                    }

                    bRet = true;
                }
            }
            catch (Exception exSelect)
            {
                logger.Error(exSelect.Message);
            }

            logger.Info("Ending to make a player summary snapshot: Y:{0} M:{1} D:{2} H:{3}", YYYY, MM, DD, HH);

            return bRet;
        }
        #endregion
    }
}
