﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;

using DigitalSignage.Common;
using DigitalSignage.DataBase;
using NLog;
//SQLite & DB routines
using System.Data.Common;
using System.Data.SQLite;
using System.IO;

namespace DigitalSignage.SchedulerService
{
    public class LogsList : MarshalByRefObject, ILogList
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

		public int DeleteLogsBefore(long dt)
		{
			try
			{
				Config cfg = Config.GetConfig;

				using (DataBase.LogsDataSetTableAdapters.logsTableAdapter da = cfg.logdbLayer.CreateLogDA())
				{
					int nRet = da.DeleteLogsBefore(dt);
					da.Dispose();
					return nRet;
				}
			}
			catch (Exception e)
			{
				logger.Error(e.Message);
			}

			return -1;
		}

		public LogsDataSet.logsDataTable GetLogsFromTo(string pid, int type, int category, long from, long to)
		{
			try
			{
				Config cfg = Config.GetConfig;

				using (DataBase.LogsDataSetTableAdapters.logsTableAdapter da = cfg.logdbLayer.CreateLogDA())
				{

					LogsDataSet.logsDataTable data = null;

					data = da.GetLogsFromTo(type, category, pid, from, to);
					da.Dispose();

					try
					{
						if(cfg.IsFileDB)
						{
							string archiveFolder = cfg.AppData + "ArchiveLog\\";

							string[] files = Directory.GetFiles(archiveFolder);

							foreach(string file in files)
							{

								FileInfo fi = new FileInfo(file);

								string onlyName = fi.Name;
								long updateTicks = 0;

								try
								{
									updateTicks = (long)Convert.ToInt64(onlyName);
								}
								catch
								{
									continue;
								}

								if (from > updateTicks)
								{
									continue;
								}							
		 
								//	파일이름이 업데이트된 날짜이기때문에 
								//	시작날짜가 업데이트 날짜보다 크다면 볼필요없는 로그파일이다.

								logger.Info("searching '" + file + "' file.");

								LogDBLayer archived = null;
								
								try
								{
									archived = new LogDBLayer();
		// 							archived.openDB(archiveFolder, onlyName);
									archived.openDB(String.Format("Driver=SQLite3 ODBC Driver;Database={0}{1}", archiveFolder, onlyName));

									using (DataBase.LogsDataSetTableAdapters.logsTableAdapter dta = archived.CreateLogDA())
									{
										data.Merge(dta.GetLogsFromTo(type, category, pid, from, to));
										dta.Dispose();
									}

									archived.closeDB();
									archived = null;
								}
								catch
								{
									if(archived != null)
									{
										archived.closeDB();
										archived = null;
									}
									logger.Error("'" + onlyName + "' access error.");
								}
							}
						}
					}
					catch (Exception ex)
					{
						logger.Error(ex.Message);
					}
					return data;
				}
			}
			catch (Exception e)
			{
				logger.Error(e.Message);
			}

			return null;
		}
		public LogsDataSet.logsDataTable GetAllLogs(string pid, int type, int category)
		{
			try
			{
				Config cfg = Config.GetConfig;
				using (DataBase.LogsDataSetTableAdapters.logsTableAdapter da = cfg.logdbLayer.CreateLogDA())
				{
					using (LogsDataSet.logsDataTable table = da.GetLogsByMasterInfo(type, category, pid))
					{
						da.Dispose();
						return table;
					}
				}
			}
			catch(Exception e)
			{
				logger.Error(e.Message);
			}

			return null;
		}
		private long GetMasterID(string pid, int type, int category)
		{
			try
			{
				Config cfg = Config.GetConfig;
				long id = 0;

				using(DataBase.LogsDataSetTableAdapters.logmasterTableAdapter da = cfg.logdbLayer.CreateLogMasterDA())
				{
					using (LogsDataSet.logmasterDataTable dt = da.GetLogMasterData(pid, type, category))
					{
						id = ((LogsDataSet.logmasterRow)dt.Rows[0]).id;
						dt.Dispose();
					}
					da.Dispose();
				}
				return id;
			}
			catch(Exception e)
			{
				logger.Error(e.Message);
			}

			return -1;
		}
		private long InsertMasterInfo(string pid, int type, int category)
		{
			try
			{
				Config cfg = Config.GetConfig;
				using (DataBase.LogsDataSetTableAdapters.logmasterTableAdapter da = cfg.logdbLayer.CreateLogMasterDA())
				{
					da.InsertLogMasterData(category, 0, type, pid);
					da.Dispose();
				}
				return GetMasterID(pid, type, category);
			}
			catch(Exception e)
			{
				logger.Error(e.Message);
			}

			return -1;
		}
		private int UpdateRegDate(long masterID, long regDate)
		{
			try
			{
				Config cfg = Config.GetConfig;

				int nRet = 0;
				using (DataBase.LogsDataSetTableAdapters.logmasterTableAdapter da = cfg.logdbLayer.CreateLogMasterDA())
				{
					nRet = da.UpdateLastUpdateDatetime(regDate, masterID);
					da.Dispose();
				}
				return nRet;

			}
			catch (Exception e)
			{
				logger.Error(e.Message);
			}

			return -1;

		}
		public int InsertLogs(string pid, int type, int category, string uuid, string name, string description, int code, long regdate)
		{
			try
			{
				Config cfg = Config.GetConfig;

				long lMasterID = -1;
				int nRet = -1;

				if (-1 == (lMasterID = GetMasterID(pid, type, category)))
				{
					lMasterID = InsertMasterInfo(pid, type, category);
				}

				if (lMasterID == -1)
					throw new Exception("Error : MasterID == -1");

				using (DataBase.LogsDataSetTableAdapters.logsTableAdapter da = cfg.logdbLayer.CreateLogDA())
				{
					nRet = da.InsertLog(code, lMasterID, regdate, description, name, uuid);
					da.Dispose();

				}

				UpdateRegDate(lMasterID, regdate);

				return nRet;

			}
			catch (Exception e)
			{
				logger.Error(e.Message);
			}

			return -1;
		}
    }
}
