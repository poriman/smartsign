﻿using System;
using System.Collections;

//SQLite & DB routines
using DigitalSignage.DataBase;
using DigitalSignage.Common;

using NLog;

namespace DigitalSignage.SchedulerService
{
    // класс для работы с очередью заданий
    public class TaskList : MarshalByRefObject, ITaskList
    {
        private Config cfg = null;
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public TaskList()
        {
            cfg = Config.GetConfig; //protection from initialization dead loop
        }

		//	TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime())에 +2를 하는 이유는 실제 지금부터 DB에 저장되는 시간을 보정하기 위해서이다.


        // getting all tasks which should be executed in a current moment
        public Task[] GetActiveTask()
        {
            Task[] result = null;
            try
            {
                using (DigitalSignage.DataBase.DataSetTableAdapters.tasksTableAdapter da = cfg.dbLayer.CreateTaskDA())
				{
					long tick = TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()) + 2;
					using (DataSet.tasksDataTable table = da.GetData(tick, tick))
					{
						result = new Task[table.Rows.Count];
						for (int i = 0; i < table.Rows.Count; i++)
						{
							result[i] = new Task((DataSet.tasksRow)table.Rows[i]);
							da.ChangeTaskState((int)TaskState.StateProcessed, tick, ((DataSet.tasksRow)table.Rows[i]).uuid);
						}
						table.Dispose();
					}
					da.Dispose();
				}
            }
            catch (Exception e)
            {
                logger.Error(e + "");
            }
            return result;
        }

        // add new task to list
        public int AddTask( AuthTicket at, Task newTask)
        {
            try
            {
                at = UsersList.GetUserLevel(at);
                if ( at.level == UserLevels.UserNone) throw new Exception("Unauthorized");
                using (DigitalSignage.DataBase.DataSetTableAdapters.tasksTableAdapter da = cfg.dbLayer.CreateTaskDA())
				{
					da.AddTask(newTask.pid, newTask.gid, (int)newTask.type, newTask.uuid.ToString(),
						TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()) + 2, newTask.TaskStart, newTask.TaskEnd, newTask.guuid.ToString(),
						newTask.duuid.ToString(), newTask.description, (int)newTask.duration, newTask.daysofweek, newTask.priority, newTask.endtimeofday, newTask.starttimeofday);
					da.Dispose();
				}
				return 0;
            }
            catch (Exception e)
            {
                logger.Error(e + "");
            }
            return -1;
        }

        // method for getting a tasks changed since "timestamp"
		public DataSet.tasksDataTable GetNewTasks(long timestamp, string pid, string gid)
        {
            try
            {
				DateTime dt = DateTime.Now;

				using (DigitalSignage.DataBase.DataSetTableAdapters.tasksTableAdapter da = cfg.dbLayer.CreateTaskDA())
				{
					using (DataSet.tasksDataTable res = da.GetNewTasks(timestamp, pid, timestamp, gid))
					{
						da.Dispose();

						logger.Info("Founded " + res.Rows.Count + " new tasks (from=" + timestamp + ";target=pid:" + pid + " gid:" + gid + ") - takes " + (dt - DateTime.Now).TotalSeconds.ToString());
						
						return res;
					}
				}
            }
            catch (Exception e)
            {
                logger.Error(e + "");
            }
            return null;
        }

        public int DeleteTask( AuthTicket at, Task task)
        {
            try
            {
				int res = 0;
                at = UsersList.GetUserLevel(at);
                if ( at.level == UserLevels.UserNone ) throw new Exception("Unauthorized");
                using (DigitalSignage.DataBase.DataSetTableAdapters.tasksTableAdapter da = cfg.dbLayer.CreateTaskDA())
				{
					logger.Info("Trying to delete tasks group = " + task.guuid);
					res = da.RemoveTask(TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()) + 2, task.guuid.ToString());
					Nullable<int> nCount = da.GetCountOfAliveTasksInGroup(task.duuid.ToString(), task.gid);
					da.Dispose();

					if(nCount.HasValue && nCount.Value == 0)
					{
						using (DigitalSignage.DataBase.DataSetTableAdapters.downloadmasterTableAdapter dta = cfg.dbLayer.CreateDownloadMasterDA())
						{
							logger.Info("Trying to delete downloadmaster by duuid = " + task.duuid);
							dta.DeleteByDuuid(task.duuid.ToString(), task.gid);
							dta.Dispose();
						}
					}
				}
				return res;
            }
            catch (Exception e)
            {
                logger.Error(e + "");
            }
            return -1;
        }

        public int UpdateTask( AuthTicket at, Task task)
        {
            try
            {
				at = UsersList.GetUserLevel(at);
				if (at.level == UserLevels.UserNone) throw new Exception("Unauthorized");

				using (DigitalSignage.DataBase.DataSetTableAdapters.tasksTableAdapter da = cfg.dbLayer.CreateTaskDA())
				{
					int res = da.UpdateTask(task.pid, task.gid, task.TaskStart, task.TaskEnd, TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()) + 2, (int)task.type, task.description,
						task.duuid.ToString(), (int)task.duration, task.daysofweek, task.priority, task.endtimeofday, task.starttimeofday, (int)task.state, task.uuid.ToString());
					da.Dispose();
					return res;
				}
            }
            catch (Exception e)
            {
                logger.Error(e + "");
            }
            return -1;
        }

        public DataSet.tasksDataTable GetActiveTasks(long timestamp)
        {
            try
            {
				using (DigitalSignage.DataBase.DataSetTableAdapters.tasksTableAdapter da = cfg.dbLayer.CreateTaskDA())
				{
					using (DataSet.tasksDataTable res = da.GetActiveTasks(timestamp))
					{
						da.Dispose();

						return res;
					}
				}
			}
            catch (Exception e)
            {
                logger.Error(e + "");
            }
            return null;
        }

        public int AddTasks( AuthTicket at, DataSet.tasksDataTable newData)
        {
            Config cfg = Config.GetConfig;
            try
            {
                at = UsersList.GetUserLevel(at);
                if (at.level == UserLevels.UserNone) throw new Exception("Unauthorized");
				
				int res = -1;
				using (DataBase.DataSetTableAdapters.tasksTableAdapter da = cfg.dbLayer.CreateTaskDA())
				{
					for (int i = 0; i < newData.Rows.Count; i++)
					{
						DataSet.tasksRow row = (DataSet.tasksRow)newData.Rows[i];
						res = da.AddTask(row.pid, row.gid, (int)row.type, row.uuid.ToString(),
							TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()) + 2, row.starttime, row.endtime,
							row.guuid.ToString(), row.duuid.ToString(), row.name, row.duration, row.daysofweek, row.priority, row.endtimeofday, row.starttimeofday);
					}
					da.Dispose();
				}
				return res;
            }
            catch (Exception e)
            {
                logger.Log(LogLevel.Error, e + "");
            }
            return -1;
        }

        public DataSet.tasksDataTable GetTasksFrom(long from, long till)
        {
            try
            {
				DateTime dt = DateTime.Now;

                using (DigitalSignage.DataBase.DataSetTableAdapters.tasksTableAdapter da = cfg.dbLayer.CreateTaskDA())
				{
					using(DataSet.tasksDataTable res = da.GetTasksFrom(from, till, from, till, from, till))
					{
						da.Dispose();

						logger.Info((dt - DateTime.Now).TotalSeconds.ToString() + " - by i-Vision Manager.");
						return res;
					}
				}
			}
            catch (Exception e)
            {
                logger.Error(e + "");
            }
            return null;
        }

        public DataSet.tasksDataTable GetTasksGroup(String guuid)
        {
            try
            {
				using (DigitalSignage.DataBase.DataSetTableAdapters.tasksTableAdapter da = cfg.dbLayer.CreateTaskDA())
				{
					using (DataSet.tasksDataTable res = da.GetTasksGroup(guuid))
					{
						da.Dispose();

						return res;
					}
				}
			}
            catch (Exception e)
            {
                logger.Error(e + "");
            }
            return null;
        }

        public int EditTasks( AuthTicket at, Task oldTask, Task newTask)
        {
            int res = -1;
            try
            {
                at = UsersList.GetUserLevel(at);
                if (at.level == UserLevels.UserNone) throw new Exception("Unauthorized");
				using (DigitalSignage.DataBase.DataSetTableAdapters.tasksTableAdapter da = cfg.dbLayer.CreateTaskDA())
				{
					res = da.EditTasks((int)newTask.type, (int)newTask.state, newTask.TaskStart, newTask.TaskEnd, TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()) + 2,
					   newTask.description, newTask.duuid.ToString(), (int)newTask.duration, newTask.daysofweek, newTask.priority, newTask.endtimeofday, newTask.starttimeofday, oldTask.guuid.ToString(), oldTask.duuid.ToString(), oldTask.TaskStart, oldTask.TaskEnd);
					da.Dispose();
				}
			}
            catch (Exception e)
            {
                logger.Error(e + "");
            }
            return res;
        }

		public bool CanDeleteByDuuid(String duuid, string ftpAddress, string ftpPath)
		{
			bool bRet = false;
			try
			{
				using (DigitalSignage.DataBase.DataSetTableAdapters.tasksTableAdapter da = cfg.dbLayer.CreateTaskDA())
				{
					using (DataSet.tasksDataTable dt = da.GetAliveTasksByDuuid(ftpPath, ftpAddress, duuid))
					{
						da.Dispose();
						bRet = (dt != null && dt.Count > 1) ? false : true;
						dt.Dispose();
					}
				}

			}
			catch (Exception e)
			{
				logger.Error(e + "");
			}
			return bRet;
		}

		public bool CanDeleteByDuuid(String duuid, string ftpAddress)
		{
			return CanDeleteByDuuid(duuid, ftpAddress, "");
		}

		public bool CanAddTaskToGroup(String uuid, long from, long till, long type, string gid, bool bOnlyGroupTask)
		{
			bool bRet = false;
			try
			{
				using (DigitalSignage.DataBase.DataSetTableAdapters.tasksTableAdapter da = cfg.dbLayer.CreateTaskDA())
				{
					DataSet.tasksDataTable dt = null;

					if (bOnlyGroupTask)
						dt = da.GetTasksBetweenStartEndOnlyGroup(uuid, gid, (int)type, from, uuid, gid, (int)type, till, uuid, gid, (int)type, from, till);
					else
						dt = da.GetTasksBetweenStartEndByGroup(uuid, gid, (int)type, from, uuid, gid, (int)type, till, uuid, gid, (int)type, from, till);

					da.Dispose();
					bRet = (dt != null && dt.Count > 0) ? false : true;

					dt.Dispose();
					dt = null;
				}
			}
			catch (Exception e)
			{
				logger.Error(e + "");
			}
			return bRet;
		}
		public bool CanAddTaskToPlayer(String uuid, long from, long till, long type, string pid, string gid, bool bOnlyPlayerTask)
		{
			bool bRet = false;
			try
			{
				using (DigitalSignage.DataBase.DataSetTableAdapters.tasksTableAdapter da = cfg.dbLayer.CreateTaskDA())
				{
					DataSet.tasksDataTable dt = null;
					if (bOnlyPlayerTask)
						dt = da.GetTasksBetweenStartEndOnlyPlayer(uuid, (int)type, pid, from, uuid, (int)type, pid, till, uuid, (int)type, pid, from, till);
					else
						dt = da.GetTasksBetweenStartEndByPlayer(uuid, (int)type, gid, from, uuid, (int)type, gid, till, uuid, (int)type, pid, from, uuid, (int)type, pid, till, uuid, (int)type, pid, from, till, uuid, (int)type, gid, from, till);

					da.Dispose();
					bRet = (dt != null && dt.Count > 0) ? false : true;

					dt.Dispose();
					dt = null;
				}
			}
			catch (Exception e)
			{
				logger.Error(e + "");
			}
			return bRet;
		}
		public int GetMinPriorityFromDefaultTasks(string gid)
		{
			int nRet = 0;
			try
			{
				using (DigitalSignage.DataBase.DataSetTableAdapters.tasksTableAdapter da = cfg.dbLayer.CreateTaskDA())
				{
					using (DataSet.tasksDataTable dt = da.GetMinPriorityFromDefaultTask(gid))
					{
						da.Dispose();
						nRet = (dt == null) ? 0 : ((DataSet.tasksRow)dt.Rows[0]).priority;

						dt.Dispose();
					}
				}

			}
			catch (Exception e)
			{
				logger.Error(e.Message);
			}

			return nRet;

		}
    }
}