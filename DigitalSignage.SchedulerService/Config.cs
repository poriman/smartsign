﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Reflection;

using NLog;

// using DigitalSignage.DataBase;
using DigitalSignage.Common;

using DigitalSignage.SerialKey;
using System.Net;
using System.ComponentModel;
using System.Data.Odbc;

namespace DigitalSignage.SchedulerService
{
    public class Config
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        // singletone routines
        private static Config instance = null;
		private static readonly object semaphore = new object();

		private ConfigSettings settings = null;
		public DigitalSignage.ServerDatabase.DBLayer serverDBLayer = null;
// 		public DBLayer dbLayer = null;
// 		public LogDBLayer logdbLayer = null;

        public TaskList localTasksList = null;
        public PlayersList localPlayersList = null;
		public LogsList localLogsList = null;

		public NESerialKey._ProductCode product_code = DigitalSignage.SerialKey.SerialKey._ProductCode.Undefined;

        private string appData;

		private bool _hasRefData = false;
		private int _refRefreshTime = 3600;

		#region SerialKey Information
		private string serialkey;
		public string AvailableIP = null;
		public int MaxCountOfPlayers = 30;

		public bool IsTrial = true;


		#region 실시간 플레이어 정보 업데이트 기능.
        public delegate bool SendMessageToPlayer(string pid, string code, string contents);
        public SendMessageToPlayer delSendMessageToPlayerMethod = null;

        public bool SendMessageDelegate(string pid, string code, string contents)
        {
            try
            {
                if (delSendMessageToPlayerMethod != null)
                {
                    return delSendMessageToPlayerMethod(pid, code, contents);
                }
                else
                {
                    logger.Error("'delSendMessageToPlayer' is null");
                }
            }
            catch (Exception e)
            {
                logger.Error(e.ToString());
            }

            return false;
        }
        public delegate void UpdatePlayerConfig(string pid);
        public UpdatePlayerConfig delUpdatePlayerConfigMethod = null;

        public void UpdatePlayerConf(string pid)
        {
            try
            {
                if (delUpdatePlayerConfigMethod != null)
                {
                    delUpdatePlayerConfigMethod(pid);
                }
                else
                {
                    logger.Error("'delUpdatePlayerConfigMethod' is null");
                }
            }
            catch (Exception e)
            {
                logger.Error(e.ToString());
            }
        }

		public delegate void UpdatePlayerID(string gid, string pid, string new_pid);
		public UpdatePlayerID delUpdatePIDMethod = null;

		public void UpdatePID(string gid, string pid, string new_pid)
		{
			try
			{
				if (delUpdatePIDMethod != null)
				{
					delUpdatePIDMethod(gid, pid, new_pid);
				}
				else
				{
					logger.Error("'delUpdatePIDMethod' is null");
				}
			}
			catch (Exception e)
			{
				logger.Error(e.ToString());
			}
		}

		public delegate void UpdateGroupInfoToAllChild(string gid);
		public UpdateGroupInfoToAllChild delUpdateGroupInfoToAllMethod = null;
		
		public void UpdateGroupInfoToAllPlayers(string gid)
		{
			try
			{
				if (delUpdateGroupInfoToAllMethod != null)
				{
					delUpdateGroupInfoToAllMethod(gid);
				}
				else
				{
					logger.Error("'delUpdateGroupInfoToAllMethod' is null");
				}
			}
			catch (Exception e)
			{
				logger.Error(e.ToString());
			}
		}
		public delegate void UpdateGroupInfo(string gid, string pid);
		public UpdateGroupInfo delUpdateGroupInfoMethod = null;

		public void UpdateGroupInfoToOnePlayer(string new_gid, string pid)
		{
			try
			{
				if (delUpdateGroupInfoMethod != null)
				{
					delUpdateGroupInfoMethod(new_gid, pid);
				}
				else
				{
					logger.Error("'delUpdateGroupInfoMethod' is null");
				}
			}
			catch (Exception e)
			{
				logger.Error(e.ToString());
			}
		}

		public delegate void UpdateServiceIP(string gid, string pid, string service_IP);
		public UpdateServiceIP delUpdateServiceIPMethod = null;

		public void UpdateSIP(string gid, string pid, string service_IP)
		{
			try
			{
				if (delUpdateServiceIPMethod != null)
				{
					delUpdateServiceIPMethod(gid, pid, service_IP);
				}
				else
				{
					logger.Error("'delUpdateServiceIPMethod' is null");
				}
			}
			catch (Exception e)
			{
				logger.Error(e.ToString());
			}
		}

		#endregion

		#region 다운로드 시간 대 지정 기능.
		public delegate void UpdateDownloadTime(string gid, string pid);
		public UpdateDownloadTime delUpdateDownloadMethod = null;

		public void UpdateTimeScope(string gid, string pid)
		{
			try
			{
				if(delUpdateDownloadMethod != null)
				{
					delUpdateDownloadMethod(gid, pid);
				}
				else
				{
					logger.Error("'delUpdateDownloadMethod' is null");
				}
			}
			catch (Exception e)
			{
				logger.Error(e.ToString());
			}
		}
		#endregion

       #region 스케줄 갱신 요청 전송
        public delegate void SendUpdateSchedule(string gid, string pid);
        public SendUpdateSchedule delSendUpdateScheduleMethod = null;

        public void SendUpdateScheduleMethod(string gid, string pid)
        {
            try
            {
                if (delSendUpdateScheduleMethod != null)
                {
                    delSendUpdateScheduleMethod(gid, pid);
                }
                else
                {
                    logger.Error("'delSendUpdateScheduleMethod' is null");
                }
            }
            catch (Exception e)
            {
                logger.Error(e.ToString());
            }
        }

        #endregion

        #region IIDB Event Message 전송
        public delegate void SendFile(EventCodes code, string pid, string filepath);
        public SendFile delSendFileMethod = null;

        public void SendDataFile(EventCodes code, string pid, string filepath)
        {
            try
            {
                if (delSendFileMethod != null)
                {
                    delSendFileMethod(code, pid, filepath);
                }
                else
                {
                    logger.Error("'delSendFileMethod' is null");
                }
            }
            catch (Exception e)
            {
                logger.Error(e.ToString());
            }
        }

        public delegate void SendMessage(EventCodes code, string pid, string sMessage);
        public SendMessage delSendEventMethod = null;

        public void SendEventMethod(EventCodes code, string pid, string sMessage)
        {
            try
            {
                if (delSendEventMethod != null)
                {
                    delSendEventMethod(code, pid, sMessage);
                }
                else
                {
                    logger.Error("'delSendEventMethod' is null");
                }
            }
            catch (Exception e)
            {
                logger.Error(e.ToString());
            }
        }
		#endregion

        #region Control Message 전송

        public delegate void SendControlMessage(int nID, ControlRequest code, int control_value, string pid);
        public SendControlMessage delSendControlEventMethod = null;

        public void SendControlEventMethod(int nID, ControlRequest code, int control_value, string pid)
        {
            try
            {
                if (delSendControlEventMethod != null)
                {
                    delSendControlEventMethod(nID, code, control_value, pid);
                }
                else
                {
                    logger.Error("'delSendControlEventMethod' is null");
                    ControlService.GetInstance.SetControlResponse(nID, ControlResponse.NotConnected);
                }
            }
            catch (Exception e)
            {
                logger.Error(e.ToString());
                ControlService.GetInstance.SetControlResponse(nID, ControlResponse.NotConnected);
            }
        }
        #endregion

        #region Start Capture 전송

        public delegate bool CaptureProcess(bool doStart, string pid);
        public CaptureProcess delCaptureProcessEventMethod = null;

        public bool CaptureProcessEventMethod(bool doStart, string pid)
        {
            try
            {
                if (delCaptureProcessEventMethod != null)
                {
                    return delCaptureProcessEventMethod(doStart, pid);
                }
                else
                {
                    logger.Error("'delCaptureProcessEventMethod' is null");
                }
            }
            catch (Exception e)
            {
                logger.Error(e.ToString());
            }

            return false;
        }
        #endregion

		#region Player ID와 Group ID를 인덱스 캐시
// 		public String CachedPlayerID
// 		{
// 			get
// 			{
// 				string addr = "0000000";
// 				try
// 				{
// 					addr = ConfigSettings.ReadSetting("cached_player_id");
// 					if(String.IsNullOrEmpty(addr))
// 					{
// 						ConfigSettings.WriteSetting("cached_player_id", "0000000");
// 						addr = "0000000";
// 					}
// 				}
// 				catch
// 				{
// 					ConfigSettings.WriteSetting("cached_player_id", "0000000");
// 					addr = "0000000";
// 				}
// 				return addr;
// 			}
// 			set
// 			{
// 				try
// 				{
// 					ConfigSettings.WriteSetting("cached_player_id", value);
// 				}
// 				catch
// 				{
// 				}
// 			}
// 		}
// 		public String CachedGroupID
// 		{
// 			get
// 			{
// 				string addr = "0000000000000";
// 				try
// 				{
// 					addr = ConfigSettings.ReadSetting("cached_group_id");
// 					if (String.IsNullOrEmpty(addr))
// 					{
// 						ConfigSettings.WriteSetting("cached_group_id", "0000000000000");
// 						addr = "0000000000000";
// 					}
// 				}
// 				catch
// 				{
// 					ConfigSettings.WriteSetting("cached_group_id", "0000000000000");
// 					addr = "0000000000000";
// 				}
// 				return addr;
// 			}
// 			set
// 			{
// 				try
// 				{
// 					ConfigSettings.WriteSetting("cached_group_id", value);
// 				}
// 				catch
// 				{
// 				}
// 			}
// 		}
		#endregion

		public bool InitializeSerialKey()
		{
			try
			{
				serialkey = ServerSettings.Serial_Key.ToUpper();
				NESerialKey key = new NESerialKey();
				int A = 0, B = 0, C = 0, D = 0;

				bool bRet = key.ParseSerialKey(serialkey, ref product_code, ref A, ref B, ref C, ref D, ref MaxCountOfPlayers);

				if (bRet)
				{
					AvailableIP = String.Format("{0}.{1}.{2}.{3}", A, B, C, D);
					logger.Info("Product Code is " + product_code.ToString());
					logger.Info("Registered IP is " + AvailableIP);

					//	127.0.0.1이면 무조건 패스
					if (AvailableIP.Equals("127.0.0.1"))
					{
						logger.Info("IP has been authenticated.");
						return true;
					}

					//	현재 자신의 IP를 가져온다.
					IPHostEntry host_entry = Dns.GetHostByName(Dns.GetHostName());

					foreach (IPAddress ip in host_entry.AddressList)
					{
						logger.Info(ip.ToString());

						if(AvailableIP.Contains(ip.ToString()))
						{
							logger.Info("IP has been authenticated.");

							return true;
						}
					}

					logger.Error("This system has not valid IP.");

					return false;
				}

				return bRet;
			}
			catch
			{
				return false;
			}
		}

		public bool InitializeTrialChecker()
		{
			bool bRet = false;
			TrialChecker checker = new TrialChecker();
			bRet = checker.IsValid;

			try
			{
				IsTrial = checker.IsTrial;
			}
			catch {}

			return bRet;

		}

		#endregion

		public Config()
        {

			String sExecutingPath = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
			appData = sExecutingPath + "\\ServiceData\\";
			settings = new ConfigSettings(appData + "config.xml");

			//for tests in user mode
            //appData = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\DigitalSignage\\Service\\";

		}

		/// <summary>
		/// 커넥션 풀을 새로 구성한다.
		/// </summary>
		public bool ReInitializeConnectionPool()
		{
			try
			{
				//logger.Info("Connection will be re-connecting.");
                
                //serverDBLayer.Reconnect();
 
                return true;
			}
			catch (Exception e)
			{
				logger.Error(e.ToString());
			}

			return false;

			
		}

		public void init()
        {
            try
            {
				String sConnection = ServerSettings.Connection_String;
				if (sConnection.ToLower().Contains("sqlite3 odbc driver"))
				{
					sConnection = String.Format("Driver=SQLite3 ODBC Driver;Database={0}playlist.db3", AppData);
				}
				else if (sConnection.ToLower().Contains("sqlite odbc driver"))
				{
					sConnection = String.Format("Driver=SQLite ODBC Driver;Database={0}playlist.db3", AppData);
				}

				logger.Info("Opening database " + sConnection);

				String sLogConnection = ServerSettings.Log_Connection_String;
				if (sLogConnection.ToLower().Contains("sqlite3 odbc driver"))
				{
					sLogConnection = String.Format("Driver=SQLite3 ODBC Driver;Database={0}playlist.db3", AppData);
				}
				else if (sLogConnection.ToLower().Contains("sqlite odbc driver"))
				{
					sLogConnection = String.Format("Driver=SQLite ODBC Driver;Database={0}playlist.db3", AppData);
				}
				logger.Info("Opening log database " + sLogConnection);

				serverDBLayer = new DigitalSignage.ServerDatabase.DBLayer();
                serverDBLayer.PoolTimeout = ServerSettings.DBSessionTimeout;
				serverDBLayer.LogPath = AppData;
				serverDBLayer.openDB(sConnection, sLogConnection);

				serverDBLayer.InitializeConnectionPool(ServerSettings.DB_Connections);
				//	워커 스레드를 통해 커넥션을 만든다.
// 				bwConnectionPoolMaker = new BackgroundWorker();
// 				bwConnectionPoolMaker.DoWork += new DoWorkEventHandler(bwConnectionPoolMaker_DoWork);
// 				bwConnectionPoolMaker.RunWorkerAsync();

// 				dbLayer = new DigitalSignage.DataBase.DBLayer();
// 				dbLayer.openDB(sConnection);
// 				dbLayer.InitializeConnectionPool(ServerSettings.DB_Connections);

// 				logdbLayer = new DigitalSignage.DataBase.LogDBLayer();
// 				logdbLayer.openDB(sLogConnection);
// 
				localTasksList = new TaskList();
                localPlayersList = new PlayersList();
				localLogsList = new LogsList();

				logger.Info("All Database started");



            }
            catch (Exception e)
            {
                logger.Error(e + "");
            }
        }

		void bwConnectionPoolMaker_DoWork(object sender, DoWorkEventArgs e)
		{
			logger.Info("start InitializeConnectionPool");
			e.Result = serverDBLayer.InitializeConnectionPool(ServerSettings.DB_Connections);
			logger.Info("end InitializeConnectionPool");
		}

		public void release()
		{
			if (serverDBLayer != null)
			{
				try
				{
					serverDBLayer.closeDB();
					serverDBLayer.CloseConnectionPool();
					serverDBLayer = null;
				}
				catch { }
			}

		}

        public string AppData
        {
            get { return appData; }
        }

		public bool HasRefData
		{
			get { return _hasRefData; }
			set { _hasRefData = value; }
		}
		public int RefRefreshTime
		{
			get { return _refRefreshTime; }
			set { _refRefreshTime = value; }
		}

        public static Config GetConfig
        {
            get
            {
                lock (semaphore)
                {
                    if (instance == null)
                    {
                        instance = new Config();
//                         instance.init();
                    }
                    return instance;
                }
            }
        }

		/// <summary>
		/// 스케줄 핸들러 서비스가 운영중인지 반환한다.
		/// </summary>
		public bool IsRunningTaskHandlerService
		{
			get
			{
				try
				{
					System.ServiceProcess.ServiceController sc = new System.ServiceProcess.ServiceController("SmartSign Schedule Service");

					return sc.Status == System.ServiceProcess.ServiceControllerStatus.Running;
				}
				catch { }
				return false;
			}
		}
		/// <summary>
		/// KVM 프록시 서버가 운영중인지 반환한다.
		/// </summary>
		public bool IsRunningKVMProxy
		{
			get
			{
				try
				{
					System.ServiceProcess.ServiceController sc = new System.ServiceProcess.ServiceController("KVM Repeater Service");
					return sc.Status == System.ServiceProcess.ServiceControllerStatus.Running;
				}
				catch { }
				return false;
			}
		}

        /// <summary>
        /// 연결/해제 성공
        /// </summary>
        /// <param name="pid"></param>
        /// <param name="start_dt"></param>
        /// <param name="end_dt"></param>
        /// <param name="reg_dt"></param>
        /// <param name="code"></param>
        /// <param name="bIsConnect"></param>
		public void PlayerConnected(string pid, long start_dt, int code)
		{
			try
			{
                using (OdbcConnection conn = serverDBLayer.GetAvailableConnection())
                {

                    String sQuery = String.Format("INSERT INTO connectionlogs (pid, logcd, errorcd, start_dt, end_dt, reg_dt) VALUES ('{0}', {1}, {2}, {3}, NULL, {3})",
                        pid, (int)(LogType.LogType_Connection | LogType.LogType_Processing), code, start_dt);

                    using (OdbcCommand comm = new OdbcCommand(sQuery, conn))
                    {
                        comm.ExecuteNonQuery();
                        comm.Dispose();
                    }
					
					sQuery = String.Format("UPDATE players SET lastconndt={0} WHERE pid='{1}'", DateTime.UtcNow.Ticks, pid);

					using (OdbcCommand comm = new OdbcCommand(sQuery, conn))
					{
						comm.ExecuteNonQuery();
						comm.Dispose();
					}
				}
			}
			catch (Exception ex)
			{
				logger.Error(ex.Message);
			}
		}


        public void PlayerDisconnected(string pid, long start_dt, long reg_dt, int code)
        {
            try
            {
                /// CONNECT 정보 갱신
                using (OdbcConnection conn = serverDBLayer.GetAvailableConnection())
                {

                    String sQuery = String.Format("UPDATE connectionlogs SET end_dt={2} WHERE pid='{0}' AND start_dt={1}", pid, start_dt, reg_dt);

                    using (OdbcCommand comm = new OdbcCommand(sQuery, conn))
                    {
                        comm.ExecuteNonQuery();
                        comm.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
            }

            try
            {
                /// DISCONNECT 정보 입력
                using (OdbcConnection conn = serverDBLayer.GetAvailableConnection())
                {

                    String sQuery = String.Format("INSERT INTO connectionlogs (pid, logcd, errorcd, start_dt, end_dt, reg_dt) VALUES ('{0}', {1}, {2}, {3}, {4}, {5})",
                        pid, (int)(LogType.LogType_Connection | LogType.LogType_Proceed), code, start_dt, reg_dt, reg_dt);

                    using (OdbcCommand comm = new OdbcCommand(sQuery, conn))
                    {
                        comm.ExecuteNonQuery();
                        comm.Dispose();
                    }

					sQuery = String.Format("UPDATE players SET lastconndt={0} WHERE pid='{1}'",  DateTime.UtcNow.Ticks, pid);
					
					using (OdbcCommand comm = new OdbcCommand(sQuery, conn))
					{
						comm.ExecuteNonQuery();
						comm.Dispose();
					}
                }
                

            }
            catch { }
            
        }

		/// <summary>
		/// 플레이어 연결 에러
		/// </summary>
		/// <param name="pid"></param>
        /// <param name="code"></param>
        public void PlayerConnectError(string pid, int code)
        {
            String sQuery = String.Empty;
			try
			{
                using (OdbcConnection conn = serverDBLayer.GetAvailableConnection())
                {

                    long currTicks = TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime());
                    sQuery = String.Format("INSERT INTO connectionlogs (pid, logcd, errorcd, start_dt, end_dt, reg_dt) VALUES ('{0}', {1}, {2}, {3}, {4}, {5})",
                        pid, (int)(LogType.LogType_Connection | LogType.LogType_Failed), code, currTicks, currTicks, currTicks);

                    using (OdbcCommand comm = new OdbcCommand(sQuery, conn))
                    {
                        comm.ExecuteNonQuery();
                        comm.Dispose();
                    }
                }
			}
			catch (Exception ex)
			{
				logger.Error(ex.Message + sQuery);
			}
		}

        /// <summary>
        /// 현재 Target 광고 Relay ID
        /// </summary>
        public long CurrentRelayID = -1;

        /// <summary>
        /// 타겟 광고 성공 로그 등록 함수
        /// </summary>
        /// <param name="relay_id"></param>
        /// <param name="locType">성공한 위치</param>
        /// <returns></returns>
        public bool WriteTargetAdSuccess(long relay_id, TargetAd_ErrorType locType)
        {
            int nRet = -1;

            String sQuery = String.Empty;
            try
            {
                logger.Info(String.Format("Target Ad Success: RelayID({0}), TargetType ({1})", relay_id, locType));

                using (OdbcConnection conn = serverDBLayer.GetAvailableLogConnection())
                {

                    sQuery = String.Format("UPDATE relaylogs SET {1} = {2} WHERE relay_id = {0}",
                        relay_id, LogsList.GetTargetAdColumn((int)locType), DateTime.UtcNow.Ticks);

                    using (OdbcCommand comm = new OdbcCommand(sQuery, conn))
                    {
                        nRet = comm.ExecuteNonQuery();
                        comm.Dispose();
                    }
                }
            }
            catch (InvalidOperationException ioe)
            {
                logger.Error(ioe + " query : " + sQuery);
                ReInitializeConnectionPool();
            }
            catch (Exception e)
            {
                logger.Error(e.Message + " query : " + sQuery);
            }
            return nRet > 0;
        }

        /// <summary>
        /// 타겟 광고 생성
        /// </summary>
        /// <param name="relay_id"></param>
        /// <param name="pid"></param>
        /// <returns></returns>
        public bool InsertTargetAd(long relay_id, string pid)
        {
            int nRet = -1;

            String sQuery = String.Empty;
            try
            {
                logger.Info(String.Format("Target Ad Insert: RelayID({0}), Player ID ({1})", relay_id, pid));

                using (OdbcConnection conn = serverDBLayer.GetAvailableLogConnection())
                {
                    sQuery = String.Format("INSERT INTO relaylogs (relay_id, pid, created_ts) VALUES ({0}, '{1}', {2})",
                        relay_id, pid, DateTime.UtcNow.Ticks);

                    using (OdbcCommand comm = new OdbcCommand(sQuery, conn))
                    {
                        nRet = comm.ExecuteNonQuery();
                        comm.Dispose();
                    }
                }
            }
            catch (InvalidOperationException ioe)
            {
                logger.Error(ioe + " query : " + sQuery);
                ReInitializeConnectionPool();
            }
            catch (Exception e)
            {
                logger.Error(e.Message + " query : " + sQuery);
            }
            return nRet > 0;
        }



        /// <summary>
        /// 타겟 광고 에러 로그 등록 함수
        /// </summary>
        /// <param name="relay_id"></param>
        /// <param name="locType">실패한 위치</param>
        /// <param name="Errorcd">위치별 에러 코드</param>
        /// <returns></returns>
        public bool WriteTargetAdError(long relay_id, TargetAd_ErrorType locType, int Errorcd)
        {
            int nRet = -1;

            String sQuery = String.Empty;
            try
            {
                logger.Error(String.Format("Target Ad Error: RelayID({0}), ErrType({1}) ErrCode ({2})", relay_id, locType, Errorcd));

                using (OdbcConnection conn = serverDBLayer.GetAvailableLogConnection())
                {

                    sQuery = String.Format("INSERT INTO relaylogs_error (relay_id, err_tp, err_cd, err_ts) VALUES ({0}, {1}, {2}, {3})",
                        relay_id, (int)locType, Errorcd, DateTime.UtcNow.Ticks);

                    using (OdbcCommand comm = new OdbcCommand(sQuery, conn))
                    {
                        nRet = comm.ExecuteNonQuery();
                        comm.Dispose();
                    }
                }
            }
            catch (InvalidOperationException ioe)
            {
                logger.Error(ioe + " query : " + sQuery);
                ReInitializeConnectionPool();
            }
            catch (Exception e)
            {
                logger.Error(e.Message + " query : " + sQuery);
            }
            return nRet > 0;
        }
    }
}