﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using NLog;

namespace DigitalSignage.SchedulerService
{
    static class ServiceMain
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            try
            {
                AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);

                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[] 
			    { 
				    new SchedulerService() 
			    };
                ServiceBase.Run(ServicesToRun);

            }
            catch (Exception ex)
            {
                string errmsg = ex.Message.ToString();
            }
        }

        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            if (e.IsTerminating)
            {
                try
                {
                    Logger logger = LogManager.GetCurrentClassLogger();

                    logger.Error("Unhandled Exception: Object ({0})" + e.ExceptionObject);
                    logger.Error("Unhandled Exception: ExceptionToString ({0})" + e.ToString());
                }
                catch { }
            }
        }
    }
}
