﻿using System;
using System.Collections;

//SQLite & DB routines
using DigitalSignage.DataBase;
using DigitalSignage.Common;

using NLog;
using Sloppycode.net;

namespace DigitalSignage.SchedulerService
{
    // RSS manupalation routines
    public class RSSList : MarshalByRefObject, IRSSList
    {
        private Config cfg = null;
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public RSSList()
        {
            cfg = Config.GetConfig; //protection from initialization dead loop
        }

        public int AddChannel(RssFeed newChannel)
        {
            return -1; //not implemented
        }

        public int DeleteChannel(int channel)
        {
            return -1; //not implemented
        }

        public RssFeed[] GetChannels()
        {
            RssFeed[] result = null;
            return result;
        }

        RssFeed GetChannel(int channel)
        {
            RssFeed result = new RssFeed();
            result.url = "http://www.bbc.co.uk/ukrainian/index.xml"; //!!!!!!! test code
            return result;
        }

        public RssItems GetChannelContent(int channel)
        {
            RssItems result = null;
            try
            {
                RssFeed rss = GetChannel(channel);
                RssReader rssReader = new RssReader();
                rssReader.RdfMode = true;
                RssFeed feed = rssReader.Retrieve(rss.url);
                if (feed.ErrorMessage == null || feed.ErrorMessage == "")
                    result = feed.Items;
            }
            catch (Exception e)
            {
                logger.Error(e + "");
            }
            return result;
        }

    }
}