﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;

using DigitalSignage.Common;
using DigitalSignage.DataBase;
using NLog;
//SQLite & DB routines
using System.Data.Common;
using System.Data.SQLite;

namespace DigitalSignage.SchedulerService
{
    public class UsersList : MarshalByRefObject, IUsersList
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        static public AuthTicket GetUserLevel(AuthTicket at)
        {
            AuthTicket res = at;
            at.level = UserLevels.UserNone;
            Config cfg = Config.GetConfig;
            if ( at.name.Equals("")) return res;
			using (DataBase.DataSetTableAdapters.usersTableAdapter da = cfg.dbLayer.CreateUsersDA())
			{
				using (DataSet.usersDataTable tbl = da.CheckUser(at.name, at.passhash))
				{
					if (tbl.Rows.Count > 0)
					{
						DataSet.usersRow row = (DataSet.usersRow)tbl.Rows[0];
						at.gid = row.gid;
						at.level = (UserLevels)row.level;
						at.pid = row.pid;
						at.uid = (int)row.id;
					}
					tbl.Dispose();
				}
				da.Dispose();
			}
			return res;
        }

		public bool AddUser(AuthTicket at, String name, String password, int level, string gid, string pid)
        {
            Config cfg = Config.GetConfig;
            try
            {
                at = UsersList.GetUserLevel(at);
                if (at.level != UserLevels.UserAdmin) throw new Exception("Unauthorized");
				using (DataBase.DataSetTableAdapters.usersTableAdapter da = cfg.dbLayer.CreateUsersDA())
				{
					MD5 md5 = new MD5CryptoServiceProvider();
					Byte[] original;
					Byte[] encoded;
					original = ASCIIEncoding.Default.GetBytes(password);
					encoded = md5.ComputeHash(original);
					StringBuilder sb = new StringBuilder();
					foreach (byte hex in encoded) sb.Append(hex.ToString("x2"));
					da.AddUser(name, sb.ToString().ToLower(), level, gid, pid);
					da.Dispose();
				}
				return true;
            }
            catch (Exception e)
            {
                logger.Log(LogLevel.Error, e + "");
            }
            return false;
        }

        public bool DeleteUser(AuthTicket at, int id)
        {
            Config cfg = Config.GetConfig;
            try
            {
                at = UsersList.GetUserLevel(at);
                if (at.level != UserLevels.UserAdmin) throw new Exception("Unauthorized");
				using (DataBase.DataSetTableAdapters.usersTableAdapter da = cfg.dbLayer.CreateUsersDA())
				{
					da.DeleteUser(id);
					da.Dispose();
				}
				return true;
            }
            catch (Exception e)
            {
                logger.Log(LogLevel.Error, e + "");
            }
            return false;
        }

        public AuthTicket GetLevel(AuthTicket at)
        {
            try
            {
                return GetUserLevel(at);
            }
            catch (Exception e)
            {
                logger.Log(LogLevel.Error, e + "");
            }
            return new AuthTicket();
        }

		public bool EditUser(AuthTicket at, int id, String password, int level, string gid, string pid)
        {
            Config cfg = Config.GetConfig;
            try
            {
                at = UsersList.GetUserLevel(at);
                if (at.level != UserLevels.UserAdmin) throw new Exception("Unauthorized");
				using (DataBase.DataSetTableAdapters.usersTableAdapter da = cfg.dbLayer.CreateUsersDA())
				{
					MD5 md5 = new MD5CryptoServiceProvider();
					Byte[] original;
					Byte[] encoded;
					original = ASCIIEncoding.Default.GetBytes(password);
					encoded = md5.ComputeHash(original);
					StringBuilder sb = new StringBuilder();
					foreach (byte hex in encoded) sb.Append(hex.ToString("x2"));

					da.UpdateUser(sb.ToString().ToLower(), level, gid, pid, id);
					da.Dispose();
				}
				return true;
			}
            catch (Exception e)
            {
                logger.Log(LogLevel.Error, e + "");
            }
            return false;
        }

        public DataSet.usersDataTable GetUsersList(AuthTicket at)
        {
            Config cfg = Config.GetConfig;
            try
            {
                at = UsersList.GetUserLevel(at);
                if (at.level != UserLevels.UserAdmin) throw new Exception("Unauthorized");
                using (DataBase.DataSetTableAdapters.usersTableAdapter da = cfg.dbLayer.CreateUsersDA())
				{
					using (DataSet.usersDataTable dt = da.GetData())
					{
						da.Dispose();
						return dt;
					}
				}
            }
            catch (Exception e)
            {
                logger.Log(LogLevel.Error, e + "");
            }
            return null;
        }
    }
}
