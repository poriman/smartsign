﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Runtime.Serialization;
using System.Threading;
using NLog;

using DigitalSignage.ServerDatabase;
using DigitalSignage.Common;
using System.Xml;
using System.ServiceModel.Channels;

using DigitalSignage.SchedulerService;

using DigitalSignage.SchedulerService.GSPointRelayService;

using System.Data.Odbc;

namespace IVisionService
{
	#region Topic Class 정의

	// 사용자 구분
    public enum UserLevels
    {
        Admin = 0,
        Group,
        Media,
        Scheduler,
        None
	}

	[DataContract]
    public class Topic
    {
        private string _Groupid;
        private string _Mediaid;
        private UserLevels _Level;

        [DataMember]
        public string Groupid
        {
            set { _Groupid = value; }
            get { return _Groupid; }
        }

        [DataMember]
        public string Mediaid
        {
            set { _Mediaid = value; }
            get { return _Mediaid; }
        }

        [DataMember]
        public UserLevels Level
        {
            set { _Level = value; }
            get { return _Level; }
        }
	}
	#endregion

	#region Message Class 정의

	//메세지 타입
	public enum MessageType
	{
		msg_Login = 0,      // 로그인
		msg_CtrlSche,   // 제어 스케쥴
		msg_MediaSche,  // 미디어 스케쥴
		msg_UrgSche,    // 긴급 스케쥴
		msg_SubtitleSche,// 자막 스케쥴
		msg_TimeSche,   // 시간 스케쥴
		msg_AppUpdate,  // 프로그램 업데이트
		msg_ChangeID,   // 그룹, 미디어 아이디 변경
		msg_Leave,      // 종료
		msg_Error,      // 에러 로그
		msg_Status,      // 상태 전송
		msg_PlayerInfo,	//	플레이어 상세 정보
        msg_RequestGSRelay,  //  GS Relay 서버에게 요청
        msg_ResponseGSRelay, //  GS Relay 서버에게서 받은 정보 전달
        msg_IidbEventSignal, // IIDB 수신 신호
        msg_IidbEventXml,   //  IIDB 수신 XML
        msg_IidbEventContent,    //  IIDB 수신 컨텐트
        msg_DoCapture,  //  캡쳐 관련 메시지
        msg_ControlResult,   //  제어 관련 응답 메시지

	};

	[DataContract]
    public class Message
    {
        private MessageType _msgType;       // 메시지 타입
        private UserLevels _level;         // 접속자 레벨
        private Topic _sender;        // 미디어 아이디
        private string _result;        // 결과 값 Success, Faile
        private string _version;       // 버전 정보
        private string _content;       // 실제 메시지
        private DateTime _time;

        [DataMember]
        public MessageType MsgType
        {
            get { return _msgType; }
            set { _msgType = value; }
        }
        [DataMember]
        public UserLevels Level
        {
            get { return _level; }
            set { _level = value; }
        }
        [DataMember]
        public Topic Sender
        {
            get { return _sender; }
            set { _sender = value; }
        }
        [DataMember]
        public string Result
        {
            get { return _result; }
            set { _result = value; }
        }
        [DataMember]
        public string Version
        {
            get { return _version; }
            set { _version = value; }
        }
        [DataMember]
        public string Content
        {
            get { return _content; }
            set { _content = value; }
        }
        [DataMember]
        public DateTime Time
        {
            get { return _time; }
            set { _time = value; }
        }
	}
	#endregion

	#region FileMessage Class 정의
	[DataContract]
    public class FileMessage
    {
        private Topic sender;
        private string fileName;
        private byte[] data;
        private string filePaht;
        private bool isExcute;
        private DateTime time;

        [DataMember]
        public Topic Sender
        {
            get { return sender; }
            set { sender = value; }
        }

        [DataMember]
        public string FileName
        {
            get { return fileName; }
            set { fileName = value; }
        }

        [DataMember]
        public string FilePath
        {
            get { return filePaht; }
            set { filePaht = value; }
        }

        [DataMember]
        public bool Excute
        {
            get { return isExcute; }
            set { isExcute = value; }
        }

        [DataMember]
        public byte[] Data
        {
            get { return data; }
            set { data = value; }
        }

        [DataMember]
        public DateTime Time
        {
            get { return time; }
            set { time = value; }
        }
	}
	#endregion

	#region INetworkService 인터페이스
	[ServiceContract(CallbackContract = typeof(INetworkCallback), SessionMode = SessionMode.Required)]
	public interface INetworkService
	{
		[OperationContract(IsInitiating = true)]
		bool Connect(Topic client);

		[OperationContract(IsOneWay = true)]
		void SendMessage(Message msg);

		[OperationContract(IsOneWay = true)]
		void Whisper(Message msg, Topic receiver);

		[OperationContract(IsOneWay = false)]
		bool SendFile(FileMessage fileMsg, Topic receiver);

		[OperationContract(IsOneWay = true, IsTerminating = true)]
		void Disconnect(Topic client);
	}
	#endregion

	#region INetworkCallback 인터페이스
	public interface INetworkCallback
	{
		[OperationContract(IsOneWay = true)]
		void RefreshClients(List<Topic> clients);

		[OperationContract(IsOneWay = true)]
		void Receive(Message msg);

		[OperationContract(IsOneWay = true)]
		void ReceiveWhisper(Message msg, Topic receiver);

		[OperationContract(IsOneWay = true)]
		void ReceiverFile(FileMessage fileMsg, Topic receiver);

		[OperationContract(IsOneWay = true)]
		void UserJoin(Topic client);

		[OperationContract(IsOneWay = true)]
		void UserLeave(Topic client);
	}
	#endregion

	#region NetworkService Class 정의
	[ServiceBehavior(InstanceContextMode = InstanceContextMode.PerSession, ConcurrencyMode = ConcurrencyMode.Multiple)]
    public class NetworkService : INetworkService
    {
		private static Logger logger = LogManager.GetCurrentClassLogger();

        private static Dictionary<Topic, INetworkCallback> clients = new Dictionary<Topic, INetworkCallback>();
		private static List<Topic> clientList = new List<Topic>();

        private static bool _process = false;
        private DigitalSignage.SchedulerService.Config cfg = DigitalSignage.SchedulerService.Config.GetConfig;

        private int Start()
        {
            if (_process == false)
            {
                _process = true;

				logger.Info("Network service started.");

				//	다운로드 시간대를 적용하는 루틴
				cfg.delUpdateDownloadMethod = ApplyDownloadTimeScopeImmediately;

                cfg.delUpdatePlayerConfigMethod = UpdatePlayerConfig;

				//	즉시 플레이어 정보 적용하는 루틴
				cfg.delUpdatePIDMethod = UpdatePlayerID;
				cfg.delUpdateGroupInfoMethod = SendToClient_UpdateGroupInfo;
				cfg.delUpdateGroupInfoToAllMethod = SendToClient_UpdateGroupInfoToAllPlayers;
				cfg.delUpdateServiceIPMethod = UpdateServiceIP;
                
                cfg.delSendEventMethod = SendIIDBEvent;

                cfg.delSendControlEventMethod = SendControlEvent;
                cfg.delCaptureProcessEventMethod = CaptureProcessEvent;

                cfg.delSendUpdateScheduleMethod = UpdateSchedule;

                cfg.delSendMessageToPlayerMethod = SendMessageEvent;

                ResponseEventHelper.GetInstance.SendMessagServerToPlayer += new ResponseEventHandler(GetInstance_SendMessagServerToPlayer);
			}
            return 0;
        }

        void GetInstance_SendMessagServerToPlayer(object sender, ResponseEventArgs args)
        {
            Message msg = new Message();

            Topic recv = new Topic();
            if (args.PlayerID == "ALL")
            {
                recv.Groupid = "";
                recv.Mediaid = "ALL";

                recv.Level = UserLevels.Media;

                String sCode = args.ResponseCode;

                logger.Info("Sending To Group ID= ({1}), Player ID= ({0}) : CODE ({2})", recv.Groupid, recv.Mediaid, sCode);
                msg.Result = sCode;
                msg.Content = args.Content;
                msg.Sender = recv;
                msg.MsgType = MessageType.msg_CtrlSche;
                msg.Level = UserLevels.None;
                msg.Time = DateTime.Now;
                msg.Version = "";

                #region 메시지 전송
                try
                {
                    SendMessage(msg);
                }
                catch (Exception ex)
                {
                    logger.Error("IVisionService, Whisper: " + ex);
                }
                #endregion
            }
            else if (!String.IsNullOrEmpty(args.PlayerID))
            {
                recv.Groupid = "";
                recv.Mediaid = args.PlayerID;

                recv.Level = UserLevels.Media;

                String sCode = args.ResponseCode;

                logger.Info("Sending To Group ID= ({1}), Player ID= ({0}) : CODE ({2})", recv.Groupid, recv.Mediaid, sCode);
                msg.Result = sCode;
                msg.Content = args.Content;
                msg.Sender = recv;
                msg.MsgType = MessageType.msg_CtrlSche;
                msg.Level = UserLevels.None;
                msg.Time = DateTime.Now;
                msg.Version = "";

                #region 메시지 전송
                try
                {
                    SendMessage(msg);
                }
                catch (Exception ex)
                {
                    logger.Error("IVisionService, Whisper: " + ex);
                }
                #endregion
            }
        }

        public void UpdateSchedule(String gid, String pid)
        {
            Message msg = new Message();

            Topic recv = new Topic();
            if (String.IsNullOrEmpty(pid))
            {
                recv.Groupid = gid;
                recv.Mediaid = "ALL";
            }
            else
            {
                recv.Groupid = gid;
                recv.Mediaid = pid;
            }

            recv.Level = UserLevels.Media;

            String sCode = iVisionCode.FUNC_SCHEDULE_REFRESH;

            logger.Info("Sending To Group ID= ({1}), Player ID= ({0}) : CODE ({2})", pid, gid, sCode);
            msg.Result = sCode;
            msg.Content = iVisionCode.FUNC_SCHEDULE_REFRESH;
            msg.Sender = recv;
            msg.MsgType = MessageType.msg_CtrlSche;
            msg.Level = UserLevels.None;
            msg.Time = DateTime.Now;
            msg.Version = "";

            #region 메시지 전송
            try
            {
                SendMessage(msg);
            }
            catch (Exception ex)
            {
                logger.Error("IVisionService, Whisper: " + ex);
            }
            #endregion
        }

        public void UpdatePlayerConfig(string pid)
        {
            Message msg = new Message();

            Topic recv = new Topic();
            recv.Groupid = "";
            recv.Mediaid = pid;
            recv.Level = UserLevels.Media;

            String sCode = iVisionCode.CONF_REFRESH;

            logger.Info("Sending To Player ID=" + pid + " : " + sCode);
            msg.Result = sCode;
            msg.Content = iVisionCode.CONF_REFRESH;
            msg.Sender = recv;
            msg.MsgType = MessageType.msg_Login;
            msg.Level = UserLevels.None;
            msg.Time = DateTime.Now;
            msg.Version = "";

            #region 메시지 전송
            try
            {
                ClientManager manager = ClientManager.GetInstance;

                INetworkCallback callback = manager.GetCallback(recv.Mediaid);
                callback.ReceiveWhisper(msg, recv);
            }
            catch (Exception ex)
            {
                logger.Error("IVisionService, Whisper: " + ex);
            }
            #endregion
        }
        

        /// <summary>
        /// 플레이어로 캡쳐 명령 전송
        /// </summary>
        /// <param name="doStart"></param>
        /// <param name="pid"></param>
        /// <returns></returns>
        public bool CaptureProcessEvent(bool doStart, string pid)
        {
            Message msg = new Message();

            Topic recv = new Topic();
            recv.Groupid = "";
            recv.Mediaid = pid;
            recv.Level = UserLevels.Media;

            String sCode = doStart ? iVisionCode.FUNC_START_CAPTURE : iVisionCode.FUNC_END_CAPTURE;

            logger.Info("Sending To Player ID=" + pid + " : " + sCode);
            msg.Result = sCode;
            msg.Content = doStart.ToString();
            msg.Sender = recv;
            msg.MsgType = MessageType.msg_DoCapture;
            msg.Level = UserLevels.None;
            msg.Time = DateTime.Now;
            msg.Version = "";

            #region 메시지 전송
            try
            {
                ClientManager manager = ClientManager.GetInstance;

                INetworkCallback callback = manager.GetCallback(recv.Mediaid);
                callback.ReceiveWhisper(msg, recv);

                return true;

            }
            catch (Exception ex)
            {
                logger.Error("IVisionService, Whisper: " + ex);

                return false;
            }
            #endregion
        }

        /// <summary>
        /// 플레이어로 제어 명령 전송
        /// </summary>
        /// <param name="nID"></param>
        /// <param name="code"></param>
        /// <param name="control_value"></param>
        /// <param name="pid"></param>
        public void SendControlEvent(int nID, ControlRequest code, int control_value, string pid)
        {
            Message msg = new Message();

            Topic recv = new Topic();
            recv.Groupid = "";
            recv.Mediaid = pid;
            recv.Level = UserLevels.Media;

            String sCode = String.Empty;

            switch (code)
            {
                case ControlRequest.PowerOff:
                    sCode = iVisionCode.FUNC_POWER_OFF;
                    break;
                case ControlRequest.Restart:
                    sCode = iVisionCode.FUNC_REBOOT;
                    break;
                case ControlRequest.PCVol:
                    sCode = iVisionCode.FUNC_PCVOLUME;
                    break;
                case ControlRequest.MonPowerOff:
                    sCode = iVisionCode.FUNC_MON_POWER_OFF;
                    break;
                case ControlRequest.MonPowerOn:
                    sCode = iVisionCode.FUNC_MON_POWER_ON;
                    break;
                case ControlRequest.MonVol:
                    sCode = iVisionCode.FUNC_MON_VOLUME;
                    break;
                default:
                    throw new NotImplementedException();
                    break;
            }

            logger.Info("Sending To Player ID=" + pid + " : " + sCode + " (" + control_value + ")");
            msg.Result = sCode;
            msg.Content = String.Format("{0}|{1}", nID, control_value);
            msg.Sender = recv;
            msg.MsgType = MessageType.msg_CtrlSche;
            msg.Level = UserLevels.None;
            msg.Time = DateTime.Now;
            msg.Version = "";

            #region 메시지 전송
            try
            {
                ClientManager manager = ClientManager.GetInstance;

                INetworkCallback callback = manager.GetCallback(recv.Mediaid);
                callback.ReceiveWhisper(msg, recv);
            }
            catch (Exception ex)
            {
                logger.Error("IVisionService, Whisper: " + ex);

                ControlService.GetInstance.SetControlResponse(nID, ControlResponse.NotConnected);
            }
            #endregion
        }

        /// <summary>
        /// 플레이어로 IIDB 이벤트 전송
        /// </summary>
        /// <param name="code"></param>
        /// <param name="pid"></param>
        /// <param name="message"></param>
        public void SendIIDBEvent(EventCodes code, string pid, string message)
        {
            Message msg = new Message();

            Topic recv = new Topic();
            recv.Groupid = "";
            recv.Mediaid = pid;
            recv.Level = UserLevels.Media;

            logger.Info("Sending To Player ID=" + pid + " : IIDB_SENDMESSAGE (" + message + ")");
            msg.Result = iVisionCode.IIDB_SENDMESSAGE;
            msg.Content = message;
            msg.Sender = recv;
            switch (code)
            {
                case EventCodes.EventDataOccured:
                    msg.MsgType = MessageType.msg_IidbEventContent;
                    break;
                case EventCodes.EventSignalOccured:
                    msg.MsgType = MessageType.msg_IidbEventSignal;
                    break;
                case EventCodes.EventXmlOccurred:
                    msg.MsgType = MessageType.msg_IidbEventXml;
                    break;
            }
            msg.Level = UserLevels.None;
            msg.Time = DateTime.Now;
            msg.Version = "";

            SendMessage(msg);
        }


        private int Stop()
        {
            _process = false;
            logger.Info("Network service ended.");
            return 0;
        }

        public INetworkCallback CurrentCallback
        {
            get
            {
                return OperationContext.Current.GetCallbackChannel<INetworkCallback>();

            }
        }

        public IContextChannel CurrentContextChannel
        {
            get
            {
                return OperationContext.Current.Channel;
            }
        }

        object syncObj = new object();
 

        #region Members

        /// <summary>
        /// 접속 인증
        /// </summary>
        /// <param name="client"></param>
        /// <returns></returns>
        public bool Connect(Topic client)
        {
			DateTime dt = DateTime.Now;

            try
            {
				ClientManager manager = ClientManager.GetInstance;

                INetworkCallback Tcallback = CurrentCallback;
				
                Message msg = new Message();
                msg.Sender = client;
                msg.MsgType = MessageType.msg_Login;
                msg.Level = UserLevels.None;
                msg.Time = DateTime.Now;
                msg.Version = "";// Player 버전 정보

				//	클라이언트 최대 개수 체크
				if (manager.ClientCount >= cfg.MaxCountOfPlayers)
				{
					
					logger.Info(string.Format("{0} Connect {1} : {2}", iVisionCode.CONN_ERROR_EXCESS_OF_ALLOWED_MAX_PLAYER.ToString(), client.Groupid, client.Mediaid));
					msg.Result = iVisionCode.CONN_ERROR_EXCESS_OF_ALLOWED_MAX_PLAYER;// 접속 실패 (플레이어 최대값보다 큼) 
					msg.Content = "";
					
					Tcallback.ReceiveWhisper(msg, client);
					//	에러 로그 기록
					cfg.PlayerConnectError(client.Mediaid, (int)ErrorCodes.IS_ERROR_CONN_EXCESS_OF_ALLOWED_MAX_PLAYER);
					return false;
				}

				//	이미 존재하면 Disconnect로그를 기록한다.
				if (manager.HasMember(client))
				{
					long currTicks = TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime());
				
					DateTime? dtConnected = manager.GetConnectedDateTimeFromMember(client);

					if (dtConnected.HasValue)
						cfg.PlayerDisconnected(client.Mediaid, TimeConverter.ConvertToUTP(dtConnected.Value.ToUniversalTime()), currTicks, (int)ErrorCodes.IS_ERROR_CONN_DISCONNECTED);
					else
                        cfg.PlayerDisconnected(client.Mediaid, currTicks, currTicks, (int)ErrorCodes.IS_ERROR_CONN_DISCONNECTED);
					//	로그상의 시간 보정의 위하여
					Thread.Sleep(1000);	
				}
	
				if(manager.AddMember(client, Tcallback))
				{
					//	플레이어 ID가 존재함.
					logger.Info(string.Format("{0} Connect {1} : {2} Client Count {3}", iVisionCode.CONN_SUCCESS.ToString(), client.Groupid, client.Mediaid, manager.ClientCount));

					msg.Result = iVisionCode.CONN_SUCCESS;// 접속 성공
					msg.Content = "";
					manager.SetPlayerStatus(client.Mediaid, true);
					Tcallback.ReceiveWhisper(msg, client);

					Start();

					if (cfg.HasRefData)
					{
						msg.Result = iVisionCode.CONF_REFDATA_INFO;
						msg.Content = "refdata|" + cfg.RefRefreshTime.ToString();
						Tcallback.ReceiveWhisper(msg, client);
					}
// 					//	Download 시간 보내주기
					ApplyDownloadTimeScopeImmediately(client.Groupid, client.Mediaid);

					//	접속 로그 기록

					long currTicks = TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime());
					cfg.PlayerConnected(client.Mediaid, currTicks, 0);

					TimeSpan ts = DateTime.Now - dt;
					if (ts.TotalSeconds > 5.0)
						logger.Warn(client.Mediaid + " : " + "Connect Exec Time : " + ts.TotalSeconds.ToString());

					return true;

				}
				else
				{
					//	플레이어 ID가 존재하지 않음.
					logger.Info(string.Format("{0} Connect {1} : {2}", iVisionCode.CONN_ERROR_NO_PLAYER.ToString(), "0000", client.Mediaid));
					msg.Result = iVisionCode.CONN_ERROR_NO_PLAYER; //DB 데이터 없음
					msg.Content = "";

					Tcallback.ReceiveWhisper(msg, client);

					//	접속 로그 기록
					cfg.PlayerConnectError(client.Mediaid, (int)ErrorCodes.IS_ERROR_CONN_NO_PLAYER);

				}
            }
            catch (Exception ex)
            {
                logger.Error("IVisionService, Connect: " + ex);
            }

			TimeSpan ts2 = DateTime.Now - dt;
			if (ts2.TotalSeconds > 5.0)
				logger.Warn(client.Mediaid + " : " + "Connect  Exec Time : " + ts2.TotalSeconds.ToString());

            return false;
        }


		public void UpdatePlayerID(string old_gid, string old_pid, string new_pid)
		{
			Message msg = new Message();

			Topic recv = new Topic();
			recv.Groupid = old_gid;
			recv.Mediaid = old_pid;
			recv.Level = UserLevels.Media;

			logger.Info("Sending To Player ID=" + old_pid + " : CONF_MODIFIED_PLAYER_ID (" + new_pid + ")");
			msg.Result = iVisionCode.CONF_MODIFIED_PLAYER_ID;
			msg.Content = new_pid;
			msg.Sender = recv;
			msg.MsgType = MessageType.msg_Login;
			msg.Level = UserLevels.None;
			msg.Time = DateTime.Now;
			msg.Version = "";

			SendMessage(msg);

			Topic newTopic = new Topic();
			newTopic.Groupid = old_gid;
			newTopic.Mediaid = new_pid;
			newTopic.Level = recv.Level;

			UpdateMember(recv, newTopic);
		}

		public void SendToClient_UpdateGroupInfoToAllPlayers(string gid)
		{
			try
			{
				XmlNode gnode = CacheObject.GetInstance.FindGroupNode(gid);

				if (gnode != null)
				{
					XmlNodeList players = gnode.SelectNodes("descendant::player[attribute::del_yn=\"N\"]");

					foreach(XmlNode player in players)
					{
						try
						{
							Message msg = new Message();

							Topic recv = new Topic();
							recv.Groupid = gid;
							recv.Mediaid = player.Attributes["pid"].Value;
							recv.Level = UserLevels.Media;
							logger.Info("Sending Message To Player ID=" + recv.Mediaid + ": CONF_MODIFIED_GROUP_ID");
							msg.Result = iVisionCode.CONF_MODIFIED_GROUP_ID;
							msg.Content = "";
							msg.Sender = recv;
							msg.MsgType = MessageType.msg_Login;
							msg.Level = UserLevels.None;
							msg.Time = DateTime.Now;
							msg.Version = "";

							SendMessage(msg);
						}
						catch{}
					}
				}
			}
			catch (Exception ex)
			{
				logger.Error(ex.ToString());
			}
		}

		public void SendToClient_UpdateGroupInfo(string new_gid, string old_pid)
		{
			Message msg = new Message();

			Topic recv = new Topic();
			recv.Groupid = "";
			recv.Mediaid = old_pid;
			recv.Level = UserLevels.Media;

			logger.Info("Sending Message To PlayerID=" + old_pid + ": CONF_MODIFIED_GROUP_ID");
			msg.Result = iVisionCode.CONF_MODIFIED_GROUP_ID;
			msg.Content = "";
			msg.Sender = recv;
			msg.MsgType = MessageType.msg_Login;
			msg.Level = UserLevels.None;
			msg.Time = DateTime.Now;
			msg.Version = "";

			SendMessage(msg);

			Topic newTopic = new Topic();
			newTopic.Groupid = new_gid;
			newTopic.Mediaid = old_pid;
			newTopic.Level = recv.Level;

			UpdateMember(recv, newTopic);
		}

		public void UpdateServiceIP(string gid, string pid, string new_service_IP)
		{
			Message msg = new Message();

			Topic recv = new Topic();
			recv.Groupid = gid;
			recv.Mediaid = pid;
			recv.Level = UserLevels.Media;

			logger.Info("Sending Message To Player ID=" + pid + ": CONF_MODIFIED_SERVER_IP (" + new_service_IP + ")");
			msg.Result = iVisionCode.CONF_MODIFIED_SERVER_IP;
			msg.Content = new_service_IP;
			msg.Sender = recv;
			msg.MsgType = MessageType.msg_Login;
			msg.Level = UserLevels.None;
			msg.Time = DateTime.Now;
			msg.Version = "";

			SendMessage(msg);
		}

        /// <summary>
        /// 플레이어로 명령 전송
        /// </summary>
        /// <param name="pid"></param>
        /// <param name="code"></param>
        /// <param name="contents"></param>
        /// <returns></returns>
        public bool SendMessageEvent(string pid, string code, string contents)
        {
            Message msg = new Message();

            Topic recv = new Topic();
            recv.Groupid = "";
            recv.Mediaid = pid;
            recv.Level = UserLevels.Media;

            logger.Info("Sending To Player ID=" + pid + " : " + code + " (" + contents + ")");
            msg.Result = code;
            msg.Content = contents;
            msg.Sender = recv;
            msg.MsgType = MessageType.msg_CtrlSche;
            msg.Level = UserLevels.None;
            msg.Time = DateTime.Now;
            msg.Version = "";

            #region 메시지 전송
            try
            {
                ClientManager manager = ClientManager.GetInstance;

                INetworkCallback callback = manager.GetCallback(recv.Mediaid);
                callback.ReceiveWhisper(msg, recv);

                return true;
            }
            catch (Exception ex)
            {
                logger.Error("IVisionService, Whisper: " + ex);
            }
            #endregion

            return false;
        }


		/// <summary>
		/// 미디어에게 지역 정보를 보낸다.
		/// </summary>
		/// <param name="pid">대상 미디어 PID</param>
		public void SendRegionInformation(string pid)
		{
			Message msgRegion = new Message();
			CacheObject obj = CacheObject.GetInstance;

		}
		/// <summary>
		/// 다운로드 시간 대를 바로 살아있는 Player에게 적용한다.
		/// </summary>
		public void ApplyDownloadTimeScopeImmediately(string gid, string pid)
		{
			//	Download 시간 보내주기
			Message msgDownloadTimeZOne = new Message();
			CacheObject obj = CacheObject.GetInstance;
			try
			{
				if (pid.Equals("ALL"))
				{

					XmlNode gnode = obj.FindGroupNode(gid);
					XmlNodeList players = gnode.SelectNodes("descendant::player[attribute::del_yn=\"N\"]");

					foreach (XmlNode player in players)
					{
						string playerid = player.Attributes["pid"].Value;

						XmlDocument doc = new XmlDocument();
						XmlNode main = doc.CreateNode(XmlNodeType.XmlDeclaration, "", "");
						doc.AppendChild(main);
						main = doc.CreateNode(XmlNodeType.Element, "timezones", "");
						doc.AppendChild(main);

						using (DigitalSignage.ServerDatabase.ServerDataSetTableAdapters.DownloadTime_Players_ASNTableAdapter tda = cfg.serverDBLayer.CreateDTPlayersAsnDA())
						{
							using (ServerDataSet.DownloadTime_Players_ASNDataTable tdt = tda.GetDataByPID(playerid))
							{
								foreach (ServerDataSet.DownloadTime_Players_ASNRow row in tdt.Rows)
								{
									XmlNode node = doc.CreateNode(XmlNodeType.Element, "timezone", "");
									XmlNode nodeData = doc.CreateTextNode(row.starttime + "-" + row.endtime);
									main.AppendChild(node);
									node.AppendChild(nodeData);
								}

								if (tdt.Rows.Count == 0)
								{
									XmlNode groupnode = player.ParentNode;

									using (DigitalSignage.ServerDatabase.ServerDataSetTableAdapters.DownloadTime_Groups_ASNTableAdapter tgda = cfg.serverDBLayer.CreateDTGroupsAsnDA())
									{
										while (groupnode != null && groupnode.LocalName.Equals("group"))
										{
											string groupid = groupnode.Attributes["gid"].Value;

											using (ServerDataSet.DownloadTime_Groups_ASNDataTable tdt2 = tgda.GetDataByGID(groupid))
											{
												foreach (ServerDataSet.DownloadTime_Groups_ASNRow row in tdt2.Rows)
												{
													XmlNode node = doc.CreateNode(XmlNodeType.Element, "timezone", "");
													XmlNode nodeData = doc.CreateTextNode(row.starttime + "-" + row.endtime);
													main.AppendChild(node);
													node.AppendChild(nodeData);
												}

												if (tdt2.Rows.Count > 0)
												{
													tdt2.Dispose();
													break;
												}
												else
													tdt2.Dispose();
											}
											groupnode = groupnode.ParentNode;
										}
										tgda.Dispose();
									}
								}
								tdt.Dispose();
							}
							tda.Dispose();
						}

						Topic recv = new Topic();
						recv.Groupid = gid;
						recv.Mediaid = playerid;
						recv.Level = UserLevels.Media;

						logger.Info("PlayerID(" + playerid + ") : " + doc.InnerXml);
						msgDownloadTimeZOne.Result = iVisionCode.CONF_DOWNLOAD_TIME_INFO;
						msgDownloadTimeZOne.Content = doc.InnerXml;
						msgDownloadTimeZOne.Sender = recv;
						msgDownloadTimeZOne.MsgType = MessageType.msg_Login;
						msgDownloadTimeZOne.Level = UserLevels.None;
						msgDownloadTimeZOne.Time = DateTime.Now;
						msgDownloadTimeZOne.Version = "";

						SendMessage(msgDownloadTimeZOne);
					}
				}
				else
				{
					XmlDocument doc = new XmlDocument();
					XmlNode main = doc.CreateNode(XmlNodeType.XmlDeclaration, "", "");
					doc.AppendChild(main);
					main = doc.CreateNode(XmlNodeType.Element, "timezones", "");
					doc.AppendChild(main);

					using (DigitalSignage.ServerDatabase.ServerDataSetTableAdapters.DownloadTime_Players_ASNTableAdapter tda = cfg.serverDBLayer.CreateDTPlayersAsnDA())
					{
						using (ServerDataSet.DownloadTime_Players_ASNDataTable tdt = tda.GetDataByPID(pid))
						{
							foreach (ServerDataSet.DownloadTime_Players_ASNRow row in tdt.Rows)
							{
								XmlNode node = doc.CreateNode(XmlNodeType.Element, "timezone", "");
								XmlNode nodeData = doc.CreateTextNode(row.starttime + "-" + row.endtime);
								main.AppendChild(node);
								node.AppendChild(nodeData);
							}

							if (tdt.Rows.Count == 0)
							{
								XmlNode player = obj.FindPlayerNode(pid);
								XmlNode groupnode = player.ParentNode;

								using (DigitalSignage.ServerDatabase.ServerDataSetTableAdapters.DownloadTime_Groups_ASNTableAdapter tgda = cfg.serverDBLayer.CreateDTGroupsAsnDA())
								{
	
									while (groupnode != null && groupnode.LocalName.Equals("group"))
									{
										string groupid = groupnode.Attributes["gid"].Value;

										using (ServerDataSet.DownloadTime_Groups_ASNDataTable tdt2 = tgda.GetDataByGID(groupid))
										{
											foreach (ServerDataSet.DownloadTime_Groups_ASNRow row in tdt2.Rows)
											{
												XmlNode node = doc.CreateNode(XmlNodeType.Element, "timezone", "");
												XmlNode nodeData = doc.CreateTextNode(row.starttime + "-" + row.endtime);
												main.AppendChild(node);
												node.AppendChild(nodeData);
											}
											if (tdt2.Rows.Count > 0)
											{
												tdt2.Dispose();
												break;
											}
											else
												tdt2.Dispose();
										}
										groupnode = groupnode.ParentNode;
									}

									tgda.Dispose();
								}
							}
							tdt.Dispose();
						}

						tda.Dispose();
					}

					Topic recv = new Topic();
					recv.Groupid = gid;
					recv.Mediaid = pid;
					recv.Level = UserLevels.Media;

					logger.Info("PlayerID(" + pid + ") : " + doc.InnerXml);
					msgDownloadTimeZOne.Result = iVisionCode.CONF_DOWNLOAD_TIME_INFO;
					msgDownloadTimeZOne.Content = doc.InnerXml;
					msgDownloadTimeZOne.Sender = recv;
					msgDownloadTimeZOne.MsgType = MessageType.msg_Login;
					msgDownloadTimeZOne.Level = UserLevels.None;
					msgDownloadTimeZOne.Time = DateTime.Now;
					msgDownloadTimeZOne.Version = "";

					SendMessage(msgDownloadTimeZOne);
				}
			}
			catch(Exception ex)
			{
				logger.Error(ex.ToString());
			}
		}

        public void SendMessage(Message msg)
        {
			try
			{
				#region From Player to Server
				//상태 정보 DB에 저장
                if (msg.MsgType == MessageType.msg_Status)
                {
					ClientManager manager = ClientManager.GetInstance;
					if(!manager.UpdatePlayerInformation(msg.Sender.Mediaid, msg.Content))
					{
						//	제거된 항목이므로
						msg.MsgType = MessageType.msg_Error;
						msg.Level = UserLevels.Media;
						msg.Result = iVisionCode.CONN_ERROR_DISCONNECTED;
						this.CurrentCallback.ReceiveWhisper(msg, msg.Sender);
					}

                    return;
                }
				else if (msg.MsgType == MessageType.msg_PlayerInfo)
				{
					//	플레이어의 상세 정보 
					//	1. 분석
					//	2. 생성 또는 갱신
					//	3. 완료
					try
					{
						String[] arrPlayerDetailInfos = msg.Content.Split('|');
						if (cfg.localPlayersList.InsertOrUpdatePlayerDetailInfo(msg.Sender.Mediaid, arrPlayerDetailInfos[0], arrPlayerDetailInfos[1], arrPlayerDetailInfos[2], arrPlayerDetailInfos[3], arrPlayerDetailInfos[4], Convert.ToInt32(arrPlayerDetailInfos[5])))
						{
							ClientManager manager = ClientManager.GetInstance;
							manager.UpdatePlayerDetailInformation(msg.Sender.Mediaid, arrPlayerDetailInfos[0], arrPlayerDetailInfos[1], arrPlayerDetailInfos[2], arrPlayerDetailInfos[3], arrPlayerDetailInfos[4], arrPlayerDetailInfos[5]);
							
						}

 					}
					catch (Exception ex)
					{
						logger.Error(ex.ToString());
					}
					return;

				}
                else if (msg.MsgType == MessageType.msg_ControlResult)  //  제어 결과 적용
                {
                    try
                    {
                        logger.Debug("msg_ControlResult: " + msg.Content);
                        String[] arrResponseInfos = msg.Content.Split('|');
                        int nID = Convert.ToInt32(arrResponseInfos[0]);
                        ControlResponse resp = (ControlResponse)Convert.ToInt32(arrResponseInfos[1]);

                        ControlService.GetInstance.SetControlResponse(nID, resp);

                    }
                    catch (Exception ex) { logger.Error("msg_ControlResult: " + ex.Message); }

                    return;
                }

				#endregion
                logger.Info("SENDING GID={0}, PID={1}", msg.Sender.Groupid, msg.Sender.Mediaid);

				//모든 클라이언트에 전송
                if (msg.Sender.Groupid.Equals("ALL") && msg.Sender.Mediaid.Equals("ALL"))
                {
                    foreach (INetworkCallback callback in clients.Values)
                    {
                        callback.Receive(msg);
                    }
                }// 그룹 전체 클라이언트에 메시지 전송
                else if (msg.Sender.Mediaid.Equals("ALL"))
                {
                    foreach (Topic rec in clients.Keys)
                    {
                        if (rec.Groupid == msg.Sender.Groupid)
                        {
                            INetworkCallback callback = clients[rec];
                            callback.ReceiveWhisper(msg, rec);
                        }
                    }
                }// 특정 미디어에 메시지 전송
                else if (!msg.Sender.Groupid.Equals("ALL"))
                {
                    Whisper(msg, msg.Sender);
                }

            }
            catch (Exception ex)
            {
                logger.Error("IVisionService, SendMessage: " + ex);
            }
        }

        void cs_GetDataUsingDataContractCompleted(object sender, GetDataUsingDataContractCompletedEventArgs e)
        {
            try
            {
                if (e.Error != null)
                {
                    Console.WriteLine(e.Error.Message);
                }
                else
                {
                    if (!e.Result.SUCCESS_FLAG) return;

                    try
                    {
                        if (String.IsNullOrEmpty(e.Result.ID) ||
                            String.IsNullOrEmpty(e.Result.GS_DATA))
                        {
                            cfg.WriteTargetAdError(cfg.CurrentRelayID, TargetAd_ErrorType.TargetAd_AppServer_ToLower, (int)AdTarget_AppServer_ErrorCodes.ErrorParsing);
                            return;
                        }
                        Message msg = new Message();
                        Topic recv = new Topic();
                        recv.Groupid = "";
                        recv.Mediaid = e.Result.MED_ID;
                        recv.Level = UserLevels.Media;
                        logger.Info("msg_ResponseGSRelay : RelayID(" + e.Result.ID + ") PlayerID(" + recv.Mediaid + ") Data(" + e.Result.GS_DATA + ")");
                        msg.Result = iVisionCode.GS_RESPONSE;
                        msg.Content = String.Format("{0}|{1}", e.Result.ID, e.Result.GS_DATA);
                        msg.Sender = recv;
                        msg.MsgType = MessageType.msg_ResponseGSRelay;
                        msg.Level = UserLevels.None;
                        msg.Time = DateTime.Now;
                        msg.Version = "";

                        SendMessage(msg);

                        cfg.WriteTargetAdSuccess(cfg.CurrentRelayID, TargetAd_ErrorType.TargetAd_AppServer_ToLower);

                    }
                    catch (Exception exReturn) 
                    {
                        cfg.WriteTargetAdError(cfg.CurrentRelayID, TargetAd_ErrorType.TargetAd_AppServer_ToLower, (int)AdTarget_AppServer_ErrorCodes.ErrorDisconnectedToClient);
                        logger.Error(exReturn.ToString());
                    }
                }

                GSRelayServiceClient cs = sender as GSRelayServiceClient;

                if (cs != null) cs.Close();
            }
            catch (Exception ex)
            {
                logger.Error("msg_RequestGSRelay: Response Error - " + ex.Message);
                cfg.WriteTargetAdError(cfg.CurrentRelayID, TargetAd_ErrorType.TargetAd_AppServer_ToLower, (int)AdTarget_AppServer_ErrorCodes.ErrorDisconnectedToRelayServer);

            }
        }

        //특정 미디어에 메시지 전송
        public void Whisper(Message msg, Topic receiver)
        {
            try
            {
				ClientManager manager = ClientManager.GetInstance;

				INetworkCallback callback = manager.GetCallback(receiver.Mediaid);
				callback.ReceiveWhisper(msg, receiver);

            }
            catch (Exception ex)
            {
                logger.Error("IVisionService, Whisper: " + ex);
            }
        }


        public bool SendFile(FileMessage fileMsg, Topic receiver)
        {
            try
            {
                foreach (Topic rcvr in clients.Keys)
                {
                    if (/*receiver.Groupid == rcvr.Groupid && */receiver.Mediaid == rcvr.Mediaid && receiver.Level == rcvr.Level)
                    {
                        INetworkCallback rcvrCallback = clients[rcvr];
                        rcvrCallback.ReceiverFile(fileMsg, receiver);
						break;
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("IVisionService, SendFile: " + ex);
            }

            return false;
        }

        public void Disconnect(Topic client)
        {
            try
            {
				ClientManager manager = ClientManager.GetInstance;
				DateTime? dtConnected = manager.GetConnectedDateTimeFromMember(client);
	
				manager.SetPlayerStatus(client.Mediaid, false);

				manager.RemoveMember(client.Mediaid);

				long currTicks = TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime());
				if (dtConnected.HasValue)
					cfg.PlayerDisconnected(client.Mediaid, TimeConverter.ConvertToUTP(dtConnected.Value.ToUniversalTime()), currTicks, (int)ErrorCodes.IS_ERROR_CONN_DISCONNECTED);
				else
                    cfg.PlayerDisconnected(client.Mediaid, currTicks, currTicks, (int)ErrorCodes.IS_ERROR_CONN_DISCONNECTED);
				//	접속 로그 기록

				logger.Info(String.Format("Disconnected : PID({0}) Alive Connection({1})", client.Mediaid, manager.ClientCount));
            }
            catch (Exception ex)
            {
                logger.Error("IVisionService, Disconnect: " + ex);
            }

        }
		private void UpdateMember(Topic old_topic, Topic new_topic)
		{
            lock (syncObj)
            {
				try
                {
                    int index = 0;
					foreach (Topic tp in clients.Keys)
					{
						if (tp.Mediaid == old_topic.Mediaid)
						{
							tp.Mediaid = clientList[index].Mediaid = new_topic.Mediaid;
							tp.Groupid = clientList[index].Groupid = new_topic.Groupid;
							tp.Level = clientList[index].Level = new_topic.Level;

							break;
						}
						index++;
					}

					logger.Info(string.Format("UpdateMember {0} : {1}", new_topic.Groupid, new_topic.Mediaid));
				}
                catch (Exception ex)
                {
					logger.Error("IVisionService, UpdateMember: " + ex);
                }

            }
   		}
        #endregion
	}
	#endregion
}
