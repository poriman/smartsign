﻿using System;
using System.Data.Common;
//using System.Data;

using NLog;

using DigitalSignage.Common;
using System.Data.Odbc;
using System.Xml;

namespace DigitalSignage.SchedulerService
{
	class GroupList : MarshalByRefObject, IGroupList
	{
		private static Logger logger = LogManager.GetCurrentClassLogger();

		public static void WriteEventLog(OdbcException e)
		{
			string errorMessages = "";

			for (int i = 0; i < e.Errors.Count; i++)
			{
				errorMessages += "Index #" + i + "\n" +
								 "Message: " + e.Errors[i].Message + "\n" +
								 "NativeError: " + e.Errors[i].NativeError.ToString() + "\n" +
								 "Source: " + e.Errors[i].Source + "\n" +
								 "SQL: " + e.Errors[i].SQLState + "\n";
			}

			using (System.Diagnostics.EventLog log = new System.Diagnostics.EventLog())
			{
				log.Source = "SmartSign Service";
				log.WriteEntry(errorMessages);
				log.Dispose();
			}
			logger.Error(errorMessages);

		}
		public string GetGroupsXmlByPID(string pid)
		{
			try
			{
				XmlNode node = CacheObject.GetInstance.FindPlayerNode(pid);

				XmlNode parent_node = node;
				string grouplist = "<groups>";
				while ((parent_node = parent_node.ParentNode) != null)
				{
					try
					{
						grouplist += String.Format("<group gid=\"{0}\" source=\"{1}\" username=\"{2}\" password=\"{3}\" path=\"{4}\"/>",
							parent_node.Attributes["gid"].Value, parent_node.Attributes["source"].Value, parent_node.Attributes["username"].Value,
							parent_node.Attributes["password"].Value, parent_node.Attributes["path"].Value);
					}
					catch { }
				}

				grouplist += "</groups>";

				return grouplist;
			}
			catch (Exception ex)
			{
				logger.Error(ex.ToString());
			}
			return null;
		}

		public string GetGroupListByPID(string pid)
		{
			try
			{
				XmlNode node = CacheObject.GetInstance.FindPlayerNode(pid);

				XmlNode parent_node = node;
				string grouplist = "";
				while ((parent_node = parent_node.ParentNode) != null)
				{
					try
					{
						grouplist += (parent_node.Attributes["gid"].Value + "|");
					}
					catch
					{
						grouplist = grouplist.TrimEnd('|');
					}
				}

				return grouplist;
			}
			catch (Exception ex)
			{
				logger.Error(ex.ToString());
			}
			return null;
		}

		public string GetGroupListByGID(string gid)
		{
			try
			{
				XmlNode node = CacheObject.GetInstance.FindGroupNode(gid);

				XmlNode parent_node = node;
				string grouplist = "";
				do
				{
					try
					{
						grouplist += (parent_node.Attributes["gid"].Value + "|");
					}
					catch
					{
						grouplist = grouplist.TrimEnd('|');
					}
				} while ((parent_node = parent_node.ParentNode) != null);

				return grouplist;
			}
			catch (Exception ex)
			{
				logger.Error(ex.ToString());
			}
			return null;
		}

		public string GetGroupInfo(string gid, string attribute)
		{
			try
			{
				XmlNode node = CacheObject.GetInstance.FindGroupNode(gid);

				return node.Attributes[attribute].Value;
			}
			catch (Exception ex)
			{
				logger.Error(ex.ToString());
			}
			return null;
		}
	}
}
