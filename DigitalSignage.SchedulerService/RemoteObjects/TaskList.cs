﻿using System;
using System.Collections;

//SQLite & DB routines
using DigitalSignage.ServerDatabase;
using DigitalSignage.Common;

using NLog;
using System.Xml;
using System.Data.Odbc;
using System.Collections.ObjectModel;
using System.Collections.Generic;

namespace DigitalSignage.SchedulerService
{
    // класс для работы с очередью заданий
    public class TaskList : MarshalByRefObject, ITaskList
    {
        private Config cfg = null;
        private static Logger logger = LogManager.GetCurrentClassLogger();

		static long updated_timestamp = TimeConverter.ConvertToUTP(DateTime.UtcNow);
		static readonly object lockObj = new object();
		
        public TaskList()
        {
            cfg = Config.GetConfig; //protection from initialization dead loop
        }

		public long GetTasksTimestamp()
		{
			lock(lockObj)
			{
				return updated_timestamp; 
			}
		}

        // add new task to list
        public int AddTask(Task newTask)
        {
			try
			{
				
				DateTime lUtc = DateTime.UtcNow;

				using (DigitalSignage.ServerDatabase.ServerDataSetTableAdapters.tasksTableAdapter da = cfg.serverDBLayer.CreateTaskDA())
				{

					da.AddTask(newTask.pid, newTask.gid, (int)newTask.type, newTask.uuid.ToString(),
						TimeConverter.ConvertToUTP(lUtc), newTask.TaskStart, newTask.TaskEnd, newTask.guuid.ToString(),
						newTask.duuid.ToString(), newTask.description, (int)newTask.duration, newTask.daysofweek, newTask.priority, newTask.endtimeofday, newTask.starttimeofday, newTask.isAd ? "Y" : "N", newTask.MetaTags, newTask.show_flag);
					da.Dispose();
				}

                lock (lockObj)
                {
					updated_timestamp = TimeConverter.ConvertToUTP(lUtc);
				}
				return 0;
			}
			catch (InvalidOperationException ioe)
			{
				logger.Error(ioe + "");
				cfg.ReInitializeConnectionPool();
			}
			catch (Exception e)
			{
				logger.Error(e + "");
			}
            return -1;
        }

        /// <summary>
        /// Sync 스케줄을 등록한다.
        /// </summary>
        /// <param name="task">스케줄에 관련한 클래스</param>
        /// <param name="synclist">동기화를 맞출 목록</param>
        /// <returns>실패: -1</returns>
        public int AddSyncTask(Task task, DigitalSignage.Common.SYNC_TASK[] synclist)
        {
            try
            {
                DateTime dtUtcNow = DateTime.UtcNow;
                long lUtcTime = TimeConverter.ConvertToUTP(dtUtcNow);
                string utpTicks = TimeConverter.ConvertYYYYMMDDH24mmSSFromUTP(lUtcTime);
                
                try
                {
                    String sQuery = String.Empty;

                    sQuery = "INSERT INTO tasks (uuid, pid, gid, type, state, etime, starttime, endtime, guuid, duuid, name, duration, daysofweek, priority, endtimeofday, starttimeofday, is_ad, meta_tag, show_flag) VALUES  (?, ?, ?, ?, 0, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

                    using (OdbcConnection conn = cfg.serverDBLayer.GetAvailableConnection())
                    {
                        using (OdbcTransaction tr = conn.BeginTransaction())
                        {
                            try
                            {
                                foreach (DigitalSignage.Common.SYNC_TASK sync in synclist)
                                {
                                    using (OdbcCommand sql = new OdbcCommand(sQuery, conn, tr))
                                    {
                                        #region 파라미터 값 생성

                                        string uuid = Guid.NewGuid().ToString();
                                        logger.Debug("uuid = {0}", uuid);

                                        sql.Parameters.Add(new OdbcParameter("uuid", OdbcType.VarChar, 36, "uuid")
                                        {
                                            Value = Guid.NewGuid().ToString(),
                                            DbType = System.Data.DbType.AnsiString,
                                        });

                                        sql.Parameters.Add(new OdbcParameter("pid", OdbcType.VarChar, 13, "pid")
                                        {
                                            Value = sync.PlayerID,
                                            IsNullable = true,
                                            DbType = System.Data.DbType.AnsiString
                                        });

                                        sql.Parameters.Add(new OdbcParameter("gid", OdbcType.VarChar, 13, "gid")
                                        {
                                            Value = sync.GroupID,
                                            IsNullable = false,
                                            DbType = System.Data.DbType.AnsiString
                                        });

                                        sql.Parameters.Add(new OdbcParameter("type", OdbcType.Int, 4, "type")
                                        {
                                            Value = (int)task.type,
                                            IsNullable = false,
                                            DbType = System.Data.DbType.Int32
                                        });

                                        sql.Parameters.Add(new OdbcParameter("etime", OdbcType.BigInt, 19, "etime")
                                        {
                                            Value = lUtcTime,
                                            IsNullable = true,
                                            DbType = System.Data.DbType.Int64
                                        });

                                        sql.Parameters.Add(new OdbcParameter("starttime", OdbcType.BigInt, 19, "starttime")
                                        {
                                            Value = task.TaskStart,
                                            IsNullable = true,
                                            DbType = System.Data.DbType.Int64
                                        });

                                        sql.Parameters.Add(new OdbcParameter("endtime", OdbcType.BigInt, 19, "endtime")
                                        {
                                            Value = task.TaskEnd,
                                            IsNullable = true,
                                            DbType = System.Data.DbType.Int64
                                        });


                                        sql.Parameters.Add(new OdbcParameter("guuid", OdbcType.VarChar, 36, "guuid")
                                        {
                                            Value = sync.GUUID,
                                            IsNullable = true,
                                            DbType = System.Data.DbType.AnsiString
                                        });

                                        sql.Parameters.Add(new OdbcParameter("duuid", OdbcType.VarChar, 36, "duuid")
                                        {
                                            Value = sync.DUUID,
                                            IsNullable = true,
                                            DbType = System.Data.DbType.AnsiString
                                        });

                                        sql.Parameters.Add(new OdbcParameter("name", OdbcType.VarChar, 255, "name")
                                        {
                                            Value = task.description,
                                            IsNullable = false,
                                            DbType = System.Data.DbType.AnsiString
                                        });


                                        sql.Parameters.Add(new OdbcParameter("duration", OdbcType.Int, 4, "duration")
                                        {
                                            Value = (int)task.duration,
                                            IsNullable = false,
                                            DbType = System.Data.DbType.Int32
                                        });

                                        sql.Parameters.Add(new OdbcParameter("daysofweek", OdbcType.Int, 4, "daysofweek")
                                        {
                                            Value = task.daysofweek,
                                            IsNullable = true,
                                            DbType = System.Data.DbType.Int32
                                        });

                                        sql.Parameters.Add(new OdbcParameter("priority", OdbcType.Int, 4, "priority")
                                        {
                                            Value = (int)task.priority,
                                            IsNullable = true,
                                            DbType = System.Data.DbType.Int32
                                        });

                                        sql.Parameters.Add(new OdbcParameter("endtimeofday", OdbcType.Int, 4, "endtimeofday")
                                        {
                                            Value = task.endtimeofday,
                                            IsNullable = true,
                                            DbType = System.Data.DbType.Int32
                                        });

                                        sql.Parameters.Add(new OdbcParameter("starttimeofday", OdbcType.Int, 4, "starttimeofday")
                                        {
                                            Value = task.starttimeofday,
                                            IsNullable = true,
                                            DbType = System.Data.DbType.Int32
                                        });

                                        sql.Parameters.Add(new OdbcParameter("is_ad", OdbcType.VarChar, 255, "is_ad")
                                        {
                                            Value = task.isAd ? "Y" : "N",
                                            IsNullable = true,
                                            DbType = System.Data.DbType.AnsiString
                                        });
                                        sql.Parameters.Add(new OdbcParameter("meta_tag", OdbcType.VarChar, 255, "meta_tag")
                                        {
                                            Value = task.MetaTags,
                                            IsNullable = true,
                                            DbType = System.Data.DbType.AnsiString
                                        });

                                        int showFlag = task.show_flag & ~((int)ShowFlag.Show_SyncMaster | (int)ShowFlag.Show_SyncSlave);

                                        sql.Parameters.Add(new OdbcParameter("show_flag", OdbcType.Int, 4, "show_flag")
                                        {
                                            Value = sync.MasterYN ? (showFlag | (int)ShowFlag.Show_SyncMaster) : (showFlag | (int)ShowFlag.Show_SyncSlave),
                                            IsNullable = true,
                                            DbType = System.Data.DbType.Int32
                                        });
                                        #endregion

                                        sql.ExecuteNonQuery();
                                        sql.Dispose();
                                    }

                                    String sSyncQuery = String.Format("INSERT INTO SYNC_TASKS (GUUID, PLAYERS_PID, SYNC_TP, EDIT_DT, REG_DT, DEL_YN) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', 'N')", sync.GUUID, sync.PlayerID, sync.MasterYN ? "M" : "S", utpTicks, utpTicks);
                                    using (OdbcCommand sql = new OdbcCommand(sSyncQuery, conn, tr))
                                    {
                                        sql.ExecuteNonQuery();
                                        sql.Dispose();
                                    }
                                }

                                tr.Commit();
                            }
                            catch (Exception exTR) 
                            { 
                                logger.Error(exTR);
                                tr.Rollback();

                                return -1;
                            }
                        }
                    }
                }
                catch (InvalidOperationException ioe)
                {
                    logger.Error(ioe + "");
                    cfg.ReInitializeConnectionPool();
                }
                catch (Exception e)
                {
                    logger.Error(e.Message);
                }

                foreach (DigitalSignage.Common.SYNC_TASK sync in synclist)
                {
                    SendUpdateService.GetInstance.AddSession(sync.GroupID, sync.PlayerID);
                }

                lock (lockObj)
                {
                    updated_timestamp = lUtcTime;
                }
                return 0;
            }
            catch (InvalidOperationException ioe)
            {
                logger.Error(ioe + "");
                cfg.ReInitializeConnectionPool();
            }
            catch (Exception e)
            {
                logger.Error(e + "");
            }
            return -1;
        }

        /// <summary>
        /// guuid와 같은 스케줄을 모두 불러온다.
        /// </summary>
        /// <param name="guuid"></param>
        /// <returns></returns>
        public SYNC_TASK[] GetSyncTask(string guuid)
        {
            try
            {

                String sQuery = String.Empty;

                sQuery = String.Format("SELECT pid, gid, type, state, uuid, etime, starttime, endtime, guuid, duuid, name, duration, daysofweek, priority, endtimeofday, starttimeofday, is_ad, meta_tag, show_flag FROM tasks WHERE guuid = '{0}' ORDER BY show_flag", guuid);

                List<SYNC_TASK> arrTasks = new List<SYNC_TASK>();

                using (ServerDataSet.tasksDataTable dt = new ServerDataSet.tasksDataTable())
                {
                    using (OdbcConnection conn = cfg.serverDBLayer.GetAvailableConnection())
                    {
                        using (OdbcCommand sql = new OdbcCommand(sQuery, conn))
                        {
                            //initializing database structure
                            using (OdbcDataReader dr = sql.ExecuteReader())
                            {
                                dt.Load(dr);

                                dr.Close();
                                dr.Dispose();
                            }

                            sql.Dispose();

                        }
                    }

                    foreach (ServerDataSet.tasksRow row in dt.Rows)
                    {
                        arrTasks.Add(new SYNC_TASK()
                        {
                            DUUID = row.duuid,
                            GUUID = row.guuid,
                            UUID = row.uuid,
                            MasterYN = (row.show_flag & (int)ShowFlag.Show_SyncMaster) > 0,
                            PlayerID = row.pid,
                            GroupID = row.gid,
                            EditDT = TimeConverter.ConvertYYYYMMDDH24mmSSFromUTP(row.etime),
                            RegDT = TimeConverter.ConvertYYYYMMDDH24mmSSFromUTP(row.etime),
                            //
                        });
                    }

                }


                return arrTasks.ToArray();
            }
            catch (InvalidOperationException ioe)
            {
                logger.Error(ioe + "");
                cfg.ReInitializeConnectionPool();
            }
            catch (Exception e)
            {
                logger.Error(e + "");
            }

            return null;
        }

        /// <summary>
        /// 이름과 타입 조건을 통해 스케줄 가져오기
        /// </summary>
        /// <param name="timestamp"></param>
        /// <param name="type"></param>
        /// <param name="name"></param>
        /// <returns></returns>
		public Task[] GetTasksByNameAndType(long timestamp, int type, string name)
		{
			try
			{
				System.Collections.Generic.List<Task> list = new System.Collections.Generic.List<Task>();

				if (timestamp == 0)
				{
					// 현재 시간에 걸려있는 스케줄을 모두 조회한다.
					timestamp = TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime());

					using (DigitalSignage.ServerDatabase.ServerDataSetTableAdapters.tasksTableAdapter da = cfg.serverDBLayer.CreateTaskDA())
					{
						logger.Info(String.Format("{0} - {1} {2}", timestamp, type, name));

						using (ServerDataSet.tasksDataTable tdt = da.GetTasksByNameAndType(timestamp, timestamp, type, name))
						{
							da.Dispose();
							logger.Info(String.Format("count {0}", tdt.Rows.Count));

							for (int i = 0; i < tdt.Rows.Count; i++)
							{
								Task task = new Task((ServerDataSet.tasksRow)tdt.Rows[i]);
								list.Add(task);
							}
							tdt.Dispose();

							return list.ToArray();
						}
					}
				}
				else
				{
					//	Timestamp 이후에 등록된 스케줄을 가져온다.
					using (DigitalSignage.ServerDatabase.ServerDataSetTableAdapters.tasksTableAdapter da = cfg.serverDBLayer.CreateTaskDA())
					{
						using (ServerDataSet.tasksDataTable tdt = da.GetNewTasksByNameAndType(timestamp, type, name))
						{
							da.Dispose();
							for (int i = 0; i < tdt.Rows.Count; i++)
							{
								Task task = new Task((ServerDataSet.tasksRow)tdt.Rows[i]);
								list.Add(task);
							}
							tdt.Dispose();

							return list.ToArray();
						}
					}
				}


			}
			catch (InvalidOperationException ioe)
			{
				logger.Error(ioe + "");
				cfg.ReInitializeConnectionPool();
			}
			catch (Exception e)
			{
				logger.Error(e + "");
			}
			return null;
		}

        // method for getting a tasks changed since "timestamp"
		public ServerDataSet.tasksDataTable GetNewTasks(long timestamp, string pid, string gid)
        {
			try
			{
				using (DigitalSignage.ServerDatabase.ServerDataSetTableAdapters.tasksTableAdapter da = cfg.serverDBLayer.CreateTaskDA())
				{
					string[] groups = gid.Split('|');
					using (ServerDataSet.tasksDataTable res = new ServerDataSet.tasksDataTable())
					{
						for (int i = 0; i < groups.Length; ++i)
						{
							using (ServerDataSet.tasksDataTable piece = da.GetNewTasks(timestamp, pid, timestamp, groups[i]))
							{
								// 								logger.Info(String.Format("{0} {1} {2}", groups[i], piece.Count, res.Count));
								res.Merge(piece);
								piece.Dispose();
							}
						}
						da.Dispose();
						return res;
					}
				}
			}
			catch (InvalidOperationException ioe)
			{
				logger.Error(ioe + "");
				cfg.ReInitializeConnectionPool();
			}
			catch (Exception e)
			{
				logger.Error(e + "");
			}
			return null;
        }

		public ServerDataSet.tasksDataTable GetRecursiveTasksFrom(string PIDorGID, bool isPlayer, long from, long till)
		{
			try
			{
				CacheObject obj = CacheObject.GetInstance;
				XmlNode selected_node =
					isPlayer ? (obj.FindPlayerNode(PIDorGID)) : (obj.FindGroupNode(PIDorGID));

				if(selected_node != null)
				{
					ServerDataSet.tasksDataTable tdt = new ServerDataSet.tasksDataTable();

					using (DigitalSignage.ServerDatabase.ServerDataSetTableAdapters.tasksTableAdapter da = cfg.serverDBLayer.CreateTaskDA())
					{
						do 
						{
							if (selected_node.LocalName.Equals("player"))
							{
								string pid = selected_node.Attributes["pid"].Value;

								
							}
							else if (selected_node.LocalName.Equals("group"))
							{
								string gid = selected_node.Attributes["pid"].Value;

							}
							else
								break;
						} while ((selected_node = selected_node.ParentNode) != null);
						
					}
				}


			}
			catch (InvalidOperationException ioe)
			{
				logger.Error(ioe + "");
				cfg.ReInitializeConnectionPool();
			}
			catch (Exception e)
			{
				logger.Error(e + "");
			}
			return null;

		}

        /// <summary>
        /// 지정한 시간 이후에 등록된 해당 플레이어의 동기화 스케줄 목록을 가져온다.
        /// </summary>
        /// <param name="timestamp">기준이되는 시간정보</param>
        /// <param name="pid">플레이어 아이디</param>
        /// <returns>싱크 스케줄 정보</returns>
        public ServerDataSet.SYNC_TASKSDataTable GetNewSyncTasks(string timestamp, string pid)
        {
            try
            {
                String sQuery = String.Empty;

                sQuery = String.Format("SELECT A.GUUID, A.PLAYERS_PID, A.SYNC_TP, A.EDIT_DT, A.REG_DT, A.DEL_YN, B._ipv4addr AS IP_ADDR FROM SYNC_TASKS A, PLAYER_DETAILINFO B WHERE A.PLAYERS_PID = B.pid AND A.PLAYERS_PID <> '{0}' AND A.GUUID IN (SELECT GUUID FROM SYNC_TASKS WHERE PLAYERS_PID = '{0}' AND EDIT_DT > '{1}' AND SYNC_TP = 'M') ORDER BY GUUID", pid, timestamp);

                List<SYNC_TASK> arrTasks = new List<SYNC_TASK>();

                using (ServerDataSet.SYNC_TASKSDataTable dt = new ServerDataSet.SYNC_TASKSDataTable())
                {
                    using (OdbcConnection conn = cfg.serverDBLayer.GetAvailableConnection())
                    {
                        using (OdbcCommand sql = new OdbcCommand(sQuery, conn))
                        {
                            //initializing database structure
                            using (OdbcDataReader dr = sql.ExecuteReader())
                            {
                                dt.Load(dr);

                                dr.Close();
                                dr.Dispose();
                            }

                            sql.Dispose();

                        }
                    }

                    return dt;
                }
            }
            catch (InvalidOperationException ioe)
            {
                logger.Error(ioe + "");
                cfg.ReInitializeConnectionPool();
            }
            catch (Exception e)
            {
                logger.Error(e + "");
            }

            return null;
        }
        public int DeleteTask(Task task)
        {
			try
            {
				int res = 0;

				using (DigitalSignage.ServerDatabase.ServerDataSetTableAdapters.tasksTableAdapter da = cfg.serverDBLayer.CreateTaskDA())
				{
					logger.Info("Trying to delete tasks group = " + task.guuid);

					DateTime lUtc = DateTime.UtcNow;

					res = da.RemoveTask(TimeConverter.ConvertToUTP(lUtc), task.guuid.ToString());
                    
                    lock (lockObj)
                    {
                        updated_timestamp = TimeConverter.ConvertToUTP(lUtc);
					}

					using (DigitalSignage.ServerDatabase.ServerDataSetTableAdapters.DownloadedScheduleInfoTableAdapter dta = cfg.serverDBLayer.CreateDownloadedSchedInfoDA())
					{
						logger.Info("Trying to delete downloadmaster by uuid = " + task.uuid);
						dta.DeleteDownloadedInfoByUuid(task.uuid.ToString());
						dta.Dispose();
					}
					/*
					Nullable<int> nCount = da.GetCountOfAliveTasksInGroup(task.duuid.ToString(), task.gid);
					da.Dispose();

					if (nCount == null || (nCount.HasValue && nCount.Value == 0))
					{
						using (DigitalSignage.ServerDatabase.ServerDataSetTableAdapters.downloadmasterTableAdapter dta = cfg.serverDBLayer.CreateDownloadMasterDA())
						{
							logger.Info("Trying to delete downloadmaster by duuid = " + task.duuid);
							dta.DeleteByDuuid(task.duuid.ToString(), task.gid);
							dta.Dispose();
						}
					}
					 * */
				}
				return res;
            }
			catch (InvalidOperationException ioe)
			{
				logger.Error(ioe + "");
				cfg.ReInitializeConnectionPool();
			}
			catch (Exception e)
            {
                logger.Error(e + "");
            }

			return -1;
        }

        public ServerDataSet.tasksDataTable GetTasksFrom(long from, long till)
        {
			try
            {
				DateTime dt = DateTime.Now;

                using (DigitalSignage.ServerDatabase.ServerDataSetTableAdapters.tasksTableAdapter da = cfg.serverDBLayer.CreateTaskDA())
				{
					using(ServerDataSet.tasksDataTable res = da.GetTasksFrom(from, till, from, till, from, till))
					{
						da.Dispose();

						logger.Info((dt - DateTime.Now).TotalSeconds.ToString() + " - by SmartSign Manager.");
						return res;
					}
				}
			}
			catch (InvalidOperationException ioe)
			{
				logger.Error(ioe + "");
				cfg.ReInitializeConnectionPool();
			}
			catch (Exception e)
            {
                logger.Error(e + "");
            }
            return null;
        }

		public int GetTasksGroupCount(String guuid)
		{
			try
			{
				using (DigitalSignage.ServerDatabase.ServerDataSetTableAdapters.tasksTableAdapter da = cfg.serverDBLayer.CreateTaskDA())
				{
					using (ServerDataSet.tasksDataTable res = da.GetTasksGroup(guuid))
					{
						da.Dispose();

						return res.Count;
					}
				}
			}
			catch (InvalidOperationException ioe)
			{
				logger.Error(ioe + "");
				cfg.ReInitializeConnectionPool();
			}
			catch (Exception e)
			{
				logger.Error(e + "");
			}
			return 0;
		}

        public ServerDataSet.tasksDataTable GetTasksGroup(String guuid)
        {
			try
            {
				using (DigitalSignage.ServerDatabase.ServerDataSetTableAdapters.tasksTableAdapter da = cfg.serverDBLayer.CreateTaskDA())
				{
					using (ServerDataSet.tasksDataTable res = da.GetTasksGroup(guuid))
					{
						da.Dispose();

						return res;
					}
				}
			}
			catch (InvalidOperationException ioe)
			{
				logger.Error(ioe + "");
				cfg.ReInitializeConnectionPool();
			}
			catch (Exception e)
            {
                logger.Error(e + "");
            }
            return null;
        }

        public int EditTasks(Task oldTask, Task newTask)
        {
			int res = -1;
            try
            {
				DateTime lUtc = DateTime.UtcNow;

				using (DigitalSignage.ServerDatabase.ServerDataSetTableAdapters.tasksTableAdapter da = cfg.serverDBLayer.CreateTaskDA())
				{
					res = da.EditTasks((int)newTask.type, (int)newTask.state, newTask.TaskStart, newTask.TaskEnd, TimeConverter.ConvertToUTP(lUtc),
						newTask.description, newTask.duuid.ToString(), (int)newTask.duration, newTask.daysofweek, newTask.priority, newTask.endtimeofday, newTask.starttimeofday, newTask.show_flag, oldTask.guuid.ToString(), oldTask.duuid.ToString(), oldTask.TaskStart, oldTask.TaskEnd);
					da.Dispose();
				}
                lock (lockObj)
                {
					updated_timestamp = TimeConverter.ConvertToUTP(lUtc);
				}
			}
			catch (InvalidOperationException ioe)
			{
				logger.Error(ioe + "");
				cfg.ReInitializeConnectionPool();
			}
			catch (Exception e)
            {
                logger.Error(e + "");
            }
            return res;
        }

        /// <summary>
        /// 싱크 스케줄을 수정한다. 예전스케줄의 정보에서 uuid만 비교하며, newTask의 uuid를 뺀 나머지를 모두 수정한다.
        /// </summary>
        /// <param name="oldTask">예전 스케줄 정보</param>
        /// <param name="newTask">새로운 스케줄 정보</param>
        /// <param name="synclist">동기화를 맞출 목록</param>
        /// <returns></returns>
        public int EditSyncTasks(Task oldTask, Task newTask, DigitalSignage.Common.SYNC_TASK[] synclist)
        {
            try
            {
                DateTime dtUtcNow = DateTime.UtcNow;
                long lUtcTime = TimeConverter.ConvertToUTP(dtUtcNow);
                string utpTicks = TimeConverter.ConvertYYYYMMDDH24mmSSFromUTP(lUtcTime);
                
                try
                {
                    String sQuery = String.Empty;

                    sQuery = "UPDATE tasks SET type = ?, etime = ?, state = ?, starttime = ?, endtime = ?, starttimeofday = ?, endtimeofday = ?, guuid = ?, duuid = ?, name = ?, show_flag = ?, daysofweek = ?, priority = ?, duration = ? WHERE uuid = ?";

                    using (OdbcConnection conn = cfg.serverDBLayer.GetAvailableConnection())
                    {
                        using (OdbcTransaction tr = conn.BeginTransaction())
                        {
                            try
                            {
                                String sDelQuery = String.Format("UPDATE SYNC_TASKS SET DEL_YN = 'Y', EDIT_DT = '{0}' WHERE GUUID = '{1}'", utpTicks, oldTask.guuid);
                                using (OdbcCommand sql = new OdbcCommand(sDelQuery, conn, tr))
                                {
                                    logger.Info(sDelQuery);
                                    sql.ExecuteNonQuery();
                                    sql.Dispose();
                                }

                                sDelQuery = String.Format("UPDATE tasks SET state = 3, etime = {0} WHERE guuid = '{1}'", lUtcTime, oldTask.guuid);
                                using (OdbcCommand sql = new OdbcCommand(sDelQuery, conn, tr))
                                {
                                    logger.Info(sDelQuery);
                                    sql.ExecuteNonQuery();
                                    sql.Dispose();
                                }

                                foreach (DigitalSignage.Common.SYNC_TASK sync in synclist)
                                {
                                    using (OdbcCommand sql = new OdbcCommand(sQuery, conn, tr))
                                    {
                                        #region 파라미터 값 생성

             
                                        sql.Parameters.Add(new OdbcParameter("type", OdbcType.Int, 4, "type")
                                        {
                                            Value = (int)newTask.type,
                                            IsNullable = false,
                                            DbType = System.Data.DbType.Int32
                                        });

                                        sql.Parameters.Add(new OdbcParameter("etime", OdbcType.BigInt, 19, "etime")
                                        {
                                            Value = lUtcTime,
                                            IsNullable = true,
                                            DbType = System.Data.DbType.Int64
                                        });

                                        sql.Parameters.Add(new OdbcParameter("state", OdbcType.Int, 4, "state")
                                        {
                                            Value = (int)TaskState.StateEdit,
                                            IsNullable = false,
                                            DbType = System.Data.DbType.Int32
                                        });

                                        sql.Parameters.Add(new OdbcParameter("starttime", OdbcType.BigInt, 19, "starttime")
                                        {
                                            Value = newTask.TaskStart,
                                            IsNullable = true,
                                            DbType = System.Data.DbType.Int64
                                        });

                                        sql.Parameters.Add(new OdbcParameter("endtime", OdbcType.VarChar, 19, "endtime")
                                        {
                                            Value = newTask.TaskEnd,
                                            IsNullable = true,
                                            DbType = System.Data.DbType.Int64
                                        });
                                        /// type = ?, etime = ?, state = ?, starttime = ?, endtime = ?, starttimeofday = ?, endtimeofday = ?, guuid = ?, duuid = ?, name = ?, show_flag = ?, daysofweek = ?, priority = ?, duration = ? WHERE uuid = ?

                                        sql.Parameters.Add(new OdbcParameter("starttimeofday", OdbcType.Int, 4, "starttimeofday")
                                        {
                                            Value = newTask.starttimeofday,
                                            IsNullable = true,
                                            DbType = System.Data.DbType.Int32
                                        });

                                        sql.Parameters.Add(new OdbcParameter("endtimeofday", OdbcType.Int, 4, "endtimeofday")
                                        {
                                            Value = newTask.endtimeofday,
                                            IsNullable = true,
                                            DbType = System.Data.DbType.Int32
                                        });

                                        sql.Parameters.Add(new OdbcParameter("guuid", OdbcType.VarChar, 36, "guuid")
                                        {
                                            Value = sync.GUUID,
                                            IsNullable = true,
                                            DbType = System.Data.DbType.AnsiString
                                        });

                                        sql.Parameters.Add(new OdbcParameter("duuid", OdbcType.VarChar, 36, "duuid")
                                        {
                                            Value = sync.DUUID,
                                            IsNullable = true,
                                            DbType = System.Data.DbType.AnsiString
                                        });

                                        sql.Parameters.Add(new OdbcParameter("name", OdbcType.VarChar, 255, "name")
                                        {
                                            Value = newTask.description,
                                            IsNullable = false,
                                            DbType = System.Data.DbType.AnsiString
                                        });

                                        int showFlag = newTask.show_flag & ~((int)ShowFlag.Show_SyncMaster | (int)ShowFlag.Show_SyncSlave);

                                        sql.Parameters.Add(new OdbcParameter("show_flag", OdbcType.Int, 4, "show_flag")
                                        {
                                            Value = sync.MasterYN ? (showFlag | (int)ShowFlag.Show_SyncMaster) : (showFlag | (int)ShowFlag.Show_SyncSlave),
                                            IsNullable = true,
                                            DbType = System.Data.DbType.Int32
                                        });


                                        sql.Parameters.Add(new OdbcParameter("daysofweek", OdbcType.Int, 4, "daysofweek")
                                        {
                                            Value = newTask.daysofweek,
                                            IsNullable = true,
                                            DbType = System.Data.DbType.Int32
                                        });

                                        sql.Parameters.Add(new OdbcParameter("priority", OdbcType.Int, 4, "priority")
                                        {
                                            Value = (int)newTask.priority,
                                            IsNullable = true,
                                            DbType = System.Data.DbType.Int32
                                        });

                                        sql.Parameters.Add(new OdbcParameter("duration", OdbcType.Int, 4, "duration")
                                        {
                                            Value = (int)newTask.duration,
                                            IsNullable = false,
                                            DbType = System.Data.DbType.Int32
                                        });
                                        
                                        sql.Parameters.Add(new OdbcParameter("uuid", OdbcType.VarChar, 36, "uuid")
                                        {
                                            Value = String.IsNullOrEmpty(sync.UUID) ? Guid.NewGuid().ToString() : sync.UUID,
                                            DbType = System.Data.DbType.AnsiString
                                        });

                                        #endregion

                                        logger.Info(sQuery);

                                        sql.ExecuteNonQuery();
                                        sql.Dispose();
                                    }

                                    try
                                    {
                                        String sSyncQuery = String.Format("INSERT INTO SYNC_TASKS (GUUID, PLAYERS_PID, SYNC_TP, EDIT_DT, REG_DT, DEL_YN) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', 'N')", sync.GUUID, sync.PlayerID, sync.MasterYN ? "M" : "S", utpTicks, utpTicks);
                                        using (OdbcCommand sql = new OdbcCommand(sSyncQuery, conn, tr))
                                        {
                                            logger.Info(sSyncQuery);

                                            sql.ExecuteNonQuery();
                                            sql.Dispose();
                                        }
                                    }
                                    catch 
                                    {
                                        String sSyncQuery = String.Format("UPDATE SYNC_TASKS SET SYNC_TP = '{2}', EDIT_DT = '{3}', DEL_YN = 'N' WHERE GUUID = '{0}' AND PLAYERS_PID = '{1}'", sync.GUUID, sync.PlayerID, sync.MasterYN ? "M" : "S", utpTicks);
                                        using (OdbcCommand sql = new OdbcCommand(sSyncQuery, conn, tr))
                                        {
                                            logger.Info(sSyncQuery);

                                            sql.ExecuteNonQuery();
                                            sql.Dispose();
                                        }
                                    }
                                }

                                tr.Commit();
                            }
                            catch (Exception exTR)
                            {
                                logger.Error(exTR);
                                tr.Rollback();

                                return -1;
                            }
                        }
                    }
                }
                catch (InvalidOperationException ioe)
                {
                    logger.Error(ioe + "");
                    cfg.ReInitializeConnectionPool();
                }
                catch (Exception e)
                {
                    logger.Error(e.Message);
                }

                foreach (DigitalSignage.Common.SYNC_TASK sync in synclist)
                {
                    SendUpdateService.GetInstance.AddSession(sync.GroupID, sync.PlayerID);
                }

                lock (lockObj)
                {
                    updated_timestamp = lUtcTime;
                }
                return 0;
            }
            catch (InvalidOperationException ioe)
            {
                logger.Error(ioe + "");
                cfg.ReInitializeConnectionPool();
            }
            catch (Exception e)
            {
                logger.Error(e + "");
            }
            return -1;

        }

		public int UpdateTask(Task task)
		{
			try
			{
                if ((task.show_flag & (int)ShowFlag.Show_SyncMaster) > 0 || (task.show_flag & (int)ShowFlag.Show_SyncSlave) > 0)
                {
                    DateTime dtUtcNow = DateTime.UtcNow;
                    long lUtcTime = TimeConverter.ConvertToUTP(dtUtcNow);
                    string utpTicks = TimeConverter.ConvertYYYYMMDDH24mmSSFromUTP(lUtcTime);


                    /// 동기화 스케줄
                    /// 
                    String sQuery = String.Empty;

                    sQuery = "UPDATE TASKS SET etime = ?, state = ?, starttime = ?, endtime = ?, starttimeofday = ?, endtimeofday = ?, priority = ? WHERE guuid = ?";

                    using (OdbcConnection conn = cfg.serverDBLayer.GetAvailableConnection())
                    {
                        using (OdbcCommand sql = new OdbcCommand(sQuery, conn))
                        {
                            #region 파라미터 값 생성

                            sql.Parameters.Add(new OdbcParameter("etime", OdbcType.BigInt, 19, "etime")
                            {
                                Value = lUtcTime,
                                IsNullable = true,
                                DbType = System.Data.DbType.Int64
                            });

                            sql.Parameters.Add(new OdbcParameter("state", OdbcType.Int, 4, "state")
                            {
                                Value = (int)TaskState.StateEdit,
                                IsNullable = true,
                                DbType = System.Data.DbType.Int32
                            });

                            sql.Parameters.Add(new OdbcParameter("starttime", OdbcType.BigInt, 14, "starttime")
                            {
                                Value = task.TaskStart,
                                IsNullable = true,
                                DbType = System.Data.DbType.Int64
                            });

                            sql.Parameters.Add(new OdbcParameter("endtime", OdbcType.BigInt, 14, "endtime")
                            {
                                Value = task.TaskEnd,
                                IsNullable = true,
                                DbType = System.Data.DbType.Int64
                            });

                            sql.Parameters.Add(new OdbcParameter("starttimeofday", OdbcType.Int, 4, "starttimeofday")
                            {
                                Value = task.starttimeofday,
                                IsNullable = true,
                                DbType = System.Data.DbType.Int32
                            });
                            sql.Parameters.Add(new OdbcParameter("endtimeofday", OdbcType.Int, 4, "endtimeofday")
                            {
                                Value = task.endtimeofday,
                                IsNullable = true,
                                DbType = System.Data.DbType.Int32
                            });

                            sql.Parameters.Add(new OdbcParameter("priority", OdbcType.Int, 4, "priority")
                            {
                                Value = (int)task.priority,
                                IsNullable = true,
                                DbType = System.Data.DbType.Int32
                            });

                            sql.Parameters.Add(new OdbcParameter("guuid", OdbcType.VarChar, 36, "guuid")
                            {
                                Value = task.guuid.ToString(),
                                IsNullable = true,
                                DbType = System.Data.DbType.AnsiString
                            });

                            #endregion

                            int res = sql.ExecuteNonQuery();
                            sql.Dispose();

                            lock (lockObj)
                            {
                                if (lUtcTime > updated_timestamp) updated_timestamp = lUtcTime;
                            }

                            SendUpdateService.GetInstance.AddSession(task.gid, task.pid);

                            return res;
                        }
                    }

                }
                else
                {
                    using (DigitalSignage.ServerDatabase.ServerDataSetTableAdapters.tasksTableAdapter da = cfg.serverDBLayer.CreateTaskDA())
                    {
                        DateTime lUtc = DateTime.UtcNow;

                        int res = da.UpdateTask(task.pid, task.gid, task.TaskStart, task.TaskEnd, TimeConverter.ConvertToUTP(lUtc), (int)task.type, task.description, task.duuid.ToString(), (int)task.duration, task.daysofweek, task.priority, task.endtimeofday, task.starttimeofday, (int)task.state, task.isAd ? "Y" : "N", task.MetaTags, task.show_flag, task.uuid.ToString());

                        da.Dispose();
                        lock (lockObj)
                        {
                            long ticks = TimeConverter.ConvertToUTP(lUtc);
                            if (ticks > updated_timestamp) updated_timestamp = ticks;
                        }
                        return res;
                    }
                }
			}
			catch (InvalidOperationException ioe)
			{
				logger.Error(ioe + "");
				cfg.ReInitializeConnectionPool();
			}
			catch (Exception e)
			{
				logger.Error(e + "");
			}
			return -1;
		}

		public bool CanDeleteByDuuid(String duuid, string ftpAddress, string ftpPath)
		{

			bool bRet = false;
			try
			{
				using (DigitalSignage.ServerDatabase.ServerDataSetTableAdapters.tasksTableAdapter da = cfg.serverDBLayer.CreateTaskDA())
				{
					using (ServerDataSet.tasksDataTable dt = da.GetAliveTasksByDuuid(ftpPath, ftpAddress, duuid))
					{
						da.Dispose();
						bRet = (dt != null && dt.Count > 1) ? false : true;
						dt.Dispose();
					}
				}

			}
			catch (InvalidOperationException ioe)
			{
				logger.Error(ioe + "");
				cfg.ReInitializeConnectionPool();
			}
			catch (Exception e)
			{
				logger.Error(e + "");
			}
			return bRet;
		}

		public bool CanDeleteByDuuid(String duuid, string ftpAddress)
		{
			return CanDeleteByDuuid(duuid, ftpAddress, "");
		}

		public bool CanAddTaskToGroup(Task task, string gid)
		{
			try
			{
				using (DigitalSignage.ServerDatabase.ServerDataSetTableAdapters.tasksTableAdapter da = cfg.serverDBLayer.CreateTaskDA())
				{
					ServerDataSet.tasksDataTable dt = null;

					dt = da.GetTasksBetweenStartEndOnlyGroup(task.uuid.ToString() , gid, (int)task.type, task.TaskStart, task.TaskEnd, task.TaskStart, task.TaskEnd);

					if ((dt != null && dt.Count > 0))
					{
						foreach (ServerDataSet.tasksRow row in dt.Rows)
						{
							if (!CheckDuplicationTime(task, row))
							{
								da.Dispose();
								dt.Dispose();

								return false;
							}
						}
					}

					da.Dispose();
					dt.Dispose();
					dt = null;

					return true;
				}
			}
			catch (InvalidOperationException ioe)
			{
				logger.Error(ioe + "");
				cfg.ReInitializeConnectionPool();
			}
			catch (Exception e)
			{
				logger.Error(e + "");
			}
			return false;
		}

		public bool CanAddTaskToGroup(String uuid, long from, long till, long type, string gid)
		{
			bool bRet = false;
			try
			{
				using (DigitalSignage.ServerDatabase.ServerDataSetTableAdapters.tasksTableAdapter da = cfg.serverDBLayer.CreateTaskDA())
				{
					ServerDataSet.tasksDataTable dt = null;

					dt = da.GetTasksBetweenStartEndOnlyGroup(uuid, gid, (int)type, from, till, from, till);

					da.Dispose();
					bRet = (dt != null && dt.Count > 0) ? false : true;

					dt.Dispose();
					dt = null;
				}
			}
			catch (InvalidOperationException ioe)
			{
				logger.Error(ioe + "");
				cfg.ReInitializeConnectionPool();
			}
			catch (Exception e)
			{
				logger.Error(e + "");
			}
			return bRet;
		}
		private bool CheckDuplicationTime(Task src, ServerDataSet.tasksRow desc)
		{
			//	원본이 반복 패턴이 있는지 검사
			if (src.starttimeofday != src.endtimeofday)
			{
				//	대상이 반복 패턴이 있는지 검사
				if(desc.starttimeofday != desc.endtimeofday)
				{
					//	요일 중복 검색
					if ((src.daysofweek & desc.daysofweek) > 0)
					{
						// 요일이 중복된다면 시간대 검색
						if ((src.starttimeofday == desc.starttimeofday && src.endtimeofday == desc.endtimeofday) || 
							(src.starttimeofday <= desc.starttimeofday && src.endtimeofday > desc.starttimeofday) ||
							(src.starttimeofday < desc.endtimeofday && src.endtimeofday >= desc.endtimeofday) ||
							(desc.starttimeofday <= src.starttimeofday && desc.endtimeofday > src.starttimeofday) ||
							(desc.starttimeofday < src.endtimeofday && desc.endtimeofday >= src.endtimeofday))
						{
							//	시간대가 중복된다면
							return false;
						}
						else return true;
					}
					else return false;
				}
				else
				{
					//	대상이 반복 패턴이 없는 경우
					DateTime dtStart = TimeConverter.ConvertFromUTP(desc.starttime).ToLocalTime();
					DateTime dtEnd = TimeConverter.ConvertFromUTP(desc.endtime).ToLocalTime();
					//aaa더해야해
					DateTime dtSrcStart = TimeConverter.ConvertFromUTP(src.TaskStart).ToLocalTime();
					DateTime dtSrcEnd = TimeConverter.ConvertFromUTP(src.TaskEnd).ToLocalTime();

					dtSrcStart = new DateTime(dtSrcStart.Year, dtSrcStart.Month, dtSrcStart.Day) + TimeSpan.FromSeconds(src.starttimeofday);
					dtSrcEnd = new DateTime(dtSrcEnd.Year, dtSrcEnd.Month, dtSrcEnd.Day) + TimeSpan.FromSeconds(src.endtimeofday);

					DateTime start = dtSrcStart;
					DateTime end = new DateTime(dtSrcStart.Year, dtSrcStart.Month, dtSrcStart.Day) + TimeSpan.FromSeconds(src.endtimeofday);

					while(start <= dtSrcEnd)
					{
						if ((src.daysofweek & (int)start.DayOfWeek) == 0)
						{
							start += TimeSpan.FromDays(1);
							end += TimeSpan.FromDays(1);

							continue;
						}

						if ((start <= dtStart && dtStart < end) || (start < dtEnd && dtEnd <= end) ||
							(dtStart <= start && start < dtEnd) || (dtStart < end && end <= dtEnd))
						{
							return false;
						}
						start += TimeSpan.FromDays(1);
						end += TimeSpan.FromDays(1);
					}

					return true;
				}
			}
			else
			{
				//	반복 패턴이 없다면
				//	대상이 반복 패턴이 있는지 검사
				if (desc.starttimeofday != desc.endtimeofday)
				{
					//	원본이 반복 패턴이 없고 대상이 반복패턴이 있는경우
					DateTime dtStart = TimeConverter.ConvertFromUTP(src.TaskStart).ToLocalTime();
					DateTime dtEnd = TimeConverter.ConvertFromUTP(src.TaskEnd).ToLocalTime();
					DateTime dtDescStart = TimeConverter.ConvertFromUTP(desc.starttime).ToLocalTime();
					DateTime dtDescEnd = TimeConverter.ConvertFromUTP(desc.endtime).ToLocalTime();

					dtDescStart = new DateTime(dtDescStart.Year, dtDescStart.Month, dtDescStart.Day) + TimeSpan.FromSeconds(desc.starttimeofday);
					dtDescEnd = new DateTime(dtDescEnd.Year, dtDescEnd.Month, dtDescEnd.Day) + TimeSpan.FromSeconds(desc.endtimeofday);

					DateTime start = dtDescStart;
					DateTime end = new DateTime(dtDescStart.Year, dtDescStart.Month, dtDescStart.Day) + TimeSpan.FromSeconds(desc.endtimeofday);

					while (start <= dtDescEnd)
					{
						if ((desc.daysofweek & (int)start.DayOfWeek) == 0)
						{
							start += TimeSpan.FromDays(1);
							end += TimeSpan.FromDays(1);

							continue;
						}

						if ((start <= dtStart && dtStart < end) || (start < dtEnd && dtEnd <= end) ||
							(dtStart <= start && start < dtEnd) || (dtStart < end && end <= dtEnd))
						{
							return false;
						}
						start += TimeSpan.FromDays(1);
						end += TimeSpan.FromDays(1);
					}

					return true;
				}
				else
				{
					return false;
				}
			}

		}
		public bool CanAddTaskToPlayer(Task task, string pid)
		{
			try
			{
				using (DigitalSignage.ServerDatabase.ServerDataSetTableAdapters.tasksTableAdapter da = cfg.serverDBLayer.CreateTaskDA())
				{
					ServerDataSet.tasksDataTable dt = null;

					dt = da.GetTasksBetweenStartEndOnlyPlayer(task.uuid.ToString(), (int)task.type, pid, task.TaskStart, task.TaskEnd, task.TaskStart, task.TaskEnd);

					if ((dt != null && dt.Count > 0))
					{
						foreach (ServerDataSet.tasksRow row in dt.Rows)
						{
							//	1. 원본 - 반복 있는지 검사
							//	2. 반복이 있다면
							if(!CheckDuplicationTime(task, row))
							{
								da.Dispose();
								dt.Dispose();

								return false;
							}
						}
					}
					da.Dispose();
					dt.Dispose();
					dt = null;

					return true;
				}
			}
			catch (InvalidOperationException ioe)
			{
				logger.Error(ioe + "");
				cfg.ReInitializeConnectionPool();
			}
			catch (Exception e)
			{
				logger.Error(e + "");
			}
			return false;
		}

		public bool CanAddTaskToPlayer(String uuid, long from, long till, long type, string pid)
		{
			bool bRet = false;
			try
			{
				using (DigitalSignage.ServerDatabase.ServerDataSetTableAdapters.tasksTableAdapter da = cfg.serverDBLayer.CreateTaskDA())
				{
					ServerDataSet.tasksDataTable dt = null;

					dt = da.GetTasksBetweenStartEndOnlyPlayer(uuid, (int)type, pid, from, till, from, till);

					da.Dispose();
					bRet = (dt != null && dt.Count > 0) ? false : true;

					dt.Dispose();
					dt = null;
				}
			}
			catch (InvalidOperationException ioe)
			{
				logger.Error(ioe + "");
				cfg.ReInitializeConnectionPool();
			}
			catch (Exception e)
			{
				logger.Error(e + "");
			}
			return bRet;
		}

		public int GetMinPriorityFromDefaultTasks(string gid)
		{

			int nRet = 0;
			try
			{
				using (DigitalSignage.ServerDatabase.ServerDataSetTableAdapters.tasksTableAdapter da = cfg.serverDBLayer.CreateTaskDA())
				{
					using (ServerDataSet.tasksDataTable dt = da.GetMinPriorityFromDefaultTask(gid))
					{
						da.Dispose();
						nRet = (dt == null) ? 0 : ((ServerDataSet.tasksRow)dt.Rows[0]).priority;

						dt.Dispose();
					}
				}

			}
			catch (InvalidOperationException ioe)
			{
				logger.Error(ioe + "");
				cfg.ReInitializeConnectionPool();
			}
			catch (Exception e)
			{
				logger.Error(e.Message);
			}

			return nRet;

		}


		public int ChangeTaskState(DigitalSignage.Common.TaskState newState, Guid taskUuid)
		{
			try
			{
                using (DigitalSignage.ServerDatabase.ServerDataSetTableAdapters.tasksTableAdapter da = cfg.serverDBLayer.CreateTaskDA())
                {
                    da.ChangeTaskState((int)newState, TimeConverter.ConvertToUTP(DateTime.UtcNow), taskUuid.ToString());
                    da.Dispose();
                }
				return 0;
			}
			catch (Exception e)
			{
				logger.Error(e.Message + "");
			}
			return -1;
		}

		/// <summary>
		/// 스케줄을 가져온다
		/// </summary>
		/// <param name="uuid"></param>
		/// <returns></returns>
		public ServerDatabase.ServerDataSet.tasksDataTable GetTaskByuuid(string uuid)
		{
			Config cfg = Config.GetConfig;
			String sQuery = String.Empty;

			using (OdbcConnection conn = cfg.serverDBLayer.GetAvailableConnection())
			{
				try
				{
					using (ServerDatabase.ServerDataSet.tasksDataTable dt = new ServerDataSet.tasksDataTable())
					{
						sQuery = String.Format("SELECT pid, type, state, uuid, starttime, endtime, etime, duuid, guuid, name, gid, duration, daysofweek, priority, endtimeofday, starttimeofday, is_ad, meta_tag, show_flag, tz_info FROM tasks WHERE uuid='{0}';", uuid);
						using (OdbcCommand comm = new OdbcCommand(sQuery, conn))
						{
							using (OdbcDataReader dr = comm.ExecuteReader())
							{
								dt.Load(dr);

								dr.Close();
								dr.Dispose();
							}
							comm.Dispose();
						}

						return dt;
					}
				}
				catch (InvalidOperationException ioe)
				{
					conn.Close();
					conn.Dispose();
					throw ioe;
				}
				catch (OdbcException oe)
				{
					logger.Error(oe.Message + " query : " + sQuery);

					if (oe.Message.Contains("hstmt"))
					{
						conn.Close();
						conn.Dispose();
						throw oe;
					}
				}
				catch (Exception e)
				{
					logger.Error(e.Message + " query : " + sQuery);
				}
			}

			return null;
		}


		#region Screen 관련 
		/// <summary>
		/// 스크린을 등록한다.
		/// </summary>
		/// <param name="s_id">페이지 아이디</param>
		/// <param name="s_name">이름</param>
		/// <param name="s_description">상세</param>
		/// <param name="s_width">너비</param>
		/// <param name="s_height">높이</param>
		/// <param name="playtime">재생시간</param>
		/// <param name="screen_type">페이지 타입</param>
		/// <param name="display_type">디스플레이 타입</param>
		/// <param name="s_is_ad">광고 여부</param>
		/// <param name="s_metatag">메타태그</param>
		/// <param name="ad_starttime">광고 시작 시간</param>
		/// <param name="ad_endtime">광고 끝 시간</param>
		/// <returns>성공 여부</returns>
		public bool InsertScreen(long s_id, string s_name, string s_description, int s_width, int s_height, int playtime, int screen_type, int display_type, string s_is_ad, string s_metatag, long ad_starttime, long ad_endtime)
		{
			try
			{
				int nRet = 0;
				using(DigitalSignage.ServerDatabase.ServerDataSetTableAdapters.screensTableAdapter da = cfg.serverDBLayer.CreateScreenDA())
				{
					nRet = da.AddScreen(s_id, ad_starttime, ad_endtime, s_width, s_height, playtime, s_name, s_description, screen_type, display_type, s_is_ad, s_metatag, "N", TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime()));
					da.Dispose();
				}
				return nRet > 0;
			}
			catch (Exception e)
			{
				logger.Error(e.Message + "");
			}
			return false;
		}

		/// <summary>
		/// 스케줄과 스크린과의 연결을 등록한다.
		/// </summary>
		/// <param name="s_id">페이지 ID</param>
		/// <param name="task_uuid">스케줄 ID</param>
		/// <param name="s_index">페이지 순서</param>
		/// <returns>성공 여부</returns>
		public bool InsertTaskAndScreenMapping(long s_id, string task_uuid, int s_index)
		{
			try
			{
				int nRet = 0;
				using (DigitalSignage.ServerDatabase.ServerDataSetTableAdapters.ScreenTaskASNTableAdapter da = cfg.serverDBLayer.CreateScreenTaskASN_DA())
				{
					nRet = da.InsertMapping(s_id, task_uuid, s_index);
					da.Dispose();
				}
				return nRet > 0;
			}
			catch (Exception e)
			{
				logger.Error(e.Message + "");
			}
			return false;
		}

        /// <summary>
        /// 스케줄과 스크린과의 연결을 등록한다.
        /// </summary>
        /// <param name="s_id">페이지 ID</param>
        /// <param name="task_uuid">스케줄 ID</param>
        /// <param name="s_index">페이지 순서</param>
        /// <param name="thumbnailURL"></param>
        /// <returns>성공 여부</returns>
        public bool InsertTaskAndScreenMappingDetail(long s_id, string task_uuid, int s_index, string thumbnailURL)
        {
            try
            {
                int nRet = 0;
                using (DigitalSignage.ServerDatabase.ServerDataSetTableAdapters.ScreenTaskASNTableAdapter da = cfg.serverDBLayer.CreateScreenTaskASN_DA())
                {
                    nRet = da.InsertMappingDetail(s_id, task_uuid, s_index, thumbnailURL);
                    da.Dispose();
                }
                return nRet > 0;
            }
            catch (Exception e)
            {
                logger.Error(e.Message + "");
            }
            return false;
        }


		/// <summary>
		/// 스케줄과 연결된 스크린을 가져온다.
		/// </summary>
		/// <param name="uuid"></param>
		/// <returns></returns>
		public ServerDatabase.ServerDataSet.screensDataTable GetScreensByUuid(string uuid)
		{
			Config cfg = Config.GetConfig;
			String sQuery = String.Empty;

			using (OdbcConnection conn = cfg.serverDBLayer.GetAvailableConnection())
			{
				try
				{
					using (ServerDatabase.ServerDataSet.screensDataTable dt = new ServerDataSet.screensDataTable())
					{
						sQuery = String.Format("SELECT s_id, s_starttime, s_endtime, s_width, s_height, s_playtime, s_name, s_description, s_screen_type, s_display_type, s_is_ad, s_meta_tag, s_delete_yn, s_regdt FROM screens WHERE s_id IN (SELECT s_id FROM screentask_asn where uuid='{0}');", uuid);
						using (OdbcCommand comm = new OdbcCommand(sQuery, conn))
						{
							using (OdbcDataReader dr = comm.ExecuteReader())
							{
								dt.Load(dr);

								dr.Close();
								dr.Dispose();
							}
							comm.Dispose();
						}

						return dt;
					}
				}
				catch (InvalidOperationException ioe)
				{
					conn.Close();
					conn.Dispose();
					throw ioe;
				}
				catch (OdbcException oe)
				{
					logger.Error(oe.Message + " query : " + sQuery);

					if (oe.Message.Contains("hstmt"))
					{
						conn.Close();
						conn.Dispose();
						throw oe;
					}
				}
				catch (Exception e)
				{
					logger.Error(e.Message + " query : " + sQuery);
				}
			}

			return null;
		}

		/// <summary>
		/// 스크린을 가져온다.
		/// </summary>
		/// <param name="screenID"></param>
		/// <returns></returns>
		public ServerDatabase.ServerDataSet.screensDataTable GetScreenByID(string screenID)
		{
			Config cfg = Config.GetConfig;
			String sQuery = String.Empty;

			using (OdbcConnection conn = cfg.serverDBLayer.GetAvailableConnection())
			{
				try
				{
					using (ServerDatabase.ServerDataSet.screensDataTable dt = new ServerDataSet.screensDataTable())
					{
						sQuery = String.Format("SELECT s_id, s_starttime, s_endtime, s_width, s_height, s_playtime, s_name, s_description, s_screen_type, s_display_type, s_is_ad, s_meta_tag, s_delete_yn, s_regdt FROM screens WHERE s_id={0};", screenID);
						using (OdbcCommand comm = new OdbcCommand(sQuery, conn))
						{
							using (OdbcDataReader dr = comm.ExecuteReader())
							{
								dt.Load(dr);

								dr.Close();
								dr.Dispose();
							}
							comm.Dispose();
						}

						return dt;
					}
				}
				catch (InvalidOperationException ioe)
				{
					conn.Close();
					conn.Dispose();
					throw ioe;
				}
				catch (OdbcException oe)
				{
					logger.Error(oe.Message + " query : " + sQuery);

					if (oe.Message.Contains("hstmt"))
					{
						conn.Close();
						conn.Dispose();
						throw oe;
					}
				}
				catch (Exception e)
				{
					logger.Error(e.Message + " query : " + sQuery);
				}
			}

			return null;
		}


		#endregion
	}
}