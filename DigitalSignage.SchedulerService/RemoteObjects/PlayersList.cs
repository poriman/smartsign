﻿using System;
using System.Data.Common;

using NLog;

using DigitalSignage.Common;
using System.Xml;
using System.Collections.Generic;
using System.Data.Odbc;

namespace DigitalSignage.SchedulerService
{
    public class PlayersList : MarshalByRefObject, IPlayersList
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public String NPCUControl(String PlayerID, String RequestCode)
        {
            try
            {
                NPCUService service = NPCUService.GetInstance;
                int nID = service.SendControlRequest(PlayerID, RequestCode);
                int nTimeout = 0;
                while(nTimeout < 50)
                {
                    String sReturn = service.GetControlResponse(nID);

                    if (!String.IsNullOrEmpty(sReturn)) return sReturn;
                    System.Threading.Thread.Sleep(100);
                    nTimeout++;
                }
                 
            }
            catch { }
            return "TIMEOUT";
        }

		class NPCUItem
		{
			public String PlayerID;
			public String RequestCode;
			public String ResponseCode;
			public NPCUItem(String pid, String req_cd)
			{
				PlayerID = pid;
				RequestCode = req_cd;
				ResponseCode = "99";
			}
		}

		public String NPCUControlByPIDArray(String[] arrPids, String RequestCode)
		{
			try
			{
				if(arrPids == null) return String.Empty;

				NPCUService service = NPCUService.GetInstance;

				Dictionary<int, NPCUItem> dicItems = new Dictionary<int, NPCUItem>();
				foreach (String PlayerID in arrPids)
				{
					int nID = service.SendControlRequest(PlayerID, RequestCode);
					dicItems.Add(nID, new NPCUItem(PlayerID, RequestCode));
					System.Threading.Thread.Sleep(10);
				}

				int nTimeout = 0;
				while (nTimeout < 5)
				{
					foreach (int nID in dicItems.Keys)
					{
						String sReturn = service.GetControlResponse(nID);
						if (!String.IsNullOrEmpty(sReturn)) dicItems[nID].ResponseCode = sReturn;
					}
					System.Threading.Thread.Sleep(1000);
					nTimeout++;
				}

				String xReturn = "<Responses>";

				foreach (NPCUItem item in dicItems.Values)
				{
					xReturn += String.Format("<Response pid=\"{0}\" res_cd=\"{1}\"/>", item.PlayerID, item.ResponseCode);
				}

				xReturn += "</Responses>";

				return xReturn;
			}
			catch (Exception ex)
			{
				logger.Error(ex.ToString());
			}
			return "TIMEOUT";
		}

		public bool AddPlayerLimitInfo(int cntPlayers, string gid)
		{
			Config cfg = Config.GetConfig;
			try
			{
				using (ServerDatabase.ServerDataSetTableAdapters.LimitPlayersTableAdapter lpta = cfg.serverDBLayer.CreateLimitPlayersDA())
				{
					using (DigitalSignage.ServerDatabase.ServerDataSet.LimitPlayersDataTable dt = lpta.GetDataByGID(gid))
					{
						if(dt != null && dt.Rows.Count > 0)
						{
							lpta.RemoveData(gid);
						}

						lpta.AddData(gid, cntPlayers);
						
						lpta.Dispose();
						dt.Dispose();

						return true;
					}
				}
			}
			catch (InvalidOperationException ioe)
			{
				logger.Error(ioe + "");
				cfg.ReInitializeConnectionPool();
			}
			catch (Exception ex)
			{
				logger.Error(ex.ToString());
			}

			return false;
		}

		public bool DeleteLimitInfo(string gid)
		{
			Config cfg = Config.GetConfig;
			try
			{
				using (ServerDatabase.ServerDataSetTableAdapters.LimitPlayersTableAdapter lpta = cfg.serverDBLayer.CreateLimitPlayersDA())
				{
						lpta.RemoveData(gid);
						lpta.Dispose();

						return true;
				}
			}
			catch (InvalidOperationException ioe)
			{
				logger.Error(ioe + "");
				cfg.ReInitializeConnectionPool();
			}
			catch (Exception ex)
			{
				logger.Error(ex.ToString());
			}

			return false;
		}

		public int GetLimitInfo(string gid)
		{
			Config cfg = Config.GetConfig;
			try
			{
				using (ServerDatabase.ServerDataSetTableAdapters.LimitPlayersTableAdapter lpta = cfg.serverDBLayer.CreateLimitPlayersDA())
				{
					using (DigitalSignage.ServerDatabase.ServerDataSet.LimitPlayersDataTable dt = lpta.GetDataByGID(gid))
					{
						foreach (DigitalSignage.ServerDatabase.ServerDataSet.LimitPlayersRow row in dt.Rows)
						{
							return row.cntplayers;
						}
					}
				}
			}
			catch (InvalidOperationException ioe)
			{
				logger.Error(ioe + "");
				cfg.ReInitializeConnectionPool();
			}
			catch (Exception ex)
			{
				logger.Error(ex.ToString());
			}

			return -1;
		}

		public bool IsAvailableAddPlayer()
		{
			Config cfg = Config.GetConfig;
			try
			{
				CacheObject obj = CacheObject.GetInstance;
				int n = obj.GetPlayersCount();

				return n < cfg.MaxCountOfPlayers;
			}
			catch (Exception e)
			{
				logger.Log(LogLevel.Error, e + "");
			}
			return false;
		}


		public bool IsAvailableAddPlayer(string gid)
		{
			try
			{

				XmlNode node = CacheObject.GetInstance.FindGroupNode(gid);

				do
				{
					try
					{
						int nTmpCnt = this.GetLimitInfo(gid);

						if (nTmpCnt != -1)
						{
							CacheObject obj = CacheObject.GetInstance;
							int n = obj.GetPlayersCount(gid);

							if (n >= nTmpCnt)
							{
								return false;
							}

						}

						gid = node.ParentNode.Attributes["gid"].Value;
					}
					catch
					{
						break;
					}
				} while ((node = node.ParentNode) != null);

				return true;
			}
			catch (Exception ex)
			{
				logger.Error(ex.ToString());
			}
			return false;
		}


		public void UpdatePlayerID(string gid, string pid, string new_pid)
		{
			Config cfg = Config.GetConfig;
			try
			{
				cfg.UpdatePID(gid, pid, new_pid);
			}
			catch (Exception e)
			{
				logger.Log(LogLevel.Error, e + "");
			}
		}

		public void UpdateServiceIP(string gid, string pid, string new_service_ip)
		{
			Config cfg = Config.GetConfig;
			try
			{
				cfg.UpdateSIP(gid, pid, new_service_ip);
			}
			catch (Exception e)
			{
				logger.Log(LogLevel.Error, e + "");
			}
		}

		public bool InsertOrUpdatePlayerDetailInfo(String PID, String IPv4Address, String MacAddress, String IsDHCP, String AuthID, String AuthPass, int MntType)
		{
			Config cfg = Config.GetConfig;
			try
			{
				using (DigitalSignage.ServerDatabase.ServerDataSetTableAdapters.playerdetailinfoTableAdapter da = cfg.serverDBLayer.CreatePlayerDetailInfoDA())
				{
					using(DigitalSignage.ServerDatabase.ServerDataSet.playerdetailinfoDataTable dt = da.GetPlayerDetail(PID))
					{
						if(dt.Rows.Count > 0)
						{
							DigitalSignage.ServerDatabase.ServerDataSet.playerdetailinfoRow row = dt.Rows[0] as DigitalSignage.ServerDatabase.ServerDataSet.playerdetailinfoRow;

							int nRet = da.UpdatePlayerDetail(IPv4Address, IsDHCP, MacAddress, MntType, AuthID, AuthPass, row.pid);
							dt.Dispose();
							da.Dispose();

							return nRet > 0;
						}
						else
						{
							int nRet = da.InsertPlayerDetail(PID, IPv4Address, IsDHCP, MacAddress, MntType, AuthID, AuthPass);
							dt.Dispose();
							da.Dispose();

							return nRet > 0;
						}
					}
				}
			}
			catch (InvalidOperationException ioe)
			{
				logger.Error(ioe + "");
				cfg.ReInitializeConnectionPool();
			}
			catch (Exception e)
			{
				logger.Error(e.Message);
			}

			return false;
		}

        /// <summary>
        /// 원격접속 요청
        /// </summary>
        /// <param name="connid"></param>
        /// <param name="pid"></param>
        /// <returns></returns>
        public bool RunRemoteControl(string connid, string pid)
        {
            try
            {
                return Config.GetConfig.SendMessageDelegate(pid, iVisionCode.FUNC_REMOTE_CONTROL, connid);
            }
            catch (Exception ex) { logger.Error(ex.ToString()); }

            return false;
        }

		//	hsshin 전원관리 - 매니저 인터페이스 삭제
		//public bool InsertOrUpdatePlayerDetailInfo(String PID, String AuthID, String AuthPass, int MntType)
		//{
		//    Config cfg = Config.GetConfig;
		//    try
		//    {
		//        using (DigitalSignage.ServerDatabase.ServerDataSetTableAdapters.playerdetailinfoTableAdapter da = cfg.serverDBLayer.CreatePlayerDetailInfoDA())
		//        {
		//            using (DigitalSignage.ServerDatabase.ServerDataSet.playerdetailinfoDataTable dt = da.GetPlayerDetail(PID))
		//            {
		//                if (dt.Rows.Count > 0)
		//                {
		//                    DigitalSignage.ServerDatabase.ServerDataSet.playerdetailinfoRow row = dt.Rows[0] as DigitalSignage.ServerDatabase.ServerDataSet.playerdetailinfoRow;

		//                    int nRet = da.UpdatePlayerDetail(row._ipv4addr, row._is_dhcp, row._macaddr, MntType, AuthID, AuthPass, row.pid);
		//                    dt.Dispose();
		//                    da.Dispose();

		//                    return nRet > 0;
		//                }
		//                else
		//                {
		//                    int nRet = da.InsertPlayerDetail(PID, "", "N", "", MntType, AuthID, AuthPass);
		//                    dt.Dispose();
		//                    da.Dispose();

		//                    return nRet > 0;
		//                }
		//            }
		//        }
		//    }
		//    catch (InvalidOperationException ioe)
		//    {
		//        logger.Error(ioe + "");
		//        cfg.ReInitializeConnectionPool();
		//    }
		//    catch (Exception e)
		//    {
		//        logger.Error(e.Message);
		//    }

		//    return false;
		//}
    }
}
