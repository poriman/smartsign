﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;

using DigitalSignage.Common;
using DigitalSignage.ServerDatabase;
using NLog;
//SQLite & DB routines
using System.Data.Common;
using System.Xml;

namespace DigitalSignage.SchedulerService
{
	public class DownloadTimer : MarshalByRefObject, IDownloadTimer
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

		public ServerDataSet.DownloadTime_Players_ASNDataTable GetDataByPID(string pid)
		{
			Config cfg = Config.GetConfig;

			try
			{
				using (ServerDatabase.ServerDataSetTableAdapters.DownloadTime_Players_ASNTableAdapter da = cfg.serverDBLayer.CreateDTPlayersAsnDA())
				{
					using (ServerDataSet.DownloadTime_Players_ASNDataTable dt = da.GetDataByPID(pid))
					{
						da.Dispose();
						return dt;
					}
				}
			}
			catch (InvalidOperationException ioe)
			{
				logger.Error(ioe + "");
				cfg.ReInitializeConnectionPool();
			}
			catch (Exception e)
			{
				logger.Error(e.Message);
			}

			return null;
		}

		public ServerDataSet.DownloadTime_Groups_ASNDataTable GetDataByGID(string gid)
		{
			Config cfg = Config.GetConfig;

			try
			{
				using (ServerDatabase.ServerDataSetTableAdapters.DownloadTime_Groups_ASNTableAdapter da = cfg.serverDBLayer.CreateDTGroupsAsnDA())
				{
					using (ServerDataSet.DownloadTime_Groups_ASNDataTable dt = da.GetDataByGID(gid))
					{
						da.Dispose();
						return dt;
					}
				}
			}
			catch (InvalidOperationException ioe)
			{
				logger.Error(ioe + "");
				cfg.ReInitializeConnectionPool();
			}
			catch (Exception e)
			{
				logger.Error(e.Message);
			}

			return null;
		}

		public ServerDataSet.DownloadTime_Groups_ASNDataTable GetDataByParents(string gid)
		{
			Config cfg = Config.GetConfig;

			try
			{
				using (ServerDatabase.ServerDataSetTableAdapters.DownloadTime_Groups_ASNTableAdapter da = cfg.serverDBLayer.CreateDTGroupsAsnDA())
				{
					CacheObject obj = CacheObject.GetInstance;
					XmlNode node = obj.FindGroupNode(gid);

					string groupid = gid;

					while (node != null && node.LocalName.Equals("group"))
					{
						groupid = node.Attributes["gid"].Value;
						using (ServerDataSet.DownloadTime_Groups_ASNDataTable dt = da.GetDataByGID(groupid))
						{
							if (dt != null && dt.Rows.Count > 0)
							{
								da.Dispose();
								return dt;
							}
						}
						
						node = node.ParentNode;
					}
					da.Dispose();
					return null;
				}
			}
			catch (InvalidOperationException ioe)
			{
				logger.Error(ioe + "");
				cfg.ReInitializeConnectionPool();
			}
			catch (Exception e)
			{
				logger.Error(e.ToString());
			}

			return null;

		}

		public int InsertDownloadTimeToPlayer(string pid, string start, string end)
		{
			Config cfg = Config.GetConfig;

			try
			{
				int nRet = 0;
				using (ServerDatabase.ServerDataSetTableAdapters.DownloadTime_Players_ASNTableAdapter da = cfg.serverDBLayer.CreateDTPlayersAsnDA())
				{
					long dt_id = DateTime.Now.Ticks;
					nRet = da.InsertDownloadRange(dt_id, start, end);
					if (nRet > -1)
					{
						nRet = da.InsertDownloadPlayer(dt_id, pid);
					}
					da.Dispose();
				}

				return nRet;
			}
			catch (InvalidOperationException ioe)
			{
				logger.Error(ioe + "");
				cfg.ReInitializeConnectionPool();
			}
			catch (Exception e)
			{
				logger.Error(e.Message);
			}

			return -1;
		}

		public int InsertDownloadTimeToGroup(string gid, string start, string end)
		{
			Config cfg = Config.GetConfig;

			try
			{
				int nRet = 0;
				using (ServerDatabase.ServerDataSetTableAdapters.DownloadTime_Groups_ASNTableAdapter da = cfg.serverDBLayer.CreateDTGroupsAsnDA())
				{
					long dt_id = DateTime.Now.Ticks;
					nRet = da.InsertDownloadRange(dt_id, start, end);
					if (nRet > -1)
					{
						nRet = da.InsertDownloadGroup(dt_id, gid);
					}
					da.Dispose();
				}

				return nRet;
			}
			catch (InvalidOperationException ioe)
			{
				logger.Error(ioe + "");
				cfg.ReInitializeConnectionPool();
			}
			catch (Exception e)
			{
				logger.Error(e.Message);
			}

			return -1;
		}

		public int DeleteTimesFromPID(string pid)
		{
			Config cfg = Config.GetConfig;

			try
			{
				int nRet = 0;
				using (ServerDatabase.ServerDataSetTableAdapters.DownloadTime_Players_ASNTableAdapter da = cfg.serverDBLayer.CreateDTPlayersAsnDA())
				{
					nRet = da.DeleteTimesFromPID(pid);
					da.Dispose();
				}
				return nRet;
			}
			catch (InvalidOperationException ioe)
			{
				logger.Error(ioe + "");
				cfg.ReInitializeConnectionPool();
			}
			catch (Exception e)
			{
				logger.Error(e.Message);
			}

			return -1;
		}

		public int DeleteTimesFromGID(string gid)
		{
			Config cfg = Config.GetConfig;

			try
			{
				int nRet = 0;
				using (ServerDatabase.ServerDataSetTableAdapters.DownloadTime_Groups_ASNTableAdapter da = cfg.serverDBLayer.CreateDTGroupsAsnDA())
				{
					nRet = da.DeleteTimesFromGID(gid);
					da.Dispose();
				}
				return nRet;
			}
			catch (InvalidOperationException ioe)
			{
				logger.Error(ioe + "");
				cfg.ReInitializeConnectionPool();
			}
			catch (Exception e)
			{
				logger.Error(e.Message);
			}

			return -1;
		}
    }
}
