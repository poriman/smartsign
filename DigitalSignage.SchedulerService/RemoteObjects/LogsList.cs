﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;

using DigitalSignage.Common;
using DigitalSignage.ServerDatabase;
using NLog;
//SQLite & DB routines
using System.Data.Common;
using System.IO;
using System.Data.Odbc;
using System.Xml;
using DigitalSignage.SchedulerService.Statistics;

namespace DigitalSignage.SchedulerService
{
    public class LogsList : MarshalByRefObject, ILogList
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

		public int DeleteLogsBefore(long dt)
		{
			Config cfg = Config.GetConfig;
			try
			{
				//using (ServerDatabase.ServerDataSetTableAdapters.logsTableAdapter da = cfg.serverDBLayer.CreateLogDA())
				//{
				//    int nRet = da.DeleteLogsBefore(dt);
				//    da.Dispose();
				//    return nRet;
				//}
			}
			catch (InvalidOperationException ioe)
			{
				logger.Error(ioe + "");
				cfg.ReInitializeConnectionPool();
			}
			catch (Exception e)
			{
				logger.Error(e.Message);
			}

			return -1;
		}

		/// <summary>
		/// 로그를 생성한다.
		/// </summary>
		/// <param name="pid"></param>
		/// <param name="logcd"></param>
		/// <param name="uuid"></param>
		/// <param name="name"></param>
		/// <param name="description"></param>
		/// <param name="error_code"></param>
		/// <param name="start_dt"></param>
		/// <param name="end_dt"></param>
		/// <returns></returns>
		public int InsertDetailLog(string pid, int logcd, string uuid, string name, string description, int error_code, long start_dt, long end_dt)
		{
			Config cfg = Config.GetConfig;

			try
			{
                using (OdbcConnection conn = cfg.serverDBLayer.GetAvailableConnection())
                {

                    #region 테이블 분기
                    String sTableName = this.GetLogTableName(logcd);
                    #endregion
                    String sQuery = String.Empty;

                    if (sTableName.Equals("downloadlogs"))
                    {
                        sQuery = String.Format("INSERT INTO downloadlogs (pid, logcd, uuid, tasknm, contentnm, errorcd, start_dt, end_dt) VALUES ('{0}', {1}, '{2}', '{3}', '{4}', {5}, {6}, {7})",
                            pid, logcd, uuid, name, description.Replace('\'', ' '), error_code, start_dt, end_dt);

                    }
                    else if (sTableName.Equals("managerlogs"))
                    {
                        sQuery = String.Format("INSERT INTO managerlogs (usernm, logcd, uuid, actionnm, actiondesc, errorcd, reg_dt) VALUES ('{0}', {1}, '{2}', '{3}', '{4}', {5}, {6})",
                            pid, logcd, uuid, name, description.Replace('\'', ' '), error_code, start_dt);
                    }
                    else if (sTableName.Equals("connectionlogs"))
                    {
                        sQuery = String.Format("INSERT INTO connectionlogs (pid, logcd, errorcd, start_dt, end_dt, reg_dt) VALUES ('{0}', {1}, {2}, {3}, {4}, {4})",
                            pid, logcd, error_code, start_dt, end_dt);
                    }
                    else if (sTableName.Equals("playlogs"))
                    {
                        //  hsshin  GS Caltex의 경우 playlogs_today에 기록하도록 되어있음
                        sTableName = "playlogs_today";


                        sQuery = String.Format("INSERT INTO playlogs_today (pid, logcd, uuid, tasknm, screennm, errorcd, start_dt, end_dt) VALUES ('{0}', {1}, '{2}', '{3}', '{4}', {5}, {6}, {7})",
                        pid, logcd, uuid, name, description.Replace('\'', ' '), error_code, start_dt, end_dt);
                    }

                    int nReturn = -1;
                    using (OdbcCommand comm = new OdbcCommand(sQuery, conn))
                    {
                        nReturn = comm.ExecuteNonQuery();
                        comm.Dispose();
                    }

                    return nReturn;
                }
			}
			catch (InvalidOperationException ioe)
			{
				logger.Error(ioe + "");
				cfg.ReInitializeConnectionPool();
			}
			catch (Exception e)
			{
				logger.Error(e.Message);
			}

			return -1;		
		}

        enum XmlTagType
        {
            xmlNone,
            xmlElement,
            xmlAttributeKey,
            xmlAttributeValue
        };

        class XmlSimpleParser
        {
            int idxOpenTag = -1;
            int idxCloseTag = -1;

            Dictionary<string, string> dicAttributes = new Dictionary<string, string>();

            string currElementName = String.Empty;

            public String LocalName { get { return currElementName; } }
            public Dictionary<string, string> Attributes { get { return dicAttributes; } }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="xmlString"></param>
            /// <returns></returns>
            public bool Read(ref String xmlString)
            {
                int nOld = idxOpenTag;
                while ((idxOpenTag = xmlString.IndexOf('<', nOld + 1)) > nOld)
                {
                    try
                    {
                        char ch = xmlString[idxOpenTag + 1];
                        if (ch == '/' || ch == '?' || ch == '!')
                        {
                            nOld = ++idxOpenTag;
                            continue;
                        }

                    }
                    catch { return false; }

                    XmlTagType type = XmlTagType.xmlElement;
                    currElementName = String.Empty;
                    dicAttributes.Clear();

                    string sKey = String.Empty;
                    string sValue = String.Empty;

                    if ((idxCloseTag = xmlString.IndexOf('>', idxOpenTag)) == -1) return false;

                    /// 요소 이름 및 어트리뷰트 분석
                    for (int i = idxOpenTag + 1; i < idxCloseTag; ++i)
                    {
                        char ch = xmlString[i];

                        /// 값 생성 상태가 아니고 공백이라면
                        if (type != XmlTagType.xmlAttributeValue
                            && ch == ' ')
                        {
                            if (String.IsNullOrEmpty(currElementName)) type = XmlTagType.xmlElement;
                            else if (type == XmlTagType.xmlElement || type == XmlTagType.xmlNone)
                            {
                                type = XmlTagType.xmlAttributeKey;
                            }
                            continue;
                        }
                        /// 값 분석 및 얻어오기
                        else if (type != XmlTagType.xmlAttributeValue
                            && ch == '=')
                        {
                            int nStartValue = xmlString.IndexOfAny(new char[] { '\'', '"' }, i);
                            char sepValue = xmlString[nStartValue];

                            int nEndValue = xmlString.IndexOf(sepValue, nStartValue + 1);

                            for (int n = nStartValue + 1; n < nEndValue; ++n)
                            {
                                sValue += xmlString[n];
                            }

                            dicAttributes.Add(sKey, sValue);

                            type = XmlTagType.xmlNone;
                            sKey = String.Empty;
                            sValue = String.Empty;
                            i = nEndValue;
                            continue;

                        }

                        switch (type)
                        {
                            case XmlTagType.xmlAttributeKey:
                                sKey += ch;
                                break;
                            case XmlTagType.xmlElement:
                                currElementName += ch;
                                break;
                        }

                    }
                    return true;
                }


                return false;
            }

            public XmlSimpleParser()
            {
                dicAttributes.Clear();
            }
        };

		/// <summary>
		/// 로그를 생성한다.
		/// </summary>
		/// <param name="xmlLogs">로그 정보 XML</param>
		/// <returns></returns>
		public int InsertDetailLogs(string xmlLogs)
		{
			int nReturn = -1;

			Config cfg = Config.GetConfig;
			String sQuery = String.Empty;

            try
            {
                DateTime dtStart = DateTime.Now;

                XmlSimpleParser parser = new XmlSimpleParser();

//				XmlDocument doc = new XmlDocument();
//				doc.LoadXml(xmlLogs);

//                XmlNode nodeHead = doc.LastChild;

  //              if (nodeHead == null) logger.Error(doc.OuterXml);

                if (!parser.Read(ref xmlLogs)) logger.Error("XML Parsing Error: return False");

                if (parser.LocalName == "Logs")
                {
                    #region Client Logs

                    try
                    {
                        //XmlNodeList list = doc.SelectNodes("//child::Log");


                        //if (list != null)
                        {
							//OdbcConnection conn = cfg.serverDBLayer.GetAvailableLogConnection();
                            using (OdbcConnection conn = cfg.serverDBLayer.GetAvailableLogConnection())
							{
                                while(parser.Read(ref xmlLogs))
//								foreach (XmlNode node in list)
								{
                                    if (!parser.LocalName.Equals("Log")) continue;

									#region 테이블 분기
									String sTableName = this.GetLogTableName(Convert.ToInt32(parser.Attributes["logcd"]));
									#endregion

									if (sTableName.Equals("downloadlogs"))
									{
                                        string uuid = parser.Attributes["uuid"];

										if (String.IsNullOrEmpty(uuid))
											sQuery = String.Format("INSERT INTO downloadlogs (pid, logcd, uuid, tasknm, contentnm, errorcd, start_dt, end_dt) VALUES ('{0}', {1}, {2}, '{3}', '{4}', {5}, {6}, {7})",
                                                parser.Attributes["pid"], parser.Attributes["logcd"], "null", parser.Attributes["name"].Replace('\'', ' ').Replace('\'', ' '), parser.Attributes["description"].Replace('\'', ' '),
                                                parser.Attributes["error_code"], parser.Attributes["start_dt"], parser.Attributes["end_dt"]);
										else
											sQuery = String.Format("INSERT INTO downloadlogs (pid, logcd, uuid, tasknm, contentnm, errorcd, start_dt, end_dt) VALUES ('{0}', {1}, '{2}', '{3}', '{4}', {5}, {6}, {7})",
                                                parser.Attributes["pid"], parser.Attributes["logcd"], uuid, parser.Attributes["name"].Replace('\'', ' '), parser.Attributes["description"].Replace('\'', ' '),
                                                parser.Attributes["error_code"], parser.Attributes["start_dt"], parser.Attributes["end_dt"]);
									}
									else if (sTableName.Equals("managerlogs"))
									{
										sQuery = String.Format("INSERT INTO managerlogs (usernm, logcd, uuid, actionnm, actiondesc, errorcd, reg_dt) VALUES ('{0}', {1}, '{2}', '{3}', '{4}', {5}, {6})",
											parser.Attributes["pid"], parser.Attributes["logcd"], parser.Attributes["uuid"], parser.Attributes["name"].Replace('\'', ' '), parser.Attributes["description"].Replace('\'', ' '),
											parser.Attributes["error_code"], parser.Attributes["start_dt"]);
									}
									else if (sTableName.Equals("connectionlogs"))
									{
										sQuery = String.Format("INSERT INTO connectionlogs (pid, logcd, errorcd, start_dt, reg_dt) VALUES ('{0}', {1}, {3}, {4}, {5})",
											parser.Attributes["pid"], parser.Attributes["logcd"], parser.Attributes["uuid"], parser.Attributes["error_code"], parser.Attributes["start_dt"], parser.Attributes["end_dt"]);
									}
									else if (sTableName.Equals("playlogs"))
									{
                                        //  hsshin  GS Caltex의 경우 playlogs_today에 기록하도록 되어있음
                                        //sTableName = "playlogs_today";


										string screen_id = parser.Attributes["screen_id"] == "-1" ? "null" : parser.Attributes["screen_id"];
										string relay_id = parser.Attributes["relay_id"] == "-1" ? "null" : parser.Attributes["relay_id"];
										string uuid = parser.Attributes["uuid"];

										if (String.IsNullOrEmpty(uuid))
                                            sQuery = String.Format("INSERT INTO playlogs (pid, logcd, uuid, tasknm, screennm, errorcd, screen_id, relay_id, start_dt, end_dt) VALUES ('{0}', {1}, {2}, '{3}', '{4}', {5}, {6}, {7}, {8}, {9})",
											parser.Attributes["pid"], parser.Attributes["logcd"], "null", parser.Attributes["name"].Replace('\'', ' '), parser.Attributes["description"].Replace('\'', ' '),
											parser.Attributes["error_code"], screen_id, relay_id, parser.Attributes["start_dt"], parser.Attributes["end_dt"]);
										else
                                            sQuery = String.Format("INSERT INTO playlogs (pid, logcd, uuid, tasknm, screennm, errorcd, screen_id, relay_id, start_dt, end_dt) VALUES ('{0}', {1}, '{2}', '{3}', '{4}', {5}, {6}, {7}, {8}, {9})",
											parser.Attributes["pid"], parser.Attributes["logcd"], uuid, parser.Attributes["name"].Replace('\'', ' '), parser.Attributes["description"].Replace('\'', ' '),
											parser.Attributes["error_code"], screen_id, relay_id, parser.Attributes["start_dt"], parser.Attributes["end_dt"]);

									}

									try
									{
										using (OdbcCommand comm = new OdbcCommand(sQuery, conn))
										{
											nReturn = comm.ExecuteNonQuery();
											comm.Dispose();
										}
									}
									catch (InvalidOperationException ioe)
									{
										conn.Close();
										conn.Dispose();
										throw ioe;
									}
									catch (OdbcException oe)
									{
										logger.Error(oe.Message + " query : " + sQuery);

										if (oe.Message.Contains("hstmt"))
										{
											conn.Close();
											conn.Dispose();
											throw oe;
										}
									}
									catch (Exception e)
									{
										logger.Error(e.Message + " query : " + sQuery);
									}
								}
								conn.Close();
								conn.Dispose();
							}
                        }
                        double dTaken = ((TimeSpan)(DateTime.Now - dtStart)).TotalSeconds;
                        if (dTaken > 5) logger.Info(String.Format("InsertLog takes {0} seconds", dTaken));

                        return nReturn;
                    }
                    catch (InvalidOperationException ioe)
                    {
                        logger.Error(ioe + " query : " + sQuery);
                        cfg.ReInitializeConnectionPool();

                        nReturn = -1;
                    }
                    catch (Exception e)
                    {
                        logger.Error(e.Message + " query : " + sQuery);
                    }
                    #endregion
                }
                else if (parser.LocalName == "TargetAdInfoLogs")
                {
                    #region Target Ad Info Logs
                    try
                    {
                        //XmlNodeList list = doc.SelectNodes("//child::Log");

                        //if (list != null)
                        {
                            using (OdbcConnection conn = cfg.serverDBLayer.GetAvailableLogConnection())
                            {

                                //foreach (XmlNode node in list)
                                while (parser.Read(ref xmlLogs))
                                {
                                    if (!parser.LocalName.Equals("Log")) continue;

                                    try
                                    {
                                        sQuery = String.Format("INSERT INTO relaylogs_detail (relay_id, r_key, r_value) VALUES ({0}, '{1}', '{2}')",
                                            parser.Attributes["relay_id"], parser.Attributes["r_key"], parser.Attributes["r_value"]);

                                        using (OdbcCommand comm = new OdbcCommand(sQuery, conn))
                                        {
                                            nReturn = comm.ExecuteNonQuery();
                                            comm.Dispose();
                                        }
                                    }
                                    catch (InvalidOperationException ioe)
                                    {
                                        throw ioe;
                                    }
                                    catch (Exception ex)
                                    {
                                        if (ex.Message.Contains("hstmt"))
                                        {
                                            throw ex;
                                        }

                                        logger.Error(ex.Message + " " + sQuery);
                                    }
                                }
                            }
                        }
                        double dTaken = ((TimeSpan)(DateTime.Now - dtStart)).TotalSeconds;
                        if (dTaken > 5) logger.Info(String.Format("Insert Target Ad Info Log takes {0} seconds", dTaken));

                        return nReturn;
                    }
                    catch (InvalidOperationException ioe)
                    {
                        logger.Error(ioe + " query : " + sQuery);
                        cfg.ReInitializeConnectionPool();
                        nReturn = -1;
                    }
                    catch (Exception e)
                    {
                        logger.Error(e.Message + " query : " + sQuery);
                    }                   
                    #endregion
                }
                else if (parser.LocalName == "TargetAdSuccessLogs")
                {
                    #region Target Ad Success Logs
                    try
                    {
                        //XmlNodeList list = doc.SelectNodes("//child::Log");

                        //if (list != null)
                        {
                            using (OdbcConnection conn = cfg.serverDBLayer.GetAvailableLogConnection())
                            {

                                //foreach (XmlNode node in list)
                                while (parser.Read(ref xmlLogs))
                                {
                                    if (!parser.LocalName.Equals("Log")) continue;


                                    String sLocType = GetTargetAdColumn(Convert.ToInt32(parser.Attributes["target_tp"]));
                                    String sRelayID = parser.Attributes["relay_id"];

                                    sQuery = String.Format("UPDATE relaylogs SET {1} = {2} WHERE relay_id = {0}",
                                        parser.Attributes["relay_id"], sLocType, parser.Attributes["target_ts"]);

                                    try
                                    {
                                        using (OdbcCommand comm = new OdbcCommand(sQuery, conn))
                                        {
                                            nReturn = comm.ExecuteNonQuery();
                                            comm.Dispose();
                                        }
                                    }
                                    catch (InvalidOperationException ioe)
                                    {
                                        throw ioe;
                                    }
                                    catch (Exception e)
                                    {
                                        if (e.Message.Contains("hstmt"))
                                        {
                                            throw e;
                                        }
                                        logger.Error(e.Message + " query : " + sQuery);
                                    }
                                }

                            }
                        }
                        double dTaken = ((TimeSpan)(DateTime.Now - dtStart)).TotalSeconds;
                        if (dTaken > 5) logger.Info(String.Format("Insert Target Ad Success Log takes {0} seconds", dTaken));

                        return nReturn;
                    }
                    catch (InvalidOperationException ioe)
                    {
                        logger.Error(ioe + " query : " + sQuery);
                        cfg.ReInitializeConnectionPool();
                        nReturn = -1;
                    }
                    catch (Exception e)
                    {
                        logger.Error(e.Message + " query : " + sQuery);
                    }      
                    #endregion
                }
                else if (parser.LocalName == "TargetAdErrorLogs")
                {
                    #region Target Ad Error Logs
                    try
                    {
                        //XmlNodeList list = doc.SelectNodes("//child::Log");

                        //if (list != null)
                        {
                            using (OdbcConnection conn = cfg.serverDBLayer.GetAvailableLogConnection())
                            {
                                //foreach (XmlNode node in list)
                                while (parser.Read(ref xmlLogs))
                                {
                                    if (!parser.LocalName.Equals("Log")) continue;

                                    String sLocType = GetTargetAdColumn(Convert.ToInt32(parser.Attributes["error_tp"]));
                                    String sRelayID = parser.Attributes["relay_id"];

                                    for (int i = 0; i < 2; ++i)
                                    {
                                        sQuery = String.Format("INSERT INTO relaylogs_error (relay_id, err_tp, err_cd, err_ts) VALUES ({0}, {1}, {2}, {3})",
                                            parser.Attributes["relay_id"], parser.Attributes["error_tp"], parser.Attributes["error_cd"], parser.Attributes["error_ts"]);

                                        try
                                        {
                                            using (OdbcCommand comm = new OdbcCommand(sQuery, conn))
                                            {
                                                nReturn = comm.ExecuteNonQuery();
                                                comm.Dispose();
                                            }
                                            break;
                                        }
                                        catch (InvalidOperationException ioe)
                                        {
                                            throw ioe;
                                        }
                                        catch (Exception e)
                                        {
                                            if (e.Message.Contains("hstmt"))
                                            {
                                                throw e;
                                            }

                                            logger.Error(e.Message + " query : " + sQuery);
                                        }

                                        try
                                        {
                                            cfg.InsertTargetAd(Convert.ToInt64(sRelayID), parser.Attributes["pid"]);
                                        }
                                        catch (Exception errInsert)
                                        {
                                            logger.Error("Insert Target Ad Error: " + errInsert.Message);
                                            break;
                                        }
                                    }

                                }
                            }
                        }
                        double dTaken = ((TimeSpan)(DateTime.Now - dtStart)).TotalSeconds;
                        if (dTaken > 5) logger.Info(String.Format("Insert Target Ad Error Log takes {0} seconds", dTaken));

                        return nReturn;
                    }
                    catch (InvalidOperationException ioe)
                    {
                        logger.Error(ioe + " query : " + sQuery);
                        cfg.ReInitializeConnectionPool();
                        nReturn = -1;
                    }
                    catch (Exception e)
                    {
                        logger.Error(e.Message + " query : " + sQuery);
                    }      

                    #endregion
                }
                else if (parser.LocalName == "PromotionInfos")
                {
                    #region Promotion Infos
                    try
                    {
                        //XmlNodeList list = doc.SelectNodes("//child::Promotion");

                        //if (list != null)
                        {
                            using (OdbcConnection conn = cfg.serverDBLayer.GetAvailableLogConnection())
                            {

                                //foreach (XmlNode node in list)
                                while (parser.Read(ref xmlLogs))
                                {
                                    if (!parser.LocalName.Equals("Log")) continue;

                                    for (int i = 0; i < 2; ++i)
                                    {
                                        sQuery = String.Format("INSERT INTO promotions (screen_id, receiver, reserved, regdt) VALUES ({0}, '{1}', '{2}', {3})",
                                            parser.Attributes["screen_id"], parser.Attributes["receiver"], parser.Attributes["reserved"], parser.Attributes["regdt"]);

                                        try
                                        {
                                            using (OdbcCommand comm = new OdbcCommand(sQuery, conn))
                                            {
                                                nReturn = comm.ExecuteNonQuery();
                                                comm.Dispose();
                                            }
                                            break;
                                        }
                                        catch (InvalidOperationException ioe)
                                        {
                                            throw ioe;
                                        }
                                        catch (Exception ex)
                                        {
                                            if (ex.Message.Contains("hstmt"))
                                            {
                                                throw ex;
                                            }

                                            logger.Error(ex.Message + " query : " + sQuery);
                                        }
                                    }
                                }
                            }
                        }
                        double dTaken = ((TimeSpan)(DateTime.Now - dtStart)).TotalSeconds;
                        if (dTaken > 5) logger.Info(String.Format("Insert Promotions takes {0} seconds", dTaken));

                        return nReturn;
                    }
                    catch (InvalidOperationException ioe)
                    {
                        logger.Error(ioe + " query : " + sQuery);
                        cfg.ReInitializeConnectionPool();
                        nReturn = -1;
                    }
                    catch (Exception e)
                    {
                        logger.Error(e.Message + " query : " + sQuery);
                    }

                    #endregion
                }
                else
                    throw new NotImplementedException();


            }
            catch (Exception e)
            {
                logger.Error(e.ToString());
            }

            return nReturn;

		}

        /// <summary>
        /// 타겟 광고 컬럼 가져오기
        /// </summary>
        /// <param name="nLocType"></param>
        /// <returns></returns>
       static public String GetTargetAdColumn(int nLocType)
        {
            switch ((TargetAd_ErrorType)nLocType)
            {
                case TargetAd_ErrorType.TargetAd_AppServer_ToLower:
                    return "app_srv_down_ts";
                case TargetAd_ErrorType.TargetAd_AppServer_ToUpper:
                    return "app_srv_up_ts";
                case TargetAd_ErrorType.TargetAd_Client_ToLower:
                case TargetAd_ErrorType.TargetAd_Device_ToLower:
                    return "client_down_ts";
                case TargetAd_ErrorType.TargetAd_Client_ToUpper:
                case TargetAd_ErrorType.TargetAd_Device_ToUpper:
                    return "client_up_ts";
                case TargetAd_ErrorType.TargetAd_RelayServer_ToLower:
                    return "relay_srv_down_ts";
                case TargetAd_ErrorType.TargetAd_RelayServer_ToUpper:
                    return "relay_srv_up_ts";
            }

            throw new NotSupportedException();
        }

		/// <summary>
		/// 로그 테이블 이름을 가져온다
		/// </summary>
		/// <param name="nCode"></param>
		/// <returns></returns>
		private String GetLogTableName(int nCode)
		{
			if ((nCode & (int)LogType.LogType_Download) > 0)
			{
				return "downloadlogs";
			}
			else if ((nCode & (int)LogType.LogType_Managing) > 0)
			{
				return "managerlogs";
			}
			else if ((nCode & (int)LogType.LogType_Connection) > 0)
			{
				return "connectionlogs";
			}
			else if ((nCode & (int)LogType.LogType_TimeSched) > 0)
			{
				return "playlogs";
			}
			else if ((nCode & (int)LogType.LogType_SubtitleSched) > 0)
			{
				return "playlogs";
			}
			else if ((nCode & (int)LogType.LogType_UrgentSched) > 0)
			{
				return "playlogs";
			}
			else if ((nCode & (int)LogType.LogType_RollingSched) > 0)
			{
				return "playlogs";
			}
			else if ((nCode & (int)LogType.LogType_ControlSched) > 0)
			{
				return "playlogs";
			}
            else if ((nCode & (int)LogType.LogType_TargetAd) > 0)
            {
                return "relaylogs";
            }
            else return "managerlogs";
		}

        /// <summary>
		/// 로그를 조회한다
		/// </summary>
		/// <param name="player_ids"></param>
		/// <param name="codes"></param>
		/// <param name="from"></param>
		/// <param name="to"></param>
		/// <param name="limit"></param>
		/// <param name="offset"></param>
		/// <returns></returns>
        public ServerDataSet.tblLogsDataTable GetDetailLogsFromTo(string[] player_ids, int[] codes, long from, long to, int limit, int offset)
        {
            return GetDetailLogsFromTo(null, player_ids, codes, from, to, limit, offset);
        }
        /// <summary>
        /// 로그를 조회한다
        /// </summary>
        /// <param name="uuid"></param>
        /// <param name="player_ids"></param>
        /// <param name="codes"></param>
        /// <param name="from"></param>
        /// <param name="to"></param>
        /// <param name="limit"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
		public ServerDataSet.tblLogsDataTable GetDetailLogsFromTo(string uuid, string[] player_ids, int[] codes, long from, long to, int limit, int offset)
		{
			Config cfg = Config.GetConfig;
            String sQuery = String.Empty;
            try
			{
               
				#region 테이블 분기
				String sTableName = "playlogs";
				#endregion

				String sPlayerID = "";
				if (player_ids.Length > 1)
				{
					foreach(string pid in player_ids)
					{
						sPlayerID += String.Format("'{0}',", pid);
					}

					sPlayerID = sPlayerID.TrimEnd(',');
				}
				else
				{
					sPlayerID = String.Format("'{0}'", player_ids[0]);
				}

				String sCode = "";

				if (codes.Length > 1)
				{
					foreach (int code in codes)
					{
						sCode += code;
						sCode += ",";

						sTableName = GetLogTableName(code);
					}

					sCode = sCode.TrimEnd(',');
				}
				else
				{
					sCode = codes[0].ToString();
					sTableName = GetLogTableName(codes[0]);
				}

				if (sTableName.Equals("downloadlogs"))
                {
                    if (cfg.serverDBLayer.LogDBType == DBLayer.DBTypeInfo.MSSQL)
                    {
                        #region MSSQL
                        if (uuid == null)
                            sQuery = String.Format("SELECT pid, logcd, uuid, tasknm as name, contentnm as description, errorcd, start_dt, end_dt FROM (SELECT ROW_NUMBER() over (order by start_dt DESC) AS ROW_NUM, {0}.* FROM {0} WHERE (pid IN ({1})) AND (logcd IN ({2})) AND (start_dt >= {3}) AND (start_dt <= {4})) AS LOGS WHERE LOGS.ROW_NUM > {5} AND LOGS.ROW_NUM <= {5} + {6}", sTableName, sPlayerID, sCode, from, to, offset, limit);
                        else
                            sQuery = String.Format("SELECT pid, logcd, uuid, tasknm as name, contentnm as description, errorcd, start_dt, end_dt FROM (SELECT ROW_NUMBER() over (order by start_dt DESC) AS ROW_NUM, {0}.* FROM {0} WHERE (uuid = '{7}') AND (pid IN ({1})) AND (logcd IN ({2})) AND (start_dt >= {3}) AND (start_dt <= {4})) AS LOGS WHERE LOGS.ROW_NUM > {5} AND LOGS.ROW_NUM <= {5} + {6}", sTableName, sPlayerID, sCode, from, to, offset, limit, uuid);

                        #endregion
                    }
                    else
                    {
                        #region MYSQL, SQLite
                        if (uuid == null)
                            sQuery = String.Format("SELECT pid, logcd, uuid, tasknm as name, contentnm as description, errorcd, start_dt, end_dt FROM {0} WHERE (pid IN ({1})) AND (logcd IN ({2})) AND (start_dt >= {3}) AND (start_dt <= {4}) ORDER BY start_dt DESC LIMIT {5}, {6}", sTableName, sPlayerID, sCode, from, to, offset, limit);
                        else
                            sQuery = String.Format("SELECT pid, logcd, uuid, tasknm as name, contentnm as description, errorcd, start_dt, end_dt FROM {0} WHERE (uuid = '{7}') AND (pid IN ({1})) AND (logcd IN ({2})) AND (start_dt >= {3}) AND (start_dt <= {4}) ORDER BY start_dt DESC LIMIT {5}, {6}", sTableName, sPlayerID, sCode, from, to, offset, limit, uuid);
                        #endregion
                    }
                }
				else if (sTableName.Equals("managerlogs"))
				{
                    if (cfg.serverDBLayer.LogDBType == DBLayer.DBTypeInfo.MSSQL)
                    {
                        #region MSSQL
                        sQuery = String.Format("SELECT usernm as pid, logcd, uuid, actionnm as name, actiondesc as description, errorcd, reg_dt as start_dt, reg_dt as end_dt FROM (SELECT ROW_NUMBER() over (order by start_dt DESC) AS ROW_NUM, {0}.* FROM {0} WHERE (logcd IN ({2})) AND (start_dt >= {3}) AND (start_dt <= {4})) AS LOGS WHERE LOGS.ROW_NUM > {5} AND LOGS.ROW_NUM <= {5} + {6}", sTableName, sPlayerID, sCode, from, to, offset, limit);
                        #endregion
                    }
                    else
                    {
                        #region MYSQL, SQLite
                        sQuery = String.Format("SELECT usernm as pid, logcd, uuid, actionnm as name, actiondesc as description, errorcd, reg_dt as start_dt, reg_dt as end_dt FROM {0} WHERE (logcd IN ({2})) AND (reg_dt >= {3}) AND (reg_dt <= {4}) ORDER BY reg_dt DESC LIMIT {5}, {6}", sTableName, sPlayerID, sCode, from, to, offset, limit);
                        #endregion
                    }
                }
				else if (sTableName.Equals("connectionlogs"))
				{
                    if (cfg.serverDBLayer.LogDBType == DBLayer.DBTypeInfo.MSSQL)
                    {
                        #region MSSQL
                        sQuery = String.Format("SELECT pid, logcd, errorcd, start_dt, end_dt, reg_dt FROM (SELECT ROW_NUMBER() over (order by reg_dt DESC) AS ROW_NUM, {0}.* FROM {0} WHERE (pid IN ({1})) AND (logcd IN ({2})) AND (reg_dt >= {3}) AND (reg_dt <= {4})) AS LOGS WHERE LOGS.ROW_NUM > {5} AND LOGS.ROW_NUM <= {5} + {6}", sTableName, sPlayerID, sCode, from, to, offset, limit);
                        #endregion
                    }
                    else
                    {
                        #region MYSQL, SQLite
                        sQuery = String.Format("SELECT pid, logcd, errorcd, start_dt, end_dt, reg_dt FROM {0} WHERE (pid IN ({1})) AND (logcd IN ({2})) AND (reg_dt >= {3}) AND (reg_dt <= {4}) ORDER BY reg_dt DESC LIMIT {5}, {6}", sTableName, sPlayerID, sCode, from, to, offset, limit);
                        #endregion
                    }
                }
				else if (sTableName.Equals("playlogs"))
				{
                    if (cfg.serverDBLayer.LogDBType == DBLayer.DBTypeInfo.MSSQL)
                    {
                        #region MSSQL
                        if (uuid == null)
                            sQuery = String.Format("SELECT pid, logcd, uuid, tasknm as name, screennm as description, errorcd, start_dt, end_dt FROM (SELECT ROW_NUMBER() over (order by start_dt DESC) AS ROW_NUM, {0}.* FROM {0} WHERE (pid IN ({1})) AND (logcd IN ({2})) AND (start_dt >= {3}) AND (start_dt <= {4})) AS LOGS WHERE LOGS.ROW_NUM > {5} AND LOGS.ROW_NUM <= {5} + {6}", sTableName, sPlayerID, sCode, from, to, offset, limit);
                        else
                            sQuery = String.Format("SELECT pid, logcd, uuid, tasknm as name, screennm as description, errorcd, start_dt, end_dt FROM (SELECT ROW_NUMBER() over (order by start_dt DESC) AS ROW_NUM, {0}.* FROM {0} WHERE (uuid = '{7}') AND (pid IN ({1})) AND (logcd IN ({2})) AND (start_dt >= {3}) AND (start_dt <= {4})) AS LOGS WHERE LOGS.ROW_NUM > {5} AND LOGS.ROW_NUM <= {5} + {6}", sTableName, sPlayerID, sCode, from, to, offset, limit, uuid);
                        #endregion
                    }
                    else
                    {
                        #region MYSQL, SQLite
                        if (uuid == null)
                            sQuery = String.Format("SELECT pid, logcd, uuid, tasknm as name, screennm as description, errorcd, start_dt, end_dt FROM {0} WHERE (pid IN ({1})) AND (logcd IN ({2})) AND (start_dt >= {3}) AND (start_dt <= {4}) ORDER BY start_dt DESC LIMIT {5}, {6}", sTableName, sPlayerID, sCode, from, to, offset, limit);
                        else
                            sQuery = String.Format("SELECT pid, logcd, uuid, tasknm as name, screennm as description, errorcd, start_dt, end_dt FROM {0} WHERE (uuid = '{7}') AND (pid IN ({1})) AND (logcd IN ({2})) AND (start_dt >= {3}) AND (start_dt <= {4}) ORDER BY start_dt DESC LIMIT {5}, {6}", sTableName, sPlayerID, sCode, from, to, offset, limit, uuid);
                        #endregion
                    }
                }
                else if (sTableName.Equals("relaylogs"))
                {
                    if (cfg.serverDBLayer.LogDBType == DBLayer.DBTypeInfo.MSSQL)
                    {
                        #region MSSQL
                        if (uuid == null)
                            sQuery = String.Format("SELECT pid, logcd, uuid, tasknm as name, screennm as description, errorcd, start_dt, end_dt ,STUFF((SELECT ', ' + CAST(r_value as VARCHAR(MAX)) FROM relaylogs_detail a WHERE a.relay_id = LOGS.relay_id FOR XML PATH('')),1,2,'') AS tags FROM (SELECT ROW_NUMBER() over (order by start_dt DESC) AS ROW_NUM, playlogs.* FROM playlogs WHERE (relay_id IS NOT NULL) AND (pid IN ({1})) AND (start_dt >= {3}) AND (start_dt <= {4})) AS LOGS WHERE LOGS.ROW_NUM > {5} AND LOGS.ROW_NUM <= {5} + {6};", sTableName, sPlayerID, sCode, from, to, offset, limit);
                        else
                            sQuery = String.Format("SELECT pid, logcd, uuid, tasknm as name, screennm as description, errorcd, start_dt, end_dt ,STUFF((SELECT ', ' + CAST(r_value as VARCHAR(MAX)) FROM relaylogs_detail a WHERE a.relay_id = LOGS.relay_id FOR XML PATH('')),1,2,'') AS tags FROM (SELECT ROW_NUMBER() over (order by start_dt DESC) AS ROW_NUM, playlogs.* FROM playlogs WHERE (relay_id IS NOT NULL) AND (uuid = '{7}') AND (pid IN ({1})) AND (start_dt >= {3}) AND (start_dt <= {4})) AS LOGS WHERE LOGS.ROW_NUM > {5} AND LOGS.ROW_NUM <= {5} + {6};", sTableName, sPlayerID, sCode, from, to, offset, limit, uuid);
                        #endregion
                    }
                    else
                    {
                        #region MYSQL, SQLite
                        if (uuid == null)
                            sQuery = String.Format("SELECT pid, logcd, uuid, tasknm as name, screennm as description, errorcd, start_dt, end_dt ,STUFF((SELECT ', ' + CAST(r_value as VARCHAR(MAX)) FROM relaylogs_detail a WHERE a.relay_id = LOGS.relay_id FOR XML PATH('')),1,2,'') AS tags FROM (SELECT ROW_NUMBER() over (order by start_dt DESC) AS ROW_NUM, playlogs.* FROM playlogs WHERE (relay_id IS NOT NULL) AND (pid IN ({1})) AND (start_dt >= {3}) AND (start_dt <= {4})) AS LOGS WHERE LOGS.ROW_NUM > {5} AND LOGS.ROW_NUM <= {5} + {6};", sTableName, sPlayerID, sCode, from, to, offset, limit);
                        else
                            sQuery = String.Format("SELECT pid, logcd, uuid, tasknm as name, screennm as description, errorcd, start_dt, end_dt ,STUFF((SELECT ', ' + CAST(r_value as VARCHAR(MAX)) FROM relaylogs_detail a WHERE a.relay_id = LOGS.relay_id FOR XML PATH('')),1,2,'') AS tags FROM (SELECT ROW_NUMBER() over (order by start_dt DESC) AS ROW_NUM, playlogs.* FROM playlogs WHERE (relay_id IS NOT NULL) AND (uuid = '{7}') AND (pid IN ({1})) AND (start_dt >= {3}) AND (start_dt <= {4})) AS LOGS WHERE LOGS.ROW_NUM > {5} AND LOGS.ROW_NUM <= {5} + {6};", sTableName, sPlayerID, sCode, from, to, offset, limit, uuid);
                        #endregion
                    }

                }

				using (ServerDataSet.tblLogsDataTable dt = new ServerDataSet.tblLogsDataTable())
				{
                    using(OdbcConnection conn = cfg.serverDBLayer.GetAvailableConnection())
                    {
                        using (OdbcCommand sql = new OdbcCommand(sQuery, conn))
					    {
						    //initializing database structure
						    using (OdbcDataReader dr = sql.ExecuteReader())
						    {

							    dt.Load(dr);

							    dr.Close();
							    dr.Dispose();
						    }

						    sql.Dispose();

						    return dt;
					    }
                    }
				}
			}
			catch (InvalidOperationException ioe)
			{
				logger.Error(ioe + "");
				cfg.ReInitializeConnectionPool();
			}
			catch (Exception e)
			{
                logger.Error(e.Message + " Query: " + sQuery);
			}

			return null;
        }

        #region 보고서 관련

        /// <summary>
        /// 타겟 광고 고객 반응 보고 자료를 반환한다.
        /// </summary>
        /// <param name="yyyy"></param>
        /// <param name="mm"></param>
        /// <param name="dd"></param>
        /// <param name="g_name"></param>
        /// <returns></returns>
        public ServerDataSet.targetreportsDataTable GetTargetReportDataByYYYYMMDD(int yyyy, int mm, int dd, string g_name)
        {
            Config cfg = Config.GetConfig;
            
            try
            {
                using (ServerDatabase.ServerDataSetTableAdapters.targetreportsTableAdapter da = cfg.serverDBLayer.CreateTargetReportInfoDA())
                {
                    using (ServerDataSet.targetreportsDataTable dt = da.GetDataByYYYYMMDD(yyyy.ToString("D4"), mm.ToString("D2"), dd.ToString("D2"), g_name))
                    {
                        da.Dispose();
                        return dt;
                    }
                }
            }
            catch (InvalidOperationException ioe)
            {
                logger.Error(ioe + "");
                cfg.ReInitializeConnectionPool();
            }
            catch (Exception e)
            {
                logger.Error(e.Message);
            }

            return null;
        }

        /// <summary>
        /// 타겟 광고 고객 반응 보고 자료를 반환한다.
        /// </summary>
        /// <param name="yyyy"></param>
        /// <param name="mm"></param>
        /// <param name="g_name"></param>
        /// <returns></returns>
        public ServerDataSet.targetreportsDataTable GetTargetReportDataByYYYYMM(int yyyy, int mm, string g_name)
        {
            Config cfg = Config.GetConfig;

            try
            {
                using (ServerDatabase.ServerDataSetTableAdapters.targetreportsTableAdapter da = cfg.serverDBLayer.CreateTargetReportInfoDA())
                {
                    using (ServerDataSet.targetreportsDataTable dt = da.GetDataByYYYYMM(yyyy.ToString("D4"), mm.ToString("D2"), g_name))
                    {
                        da.Dispose();
                        return dt;
                    }
                }
            }
            catch (InvalidOperationException ioe)
            {
                logger.Error(ioe + "");
                cfg.ReInitializeConnectionPool();
            }
            catch (Exception e)
            {
                logger.Error(e.Message);
            }

            return null;
        }

        /// <summary>
        /// 타겟 광고 고객 반응 보고 자료를 반환한다.
        /// </summary>
        /// <param name="yyyy"></param>
        /// <param name="g_name"></param>
        /// <returns></returns>
        public ServerDataSet.targetreportsDataTable GetTargetReportDataByYYYY(int yyyy, string g_name)
        {
            Config cfg = Config.GetConfig;

            try
            {
                using (ServerDatabase.ServerDataSetTableAdapters.targetreportsTableAdapter da = cfg.serverDBLayer.CreateTargetReportInfoDA())
                {
                    using (ServerDataSet.targetreportsDataTable dt = da.GetDataByYYYY(yyyy.ToString("D4"), g_name))
                    {
                        da.Dispose();
                        return dt;
                    }
                }
            }
            catch (InvalidOperationException ioe)
            {
                logger.Error(ioe + "");
                cfg.ReInitializeConnectionPool();
            }
            catch (Exception e)
            {
                logger.Error(e.Message);
            }

            return null;
        }

        /// <summary>
        /// 재생 통계 보고 자료를 반환한다.
        /// </summary>
        /// <param name="yyyy"></param>
        /// <param name="mm"></param>
        /// <param name="dd"></param>
        /// <returns></returns>
        public ServerDataSet.playreportsDataTable GetPlayReportDataByYYYYMMDD(int yyyy, int mm, int dd)
        {
            Config cfg = Config.GetConfig;

            try
            {
                using (ServerDatabase.ServerDataSetTableAdapters.playreportsTableAdapter da = cfg.serverDBLayer.CreatePlayReportInfoDA())
                {
                    using (ServerDataSet.playreportsDataTable dt = da.GetDataByYYYYMMDD(yyyy.ToString("D4"), mm.ToString("D2"), dd.ToString("D2")))
                    {
                        da.Dispose();
                        return dt;
                    }
                }
            }
            catch (InvalidOperationException ioe)
            {
                logger.Error(ioe + "");
                cfg.ReInitializeConnectionPool();
            }
            catch (Exception e)
            {
                logger.Error(e.Message);
            }

            return null;
        }

        /// <summary>
        /// 재생 통계 보고 자료를 반환한다.
        /// </summary>
        /// <param name="yyyy"></param>
        /// <param name="mm"></param>
        /// <returns></returns>
        public ServerDataSet.playreportsDataTable GetPlayReportDataByYYYYMM(int yyyy, int mm)
        {
            Config cfg = Config.GetConfig;

            try
            {
                using (ServerDatabase.ServerDataSetTableAdapters.playreportsTableAdapter da = cfg.serverDBLayer.CreatePlayReportInfoDA())
                {
                    using (ServerDataSet.playreportsDataTable dt = da.GetDataByYYYYMM(yyyy.ToString("D4"), mm.ToString("D2")))
                    {
                        da.Dispose();
                        return dt;
                    }
                }
            }
            catch (InvalidOperationException ioe)
            {
                logger.Error(ioe + "");
                cfg.ReInitializeConnectionPool();
            }
            catch (Exception e)
            {
                logger.Error(e.Message);
            }

            return null;
        }

        /// <summary>
        /// 재생 통계 보고 자료를 반환한다.
        /// </summary>
        /// <param name="yyyy"></param>
        /// <returns></returns>
        public ServerDataSet.playreportsDataTable GetPlayReportDataByYYYY(int yyyy)
        {
            Config cfg = Config.GetConfig;

            try
            {
                using (ServerDatabase.ServerDataSetTableAdapters.playreportsTableAdapter da = cfg.serverDBLayer.CreatePlayReportInfoDA())
                {
                    using (ServerDataSet.playreportsDataTable dt = da.GetDataByYYYY(yyyy.ToString("D4")))
                    {
                        da.Dispose();
                        return dt;
                    }
                }
            }
            catch (InvalidOperationException ioe)
            {
                logger.Error(ioe + "");
                cfg.ReInitializeConnectionPool();
            }
            catch (Exception e)
            {
                logger.Error(e.Message);
            }

            return null;
        }

        /// <summary>
        /// DID 운영 보고서 자료를 반환한다.
        /// </summary>
        /// <param name="pids"></param>
        /// <param name="yyyy"></param>
        /// <param name="mm"></param>
        /// <param name="dd"></param>
        /// <returns></returns>
        public ServerDataSet.report_running_timeDataTable GetRunningReportDataByYYYYMMDD(String[] pids, int yyyy, int mm, int dd)
        {
            Config cfg = Config.GetConfig;
            String sQuery = String.Empty;

            try
            {
                #region Player IDs
                String sPlayerID = "";
                if (pids.Length > 1)
                {
                    foreach (string pid in pids)
                    {
                        sPlayerID += String.Format("'{0}',", pid);
                    }

                    sPlayerID = sPlayerID.TrimEnd(',');
                }
                else
                {
                    sPlayerID = String.Format("'{0}'", pids[0]);
                }
                #endregion

                sQuery = String.Format("SELECT pid, YYYY, MM, DD, H24, TM FROM report_running_time WHERE YYYY = '{0}' AND MM = '{1}' AND DD = '{2}' AND pid IN ({3}) ORDER BY pid",
                    yyyy.ToString("D4"), mm.ToString("D2"), dd.ToString("D2"), sPlayerID);

                using (ServerDataSet.report_running_timeDataTable dt = new ServerDataSet.report_running_timeDataTable())
                {
                    using (OdbcConnection conn = cfg.serverDBLayer.GetAvailableConnection())
                    {
                        using (OdbcCommand sql = new OdbcCommand(sQuery, conn))
                        {
                            //initializing database structure
                            using (OdbcDataReader dr = sql.ExecuteReader())
                            {

                                dt.Load(dr);

                                dr.Close();
                                dr.Dispose();
                            }

                            sql.Dispose();

                            return dt;
                        }
                    }
                }
            }
            catch (InvalidOperationException ioe)
            {
                logger.Error(ioe + "");
                cfg.ReInitializeConnectionPool();
            }
            catch (Exception e)
            {
                logger.Error(e.Message + "Query: " + sQuery);
            }

            return null;
        }

        /// <summary>
        /// 플레이어의 일일 상태 요약 자료를 반환한다.
        /// </summary>
        /// <param name="pids"></param>
        /// <param name="yyyy"></param>
        /// <param name="mm"></param>
        /// <param name="dd"></param>
        /// <returns></returns>
        public ServerDataSet.summary_playersDataTable GetPlayerSummaryDataByYYYYMMDD(String[] pids, int yyyy, int mm, int dd)
        {
            Config cfg = Config.GetConfig;
            String sQuery = String.Empty;

            try
            {
                #region Player IDs
                String sPlayerID = "";
                if (pids.Length > 1)
                {
                    foreach (string pid in pids)
                    {
                        sPlayerID += String.Format("'{0}',", pid);
                    }

                    sPlayerID = sPlayerID.TrimEnd(',');
                }
                else
                {
                    sPlayerID = String.Format("'{0}'", pids[0]);
                }
                #endregion

                sQuery = String.Format("SELECT pid, YYYY, MM, DD, H24, gid, name, host, state, uptime, lastconndt, versionH, versionL, freespace, memoryusage, cpurate, volume, serial_power, serial_source, serial_volume FROM summary_players WHERE YYYY = '{0}' AND MM = '{1}' AND DD = '{2}' AND pid IN ({3}) ORDER BY pid",
                    yyyy.ToString("D4"), mm.ToString("D2"), dd.ToString("D2"), sPlayerID);

                using (ServerDataSet.summary_playersDataTable dt = new ServerDataSet.summary_playersDataTable())
                {
                    using (OdbcConnection conn = cfg.serverDBLayer.GetAvailableLogConnection())
                    {
                        using (OdbcCommand sql = new OdbcCommand(sQuery, conn))
                        {
                            //initializing database structure
                            using (OdbcDataReader dr = sql.ExecuteReader())
                            {
                                String prev_pid = String.Empty;

                                string gid = String.Empty;
                                string sYYYY = String.Empty;
                                string sMM = String.Empty;
                                string sDD = String.Empty;
                                string host = String.Empty;
                                string name = String.Empty; 
                                int cntOnline = 0;
                                Int64 startdt = -1;
                                Int64 enddt = -1;
                                int memoryrate = 0;
                                int cpurate = 0;
                                int verH = 0;
                                int verL = 0;
                                int freespace = -1;
                                int volume = -1;
                                string serial_power = String.Empty;
                                string serial_source = String.Empty;
                                int serial_volume = -1;

                                while (dr.Read())
                                {
                                    String pid = dr["pid"] as String;
                                    
                                    if (!String.IsNullOrEmpty(prev_pid) && !prev_pid.Equals(pid))
                                    {
                                        //  PID가 다른 경우
                                        //  1. 로우 삽입
                                        //  2. 변수 초기화

                                        #region 로우 삽입
                                        ServerDataSet.summary_playersRow row = dt.Newsummary_playersRow();
                                        row.memoryusage = cntOnline > 0 ? memoryrate / cntOnline : memoryrate;
                                        row.cpurate = cntOnline > 0 ? cpurate / cntOnline : cpurate;
                                        row.freespace = freespace;
                                        row.volume = volume;
                                        row.versionH = verH;
                                        row.versionL = verL;
                                        row.pid = prev_pid;
                                        row.YYYY = sYYYY;
                                        row.MM = sMM;
                                        row.DD = sDD;
                                        row.H24 = "24";
                                        row.gid = gid;
                                        row.host = host;
                                        row.beginconndt = startdt;
                                        row.lastconndt = enddt;
                                        row.serial_power = serial_power;
                                        row.serial_source = serial_source;
                                        row.serial_volume = serial_volume;
										row.state = cntOnline > 0 ? 1 : 0;
                                        try
                                        {
                                            dt.Rows.Add(row);
                                        }
                                        catch (Exception exInsert) { logger.Error("Insert Row Error: " + exInsert.ToString()); }

                                        #endregion

                                        #region 변수 초기화
                                        gid = String.Empty;
                                        sYYYY = String.Empty;
                                        sMM = String.Empty;
                                        sDD = String.Empty;
                                        host = String.Empty;
                                        name = String.Empty;
                                        cntOnline = 0;
                                        startdt = -1;
                                        enddt = -1;
                                        memoryrate = 0;
                                        cpurate = 0;
                                        verH = 0;
                                        verL = 0;
                                        freespace = -1;
                                        volume = -1;
                                        serial_power = String.Empty;
                                        serial_source = String.Empty;
                                        serial_volume = -1;
                                        #endregion
                                    }

                                    bool bIsOnline = false;

                                    #region 온라인 여부 확인
                                    try
                                    {
                                        if (bIsOnline = (CommonUnit.GetInt(dr["state"]) == 1))
                                        {
                                            cntOnline++;
                                        }
                                    }
                                    catch { }
                                    #endregion

                                    if (bIsOnline)
                                    {
                                        if (startdt == -1)
                                        {
                                            startdt = CommonUnit.GetInt64(dr["lastconndt"]);
                                        }
                                        enddt = -1;

                                        memoryrate += CommonUnit.GetInt(dr["memoryusage"]);
                                        cpurate += CommonUnit.GetInt(dr["cpurate"]);

                                        verH = CommonUnit.GetInt(dr["versionH"]);
                                        verL = CommonUnit.GetInt(dr["versionL"]);
                                        freespace = CommonUnit.GetInt(dr["freespace"]);
                                        volume = CommonUnit.GetInt(dr["volume"]);

                                    }
                                    else
                                    {
                                        enddt = CommonUnit.GetInt64(dr["lastconndt"]);

                                        if (verH == 0) verH = CommonUnit.GetInt(dr["versionH"]);
                                        if (verL == 0) verL = CommonUnit.GetInt(dr["versionL"]);
                                        if (freespace == -1) freespace = CommonUnit.GetInt(dr["freespace"]);
                                        if (volume == -1) volume = CommonUnit.GetInt(dr["volume"]);
                                    }

                                    sYYYY = CommonUnit.GetString(dr["YYYY"]);
                                    sMM = CommonUnit.GetString(dr["MM"]);
                                    sDD = CommonUnit.GetString(dr["DD"]);
                                    host = CommonUnit.GetString(dr["host"]);
                                    name = CommonUnit.GetString(dr["name"]);
                                    serial_power = CommonUnit.GetString(dr["serial_power"]);
                                    serial_source = CommonUnit.GetString(dr["serial_source"]);
                                    serial_volume = CommonUnit.GetInt(dr["serial_volume"]);

                                    prev_pid = pid;
                                    
                                }

                                if (!String.IsNullOrEmpty(prev_pid))
                                {
                                    //  PID가 다른 경우
                                    //  1. 로우 삽입

                                    #region 로우 삽입

                                    ServerDataSet.summary_playersRow row = dt.Newsummary_playersRow();
                                    row.memoryusage = cntOnline > 0 ? memoryrate / cntOnline : memoryrate;
                                    row.cpurate = cntOnline > 0 ? cpurate / cntOnline : cpurate;
                                    row.freespace = freespace;
                                    row.volume = volume;
                                    row.versionH = verH;
                                    row.versionL = verL;
                                    row.pid = prev_pid;
                                    row.YYYY = sYYYY;
                                    row.MM = sMM;
                                    row.DD = sDD;
                                    row.H24 = "24";
                                    row.gid = gid;
                                    row.host = host;
                                    row.name = name;
                                    row.beginconndt = startdt;
                                    row.lastconndt = enddt;
                                    row.serial_power = serial_power;
                                    row.serial_source = serial_source;
                                    row.serial_volume = serial_volume;
									row.state = cntOnline > 0 ? 1 : 0;

                                    try
                                    {
                                        dt.Rows.Add(row);
                                    }
                                    catch (Exception exInsert) { logger.Error("Insert Row Error: " + exInsert.ToString()); }
                                    #endregion
                                }
                                dr.Close();
                                dr.Dispose();
                            }

                            sql.Dispose();
                          

                            return dt;
                        }
                    }
                }
            }
            catch (InvalidOperationException ioe)
            {
                logger.Error(ioe + "");
                cfg.ReInitializeConnectionPool();
            }
            catch (Exception e)
            {
                logger.Error(e.Message + "Query: " + sQuery);
            }

            return null;
        }

        /// <summary>
        /// 일일 스케줄 보고서 자료를 반환한다.
        /// </summary>
        /// <param name="pids"></param>
        /// <param name="yyyy"></param>
        /// <param name="mm"></param>
        /// <param name="dd"></param>
        /// <returns></returns>
        public ServerDataSet.report_time_scheduleDataTable GetScheduleReportDataByYYYYMMDD(String[] pids, int yyyy, int mm, int dd)
        {
            Config cfg = Config.GetConfig;
            String sQuery = String.Empty;

            try
            {
                #region Player IDs
                String sPlayerID = "";
                if (pids.Length > 1)
                {
                    foreach (string pid in pids)
                    {
                        sPlayerID += String.Format("'{0}',", pid);
                    }

                    sPlayerID = sPlayerID.TrimEnd(',');
                }
                else
                {
                    sPlayerID = String.Format("'{0}'", pids[0]);
                }
                #endregion

                sQuery = String.Format("SELECT pid, YYYY, MM, DD, H24, CNT, type FROM report_time_schedule WHERE YYYY = '{0}' AND MM = '{1}' AND DD = '{2}' AND pid IN ({3}) ORDER BY pid",
                    yyyy.ToString("D4"), mm.ToString("D2"), dd.ToString("D2"), sPlayerID);

                using (ServerDataSet.report_time_scheduleDataTable dt = new ServerDataSet.report_time_scheduleDataTable())
                {
                    using (OdbcConnection conn = cfg.serverDBLayer.GetAvailableConnection())
                    {
                        using (OdbcCommand sql = new OdbcCommand(sQuery, conn))
                        {
                            //initializing database structure
                            using (OdbcDataReader dr = sql.ExecuteReader())
                            {

                                dt.Load(dr);

                                dr.Close();
                                dr.Dispose();
                            }

                            sql.Dispose();

                            return dt;
                        }
                    }
                }
            }
            catch (InvalidOperationException ioe)
            {
                logger.Error(ioe + "");
                cfg.ReInitializeConnectionPool();
            }
            catch (Exception e)
            {
                logger.Error(e.Message + "Query: " + sQuery);
            }

            return null;
        }

        public bool MakeStatistics(DateTime startdt, DateTime enddt, bool bDaily, bool bHourly)
        {
            try
            {
                List<IReport> dailyReports = new List<IReport>();
                List<IReport> hourlyReports = new List<IReport>();

                dailyReports.Add(new PlayReport());
                dailyReports.Add(new RelayReport());
                dailyReports.Add(new RunningReport());

                hourlyReports.Add(new ScheduleReport());
                hourlyReports.Add(new PlayerSummary());

                for (DateTime st = startdt; st <= enddt; st += TimeSpan.FromHours(1))
                {
                    if (bHourly)
                    {
                        foreach (IReport r in hourlyReports)
                        {
                            r.MakeHourlyReport(st.Year, st.Month, st.Day, st.Hour);
                        }
                    }

                    if (bDaily & st.Hour == 0)
                    {
                        foreach (IReport r in dailyReports)
                        {
                            r.MakeDailyReport(st.Year, st.Month, st.Day);
                        }
                    }
                }

                return true;
            }
            catch (Exception ex) { logger.Error(ex.ToString()); }

            return false;
        }

        #endregion
    }
}
