﻿using System;
using System.Reflection;
using System.Threading;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Messaging;
using System.Net;

using DigitalSignage.Common;

using NLog;

namespace DigitalSignage.SchedulerService
{
    class ServerCmdReceiver : MarshalByRefObject, IServerCmdReceiver
    {
		public Object ProcessExpandCommand(CmdExReceiverCommands cmd)
		{
			return null;
		}

		/// <summary>
		/// 커맨드에 따라 상태 값을 리턴한다.
		/// CmdEcho : 아무 값이나 리턴
		/// CmdGetTimeStamp : 서버 시간 리턴
		/// CmdGetVersion : 서버 버전 리턴
		/// CmdGetSyncState : 시간 동기를 할 것인지 리턴
		/// </summary>
		/// <param name="cmd"></param>
		/// <returns></returns>
        public long ProcessSimpleCommand(CmdReceiverCommands cmd)
        {
            Logger logger = LogManager.GetCurrentClassLogger();
            Config cfg = Config.GetConfig;
            IPAddress clientIP = (IPAddress)CallContext.GetData("ClientIP");
            long res = -1; //not implemented

            switch (cmd)
            {
                case CmdReceiverCommands.CmdEcho:
                    res = new Random(128).Next(128);
                    break;
				case CmdReceiverCommands.CmdGetGID:
                    {
						res = -1;
                    }
                    break;
                case CmdReceiverCommands.CmdGetTimeStamp:
//                     logger.Info( "CmdGetTimeStamp UTC="+DateTime.UtcNow);
                    res = TimeConverter.ConvertToUTP(DateTime.UtcNow);
                    break;
                case CmdReceiverCommands.CmdGetVersion:
                    Assembly myAssembly = Assembly.GetExecutingAssembly();
                    AssemblyName myAssemblyName = myAssembly.GetName();
                    res = ((long)myAssemblyName.Version.Major << 48) |
                        ((long)myAssemblyName.Version.Minor << 32) |
                        (myAssemblyName.Version.Build << 16) |
                        myAssemblyName.Version.Revision;
                    break;
                case CmdReceiverCommands.CmgGetClientPID:
                    {
						res = -1;
                    }
                    break;
               case CmdReceiverCommands.CmdGetSyncState:
                    res = ServerSettings.TimeSyncMode == true ? 1 : 0;
                    break;
            }
            return res; 
        }
    }
}
