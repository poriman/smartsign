﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;

using DigitalSignage.Common;
using DigitalSignage.ServerDatabase;
using NLog;
//SQLite & DB routines
using System.Data.Common;

namespace DigitalSignage.SchedulerService
{
    public class UsersList : MarshalByRefObject, IUsersList
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

		public ServerDataSet.rolesDataTable GetAvailableRoleList(string own_role_name, int own_role_level)
		{
			Config cfg = Config.GetConfig;
			try
			{

				using (ServerDatabase.ServerDataSetTableAdapters.rolesTableAdapter rta = cfg.serverDBLayer.CreateRole_DA())
				{
					using (ServerDataSet.rolesDataTable rdt = rta.GetAvailableRoleList(own_role_level, own_role_name))
					{
				    	rta.Dispose();
						return rdt;
					}

				}
			}
			catch (InvalidOperationException ioe)
			{
				logger.Error(ioe + "");
				cfg.ReInitializeConnectionPool();
			}
			catch (Exception ex)
			{
				logger.Error(ex.ToString());
			}

			return null;

		}

		public String[] GetGroupList(long user_id)
		{
			Config cfg = Config.GetConfig;
			try
			{
				String[] arrGroupID = null;
				using (ServerDatabase.ServerDataSetTableAdapters.user_group_ASNTableAdapter ugata = cfg.serverDBLayer.CreateUser_GroupASN_DA())
				{
					using (ServerDatabase.ServerDataSet.user_group_ASNDataTable dt = ugata.GetGroupIDsByUserID(user_id))
					{
						if(dt.Rows.Count > 0)
						{
							arrGroupID = new String[dt.Rows.Count];
							int i = 0;
							foreach(ServerDataSet.user_group_ASNRow row in dt.Rows)
							{
								arrGroupID[i++] = row.gid;
							}
						}
						dt.Dispose();
					}
					ugata.Dispose();

					return arrGroupID;
				}
			}
			catch (InvalidOperationException ioe)
			{
				logger.Error(ioe + "");
				cfg.ReInitializeConnectionPool();
			}
			catch (Exception ex)
			{
				logger.Error(ex.ToString());
			}

			return null;
		}

		public String[] GetPlayerList(long user_id)
		{
			Config cfg = Config.GetConfig;
			try
			{
				String[] arrPlayerID = null;
				using (ServerDatabase.ServerDataSetTableAdapters.user_player_ASNTableAdapter upata = cfg.serverDBLayer.CreateUser_PlayerASN_DA())
				{
					using (ServerDatabase.ServerDataSet.user_player_ASNDataTable dt = upata.GetPlayerIDsByUserID(user_id))
					{
						if (dt.Rows.Count > 0)
						{
							arrPlayerID = new String[dt.Rows.Count];
							int i = 0;
							foreach (ServerDataSet.user_player_ASNRow row in dt.Rows)
							{
								arrPlayerID[i++] = row.pid;
							}
						}
						dt.Dispose();
					}
					upata.Dispose();

					return arrPlayerID;
				}
			}
			catch (InvalidOperationException ioe)
			{
				logger.Error(ioe + "");
				cfg.ReInitializeConnectionPool();
			}
			catch (Exception ex)
			{
				logger.Error(ex.ToString());
			}

			return null;
		}

		public bool ModifyEnabled(long user_id, bool bEnabled)
		{
			Config cfg = Config.GetConfig;
			CacheObject obj = CacheObject.GetInstance;

			try
			{
				using (ServerDatabase.ServerDataSetTableAdapters.usersTableAdapter uta = cfg.serverDBLayer.CreateUser_DA())
				{
					logger.Info(String.Format("user_id: [{0}], is_enabled: [{1}]", user_id, bEnabled ? "Y" : "S", user_id));

					//	S는 대기, Y는 활성
					uta.UpdateUserStatus(bEnabled ? "Y" : "S", user_id);
					if(obj.UpdateUserStatus(user_id, bEnabled ? "Y" : "S"))
					{
						uta.Dispose();
						return true;
					}
					uta.Dispose();
				}
			}
			catch (InvalidOperationException ioe)
			{
				logger.Error(ioe + "");
				cfg.ReInitializeConnectionPool();
			}
			catch (Exception ex)
			{
				logger.Error(ex.ToString());
			}

			return false;
		}


		public bool AddUser(String name, String password, String role_ids, long parent_id, String[] groups, String[] players)
		{
			Config cfg = Config.GetConfig;
			CacheObject obj = CacheObject.GetInstance;

			try
			{
				
				using (ServerDatabase.ServerDataSetTableAdapters.usersTableAdapter uta = cfg.serverDBLayer.CreateUser_DA())
				{
					uta.AddUser(name, password, parent_id, role_ids);

					long uid = -1;
					using (ServerDatabase.ServerDataSet.usersDataTable gdt = uta.GetUserByName(name, password))
					{
						if (gdt.Rows.Count > 0)
						{
							uid = ((ServerDatabase.ServerDataSet.usersRow)gdt.Rows[0]).id;
						}
						else
						{
							uta.Dispose();
							return false;
						}

						if(groups != null)
						{
							using(ServerDatabase.ServerDataSetTableAdapters.user_group_ASNTableAdapter gata = cfg.serverDBLayer.CreateUser_GroupASN_DA())
							{
								foreach(string gid in groups)
								{
									gata.AddData(uid, gid);
								}
								gata.Dispose();
							}
						}

						if(players != null)
						{
							using (ServerDatabase.ServerDataSetTableAdapters.user_player_ASNTableAdapter pata = cfg.serverDBLayer.CreateUser_PlayerASN_DA())
							{
								foreach (string pid in players)
								{
									pata.AddData(uid, pid);
								}
								pata.Dispose();
							}
						}

						if (!obj.AddUserNode(uid, name, password, role_ids, parent_id, "Y"))
							logger.Error("Failed to add user in cache object. username : " + name);

						gdt.Dispose();
					}

					uta.Dispose();
				}

				return true;
			}
			catch (InvalidOperationException ioe)
			{
				logger.Error(ioe + "");
				cfg.ReInitializeConnectionPool();
			}
			catch (Exception ex)
			{
				logger.Error(ex.ToString());
			}

			return false;
		}

		public bool EditUser(long user_id, String password, String role_ids, long parent_id, String[] groups, String[] players)
		{
			Config cfg = Config.GetConfig;
			CacheObject obj = CacheObject.GetInstance;

			try
			{
				using (ServerDatabase.ServerDataSetTableAdapters.usersTableAdapter uta = cfg.serverDBLayer.CreateUser_DA())
				{
					uta.EditUser(password, parent_id, role_ids, user_id);

					if (groups != null)
					{
						using (ServerDatabase.ServerDataSetTableAdapters.user_group_ASNTableAdapter gata = cfg.serverDBLayer.CreateUser_GroupASN_DA())
						{
							gata.DeleteFromUserID(user_id);

							foreach (string gid in groups)
							{
								gata.AddData(user_id, gid);
							}
							gata.Dispose();
						}
					}

					if (players != null)
					{
						using (ServerDatabase.ServerDataSetTableAdapters.user_player_ASNTableAdapter pata = cfg.serverDBLayer.CreateUser_PlayerASN_DA())
						{
							pata.DeleteFromUserID(user_id);

							foreach (string pid in players)
							{
								pata.AddData(user_id, pid);
							}
							pata.Dispose();
						}
					}

					if (!obj.UpdateUserNode(user_id, password, role_ids, parent_id))
						logger.Error("Failed to edit user in cache object. user_id : " + user_id.ToString());
					
					uta.Dispose();
				}

				return true;
			}
			catch (InvalidOperationException ioe)
			{
				logger.Error(ioe + "");
				cfg.ReInitializeConnectionPool();
			}
			catch (Exception ex)
			{
				logger.Error(ex.ToString());
			}

			return false;
		}

		public bool DeleteUser(long user_id)
		{
			Config cfg = Config.GetConfig;
			CacheObject obj = CacheObject.GetInstance;

			try
			{
				using (ServerDatabase.ServerDataSetTableAdapters.usersTableAdapter uta = cfg.serverDBLayer.CreateUser_DA())
				{
					if (0 < uta.DeleteUser(user_id))
					{
						if (!obj.DeleteUserNode(user_id))
							logger.Error("Failed to delete user in cache object. user_id : " + user_id.ToString());

						uta.Dispose();
						return true;
					}
					
					uta.Dispose();
				}

			}
			catch (InvalidOperationException ioe)
			{
				logger.Error(ioe + "");
				cfg.ReInitializeConnectionPool();
			}
			catch (Exception ex)
			{
				logger.Error(ex.ToString());
			}

			return false;

		}

		/*
		public bool AddConnectionInfo(string user_id, string computer_name, string ipaddr1, string ipaddr2, string ipaddr3, string version)
		{
			Config cfg = Config.GetConfig;
			CacheObject obj = CacheObject.GetInstance;

			try
			{
				using (ServerDatabase.ServerDataSetTableAdapters.tblManagerConnInfoTableAdapter mcita = cfg.serverDBLayer.CreateManagerConnInfoDA())
				{
					mcita.AddManagerConnectionInfo(computer_name, version, user_id, ipaddr1, ipaddr2, ipaddr3, DateTime.Now.ToString());
					mcita.Dispose();
				}

				return true;
			}
			catch (InvalidOperationException ioe)
			{
				logger.Error(ioe + "");
				cfg.ReInitializeConnectionPool();
			}
			catch (Exception ex)
			{
				logger.Error(ex.ToString());
			}

			return false;
		}
		*/
    }

    public class DataInfoList : MarshalByRefObject, IDataInfoList
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public ServerDataSet.DataInfoDataTable GetDatainfoList(long user_id, string template_id)
        {
            Config cfg = Config.GetConfig;
            try
            {

                using (ServerDatabase.ServerDataSetTableAdapters.DataInfoTableAdapter rta = cfg.serverDBLayer.CreateDataInfoConnectDA())
                {
                    using (ServerDataSet.DataInfoDataTable rdt = rta.GetDatabyUserid(user_id, template_id))
                    {
                        rta.Dispose();
                        return rdt;
                    }

                }
            }
            catch (InvalidOperationException ioe)
            {
                logger.Error(ioe + "");
                cfg.ReInitializeConnectionPool();
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }

            return null;
        }

        public bool AddDataInfo(string id, long user_id, string template_id, string title)
        {
            Config cfg = Config.GetConfig;

            try
            {
                using (ServerDatabase.ServerDataSetTableAdapters.DataInfoTableAdapter dta = cfg.serverDBLayer.CreateDataInfoConnectDA())
                {
                    if (0 < dta.AddDatainfo(id, user_id, template_id, title))
                    {
                        dta.Dispose();
                        return true;
                    }
                }
            }
            catch (InvalidOperationException ioe)
            {
                logger.Error(ioe + "");
                cfg.ReInitializeConnectionPool();
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }

            return false;
        }

        public bool DelDataInfo(string id)
        {
            Config cfg = Config.GetConfig;

            try
            {
                using (ServerDatabase.ServerDataSetTableAdapters.DataInfoTableAdapter dta = cfg.serverDBLayer.CreateDataInfoConnectDA())
                {
                    if (0 < dta.DelDataInfo(id))
                    {
                        dta.Dispose();
                        return true;
                    }
                }
            }
            catch (InvalidOperationException ioe)
            {
                logger.Error(ioe + "");
                cfg.ReInitializeConnectionPool();
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }

            return false;
        }

    }

    public class DataFieldList : MarshalByRefObject, IDataFieldList
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public ServerDataSet.Data_FieldDataTable GetDatabyDataid(string id)
        {
            Config cfg = Config.GetConfig;
            try
            {
                using (ServerDatabase.ServerDataSetTableAdapters.Data_FieldTableAdapter rta = cfg.serverDBLayer.CreateDataFieldConnectDA())
                {
                    using (ServerDataSet.Data_FieldDataTable rdt = rta.GetDataByDataFieldID(id))
                    {
                        rta.Dispose();
                        return rdt;
                    }

                }
            }
            catch (InvalidOperationException ioe)
            {
                logger.Error(ioe + "");
                cfg.ReInitializeConnectionPool();
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }

            return null;
        }

        public ServerDataSet.Data_FieldDataTable GetDataFieldList(string datainfo_id)
        {
            Config cfg = Config.GetConfig;
            try
            {

                using (ServerDatabase.ServerDataSetTableAdapters.Data_FieldTableAdapter rta = cfg.serverDBLayer.CreateDataFieldConnectDA())
                {
                    using (ServerDataSet.Data_FieldDataTable rdt = rta.GetDatabyTemplateid(datainfo_id))
                    {
                        rta.Dispose();
                        return rdt;
                    }

                }
            }
            catch (InvalidOperationException ioe)
            {
                logger.Error(ioe + "");
                cfg.ReInitializeConnectionPool();
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }

            return null;
        }

        public bool AddDataField(string id, string datainfo_id, string field_name, string[] value)
        {
            Config cfg = Config.GetConfig;

            try
            {
                using (ServerDatabase.ServerDataSetTableAdapters.Data_FieldTableAdapter dta = cfg.serverDBLayer.CreateDataFieldConnectDA())
                {
                    int i = 0;
                    string[] values = new string[30];
                    for (i = 0; i < 30; i++)
                        values[i] = "";
                    for (i = 0; i < value.Length; i++)
                    {
                        values[i] = value[i];
                    }
                    if (0 < dta.AddDataField(id, datainfo_id, values[0], values[1], values[2], values[3], values[4], values[5], values[6], values[7], values[8], values[9], values[10],
                                                 values[11], values[12], values[13], values[14], values[15], values[16], values[17], values[18], values[19], values[20],
                                                 values[21], values[22], values[23], values[24], values[25], values[26], values[27], values[28], values[29], field_name))
                    {
                        dta.Dispose();
                        return true;
                    }
                }
            }
            catch (InvalidOperationException ioe)
            {
                logger.Error(ioe + "");
                cfg.ReInitializeConnectionPool();
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }

            return false;
        }

        public bool DelDataFieldByid(string id)
        {
            Config cfg = Config.GetConfig;

            try
            {
                using (ServerDatabase.ServerDataSetTableAdapters.Data_FieldTableAdapter dta = cfg.serverDBLayer.CreateDataFieldConnectDA())
                {
                    if (0 < dta.DelDataFieldbyId(id))
                    {
                        dta.Dispose();
                        return true;
                    }
                }
            }
            catch (InvalidOperationException ioe)
            {
                logger.Error(ioe + "");
                cfg.ReInitializeConnectionPool();
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }

            return false;
        }

        public bool DelDataFieldByDatainfoid(string datainfo_id)
        {
            Config cfg = Config.GetConfig;

            try
            {
                using (ServerDatabase.ServerDataSetTableAdapters.Data_FieldTableAdapter dta = cfg.serverDBLayer.CreateDataFieldConnectDA())
                {
                    if (0 < dta.DelDataFieldbyDataInfoId(datainfo_id))
                    {
                        dta.Dispose();
                        return true;
                    }
                }
            }
            catch (InvalidOperationException ioe)
            {
                logger.Error(ioe + "");
                cfg.ReInitializeConnectionPool();
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return false;
        }

        public bool EditDataField(string id, string datainfo_id, string[] value)
        {
            Config cfg = Config.GetConfig;

            try
            {
                using (ServerDatabase.ServerDataSetTableAdapters.Data_FieldTableAdapter dta = cfg.serverDBLayer.CreateDataFieldConnectDA())
                {
                    int i = 0;
                    string[] values = new string[30];
                    for (i = 0; i < 30; i++)
                        values[i] = "";
                    for (i = 0; i < value.Length; i++)
                    {
                        values[i] = value[i];
                    }
                    if (0 < dta.UpdateDataField(id, datainfo_id, values[0], values[1], values[2], values[3], values[4], values[5], values[6], values[7], values[8], values[9], values[10],
                                                 values[11], values[12], values[13], values[14], values[15], values[16], values[17], values[18], values[19], values[20],
                                                 values[21], values[22], values[23], values[24], values[25], values[26], values[27], values[28], values[29]))
                    {
                        dta.Dispose();
                        return true;
                    }
                }
            }
            catch (InvalidOperationException ioe)
            {
                logger.Error(ioe + "");
                cfg.ReInitializeConnectionPool();
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }

            return false;
        }
    }

    public class ContentsList : MarshalByRefObject, IContentsList
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public ServerDataSet.ContentsDataTable GetDatabyDataFieldid(string datafield_id)
        {
            Config cfg = Config.GetConfig;
            try
            {
                using (ServerDatabase.ServerDataSetTableAdapters.ContentsTableAdapter rta = cfg.serverDBLayer.CreateContentsConnectDA())
                {
                    using (ServerDataSet.ContentsDataTable rdt = rta.GetDatabyDataFieldid(datafield_id))
                    {
                        rta.Dispose();
                        return rdt;
                    }

                }
            }
            catch (InvalidOperationException ioe)
            {
                logger.Error(ioe + "");
                cfg.ReInitializeConnectionPool();
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return null;
        }
        public ServerDataSet.ContentsDataTable GetDataByID(string id)
        {
            Config cfg = Config.GetConfig;
            try
            {
                using (ServerDatabase.ServerDataSetTableAdapters.ContentsTableAdapter rta = cfg.serverDBLayer.CreateContentsConnectDA())
                {
                    using (ServerDataSet.ContentsDataTable rdt = rta.GetContentsByID(id))
                    {
                        rta.Dispose();
                        return rdt;
                    }

                }
            }
            catch (InvalidOperationException ioe)
            {
                logger.Error(ioe + "");
                cfg.ReInitializeConnectionPool();
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return null;
        }

        public bool AddContets(string id, string datafield_id, string type, string value, string file_nm, string path, string desc, string datainfo_id)
        {
            Config cfg = Config.GetConfig;

            try
            {
                using (ServerDatabase.ServerDataSetTableAdapters.ContentsTableAdapter dta = cfg.serverDBLayer.CreateContentsConnectDA())
                {
                    if (0 < dta.AddContets(id, datafield_id, type, value, file_nm, path, desc, datainfo_id))
                    {
                        dta.Dispose();
                        return true;
                    }
                }
            }
            catch (InvalidOperationException ioe)
            {
                logger.Error(ioe + "");
                cfg.ReInitializeConnectionPool();
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return false;
        }

        public bool DeletebyContentsId(string id)
        {
            Config cfg = Config.GetConfig;

            try
            {
                using (ServerDatabase.ServerDataSetTableAdapters.ContentsTableAdapter dta = cfg.serverDBLayer.CreateContentsConnectDA())
                {
                    if (0 < dta.DeletebyContentsId(id))
                    {
                        dta.Dispose();
                        return true;
                    }
                }
            }
            catch (InvalidOperationException ioe)
            {
                logger.Error(ioe + "");
                cfg.ReInitializeConnectionPool();
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return false;
        }

        public bool DeletebyDataFieldid(string datafield_id)
        {
            Config cfg = Config.GetConfig;

            try
            {
                using (ServerDatabase.ServerDataSetTableAdapters.ContentsTableAdapter dta = cfg.serverDBLayer.CreateContentsConnectDA())
                {
                    if (0 < dta.DeletebyDataFieldid(datafield_id))
                    {
                        dta.Dispose();
                        return true;
                    }
                }
            }
            catch (InvalidOperationException ioe)
            {
                logger.Error(ioe + "");
                cfg.ReInitializeConnectionPool();
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return false;
        }
        public bool DeletebyDataInfoid(string datainfo_id)
        {
            Config cfg = Config.GetConfig;

            try
            {
                using (ServerDatabase.ServerDataSetTableAdapters.ContentsTableAdapter dta = cfg.serverDBLayer.CreateContentsConnectDA())
                {
                    if (0 < dta.DeletebyDatainfoid(datainfo_id))
                    {
                        dta.Dispose();
                        return true;
                    }
                }
            }
            catch (InvalidOperationException ioe)
            {
                logger.Error(ioe + "");
                cfg.ReInitializeConnectionPool();
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return false;
        }
        public bool UpdateContents(string value, string file_nm, string path, string id)
        {
            Config cfg = Config.GetConfig;

            try
            {
                using (ServerDatabase.ServerDataSetTableAdapters.ContentsTableAdapter dta = cfg.serverDBLayer.CreateContentsConnectDA())
                {
                    if (0 < dta.EditContents(value, file_nm, path, id))
                    {
                        dta.Dispose();
                        return true;
                    }
                }
            }
            catch (InvalidOperationException ioe)
            {
                logger.Error(ioe + "");
                cfg.ReInitializeConnectionPool();
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }

            return false;
        }

        public bool UpdateContents(string id, string type, string value, string file_nm, string path)
        {
            Config cfg = Config.GetConfig;

            try
            {
                using (ServerDatabase.ServerDataSetTableAdapters.ContentsTableAdapter dta = cfg.serverDBLayer.CreateContentsConnectDA())
                {
                    if (0 < dta.UpdateContents(type, value, file_nm, path, id))
                    {
                        dta.Dispose();
                        return true;
                    }
                }
            }
            catch (InvalidOperationException ioe)
            {
                logger.Error(ioe + "");
                cfg.ReInitializeConnectionPool();
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }

            return false;
        }

    }

    public class ResultDataList : MarshalByRefObject, IResultDataList
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public ServerDataSet.resultdatasDataTable GetResultData(long date, string player_id, string datafield_id)
        {
            Config cfg = Config.GetConfig;
            try
            {
                using (ServerDatabase.ServerDataSetTableAdapters.resultdatasTableAdapter dta = cfg.serverDBLayer.CreateResultDataConnectDA())
                {
                    using (ServerDataSet.resultdatasDataTable rdt = dta.CallResultData(date, player_id, datafield_id))
                    {
                        dta.Dispose();
                        return rdt;
                    }
                }
            }
            catch (InvalidOperationException ioe)
            {
                logger.Error(ioe + "");
                cfg.ReInitializeConnectionPool();
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }

            return null;
        }

        public bool AddResultData(long date, string player_id, string datafield_id, string values)
        {
            Config cfg = Config.GetConfig;
            try
            {
                using(ServerDatabase.ServerDataSetTableAdapters.resultdatasTableAdapter dta = cfg.serverDBLayer.CreateResultDataConnectDA())
                {
                    ServerDataSet.resultdatasDataTable rdt = dta.CallResultData(date, player_id, datafield_id);
                    if (rdt == null || rdt.Rows.Count == 0)
                    {
                        string template_id = "";
                        string desc = "";
                        using (ServerDatabase.ServerDataSetTableAdapters.Data_FieldTableAdapter datalist = cfg.serverDBLayer.CreateDataFieldConnectDA())
                        {
                            ServerDatabase.ServerDataSet.Data_FieldDataTable table = datalist.GetDataByDataFieldID(datafield_id);
                            foreach (ServerDatabase.ServerDataSet.Data_FieldRow row in table.Rows)
                            {
                                template_id = row.Field_name;
                                datalist.Dispose();
                                using (ServerDatabase.ServerDataSetTableAdapters.ContentsTableAdapter contlist = cfg.serverDBLayer.CreateContentsConnectDA())
                                {
                                    ServerDatabase.ServerDataSet.ContentsDataTable cTable = contlist.GetContentsByID(row.Value1);
                                    foreach (ServerDatabase.ServerDataSet.ContentsRow _row in cTable.Rows)
                                    {
                                        desc = _row._Value;
                                        break;
                                    }
                                    contlist.Dispose();
                                }
                                break;
                            }
                        }

                        if (0 < dta.AddResultDataValues(date, player_id, datafield_id, template_id, desc, values))
                        {
                            dta.Dispose();
                            return true;
                        }
                    }
                    else
                    {
                        dta.Dispose();
                        if (true == UpdateResultData(date, player_id, datafield_id, values))
                            return true;
                    }
                }
            }
            catch (InvalidOperationException ioe)
            {
                logger.Error(ioe + "");
                cfg.ReInitializeConnectionPool();
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }

            return false;
        }

        public bool UpdateResultData(long date, string player_id, string datafield_id, string values)
        {
            Config cfg = Config.GetConfig;
            try
            {
                using (ServerDatabase.ServerDataSetTableAdapters.resultdatasTableAdapter dta = cfg.serverDBLayer.CreateResultDataConnectDA())
                {
                    if (0 < dta.UpdateResData(values, date, player_id, datafield_id))
                    {
                        dta.Dispose();
                        return true;
                    }
                }
            }
            catch (InvalidOperationException ioe)
            {
                logger.Error(ioe + "");
                cfg.ReInitializeConnectionPool();
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }

            return false;
        }

        public ServerDataSet.resultdatasDataTable GetResultDatabyDatafieldid(long from_date, long end_date, string player_id, string datafield_id)
        {
            Config cfg = Config.GetConfig;
            try
            {
                using (ServerDatabase.ServerDataSetTableAdapters.resultdatasTableAdapter dta = cfg.serverDBLayer.CreateResultDataConnectDA())
                {
                    using (ServerDataSet.resultdatasDataTable rdt = dta.GetResultDatabyFieldid(from_date, end_date, player_id, datafield_id))
                    {
                        dta.Dispose();
                        return rdt;
                    }
                }
            }
            catch (InvalidOperationException ioe)
            {
                logger.Error(ioe + "");
                cfg.ReInitializeConnectionPool();
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }

            return null;
        }

        public ServerDataSet.resultdatasDataTable GetResultDatabyTemplateid(long from_date, long end_date, string player_id, string template_id)
        {
            Config cfg = Config.GetConfig;
            try
            {
                using (ServerDatabase.ServerDataSetTableAdapters.resultdatasTableAdapter dta = cfg.serverDBLayer.CreateResultDataConnectDA())
                {
                    using (ServerDataSet.resultdatasDataTable rdt = dta.GetResultDatabyTemplateid(from_date, end_date, template_id, player_id))
                    {
                        dta.Dispose();
                        return rdt;
                    }
                }
            }
            catch (InvalidOperationException ioe)
            {
                logger.Error(ioe + "");
                cfg.ReInitializeConnectionPool();
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }

            return null;
        }

        public bool DeleteByBetweenDate(long from_date, long end_date)
        {
            Config cfg = Config.GetConfig;
            try
            {
                using (ServerDatabase.ServerDataSetTableAdapters.resultdatasTableAdapter dta = cfg.serverDBLayer.CreateResultDataConnectDA())
                {
                    if (0 < dta.DeleteByBetweenDate(from_date, end_date))
                    {
                        dta.Dispose();
                        return true;
                    }
                }
            }
            catch (InvalidOperationException ioe)
            {
                logger.Error(ioe + "");
                cfg.ReInitializeConnectionPool();
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }

            return false;
        }

        public bool DeleteByPlayerid(string plaer_id)
        {
            Config cfg = Config.GetConfig;
            try
            {
                using (ServerDatabase.ServerDataSetTableAdapters.resultdatasTableAdapter dta = cfg.serverDBLayer.CreateResultDataConnectDA())
                {
                    if (0 < dta.DeleteByPlayerid(plaer_id))
                    {
                        dta.Dispose();
                        return true;
                    }
                }
            }
            catch (InvalidOperationException ioe)
            {
                logger.Error(ioe + "");
                cfg.ReInitializeConnectionPool();
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }

            return false;
        }
    }
}
