﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Cryptography;

using DigitalSignage.Common;
using DigitalSignage.ServerDatabase;
using NLog;
//SQLite & DB routines
using System.Data.Common;
using System.Data.Odbc;

namespace DigitalSignage.SchedulerService
{
	public class DownloadList : MarshalByRefObject, IDownloadList
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

		static long currTimeStamp = DateTime.UtcNow.Ticks;

		public long GetCurrTS()
		{
			return currTimeStamp;
		}
        /// <summary>
        /// 다운로드 정보를 가져온다
        /// </summary>
        /// <param name="arrUuids">스케줄 목록</param>
        /// <param name="arrPids">플레이어 아이디 목록</param>
        /// <returns>다운로드 정보</returns>
        public ServerDataSet.DownloadSummaryDataTable GetDownloadCountFromUuid(string[] arrUuids, string[] arrPids)
        {
            Config cfg = Config.GetConfig;

            try
            {
                String sPlayerID = "";
                String sQuery = String.Empty;
                if (arrPids.Length > 1)
                {
                    foreach (string pid in arrPids)
                    {
                        sPlayerID += String.Format("'{0}',", pid);
                    }

                    sPlayerID = sPlayerID.TrimEnd(',');
                }
                else
                {
                    sPlayerID = String.Format("'{0}'", arrPids[0]);
                }

                String sUuid = "";

                if (arrUuids.Length > 1)
                {
                    foreach (string uuid in arrUuids)
                    {
                        sUuid += String.Format("'{0}',", uuid);
                    }

                    sUuid = sUuid.TrimEnd(',');
                }
                else
                {
                    sUuid = String.Format("'{0}'", arrUuids[0]);
                }

                sQuery = String.Format("SELECT uuid, count(pid) as player_cnt FROM downloadmaster WHERE (pid IN ({0})) AND (uuid IN ({1})) GROUP BY uuid", sPlayerID, sUuid);

                using (ServerDataSet.DownloadSummaryDataTable dt = new ServerDataSet.DownloadSummaryDataTable())
                {
                    using (OdbcConnection conn = cfg.serverDBLayer.GetAvailableConnection())
                    {
                        using (OdbcCommand sql = new OdbcCommand(sQuery, conn))
                        {
                            //initializing database structure
                            using (OdbcDataReader dr = sql.ExecuteReader())
                            {
                                dt.Load(dr);

                                dr.Close();
                                dr.Dispose();
                            }

                            sql.Dispose();

                            return dt;
                        }
                    }

                }
            }
            catch (InvalidOperationException ioe)
            {
                logger.Error(ioe + "");
                cfg.ReInitializeConnectionPool();
            }
            catch (Exception e)
            {
                logger.Error(e.Message);
            }

            return null;
        }

		public ServerDataSet.downloadmasterDataTable GetDownloadInformation(string[] arrUuids, string[] arrPids)
		{
			Config cfg = Config.GetConfig;

			try
			{
				String sPlayerID = "";
				String sQuery = String.Empty;
				if (arrPids.Length > 1)
				{
					foreach (string pid in arrPids)
					{
						sPlayerID += String.Format("'{0}',", pid);
					}

					sPlayerID = sPlayerID.TrimEnd(',');
				}
				else
				{
					sPlayerID = String.Format("'{0}'", arrPids[0]);
				}

				String sUuid = "";

				if (arrUuids.Length > 1)
				{
					foreach (string uuid in arrUuids)
					{
						sUuid += String.Format("'{0}',", uuid);
					}

					sUuid = sUuid.TrimEnd(',');
				}
				else
				{
					sUuid = String.Format("'{0}'", arrUuids[0]);
				}

				sQuery = String.Format("SELECT uuid, pid FROM downloadmaster WHERE (pid IN ({0})) AND (uuid IN ({1}))", sPlayerID, sUuid);

				using (ServerDataSet.downloadmasterDataTable dt = new ServerDataSet.downloadmasterDataTable())
				{
                    using (OdbcConnection conn = cfg.serverDBLayer.GetAvailableConnection())
                    {
                        using (OdbcCommand sql = new OdbcCommand(sQuery, conn))
                        {
                            //initializing database structure
                            using (OdbcDataReader dr = sql.ExecuteReader())
                            {
                                dt.Load(dr);

                                dr.Close();
                                dr.Dispose();
                            }

                            sql.Dispose();

                            return dt;
                        }
                    }
					
				}
					
			}
			catch (InvalidOperationException ioe)
			{
				logger.Error(ioe + "");
				cfg.ReInitializeConnectionPool();
			}
			catch (Exception e)
			{
				logger.Error(e.Message);
			}

			return null;
		}


		public int InsertDownloadInformation(string uuid, string pid, long reg_date)
		{
			Config cfg = Config.GetConfig;

			try
			{
				int nRet = 0;
				using (ServerDatabase.ServerDataSetTableAdapters.DownloadedScheduleInfoTableAdapter da = cfg.serverDBLayer.CreateDownloadedSchedInfoDA())
				{
					nRet = da.InsertDownloadInfo(uuid, pid, reg_date);
					da.Dispose();
				}

				currTimeStamp = DateTime.UtcNow.Ticks;

				return nRet;
			}
			catch (InvalidOperationException ioe)
			{
				logger.Error(ioe + "");
				cfg.ReInitializeConnectionPool();
			}
			catch (Exception e)
			{
				logger.Error(e.Message+" pid : " + pid + "uuid : " + uuid);
			}

			return -1;
		}
	}
}
