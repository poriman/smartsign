﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DigitalSignage.Common;
using DigitalSignage.ServerDatabase;

using NLog;

using System.Xml;
using System.Data.Odbc;

namespace DigitalSignage.SchedulerService
{
	public class DataCacheList : MarshalByRefObject, IDataCacheList
	{
		private static Logger logger = LogManager.GetCurrentClassLogger();

		/// <summary>
		/// 서버의 제품 코드를 리턴한다.
		/// </summary>
		/// <returns></returns>
		public SerialKey.NESerialKey._ProductCode GetProductCode()
		{
			Config cfg = Config.GetConfig;
			try
			{
				return cfg.product_code;
			}
			catch (Exception e)
			{
				logger.Error(e.ToString());
			}

			return SerialKey.NESerialKey._ProductCode.Undefined;
		}

		/// <summary>
		/// 사용자 ID, Password 기준으로 해당 사용자 노드 정보를 리턴한다. (사용자 정보, 권한 등)
		/// </summary>
		/// <param name="id"></param>
		/// <param name="pwd"></param>
		/// <returns></returns>
		public String GetUserNode(string id, string pwd)
		{
			try
			{
				CacheObject obj = CacheObject.GetInstance;

				return obj.FindUserNode(id, pwd).OuterXml;
			}
			catch (Exception e)
			{
				logger.Error(e.ToString());
			}
			return null;
		}

		private long GetTimestamp(string key)
		{
			try
			{
				long ts = -1;

				Config cfg = Config.GetConfig;

				using (DigitalSignage.ServerDatabase.ServerDataSetTableAdapters.timestampsTableAdapter tsTA = cfg.serverDBLayer.CreateTimestampsInfoDA())
				{
					using (DigitalSignage.ServerDatabase.ServerDataSet.timestampsTableDataTable tsDT = tsTA.GetTimestampsBy(key))
					{
						if (tsDT != null && tsDT.Rows != null && tsDT.Rows.Count > 0)
						{
							DigitalSignage.ServerDatabase.ServerDataSet.timestampsTableRow row = tsDT.Rows[0]
								as DigitalSignage.ServerDatabase.ServerDataSet.timestampsTableRow;

							ts = row.ts_value;
						}
						tsDT.Dispose();
					}

					tsTA.Dispose();
				}
				return ts;
			}
			catch { return -1; }
		}

		/// <summary>
		/// 메타 정보의 XmlNode를 가져온다.
		/// </summary>
		/// <param name="ts">Client의 TimeStamp 정보</param>
		/// <returns>내려줄 메타 태그 정보가 있다면 XmlNode를 반환한다.</returns>
		public string GetMetaTags(long ts)
		{
			Config cfg = Config.GetConfig;

			long lTS = GetTimestamp("meta_tag_groups");

			if (lTS > ts)
			{
				try
				{
					XmlDocument doc = new XmlDocument();
					XmlNode node = doc.CreateNode(XmlNodeType.Element, "tag_groups", null);
					XmlAttribute attr = doc.CreateAttribute("ts");
					attr.Value = lTS.ToString();
					node.Attributes.Append(attr);

					#region Make Tag Groups
					using (DigitalSignage.ServerDatabase.ServerDataSetTableAdapters.meta_tag_groupsTableAdapter tgta = cfg.serverDBLayer.CreateMetaTagGroupInfoDA())
					{
						using (DigitalSignage.ServerDatabase.ServerDataSet.meta_tag_groupsDataTable tgdt = tgta.GetData())
						{
							if(tgdt != null && tgdt.Rows != null)
							{
								foreach(DigitalSignage.ServerDatabase.ServerDataSet.meta_tag_groupsRow grow in tgdt.Rows)
								{
									XmlNode groupNode = doc.CreateNode(XmlNodeType.Element, "tag_group", null);

                                    XmlAttribute g_attr0 = doc.CreateAttribute("id");
                                    g_attr0.Value = grow.g_id.ToString();
                                    groupNode.Attributes.Append(g_attr0);

									XmlAttribute g_attr1 = doc.CreateAttribute("name");
									g_attr1.Value = grow.g_name;
									groupNode.Attributes.Append(g_attr1);

									XmlAttribute g_attr2 = doc.CreateAttribute("priority");
									g_attr2.Value = grow.g_priority.ToString();
									groupNode.Attributes.Append(g_attr2);

									XmlAttribute g_attr3 = doc.CreateAttribute("ui_type");
									switch (grow.g_ui_type)
									{
										case 0:
											g_attr3.Value = "CheckBox";
											break;
										case 1:
											g_attr3.Value = "RadioBox";
											break;
										case 2:
											g_attr3.Value = "SearchBox";
											break;
                                        case 3:
                                            g_attr3.Value = "PromotionBox";
                                            break;
										default:
											g_attr3.Value = "CheckBox";
											break;
									}
									groupNode.Attributes.Append(g_attr3);

									#region Make Meta Tags
									using (DigitalSignage.ServerDatabase.ServerDataSetTableAdapters.meta_tagsTableAdapter tta = cfg.serverDBLayer.CreateMetaTagsInfoDA())
									{
										using (DigitalSignage.ServerDatabase.ServerDataSet.meta_tagsDataTable tdt = tta.GetData(grow.g_id))
										{
											if (tdt != null && tdt.Rows != null)
											{
												foreach (DigitalSignage.ServerDatabase.ServerDataSet.meta_tagsRow trow in tdt.Rows)
												{
													XmlNode tagNode = doc.CreateNode(XmlNodeType.Element, "tag", null);

													XmlAttribute t_attr1 = doc.CreateAttribute("name");
													t_attr1.Value = trow.m_tagname;
													tagNode.Attributes.Append(t_attr1);

													XmlAttribute t_attr2 = doc.CreateAttribute("value");
													t_attr2.Value = trow.m_tagvalue;
													tagNode.Attributes.Append(t_attr2);

													groupNode.AppendChild(tagNode);
												}
												tdt.Dispose();
											}
										}
										tta.Dispose();
									}
									#endregion

									node.AppendChild(groupNode);

								}

								tgdt.Dispose();
							}

						}
						tgta.Dispose();
					}
					#endregion
					doc.AppendChild(node);

					return doc.OuterXml;

				}
				catch {return string.Empty;}
			}
			else
				return String.Empty;
		}

		/// <summary>
		/// 해당하는 플레이어 ID에 해당하는 플레이어와 상위 그룹 중 최신 타임 스템프를 가져온다.
		/// </summary>
		/// <param name="pid">플레이어 ID</param>
		/// <returns></returns>
		public long GetRecentTimestamp(string pid)
		{
			try
			{
				CacheObject obj = CacheObject.GetInstance;

				long tm = -1;

				XmlNode pnode = obj.FindPlayerNode(pid);

				do
				{
					try
					{
						long newTM = Convert.ToInt64(pnode.Attributes["_modified"].Value);
						if(newTM > tm) tm = newTM;
					}
					catch { }
				}
				while ((pnode = pnode.ParentNode) != null);

				return tm;
			}
			catch (Exception e)
			{
				logger.Error(e.ToString());
			}
			return -1;
		}

		public String GetNetworkTreeByUser(string userID)
		{
			Config cfg = Config.GetConfig;

			try
			{
                string sReturn = String.Format("<NetworkTree ScheduleHandlerService='{0}' KVMProxy='{1}' NPCUService='{2}' UseThumb='{3}' ThumbGetURL='{4}' ThumbSetURL='{5}' ThumbSetAuthID='{6}' ThumbSetAuthPass='{7}' >", cfg.IsRunningTaskHandlerService, cfg.IsRunningKVMProxy, NPCUService.GetInstance.IsConnected, ServerSettings.IsSupportedThumbnail, ServerSettings.ThumbGetURL, ServerSettings.ThumbSetURL, ServerSettings.ThumbSetAuthID, ServerSettings.ThumbSetAuthPassword);
 				using(DigitalSignage.ServerDatabase.ServerDataSetTableAdapters.user_group_ASNTableAdapter ugda = cfg.serverDBLayer.CreateUser_GroupASN_DA())
				{
					using (ServerDataSet.user_group_ASNDataTable ugdt = ugda.GetGroupIDsByUsername(userID))
					{
						foreach(ServerDataSet.user_group_ASNRow row in ugdt)
						{
							XmlNode cNode = CacheObject.GetInstance.FindGroupNode(row.gid);
							sReturn += cNode.OuterXml;
						}
						ugdt.Dispose();
					}
					ugda.Dispose();
				}

				using (DigitalSignage.ServerDatabase.ServerDataSetTableAdapters.user_player_ASNTableAdapter upda = cfg.serverDBLayer.CreateUser_PlayerASN_DA())
				{
					using (ServerDataSet.user_player_ASNDataTable updt = upda.GetPlayerIDsByUsername(userID))
					{
						foreach (ServerDataSet.user_player_ASNRow row in updt)
						{
							XmlNode cNode = CacheObject.GetInstance.FindPlayerNode(row.pid);
							sReturn += cNode.OuterXml;
						}
						updt.Dispose();
					}
					upda.Dispose();
				}
				sReturn += "</NetworkTree>";

				return sReturn;
			}
			catch (InvalidOperationException ioe)
			{
				logger.Error(ioe + "");
				cfg.ReInitializeConnectionPool();
			}
			catch (Exception e)
			{
				logger.Error(e.ToString());
			}
			return null;
		}


		public String GetGroupNode(string gid)
		{
			try
			{
				CacheObject obj = CacheObject.GetInstance;

				return obj.FindGroupNode(gid).OuterXml;
			}
			catch (Exception e)
			{
				logger.Error(e.ToString());
			}
			return null;
		}

		public String GetPlayerNode(string pid)
		{
			try
			{
				CacheObject obj = CacheObject.GetInstance;

				return obj.FindPlayerNode(pid).OuterXml;
			}
			catch (Exception e)
			{
				logger.Error(e.ToString());
			}
			return null;
		}

		public String GetInheritedFTPInfo(string gid)
		{
			if(String.IsNullOrEmpty(gid))
				return null;

			try
			{
				CacheObject obj = CacheObject.GetInstance;

				XmlNode gNode = obj.FindNoInheritedGroup(gid);

				String sReturn = String.Format("{0}|{1}|{2}|{3}", gNode.Attributes["source"].Value,
					gNode.Attributes["username"].Value, gNode.Attributes["password"].Value, gNode.Attributes["path"].Value);

				return sReturn;
			}
			catch (Exception e)
			{
				logger.Error(e.ToString());
			}

			return null;
		}

		public int GetGroupDepth(string parent_gid)
		{
			if (String.IsNullOrEmpty(parent_gid) || parent_gid.Equals("-1"))
				return 0;

			try
			{
				CacheObject obj = CacheObject.GetInstance;
				XmlNode gNode = obj.FindGroupNode(parent_gid);

				return Convert.ToInt32(gNode.Attributes["depth"].Value) + 1;
			}
			catch
			{
				return 1;
			}
		}
		public int GetPlayerDepth(string pid)
		{
			if (String.IsNullOrEmpty(pid))
				return 0;

			try
			{
				CacheObject obj = CacheObject.GetInstance;
				XmlNode gNode = obj.FindPlayerNode(pid).ParentNode;

				return Convert.ToInt32(gNode.Attributes["depth"].Value) + 1;
			}
			catch
			{
				return 1;
			}
			

		}

		public string AddPlayer(string gid, string name, string host)
		{
			try
			{
				CacheObject obj = CacheObject.GetInstance;
	
				string playerid = SearchManager.GetInstance.GetAvailablePlayerID;
				if (String.IsNullOrEmpty(playerid))
					return null;

				//	Update Node
				//	Update Database
				using (ServerDatabase.ServerDataSetTableAdapters.playersTableAdapter pdt = Config.GetConfig.serverDBLayer.CreatePlayer_DA())
				{
					if (0 < pdt.AddPlayer(playerid, gid, name, host))
					{
						if(obj.AddPlayerNode(playerid, gid, name, host))
						{
							pdt.Dispose();
							return playerid;
						}
						else
						{
							pdt.DeletePlayer(playerid);
							pdt.Dispose();

							return null;
						}
					}
					pdt.Dispose();
				}
			}
			catch (Exception ex)
			{
				logger.Error(ex.ToString());
			}

			return null;
		}

		///// <summary>
		///// 플레이어를 새로 추가한다.
		///// </summary>
		///// <param name="gid">상위 그룹 아이디</param>
		///// <param name="name">플레이어 이름</param>
		///// <param name="host">플레이어 컴퓨터 이름</param>
		///// <param name="nPowerType">전원 관리 타입</param>
		///// <param name="authID">AMT 계정</param>
		///// <param name="authPass">AMT 패스워드</param>
		///// <returns>성공 여부</returns>
		//public string AddPlayer(string gid, string name, string host, int nPowerType, string authID, string authPass)
		//{
		//    try
		//    {
		//        CacheObject obj = CacheObject.GetInstance;

		//        string playerid = SearchManager.GetInstance.GetAvailablePlayerID;
		//        if (String.IsNullOrEmpty(playerid))
		//            return null;

		//        //	Update Node
		//        //	Update Database
		//        using (ServerDatabase.ServerDataSetTableAdapters.playersTableAdapter pdt = Config.GetConfig.serverDBLayer.CreatePlayer_DA())
		//        {
		//            if (0 < pdt.AddPlayer(playerid, gid, name, host))
		//            {
		//                if (obj.AddPlayerNode(playerid, gid, name, host, nPowerType, authID, authPass))
		//                {
		//                    pdt.Dispose();

		//                    Config.GetConfig.localPlayersList.InsertOrUpdatePlayerDetailInfo(playerid, authID, authPass, nPowerType);

		//                    return playerid;
		//                }
		//                else
		//                {
		//                    pdt.DeletePlayer(playerid);
		//                    pdt.Dispose();

		//                    return null;
		//                }
		//            }
		//            pdt.Dispose();
		//        }
		//    }
		//    catch (Exception ex)
		//    {
		//        logger.Error(ex.ToString());
		//    }

		//    return null;
		//}
		///// <summary>
		///// 플레이어 기존 정보를 유지하고 파라미터 정보를 수정한다.
		///// </summary>
		///// <param name="pid">수정할 플레이어 아이디</param>
		///// <param name="gid">그룹 아이디</param>
		///// <param name="name">플레이어 이름</param>
		///// <param name="nPowerType">전원 관리 타입</param>
		///// <param name="authID">AMT 계정</param>
		///// <param name="authPass">AMT 패스워드</param>
		///// <returns>성공 여부</returns>
		//public bool UpdatePlayer(string pid, string gid, string name, int nPowerType, string authID, string authPass)
		//{
		//    try
		//    {
		//        CacheObject obj = CacheObject.GetInstance;

		//        //	Update Database
		//        using (ServerDatabase.ServerDataSetTableAdapters.playersTableAdapter pdt = Config.GetConfig.serverDBLayer.CreatePlayer_DA())
		//        {
		//            if (0 < pdt.UpdatePlayer(gid, name, pid))
		//            {
		//                bool bRet = false;
		//                bRet = obj.UpdatePlayerNode(pid, gid, name, nPowerType, authID, authPass);
						
		//                Config.GetConfig.localPlayersList.InsertOrUpdatePlayerDetailInfo(pid, authID, authPass, nPowerType);
						
		//                Config.GetConfig.UpdateGroupInfoToOnePlayer(gid, pid);
		//                //	살아있는 연결에 다운로드 시간대를 갱신한다.
		//                Config.GetConfig.UpdateTimeScope(gid, pid);

		//                return bRet;
		//            }
		//        }
		//    }
		//    catch (Exception ex)
		//    {
		//        logger.Error(ex.ToString());
		//    }

		//    return false;
		//}


		public bool UpdatePlayer(string pid, string gid, string name)
		{
			try
			{
				CacheObject obj = CacheObject.GetInstance;

				//	Update Database
				using (ServerDatabase.ServerDataSetTableAdapters.playersTableAdapter pdt = Config.GetConfig.serverDBLayer.CreatePlayer_DA())
				{
					if (0 < pdt.UpdatePlayer(gid, name, pid))
					{
						bool bRet = false;
						bRet = obj.UpdatePlayerNode(pid, gid, name);

						Config.GetConfig.UpdateGroupInfoToOnePlayer(gid, pid);
						//	살아있는 연결에 다운로드 시간대를 갱신한다.
						Config.GetConfig.UpdateTimeScope(gid, pid);

						return bRet;
					}
				}
			}
			catch (Exception ex)
			{
				logger.Error(ex.ToString());
			}

			return false;
		}


		public bool DeletePlayer(string pid)
		{
			try
			{
				CacheObject obj = CacheObject.GetInstance;
				//	Update Database
				using(ServerDatabase.ServerDataSetTableAdapters.playersTableAdapter pdt = Config.GetConfig.serverDBLayer.CreatePlayer_DA())
				{
					if(0 < pdt.DeletePlayer(pid))
					{
						return obj.DeletePlayerNode(pid);
					}
				}
			}
			catch (Exception ex)
			{
				logger.Error(ex.ToString());
			}

			return false;
		}

		public string AddGroup(string name, string source, string username, string password, string parent_gid, string is_inherited, int depth, string pid, string path)
		{
			try
			{
				CacheObject obj = CacheObject.GetInstance;
				string groupId = SearchManager.GetInstance.GetAvailableGroupID;

				if (String.IsNullOrEmpty(groupId))
					return null;

				//	Update Node
				if (obj.AddGroupNode(groupId, name, source, username, password, parent_gid, is_inherited, depth, path))
				{

					//	Update Database
					using (ServerDatabase.ServerDataSetTableAdapters.groupsTableAdapter gdt = Config.GetConfig.serverDBLayer.CreateGroup_DA())
					{
                        if (0 < gdt.AddGroup(path, username, password, groupId, name, source, String.IsNullOrEmpty(parent_gid) ? null : parent_gid, depth, is_inherited))
						{
							gdt.Dispose();
							return groupId;
						}
						else
						{
							gdt.Dispose();
							obj.DeleteGroupNode(groupId);
							return null;
						}
					}
				}

			}
			catch (Exception ex)
			{
				logger.Error(ex.ToString());
			}
			return null;
		}

		public bool UpdateGroup(string gid, string name, string source, string username, string password, string parent_gid, string is_inherited, int depth, string path)
		{
			try
			{
				CacheObject obj = CacheObject.GetInstance;
				//	Update Database
				using (ServerDatabase.ServerDataSetTableAdapters.groupsTableAdapter gdt = Config.GetConfig.serverDBLayer.CreateGroup_DA())
				{
                    if (0 < gdt.UpdateGroup(path, username, password, name, source, String.IsNullOrEmpty(parent_gid) ? null : parent_gid, depth, is_inherited, gid))
					{
						bool bRet = false;
                        bRet = obj.UpdateGroupNode(gid, name, source, username, password, parent_gid, is_inherited, depth, path);

						Config.GetConfig.UpdateGroupInfoToAllPlayers(gid);
						//	살아있는 연결에 다운로드 시간대를 갱신한다.
						Config.GetConfig.UpdateTimeScope(gid, "ALL");

						//	하위 그룹의 컨텐츠 서버 정보를 업데이트한다.
						UpdateChildGroupInfo(gdt, obj.FindGroupNode(gid), source, username, password, path);

						return bRet;
					}
				}
			}
			catch (Exception ex)
			{
				logger.Error(ex.ToString());
			}
			return false;
		}

		private void UpdateChildGroupInfo(ServerDatabase.ServerDataSetTableAdapters.groupsTableAdapter gdt, XmlNode group, string source, string username, string password, string path)
		{
			try
			{
				if (!group.HasChildNodes) return;

				foreach (XmlNode child in group.ChildNodes)
				{
					if (child.LocalName.Equals("group"))
					{
						if (child.Attributes["is_inherited"].Value.Equals("N")) continue;

						string gid = child.Attributes["gid"].Value;
						string name = child.Attributes["name"].Value;
						string parent_gid = child.Attributes["parent_gid"].Value;
						int depth = -1;

						int.TryParse(child.Attributes["depth"].Value, out depth);

                        if (0 < gdt.UpdateGroup(path, username, password, name, source, String.IsNullOrEmpty(parent_gid) ? null : parent_gid, depth, "Y", gid))
						{
							CacheObject.GetInstance.UpdateGroupNode(gid, name, source, username, password, parent_gid, "Y", depth, path);
						}

						UpdateChildGroupInfo(gdt, child, source, username, password, path);
					}
				}
			}
			catch (Exception ex)
			{
				logger.Error(ex.ToString());
			}

		}

		public bool DeleteGroup(string gid)
		{
			try
			{
				CacheObject obj = CacheObject.GetInstance;

				//	Update Database
				using (ServerDatabase.ServerDataSetTableAdapters.groupsTableAdapter gdt = Config.GetConfig.serverDBLayer.CreateGroup_DA())
				{
					//	Update Node
					if (0 < gdt.DeleteGroup(gid))
					{
						return obj.DeleteGroupNode(gid);
					}
				}

			}
			catch (Exception ex)
			{
				logger.Error(ex.ToString());
			}

			return false;
		}

        /// <summary>
        /// gid에 해당하는 하위 플레이어의 상세를 가져온다.
        /// </summary>
        /// <param name="gid"></param>
        /// <returns></returns>
        public String GetPlayersByGid(string gid)
        {
            try
            {
                CacheObject obj = CacheObject.GetInstance;
                XmlNode gnode = obj.FindGroupNode(gid);

                if (gnode == null) return String.Empty;
                XmlDocument doc = new XmlDocument();

                #region 대상 노드
                XmlNode nodeParentGroup = doc.CreateElement("group");

                XmlNodeList nodePlayers = gnode.SelectNodes("descendant::player[attribute::del_yn=\"N\"]");

                int nTotalPlayers = 0;
                int nOnlinedPlayers = 0;

                if (nodePlayers != null)
                {
                    nTotalPlayers = nodePlayers.Count;

                    foreach (XmlNode pnode in nodePlayers)
                    {
                        XmlNode NewPlayerNode = doc.CreateElement("player");


                        if (pnode.Attributes["state"].Value.Equals("1")) nOnlinedPlayers++;

                        XmlAttribute pattr01 = doc.CreateAttribute("pid");
                        pattr01.Value = pnode.Attributes["pid"].Value;
                        XmlAttribute pattr02 = doc.CreateAttribute("state");
                        pattr02.Value = pnode.Attributes["state"].Value;
                        XmlAttribute pattr03 = doc.CreateAttribute("version");
                        {
                            int nVersionH = System.Convert.ToInt32(pnode.Attributes["versionH"].Value);
                            int nVersionL = System.Convert.ToInt32(pnode.Attributes["versionL"].Value);

                            int major = (nVersionH >> 16) & 0xFFFF;
                            int minor = nVersionH & 0xFFFF;
                            int build = (nVersionL >> 16) & 0xFFFF;
                            int subversion = nVersionL & 0xFFFF;

                            pattr03.Value = string.Format("{0}.{1}.{2}.{3}", major, minor, build, subversion);
                        }
                        XmlAttribute pattr04 = doc.CreateAttribute("freespace");
                        pattr04.Value = pnode.Attributes["freespace"].Value;
                        XmlAttribute pattr05 = doc.CreateAttribute("uptime");
                        pattr05.Value = pnode.Attributes["uptime"].Value;
                        XmlAttribute pattr06 = doc.CreateAttribute("lastconn");
                        pattr06.Value = pnode.Attributes["lastconndt"].Value;
                        XmlAttribute pattr07 = doc.CreateAttribute("computername");
                        pattr07.Value = pnode.Attributes["host"].Value;
                        XmlAttribute pattr08 = doc.CreateAttribute("cpurate");
                        pattr08.Value = pnode.Attributes["cpurate"].Value;
                        XmlAttribute pattr09 = doc.CreateAttribute("memoryusage");
                        pattr09.Value = pnode.Attributes["memoryusage"].Value;
                        XmlAttribute pattr10 = doc.CreateAttribute("downloadprogress");
                        pattr10.Value = pnode.Attributes["all_d_progress"].Value;
                        XmlAttribute pattr11 = doc.CreateAttribute("curr_downloadname");
                        pattr11.Value = pnode.Attributes["curr_d_progress"].Value;
                        XmlAttribute pattr12 = doc.CreateAttribute("curr_schedule");
                        pattr12.Value = pnode.Attributes["curr_screen"].Value;
                        XmlAttribute pattr13 = doc.CreateAttribute("curr_subtitle");
                        pattr13.Value = pnode.Attributes["curr_subtitle"].Value;
                        XmlAttribute pattr14 = doc.CreateAttribute("curr_volume");
                        pattr14.Value = pnode.Attributes["curr_volume"].Value;
                        XmlAttribute pattr15 = doc.CreateAttribute("curr_thumb_url");

                        #region Thumbnail 조회
                        string sCurrUuid = String.Empty;

                        try
                        {
                            sCurrUuid = pnode.Attributes["curr_screen_uuid"].Value;
                        }
                        catch
                        {
                            logger.Error("curr_screen_uuid 없음 " + pnode.OuterXml );
                        }

                        if (!String.IsNullOrEmpty(sCurrUuid))
                        {
                            try
                            {
                                using (ServerDatabase.ServerDataSetTableAdapters.ScreenTaskASNTableAdapter sda = Config.GetConfig.serverDBLayer.CreateScreenTaskASN_DA())
                                {
									try
									{

										using (ServerDatabase.ServerDataSet.ScreenTaskASNDataTable sdt = sda.GetDataByUuid(sCurrUuid))
										{
											if (sdt != null)
											{
												foreach (ServerDatabase.ServerDataSet.ScreenTaskASNRow row in sdt.Rows)
												{
													pattr15.Value = row.thumb_url;
													break;
												}
											}
											
											sdt.Dispose();
										}
										sda.Dispose();
									}
									catch (Exception ex2) { logger.Error(ex2.ToString()); }
                                }
                            }
                            catch (Exception ex) { logger.Error(ex.ToString()); }
                        }
                        #endregion

                        XmlAttribute pattr16 = doc.CreateAttribute("name");
                        pattr16.Value = pnode.Attributes["name"].Value;

						XmlAttribute pattr17 = doc.CreateAttribute("curr_uuid");
						pattr17.Value = sCurrUuid;

                        NewPlayerNode.Attributes.Append(pattr01);
                        NewPlayerNode.Attributes.Append(pattr02);
                        NewPlayerNode.Attributes.Append(pattr03);
                        NewPlayerNode.Attributes.Append(pattr04);
                        NewPlayerNode.Attributes.Append(pattr05);
                        NewPlayerNode.Attributes.Append(pattr06);
                        NewPlayerNode.Attributes.Append(pattr07);
                        NewPlayerNode.Attributes.Append(pattr08);
                        NewPlayerNode.Attributes.Append(pattr09);
                        NewPlayerNode.Attributes.Append(pattr10);
                        NewPlayerNode.Attributes.Append(pattr11);
                        NewPlayerNode.Attributes.Append(pattr12);
                        NewPlayerNode.Attributes.Append(pattr13);
                        NewPlayerNode.Attributes.Append(pattr14);
                        NewPlayerNode.Attributes.Append(pattr15);
						NewPlayerNode.Attributes.Append(pattr16);
						NewPlayerNode.Attributes.Append(pattr17);

                        nodeParentGroup.AppendChild(NewPlayerNode);
                    }
                }

                #region 대상 노드의 속성
                XmlAttribute attr1 = doc.CreateAttribute("gid");
                attr1.Value = gnode.Attributes["gid"].Value;
                XmlAttribute attr2 = doc.CreateAttribute("name");
                attr2.Value = gnode.Attributes["name"].Value;
                XmlAttribute attr3 = doc.CreateAttribute("totalplayers");
                attr3.Value = nTotalPlayers.ToString();
                XmlAttribute attr4 = doc.CreateAttribute("onlineplayers");
                attr4.Value = nOnlinedPlayers.ToString();

                nodeParentGroup.Attributes.Append(attr1);
                nodeParentGroup.Attributes.Append(attr2);
                nodeParentGroup.Attributes.Append(attr3);
                nodeParentGroup.Attributes.Append(attr4);
                #endregion

                doc.AppendChild(nodeParentGroup);
                #endregion

                return nodeParentGroup.OuterXml;

            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }

            return String.Empty;
        }

        /// <summary>
        /// 그룹의 온라인 상태 정보를 조회한다.
        /// </summary>
        /// <param name="gid"></param>
        /// <returns></returns>
        public String GetGroupOnlineInfo(string gid)
        {
            try
            {
                CacheObject obj = CacheObject.GetInstance;
                XmlNode gnode = obj.FindGroupNode(gid);

                if (gnode == null) return String.Empty;
                XmlDocument doc = new XmlDocument();
                doc.CreateElement("group");

                #region 대상 노드
                XmlNode nodeParentGroup = doc.CreateElement("group");

                int nTotalPlayers = 0;
                int nOnlinedPlayers = 0;

				try
				{
					nTotalPlayers = gnode.SelectNodes("child::player[attribute::del_yn=\"N\"]").Count;
				}
				catch { }

				try
				{
					nOnlinedPlayers = gnode.SelectNodes("child::player[attribute::del_yn=\"N\" and attribute::state=\"1\"]").Count;
				}
				catch { }

                foreach (XmlNode node in gnode.ChildNodes)
                {
                    if (!node.LocalName.Equals("group") || node.Attributes["del_yn"].Value.Equals("Y")) continue;

                    int cntPlayers = 0;
                    int cntOnline = 0;

                    XmlNode nodeChildGroup = doc.CreateElement("group");

                    try
                    {
                        cntPlayers = node.SelectNodes("descendant::player[attribute::del_yn=\"N\"]").Count;
                    }
                    catch { }

                    try
                    {
                        cntOnline = node.SelectNodes("descendant::player[attribute::del_yn=\"N\" and attribute::state=\"1\"]").Count;
                    }
                    catch { }

                    #region 하위 노드의 속성
                    XmlAttribute cattr1 = doc.CreateAttribute("gid");
                    cattr1.Value = node.Attributes["gid"].Value;
                    XmlAttribute cattr2 = doc.CreateAttribute("name");
                    cattr2.Value = node.Attributes["name"].Value;
                    XmlAttribute cattr3 = doc.CreateAttribute("totalplayers");
                    cattr3.Value = cntPlayers.ToString();
                    XmlAttribute cattr4 = doc.CreateAttribute("onlineplayers");
                    cattr4.Value = cntOnline.ToString();

                    nodeChildGroup.Attributes.Append(cattr1);
                    nodeChildGroup.Attributes.Append(cattr2);
                    nodeChildGroup.Attributes.Append(cattr3);
                    nodeChildGroup.Attributes.Append(cattr4);
                    #endregion

                    nodeParentGroup.AppendChild(nodeChildGroup);

                    nTotalPlayers += cntPlayers;
                    nOnlinedPlayers += cntOnline;
                }

                #region 대상 노드의 속성
                XmlAttribute attr1 = doc.CreateAttribute("gid");
                attr1.Value = gnode.Attributes["gid"].Value;
                XmlAttribute attr2 = doc.CreateAttribute("name");
                attr2.Value = gnode.Attributes["name"].Value;
                XmlAttribute attr3 = doc.CreateAttribute("totalplayers");
                attr3.Value = nTotalPlayers.ToString();
                XmlAttribute attr4 = doc.CreateAttribute("onlineplayers");
                attr4.Value = nOnlinedPlayers.ToString();

                nodeParentGroup.Attributes.Append(attr1);
                nodeParentGroup.Attributes.Append(attr2);
                nodeParentGroup.Attributes.Append(attr3);
                nodeParentGroup.Attributes.Append(attr4);
                #endregion

                doc.AppendChild(nodeParentGroup);
                #endregion

                return nodeParentGroup.OuterXml;

            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }

            return String.Empty;
        }

		/// <summary>
		/// 지역 정보를 가져온다.
		/// </summary>
		/// <param name="geocode"></param>
		/// <returns></returns>
		public ServerDatabase.ServerDataSet.geo_masterDataTable GetGeoMasterInformation(int geocode)
		{
			if (geocode == -1) return null;

			Config cfg = Config.GetConfig;
			String sQuery = String.Empty;
			try
			{
				using (OdbcConnection conn = cfg.serverDBLayer.GetAvailableConnection())
				{
					using (ServerDatabase.ServerDataSet.geo_masterDataTable dt = new ServerDataSet.geo_masterDataTable())
					{
						sQuery = String.Format("SELECT geocode, x, y, postcode, address, ref_code FROM geo_master WHERE geocode = {0};", geocode);
						using (OdbcCommand comm = new OdbcCommand(sQuery, conn))
						{
							using (OdbcDataReader dr = comm.ExecuteReader())
							{
								dt.Load(dr);
							}
							comm.Dispose();
						}
						conn.Close();
						conn.Dispose();

						return dt;
					}
				}
			}
			catch (Exception e)
			{
				logger.Error(e.Message + " query : " + sQuery);
			}

			return null;
		}

		/// <summary>
		/// GeoMaster 정보를 갱신한다.
		/// </summary>
		/// <param name="gid"></param>
		/// <param name="geocode"></param>
		/// <param name="x"></param>
		/// <param name="y"></param>
		/// <param name="postcode"></param>
		/// <param name="address"></param>
		/// <param name="ref_code"></param>
		/// <returns></returns>
		public bool UpdateGeoMasterInformation(string gid, int geocode, float x, float y, string postcode, string address, string ref_code)
		{
			bool bRet = false;
			Config cfg = Config.GetConfig;
			String sQuery = String.Empty;

			try
			{
				using (OdbcConnection conn = cfg.serverDBLayer.GetAvailableConnection())
				{
					if(geocode == -1)
					{
						///	추가 프로세스

						///	1. geo_master 추가
						sQuery = String.Format("INSERT INTO geo_master (x, y, postcode, address, ref_code) VALUES ({1}, {2}, '{3}', '{4}', {5});"
							, geocode, x, y, postcode, address, (String.IsNullOrEmpty(ref_code) ? "null" : "'" + ref_code + "'"));

						using (OdbcCommand comm = new OdbcCommand(sQuery, conn))
						{
							bRet = comm.ExecuteNonQuery() > 0;
						}

						///	2. geocode 카운트 열거
						sQuery = "SELECT MAX(geocode) FROM geo_master;";

						using (OdbcCommand comm = new OdbcCommand(sQuery, conn))
						{
							using (OdbcDataReader dr = comm.ExecuteReader())
							{
								if (dr.Read())
								{
									try
									{
										geocode = Convert.ToInt32(dr.GetValue(0));
									}
									catch { geocode = 1; }
								}
								else geocode = 1;
							}
							comm.Dispose();
						}

						///	3. groups 업데이트
						sQuery = String.Format("UPDATE groups SET geocode = {0} WHERE gid='{1}'", geocode, gid);

						using (OdbcCommand comm = new OdbcCommand(sQuery, conn))
						{
							bRet = comm.ExecuteNonQuery() > 0;
						}

						if (bRet)
						{
							CacheObject.GetInstance.UpdateGroupGeoCode(gid, geocode);
						}

						return bRet;
					}
					else
					{
						///	업데이트

						sQuery = String.Format("UPDATE geo_master SET x = {1}, y = {2}, postcode = '{3}', address = '{4}', ref_code = {5} WHERE geocode = {0};"
							, geocode, x, y, postcode, address, (String.IsNullOrEmpty(ref_code) ? "null" : "'" + ref_code + "'"));

						using (OdbcCommand comm = new OdbcCommand(sQuery, conn))
						{
							bRet = comm.ExecuteNonQuery() > 0;
						}

						if (bRet)
						{
							CacheObject.GetInstance.UpdateGroupGeoCode(gid, geocode);
						}

						return bRet;
					}
				}
			}
			catch (Exception e)
			{
				logger.Error(e.Message + " query : " + sQuery);
			}

			return false;
		}

		/// <summary>
		/// GeoMaster 정보를 없앤다
		/// </summary>
		/// <param name="gid"></param>
		/// <returns></returns>
		public bool RemoveGeoMasterInformation(string gid)
		{
			bool bRet = false;
			Config cfg = Config.GetConfig;
			String sQuery = String.Empty;

			try
			{
				using (OdbcConnection conn = cfg.serverDBLayer.GetAvailableConnection())
				{
					///	3. groups 업데이트
					sQuery = String.Format("UPDATE groups SET geocode = null WHERE gid='{0}'", gid);

					using (OdbcCommand comm = new OdbcCommand(sQuery, conn))
					{
						bRet = comm.ExecuteNonQuery() > 0;
					}

					if (bRet)
					{
						CacheObject.GetInstance.UpdateGroupGeoCode(gid, -1);
					}

					return bRet;
				}
			}
			catch (Exception e)
			{
				logger.Error(e.Message + " query : " + sQuery);
			}

			return false;
		}

        /// <summary>
        /// 실시간 캡쳐 파일을 기록한다.
        /// </summary>
        /// <param name="pid"></param>
        /// <param name="stream"></param>
        /// <returns></returns>
        public bool SetCapture(String pid, byte[] stream)
        {
            try
            {
                return CaptureService.GetInstance.SaveCapture(pid, stream);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }

            return false;
        }
        /// <summary>
        /// 해당 플레이어에 SMIL을 재생 요청한다.
        /// </summary>
        /// <param name="pid"></param>
        /// <param name="smil"></param>
        /// <param name="isSync"></param>
        /// <returns></returns>
        public bool RunSmilToPlayer(string[] pid, string[] smil, bool isSync)
        {
            logger.Info("RunSmilToPlayer 시작!");

            try
            {
                if (pid == null || pid.Length == 0 || smil == null || smil.Length == 0)
                {
                    logger.Info("파라메터 부족");

                    return false;
                }

                /// 동기화는 나중에 고민!!
                /// 
                if (isSync)
                {
                    Guid syncGroupID = Guid.NewGuid();

                    String sIPList = String.Empty;

                    for (int i = 1; i < pid.Length; ++i)
                    {
                        XmlNode pnode = CacheObject.GetInstance.FindPlayerNode(pid[i]);

                        if (pnode != null)
                        {
                            sIPList += (pnode.Attributes["_ipv4addr"].Value + ",");
                        }

                        ResponseEventHelper.GetInstance.OnSendMessageServerToPlayer(this, new ResponseEventArgs() { PlayerID = pid[i], ResponseCode = iVisionCode.ETRI_RESPONSE_SYNC_SMIL, Content = String.Format("{0}|S|{1}", syncGroupID.ToString(), smil[i]) });

                    }

                    ResponseEventHelper.GetInstance.OnSendMessageServerToPlayer(this, new ResponseEventArgs() { PlayerID = pid[0], ResponseCode = iVisionCode.ETRI_RESPONSE_SYNC_LIST, Content = String.Format("{0}|{1}", syncGroupID.ToString(), sIPList.TrimEnd(',')) });

                    ResponseEventHelper.GetInstance.OnSendMessageServerToPlayer(this, new ResponseEventArgs() { PlayerID = pid[0], ResponseCode = iVisionCode.ETRI_RESPONSE_SYNC_SMIL, Content = String.Format("{0}|M|{1}", syncGroupID.ToString(), smil[0]) });
                }
                else
                {
                    for (int i = 0; i < pid.Length; ++i)
                    {
                        string playerID = pid[i];
                        string smilXML = string.Empty;
                        try
                        {
                            smilXML = smil[i];
                        }
                        catch { smilXML = smil[0]; }

                        ResponseEventHelper.GetInstance.OnSendMessageServerToPlayer(this, new ResponseEventArgs() { PlayerID = playerID, ResponseCode = iVisionCode.ETRI_RESPONSE_SMIL, Content = smilXML });
                    }
                }

                return true;

            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            finally
            {
                logger.Info("RunSmilToPlayer 종료!");
            }

            return false;
        }

        /// <summary>
        /// 해당 그룹에 SMIL을 재생 요청한다.
        /// </summary>
        /// <param name="gid"></param>
        /// <param name="smil"></param>
        /// <param name="isSync"></param>
        /// <returns></returns>
        public bool RunSmilToGroup(string gid, string[] smil, bool isSync)
        {
            logger.Info("RunSmilToGroup 시작!");

            try
            {
                if (gid == null || smil == null || smil.Length == 0)
                {
                    logger.Info("파라메터 부족");

                    return false;
                }

                /// 동기화는 나중에 고민!!
                /// 

                XmlNode gnode = CacheObject.GetInstance.FindGroupNode(gid);

                if (gnode == null)
                {
                    logger.Info("해당 그룹이 없습니다. " + gid);

                    return false;
                }

                XmlNodeList list = gnode.SelectNodes("descendant::player[attribute::del_yn=\"N\"]");

                logger.Info("List 길이 : " + list.Count);
                if (isSync)
                {
                    Guid syncGroupID = Guid.NewGuid();

                    String sIPList = String.Empty;
                    for (int i = 1; i < list.Count; ++i)
                    {
                        XmlNode pnode = list[i];

                        if (pnode != null)
                        {
                            sIPList += (pnode.Attributes["_ipv4addr"].Value + ",");
                        }
                        logger.Info("Send 메시지: {0}, {1}, {2}" + pnode.Attributes["pid"].Value, pnode.Attributes["name"].Value, smil[i]);

                        ResponseEventHelper.GetInstance.OnSendMessageServerToPlayer(this, new ResponseEventArgs() { PlayerID = pnode.Attributes["pid"].Value, ResponseCode = iVisionCode.ETRI_RESPONSE_SYNC_SMIL, Content = String.Format("{0}|S|{1}", syncGroupID.ToString(), smil[i]) });

                    }

                    logger.Info("Send 메시지: {0}, {1}, {2}" + list[0].Attributes["pid"].Value, list[0].Attributes["name"].Value, smil[0]);

                    ResponseEventHelper.GetInstance.OnSendMessageServerToPlayer(this, new ResponseEventArgs() { PlayerID = list[0].Attributes["pid"].Value, ResponseCode = iVisionCode.ETRI_RESPONSE_SYNC_LIST, Content = String.Format("{0}|{1}", syncGroupID.ToString(), sIPList.TrimEnd(',')) });

                    ResponseEventHelper.GetInstance.OnSendMessageServerToPlayer(this, new ResponseEventArgs() { PlayerID = list[0].Attributes["pid"].Value, ResponseCode = iVisionCode.ETRI_RESPONSE_SYNC_SMIL, Content = String.Format("{0}|M|{1}", syncGroupID.ToString(), smil[0]) });
                }
                else
                {

                    int i = 0;

                    foreach (XmlNode pnode in list)
                    {
                        string playerID = pnode.Attributes["pid"].Value;
                        string smilXML = string.Empty;
                        try
                        {
                            smilXML = smil[i++];
                        }
                        catch { smilXML = smil[i = 0]; }

                        logger.Info("Send 메시지: {0}, {1}, {2}" + pnode.Attributes["pid"].Value, pnode.Attributes["name"].Value, smil[i]);

                        ResponseEventHelper.GetInstance.OnSendMessageServerToPlayer(this, new ResponseEventArgs() { PlayerID = playerID, ResponseCode = iVisionCode.ETRI_RESPONSE_SMIL, Content = smilXML });
                    }
                }
                return true;

            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            finally
            {
                logger.Info("RunSmilToGroup 종료!");
            }

            return false;
        }


        /// <summary>
        /// 해당 플레이어에 자막을 재생한다.
        /// </summary>
        /// <param name="pid"></param>
        /// <param name="subtitleText"></param>
        /// <param name="startDT"></param>
        /// <param name="endDT"></param>
        /// <returns></returns>
        public bool RunSubtitleToPlayer(string[] pid, string[] subtitleText, string startDT, string endDT)
        {
            logger.Info("RunSubtitleToPlayer 시작!");

            try
            {
                if (pid == null || pid.Length == 0 || subtitleText == null || subtitleText.Length == 0)
                {
                    logger.Info("파라메터 부족");

                    return false;
                }

                for (int i = 0; i < pid.Length; ++i)
                {
                    string playerID = pid[i];
                    string smilXML = string.Empty;
                    try
                    {
                        smilXML = subtitleText[i];
                    }
                    catch { smilXML = subtitleText[0]; }

                    Guid guid = Guid.NewGuid();

                    ResponseEventHelper.GetInstance.OnSendMessageServerToPlayer(this, new ResponseEventArgs() { PlayerID = playerID, ResponseCode = iVisionCode.ETRI_RESPONSE_SUBTITLE, Content = guid.ToString() + "|" +smilXML });
                }

                return true;

            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            finally
            {
                logger.Info("RunSubtitleToPlayer 종료!");
            }

            return false;
        }

        /// <summary>
        /// 해당 그룹에 자막을 재생한다.
        /// </summary>
        /// <param name="gid"></param>
        /// <param name="subtitleText"></param>
        /// <param name="startDT"></param>
        /// <param name="endDT"></param>
        /// <returns></returns>
        public bool RunSubtitleToGroup(string gid, string[] subtitleText, string startDT, string endDT)
        {
            logger.Info("RunSubtitleToGroup 시작!");

            try
            {
                if (gid == null || subtitleText == null || subtitleText.Length == 0)
                {
                    logger.Info("파라메터 부족");

                    return false;
                }


                XmlNode gnode = CacheObject.GetInstance.FindGroupNode(gid);

                if (gnode == null)
                {
                    logger.Info("해당 그룹이 없습니다. " + gid);

                    return false;
                }

                XmlNodeList list = gnode.SelectNodes("descendant::player[attribute::del_yn=\"N\"]");


                int i = 0;

                foreach (XmlNode pnode in list)
                {
                    string playerID = pnode.Attributes["pid"].Value;
                    string smilXML = string.Empty;
                    try
                    {
                        smilXML = subtitleText[i++];
                    }
                    catch { smilXML = subtitleText[i = 0]; }

                    Guid guid = Guid.NewGuid();

                    ResponseEventHelper.GetInstance.OnSendMessageServerToPlayer(this, new ResponseEventArgs() { PlayerID = playerID, ResponseCode = iVisionCode.ETRI_RESPONSE_SUBTITLE, Content = guid.ToString() + "|" + smilXML });
                }

                return true;

            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            finally
            {
                logger.Info("RunSubtitleToGroup 종료!");
            }

            return false;
        }


        /// <summary>
        /// 해당 플레이어에 이벤트를 전송한다.
        /// </summary>
        /// <param name="pid"></param>
        /// <param name="eventValues"></param>
        /// <returns></returns>
        public bool RunEventToPlayer(string[] pid, string[] eventValues)
        {
            logger.Info("RunEventToPlayer 시작!");

            try
            {
                if (pid == null || pid.Length == 0 || eventValues == null || eventValues.Length == 0)
                {
                    logger.Info("파라메터 부족");

                    return false;
                }

                for (int i = 0; i < pid.Length; ++i)
                {
                    string playerID = pid[i];
                    string smilXML = string.Empty;
                    try
                    {
                        smilXML = eventValues[i];
                    }
                    catch { smilXML = eventValues[0]; }

                    ResponseEventHelper.GetInstance.OnSendMessageServerToPlayer(this, new ResponseEventArgs() { PlayerID = playerID, ResponseCode = iVisionCode.ETRI_RESPONSE_EVENT, Content = smilXML });
                }

                return true;

            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            finally
            {
                logger.Info("RunEventToPlayer 종료!");
            }

            return false;
        }

        /// <summary>
        /// 해당 그룹에 이벤트를 전송한다.
        /// </summary>
        /// <param name="gid"></param>
        /// <param name="eventValues"></param>
        /// <returns></returns>
        public bool RunEventToGroup(string gid, string[] eventValues)
        {
            logger.Info("RunEventToGroup 시작!");

            try
            {
                if (gid == null || eventValues == null || eventValues.Length == 0)
                {
                    logger.Info("파라메터 부족");

                    return false;
                }


                XmlNode gnode = CacheObject.GetInstance.FindGroupNode(gid);

                if (gnode == null)
                {
                    logger.Info("해당 그룹이 없습니다. " + gid);

                    return false;
                }

                XmlNodeList list = gnode.SelectNodes("descendant::player[attribute::del_yn=\"N\"]");


                int i = 0;

                foreach (XmlNode pnode in list)
                {
                    string playerID = pnode.Attributes["pid"].Value;
                    string smilXML = string.Empty;
                    try
                    {
                        smilXML = eventValues[i++];
                    }
                    catch { smilXML = eventValues[i = 0]; }

                    ResponseEventHelper.GetInstance.OnSendMessageServerToPlayer(this, new ResponseEventArgs() { PlayerID = playerID, ResponseCode = iVisionCode.ETRI_RESPONSE_EVENT, Content = smilXML });
                }

                return true;

            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            finally
            {
                logger.Info("RunEventToPlayer 종료!");
            }

            return false;
        }

	}
}
