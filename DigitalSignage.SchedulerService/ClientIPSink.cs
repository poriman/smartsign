﻿using System;
using System.Net;

// .NET remoting
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Messaging;

namespace DigitalSignage.SchedulerService
{
    class ClientIPSink : BaseChannelObjectWithProperties, IServerChannelSink
    {
        private IServerChannelSink nextSink;
        public ClientIPSink(IServerChannelSink nextSink)
        {
            this.nextSink = nextSink;
        }

        #region IServerChannelSink Members
        public System.IO.Stream GetResponseStream(IServerResponseChannelSinkStack
        sinkStack, object state, IMessage msg, ITransportHeaders headers)
        {
            return null;
        }
        
        public System.Runtime.Remoting.Channels.ServerProcessing ProcessMessage(IServerChannelSinkStack sinkStack, 
            IMessage requestMsg,
            ITransportHeaders requestHeaders, System.IO.Stream requestStream, out
            IMessage responseMsg, out ITransportHeaders responseHeaders, out
            System.IO.Stream responseStream)
        {
            //get the client's ip address, and put it in the call context. This value will be
            //extracted later so we can determine the actual address of the client.
            try
            {
                IPAddress ipAddr = (IPAddress)requestHeaders[CommonTransportKeys.IPAddress];
                 CallContext.SetData("ClientIP", ipAddr);
            }
            catch(Exception)
            {
                //do nothing
            }
            //pushing onto stack and forwarding the call
            sinkStack.Push(this, null);
            ServerProcessing srvProc = nextSink.ProcessMessage(sinkStack, requestMsg,
                requestHeaders, requestStream, out responseMsg, out responseHeaders, out responseStream);
            if (srvProc == ServerProcessing.Complete)
            {
                //TODO - implement post processing
            }
            return srvProc;
        }
        
        public void AsyncProcessResponse(IServerResponseChannelSinkStack sinkStack, object state, 
                                         IMessage msg, ITransportHeaders headers, System.IO.Stream stream)
        {
            // get the client's ip address, and put it in the call context. This value will be
            // extracted later so we can determine the actual address of the client.
            try
            {
                IPAddress ipAddr = (IPAddress)headers[CommonTransportKeys.IPAddress];
                CallContext.SetData("ClientIP", ipAddr);
            }
            catch(Exception)
            {
                //do nothing
            }
            //forward to stack for further processing
            sinkStack.AsyncProcessResponse(msg, headers, stream);
        }

        public IServerChannelSink NextChannelSink
        {
            get
            {
                return nextSink;
            }
        }
        #endregion
    }

    class ClientIPProvider : IServerChannelSinkProvider
    {
        private IServerChannelSinkProvider nextProvider;

        #region IServerChannelSinkProvider Members
        public IServerChannelSink CreateSink(IChannelReceiver channel)
        {
            //create other sinks in the chain
            IServerChannelSink nextSink = nextProvider.CreateSink(channel);
            return new ClientIPSink(nextSink);
        }

        public IServerChannelSinkProvider Next
        {
            get
            {
                return nextProvider;
            }
            set
            {
                nextProvider = value;
            }
        }
        public void GetChannelData(IChannelDataStore channelData)
        {
            //not needed
        }
        #endregion
    }
}
