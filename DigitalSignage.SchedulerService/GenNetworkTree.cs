﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using NLog;
using DigitalSignage.ServerDatabase;

namespace DigitalSignage.SchedulerService
{
	public class GenNetworkTree
	{
		public static DBLayer ServerDBLayer = null;
		/// <summary>
		/// 그룹에 해당하는 Tree를 생성한다.
		/// </summary>
		/// <param name="groupID"></param>
		/// <param name="parent_Node"></param>
		/// <returns></returns>
		public static XmlDocument GenerateNetworkTree(XmlDocument doc, string groupID)
		{
			if (ServerDBLayer == null)
				throw new Exception("There is no DB Layer.");
			if (String.IsNullOrEmpty(groupID))
				throw new ArgumentException("group ID is wrong.");

			XmlNode rootnode = doc.CreateNode(XmlNodeType.Element, "Datalist", null);			
			doc.AppendChild(rootnode);

			using (DigitalSignage.ServerDatabase.ServerDataSetTableAdapters.usersTableAdapter uta = ServerDBLayer.CreateUser_DA())
			{
				using (ServerDataSet.usersDataTable udt = uta.GetRootUser())
				{
					foreach (ServerDataSet.usersRow row in udt.Rows)
					{
						XmlNode cNode = ConvertUserRow2Xml(doc, row);

						GenerateRoleTree(doc, cNode, row.role_ids);
						GenerateUserTree(doc, cNode, (int)row.id);

						rootnode.AppendChild(cNode);
					}
					udt.Dispose();
				}
				uta.Dispose();
			}

			using (DigitalSignage.ServerDatabase.ServerDataSetTableAdapters.groupsTableAdapter gta = ServerDBLayer.CreateGroup_DA())
			{
				using (ServerDataSet.groupsDataTable gdt = gta.GetData(groupID))
				{
					foreach (ServerDataSet.groupsRow row in gdt.Rows)
					{
						XmlNode cNode = ConvertGroupRow2Xml(doc, row);

						GenerateGroupTree(doc, cNode, row.gid);
						GeneratePlayerTree(doc, cNode, row.gid);

						rootnode.AppendChild(cNode);
					}
					gdt.Dispose();
				}
				gta.Dispose();
			}

			return doc;
		}

		private static void GenerateGroupTree(XmlDocument doc, XmlNode parent_Node, string parent_groupID)
		{
			if (ServerDBLayer == null)
				throw new Exception("There is no DB Layer.");

			if (String.IsNullOrEmpty(parent_groupID))
				throw new ArgumentException("group ID is wrong.");

			using (DigitalSignage.ServerDatabase.ServerDataSetTableAdapters.groupsTableAdapter gta = ServerDBLayer.CreateGroup_DA())
			{
				using (ServerDataSet.groupsDataTable gdt = gta.GetDataByParentGID(parent_groupID))
				{
					foreach (ServerDataSet.groupsRow row in gdt.Rows)
					{
						XmlNode cNode = ConvertGroupRow2Xml(doc, row);

						GenerateGroupTree(doc, cNode, row.gid);
						GeneratePlayerTree(doc, cNode, row.gid);

						parent_Node.AppendChild(cNode);
				}
					gdt.Dispose();
				}
				gta.Dispose();
			}
		}

		private static void GeneratePlayerTree(XmlDocument doc, XmlNode parent_Node, string parent_groupID)
		{
			if (ServerDBLayer == null)
				throw new Exception("There is no DB Layer.");

			if (String.IsNullOrEmpty(parent_groupID))
				throw new ArgumentException("group ID is wrong.");

			using (DigitalSignage.ServerDatabase.ServerDataSetTableAdapters.players_detail_ASNTableAdapter pta = ServerDBLayer.CreatePlayerDetailASN_DA())
			{
				using (ServerDataSet.players_detail_ASNDataTable pdt = pta.GetDataByGID(parent_groupID))
				{
					foreach (ServerDataSet.players_detail_ASNRow row in pdt.Rows)
					{
						XmlNode cNode = ConvertPlayerRow2Xml(doc, row);

						parent_Node.AppendChild(cNode);
					}
					pdt.Dispose();
				}
				pta.Dispose();
			}
		}

		private static void GenerateUserTree(XmlDocument doc, XmlNode parent_Node, int parent_id)
		{
			if (ServerDBLayer == null)
				throw new Exception("There is no DB Layer.");

			using (DigitalSignage.ServerDatabase.ServerDataSetTableAdapters.usersTableAdapter uta = ServerDBLayer.CreateUser_DA())
			{
				using (ServerDataSet.usersDataTable udt = uta.GetUsersByParentID(parent_id))
				{
					foreach (ServerDataSet.usersRow row in udt.Rows)
					{
						XmlNode cNode = ConvertUserRow2Xml(doc, row);
						GenerateRoleTree(doc, cNode, row.role_ids);
						GenerateUserTree(doc, cNode, (int)row.id);

						parent_Node.AppendChild(cNode);
					}
					udt.Dispose();
				}
				uta.Dispose();
			}
		}

		public static void GenerateRoleTree(XmlDocument doc, XmlNode parent_Node, string role_ids)
		{
			if (ServerDBLayer == null)
				throw new Exception("There is no DB Layer.");

			string[] arrRoles = role_ids.Split('|');

			foreach(string role_id in arrRoles)
			{
				using (DigitalSignage.ServerDatabase.ServerDataSetTableAdapters.rolesTableAdapter rta = ServerDBLayer.CreateRole_DA())
				{
					using (ServerDataSet.rolesDataTable rdt = rta.GetRole(Convert.ToInt64(role_id)))
					{
						foreach (ServerDataSet.rolesRow row in rdt.Rows)
						{
							XmlNode cNode = ConvertRoleRow2Xml(doc, row);

							GeneratePermissionTree(doc, cNode, Convert.ToInt64(role_id));

							parent_Node.AppendChild(cNode);
						}
						rdt.Dispose();
					}
					rta.Dispose();
				}
			}
		}

		private static void GeneratePermissionTree(XmlDocument doc, XmlNode parent_Node, long role_id)
		{
			if (ServerDBLayer == null)
				throw new Exception("There is no DB Layer.");

			using (DigitalSignage.ServerDatabase.ServerDataSetTableAdapters.role_perm_ASNTableAdapter rta = ServerDBLayer.Createrole_perm_ASN_DA())
			{
				using (ServerDataSet.role_perm_ASNDataTable rdt = rta.GetPermissionsByRoleID(Convert.ToInt64(role_id)))
				{
					foreach (ServerDataSet.role_perm_ASNRow row in rdt.Rows)
					{
						XmlNode cNode = ConvertPermissionRow2Xml(doc, row);
						
						parent_Node.AppendChild(cNode);
					}
					rdt.Dispose();
				}
				rta.Dispose();
			}
		}

		public static XmlNode ConvertGroupRow2Xml(XmlDocument doc, ServerDataSet.groupsRow row)
		{
			return ConvertGroupData2Xml(doc, row.gid, row.name, row.source, row.username, row.password,
				row.parent_gid, row.is_inherited, row.depth, row.path, row.geocode, row.del_yn);
		}

		public static XmlNode ConvertGroupData2Xml(XmlDocument doc, string gid, string name, string source, string username, string password, string parent_gid,
			string is_inherited, int depth, string path, int geocode, string isDeleted)
		{
			XmlNode node = doc.CreateNode(XmlNodeType.Element, "group", null);
			XmlAttribute attr1 = doc.CreateAttribute("gid");
			attr1.Value = gid;
			XmlAttribute attr2 = doc.CreateAttribute("name");
			attr2.Value = name;
			XmlAttribute attr3 = doc.CreateAttribute("source");
			attr3.Value = source;
			XmlAttribute attr4 = doc.CreateAttribute("username");
			attr4.Value = username;
			XmlAttribute attr5 = doc.CreateAttribute("password");
			attr5.Value = password;
			XmlAttribute attr6 = doc.CreateAttribute("parent_gid");
			attr6.Value = parent_gid;
			XmlAttribute attr7 = doc.CreateAttribute("is_inherited");
			attr7.Value = is_inherited;
			XmlAttribute attr8 = doc.CreateAttribute("depth");
			attr8.Value = depth.ToString();
			XmlAttribute attr9 = doc.CreateAttribute("path");
			attr9.Value = path;
			XmlAttribute attr10 = doc.CreateAttribute("del_yn");
			attr10.Value = isDeleted;
			XmlAttribute attr11 = doc.CreateAttribute("_timestamp");
			attr11.Value = DateTime.UtcNow.Ticks.ToString();
			XmlAttribute attr12 = doc.CreateAttribute("_modified");
			attr12.Value = DateTime.UtcNow.Ticks.ToString();
            
            XmlAttribute attr13 = doc.CreateAttribute("geocode");
			attr13.Value = geocode.ToString();
			
			node.Attributes.Append(attr1);
			node.Attributes.Append(attr2);
			node.Attributes.Append(attr3);
			node.Attributes.Append(attr4);
			node.Attributes.Append(attr5);
			node.Attributes.Append(attr6);
			node.Attributes.Append(attr7);
			node.Attributes.Append(attr8);
			node.Attributes.Append(attr9);
			node.Attributes.Append(attr10);
			node.Attributes.Append(attr11);
			node.Attributes.Append(attr12);
            node.Attributes.Append(attr13);

			return node;
		}

		public static XmlNode ConvertPlayerRow2Xml(XmlDocument doc, ServerDataSet.players_detail_ASNRow row)
		{
			return ConvertPlayerData2Xml(doc, row.pid, row.gid, row.name, row.host, row.state, 0,
				"", 0, 0, 0, 0, row.versionH, row.versionL, 0, 0, 0, "", "", "", "", row._ipv4addr, row._macaddr, row._is_dhcp, row._mnttype, row._auth_id, row._auth_pass, row.lastconndt, row.del_yn, row.tz_info);

			//return ConvertPlayerData2Xml(doc, row.pid, row.gid, row.name, row.host, row.state, row.freespace,
			//    row.port, row.ComSpeed, row.ComBits, row.temperature, row.uptime, row.versionH, row.versionL,
			//    row.angle, row.memoryusage, row.cpurate, row.curr_subtitle, row.curr_screen, row.curr_d_progress, row.all_d_progress, row._ipv4addr, row._macaddr, row._is_dhcp, row._mnttype, row._auth_id, row._auth_pass);
		}

		//public static XmlNode ConvertPlayerData2Xml(XmlDocument doc, string pid, string gid, string name, string host, int state,
		//    int freespace, string port, int comspeed, int combits, int temperature, int uptime, int versionH, int versionL, int angle,
		//    int memoryusage, int cpurate, string curr_subtitle, string curr_screen, string curr_d_progress, string all_d_progress, string _ipv4addr, string _macaddr, string _is_dhcp, int _mnttype, string _auth_id, string _auth_pass)
		//{
		//    return ConvertPlayerData2Xml(doc, pid, gid, name, host, state, freespace, port, comspeed, combits, temperature, uptime, versionH, versionL, angle, memoryusage, cpurate, curr_subtitle, curr_screen, curr_d_progress, all_d_progress, _ipv4addr, _macaddr, _is_dhcp, _mnttype, _auth_id, _auth_pass, "", "", -1, false);
		//}

		public static XmlNode ConvertPlayerData2Xml(XmlDocument doc, string pid, string gid, string name, string host, int state, 
			int freespace, string port, int comspeed, int combits, int temperature, int uptime, int versionH, int versionL, int angle,
			int memoryusage, int cpurate, string curr_subtitle, string curr_screen, string curr_d_progress, string all_d_progress, string _ipv4addr, string _macaddr, string _is_dhcp, int _mnttype, string _auth_id, string _auth_pass, long lastconndt, string isDeleted, string timezone)
		{
			long lConn = lastconndt <= 0 ? DateTime.UtcNow.Ticks : lastconndt;

			XmlNode node = doc.CreateNode(XmlNodeType.Element, "player", null);
			XmlAttribute attr1 = doc.CreateAttribute("pid");
			attr1.Value = pid;
			XmlAttribute attr2 = doc.CreateAttribute("gid");
			attr2.Value = gid;
			XmlAttribute attr3 = doc.CreateAttribute("name");
			attr3.Value = name;
			XmlAttribute attr4 = doc.CreateAttribute("host");
			attr4.Value = host;
			XmlAttribute attr5 = doc.CreateAttribute("state");
			attr5.Value = state.ToString();
			XmlAttribute attr6 = doc.CreateAttribute("freespace");
			attr6.Value = freespace.ToString();
			XmlAttribute attr7 = doc.CreateAttribute("port");
			attr7.Value = port;
			XmlAttribute attr8 = doc.CreateAttribute("ComSpeed");
			attr8.Value = comspeed.ToString();
			XmlAttribute attr9 = doc.CreateAttribute("ComBits");
			attr9.Value = combits.ToString();
			XmlAttribute attr10 = doc.CreateAttribute("temperature");
			attr10.Value = temperature.ToString();
			XmlAttribute attr11 = doc.CreateAttribute("uptime");
			attr11.Value = uptime.ToString();
			XmlAttribute attr12 = doc.CreateAttribute("versionH");
			attr12.Value = versionH.ToString();
			XmlAttribute attr13 = doc.CreateAttribute("versionL");
			attr13.Value = versionL.ToString();
			XmlAttribute attr14 = doc.CreateAttribute("angle");
			attr14.Value = angle.ToString();
			XmlAttribute attr15 = doc.CreateAttribute("memoryusage");
			attr15.Value = memoryusage.ToString();
			XmlAttribute attr16 = doc.CreateAttribute("cpurate");
			attr16.Value = cpurate.ToString();
			XmlAttribute attr17 = doc.CreateAttribute("curr_subtitle");
			attr17.Value = curr_subtitle;
			XmlAttribute attr18 = doc.CreateAttribute("curr_screen");
			attr18.Value = curr_screen;
			XmlAttribute attr19 = doc.CreateAttribute("curr_d_progress");
			attr19.Value = curr_d_progress;
			XmlAttribute attr20 = doc.CreateAttribute("all_d_progress");
			attr20.Value = all_d_progress;
			XmlAttribute attr21 = doc.CreateAttribute("_serial_device_name");
			attr21.Value = "None";
			XmlAttribute attr22 = doc.CreateAttribute("_serial_stat_power");
			attr22.Value = "-";
			XmlAttribute attr23 = doc.CreateAttribute("_serial_stat_source");
			attr23.Value = "-";
			XmlAttribute attr24 = doc.CreateAttribute("_serial_stat_volume");
			attr24.Value = "-1";
			XmlAttribute attr25 = doc.CreateAttribute("_ipv4addr");
			attr25.Value = _ipv4addr;
			XmlAttribute attr26 = doc.CreateAttribute("_macaddr");
			attr26.Value = _macaddr;
			XmlAttribute attr27 = doc.CreateAttribute("_is_dhcp");
			attr27.Value = _is_dhcp;
			XmlAttribute attr28 = doc.CreateAttribute("_mnttype");
			attr28.Value = _mnttype.ToString();
			XmlAttribute attr29 = doc.CreateAttribute("_auth_id");
			attr29.Value = _auth_id;
			XmlAttribute attr30 = doc.CreateAttribute("_auth_pass");
			attr30.Value = _auth_pass;
			XmlAttribute attr31 = doc.CreateAttribute("lastconndt");
			attr31.Value = lConn.ToString();
			XmlAttribute attr32 = doc.CreateAttribute("del_yn");
			attr32.Value = isDeleted;
			XmlAttribute attr33 = doc.CreateAttribute("timezone");
			attr33.Value = timezone;
            XmlAttribute attr34 = doc.CreateAttribute("curr_volume");
            attr34.Value = "-1";
			XmlAttribute attr35 = doc.CreateAttribute("_timestamp");
			attr35.Value = lConn.ToString();
			XmlAttribute attr36 = doc.CreateAttribute("_modified");
			attr36.Value = lConn.ToString();
            XmlAttribute attr37 = doc.CreateAttribute("curr_screen_uuid");
            attr37.Value = "";
            XmlAttribute attr38 = doc.CreateAttribute("geocode");
            attr38.Value = "-1";

            node.Attributes.Append(attr1);
			node.Attributes.Append(attr2);
			node.Attributes.Append(attr3);
			node.Attributes.Append(attr4);
			node.Attributes.Append(attr5);
			node.Attributes.Append(attr6);
			node.Attributes.Append(attr7);
			node.Attributes.Append(attr8);
			node.Attributes.Append(attr9);
			node.Attributes.Append(attr10);
			node.Attributes.Append(attr11);
			node.Attributes.Append(attr12);
			node.Attributes.Append(attr13);
			node.Attributes.Append(attr14);
			node.Attributes.Append(attr15);
			node.Attributes.Append(attr16);
			node.Attributes.Append(attr17);
			node.Attributes.Append(attr18);
			node.Attributes.Append(attr19);
			node.Attributes.Append(attr20);
			node.Attributes.Append(attr21);
			node.Attributes.Append(attr22);
			node.Attributes.Append(attr23);
			node.Attributes.Append(attr24);
			node.Attributes.Append(attr25);
			node.Attributes.Append(attr26);
			node.Attributes.Append(attr27);
			node.Attributes.Append(attr28);
			node.Attributes.Append(attr29);
			node.Attributes.Append(attr30);
			node.Attributes.Append(attr31);
			node.Attributes.Append(attr32);
			node.Attributes.Append(attr33);
            node.Attributes.Append(attr34);
			node.Attributes.Append(attr35);
            node.Attributes.Append(attr36);
            node.Attributes.Append(attr37);
            node.Attributes.Append(attr38);

			return node;
		}

		public static XmlNode ConvertUserRow2Xml(XmlDocument doc, ServerDataSet.usersRow row)
		{
			return ConvertUserData2Xml(doc, row.id, row.name, row.hash, row.parent_id, row.role_ids, row.is_enabled);
		}

		public static XmlNode ConvertUserData2Xml(XmlDocument doc, long id, string name, string hash, long parent_id, string role_ids, string is_enabled)
		{
			XmlNode node = doc.CreateNode(XmlNodeType.Element, "user", null);
			XmlAttribute attr1 = doc.CreateAttribute("id");
			attr1.Value = id.ToString();
			XmlAttribute attr2 = doc.CreateAttribute("name");
			attr2.Value = name;
			XmlAttribute attr3 = doc.CreateAttribute("hash");
			attr3.Value = hash;
			XmlAttribute attr4 = doc.CreateAttribute("parent_id");
			attr4.Value = parent_id.ToString();
			XmlAttribute attr5 = doc.CreateAttribute("role_ids");
			attr5.Value = role_ids;
			XmlAttribute attr6 = doc.CreateAttribute("is_enabled");
			attr6.Value = is_enabled;
			XmlAttribute attr7 = doc.CreateAttribute("_timestamp");
			attr7.Value = DateTime.UtcNow.Ticks.ToString();

			node.Attributes.Append(attr1);
			node.Attributes.Append(attr2);
			node.Attributes.Append(attr3);
			node.Attributes.Append(attr4);
			node.Attributes.Append(attr5);
			node.Attributes.Append(attr6);
			node.Attributes.Append(attr7);

			return node;
		}

		public static XmlNode ConvertRoleRow2Xml(XmlDocument doc, ServerDataSet.rolesRow row)
		{
			XmlNode node = doc.CreateNode(XmlNodeType.Element, "role", null);
			XmlAttribute attr1 = doc.CreateAttribute("role_id");
			attr1.Value = row.role_id.ToString();
			XmlAttribute attr2 = doc.CreateAttribute("role_name");
			attr2.Value = row.role_name;
			XmlAttribute attr3 = doc.CreateAttribute("role_level");
			attr3.Value = row.role_level.ToString();
			XmlAttribute attr4 = doc.CreateAttribute("is_enabled");
			attr4.Value = row.is_enabled;
			XmlAttribute attr5 = doc.CreateAttribute("_timestamp");
			attr5.Value = DateTime.UtcNow.Ticks.ToString();

			node.Attributes.Append(attr1);
			node.Attributes.Append(attr2);
			node.Attributes.Append(attr3);
			node.Attributes.Append(attr4);
			node.Attributes.Append(attr5);

			return node;
		}

		public static XmlNode ConvertPermissionRow2Xml(XmlDocument doc, ServerDataSet.role_perm_ASNRow row)
		{
			XmlNode node = doc.CreateNode(XmlNodeType.Element, "permission", null);
			XmlAttribute attr1 = doc.CreateAttribute("perm_id");
			attr1.Value = row.perm_id.ToString();
			XmlAttribute attr2 = doc.CreateAttribute("perm_name");
			attr2.Value = row.perm_name;
			XmlAttribute attr3 = doc.CreateAttribute("parent_perm_id");
			attr3.Value = row.parent_perm_id.ToString();
			XmlAttribute attr4 = doc.CreateAttribute("_timestamp");
			attr4.Value = DateTime.UtcNow.Ticks.ToString();

			node.Attributes.Append(attr1);
			node.Attributes.Append(attr2);
			node.Attributes.Append(attr3);
			node.Attributes.Append(attr4);

			return node;
		}
	}
}
