﻿using System;
using System.Data.Common;
//using System.Data;

using NLog;

using DigitalSignage.DataBase;
using DigitalSignage.Common;

namespace DigitalSignage.SchedulerService
{
    public class CmdList : MarshalByRefObject, ICmdList
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public int Update(AuthTicket at, DataSet.RSCommandsDataTable data)
        {
            Config cfg = Config.GetConfig;
            int res = -1;
            try
            {
                at = UsersList.GetUserLevel(at);
                if (at.level == UserLevels.UserNone) throw new Exception("Unauthorized"); 
                DataBase.DataSetTableAdapters.RSCommandsTableAdapter da = cfg.dbLayer.CreateCmdDA();
                DataBase.DataSetTableAdapters.tasksTableAdapter tda = cfg.dbLayer.CreateTaskDA();
                for (int i = 0; i < data.Rows.Count; i++)
                {
                    DataSet.RSCommandsRow row = (DataSet.RSCommandsRow)data.Rows[i];
                    if (row.id < 1) //new record
                    {
                        da.AddCommand(row.name, row.data);
                    }
                    else
                    {
                        // updating old record
                        da.UpdateCommand(row.data, row.name, row.id);
                        
                        // updating task table
                        DataSet.RSCommandsDataTable updatedTable = da.GetCommand(row.id);
                        row = (DataSet.RSCommandsRow)updatedTable.Rows[0];

                        System.Guid duuid = new System.Guid((int)row.id, (short)((row.etime & 0xFFFF0000) >> 16),
                            (short)(row.etime & 0xFFFF), new byte[] { 0, 0, 0, 0, 0, 0, 0, 0 });
                        string oldduuid = duuid.ToString(); 
                        
                        duuid = new System.Guid((int)row.id, (short)((row.etime & 0xFFFF0000) >> 16),
                            (short)(row.etime & 0xFFFF), new byte[] { 0, 0, 0, 0, 0, 0, 0, 0 });
                        string newduuid = duuid.ToString();
                        
                        DataSet.tasksDataTable tasks = tda.GetTasksByDuuid(oldduuid, (int)TaskState.StateWait);
                        for (int j = 0; i < tasks.Rows.Count; j++)
                        {
                            DataSet.tasksRow task = tasks.Rows[j] as DataSet.tasksRow;
                            tda.UpdateTask(task.pid, task.gid, task.starttime, task.endtime, TimeConverter.ConvertToUTP(DateTime.Now), task.type,
								task.name, newduuid, task.duration, task.daysofweek, task.priority, task.endtimeofday, task.starttimeofday, task.state, task.uuid);
                        }
                    }
                }
                res = 0;
            }
            catch (Exception e)
            {
                logger.Log(LogLevel.Error, e + "");
            }
            return res;
        }

        public int Delete(AuthTicket at, int id)
        {
            Config cfg = Config.GetConfig;
            try
            {
                at = UsersList.GetUserLevel(at);
                if (at.level == UserLevels.UserNone) throw new Exception("Unauthorized");
                DataBase.DataSetTableAdapters.RSCommandsTableAdapter da = cfg.dbLayer.CreateCmdDA();
                return da.DeleteCommand(id);
            }
            catch (Exception e)
            {
                logger.Log(LogLevel.Error, e + "");
            }
            return -1;
        }

        public DataSet.RSCommandsDataTable Get(int id)
        {
            Config cfg = Config.GetConfig;
            try
            {
                DataBase.DataSetTableAdapters.RSCommandsTableAdapter da = cfg.dbLayer.CreateCmdDA();
                return da.GetCommand(id);
            }
            catch (Exception e)
            {
                logger.Log(LogLevel.Error, e + "");
            }
            return null;
        }

        public bool IsInUse(int id)
        {
            Config cfg = Config.GetConfig;
            try
            {
                // creating duuid
                DataBase.DataSetTableAdapters.RSCommandsTableAdapter da = cfg.dbLayer.CreateCmdDA();
                DataSet.RSCommandsDataTable table = da.GetCommand(id);
                if (table == null) throw new Exception("Null data table");
                if (table.Rows.Count < 1) throw new Exception("Unknown Command ID");
                DataSet.RSCommandsRow row = table.Rows[0] as DataSet.RSCommandsRow;
                System.Guid duuid = new System.Guid((int)row.id, (short)((row.etime & 0xFFFF0000) >> 16),
                            (short)(row.etime & 0xFFFF), new byte[] { 0, 0, 0, 0, 0, 0, 0, 0 });
                // searching
                DataBase.DataSetTableAdapters.tasksTableAdapter tska = cfg.dbLayer.CreateTaskDA();
                DataSet.tasksDataTable tasks = tska.GetTasksByDuuid(duuid.ToString(), (int)TaskState.StateWait);
                if ((tasks != null) && (tasks.Rows.Count > 0)) return true;

            }
            catch (Exception e)
            {
                logger.Log(LogLevel.Error, e + "");
            }
            return false;
        }

        public DataSet.RSCommandsDataTable GetCommands()
        {
            Config cfg = Config.GetConfig;
            try
            {
                DataBase.DataSetTableAdapters.RSCommandsTableAdapter da = cfg.dbLayer.CreateCmdDA();
                return da.GetCommands();
            }
            catch (Exception e)
            {
                logger.Log(LogLevel.Error, e + "");
            }
            return null;
        }
    }
}
