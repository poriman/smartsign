﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NLog;
using DigitalSignage.EventReciever;
using DigitalSignage.Common;
using System.ComponentModel;
using System.Threading;

namespace DigitalSignage.SchedulerService
{
    public class ControlService
    {
        static readonly object lockObject = new object();

        /// <summary>
        /// Control 대화 세션
        /// </summary>
        public class ControlSession
        {
            /// <summary>
            /// 생성자
            /// </summary>
            /// <param name="id"></param>
            /// <param name="pid"></param>
            /// <param name="request"></param>
            /// <param name="r_value"></param>
            public ControlSession(int id, String pid, ControlRequest request, int r_value)
            {
                ID = id;
                PlayerID = pid;
                RequestCode = request;
                RequestValue = r_value;
                RequestDT = DateTime.Now;
            }
            public int ID = 0;

            public DateTime RequestDT = DateTime.MinValue;
            public String PlayerID = String.Empty;
            public ControlRequest RequestCode = ControlRequest.Undefinded;
            public int RequestValue = 0;
            public ControlResponse? ResponseCode = null;

            public bool Responsed
            {
                get { return ResponseCode != null; }
            }
        }

        private static int nUniqID = 0;

        private Dictionary<int, ControlSession> dicConversations = new Dictionary<int, ControlSession>();


        private static Logger logger = LogManager.GetCurrentClassLogger();

        #region Single Tone 객체 관련
        private static ControlService instance = null;
        private static readonly object objLock = new object();

        /// <summary>
        /// 싱글톤 객체 인스턴스
        /// </summary>
        public static ControlService GetInstance
        {
            get
            {
                lock (objLock)
                {
                    if (instance == null)
                    {
                        instance = new ControlService();
                    }
                    return instance;
                }
            }
        }
        #endregion

        int nModuleID = -1;

        BackgroundWorker _thConversationKiller = null;

        /// <summary>
        /// 초기화 함수
        /// </summary>
        public void Initialize()
        {
            if (_thConversationKiller == null)
            {
                _thConversationKiller = new BackgroundWorker();
                _thConversationKiller.WorkerSupportsCancellation = true;
                _thConversationKiller.DoWork += new DoWorkEventHandler(_thConversationKiller_DoWork);

            }

            if (_thConversationKiller != null && !_thConversationKiller.IsBusy)
            {
                _thConversationKiller.RunWorkerAsync();

            }

            logger.Info("ControlService is started.");
        }

        void _thConversationKiller_DoWork(object sender, DoWorkEventArgs e)
        {
            logger.Info("ControlService Background Worker is started.");

            int nCount = 0;
            TimeSpan tsThreeMin = TimeSpan.FromMinutes(1);
            while (!e.Cancel)
            {
                if (nCount++ > 60)
                {
                    DateTime dtNow = DateTime.Now;

                    List<int> arrRemoveIds = new List<int>();

                    lock (lockObject)
                    {
                        foreach (int id in dicConversations.Keys)
                        {
                            if ((dtNow - dicConversations[id].RequestDT) > tsThreeMin)
                            {
                                arrRemoveIds.Add(id);
                                logger.Debug("Session will be terminated: {0}", id);
                            }
                        }
                    }

                    lock (lockObject)
                    {
                        foreach (int remove in arrRemoveIds)
                        {
                            dicConversations.Remove(remove);
                        }
                    }

                    nCount = 0;
                }
                System.Threading.Thread.Sleep(1000);
            }
        }

        /// <summary>
        /// 제어 요청
        /// </summary>
        /// <param name="sPlayerID"></param>
        /// <param name="RequestCode"></param>
        /// <param name="RequestValue"></param>
        /// <returns></returns>
        public int SendControlRequest(String sPlayerID, ControlRequest RequestCode, int RequestValue)
        {
            int nID = 0;

            lock (lockObject)
            {
                nID = nUniqID++;
                ControlSession session = new ControlSession(nID, sPlayerID, RequestCode, RequestValue);
                dicConversations.Add(nID, session);

                try
                {
                    /// 제어 스레드 생성
                    Thread th = new Thread(new ParameterizedThreadStart(thControlProcess));
                    th.Start(session);
                }
                catch
                {
                    /// 서버 접속 오류 
                    dicConversations[nID].ResponseCode = ControlResponse.NotConnected;
                }
            }
            return nID;

        }

        /// <summary>
        /// 제어 명령 처리 스레드
        /// </summary>
        /// <param name="ob"></param>
        void thControlProcess(object ob)
        {
            ControlSession c = ob as ControlSession;

            if(ob != null)
            {
                ControlResponse resp_code = ControlResponse.NotConnected;
                try
                {

                    switch ((ControlRequest)Convert.ToInt32(c.RequestCode))
                    {
                        case ControlRequest.PowerOn:
                            {
                                logger.Info(String.Format("Control Type: PowerOn, Player ID: {0}", c.PlayerID));

                                System.Xml.XmlNode player = CacheObject.GetInstance.FindPlayerNode(c.PlayerID); 

                                try
                                {
                                    int nMntType = Convert.ToInt32(player.Attributes["_mnttype"].Value.ToString());

                                    if (nMntType != 0)
                                    {
                                        string sIPv4 = player.Attributes["_ipv4addr"].Value.ToString();
                                        string sMAC = player.Attributes["_macaddr"].Value.ToString();

                                        if (String.IsNullOrEmpty(sIPv4))
                                        {
                                            resp_code = ControlResponse.NotConnected;
                                            break;
                                        }

                                        if ((nMntType & (int)PowerMntType.AMT) == (int)PowerMntType.AMT)
                                        {

                                            string sAuthId = player.Attributes["_auth_id"].Value.ToString();
                                            string sAuthPass = player.Attributes["_auth_pass"].Value.ToString();

                                            logger.Info(String.Format("AMT Call: IP({0}), AuthID({1}), AuthPass({2})", sIPv4, sAuthId, sAuthPass));

                                            try
                                            {


                                                Intel.vPro.AMT.CVProCtrl vProControl = new Intel.vPro.AMT.CVProCtrl();
                                                if (vProControl.SetPowerOn(sIPv4, sAuthId, sAuthPass))
                                                {
                                                    resp_code = ControlResponse.Success;
                                                }
                                                logger.Info("AMT Call Complete!");
                                            }
                                            catch (Exception exr)
                                            {
                                                logger.Error(String.Format("AMT Call Exception: ", exr.ToString()));
                                                resp_code = ControlResponse.Internal_Error;
                                            }
                                        }

                                        if ((nMntType & (int)PowerMntType.WoL) == (int)PowerMntType.WoL)
                                        {
                                            try
                                            {
                                                logger.Info(String.Format("WoL Call: IP({0}) MAC({1})", sIPv4, sMAC));

                                                Intel.vPro.AMT.CVProCtrl vProControl = new Intel.vPro.AMT.CVProCtrl();
                                                for (int i = 0; i < 2; ++i)
                                                {
                                                    vProControl.CallWOL(sMAC.Replace(":", ""), sIPv4, 0x2fff);
                                                    Thread.Sleep(50);
                                                    vProControl.CallWOLByUnicast(sMAC.Replace(":", ""), sIPv4, 0x2fff);
                                                    Thread.Sleep(50);
                                                }
                                                logger.Info("WoL Call Complete!");
                                                resp_code = ControlResponse.Success;
                                            }
                                            catch (Exception exr)
                                            {
                                                logger.Error(String.Format("WoL Call Exception: ", exr.ToString()));
                                                resp_code = ControlResponse.Internal_Error;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        resp_code = ControlResponse.Failed;
                                    }
                                }
                                catch { }

                                c.ResponseCode = resp_code;
                            }
                            break;
                        case ControlRequest.PowerOff:
                        case ControlRequest.Restart:
                        case ControlRequest.PCVol:
                        case ControlRequest.MonPowerOn:
                        case ControlRequest.MonPowerOff:
                        case ControlRequest.MonVol:
                            Config.GetConfig.SendControlEventMethod(c.ID, c.RequestCode, c.RequestValue, c.PlayerID);
                            break;
                        default:
                            c.ResponseCode = resp_code;
                            break;
                    }
                }
                catch { }
            }
        }

        /// <summary>
        /// 제어 반환
        /// </summary>
        /// <param name="nID"></param>
        /// <returns></returns>
        public ControlResponse GetControlResponse(int nID)
        {
            try
            {
                lock (lockObject)
                {
                    ControlSession session = dicConversations[nID];
                    if (session.Responsed)
                        return session.ResponseCode != null ? session.ResponseCode.Value : ControlResponse.Timeouted;
                    else return ControlResponse.Failed;
                }
            }
            catch { return ControlResponse.Timeouted; /*세션 타임 아웃*/}
        }

        /// <summary>
        /// 제어 결과 반환
        /// </summary>
        /// <param name="nID"></param>
        /// <param name="resp"></param>
        public void SetControlResponse(int nID, ControlResponse resp)
        {
            try
            {
                lock (lockObject)
                {
                    ControlSession session = dicConversations[nID];
                    session.ResponseCode = resp;
                }
            }
            catch { }
        }

        /// <summary>
        /// 소멸자 함수
        /// </summary>
        public void Uninitialize()
        {
            logger.Info("ControlService is ended.");

        }

    }
}
