﻿using System;
using System.Data.Common;

using NLog;

using DigitalSignage.DataBase;
using DigitalSignage.Common;

namespace DigitalSignage.SchedulerService
{
    public class PlayersList : MarshalByRefObject, IPlayersList
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public DataSet.playersDataTable GetPlayersByGID(string gid)
        {
            Config cfg = Config.GetConfig;
            try
            {
				using (DataBase.DataSetTableAdapters.playersTableAdapter da = cfg.dbLayer.CreatePlayersDA())
				{
					using (DataSet.playersDataTable dt = da.GetPlayersByGID(gid))
					{
						da.Dispose();
						return dt;
					}
				}
            }
            catch (Exception e)
            {
                logger.Log(LogLevel.Error, e + "");
            }
            return null;
        }

		public int Add(string gid, DataSet.playersDataTable newData)
        {
            Config cfg = Config.GetConfig;
            try
            {
				using (DataBase.DataSetTableAdapters.playersTableAdapter da = cfg.dbLayer.CreatePlayersDA())
				{
					for (int i = 0; i < newData.Rows.Count; i++)
					{
						DataSet.playersRow row = (DataSet.playersRow)newData.Rows[i];
						da.AddPlayer(row.pid, row.gid, row.name, row.host, row.port, row.ComSpeed, row.ComBits);
					}
					da.Dispose();
				}
				return 0;
            }
            catch (Exception e)
            {
                logger.Log(LogLevel.Error, e + "");
            }
            return -1;
        }

		public int Update(string pid, String name, string gid, String hostname, String defProject, String ComPort, int ComBitrate, int ComBits, int rotate, int memoryusage, int cpurate, string all_d_progress, string piece_d_progress, string curr_screen, string curr_subtitle)
        {
            Config cfg = Config.GetConfig;
            try
            {
                using (DataBase.DataSetTableAdapters.playersTableAdapter da = cfg.dbLayer.CreatePlayersDA())
				{
					using (DataSet.playersDataTable ta = da.GetPlayerByPID(pid))
					{
						try
						{
							string old_gid = ((DataSet.playersRow)ta.Rows[0]).gid;

							if (!old_gid.Equals(gid))
							{
								cfg.UpdateGID(old_gid, pid, gid);
							}
						}
						catch (Exception exx)
						{
							logger.Error(exx.ToString());
						}
						da.Update1(gid, name, hostname, 0, ComBitrate, ComBits, 0, 0, ComPort, curr_subtitle, curr_screen, piece_d_progress, all_d_progress, 0, 0, 0, 0, 0, pid);
						// 				da.Update1(gid, name, hostname, 0, 0, 0, 0, 0, pid, ComPort, /*ComBitrate, ComBits, memoryusage, cpurate,*/ all_d_progress, piece_d_progress, curr_screen, curr_subtitle, 0, 0, 0, 0, 0);
						ta.Dispose();
					}
					da.Dispose();
				}

				//	살아있는 연결에 다운로드 시간대를 갱신한다.
				cfg.UpdateTimeScope(gid, pid);
 
				return 0;
            }
            catch (Exception e)
            {
                logger.Log(LogLevel.Error, e + "");
            }
            return -1;
        }

        public int Delete(DataSet.playersDataTable data)
        {
            Config cfg = Config.GetConfig;
            try
            {
				using (DataBase.DataSetTableAdapters.playersTableAdapter da = cfg.dbLayer.CreatePlayersDA())
				{
					for (int i = 0; i < data.Rows.Count; i++)
					{
						DataSet.playersRow row = (DataSet.playersRow)data.Rows[i];
						logger.Info("Deleting player with PID=" + row.pid);
						da.DeletePlayer(row.pid);
					}
					da.Dispose();
				}
                return 0;
            }
            catch (Exception e)
            {
                logger.Log(LogLevel.Error, e + "");
            }
            return -1;
        }

        public DataSet.playersDataTable FindPlayerByIPAddress( string ipaddress )
        {
            Config cfg = Config.GetConfig;
            try
            {
				using (DataBase.DataSetTableAdapters.playersTableAdapter da = cfg.dbLayer.CreatePlayersDA())
				{
					using (DataSet.playersDataTable dt = da.FindPlayerByHost(ipaddress))
					{
						da.Dispose();
						return dt;
					}
				}
            }
            catch (Exception e)
            {
                logger.Log(LogLevel.Error, e + "");
            }
            return null;
        }

        public DataSet.playersDataTable GetPlayersList()
        {
			Config cfg = Config.GetConfig;
			try
			{
				using (DataBase.DataSetTableAdapters.playersTableAdapter da = cfg.dbLayer.CreatePlayersDA())
				{
					using (DataSet.playersDataTable dt = da.GetData())
					{
						da.Dispose();
						return dt;
					}
				}
			}
			catch (Exception e)
			{
				logger.Log(LogLevel.Error, e + "");
			}
			return null;
        }

        public DataSet.playersDataTable FindPlayerByHost(String host)
        {
            Config cfg = Config.GetConfig;
            try
            {
				using (DataBase.DataSetTableAdapters.playersTableAdapter da = cfg.dbLayer.CreatePlayersDA())
				{
					using (DataSet.playersDataTable dt = da.FindPlayerByHost(host))
					{
						da.Dispose();
						return dt;
					}
				}
            }
            catch (Exception e)
            {
                logger.Log(LogLevel.Error, e + "");
            }
            return null;
        }

		public int ChangeGroup(string oldGid, string newGid)
        {
            Config cfg = Config.GetConfig;
            try
            {
				using (DataBase.DataSetTableAdapters.playersTableAdapter da = cfg.dbLayer.CreatePlayersDA())
				{
					using (DataSet.playersDataTable table = da.GetPlayersByGID(oldGid))
					{
						da.ChangeGroup(newGid, oldGid);

						foreach (DataSet.playersRow row in table.Rows)
						{
							cfg.UpdateGID(oldGid, row.pid, newGid);
						}
						table.Dispose();
					}

					da.Dispose();
				}
				return 0;
            }
            catch (Exception e)
            {
                logger.Log(LogLevel.Error, e + "");
            }
            return -1;
        }

		public DataSet.playersDataTable GetPlayerByPID(string pid)
        {
            Config cfg = Config.GetConfig;
            try
            {
				using (DataBase.DataSetTableAdapters.playersTableAdapter da = cfg.dbLayer.CreatePlayersDA())
				{
					using (DataSet.playersDataTable dt = da.GetPlayerByPID(pid))
					{
						da.Dispose();
						return dt;
					}
				}
            }
            catch (Exception e)
            {
                logger.Log(LogLevel.Error, e + "");
            }
            return null;
        }
		public int DeletePlayer(string pid)
		{
			Config cfg = Config.GetConfig;
			try
			{
				int n = 0;

				using (DataBase.DataSetTableAdapters.playersTableAdapter da = cfg.dbLayer.CreatePlayersDA())
				{
					n = da.DeletePlayer(pid);
					da.Dispose();
				}
				return n;
			}
			catch (Exception e)
			{
				logger.Log(LogLevel.Error, e + "");
			}
			return -1;
		}
		public int ResetPlayerState()
		{
			Config cfg = Config.GetConfig;
			try
			{
				int n = 0;
				using (DataBase.DataSetTableAdapters.playersTableAdapter da = cfg.dbLayer.CreatePlayersDA())
				{
					n = da.ResetState();
					da.Dispose();
				}
				return n;
			}
			catch (Exception e)
			{
				logger.Log(LogLevel.Error, e + "");
			}
			return -1;

		}

		public bool IsAvailableAddPlayer()
		{
			Config cfg = Config.GetConfig;
			try
			{
				int n = 0;
				using (DataBase.DataSetTableAdapters.playersTableAdapter da = cfg.dbLayer.CreatePlayersDA())
				{
					using (DataBase.DataSet.playersDataTable dt = da.GetData())
					{
						n = dt.Rows.Count;
						dt.Dispose();
					}
					da.Dispose();
				}
				return n < cfg.MaxCountOfPlayers;
			}
			catch (Exception e)
			{
				logger.Log(LogLevel.Error, e + "");
			}
			return false;


		}

		public void UpdatePlayerID(string gid, string pid, string new_pid)
		{
			Config cfg = Config.GetConfig;
			try
			{
				cfg.UpdatePID(gid, pid, new_pid);
			}
			catch (Exception e)
			{
				logger.Log(LogLevel.Error, e + "");
			}
		}

		public void UpdateServiceIP(string gid, string pid, string new_service_ip)
		{
			Config cfg = Config.GetConfig;
			try
			{
				cfg.UpdateSIP(gid, pid, new_service_ip);
			}
			catch (Exception e)
			{
				logger.Log(LogLevel.Error, e + "");
			}
		}
    }
}
