﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using NLog;

namespace DigitalSignage.SchedulerService
{
	public class CacheObject
	{
        public static string ROOT_GROUP_ID = "0000000000000";

		private static Logger logger = LogManager.GetCurrentClassLogger();

		private static CacheObject instance = null;
		private static readonly object objLock = new object();

		private XmlDocument xmlNetworkTree = null;

		public static CacheObject GetInstance
		{
			get	{
				lock(objLock)
				{
					if (instance == null)
					{
						instance = new CacheObject();
// 						instance.Initialize();
					}
					return instance;
				}
			}
		}

		/// <summary>
		/// Cache Object를 생성한다.
		/// </summary>
		public void Initialize()
		{
			logger.Info("Cache Object will be starting..");
			lock(objLock)
			{
				GenNetworkTree.ServerDBLayer = Config.GetConfig.serverDBLayer;
                xmlNetworkTree = GenNetworkTree.GenerateNetworkTree(new XmlDocument(), ROOT_GROUP_ID);

				logger.Info(xmlNetworkTree.OuterXml);
			}
		}

		/// <summary>
		/// Cache Object를 생성한다. (debug 용)
		/// </summary>
		public void Initialize(string dsn)
		{
			logger.Info("Cache Object will be starting..");
			lock (objLock)
			{
				GenNetworkTree.ServerDBLayer = new DigitalSignage.ServerDatabase.DBLayer();
				GenNetworkTree.ServerDBLayer.openDB(dsn);
				GenNetworkTree.ServerDBLayer.InitializeConnectionPool(20);

                xmlNetworkTree = GenNetworkTree.GenerateNetworkTree(new XmlDocument(), ROOT_GROUP_ID);
			}
		}
		/// <summary>
		/// Cache 된 데이터를 모두 DB와 동기화하고 해제한다. 
		/// </summary>
		public void Uninitialize()
		{
			try
			{
				//	Player, Group과 DB와의 동기화
			}
			catch(Exception ex)
			{

			}
			instance = null;
			logger.Info("Cache Object ended..");
		}

		/// <summary>
		/// Id와 Password에 맞는 사용자의 XMLNODE를 반환한다.
		/// </summary>
		/// <param name="userID">사용자 ID</param>
		/// <param name="password">사용자 Password</param>
		/// <returns></returns>
		public XmlNode FindUserNode(string userID, string password)
		{
			if (String.IsNullOrEmpty(userID) || String.IsNullOrEmpty(password))
				throw new ArgumentException("ID or Password is wrong.");

			XmlNodeList nodelist = xmlNetworkTree.SelectNodes("//child::user[attribute::name=\"" + userID + "\" and attribute::hash=\"" + password + "\" and attribute::is_enabled=\"Y\"]");

			if (nodelist != null)
			{
				try
				{
					return nodelist.Item(0);
				}
				catch
				{ }
			}

			return null;
		}

		/// <summary>
		/// User ID로 User를 찾는다.
		/// </summary>
		/// <param name="user_id"></param>
		/// <returns></returns>
		public XmlNode FindUserNode(long user_id)
		{
			if (user_id < 0)
				throw new ArgumentException("USER ID is wrong.");

			return xmlNetworkTree.SelectSingleNode("//child::user[attribute::id=\"" + user_id.ToString() + "\"]");
		}

		/// <summary>
		/// 해당 그룹의 XML NODE를 반환한다.
		/// </summary>
		/// <param name="groupID">선택된 그룹 아이디</param>
		/// <returns></returns>
		public XmlNode FindGroupNode(string groupID)
		{
			if (String.IsNullOrEmpty(groupID))
				throw new ArgumentException("group ID is wrong.");

			return xmlNetworkTree.SelectSingleNode("//child::group[attribute::gid=\"" + groupID + "\"]");
		}

		/// <summary>
		/// 해당 플레이어의 XML NODE를 반환한다.
		/// </summary>
		/// <param name="groupID">선택된 플레이어 아이디</param>
		/// <returns></returns>
		public XmlNode FindPlayerNode(string playerID)
		{
			if (String.IsNullOrEmpty(playerID))
				throw new ArgumentException("player ID is wrong.");

			return xmlNetworkTree.SelectSingleNode("//child::player[attribute::pid=\"" + playerID + "\"]");
		}
		
		/// <summary>
		/// timestamp 이후에 수정된 모든 group 요소를 가져온다.
		/// </summary>
		/// <param name="timestamp"></param>
		/// <returns></returns>
		public XmlNodeList FindModifiedGroups(long timestamp)
		{
			if(xmlNetworkTree == null) 
				throw new Exception("Network Tree is null.");

			return xmlNetworkTree.SelectNodes("//child::group[attribute::_timestamp > " + timestamp + "]");
		}

		/// <summary>
		/// timestamp 이후에 수정된 모든 player 요소를 가져온다.
		/// </summary>
		/// <param name="timestamp"></param>
		/// <returns></returns>
		public XmlNodeList FindModifiedPlayers(long timestamp)
		{
			if (xmlNetworkTree == null)
				throw new Exception("Network Tree is null.");

			return xmlNetworkTree.SelectNodes("//child::player[attribute::_timestamp > " + timestamp + "]");
		}

		/// <summary>
		/// 플레이어의 개수를 반환한다.
		/// </summary>
		/// <returns></returns>
		public int GetPlayersCount()
		{
			if (xmlNetworkTree == null)
				throw new Exception("Network Tree is null.");

			return xmlNetworkTree.SelectNodes("//child::player[attribute::del_yn=\"N\"]").Count;
		}

		public int GetPlayersCount(string gid)
		{
			if (xmlNetworkTree == null)
				throw new Exception("Network Tree is null.");

			try
			{
				XmlNode node = this.FindGroupNode(gid);

				if (null != node)
				{
					return node.SelectNodes("descendant::player[attribute::del_yn=\"N\"]").Count;
				}
			}
			catch
			{	
			}
			return 0;
		}
		
		/// <summary>
		/// 플레이어 노드를 생성한다.
		/// </summary>
		/// <param name="pid"></param>
		/// <param name="gid"></param>
		/// <param name="name"></param>
		/// <param name="host"></param>
		/// <returns></returns>
		public bool AddPlayerNode(string pid, string gid, string name, string host)
		{
			if (xmlNetworkTree == null)
				throw new Exception("Network Tree is null.");

			try
			{
				XmlNode gNode = FindGroupNode(gid);
				XmlNode pNode = GenNetworkTree.ConvertPlayerData2Xml(xmlNetworkTree, pid, gid, name, host, 0, 0, "", 0, 0, 0, 0, 0, 0, 0, 0, 0, "", "", "", "","","","N",0,"","", DateTime.UtcNow.Ticks, "N", "");

				gNode.AppendChild(pNode);

				return true;
			}
			catch (Exception ex)
			{
				logger.Error(ex.ToString());
			}

			return false;
		}
/*
		/// <summary>
		/// 플레이어 노드를 생성한다.
		/// </summary>
		/// <param name="pid"></param>
		/// <param name="gid"></param>
		/// <param name="name"></param>
		/// <param name="host"></param>
		/// <param name="mntType"></param>
		/// <param name="authID"></param>
		/// <param name="authPass"></param>
		/// <returns></returns>
		public bool AddPlayerNode(string pid, string gid, string name, string host, int mntType, string authID, string authPass)
		{
			if (xmlNetworkTree == null)
				throw new Exception("Network Tree is null.");

			try
			{
				XmlNode gNode = FindGroupNode(gid);
				XmlNode pNode = GenNetworkTree.ConvertPlayerData2Xml(xmlNetworkTree, pid, gid, name, host, 0, 0, "", 0, 0, 0, 0, 0, 0, 0, 0, 0, "", "", "", "", "", "", "N", mntType, authID, authPass);

				gNode.AppendChild(pNode);

				return true;
			}
			catch (Exception ex)
			{
				logger.Error(ex.ToString());
			}

			return false;
		}

		/// <summary>
		/// 플레이어 노드를 수정한다.
		/// </summary>
		/// <param name="pid"></param>
		/// <param name="gid"></param>
		/// <param name="name"></param>
		/// <param name="mntType"></param>
		/// <param name="authID"></param>
		/// <param name="authPass"></param>
		/// <returns></returns>
		public bool UpdatePlayerNode(string pid, string gid, string name, int mntType, string authID, string authPass)
		{
			if (xmlNetworkTree == null)
				throw new Exception("Network Tree is null.");

			try
			{
				XmlNode pNode = FindPlayerNode(pid);

				if (pNode.Attributes["gid"].Value != gid)
				{
					XmlNode gNewNode = FindGroupNode(gid);

					if (gNewNode != null)
					{
						pNode.ParentNode.RemoveChild(pNode);
						gNewNode.AppendChild(pNode);
						pNode.Attributes["gid"].Value = gid;
					}
					else return false;
				}

				pNode.Attributes["name"].Value = name;
				pNode.Attributes["_mnttype"].Value = mntType.ToString();
				pNode.Attributes["_auth_id"].Value = authID;
				pNode.Attributes["_auth_pass"].Value = authPass;
				pNode.Attributes["_timestamp"].Value = DateTime.UtcNow.Ticks.ToString();

				return true;
			}
			catch (Exception ex)
			{
				logger.Error(ex.ToString());
			}

			return false;
		}
		*/
		
		/// <summary>
		/// 플레이어 노드를 수정한다.
		/// </summary>
		/// <param name="pid"></param>
		/// <param name="gid"></param>
		/// <param name="name"></param>
		/// <returns></returns>
		public bool UpdatePlayerNode(string pid, string gid, string name)
		{
			if (xmlNetworkTree == null)
				throw new Exception("Network Tree is null.");

			try
			{
				XmlNode pNode = FindPlayerNode(pid);

				if(pNode.Attributes["gid"].Value != gid)
				{
					XmlNode gNewNode = FindGroupNode(gid);

					if (gNewNode != null)
					{
						pNode.ParentNode.RemoveChild(pNode);
						gNewNode.AppendChild(pNode);
						pNode.Attributes["gid"].Value = gid;
					}
					else return false;
				}

				pNode.Attributes["name"].Value = name;
				pNode.Attributes["_modified"].Value = pNode.Attributes["_timestamp"].Value = DateTime.UtcNow.Ticks.ToString();
				
				
				return true;
			}
			catch(Exception ex)
			{
				logger.Error(ex.ToString());
			}

			return false;
		}

		/// <summary>
		/// 플레이어의 GEO Code를 갱신한다.
		/// </summary>
		/// <param name="pid"></param>
		/// <param name="geocode"></param>
		/// <returns></returns>
		public bool UpdatePlayerGeoCode(string pid, int geocode)
		{
			if (xmlNetworkTree == null)
				throw new Exception("Network Tree is null.");

			try
			{
				XmlNode pNode = FindPlayerNode(pid);

				pNode.Attributes["geocode"].Value = geocode.ToString();
				pNode.Attributes["_modified"].Value = pNode.Attributes["_timestamp"].Value = DateTime.UtcNow.Ticks.ToString();

				return true;

			}
			catch (Exception ex)
			{
				logger.Error(ex.ToString());
			}

			return false;
		}

		/// <summary>
		/// 플레이어 노드를 삭제한다.
		/// </summary>
		/// <param name="pid"></param>
		/// <returns></returns>
		public bool DeletePlayerNode(string pid)
		{
			if (xmlNetworkTree == null)
				throw new Exception("Network Tree is null.");

			try
			{
				XmlNode pNode = FindPlayerNode(pid);
				if (pNode.HasChildNodes)
					return false;

				pNode.Attributes["del_yn"].Value = "Y";
				pNode.Attributes["_modified"].Value = pNode.Attributes["_timestamp"].Value = DateTime.UtcNow.Ticks.ToString();

				//pNode.ParentNode.RemoveChild(pNode);
				//pNode = null;

				return true;
			}
			catch (Exception ex)
			{
				logger.Error(ex.ToString());
			}

			return false;
		}
		
		/// <summary>
		/// 그룹 노드를 생성한다.
		/// </summary>
		/// <param name="gid"></param>
		/// <param name="name"></param>
		/// <param name="source"></param>
		/// <param name="username"></param>
		/// <param name="password"></param>
		/// <param name="parent_gid"></param>
		/// <param name="is_inherited"></param>
		/// <param name="depth"></param>
		/// <param name="path"></param>
		/// <returns></returns>
		public bool AddGroupNode(string gid, string name, string source, string username, string password, string parent_gid, string is_inherited, int depth, string path)
		{
			if (xmlNetworkTree == null)
				throw new Exception("Network Tree is null.");

			try
			{
				XmlNode gNode = FindGroupNode(parent_gid);
				XmlNode pNode = GenNetworkTree.ConvertGroupData2Xml(xmlNetworkTree, gid, name, source, username, password, parent_gid, is_inherited, depth, path, -1, "N");

				gNode.AppendChild(pNode);

				return true;
			}
			catch (Exception ex)
			{
				logger.Error(ex.ToString());
			}

			return false;
		}
		
		/// <summary>
		/// 그룹노드를 수정한다.
		/// </summary>
		/// <param name="gid"></param>
		/// <param name="name"></param>
		/// <param name="source"></param>
		/// <param name="username"></param>
		/// <param name="password"></param>
		/// <param name="parent_gid"></param>
		/// <param name="is_inherited"></param>
		/// <param name="depth"></param>
		/// <param name="path"></param>
		/// <returns></returns>
		public bool UpdateGroupNode(string gid, string name, string source, string username, string password, string parent_gid, string is_inherited, int depth, string path)
		{

			if (xmlNetworkTree == null)
				throw new Exception("Network Tree is null.");

			try
			{
				XmlNode gNode = FindGroupNode(gid);

				if (gNode.Attributes["parent_gid"].Value != parent_gid)
				{
					XmlNode gNewNode = FindGroupNode(parent_gid);

					if (gNewNode != null)
					{
						gNode.ParentNode.RemoveChild(gNode);
						gNewNode.AppendChild(gNode);
						gNode.Attributes["parent_gid"].Value = parent_gid;
					}
					else return false;
				}

				gNode.Attributes["name"].Value = name;
				gNode.Attributes["source"].Value = source;
				gNode.Attributes["username"].Value = username;
				gNode.Attributes["password"].Value = password;
				gNode.Attributes["is_inherited"].Value = is_inherited;
				gNode.Attributes["depth"].Value = depth.ToString();
				gNode.Attributes["path"].Value = path;
				gNode.Attributes["_modified"].Value = gNode.Attributes["_timestamp"].Value = DateTime.UtcNow.Ticks.ToString();
				
				return true;

			}
			catch (Exception ex)
			{
				logger.Error(ex.ToString());
			}

			return false;
		}

		/// <summary>
		/// 그룹의 GEO Code를 갱신한다.
		/// </summary>
		/// <param name="gid"></param>
		/// <param name="geocode"></param>
		/// <returns></returns>
		public bool UpdateGroupGeoCode(string gid, int geocode)
		{
			if (xmlNetworkTree == null)
				throw new Exception("Network Tree is null.");

			try
			{
				XmlNode gNode = FindGroupNode(gid);

				gNode.Attributes["geocode"].Value = geocode.ToString();
				gNode.Attributes["_modified"].Value = gNode.Attributes["_timestamp"].Value = DateTime.UtcNow.Ticks.ToString();

				return true;

			}
			catch (Exception ex)
			{
				logger.Error(ex.ToString());
			}

			return false;
		}


		/// <summary>
		/// 그룹 노드를 삭제한다.
		/// </summary>
		/// <param name="gid"></param>
		/// <returns></returns>
		public bool DeleteGroupNode(string gid)
		{
			if (xmlNetworkTree == null)
				throw new Exception("Network Tree is null.");

            if (gid.Equals(ROOT_GROUP_ID))
				return false;

			try
			{
				XmlNode gNode = FindGroupNode(gid);
				
				gNode.Attributes["del_yn"].Value = "Y";
				gNode.Attributes["_modified"].Value = gNode.Attributes["_timestamp"].Value = DateTime.UtcNow.Ticks.ToString();
				//gNode.ParentNode.RemoveChild(gNode);
				//gNode = null;

				return true;
			}
			catch (Exception ex)
			{
				logger.Error(ex.ToString());
			}

			return false;
		}
		
		/// <summary>
		/// User 노드를 생성한다.
		/// </summary>
		/// <param name="id"></param>
		/// <param name="name"></param>
		/// <param name="hash"></param>
		/// <param name="role_ids"></param>
		/// <param name="parent_id"></param>
		/// <param name="is_enabled"></param>
		/// <returns></returns>
		public bool AddUserNode(long id, string name, string hash, string role_ids, long parent_id, string is_enabled)
		{
			if (xmlNetworkTree == null)
				throw new Exception("Network Tree is null.");

			try
			{
				XmlNode uNode = FindUserNode(parent_id);
				XmlNode newNode = GenNetworkTree.ConvertUserData2Xml(xmlNetworkTree, id, name, hash, parent_id, role_ids, is_enabled);

				uNode.AppendChild(newNode);
				GenNetworkTree.GenerateRoleTree(xmlNetworkTree, newNode, role_ids);

				return true;
			}
			catch (Exception ex)
			{
				logger.Error(ex.ToString());
			}

			return false;
		}

		/// <summary>
		/// 사용자의 상태를 변경한다.
		/// </summary>
		/// <param name="id">사용자 id</param>
		/// <param name="is_enabled">활성 여부</param>
		/// <returns>성공 여부</returns>
		public bool UpdateUserStatus(long id, string is_enabled)
		{
			if (xmlNetworkTree == null)
				throw new Exception("Network Tree is null.");

			try
			{
				XmlNode uNode = FindUserNode(id);
				uNode.Attributes["is_enabled"].Value = is_enabled;

				return true;
			}
			catch (Exception ex)
			{
				logger.Error(ex.ToString());
			}

			return false;
		}

		/// <summary>
		/// User 노드를 수정한다.
		/// </summary>
		/// <param name="id"></param>
		/// <param name="hash"></param>
		/// <param name="role_ids"></param>
		/// <param name="parent_id"></param>
		/// <returns></returns>
		public bool UpdateUserNode(long id, string hash, string role_ids, long parent_id)
		{
			if (xmlNetworkTree == null)
				throw new Exception("Network Tree is null.");

			try
			{
				XmlNode uNode = FindUserNode(id);

				if (!uNode.Attributes["parent_id"].Value.Equals(parent_id.ToString()))
				{
					XmlNode uNewNode = FindUserNode(parent_id);

					if (uNewNode != null)
					{
						uNode.ParentNode.RemoveChild(uNode);
						uNewNode.AppendChild(uNode);
						uNode.Attributes["parent_id"].Value = parent_id.ToString();
					}
					else return false;
				}

				if(uNode.Attributes["role_ids"].Value != role_ids)
				{
					for(int i = 0 ; i < uNode.ChildNodes.Count ; ++i)
					{
						if(uNode.ChildNodes[i].LocalName.Equals("role"))
						{
							uNode.RemoveChild(uNode.ChildNodes[i]);
							i--;
						}
					}

					uNode.Attributes["role_ids"].Value = role_ids;
					GenNetworkTree.GenerateRoleTree(xmlNetworkTree, uNode, role_ids);
				}

				uNode.Attributes["hash"].Value = hash;
				
				uNode.Attributes["is_enabled"].Value = "Y";
				uNode.Attributes["_timestamp"].Value = DateTime.UtcNow.Ticks.ToString();

				return true;
			}
			catch (Exception ex)
			{
				logger.Error(ex.ToString());
			}

			return false;
		}
		public bool DeleteUserNode(long id)
		{
			if (xmlNetworkTree == null)
				throw new Exception("Network Tree is null.");

			try
			{
				XmlNode uNode = FindUserNode(id);
				uNode.Attributes["is_enabled"].Value = "N";
				uNode.Attributes["_timestamp"].Value = DateTime.UtcNow.Ticks.ToString();
				
				return true;
			}
			catch (Exception ex)
			{
				logger.Error(ex.ToString());
			}

			return false;
		}

		public XmlNode FindNoInheritedGroup(string gid)
		{
			try
			{
				XmlNode gNode = FindGroupNode(gid);

				do
				{
					if (gNode.Attributes["is_inherited"].Value.Equals("N"))
					{
						return gNode;
					}
				} while (null != (gNode = gNode.ParentNode));
			}
			catch(Exception e)
			{
				logger.Error(e.ToString());
			}

			return null;
		}

		/// <summary>
		/// 타임 스탬프 갱신
		/// </summary>
		/// <param name="pids"></param>
		/// <returns></returns>
		public bool UpdateTimestamp(string[] pids)
		{
			if (pids == null) return true;

			try
			{
				string sTicks = DateTime.UtcNow.Ticks.ToString();

				foreach (string pid in pids)
				{
					try
					{
						XmlNode pNode = FindPlayerNode(pid);
						pNode.Attributes["_modified"].Value = pNode.Attributes["_timestamp"].Value = sTicks;
					}
					catch { }
				}

				return true;
			}
			catch { }

			return false;
			
		}

	}
}
