﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Configuration;
using System.Xml;
using System.IO;
using DigitalSignage.Common;
using DigitalSignage.ServerDatabase;

using NLog;

namespace DigitalSignage.SchedulerService
{
	class RefDataService
	{
		private static bool _process = false;
		private static Logger logger = LogManager.GetCurrentClassLogger();

		private String sRefDataPath;

		const string id = "MS00001";
		const string pwd = "sksmsqhddldi.";

		Config cfg = Config.GetConfig;

		public int Start()
        {
			CleanRefDirectory();

			if (_process == false)
            {
                _process = true;

                //starting control thread
                Thread control = new Thread(this.ControlThread);
                control.IsBackground = true;
                control.Start();
            }
            return 0;
        }

		public int Stop()
        {
            _process = false;
            logger.Info("service going down");
            return 0;
        }

		private int update()
		{
			String refDataArray = ConfigurationManager.AppSettings["webservice_refdata_list"];

			if(!String.IsNullOrEmpty(refDataArray))
			{
				cfg.HasRefData = true;
				String[] array = refDataArray.Split('|');

				foreach(string ref_id in array)
				{
					WSGetRefData(ref_id);
				}

				UploadToFTP();
			}
			else
			{
				cfg.HasRefData = false;
			}


			return 0;
		}

		private bool CleanRefDirectory()
		{
			try
			{
				sRefDataPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + "\\RefData";

				if (Directory.Exists(sRefDataPath))
				{
					Directory.Delete(sRefDataPath, true);
				}
				Directory.CreateDirectory(sRefDataPath);

				return true;
			}
			catch (Exception e)
			{
				logger.Error(e.Message);
			}
			return false;
		}

		private bool WSGetRefData(string refID)
		{
			try
			{
				string output;
				string message;

				intfGetIFrameRefDataservice service = new intfGetIFrameRefDataservice();
				string sReturn = service.GetIFrameRefDataOp(id, pwd, refID, out message, out output);

				if (sReturn.Contains("0000"))
				{
					try
					{
						//	save XML
						XmlDocument doc = new XmlDocument();
						doc.LoadXml(output);

						if (doc.HasChildNodes)
						{
							doc.Save(sRefDataPath + "\\" + refID + ".xml");

							logger.Info("Get WS data To local : Reference data - " + refID);
						}
						else
						{
							logger.Info("Skip WS data to local : " + refID + " has empty data.");
						}
						return true;
					}
					catch
					{
						logger.Error("Error WS data to local : " + refID + " was not converted to xml.");
					}
				}
				else
					logger.Error(message + " (" + sReturn + ")");
			}
			catch(Exception e)
			{
				logger.Error(e.Message + " (Ref ID : " + refID +")");
			}

			return false;
		}

		private String AnalyzeSource(String source)
		{
			if (source.StartsWith("$"))
			{
				int nIndex = source.IndexOf(":");

				if (source.ToLower().Contains("$local"))
				{
					source = "127.0.0.1" +
						(nIndex != -1 ? source.Substring(nIndex) : "");
				}
				else if (source.ToLower().Contains("$server"))
				{
					source = "127.0.0.1" +
						(nIndex != -1 ? source.Substring(nIndex) : "");
				}
			}

			return source;
		}
		private void UploadToFTP()
		{
			try
			{
				ServerDatabase.ServerDataSetTableAdapters.groupsTableAdapter da = cfg.serverDBLayer.CreateGroup_DA();
				ServerDataSet.groupsDataTable dt = da.GetAllFTPData();
				da.Dispose();
				da = null;

				foreach(ServerDataSet.groupsRow row in dt.Rows)
				{
					try
					{
						logger.Info("FTP source : " + row.source + " Path : " + row.path);
						FTPFunctions ftp = new FTPFunctions(AnalyzeSource(row.source), row.path, row.username, row.password, 10000);
						if(ftp.CheckState())
						{
							ftp.FTPRecursiveCopy(sRefDataPath, "refdata", "", true);
							logger.Info("Put to FTP : Reference data");
						}

					}
					catch(Exception e)
					{
						logger.Error(e.Message);
					}
				}
				dt.Dispose();
				dt = null;
			}
			catch (InvalidOperationException ioe)
			{
				logger.Error(ioe + "");
				cfg.ReInitializeConnectionPool();
			}
			catch(Exception ee)
			{
				logger.Error(ee.Message);
			}

		}

		private void ControlThread()
		{
			try
			{
				cfg.RefRefreshTime = Convert.ToInt32(ConfigurationManager.AppSettings["webservice_refreshtime"]);
				while (_process)
				{
					try
					{
						update();
					}
					catch (Exception e)
					{
						logger.Error(e + "");
					}

					Thread.Sleep(cfg.RefRefreshTime * 1000);
				}
			}
			catch (Exception ex)
			{
				logger.Error("RefDataService, ControlThread: " + ex);
			}
		}

	}
}
