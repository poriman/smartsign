﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace DigitalSignage.SchedulerService
{
	// 참고: 여기서 인터페이스 이름 "IConnectService"를 변경하는 경우 App.config에서 "IConnectService"에 대한 참조도 업데이트해야 합니다.
	[ServiceContract]
	public interface IConnectService
	{
		[OperationContract]
		void DoWork();
	}
}
