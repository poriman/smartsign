﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NLog;
using DigitalSignage.EventReciever;
using DigitalSignage.Common;
using System.ComponentModel;

namespace DigitalSignage.SchedulerService
{
    public class CaptureService
    {
        static readonly object lockObject = new object();
        
        /// <summary>
        /// NPCU 대화 세션
        /// </summary>
        public class CaptureSession
        {
            /// <summary>
            /// 생성자
            /// </summary>
            /// <param name="sessionId"></param>
            /// <param name="pid"></param>
            public CaptureSession(String sessionId, String pid)
            {
                PlayerID = pid;
                SessionID = sessionId;
                RequestDT = DateTime.Now;
            }

            public DateTime RequestDT = DateTime.MinValue;
            public String PlayerID = String.Empty;
            public String SessionID = String.Empty;

            /// <summary>
            /// 유효 시간 갱신
            /// </summary>
            public void Update()
            {
                RequestDT = DateTime.Now;
            }
        }

        private Dictionary<string, CaptureSession> dicConversations = new Dictionary<string, CaptureSession>();

        private String Saved_Path = String.Empty;

        private static Logger logger = LogManager.GetCurrentClassLogger();

        #region Single Tone 객체 관련
        private static CaptureService instance = null;
        private static readonly object objLock = new object();

        /// <summary>
        /// 싱글톤 객체 인스턴스
        /// </summary>
        public static CaptureService GetInstance
        {
            get
            {
                lock (objLock)
                {
                    if (instance == null)
                    {
                        instance = new CaptureService();
                    }
                    return instance;
                }
            }
        }
        #endregion

        /// <summary>
        /// 이벤트 리시버
        /// </summary>
        private EventReciever.EventReciever eventListener = null;

        BackgroundWorker _thConversationKiller = null;

        /// <summary>
        /// 초기화 함수
        /// </summary>
        public void Initialize()
        {
            if (_thConversationKiller == null)
            {
                _thConversationKiller = new BackgroundWorker();
                _thConversationKiller.WorkerSupportsCancellation = true;
                _thConversationKiller.DoWork += new DoWorkEventHandler(_thConversationKiller_DoWork);
 
            }

            if (_thConversationKiller != null && !_thConversationKiller.IsBusy)
            {
                _thConversationKiller.RunWorkerAsync();

            }

            /// 캡쳐 디렉토리 생성
            String sExecutingPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            Saved_Path = sExecutingPath + "\\Captures\\";

            if (!System.IO.Directory.Exists(Saved_Path))
                System.IO.Directory.CreateDirectory(Saved_Path);


            logger.Info("CaptureService is started.");
        }

        /// <summary>
        /// 유효 시간이 지난 캡쳐 세션을 삭제 관리한다.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void _thConversationKiller_DoWork(object sender, DoWorkEventArgs e)
        {
            logger.Info("CaptureService Background Worker is started.");

            int nCount = 0;
            TimeSpan tsThreeMin = TimeSpan.FromMinutes(1);
            while (!e.Cancel)
            {
                if (nCount++ > 10)
                {
                    DateTime dtNow = DateTime.Now;

                    List<string> arrRemoveIds = new List<string>();

                    lock (lockObject)
                    {
                        foreach (string id in dicConversations.Keys)
                        {
                            if ((dtNow - dicConversations[id].RequestDT) > tsThreeMin)
                            {
                                arrRemoveIds.Add(id);
                                logger.Debug("Session will be terminated: {0}", id);
                            }
                        }
                    }

                    foreach (string remove in arrRemoveIds)
                    {
                        lock (lockObject)
                        {
                            /// 캡쳐 종료 전송
                            StopCapture(remove);
                            dicConversations.Remove(remove);
                        }
                    }

                    nCount = 0;
                }
                System.Threading.Thread.Sleep(1000);
            }
        }

        /// <summary>
        /// 세션 시작
        /// </summary>
        /// <param name="sSession"></param>
        /// <param name="pid"></param>
        /// <returns></returns>
        public bool StartCapture(String sSession, String pid)
        {
            lock (lockObject)
            {
                if (dicConversations.ContainsKey(sSession))
                {
                    dicConversations[sSession].PlayerID = pid;
                    dicConversations[sSession].Update();
                }
                else
                {
                    dicConversations.Add(sSession, new CaptureSession(sSession, pid));
                    /// 플레이어로 캡쳐 시작 메소드 전송
                    Config.GetConfig.CaptureProcessEventMethod(true, pid);
                }

                return true;
            }
        }

        /// <summary>
        /// 플레이어 ID가 현재 관리 세션안에 존재하는지 
        /// </summary>
        /// <param name="pid"></param>
        /// <returns></returns>
        public bool HasPlayerIDInSession(String pid)
        {
            try
            {
                lock (lockObject)
                {
                    foreach (CaptureSession c in dicConversations.Values)
                    {
                        if(c.PlayerID.Equals(pid)) return true;
                    }
                }
            }
            catch {}
            return false;
        }

        /// <summary>
        /// 캡쳐 세션 종료
        /// </summary>
        /// <param name="sSession"></param>
        /// <returns></returns>
        public bool StopCapture(String sSession)
        {
            if (dicConversations.ContainsKey(sSession))
            {
                String sPlayerID = dicConversations[sSession].PlayerID;
                if (dicConversations.Values.Count(c => c.PlayerID == sPlayerID) <= 1)
                {
                    /// 기존 캡처 파일 삭제하기
                    DeleteCapture(sPlayerID);

                    /// 캡처중인 플레이어가 1개 이하인 경우 플레이어에게 캡쳐 종료 메소드 전송
                    return Config.GetConfig.CaptureProcessEventMethod(false, sPlayerID);
                }
            }

            return true;
        }

        /// <summary>
        /// 캡쳐 파일을 쓴다.
        /// </summary>
        /// <param name="pid"></param>
        /// <param name="arrImages"></param>
        /// <returns></returns>
        public bool SaveCapture(String pid, byte[] arrImages)
        {
            /// 관리 세션안에 해당 PID가 없는 경우는 마지막으로 저장하고 플레이어에게 캡쳐 중단 메시지를 전달한다.
            bool bRet = HasPlayerIDInSession(pid);
            {
                lock (lockObject)
                {
                    try
                    {
                        using (System.IO.FileStream f = new System.IO.FileStream(String.Format("{0}{1}.png", Saved_Path, pid), System.IO.FileMode.Create))
                        {
                            f.Write(arrImages, 0, arrImages.Length);
                            f.Close();
                        }

                        return bRet;
                    }
                    catch (Exception ex) { logger.Error(ex.Message); }
                }
            }

            return false;
        }

        /// <summary>
        /// 기존 캡쳐 파일 삭제하기
        /// </summary>
        /// <param name="pid"></param>
        /// <returns></returns>
        public bool DeleteCapture(String pid)
        {
            try
            {
                System.IO.File.Delete(String.Format("{0}{1}.png", Saved_Path, pid));

                return true;
            }
            catch (Exception ex) { logger.Error(ex.Message); }

            return false;
        }

        /// <summary>
        /// 캡처 가져오기
        /// </summary>
        /// <param name="sSession"></param>
        /// <returns></returns>
        public byte[] GetCapture(String sSession)
        {
            byte[] arrBytes = null;

            lock (lockObject)
            {
                try
                {
                    CaptureSession c = dicConversations[sSession];

                    using(System.IO.FileStream f = System.IO.File.OpenRead(String.Format("{0}{1}.png", Saved_Path, c.PlayerID)))
                    {
                        arrBytes = new byte[f.Length];
                        f.Read(arrBytes, 0, (int)f.Length);
                        f.Close();
                    }
                   

                }
                catch { arrBytes = null; }
            }

            return arrBytes;
        }

        /// <summary>
        /// 소멸자 함수
        /// </summary>
        public void Uninitialize()
        {
            logger.Info("CaptureService is ended.");

        }
    }
}
