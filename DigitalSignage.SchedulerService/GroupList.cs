﻿using System;
using System.Data.Common;
//using System.Data;

using NLog;

using DigitalSignage.DataBase;
using DigitalSignage.Common;
using System.Data.Odbc;

namespace DigitalSignage.SchedulerService
{
    class GroupList : MarshalByRefObject, IGroupList
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

		public static void WriteEventLog(OdbcException e)
		{
			string errorMessages = "";

			for (int i = 0; i < e.Errors.Count; i++)
			{
				errorMessages += "Index #" + i + "\n" +
								 "Message: " + e.Errors[i].Message + "\n" +
								 "NativeError: " + e.Errors[i].NativeError.ToString() + "\n" +
								 "Source: " + e.Errors[i].Source + "\n" +
								 "SQL: " + e.Errors[i].SQLState + "\n";
			}

			using (System.Diagnostics.EventLog log = new System.Diagnostics.EventLog())
			{
				log.Source = "i-Vision Service";
				log.WriteEntry(errorMessages);
				log.Dispose();
			}
			logger.Error(errorMessages);

		}

		public DataSet.groupsDataTable GetGroupByID(string id)
        {
            Config cfg = Config.GetConfig;
            try
            {
				using(DataBase.DataSetTableAdapters.groupsTableAdapter da = cfg.dbLayer.CreateGroupDA())
				{
					using (DataSet.groupsDataTable table = da.GetData(id))
					{
						da.Dispose();
						return table;
					}

				}

            }
			catch (OdbcException e)
			{
				WriteEventLog(e);
			}
            catch (Exception e)
            {
                logger.Log(LogLevel.Error, e + "");
            }
            return null;
        }

        public int AddGroups(DataSet.groupsDataTable newGroups)
        {
            Config cfg = Config.GetConfig;
            try
            {
				using (DataBase.DataSetTableAdapters.groupsTableAdapter da = cfg.dbLayer.CreateGroupDA())
				{
					for (int i = 0; i < newGroups.Rows.Count; i++)
					{
						DataSet.groupsRow row = (DataSet.groupsRow)newGroups.Rows[i];
						da.AddGroup(row.gid, row.pid, row.name, row.source, row.username, row.password, row.path);
					}
					da.Dispose();
				}
                return 0;
            }
			catch (OdbcException e)
			{
				WriteEventLog(e);
			}
			catch (Exception e)
            {
                logger.Log(LogLevel.Error, e + "");
            }
            return -1;
        }

        public int UpdateGroups(DataSet.groupsDataTable groups)
        {
            Config cfg = Config.GetConfig;
            try
            {
				using (DataBase.DataSetTableAdapters.groupsTableAdapter da = cfg.dbLayer.CreateGroupDA())
				{
					for (int i = 0; i < groups.Rows.Count; i++)
					{
						DataSet.groupsRow row = (DataSet.groupsRow)groups.Rows[i];
						logger.Info("Modifying group with GID=" + row.gid);
						da.UpdateGroup(row.path, row.password, row.username, row.pid, row.name, row.source, row.gid);

						//	살아있는 연결에 다운로드 시간대를 갱신한다.
						cfg.UpdateTimeScope(row.gid, "ALL");
					}
					da.Dispose();
				}
                return 0;
            }
			catch (OdbcException e)
			{
				WriteEventLog(e);
			}
			catch (Exception e)
            {
                logger.Log(LogLevel.Error, e + "");
            }
            return -1;
        }

        public int DeleteGroups(DataSet.groupsDataTable groups)
        {
            Config cfg = Config.GetConfig;
            try
            {
				using (DataBase.DataSetTableAdapters.groupsTableAdapter da = cfg.dbLayer.CreateGroupDA())
				{
					for (int i = 0; i < groups.Rows.Count; i++)
					{
						DataSet.groupsRow row = (DataSet.groupsRow)groups.Rows[i];
						logger.Info("Deleting group with GID=" + row.gid);
						da.DeleteGroup(row.gid);
					}
					da.Dispose();
				}
                return 0;
            }
			catch (OdbcException e)
			{
				WriteEventLog(e);
			}
			catch (Exception e)
            {
                logger.Log(LogLevel.Error, e + "");
            }
            return -1;
        }

		public DataSet.groupsDataTable GetGroupsByPID(string pid)
        {
            Config cfg = Config.GetConfig;
            try
            {
				using (DataBase.DataSetTableAdapters.groupsTableAdapter da = cfg.dbLayer.CreateGroupDA())
				{
					//logger.Info( "PID = "+pid + " ");
					using (DataSet.groupsDataTable result = da.GetDataByParent(pid))
					{
						da.Dispose();
						return result;
					}
				}
			}
			catch (OdbcException e)
			{
				WriteEventLog(e);
			}
			catch (Exception e)
            {
                logger.Log(LogLevel.Error, e + "");
            }
            return null;            
        }

		public DataSet.groupsDataTable GetGroups()
		{
            Config cfg = Config.GetConfig;
			try
			{
                using (DataBase.DataSetTableAdapters.groupsTableAdapter da = cfg.dbLayer.CreateGroupDA())
				{
					using (DataSet.groupsDataTable result = da.GetDataAll())
					{
						da.Dispose();
						return result;
					}
				}
			}
			catch (OdbcException e)
			{
				WriteEventLog(e);
			}
			catch (Exception e)
            {
                logger.Log(LogLevel.Error, e + "");
            }
            return null;
		}
    }
}
