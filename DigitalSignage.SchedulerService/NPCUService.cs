﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NLog;
using DigitalSignage.EventReciever;
using DigitalSignage.Common;
using System.ComponentModel;

namespace DigitalSignage.SchedulerService
{
    public class NPCUService
    {
        static readonly object lockObject = new object();

		/// <summary>
		/// NPCU 서비스가 돌고 있는지
		/// </summary>
		public bool IsConnected = false;

        /// <summary>
        /// NPCU 대화 세션
        /// </summary>
        public class NPCUConversation
        {
            /// <summary>
            /// 생성자
            /// </summary>
            /// <param name="pid"></param>
            /// <param name="request"></param>
            public NPCUConversation(String pid, String request)
            {
                PlayerID = pid;
                RequestCode = request;
                RequestDT = DateTime.Now;
            }

            public DateTime RequestDT = DateTime.MinValue;
            public String PlayerID = String.Empty;
            public String RequestCode = "00";

            public String ResponseCode = String.Empty;

            public bool Responsed
            {
                get { return !String.IsNullOrEmpty(ResponseCode); }
            }
        }

        private static int nUniqID = 0;

        private Dictionary<int, NPCUConversation> dicConversations = new Dictionary<int, NPCUConversation>();


        private static Logger logger = LogManager.GetCurrentClassLogger();

        #region Single Tone 객체 관련
        private static NPCUService instance = null;
        private static readonly object objLock = new object();

        /// <summary>
        /// 싱글톤 객체 인스턴스
        /// </summary>
        public static NPCUService GetInstance
        {
            get
            {
                lock (objLock)
                {
                    if (instance == null)
                    {
                        instance = new NPCUService();
                    }
                    return instance;
                }
            }
        }
        #endregion

        int nModuleID = -1;

        /// <summary>
        /// 이벤트 리시버
        /// </summary>
        private EventReciever.EventReciever eventListener = null;

        BackgroundWorker _thConversationKiller = null;

        /// <summary>
        /// 초기화 함수
        /// </summary>
        public void Initialize()
        {
            if (eventListener != null) return;

			IsConnected = false;

            eventListener = new EventReciever.EventReciever(ServerSettings.NPCUServiceMethod, ServerSettings.NPCUServicePort);
            eventListener.Start();
            eventListener.OnEventOccured += new RecieverEventHandler(eventListener_OnEventOccured);

            if (_thConversationKiller == null)
            {
                _thConversationKiller = new BackgroundWorker();
                _thConversationKiller.WorkerSupportsCancellation = true;
                _thConversationKiller.DoWork += new DoWorkEventHandler(_thConversationKiller_DoWork);
 
            }

            if (_thConversationKiller != null && !_thConversationKiller.IsBusy)
            {
                _thConversationKiller.RunWorkerAsync();

            }
            logger.Info("NPCUService is started.");
        }

        void _thConversationKiller_DoWork(object sender, DoWorkEventArgs e)
        {
            logger.Info("NPCUService Background Worker is started.");

            int nCount = 0;
            TimeSpan tsThreeMin = TimeSpan.FromMinutes(1);
            while (!e.Cancel)
            {
                if (nCount++ > 60)
                {
                    DateTime dtNow = DateTime.Now;

                    List<int> arrRemoveIds = new List<int>();

                    lock (lockObject)
                    {
                        foreach (int id in dicConversations.Keys)
                        {
                            if ((dtNow - dicConversations[id].RequestDT) > tsThreeMin)
                            {
                                arrRemoveIds.Add(id);
                                logger.Debug("Session will be terminated: {0}", id);
                            }
                        }
                    }

                    lock (lockObject)
                    {
                        foreach (int remove in arrRemoveIds)
                        {
                            dicConversations.Remove(remove);
                        }
                    }

                    nCount = 0;
                }
                System.Threading.Thread.Sleep(1000);
            }
        }

        /// <summary>
        /// 제어 요청
        /// </summary>
        /// <param name="sPlayerID"></param>
        /// <param name="RequestCode"></param>
        /// <returns></returns>
        public int SendControlRequest(String sPlayerID, String RequestCode)
        {
			int nID = 0;
            lock (lockObject)
            {
				nID = nUniqID++;
                dicConversations.Add(nID, new NPCUConversation(sPlayerID, RequestCode));

                try
                {
                    if (!eventListener.SendMessage(nModuleID, (int)NPCUCommandType.RequestCommand, String.Format("{0}|{1}", sPlayerID, RequestCode))) dicConversations[nID].ResponseCode = "98";
                }
                catch (Exception ex)
                {
					logger.Error("SendControlRequest Exception: " + ex.Message);
                    /// 서버 접속 오류 
                    dicConversations[nID].ResponseCode = "98";
                }
            }
            return nID;

        }

        /// <summary>
        /// 제어 반환
        /// </summary>
        /// <param name="nID"></param>
        /// <returns></returns>
        public String GetControlResponse(int nID)
        {
            try
            {
                lock (lockObject)
                {
                    NPCUConversation session = dicConversations[nID];
                    if (session.Responsed)
                        return session.ResponseCode;
                    else return null;
                }
            }
            catch { return "99"; /*세션 타임 아웃*/}
        }

        /// <summary>
        /// 소멸자 함수
        /// </summary>
        public void Uninitialize()
        {
            if (eventListener != null)
            {
                eventListener.Stop();

                eventListener = null;

                logger.Info("NPCUService is ended.");
            }

        }
        
        void eventListener_OnEventOccured(object sender, RecieverEventArgs e)
        {
            this.nModuleID = e.EventModuleID;

            try
            {
				IsConnected = true;

                if (e.EventCode == (int)NPCUCommandType.ResponseCommand)
                {
                    Dictionary<String, String> dicData = ParseResponseValue(e.EventValues);
                    string pid = dicData["PID"];
                    string resp = dicData["RESPONSE"];
                    string req = dicData["REQUEST"];
                    lock (lockObject)
                    {
                        foreach (NPCUConversation c in dicConversations.Values)
                        {
                            if (c.PlayerID.Equals(pid) && c.RequestCode.Equals(req) && String.IsNullOrEmpty(c.ResponseCode))
                            {
                                c.ResponseCode = resp;
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex) { logger.Error("Error: " + e.EventValues + " : " + ex.Message); }
        }

        /// <summary>
        /// NPCU에서 넘어온 데이터를 파싱한다.
        /// </summary>
        /// <param name="values"></param>
        /// <returns></returns>
        private Dictionary<String, String> ParseResponseValue(String values)
        {
            if (String.IsNullOrEmpty(values)) return null;

            Dictionary<String, String> arrDictionary = new Dictionary<string, string>();
            String[] arrTemp = values.Split(')');
            foreach (string temp in arrTemp)
            {
                try
                {
                    string[] arrKeyValue = temp.Trim(new char[] { '(', ' ' }).Split('=');
                    arrDictionary.Add(arrKeyValue[0], arrKeyValue[1]);
                }
                catch { }
            }

            return arrDictionary;
        }
    }
}
