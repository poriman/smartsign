﻿using System;
//using System.IO;

using System.Collections.Generic;
using System.ComponentModel;
//using System.Data;
using System.Diagnostics;
using System.ServiceProcess;

using NLog;
using NLog.Targets;
using NLog.Targets.Wrappers;

using System.ServiceModel;
using IVisionService;
using System.ServiceModel.Description;
using DigitalSignage.Common;
using DigitalSignage.EventReciever;
using DigitalSignage.SchedulerService.Statistics;

namespace DigitalSignage.SchedulerService
{
    public partial class SchedulerService : ServiceBase
    {
		private static Logger logger = LogManager.GetCurrentClassLogger();
		
		Config cfg = Config.GetConfig;
		
		CacheObject cache = null;

		ClientManager clientMgr = null;

		ServerThread thread = null;

		BackgroundWorker bwDataGeneration = null;

		BroadcastServer broadcastServer = null;

        NPCUService npcuservice = null;

        ReportService reportservice = null;

        ControlService controlservice = null;

        CaptureService captureservice = null;

        SendUpdateService sendupdateservice = null;

 		public SchedulerService()
        {
            InitializeComponent();
		}

		protected void Initialize_Log_Object()
		{
			// configuring log output
			FileTarget target = new FileTarget();
			target.Layout = "${longdate}\t[${level}]\t[${callsite}]\t${message}";

			target.FileName = cfg.AppData + "Service.Log.txt";
			target.ArchiveFileName = cfg.AppData + "archives/Service.Log.{#####}.txt";
			target.MaxArchiveFiles = 31;
            target.ArchiveEvery = FileArchivePeriod.Day;// FileTarget.ArchiveEveryMode.Day;

            target.ArchiveNumbering = ArchiveNumberingMode.Sequence;// FileTarget.ArchiveNumberingMode.Sequence;
			// this speeds up things when no other processes are writing to the file
			target.ConcurrentWrites = true;

			NLog.Config.SimpleConfigurator.ConfigureForTargetLogging(target, LogLevel.Debug);


		}


		#region WCF Objects
		ServiceHost host = null;
		protected bool Initialize_WCF_Objects()
		{
			try
			{
				Uri uri = new Uri(ServerSettings.Message_Address);
				host = new ServiceHost(typeof(IVisionService.NetworkService), uri);

				NetTcpBinding tcpBinding = new NetTcpBinding(SecurityMode.None, true);
				//Updated: to enable file transefer of 64 MB
				tcpBinding.MaxBufferPoolSize = (int)67108864;
				tcpBinding.MaxBufferSize = 67108864;
				tcpBinding.MaxReceivedMessageSize = (int)67108864;
				tcpBinding.TransferMode = TransferMode.Buffered;
				tcpBinding.ReaderQuotas.MaxArrayLength = 67108864;
				tcpBinding.ReaderQuotas.MaxBytesPerRead = 67108864;
				tcpBinding.ReaderQuotas.MaxStringContentLength = 67108864;
				tcpBinding.SendTimeout = new TimeSpan(0, 0, ServerSettings.Send_Timeout);

				tcpBinding.MaxConnections = ServerSettings.MaxConnections;
				//To maxmize MaxConnections you have to assign another port for mex endpoint

				//and configure ServiceThrottling as well
				ServiceThrottlingBehavior throttle;
				throttle = host.Description.Behaviors.Find<ServiceThrottlingBehavior>();
				if (throttle == null)
				{
					throttle = new ServiceThrottlingBehavior();
					throttle.MaxConcurrentCalls = ServerSettings.MaxConnections;
					throttle.MaxConcurrentSessions = ServerSettings.MaxConnections;
					host.Description.Behaviors.Add(throttle);
				}

				//Enable reliable session and keep the connection alive for 20 hours.
				tcpBinding.ReceiveTimeout = new TimeSpan(20, 0, 0);
				tcpBinding.ReliableSession.Enabled = true;
				tcpBinding.ReliableSession.InactivityTimeout = new TimeSpan(20, 0, 10);

				host.AddServiceEndpoint(typeof(IVisionService.INetworkService), tcpBinding, "tcp");

				//Define Metadata endPoint, So we can publish information about the service
				ServiceMetadataBehavior mBehave = new ServiceMetadataBehavior();
				host.Description.Behaviors.Add(mBehave);

				uri = new Uri(ServerSettings.File_Address);
				host.AddServiceEndpoint(typeof(IMetadataExchange),
					MetadataExchangeBindings.CreateMexTcpBinding(), uri);

				host.Open();

				host.Faulted += new EventHandler(host_Faulted);
				host.Closed += new EventHandler(host_Closed);
				logger.Info("Max Connection = " + ServerSettings.MaxConnections.ToString());
				logger.Info("Reconnect Timeout = " + ServerSettings.Send_Timeout.ToString());

			}
			catch (Exception e)
			{
				logger.Error(e + "");

				return false;
			}
			finally
			{
				if (host.State == CommunicationState.Opened)
				{
					logger.Info("NetworkService Open Success!");
				}
			}

			return true;
		}

		void host_Closed(object sender, EventArgs e)
		{
			try
			{
				logger.Error("WCF Closed " + sender.ToString());
			}
			catch
			{
				logger.Error("WCF Closed Exception!!");
			}
		}

		void host_Faulted(object sender, EventArgs e)
		{
			try
			{
				logger.Error("WCF Faulted " + sender.ToString());

				if (host != null)
				{
					try
					{
						host.Close();
						logger.Info("WCF Object is closed... And WCF Object will be recovered.");
					}
					catch (Exception ex)
					{
						logger.Info("Host : " + ex.Message.ToString() + "......");
					}
					finally
					{
						host = null;
					}
				}

				Initialize_WCF_Objects();
			}
			catch
			{
				logger.Error("WCF Faulted Exception!!");
			}
		}
		#endregion

		#region Broadcast Object
		protected bool Initialze_Broadcast_Server_Object()
		{
			try
			{
				broadcastServer = new BroadcastServer();
				broadcastServer.BeginServer += new EventHandler(broadcastServer_BeginServer);
				broadcastServer.EndServer += new EventHandler(broadcastServer_EndServer);
				broadcastServer.RecvMessage += new SocketEventHandler(broadcastServer_RecvMessage);
				broadcastServer.Initialiaze(ServerSettings.Initial_Code);

				return true;
			}
			catch (System.Exception e)
			{
				logger.Error(e.ToString());
			}
			return false;
		}

		void broadcastServer_RecvMessage(object sender, SocketEventArgs e)
		{
			logger.Info(String.Format("Broadcast>Received : IP({0}), HOST({1})", e.IPEndpoint.Address.ToString(), e.ReceivedData));
		}

		void broadcastServer_EndServer(object sender, EventArgs e)
		{
			logger.Info("Ending Broadcast Server...");
		}

		void broadcastServer_BeginServer(object sender, EventArgs e)
		{
			logger.Info("Starting Broadcast Server...");
		}
		#endregion

		#region RefData Objects

		RefDataService refThread = null;
		protected bool Initialize_RefData_Objects()
		{
			try
			{
				refThread = new RefDataService();
				refThread.Start();
			}
			catch (Exception eee)
			{
				logger.Error(eee.Message);
				return false;
			}
			return true;
		}
		#endregion

        protected void Initialize_Reporting_Service()
        {
            if (reportservice != null) reportservice.Stop();

            reportservice = new ReportService();
            reportservice.Start();
        }


		protected void InitializeCacheObjects()
		{
			cache = CacheObject.GetInstance;

			cache.Initialize();
		}

		protected void InitializeClientManager()
		{
			clientMgr = ClientManager.GetInstance;

			clientMgr.Initialize();
		}

        protected void InitializeEventReciever()
        {
            npcuservice = NPCUService.GetInstance;
            
            npcuservice.Initialize();

            logger.Info("Started Event Listener...");
        }

        protected void InitializeControlService()
        {
            controlservice = ControlService.GetInstance;

            controlservice.Initialize();

            logger.Info("Started Control Service...");
        }

        protected void InitializeCaptureService()
        {
            captureservice = CaptureService.GetInstance ;

            captureservice.Initialize();

            logger.Info("Started Capture Service...");
        }

        protected void InitializeSendUpdateService()
        {
            sendupdateservice = SendUpdateService.GetInstance;

            sendupdateservice.Initialize();

            logger.Info("Started SendUpdate Service...");
        }

		#region Trial Key Check
		protected bool CheckTrial()
		{
			if (!cfg.InitializeTrialChecker())
				return false;

			return true;
		}
		#endregion

		#region Serial Key Check
		protected bool CheckSerialKey()
		{
			if (cfg.InitializeSerialKey())
				return true;

			return false;
		}
		#endregion

		#region Uninitialize Objects
		public void UninitializeObjects()
		{
			try
			{
				thread.Stop();
				thread = null;
			}
			catch (Exception ex)
			{
				logger.Info("Remote" + ex.Message.ToString());
			}
			finally
			{
				logger.Info("Remote Terminating service");
			}

			if (host != null)
			{
				try
				{
					host.Close();
				}
				catch (Exception ex)
				{
					logger.Info("Host : " + ex.Message.ToString() + "......");
				}
				finally
				{
					if (host.State == CommunicationState.Closed)
					{
						logger.Info("NetworkService Close...........");
					}

					host = null;
				}
			}

			if (refThread != null)
			{
				try
				{
					refThread.Stop();
					refThread = null;

					logger.Info("RefDataService Close........");
				}
				catch (Exception ee)
				{
					logger.Error(ee.Message);
				}
			}

			if (clientMgr != null)
			{
				try
				{
					clientMgr.Uninitialize();

					clientMgr = null;

					logger.Info("Client Manager Close........");
				}
				catch (Exception ee)
				{
					logger.Error(ee.Message);
				}
			}

            if (npcuservice != null)
            {
                npcuservice.Uninitialize();

                npcuservice = null;
                
                logger.Info("Stopped Event Listener.........");
            }

            if (captureservice != null)
            {
                captureservice.Uninitialize();

                captureservice = null;

                logger.Info("Stopped Capture Service.........");
            }

            if (controlservice != null)
            {
                controlservice.Uninitialize();

                controlservice = null;

                logger.Info("Stopped Control Service.........");
            }

            if (sendupdateservice != null)
            {
                sendupdateservice.Uninitialize();

                sendupdateservice = null;

                logger.Info("Stopped Send Update Service.........");
            }

			if (cache != null)
			{
				try
				{
					cache.Uninitialize();

					cache = null;

					logger.Info("Cache Object Close........");
				}
				catch (Exception ee)
				{
					logger.Error(ee.Message);
				}
			}

			if(broadcastServer != null)
			{
				broadcastServer.Uninitialize();
				broadcastServer = null;
			}

            if (reportservice != null)
            {
                reportservice.Stop();
                reportservice = null;
            }
		}
		#endregion

		protected void PrepareData(object sender, DoWorkEventArgs e)
		{
			try
			{
				logger.Info("Configuration Class init..");

				cfg.init();

				logger.Info("Begin to prepare Data...");

				/*
				logger.Info("Oracle Connecting.." );
				using (System.Data.Odbc.OdbcConnection con = cfg.serverDBLayer.GetAvailableConnection())
				{
					using (ServerDatabase.ServerDataSet.rolesDataTable dt = new DigitalSignage.ServerDatabase.ServerDataSet.rolesDataTable())
					{
						logger.Info("Oracle Connected..");

						String sQuery = String.Format("SELECT  * FROM  roles WHERE role_id = ?");
						logger.Info("Oracle Prepare Query: " + sQuery);

						using (System.Data.Odbc.OdbcCommand comm = new System.Data.Odbc.OdbcCommand(sQuery, con))
						{
							logger.Info("Oracle Binding..");
							comm.Parameters.Add(new System.Data.Odbc.OdbcParameter("role_id", System.Data.Odbc.OdbcType.Decimal, 19, "role_id"));
							comm.Parameters["role_id"].Value = 1;

							logger.Info("Oracle Commanding..");
							using (System.Data.Odbc.OdbcDataReader dr = comm.ExecuteReader())
							{
								logger.Info("Oracle Commanded..");

								dt.Load(dr);


								logger.Info("Oracle Returned rows.." + dt.Rows.Count);

								dr.Close();
								dr.Dispose();
							}

							comm.Dispose();
						}
					}

				}
				logger.Info("Oracle Test Success..");
				*/

				//	Cache Object를 생성한다.
				InitializeCacheObjects();
				//	Client Manager를 생성한다.
				InitializeClientManager();
                //  NCPU 서비스를 시작한다.
                InitializeEventReciever();
                //  컨트롤 서비스를 시작한다.
                InitializeControlService();
                //  캡쳐 서비스를 시작한다.
                InitializeCaptureService();
                //  스케줄 변경 사항 추적 서비스를 시작한다.
                InitializeSendUpdateService();

				thread = new ServerThread();
				thread.Start();

				logger.Info("Finish to prepare Data...");
				e.Result = true;
			}
			catch (System.Exception err)
			{
				logger.Error(err.ToString());
                try
                {
                    this.Stop();
                }
                catch { }
			}

		}

		protected override void OnStart(string[] args)
        {
            try
			{
				Initialize_Log_Object();

                /*
				if(!CheckTrial())
				{
					logger.Error("SmartSign has not been installed.");
					this.Stop();
					return;
				}
                 */

				//	키인증
				if (CheckSerialKey())
				{
					bwDataGeneration = new BackgroundWorker();

					bwDataGeneration.DoWork += new DoWorkEventHandler(PrepareData);
					bwDataGeneration.RunWorkerAsync();
				}
				else
				{
					this.Stop();
					return;
				}
				if(!Initialize_WCF_Objects())
				{
					this.Stop();
					return;
				}

				if(!Initialize_RefData_Objects())
				{
					this.Stop();
					return;
				}

                Initialize_Reporting_Service();

				Initialze_Broadcast_Server_Object();

			}
            catch (Exception ex)
            {
                logger.Info("Starting service Error : " + ex.Message.ToString());
            }

			logger.Info("Starting service");

        }

        protected override void OnStop()
        {
			UninitializeObjects();
        }
    }
}
