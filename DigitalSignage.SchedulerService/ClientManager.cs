﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

// using System.Collections.ObjectModel;

using System.Xml;
using System.Threading;

using IVisionService;
using NLog;
using System.Collections.ObjectModel;
using DigitalSignage.Common;
using System.ServiceModel;


namespace DigitalSignage.SchedulerService
{
	#region Client Collection 정의

	/// <summary>
	/// 클라이언트 관리를 위한 단위 오브젝트로써 각 Session 마다 하나의 오브젝트가 생성된다.
	/// </summary>
	public class ClientObject
	{
		private Topic _topic = null;
		private INetworkCallback _callback = null;
        private IContextChannel _channel = null;
		private XmlNode _xmlPlayer = null;
		private DateTime _dtConnected = DateTime.UtcNow;

		public Topic ClientTopic
		{
			get { return _topic; }
			set { _topic = value; }
		}
		public INetworkCallback ClientCallback
		{
			get { return _callback; }
			set { _callback = value; }
		}

        public IContextChannel ClientChannel
        {
            get { return _channel; }
            set { _channel = value; }
        }

		public XmlNode PlayerReference
		{
			get { return _xmlPlayer; }
			set { _xmlPlayer = value; }
		}
		public DateTime ConnectedDateTime
		{
			get { return _dtConnected; }
			set { _dtConnected = value;}
		}
	}

	#endregion

	/// <summary>
	/// ClientObject를 관리하기 위한 오브젝트로써 싱글톤으로 관리된다.
	/// </summary>
	public class ClientManager
	{

		private static Logger logger = LogManager.GetCurrentClassLogger();

		#region Single Tone 객체 관련
		private static ClientManager instance = null;
		private static readonly object objLock = new object();

		/// <summary>
		/// 싱글톤 객체 인스턴스
		/// </summary>
		public static ClientManager GetInstance
		{
			get
			{
				lock (objLock)
				{
					if (instance == null)
					{
						instance = new ClientManager();
					}
					return instance;
				}
			}
		}
		#endregion

// 		private Collection<ClientObject> m_arrClients = new Collection<ClientObject>();

		/// <summary>
		/// PID를 이용하여 조회 가능한 ClientObject의 집합이다. 
		/// </summary>
		private Dictionary<String, ClientObject> m_arrClients = new Dictionary<String, ClientObject>();
		
		/// <summary>
		/// 클라이언트의 개수
		/// </summary>
		public int ClientCount {
			get { return m_arrClients.Count; }
		}

        /// <summary>
        /// 클라이언트를 보관하고 있는 Dictionary를 반환한다.
        /// </summary>
        public Dictionary<String, ClientObject> ClientDictionary
        {
            get { return m_arrClients; }
        }

		private Thread thAliveChecker = null;

		/// <summary>
		/// 각 클라이언트가 유효한지 체크하는 스레드
		/// </summary>
		private void AliveCheckingThread()
		{
			try
			{
				while (true)
				{
					Thread.Sleep(60000);
					try
					{
						logger.Info("Alive Checking...");

						ICollection<ClientObject> _vc = m_arrClients.Values;

						ClientObject[] arrValues = m_arrClients.Values.ToArray<ClientObject>();

						for (int i = 0; i < arrValues.Length; ++i)
						{
							ClientObject obj = null;

							lock (objLock)
							{
								obj = arrValues[i];
							}

							Message msg = new Message();
							msg.Sender = obj.ClientTopic;
							msg.Result = "S_OK";
							msg.MsgType = MessageType.msg_Status;
							msg.Level = IVisionService.UserLevels.None;
							msg.Time = DateTime.Now;
							msg.Version = "";
							msg.Content = "Status";

							try
							{
								obj.ClientCallback.ReceiveWhisper(msg, obj.ClientTopic);
							}
							catch (Exception ex)
							{
								logger.Info(obj.ClientTopic.Mediaid + " has some network problem. will be disconnected. :" + ex.Message);
								SetPlayerStatus(obj.ClientTopic.Mediaid, false);

								//	접속 로그 기록
								DateTime? dtConnected = GetConnectedDateTimeFromMember(obj.ClientTopic);

								long currTicks = TimeConverter.ConvertToUTP(DateTime.Now.ToUniversalTime());

								if (dtConnected.HasValue)
									Config.GetConfig.PlayerDisconnected(obj.ClientTopic.Mediaid, TimeConverter.ConvertToUTP(dtConnected.Value.ToUniversalTime()), currTicks, (int)ErrorCodes.IS_ERROR_CONN_TIMEOUT_DISCONNECTED_BY_SERVER);
								else
                                    Config.GetConfig.PlayerDisconnected(obj.ClientTopic.Mediaid, currTicks, currTicks, (int)ErrorCodes.IS_ERROR_CONN_TIMEOUT_DISCONNECTED_BY_SERVER);

								RemoveMember(obj.ClientTopic.Mediaid);
							}
							Thread.Sleep(1);

						}
					}
					catch (Exception e)
					{
						logger.Error(e + "");
					}
				}

			}
			catch (Exception ex)
			{
				logger.Error(ex.ToString());
			}
		}

		/// <summary>
		/// 초기화 함수
		/// </summary>
		public void Initialize()
		{
			if (thAliveChecker != null) return;

			thAliveChecker = new Thread(AliveCheckingThread);
			thAliveChecker.IsBackground = true;
			thAliveChecker.Start();

			logger.Info("AliveChecking thread is started.");
		}
		
		/// <summary>
		/// 소멸자 함수
		/// </summary>
		public void Uninitialize()
		{
			thAliveChecker.Abort();
			thAliveChecker = null;

			logger.Info("AliveChecking thread is ended.");

		}

		#region Member 관련 함수
		/// <summary>
		/// 기존의 Client를 해제(제거)한다.
		/// </summary>
		/// <param name="player_id"></param>
		public void RemoveMember(string player_id)
		{
			try
			{
				lock (objLock)
				{
					m_arrClients.Remove(player_id);
				}
			}
			catch (Exception ex)
			{
				logger.Error(ex.ToString());

			}
		}

		/// <summary>
		/// 이미 Client의 플레이어가 등록되었는지 검사
		/// </summary>
		/// <param name="_topic"></param>
		/// <returns></returns>
		public bool HasMember(Topic _topic)
		{
			try
			{
				lock (objLock)
				{
					return m_arrClients[_topic.Mediaid] != null;
				}
			}
			catch { }

			return false;
		}

		public DateTime? GetConnectedDateTimeFromMember(Topic _topic)
		{
			try
			{
				lock (objLock)
				{
					return m_arrClients[_topic.Mediaid].ConnectedDateTime;
				}
			}
			catch { }

			return null;

		}

		/// <summary>
		/// 새로운 Client의 플레이어를 등록한다.
		/// </summary>
		/// <param name="_topic"></param>
		/// <param name="_cb"></param>
		public bool AddMember(Topic _topic, INetworkCallback _cb)
		{
			try
			{
				XmlNode node = CacheObject.GetInstance.FindPlayerNode(_topic.Mediaid);
				if (node == null || node.Attributes["del_yn"].Value.Equals("Y")) return false;

				logger.Info("Connect : " + node.OuterXml);
				ClientObject obj = new ClientObject();
				obj.ClientTopic = _topic;
				obj.ClientCallback = _cb;
				obj.PlayerReference = node;
				obj.ConnectedDateTime = DateTime.UtcNow;

				lock (objLock)
				{
					try
					{
						m_arrClients[_topic.Mediaid] = obj;
					}
					catch {
						m_arrClients.Add(_topic.Mediaid, obj);
					}
					
				}
				return true;
			}
			catch (Exception ex)
			{
				logger.Error(ex.ToString());
			}

			return false;
		}

        ClientObject FindClientByChannel(IContextChannel _chn)
        {
            if (_chn == null) return null;

            lock(objLock)
            {
                foreach (ClientObject obj in m_arrClients.Values)
                {
                    if (obj.ClientChannel == _chn) return obj;
                }
                return null;
            }

        }

        void ClientChannel_Faulted(object sender, EventArgs e)
        {
            logger.Error("Client Channel Has been faulted!");
            try
            {
                IContextChannel chn = sender as IContextChannel;

                if (chn != null)
                {
                    ClientObject obj = FindClientByChannel(sender as IContextChannel);
                    if (obj != null)
                    {
                        String sPlayerID = obj.ClientTopic.Mediaid;
                        RemoveMember(sPlayerID);
                        logger.Info("Client session Closed : Media ID ({0})", sPlayerID);
                    }
                }
            }
            catch { }
        }

        void ClientChannel_Closed(object sender, EventArgs e)
        {
            logger.Error("Client Channel Has been closed!");
            try
            {
                ClientObject obj = FindClientByChannel(sender as IContextChannel);
                if (obj != null)
                {
                    String sPlayerID = obj.ClientTopic.Mediaid;
                    RemoveMember(sPlayerID);
                    logger.Info("Client session Closed : Media ID ({0})", sPlayerID);
                }

            }
            catch { }
        }

		public INetworkCallback GetCallback(string playerid)
		{
			ClientObject obj = null;
			obj = m_arrClients[playerid];

			if (obj == null) return null;

			return obj.ClientCallback;

		}
		#endregion

		#region Functions
		/// <summary>
		/// 플레이어의 접속상태만 변경한다.
		/// </summary>
		/// <param name="sPlayerID"></param>
		/// <param name="_bIsOnline"></param>
		public void SetPlayerStatus(string sPlayerID, bool _bIsOnline)
		{
			try
			{
				ClientObject obj = null;
				lock (objLock)
				{
					obj = m_arrClients[sPlayerID];
				}

				if (obj.PlayerReference == null) logger.Error("obj.PlayerReference NULL");
				if (obj.PlayerReference.Attributes["state"] == null) logger.Error("obj.PlayerReference NULL");
				if (obj.PlayerReference.Attributes["_timestamp"] == null) logger.Error("obj.PlayerReference NULL");

				obj.PlayerReference.Attributes["state"].Value = _bIsOnline == true ? "1" : "0";
				obj.PlayerReference.Attributes["_timestamp"].Value = DateTime.UtcNow.Ticks.ToString();
				
				obj.PlayerReference.Attributes["lastconndt"].Value = DateTime.UtcNow.Ticks.ToString();
			}
			catch /*(Exception ex)*/
			{
// 				logger.Error(ex.ToString());
			}
		}

		/// <summary>
		/// IPv4 Address, Mac Address, DHCP 여부를 갱신한다.
		/// </summary>
		/// <param name="sPlayerID"></param>
		/// <param name="ipv4Addr"></param>
		/// <param name="macAddr"></param>
		/// <param name="isDHCP"></param>
		/// <param name="authid"></param>
		/// <param name="authpass"></param>
		/// <param name="mntType"></param>
		/// <returns></returns>
		public bool UpdatePlayerDetailInformation(string sPlayerID, string ipv4Addr, string macAddr, string isDHCP, string authid, string authpass, string mntType)
		{
			try
			{
				ClientObject obj = null;
				lock (objLock)
				{
					obj = m_arrClients[sPlayerID];
				}
				obj.PlayerReference.Attributes["_ipv4addr"].Value = ipv4Addr;
				obj.PlayerReference.Attributes["_macaddr"].Value = macAddr;
				obj.PlayerReference.Attributes["_is_dhcp"].Value = isDHCP;
				obj.PlayerReference.Attributes["_auth_id"].Value = authid;
				obj.PlayerReference.Attributes["_auth_pass"].Value = authpass;
				obj.PlayerReference.Attributes["_mnttype"].Value = mntType;

				obj.PlayerReference.Attributes["_timestamp"].Value = DateTime.UtcNow.Ticks.ToString();

				return true;
			}
			catch (Exception ex) { logger.Error(ex.ToString()); }

			return false;
		}

		/// <summary>
		/// 플레이어의 XML 데이터를 갱신한다.
		/// </summary>
		/// <param name="message">이 파라메터는 플레이어가 전달하는 Status의 나열 정보이다.</param>
		/// <returns></returns>
		public bool UpdatePlayerInformation(string sPlayerID, string message)
		{
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(message);

                XmlNode node = doc.SelectSingleNode("//child::Status");

                if (node != null)
                {
                    ClientObject obj = null;
                    lock (objLock)
                    {
                        obj = m_arrClients[sPlayerID];
                    }
                    obj.PlayerReference.Attributes["versionH"].Value = node.Attributes["versionH"].Value;
                    obj.PlayerReference.Attributes["versionL"].Value = node.Attributes["versionL"].Value;
                    obj.PlayerReference.Attributes["freespace"].Value = node.Attributes["freespace"].Value;
                    obj.PlayerReference.Attributes["uptime"].Value = node.Attributes["uptime"].Value;
                    obj.PlayerReference.Attributes["temperature"].Value = node.Attributes["temperature"].Value;
                    obj.PlayerReference.Attributes["host"].Value = node.Attributes["host"].Value;
                    obj.PlayerReference.Attributes["cpurate"].Value = node.Attributes["cpurate"].Value;
                    obj.PlayerReference.Attributes["memoryusage"].Value = node.Attributes["memoryusage"].Value;
                    obj.PlayerReference.Attributes["all_d_progress"].Value = node.Attributes["download1"].Value;
                    obj.PlayerReference.Attributes["curr_d_progress"].Value = node.Attributes["download2"].Value;
                    obj.PlayerReference.Attributes["curr_screen"].Value = node.Attributes["screen"].Value;
                    obj.PlayerReference.Attributes["curr_subtitle"].Value = node.Attributes["subtitle"].Value;
                    try
                    {
                        obj.PlayerReference.Attributes["_serial_device_name"].Value = node.Attributes["serialname"].Value;
                    }
                    catch { }
                    try
                    {
                        obj.PlayerReference.Attributes["_serial_stat_power"].Value = node.Attributes["serialpower"].Value;
                    }
                    catch { }
                    try
                    {
                        obj.PlayerReference.Attributes["_serial_stat_source"].Value = node.Attributes["serialsource"].Value;
                    }
                    catch { }
                    try
                    {
                        obj.PlayerReference.Attributes["_serial_stat_volume"].Value = node.Attributes["serialvolume"].Value;
                    }
                    catch { }

                    try
                    {
                        obj.PlayerReference.Attributes["curr_screen_uuid"].Value = node.Attributes["curr_schedule"].Value;
                    }
                    catch { }

                    try
                    {
                        obj.PlayerReference.Attributes["curr_volume"].Value = node.Attributes["volume"].Value;
                    }
                    catch { }

                    obj.PlayerReference.Attributes["_timestamp"].Value = DateTime.UtcNow.Ticks.ToString();

                    return true;
                }

            }
            catch (Exception exp)
            {
                logger.Error(exp);
            }

			return false;
		}

		public bool UpdatePlayerInformation(string sPlayerID, int versionH, int versionL,
			int freespace, int uptime, int temperature, string host, int cpurate,
			int memoryusage, string allProgress, string pieceProgress,
			string currentScreen, string currentSubtitle)
		{
			try
			{
				ClientObject obj = null;
				lock (objLock)
				{
					obj = m_arrClients[sPlayerID];
				}
				if (obj.PlayerReference == null)
					return false;

				obj.PlayerReference.Attributes["versionH"].Value = versionH.ToString();
				obj.PlayerReference.Attributes["versionL"].Value = versionL.ToString();
				obj.PlayerReference.Attributes["freespace"].Value = freespace.ToString();
				obj.PlayerReference.Attributes["uptime"].Value = uptime.ToString();
				obj.PlayerReference.Attributes["temperature"].Value = temperature.ToString();
				obj.PlayerReference.Attributes["host"].Value = host;
				obj.PlayerReference.Attributes["cpurate"].Value = cpurate.ToString();
				obj.PlayerReference.Attributes["memoryusage"].Value = memoryusage.ToString();
				obj.PlayerReference.Attributes["all_d_progress"].Value = allProgress;
				obj.PlayerReference.Attributes["curr_d_progress"].Value = pieceProgress;
				obj.PlayerReference.Attributes["curr_screen"].Value = currentScreen;
				obj.PlayerReference.Attributes["curr_subtitle"].Value = currentSubtitle;
				obj.PlayerReference.Attributes["_timestamp"].Value = DateTime.UtcNow.Ticks.ToString();
				return true;
			}
			catch (Exception ex) { logger.Error(ex.ToString()); }

			return false;
		}
		#endregion
	}
}
